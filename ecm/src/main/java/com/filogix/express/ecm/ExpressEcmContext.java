/*
 * @(#)ExpressEcmContext.java    2008-3-25
 *
 * Copyright (C) 2008 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.ecm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.context.ExpressContext;
import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.context.ExpressContextDirector;

/**
 * ExpressEcmContext provides the unified interfaces to access services in
 * Express ECM context.
 *
 * @version   1.0 2008-3-25
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressEcmContext implements ExpressContext {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(ExpressEcmContext.class);

    // the key for the ecm service factory.
    private final static String EXPRESS_ECM_CONTEXT_KEY =
        "com.filogix.express.ecm";

    /**
     * returns a service instance from Express ECM context.
     *
     * @param name the service name.
     * @return an instance of the service.
     * @throw ExpressServiceException if failed to get an instance for the
     * service.
     */
    synchronized public static Object getService(String name)
        throws ExpressServiceException {

        return ExpressContextDirector.getDirector().
            getServiceFactory(EXPRESS_ECM_CONTEXT_KEY).
            getService(name);
    }
}