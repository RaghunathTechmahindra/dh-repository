package com.filogix.ecm;

public class ECMMain {

    public static void main(String[] args) throws Exception {
        
        try {
            
            //if (args.length!=0 && args[0] != null && args[0].trim().length() != 0) {

                // create a new ECMManager and pass the path of moss properties
                // to
                // it.
                System.out.println("Start ECMManager.......");
                ECMManager ecM = new ECMManager();
                // start a thread of ECMManager
                ecM.start();
//            }
//            // if no path of moss properties file, an error message should be
//            // logged.
//            else {
//                System.out.println(ECMConstants.ERROR_MESSAGE_NO_PATH_NAME);
//
//                // throw an exception to stop the program.
//                throw new Exception(ECMConstants.ERROR_MESSAGE_NO_PATH_NAME);
//            }
        }
        // if the user is not allowed to login, the ECM engine should stop too.
        catch (ECMLoginException ex) {
           System.out.println(ECMConstants.ERROR_MESSAGE_MAIN_USER_NOT_LOGIN);
           throw new Exception(ex.getMessage());
        }
        // if any exception thrown, the program should stop too .
        catch (Exception ek) {
            ek.printStackTrace();
            throw new Exception(ek.getMessage());
        }
    }
}
