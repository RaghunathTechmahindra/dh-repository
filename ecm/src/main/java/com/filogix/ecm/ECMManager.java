package com.filogix.ecm;

import org.apache.log4j.Logger;

import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;

import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.basis100.deal.calc.CalcMonitor;
import com.filogix.ecm.ECMSignInService;
import com.filogix.ecm.handler.IHandler;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ProcessStatus;
import com.basis100.deal.entity.ServiceResponseQueue;
import com.basis100.deal.security.DLM;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.util.thread.ThreadPool;


import config.Config;

public class ECMManager extends Thread { 
    private static Logger logger = Logger.getLogger(ECMManager.class);
    //private static SysLogger logger; 
    private ThreadPool threadPool;

    /**
     * 
     */
    private PropertiesCache propertiesCache;

    private SessionResourceKit srk;

    private String strUserId;

    private String strPassword;

    private int intUserProfileId;

    private static int intMaxTry; 

    private String strWaitTime;

    private static int retryInterval;

    public ECMManager() throws Exception{

        init();
    }

    private void init() throws Exception {

        // --> In order to simplify the config of the ECM, we need to share the
        // Main Properties file (Specified by passing VM Parameter -D)
        // --> and then load the ECM specific Properties (Passed as VM input
        // parameter) on top of the Main Properties to override.

        // Load the Main Peoperties file first
        try {

            propertiesCache = PropertiesCache.getInstance();


            String strThreadPoolSize = propertiesCache.getInstanceProperty(ECMConstants.PROPERTY_THREADPOOL_SIZE, "2");
            String strRetryCounter = propertiesCache.getInstanceProperty(ECMConstants.PROPERTY_RETRY_COUNTRER, "5");
                                   
            strUserId = propertiesCache.getProperty(-1, ECMConstants.PROPERTY_USER_ID, "");
            strPassword = propertiesCache.getProperty(-1, ECMConstants.PROPERTY_PASSWORD, "");
            strWaitTime = propertiesCache.getInstanceProperty(ECMConstants.PROPERTY_AMEMANAGER_INTERVAL, "5");
            intMaxTry = TypeConverter.intTypeFrom(strRetryCounter);
            
            retryInterval = TypeConverter.intTypeFrom(
                    propertiesCache.getInstanceProperty(
                            ECMConstants.PROPERTY_RETRY_INTERVAL_FOR_LOCKED_DEAL_SECONDS, "600"));//default 10 minutes 
            
            ResourceManager.init();
            srk = new SessionResourceKit(ECMConstants.IDENTITY_CLASS_NAME_AMEMANAGER);
            
            int intThreadPoolSize = TypeConverter.intTypeFrom(strThreadPoolSize);
            PicklistData.init();
            // 3.2.2GR: added 
            BXResources bxResources = new BXResources();
            DLM.init(srk, false);
            threadPool = new ThreadPool(intThreadPoolSize);
            if (!login()) {
                throw new ECMLoginException();
            }
            logger.info("init done");
        } catch (ECMLoginException le){
            logger.error(ECMConstants.ERROR_MESSAGE_LOGIN_FAILED);
            System.out.println("Login Error, System exits ......");
            System.exit(0);
        } catch (Exception ex) {
            logger.warn(ex.getMessage());
            System.out.println("Unexpected Exception occurs!");
            ex.printStackTrace();
            throw new Exception(this.getClass().getName() + ECMConstants.INEDNTITY_METHOD_NAME_INIT + ex.getMessage());
        }

        // Here should clean up all the tasks :
       
        resetLockedResponses();

        // Add the Hook to the VM to perform Logout if VM terminate
        // Getting the runtime object associated with the current Java
        // application.
        Runtime runTime = Runtime.getRuntime();
        // Initiating the shutdown hook
        ECMShutdownHook hook = new ECMShutdownHook(strUserId);
        // Registering the Shutdown Hook
        runTime.addShutdownHook(new Thread(hook));        
    }

    public void run() {
        int intWaitTime = Integer.parseInt(strWaitTime);
        long lngWaitTime = intWaitTime * 1000;
       
        try {
            for (;;) {
            	//FXP24416, Mar 5 09, added clear insitution
            	srk.getExpressState().cleanDealIds();
                ServiceResponseQueue srvResItem = getResponseQueueItem();
                if (srvResItem == null) {
                    srk.freeResources();
                    logger.info("===============Monitor is sleeping===============");
                    sleep(lngWaitTime);                    
                } else {
                    logger.info("========== One Service Response found ==> Response ID = "
                                + srvResItem.getServiceResponseQueueId() + " ==========");                          
                    try {
                        srk.getExpressState().setDealInstitutionId(srvResItem.getInstitutionProfileId());
                        intUserProfileId = ECMSignInService.getUserProfile(strUserId, srk).getUserProfileId();
                        
                        changeProcessStatus(srvResItem, ProcessStatus.PROCESSING, this.srk);
                    } catch (Exception ex) {
                        logger.warn(this.getClass().getName()+ ECMConstants.ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_SETPROCESSSTATUS);
                    }

                    ECMWorker ecmWork = new ECMWorker(srvResItem.getServiceResponseQueueId(), intUserProfileId);
                    threadPool.addRequest(ecmWork);

                    logger.info("========== Response was assigned to Worker ==>  Response ID = "
                                + srvResItem.getServiceResponseQueueId() + " ==========");
                }
            }
        }
        // Consider and try to catch InterruptedException in order to perform
        // Logout.

        catch (Exception ex) {

            logger.error(ECMConstants.ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_RUN_METHOD_OF_ECMMANAGER);
            logger.error(StringUtil.stack2string(ex));

            try {
                // Perform Logout
                logout();
            } catch (Exception ek) {
                logger.error(ECMConstants.ERROR_MESSAGE_ECMMANAGER_EXCEPTION_USER_LOGOU);
            }

        } finally {
            srk.freeResources();
        }
    }

    public ServiceResponseQueue getResponseQueueItem() throws Exception{ 

        ServiceResponseQueue srvResItem = null;
        try {
            srvResItem = new ServiceResponseQueue(srk, null);
            srvResItem.findNextUnprocessedResponse(intMaxTry, retryInterval); 
          
        } catch (Exception e) {
            logger.warn(ECMConstants.ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_GETRESPONEQUEUEITEM);
            return null;
        }
                                                                                
        return srvResItem;
    }

    //Change ServiceResponseQueue processStatus to newStaus
    public static void changeProcessStatus(ServiceResponseQueue srvRespItem, int newStatus, SessionResourceKit srkt) {
        
        try {
            srkt.beginTransaction();
            srvRespItem.setProcessStatusId(newStatus);            
            srvRespItem.updateEntity();
            srkt.commitTransaction();
        } catch (Exception e) {
            logger.warn(ECMConstants.ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_SETPROCESSSTATUS);
            srkt.cleanTransaction();
            e.printStackTrace();
        }
    }
    
    //Increment the ServiceResponseQueue reTryCounter
    public static void incrRetryCounter(ServiceResponseQueue srvRespItem, int step, SessionResourceKit srkt) {
        if(srvRespItem.getRetryCounter()< intMaxTry){
            try {
                srkt.beginTransaction();
            
                srvRespItem.setRetryCounter(srvRespItem.getRetryCounter() + step);            
                srvRespItem.updateEntity();
                srkt.commitTransaction();
            } catch (Exception e) {
                logger.warn(ECMConstants.ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_INCRRETRYCOUNTER);
                srkt.cleanTransaction();
                e.printStackTrace();
            }     
        }
    }

    public static void updateToRetryForLockedDeal(ServiceResponseQueue servResp) {
        
        try {
            servResp.setLastProcessDateDBSystemDate();
            servResp.setProcessStatusId(ProcessStatus.AVAILABLE);
            servResp.updateEntity();
        } catch (Exception e) {
            logger.warn(ECMConstants.ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_SETPROCESSSTATUS);
            servResp.srk.cleanTransaction();
            e.printStackTrace();
        }
    }
    
    private boolean login() throws Exception {

        return ECMSignInService.login(strUserId, strPassword);

    }

    private void logout() throws Exception {

        ECMSignInService.logout(strUserId);

    }

    private void resetLockedResponses() {
        try {
            ServiceResponseQueue srvRespQueue = new ServiceResponseQueue(srk, null);

            srk.beginTransaction();
            int intAffected = srvRespQueue.resetLockedResponse();
            logger.info("ECMManager.resetLockedResponses(): " + intAffected + " items affected.");
            srk.commitTransaction();
        } catch (Exception ex) {
            srk.cleanTransaction();
            logger.error("ECMManager.resetLockedResponses(): Error resetting locked responses.");
            ex.printStackTrace();
        } finally {
            srk.freeResources();
        }
    }

    public class ECMShutdownHook implements Runnable {
        private String userLoginId;

        // this is the shutdown hook thread
        public ECMShutdownHook(String userLoginId) {
            this.userLoginId = userLoginId;
            System.out.println("*** ECM Shutdown Hook init ***");
        }

        /*
         * run method of shutdown hook thread, to display message when started
         */
        public void run() {
            System.out.println("*** ECM Shutdown Hook Started ***");
            try {
                ECMSignInService.logout(userLoginId);
            } catch (Exception e) {
                System.out.println("*** ECM Shutdown Hook Exception encounted !! ***");
                System.out.println(e);
            }            
            System.out.println("*** ECM Shutdown Hook Ended ***");
        }
    }

    public static int getIntMaxTry() {
        return intMaxTry;
    }
    

}
