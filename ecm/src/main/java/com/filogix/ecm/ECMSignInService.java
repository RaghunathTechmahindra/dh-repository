package com.filogix.ecm;

import MosSystem.Sc;

import com.basis100.resources.PropertiesCache;
import com.basis100.deal.security.PasswordEncoder;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.security.DLM;
import java.util.Date;
import java.util.Calendar;
import com.basis100.deal.entity.UserProfile;
public class ECMSignInService {

	private static SessionResourceKit srk;
	static {
		srk = new SessionResourceKit(ECMConstants.IDENTITY_CLASS_NAME_ECMSIGNINSERVICE);
	}

	public ECMSignInService() {

	}

	/**
	 * In order to resue the login serivce later, the method is made to static.
	 * @param i_strUserId String : User id.
	 * @param i_strPw String : password
	 * @throws Exception
	 * @return boolean
	 */
	public static void logout(String i_strUserId) throws Exception {
		try {
			UserProfile userProfile = getUserProfile(i_strUserId, srk);
			int intUserProfielId = userProfile.getUserProfileId();
			DLM dlm = DLM.getInstance();
			srk.beginTransaction();
			dlm.unlockAnyByUser(intUserProfielId, srk, true);
			userProfile.setLoginFlag(ECMConstants.NO);
			userProfile.setLastLogoutTime(new Date());
			userProfile.ejbStore();
			srk.commitTransaction();
		} catch (Exception ex) {
			srk.cleanTransaction();
			throw ex;
		} finally {
			srk.freeResources();
		}
	}

	public static UserProfile getUserProfile(String i_strUserId,
			SessionResourceKit i_srk) throws Exception {
		UserProfile userProfile = new UserProfile(i_srk);
		userProfile.findByLoginId(i_strUserId);
		return userProfile;
	}

	public static boolean login(String strUserId, String i_strPw)
			throws Exception {
		try {

			UserProfile userProfile = getUserProfile(strUserId, srk);
			int intProfileStatusId = userProfile.getProfileStatusId();

			// user status has to be active.
			if (intProfileStatusId == ECMConstants.USER_PROFILE_STATUS_ACTIVE) {
				// to check if the password is locked?
				if (!userProfile.isPasswordLocked()) {
					String strPw = i_strPw;
					if (PropertiesCache.getInstance().getInstanceProperty(
//							if (PropertiesCache.getInstance().getProperty(
							ECMConstants.PROPERTY_PASSWORDENCRYTED,
							ECMConstants.YES).equals(ECMConstants.YES)) {

						strPw = PasswordEncoder.getEncodedPassword(strPw);
                        //System.out.println("=======================>>>>> "+strPw);

					}
					//to check if the password from the properties file is matched to the password retrived from db.
					if (userProfile.getPassword() != null
							&& userProfile.getPassword().equals(strPw))

					{
						Calendar calendar = Calendar.getInstance();
						Date dteCurrent = calendar.getTime();
						srk.beginTransaction();
						userProfile
								.setPasswordFailedAttempts(Sc.PASSWORD_FAILED_ATTEMPTS_TRUE);
						userProfile.setLoginFlag(ECMConstants.YES);
						userProfile.setLastLoginTime(dteCurrent);
						userProfile.ejbStore();
						srk.commitTransaction();

					} else {

						return false;
					}
				} else {

					return false;
				}
			} else {

				return false;
			}
		} catch (Exception ex) {
			srk.cleanTransaction();
			throw ex;
		} finally {
			srk.freeResources();
		}
		return true;
	}
}