package com.filogix.ecm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ProcessStatus;
import com.basis100.deal.entity.ServiceResponseQueue;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ServiceResponseQueuePK;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.ecm.handler.IHandler;
import com.filogix.ecm.handler.ResponseHandler;
import com.filogix.express.ecm.ExpressEcmContext;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.spring.SpringContextException;

public class ECMWorker implements Runnable{

    private final static Logger logger = 
        LoggerFactory.getLogger(ECMWorker.class);

	private int responseQueueId;
	private SessionResourceKit srk;
	private ServiceResponseQueue srvResp;
	private int ecmUserProfileId;

    public ECMWorker(int respQId, int userProfileId) {

        logger.debug("respQId = " + respQId + ", userProfileId =" + userProfileId);
		this.responseQueueId = respQId;
		srk = new SessionResourceKit(new Integer(userProfileId).toString());
        srvResp = getServiceResponse();
        ecmUserProfileId = userProfileId;
	}

	public void run() {
	    
	    //you can see this name with %t in log4j
	    Thread.currentThread().setName("QID=" + srvResp.getServiceResponseQueueId());
	    
		logger.debug("start run() ----------------- ");
		Deal deal = null;
		ResponseHandler resHandler = null;
		try{
		    srk.getExpressState().setDealInstitutionId(srvResp.getInstitutionProfileId());
   
            deal = new Deal(srk, null);
            //allow gold copy as well as the S copy
            deal.findByRecommendedScenario(new DealPK(srvResp.getDealId(), 
                         srvResp.getCopyId()), true);
			resHandler = (ResponseHandler)getHandler();
			ServiceInfo info = new ServiceInfo();
			info.setDeal(deal);
			info.setSrk(srk);
			info.setChannelTransactionKey(srvResp.getChannelTransactionKey());
			info.setRequestType(srvResp.getRequestType());
			resHandler.setServiceInfo(info);
			resHandler.setEcmUserProfileId(ecmUserProfileId);
			
			String respPayload = srvResp.getResponseXML();

			logger.debug("before calling handler : resHandler = " + resHandler);
			int result = resHandler.handleResponse(respPayload);
			logger.debug("after calling handler : result = " + result);
			
			switch (result){

				//Sucessful handling
				case ResponseHandler.COMPLETE:
					ECMManager.changeProcessStatus(srvResp,	ProcessStatus.PROCESSED, srk);
					break;
	            //received expired unsolicited response
                case ResponseHandler.RESPONSE_EXPIRED:
                    ECMManager.changeProcessStatus(srvResp, ProcessStatus.RESPONSE_EXPIRED, srk);
                    break;
                    //Deal has been locked
                case ResponseHandler.DEALLOCKED:
                    ECMManager.updateToRetryForLockedDeal(srvResp);
                    break;
				//Try again
				case ResponseHandler.RETRY_INTERNAL:
					ECMManager.changeProcessStatus(srvResp,	ProcessStatus.AVAILABLE, srk);
					ECMManager.incrRetryCounter(srvResp, 1, srk);
					break;
				//temp fix for Workflow failiure	
				case ResponseHandler.ERROR:
					ECMManager.changeProcessStatus(srvResp, ProcessStatus.ERROR, srk);
					break;
				//Unexpected return value <=== should not occur
				default:
					ECMManager.changeProcessStatus(srvResp,	ProcessStatus.AVAILABLE, srk);
					ECMManager.incrRetryCounter(srvResp, 1, srk);
					System.out.println("@ECMWorker.run()======>>> Invalid value returned from handleResponse()");
			}
			//If Spring context cannot be initialized, stop running
		} catch (SpringContextException sce) {
		    srk.cleanTransaction();
		    
		    ECMManager.changeProcessStatus(srvResp,ProcessStatus.AVAILABLE, srk);
		    
			logger.error("@ECMWorker.run(): Spring Configuration Error! System exits ... ");
			System.out.println("Spring Context Initializtion Error, System exists ... ");
			sce.printStackTrace();
			System.exit(0);

			//Handler is not defined in the Spring context
		} catch (ResponseHanlderNotFoundException rhnfe) {
			//change ProcessStatus to Error
		    srk.cleanTransaction();

		    ECMManager.changeProcessStatus(srvResp, ProcessStatus.ERROR, srk);

			//send notification
			try {
			    PropertiesCache pc = PropertiesCache.getInstance();
		        String emailTo = pc.getInstanceProperty(ECMConstants.EMAIL_SENT_TO_FOR_NO_HANDLER_CLASS);
		        String emailSubj = pc.getInstanceProperty(ECMConstants.EMAIL_SUBJECT_FOR_NO_HANDLER_CLASS);
		        StringBuffer emailText = new StringBuffer(500);
		        
				emailText.append("This Response with ID: " + responseQueueId + " ");
				emailText.append(ECMConstants.EMAIL_TEXT_FOR_NO_HANDLER_CLASS);
				DocumentRequest.requestAlertEmail(srk, deal, emailTo, emailSubj, emailText.toString());
				logger.error(this.getClass().getName() + ECMConstants.ALERT_MESSAGE_FOR_SENDING_EMAIL
					+ deal.getDealId());
			} catch (Exception e) {
				logger.error(StringUtil.stack2string(e));
				e.printStackTrace();
			}
		} catch (Exception e) {
		    srk.cleanTransaction();
			ECMManager.changeProcessStatus(srvResp, ProcessStatus.AVAILABLE, srk);
			ECMManager.incrRetryCounter(srvResp, 1, srk);
			e.printStackTrace();
		} finally {
            
			srk.freeResources();
		}
		logger.debug("end run() ----------------- ");
	}

	//Get the ServiceResponseQueue entry associated with the worker
	private ServiceResponseQueue getServiceResponse() {
		ServiceResponseQueue responseQueue = null;
		try {
			responseQueue = new ServiceResponseQueue(srk, null); //no need to execute calcs on Queue table
			responseQueue.findByPrimaryKey(new ServiceResponseQueuePK(responseQueueId));
		} catch (Exception e) {
			logger.error(ECMConstants.ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_GETSERVICERESPONE, e);
		}
		return responseQueue;
	}

	//Get the handler
    private IHandler getHandler() throws Exception {
		String handlerIdentifier = srvResp.getRequestType();
		logger.info("ECM Response handler Identifier:==========>>" + handlerIdentifier);
		return (IHandler) ExpressEcmContext.getService(handlerIdentifier);
	}

}
