/**
 * ResponseHanlderNotFoundException.java
 * UGMI
 */
package com.filogix.ecm;

/**
 * Jul 19, 2006
 * @author David Sun
 *
 */
public class ResponseHanlderNotFoundException extends Exception {

    /**
     * 
     */
    public ResponseHanlderNotFoundException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public ResponseHanlderNotFoundException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public ResponseHanlderNotFoundException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public ResponseHanlderNotFoundException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
