package com.filogix.ecm;
public class ECMConstants {

    //classes and methods
    public static final String IDENTITY_CLASS_NAME_AMEMANAGER = "ECMManager";
    public static final String IDENTITY_CLASS_NAME_AMEMAIN = "ECMMain";
    public static final String INEDNTITY_METHOD_NAME_INIT = "init()";
    public static final String IDENTITY_METHOD_NAME_RUN = "run()";
    public static final String IDENTITY_CLASS_NAME_ECMSIGNINSERVICE = "ECMSignInService";

    //Alias
    public static final String FALSE = "false";
    public static final String TRUE = "true";
    public static final String YES = "Y";
    public static final String NO = "N";

    //resources
    public static final String PROPERTY_SYS_PROPERTIES_LOCATION_MAIN = "SYS_PROPERTIES_LOCATION_MAIN";//JVM parameter set by -D
    public static final String PROPERTY_SYS_PROPERTIES_LOCATION_ECM = "SYS_PROPERTIES_LOCATION_ECM"; //JVM parameter set by -D
    public static final String PROPERTY_MOSSYS_PROPERTIES_NAME = "mossys.properties";
    public static final String PROPERTY_ECM_PROPERTIES_NAME = "ecm.properties";
    public static final String PROPERTY_RESOURCE_INIT = "com.basis100.resource.init";
//    public static final String PROPERTY_RESOURCE_CONNECTIONPOOL_NAME = "com.basis100.resource.connectionpoolname";
    public static final String PROPERTY_ECM_RESOURCE_INIT = "com.filogix.ecm.resource.init";

    //ECM configuration
    public static final String PROPERTY_THREADPOOL_SIZE = "com.filogix.ecm.threadpool.size";
    public static final String PROPERTY_RETRY_COUNTRER = "com.filogix.maxautotaskretry"; //Does the ECB need a retry counter?
    public static final String PROPERTY_AMEMANAGER_INTERVAL = "com.filogix.ecm.ECMManager.interval";
    public static final String DEFAULT_THREAD_POOL_SIZE = "2";
    public static final String DEFAULT_RETRY_COUNTER = "5";
    public static final String DEFAULT_THEARD_WAIT_TIME = "5";
    public static final String PROPERTY_RETRY_INTERVAL_FOR_LOCKED_DEAL_SECONDS = "com.filogix.ecm.deallock.retryInterval.seconds";

    //exceptions
    public static final String ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_SETPROCESSSTATUS = "exception from setProceeStatus()";
    public static final String ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_RUN_METHOD_OF_ECMMANAGER = "exception from run() of ECMManager";
    public static final String ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_GETRESPONEQUEUEITEM = "@ECMMAnager: exception from getResponseQueueItem()";
    public static final String ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_GETSERVICERESPONE = "@ECMWorker: exception from getServiceResponse()";
    public static final String ERROR_MESSAGE_ECMMANAGER_EXCEPTION_FROM_INCRRETRYCOUNTER = "exception from incrRetryCounter()";

    //login/logout
    public static final String PROPERTY_USER_ID = "com.filogix.ecm.user.id";
    public static final String PROPERTY_PASSWORD = "com.filogix.ecm.password";
    public static final String ERROR_MESSAGE_LOGIN_FAILED = "Login attempt failed, System exits ...";
    public static final String PROPERTY_PASSWORDENCRYTED= "com.basis100.useradmin.passwordencrypted"; //David:  (TBD)
    public static final int    USER_PROFILE_STATUS_ACTIVE = 0; //david: (TBD)
    public static final String ERROR_MESSAGE_ECMMANAGER_EXCEPTION_USER_LOGOU = "exception from logout() of ECMManager";
    public static final String ERROR_MESSAGE_NO_PATH_NAME = "No path is defined";
    public static final String ERROR_MESSAGE_MAIN_USER_NOT_LOGIN = "User can not login";

    //mail
    public static final String EMAIL_SENT_TO_FOR_NO_HANDLER_CLASS = "com.filogix.ecm.email.to";
    public static final String EMAIL_SUBJECT_FOR_NO_HANDLER_CLASS = "com.filogix.ecm.email.subject";
    public static final String EMAIL_TEXT_FOR_NO_HANDLER_CLASS = "No handler could be obtained. "; 
    public static final String ALERT_MESSAGE_FOR_SENDING_EMAIL = "Alert email has been sent for this deal: ";

}
