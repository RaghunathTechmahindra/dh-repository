package com.filogix.ecm.handler;



/**
*
* <p>Title: Filogix Express - AIG UG</p>
*
* <p>Description: Filogix AIGUG MI Project</p>
*
* <p>Copyright: Copyright (c) 2006</p>
*
* <p>Company: Filogix</p>
*
* @author Clement Cheng
* @version 1.0
*/

public interface IResponseHandler extends IHandler{
	public int handleResponse(Object obj);

}
