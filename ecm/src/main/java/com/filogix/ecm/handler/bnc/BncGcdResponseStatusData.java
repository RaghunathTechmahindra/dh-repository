/**
 * 
 */
package com.filogix.ecm.handler.bnc;

/**
 * @author cc_vdolgo
 *
 */
public final class BncGcdResponseStatusData {
	protected static final int NOT_SUBMITTED = 1;
	protected static final int REQUEST_ACCEPTED = 2;
	protected static final int JOB_ASSIGNED = 3;
	protected static final int JOB_DELAYED = 4;
	protected static final int INSPECTION_COMPLETED = 5;
	protected static final int COMPLETED = 6;
	protected static final int COMPLETED_CONFIRMED = 7;
	protected static final int CLOSING_CONFIRMED = 8;
	protected static final int SYSTEM_ERROR = 9;
	protected static final int PROCESSING_ERROR = 10;
	protected static final int CANCELLATION_NOT_SUBMITTED = 11;
	protected static final int CANCELLATION_REQUESTED = 12;
	protected static final int CANCELLATION_ACCEPTED = 13;
	protected static final int CANCELLATION_CONFIRMED = 14;
	protected static final int CANCELLATION_REJECTED = 15;
	protected static final int QUOTATION_RECEIVED = 16;
	protected static final int QUOTATION_CONFIRMED = 17;
	protected static final int FAILED = 18;
	protected static final int RECEIPT_PENDING = 19;
	protected static final int RECEIVED = 20;
	protected static final int ERROR_RECEIVED = 21;
	protected static final int ERROR_RESPOND = 22;
}
