/**
 *
 */
package com.filogix.ecm.handler.bnc;

import org.apache.log4j.Logger;

import com.basis100.deal.entity.Response;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.ecm.handler.ResponseHandler;
import com.filogix.externallinks.services.responseprocessor.bnc.
	BncGcdResponseProcessor;
import com.basis100.deal.pk.ResponsePK;
import com.filogix.externallinks.services.ServiceInfo;
import com.basis100.deal.entity.Request;
import com.basis100.deal.pk.RequestPK;
import MosSystem.Mc;

/**
 *
 * <p>Title: BncGcdResponseHandler</p>
 * <p>Description: Handles the GCD response</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */
public class BncGcdResponseHandler
	extends ResponseHandler
{

	protected static final Logger log = Logger.getLogger(BncGcdResponseHandler.class);

	public BncGcdResponseHandler()
	{

	}
	
	//implement general class in ResponseHandler (merge with ug)
	public int handleResponse(Object obj){
		return handleResponse(obj,this.getServiceInfo());
	}

	/**
	 * Handles processing of response data.
	 * @param obj XML document to be parsed and processed
	 * @param info Service Info object
	 * @return Resulting code as follows:
	 * 	ResponseHandler.COMPLETE if the deal is processed, unlocked and workflow trigger succeeds
	 * otherwise
	 * 	ResponseHandler.RETRY_INTERNAL if error occurs
	 */
	public int handleResponse(Object obj, ServiceInfo info)
	{
		BncGcdResponseProcessor processor = new BncGcdResponseProcessor();
		int resultCode = RETRY_INTERNAL; // default value signalling failure
		boolean success = false;
		try
		{
			srk.beginTransaction();
			boolean isDealLocked = placeDealLock();
			srk.commitTransaction();
			if (isDealLocked == false)
			{
				return DEALLOCKED;
			}

			try
			{
				processor.processResponse(obj, info, true);
				success = true;
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				success = false;
			}

			if (success)
			{
				updateResponseStatus(processor.getResponseId(),
									 BncGcdResponseStatusData.RECEIVED);
				updateRequestStatus(processor.getRequestId(),
									Mc.REQUEST_STATUS_ID_RESPONSE_AVAILABLE,
									processor.getCopyId());
				workflowTrigger();
				resultCode = COMPLETE;
			}
			else
			{
//				updateResponseStatus(processor.getResponseId(),
//									 BncGcdResponseStatusData.ERROR_RESPOND);
			}

		}
		catch (Exception e)
		{
			StackTraceElement[] trace = e.getStackTrace();
			StringBuffer stackTraceBuffer = new StringBuffer();
			for (int i = 0; i < trace.length; i++)
			{
				stackTraceBuffer.append("\t" + trace[i].toString() + "\n");
			}
			System.out.println("Failed to process response: " + e.toString());
			log.error("Failed to process response: " + e.toString());
			log.error(stackTraceBuffer.toString());

			//temp fix for Workflow failiure
			return ResponseHandler.ERROR;
 

		}
		finally
		{
			try
			{
				srk.beginTransaction();
				unLockDeal();
				srk.commitTransaction();
			}
			catch (JdbcTransactionException ex1)
			{
				log.error(ex1.getMessage());
				log.error(ex1.getStackTrace().toString());
			}
		}
		return resultCode;
	}

	/**
	 * Update response status in <code>RESPONSE</code> table
	 * @param status Response status according to <code>RESPONSESTATUS</code> table
	 */
	/**
	 *
	 * @param responseId int
	 * @param status int
	 * @throws Exception
	 */
	private void updateResponseStatus(int responseId, int status)
		throws Exception
	{
		try
		{
			if (srk == null)
			{
				if (info != null)
				{
					// get session resource kit from ServiceInfo
					setSessionResourceKit(info.getSrk());
				}
				else
				{
					// both ServiceInfo and Session Resource Kit are null
					setSessionResourceKit(new SessionResourceKit("System"));
				}
			}
			Response response = new Response(srk);
			response.findByPrimaryKey(new ResponsePK(responseId));
			response.setResponseStatusId(status);
			response.ejbStore();
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
			try
			{
				srk.rollbackTransaction();
			}
			catch (JdbcTransactionException jdbcEx)
			{
				jdbcEx.printStackTrace();
			}
			finally
			{
				log.error("Failed to update response status: " + ex.toString());
			}
			throw ex;
		}
	}

	/**
	 * Update request status in <code>REQUEST</code> table
	 * @param requestId int ID of the request to be updated
	 * @param status int Response status according to <code>REQUESTSTATUS</code> table
	 * @param copyId int Copy Id of the request
	 * @throws Exception Thrown if error
	 */
	private void updateRequestStatus(int requestId, int status, int copyId)
		throws Exception
	{
		try
		{
			if (srk == null)
			{
				if (info != null)
				{
					// get session resource kit from ServiceInfo
					setSessionResourceKit(info.getSrk());
				}
				else
				{
					// both ServiceInfo and Session Resource Kit are null
					setSessionResourceKit(new SessionResourceKit("System"));
				}
			}
			Request request = new Request(srk);
			request.findByPrimaryKey(new RequestPK(requestId, copyId));
			request.setRequestStatusId(status);
			request.ejbStore();
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
			try
			{
				srk.rollbackTransaction();
			}
			catch (JdbcTransactionException jdbcEx)
			{
				jdbcEx.printStackTrace();
			}
			finally
			{
				log.error("Failed to update request status: " + ex.toString());
			}
			throw ex;
		}
	}

}
