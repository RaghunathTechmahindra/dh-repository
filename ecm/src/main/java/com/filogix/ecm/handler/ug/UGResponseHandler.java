package com.filogix.ecm.handler.ug;

import javax.xml.bind.JAXBContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.util.StringUtil;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.ecm.handler.ResponseHandler;
import com.filogix.externallinks.mi.aigug.jaxb.response.MortgageInsuranceResponse;
import com.filogix.externallinks.services.responseprocessor.ug.UGMIResponseProcessor;

public class UGResponseHandler extends ResponseHandler {
	
    private final static Logger _log = 
        LoggerFactory.getLogger(UGResponseHandler.class);
	private SysLogger logger;
	private static JAXBContext jc;
	private MortgageInsuranceResponse miResponse;
	private UGMIResponseProcessor ugResponseProcessor;
	private static final String schema = "com.filogix.externallinks.mi.aigug.jaxb.response";
	
	/**
	 * Constructs an initialized AIG UG response handler.
	 */
	public UGResponseHandler() {
		
		logger = ResourceManager.getSysLogger("UGResponseHandler");
		
	}
	

	/**
	 * Handles the response from the DatX.
	 */
	public int handleResponse(Object obj) {
		_log.debug("UGResponseHandler.handleResponse start");
		int resultCode = RETRY_INTERNAL; // default value signalling failure
		boolean success = false;
		try {
			SessionResourceKit srk = this.getSrk();
			srk.beginTransaction();
			boolean lockResult = placeDealLock();
            srk.commitTransaction();
            if (lockResult == false) {
            	_log.debug("UGResponseHandler.handleResponse: faild to lock");
                return resultCode;
            }
			
            this.setUGResponseProcessor((UGMIResponseProcessor)UGMIResponseProcessor.getInstance());
            try
			{
            	_log.debug("UGResponseHandler.handleResponse: before executing processor : " + ugResponseProcessor);
            	ugResponseProcessor.processResponse(obj, this.getServiceInfo(), false);
            	_log.debug("UGResponseHandler.handleResponse: after executing processor ");
				success = true;
			}
			catch (Exception e)
			{
				_log.error(e.getMessage(), e);
				_log.error(StringUtil.stack2string(e));
				System.out.println(e.getMessage());
				success = false;
			}
			if (success)
			{				
				workflowTrigger();
				resultCode = COMPLETE;
			}
		
		}
		catch(Exception e) {
			logger.error("Exception @UGResponseHandler.handleRespons :: " + StringUtil.stack2string(e));
			logger.error(e.getStackTrace().toString());
			return ResponseHandler.ERROR;
		}
		finally {
            try {
                srk.beginTransaction();
                this.unLockDeal();
                srk.commitTransaction();
            } 
            catch (Exception ex) {
                srk.cleanTransaction();
                logger.error(ex.getStackTrace().toString());
            }
        }
		
		return resultCode;
		
	}
	
	/**
	 * Sets the value of ugResponseProcessor property.
	 * Done by the "Spring" framwork container.
	 * @param ugResponseProcessor
	 */
	public void setUGResponseProcessor(UGMIResponseProcessor ugResponseProcessor) {
		this.ugResponseProcessor = ugResponseProcessor;
	}
	
	

	
}
