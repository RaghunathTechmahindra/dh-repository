package com.filogix.ecm.handler.mi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.security.DLM;
import com.filogix.ecm.handler.ResponseHandler;
import com.filogix.externallinks.services.responseprocessor.IResponseProcessor;
import com.filogix.externallinks.services.responseprocessor.ResponseOutdatedException;

public class MIAsyncResponseHandler extends ResponseHandler {

    private final static Logger logger = LoggerFactory.getLogger(MIAsyncResponseHandler.class);

    private IResponseProcessor responseProcessor;

    private boolean acquiredDealLock = false;

    public int handleResponse(Object obj) {

        logger.debug("MIAsyncResponseHandler -- start for deal=" + deal.getDealId());

        int resultCode = RETRY_INTERNAL; // default value signalling failure
        try {
            this.srk.getConnection().setAutoCommit(true);

            boolean lockResult = placeDealLock();

            if (lockResult) {
                acquiredDealLock = true;
            } else {
                DLM dlm = DLM.getInstance();
                //this displays "pending MI response" label and "process pending MI response" button on MI screen
                dlm.setMIResponseExpected(srk, deal.getDealId(), "Y");
                logger.debug("UGResponseHandler.handleResponse: faild to lock");
                return DEALLOCKED;
            }

            responseProcessor.processResponse(obj, this.info, false);

            workflowTrigger();
            
            resultCode = COMPLETE;

        } catch (ResponseOutdatedException e) {
            logger.warn(e.getMessage(), e);
            resultCode = RESPONSE_EXPIRED;

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultCode = ERROR;

        } finally {
            if (acquiredDealLock) {
                unLockDeal();
            }
        }

        logger.debug("MIAsyncResponseHandler -- end for deal=" + deal.getDealId());
        return resultCode;

    }

    public void setResponseProcessor(IResponseProcessor responseProcessor) {
        this.responseProcessor = responseProcessor;
    }

}
