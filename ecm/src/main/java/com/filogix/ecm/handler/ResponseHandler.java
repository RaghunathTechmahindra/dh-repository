package com.filogix.ecm.handler;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.security.DLM;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFEExternalTaskCompletion;
import com.basis100.workflow.WFETrigger;
import com.filogix.externallinks.services.ServiceInfo;


public abstract class ResponseHandler implements IResponseHandler {

	public final static int RETRY_INTERNAL = 0;
	public final static int RETRY_EXTERNAL = 1;
	public final static int EXPIRE_RETRY = 2;
	public final static int COMPLETE = 3;
	public final static int ERROR = 4;
	public final static int DEALLOCKED = 5;
	public final static int RESPONSE_EXPIRED = 6;
    

	// resources kit provided by caller in (sub-class) constructors
    protected SessionResourceKit srk;

    // resources from resource kit
    protected SysLogger logger;
    protected JdbcExecutor jExec;
    protected Deal deal;
    protected DealHistoryLogger hLog;
    protected ServiceInfo info;
    protected int ecmUserProfileId;

    public void setSessionResourceKit(SessionResourceKit srk)
    {
        this.srk = srk;
        logger = srk.getSysLogger();
        jExec = srk.getJdbcExecutor();
    }

	public SessionResourceKit getSrk() {
		return srk;
	}

	public void setDeal(Deal deal){

		this.deal = deal;
	}

	/**
     * placeDealLock Method the place a deal lock.
     *
     * @return boolean : true - deal lock placed successfully false - the deal
     *         was locked by other user and was not overriable
     */
    public boolean placeDealLock()
    {
        try
        {
            String className = this.getClass().getName();
            // Take out the Package name
            className = className.substring(className.lastIndexOf(".") + 1);

            DLM dlm = DLM.getInstance();
            dlm.placeLock(deal.getDealId(), ecmUserProfileId,
                    "Lock by ECM for " + className + " !!", srk, true);

            return true;
        } catch (Exception ex)
        {
            logger.warning("Deal was locked by other user :: " + ex);
            return false;
        }
    }


    /**
     * placeSoftDealLock Method the place a soft deal lock, i.e. users can
     * override the lock.
     *
     * @return boolean : true - deal lock placed successfully false - the deal
     *         was locked by other user and was not overriable
     */
    public boolean placeSoftDealLock()
    {
        try
        {
            String className = this.getClass().getName();
            // Take out the Package name
            className = className.substring(className.lastIndexOf(".") + 1);

            DLM dlm = DLM.getInstance();
            dlm.placeSoftLock(deal.getDealId(), ecmUserProfileId,
                    "Lock by ECM for " + className + " !!", srk, true);

            return true;
        } catch (Exception ex)
        {
            logger.warning("Deal was locked by other user :: " + ex);
            return false;
        }
    }

    /**
     * unLockDeal Method to unlock a deal.
     *
     * @return boolean : true - deal was unlocked successfully false - deal was
     *         failed to unlock
     */
    public boolean unLockDeal()
    {
        try
        {
            DLM dlm = DLM.getInstance();
            dlm.doUnlock(deal.getDealId(), ecmUserProfileId, srk, true,
                    true); // Assume always in transaction

            return true;
        } catch (Exception ex)
        {
            logger.warning("Problem when try to unlock the deal :: " + ex);
            return false;
        }
    }

    public boolean isDealLocked()
    {
        boolean lockResult = false;
        try
        {
            srk.beginTransaction();
            lockResult = placeDealLock();
            srk.commitTransaction();
        } catch (Exception ex)
        {

        }
        return lockResult;
    }





 /**
     * workflowTrigger Method to trigger workflow.
     * 
     * @param type
     *            int 0 - task completion activity 1 - workflow selection and
     *            initial task generation 2 - as 1) but add unconditional asnc
     *            state (test) task generation (e.g. possible MI task(s))
     * @param extObj
     *            WFEExternalTaskCompletion
     * @param theDeal
     *            Deal
     * @throws Exception
     */
    protected void workflowTrigger(int type, WFEExternalTaskCompletion extObj,
            Deal theDeal) throws Exception
    {
        logger
                .debug("@WFProcessBase.workflowTrigger :: ================ Begin =================");
        WFETrigger.workflowTrigger(type, srk, theDeal.getUnderwriterUserId(),
                theDeal.getDealId(), theDeal.getCopyId(), -1, extObj);
        logger
                .debug("@WFProcessBase.workflowTrigger :: ================ End =================");
    }

    /**
     * workflowTrigger Method to trigger workflow.
     * 
     * @param type
     *            int 0 - task completion activity 1 - workflow selection and
     *            initial task generation 2 - as 1) but add unconditional asnc
     *            state (test) task generation (e.g. possible MI task(s))
     * @param extObj
     *            WFEExternalTaskCompletion
     * @throws Exception
     */
    protected void workflowTrigger(int type, WFEExternalTaskCompletion extObj)
            throws Exception
    {
        logger
                .debug("@WFProcessBase.workflowTrigger :: ================ Begin =================");
        WFETrigger.workflowTrigger(type, srk, deal.getUnderwriterUserId(), deal
                .getDealId(), deal.getCopyId(), -1, extObj);
        logger
                .debug("@WFProcessBase.workflowTrigger :: ================ End =================");
    }

    /**
     * workflowTrigger Simplied method to trigger workflow
     * 
     * @param type
     *            int
     * @throws Exception
     */
    protected void workflowTrigger(int type) throws Exception
    {
        workflowTrigger(type, null);
    }

    /**
     * workflowTrigger Simplied method to trigger workflow : by default will
     * trigger with type 2
     * 
     * @throws java.lang.Exception
     */
    protected void workflowTrigger() throws Exception
    {
        workflowTrigger(2, null);
    }

	
	public ServiceInfo getServiceInfo() {
		
		return info;
	}
	
	public void setServiceInfo(ServiceInfo sInfo){
		
		info = sInfo;
		setDeal(info.getDeal());
		setSessionResourceKit(info.getSrk());		
	}

    public int getEcmUserProfileId() {
        return ecmUserProfileId;
    }

    public void setEcmUserProfileId(int ecmUserProfileId) {
        this.ecmUserProfileId = ecmUserProfileId;
    }

}
