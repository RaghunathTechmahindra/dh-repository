<?xml version="1.0" encoding="UTF-8"?>
<!--
The Upload File defines transformation rules for achieving the upload xml file.
     	The upload is created starting with the creation of Elements and fields reflecting the deal tree. 
	Once the Deal tree has been constructed an number of rules can be applied to the nodes of the tree.

1. Entity Level rules - Extensions to the tree:
	a. subtree rule	 - Creates an Element with name = @name 
					- Constructs an entity with class name com.basis100.deal.entity = @entity or if @entity is not present @name 
					  	with primaryKey id given by  the parent node value given by @searchKey and the copyid indicated.
					       (See note on the copyid attribute behaviour)
					- Calls getSecondaryParents and getChildren on the entity and each delivered child constructing all possible entity and field elements.
	
	b. element rule  -performs as in the subtree rule but does not call for secondaryParents or children
	
2. Method Level rules - method calls against parent entity
		the method rule calls the method indicated by @name on the given entity constructed with the parentField - id given in 
		by @searchKey with the indicated parameters and appends an element with the name 
		given by @elementName with the  method return 	value.

3. Field Level rules - 
	a. Exclude rule - exclude the field given by @name 
	b. Map rules - @type = 'picklist' map the value of the field indicated by name to the value of the picklist description for the fieldName given by @with
	c. Sql statements - the statement given is executed. The value element returned will be the first column of the first record found placed in
	an element with the given elementName. Variables in the statement are indicated by ? . Presently only parameters of 'parent' type are supported. This means that the variable value will be found in the parent element of the Upload tree in the field given by the searchKey parameter.

 -->
<Document>
	<Deal>
		<Extension copyid="1" name="SourceOfBusinessProfile" action="subtree" searchKey="sourceOfBusinessProfileId"/>
		<Extension copyid="1" name="InvestorProfile" action="subtree" searchKey="investorProfileId"/>
		<Extension copyid="1" name="BranchProfile" action="subtree" searchKey="branchProfileId"/>
		<Extension copyid="1" name="SourceFirmProfile" action="subtree" searchKey="sourceFirmProfileId"/>
		<Extension copyid="1" name="GroupProfile" action="subtree" searchKey="groupProfileId"/>
		<Extension copyid="1" name="PricingProfile" action="subtree">
			<SearchKey type="pemethod" name="getActualPricingProfileId" searchKey="dealId"/>
		</Extension>

		<!-- <Extension copyid="false" name="PartyProfile" action="subtree" ></Extension> -->
		<!-- <Extension callProcessingMethod="true" action="subtree" nodeName="PartyProfile">
			<Param>dealId</Param>
		</Extension> -->

		<!-- <Extension callProcessingMethod="true" action="subtree" nodeName="DealNotes">
			<Param>dealId</Param>
		</Extension> -->
		
		<Extension copyid="1" name="LenderProfile" action="subtree" searchKey="lenderProfileId"/>
		<Extension copyid="1" name="Underwriter" action="subtree" entity="UserProfile" searchKey="underwriterUserId"/>
		<Extension copyid="1" name="MtgProd" action="subtree" entity="MtgProd" searchKey="mtgProdId"/>
		<Extension copyid="false" name="DealNotes" action="element" dbchild="true"/>
		<!-- <Extension copyid="false" name="CreditBureauReport" action="element" dbchild="true"/> -->
		<Field action="sqlstmt" elementName="mortyLenderId" query="Select mortyLenderId from InstitutionProfile where InstitutionProfileId = ?">
				<Param searchKey="InstitutionProfileId"/>
		</Field>			
    	<Field action="sqlstmt" elementName="ecniLenderId" query="Select ecniLenderId from InstitutionProfile where InstitutionProfileId = ?">				<Param searchKey="InstitutionProfileId"/>
		</Field>			

		<Field action="map" type="renameentityfield" name="MIPayorId" with="mortgageInsurancePayorId"/>
		<Field action="map" type="renameentityfield" name="MITypeId" with="mortgageInsuranceTypeId"/>
		<Field action="map" type="picklist" name="MIPayorId" with="mortgageInsurancePayorId"/>
		<Field action="map" type="picklist" name="MITypeId" with="mortgageInsuranceTypeId"/>
		<Field action="exclude" name="sourceOfBusinessProfileId"/>
		<Field action="exclude" name="investorProfileId"/>
		<Field action="exclude" name="branchProfileId"/>
		<Field action="exclude" name="sourceFirmProfileId"/>
		<Field action="exclude" name="groupProfileId"/>
		<Field action="exclude" name="pricingProfileId"/>
		<Field action="exclude" name="lenderProfileId"/>
	</Deal>
	<Borrower>
		<!--PEMethod name="getEquifaxCreditReportXml" elementName="equifaxCreditReportXml" searchKey="BorrowerId"/-->
		<Extension callProcessingMethod="true" action="element" nodeName="equifaxCreditReportXml">
			<!--Param>DealId</Param-->
			<Param>BorrowerId</Param>
			<Param>CopyId</Param>
		</Extension>
	</Borrower>
	<Property>
		<PEMethod copyid="true" name="fetchFirstAppraiserCompanyName" elementName="AppraiserCompanyName" searchKey="propertyId"/>
	</Property>
	<PartyProfile>
		<Extension copyid="1" name="Contact" action="subtree" searchKey="contactId"/>
	</PartyProfile>
	<EmploymentHistory>
		<Field action="exclude" name="jobTitle"/>
	</EmploymentHistory>
	<MtgProd>
		<Field action="sqlstmt" elementName="ptTypeId" query="select pttypeid from paymenttermtype where pttypeid in (select pttypeid from paymentterm where paymenttermid in (select paymenttermid from mtgprod where mtgprodid = ? ))" >
			<Param searchKey="mtgProdId"/>			
		</Field>
		<Field action="sqlstmt" elementName="ptDescription" query="select pttdescription from paymenttermtype where pttypeid in (select pttypeid from paymentterm where paymenttermid in (select paymenttermid from mtgprod where mtgprodid = ? ))" >
			<Param searchKey="mtgProdId"/>
		</Field>		
	</MtgProd>	
	<!-- <SourceOfBusinessProfile>
		<Field action="sqlstmt" elementName="sourceSystemMailboxNbr" query="Select sourceSystemMailboxNbr from sourceofBusinessMailbox where sourceOfBusinessProfileId 	= ? ">
			<Param searchKey="sourceOfBusinessProfileId"/>
		</Field>
	</SourceOfBusinessProfile> -->	
	<!-- <CreditBureauReport>
		<Field action="exclude" name="dealId"/>
		<Field action="exclude" name="creditBureauReportId"/>
	</CreditBureauReport> -->
	<DealFee>
		<Extension name="Fee" copyid="true" action="element" searchKey="feeId"/>
	</DealFee>
	<Fee>
		<Field action="exclude" name="feeId"/>
		<Field action="exclude" name="dealFeeId"/>
		<Field action="map" type="picklist" name="defaultPaymentMethodId" with="feePaymentMethodId"/>
		<Field action="map" type="picklist" name="defaultPaymentDateTypeId" with="feePaymentDateTypeId"/>
	</Fee>
	<DocumentTracking>
		<PEMethod copyid="true" name="getDocumentTextAndRole" elementName="documentText" searchKey="documentTrackingId">
			<Param type="int" value="0"/>
		</PEMethod>
		<PEMethod copyid="true" name="getSectionCode" elementName="sectionCode" searchKey="documentTrackingId"/>
		<PEMethod copyid="true" name="getSectionDescription" elementName="sectionDescription" searchKey="documentTrackingId"/>
		<PEMethod copyid="true" name="getCCRDescription" elementName="CCRDescription" searchKey="documentTrackingId"/>
	</DocumentTracking>
	<!-- <PartyDealAssoc>
		<Extension name="PartyProfile" copyid="false" action="subtree" searchKey="partyProfileId"/>
	</PartyDealAssoc>	 -->
</Document>
