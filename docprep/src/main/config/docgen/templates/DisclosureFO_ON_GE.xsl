
<!--
10/Aug/2006 DVG #DG480 #3996  3.1 - Statement of Mortgage form extending past 2 pages with multiple fees  
==========================================  -->
<!--created by Sasha Aleksandric										-->
<!--  Jun 07, 2006																-->
<!--																						-->
<!--==========================================  -->

<xsl:stylesheet version="1.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:fo="http://www.w3.org/1999/XSL/Format" 
		xmlns:fox="http://xml.apache.org/fop/extensions"
    xmlns:java="http://xml.apache.org/xalan/java">

	<!-- Venkata - Hardcoding Lender Name - CR to be given - Begin -->
	<xsl:variable name="lenderName" select="'GE Money'" />	
	<!-- Venkata - Hardcoding Lender Name - CR to be given - End -->

  <!--#DG480 that is a line that will maximize the space approximately = 54 chars   
              WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW -->
  <xsl:variable name="maxChars" select="54"/>

	<xsl:output method="xml" version="1.0"/>

	<xsl:template match="/">	
		<fo:root>	
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" page-width="8.5in" page-height="14in" margin-top="0.3in" margin-left="0.4in" margin-bottom="0.3in" margin-right="0.4in">
					<fo:region-before extent="15mm"/>
					<fo:region-body margin-top="14mm"/>
					<fo:region-after extent="15mm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			
			<fo:page-sequence master-reference="main" language="en">
			
				<!--*************************************************************-->
				<!-- Header                                                                     -->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="CreateTitleLine"/>
				</fo:static-content>
				
				<fo:flow flow-name="xsl-region-body">
					<xsl:call-template name="CreatePages"/>
				</fo:flow>
					
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	
	<!--*************************************************************-->
	<!-- Start with page creation                                          -->
	<!--                                                                                  -->
	<xsl:template name="CreatePages">

			<!--*************************************************************-->
			<!-- First Page                                                                 -->
			<!--                                                                                  -->
			<xsl:call-template name="WhiteOnBlackText"><xsl:with-param name="titleText" select="'Important'"/></xsl:call-template>
			<xsl:call-template name="FirstParagraph"/>
			
			<!--*************************************************************-->
			<!-- Big Table                                                                  -->
			<fo:block border="solid black 2pt" padding-top="6pt" line-height="10px">
				<xsl:copy use-attribute-sets="NormalFont_10Left"/>
		
				<xsl:call-template name="Section1_PropertyToBeMortgaged"/>
				<xsl:call-template name="Section2_IsTheMortgageBroker"/>
				<xsl:call-template name="Section3_PrincipalAmount"/>
				<xsl:call-template name="Section4_ThePrincipalAmount"/>
				<xsl:call-template name="Section5_TheMortgageWillBeAmortized"/>
				<xsl:call-template name="Section6_FeesAndCostsPayable"/>
				<xsl:call-template name="Section6_FeesTable"/>
			</fo:block>

			<!--*************************************************************-->
			<!-- Big Table's footer                                                     -->
			<xsl:call-template name="BigTableFooter"/>
			<xsl:call-template name="BreakPage"/>
			
			
			<!--*************************************************************-->
			<!-- Second Page                                                           -->
			<!--                                                                                  -->
			<fo:block height="10mm"><fo:leader leader-pattern="space" leader-length="20mm"/></fo:block> 
			<fo:block border="solid black 2pt" padding-top="6pt" line-height="10px">
				<xsl:copy use-attribute-sets="NormalFont_10Left"/>
				
				<xsl:call-template name="Section7_TheMortgageWillBeDueAndPayable"/>
				<xsl:call-template name="Section8_TheMortgageIsNotRenewable"/>
				<xsl:call-template name="Section9_OtherTermsAndConditions"/>
				<xsl:call-template name="Section10_ThisMortgageShallBe"/>
				<xsl:call-template name="Section11_NameOfMortgageBroker"/>
				<xsl:call-template name="Section11_TheMortgageBrokerHasNot"/>
				<xsl:call-template name="Section12_IHaveFullyCompleted"/>
				<xsl:call-template name="WhiteOnBlackText"><xsl:with-param name="titleText" select="'ACKNOWLEDGEMENT'"/></xsl:call-template>
				<xsl:call-template name="BottomPartOfSecondPage"/>
			</fo:block>
			<fo:block font-size="8px" padding-top="1mm" text-align="center">One copy of this form must be provided to the prospective borrower and one copy must be retained by the mortgage broker.</fo:block>
	</xsl:template>
	
	<!--*************************************************************-->
	<!-- Create header part of the document                       -->
	<!--                                                                                  -->
	<xsl:template name="CreateTitleLine">
		<fo:table table-layout="fixed" inline-progression-dimension.optimum="20px">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<fo:table-column/>
			<fo:table-column column-width="90mm"/>
			<fo:table-column/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell><fo:block padding-top="12px">
              <!-- commented out as per pvcs #5643 <xsl:copy use-attribute-sets="NormalFont_10Left"/><xsl:value-of select="//lenderOffice"/> -->
              </fo:block></fo:table-cell>
					<fo:table-cell padding-top="1mm" text-align="center">
						<fo:block><xsl:copy use-attribute-sets="TitleFont_FirstRow"/>Mortgage Broker Act</fo:block>
						<fo:block><xsl:copy use-attribute-sets="TitleFont_SecondRow"/>Statement of Mortgage</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:table table-layout="fixed" inline-progression-dimension.optimum="20px">
							<fo:table-column/>
							<fo:table-body>
								<fo:table-row><fo:table-cell height="10pt"/></fo:table-row>
								<fo:table-row><fo:table-cell><xsl:copy use-attribute-sets="TrasactionNoFirstRow"/><fo:block>Transaction No.:</fo:block></fo:table-cell></fo:table-row>
								<fo:table-row><fo:table-cell><xsl:copy use-attribute-sets="TrasactionNoSecondRow"/><fo:block><xsl:value-of select="//transactionNumber"/></fo:block></fo:table-cell></fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	<!--*************************************************************-->
	<!-- Black box with text across the page                      -->
	<!--                                                                                  -->
	<xsl:template name="WhiteOnBlackText">
		<xsl:param name="titleText"/>
		<fo:block text-align="center" background-color="black" color="white" font-size="16pt" line-height="20pt" font-weight="bold"><xsl:value-of select="$titleText"/></fo:block>
	</xsl:template>
	
	
	<!--*************************************************************-->
	<!-- First Paragraph                                                        -->
	<!--                                                                                  -->
	<xsl:template name="FirstParagraph">
		<fo:block padding-before="5pt" line-height="12pt">
			<xsl:copy use-attribute-sets="NormalFont_9Justify"/>
			<fo:block>
				This Statement of Mortgage must be completed by the mortgage broker and an amortization schedule for the mortgage must be attached to it. 
				A copy of this Statement of Mortgage signed by the mortgage broker must be given to you 
				at least 72 hours (excluding Sundays and holidays) before you are asked to sign the mortgage instrument or a commitment to enter into the mortgage. 
				This 72-hour period does not apply IF:
			</fo:block>
			<fo:list-block>
				<xsl:copy use-attribute-sets="NormalListBlock"/>
				<fo:list-item>
					<fo:list-item-label>
						<fo:block>1)</fo:block>
					</fo:list-item-label>
					<fo:list-item-body>
						<xsl:copy use-attribute-sets="NormalListBlockBody"/>
						<fo:block>no brokerage fee is payable by you to the mortgage broker, AND</fo:block>
					</fo:list-item-body>
				</fo:list-item>
				<fo:list-item>
					<fo:list-item-label>
						<fo:block>2)</fo:block>
					</fo:list-item-label>
					<fo:list-item-body>
						<xsl:copy use-attribute-sets="NormalListBlockBody"/>
						<fo:block>the lender is a bank, loan or trust corporation, insurance company, credit union or finance company.</fo:block>
					</fo:list-item-body>
				</fo:list-item>
			</fo:list-block>
			<fo:block>
				If the 72-hour period applies to your mortgage, it may be reduced to 24 hours, but only if you obtain independent legal advice.
			</fo:block>
			<fo:block padding-before="4pt">
				<xsl:copy use-attribute-sets="NormalFont_10Justify"/>
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				YOU ARE STRONGLY ADVISED TO OBTAIN INDEPENDENT LEGAL ADVICE ABOUT THIS MORTGAGE BEFORE YOU SIGN THE MORTGAGE CONTRACT.
			</fo:block>
			<fo:block padding-before="4pt" padding-after="4pt">
				If the principal amount of the mortgage is $200,000 or less, the mortgage broker cannot require you to make, and cannot accept an advance payment or 
				deposit for services to be rendered or expenses to be incurred by the mortgage broker or any other person.
			</fo:block>
		</fo:block>
	</xsl:template>
	

	<!-- ************************************************************* -->
	<!-- Section 1. - Property to be mortaged ...                    -->
	<!--                                                                                   -->
	<xsl:template name="Section1_PropertyToBeMortgaged">
		<fo:block start-indent="4mm">
			<fo:inline font-weight="bold">1.</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="3mm"></fo:leader></fo:inline>
			<fo:inline font-weight="bold">Property to be mortgaged:</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="3mm"></fo:leader></fo:inline>
			<fo:inline>(legal description, municipal address and description of buildings)</fo:inline>
		</fo:block>
		<fo:table padding-bottom="15px">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell border-bottom="solid gray 1pt" margin-left="0pt" padding-top="10pt">
						<fo:block>
							<xsl:value-of select="//propAddrLine1"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell/>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell border-bottom="solid gray 1pt" margin-left="0pt" padding-top="10pt">
						<fo:block>
							<xsl:value-of select="//propAddrLine2"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell/>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>
		
		
	<!--*************************************************************-->
	<!-- Section 2. - Is the mortgage broker ...                      -->
	<!--                                                                                  -->	
	<xsl:template name="Section2_IsTheMortgageBroker">
		<fo:table padding-bottom="4px" line-height="1">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row font-weight="bold">
					<fo:table-cell text-align="center">
						<fo:block>2.</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>Is the mortgage broker, or any party with other than an arm's length business relationship to the mortgage broker, acting as a lender for this mortgage loan?</fo:block>
					</fo:table-cell>
					<fo:table-cell/>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<!--	No Check Box	 -->
		<fo:table padding-bottom="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="18px"/>
			<fo:table-column column-width="12mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell margin-left="0px" padding-top="2px">
						<xsl:choose>
							<xsl:when test="//isActingAsLender = 'N' or //isActingAsLender = 'n'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</fo:table-cell>
					<fo:table-cell margin-left="2px" margin-right="0px" margin-bottom="0px" padding-top="3px">
						<fo:block>No</fo:block>
					</fo:table-cell>
					<fo:table-cell/>
				</fo:table-row>
				
				<!--	Yes Check  Box	 -->	
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell margin-left="0px" padding-top="4px">
						<xsl:choose>
							<xsl:when test="//isActingAsLender = 'N' or //isActingAsLender = 'n'">
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</fo:table-cell>
					<fo:table-cell margin-left="2px" margin-right="0px" margin-bottom="0px" padding-top="5px">
						<fo:block>Yes</fo:block>
					</fo:table-cell>
					<fo:table-cell margin-left="2px" margin-right="0px" margin-bottom="0px" padding-top="5px">
						<fo:block>If "Yes", describe the nature of the relationship, name and address of lender:</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<!-- Long multi-line Acting as a lender description -->
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell number-columns-spanned="3" padding-top="3mm">
						<fo:block linefeed-treatment="preserve">
							<!--#DG480 xsl:call-template name="str_tokenizer">
								<xsl:with-param name="string" select='string(//actingAsLenderDescription)'/>
								<xsl:with-param name="delimiters" select="'&#xD;&#xA;'" />
								<xsl:with-param name="tokendtag" select="'fo:block'" />
							</xsl:call-template-->
							<xsl:call-template name="makeWhollyBreakable">
								<xsl:with-param name="inp" select="substring(/*/actingAsLenderDescription,1,13*$maxChars)"/>
							</xsl:call-template>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row padding-top="13mm" height="1mm">
					<fo:table-cell/>
					<fo:table-cell number-columns-spanned="3" border-bottom="solid gray 1pt"/><!--#DG480 -->
				</fo:table-row>
					
			</fo:table-body>
		</fo:table>

		<!--	Acting as lender date line	-->
		<fo:table padding-bottom="4px">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="125mm"/>
			<fo:table-column/>				
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell margin-left="0px" margin-right="0px" margin-bottom="0px"  padding-top="4px">
						<fo:block>A signed commitment to fund the mortgage described below was obtained on</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" padding-top="4px" text-align="center">
						<fo:block><xsl:value-of select="//commitDate"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
					
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell/>
					<fo:table-cell font-size="8px" text-align="center" padding-top="2px">
						<fo:block>(date)</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>
	

	<!-- ************************************************************* -->
	<!-- Section 3. - Principal amount of ....                            -->
	<!--                                                                                    -->
	<xsl:template name="Section3_PrincipalAmount">
		<fo:table>
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="42mm"/>
			<fo:table-column column-width="20mm"/>
			<fo:table-column column-width="5mm"/>
			<fo:table-column column-width="20mm"/>
			<fo:table-column column-width="70mm"/>
			<fo:table-column column-width="24mm"/>
			<fo:table-column/>
			<fo:table-body>
				<fo:table-row margin-left="0px" margin-right="0px">
					<fo:table-cell text-align="center" font-weight="bold">
						<fo:block>3.</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold">
						<fo:block>Principal amount of the</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center">
						<fo:block><xsl:value-of select="//regularOrCollateral"/></fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" font-weight="bold">
						<fo:block>,</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center">
						<fo:block><xsl:value-of select="//firstSecondOrThird"/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="4px" font-weight="bold">
						<fo:block>mortgage to be repaid by the borrower </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt">
						<fo:block>$<xsl:value-of select='format-number(//principalAmt, "###,###,###.00")'/></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:block start-indent="49mm" font-size="8px" line-height="8px" padding="2px" padding-bottom="6px">
			<fo:inline>(regular or collateral)</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="2mm"></fo:leader></fo:inline>
			<fo:inline>(1st, 2nd or 3rd)</fo:inline>
		</fo:block>
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>
	
	
	<!-- ************************************************************* -->
	<!-- Section 4. - The principal amount ...                          -->
	<!--                                                                                   -->
	<xsl:template name="Section4_ThePrincipalAmount">

		<fo:table padding-bottom="4px">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="97mm"/>
			<fo:table-column column-width="16mm"/>
			<fo:table-column/>
			<fo:table-column column-width="3mm"/>
			<fo:table-body>
				<fo:table-row font-weight="bold" margin-left="0px" margin-right="0px">
					<fo:table-cell text-align="center">
						<fo:block>4.</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>The principal amount of the mortgage will bear interest at</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center" font-weight="normal">
						<fo:block><xsl:value-of select="//intRate"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>per year or at the variable interest rate of</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:table>
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="20mm"/>
			<fo:table-column column-width="45mm"/>
			<fo:table-column column-width="25mm"/>
			<fo:table-column column-width="30mm"/>
			<fo:table-column column-width="20mm"/>
			<fo:table-column column-width="5mm"/>
			<fo:table-column column-width="20mm"/>
			<fo:table-column/>
			<fo:table-body>
				<fo:table-row font-weight="bold" margin-left="0px" margin-right="0px" padding-left="4px">
					<fo:table-cell/>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center" font-weight="normal">
						<fo:block><xsl:value-of select="//variableIntRate"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>and will be repayable in</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center" font-weight="normal">
						<fo:block><xsl:value-of select="//repayFreq"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>installments of </fo:block>
					 </fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center" font-weight="normal">
						<fo:block>$<xsl:value-of select='format-number(//freqPmtAmt, "###,###.00")'/></fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>,</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center" font-weight="normal">
						<fo:block><xsl:value-of select="//plusOrIncluding"/></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>interest.</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:block start-indent="12mm" line-height="8px" font-size="8px" padding-top="2px" padding-bottom="6px">
			<fo:inline>(description)</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="46mm"></fo:leader></fo:inline>
			<fo:inline>(weekly, monthly, etc.)</fo:inline>
		</fo:block>
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>
	
	<!-- ************************************************************* -->
	<!-- Section 5. - The mortgage will be amortized ...         -->
	<!--                                                                                   -->
	<xsl:template name="Section5_TheMortgageWillBeAmortized">
		<fo:table>
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="62mm"/>
			<fo:table-column column-width="12mm"/>
			<fo:table-column column-width="70mm"/>
			<fo:table-column column-width="35mm"/>
			<fo:table-column/>
			<fo:table-body>
				<fo:table-row font-weight="bold" margin-left="0px" margin-right="0px">
					<fo:table-cell text-align="center">
						<fo:block>5.</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>The Mortgage will be amortized over</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center" font-weight="normal">
						<fo:block><xsl:value-of select="//years"/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="4px">
						<fo:block>years and interest is to be compounded</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="solid gray 1pt" text-align="center" font-weight="normal">
						<fo:block><xsl:value-of select="//compounding"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:block height="8px" font-size="8px" padding-top="2px" start-indent="145mm" padding-bottom="6px">
			(monthly, semi-annually, annually, etc.)
		</fo:block>
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>
	
	
	<!-- ************************************************************* -->
	<!-- Section 6. - Fees and costs payable ...                    -->
	<!--                                                                                   -->
	<xsl:template name="Section6_FeesAndCostsPayable">
		<fo:block start-indent="4mm" font-weight="bold" padding-bottom="6px">
			<fo:inline>6.</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="4mm"></fo:leader></fo:inline>
			<fo:inline>Fees and costs payable by the borrower:</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="25mm"></fo:leader></fo:inline>
			<fo:inline>Check if amount is to be deducted from principal</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="4mm"></fo:leader></fo:inline>
			<fo:inline>X</fo:inline>
		</fo:block>
	</xsl:template>
	
	
	<!-- ************************************************************* -->
	<!-- Section 6. - Fees table                                              -->
	<!--                                                                                   -->
	<xsl:template name="Section6_FeesTable">
		<fo:table padding-left="2mm" padding-right="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="15mm"/>
			<fo:table-column column-width="52mm"/>
			<fo:table-column column-width="78mm"/>
			<fo:table-column column-width="42mm"/>
			<fo:table-column/>
			<fo:table-body>
			
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell1" select="'i)'"/>
					<xsl:with-param name="Cell2a" select="'Mortgage Brokers fee or commission: '"/>
					<xsl:with-param name="Cell2b" select="//feeMtgBrokerDesc"/>
					<xsl:with-param name="Cell3" select="//feeMtgBrokerAmt"/>
					<xsl:with-param name="Cell4" select="//isFeeMtgBrokerAmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'first'"/>
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell1" select="'ii)'"/>
					<xsl:with-param name="Cell2a" select="'Bonus: '"/>
					<xsl:with-param name="Cell2b" select="//feeBonusDesc"/>
					<xsl:with-param name="Cell3" select="//feeBonusAmt"/>					
					<xsl:with-param name="Cell4" select="//isFeeBonusAmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'first'"/>					
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell1" select="'iii)'"/>
					<xsl:with-param name="Cell2a" select="'Other lender fees: '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherLendersFeesDesc"/>
					<xsl:with-param name="Cell3" select="//feeOtherLendersFeesAmt"/>
					<xsl:with-param name="Cell4" select="//isFeeOtherLendersFeesAmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'first'"/>					
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell1" select="'iv)'"/>
					<xsl:with-param name="Cell2a" select="'Lenders legal fees and estimated disbursements: '"/>
					<xsl:with-param name="Cell2b" select="//feeLenderLegalDesc"/>
					<xsl:with-param name="Cell3" select="//feeLenderLegalAmt"/>
					<xsl:with-param name="Cell4" select="//isFeeLenderLegalAmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'first'"/>					
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell1" select="'v)'"/>
					<xsl:with-param name="Cell2a" select="'Inspection and appraisal fees: '"/>
					<xsl:with-param name="Cell2b" select="//feeAppraisalDesc"/>
					<xsl:with-param name="Cell3" select="//feeAppraisalAmt"/>
					<xsl:with-param name="Cell4" select="//isFeeAppraisalAmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'first'"/>					
				</xsl:call-template>
				
				<!--	Second part of the table with first group of 4 spanned rows	-->
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell1" select="'vi)'"/>
					<xsl:with-param name="Cell2" select="'Other costs or fees payable to persons other than broker or lender (Itemize; e.g. registration fees, mortgage fees, insurance fees)'"/>
					<xsl:with-param name="Cell2a" select="'1) '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherFee1Desc"/>
					<xsl:with-param name="Cell3" select="//feeOtherFee1Amt"/> 
					<xsl:with-param name="Cell4" select="//isFeeOtherFee1AmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'second'"/>
					<xsl:with-param name="SpannedRowsCreated" select="'no'"/>
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell2a" select="'2) '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherFee2Desc"/>
					<xsl:with-param name="Cell3" select="//feeOtherFee2Amt"/>
					<xsl:with-param name="Cell4" select="//isFeeOtherFee2AmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'second'"/>
					<xsl:with-param name="SpannedRowsCreated" select="'yes'"/>
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell2a" select="'3) '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherFee3Desc"/>
					<xsl:with-param name="Cell3" select="//feeOtherFee3Amt"/>
					<xsl:with-param name="Cell4" select="//isFeeOtherFee3AmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'second'"/>
					<xsl:with-param name="SpannedRowsCreated" select="'yes'"/>
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell2a" select="'4) '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherFee4Desc"/>
					<xsl:with-param name="Cell3" select="//feeOtherFee4Amt"/>
					<xsl:with-param name="Cell4" select="//isFeeOtherFee4AmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'second'"/>
					<xsl:with-param name="SpannedRowsCreated" select="'yes'"/>
				</xsl:call-template>
				
				<!--	Second part of the table with second group of 4 spanned rows	-->
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell1" select="'vii)'"/>
					<xsl:with-param name="Cell2" select="'Any other amount payable by the borrower regarding this mortgage: (Itemize)'"/>
					<xsl:with-param name="Cell2a" select="'1) '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherAmt1Desc"/>
					<xsl:with-param name="Cell3" select="//feeOtherAmt1Amt"/>
					<xsl:with-param name="Cell4" select="//isFeeOtherAmt1AmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'second'"/>
					<xsl:with-param name="SpannedRowsCreated" select="'no'"/>
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell2a" select="'2) '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherAmt2Desc"/>
					<xsl:with-param name="Cell3" select="//feeOtherAmt2Amt"/>
					<xsl:with-param name="Cell4" select="//isFeeOtherAmt2AmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'second'"/>
					<xsl:with-param name="SpannedRowsCreated" select="'yes'"/>
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell2a" select="'3) '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherAmt3Desc"/>
					<xsl:with-param name="Cell3" select="//feeOtherAmt3Amt"/>
					<xsl:with-param name="Cell4" select="//isFeeOtherAmt3AmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'second'"/>
					<xsl:with-param name="SpannedRowsCreated" select="'yes'"/>
				</xsl:call-template>
				
				<xsl:call-template name="FeesTable_ConstructRow">
					<xsl:with-param name="Cell2a" select="'4) '"/>
					<xsl:with-param name="Cell2b" select="//feeOtherAmt4Desc"/>
					<xsl:with-param name="Cell3" select="//feeOtherAmt4Amt"/>
					<xsl:with-param name="Cell4" select="//isFeeOtherAmt4AmtDeducted"/>
					<xsl:with-param name="WhichPartOfTable" select="'second'"/>
					<xsl:with-param name="SpannedRowsCreated" select="'yes'"/>
				</xsl:call-template>
				
				<!--	Total row	-->
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3" padding-top="2mm" padding-right="2mm" padding-bottom="2mm" border-top="solid black 2px" border-right="solid black 2px">
						<fo:block start-indent="35mm">
							<fo:inline font-size="8px">*Client to negotiate fees with an approved solicitor of own choice.</fo:inline>
							<fo:inline><fo:leader leader-pattern="space" leader-length="12mm"></fo:leader></fo:inline>
							<fo:inline font-size="11px" font-weight="bold">TOTAL</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-top="2mm" padding-left="2mm" padding-right="4mm" padding-bottom="2mm" border-top="solid black 2px">
						<fo:block>$<xsl:value-of select='format-number(//feeTotalAmt, "###,###.00")'/></fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="solid black 2px"/>
				</fo:table-row>
				
			</fo:table-body>
		</fo:table>
		
		<!--	Bottom of the table	-->
		<fo:table>
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<xsl:call-template name="FirstThreeColumnsOfBottomOfBigTable"/>
			<fo:table-column column-width="58mm"/>
			<fo:table-column/>
			<fo:table-column column-width="2mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-top="3mm" padding-left="4mm"  border-top="solid black 2px">
						<fo:block font-weight="bold">Principal Amount</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="3mm" padding-right="4mm" border-top="solid black 2px" border-bottom="solid grey 1px" text-align="right">
						<fo:block>$<xsl:value-of select='format-number(//principalAmt, "###,###.00")'/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="3mm" border-top="solid black 2px" border-right="solid grey 1px"/>
					<fo:table-cell padding-top="3mm" padding-left="2mm" border-top="solid black 2px">
						<fo:block font-weight="bold">TOTAL COST OF BORROWING:</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding-top="3mm" padding-left="2mm" padding-right="8mm" border-bottom="solid grey 1px" border-top="solid black 2px">
						<fo:block><xsl:value-of select="//totalCostOfBorrowingPct"/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="3mm" border-top="solid black 2px"/>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-top="3mm" padding-left="4mm">
						<fo:block font-weight="bold">Less Fees (X in Item 6)</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="3mm" padding-right="4mm" border-bottom="solid grey 1px" text-align="right">
						<fo:block>$<xsl:value-of select='format-number(//feeTotalDeductedAmt,"###,###.00")'/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="3mm" border-right="solid grey 1px"/>
					<fo:table-cell number-columns-spanned="3" number-rows-spanned="2" padding-top="2mm" padding-left="2mm" padding-right="2mm"  text-align="justify">
						<fo:block line-height="11px">(i.e. costs, including rate of interest shown on mortgage and all costs itemized in paragraph 6, expressed as a percentage accurate to within one eighth of 1 per cent.)</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-top="3mm" padding-left="4mm" >
						<fo:block font-weight="bold">equals NET ADVANCE OF FUNDS:</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="3mm" padding-right="4mm" border-bottom="solid grey 1px" text-align="right">
						<fo:block>$<xsl:value-of select='format-number(//netAdvanceAmt,"###,###.00")'/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="3mm" border-right="solid grey 1px"/>
					<fo:table-cell padding-top="3mm"/>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell/>
					<fo:table-cell height="8px" border-right="solid grey 1px"/>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<!-- ************************************************************* -->
	<!-- First 3 columns of the big table's bottom                   -->
	<!--                                                                                   -->
	<xsl:template name="FirstThreeColumnsOfBottomOfBigTable">
		<fo:table-column column-width="67mm"/>
		<fo:table-column column-width="32mm"/>
		<fo:table-column column-width="3mm"/>
	</xsl:template>
	

	<!-- ************************************************************* -->
	<!-- Big table's footer                                                       -->
	<!--                                                                                   -->
	<xsl:template name="BigTableFooter">
		<fo:table padding-right="0mm" margin-right="0mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<xsl:copy use-attribute-sets="NormalFont_10Left"/>
			<xsl:call-template name="FirstThreeColumnsOfBottomOfBigTable"/>
			<fo:table-column column-width="50mm"/>
			<fo:table-column/>
			
			<fo:table-body>
				<fo:table-row height="30px">
					<fo:table-cell number-columns-spanned="3" border-right="solid grey 1px"/>
					<fo:table-cell padding-left="2mm" padding-top="1mm" border-bottom="solid grey 1px" border-right="solid grey 1px">
						<fo:block>Initials</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2mm" padding-top="1mm" border-bottom="solid grey 1px" border-right="solid grey 1px">
						<fo:block>Date</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>


	<!-- ************************************************************* -->
	<!-- FeesTable_ConstructRow                                        -->
	<!--                                                                                   -->
	<xsl:template name="FeesTable_ConstructRow">
		<xsl:param name="Cell1"/>
		<xsl:param name="Cell2"/>
		<xsl:param name="Cell2a"/>
		<xsl:param name="Cell2b"/>
		<xsl:param name="Cell3"/>
		<xsl:param name="Cell4"/>
		<xsl:param name="WhichPartOfTable"/>
		<xsl:param name="SpannedRowsCreated"/>
		
		<fo:table-row height="6mm">
			<xsl:if test="$WhichPartOfTable='first'">
				<fo:table-cell text-align="right"  padding-top="4px" padding-right="2mm" border-top="solid gray 1pt" border-right="solid gray 1pt">
					<fo:block><xsl:value-of select="$Cell1"/></fo:block>
				</fo:table-cell>
				<fo:table-cell number-columns-spanned="2"  padding-bottom="2px" padding-top="4px" padding-left="2mm" padding-right="2mm" border-top="solid gray 1pt" border-right="solid black 2pt">
					<fo:block>
						<fo:inline><xsl:value-of select="$Cell2a"/></fo:inline>
						<fo:inline font-size="9px"><xsl:value-of select="$Cell2b"/></fo:inline>
					</fo:block>
				</fo:table-cell>
			</xsl:if>
					
			<xsl:if test="$WhichPartOfTable='second'">
				<xsl:if test="$SpannedRowsCreated='no'">
					<fo:table-cell number-rows-spanned="4" text-align="right" padding-top="4px" padding-right="2mm" border-top="solid gray 1pt" border-right="solid gray 1pt">
						<fo:block><xsl:value-of select="$Cell1"/></fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="4" padding-top="4px" padding-left="2mm" padding-right="2mm" border-top="solid gray 1pt" border-right="solid gray 1pt">
						<fo:block line-height="12px"><xsl:value-of select="$Cell2"/></fo:block>
					</fo:table-cell>
				</xsl:if>
				<fo:table-cell padding-top="4px" padding-left="5mm" padding-right="2mm" border-top="solid gray 1pt" border-right="solid black 2pt">
					<fo:block line-height="12px">
						<fo:inline><xsl:value-of select="$Cell2a"/></fo:inline>
						<fo:inline><xsl:value-of select="$Cell2b"/></fo:inline>
					</fo:block>
				</fo:table-cell>
			</xsl:if>
				
			<fo:table-cell padding-top="4px" padding-left="2mm" padding-right="4mm" border-top="solid gray 1pt" border-right="solid gray 1pt" text-align="right">
				<xsl:if test="$Cell3">
					<fo:block>$<xsl:value-of select='format-number($Cell3,"###,###.00")'/></fo:block>
				</xsl:if>
			</fo:table-cell>
					
			<fo:table-cell padding-top="4px" border-top="solid gray 1pt" text-align="center">
				<fo:block><xsl:if test="$Cell4"> X </xsl:if></fo:block>
			</fo:table-cell>
				
		</fo:table-row>

	</xsl:template>


	<!-- ******************************************************************* -->
	<!-- Section 7 - The mortgage will be due and payable ...      -->
	<!--                                                                                            -->
	<xsl:template name="Section7_TheMortgageWillBeDueAndPayable">
		<fo:table padding-top="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column column-width="80mm"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column column-width="6mm"/>
			<fo:table-column column-width="16mm"/>
			<fo:table-column/>
			<fo:table-body>
				<fo:table-row font-weight="bold">
					<fo:table-cell text-align="center"><fo:block>7.</fo:block></fo:table-cell>
					<fo:table-cell ><fo:block>The mortgage will become due and payable in</fo:block></fo:table-cell>
					<fo:table-cell border-bottom="solid grey 1px" font-weight="normal" text-align="center"><fo:block><xsl:value-of select="//termLength"/></fo:block></fo:table-cell>
					<fo:table-cell/>
					<fo:table-cell border-bottom="solid grey 1px" font-weight="normal" text-align="center"><fo:block><xsl:value-of select="//termLengthUnits"/>,</fo:block></fo:table-cell>
					<fo:table-cell padding-left="2mm"><fo:block> at which time the borrower, if all payments</fo:block></fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:block start-indent="86.5mm" font-size="8px" padding-top="1mm">
			<fo:inline>(number)</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="2mm"></fo:leader></fo:inline>
			<fo:inline>(months, years)</fo:inline>
		</fo:block>
			
		<fo:table padding-top="2mm" padding-bottom="5mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column column-width="133mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell/>					
					<fo:table-cell font-weight="bold"><fo:block>are made on the due date and any prepayment privilege is not used, will owe </fo:block></fo:table-cell>
					<fo:table-cell border-bottom="solid grey 1px"><fo:block>$<xsl:value-of select='format-number(//remainingAmt, "###,###.00")'/></fo:block></fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
			
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>
	

	<!-- ******************************************************************* -->
	<!-- Section 8 - The mortgage is not renewable ...                  -->
	<!--                                                                                            -->
	<xsl:template name="Section8_TheMortgageIsNotRenewable">
	
		<fo:table font-weight="bold" padding-bottom="5mm" line-height="12px" padding-top="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell text-align="center"><fo:block>8.</fo:block></fo:table-cell>
					<fo:table-cell>
						<fo:block>The mortgage is not renewable on the same terms as described in items 4 and 5 above and does not contain any privileges or penalties (including charges for NSF cheques), except as follows:</fo:block>
					</fo:table-cell>
					<fo:table-cell/>
				</fo:table-row>
				<fo:table-row font-weight="normal"><!--#DG480 height="34mm" -->
					<fo:table-cell/>
					<fo:table-cell padding-top="3mm">
						<fo:block linefeed-treatment="preserve">
							<!--#DG480 xsl:call-template name="str_tokenizer">
								<xsl:with-param name="string" select='string(//privilegesOrPenalties)'/>
								<xsl:with-param name="delimiters" select="'&#xD;&#xA;'" />
								<xsl:with-param name="tokendtag" select="'fo:block'" />
								<xsl:with-param name="inp" select="substring(/*/privilegesOrPenalties,1,702)"/>
							</xsl:call-template-->
							<xsl:call-template name="makeWhollyBreakable">
								<xsl:with-param name="inp" select="substring(/*/privilegesOrPenalties,1,8*$maxChars)"/>
							</xsl:call-template>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>

	
	<!-- ******************************************************************* -->
	<!-- Section 9 - Other terms and conditions ...                         -->
	<!--                                                                                            -->
	<xsl:template name="Section9_OtherTermsAndConditions">
		<fo:block font-weight="bold" start-indent="3mm" padding-bottom="3mm" padding-top="2mm">
			<fo:inline>9.</fo:inline>
			<fo:inline><fo:leader leader-pattern="space" leader-length="3mm"/></fo:inline>
			<fo:inline>Other terms and conditions of the mortgage:</fo:inline>
		</fo:block>
		<fo:table padding-bottom="5mm"><!--#DG480  height="34mm"-->
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell line-height="12px">
						<fo:block linefeed-treatment="preserve">
							<!--xsl:call-template name="str_tokenizer">
								<xsl:with-param name="string" select='string(//otherTermsAndConditions)'/>
								<xsl:with-param name="delimiters" select="'&#xD;&#xA;'" />
								<xsl:with-param name="tokendtag" select="'fo:block'" />
								<xsl:with-param name="inp" select="substring(/*/otherTermsAndConditions,1,702)"/>
							</xsl:call-template-->
							<xsl:call-template name="makeWhollyBreakable">
								<xsl:with-param name="inp" select="substring(/*/otherTermsAndConditions,1,9*$maxChars)"/>
							</xsl:call-template>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell/>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>
	
	
	<!-- ******************************************************************* -->
	<!-- Section 10 - This mortgage shall be ...                              -->
	<!--                                                                                            -->
	<xsl:template name="Section10_ThisMortgageShallBe">
		<fo:table padding-bottom="5mm" padding-top="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column column-width="85mm"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="15mm"/>
			<fo:table-column column-width="30mm"/>
			<fo:table-column column-width="5mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
			
				<fo:table-row font-weight="bold">
					<fo:table-cell text-align="center"><fo:block>10.</fo:block></fo:table-cell>
					<fo:table-cell><fo:block>This mortgage shall be arranged on or before the</fo:block></fo:table-cell>
					<fo:table-cell border-bottom="solid grey 1px" text-align="center" font-weight="normal"><fo:block><xsl:value-of select="substring(//commitExpiresDate,1,2)"/></fo:block></fo:table-cell>
					<fo:table-cell padding-left="2mm"><fo:block>day of</fo:block></fo:table-cell>
					<fo:table-cell border-bottom="solid grey 1px" text-align="center" font-weight="normal"><fo:block><xsl:value-of select="substring-before(substring-after(//commitExpiresDate, '-'), '-')"/></fo:block></fo:table-cell>
					<fo:table-cell font-weight="normal" padding-left="2mm"><fo:block>,</fo:block></fo:table-cell>
					<fo:table-cell border-bottom="solid grey 1px" text-align="center" font-weight="normal"><fo:block><xsl:value-of select="substring-after(substring-after(//commitExpiresDate, '-'), '-')"/></fo:block></fo:table-cell>
				</fo:table-row>
				
				<fo:table-row font-size="8px">
					<fo:table-cell number-columns-spanned="4"/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block>(month)</fo:block></fo:table-cell>
					<fo:table-cell/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block>(year)</fo:block></fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
		</fo:table>
	
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>
	
	
	<!-- ******************************************************************* -->
	<!-- Section 11 - Name of morgage broker ...                           -->
	<!--                                                                                            -->
	<xsl:template name="Section11_NameOfMortgageBroker">
		<fo:table padding-top="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column column-width="125mm"/>
			<fo:table-column column-width="5mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
			
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="center"><fo:block>11.</fo:block></fo:table-cell>
					<fo:table-cell border-bottom="solid grey 1px"><fo:block><xsl:value-of select="//bkrName"/></fo:block></fo:table-cell>
					<fo:table-cell padding-left="2mm"/>
					<fo:table-cell border-bottom="solid grey 1px"><fo:block><xsl:value-of select="//bkrRegNumber"/></fo:block></fo:table-cell>
				</fo:table-row>
				
				<fo:table-row font-size="8px">
					<fo:table-cell/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block>(name of mortgage broker)</fo:block></fo:table-cell>
					<fo:table-cell/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block>(registration no.)</fo:block></fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:block start-indent="8mm" font-weight="bold" padding-top="3mm" padding-bottom="5mm">
			<fo:inline>is presently registered and in good standing as a mortgage broker under the </fo:inline>
			<fo:inline font-style="italic">Mortgage Brokers Act. </fo:inline>
		</fo:block>
		
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>


	<!-- ******************************************************************* -->
	<!-- Section 12 - The mortgage broker has not ...                    -->
	<!--                                                                                            -->
	<xsl:template name="Section11_TheMortgageBrokerHasNot">
		<fo:table padding-top="2mm" padding-bottom="5mm" font-weight="bold">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
			
				<fo:table-row line-height="12px">
					<fo:table-cell text-align="center"><fo:block>12.</fo:block></fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline>
								The mortgage broker has not requested or required the borrower to sign the mortgage instrument (whether completed or blank) or any 
								commitment to enter into the mortgage, and will not do so until permitted to do so by the regulations made under the
							</fo:inline>
							<fo:inline font-style="italic"> Mortgage Brokers Act. </fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:call-template name="DisclosureSectionsSeparator"/>
	</xsl:template>


	<!-- ******************************************************************* -->
	<!-- Section 13 - I have fully completed ...                                -->
	<!--                                                                                            -->
	<xsl:template name="Section12_IHaveFullyCompleted">
		<fo:table padding-top="2mm" font-weight="bold">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row font-weight="bold" line-height="12px">
					<fo:table-cell text-align="center"><fo:block>13.</fo:block></fo:table-cell>
					<fo:table-cell><fo:block>I have fully completed the above Statement of Mortgage in accordance with the Mortgage Brokers Act and regulations. </fo:block></fo:table-cell>
					<fo:table-cell/>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:table padding-bottom="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="15mm"/>
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="40mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
			
				<fo:table-row height="8mm">
					<fo:table-cell/>
					<fo:table-cell padding-top="2mm" border-bottom="solid gray 1px" text-align="center"><fo:block><xsl:value-of select="//bkrSignatureDate"/></fo:block></fo:table-cell>
					<fo:table-cell/>
					<fo:table-cell border-bottom="solid gray 1px"/>
				</fo:table-row>
				
				<fo:table-row font-size="8px">
					<fo:table-cell/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block>Date</fo:block></fo:table-cell>
					<fo:table-cell/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block start-indent="8mm" end-indent="8mm">Signature of Mortgage Broker, or of a person authorized to sign on behalf of the mortgage broker</fo:block></fo:table-cell>
				</fo:table-row>
				
				<fo:table-row height="5mm">
					<fo:table-cell/>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3"/>
					<fo:table-cell border-bottom="solid gray 1px" padding-bottom="1mm" text-align="center"><fo:block><xsl:value-of select="//bkrFullName"/></fo:block></fo:table-cell>
				</fo:table-row>
				
				<fo:table-row font-size="8px">
					<fo:table-cell number-columns-spanned="3"/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block>Print name of person signing</fo:block></fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	<!-- ******************************************************************* -->
	<!-- Bottom part of the second page                                        -->
	<!--                                                                                            -->
	<xsl:template name="BottomPartOfSecondPage">
		<fo:table padding-before="8mm" padding-bottom="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="6mm"/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="8mm"/>
			<fo:table-column/>
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				
				<fo:table-row font-weight="bold">
					<fo:table-cell/>
					<fo:table-cell><fo:block>I/We, </fo:block></fo:table-cell>
					<fo:table-cell font-weight="normal" border-bottom="solid grey 1px"><fo:block><xsl:value-of select="//borrowerName"/></fo:block></fo:table-cell>
					<fo:table-cell padding-left="1mm" padding-right="1mm"><fo:block>, of </fo:block></fo:table-cell>
					<fo:table-cell font-weight="normal" border-bottom="solid grey 1px"><fo:block><xsl:value-of select="//borrowerAddr"/></fo:block></fo:table-cell>
					<fo:table-cell padding-left="1mm" padding-right="1mm"><fo:block>, the</fo:block></fo:table-cell>
					<fo:table-cell/>
				</fo:table-row>
				
				<!--fo:table-row height="10mm"><fo:table-cell/></fo:table-row-->
				
				<fo:table-row font-weight="bold">
					<fo:table-cell/>
					<fo:table-cell number-columns-spanned="5" padding-top="9mm"><fo:block text-align="justify">borrower(s) under the proposed mortgage, have read and fully understand the above Statement of Mortgage</fo:block></fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
		</fo:table>
		
		<fo:table padding-top="3mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="6mm"/>
			<fo:table-column column-width="41mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell font-weight="bold" padding-right="3mm"><fo:block>furnished to me/us by: </fo:block></fo:table-cell>
					<fo:table-cell border-bottom="solid grey 1px">\
					<fo:block>
					
					 <!-- Commented by Venkata for  - Hardcoding Lender Name -  CR
							<xsl:value-of select="//bkrOneLineNameAndAddress" />
						Begin -->
						  <xsl:variable name="myString" select="//bkrOneLineNameAndAddress" />
						  <xsl:variable name="myNewString">
						    <xsl:call-template name="replace-string">
						      <xsl:with-param name="text" select="string($myString)"/>
						      <xsl:with-param name="replace" select="'GE Money Broker Prime'"/>
						      <xsl:with-param name="with" select="$lenderName"/>
						    </xsl:call-template>
						  </xsl:variable>
						  
						  <xsl:variable name="myNewString1">
						    <xsl:call-template name="replace-string">
						      <xsl:with-param name="text" select="string($myNewString)"/>
						      <xsl:with-param name="replace" select="'GE Money Prime'"/>
						      <xsl:with-param name="with" select="$lenderName"/>
						    </xsl:call-template>
						  </xsl:variable> 
						 <xsl:value-of select="$myNewString1"/> 
						 <!-- Commented by Venkata for  - Hardcoding Lender Name - CR End -->

					</fo:block></fo:table-cell>
				</fo:table-row>
				
				<fo:table-row height="8px" font-size="8px">
					<fo:table-cell/>
					<fo:table-cell/>
					<fo:table-cell padding-top="1mm" padding-bottom="4mm" text-align="center"><fo:block>Name and address of mortgage  broker</fo:block></fo:table-cell>
				</fo:table-row>
				
				<fo:table-row font-weight="bold">
					<fo:table-cell/>
					<fo:table-cell number-columns-spanned="2" padding-bottom="4mm"><fo:block text-align="justify">I/We  have not yet signed the mortgage instrument (whether completed or blank) or a commitment to enter into the mortgage.</fo:block></fo:table-cell>
				</fo:table-row>
				
				<fo:table-row font-weight="bold">
					<fo:table-cell/>
					<fo:table-cell number-columns-spanned="2" padding-bottom="15mm"><fo:block text-align="justify">I/We acknowledge receipt of a fully completed copy of this Statement of Mortgage, signed by the mortgage broker.</fo:block></fo:table-cell>
				</fo:table-row>

			</fo:table-body>
		</fo:table>
		
		<fo:table  padding-bottom="2mm">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column column-width="6mm"/>
			<fo:table-column column-width="70mm"/>
			<fo:table-column column-width="30mm"/>
			<fo:table-column/>
			<fo:table-column column-width="5mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell/>
					<fo:table-cell border-bottom="solid grey 1px"/>
					<fo:table-cell/>
					<fo:table-cell border-bottom="solid grey 1px"/>
					<fo:table-cell/>
				</fo:table-row>
				
				<fo:table-row height="8px" font-size="8px">
					<fo:table-cell/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block>Dated By Borrowers</fo:block></fo:table-cell>
					<fo:table-cell/>
					<fo:table-cell text-align="center" padding-top="1mm"><fo:block>Signatures of Borrowers</fo:block></fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

	</xsl:template>
	
	 <!-- Venkata for  - Hardcoding Lender Name - CR Begin -->
	 <xsl:template name="replace-string">
	    <xsl:param name="text"/>
	    <xsl:param name="replace"/>
	    <xsl:param name="with"/>
	    <xsl:choose>
	      <xsl:when test="contains($text,$replace)">
		<xsl:value-of select="substring-before($text,$replace)"/>
		<xsl:value-of select="$with"/>
		<xsl:call-template name="replace-string">
		  <xsl:with-param name="text"
	select="substring-after($text,$replace)"/>
		  <xsl:with-param name="replace" select="$replace"/>
		  <xsl:with-param name="with" select="$with"/>
		</xsl:call-template>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:value-of select="$text"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:template>
	 <!-- Venkata for  - Hardcoding Lender Name - CR End -->
	
	<!-- ************************************************************* -->
	<!-- Document section's separator                                  -->
	<!--                                                                                   -->
	<xsl:template name="DisclosureSectionsSeparator">
		<fo:table padding-bottom="6pt">
			<xsl:copy use-attribute-sets="GeneralTable"/>
			<fo:table-column/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell border-bottom="solid gray 1pt"/>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	<!-- ************************************************************* -->
	<!-- Checked Check Box                                                  -->
	<!--                                                                                   -->
	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="11px" height="11px">
			<svg xmlns="http://www.w3.org/2000/svg" width="11px" height="11px">
				<rect x="0" y="0" width="11" height="11" style="fill: none; stroke: black"/>
				<line x1="0" y1="0" x2="11" y2="11" style="stroke:black"/>
				<line x1="0" y1="11" x2="10" y2="0" style="stroke:black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	
	<!-- ************************************************************* -->
	<!-- Unchecked Check Box                                              -->
	<!--                                                                                   -->
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="11px" height="11px">
			<svg xmlns="http://www.w3.org/2000/svg" width="11px" height="11px">
				<rect x="0" y="0" width="11" height="11" style="fill: none; stroke: black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	
	<!-- ************************************************************* -->
	<!-- Break Page                                                                -->
	<!--                                                                                   -->
	<xsl:template name="BreakPage">
		<fo:block break-after="page"/>
	</xsl:template>
	

	<!--#DG480 not used anymore -->
	
<!-- ********************************************************************************************************************** 
                     Tokenize strings                                                                                                                      
                                                                                                                                                                     
	    Splits passed string using string of delimiters defind in second input parameter and forms element 
        with tag name defined in third parameter. Empty string of delimiters will make this template return   
        character by character of the input string                                                                                          
                                                                                                                                                                     
        Tag name here will be <fo:block>                                                                                                        
- ->
	                                                                                                                                                                  
	<xsl:template name="str_tokenizer">
		<xsl:param name="string" select="''" />
		<xsl:param name="delimiters" select="'&#xD;&#xA;&#x9;&#x20;'" />
		<xsl:param name="tokendtag" select="'token'"/>
		
		<xsl:choose>
			<xsl:when test="not($string)" />
			<xsl:when test="not($delimiters)">
				<xsl:call-template name="str_tokenize-characters">
					<xsl:with-param name="string" select="$string" />
					<xsl:with-param name="tokendtag" select="$tokendtag" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="str_tokenize-delimiters">
					<xsl:with-param name="string" select="$string" />
					<xsl:with-param name="delimiters" select="$delimiters" />
					<xsl:with-param name="tokendtag" select="$tokendtag" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template-->

    <!--                                                                                                                                                                                    -->
	<!-- str_tokenize-characters: in case of empty string of delimiters, returns character by character of the input string -->
    <!--                                                                                                                                                                                    - ->
	<xsl:template name="str_tokenize-characters">
		<xsl:param name="string" />
		<xsl:param name="tokendtag" select="'token'"/>
		
		<xsl:if test="$string">
			<xsl:element name="{$tokendtag}"><xsl:value-of select="substring($string, 1, 1)" /></xsl:element>
			<xsl:call-template name="str_tokenize-characters">
				<xsl:with-param name="string" select="substring($string, 2)" />
				<xsl:with-param name="tokendtag" select="$tokendtag" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template-->
	
    <!--                                                                                                                                                                                    -->	
	<!-- str_tokenize-delimiters: does actual parsing                                                                                                             -->
    <!--                                                                                                                                                                                    - ->
	<xsl:template name="str_tokenize-delimiters">
		<xsl:param name="string" />
		<xsl:param name="delimiters" />
		<xsl:param name="tokendtag" select="'token'" />
		
		<xsl:variable name="delimiter" select="substring($delimiters, 1, 1)" />
		<xsl:choose>
			<xsl:when test="not($delimiter)">
				<xsl:element name="{$tokendtag}"><xsl:value-of select="$string" /></xsl:element>
			</xsl:when>
			<xsl:when test="contains($string, $delimiter)">
				<xsl:if test="not(starts-with($string, $delimiter))">
					<xsl:call-template name="str_tokenize-delimiters">
						<xsl:with-param name="string" select="substring-before($string, $delimiter)" />
						<xsl:with-param name="delimiters" select="substring($delimiters, 2)" />
						<xsl:with-param name="tokendtag" select="$tokendtag" />
					</xsl:call-template>
				</xsl:if>
				<xsl:call-template name="str_tokenize-delimiters">
					<xsl:with-param name="string" select="substring-after($string, $delimiter)" />
					<xsl:with-param name="delimiters" select="$delimiters" />
					<xsl:with-param name="tokendtag" select="$tokendtag" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="str_tokenize-delimiters">
					<xsl:with-param name="string" select="$string" />
					<xsl:with-param name="delimiters" select="substring($delimiters, 2)" />
					<xsl:with-param name="tokendtag" select="$tokendtag" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template-->
	
	<!--#DG480 receives a string as input and inserts breakable ZERO WIDTH SPACE  -->
	<xsl:template name="makeWhollyBreakable">
		<xsl:param name="inp" select="''"/>
	  <xsl:variable name="inpc" select="java:replaceAll($inp,'\s', '&#x00A0;')"/>
	  <xsl:variable name="resulta" select="java:replaceAll($inpc,'\S', '$0&#x200B;')"/>
	  <xsl:value-of select="$resulta"/>
	</xsl:template>

	
	<!-- ************************************************************************* -->
	<!--  Define attribute sets                                                                 -->
	<!--                                                                                                   -->
	<!-- ************************************************************************* -->
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	
	
	<!-- ************************************************************************* -->
	<!-- Title Font                                                                                    -->
	<!--                                                                                                   -->
	<xsl:attribute-set name="TitleFont">
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFont_FirstRow" use-attribute-sets="TitleFont">
		<xsl:attribute name="line-height">14pt</xsl:attribute>
		<xsl:attribute name="font-style">italic</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFont_SecondRow" use-attribute-sets="TitleFont">
		<xsl:attribute name="line-height">24pt</xsl:attribute>
		<xsl:attribute name="font-size">16pt</xsl:attribute>
	</xsl:attribute-set>
	
	
	<!-- ************************************************************************* -->
	<!-- Normal font                                                                                -->
	<!--                                                                                                   -->	
	<xsl:attribute-set name="NormalFont">
		<xsl:attribute name="font-weight">normal</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="NormalFont_9Left" use-attribute-sets="NormalFont">
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="text-align">start</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="NormalFont_9Justify" use-attribute-sets="NormalFont">
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="NormalFont_10Left" use-attribute-sets="NormalFont">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="NormalFont_10Right" use-attribute-sets="NormalFont">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="NormalFont_10Justify" use-attribute-sets="NormalFont">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	
	
	<!-- ************************************************************************* -->
	<!-- TransactionNoFirstRow                                                            -->
	<!--                                                                                                   -->
	<xsl:attribute-set name="TrasactionNoFirstRow" use-attribute-sets="NormalFont_9Left">
		<xsl:attribute name="border">solid gray 1px</xsl:attribute>
		<xsl:attribute name="border-bottom-style">none</xsl:attribute>
		<xsl:attribute name="padding-left">5pt</xsl:attribute>
		<xsl:attribute name="padding-top">2pt</xsl:attribute>
	</xsl:attribute-set>
	<!-- TransactionNoSecondRow -->
	<xsl:attribute-set name="TrasactionNoSecondRow" use-attribute-sets="NormalFont_10Right">
		<xsl:attribute name="border">solid gray 1px</xsl:attribute>
		<xsl:attribute name="border-top-style">none</xsl:attribute>
		<xsl:attribute name="padding-right">15mm</xsl:attribute>
	</xsl:attribute-set>
	
	
	<!-- *************************************************************************-->
	<!-- List Block Indentation                                                                -->
	<!--                                                                                                  -->
	<xsl:attribute-set name="NormalListBlock">
		<xsl:attribute name="start-indent">5mm</xsl:attribute>
		<xsl:attribute name="end-indent">10mm</xsl:attribute>
	</xsl:attribute-set>
	<!-- List Block Body Start -->
	<xsl:attribute-set name="NormalListBlockBody">
		<xsl:attribute name="start-indent">12mm</xsl:attribute>
	</xsl:attribute-set>

	
	<!-- *************************************************************************-->
	<!-- General tabel attributes                                                            -->
	<!--                                                                                                  -->
	<xsl:attribute-set name="GeneralTable">
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
		<xsl:attribute name="inline-progression-dimension.optimum">20px</xsl:attribute>
	</xsl:attribute-set>
	
</xsl:stylesheet>
	