<?xml version="1.0" encoding="UTF-8"?>
<!--22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik-->
<!-- Author Zivko Radulovic -->
<!-- edited by Catherine Rutgaizer, March 2004, ex-CR053_f, page 3 -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:template name="CreatePageThree">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					<!-- Page <fo:page-number/> of <fo:page-number-citation ref-id="page_3"/></xsl:element> -->
					Page 1 de 1</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage3"/>
				<xsl:call-template name="Page_3_Item_1"/>
				<fo:block id="page_3" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_3_Item_1">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Le prêt sera consenti aux conditions suivantes, en plus de celles en usage à la caisse.</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute> -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 1. DOCUMENTS À REMETTRE AU NOTAIRE</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Sur réception de la présente, vous devez transmettre au notaire les documents suivants s'ils sont disponibles, et tout autre document que le notaire pourra exiger :</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<fo:list-block>
							<xsl:attribute name="start-indent">1mm</xsl:attribute>
							<xsl:attribute name="provisional-distance-between-starts">12mm</xsl:attribute>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										votre contrat de mariage, le cas échéant;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ======================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										votre titre d'acquisition de l'immeuble, le cas échéant;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										copie du contrat préliminaire (offre d'achat) et de la note d'information, s'il y a lieu;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										un certificat de localisation à la satisfaction de la caisse, ou conforme aux spécifications prévues au recto, le cas échéant;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										une police d'assurance incendie et autres risques pour un montant suffisant pour couvrir le prêt, et l'ouverture de crédit le cas échéant, ou une note de couverture ou s'il s'agit d'un condominium, le certificat attestant qu'une assurance couvrant l'immeuble de la copropriété a été souscrite par les administrateurs;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										les reçus de taxes municipales et scolaires; et,
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										s'il s'agit d'une personne morale, la preuve de son existence juridique et de son pouvoir d'effectuer l'emprunt projeté.
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
						</fo:list-block>
						<!-- =============================================================================-->
						<!-- GROUP 2  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 2. SIGNATURE DE L'ACTE HYPOTHÉCAIRE ET DÉBOURSEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Vous devrez signer un acte de prêt hypothécaire selon les formulaires en usage à la caisse. Le prêt sera déboursé lorsqu'il aura été démontré à la caisse, à sa satisfaction :</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<fo:list-block>
							<xsl:attribute name="start-indent">1mm</xsl:attribute>
							<xsl:attribute name="provisional-distance-between-starts">12mm</xsl:attribute>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										qu'elle détient une bonne et valable hypothèque du rang exigé;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ======================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>
										que l'immeuble est assuré contre l'incendie et autres risques pour un montant suffisant pour couvrir la créance de la caisse, et que la police contient la clause relative à la garantie hypothécaire en faveur de la caisse;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										que toutes les taxes ou charges pouvant affecter l'immeuble sont payées;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="text-align">justify</xsl:attribute>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										que toute irrégularité pouvant avoir été constatée dans le certificat de localisation est corrigée;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										que le certificat d'autorisation exigé par la Loi sur la qualité de l'environnement (L.R.Q., c. Q.-2) est délivré au nom de l'emprunteur;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										que toutes les conditions applicables au prêt sont respectées.
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
						</fo:list-block>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>L'ouverture de crédit, le cas échéant, sera déboursée uniquement si vous vous prévalez du privilège conféré par la clause "Protection propriétaire" conformément aux conditions de l'acte hypothécaire.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 3  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 3. CONSTRUCTION, RÉPARATION OU AMÉLIORATION D'UN IMMEUBLE</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>S'il s'agit d'un prêt à des fins de construction, de réparation ou d'amélioration d'un immeuble, le prêt sera versé au moyen de
déboursements successifs, au fur et à mesure de la progression de travaux. Vous devrez, avant chaque déboursé, remettre à la
caisse ou au notaire, une déclaration solennelle relative aux hypothèques légales de construction. Si les travaux on été confiés à
un entrepreneur général, les déboursés pourront être faits par chèques conjoints à votre ordre conjointement avec l'entrepreneur
général et, le cas échéant, les personnes vous ayant dénoncé leur contrat avec l'entrepreneur général, ou à votre ordre
conjointement avec les personnes avec qui vous avez contracté directement. Si les travaux n'ont pas été confiés à un entrepreneur
général, les déboursements pourront être faits par chèques conjoints à votre ordre conjointement avec toute personne avec qui
vous avez contracté directement. Au lieu de s'assurer du paiement des personnes pouvant détenir une hypothèque légale de la
construction tel qu'indiqué ci-dessus, la caisse pourra exiger que vous lui remettiez, avant tout déboursement, un "Consentement à
priorité d'hypothèque" signé en sa faveur par chacune de ces personnes. Dans tous les cas, la caisse pourra faire des retenues de
façon à ce que la valeur des travaux excède, dans chaque cas, de 15 % le total des déboursements demandés et des
déboursements déjà effectués, le dernier déboursement de 15 % pouvant être effectué 35 jours après la fin des travaux, le tout à la
discrétion de la caisse.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 4  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 4. PAIEMENT DE L'INTÉRÊT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Si le prêt est versé au moyen de déboursements successifs, la caisse pourra, en tout temps, exiger que l'intérêt accumulé soit
payé périodiquement en entier. L'intérêt accumulé au jour de chaque déboursement, poura être déduit de ce dernier par la caisse.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 5  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 5. DÉLAI DE DÉBOURSEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Si le déboursement entier du prêt ne peut être effectué dans les trois mois de la date de signature de l'acte hypothécaire, la caisse
pourra, moyennant un avis écrit à cet effet, refuser de faire tout autre déboursement, conservant alors ses recours à l'égard des
sommes déjà versées.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 6  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 6. FRAIS</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Vous devez payer les honoraires de notaire, d'arpenteur-géomètre et d'évaluateur, ainsi que les frais de publication, d'inspection,
de vérification environnementale et autres frais de même nature. La caisse pourra retenir les sommes suffisantes à même le
produit du prêt pour les acquitter. Vous devrez payer également, lorsque le prêt sera remboursé avec ou sans subrogation, les
frais et honoraires de radiation notariée.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 7  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 7. AUTORISATION DE DÉBIT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Toute somme exigible pourra être débitée directement de l'un ou l'autre de vos comptes d'épargne à la caisse.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 8  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 8. PRIX DE VENTE DE L'IMMEUBLE</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Si le prêt sert à l'acquisiton d'un immeuble, la caisse se réserve le droit de réduire le montant du prêt si le prix de vente est inférieur
à celui prévu à l'offre d'achat.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 9  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 9. MODIFICATION DE VOTRE SITUATION FINANCIÈRE</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>La caisse se réserve le droit de retirer la présente offre de financement si elle constate que votre situation financière s'est
détériorée sensiblement depuis votre demande de crédit.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="CreateTitleLinePage3">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">11cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Conditions à l'Offre de financement
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageThree"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
