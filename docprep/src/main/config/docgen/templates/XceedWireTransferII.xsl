<?xml version="1.0" encoding="UTF-8"?>
<!-- Author Zivko Radulovic -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
		<xsl:call-template name="Test01"/>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>
<xsl:template name="Test01">	
<xsl:text>\~\par</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx5000
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx10908
\intbl {\ql\f1\fs24\b CIBC Business Contact Centre }\cell 
\intbl {\qr\f1\fs24\b Request for Cable and InterBranch Payment }\cell 
\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx10908
\intbl{\qr\f1\fs24 \par Fax to CIBC BCC: \~\~\~\~\~ 1-866-463-9002 \par\par }\cell 
\pard \intbl\row
</xsl:text>
	
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrs 
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx2500

\clbrdrt\brdrs 
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx5000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx7500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\intbl {\f1\fs24\b\ql Date:}\cell 
\intbl {\f1\fs24 </xsl:text> <xsl:value-of select="//Document/AdjustedEstClosingDate"></xsl:value-of><xsl:text>}\cell 
\intbl {\f1\fs24\b Company Name: }\cell 
\intbl {\f1\fs24 Xceed Mortgage Corporation }\cell
\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx2500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx5000

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx7500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Debit Transit #:}\cell 
\intbl {\f1\fs24 00002}\cell 
\intbl {\f1\fs24 Telephone #: }\cell 
\intbl {\f1\fs24 (416) 364-7944}\cell

\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx2500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx5000

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx7500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Account #:}\cell 
\intbl {\f1\fs24 31-57210}\cell 
\intbl {\f1\fs24 Fax #: }\cell 
\intbl {\f1\fs24 (416) 364-2890}\cell

\pard \intbl\row
</xsl:text>

<!-- <xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx2500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx5000

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx7500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
{\f1\fs24 Account #:}\cell 
\cell 
{\f1\fs24 Fax #: }\cell 
{\f1\fs18 (\~\~\~\~\~\~\~ )}\cell

\pard \intbl\row
</xsl:text> -->

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrnone\cellx2500

\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrs
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrs
\clbrdrr\brdrnone\cellx7500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl \~ \cell 
\intbl \~ \cell 
{\f1\fs24 Client Trader ID #:}\cell 
{\f1\fs24 4673875}\cell

\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clcbpat1
\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24\b\cf8 PLEASE WIRE THE FOLLOWING:}\cell

\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx5454

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\intbl {\f1\fs24 Beneficiary Name:\par  </xsl:text>
<xsl:value-of select="//Solicitor/Firm"></xsl:value-of><xsl:text>  "In Trust" </xsl:text>
<xsl:text> \par}\cell 
\intbl {\f1\fs24 Beneficiary Address:\par </xsl:text>
<xsl:value-of select="concat( //Solicitor/Address1 , ' \par ' )"></xsl:value-of>
<xsl:if test="//Solicitor/Address2">
<xsl:value-of select="concat( //Solicitor/Address2 , ' \par ' )"></xsl:value-of>
</xsl:if>
<xsl:value-of select="concat( //Solicitor/AddressCity, ' ', //Solicitor/AddressProvince,' \par ' )"></xsl:value-of>
<xsl:value-of select="concat( //Solicitor/AddressPostal, ' ' )"></xsl:value-of>
<xsl:text> }\cell 
\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\intbl {\f1\fs24  Beneficiary Account Number:}\cell 
\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx3500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx6000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx8000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx9500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\intbl {\f1\fs24 Amount:\par </xsl:text><xsl:value-of select="//Document/NetWire"></xsl:value-of> <xsl:text> \par}\cell 
\intbl {\f2\fs24 x}{\f1\fs24\ CDN\~\~\~}{\f2\fs24 q}{\f1\fs24 US}\cell 
\intbl {\f1\fs24 Other Currency:}\cell 
\intbl {\f1\fs24 Rate: }\cell
\intbl {\f1\fs24 Contract #:}\cell
\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clcbpat1
\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24\b\cf8 BENEFICIARY'S BANK INFORMATION}\cell

\pard \intbl\row
</xsl:text>

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Bank Name:}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Bank Address:}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Bank Number (ABA, Routing#, Swift Code):}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Payment Details}{\f1\fs18\i (if any): }\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Payment Instructions:}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx5454

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Intermediary Bank:}\cell
\intbl {\f1\fs24 SWIFT Code (if known):}\cell

\pard \intbl\row
</xsl:text>
<!-- ============================================================================================= -->
<!-- ============================================================================================= -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 \~\~}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24\b Client Authorization:}{\f1\fs18 (signed in accordance with existing company documented delegated authorities)}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx3000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx6500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Name:}\cell
\intbl {\f1\fs24 \~}\cell
\intbl {\f1\fs18 Signature}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx3000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx6500

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Title:}\cell
\intbl {\f1\fs24 Manager Funding &amp; Fulfillment}\cell
\intbl {\f1\fs18 \~}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx3000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx6500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Name:}\cell
\intbl {\f1\fs24 \~}\cell
\intbl {\f1\fs18 Signature}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx3000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx6500

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Title:}\cell
\intbl {\f1\fs24 Financial Analyst}\cell
\intbl {\f1\fs18 \~}\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 \~\~}\cell

\pard \intbl\row
</xsl:text>
<!-- ============================================================================================= -->
<!-- ============================================================================================= -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24\b For Internal BCC Use Only: }\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx3200

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx7000

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx8500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Client ID#:}\cell
\intbl {\f1\fs24 \~ }\cell
\intbl {\f1\fs24 SR #; }\cell
\intbl {\f1\fs24\b }\cell

\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f2\fs24 q}{\f1\fs20 \~\~ Transfer required prior to cable.}\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx6000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f2\fs24 q}{\f1\fs20 \~\~ Authorized to create or increase overdraft}\cell
\intbl {\f1\fs24 \~\~ Signature: (BCC officer)}\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Debit Trans/Acct for S/C: }{\f1\fs20 (if different from above)}\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx3000

\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Service Charges:}\cell
\intbl {\f2\fs24 q}{\f1\fs24 \~ STANDARD\~\~\~}{\f2\fs24 q}{\f1\fs24 \~ WAIVED\~\~\~}{\f2\fs24 q}{\f1\fs24 \~ SPECIAL}\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx3000

\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Cable Charges:}\cell
\intbl {\f2\fs24 q}{\f1\fs24 \~ STANDARD\~\~\~}{\f2\fs24 q}{\f1\fs24 \~ SPECIAL\~\~\~}{\f1\fs20\i \~ (insert trans/acct to debit)}\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx5454

\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f2\fs24 x}{\f1\fs24 Fax confirmation to: Funding Department }\cell
\intbl {\f1\fs24 FAX: (416) 364-2890}\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs20 Authorized by BCC in accordance with delegation letter by: }\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx2727

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx5454

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx8181

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Signature }{\f1\fs20 (BCC officer)}\cell
\intbl {\f1\fs24 \~\par}\cell
\intbl {\f1\fs24 Signature }{\f1\fs20 (BCC officer)}\cell
\intbl {\f1\fs24 \~}\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx2727

\clbrdrt\brds
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx5454

\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrnone
\clbrdrr\brdrs\cellx8181

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Name: }\cell
\intbl {\f1\fs24 \~}\cell
\intbl {\f1\fs24 Name: }\cell
\intbl {\f1\fs24 \~}\cell
\pard \intbl\row
</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrnone\cellx2127

\clbrdrt\brds
\clbrdrl\brdrnone
\clbrdrb\brdrs
\clbrdrr\brdrnone\cellx4854

\clbrdrt\brds
\clbrdrl\brdrnone
\clbrdrb\brdrs
\clbrdrr\brdrnone\cellx6617

\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrs
\clbrdrr\brdrnone\cellx7681

\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard
\intbl {\f1\fs24 Telephone#: }\cell
\intbl {\f1\fs24 ( \~\~\~\~ )}\cell
\intbl {\f1\fs24 \~ Ext#:}\cell
\intbl {\f1\fs24 Fax: }\cell
\intbl {\f1\fs24 ( \~\~\~\~ )}\cell
\pard \intbl\row
</xsl:text>

	</xsl:template>
	<!-- ==================================================================== -->
	<!-- ==================================================================== -->
	<xsl:template name="RTFFileStart">
	 <!-- #DG670 -->
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl 
{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} 
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f2\fnil\fcharset2\fprq2{\*\panose 05000000000000000000}Wingdings;}
}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\stylesheet {\ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}}
{\info {\author Zivko Radulovic}{\operator BASIS100 automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company Basis100 Inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \margl720\margr720\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
</xsl:text>

</xsl:template>
	<!-- ==================================================================== -->
	<!-- ==================================================================== -->
	<xsl:template name="RTFFileEnd">
		<xsl:text>}</xsl:text>
	</xsl:template>
</xsl:stylesheet>
