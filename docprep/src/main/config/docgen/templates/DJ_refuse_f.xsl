<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xmlns="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	
	<xsl:template name="CreatePageOne">
		<xsl:element name="page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-before</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateNameAddressLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
				<xsl:call-template name="CreateCommentLines"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">25mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">f:\temporary\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">0.7in</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<!-- empty column -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
						</xsl:element>
					</xsl:element>
					<!-- end empty column -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Avis de Refus Hypothécaire
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-after">2em</xsl:attribute>
							<xsl:attribute name="space-before">2em</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Le  <xsl:value-of select="//General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">0.5cm</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1.3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Objet:
							</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Demande de financement hypothécaire 
							</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Numéro de dossier: <xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- Catherine starts here -->
	<xsl:template name="CreateNameAddressLines">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<!-- Borrowers -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">0.5em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
							 <xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
								<xsl:value-of select="./salutation"/><xsl:text> </xsl:text><xsl:value-of select="./borrowerFirstName"/><xsl:text> </xsl:text>
								<xsl:if test="./borrowerMiddleInitial">
									<xsl:value-of select="./borrowerMiddleInitial"/><xsl:text> </xsl:text>										
								</xsl:if>
								<xsl:value-of select="./borrowerLastName"/><xsl:element name="fo:block"><xsl:text> </xsl:text></xsl:element>
							</xsl:for-each>
						</xsl:element>
						<!-- prim Borrower Address -->												
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">0.5em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
							 <xsl:attribute name="keep-together">always</xsl:attribute>
							 	<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
							 		<xsl:if test="./borrowerTypeId='0'">
								 		<xsl:for-each select="./BorrowerAddress" >
								 			<xsl:if test="./borrowerAddressTypeId='0'"> 
	 										<xsl:value-of select="./Address/addressLine1"/><xsl:text> </xsl:text>
												<xsl:if test="./Address/addressLine2">
													<xsl:element name="fo:block"><xsl:value-of select="./Address/addressLine2"/><xsl:text> </xsl:text></xsl:element>
												</xsl:if>													
												<xsl:element name="fo:block"><xsl:value-of select="./Address/city"/>, <xsl:value-of select="./Address/province"/><xsl:text> </xsl:text></xsl:element>
												<xsl:element name="fo:block"><xsl:value-of select="./Address/postalFSA"/><xsl:text> </xsl:text><xsl:value-of select="./Address/postalLDU"/></xsl:element>
											</xsl:if>	 
										</xsl:for-each>  	
									</xsl:if>		 
								</xsl:for-each> 
						</xsl:element>	
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateCommentLines">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>			
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="space-before.optimum">20pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			Commentaire(s)
		</xsl:element>	
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>			
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">15pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
				<xsl:value-of select="//specialRequirementTags/dealNotes/dealNotesText"/>
		</xsl:element>			
	</xsl:template>

	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>			
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">7pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>								 
		Monsieur (Madame),
	    </xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">7pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>Nous avons le regret de vous informer qu'après étude de votre dossier, nous ne pouvons malheureusement autoriser votre demande de financement hypothécaire.
	    </xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">7pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			Pour toute information relativement à votre dossier, nous vous prions de communiquer avec votre représentant hypothécaire, 
			<xsl:if test="//Deal/SourceOfBusinessProfile/Contact/salutation"><xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/salutation"/><xsl:text> </xsl:text></xsl:if>
			<xsl:value-of select="concat(//Deal/SourceOfBusinessProfile/Contact/contactFirstName , '   ', //Deal/SourceOfBusinessProfile/Contact/contactLastName ) "/>
			au numéro de téléphone suivant : <xsl:element name="fo:block"><xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>.</xsl:element>
         </xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">7pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>								 
		Nous vous prions de recevoir, Monsieur (Madame), l'expression de nos sentiments distingués.
         </xsl:element>
     </xsl:template>  
     
     <!-- Underwriter -->
     <xsl:template name="CreateUnderwriterLines">         
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">4mm</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">12pt</xsl:attribute>
								<xsl:attribute name="font-style">italic</xsl:attribute>
								<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
								<xsl:attribute name="font-size">14pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">40pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">12pt</xsl:attribute>
								<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
								<xsl:attribute name="font-size">10pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select="//Deal/underwriter/userType"/>
							</xsl:element>		
							<xsl:element name="fo:table">
								<xsl:attribute name="text-align">left</xsl:attribute>
								<xsl:attribute name="table-layout">fixed</xsl:attribute>
								<xsl:element name="fo:table-column">
									<xsl:attribute name="column-width">3cm</xsl:attribute>
								</xsl:element>
								<xsl:element name="fo:table-column">
									<xsl:attribute name="column-width">3cm</xsl:attribute>
								</xsl:element>
								<xsl:element name="fo:table-column">
									<xsl:attribute name="column-width">3cm</xsl:attribute>
								</xsl:element>
								<xsl:element name="fo:table-body">
									<xsl:element name="fo:table-row">
										<xsl:element name="fo:table-cell">
											<xsl:attribute name="padding-top">0mm</xsl:attribute>
											<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
											<xsl:attribute name="padding-right">0mm</xsl:attribute>
											<xsl:attribute name="padding-left">0mm</xsl:attribute>
												<xsl:element name="fo:block">
													<xsl:attribute name="line-height">12pt</xsl:attribute>
													<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
													<xsl:attribute name="font-size">10pt</xsl:attribute>
													<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
													<xsl:attribute name="keep-together">always</xsl:attribute>
													téléphone :
												</xsl:element>	
										</xsl:element>
										<xsl:element name="fo:table-cell">
											<xsl:attribute name="padding-top">0mm</xsl:attribute>
											<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
											<xsl:attribute name="padding-right">0mm</xsl:attribute>
											<xsl:attribute name="padding-left">0mm</xsl:attribute>
												<xsl:element name="fo:block">
													<xsl:attribute name="line-height">12pt</xsl:attribute>
													<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
													<xsl:attribute name="font-size">10pt</xsl:attribute>
													<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
													<xsl:attribute name="keep-together">always</xsl:attribute>
														<xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumber"/>
												</xsl:element>	
										</xsl:element>
										<xsl:element name="fo:table-cell">
											<xsl:attribute name="padding-top">0mm</xsl:attribute>
											<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
											<xsl:attribute name="padding-right">0mm</xsl:attribute>
											<xsl:attribute name="padding-left">0mm</xsl:attribute>
												<xsl:element name="fo:block">
													<xsl:attribute name="line-height">12pt</xsl:attribute>
													<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
													<xsl:attribute name="font-size">10pt</xsl:attribute>
													<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
													<xsl:attribute name="keep-together">always</xsl:attribute>
														poste : <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:element>	
										</xsl:element>
									</xsl:element>	
									<xsl:element name="fo:table-row">
										<xsl:element name="fo:table-cell">
											<xsl:element name="fo:block">
												<xsl:attribute name="line-height">12pt</xsl:attribute>
												<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
												<xsl:attribute name="font-size">10pt</xsl:attribute>
												<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
												<xsl:attribute name="keep-together">always</xsl:attribute>
												télécopieur :
											</xsl:element>	
										</xsl:element>
										<xsl:element name="fo:table-cell">
											<xsl:element name="fo:block">
												<xsl:attribute name="line-height">12pt</xsl:attribute>
												<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
												<xsl:attribute name="font-size">10pt</xsl:attribute>
												<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
												<xsl:attribute name="keep-together">always</xsl:attribute>
													<xsl:value-of select="//Deal/underwriter/Contact/contactFaxNumber"/>
											</xsl:element>	
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
	</xsl:template>
<!-- end Catherine -->
	
	<xsl:template name="FOStart">
		<xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:root">
			<xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute>
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">1in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">1in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">40mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">25mm</xsl:attribute>
						<xsl:attribute name="margin-left">30mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
