<?xml version="1.0" encoding="ISO-8859-1"?>	
<!-- 
04.Dec.2008 DVG #DG788 FXP23607: vMCM_Express_Deal Summary Generic_Allocate Tax Escrow label is display as English in a French Document.
  FXP23639: vMCM_Express_CitizensBank_Documents - Deal Summary Generic French - In the "D�tails d'admissibilit� abtenus du point de vente" Rate is not translated to "Taux" 
Venkata 27-August,2007 - FXP18053: NBC CR022 - Deal summary changes
	@version 1.1 MCM Impl Team XS_16.11 :Added Component Details Sections
	@version 1.2 MCM Impl Team XS_16.22 : Added French Version of the following sections:
			- EquityTakeoutSection
			- QualifyingDetailsSection
			- ComponentSummarySection
			- ComponentDetailsSection
	@version 1.3 MCM Impl Team FXP22747 : Correct French version for Qualifying Details
	@version 1.4 MCM Impl Team FXP22959 : Added if statement for French Component Details Section
 -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
  <xsl:output method="text"/>
  
	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
    <!-- #DG788 -->		
		<xsl:choose>
			<xsl:when test="/*/LanguageFrench">
				<xsl:call-template name="FrenchTitle"/>
				<xsl:call-template name="FrenchSummarySnapshot"/>
				<xsl:call-template name="FrenchApplicantSection"/>
				<xsl:call-template name="FrenchPropertiesSection"/>
				<xsl:call-template name="FrenchTermsSection"/>
				<xsl:call-template name="FrenchOtherDetailsSection"/>
				<!--MCM Team Impl XS_16.22 Changes Starts  -->
				<xsl:call-template name="FrenchEquityTakeoutSection"/>
				<xsl:call-template name="FrenchQualifyingDetailsSection"/>
				<xsl:call-template name="FrenchComponentSummarySection"/>
				<xsl:call-template name="FrenchComponentDetailsSection"/>
				<!--MCM Team Impl XS_16.22 Changes Ends  -->
				<xsl:call-template name="FrenchProgressAdvanceSection"/>
				<xsl:call-template name="FrenchMortgageInsuranceSection"/>
				<xsl:call-template name="FrenchDownPaymentsSection"/>
				<xsl:call-template name="FrenchEscrowPayment"/>
				<xsl:call-template name="FrenchFeesSection"/>
				<xsl:call-template name="FrenchParties"/>
				<xsl:call-template name="FrenchConditionsSection"/>
				<xsl:call-template name="FrenchNotesSection"/>
				<xsl:call-template name="FrenchCreditBureauSection"/>
				<xsl:call-template name="FrenchBusinessRulesSection"/>						
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="EnglishTitle"/>
				<xsl:call-template name="EnglishSummarySnapshot"/>
				<xsl:call-template name="EnglishApplicantSection"/>
				<xsl:call-template name="EnglishPropertiesSection"/>
				<xsl:call-template name="EnglishTermsSection"/>
				<xsl:call-template name="EnglishOtherDetailsSection"/>
				<!--MCM Team Impl XS_16.11 Changes Starts  -->
				<xsl:call-template name="EnglishEquityTakeoutSection"/>
				<xsl:call-template name="EnglishQualifyingDetailsSection"/>
				<xsl:call-template name="EnglishComponentSummarySection"/>
				<xsl:call-template name="EnglishComponentDetailsSection"/>
				<!--MCM Team Impl XS_16.11 Changes Starts  -->
				<xsl:call-template name="EnglishProgressAdvanceSection"/>
				<xsl:call-template name="EnglishMortgageInsuranceSection"/>
				<xsl:call-template name="EnglishDownPaymentsSection"/>
				<xsl:call-template name="EnglishEscrowPayment"/>
				<xsl:call-template name="EnglishFeesSection"/>
				<xsl:call-template name="EnglishParties"/>
				<xsl:call-template name="EnglishConditionsSection"/>
				<xsl:call-template name="EnglishNotesSection"/>
				<xsl:call-template name="EnglishCreditBureauSection"/>
				<xsl:call-template name="EnglishBusinessRulesSection"/>
     </xsl:otherwise>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>
	
	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishTitle">
		<xsl:text>\trowd \trftsWidth3\trwWidth14400\trautofit1\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth7255 \cellx7255
\clvertalb\cltxlrtb\clftsWidth3\clwWidth7145 \cellx14400
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs28 Deal Summary \cell }
\pard\plain \s15\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 </xsl:text>

    <xsl:if test="//ReferenceNum">
      <xsl:text>Servicing Mortgage Number: </xsl:text><xsl:value-of select="//ReferenceNum"/>
    </xsl:if>

    <xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trftsWidth3\trwWidth14400\trautofit1\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth7255 \cellx7255
\clvertalb\cltxlrtb\clftsWidth3\clwWidth7145 \cellx14400
\row 
}
\trowd \trrh20\trbrdrt\brdrengrave\brdrw60 
\trftsWidth3\trwWidth14400\trautofit1\trpaddfl3\trpaddfr3 
\clbrdrt\brdrengrave\brdrw60 
\cltxlrtb\clftsWidth3\clwWidth14400 \cellx14400
\pard \ql \li0\ri-837\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin-837\lin0 
{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trrh20\trbrdrt\brdrengrave\brdrw60 
\trftsWidth3\trwWidth14400\trautofit1\trpaddfl3\trpaddfr3 
\clbrdrt\brdrengrave\brdrw60 
\cltxlrtb\clftsWidth3\clwWidth14400 \cellx14400
\row 
}

\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 </xsl:text>
		<xsl:value-of select="//CurrentDate"/>
		<xsl:text>} \par \par </xsl:text>
	</xsl:template>
	
	<xsl:template name="EnglishSummarySnapshot">
    <xsl:text>\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Summary Snapshot\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Deal #\cell Borrower Name\cell Deal Status\cell Deal Status Date\cell Source Firm\cell Source\cell LOB\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright  
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text>
    <xsl:value-of select="//DealNum"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="//BorrowerName"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="//DealStatus"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//DealStatusDate"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//SourceFirm"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//Source"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//LOB"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Deal Type\cell Deal Purpose\cell Tot Ln Amt\cell Purchase Price\cell Payment Term\cell Est. Closing Date\cell Special Feature\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text>
    <xsl:value-of select="//DealType"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//DealPurpose"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//TotalLoanAmount"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//PurchasePrice"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//ActualPaymentTerm"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//EstClosingDate"/>
    <xsl:text>\cell
</xsl:text>
    <xsl:value-of select="//SpecialFeature"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{ \f1\par }
</xsl:text>
	</xsl:template>
	
	<xsl:template name="EnglishApplicantSection">
	   <xsl:for-each select="//ApplicantDetails/Applicant">
	   <!-- *** Applicant Info *** -->
    <xsl:text>
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8306\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 </xsl:text>
    <xsl:if test="./PrimaryBorrower='Y'">
      <xsl:text>Primary </xsl:text>
    </xsl:if>
    <xsl:text>Applicant Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8306\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5466 \cellx11380
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Name\cell Type\cell Net Worth\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5466 \cellx11380
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5466 \cellx11380
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./Name"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./Type"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./NetWorth"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5466 \cellx11380
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Date of Birth\cell Marital Status\cell SIN\cell Citizenship Status\cell # of Dependents\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./DateOfBirth"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./MaritalStatus"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./SIN"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./CitizenshipStatus"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./NumDependants"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Existing Client?\cell Reference Client #\cell # of Times Bankrupt\cell Bankruptcy Status\cell Staff of Lender?\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./ExistingClient"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./ClientReferenceNum"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./TimesBankrupt"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./BankruptcyStatus"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./StaffOfLender"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Home Phone #\cell Work Phone #\cell Cell Phone #\cell Fax #\cell E-mail Address\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./HomePhone"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./WorkPhone"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./CellPhone"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./Fax"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./EmailAddress"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Preferred Contact Method\cell Language Preference\cell\cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./PreferredContactMethod"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./LanguagePreference"/>
    <xsl:text>\cell 
\cell 
\cell 
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\par }</xsl:text>
<!--
***********************************
 *** Applicant Addresses  *** 
***********************************
-->

    <xsl:if test="./Addresses">
    <xsl:text>\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Address Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\row 
}</xsl:text>

    <xsl:for-each select="./Addresses/Address">
    <xsl:text>\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 </xsl:text>
    <xsl:value-of select="./Status"/>
    <xsl:text> Address\cell Time at Residence\cell Residential Status\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./Line1"/>
    <xsl:text>, 
</xsl:text>
    <xsl:value-of select="./Line2"/>
    <xsl:if test="./Line2">
      <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:value-of select="./City"/>
    <xsl:if test="./City and ./Province">
      <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:value-of select="./Province"/>
    <xsl:text>, 
</xsl:text>
    <xsl:value-of select="./PostalCode"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./TimeAtResidence"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./ResidentialStatusID"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\row 
}</xsl:text>
    </xsl:for-each><!-- *** End Applicant Addresses  *** -->
    <xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
</xsl:if>
	<!-- *** Applicant Employment History  *** -->
    <xsl:for-each select="./EmploymentHistory/Employment">
    <xsl:text>\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 </xsl:text>
    <xsl:value-of select="./EmploymentStatus"/>
    <xsl:text> Employment\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8280 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Employer Name\cell Employment Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8280 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8280 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./EmployerName"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./EmploymentType"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8280 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Employer Phone #\cell 
Employer Fax #\cell Employer E-mail Address\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./EmployerWorkPhone"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./EmployerFax"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./EmployerEmailAddress"/>
    <xsl:text>
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Employer Mailing Address\cell 
Industry Sector\cell Occupation\cell Job Title\cell Time at Job\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./EmployerMailingAddressLine1"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="./EmployerMailingAddressLine2"/>
    <xsl:if test="./EmployerMailingAddressLine2">
      <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:value-of select="./EmployerCity"/>
    <xsl:if test="./EmployerCity and ./EmployerProvince">
      <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:value-of select="./EmployerProvince"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="./EmployerPostalCode"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./IndustrySector"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./Occupation"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./JobTitle"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./TimeAtJob"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2790 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2790 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Description\cell Period\cell Amount\cell % Inc in GDS\cell % Inc in TDS\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2790 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2790 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./IncomeType"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./IncomeDescription"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./IncomePeriod"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./IncomeAmount"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./PercentIncludedInGDS"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./PercentIncludedInTDS "/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
    </xsl:for-each> <!--Employment History loop-->

    <xsl:if test="./EmploymentHistory/OtherIncome">
      <xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11520\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Other Income\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11520\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Description\cell Period\cell Amount\cell % Included in GDS\cell % Included in TDS\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\row 
}</xsl:text>
    <xsl:for-each select="./EmploymentHistory/OtherIncome/Line">
      <xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./IncomeType"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./IncomeDescription"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./IncomePeriod"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./IncomeAmount"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./PercentIncludedInGDS"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./PercentIncludedInTDS"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\row 
}</xsl:text>
		</xsl:for-each><!--Other Income loop-->
		<xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 \par }</xsl:text>
		</xsl:if>
	
	<!-- *** ASSETS SECTION *** -->
	<xsl:if test="./Assets">
	<xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Asset Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4401 \cellx7641
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Description\cell Value\cell % Included in Net Worth\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4401 \cellx7641
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="./Assets/Asset"><xsl:text>
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4401 \cellx7641
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text><xsl:value-of select="./AssetType"/><xsl:text>\cell
</xsl:text><xsl:value-of select="./AssetDescription"/><xsl:text>\cell
</xsl:text><xsl:value-of select="./AssetValue"/><xsl:text>\cell
</xsl:text><xsl:value-of select="./PercentIncludedInNetWorth"/><xsl:text>\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4401 \cellx7641
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\row 
}</xsl:text>
	</xsl:for-each>
<xsl:text>\trowd \trgaph108\trleft378\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7263 \cellx7641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Total:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24\b </xsl:text><xsl:value-of select="./Assets/Total"/><xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7263 \cellx7641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
</xsl:if><!--Assets-->

	<!-- *** LIABILITIES SECTION *** -->
	<xsl:if test="./Liabilities">
<xsl:text>\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11707\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2142 \cellx2513
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Liability Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11707\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2142 \cellx2513
\row 
}
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth629 \cellx1000
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1513 \cellx2513
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2827 \cellx5340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1400 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1140 \cellx9780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1600 \cellx11680
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1030 \cellx13630
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 CB\cell Type\cell Description\cell Limit\cell Amount\cell Monthly\cell Marurity Date\cell % in GDS\cell % in TDS\cell Payoff\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth629 \cellx1000
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1513 \cellx2513
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2827 \cellx5340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1400 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1140 \cellx9780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1600 \cellx11680
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1030 \cellx13630
\row 
}</xsl:text>
<xsl:for-each select="./Liabilities/Liability"><xsl:text>
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth629 \cellx1000
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1513 \cellx2513
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2827 \cellx5340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1400 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1140 \cellx9780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1600 \cellx11680
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1030 \cellx13630
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./CB"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityDescription"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityLimit"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityMonthlyPayment"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityMarutityDate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./PercentIncludedInTDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityPayoffType"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth629 \cellx1000
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1513 \cellx2513
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2827 \cellx5340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1400 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1140 \cellx9780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1600 \cellx11680
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1030 \cellx13630
\row 
}</xsl:text>
	</xsl:for-each>
<xsl:text>\trowd \trgaph115\trleft371\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6369 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1240 \cellx9580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4840 \cellx14920
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Totals:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24\b
</xsl:text><xsl:value-of select="./Liabilities/TotalAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./Liabilities/TotalMonthly"/><xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft371\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6369 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1240 \cellx9580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4840 \cellx14820
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par \par}</xsl:text>
      </xsl:if><!-- Liabilities -->
    </xsl:for-each> <!--Applicant loop-->
  </xsl:template>

  <!--  Properties -->
  <xsl:template name="EnglishPropertiesSection">
    <xsl:for-each select="//Properties/Property">
      <xsl:text>\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Property Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Primary Property?\cell Property Address\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth10980 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./PrimaryProperty"/><xsl:text>
\cell </xsl:text>
<xsl:value-of select="./AddressLine1"/><xsl:text>, </xsl:text>
<xsl:value-of select="./AddressLine2"/>
<xsl:if test="./AddressLine2">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./City"/><xsl:text>, </xsl:text>
<xsl:value-of select="./Province"/><xsl:text>, </xsl:text>
<xsl:value-of select="./PostalCode"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth10980 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Legal Description\cell Dwelling Type\cell Dwelling Style\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./LegalLine1"/>

<xsl:if test="./LegalLine2">
<xsl:text>\par </xsl:text>
<xsl:value-of select="./LegalLine2"/>
</xsl:if>

<xsl:if test="./LegalLine3">
<xsl:text>\par </xsl:text>
<xsl:value-of select="./LegalLine3"/>
</xsl:if>

<xsl:text>\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
<xsl:value-of select="./DwellingType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./DwellingStyle"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth9540 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Builder Name\cell Garage Size\cell Garage Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth9540 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9540 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
<xsl:value-of select="./BuilderName"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./GarageSize"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./GarageType"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9540 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 

\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 New Construction?\cell Structure Age\cell # of Units\cell # of Bedrooms\cell Living Space\cell Lot Size\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./NewConstruction"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./StructureAge"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./NumUnits"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./NumBedrooms"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./LivingSpace"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./LotSize"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Heat Type\cell Environmental Hazard\cell Water\cell Sewage\cell MLS\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./HeatingType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./UFFIInsulation"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Water"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Sewage"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./MLS"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
<!-- Rel 3.1 - Sasa -->
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Tenure\cell MI Energy Efficiency\cell Subdivision Discount\cell 
On Reserve Trust Agreement No.\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./tenureType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./MIEnergyEfficiency"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./subdivisionDiscount"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./onReserveTrustAgreementNumber"/><xsl:text>\cell </xsl:text>
<xsl:text> \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
<!-- End of Rel 3.1 - Sasa -->
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Property Usage\cell Occupancy\cell Property Type\cell Property Location\cell Zoning\cell }
\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./PropertyUsage"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./OccupancyType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./PropertyType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./PropertyLocation"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Zoning"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Purchase Price\cell Land Value\cell Equity Available\cell Estimated Value\cell Actual Appraised Value\cell }
\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./PurchasePrice"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LandValue"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./EquityAvailable"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./EstimatedValue"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./ActualAppraisalValue"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11520 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Appraisal Date\cell Appraisal Source\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11520 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11520 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./AppraisalDate"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./AppraisalSource"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11520 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3195 \cellx11115
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3105 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Appraiser\cell Appraiser Address\cell 
Appraiser Phone #\cell Appraiser Fax #\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3195 \cellx11115
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3105 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3195 \cellx11115
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3105 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./AppraiserName"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./AppraiserAddress"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./AppraiserPhone"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./AppraiserFax"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3195 \cellx11115
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3105 \cellx14220
\row 
}</xsl:text>
<xsl:if test="./PropertyExpenses">
<xsl:text>\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13860 \cellx14220
\pard\plain \s1\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs24 Expenses\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13860 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Period\cell Amount\cell % Inc in GDS\cell % Inc in TDS\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="./PropertyExpenses/Expense">
<xsl:text>
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./ExpenseType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./ExpensePeriod"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./ExpenseAmount"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./PercentIncludedInTDS"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\row 
}
</xsl:text>
          </xsl:for-each>
        </xsl:if>
      <xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\par}</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <!--  TERMS -->
  <xsl:template name="EnglishTermsSection">
    <xsl:text>\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA7020\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\pard\plain \ql \keep\keepn\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Terms\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA7020\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\row 
}
\trowd \irow1\irowband1\ts11\trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847\clcbpatraw16 \cellx7200
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3510\clcbpatraw16 \cellx10710
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3510\clcbpatraw16 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\b\f1\fs20\insrsid1076693 Total Purchase Price\cell Product Type\cell Charge\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{
\insrsid1076693 
\trowd \irow1\irowband1\ts11\trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847\clcbpatraw16 \cellx7200
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3510\clcbpatraw16 \cellx10710
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3510\clcbpatraw16 \cellx14220
\row 
}
\trowd \irow2\irowband2\ts11\trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847\clshdrawnil \cellx7200
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx10710
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx14220
\pard \ql \li0\ri0\sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1\fs18\insrsid1076693 
</xsl:text>
    <xsl:value-of select="//TotalPurchasePrice"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="//productType"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="//Charge"/>
    <xsl:text>\cell 
}
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{
\insrsid1076693 \trowd \irow2\irowband2\ts11\trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847\clshdrawnil \cellx7200
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx10710
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Down Payment\cell Lender\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//RequiredDownPaymentAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Lender"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 LTV\cell Product\cell Product Code\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//LTV"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Product"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//productCode"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}



\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9540
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220

\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Loan\cell Posted Interest Rate\cell Index Rate\cell Index Adjustment\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9540
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9540
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//NetLoanAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//PostedInterestRate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//IndexRate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//IndexAdjustment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9540
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220

\row 
}

\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 MI Premium\cell Discount\cell </xsl:text><xsl:value-of select="//bestRateLabel"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//MI/Premium"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Discount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//bestRate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}

\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Total Loan\cell Premium\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//TotalLoanAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Premium"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Amortization Period\cell Buydown\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//Amortization"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BuydownRate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Effective Amortization Period\cell Net Rate\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//EffectiveAmortization"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//NetRate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Payment Term Description\cell P and I\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//PaymentTerm"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//PandIPayment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Actual Payment Term\cell Additional P and I\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard 
\ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//ActualPaymentTerm"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//AdditionalPrincipalPayment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Payment Frequency\cell Total Escrow\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//PaymentFrequency"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//TotalEscrowPayment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Prepayment Penalty\cell Total Payment\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>

<xsl:for-each select="//Prepayment/Line">
<xsl:value-of select="."/>
<xsl:text>\par </xsl:text>
</xsl:for-each>

<xsl:text>\cell 
</xsl:text><xsl:value-of select="//TotalPayment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Privilege Payment Option\cell Holdback Amount\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//PrivilegePaymentOption"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//AdvanceHold"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}


\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Cashback Amount\cell Interest Rate Change Frequency\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//CashbackAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//interestRateChangeFreq"/><xsl:text>
</xsl:text> months <xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}

\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
  </xsl:template>

  <!--  Other Details -->
  <xsl:template name="EnglishOtherDetailsSection">
    <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8316\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\pard\plain \ql \keep\keepn\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Other Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8316\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1832 \cellx1832
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2072 \cellx3904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2972 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2072 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1972 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx13220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Credit Score\cell Combined GDS\cell GDS (Qualifying Rate)\cell Combined TDS\cell TDS (Qualifying Rate)\cell Qualify Rate\cell Qualifying Product\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1832 \cellx1832
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2072 \cellx3904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2972 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2072 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1972 \cellx13220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1832 \cellx1832
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2072 \cellx3904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2972 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2072 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1972 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx13220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//CreditScore"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//CombinedGDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Combined3YrGDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//CombinedTDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Combined3YrTDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//QualifyingRate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//QualifyProduct"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1832 \cellx1832
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2072 \cellx3904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2972 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2072 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1972 \cellx13220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Deal Purpose\cell Branch\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//DealPurpose"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BranchName"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Deal Type\cell Cross Sell\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//DealType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//CrossSell"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Special Feature\cell Reference Existing Mortgage Number\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SpecialFeature"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//RefExistingMtgNum"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Pre-App Est. Purchase Price\cell Reference Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//PreAppEstPurchasePrice"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//ReferenceType"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Repayment Type\cell Reference Deal #\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//RepaymentType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//ReferenceNum"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Second Mortgage Exists?\cell Estimated Closing Date\cell Interest Adjustment Date\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544\cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5544\cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SecondMortgage"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//InterestAdjustmentRate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Affiliation Program\cell First Payment Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard 
\ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//AffiliationProgram"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//FirstPaymentDate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Tax Payor\cell Maturity Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//TaxPayor"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//MaturityDate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Rate Locked In?\cell Commitment Expected Return Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//RateLockedIn"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//CommitmentExpiryDate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{ <!-- Dave was here - start -->
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Financing Waiver Date\cell Reference Source App #\cell Underwriter\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//WaiverDate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Underwriter"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Source Information 1 Name\cell Source Information 1 Firm\cell Source Information 1 License Registration Number\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SourceName"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//SourceFirm"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//LicenseNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Source Information 1 Address\cell Source Information 1 Phone\cell Source Information 1 Fax\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SourceAddress"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\row 
}<!-- Dave was here - end -->
<!-- Dave was here - start second guy -->
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Source Information 2 Name\cell Source Information 2 Firm\cell Source Information 2 License Registration Number \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//Source2Name"/><xsl:text>\cell 
</xsl:text>
<xsl:if test="//Source2Name"><xsl:value-of select="//SourceFirm"/></xsl:if>
<xsl:text> \cell </xsl:text>
<xsl:if test="//Source2Name"><xsl:value-of select="//License2Number"/></xsl:if>
<xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Source Information 2 Address\cell Source Information 2 Phone\cell Source Information 2 Fax\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//Source2Address"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Source2Phone"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Source2Fax"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\row 
}<!-- Dave was here - end second guy -->
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Source Business ID\cell Source Alternative ID\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SOBBusinessID"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//alternativeID"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Bank Name\cell Bank and Transit Number \cell Bank Account Number\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544\cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5544\cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//BankName"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BankTransitNumber"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BankAccountNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
  </xsl:template>

  <!--  Equity Takeout -->
  <xsl:template name="EnglishEquityTakeoutSection">
    <xsl:if test="//Refi">
      <xsl:text>\trowd\trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trwWidthA11025\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2815\cellx2700
\pard \ql \nowidctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1 Equity Take-Out\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trwWidthA11025\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2815\cellx2700
\row
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2815\cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2721\cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\pard \ql \widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Date Purchased\cell Mortgage Holder\cell Original Purchase Price\cell Value of Improvements\cell Blended Amortization\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2815\cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2721 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768\cellx13725
\row 
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2815 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2721 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\pard \ql \sa120\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//Refi/OriginalPurchaseDate"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Refi/MortgageHolder"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Refi/PurchasePrice"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Refi/ImprovementValue"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Refi/BlendedAmortization"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright  
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2815 \cellx2700\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2721 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\row
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3000\cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\pard \ql \widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Purpose\cell Existing Account Reference\cell Original Mtg. Amount\cell Outstanding Mtg. Amount\cell Product Type\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3000\cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768\cellx13725
\row 
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\pard \ql \sa120\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright {\f1\fs18 </xsl:text>
			<xsl:value-of select="//Refi/Purpose"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="//Refi/ExistingMtgNumber"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="//Refi/OriginalMtgAmount"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="//Refi/ExistingLoanAmount"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="//Refi/ProductType"/>	
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright  
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\row
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\pard \ql\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Improvements\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\row 
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\pard \ql \sa120\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//Refi/ImproveDesc"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\row 
}
<!-- ***********************MCM Impl Team XS_16.11 Starts Added new field for Equity Take out Section-->
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\pard \ql\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Additional Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\row 
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\pard \ql \sa120\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
			<xsl:value-of select="//Refi/AdditionalInformation"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\row 
}
<!-- ***********************MCM Impl Team XS_16.11 Ends***************** -->
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\par }
      </xsl:text>
    </xsl:if>
  </xsl:template>

  <!-- ***********************MCM Impl Team XS_16.11 Starts******************** -->	
  <!--- Qualifying Details-->
  <xsl:template name="EnglishQualifyingDetailsSection">
    <xsl:for-each select="//QualifyingDetails">
      <xsl:choose>
        <xsl:when test="./SectionHeader='Not Available'">
          <xsl:text>\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Qualifying Details From Source of Business - Not Available\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6880 \cellx3240
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\par }
</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8316\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3880 \cellx5307
\pard\plain \ql \keep\keepn\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Qualifying Details From Source of Business\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8316\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3880 \cellx5307
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2634 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Rate\cell Compounding Period\cell Amortization\cell Repayment Type\cell Payment\cell GDS\cell TDS\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright  
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2634 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx3240
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2634 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx3240
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text>
		<xsl:value-of select="//QualifyRate"/>
		<xsl:text>\cell 
</xsl:text>
		<xsl:value-of select="//CompoundingPeriod"/>
		<xsl:text>\cell 
</xsl:text>
		<xsl:value-of select="//AmortizationTerm"/>
		<xsl:text>\cell
</xsl:text>
		<xsl:value-of select="//RepaymentType"/>
		<xsl:text>\cell
</xsl:text>
		<xsl:value-of select="//Payment"/>
		<xsl:text>\cell
</xsl:text>
		<xsl:value-of select="//GDS"/>
		<xsl:text>\cell
</xsl:text>
		<xsl:value-of select="//TDS"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2634 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx3240
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>	
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <!-- Qualifying Details : French Section Starts-->
  <xsl:template name="FrenchQualifyingDetailsSection">
    <xsl:for-each select="//QualifyingDetails">
      <xsl:choose>
        <xsl:when test="./SectionHeader='Not Available'">
          <xsl:text>\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 D�tails d'admissibilit� obtenus de la source d'affaire - Non disponible\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6880 \cellx3240
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\par }
</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8316\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3880 \cellx5307
\pard\plain \ql \keep\keepn\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 D�tails d'admissibilit� obtenus de la source d'affaire\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8316\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3880 \cellx5307
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2634 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Taux\cell <!-- #DG788 -->
P�riode de calcul de l'int�r�t\cell Amortissement\cell Type de remboursement\cell Paiement\cell ABD \cell ATD\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright  
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2634 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx3240
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2634 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx3240
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text>
		<xsl:value-of select="//QualifyRate"/>
		<xsl:text>\cell 
</xsl:text>
		<xsl:value-of select="//CompoundingPeriod"/>
		<xsl:text>\cell 
</xsl:text>
		<xsl:value-of select="//AmortizationTerm"/>
		<xsl:text>\cell
</xsl:text>
		<xsl:value-of select="//RepaymentType"/>
		<xsl:text>\cell
</xsl:text>
		<xsl:value-of select="//Payment"/>
		<xsl:text>\cell
</xsl:text>
		<xsl:value-of select="//GDS"/>
		<xsl:text>\cell
</xsl:text>
		<xsl:value-of select="//TDS"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2634 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx3240
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>	
      </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>	
  <!-- Qualifying Details : French Section Ends -->			
		
  <!-- Component Summary Section Starts -->
  <xsl:template name="EnglishComponentSummarySection">
    <xsl:if test="//Components">
      <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Component Summary\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx4500
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Total Amount\cell Unallocated Amount\cell Total Cashback Amount\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}</xsl:text>
			<xsl:for-each select="//Components/ComponentSummary">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./TotalAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./UnAllocatedAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalCashBackAmount"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>			
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
  
  <!-- ************************************ Componet Details Starts ********************************************************* -->
  <xsl:template name="EnglishComponentDetailsSection">
    <xsl:if test="//Components">
      <xsl:text>\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13860 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Component Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13860 \cellx3240
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\par }
</xsl:text>
    <!--Mortgage Component Section Starts  -->
    <xsl:for-each select="//Components/Component[ComponentType='Mortgage']">
      <xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Component Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx4500
\row 
}
</xsl:text>
      <xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Mortgage Amount\cell MI Premium\cell Allocate MI Premium\cell Total Mortgage Amount\cell Amortization Period\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./MortgageAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./MIPremium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AllocateMIPremium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalMortgageAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AmortizationPeriod"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Effective Amortization\cell Payment Term Description\cell Actual Payment Term\cell Payment Frequency\cell Prepayment Option\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./EffectiveAmortization"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentTerm"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ActualPaymentTerm"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentFrequency"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PrepaymentOption"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Privilege Payment Option\cell Commission Code\cell Cashback Percentage\cell Cashback Amount\cell Repayment Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./PrivilegePaymentOption"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./CommissionCode"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./CashBackPercent"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./CashBackAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./RepaymentType"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Rate Locked In?\cell Product\cell Posted Rate\cell Discount\cell Premium\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./RateLockedIn"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Product"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PostedRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Discount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Premium"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Buydown Rate\cell Net Rate\cell P and I Payment\cell Additional Principal Payment\cell Allocate Tax Escrow\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./BuyDownRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./NetRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PAndIPayment"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AdditionalPrincipalPayment"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AllocateTaxEscrow"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Tax Escrow\cell Total Payment\cell Holdback Amount\cell Rate Guarantee Period\cell Existing Account\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./TaxEscrow"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalPayment"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./HoldBackAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./RateGuaranteePeriod"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ExistingAccount"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2580 \cellx12220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Existing Account Reference\cell First Payment Date\cell Maturity Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ExistingAccountReference"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./FirstPaymentDate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./MaturityDate"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\row 
}
</xsl:text>
<xsl:text>\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Additional Details\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>	
</xsl:for-each>
<!--Mortgage Component Section Ends  -->
<!--Loc Component Section Starts  -->
<xsl:for-each select="//Components/Component[ComponentType='Line of Credit']">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Component Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 LOC Amount\cell MI Premium\cell Allocate MI Premium\cell Total LOC Amount\cell Payment Frequency\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./LOCAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./MIPremium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AllocateMIPremium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalLOCAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentFrequency"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Commission Code\cell Repayment Type\cell Existing Account\cell Existing Account Reference\cell First Payment Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./CommissionCode"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./RepaymentType"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ExistingAccount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ExistingAccountReference"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./FirstPaymentDate"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Product\cell Posted Rate\cell Discount\cell Premium\cell Net Rate\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./Product"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PostedRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Discount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Premium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./NetRate"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Interest Only Payment\cell Allocate Tax Escrow\cell Tax Escrow\cell Total Payment\cell Holdback Amount\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./PandIPaymentAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AllocateTaxEscrow"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TaxEscrow"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalPayment"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./HoldBackAmount"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Additional Details\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>	
</xsl:for-each>
<!--Loc Componenst Section Ends  -->

<!-- Loan Component Section Starts -->
<xsl:for-each select="//Components/Component[ComponentType='Loan']">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Component Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Loan Amount\cell Payment Term Description\cell Actual Payment Term\cell Payment Frequency\cell Product\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./LoanAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentTerm"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ActualPaymentTerm"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentFrequency"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Product"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Posted Rate\cell Discount\cell Premium\cell Net Rate\cell Total Payment\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./PostedRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Discount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Premium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./NetRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalPayment"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Additional Details\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>	
</xsl:for-each>
<!-- Loan Component Section Ends --> 
<!-- CreditCard Component Section Starts -->
<xsl:for-each select="//Components/Component[ComponentType='Credit Card']">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Component Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Credit Card Amount\cell Product\cell Interest Rate\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./CreditCardAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Product"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./InterestRate"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1000 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Additional Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\row 
}
\pard\plain \s5\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>
</xsl:for-each>
<!-- Creditcard Component Section Ends -->
<!-- Overdraft Conmponent Section starts -->
<xsl:for-each select="//Components/Component[ComponentType='Overdraft']">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Component Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Overdraft Amount\cell Product\cell Interest Rate\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./OverDraftAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Product"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./InterestRate"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Additional Details\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\row 
}
</xsl:text>
        <xsl:text>\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0{\f1 \par }</xsl:text>
      </xsl:for-each>
    <!-- Overdraft Component Section Ends -->
    </xsl:if>
  </xsl:template>
  <!-- ******Componet Details Ends ***** -->
  <!-- ***********************MCM Impl Team XS_16.11 Ends******************************************************************* -->
	
	
  <!-- ***********************MCM Impl Team XS_16.22 Starts******************************************************************* -->
  <!-- Component Summary : French Section Starts -->
  <xsl:template name="FrenchComponentSummarySection">
    <xsl:if test="//Components">
      <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Sommaire de la composante\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx4500
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Montant total\cell Montant non attribu�\cell Montant total de la remise en argent\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}</xsl:text>
			<xsl:for-each select="//Components/ComponentSummary">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./TotalAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./UnAllocatedAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalCashBackAmount"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>			
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
  <!-- Component Summary : French Section Ends -->	
  
  <!-- Component Details : French Section Starts -->		
  <!-- ************************************ Componet Details Starts ********************************************************* -->
  <xsl:template name="FrenchComponentDetailsSection">
    <xsl:if test="//Components">
      <xsl:text>\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13860 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 D�tails de la composante\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13860 \cellx3240
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\par }
</xsl:text>
<!--Mortgage Component Section Starts  -->
    <xsl:for-each select="//Components/Component[ComponentType='Hypoth�que']">
    <xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Type de composante\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Montant de l'hypoth�que\cell Prime d'assurance\cell Attribuer la prime d'assurance\cell Montant total de l'hypoth�que\cell P�riode d'amortissement\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./MortgageAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./MIPremium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AllocateMIPremium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalMortgageAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AmortizationPeriod"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Amortissement effectif\cell Description du terme du paiement\cell Terme r�el du remboursement\cell Fr�quence des paiements\cell Option de remboursement anticip�\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./EffectiveAmortization"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentTerm"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ActualPaymentTerm"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentFrequency"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PrepaymentOption"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Option de paiement sp�cial\cell Code de commissionnement\cell Remise en argent(%)\cell Remise en argent($)\cell Type de remboursement\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./PrivilegePaymentOption"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./CommissionCode"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./CashBackPercent"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./CashBackAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./RepaymentType"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Taux immobilis�?\cell Produit\cell Taux affich�\cell Escompte\cell Prime\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
    <xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./RateLockedIn"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Product"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PostedRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Discount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Premium"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
    <xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980

\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Taux d'int�r�t r�duit\cell Taux net\cell Paiement de C&amp;I\cell 
Paiement suppl�mentaire sur le capital\cell Attribuer l'imp�t foncier\cell }<!-- #DG788 -->
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./BuyDownRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./NetRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PAndIPayment"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AdditionalPrincipalPayment"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AllocateTaxEscrow"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Paiements en main tierce pour l'imp�t foncier\cell Paiement total\cell Montant de la retenue\cell P�riode de garantie du taux\cell Compte ant�rieur\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./TaxEscrow"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalPayment"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./HoldBackAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./RateGuaranteePeriod"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ExistingAccount"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2580 \cellx12220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 R�f�rence au compte existant\cell Date du premier paiement\cell Date d'�ch�ance\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ExistingAccountReference"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./FirstPaymentDate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./MaturityDate"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\row 
}
</xsl:text>
<xsl:text>\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 D�tails suppl�mentaires\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>	
</xsl:for-each>
<!--Mortgage Component Section Ends  -->
<!--Loc Component Section Starts  -->
<xsl:for-each select="//Components/Component[ComponentType='Marge de cr�dit']">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Type de composante\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Montant de la marge de cr�dit\cell Prime d'assurance\cell Attribuer la prime d'assurance\cell Montant total de la marge de cr�dit\cell Fr�quence des paiements\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./LOCAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./MIPremium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AllocateMIPremium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalLOCAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentFrequency"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Code de commissionnement\cell Type de remboursement\cell Compte ant�rieur\cell R�f�rence du compte existant\cell Date du premier paiement\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./CommissionCode"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./RepaymentType"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ExistingAccount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ExistingAccountReference"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./FirstPaymentDate"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Produit\cell Taux affich�\cell Escompte\cell Prime\cell Taux net\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./Product"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PostedRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Discount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Premium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./NetRate"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Paiement d'int�r�ts seulement\cell Attribuer l'imp�t foncier\cell <!-- #DG788 -->
Paiements en main tierce pour l'imp�t foncier\cell Paiement total\cell 
Montant de la retenue\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./PandIPaymentAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./AllocateTaxEscrow"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TaxEscrow"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalPayment"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./HoldBackAmount"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 D�tails suppl�mentaires\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>	
</xsl:for-each>
<!--Loc Componenst Section Ends  -->

<!-- Loan Component Section Starts -->
<xsl:for-each select="//Components/Component[ComponentType='Pr�t']">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Type de composante\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Montant du pr�t\cell Description du terme du paiement\cell Terme r�el du remboursement\cell Fr�quence des paiements\cell Produit\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./LoanAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentTerm"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./ActualPaymentTerm"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./PaymentFrequency"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Product"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Taux affich�\cell Escompte\cell Prime\cell Taux net\cell Paiement total\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./PostedRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Discount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Premium"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./NetRate"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./TotalPayment"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3080 \cellx13100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2180 \cellx14980
\row 
}
</xsl:text>
<xsl:text>\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 D�tails suppl�mentaires\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13855 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>	
</xsl:for-each>
<!-- Loan Component Section Ends --> 
<!-- CreditCard Component Section Starts -->
<xsl:for-each select="//Components/Component[ComponentType='Carte de cr�dit']">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Type de composante\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Montant de la carte de cr�dit\cell Produit\cell Taux d'int�r�t\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./CreditCardAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Product"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./InterestRate"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2000 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1000 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 D�tails suppl�mentaires\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\row 
}
\pard\plain \s5\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>
</xsl:for-each>
<!-- Creditcard Component Section Ends -->
<!-- Overdraft Conmponent Section starts -->
<xsl:for-each select="//Components/Component[ComponentType='D�couvert']">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Type de composante\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./ComponentType"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Montant du d�couvert\cell Produit\cell Taux d'int�r�t\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./OverDraftAmount"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Product"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./InterestRate"/>		
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3040 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2480 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 D�tails suppl�mentaires\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\row 
}
</xsl:text>
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./AdditionalInformation"/>		
				<xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6000 \cellx6824
\row 
}
</xsl:text>
        <xsl:text>\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0{\f1 \par }</xsl:text>
      </xsl:for-each>
      <!-- Overdraft Component Section Ends -->
    </xsl:if>
  </xsl:template>
  <!-- ******Componet Details Ends ***** -->
	
  <!-- Component Details : French Section Ends -->				
  <!-- ***********************MCM Impl Team XS_16.22 Ends******************************************************************* -->	
		
  <!--  Progress Advance -->
  <xsl:template name="EnglishProgressAdvanceSection">
    <xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10440\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Progress Advance\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10440\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3519 \cellx7299
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3460 \cellx10759
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3461 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Progress Advance\cell Advance to Date Amount\cell Next Advance Amount\cell Advance Number\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3519 \cellx7299
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3460 \cellx10759
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3461 \cellx14220
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3519 \cellx7299
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3460 \cellx10759
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3461 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//ProgressAdvance"/><xsl:text>\cell </xsl:text><xsl:value-of select="//AdvanceToDateAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//NextAdvanceAmount"/><xsl:text>\cell </xsl:text><xsl:value-of select="//AdvanceNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3519 \cellx7299
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3460 \cellx10759
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3461 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }</xsl:text>
  </xsl:template>

  <!--  Mortgage Insurance -->
  <xsl:template name="EnglishMortgageInsuranceSection">
    <xsl:if test="//MI">
      <xsl:text>
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8640\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\pard \ql \keep\keepn\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Mortgage Insurance Information\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8640\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16
\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\row 
}
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx8100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx10620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Indicator\cell Status\cell Existing Policy #\cell Certificate #\cell Prequalification Certificate #\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\f1\fs18 
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx8100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx10620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx8100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx10620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//MI/Indicator"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/Status"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/ExistingPolicyNum"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/CertificateNum"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/PrequalificationMINum"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx8100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx10620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx8100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx10620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Insurer\cell Payor\cell Upfront\cell Premium\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx8100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx10620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx8100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx10620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//MI/Type"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/Insurer"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/Payor"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/Upfront"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/Premium"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx8100
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx10620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\row 
}
\trowd \irow5\irowband5\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\clcbpatraw16 \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5040\clcbpatraw16 \cellx8100
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520\clcbpatraw16 \cellx10620
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3600\clcbpatraw16 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\b\f1\fs20\insrsid10225755 Progress Advance Type\cell Progress Advance Inspection By\cell 
Self Directed RRSP\cell CMHC Product Tracker Identifier\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{
\trowd \irow5\irowband5\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\clcbpatraw16 \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5040\clcbpatraw16 \cellx8100
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520\clcbpatraw16 \cellx10620
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3600\clcbpatraw16 \cellx14220
\row 
}
\trowd \irow6\irowband6\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid2312886 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700\clshdrawnil \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5040\clshdrawnil \cellx8100
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520\clshdrawnil \cellx10620
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3600\clshdrawnil \cellx14220
\pard \ql \li0\ri0\sa120\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid9269504 
{\f1\fs18\insrsid10762402\charrsid9269504 </xsl:text>
    <xsl:value-of select="//MI/progressAdvanceType"/><xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/progressAdvanceInspectionBy"/><xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/selfDirectedRRSP"/><xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/cmhcProductTrackerIdentifier"/><xsl:text>\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\fs18\insrsid10225755\charrsid9269504 
\trowd \irow6\irowband6\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid2312886 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700\clshdrawnil \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5040\clshdrawnil \cellx8100
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520\clshdrawnil \cellx10620
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3600\clshdrawnil \cellx14220
\row 
}
\trowd \irow7\irowband7\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3
\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\clcbpatraw16 \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520\clcbpatraw16 \cellx5580
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5040\clcbpatraw16 \cellx10620
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3600\clcbpatraw16 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\b\f1\fs20\insrsid420773 LOC Repayment\cell LOC Amortization\cell LOC Interest Only Maturity Date\cell \cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{
\trowd \irow7\irowband7\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\clcbpatraw16 \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520\clcbpatraw16 \cellx5580
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5040\clcbpatraw16 \cellx10620
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3600\clcbpatraw16 \cellx14220
\row 
}
\trowd \irow8\irowband8\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid420773 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\pard \ql \li0\ri0\sa120\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid9269504 
{\f1\fs18\insrsid420773\charrsid9269504 </xsl:text>
    <xsl:value-of select="//MI/locRepaymentType"/><xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/locAmortizationMonths"/><xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/locInterestOnlyMaturityDate"/><xsl:text>\cell \cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\fs18\insrsid420773\charrsid9269504 
\trowd \irow8\irowband8\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid420773 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx14220
\row 
}
\trowd \irow9\irowband9\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid420773 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\clcbpatraw16 \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11160\clcbpatraw16 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\b\f1\fs20\insrsid420773 RU Intervention\cell Request Standard Service\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid420773 
\trowd \irow9\irowband9\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid420773 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\clcbpatraw16 \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11160\clcbpatraw16 \cellx14220
\row 
}
\trowd \irow10\irowband10\lastrow \ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid420773 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700\clshdrawnil \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11160\clshdrawnil \cellx14220
\pard \ql \li0\ri0\sa120\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1\fs18\insrsid420773 </xsl:text>
<xsl:value-of select="//MI/RUIntervention"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/requestStandardService"/><xsl:text>\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid420773 
\trowd \irow10\irowband10\lastrow \ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid420773 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700\clshdrawnil \cellx3060
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11160\clshdrawnil \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>
    </xsl:if>
  </xsl:template>

  <!--  Down Payments -->
  <xsl:template name="EnglishDownPaymentsSection">
    <xsl:if test="//DownPayments">
      <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Down Payments\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx4500
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Source\cell Description\cell Amount\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="//DownPayments/DownPayment">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./Source"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Description"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Amount"/><xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
</xsl:text>
</xsl:for-each>
<xsl:text>
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Down Payment Available:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 </xsl:text><xsl:value-of select="//DownPayments/Total"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Actual Down Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 </xsl:text><xsl:value-of select="//DownPayments/RequiredDownPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="EnglishEscrowPayment">	
    <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Escrow Payments\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx4500
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Description\cell Amount\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="//EscrowDetails/Escrow">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./Type"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Description"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Amount"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
</xsl:text>
    </xsl:for-each>
    <xsl:text>
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Total Escrow Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 </xsl:text><xsl:value-of select="//EscrowDetails/TotalEscrowPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }</xsl:text>

	</xsl:template>

  <!--  Fees -->
  <xsl:template name="EnglishFeesSection">
    <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Fees\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx4500
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5210 \cellx9710
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4510 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Description\cell Status\cell Amount\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5210 \cellx9710
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4510\cellx14220
\row 
}</xsl:text>
<xsl:for-each select="//Fees/Fee">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5210 \cellx9710
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4510 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./Type"/><xsl:text>\cell </xsl:text>

<xsl:value-of select="./Status"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Amount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5210 \cellx9710
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4510 \cellx14220
\row 
}
</xsl:text>
    </xsl:for-each>
    <xsl:text>
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }</xsl:text>

  </xsl:template>

  <!--  Conditions -->
  <xsl:template name="EnglishConditionsSection">
    <xsl:if test="//Conditions">
      <xsl:text>\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl115\trpaddt115\trpaddb115\trpaddr115\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3593
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Conditions\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl115\trpaddt115\trpaddb115\trpaddr115\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3593
\row 
}</xsl:text>

<xsl:for-each select="//Conditions/Condition">
<xsl:text>\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddt115\trpaddb115\trpaddr115\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3 
</xsl:text>

<xsl:choose>
	<!--  Only 1 condition -->
	<xsl:when test="position()=1 and position()=last()">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
	</xsl:text>
	</xsl:when>

	<!--  First row -->
	<xsl:when test="position()=1">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrnone
</xsl:text>
	</xsl:when>
	
	<!-- Last row -->
	<xsl:when test="position()=last()">
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrs\brdrw10 
</xsl:text>
	</xsl:when>

	<!-- Rows in between -->
	<xsl:otherwise>
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrnone 
</xsl:text>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth540 \cellx893
</xsl:text>

<xsl:choose>
	<!--  Only 1 condition -->
	<xsl:when test="position()=1 and position()=last()">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
	</xsl:text>
	</xsl:when>

	<!--  First row -->
	<xsl:when test="position()=1">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrnone
</xsl:text>
	</xsl:when>

	<!-- Last row -->
	<xsl:when test="position()=last()">
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrs\brdrw10 
</xsl:text>
	</xsl:when>

	<!-- Rows in between -->
	<xsl:otherwise>
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrnone 
</xsl:text>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\clbrdrl\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13320 \cellx14213
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="position()"/><xsl:text>.\cell </xsl:text>

<xsl:for-each select="./Line">
<xsl:value-of select="."/>
<xsl:text>
\par </xsl:text>
</xsl:for-each>

<xsl:text>\cell } 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddt115\trpaddb115\trpaddr115\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3 
</xsl:text>

<xsl:choose>
	<!--  Only 1 condition -->
	<xsl:when test="position()=1 and position()=last()">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
	</xsl:text>
	</xsl:when>

	<!--  First row -->
	<xsl:when test="position()=1">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrnone
</xsl:text>
	</xsl:when>
	<!-- Last row -->
	<xsl:when test="position()=last()">
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrs\brdrw10 
</xsl:text>
	</xsl:when>

	<!-- Rows in between -->
	<xsl:otherwise>
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrnone 
</xsl:text>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth540 \cellx893
</xsl:text>

<xsl:choose>
	<!--  Only 1 condition -->
	<xsl:when test="position()=1 and position()=last()">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
	</xsl:text>
	</xsl:when>

	<!--  First row -->
	<xsl:when test="position()=1">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrnone
</xsl:text>
	</xsl:when>
	<!-- Last row -->
	<xsl:when test="position()=last()">
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrs\brdrw10 
</xsl:text>
	</xsl:when>

	<!-- Rows in between -->
	<xsl:otherwise>
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrnone 
</xsl:text>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\clbrdrl\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13320 \cellx14213
\row 
}</xsl:text>
	
</xsl:for-each>

<xsl:text>\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1 \par }</xsl:text>
    </xsl:if>
  </xsl:template>

  <!--  Notes -->
  <xsl:template name="EnglishNotesSection">
  <xsl:if test="//Notes">
    <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Notes\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx3660
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6660 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\pard \ql \keep\keepn\widctlpar\intbl\faauto 
{\b\f1\fs20 Date\cell User Name\cell Text\cell  Category Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6660 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3600
\row 
}
</xsl:text>
<xsl:for-each select="//Notes/Note">
<xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3600
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="./Date"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="UserFullName"/><xsl:text>\cell 
</xsl:text>
<xsl:for-each select="Text/Line">
<xsl:value-of select="."/>
<xsl:if test="not(position()=last())"><xsl:text>\par </xsl:text></xsl:if>
</xsl:for-each>
<xsl:text>\cell </xsl:text><xsl:value-of select="DealNotesCategory"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3600

\row 
}</xsl:text>
      </xsl:for-each>
      <xsl:text>
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }</xsl:text>
    </xsl:if>
  </xsl:template>

  <!--  Bureau -->
  <xsl:template name="EnglishCreditBureauSection">
    <xsl:if test="//CreditBureauReport">
      <xsl:text>
{\par \sect }
\sectd \pgwsxn12240\pghsxn15840\linex0\endnhere\sectdefaultcl 
{\footer \pard\plain \s17\ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\f1 \par }}
\pard\plain \ql \widctlpar\faauto\itap0
{\b\fs24\f1 Credit Bureau Report: \par \par }
{\f1\fs20 
</xsl:text>
      <xsl:for-each select="//CreditBureauReport/Line">
	       <xsl:value-of select="."/>
	       <xsl:text>\par </xsl:text>
      </xsl:for-each>
      <xsl:text>}</xsl:text>
    </xsl:if>
  </xsl:template>

  <!--  Business Rules -->
  <xsl:template name="EnglishBusinessRulesSection">
    <xsl:if test="//BusinessRules">
      <xsl:text>{\par \sect }
\sectd \pgwsxn12240\pghsxn15840\psz1\linex0\headery0\footery14\endnhere\sectdefaultcl 
{\footer \pard\plain \s17\ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\f1 \par }}
\pard\plain \ql \widctlpar\faauto\itap0
{\f1\fs24\b Business Rules:\par}
{\f1\fs20 </xsl:text>
<xsl:for-each select="//BusinessRules/Rule">
<!--  Here we create a table to hold the business rule and critical indicator.  If the item
	doesn't have a critical flag, then we assume it to be a description and insert 
	it into a single column row. -->
		<xsl:choose>
			<xsl:when test = "@Critical = 'Y' or @Critical = 'N'" >
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\cltxlrtb\clftsWidth3\clwWidth10728 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\fs20 </xsl:text>
<xsl:choose>
<xsl:when test="@Critical = 'Y'">C</xsl:when>
<xsl:otherwise>-</xsl:otherwise>
</xsl:choose>
<xsl:text>\cell </xsl:text>
<xsl:value-of select="."/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\fs20 \trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\cltxlrtb\clftsWidth3\clwWidth10728 \cellx10908
\row 
}</xsl:text>
</xsl:when>
<xsl:otherwise>	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\fs20 </xsl:text><xsl:value-of select="."/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\fs20 \trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
      </xsl:for-each>
      <xsl:text>}\par </xsl:text>
    </xsl:if>
  <!--  END BUSINESS RULES -->
  </xsl:template>

  <!-- Parties-->
  <xsl:template name="EnglishParties">
    <xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11520\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1Parties\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11520\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Name\cell Business ID\cell Company Name\cell Address\cell Bank Information\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="//PartyProfile/Parties">
<xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./partyType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./partyName"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./BusinessID"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./CompanyName"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./PartyAddress"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./BankAddress"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\row 
}</xsl:text>
		</xsl:for-each><!--Other Income loop-->
		<xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 \par }</xsl:text>
	
  </xsl:template>


	<!-- ************************************************************************ 	-->
	<!-- French template section                                             			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="FrenchTitle">
		<xsl:text>\trowd \trftsWidth3\trwWidth14400\trautofit1\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth7255 \cellx7255
\clvertalb\cltxlrtb\clftsWidth3\clwWidth7145 \cellx14400
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs28 R�capitulatif de la demande\cell }
\pard\plain \s15\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 </xsl:text>

    <xsl:if test="//ReferenceNum">
      <xsl:text>Num�ro de gestion d'hypoth�que: </xsl:text><xsl:value-of select="//ReferenceNum"/>
    </xsl:if>

    <xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trftsWidth3\trwWidth14400\trautofit1\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth7255 \cellx7255
\clvertalb\cltxlrtb\clftsWidth3\clwWidth7145 \cellx14400
\row 
}
\trowd \trrh20\trbrdrt\brdrengrave\brdrw60 
\trftsWidth3\trwWidth14400\trautofit1\trpaddfl3\trpaddfr3 
\clbrdrt\brdrengrave\brdrw60 
\cltxlrtb\clftsWidth3\clwWidth14400 \cellx14400
\pard \ql \li0\ri-837\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin-837\lin0 
{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trrh20\trbrdrt\brdrengrave\brdrw60 
\trftsWidth3\trwWidth14400\trautofit1\trpaddfl3\trpaddfr3 
\clbrdrt\brdrengrave\brdrw60 
\cltxlrtb\clftsWidth3\clwWidth14400 \cellx14400
\row 
}

\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 </xsl:text>
		<xsl:value-of select="//CurrentDate"/>
		<xsl:text>} \par \par </xsl:text>
	</xsl:template>
	
	<xsl:template name="FrenchSummarySnapshot">
    <xsl:text>\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Panorama du r�capitulatif\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 N{\super o} de la demande\cell Nom de l'emprunteur\cell Statut de la demande\cell Date de statut de la demande\cell Firme source\cell Source\cell Secteur d'activit�\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright  
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//DealNum"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BorrowerName"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//DealStatus"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//DealStatusDate"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//SourceFirm"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//Source"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//LOB"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type de la demande\cell But de la demande\cell Montant total du pr�t\cell Prix d'achat\cell Terme du paiement\cell Date pr�vue de cl�ture\cell Traitement sp�cial\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//DealType"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//DealPurpose"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//TotalLoanAmount"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//PurchasePrice"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//ActualPaymentTerm"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//SpecialFeature"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2067 \cellx5307
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1517 \cellx6824
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1885 \cellx8709
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1737 \cellx10446
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1951 \cellx12397
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1823 \cellx14220
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{ \f1\par }
</xsl:text>
	</xsl:template>
	
	<xsl:template name="FrenchApplicantSection">
	   <xsl:for-each select="//ApplicantDetails/Applicant">
	   <!-- *** Applicant Info *** -->
    <xsl:text>
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8306\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 D�tails du demandeur </xsl:text><xsl:if test="./PrimaryBorrower='Y'"><xsl:text>principal </xsl:text></xsl:if><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8306\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5466 \cellx11380
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nom\cell Type de demandeur\cell Valeur nette\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5466 \cellx11380
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5466 \cellx11380
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./Name"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./Type"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./NetWorth"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5554 \cellx5914
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5466 \cellx11380
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Date de naissance\cell �tat civil\cell NAS\cell Statut de citoyennet�\cell Nbre de personnes � charge\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./DateOfBirth"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./MaritalStatus"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./SIN"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./CitizenshipStatus"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./NumDependants"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Client existant?\cell N{\super o} de r�f�rence client\cell Nbre de faillites\cell Statut de faillite\cell Personnel du pr�teur?\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./ExistingClient"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./ClientReferenceNum"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./TimesBankrupt"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./BankruptcyStatus"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./StaffOfLender"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 N{\super o} de t�l�phone maison\cell N{\super o} de t�l�phone bureau\cell Num�ro de t�l�phone cellulaire\cell N{\super o} de t�l�copieur\cell Adresse courriel\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./HomePhone"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./WorkPhone"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./CellPhone"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./Fax"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./EmailAddress"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 M�thode de communication pr�f�r�e\cell Langue d�sir�e\cell \cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./PreferredContactMethod"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LanguagePreference"/><xsl:text>\cell 
\cell 
\cell 
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2674 \cellx5914
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2732 \cellx8646
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2734 \cellx11380
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2840 \cellx14220
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\par }</xsl:text>
<!--
***********************************
 *** Applicant Addresses  *** 
***********************************
-->
<xsl:if test="./Addresses">
<xsl:text>\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 D�tails de l'adresse\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\row 
}</xsl:text>
<xsl:for-each select="./Addresses/Address">
<xsl:text>\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Adresse </xsl:text>

<xsl:choose>
	<xsl:when test="./Status='Current'">
	<xsl:text>actuelle</xsl:text>
	</xsl:when>
	<xsl:when test="./Status='Previous'">
	<xsl:text>pr�c�dente</xsl:text>
	</xsl:when>
</xsl:choose>

<xsl:text>\cell Temps pass� � cette r�sidence\cell Statut r�sidentiel\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./Line1"/><xsl:text>, 
</xsl:text><xsl:value-of select="./Line2"/>
<xsl:if test="./Line2">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./City"/>
<xsl:if test="./City and ./Province">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./Province"/><xsl:text>, 
</xsl:text><xsl:value-of select="./PostalCode"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./TimeAtResidence"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./ResidentialStatusID"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\row 
}</xsl:text>
</xsl:for-each><!-- *** End Applicant Addresses  *** -->
<xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
</xsl:if>
	<!-- *** Applicant Employment History  *** -->
<xsl:for-each select="./EmploymentHistory/Employment">
<xsl:text>\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Employeur </xsl:text>

<xsl:choose>
	<xsl:when test="./EmploymentStatus='Current'">
	<xsl:text>actuel</xsl:text>
	</xsl:when>
	<xsl:when test="./EmploymentStatus='Previous'">
	<xsl:text>pr�c�dente</xsl:text>
	</xsl:when>
</xsl:choose>

<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8280 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nom de l'employeur\cell Type d'emploi\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8280 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8280 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./EmployerName"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./EmploymentType"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8280 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Num�ro de t�l�phone bureau\cell Num�ro de t�l�copie bureau\cell Adresse �lectronique bureau\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./EmployerWorkPhone"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./EmployerFax"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./EmployerEmailAddress"/><xsl:text>
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Adresse du bureau\cell Secteur d'activit�\cell Profession/M�tier\cell Titre/Poste\cell Depuis combine de temps?\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./EmployerMailingAddressLine1"/><xsl:text>, </xsl:text>
<xsl:value-of select="./EmployerMailingAddressLine2"/>
<xsl:if test="./EmployerMailingAddressLine2">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./EmployerCity"/>
<xsl:if test="./EmployerCity and ./EmployerProvince">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./EmployerProvince"/>
<xsl:text>, </xsl:text><xsl:value-of select="./EmployerPostalCode"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./IndustrySector"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./Occupation"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./JobTitle"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./TimeAtJob"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5580 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2790 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2790 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type de revenu\cell Description du revenu\cell P�riode du revenu\cell Montant du revenu\cell % compris dans l'ABD\cell % compris dans l'ATD\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2790 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2790 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./IncomeType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./IncomeDescription"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./IncomePeriod"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./IncomeAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./PercentIncludedInTDS "/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx8010
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx10080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx12150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx14220
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
</xsl:for-each> <!--Employment History loop-->

<xsl:if test="./EmploymentHistory/OtherIncome">
<xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11520\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Autre revenu\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11520\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type de revenu\cell Description du revenu\cell P�riode du revenu\cell Montant du revenu\cell % compris dans l'ABD\cell % compris dans l'ATD\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="./EmploymentHistory/OtherIncome/Line">
<xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./IncomeType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./IncomeDescription"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./IncomePeriod"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./IncomeAmount"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./PercentIncludedInTDS"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\row 
}</xsl:text>
		</xsl:for-each><!--Other Income loop-->
		<xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 \par }</xsl:text>
		</xsl:if>
	
	<!-- *** ASSETS SECTION *** -->
	<xsl:if test="./Assets">
	<xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 �l�ments de l'actif\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4401 \cellx7641
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type d'actif\cell Description de l'actif\cell Valeur de l'actif\cell % compris dans la valeur nette\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4401 \cellx7641
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="./Assets/Asset"><xsl:text>
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4401 \cellx7641
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text><xsl:value-of select="./AssetType"/><xsl:text>\cell
</xsl:text><xsl:value-of select="./AssetDescription"/><xsl:text>\cell
</xsl:text><xsl:value-of select="./AssetValue"/><xsl:text>\cell
</xsl:text><xsl:value-of select="./PercentIncludedInNetWorth"/><xsl:text>\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2862 \cellx3240
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth4401 \cellx7641
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\row 
}</xsl:text>
	</xsl:for-each>
<xsl:text>\trowd \trgaph108\trleft378\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7263 \cellx7641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Total:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24\b </xsl:text><xsl:value-of select="./Assets/Total"/><xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7263 \cellx7641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3339 \cellx10980
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx14220
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
</xsl:if><!--Assets-->

	<!-- *** LIABILITIES SECTION *** -->
	<xsl:if test="./Liabilities">
<xsl:text>\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11707\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2142 \cellx2513
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 �l�ments du passif\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11707\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2142 \cellx2513
\row 
}
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth629 \cellx1000
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1513 \cellx2513
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2827 \cellx5340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1400 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1140 \cellx9780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1600 \cellx11680
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1030 \cellx13630
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 BC\cell Type de passif\cell Description des �l�ments du passif\cell Limite\cell Montant\cell Mensuel\cell Date d'�ch�ance\cell % ABD\cell % ATD\cell Pay�\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth629 \cellx1000
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1513 \cellx2513
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2827 \cellx5340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1400 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1140 \cellx9780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1600 \cellx11680
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1030 \cellx13630
\row 
}</xsl:text>
<xsl:for-each select="./Liabilities/Liability"><xsl:text>
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth629 \cellx1000
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1513 \cellx2513
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2827 \cellx5340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1400 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1140 \cellx9780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1600 \cellx11680
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1030 \cellx13630
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./CB"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityDescription"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityLimit"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityMonthlyPayment"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityMarutityDate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./PercentIncludedInTDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityPayoffType"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft371
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth629 \cellx1000
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1513 \cellx2513
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2827 \cellx5340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1400 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1140 \cellx9780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1600 \cellx11680
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1160 \cellx12500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1030 \cellx13630
\row 
}</xsl:text>
	</xsl:for-each>
<xsl:text>\trowd \trgaph115\trleft371\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6369 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth1240 \cellx9580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4840 \cellx14920
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Totaux:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24\b
</xsl:text><xsl:value-of select="./Liabilities/TotalAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./Liabilities/TotalMonthly"/><xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft371\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6369 \cellx6740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1300 \cellx8140
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1240 \cellx9580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4840 \cellx14820
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par \par}</xsl:text>
        </xsl:if><!-- Liabilities -->
    </xsl:for-each> <!--Applicant loop-->
  </xsl:template>

  <!--  Properties -->
  <xsl:template name="FrenchPropertiesSection">
    <xsl:for-each select="//Properties/Property">
      <xsl:text>\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 D�tails de la propri�t�\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10980\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Propri�t� principale?\cell Adresse de la propri�t�\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth10980 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./PrimaryProperty"/>
    <xsl:text>
\cell </xsl:text>
    <xsl:value-of select="./AddressLine1"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="./AddressLine2"/>
    <xsl:if test="./AddressLine2">
      <xsl:text>, </xsl:text>
    </xsl:if>
    <xsl:value-of select="./City"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="./Province"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="./PostalCode"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx3240
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth10980 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Description l�gale\cell Type d'habitation\cell Style d'habitation\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./LegalLine1"/>
    <xsl:if test="./LegalLine2">
      <xsl:text>\par </xsl:text>
      <xsl:value-of select="./LegalLine2"/>
    </xsl:if>
    <xsl:if test="./LegalLine3">
      <xsl:text>\par </xsl:text>
      <xsl:value-of select="./LegalLine3"/>
    </xsl:if>
    <xsl:text>\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./DwellingType"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./DwellingStyle"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth9540 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nom du constructeur\cell Taille du garage\cell Type de garage\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth9540 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9540 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./BuilderName"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./GarageSize"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./GarageType"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9540 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 

\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nouvelle construction?\cell �ge de la structure\cell Nbre d'unit�s\cell Nbre de pi�ces\cell Espace habitable\cell Taille du terrain\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./NewConstruction"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./StructureAge"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./NumUnits"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./NumBedrooms"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./LivingSpace"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./LotSize"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type de chauffage\cell Risque �cologique\cell Eau\cell Eaux us�es\cell MLS- (SIA Services inter-agences)\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./HeatingType"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./UFFIInsulation"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./Water"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./Sewage"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./MLS"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Tenure\cell Efficacit� �nerg�tique pour Assurance Hypotheque\cell Rabais de subdivision\cell Le numero de reserve pour la convetion de fiducie #\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./tenureType"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./MIEnergyEfficiency"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./subdivisionDiscount"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./onReserveTrustAgreementNumber"/>
    <xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Usage de la propri�t�\cell Occupation\cell Type de propri�t�\cell Emplacement de la propri�t�\cell Zonage\cell }
\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./PropertyUsage"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./OccupancyType"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./PropertyType"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./PropertyLocation"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./Zoning"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx12060
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Prix d'achat\cell Valeur du terrain\cell Avoir disponible\cell Valeur estimative de l'�valuation\cell Valeur r�elle de l'�valuation\cell }
\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./PurchasePrice"/>
    <xsl:text>\cell 
</xsl:text>
    <xsl:value-of select="./LandValue"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./EquityAvailable"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./EstimatedValue"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./ActualAppraisalValue"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx9900
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11520 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Date de l'�valuation\cell Source de l'�valuation\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11520 \cellx14220\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11520 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./AppraisalDate"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./AppraisalSource"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11520 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3195 \cellx11115
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3105 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 �valuateur\cell Adresse de l'�valuateur\cell N{\super o} de t�l�phone de l'�valuateur\cell N{\super o} de t�l�copie de l'�valuateur\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3195 \cellx11115
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3105 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3195 \cellx11115
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3105 \cellx14220
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./AppraiserName"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./AppraiserAddress"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./AppraiserPhone"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./AppraiserFax"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx2700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3195 \cellx11115
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3105 \cellx14220
\row 
}</xsl:text>
    <xsl:if test="./PropertyExpenses">
      <xsl:text>\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13860 \cellx14220
\pard\plain \s1\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs24 D�penses\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13860 \cellx14220
\row 
}
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell P�riode\cell Montant\cell % ABD\cell % ATD\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\row 
}</xsl:text>
    <xsl:for-each select="./PropertyExpenses/Expense">
      <xsl:text>
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="./ExpenseType"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./ExpensePeriod"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./ExpenseAmount"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./PercentIncludedInGDS"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="./PercentIncludedInTDS"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\row 
}
</xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\par}</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <!--  TERMS -->
  <xsl:template name="FrenchTermsSection">
    <xsl:text>\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA7020\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\pard\plain \ql \keep\keepn\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Termes\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA7020\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\row 
}
\trowd \irow1\irowband1\ts11\trgaph115\trleft353\trkeep\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847\clcbpatraw16 \cellx7200
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3510\clcbpatraw16 \cellx10710
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3510\clcbpatraw16 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\b\f1\fs20\insrsid14904860 Prix d'achat\cell Type de produit\cell Rang\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid14904860 
\trowd \irow1\irowband1\ts11\trgaph115\trleft353\trkeep\trbrdrt\brdrs\brdrw10 

\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847\clcbpatraw16 \cellx7200
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3510\clcbpatraw16 \cellx10710
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3510\clcbpatraw16 \cellx14220
\row 
}
\trowd \irow2\irowband2\ts11\trgaph115\trleft353\trkeep\trbrdrt\brdrs\brdrw10 

\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847\clshdrawnil \cellx7200
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx10710
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx14220
\pard \ql \li0\ri0\sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1\fs18\insrsid14904860 </xsl:text>
<xsl:value-of select="//TotalPurchasePrice"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//productType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Charge"/><xsl:text>\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid14904860 
\trowd \irow2\irowband2\ts11\trgaph115\trleft353\trkeep\trbrdrt\brdrs\brdrw10 

\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847\clshdrawnil \cellx7200
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx10710
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Mise de fonds\cell Pr�teur\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//RequiredDownPaymentAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Lender"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 RPV\cell Produit\cell Code du produit\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//LTV"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Product"/><xsl:text>\cell
</xsl:text><xsl:value-of select="//productCode"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}



\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9540
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220

\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Pr�t\cell Taux d'int�r�t affich�\cell Taux d'index\cell Adjustment d'index\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9540
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9540
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//NetLoanAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//PostedInterestRate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//IndexRate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//IndexAdjustment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9540
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220

\row 
}

\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Prime d'assurance\cell Escompte\cell </xsl:text><xsl:value-of select="//bestRateLabel"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//MI/Premium"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Discount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//bestRate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4680 \cellx11880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx14220
\row 
}

\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Total du pr�t\cell Prime\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//TotalLoanAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Premium"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 P�riode d'amortissement\cell Taux d'int�r�t r�duit\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//Amortization"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BuydownRate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 P�riode d'amortissement effective\cell Taux net\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//EffectiveAmortization"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//NetRate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Description du terme du paiement\cell C&amp;I\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//PaymentTerm"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//PandIPayment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Terme r�el du remboursement\cell C&amp;I suppl�mentaire\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard 
\ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//ActualPaymentTerm"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//AdditionalPrincipalPayment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Fr�quence des paiements\cell Total des fonds plac�s en main tierce\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//PaymentFrequency"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//TotalEscrowPayment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 P�nalit� de remboursement anticip�\cell Paiement total\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>

<xsl:for-each select="//Prepayment/Line">
<xsl:value-of select="."/>
<xsl:text>\par </xsl:text>
</xsl:for-each>

<xsl:text>\cell 
</xsl:text><xsl:value-of select="//TotalPayment"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Option de remboursement anticip�\cell Montant de la retenue de garantie\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//PrivilegePaymentOption"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//AdvanceHold"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}

\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Remise en argent\cell Fr�quence du changement du taux\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//CashbackAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//interestRateChangeFreq"/><xsl:text>
</xsl:text> mois <xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6847 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
</xsl:template>

  <!--  Other Details -->
  <xsl:template name="FrenchOtherDetailsSection">
    <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8316\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\pard\plain \ql \keep\keepn\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Autres d�tails\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8316\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1832 \cellx1832
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2072 \cellx3904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2972 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2072 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1972 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx13220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Cote de cr�dit\cell ABD combin�\cell ABD (Taux Admissible)\cell ATD combin�\cell ATD (Taux Admissible)\cell Taux Admissible\cell Produit Admissible:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1832 \cellx1832
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2072 \cellx3904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2972 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2072 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1972 \cellx13220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1832 \cellx1832
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2072 \cellx3904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2972 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2072 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1972 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx13220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//CreditScore"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//CombinedGDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Combined3YrGDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//CombinedTDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Combined3YrTDS"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//QualifyingRate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//QualifyProduct"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1832 \cellx1832
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2072 \cellx3904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2972 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2072 \cellx8676
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx11448
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1972 \cellx13220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 But de la demande\cell Succursale\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//DealPurpose"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BranchName"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type de la demande\cell Autres produits\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//DealType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//CrossSell"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Traitement sp�cial\cell N{\super o} de r�f�rence de l'hypoth�que existante\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SpecialFeature"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//RefExistingMtgNum"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Prix d'achat estimatif pr�autoris�\cell Type de r�f�rence\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//PreAppEstPurchasePrice"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//ReferenceType"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type de remboursement\cell N{\super o} de r�f�rence de la demande\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//RepaymentType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//ReferenceNum"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 2�me propri�t� existante?\cell Date pr�vue de cl�ture\cell Date du rajustement des int�r�ts:\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544\cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5544\cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SecondMortgage"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//InterestAdjustmentRate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}

\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Programme d'affiliation\cell Date du premier paiement\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard 
\ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//AffiliationProgram"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//FirstPaymentDate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Payeur de taxe\cell Date d'�ch�ance\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//TaxPayor"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//MaturityDate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Taux immobilis�?\cell Date de retour pr�vue de l'offre d'engagement\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//RateLockedIn"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//CommitmentExpiryDate"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{ <!-- Dave was here - start -->
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Date de retrait de la condition de financement\cell Code de demande de la source\cell Souscripteur\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//WaiverDate"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Underwriter"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nom de la source 1\cell Firme source 1\cell Num�ro d'enregistrement du permis de la source 1\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SourceName"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//SourceFirm"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//LicenseNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Adresse de la source 1\cell N{\super o} de t�l�phone de la source 1\cell N{\super o} de t�l�copieur de la source 1\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SourceAddress"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\row 
}<!-- Dave was here - end -->
<!-- Dave was here - start second guy -->
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nom de la source 2\cell Firme source 2\cell Num�ro d'enregistrement du permis de la source 2\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//Source2Name"/><xsl:text>\cell 
</xsl:text>
<xsl:if test="//Source2Name"><xsl:value-of select="//SourceFirm"/></xsl:if>
<xsl:text> \cell </xsl:text>
<xsl:if test="//Source2Name"><xsl:value-of select="//License2Number"/></xsl:if>
<xsl:text>\cell  
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx3132
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2772 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Adresse de la source 2\cell N{\super o} de t�l�phone de la source 2\cell N{\super o} de t�l�copieur de la source 2\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//Source2Address"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Source2Phone"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//Source2Fax"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8820 \cellx9180
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx11700
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx14220
\row 
}<!-- Dave was here - end second guy -->
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 No du client du source\cell Autre code d'usager du source\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//SOBBusinessID"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//alternativeID"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8316 \cellx14220
\row 
}


\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nom du banque\cell No de compte et de succursale \cell Num�ro de compte bancaire\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5544\cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5544\cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="//BankName"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BankTransitNumber"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//BankAccountNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5544 \cellx5904

\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx10062
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4158 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
</xsl:template>


  <!--  Equity Takeout -->
  <xsl:template name="FrenchEquityTakeoutSection">
    <xsl:if test="//Refi">
      <xsl:text>\trowd\trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trwWidthA11025\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2815\cellx2700
\pard \ql \nowidctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1 Hypoth�que sur plus value\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trwWidthA11025\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2815\cellx2700
\row
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2815\cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2721\cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\pard \ql \widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Date d'achat\cell Pr�teur\cell Prix d'achat original\cell Valeur des am�liorations\cell Amortissement mixte\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2815\cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2721 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768\cellx13725
\row 
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2815 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2721 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\pard \ql \sa120\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//Refi/OriginalPurchaseDate"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Refi/MortgageHolder"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Refi/PurchasePrice"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Refi/ImprovementValue"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//Refi/BlendedAmortization"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright  
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2815 \cellx2700\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2721 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\row
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3000\cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\pard \ql \widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 But\cell R�f�rence au compte existant\cell Montant original de l'hypoth�que\cell Montant de l'hypoth�que impay�\cell Type de produit\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3000\cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2768\cellx13725
\row 
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\pard \ql \sa120\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright {\f1\fs18 </xsl:text>
			<xsl:value-of select="//Refi/Purpose"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="//Refi/ExistingMtgNumber"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="//Refi/OriginalMtgAmount"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="//Refi/ExistingLoanAmount"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="//Refi/ProductType"/>	
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright  
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth3000 \cellx5421
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx8189
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx10957
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2768 \cellx13725
\row
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\pard \ql\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 Am�liorations\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\row 
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\pard \ql \sa120\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//Refi/ImproveDesc"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\row 
}
<!-- ***********************MCM Impl Team XS_16.11 Starts Added new field for Equity Take out Section-->
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\pard \ql\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs20 D�tails suppl�mentaires\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\row 
}
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\pard \ql \sa120\widctlpar\intbl\pvpara\phpg\posx1215\posy158\absh4320\absw12960\dxfrtext187\dfrmtxtx187\dfrmtxty0\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
			<xsl:value-of select="//Refi/AdditionalInformation"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\tpvpara\tphpg\tposx1215\tposy158\tdfrmtxtLeft187\tdfrmtxtRight187\trftsWidth3\trwWidth13840\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth13840 \cellx13725
\row 
}
<!-- ***********************MCM Impl Team XS_16.11 Ends***************** -->
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\par }
</xsl:text>
</xsl:if>
</xsl:template>

<!--End Of Equity Takeout Section-->
	
	
  <!--  Progress Advance -->
  <xsl:template name="FrenchProgressAdvanceSection">
    <xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10440\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs24 Avance progressive\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10440\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3519 \cellx7299
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3460 \cellx10759
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3461 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Avance progressive\cell Montant de l'avance � ce jour\cell Montant de la prochaine avance\cell Num�ro de l'avance\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3519 \cellx7299
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3460 \cellx10759
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3461 \cellx14220
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3519 \cellx7299
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3460 \cellx10759
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3461 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//ProgressAdvance"/><xsl:text>\cell </xsl:text><xsl:value-of select="//AdvanceToDateAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="//NextAdvanceAmount"/><xsl:text>\cell </xsl:text><xsl:value-of select="//AdvanceNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3402 \cellx3780
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3519 \cellx7299
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3460 \cellx10759
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3461 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }</xsl:text>
  </xsl:template>

  <!--  Mortgage Insurance -->
  <xsl:template name="FrenchMortgageInsuranceSection">
    <xsl:if test="//MI">
      <xsl:text>
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8640\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\pard \ql \keep\keepn\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Information sur l'assurance pr�t-hypoth�caire\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA8640\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16
\cltxlrtb\clftsWidth3\clwWidth5220 \cellx5580
\row 
}
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Indicateur\cell Statut\cell N{\super o} de police de l'assurance existant\cell N{\super o} de certificat\cell N{\super o} du certificat de pr�qualification\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//MI/Indicator"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/Status"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/ExistingPolicyNum"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/CertificateNum"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/PrequalificationMINum"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Assureur\cell Payeur\cell  Frais pr�lev�s � l'acquis. ass.\cell Prime\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2160 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\pard \ql \sa120\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
    <xsl:value-of select="//MI/Type"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/Insurer"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/Payor"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/Upfront"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/Premium"/>
    <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2160 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\row 
}
\trowd \irow5\irowband5\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid3374656 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520\clcbpatraw16 \cellx2880
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4860\clcbpatraw16 \cellx7740
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3060\clcbpatraw16 \cellx10800
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3420\clcbpatraw16 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0
<!-- #DG788 --> 
{\b\f1\fs20 Type d'avance �chelonn�e\cell Inspection de l'avance de progress par\cell 
Hypotheques REER autog�r�\cell L'identificateur poursuivant de produit pour SCHL\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid9926668 
\trowd \irow5\irowband5\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid3374656 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520\clcbpatraw16 \cellx2880
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4860\clcbpatraw16 \cellx7740
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3060\clcbpatraw16 \cellx10800
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3420\clcbpatraw16 \cellx14220
\row 
}
\trowd \irow6\irowband6\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3
\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid3374656 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx7740
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1\fs20\insrsid9926668\charrsid9926668 </xsl:text>
    <xsl:value-of select="//MI/progressAdvanceType"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/progressAdvanceInspectionBy"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/selfDirectedRRSP"/>
    <xsl:text>\cell </xsl:text>
    <xsl:value-of select="//MI/cmhcProductTrackerIdentifier"/>
    <xsl:text>\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid9926668\charrsid9926668 
\trowd \irow6\irowband6\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3
\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid3374656 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx2880
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx7740
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx10800
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx14220
\row 
}
\trowd \irow7\irowband7\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3
\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid1125628 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520\clcbpatraw16 \cellx2880
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\clcbpatraw16 \cellx5580
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220\clcbpatraw16 \cellx10800
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3420\clcbpatraw16 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid1125628
<!-- #DG788 --> 
{\b\f1\fs20 Remboursement MCVD\cell Amortissement MCVD\cell 
Date d'�ch�ance pour un pr�t non Amorti MCVD\cell \cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid8210755 
\trowd \irow7\irowband7\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid1125628 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520\clcbpatraw16 \cellx2880
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700\clcbpatraw16 \cellx5580
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220\clcbpatraw16 \cellx10800
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3420\clcbpatraw16 \cellx14220
\row 
}
\trowd \irow8\irowband8\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid1125628 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520\clshdrawnil \cellx2880
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700\clshdrawnil \cellx5580
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220\clshdrawnil \cellx10800
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420\clshdrawnil \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid1125628 
{\f1\fs20\insrsid8210755\charrsid9926668 </xsl:text>
<xsl:value-of select="//MI/locRepaymentType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/locAmortizationMonths"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/locInterestOnlyMaturityDate"/><xsl:text>\cell \cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid8210755\charrsid9926668 
\trowd \irow8\irowband8\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid1125628 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520\clshdrawnil \cellx2880
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700\clshdrawnil \cellx5580
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220\clshdrawnil \cellx10800
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420\clshdrawnil \cellx14220
\row 
}
\trowd \irow9\irowband9\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid13443886 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220\clcbpatraw16 \cellx5580
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8640\clcbpatraw16 \cellx14220
\pard \ql \li0\ri0\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\b\f1\fs20\insrsid13443886 Intervention du souscripteur hypoth\'e9caire requise\cell Demande pour service de base\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid13443886 
\trowd \irow9\irowband9\ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid13443886 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5220\clcbpatraw16 \cellx5580
\clvertalt\clbrdrt\brdrs\brdrw10 





\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth8640\clcbpatraw16 \cellx14220
\row 
}
\trowd \irow10\irowband10\lastrow \ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid13443886 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220\clshdrawnil \cellx5580
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8640\clshdrawnil \cellx14220
\pard \ql \li0\ri0\sa120\keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1\fs18\insrsid13443886 </xsl:text>
<xsl:value-of select="//MI/RUIntervention"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="//MI/requestedStandardService"/><xsl:text>\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\insrsid13443886 
\trowd \irow10\irowband10\lastrow \ts11\trgaph108\trleft360\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\tblrsid13443886 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5220\clshdrawnil \cellx5580
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8640\clshdrawnil \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }
</xsl:text>
</xsl:if>
</xsl:template>

  <!--  Down Payments -->
  <xsl:template name="FrenchDownPaymentsSection">
    <xsl:if test="//DownPayments">
    <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Mise de fonds\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Source\cell Description\cell Montant\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="//DownPayments/DownPayment">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./Source"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Description"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Amount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
</xsl:text>
</xsl:for-each>
<xsl:text>
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Mise de fonds disponible:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 </xsl:text><xsl:value-of select="//DownPayments/Total"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Mise de fonds r�elle:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 </xsl:text><xsl:value-of select="//DownPayments/RequiredDownPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }</xsl:text>
</xsl:if>
</xsl:template>


  <!-- Parties-->
  <xsl:template name="FrenchParties">
    <xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11520\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1Intervenants\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA11520\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\row 
}
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Nom\cell No du client\cell Nom du client\cell Adresse\cell Coordonn�es bancaire\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="//PartyProfile/Parties">
<xsl:text>\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./partyType"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./partyName"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./BusinessID"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./CompanyName"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./PartyAddress"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./BankAddress"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft378
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2322 \cellx2700
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2292 \cellx4992
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx7299
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx9606
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx11913
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth2307 \cellx14220
\row 
}</xsl:text>
		</xsl:for-each><!--Other Income loop-->
		<xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 \par }</xsl:text>
	
</xsl:template>

  <!--  Fees -->
  <xsl:template name="FrenchFeesSection">
    <xsl:if test="//Fees">
      <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3600
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Frais\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3600
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3060 \cellx6660
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7560 \cellx14220
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs20 Type\cell Montant\cell Statut\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3060 \cellx6660
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth7560 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="//Fees/Fee">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3240 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3060 \cellx6660
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7560 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="./Type"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Amount"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Status"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx6660
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7560 \cellx14220
\row 
}</xsl:text>
</xsl:for-each>
<xsl:text>\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }</xsl:text>
</xsl:if>
</xsl:template>

  <xsl:template name="FrenchEscrowPayment">	
    <xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Fonds plac�s en main tierce\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA9720\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx4500
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type\cell Description\cell Montant\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}</xsl:text>
<xsl:for-each select="//EscrowDetails/Escrow">
<xsl:text>
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="./Type"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Description"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="./Amount"/><xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx4500
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6840 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
</xsl:text>
</xsl:for-each>
<xsl:text>

\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Total des fonds plac�s en main tierce:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 </xsl:text><xsl:value-of select="//EscrowDetails/TotalEscrowPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx14220
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }</xsl:text>

</xsl:template>

  <!--  Conditions -->
  <xsl:template name="FrenchConditionsSection">

    <xsl:if test="//Conditions">
      <xsl:text>\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl115\trpaddt115\trpaddb115\trpaddr115\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3593
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Conditions\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl115\trpaddt115\trpaddb115\trpaddr115\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3593
\row 
}</xsl:text>

<xsl:for-each select="//Conditions/Condition">
<xsl:text>\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddt115\trpaddb115\trpaddr115\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3 
</xsl:text>

<xsl:choose>
	<!--  Only 1 condition -->
	<xsl:when test="position()=1 and position()=last()">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
	</xsl:text>
	</xsl:when>

	<!--  First row -->
	<xsl:when test="position()=1">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrnone
</xsl:text>
	</xsl:when>
	
	<!-- Last row -->
	<xsl:when test="position()=last()">
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrs\brdrw10 
</xsl:text>
	</xsl:when>

	<!-- Rows in between -->
	<xsl:otherwise>
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrnone 
</xsl:text>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth540 \cellx893
</xsl:text>

<xsl:choose>
	<!--  Only 1 condition -->
	<xsl:when test="position()=1 and position()=last()">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
	</xsl:text>
	</xsl:when>

	<!--  First row -->
	<xsl:when test="position()=1">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrnone
</xsl:text>
	</xsl:when>

	<!-- Last row -->
	<xsl:when test="position()=last()">
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrs\brdrw10 
</xsl:text>
	</xsl:when>

	<!-- Rows in between -->
	<xsl:otherwise>
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrnone 
</xsl:text>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\clbrdrl\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13320 \cellx14213
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="position()"/><xsl:text>.\cell </xsl:text>

<xsl:for-each select="./Line">
<xsl:value-of select="."/>
<xsl:text>
\par </xsl:text>
</xsl:for-each>

<xsl:text>\cell } 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph115\trleft353\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl115\trpaddt115\trpaddb115\trpaddr115\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3 
</xsl:text>

<xsl:choose>
	<!--  Only 1 condition -->
	<xsl:when test="position()=1 and position()=last()">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
	</xsl:text>
	</xsl:when>

	<!--  First row -->
	<xsl:when test="position()=1">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrnone
</xsl:text>
	</xsl:when>
	<!-- Last row -->
	<xsl:when test="position()=last()">
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrs\brdrw10 
</xsl:text>
	</xsl:when>

	<!-- Rows in between -->
	<xsl:otherwise>
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrnone 
</xsl:text>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth540 \cellx893
</xsl:text>

<xsl:choose>
	<!--  Only 1 condition -->
	<xsl:when test="position()=1 and position()=last()">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
	</xsl:text>
	</xsl:when>

	<!--  First row -->
	<xsl:when test="position()=1">
	<xsl:text>\clbrdrt\brdrs\brdrw10 \clbrdrb\brdrnone
</xsl:text>
	</xsl:when>
	<!-- Last row -->
	<xsl:when test="position()=last()">
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrs\brdrw10 
</xsl:text>
	</xsl:when>

	<!-- Rows in between -->
	<xsl:otherwise>
	<xsl:text>\clbrdrt\brdrnone \clbrdrb\brdrnone 
</xsl:text>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\clbrdrl\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth13320 \cellx14213
\row 
}</xsl:text>
	
</xsl:for-each>

<xsl:text>\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1 \par }</xsl:text>
</xsl:if>
</xsl:template>

    <!--  Notes -->
    <xsl:template name="FrenchNotesSection">
      <xsl:if test="//Notes">
<xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\pard\plain \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1 Notes\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trwWidthA10620\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\row 
}
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx3660
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6660 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\pard \ql \keep\keepn\widctlpar\intbl\faauto 
{\b\f1\fs20 Date\cell User Name\cell Text\cell  Category Type\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2520 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6660 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3600
\row 
}
</xsl:text>
<xsl:for-each select="//Notes/Note">
<xsl:text>\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3600
\pard \ql \keep\keepn\widctlpar\intbl\tx900\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 
</xsl:text><xsl:value-of select="./Date"/><xsl:text>\cell </xsl:text>
<xsl:value-of select="UserFullName"/><xsl:text>\cell 
</xsl:text>
<xsl:for-each select="Text/Line">
<xsl:value-of select="."/>
<xsl:if test="not(position()=last())"><xsl:text>\par </xsl:text></xsl:if>
</xsl:for-each>
<xsl:text>\cell </xsl:text><xsl:value-of select="DealNotesCategory"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft360
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx2340
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2520 \cellx3600
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx12220
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2700 \cellx3600

\row 
}</xsl:text>
</xsl:for-each>
<xsl:text>
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0  
{\f1 \par }</xsl:text>
</xsl:if>
</xsl:template>

  <!--  Bureau -->
  <xsl:template name="FrenchCreditBureauSection">
    <xsl:if test="//CreditBureauReport">
      <xsl:text>
{\par \sect }
\sectd \pgwsxn12240\pghsxn15840\linex0\endnhere\sectdefaultcl 
{\footer \pard\plain \s17\ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\f1 \par }}
\pard\plain \ql \widctlpar\faauto\itap0
{\b\fs24\f1 Bureau de cr�dit:\par \par }
{\f1\fs20 
</xsl:text>
<xsl:for-each select="//CreditBureauReport/Line">
	<xsl:value-of select="."/>
	<xsl:text>\par </xsl:text>
</xsl:for-each>
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template>

  <!--  Business Rules -->
  <xsl:template name="FrenchBusinessRulesSection">
    <xsl:if test="//BusinessRules">
      <xsl:text>{\par \sect }
\sectd \pgwsxn12240\pghsxn15840\psz1\linex0\headery0\footery14\endnhere\sectdefaultcl 
{\footer \pard\plain \s17\ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\f1 \par }}
\pard\plain \ql \widctlpar\faauto\itap0
{\f1\fs24\b R�gles d'affaire:\par}
{\f1\fs20 </xsl:text>
<xsl:for-each select="//BusinessRules/Rule">
<!--  Here we create a table to hold the business rule and critical indicator.  If the item
	doesn't have a critical flag, then we assume it to be a description and insert 
	it into a single column row. -->
		<xsl:choose>
			<xsl:when test = "@Critical = 'Y' or @Critical = 'N'" >
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\cltxlrtb\clftsWidth3\clwWidth10728 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\fs20 </xsl:text>
<xsl:choose>
<xsl:when test="@Critical = 'Y'">C</xsl:when>
<xsl:otherwise>-</xsl:otherwise>
</xsl:choose>
<xsl:text>\cell </xsl:text>
<xsl:value-of select="."/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\fs20 \trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\cltxlrtb\clftsWidth3\clwWidth10728 \cellx10908
\row 
}</xsl:text>
</xsl:when>
<xsl:otherwise>	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\fs20 </xsl:text><xsl:value-of select="."/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\fs20 \trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
<xsl:text>}\par </xsl:text>
</xsl:if>
<!--  END BUSINESS RULES -->
</xsl:template>	

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>{\f1\par }}</xsl:text>
	</xsl:template>
	
  <xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
}

{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}

{\stylesheet
{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \snext0 Normal;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext15 header;}
{\*\cs16 \additive \sbasedon10 page number;}
{\s17\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext17 footer;}
}

\paperw15840\paperh12240\margl720\margr720\margt720\margb720 \widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\formshade\horzdoc\dgmargin
\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \lndscpsxn\psz1\pgnrestart\linex0\headery0\footery20\endnhere\sectdefaultcl 

{\footer \trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth7308 \cellx7200
\clftsWidth3\clwWidth7308 \cellx14508
\pard\plain \s17\ql \ri360\widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360 
{\f1\lang1024\langfe1024\cgrid0\noproof <!--DOCUMENTID-->\cell }\pard 
\s17\qr \ri360\widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360 
{\f1\lang1024\langfe1024\cgrid0\noproof Page }{\field{\*\fldinst {\f1\lang1024\langfe1024\cgrid0\noproof  PAGE }}{\fldrslt {
\f1\lang1024\langfe1024\cgrid0\noproof 1}}}{\f1\lang1024\langfe1024\cgrid0\noproof </xsl:text>
 
    <xsl:choose>
  		<xsl:when test="/*/LanguageFrench">
        <xsl:text> de</xsl:text>
      </xsl:when>
  		<xsl:otherwise>
        <xsl:text> of</xsl:text>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:text> }{\field{\*\fldinst {\f1\lang1024\langfe1024\cgrid0\noproof  NUMPAGES }}{\fldrslt {\f1\lang1024\langfe1024\cgrid0\noproof 1}}}{\f1\lang1024\langfe1024\cgrid0\noproof 
\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\lang1024\langfe1024\cgrid0\noproof \trowd 
\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clftsWidth3\clwWidth7308 \cellx7200\clftsWidth3\clwWidth7308 \cellx14508\row }
\pard\plain \s17\ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 
{\f1 \par }}
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
</xsl:text>
	</xsl:template>
</xsl:stylesheet>
