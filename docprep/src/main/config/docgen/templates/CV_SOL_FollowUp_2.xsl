<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
13/Oct/2006 DVG #DG522 #4922  Cervus CR # 106 Rebranding  
12/Aug/2005 DVG #DG294 #662  Cervus - docs to be created for PII - reformat - add French
06/Jul/2005 DVG #DG246 #662  Cervus - docs to be created for PII - reformat - reorganized
prepared by: Catherine Rugaizer 
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- Legal size paper -->
	<xsl:import href="CV_SOL_FollowUp_1.xsl"/>	
	<xsl:output method="text"/>
	
	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>		
		<!--#DG294 add French-->
		<xsl:choose>
			<xsl:when test="/Document[@lang = 1]">
				<xsl:call-template name="FrToSection"/>
				<xsl:call-template name="FrReSection"/>
				<xsl:call-template name="FrMainTextFurth"/>
				<xsl:call-template name="FrMainTextToCv"/>
				<xsl:call-template name="FrMainTextThxAdv"/>
			</xsl:when>
			<xsl:otherwise>
				<!--xsl:call-template name="DocumentHeader"/-->
				<xsl:call-template name="ToSection"/>
				<xsl:call-template name="ReSection"/>
				<xsl:call-template name="MainTextFurth"/>
				<!--#DG294 xsl:call-template name="MainTextTranClosed"/-->
				<xsl:call-template name="MainTextToCv"/>
				<!--#DG294 xsl:call-template name="MainTextLastAtempt"/-->
				<xsl:call-template name="MainTextThxAdv"/>
			</xsl:otherwise>		
		</xsl:choose>
		
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- templates section                                                                                -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="MainTextTranClosed">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 \qj </xsl:text>
		<xsl:text>This transaction closed on </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text> and more than 80 days have elapsed. </xsl:text>
		<xsl:text>\par }</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 </xsl:text>
		<xsl:text>Please forward the Final Report together with the required documentation outlined in the Solicitor Instructions to:</xsl:text>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="MainTextLastAtempt">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 \qj </xsl:text>
		<xsl:text>Please be advised that this is our last attempt to follow up with your office.  This matter will be escalated to our legal department if we do not receive your final report together with the required documentation within 11 calendar days.</xsl:text>
		<xsl:text>\par }</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 \b \qj </xsl:text>
		<xsl:text>If we are required to escalate this issue to our Legal Department, your firm will not 
be permitted to act on behalf of 
<!--#DG522 Cervus Financial Corp.--></xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>, pending your compliance with our 
requirements and additional approval from our Legal Department.</xsl:text>
		<xsl:text>\par }</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 </xsl:text>
		<xsl:text>Thank your prompt attention in this matter.</xsl:text>
		<xsl:text>\par }</xsl:text>
	</xsl:template>
</xsl:stylesheet>
