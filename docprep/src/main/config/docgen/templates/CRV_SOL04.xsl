<!-- prepared by: Catherine Rugaizer -->
<!-- Letter size -->
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' >  
<xsl:output method="text"/>

<xsl:template match="/">
	<xsl:call-template name="RTFFileStart4"/>
	<xsl:call-template name="EnglishHeader"/>
	<!-- <xsl:call-template name="EnglishFooter"/>-->
	<xsl:call-template name="EnglishPage1"/>
	<xsl:call-template name="EnglishPage2Start"/>
	<xsl:call-template name="EnglishPage2"/>
     <xsl:call-template name="EnglishPage3Start"/>
	<xsl:call-template name="EnglishPage3"/>	
 	<xsl:call-template name="RTFFileEnd4"/> 	
</xsl:template>  
	
<!-- ************************************************************************ -->
<!-- templates section                                                                                -->
<!-- ************************************************************************ -->

<!-- ================================================  -->
<!-- ================================================ EnglishHeader start  ================================================  -->
<xsl:template name="EnglishHeader">

<!-- ====== #727 by Catherine ====== -->
  <xsl:if test="//specialRequirementTags/isAmended = 'Y'">
          <xsl:text>{\f1\fs20 AMENDED SOLICITOR INSTRUCTIONS AS OF </xsl:text>  <xsl:value-of select="//General/CurrentDate"/>
          <xsl:text>\par \par }</xsl:text>
  </xsl:if>
<!-- ====== #727 by Catherine end ====== -->

<xsl:text>{\header \pard\plain \s20\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\b\i\f1\fs28 MORTGAGE COMMITMENT}}</xsl:text>

</xsl:template>

<!-- ================================================ EmptyHeaderstart  ================================================  -->
<xsl:template name="EmptyHeader">
<xsl:text>{\header \pard\plain \s20\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 { }}</xsl:text>
</xsl:template>

<!-- ================================================ EnglishFooter start  ================================================  -->

<xsl:template name="EnglishFooter">
<xsl:text>{\footer \trowd \trgaph108\trleft-90\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx3720\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb
\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx7530\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx11340\pard\plain \s21\qr \widctlpar\intbl
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright {\f1\fs20\cell }\pard \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright {\f1\fs20 Page 
{\field{\*\fldinst {\cs22  PAGE }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 1}}}{\f1  of }{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}\cell }\pard \s21\qr \widctlpar\intbl
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright {\f1\fs20 Initials _____ Date _______________}{\cell }\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx3720\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb
\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx7530\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx11340\row }\pard\plain \s21\ql \widctlpar
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\fs20\par }}</xsl:text>
</xsl:template>

<!-- ================================================ EnglishPage1 start  ================================================  -->

	<xsl:template name="EnglishPage1">
	<xsl:text>
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \s4\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel3\adjustright\itap0 
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6030 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx11340
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
<xsl:call-template name="CervusLogo"/>
<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/LenderProfile/lenderName"/><xsl:text>
\par </xsl:text><xsl:value-of select="//Deal/BranchProfile/Contact/Addr/addressLine1"/><xsl:text>
\par </xsl:text><xsl:value-of select="//Deal/BranchProfile/Contact/Addr/addressLine2"/>

<xsl:if test="//Deal/BranchProfile/Contact/Addr/addressLine2">
<xsl:text>\par </xsl:text>
</xsl:if>

<xsl:text>}
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/BranchProfile/Contact/Addr/city"/><xsl:text>, </xsl:text>
<xsl:value-of select="//Deal/BranchProfile/Contact/Addr/province"/><xsl:text>  </xsl:text>
<xsl:value-of select="concat(//Deal/BranchProfile/Contact/Addr/postalFSA,' ', //Deal/BranchProfile/Contact/Addr/postalLDU)"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6030 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr
\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
<xsl:if test="//Deal/BranchProfile/Contact/contactPhoneNumber">
<xsl:text>Telephone:</xsl:text>
</xsl:if>
<xsl:text>\cell </xsl:text>
<xsl:call-template name="FormatPhone">
	<xsl:with-param name="pnum" select=".//Deal/BranchProfile/Contact/contactPhoneNumber"/>
</xsl:call-template>	

<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>

<xsl:if test="//Deal/BranchProfile/Contact/contactFaxNumber">
<xsl:text>Fax:</xsl:text>
</xsl:if>
<xsl:text>\cell </xsl:text>

<xsl:call-template name="FormatPhone">
	<xsl:with-param name="pnum" select="//Deal/BranchProfile/Contact/contactFaxNumber"/>
</xsl:call-template>	

<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8730
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 TO:\cell </xsl:text>
<xsl:call-template name="PrimaryBorrowerName"/>
<xsl:text>}\line {\f1\fs20 </xsl:text><xsl:call-template name="PrimaryBorrowerAddress"/><xsl:text>\cell</xsl:text>
<xsl:text>}
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\b\fs20\ul\f1 Underwriter \par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
<xsl:call-template name="DealUnderwriter"/>
<xsl:text> \cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\b\fs20\ul Date\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//General/CurrentDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8730
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11340
\row 
}
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//specialRequirementTags/CareOf"/><xsl:text> \cell </xsl:text>
<xsl:value-of select="//Deal/SourceFirmProfile/sourceFirmName"/><xsl:text>\par </xsl:text>
<xsl:value-of select="concat(//specialRequirementTags/currentUser/Contact/contactFirstName,' ', //specialRequirementTags/currentUser/Contact/contactLastName)"/>
<xsl:text>\line </xsl:text>
<xsl:call-template name="CurrentUserAddress"/>
<xsl:if test="//specialRequirementTags/currentUser/Contact/contactEmailAddress">
	<xsl:text> \par </xsl:text><xsl:value-of select="//specialRequirementTags/currentUser/Contact/contactEmailAddress"/>
</xsl:if>

<xsl:text>\cell }
{\b\f1\ul\fs20 Lender Reference Number:\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/sourceApplicationId"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmgf\clbrdrt
\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trrh230\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell \cell }
{\b\f1\ul\fs20 <!-- Lender Reference Number:-->\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><!-- <xsl:value-of select="//Deal/dealId"/>--><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh230\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell Telephone:\cell </xsl:text>
<xsl:call-template name="FormatPhone">
	<xsl:with-param name="pnum" select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>
</xsl:call-template>	
<xsl:if test="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension and string-length(//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension)!=0">
	<xsl:text> Ext. </xsl:text><xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension"/>
</xsl:if>
<xsl:text>\cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\fs20 \cell }
\pard\plain \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell Fax:\cell </xsl:text>

<xsl:call-template name="FormatPhone">
	<xsl:with-param name="pnum" select="//Deal/SourceOfBusinessProfile/Contact/contactFaxNumber"/>
</xsl:call-template>	

<xsl:text>\cell \cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\fs20 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s15\ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\i\f1\fs28 We are pleased to confirm that your application for a Mortgage Loan has been approved under the following terms and conditions:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5386 \cellx11340
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\b\fs20\ul Mortgagor(s):\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
<xsl:call-template name="BorrowersListedBlocks"/>

<!-- 
<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
<xsl:value-of select="."/>
<xsl:if test="not(position()=last())">
<xsl:text>\par </xsl:text>
</xsl:if>
</xsl:for-each>
-->

<xsl:text>\cell }{\b\f1\fs20\ul Security Address:\par }</xsl:text>

<xsl:text>{\f1\fs20 </xsl:text>
<xsl:call-template name="SubjectPropertyLines"/>
<xsl:text>\par </xsl:text>
<!--
<xsl:for-each select="//CommitmentLetter/Properties/Property">
<xsl:text>{\f1\fs20 </xsl:text>
<xsl:value-of select="./AddressLine1"/><xsl:text>\par </xsl:text>
<xsl:value-of select="./AddressLine2"/>
<xsl:if test="./AddressLine2">
<xsl:text>\par </xsl:text>
</xsl:if>
<xsl:value-of select="./City"/><xsl:text>, </xsl:text><xsl:value-of select="./Province"/><xsl:text>  </xsl:text><xsl:value-of select="./PostalCode"/><xsl:text>}</xsl:text>
<xsl:if test="not(position()=last())">
<xsl:text>\par </xsl:text>
</xsl:if>
</xsl:for-each>
-->

<xsl:text>\cell
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5386 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20\ul </xsl:text><xsl:value-of select="//specialRequirementTags/GuarantorClause"/><xsl:text>:\cell }
{\f1\fs20 </xsl:text>
<xsl:call-template name="GuarantorNames"/>

<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 LOAN TYPE:   }
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/MtgProd/mtgProdName"/><xsl:text> \tab\tab }
{\b\f1\fs22 LTV:  }{\f1\fs20 </xsl:text><xsl:call-template name="FormatLTV"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 COMMITMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/commitmentIssueDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 FIRST PAYMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/firstPaymentDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 INTEREST ADJUSTMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/interimInterestAdjustmentDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 CLOSING DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/estimatedClosingDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 MATURITY DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/maturityDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 INTEREST ADJUSTMENT AMOUNT:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/interimInterestAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22\ul LOAN\cell }
\pard\plain \s5\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel4\adjustright 
{\b\f1\fs22\ul TERMS\cell }
\pard\plain \s6\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\f1\fs22\ul INSTALLMENT - </xsl:text><xsl:value-of select="//Deal/paymentFrequency"/><xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 

\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl
\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt
\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Amount:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//specialRequirementTags/LoanAmountMinusFees"/><xsl:text> \cell }
{\b\f1\fs20 Interest Rate: \cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//specialRequirementTags/InterestRate"/><xsl:text> \cell }
{\b\f1\fs20 Principal and Interest:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/PandiPaymentAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Insurance:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//specialRequirementTags/Premium"/><xsl:text>\cell }
{\b\f1\fs20 Term:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//specialRequirementTags/PaymentTerm"/><xsl:text> \cell }
{\b\f1\fs20 Taxes (estimated):\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//specialRequirementTags/TaxPortion"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell }
{\f1\fs20 \cell }
{\b\f1\fs20 Amortization:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//specialRequirementTags/AmortizationInYrsMnth"/><xsl:text>\cell }
{\b\f1\fs20 Total Installment:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//Deal/totalPaymentAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Total Loan:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//specialRequirementTags/TotalAmount"/><xsl:text> \cell \cell \cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s18\ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\b Please be advised the solicitor acting on behalf of the transaction, will be:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Solicitor Name:\cell </xsl:text>
<xsl:call-template name="SolicitorName"/>
<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Address:\cell </xsl:text>
<xsl:call-template name="SolicitorAddress"/>
<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Phone / Fax:\cell </xsl:text>
<xsl:call-template name="SolicitorPhoneAndFax"/>
<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\pard\plain \s18\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl
\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb
\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \sa120\sb180 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text> <!-- To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than 
<xsl:value-of select="//Deal/returnDate"/>
after which time if not accepted, shall be considered null and void. \line \line -->
<xsl:text> Thank you for choosing Cervus Financial Corp.  for your financing.
\par }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\ql </xsl:text>

<!--  removed as per Derek's suggestion 29/06/04
<xsl:for-each select="//specialRequirementTags/ClosingText/Line">
<xsl:value-of select="."/><xsl:text>\line </xsl:text>
</xsl:for-each>
-->

<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s20\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
</xsl:text>
	</xsl:template>
<!-- ================================================ EnglishPage1 end  ================================================  -->

<!-- ================================================ EnglishPage2Start start ================================================  -->

<xsl:template name="EnglishPage2Start">
<xsl:text>{\sect } \linex0\headery706\footery60\endnhere\sectdefaultcl</xsl:text>
</xsl:template>

<!-- ================================================ EnglishPage2Start end  ================================================  -->

<!-- ================================================ EnglishPage2 start ================================================  -->

<xsl:template name="EnglishPage2">
<xsl:text>\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \ql \sb120\sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul TERMS AND CONDITIONS:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING:\par }
{\b\f1\fs18 The following conditions must be met, and the requested documents must be received in form and content satisfactory to </xsl:text><xsl:value-of select="//Deal/LenderProfile/lenderName"/><xsl:text> 
no later than ten (10) business days prior to the advance of the mortgage. Failure to do so will delay current closing and/or void this commitment.\par }{\f1\fs18 </xsl:text>

<xsl:for-each select="//specialRequirementTags/Conditions/Condition">
	<xsl:value-of select="position()"/><xsl:text>.  </xsl:text>
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())">
		<xsl:text>\par </xsl:text>
	</xsl:if>
</xsl:for-each>

<xsl:text> \cell } \row }
</xsl:text>
	</xsl:template>

<!-- ================================================ EnglishPage2 end  ================================================  -->

<!-- ================================================ EnglishPage3Start start ================================================  -->

<xsl:template name="EnglishPage3Start">
	<xsl:text>{\sect }\sectd \linex0\headery706\footery60\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
</xsl:template>
<!-- ================================================ EnglishPage3Start end ================================================  -->

<!-- ================================================ EnglishPage3 start ================================================  -->
<xsl:template name="EnglishPage3">
<xsl:text>\trowd\trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth11440\cellx11350
\pard\plain\s17\qj\sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\caps\f1\fs18\ul  \par }
</xsl:text>
<!-- #554 
<xsl:text>The interest rate indicated on page 1 of this Commitment Letter will be guaranteed to apply to the mortgage, provided that the mortgage funds on or before the scheduled closing date which in any event must be no more than 90 days from the date of the commitment. If the scheduled closing date is more than 90 days from the date of the commitment, the interest rate will be fixed on the 90th day prior to the scheduled closing date at the rate that Cervus Financial Corp. then posting for this product. \par
If your mortgage does not close by the scheduled closing date, your rate guarantee will expire, and at Cervus Financial Corp.’s option the interest rate applicable to the mortgage may be reset in accordance with currently prevailing rates and with reference to a revised scheduled closing date which in any event cannot be more than 90 days from the date on which the rate is reset.\par 
Once the interest rate is established and provided the mortgage funds on or before the scheduled closing date, the interest rate applicable to the mortgage will not be changed.\par
The interest rate is guaranteed only for the product and term shown. \par 
Cervus Financial Corp. will obtain a credit report at the time of instructing the solicitor. Cervus Financial Corp. reserves the right to cancel or change terms, if credit rating or financial status does not meet Cervus Financial Corp.'s lending standards.  
</xsl:text>
-->
<!--
<xsl:for-each select="//specialRequirementTags/RateGuarantee/Line">
	<xsl:value-of select="."/>
		<xsl:if test="not(position()=last())"><xsl:text>//specialRequirementTags/RateGuarantee/Line \par </xsl:text>
	</xsl:if>
</xsl:for-each>
-->
<!-- #554 <xsl:text>

{\f1\fs18\b\ul\caps Payment Policy:\par }
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>

<xsl:text>Provided the mortgage is not in default, the mortgagor(s) may prepay up to 20% of the original principal amount of the mortgage without notice, bonus or penalty in each calendar year. Each prepayment shall be made on a regular payment date and shall be not less than $100.00. In addition, provided the mortgage is not in default, the mortgagor(s) have the privilege of increasing their regular payment amount up to a maximum of 20% of the current principal and interest payment once per calendar year. \par 
These privileges are non-cumulative from year to year. \par 
Interest is calculated semi-annually not in advance.  
</xsl:text>
-->
<!--
<xsl:for-each select="//specialRequirementTags/PrivilegePayment/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())"><xsl:text> \par </xsl:text>
	</xsl:if>
</xsl:for-each>

<xsl:for-each select="//specialRequirementTags/InterestCalc/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())"><xsl:text>\par </xsl:text>
	</xsl:if>
</xsl:for-each>
-->
<xsl:text>

\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18 Closing:\par }
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
<xsl:text>This approval is subject to cancellation at the option of Cervus Financial Corp. should any of the following occur: \par 
If there has been any material change to the applicant's financial status as disclosed in the loan application or if there have been any unapproved material changes to the mortgaged premises that adversely affect the value thereof. \par 
Any misrepresentation of facts contained in this loan application or other documentation entitles Cervus Corporation to decline to advance a portion or all of the loan proceeds, or to demand immediate repayment of all monies secured by the mortgage. \par 
Cervus Corporation determines in good faith (which determination shall be final, conclusive and binding upon the Mortgagor) and notifies the Mortgagor at least seven days prior to the Closing Date that, by reason of circumstance affecting the Canadian money markets, there is no market for extendible commercial paper. \par 
I/We accept the Offer to provide mortgage financing and agree to be bound by the terms, conditions and provisions contained herein. I/We hereby authorize Cervus Corporation, it's successor and/or assigns, to make whatever credit/payoff inquiries it deems necessary in connection with my/our credit application or in the course of review or collection of any credit extended in reliance on the application. I/We hereby authorize any person or consumer reporting agency to compile and furnish any information it may have or obtain in response to such credit inquiries. I/We authorize this document to be reproduced by a copy machine to facilitate multiple credit and payoff inquiries. The Mortgagor(s) and Guarantor (if applicable) acknowledge receipt of a signed copy of the Mortgage Loan Commitment. \par 
Cervus Financial Corp. is a wholly owned subsidiary of Cervus Financial Group Inc. 
</xsl:text>
<!--
<xsl:for-each select="//specialRequirementTags/ClosingText/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())"><xsl:text> \par </xsl:text></xsl:if>
</xsl:for-each>
-->
<xsl:text>\par }
\pard \s17\qj \sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18 <!-- ACCEPTANCE:-->\par }
\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
<xsl:text><!-- This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text><xsl:value-of select="//Deal/commitmentExpirationDate"/>  <xsl:text> after which time, if not accepted, shall be considered null and void.--></xsl:text>
<!--
<xsl:for-each select="//specialRequirementTags/Acceptance/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())">
		<xsl:text>\par </xsl:text>
	</xsl:if>
</xsl:for-each>
-->
<xsl:text>\par \par }
\pard \s17\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text><xsl:value-of select="//Deal/LenderProfile/lenderName"/><xsl:text>\par \par }
\pard\plain \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 Authorized by: __________</xsl:text><xsl:call-template name="DealUnderwriter"/><xsl:text> _____________\par }
{\f1 \cell }
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text>
<!--
<xsl:for-each select="//specialRequirementTags/Signature/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())">
		<xsl:text> \par </xsl:text>
	</xsl:if>
</xsl:for-each>
-->
<xsl:text>\par }
{\f1 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 APPLICANT\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 CO-APPLICANT\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><xsl:value-of select="//specialRequirementTags/GuarantorClause"/><xsl:text>\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
<!-- \pard \ql \widctlpar\tx9644\aspalpha\aspnum\faauto\adjustright\itap0 -->
<!--\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 -->
</xsl:text></xsl:template>
<!-- ================================================ EnglishPage3 end  ================================================  -->

<xsl:template name="CervusLogo">
{\pict\wmetafile8\picw5635\pich3968\picwgoal3195\pichgoal2250 \picscalex65\picscaley65\picbmp\picbpp24 010009000003c6bb00000000a2bb00000000050000000b0200000000050000000c029600d50005000000090200000000050000000102ffffff000400000007010300a2bb0000430f2000cc0000009600d500000000009600d5000000000028000000d500000096000000010018000000000000770100000000000000000000
00000000000000f0f7f0ffffffffffffd0d7d0fffffff0f0f0f0f7f0e0e0e0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0f0f7f0f0f0f0ff
fffff0f7f0e0e7e0ffffff00f0f0f0ffffffffffffb0b0b0c0c0c0bfb8bfcfc8cfc0c0c0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0
bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bf
c0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0
c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0
bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0bfc0c0c0c0c0c0c0bfbfbfbfbfbfc0bfc0c0c0c0c0c7c0c0c7
c0c0c0c0c0c7c0cfc8cfd0d0d0ffffff00ffffffffffffb0b7b0100f100000000000001f171f0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000f080f0000000f080fb0b0b0e0e0e000ffffffffffffb0b7b0100f100000001010101f181f0000000f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f
0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f
070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f07
0f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f
0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f070f0f
070f0f070f0f070f0000000f070f0f070f0f070fcfc8cffff8ff00ffffffffffffb0b0b01f171f0000000000000f070f00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000d0d0d0ffffff00ffffffffffffb0b0b0101010000000000000707770cfcfcfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8
bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bf
bfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbf
b8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfc0b8bfbfb8bfbfb8bfbfb8bfbfb8bfb0b8c0b0b8c0b0b7c0b0b8c0bfb8bfbfb8bfbfb8bfbfb8bfc0b8bfbfb8bfbfb8bfb0b8bfb0b8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8
bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfb0b0b0
b0b7b0bfbfbfc0c0c0bfbfbfb0b7b0afa8afa0a0a00f080f000000100f10bfb8bfefe8ef00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffff0f7f0efefeff0f0f0ffffffffffffefefefffffffe0e7e0f0f0f0fffffffffffffffffffff8fff0f7f0f0f7f0ffffffe0e7e0fffffffffffffffffff0f0f0
ffffffffffffefe8effffffff0f0f0ffffffffffffefe8effffffffffffffffffff0f7f0fff8fffffffff0f7f0fffffffffffffff8fffffffffffffff0f0f0ffffffffffffffffffffffffefefeffffffff0f0f0efefeffffffffffffff0f0f0ffffffefefefffffffffffffdfd8dfffffffffffffefe8effff8ffffffffff
f8fffffffff0f7f0efe8eff0f7f0fffffff0f0f0fffffffffffff0f7f0fffffff0f7f0efefeffff8ffe0e7e0ffffffffffffefefefffffffffffffefe8effffffff0f7f0fffffffffffffffffff0f7f0f0f0f0ffffffffffffffffffe0e0e0fffffff0f7f0fffffffff8ffffffffe0e0e0efe8effff8fffff8fffff7fffff7
fffff7fffff8fffff8fffff8fffff8fffff8ffefefeffff8ffffffffeff0f0eff0f0fff8fff0f7f0fff8fffff8f0fff7fffff0fffff7fff0f7fff0f7ffe0e7ffe0e7fff0f7fff0f7ffefe7f0f0eff0fff0f0ffefeffff8ffdfe0e0f0f8fff0f8fff0f8fff0f8ffefeff0eff0fff0f7fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffffffffffffffffffffffffff0f0f0fff8fffffffffff8fffffffff0f0f0efe8eff0f7f0fffffffffffffff8fffffffff0f7f0ffffffefefefffffffefe8efffffffffffffefe8efefefeffffffffffffffffffffffffffffffffffffff0f7f0ffffffefefefffffffffffffefefeffffffff0f0f0ff
f8fffffffffff8fff0f7f0fff8fffff8fffffffff0f0f0cfc7cf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffff0f0f0fff8ffffffffffffffd0cfd0d0d0d0cfcfcfd0cfd0e0e0e0fffffffffffffff8ffefefeff0f7f0fff8ffffffffd0d7d0b0
b7b0b0b7b0efefefe0e7e0afafaffffffff0f7f0ffffffffffffefe8efffffffefe8efcfcfcfc0c7c0cfc8cffffffffffffff0f7f0ffffffefefeffffffffffffffff8fff0f0f0afa8afc0c0c0dfdfdfffffffe0e0e0ffffffffffffdfd8dff0f7f0fff8ffefe8efc0c0c0909090e0e7e0ffffffffffffc0bfc0e0e0e0ffff
fffffffff0f7f0ffffffdfdfdfbfbfbfffffffffffffd0d7d0bfb8bfffffffffffffffffffbfb8bfe0e7e0fffffff0f7f0ffffffd0d0d0fffffffffffff0f7f0ffffffe0e0e0c0c0c0b0b0b0e0e0e0ffffffffffffefe8efe0e7e0efefefffffffffffffd0d7d0b0afb0fffffff0f7f0fffffffffffffffffffff8fffff8ff
fff7fffff7fffff7fffff7fffff8fffff8fffff8fffff8ffefeff0f0f8ffe0e8efbfc0cfd0d8dff0fffff0f8fffff8f0fff8fffff7fff0effff0f7fff0f7ffc0c8efb0bfdfb0b8dfcfd0f0eff7fff0f7fffff0fffff0f0fff8f0fff7f0eff7ffeff8ffcfd0dfcfd0dff0f8ffeff7fff0f8fff0f8fff0f8fffff8fffff8ffff
f8fffff8fffff8fffff8f0fff8f0efefeffff8ffffffffffffffffffffe0e0e0cfcfcfcfcfcfe0e7e0d0d0d0dfdfdffffffffffffff0f7f0e0e7e0f0f0f0ffffffcfc8cf909790efefeffffffffffffffffffff0f0f0f0f0f0ffffffe0e0e0afafafefe8eff0f7f0fff8fffff8ffefefefffffffefefefefefefffffffffff
ffdfd8dfc0c0c0e0e0e0efefeffff8fffff8fffffffffffffff0f7f0a0a7a00000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0efefefffffffffffffffffffffffffdfdfdf8f8f8f303730000000000000100f105f585fbfb8bffffffffffffffffffffff8ffffffffefe8
ef4f4f4f2020203f373f5f5f5f1f181f5f585ff0f7f0fff8fffff8ffffffffefefefffffffdfd7df201f202f272f0000005f585ff0f0f0ffffffffffffffffffdfd8dff0f7f0ffffff8f888f000000302f30706f70e0e0e0fff8fff0f7f0ffffffefefefffffffc0c0c03f3f3f3f3f3f4f484f3f383f909090fffffffff8ff
fffffffffffffff8ffffffffe0e0e07f777f3f373fffffffffffff8f888f201f20908f90efefefd0d7d02f282f9f9f9fffffffffffffe0e7e0ffffffffffffefe8eff0f0f09f979f0f080f0f070f201f203f3f3fa0a0a0efe8eff0f7f0f0f7f0efefefefefeffff8ff404040202020d0d0d0efefefefefeffff8ffefefeff0
f0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8ffeff7ffeff0f0b0c0c0808f90b0bfc0effffff0fffffff8f0fffffff0f7f0eff0ffeff8ffbfc7df7f88a090a8cf8f9fbf7f8fafbfc7dfeff8fff0f8fffff7fffffffff0f0eff0ffffeff8ff80979f808f90dfeff0eff8ffeff7fff0fffff0f8fff0f8
fffff8fffff8fffff8fffff8fffff8f0fff8f0fff7f0fffffffffffffff8ffe0e7e06f686f0f080f303030000000000000302f30afa8afefefefefe8eff0f7f0ffffffffffff7070702f282fd0d7d0fffffffff8ffefefeffffffff0f7f0ffffffafa7af404040bfb8bfe0e7e0f0f7f0fff8fff0f0f0ffffffffffffffffff
bfb8bf1f171f0000001f171f2f2f2f9f9f9fffffffffffffefe8effffffff0f7f0afafaf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0efe8effffffff0f7f0fffffff0f7f05f5f5f0f070f2f2f2fbfb7bfafa8afa0a7a0c0c7c0f0f0f0ffffffffffffefe8efffffff
ffffffc0bfc00f070f3f373f908f909f9f9f000000202020efefeffffffff0f7f0fffffff0f7f0ffffffdfd8dfbfb8bfefefef4f4f4f0f070fb0b7b0fff8ffffffffcfcfcfffffffffffffffffff5f585f100f10c0c0c0dfdfdffffffff0f7f0d0d7d0f0f7f0ffffffefe8ef3f383f201f20bfb7bfdfdfdf2f2f2f0f080fcf
c8cfffffffffffffefefeff0f7f0fffffff0f7f0605f60000000fff8ffffffff7070700000008f888fffffffe0e0e00000008f888ff0f0f0ffffffefe8effffffffffffff0f7f0afafaf201f203f373fd0d7d0fff8ffb0b0b0f0f0f0fffffff0f7f0fff8fffffffff0f7f0efefef100f10000000efe8effffffff0f0f0ffff
fff0f0f0fffffffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8ffeff0f0f0f8ffe0f7ff9fa8b05f6f709fb0bfe0ffffeffffff0fffffffffff0ffffeff8ffd0e7f07f90a0607f90afc8e0a0c7df6f879f7f97a0d0e0efeffffff0fffff0fff0f0f8ffe0f8ffdff7ff50687050676fdfe8f0efffffe0eff0f0ffff
f0f8fff0f8fffff8fffff8fffff8fffff8fffff8f0fff8f0f0f7f0ffffffffffffffffffffffff706f70000000606060afafafa0a0a0bfbfbffffffffffffffffffff0f7f0ffffffffffff404040000000c0c7c0fffffff0f7f0efefeffffffffff8ffffffff8f888f000000b0b0b0efefeffff8fffff8fffff8ffffffffff
ffffc0c0c01f181f1f171fbfb8bff0f7f0c0c0c0f0f0f0ffffffefe8efdfd8dfffffffffffffc0c7c00000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffefefeff0f7f0b0afb0100f103f373fdfd7dffffffffffffffff8fffff8fffffffffffffff0f7f0ef
e8efffffffffffffc0c0c00f080f909790ffffffffffff4f484f100f10f0f0f0ffffffefefeffffffffff8ffffffffefe8efffffffb0b7b01f181f100f10cfcfcffff8fffffffffffffffff8fffffffff0f0f02f282f100f10fffffffffffff0f0f0f0f7f0ffffffffffffffffffafafaf100f10707070ffffffffffff908f
90000000909090efe8efefefeff0f7f0ffffffffffffffffff707770000000ffffffffffff7f777f0f070f909090ffffffdfdfdf0f070f909790e0e0e0fffffffffffffff8ffefe8efffffff8f888f0000006f686ffffffffffffff0f0f0ffffffffffffefe8efefefeffffffffffffffff8ff2f282f1f171fffffffffffff
efe8effff8fffff8fffffffffff8f0fff8f0fff8f0fff8f0fff8fffff8fff0f8fff0f8ffe0eff0efffffefffff90afb050687090b0bfe0ffffe0ffffefffffe0f7f0efffffe0ffff9fb8c05070807f9fb0d0f8ffd0f8ff8fa8bf50708090b0bfe0ffffefffffe0f0efefffffdff8ffdff7ff5f707f5f707fe0ffffefffffe0
f0f0f0fffff0f8fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0f0f0f0fffffff0f7f0fff8ffffffff8f8f8f201f20cfc8cffffffffffffffffffffffffffffffffffffffffffff0f0f0fff8ff403f40000000c0c0c0fffffff0f0f0f0f7f0fffffff0f7f0ffffff909090000000cfc8cffffffffffffff0f7f0ffff
ffe0e0e0ffffff9097900f070f606060e0e7e0efefeff0f0f0fffffffffffffffffff0f0f0fffffffff8ffc0c7c00000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0f0f0f0ffffffffffffffffff606060000000909790ffffffe0e7e0efefeffff8fffff8fff0f7f0f0f7
f0fffffffffffff0f7f0ffffffcfc8cf0000009f979fffffffffffff302f30101010ffffffffffffefe8effffffffff8fffffffff0f0f0808780000000302f30908f90ffffffdfd8dff0f7f0f0f0f0f0f7f0ffffffffffff4f484f0f080ffff8fffffffff0f0f0ffffffffffffffffffffffffb0b7b01f171f7f777ff0f0f0
ffffffa0a7a00f080f909790f0f7f0f0f7f0fffffffffffffff8ffefe8ef5f575f0f080fffffffffffff7f787f000000808080ffffffe0e0e00f070f7f7f7ff0f0f0fffffff0f7f0fffffff0f0f0ffffff7f777f000000403f40605f604f474f2f272f9f989fe0e7e0efefeff0f7f0fffffffff8fff0f0f0201f201f181fef
e8efffffffffffffffffffefefeffff8fffffff0fffff0fffff0fffff0fff8f0f0f8fff0ffffeff8ffe0f0f0e0ffffe0ffff9fb7bf5f778090b0bfdff8ffdfffffefffffdff0efe0ffffe0ffff8fafb04f6f7f8fb0c0d0ffffd0ffff9fc0d04f6f7f80a0afe0ffffe0ffffdfefe0efffffe0ffffdfffff708f90708f90dff8
ffe0ffffe0f8ffefffffeff8fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffffffffffff0f0f0dfdfdfefefef606060000000bfb7bffff8ffffffffefefefdfd8dfe0e0e0fffffffffffff0f0f0fff8ff4f484f100f10cfc7cff0f7f0f0f7f0fffffffff8fffff8ffffffff9f9f9f0f070fd0d7d0fff8ffffffff
f0f7f0ffffffe0e7e0ffffffafa8af2f282f4047405f585f504f50201f20707770e0e0e0ffffffffffffffffffefe8efbfb7bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0e0e7e0ffffffffffffffffff4f484f000000afa8aff0f0f0fff8ffffffffffffffffffff
fff8fff0f7f0fff8ffffffffefe8effff8ffc0c0c0000000a0a0a0ffffffffffff3f383f1f181fffffffffffffefe8effffffff0f0f0fff8ffefe8ef0000003f383ffffffff0f0f0fffffffff8ffffffffefefeff0f7f0fff8ffdfdfdf404040201f20f0f7f0fffffffff8ffefefefefe8eff0f7f0efefefc0c0c01010104f
474fffffffffffff808080100f10afa7aff0f0f0efefefefe8effff8fffffffffff8ff505750101010cfc8cfefefef6060600f070f8f888fffffffc0c7c01f181f807f80fffffff0f7f0d0d0d0f0f0f0fffffffff8ff7070700f080f8f878fefefefafa8af0f080f909790fff8fffffffff0f7f0fffffffff8fffff8ff2020
201f171f9f979fcfc8cffffffffffffffffffffffffffffff0fffff0fffff0fffff0fffff0f0fff0efffffefffffe0f8ffe0f8ffd0f0ff90b0bf5077808fafbfd0f8ffdff8ffe0ffffe0fff0efffffe0ffffafc8d060808f7098a0cfefffcff0ff80a7b0608790a0c7cfe0ffffefffffdff7efefffffdfffffd0f8ff70909f
60878fa0c0c0bfd0d0d0e8efefffffeff8fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fff8fffff8ffffffffefefeff0f7f07077700f080fcfcfcffffffffffffff0f7f0efefeff0f7f0fffffffff8fff0f0f0ffffff4f474f100f10cfcfcffff8fffff8fffffffff0f0f0ffffffefefef9f9f9f0f070fc0c7c0df
d8dffffffffffffff0f7f0ffffffffffff9090900f080f6f6f6ff0f7f0f0f0f0201f207f777fd0d0d0efefeff0f7f0ffffffffffffafafaf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fff8ffffffffefefefdfdfdf5050500f070fb0b0b0fffffffff8fffff8ffff
f8fff0f7f0f0f7f0fff8fffff8fffff8ffffffffffffffbfb8bf000000b0afb0fffffffff8ff3030301f181fffffffffffffefefefffffffefefefefe8efdfdfdf100f10202020afafaf7f787fffffffffffffe0e7e0fffffffffffffff8ffd0d7d04f4f4f0f080fafa8afdfd7dffffffffffffff0f7f0ffffffffffffffff
ff6067600000008f878f9f989f1f181f302f30e0e0e0fffffffffffff0f7f0fff8ffefe8eff0f7f0605f600000003030304f484f0f080f1010102f272f3f373f100f10100f10909790ffffffefefefffffffffffffefefefffffffdfd8df1010102f272fbfb8bf6f676f303030b0b7b0fffffffffffffffffffffffffff8ff
efefef2027200000000f070f2f282f9f9f9fd0d7d0fff8fffffffffffff0fffff0fffff0fffff0fffff0f0fff0efffffefffffe0ffffdff0ffc0e7ef8fa8bf5070807fa7b0cff0ffd0ffffe0f8f0efffffe0f8f0e0f7f0d0eff07f98a05070809fbfcf90b7c0607f8f809fa0d0f0f0efffffefffffefffffe0f7f0dff8ffc0
e0ef60808f4f606f607f8080989fbfd0d0dff0f0eff8fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0f0f7f0fff8fffffffffffffffff8ff7f7f7f0f080fc0c7c0fff8ffe0e7e0e0e7e0fffffffffffffffffff0f7f0ffffffffffff3f383f000000d0d0d0ffffffffffffffffffefefefffffffdfdfdf9097900000
00afa8afbfb8bfffffffffffffffffffe0e7e0fffffffff8ff2f272f1f181fafa8af707770100f10a0a7a0ffffffffffffdfdfdffffffffff8ffb0b0b00000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0f0f0f0fff8ffffffffffffffafafaf0f070f202020efefefffff
fffff8fff0f0f0efefeff0f7f0ffffffffffffffffffefefeff0f7f0d0d0d0a0a7a0bfb8bff0f7f0efe8efafafafc0c0c0dfd8dff0f7f0ffffffffffffffffffffffffffffffbfbfbf5f585f3f383f9f989ffffffffffffffff8ffefefefffffffe0e0e0bfbfbf1f171f000000807f80afafafefe8effffffff0f7f0f0f7f0
ffffffffffffe0e0e09097905f575f4f484f8f888fd0d7d0fffffffffffff0f7f0f0f7f0ffffffefe8eff0f0f0afa8af8f888fafa8af6f676f3f373f8f878fd0d7d06f686f403f40909090f0f7f0fffffffff8fff0f7f0ffffffffffffffffffefe8efc0c7c0909090605f60404040fff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8ff8f888f9f989f9f9f9f9f9f9fb0b7b0e0e0e0fffffffffffffffff0fffff0fffff0fffff0fffff0f0fff0efffffefffffe0ffffe0ffffbfd8e070909f4f6f7f6f90a0afd0e0d0f8ffeffffff0fffff0fffff0ffffefffffd0e7efafbfcf8fa0af8fa0afafbfcfd0e7efeffffff0fffff0fffff0fffff0ff
ffe0f8ffcfe8f0b0cfd0a0b8c09fb0bfafc0c0c0d7dfd0e0e0eff8fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0ffffffdfdfdffff8ffffffffffffff707770000000cfcfcffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffff9f9f9f807f80dfd8dfffffffffffffffffffffffffffffffb0b7b0
5f5f5f0000006f6f6f9f979ff0f0f0fffffff0f7f0ffffffffffffffffffdfd8df9f9f9f5f5f5f3f373fe0e0e0efefeffffffffff8fffff8fffffffffff8ffbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fff8fffffffffff8ffffffffdfdfdf5f585f0f080f
3f383fdfd8dfcfc8cfbfbfbfcfcfcfefe8effff8fff0f7f0e0e7e0fffffffffffffff8ffdfd7dfefe8efffffffffffffdfdfdfdfd8dfefe8effffffffffffffffffffff8fffff8ffffffffffffffdfdfdfcfcfcff0f0f0ffffffefefefe0e7e0fff8ffefe8efffffffffffff4f474f303030dfdfdff0f7f0e0e7e0fffffff0
f0f0efe8eff0f7f0ffffffffffffefe8efcfcfcfd0d7d0f0f0f0fffffffffffffff8ffefefeff0f7f0ffffffffffffffffffefefefefefeffffffff0f0f0dfd8dfffffffffffffdfdfdfcfc8cfefe8efffffffefefefefefeffffffff0f0f0f0f7f0fffffffffffffff8ffefefefdfdfdfd0d0d0fff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8ffefefeff0f0f0efefefdfdfdfe0e0e0f0f7f0fffffffffffffffff0fffff0fffff0fffff0fffff0f0fff0efffffefffffd0e8efe0ffffdfffff80a0af5077808fa8bfc0e8ffdfffffeff8fff0f7f0f0f7f0f0f8fff0ffffeff8ffdfe8efcfd8e0cfd8e0dfe8efeff8fff0fffff0f8fff0f7f0
f0f7f0f0f8ffefffffe0f8ffe0f7ffdfeff0d0e8efdfeff0eff7fff0fffff0f8fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0ffffffe0e7e0f0f7f0fff8fff0f7f06f686f000000b0b0b0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffffbfbfbfbfb8bfefefeff0f7f0e0e0e0efefefffffffdf
d7dfffffffafa8af0f070fbfbfbff0f0f0fffffffffffff0f7f0fff8fffffffffff8fff0f7f0efe8efe0e0e0dfd8dfe0e7e0f0f0f0fffffffff8fffff8fffffffffff8ffbfb7bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0f0f0f0fffffffff8fff0f7f0ffffffe0
e0e0707070303030000000000000100f105f575fb0b7b0fff8fffffffffffffffffffffffffffffffff0f0f0fff8fffffffffffffff0f0f0f0f7f0fffffffffffffffffffff8fff0f7f0f0f7f0f0f7f0fffffffffffffffffffffffffff8ffe0e7e0efefeffffffff0f7f0ffffffffffffb0b0b0a0a7a0ffffffffffffffff
fffffffffffffff0f7f0f0f0f0ffffffffffffffffffffffffffffffffffffffffffffffffefefefefefeff0f7f0fffffffffffff0f7f0efefeffff8fffffffffff8fff0f7f0fffffffffffffffffffffffffffffffff8ffe0e7e0f0f0f0fffffffff8fffff8fffffffffffffffffffffffffffffffffffffffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffff8fffff8fffffffffffffffffffffffff0fffff0fffff0fffff0fff8f0f0f8fff0ffffeff8ffeff8ffe0ffffe0f8ffb0cfdf7f97a06f879090b0c0d0eff0f0fffffff7f0f0f0eff0f7f0fffffffff8fff0f8fff0f8fff0f8fff0f8fffff8fffffffff0
f7f0f0f0effff7f0fffffff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffffff0f0f0ffffffefefefefe8ef8f888f303030bfbfbffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefe8ef4040402f272fbfb8bffffffffff8ffe0e7
e0ffffffefe8efffffffd0d7d00f070f303030808080efefeffffffffffffffffffff0f7f0f0f0f0f0f7f0ffffffffffffffffffefefeff0f7f0fffffffff8fff0f7f0fffffff0f7f0b0b7b00000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0efefefffffffffffffe0e7
e0fffffffffffff0f7f0cfc8cfbfb7bfb0b0b0b0b7b0d0d0d0f0f7f0fffffffffffff0f0f0f0f0f0efefeff0f0f0f0f7f0f0f7f0f0f0f0f0f0f0f0f7f0fffffffffffffffffffffffffff8fffff8fffff8fffff8ffefe8eff0f0f0f0f7f0f0f7f0fff8ffffffffffffffffffffffffffe0e0e0fffffffffffff0f0f0f0f0f0
e0e7e0fffffffffffffffffffffffffffffffff8fff0f7f0f0f0f0f0f0f0efefefefefeff0f0f0f0f0f0f0f0f0f0f7f0f0f7f0f0f7f0ffffffefefeff0f0f0fffffffff8ffefefefefefeff0f7f0efefeff0f7f0fff8fff0f7f0f0f7f0f0f7f0fff8fffff8fffffffffffffffffffff0f7f0f0f0f0f0f0f0f0f0f0f0f7f0ff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefe8eff0f0f0f0f0f0f0f7f0fff8fffffffffffffffffffffff8f0fff8f0fff8f0fff8f0fff8fffff8fff0f8fff0f8fff0ffffe0f7ffdff0ffdff0ffb0c8d07f97a090a8b0d0e7effffffffffffffff8fffff7f0fff8fffff8fffff8fffffffffff8fffff8fffff8
fffff8fffff7f0fff8fffffffffff8ffefe8f0efe8ffefeffff0f0fff0f7fff0f0ffefeff0efeff0fff8fffff8fffff8fffff8fffff8fffff8fffff8f0fff8f0fffffffffffffffffff0f7f0fff8ffcfc8cf9f979ff0f0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0f09f989f8f888fd0d0d0ffffff
fffffffffffffffffffffffff0f0f0ffffffafa8af505750706f70fff8fff0f7f0fffffffffffffff8fff0f7f0f0f0f0efefefefefefefefeff0f7f0fffffffffffffff8fff0f7f0fffffff0f0f0b0b0b00000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0f0f7f0ffffff
ffffffe0e7e0efefeffffffffffffffffffff0f7f0fff8fffffffffffffffff8ffefefefefe8efe0e7e0f0f7f0f0f7f0fff8fffffffffffffff0f7f0fff8fffffffffffffffffffffff8fffff8fffffffffffffffffffffffffffff8fffffffffffffffff8fffffffffffffffffffffff8fff0f7f0f0f7f0fffffffff8fff0
f0f0fff8ffefefeff0f7f0e0e0e0efe8eff0f7f0fff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffffffffff0f7f0efefefffffffefefeffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8fffffffffffffffff8ffefe8efffffffffffffffffffffffffffffffffffffffff
fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffff0f7f0f0f0f0f0efeffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8fff0f8fff0f8ffefffffeff8ffe0f8ffe0f7ffe0f8ffeffffffff8fffffffffffffffffffffff8f0f0f7f0f0f7f0f0f7f0f0f0f0
f0f7f0f0f7f0fff8f0fffffffffffffffffffff8fffff7fffff0fffff7fffff7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fff8f0fff8fffffffffffffffff8fffffffff0f7f0e0e0e0fffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffffffffffffffffff
ffffefefeff0f0f0fffffffffffffffffff0f0f0ffffffffffffffffffefefeffffffffffffffff8fffff8fffffffffffffffffffffffffffff8fffff8fffff8fffffffffffffffff8fff0f0f0fffffff0f0f0b0b0b00000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0f0
f7f0f0f7f0fff8fffffffffffffff0f7f0f0f0f0f0f0f0e0e0e0f0f0f0fffffffffffff0f7f0f0f0f0fff8fffffffffff8fffff8fffffffffffffffffffffffffffffffffffffffffffffff8fff0f7f0fff8fffffffffffffffffffffffffff0f7f0fffffffffffffffffffff8fff0f7f0fff8fffffffffff8ffffffffffff
fff0f7f0f0f0f0ffffffffffffefefeff0f7f0fff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffffffffffff8fffffffffffffffffffffffffffff8ffefefeff0f7f0fffffff0f7f0f0f0f0f0f7f0f0f0f0f0f0f0fffffffffffffffffff0f7f0f0f7f0fff8fffffffff0f0f0f0f7f0fff8ffffffffffffff
fffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffff0f7f0f0f0f0fff7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8ffeff0f0e0eff0f0fffff0ffffefffffeffffff0ffffe0e8eff0f7f0fffffffffffffff8fff0f7f0f0f0f0f0
f7f0f0f7f0f0f0f0f0f7f0fff8fffffffffffffff0f7f0f0e8effff7fffff7fffff7fffff7fffff7fffff7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fff8f0fff8fffff8fffffffff0f7f0f0f7f0fff8fffff8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffffefe8
efefefeff0f7f0efefeff0f7f0ffffffefefeff0f0f0fffffff0f7f0e0e7e0fff8fff0f7f0f0f0f0fffffffffffffff8fff0f7f0f0f0f0f0f7f0fffffffffffffffffffffffffffffffffffffff8fff0f0f0fffffff0f7f0bfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0
c0f0f7f0fff8ffefefeffff8fffffffffffffffffffffffffffff8fffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0fff8fff0f7f0efefeff0f0f0fff8fffff8fff0f0f0fffffffff8fff0f7f0fff8fffffffffffffffff8fff0f0f0e0e7e0efefeffff8fffffffffff8ffefefeff0f0f0ffffffffffff
fffffff0f7f0fffffffffffff0f7f0fff8fffffffffffffffffffffffffffffffffff8fffff8fffff8fff0f7f0efefeff0f0f0f0f7f0f0f7f0f0f7f0fff8fffffffffffffffffffffffffffffffffff8fffffffff0f7f0f0f0f0fff8ffefe8efefefeff0f7f0fffffff0f7f0efefeffff8fffffffffff8fffff8fff0f7f0f0
f7f0f0f7f0f0f0f0f0f0f0f0f0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0efefefefefeffff8fffffffff0f7f0fff8fffff8fffff8fffff7fffff7fffff7fffff7fffff8fffff8fffff8fffff8ffefeff0e0e8eff0fffff0f8ffdfe8efdfe0e0f0f7ffefefeff0f8f0fffffffffffffff8f0f0f7
f0fff8f0fffffffffffffff8f0f0f7f0fff8f0fffffffffffff0f8f0f0e8effff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff7fffff8fffff8fffff8fffff8fffff8fffffff0fffff0fffffff0f7f0f0f7f0fff8fff0f0f0f0f7f0fffffff0f0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
f0f7f0fffffffffffff0f7f0dfdfdfffffffffffffdfdfdfefefefffffffffffffffffffefe8eff0f0f0f0f0f0efe8effffffffffffff0f7f0f0f0f0f0f0f0f0f0f0f0f7f0fff8fffffffffffffffffffff0f7f0f0f0f0fffffffff8ffbfbfbf0000000f070f000000bfbfbffff8ff00fff8fffff8ffc0c0c000000000070f
000000bfc0c0f0f7f0fffffffffffffffffffff7f0efe8eff0efeffffffffffffffffffff0fff0efefefefefeff0f7f0fffffffff8fff0f0f0fffffffffffffff8ffefe8e0f0f0effffffffffff0efefeffffffffff8fffff7f0fff8ffffffffffffffeff0f0dfe7e0ffffffeff0f0eff0effffffffffffffffffff0f7f0ff
f8fff0f7f0f0f7f0efefeff0f7f0fff8fff0f7fffff8fffff8ffefe8f0efe8f0f0f0f0fff8fffff8fffff8fff0f0f0efe0e0fff0f0fffffffffffffffffffffffff0f7f0efefefefefeff0f0effffffff0f0f0efefeffffffffffffffff8fffffffffffffffff8fffff7f0fffffffffffff0f8f0f0f7f0ffffffffffffffff
fffff8fff0f7f0f0f7f0fff8fffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffff8fffffffffffffff0f7f0fff8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefe8effff8fffff8fff0f0f0f0f0f0ffffffffffffffffffffffffffffffffffffffffff
f0f7f0eff0eff0f8f0fffffffffffff0f8f0eff0f0f0f8f0fffffffffffffffffffff8fffff7fffff7fffff7fffff7fffff7fffff8fffff0ffffeff0fff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0fff0fffffff0f7f0f0f7f0fffffffff8fffffffffffffff0f7f0fff8fffff8f0fff8f0fff8f0fff8fffff8ffff
f8fffff8fffff8ffefefeff0f7f0fffffffffffffff8ffffffffffffffffffffffffffe0e0dfefefe0fffffffffffffffffffffffffff8f0fffffffffffffffffffffffffffffffffffffff8fffff8fffffffffffffff0f7f0f0f0f0ffffffffffffc0c0c00000000f070f000000bfbfbffff8ff00fff7fffff8ffcfbfc000
000000070f000000b0c7c0eff8f0fffffffffffffffffffffffffffffffffffffffffffffff0eff7efefffeff0fff0f0fff0f0fffffffffffffffffff8fffffffffffffffffff0fffff0fffff0ffffefffffeffffff0fff8f0fff8fffff8fffff8fffffffffffffff0fffff0fffff0fffff0fff0f0fff0f0fff0fffff0ffff
f0fffff0fffff0fffffffffffffffffffffffff0f8ffeff0fff0f7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff8fffff8fffffffffffffffffff0fffff0fff8effffff0fffff0fffff0fff8f0fff8fffff8fffff8fffff8fffff7f0fff8f0fffff0fffff0fffff0fffff0fffff0fffff0
eff0eff0f0f0f0f7f0fff8fffffffffffffff0f7f0eff0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0fffff0fff0f0f7effff0effff0effff7effff7effff7effff8f0fff7efffefeffff0effffff0fffff0fffffffff8f0fff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffff0f0f8f0f0f8f0f0f8f0fff7f0fff8fffff8fffff8fffff8fffff8fff0f7f0eff7f0eff7effffff0eff0e0fffff0fffffff0efeff0f7f0ffffffeff0efeff7eff0f8f0fffff0fffff0fff7
f0fff0f0fff7fffff7fffff7fff0f0f0f0f7f0fffffffffffffffffffffffffffff0fffff0fffff0fffff0fffff0f0fff0f0fff0f0ffeff0fff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fff8fffff8fffff7f0fff8fffffffffff8ffffffffefeff0bfb8bf0000000f070f000000bfbfbffff8ff00fff7fffff8
ffcfbfc000000000070f000000b0c7c0eff8f0fffffffffffffffffffffffffffffffffff0fffffffffff0eff7e0eff8eff0fff0f0fff0f0f8f0f0f8f0fffffffffffff0f7f0fff7f0fff8f0fffff0fffff0ffffefffffefffffeffff8f0fff8fffff8fffff8fffffffff0fffff0fffff0ffffeffff0effff0f0fff0f0fff0
f0fff0fffff0fffff0fffff0fff7effffff0fffffffffffff0f0f0eff0fff0f8fff0f8fff0f7fffff7fffff7fffff7fffff7fffff7fffff7fffff8ffffeff0fff7fffff8fffffffffffff0fff8f0fffff0fffff0fffff0fffff0fffff0fffff0fff8fffff8fffff7fffff7ffffe7e0fff7f0fffff0f0f0efefefe0eff7e0e0
f7efe0efe0f0f8f0f0f7f0fff7fffff8fffff8fffff8fff0f8f0f0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ffeffff0f0fff0fffff0fffff0fffff0fffff0fffff0fffff0fff8effff7effff0effff7effff8f0fffff0fffff0fffff0fff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffff0fffff0fffff0f0fff0f0fff0f0fffffffffffffffffff0f0fff7f0fff7fffff7fffff8fffff8fff0fffff0fffffffff0eff0e0fffff0fffffffff8fffff8fffffffff0fffff0fff0fffff0fffff0
fffff0fff7f0fff7fffff7fffff7fffff8fff0f8fff0f7f0f0f8fffff8fffffffffffffffff8f0fff8f0fffff0fffff0fffff0fffff0fffff0ffffeffffff0fffff0fffff0fff8effff7effff8f0fff8f0fff7f0fff0f0fff8fffff7f0fff8fffff8fffff8fffff8ffefeff0bfb8bf0000000f070f000000bfbfbffff8ff00
fff7fffff8ffcfbfc0000000000700000000b0c7c0eff8f0fffffffffffffffffffff8fffffffffffffffffffffffffffffffff0fffff0fffff0f8ffefeff0e0e8f0eff0fffff8fff0f0fff0f0fff0f7fffff7f0fff8f0fff8f0fffff0fffff0fff8fffff8fffff8fffffffffffffff0fffff0ffffefffffeffff0effff0f0
fff0f0fff0f0fff0fffff0fff8f0fff8f0ffffeffffff0fffff0fffff0fff7f0f0f0f0f0f7fff0f8fff0f8fff0f8fff0f8fffff8fffff8fffff8fffff8fffff8ffeff0fff0f7fff0f8fffff8fff0f8f0f0f0effffff0fffff0fffff0fffff0fffff0fff8f0fff8fffff8fffff8fffff7ffffeff0fff8fffff8ffffffffeff8
f0eff8f0effff0eff8f0f0fffff0fffffff8fffff8fffff8fff0f8fffff8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0fffff0fff0fffffffffffffff8f0fffffffffffffff8f0fff7f0fff8fffff8fffff8fffffffffff8f0fffff0
fffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffff8fffff7fffff0fffff0fffff0fffff0fffff0fff0f7fff0f8fff0f8f0f0f7effffffffff8ffefeff0f0f7fff0f8ffdfe8eff0fffff0
fff0f0fff0f0f8effff7f0fff8fffff7fffff0fffff8ffffffffffffffeff0f0efefeff0f7f0fff8fffff8fff0f0f0fff7f0fff7f0fff7f0fff7f0fff8f0fff8f0fff8fff0ffffeff8f0eff0eff0f7f0fffffffffffffff8fffff7fffff8fffff7f0fff8fffff8fffff8fffff8ffefeff0bfb8bf0000000f070f000000bfbf
bffff8ff00fff7fffff8ffc0bfc0000000000700000000b0c7c0eff8f0fffffffffffffffffffff8fffff8fffffffffffffffffffff0f7fff0f8fff0f8fff0f8ffeff7ffe0efffeff7ffeff7ffeff7fff0f7fff0f8fff0f8fff0f8fff0f8fff0fffffff8fffff8fffff8fffff8fffffffffffffff0fffff0f8f0eff7f0f0ff
f0f0fff0f0fff0f0fffff0f8fffff8fffff8fffff8f0ffefe0fff7effffff0fffff0fffffff0fffff0ffffefffffefffffe0ffffefffffefffffeffffff0fffff0fffff0ffffe0f7ffe0f7ffeff8fff0f8fff0ffffefefeffff8f0fffffffffff0fff8f0fff8f0fff8f0fff8fffff8fffff8fffff7ffffe8f0fff7fffff8ff
f0ffffe0f7ffdff8ffe0f8ffe0ffffeffffff0fffffff8fffff8fff0f7f0f0f7f0fff8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0fff0f0fffff0fffff0ffffeff0f0eff7ffeff7ffe0eff0f0f8fff0f8fffff8fffff8fffff8ffff
f8fffff8f0fffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff0f0fff7fffff8fffff8fffff8fff0f8fff0f8fff0f8fffff7fffff7fffff7fffff7fffff0fff0f0fff0f0fff0f0ffefeff0f0f7f0fff8ffeff0ffe0efffeff8ffeff8ffdfef
f0efffffeffffff0fff0eff7eff0f8f0fff8fffff8fff0eff0fff8ffffffffffffffeff0f0e0efeff0f8fff0fffff0fffff0f8fff0f8fffff8fffff8fffff7fffff7fffff7fffff7ffefffffe0ffffeff8ffeffffff0f8fff0f8fffff8ffefe8f0fff8fff0f0fffff8fffff8fffff8fffff8ffefeff0bfb8bf0000000f070f
000000bfbfbffff8ff00fff7fffff8ffc0bfc0000000000700000000b0c7bfeff8f0fffffffffffffff8fffff8fffff8fffff8fffffffff0ffffdfe8f0e0f7ffe0f8ffe0f8ffdff7ffd0efffd0efffd0f0ffc0e0ffcfe0ffcfe7ffd0e8ffd0f0ffdff7ffe0f8ffeff8ffeff7fff0f8fff0fffff0fffff0fffff0ffffeff8f0
eff7f0f0fff0f0fff0f0fffff0f8fffff8fffff8fffff8fffff8fff0e8e0ffefeffff8fffff8fff0ffffefffffdff8ffcff0ffc0efffc0efffc0efffc0efffc0e8ffcfe8ffcfeff0cfe8ffcfefffcfe8ffdff7ffe0f8ffeff8ffeff0f0f0f7f0fffffffff8f0fff8f0fff8f0fff8fffff8fffff8fffff8fffff7fffff7fff0
f7ffeff7ffe0f8ffdfffffd0ffffdfffffdfffffdff0ffeff8fff0f8fffff8fff0f7f0f0f0f0fffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefefeff0fffff0ffffe0f8ffdff0ffe0f8ffdff8ffdff8ffd0f0ffdff7ffe0f8ffeff8
fff0f8fffff8fffff8fffff7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0e0efffe7fffff0fff0f8ffeff8ffe0f8ffd0efffd0e8ffd0e7ffdfe7ffdfe7ffe0e7ffe0efffeff0ffe0f0ffeff7ffe0effff0f8fff0f8ffe0e8ffe0f0ffe0f7ff
e0f8ffe0f8ffcfe8efe0ffffefffffe0f8f0eff7f0fffffffff8fff0eff0fff7fffff8fffff8ffeff7ffeff7ffefffffe0ffffe0f8ffe0f8ffe0f8ffe0f8ffe0f0ffe0e8ffdfe0ffdfd8ffd0d8f0dff7ffd0f7ffdff8ffdff8ffe0f8ffe0f8ffe0f0ffdfe0eff0f8fff0f0fffff8fffff8fffff8ffffffffefeff0bfbfbf00
00000f070f000000bfbfbffff8ff00fff7fffff8ffbfc0c0000000000700000000bfc7bff0f8f0fffffffff8fffff8fffff8fffff8fffff8fff0f8ffeff8ffe0f8ffdff8ffcfefffb0dfffa0c8ef8fb7d070a0c06f98bf5087af5f88b06090b06f9fbf7fa7cf80b0d08fb7df9fbfd0dfefffeff0f0f0f8fff0fffff0fffff0
fffff0f8f0eff7f0f0fff0f0f8fff0f8fff0f8fff0f8fff0f8fffff7fffff7fffff8fff0f8ffeff7ffd0e7f0bfd7e09fc0d07fafc0609fb05f9fb05f9fb05f98b05f98b06098b06098b06098b06098b09fc8e090bfdfafcfe0d0f7ffe0f8ffeff7ffeff0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7
fff0f7ffd0dff0afc7dfa0c7e0a0d0efa0d7f0a0d7efafd7efc0dff0dfeffff0f8fffff8fff0f7fff0f0f0fff8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefefeff0f8ffefffffbfd7e09fbfd09fc8dfa0d0efa0cfef9fc7e0a0c7e0
afcfe0c0e0f0e0f7fffff8fffff8fffff0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff7fffff0ffeff0ffcfe0f0a0c8e080b0cf6fa0b06097af5f88a0608faf7097b080a0c09fb7dfafcff0bfe0ffd0efffe0f7ffeff8ffdfefffafc0e0a0
bfe0afc8efa0c8e0a0c7dfa0c0d0cfeff0e0ffffeffff0eff7f0f0fffffff8fff0eff0fff0f0fff8fffff8fff0f7ffefffffe0ffffd0f8ffafd0e08fb8cf80b7cf80afc07fa7c07f9fbf7097b0708fb06f8fb0709fbf7fa8c090bfdfafd0efbfe7ffd0f7ffdff8ffe0f8ffeff8ffeff0fffff8fffff8fffff8ffffffffeff0
efbfbfbf0000000f070f000000bfbfbffff8ff00fff7fffff8ffbfc0c0000000000700000000bfc7bff0f8f0fffffffff8fffff8fffff8fffff8fff0f8fff0f8ffe0f8ffcff8ffa0e0ff7fb7df5f98bf4087af3078a020678f0f5780105880105f8010608f1f6790206f902f709f30779f40789fcfe7f0dfeff0eff8f0f0ff
fff0fffff0fffff0fffff0f8f0f0fff0f0f8fff0f8fff0f8fff0f8fff0f7fff0f7ffeff7ffe0f7ffd0f0ffa0c7ef7fa0c05f88af3f789f206890105f8010678f10678f10678f10678f10608f1f608f1f608f20608f508fb04f7f9f6f98b0b0d7efe0f8ffeff8ffeff0fffff8fffff7fffff8fffff7fffff8fffff8fffff8ff
fff8fff0f7ffd0d8f08fa0c04f6f903f688f4078a03f789f3f70903f6880afc8e0cfe0f0f0f8fffff8fffff8fff0f0f0f0f8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7fff0f8ffd0e8ff7fa7bf4078903f789f407fa03f779f4f
87b04f80af5f8fb08fb7d0cfe7fff0f8fffff7ffffeff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ffeff7ffdff7ffafd0f070a8c04088a020708f0f67800f60800f5f7f105f801f5f802f678f3f709f4f87b05f97c06fa0c0cfefffdff7ffa0c7
ef5f80af4f77a04f7faf40709f4070907098afb0d8e0e0ffffefffffeff7f0f0fffffff8fffff7ffffeff0fff8fffff8ffeff7ffe0ffffdfffffa0d7e05f98af30788f2f778f207090206f901f68901f678f1f608f1f5f8f2058803067904078a04f88af6098bf8fb8dfc0e8ffdff8ffe0f8ffe0f0fff0f8fffff8fffff8ff
ffffffeff0efbfbfbf0000000f070f000000bfbfbffff8ff00fff7fffff8ffbfc0c0000000000700000000bfc7bff0f8f0fffffffff8fffff8fffff8fffff8fff0f8ffeff8ffdff8ff7fb8df4f90bf1f678f0f577f0f5f801f68901f6f9f1067902078a02078a02077a01f70a01f6f9f10689f10679020688fbfe0f0dfe8ef
e0f7f0f0fffff0fffff0fffffffffffffff0fffff0fff8fff0f8fff0f8fff0f7ffeff7ffeff7ffe0f7ffafd7ff7fb0df4f80af20608f10588f106090106f9f1f70a01070a01070a01f70a01f70a01f6fa0206fa0206fa02f6f9f1f5f80104f703f6f8f9fc0dfdff8ffeff8ffeff0fffff7fffff7fffff7fffff7fffff8ffff
f7fffff8fffff8fff0f7ffe0f7ff90afd04068902f608f2f6f9f2f6f9f1f5f8f1f507090bfdfc0d8eff0f8fffff8fffff8fff0f0f0f0f7fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0efffeff8ffbfdff06097af2060801f608f2068
9f1f609010578f104f8020608f6097bfb0d7f0e0f7fffff7ffffeffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffeff8ffbfdfff8fbfe05097bf2070900060800067800070900f7f9f1f80a01f789f106f9010608f0f5f8f105f8f10679020689090c7e0
afd7f06fa0c020588f20588f2f6f9f2f68903f709050809fafcfdfe0ffffefffffeff7f0fff8fffff8fffff7ffffeff0fff8fffff8ffefeff0e0f8ffcff7ff7fb7c01f677f20778f1f778f10779010779010779f10779f1f70a02070a020689f2f6f9f20679f1058800f507f2f68906f9fbfa0c8e0dff8ffe0f0fff0f8ffff
f8fffff8ffffffffeff0efbfbfbf0000000f070f000000bfbfbffff8ff00fff7fffff8ffbfbfc0000000000700000000c0c0bffff7f0fffffffff8fffff0fff0effff0f8ffeff8ffcfe7ff90b8d01f608f10689020709f2077a02077a01f709f1f6f9f106f9f106f9f106f9f1f709f1f709f1f70a01f70a01f70a02f709f9f
bfcfefffffefffffe0f7f0eff7f0fffffffffffffffffff0f7f0fff8f0eff0f0efeffff0f8ffeff7ffe0f7ffb0cfef3f78a02f77af2f77a02f78af2f7faf2078af106f9f0f67900f67900f67901067901f67901f679f1f609020609020609030779f205f803f6f8f8fb7cfdff8ffeff8fff0f8fffff0fffff7fffff7fffff7
fffff8fffff7fffff8fffff8fff0f7ffdfefff8fafd02f5f8f2f6f9f2f78af105f9020689f2f679090bfdfdff0fff0f8fff0f7fffff8fffff8fff0f7fff0fffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7ffe0efff7098b040809f206f90
0f588f2f7fb02068a02f77b0004f80105f8f2f689080b7dfdff7fffff7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffeff8ffb0e0ff2f709f1f6f901f80a000779f006f9000678f0080a00070900070900f70901070901f6f901f6f901f6f9020
6f902f6f907fb7d06fafd02f77a02070a02070a01f689f30709f3f7090afcfe0e0f8fff0f8fffff8fffff8fffff8fffff8fffff7fffff0f0f0e8efeff0ffe0ffffd0ffff80b8cf2070803088a020879f1f789f0f70900f68900067900f67900f60901f68a0105f9f2070af3080b01f6890105f803077905080a0c0e7ffe0f8
ffeff7fffff7fffff8fffffffffffff0c0c7c00000000f070f000000bfbfbffff8ff00fff7fffff8ffbfbfc0000000000700000000c0c7bffff8f0fff8f0fff8fffff8fffff8ffeff8ffdff0ffa0c7df6097b02f779f2077a02070a01f6f9f10679010608f1f6790206f9f1f5f8f1f608f206890206f90206790105f8f0f57
80104f708fa8bfdfeff0efffffe0f7f0f0f8fffffffffffff0fff8f0fffffffffffff0f7ffeff7ffeff8ffe0f7ffc0d7f080a8cf2f6f9f106f9f10689f1067900f60901067901068901f6f902f789f307fa04087af4f88b05088b05087af4f78a040779f4f7fa03067803f68807fa0bfcfe7ffeff8fff0f8fffff0fffff7ff
fff8fffff7fffff8fffff8fffff8fffff8fff0f7ffdff0ff8fb0d02f5f8f2068a02077b00f5f9f1f68a020609090bfdfd0f0fff0f8fff0f7fffff8fffff8fff0f7fff0fffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f7ffcfe0ff5f8faf30
789f1f709f005f8f2078af1f67a02070af10609f1067901f68906fa7cfcfeffff0f0fffff0fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffe0f8ff8fc0e01f67901068901078a000779f00779f006f900080a000789f0f779f106f901068901f6890206f
902070902f7790206780408faf3f90bf1f70a00f68a0106fa0106f9f206f9f3f7090afcfe0eff8fff0f8fffff7fffff8fffff7fffff8fffff8fffff8fffff7fff0f8ffe0ffffcfeff070a8b020677010586f10607f20708f3080a03f88af4088b03f87b03f7fb02f70af0f589f1067a02078b0106f9f1067902f70903f7790
a0c8e0e0f8fff0f8fffff7fffff8fffffffffffff0bfc0bf0000000f070f000000bfbfbffff8ff00fff7fffff8ffc0bfc00000000f0700000000c0c0bffff7f0fff8f0fff8fffff8fff0f8ffe0f8ffb0d7ef6f98bf306f903078a02078a0206f9f1f5f8f1f58802f678f4f78a0608fb07fa8cf80afd08fb7d08fb8df80afd0
6fa0c05f8fb05f87a080a7b0cfe0e0efffffeff7f0fffffffffffffff8f0f0f7effffffffffffff0f8ffeff8ffe0f8ffd0e7ff90afd0507fa0206f9010709f1068900f60800f587f20678040809f609fb0a0d7efb0dff0c0e8ffd0f0ffd0f0ffd0e8ffc0d8f0b0d0ef8fb0cf6f97b06088a08fa8bfc0d8efeff7fff0f8ffff
f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7ffe0f7ff90b8df2f608f20689f1f77b0005f9f1068a01f60908fbfdfd0f0fff0f8fff0f7fffff8fffff8fff0f7fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ffeff7ffb0cf
f0407f9f1f709010709f0f679f2070af1f609f1f67a02f77b010679f1068904088b090bfe0e0f0fff0f0fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffe0f8ff6097bf0f589010689f1070a000709f0f7fa0007090107fa00060802070904087af5f97b0
5f97b04f87a03f708f20607f20688010608010689010709f00679f006fa01070a01f68903f779fafcfe0eff8fff0f8fffff7fffff8fffff0f0fff7fffff7fffff8fffff8fff0f8ffeff8ffc0e0ef8fa8b050788f70a0b080b7cfa0d0efc0efffd0f7ffd0f7ffcfe8ffbfe0ff5090cf1067a0005f9f0f70a00f70a010709f20
70902060807fa0bfdff7fff0f8fffff7fffff7f0fffffffffff0b0b7af0000000f070f000000bfbfbffff8ff00fff7fffff8ffc0bfc00000000f07000f0000cfc0bffff7f0fff8f0fff8fffff8ffeff8ffd0efff8fb7d0407f9f1f5f8020709f1f6f9f1f608f1f5f8030678f5f87af90b0d0bfd0efe0f7ffe0f7ffe0f7ffe0
f8ffdff8ffd0f7ffc0e8ffc0e0f0a0bfcfdfeff0efffffeff7f0fffffffffff0fff7effff7effffffffff8fff0f8ffeff8ffe0f8ffb0d0ef6f8fb02f608f20779f207fa020789f20708f2f708f508fa08fbfcfc0e7f0dff8ffe0ffffeffffff0fffff0f8fff0f8fff0f8ffeff7ffdff8ffc0e0f0b0cfe0c0d7e0dfeff0f0ff
fffff8fff0f0f0fff8f0fff8f0fff8f0fff8fffff8fffff8fffff8fff0f8ffe0f8ff9fc0df3068901f689f1f70af0060a0106faf1f60908fbfdfd0f0fff0f8fffff7f0fff8fffff8fff0f7fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ff
e0f7ff90b0df2f6f900f688f10779f0f6f9f1f6fa02f6faf1f5f9f3080bf0f67900f689020779f5f90bfdfe8fff0f0fff0f7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffe0f8ff4078a00f58901f6fa0106f9f0f6f9f1f7fa00f689020789f60afcf80c0e0b0
e0ffd0f8ffdff8ffc0e0f0a0c0d080afc0508fa00f5870005f7f1077a000689f006fa01077af1067903f779fafcfefeff8fff0f8fffff7fffff7fffff0f0fff7f0f0e8effff7f0fff8fff0ffffeff8ffdfe8f0bfd0dfafbfcfd0e8f0dff0ffe0f8ffeff7ffeff7fff0f7fff0f0ffdfefff8fc7ff2f80c0005f9f00689f0070
a01078a01f77901f5f7f5f889fd0effff0f8fffff7fffff0f0fffffffffff0afafa00000000f070f000000bfbfbffff8ff00fff0fffff7ffcfbfc00f00001007000f0000cfc0bffff7effff8f0fff0f0f0f7ffeff8ffbfd8f06f98bf2f70901f68901f779f10689010608f2f6f8f5f88af9fb7d0d0e7fff0f7fffff7fffff8
fffff8fffff8fff0f8ffeff8ffeff8ffe0f8ffd0e8f0effffff0ffffeff0f0fff8f0fffff0fff8effffff0ffffffefefefeff0ffe0f8ffdff8ff90b8df4f78a0205f8f20779f1f779f20779f30789f5f90af90b8cfd0efffeffffff0f7fff0f7f0fff7f0fff7f0fff7effff7effff7f0f0f8f0eff8ffeff8ffeff8ffeff8ff
f0fffff0fffffffffff0f0effffff0fff8f0fff8f0fff8f0fff8fffff8fffff8fff0f7ffeff8ffafcfdf3f709020689f106faf0060a01f70af105f908fbfdfd0f0fff0f8fffff7f0fff8fffff8fff0f0fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fff0f7ffd0e8ff6f97bf1f678f0068900f779f0f709f206fa0508fc02f679f3f7fb0005f8f0f68901068903f77a0bfd7ffeff0fff0f7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffe0f8ff3f70a0105f9f2077af10689f106f9f20779f1f678f4087a0cff8
ffd0f8ffe0f8ffeff8fff0f8fff0f8fff0f8ffe0ffff90c0cf30778f1067801f7fa000709f006f9f1077af0f6790307090a0cfe0e0f8fff0f7fffff7fffff8fffff7f0fff7f0f0f0f0f0f8fffffffff0fffff0f8ffeff0f0eff7fff0fffff0f8fff0f7fff0f0fff0eff0f0e8f0f0e7f0f0e7f0dfe0ffb0e8ff4097d000679f
00679f00709f0f7fa01f789f1f677f5f889fd0effff0f8fffff7fffff0f0fffff0fffff0afafa00000000f070f000000bfbfbffff8ff00fff0fffff7ffcfbfc00f00001000000f0000d0c0bffff7effff8fff0eff0f0f0ffe0f8ffafd0ef4f88af1f68902078a01f7fa00f7090106f8f3f809f80b0cfc0dff0eff8fffff8ff
fff8fffff7f0fff0f0ffefeff0f0eff0f8f0f0ffffefffffefffffeffffff0ffffeff0f0fff8f0fff8f0fff7effffff0fff8f0efe7efe0f0ffe0f8ffcfe8ff709fc03f6f9020689010689f00609010608f3f7fa07fa8cfbfd8f0eff8fff0f8fffff8fffff8fffffff0fff8effff8effffff0fffff0fffff0f0f8fff0f8fff0
f8fff0f8fffffffffffffffffff0f0f7effffff0fffff0fffff0fff8f0fff8fffff8fffff8fff0f8ffefffffb0d7e04078901f6890106faf0067a01f77af105f8f8fbfdfdff0fff0f8fffff7f0fff8f0fff8fff0f0fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8ffeff7ffafc7ef4077a01068900f709000779f0f6f9f3077a080b8ef5088bf3077a0005f8f0f709f106f902068908fafdfe0f0fff0f7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffe0f8ff3f78a01068a01f70af10689f1f709f206f9f20688f
70a8c0cfefffdfefffefeffff0effff0eff0f0eff0f0f0f0e0f0f0cfeff06fa8bf307f9f1f789f0f709f0070a00f70a0106f902f6f909fc8e0e0f8ffeff7fff0f7fffff8fff0f8f0f0f8fffffffffffffffffffffffffff0f8fff0f0f0fffffffffffffffffffffffffffffffff8f0fff8f0fff8fffff8fff0f8ffa0dfff40
90cf0f689f006f9f00709f0f789f1f7f9f206f8070a7b0dff7fff0f8fffff7fffff7f0fffff0fffff0b0b7af0000000f070f000000bfbfbffff8ff00fff0fffff7ffcfb8c00f0000100000100000d0c0bffff7effff8fffff0f0f0f8ffe0f8ffa0d0ef3f7fa00f5f801078a00f7f9f006f8f0f6f8f4097af90d0e0dfffffef
fffffff7f0fffffffffff0fffff0fffff0fffff0fffff0f0fff0efffffefffffeff8fff0f8fff0f0f0fffffffffff0fff0effffff0ffffffefefefeff7ffe0f8ffb0d7f05080af20608f2f70a0106faf0058900f58904080b090c7f0dff7ffeff7ffefeffffff0fffff0f0fff0f0fff0effff0effff7effff8effff8f0f0e8
effff7fffff8fff0f0f0f0f0effffff0fffff0f0f8effffff0fffff0fffff0fffff0fff8fffff8fffff7fffff8ffefffffbfdfe04f7f901f68900f68a00067a01f77b0105f8f8fbfdfdff0fff0fffffff7f0fff8f0fff8fff0f0fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8ffeff7ff8fa7cf2f60901f709f1078a00070900f68904080afbfe8ff8fbfef30709f0f678f1078a010709f10608f5f80b0d0e0fff0f7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffe0f8ff3f80af0f679f0f6fa00f689f1f77a020
6f90307790a0d7f0d0efffeff7fffff8fffff8fffff8fffffff0fff8f0f0f7efe0ffffa0d0e04f8fa010678f0f709f0f77a00f6f9f2078a020688f90c0dfdff8ffe0f7ffeff7fff0ffffeffffff0fffff0fffffff8ffffffffffffffffffffefefeff0f0effffffff0f8f0fffff0fffff0fffff0fffff0fffff0ffffefdff8
ff6fa0cf2070af0067900f77a000779f0078901f7f9f20778f9fcfdfe0f8fffff8fffff7fffffffffffff0fffff0bfc0b00000000f070f000000bfbfbffff8ff00fff0fffff7ffcfbfc00f0000100000100000d0c0bffff7f0fff8fffff7fff0f8ffdff8ffa0d8f030779f0057800f709f006f90005f800060804f97b0afe0
f0dffffff0ffffefefeffff7effff8effffff0fffff0fffff0f0ffefeff7efe0f0efdfe8f0eff7ffeff0f0f0f0f0ffffffffffffffefe0fff7f0fff8fff0f7fff0f8ffe0f8ffa0cfef3f70901058802f78af2080bf0f679f10609f5097c0b0e0ffd0f7ffe0f7ffdfd8effff0fffff7fffff8fffff8fffff8fffffffff0f8ff
f0f7f0f0f8fffff8fffff8ffeff0f0e0e8e0f0f8effffff0f0ffeffffff0fffff0fffff0fff8f0fff8fffff8fffff7fff0f8ffefffffbfdfef4f809f1f67901068a00f68a01f77af105f8f8fbfdfd0f0fff0fffffff7f0fff8f0fff8fff0f7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fff0f8fff0f8ffe0f8ff6f90bf1057801f77a0107faf0f6f9010678f5087afcff7ffbfe7ff3f70901f6890207faf1f77a00f5f8f2f5f90b0d0f0eff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8fff0f8fffff8fffff8fffff8ffe0f8ff3f87af00609f0067900f68
9f1f78a01f70903f80a0cff8ffdff8fff0f8fff0f0f0ffe8efffefe0fff7effffff0ffffffefffffc0e8f05090a00f58700f6f900f77a0006f9f2080af20678f90c0dfdff7ffe0f0ffeff7f0f0fffff0fffff0ffffefeff0efe8eff0f0f0fffffffffffff0f0efefefe0fff8eff0f7effffff0f0fff0f0fff0efffffd0f0e0
b0d7d090c0cf206f90005080005f900f78a00f789f0f779020789030778fbfe8f0e0fffff0f7fffff8fffffffffffff0fffff0c0c8bf0000000f070f000000bfbfbffff8ff00fff7fffff8ffc0bfc00f00001007000f0000d0c0bffff7f0fff8fffff8fff0f7ffdff7ff9fd0f01f608f1070a00f70af1070af0f67a01f6090
6098c0cfe8ffeff8fff0f8fffffffffffff0fffff0fffff0fffff0fff8fffff8fffff7fff0f7fff0f8fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefefffe0efffb0d7f04f88af0f608010779f207fa0106890105f805090b0b0e7ffd0f8ffdff7ffeff8ffeff7fff0f8fffff8fff0f8fff0f8fff0
f8ffeff8ffeff8ffdff7ffdff8ffdff0ffe0ffffefffffeff7f0f0ffffdfe8dffffff0fffff0fff8f0fff8fffff8fffff7fffff7fff0f7ffe0f8ffbfe0ff40789f105f8f1f70af106fa01f70a0105f8f7fafcfd0f0ffeff8fff0efeffffff0fffff0efe7dffffffffff8fffff7fffff8fff0f8fff0f8fff0fffff0f8fffff8
fffff8ffffeff0fff8fffff8ffefe8f0eff7ffeff8ffb0d7ef3f789f0f608f1070a01f7fb00f5f8f2f709090c0dfdff8ffdff8ff90bfdf2f68900057801f78af1f77af1f68904f87a0dff0fffff8fff0eff0f0eff0fff8ffffffffefefeffffffff0fff0effff0effff0effffff0f8fff0f8fffff8ffe0f8ff3f80a00f6790
0f709f0f77a00f709f0f678f2f7790a0e0f0dff8ffeff8fffff8fffff8f0fff8f0fff8f0fff8f0f0f8ffefffffd0f8ff90c8df1f678010709f0068901078af0f6f9f1f678f6fa0bfdff8ffeffffff0f7f0fffffff0f0effffffffff7fffff7fffff7fffff8fffff8f0fff8f0fffff0fffff0fffff0eff7f0efffffdfffffd0
ffff8fc0df4f87a010608f1f80b00067a000679f1f7faf1f789f1f6f8f5097afafd7e0dfffffe0f7fff0fffffffffffffff0fffff0eff7e0bfbfb00000000f070f000000bfbfbffff8ff00f0f7fffff8ffc0bfc00000000f07000f0000d0c0bffff7f0fff8fffff7fff0f7ffd0f0ff9fd0f01f60901070a00f6faf1070b010
60a01f5f9f6f97c0d0e8ffeff8fff0f8fffffffffffff0fffff0f0fff0f0fff0f0f8fffff7fffff7fffff7fff0f7fff0f8fffff7fffff7fffff8fffff8fff0f8fff0f8fff0f8fffff7fff0f0ffe0efffb0d0f04f87af005f800f779f1f7fa01f709f1f68904f90b090cfefafdfffafd7efc0dff0c0dff0cfdff0cfdff0cfdf
f0c0dff0c0dfffc0e0ffc0e7ffb0e0f0b0e7f0afd7e0c0e8f0c0e0efd0e8efeffffff0fffff0fff0fff8f0fff8fffff8fffff8fffff7fffff7fff0f7ffe0f8ffbfe0ff40789f1f5f8f2070a0106fa02070a01f608f70a7c0d0f7ffeff8fff0f0f0fffff0fffff0f0efdffffff0fff8fffff7fffff8fff0f8ffeffffff0ffff
f0fff0fff8f0fff8fffff7f0fff7fffff7ffefe8ffeff8ffdff0ff8fbfd02077900f68901070af1f70af10588f407fa0afd0e0e0ffffe0ffffafcfdf40789f0f578f106fa01077af106f9f3f809fb0d7eff0f7fff0f7fffff0fffff8fffff8fff0f0effffff0f0fff0effff0effff0effffff0fffff0f8fffff7ffe0f7ff40
87a00f688f0f709f0f779f0f709f00678f2f789fa0e0ffdff8ffeff8fffff8fffff8f0fff8f0fff8f0fff8fff0f8ffefffffdfffff9fcfdf20688010709f00689f0f78af0f6f9f10678f60a0bfd0f8ffeffffff0f7f0fffffffff0effffffffff7fffff7fffff7fffff8fffff8f0fff8f0fffff0fffff0f0ffffefffffdff8
ffcff0ff80bfdf4080af206f9f2078b00067a00f77af1078af1068901f688f4f88a090c0d0cfe8f0efffffeff7f0fffffffffff0fffff0fffff0f0f0e0bfbfb00000000f070f000000bfbfbffff8ff00f0f7fffff8ffc0bfc00000000f07000f0000d0c0bffff7f0fff7fffff7fff0f7ffd0f0ffa0d7f01f60901070a00068
a01077b00f67a01f5f906097bfcfefffeff8fff0fffff0f7efffffefffffeff0ffeff0fff0f0fff0fff8fffff7fffff7fff0f8fff0f8fffff7fffff7fffff8fffff8fff0f8fff0f8fff0f7fff0f7fffff7ffeff7ffb0cfef4080a000587f00779f005f90005f9000588f10689f3f87b03f80a03f77905080a040708f4f708f
406f8f3f68803f688f40779040789f3f779f4f879f4f889f40788f5087905f7f8f7f979fd0e7e0e0f0f0f0fffffff8fffff8fffff8fffff8fffff8fffff8fff0f7ffeff8ffc0e0ff4f7f9f20608f2070a010689f2070a01f608f6097b0d0f0ffeff8fff0f7f0fffff0ffffeffff7e0fffff0fff7fffff7fffff8fff0f8fff0
fffff0fffff0fff0fff8f0fff7f0fff8fffff8fffff0ffefefffe0f8ffc0dff06097b0206f9010709f1070af0f679f1f60906098bfc0e7f0efffffefffffc0e0f06097bf105f8f0f609f1077af1f77a02f779f80a8c0dfe8fff0f8fffff8fffff7fffff7f0fff7f0fffff0f0fff0f0fff0effff0effffff0f8fff0f8fffff7
ffe0f7ff4087a00f678f0f6f900f709f0f779f0f68902f7f9fafe7ffdff8ffeff8fffff8fffff8f0fff8f0fff8f0fff8fff0f8fff0ffffe0ffffa0d0e02f6f8f10709f0068900f78af0f6f9f10678f609fbfd0f8ffeffffff0f7f0fffffffff0effffffffff8fffff7fffff7fffff8fffff8fffff8f0fff8f0f0f8ffefffff
dff8ffbfe0f080bfd04087af10588f1060902f80b00f679f106fa01f6f9f2f6f905088af90bfd0cfe8ffeff8fff0fffff0f7f0fffffffffff0fff8f0fffff0f0f0efc0bfb00000000f070f000000bfbfbffff8ff00f0f7fffff8ffc0bfc00000000f07000f0000cfc0bffff7f0fff7fffff0fff0f7ffdff0ffa0d7ff206890
1f709f00689f1078b00f6fa01060905f90b0cfe8ffeff8fff0fffff0f0e0ffffefffffefffffefffffeffffff0fff8f0fff8fffff8fff0f8fff0f8fffff8fffff7fffff7fffff8fffff8fff0f8fff0f0fff0f8fffff7ffeff7ffb0d0ef4f80a000587f0f779f0068a00078bf0070b00f6fa01f70a010608f10577f20608030
6f8f3f708f306880205f7f20678f2f709f2f779f1f689010678f206f8f1f5f7f2f687f305f6f5f788fc0dfe0effffff0f8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ffeff8ffcfe7f05f809f2f678f2070a010679f2070a02067904f87a0c0e8ffeff8fff0f8fffff8efffffefffffeffffff0fff8fffff7fffff7
fff0f8fff0f8fff0fffffff8f0fff8f0fff0f0fff8fffff8fff0efffeff0ffe0f8ffa0c8df40779010678f1f77a01070a000588f2f6f9f8fb8d0dff8ffefffffefffffdff7ff8fb8d02f6f9f0f588f1f77af1f78af1f6f9050879fb0d0e0e0f8fff0f8ffefeff0f0f0f0fff8fffffffffffff0f0fff0f0fff0f0fffff0f8ff
fff8fffff8ffe0f7ff4f8faf0f678f0f6f900f6f9f1077a0106f90307f9fafe7ffdff8ffeff8fffff8fffff8f0fff8f0fff8f0fff8fff0f8fff0ffffe0ffffafd8ef2f77901f779f0067900f77af0f6f9f1f678f609fbfd0f8ffeffffff0f7f0fffffffff0effffff0fff8fffff8fffff8fffff8fffff8fffff8fff0f8ffef
f8ffdfffffafd7e06097af3f789f2f709f1f679010609f1f689f2f77a010588020608f6f9fbfafd7f0d0efffe0f7fff0f8fffff8fffff7f0fff8fffff8fffff8f0fffffff0f0efc0b8bf0000000f070f000000bfbfbffff8ff00f0f7fffff8ffbfbfc00000000f07000f0000cfc0bffff7f0fff7fffff0ffeff7ffdff0ffb0
dfff30709f20779f0f67901078af1070a01f608f5087afbfd8f0eff8fffffffff0f0efffffefffffefffffefffffeffffff0fffff0fff8f0fff8ffeff8ffeff8fffff8fffff7fffff7fffff8fffff8fff0f8ffeff7fff0f8fffff7fff0f7ffc0d8f05f88af105f7f10779f0057900067b00060a000589010679f1f608f1f5f
8030688f3f708f4078903f708f20608020688f307fa02f779f0f5f8f0f68901f779f1f6f902f779f3067805f8090cfe7f0effffff0f8fffff8fffff8fffff8f0fff8f0fff8f0fff8fff0f8fff0ffffd0e8f060889f30688f20709f10679f1f70a01f689f3f779fafcfefe0f8fff0fffff0f7effffff0ffffeffffff0fff7ff
fff7fffff7fffff8fff0f8fffff8fffff8f0fff8f0fff0f0fff8fffff8ffefefffe0f7ffd0efff80afc0205f7f0f608f1f78af106fa00f578f4f80afafd7efeffffff0fffff0ffffefffffb0d8ef4f87af0f5f8f1f70a01f7faf106790306f808fb0c0d0f8ffe0f8ffefeff0f0f0f0fff8fffffffffff8f0fffff0fffff0f0
f8fff0f8fffff8fffff8ffe0f7ff5f97b01068900f68900f6f901077a0107090307f9fafe0f0dff8ffeff8fffff8fffff8f0fff8f0fff8f0fff8fff0f8fff0ffffe0ffffb0e0f03078901f779f0067900f77af10709f1f678f6f9fbfd0f8ffeffffff0f7f0fffffffff0effffff0fffff0fff8fffff8fffff8fffff7fffff7
fff0f7ffe0f8ffc0f0ff6fa8bf1f67800f5f801f709f2077af1f68a0105f902f68904070907098bfbfd8f0e0f8ffeff8fff0f8fffff8fffff8fffff0f0fff8fffff8fffff8fffff8ffffefefc0b8bf0000000f070f000000bfbfbffff8ff00f0f7fffff8ffbfc0c00000000f0700000000cfc0bffff7f0fff8fffff0fff0f7
ffe0f8ffc0e7ff40789f2f78a01068901077a01f70a01f609040789fa0c7dfe0f7fff0f8fffff7f0fff8f0ffffefffffefffffeffffff0fffff0fff8f0fff8ffeff8ffeff8fffff7fffff7fffff7fffff8fffff8fff0f8fff0f8fff0f8fff0f7fff0f7ffd0e8ff709fb02068801f779f0f70b01f80c01f78bf2f80b05fa8d0
80c7ef90cfe0a0d7ef90bfcf9fc7df90c0d080b0cf80b8df8fc8ef70b7df4090bf005f8f00689f0067901f709f2060804f7890b0d7e0dfeffff0f8fffff8fffff8fffff8f0fff8f0fffff0fffff0f0fff0f0ffffdfe8f06f8fa0306f8f20709f0f609f1f70af1f6fa02f689080afcfd0efffeffffff0fffffffff0fffff0ff
fff0fff7fff0f7fff0f7fffff7fffff8fffff8fffff8f0fff8f0fff0effff8fffff8ffe0f0ffdff8ffb0d8ef6090af1f5f800f60901f78af10679f1f5f8f6fa0c0cfe8fff0fffffffff0fffff0f0ffffcff0ff70a8c02067901f6fa01f78b00f67902068805090a0afd8e0dfffffeff7fff0f0fffff8fffff8fffff8f0fff8
f0fff8f0fff8fffff8fffff8fffff8ffe0f8ff60a0bf1f70900f68900f68901077a0106f9030789fa0d8f0dff8ffeff8fffff8fffff8f0fff8f0fff8f0fff8fff0f8fff0ffffe0ffffbfe8ff40809f20779f0060900f77a01070a01f608f6f9fbfd0f8ffeffffff0f7f0fffffffff0effffff0f0fff0f0fff0fff8fffff8ff
fff7fffff7ffeff7ffdff7ff7fafcf3f87a00f608f0067901070a0106fa01f68a02f6fa030608090b7cfdff8ffe0f8ffeff0fff0f8fffff7fffff7fffff8fffff0f0fff8fffff8fffff7fffff8fff0eff0c0b8bf0000000f070f000000bfbfbffff8ff00fff7fffff8ffbfc0c0000000000700000000c0c0bffff7f0fff8ff
fff7fff0f8ffe0f8ffcfefff5087a03f80a01f6f901f6f9f2077a02060903f68908fa8cfd0e0f0f0f0fffff8fffff8fffff8f0fff8f0fff8f0fff8f0fff8f0fff8fffff8ffeff8ffeff8fffff7fffff7fffff7fffff8fffff8fff0f8fff0fffff0f7ffefeffff0f8ffe0f7ff8fafc03f77902f789f10679f1068af105f902f
689f7fb0d0bfe8ffd0f7ffdff8ffdff0f0efffffe0f8ffd0e8f0d0f0ffd0f8ffbfe0ff70afd0006fa00077af006fa02078af2f709f5f8fafc0e7ffeff8fff0f7fffff8fffff8fffff8f0fffff0fffff0fffff0f0fff0f0ffffdfeff06f90a0306f901f6f9f00609f106faf106faf1f679f508fb0afcfefdff8ffefffffeff8
ffeffffff0ffffeff7fff0f7fff0f7fffff7fffff8fffff8fffff8f0fff8f0fff0f0ffffffeff7ffe0f7ffdff8ff90c0df40789f20678f0f679f1f70af1060902f6f9090b8d0dff8fff0fffffff8effff8f0f0ffffe0f8ff9fc7df30779f1f679f1f77af0f67901f708f2f778f7fb7c0d0f8ffeff8fff0f7fffff8fffff8ff
fff8f0fff8f0fff8f0fff8fffff8fffff8fffff8ffe0f8ff70a8cf2f779f1068900f679010709f1f6f902f779f9fd7efdff8ffeff8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fff0f8ffefffffc0efff4f87a02078a00060901077a01070a01f608f6f9fbfd0f8ffeffffff0f7f0fffffff0f0effffff0f0fff0f0fff0ff
f8f0fff8fffff7fffff7ffeff7ffd0f7ff4f88a020779f006f900070a00f70a00f68a01f68af3f78b080a8c0dfefffe0f8ffeff8ffefeffffff8fffff8fffff0fffff7fffff0fffff8fffff8fffff8fffff8ffefeff0bfb8bf0000000f070f000000bfbfbffff8ff00fff8fffff8ffbfc0c0000000000700000000c0c0bfff
f7f0fff8fffff8fff0f8ffeff8ffd0f7ff5f88a04f87a0206f901f689f2077af2067902f60907098c0bfd0f0e0effff0f7fffff7fffff7fffff7fffff7fffff8fffff8fffff7fff0f8ffe0f8ffe0f8fff0f8fffff7fffff7fffff8fffff8fff0f8fff0ffffeff7ffefe8f0f0f8ffeff8ff9fbfd04f7f9030789f2070a01f68
a00f50802f608f8fb7d0d0f8ffe0ffffefffffeff7f0f0fffff0f8ffeff7ffeff8ffe0f8ffcfdfff70a7cf0068a00070af00609f1070a0206f905f88afc0e7ffeff8fff0f7fffff8fffff8fffff8f0fffff0fffff0fffff0f0fff0f0ffffd0efff6f90af306f901f6fa0005f9f0f6fb0106fb010609f3077af8fb8e0d0f8ff
dff8ffdff7ffdfffffe0ffffe0f7ffeff7fff0f7fff0f7fffff7fffff8fffff8f0fff8f0fff0f0ffffffeff7ffe0f8ffd0f8ff80b0cf306f8f206f9010689f1f70af105f8f3f779fafcfe0effffff0f7f0fff7effff8effffff0efffffb0d7ef4f80a01f67901f70af0f689f1f78900f677f5fa0b0cff8ffe0f8fff0f8ffff
f8fffff8fffff8fffff8f0fff8f0fff8f0fff8fffff8fffff8ffeff8ff7fb0d02f78a01f68900f67901f709f1f6f902f70909fcfe0dff8ffeff8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fff0f8fff0ffffcfefff4f87a02078a00060901077a01f70a01f608f6f9fbfdff8ffeffffff0f7f0fffffff0f0effffff0f0ff
f0f0fff0fffff0fff8fffff7fffff7ffe0f7ffcff7ff4087a0106f900067900070a00f78b01070af1f6faf3077afd0f8ffe0f8ffe0f7ffeff8fff0f8fffff8fffff7fffff0fffff8fffff0fffff8fffff8fffff8fffff8ffefeff0b0b8bf0000000f070f000000bfbfbffff8ff00fff8fffff8ffbfc0c00000000007000000
00c0c0c0fff7f0fff8fffff7fffff8ffeff8ffe0f8ffa0cfe05f8fa010577f2070a01f70af1f689f1f5f8f205f8f5f87afa0c8e0dff7ffe0f7ffeff7ffeff7ffe0f0ffe0f0ffe0f7ffe0f8ffe0f8ffd0efffe0fffff0f8fff0eff0ffe8f0fff8fffffffff0fffff0fffff0fffff0f7ffefeff0eff8ffe0f8ff80b0c01f5770
1f67902f77af3077a0306f9050879f9fc0d0dff7ffe0ffffdfe7e0f0f8fff0f8ffeff7fff0f7ffeff7ffbfc8e05f87af004f800078b01080bf10689f2f709f7fa8cfcfe7ffeff8fff0f7fffff8fffff8fffff8f0fffff0fffff0f0fff0f0fff0f0ffffe0f8ff7fa7c01f5f8f0f609f0067af005fa00f6fbf00589f1f67a030
6f9f6f9fc0bfe8ffcff8ffcfffffd0f8ffdff8ffcfdff0eff7fff0f8fffff7fffff8fffff8fffff7f0ffffffe0e8efdff0f0dfffffafd7ef4078901f587f2f78a0106fa02f7fb03078a06f98b0cfe8fff0fffffff8f0fff7e0fffff0fff8f0f0ffffdff7ff8fb8d020689000578f0f6fa00f70901f7f903080907fb0c0dff7
fff0f8fff0eff0fff8fffff8fffff8fffff8f0fff8f0fff8fffff8fffff8ffeff8ff90c7e02068900f58801f6f9f1f70a01f68902f688f8fb8d0dff8ffeff8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fffff8fff0f8ffdff8ff5f88af206f9f0f6090006090005f8f2f709f5087a0cfe8ffeffffff0fffffff7f0efefe0
fffff0f0fff0f0fff0fffffffff8fffff7fff0efffd0e7ffb0dfff2f688f0f68900f78a01087b0006fa00057902078bf7fb8efcff8ffdfffffdff7ffdff0ffe0f7fff0f8fff0f8fff0f8ffe0e7f0fff8fff0f8fff0f8fff0f8ffeff7ffe0eff0afb7bf0000000f070f000000bfbfbffff8ff00fff8fffff8ffbfc0c0000000
000700000000bfc0c0f0f7f0fff8fffff7fffff8fff0f8ffe0f8ffbfd8ef7fa0b03f77902070a01f77af1f77a01f689f1f6790206f904f88af6f9fbfafd7ffbfe0ffcfefffcff0ffcff0ffc0e8ffb0dff0afd7ef709fafafc8d0efeff0fff7fffff0fffff8fffff8ffeff7f0eff8f0f0fffff0f8ffeff0fff0f8ffe0f8ffa0
c0d04077900f588f10689f20709f1f608f20607f508fa0a0cfdfdfffffdfffffe0f8ffe0f8ffe0f7ffd0e7ffcfd8ff90a7cf4067900f679f0f77b00f6fa00f5f8f3f78a090bfdfd0efffeff8fffff7fffff8fffff8fffff8f0fffff0fffff0f0fff0f0fff0eff8ffdff7ff7fa8cf2067901068a00068af0058a00068b00f67
a02f7fbf3077af306f905088b07fb0d08fc7e0a0d8f090bfdf90b0cfd0e7ffefeffff0e8f0fff8fffff7f0fff8f0ffffffeff7f0e0f8ffdfffff90c7df3f77901f5880206f9f0f609f1f689f3068907098b0d0e8f0fffffffff7effffff0fffff0fff8f0ffffffe0ffffa0cfe04f87af10679f106fa0006f900f708f1f6f8f
6098b0cfe8ffeff8fffff7fffff8fffff8fffff8fffff8f0fff8f0fff8fffff8fffff8ffeff8ff90c0df2f689010608f2070a02078af1f68902f678f80b0cfdff8fff0f8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fffff8ffeff0ffdff7ff5f88af2f709f1f70a01f78af2078af20678f407790b0d7e0efffffeff0f0f0
f7eff0efe0fffff0f0fff0f0fff0fffffffff8fffff7fff0f0ffdfe8ffbfe0ff2f68800f60800f6f901080b00f77af0058901f68af5098cfbfefffd0f8ffdfffffdff8ffdff8ffe0f8ffd0e7f0cfd7efb0b8cfeff7ffeff7ffeff8fff0ffffefffffefffffb0bfbf0000000f070f000000bfbfbffff8ff00fff8ffffffffbf
c0c0000000000700000000bfc0c0f0f7f0fff8fffff7fffff7fff0f8ffeff8ffd0e8ffa0c0d06fa7bf10678f0f689f1070a01077a010779f0f6f900f689010678f20709f307fa04088af5090b04f90b04087a02f77902f6f8030607090a8b0e0eff0fff8fffff7fffff7f0fff8f0f0f8f0e0f0eff0fffff0fffff0f7fff0f8
ffeff8ffc0dfef7fafc010609000679f1070a01f77a01f70902070904088a060a7bfafe7ffc0f8ffc0f8ffafd8ff90c0e08fafdf6088b02f5f902077af1f70a01060901f5f8f608fb0b0d8f0e0f8ffeff7fffff8fffff8fffff8fffff8f0fffff0fffff0f0fff0efffffdfe8f0d0e8ff7fa7c0206790106fa00f68af0058a0
0068af005f9f1f6faf20679f1f5f8f2f679020678f206f904087af5087af6f90afc0dff0eff0fff0eff0fff8fffff7f0fffff0f0efeff0f8f0e0ffffbfe0f070a8bf30709020608f206f9f1f679f1f609040709090b0c0eff7fffff8fffff0effffffffffffffff7f0fffffff0ffffc0e8ff70a8cf2f77a00f679f1078a010
78901f68804f879fafd0efe0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fff8fffff8fffff8ffeff8ff9fc7e030709f206f9f307faf3080b020709f2f68907fafc0dff8fff0f8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fffff8fff0efffdfefff507f9f206790106090106fa0106f9f206890407790b0d0e0e0f8
ffeff0f0f0f7f0f0f7effffff0eff8eff0f8f0fffffffff8fffff8fffff7ffe0efffcfe8ff5088a02f7090106f901f7faf1f78af0f609f0f589f206fa070afcf8fc0d0a0d7efb0dff0b0d8ef9fc0df80a0bf7f8fa07f8fa0d0e7f0eff8ffeff8ffeff8ffefffffefffffafb8bf0000000f070f000000bfbfbffff8ff00fff8
ffffffffbfc0c0000000000700000000bfc0c0f0f7f0fff8fffff7fffff7fffff8fff0f8ffeff8ffcfe7f0a0d0e02f688f10678f0f678f0f6f900f78a01080a0107fa010779f10779f1f709f1f709f2070901f6f901f6f90106f90206f8f30677090b7bff0f8fffff8fffff0f0ffefeffffffff0ffffdff0e0f0fffff0ffff
fff8fff0f8fff0f8ffe0f8ffb0e0f04f98c01070a0005f8f1f709f2f87af2078a01f6f901f6f90307fa04f98bf5fa0cf4f8fb0407faf3f78a0306f9f1f588f2060902f6f9f3f709f5087af90b8d0dff0ffeff8fff0f7fffff8fffff8fffff8fffff8f0fff8f0fffff0f0fffff0f8ffeff7ffdff0ff80a8cf2f68902070a010
77af0068a01f78b02f80bf206fa01f588f2f68903f78a01f6790005780206f90307090608fafcfe8ffeff8fffff7fffff8fffff7f0fffff0fff7f0f0ffffe0ffffa0c7d05f88a03070903077a03078af2f77af2f689f608fafc0dfeff0ffffffffffffefeffff8fffff8fffff7f0fffffff0ffffdff7ff90c0df3f7faf0057
8f10789f10779f10608020607f7097afc0d8efeff0fff0f0fffff8fffff8fffff8fffffff0fff8fffff8fffff8ffeff8ff8fb8df2f68901f5f8f1f67901f689f1f5f8f20587f6f98b0e0f8fff0f8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fffff8fff0f0ffdfefff4f789f1f5f8f0f5f9010679f10689f30779f4f7f9f
bfd8efeffffff0fffffffffffff7effffff0f0f7f0f0f7f0fff8fffff8fffff8fffff8fff0f7ffdff7ff80a8bf40809f1f67801f68902077a01f70a01f68a02070a01f60802068802f6f8f3070903f6f8f3f67803f5f7f3f5770607f90d0e8ffe0f8ffeff8ffe0f7fff0fffff0ffffa0a8a00000000f070f000000bfbfbfff
f8ff00fff8ffffffffbfc0c000000000070f000000b0c0c0eff7f0fff8fffff7fffff8fffff8fff0f8ffeff8ffe0f8ffd0f0ff7fafc05090af2f70900f608000678f0f70900f789f0f789f10779f10779f1f6f9f1f6f901f6f901f6f901f779f20789030687f90b8c0f0f8fffff8fffff0f0ffefeffffffff0ffffdff0e0f0
fffff0fffffffffffff7fff0f8fff0f8ffdff8ffafe0ff509fc01f608f0f577f10608f1067901f6f90207fa00058801f709f2f7faf20709f1f679020689f2f70a030709f20507f4f779f7f9fbfa0c0dfd0e7ffeff8fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8fff0f8ffe0f8ff8fb0cf3068
8f206f901f6f9f0f60902077a05fa7cf60a8d04f87af20608020678f2f709f2070a02f7faf1f678f4f87a0c0e8ffe0f8fff0f7fffff8fffff0f0fff8f0fffffff0ffffe0f8ff80a8bf3f688f2f678f306f9f2f6fa020679f2f67907fa0b0e0f7fffffffffff8f0ffeff0fff8fffff8fffff8fffff8f0ffffffe0f8ffafd7e0
508fb00050801f7faf1f80af20789f2f6780507890afc0d0eff8fff0f8fff0f8fff0f8fff0fffff0fffff0f8fffff8fffff8ffeff8ff9fc0e03f709f2060901f67901f67902067902f678f7fa7bfe0f8fff0f8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fffff8fff0efffdff0ff5f7fa02f68901f689f1f77af2077a02f
6890407090b0d0e0effffff0fffffffffff0f7effffff0f0f7f0fff7fffff8fffff8fffff8fffff8fffff8ffeff8ffafc7cf6f90a02f607f1057701f608f206f9f2077af2f80b01f6f9f1f6f901f689020678f20678f3068904068904f6f90607f9fd0e7ffe0f8ffeff8ffeff7f0fffffffffff0afa8a00000000f070f0000
00bfbfbffff8ff00fff8ffffffffbfc0c000000000070f000000b0c0c0eff7f0fffffffff8fffff8fffff8fff0f8fff0f8ffefffffe0ffffe0f8ffbfd7ef70a0bf3f78901f68800f67800060800060800f6790106890206f9f2f77a02f77a02f779f20779f2f779030687f90b0bfeff7fffff8fffff8fffff7f0fff8f0f0f8
f0e0f7eff0fff0f0fffffffffffff7fffff8fff0f8ffeff8ffdff8ffbfe0ff8fb0d05f88af2f678f0f50700f577f10608f10608f1f6f9f1f6f9f10608f0f5780105880206790306f904f6f908098b0bfcfe0e0f0fff0f8fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8fff0f8ffe0f7ff
8fa7b0305f7020607f105f7f004f70105f7f30789090d7ef9fd0ef3f7790105070206f901f6f9f0058800f5f803f789fafdff0dff8ffefeff0fff7fffff0f0fff8f0fffffff0ffffbfd7df60889f2057701f507f1f578f10508020609030688f8fa8bfe0f7f0fffffffff7f0fff7fffff7fffff7fffff8fffff8f0fff7f0ef
f8ffcfe8ff70a8cf206890106f9f0f6f9020709f2f688f3f6780809fb0dfe8fff0f8ffeff8fff0fffff0fffff0fffff0fffffff8fffff8ffeff8ff8fb0d03f6890205f8f1f5f8f1f60902067902f608f7098b0e0f8fff0f8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fffff8fffff0ffeff7ff6087a02f68901060901067
9f1060902067903f688fa0c0d0e0f0fff0f8fffffffff0f0effffff0fff7fffff7fffff7fffff8fffff8fffffffffffffff0ffffefffffb0cfd06f97a03f708f2060801f5f8f105f900f5f901f70a01f70a02070a02070a02f70a0306fa03f6f9f406f906f8fb0cfe0ffeff8ffeff7fffff8fffffffffffff0bfb8b0000000
0f070f000000bfbfbffff8ff00fff8ffffffffbfc0c000000000070f000000b0c0c0eff7f0fffffffff8fffff8fffff8fffff8fff0f8ffeff8fff0f8fffff7fffff7ffd0d8efa0c0d070a8c05098b03f87a02f7fa0105f801f5f8f205f8f205f8f2f679030689030709f3f779070a8bfb0d7dfeff7fffff8fffff7fffff8ff
fff8f0eff0efeffff0f0fff0f0fffffff8f0fff7f0fff7fffff8fffff8fffff8fffff8fff0f7ffcfdff09fb0cf6f97b04f789f30688f2f6f902f6f9020688f1f60801f5f802f688f3f78905f87a0afbfdfcfd0e0efe8fffff7fffff8fffff8fffff8fffff8fffff8f0fff8f0fff8fffff8fffff8fffff8fff0f8fff0f8ffe0
e8efeff0f0a0b8c05f88905f97af5097af3f879f4f8fa04f8790b0e8ffcfffff8fc0d05097b04088af20779f0f608f2f7fa04f90afb0e7ffdff8ffe0e8f0fff8fffff0effff8f0fff0efeff0efbfd0df7fa7b05088a0508fb04f88bf407fb05f97c06fa0bfbfd7e0f0fffff0f0effff7f0fff8fffff7ffffeffffff8ffffff
fffff7eff0ffffdfffffa0d8f0509fc02080af106f9f307fa04f87af4f78907090afb0c7d0d0dfe0eff8ffefffffefffffeffffff0fffffff8fffff8ffeff8ff8fa8cf3f709f3070a03078af3f7faf3f7faf3f70907098b0e0f7fff0f8fffff8fffff8f0fff8f0fff8f0f0f8fff0f8fffff8fffff8ffeff7ff7fa0c04f88b0
3f87bf3088bf3087b05f9fc06090b0b0d7efe0f7ffeff7f0fffffff0f7effffffffff8fffff7fffff7fffff8fffffffffffff0fffff0fffff0f0ffffefffffc0e0ef90bfd06f9fbf4087af2f77a01f6fa000609f00609f0f679f1067a02068a0306fa03f70a04f77a09fb7dfdff0ffeff0fff0f7fffff8fffffffffffff0cf
bfb00f00000f070f000000bfbfbffff8ff00fff8ffffffffbfc0c000000000070f000000b0c0c0eff7f0fffffffff8fffff8fffff8fffff8fff0f7ffeff7f0f0f0f0ffeff0fff7fffff8ffeff8ffdff8ffbfe8ff9fd0e08fc0df9fd0f090c7e08fb0d080a8cf8fb0d0a0c7e0bfe0ffcff7ffd0ffffe0fffff0f8fffff0ffff
eff0fff8fffffffff0fff0f0fff0f0fffffffffffff8f0fff0f0f0f0f0f0f7fffff7fffff7f0fff8fffff8fff0f0fff0f8ffe0f8ffd0efffa0c8e090c0df8fbfd08fb7d08fb8d09fc7dfb0dfefcff7ffdff8ffeff8fff0f8fff0f8fff0f7fffff7fffff8fffff8fffffffffff8f0fff8f0fff8f0fff8fffff8fffff8fffff8
fffff8fffff8fff0ffffdff7f0bfd8dfcff8ffcfffffbff0f0c0f8ffd0ffffdfffffdfffffd0ffffc0f0ff8fc7df6fafc07fc0df70b0cf80b7cfd0f8ffe0f8fff0f7fffff8fffff7f0fff8f0ffffffffffffeff8ffd0efffcfefffcff7ffc0f0ffbfe7ffb0e0ffc0e7ffeffffffffffff0efeffff7f0fff8fffff7fff0efff
fff8fffff8fffff7f0ffffffefffffdff8ff90c7e0b0f8ff9fe0ffb0efffcff8ffd0f0ffd0efffeff8ffefffffefffffefffffeffffff0fffff0fffffff8fffff8fff0f8ffdff8ffb0d8ffbfe8ffc0f7ffc0f7ffc0f8ffb0dfffdff8ffe0f8fff0f8fffff8fffff8f0fff8f0f0f8ffeffffff0f8fffff0f0fff8ffeff8ffb0
cfe0a0d7f0afe7ffb0f7ffb0f7ffb0e8ffa0cfe0e0ffffeffffff0f8fffffffff0f7effffffffff8fffff7fffff8fffff8fffff8fffff8f0fffff0fffff0f0f0eff0ffffefffffe0f8ffcfefffafd8f090c8ef80c0ef6fb7e06fb0df6fafdf70b0df80bfefa0d0ffbfe7ffd0f7ffcfe0ffeff7fff0f7fffff8fffff8fffff7
f0ffefe0c0b7af0000000f070f000000bfbfbffff8ff00fff8ffffffffbfc0c000000000070f000000bfc0c0f0f7f0fffffffffffffffffffffffffff8fffffffffffffffff8fffff8fffff8fffff8fff0f8ffeff8ffeff8ffeff8ffe0f8ffe0f8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff7fff0f8fffff8
fffff8fffff8fffff8fffffffffffffffffffffffffffffffffff8fffff7f0fff7f0fff0f0f0f0f0fff8fffff8fffff8fffff8fff0f8fff0f8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffefffffefffffeffff0f0fff0f0fff0f0fff0fffff0fffff0fffff0fffff0fffff0fffff0fff8f0
fff8f0fff8f0fff8f0fffff0f0fff0f0fff0effff0effff0effff0e0fff0effff0effff0effff0effff0effff0efffffe0ffffe0ffffe0ffffeff8ffeff8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fff8f0fff8f0fff8fffff8fff0f8fff0f8fff0f7fff0f7fff0f7fff0f7fffff8fffff8fffff8fffff8fffff7ffff
f7fffff7fffff7fffff8fffff8fffff8fffff8fff0f7ffeff8ffe0f8ffe0f8ffe0f8ffeff8ffeff8fff0f8fff0f8fff0f8fff0f8fff0f8fff0fff0f0fff0fffff0fff8f0fff8f0fff8fff0f8ffeff7ffeff7ffeff7ffeff8ffeff8ffeff8ffeff8ffeff8fff0f8fff0f8fff0f8fff0ffffefffffefffffeffffff0fffff0ff
ffefffffefffffe0f8ffdfffffdfffffdfffffdff8ffe0f8ffefffffeffffff0fffff0fffff0fffff0fffffff8fffff8fffff8fffff8fff0f7f0f0f0f0f0f0eff0efeffff8fffff8fff0f8fff0f8ffeff8ffeff8ffe0f8ffe0f8ffdff8ffdff8ffdff8ffe0f8ffe0f8ffe0f7ffeff7ffeff7fff0f8ffeff7fffff8fffff8ff
fff8fffffffff0efefc0bfbf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ffeffff0effff0effff0effff0effff0effff0f0fff0f0fff0f0fff0fffff0fffff0ff
f8f0fff8f0fff8f0fff8f0fff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8fff0f8fff0ffffefffffefffffefffff
f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0eff8f0eff8f0eff8f0eff7f0eff7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0ff
fffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff7f0f0f7f0f0f0f0f0f0f0f0f0f0f0f7f0f0f7f0fff7f0fff8fffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefffffefffffefffffefffffefffffeff8fff0f8fff0f8fff0f8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff8fff0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0f8fff0f8fff0f8fff0f8ff
f0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fffff8fffff8fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fff0f8fff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0f8f0f0f7f0eff7f0eff0efeff0efefefeffff8fffff8fffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffff
fff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8ffefefefefefefeff0f0f0f0f0f0f0f0f0f7f0f0f7f0f0f7f0fff8fffff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8fff0f8ffeff8ffeff8ffeff8fff0f8fff0f8fff0f8fff0f8ff
fff8fffff8fffff8fffff8fffff7fffff8fffff7fffff7fffff7fffff7fffff0fffff7fffff0fffff7fffff0fffff7fffff0fffff7fffff0fffff7fffff0fffff7fffff0fffff7fff0f8fff0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fffff0fffff0
fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0f8fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff8fffff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8
f0fffff0fffff0fff8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f7fff0f7fff0f7f0f0f7f0f0f7f0f0f7f0f0f7f0f0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fff0f8fff0f8fff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0f8fff0f8fff0f8f0eff7f0eff7f0f0f7f0fff8fffff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8fff0f8fff0f8fff0f8fff0f8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fff0f8fff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0ff
f0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fffffff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0effff0effff0effff0
effff0effff0effff0effff0effffff0f8fffff7fffff7fffff7fffff7fffff7fffff7fffff7fff0f7fff0f7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffff8fffff8fff0f7f0f0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fff0f8ffeffffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffffff0fff8fffffff0fff8fffffff0fff8fffffff0fff8fffffff0fff8fffffff0fff8fffffff0fff8fffff8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0fffff0ffffefffffef
ffffefffffefffffefffffefffffefffffeffffff0f8fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f8fffff8fff0f8fffff8fffff8fffffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0ffffefffffeff8ffeff7f0e0f0f0e0efefdfefefdfe8efdfefefe0efefe0f0f0eff8f0effffff0fffff0fffff0fffff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fffff8f0fffff0fffff0
fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fff8f0fff8f0fff8f0fff8f0fffff0fffff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0fffff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fff8fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7
fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fff0f7ffeff7fff0f7ffeff7fff0f7ffeff7fff0f7ffeff7fff0f8fff0f8fff0f7ffeff0ffeff0ffe0efffe0efffe0eff0efe8efefefefefefeff0f0f0f0f7f0fff8fffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0ffffefffffeff8f0dff0efd0e7e0cfdfdfc0d7d0c0d0d0bfc8cfbfcfcfc0d7d0cfdfdfdfe8e0e0f7f0efffffefffffeff8ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff8fffff8ffff
fff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0fffff0fff8fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7fffff7
fffff7fffff7fffff7fffff7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff7fffff0fffff0fffff0fffff0fffff0ff
fff0fffff0fffff0fffff0fffff0fffff0fffff7fffff7fffff7fffff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff7ffeff8ffeff8ffe0f0ffdfefffdfe7f0d0dfefcfd7e0cfd7dfc0c0c0c0c7c0cfcfcfd0d7d0e0e0e0efe8eff0f0f0f0f0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ffefffffefffffefffffefffffefffffefffffefffffefffffc0d7d0c0d0cfb0c7c0afb8b09fa8a08f989f808f8f7f878f9fa8bf9fafc0afb8d0b0c7dfc0d7efd0e0ffdfe8ffdff0ffdfefffdff0ffe0f7ffe0f7ffe0f7ffe0f7ffe0f7ffe0f7
ffefffffefffffefffffefffffefffffefffffeff8ffe0f8ffefffffefffffefffffefffffeff8ffe0f8ffe0f8f0eff8f0eff0eff0f7effff8f0fffffffffffffffffffffffffffff0f0f7f0f0f8f0fff8f0fffff0fffffffffffffffffffffffff0f0fff0f0fffff7fffff7fffff7fff0f0fff0efffefe8fffff7fffff7ff
fff7fffff7fffff7fffff7fffff7fffff7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fffff8fffff8fffff8fffff8fffff7fffff7fff0f7fff0f7fff0f7fffff7fffff8fffff8fffff8fffff8fff0f7fff0f0fffff7fff0f7fffff7fffff7fffff8fffff8fffff8fffff8fff0f7fff0f7fff0f7fff0f7fff0
f7ffeff7ffeff0ffeff0fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7fff0f7ffeff7ffe0f0ffe0f0ffdfefffd0e7f0c0d7e0afc0cf9fafbf90a0b08f98a08090a07f88907087907f8f9f90a0b0afb8c0bfc8d0f0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0ffffefffffefffffefffffefffffefffffefffffefffffeff8ffeff8f0e0f0f0dfe8efd0e0dfcfd8dfc0d0d0bfcfd07f8fa07088a07087a06f7f9f6078906070905f6f8f5f6f8f6f809f7087a07f90af8098b08fa0bf90a7c0
90a7c090a8c0b0cfdfb0d0dfbfd8e0bfdfefc0e8f0cfefffd0f7ffd0f8ffdfffffdfffffdfffffdfffffdfffffd0ffffd0ffffdfffffe0f7ffeff8ffeff8ffefffffeff8ffeff7ffe0f0ffe0eff0eff8ffeff8ffefffffefffffefffffefffffefffffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffef
f8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffeff8ffe0f8ffdfe8ffdfefffe0f7ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffdff0ffdff0ffe0f0ffe0f7ffe0f8ffe0f8ffe0f8ffe0f8ffcfe7ffc0e8ffc0e8
ffcfe8ffcfe8ffcfe8ffcfe8ffcfe8ffafc7e0afc7dfa0bfd09fb0cf90a8c080a0bf8098b07f97af5067705f6f7f6078806f808f7088907088906f87906f808fbfd0dfbfd0dfbfd0dfbfd7dfc0dfe0cfe7f0d0efffe0f7fff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ffefffffefffffefffffefffffefffffefffffeffffff0fffff0fffff0fffff0fffff0ffffefffffefffffeff8ffd0e8ffcfe0ffc0d7f0b0c7e0a0b8d090a8c08fa0bf8098b06f7f9f6f789f6078906077905f
6f8f5067804f5f7f40587f5077904f77904f77904070904070903f6f8f3f6f8f3f6f8f5f8faf5f90b06098b06fa7c07fafcf80b8df8fc0df90c8e07fa8c08fb0cf90bfd0a0c8e0b0d8efbfe0ffc0e8ffc0efffbfe7ffbfe7ffbfe7ffbfe7ffbfe7ffbfe8ffbfe8ffc0e8ffbfe8ffbfe8ffc0efffc0efffc0f0ffc0efffc0ef
ffc0efffc0f0ffc0f0ffc0f0ffc0f0ffc0f0ffc0f0ffc0f0ffc0f0ffc0efffc0efffc0efffc0efffc0efffc0efffc0efffc0efffbfe8f0bfe8f0bfe8f0bfe8f0bfe8f0bfe8ffbfe8ffbfe8ffbff0ffb0efffafe7ffa0dfff90cfef8fc0e07fb7df70b0d06fa8cf6fa8cf70a8cf6fa8cf70a8cf6fa8cf70a8cf6fa8cf4088a0
40879f3f879f3f80903f7f9030788f30778f30708f1f576f1f5870205f702060702f677f2f687f3068803f6f7f6f88907f979f8fa7afa0b8c0b0c8d0bfd7dfc0d8e0c0dfe0d0e8f0dff0ffe0f8ffe0ffffe0ffffe0ffffe0ffffeffffff0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8ffeff7f0eff7f0eff7f0f0f8f0f0f8fff0fffff0fffff0ffffeff8ffe0f7ffe0f7ffe0f7ffe0f7ffe0f7ffe0f7ffe0f7ffc0d7efbfcfefb0c7
dfa0b7cf90a7bf8090af7087a0607f9f3f678f30678f30678f30678f30678f30678f30678f2f678f3f709f307090306f902f688f2060801f5f801f587f10577f20608020608020678f2f688f2f6f8f2f6f90306f90306f903f789f3f789f3f789f3f789f3f789f3f789f3f789f3f789f3070903f77903f77903f78903f789f
3f789f3f789f3f789f4080a04080a04080a04080a04080a04080a04080a04087a0407f904080904080904080904080904080904080904080903f7f903f7f903f7f903f7f903f7f903f7f903f7f903f7f9f2f779020779f20709f2070901f6f901f6f901f68901f688f20779f20779f20709f1f70901f6f901f68901f688f1f
688f10607f1f67701f67701f677f20687f20687f2f6f7f2f6f8030778f407f904f8f9f609fb070b0c08fc7d09fd0e0afd8e0cfe8f0dfeff0e0f7ffe0ffffefffffe0ffffefffffe0ffffdfeff0dff0ffefffffe0ffffefffffe0ffffe0f8ffe0f0f0f0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffff
fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffffffffffffffff0ffffdfe8ffdfe8ffe0efffe0f0ffeff7ffeff8ffeff8ffeff8ffeff8ff
eff8ffeff8ffe0f0ffe0efffdfe8ffdfe8ffd0e7ffafcfef9fc7ef8fb8df7fa8cf6f98bf5f87af4f78a040779f3f688f30688f30688f2f678f2f678f206080205f801f5f80207090206f901f6f901f689010678f10608f10608f105f801f68901f68901f68901f68901f68901f68901f68901f68901f688f1f678f1f678f1f
678f1f688f2068902068902068901f678f1f678f1f678f1f678f1f678f1f678f1f678f1f678f10607f10607f10607f10607f10607f10607f10607f10607f1f68801f68801f68801f6f80206f8f206f8f20708f20708f0f67800f678f0f688f0f688f106f901070901f77901f779f0060800f67800f678f0f678f0f688f0f68
8f106f8f1f6f8f40788f507f8f60889f6f98a080a8b08fb7c09fbfcfa0c7d0cfefffd0efffdff7ffdff8ffe0f8ffe0f8ffe0f8ffe0f8ffeff8ffeff8ffeff8ffefffffeff8ffeff7f0e0f0f0e0efefeff8ffeffffff0fffff0fffff0ffffefffffeff8ffeff7f0f0f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffff
fffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffffff8fffff8fff0f7fff0f8ffeff8ffeff8ffeff7ffeff7ffeff7ffeff7ffef
f7fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8ffeff8ffe0f7ffdff7ffdff7ffdff7ffd0f7ffd0f7ffd0f7ffd0f7ffafd7efa0cfe090c0df80b0cf70a0bf6090af50879f4f7f9f2f709020709f206f90206f90206f90206f9020709f2f709f3078a03078a02f78a02f779f2f779f2f779f2f779f2f779f307faf2f7f
a02f77a02f77a02070a02077a02f77a02f78a02f77a02f77a02f77a02f77a02f77a02f77a02f77a02f77a020779f2078902078902078902078902078902078902078902077902077902078902f789f2f7f9f2f7f9f2f7f9f30809f2f77902f778f2f708f2f708f206f8f2f708f2f708f2f708f3f879f4088a04f90af5098b0
5fa0bf60a8c06fb0cf70b0cfcff0ffdff0ffdff7ffe0ffffe0ffffefffffeff8ffeff8fff0f8fff0f8fff0f8fff0f8fff0f7fff0f0ffefefffefeff0f0f8fff0f8fffffffffffffffffffffffffffffffffffffffffffff0fffff0f8f0f0f7f0f0f7f0eff7f0f0f7f0f0f7f0fff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffff
fffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff7f0fff7f0fff7f0fff0f0f0f0f0f0f0f0f0f0f0f0f0f0fff8fffffffffffffffffffff0f8fff0f7
f0f0f0f0f0f0f0fffffffffffff0f7fff0f0f0f0f0f0f0f0f0f0f7f0f0f7fff0f8ffeff7ffeff0ffe0f0ffe0efffdfe8f0dfe7f0d0e7f0e0f8ffe0ffffe0ffffe0ffffdfffffdfffffdfffffd0f8ffbfe0ffafd8ffa0cff090c0e08fb7df80afd07fa8cf7fa7cf407090407090406f90406f90406f9040688f40688f3f6890
2067901f60901058900f578f0f508f0f50800f508f0f578f105f90105f90105f90105f90105f90105f90105f90105f8f10608f10608f10608f10608f10608f10608f10608f10608f1f68901f6890206f90206f9020709020709f2f779f3f779080a7bf90a8bf9fa8bf9fb0c0afbfcfb0c8dfc0d7e0c0d8efcfe0efcfe0f0d0
e8f0dfefffe0f7ffe0f8ffe0f8ffe0f8ffefffffeffffff0ffffeff8ffe0f0f0e0efefe0e8efe0e8effffffffff8fffff8fffff8fffff7fffff7fffff7fffff7f0fff7f0fff8f0fff8f0fff8fffffffffffffffffffffffffffff8f0fff7f0fff7f0fff7f0fff7f0f0f0effff0eff0f0effff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffff
fffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff7f0fff7f0fff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffff
fffffffffffffffffffffffffffffffffffffff8fffff7f0fff8fffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffffffff0fffff0ffffeff7ffeff8ffeff8ffeff8ffeff8ffefffffefffffefffffe0f8ffe0f8ffe0f8ffdff7ffdff7ffdff7ffe0f8ffe0f8ffdff0ffdff0ffdfefffd0efffd0efffd0e8ffd0
e8ffcfe7ffa0cff090c7f08fbfef80b7e080b0df7fb0df7fb0df80b0df7fafdf7fafdf7fafdf7fafdf7fafdf7fafdf7fafdf7fb0d080b7d080b7d080b7d080b7d080b7d080b7d080b7d080b7d0afe7ffb0e8ffb0e8ffb0efffb0f0ffbff0ffbff7ffcff7ffeff8fffff7fff0f0f0f0eff0f0eff0f0f0f0fff7fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0eff8f0f0f8f0f0f8fffffffffffffffffffffffffffff7f0fff7f0fff7f0fff7f0fff8fffff8fffff8fffff8fffffffffffffffffffffffffffff8f0fff7f0fff7f0fff0f0fffffffffffffffffffffffffffffffffffffffffffff8f0fff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0
fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffff0f0f0f0f0f0fff7ffffffffffffffffffffffffffffffffffffffe0e8efefefeff0fffff0ffffe0eff0f0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8
fff0f8fff0f8ffeff8ffeff8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffe0f8ffeff8fff0f8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0
c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0f0f7f0fffffffffffff0f7f0cfc8cfa0a7a0dfd7dfd0d0d0e0e7e0fffffffffffffffffff0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fff8f0fff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f07
0f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0f0f0f7f0fffffffffffff0f7f0b0b0b07f777f5057504f484f808080e0e7e0fffffff0f7f0f0f0f0fffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fff8f0fff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c0
0000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffff8fff0f7f0ffffffffffffffffffdfd8dfbfbfbf403f40100f101010106f686fd0d7d0fffffffffffff0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fff8f0fff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffff
ffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffff8fff0f7f0f0f7f0fff8ffffffffffffffd0cfd0808780202020100f10707070f0f0f0ffffffefe8effff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fff8f0ff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff
00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefe8eff0f7f0fffffffff8ffefefefefefeffff8ffffffffffffffffffffafa8af3f373f1f181f707070dfdfdffffffffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0ffff
f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bf
bfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0f0f0f7f0fff8fffff8fff0f7f0f0f0f0f0f7f0f0f7f0f0f0f0ffffffffffffbfb8bf4f484f201f207f777ff0f0f0fff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0
fffff0fffff0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f07
0f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffff0f7f0fff8fffffffffffffffffffffff8ffffffffe0e0e0f0f7f0ffffffdfd8df4040402020206f686ffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0ff
fff0fffff0fffff0fffff0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf
0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffff0f0f0fff8ffe0e0e080878020
1f206f6f6ffff8fffffffff0f0f0f0f7f0f0f7f0fffffffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f0efefefeffff8f0fffffff0f8f0fffff0ffffffeff0efffff
fffffff0efe8e0efe8e0fffffffffffff0f0eff0f0effff8fffffffffffffffffffffffffffffffff0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffef
efefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0fffffffffffff0f7f0ffffffffff
ffd0cfd08087801f171f807f80f0f7f0fffffff0f7f0f0f0f0f0f7f0fff8fffffffffffffffff8fffff8fffff8fffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffffd0d0cf80807f60605f9f9890efefefffffffeff0ef
fffffffffffffffffffffffffffffffffffffffffff0f0effffffffffffffffffffff8ffefe8efe0e7e0f0f7f0fffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8
ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffefefeffffffffff8fff0f7f0
ffffffffffffffffffe0e0e07f7f7f0000008f888fffffffefe8efe0e7e0f0f7f0fffffffffffffffffffff8fff0f7f0f0f7f0fff8fffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffffffffffbfb8b02f2f2f0000002f2f2f6f
67606f686fbfb8b0cfcfcfefefeffffffffff8f0f0f0effffffffffffff0f0f0fff8fffffffffff8fff0f0f0f0f0f0fffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0ffffff
fffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0f0ffffffff
f8ffefefeff0f0f0fffffffffffffff8ffffffff606060201f20808080ffffffffffffdfd8dffffffffffffffffffffff8fff0f7f0f0f0f0f0f7f0fff8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0efffffffdfdfdf70706f3f38
304f4f4f5f58503f3f3f40474030302f4f484090908fd0d7d0f0f0effffff0fffffffffffffffffff0f7f0fff8fffffffffffffffffffff0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0
f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0
f0fffffffffffff0f7f0efefeff0f7f0f0f7f0f0f0f0ffffffdfdfdf302f301f181fcfc7cffffffffff8fffff8fffffffffffffffff8fff0f7f0f0f7f0f0f7f0fff8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0effff8f0f0f0ef
e0e7e0efefe0f0f7f0e0e7e0c0c7c0d0d7cf8f88803f3f3f2020203030305f5f5f9f9f9fd0d7d0fffffffffffffffffffff8fff0f7f0f0f0f0f0f0f0f0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fff0f7f0fffffffffffffffffffff8fffff8fffffffffffffff0f7f0efefef6f676f1010102f2f2f8f888ffffffffffffff0f7f0f0f7f0f0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0ff
ffffffffffffffffffffffefefefffffffffffffffffffffffffffffffd0d0cf7077702f2820101010201f1f6f6f6fa0a0a0e0e0e0fffffffffffff0f7f0f0f7f0fffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fff0f7f0fffffffffffffffffffffffffffffffffffffffffff0f0f0ffffff8f888f1f171f100f102f272f707770e0e7e0efe8efefefeff0f7f0fffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fff0f8f0efefefefe8e0efefefefe8e0e0e8e0f0f8f0fffffff0f8f0ffffffffffffffffffffffffc0c8c06f68601f1f1f0000003030307f787fc0bfc0efefeffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fff0f7f0f0f7f0f0f7f0f0f0f0f0f0f0efefeff0f7f0fff8fffffffff0f7f0c0c0c04047401f1f20303030201f204f484fdfe0dfe0e8e0f0f8f0fffffffffff0fffff0fffff0f0f8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffffffffffffffffffffffffffffffffffffffffffff0f7f0fff8f0efefefe0e0dfefe8e0ffffffffffffd0d7d080878030302f101010100f0f4f484fafa8aff0f7f0fffffff0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0ffffffffffffa0a7a03037301017102027201f201f1f271f4f584f809780b0c0afdff0dff0ffeff0ffefefffeff0f8f0f0f8fffffffffffffffffffffffffff0f8ffeff0f0f0f8fff0f8fff0f8fff0f8fff0
f8fff0f8fff0f8fff0f8fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0f0f8fffffffffffffffff8ffefefef30372f1f181020282020201f6f6f6ff0f8f0fffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f8ffeff0efdfe0df9fa09f303f300f100f2030202f37201f301f1f2f102f40205f785fa0c0a0dff8d0efffeff0fff0fffffff0f7f0efe8efefe8eff0f7f0ffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff8fff0f8fff0f7f0f0f0f0efefefefefefefefefeff0f0efefefefefeff0f7f0fff8ffeff0f0d0d8dfc0c0c030372f30372f10100f30372fa0a89ff0f8effffffffffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0efefeffffffff0ffff808f801f271f202f202030201f2f1f0f200f0017000018003050309fb790efffefe0f0e0f0f7f0fffffffffffffffffff0f8fff0f7f0eff0f0f0f8ff
f0f8f0f0f8fff0f8f0f0f8fff0f8f0f0f8fff0f8f0f0f7f0f0f7f0f0f8fffffffffffffffffffffffffffffffff0f8f0e0e8e0e0e8efffffffffffffefefef8f888f3037302f282040473f0f0f0060675fefefe0f0f8effffff0fffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fffffffffffffffffffff0ffffdfe7df6f776f0f180f202f1f20372030472f2f40200f270f0f27004f67409fb090eff7eff0f8f0fffffffffffff0f7f0eff0efefefefef
f0eff0f7f0f0f7f0f0f7f0f0f7f0f0f7f0f0f7f0f0f7f0f0f7f0fffffffffffffffffffffffffffffff0f8f0eff0f0eff0eff0f8f0ffffffffffffffffffc0c8cf70787f3037300f0f0f10170f2f2f2030372fa0a89ffffff0fffff0efefeffffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f8fffffffff0f8f0eff0eff0fff0bfc7bf1f271f2f3f2f10281010270f203f202f47201f371010300f20371fb0c0b0d0d8d0f0f8f0f0fffff0fffff0ff
fff0f8f0eff7eff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0ffffe0efefeff0eff0f7f0f0fff0f0fffff0fff0f0f8f0eff7f0f0ffffeff7f0c0c8c0808f8f4f4f4f2027201f1f1f2028200f0f001f180f80877fe0e7dffffff0fffff0e0e7e0fffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffff
fffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffffe0e7e0f0f7f0ffffffdfe7e0f0fff0e0efe0505f502f382f1f2f1010200f10280f1f30101030101030101f3710303f2f6f706fb0b8b0eff7ef
f0fff0f0fff0f0fff0f0fff0f0f8f0f0f8f0f0f8f0f0f8f0f0f8f0f0f8f0f0f8f0f0f8f0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0f8f0eff7efbfc0bf7f877f3f3f3f1f1f1f2027202f302f20282010181020282040473fcfcfc0fffff0fffff0fffff0e0e8e0fffffffff8f0fff8f0fff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffff
fffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffffe0e8eff0f8ffffffffeff0eff0fff0eff8ef80978010200f2038202f402f1f37100f2000102f0f204020304f300f1700202f204f
504f606f607f877f9fa79fc0cfc0e0efe0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0eff8efe0efe0cfd8cfb0bfb0909f907f807f6f706f2f302f2f372f2f302f202f201f201f1f201f1f271f202f2060675fafb0a0f0f8effffff0fffff0fffff0f0f7f0fffffffff8f0fff8f0fff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffff
fffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffe0e8effffffffffffffffffff0fff0f0fff0eff8ef9faf9f000f001f28102038201f37101f371f2f47201f381f0020000f18
001f271f202f201f281f101f1010201030382f405040303f30303f30303f30303f30303f30303f30303f30303f303f473f3f483f3f483f3f47302f382f1f281f10180f0010001f20101f281f2f302020302020281f1f281f202f202f372f90988ffffff0fffff0f0f7effffff0fffff0fffffff0f7f0fff8f0fff8f0fff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffff
fffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0f0f8ffffffffeff7f0eff8efeff8efdfe8df20301f20301f20301f1f371f1f371f1f37101f3710
1f371020301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f1027102037201f281f2030202f382f0010001f2f1f90a790fffff0fffff0efefe0efefe0fffff0fffff0efefe0fff8f0fff8f0ff
f8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffff
fffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0f0f8ffffffffeff7f0f0fff0f0fff0e0f0e020301f20301f20301f1f371f1f371f1f
37101f37101f371020301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f20301f2f38202f402f1f28101f28102f3820102710405740c0d0bffffff0fffff0f0f7efe0e8dffffff0fffff0f0f7f0ffff
f0fff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0
f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0f0f8fffffffff0f8f0f0fff0f0fff0dfefdf20301f20301f20301f1f37
1f1f371f1f37101f37101f37101f371f1f301f20301f1f301f20301f1f301f20301f1f301f20301f1f301f20301f1f301f20301f1f301f20301f1f301f20301f1f301f20301f1f301f20301f1f301f20301f1f301f2038202f40201f2f101028102038201f301f4f5840afc0affffff0fffff0fffff0efefe0fffff0fffff0
fffffffffff0fff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f00
0000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffff0f8f0f0fff0eff7efbfcfbf20301f20301f
20301f1f371f1f371f1f37101f37101f37101f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f10270f1f371f1f371f20381f2038200f2700102f1050674fafb8a0e0efdffffff0fffff0f0
f8effffff0fffffff0f8f0fff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000
000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0f0f7f0fffffffffffff0f8f0f0fff0d0dfd090a09020
301f20301f20301f1f371f1f371f1f37101f37101f37101f37101f37101f371f1f37101f371f1f37101f371f1f37101f371f1f37101f371f1f37101f371f1f37101f371f1f37101f371f1f37101f371f1f37101f371f1f37101f371f1f3710001f001f301020381f203f2020381f00200000170010270f4f503f9fa090f0f8
effffff0fffff0f0f8eff0f8f0f0f7effff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffff
c0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0f0f7f0fffffffffffff0f8f0f0fff0cfd8
cf80908020301f20301f20301f1f371f1f371f1f37101f37101f38101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101f37101030101f381f20381f1f38101f37101f30101030101f2f100f1000
5f584fcfcfc0fffff0fffff0f0f7eff0f7f0f0f0effff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00ff
f8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffff8fffffffff0f8f0f0f8f0
f0fff0e0efe09fa89f20301f20301f20301f1f371f1f371f1f37101f37101f38101f38101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f381f203f1f1f3f1f10371010301020401f2f472020
371f0f17002f2f2090978ff0f7effffff0fffff0fffffff0f8f0fff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbf
fff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffff0f8ffef
f0eff0f8f0fffff0f0fff0bfc0b020301f20301f20301f20301f20371f2037102037101f37101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f38101f37101f37101f37101f37101f37101f37101f37101f37101f38101f37101037101f37101f3f1f1037101030102040
202040200f20002f37201f201060685fd0dfd0fffff0fffff0fffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f00
0000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffffff0fffff0fffff0fffff0fffff04f50402f37201f271020301f303f2020301f1f270f1027002037101f37101f37101f37101f38101f38101f38101f38100f2800102f0010300f1f370f1f37101f37101f37101f300f2f472020381f1f300f102f0f102f0f1f30101f37101f30101f37101f37101f371f10371f
10371f10371f10371f1f371f20371f1f2810102010b0c0b0f0f8f0f0ffffeff7f0fffffff0f8fffff8fffff8fffff8fffff8fffff8fffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000
000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8f0fff8f0fffff0fffff0fffff0c0c8bf5f5f4f10180f2f2f1f3f402f202810202f104050302f3f1f203f1f2038102038102038101f37101f37101f37102f401f2f401f2f401f2f401f2038101f3710102f0f1028000f20001f2f0f2f3f1f2f47202f402020381f1f37101f30101f371f1f371f1f
371f10371f10371f10371f10371f10371f1f371f1f301f101f0f8f9f8feff8f0f0fffff0f7f0f0f8fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefef
bfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8f0fff8f0fffff0fffff0fffff0fffff0afb0a050574f2f2f1f1f1f0f1018001f200f20301f1f2f101f2f101f2f0f10280f10280f1027001027000f200010280f1f2f0f1f30101f30102037101f30101f2f101f2f0f1f2f0f20381f2f40202f3f1f1f2f1010270f102f101f371f1f37
1f1f371f1f371f10371f10371f10371f10371f10371f1f301f2f38200f1f0f506750eff8eff0fffff0f8fff0f7fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffff
ffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0eff0e0c0c7b07f87704047302027101f1f0f1f200f1f2f101f2f101f2f101f2f101f2f101f30101f301f1f301f1f2f101f301f20371f20371f20371f1f301f1f2f101f2f101f280f1f2f0f1f2f0f1f280f10270f10280f1f3010
20371f1f371f1f371f1f371f1f371f10371f10371f10371f1f371f1f2f1030402f0f180f2f382feff7eff0fff0f0ffffeff0f0f0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0ffffffffff
fffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0cfd7c080887f6f70608088704f58404f58404f5f4f50604f5f68505f6f5f60705f6077606f7f6f6f78606077605f68504f5f4f3f4f3f30402f2f38201f281010270f10200f1f280f20371f2f
3f2020382020371f1f371f1f371f1f371f1f371f1f371f10371f10371f1f371f1f2f103047300f1f0f20372fe0f0e0f0fff0f0ffffefeff0f0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0
fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fff8f0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0fffff0eff7e0e0e8dfe0e8dfdfefdfe0f0e0eff8eff0fff0f0fff0f0fff0f0fff0f0fff0f0fff0f0fff0eff8efe0efe0d0e7d0cfd8cfc0d0c090a08f70806f4f58402037
1f1f270f1f281020301f2038201f371f1f371f1f371f1f371f1f371f1f371f1f371f1f371f1f2f102f402f101f0f405740dfe8dff0fffff0ffffeff0f0f0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
fffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fff8f0fff8f0fffff0fffff0fffff0fffff0eff0effffff0f0f8efefefe0fffff0fffff0fffff0f0f8eff0f8f0f0fff0f0fff0f0fff0f0fffff0fff0f0fffff0fff0eff0efeff7eff0f8f0f0fff0f0fffff0fff0f0f8f0f0f8eff0fff0eff7e0
bfc7b0707f6f30402f10200f1f271020382020301f1f301f1f371f1f371f1f371f1f371f1f371f1f371f1f2f102f3820102010708770d0e0dff0fffff0ffffeff7f0f0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fff8f0fff8f0fff8f0fffff0fffff0fffff0fffff0fffffffffff0fff8f0fffffff0f7efe0e8dfefefe0fffffffffffff0fffff0fffff0fffff0fffff0f8f0f0f8f0f0f8f0f0fffff0fffff0fffff0fffff0fffff0fffff0f8fff0f8f0df
e7dffffff0fffff0e0efdf8f97803f483f1f281f1f2f1f20301f20301f1f371f1f371f1f371f1f371f1f371f1f371f1f2f1020372010271090a790d0dfd0f0fffff0fffff0f7fff0f8fff0f8fffff8fffff8fffff8fffff8f0fff8f0fffff0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8f0fff8fffff8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8fff0f8
fff0f8ffeff0effffff0fffff0fffff0fffff0b0bfb03f483f0f180f20371f20371f20371f1f371f1f37101f30101f30101f30100f270f2f402f90a08feffff0f0fff0eff8f0eff0f0f0f8fff0f7fff0f7fffff8fffff8fffffffffff8f0f0f0efefe8e0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8f0fffff0fffff0fffff0fffff0fffff0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8fff0f8fffff8ff
f0f8fffff8fffff8fffff8f0fff8f0fffff0fffff0fffff0dfe0d07f887f1f271020302020371f20371f20371f1f30101f30101f30101f30102f402f7f907fd0e0cff0fff0f0fff0f0fffff0fffff0fffff0f0fff0f7fff0f7fffff8fffffffffffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffff0fffff0fffff0fffff0fff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffffffff0f7f0fff8f0f0f7f0eff0effffff0cfd0c04f574020302020371f20301f20301f2030101f30101f30101f30105f6f50dfefd0f0ffeff0fff0effff0f0fffff0fffff0ffffeff0f0f0f7fff0f7fffff8fffff8fffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffff0fffff0fffff0fffff0fff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff7fffff7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0f0fff7f0ffffffeff7ef9fa89f2f302020301f20301f20371f2030101f30101f30101f3010607f60efffe0f0ffefdfefdfeff8eff0fff0e0e8eff0f7fff0f7fff0f8fffff8fffff8fffff7f0fff8f0fffffffffffffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffff0fffff0fffff0fffff0fff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff7fffff7fffff7fffff7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff0fffff8fffff7fffff7ffffffffffffffeff0e0f0f8ef2f30202f302020301f20371f2030101f30101f30101f301090a88fe0ffe0f0ffefeff8eff0fff0f0ffffe0e8eff0f8fff0f7fffff8fffff8fffff8fff0f7f0f0f0effff8f0fffffffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffff0fffff0fffff0fffff0fff8f0fff8f0fff8fffff8fffff8fffff8fffff7fffff7fffff7fffff7fffff7fffff7fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0e8f0fff8fffff8fffff8fffff8fff0f7f0e0e8e0fffff02f30202f302020301f20301f2030102030101f30101f30102f3f205067506f806f9fafa0eff7eff0fffff0f8fff0ffffe0e8eff0f8fffff8fffff8fff0f7f0efefeff0f7effffffffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffff0fffff0fffff0fffff0fff8f0fff8f0fff8fffff8fffff8fffff8fffff7fffff7fffff7fffff7fffff7fffff7fffff8fffff8f0fff8fffff8f0fff8
fffff8f0fff8fffff8f0fff8fffff8f0fff8fffff8f0fff8fffff8f0fff8fffff8fff0eff0fff8fffff8fffff8fffff8fffffffffff8f0f0fff02f30202f302020301f20301f2030102030102030101f301010280f1f28101f2f1f2f382f404f404f504f40474f3f3f40cfcfd0e0e7effff8fffff8fffff8fff0f0effff8f0
fffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffff
fffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffff0fffff0fffff0fffff0fff8f0fff8fffff8fffff8fffff8fffff8fffff7fffff7fffff7fffff7fffff7fffff7fffff8fffff8f0fff8ff
fff8f0fff8fffff8f0fff8fffff8f0fff8fffff8f0fff8fffff8f0fff8fffff8f0fff8fffff8fffff0fffff7fffff8fffff7ffefe7e0ffffffffffffcfd0cf2f30202f302020301f20301f2030101f30101f30101f301020301f202f1f1f281f10180f000f001f1f1f2027200f080fb0b7bfd0d7dffff7fffff8ffffffffff
f7f0fffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffff
fffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff0fffff0fffff7fffff8fffff8fffffffff0f0efdfe7df3038302f372f20371f20371f2f3f202f4020203f1f2030102f372020281f20281f2f2f202f2f202027201f1f1f101710c0c8c0dfdfdffff8ffffff
fffff8fffffffffff8fffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffff
fffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff7ffffeffff0eff0fff7fff0f7f0f0f0efe0efe02f372f2030201f2f1f1f281010280f102f0f1f2f101f3010303f2f20201020201f30302f2f2f202f302f70706fc0c8c0dfdfdfefefef
fffffffffffffff8fff0f7fff0f7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffff
fffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff7fffff0fffff7fffff8fffffffffffffffffff02f302f20302020301f20371f20371f1f30101f301020371f20281f3f3f304f484050574f7f7f70bfc0b0f0f8f0ffffffff
f8f0fffffffffffffffffffff8fff0f0f0f0f0f0f0f0f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7
f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffff0f8ef4048402f30201f271020371f2f4020203f201f30101f2f101f1f0f3f3f3080807fd0d7cffffff0fffff0ffff
fffffffffffffffffffffffffffffffffff8fff0f7fffff7fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000
c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0fff0f0f0fff8fffff8f0eff0efe0efe08f978f4f50400f180f0f1f0f20371f20371f1f281010200f3038207f7f70dfe0d0fffff0fffff0
f0f7efe0e8e0eff0effffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f
070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0f0efe8efefeff0fffffffffffffffffffffff0d0e0d07f8070202f1f101f0f20301f2038202f382030402fafaf9fefefdffffff0ff
fff0efefe0fffff0fffffffffffffffffff0f8f0f0f0f0f0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0
c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0f0f7f0f0f7ffffffffffffffffffffeff0efdfe0dfafb0a060685f2f302020301f2f3820203720303f2f40503feff7e0eff0
e0f0f7effffff0fffff0fffff0fffffffffffffffffff0f7f0f0f0f0fff8fffff8fffff8fffff8fff0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ff
ffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f8ffffffffffffffffffffeff0efb0b8b07078704048402f302f0f170f0f1000202f202f3720101f0f101f0f2f3720
7f8770cfd0c0fffff0fffff0efefe0efefe0f0f0efefefeffffffffff8f0f0f7f0fff8fffff8fffff8fff0f7ffe0e7effff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8
ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffe0e0e0f0f0f0fffffffffffffff8fff0f7f0fffffffffffffff8ffe0e0e0fffffff0f7f0f0f7f0fffffff0f0f0ffffffffffffeff7f0b0b7b07f87803f3f3f2f302f0008003f403fbfc7bfc0cfc08f97801f1f10404f3f80887040
4f3f4f504010180f30372ffffff0fffff0fffff0fffff0fffff0fffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000
bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f0f0fff8fffffffffffffffff8fff0f7f0fff8fffffffff0f7f0fffffff0f0f0efefefffffffffffffefe8efd0d8df6f706f2f302f2f372f606860909f909fa79f303f308f908ffffff0e0e8df909f903f4030bfc0
b0fffff0efefe0f0ffef8f90804f4f4070776fdfe0dfe0e8dffffff0f0f7efeff0effff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f
070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffff8fff0f7f0fff8ffffffffffffffffffffe0e0e0f0f7f0fffffffff8ffafafaf1f1f1f303830606f60cfd8cfe0f0e0f0fff0e0f0e04f574fbfc7bfb0b7af808f8040483f
40473fefefe0fffff0f0f7eff0f8eff0f7e0c0c7b040473f7f7f70eff0e0fffff0efefeffffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8
bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffff8fffff8ffffffffffffffe0e0e0fffffffffffffff8ffd0cfd04f4f4f0f080f6f686fe0f0efeffff0f0fff0eff8eff0fff0f0fff02f372f60685f5058505f
5f500f0f0f80887ffffff0fffff0fffff0fffff0fffff0fffff0afb0a040403fc0c8bffffff0fffffffffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0fffffffffffffff8ffffffff
efefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffff0f0f0efefefb0b0b0201f20000000707070efefeff0fff0f0fff0dfefdfeffff0eff8ef809080000f007f87
7feff7efdfe0d03f403fe0e0dffff8f0e0e0dffff7eff0f7efefefe0f0f8effffff070786f70776fe0e8dfffffffeff0effff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0ffffffffffffff
f8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0f0f7f0f0f7f0fff8fffffffffffffffffffff0f7f0ffffffc0c7c0302f303037303f383f404740dfdfdfffffffcfd8cfeffff0e0f8efc0d0c0607060101f10
20281feff7eff0fff09fa79f30372fefe8e0f0f0effff8f0fffff0fffff0f0f8efeff7e0fffff0d0d7cf90908ffffff0efefeffffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffff0f7f0ffff
fffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffff8fffff8ffffffffffffffffffffefe8efdfd8dfa0a0a04f484f303030b0b0b06f686f404040fffffffffffff0fff0bfd0bf5f685f1f2f201f
281f5067505f675fe0efdfe0efe04f504f90908ffffff0ffffffffffffffffffefe8e0fffff0fffff0fffff0fffff0efefe0fffff0e0e7dffffffffff8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffffffff
f0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0fffffffffffffffffffffffffffffffffffffffffffffffffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffff0f7f0dfd8dfc0c0c01f171f7f777ff0f7f0f0f7f05f575f6f686fffffffeff0efc0d0c04f5f505060
506f7870808f80afb7af6f776ffffff0e0e8e01f1810f0f7effffffffff8f0f0efeffff8f0fffffffffff0e0e7dff0f8f0fff8f0fffff0e0e8e0fffffff0f8f0fff8f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0f0f7f0f0f0f0f0f7f0fff8ffdfd8df8080802f282f9f9f9ff0f7f0fffffff0f7f04f484fdfd7dfffffffafafaf404040
afafaff0f8f0707770c0c7c09090908f8f8fe0e0e07f7f7fafa8a0dfdfdffffffffffffffff7f0fff7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0ffffffffffffefe8efb0b0b09f9f9fb0afb0f0f0f0ffffffffffffd0d0d04f484fdfd8dfd0d0d05f
575fb0afb0dfd8dfe0e0e0606760d0d0d0c0c7c0c0c7c0ffffffafafafcfc8cfefefeffffffffff8fff0f7f0fff8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefefffffffffffffffffffd0d7d0cfcfcfe0e0e0fffffffffffff0f7f0efe8efafafaf5f575ffff8
ffd0d7d0706f70fffffffff8ffcfcfcf6f686fefe8effff8fff0f0f0ffffffe0e7e0f0f7f0fffffffffffff0f7f0f0f0f0fff8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefe8effffffffffffff0f7f0f0f0f0fffffffffffffff8fff0f7f0e0e7e0f0f7f0a0a0a0
606760ffffffffffffdfdfdffffffffff8ffd0d7d09f9f9ffffffffffffff0f7f0efe8effffffffffffffffffffffffff0f7f0f0f0f0fff8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefe8efefe8effffffffffffffffffff0f7f0fffffffffffffff8ffff
ffff9f989f6f6f6ffff8ffffffffffffffe0e0e0fff8fff0f7f0dfdfdffffffff0f7f0f0f0f0e0e7e0f0f7f0fffffffffffffffffffffffff0f7f0f0f7f0fff8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8fff0f7f0f0f7f0f0f7f0f0f0f0fff8ffffffffffff
ffffffffefefef908f908f8f8ff0f7f0f0f7f0f0f0f0efe8efffffffffffffffffffffffffefefefffffffffffffefe8eff0f0f0fff8fffffffffffffffffffffff8fff0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f0f0ffffffffffffefe8efe0e7e0ffffffffffff
fff8fff0f7f0ffffffe0e7e0a0a7a0c0c0c0fffffffffffff0f0f0fff8fffffffff0f7f0fffffffff8fff0f0f0fffffffffffff0f0f0f0f7f0fff8fffffffffffffffffffffffffffff8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fff8ffffffffc0c0c00000000f070f000000c0c0c0f0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f0f0f0f0f0ffffffffffffff
fffff0f7f0f0f7f0efe8efffffffffffffd0d0d0efefeffffffffffffffffffffff8fff0f7f0dfd7dffffffffffffffff8ffffffffe0e0e0fffffffffffffff8fffff8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f7f0fffffffffffffff8ffffffffefefefbfb8bf0000000f070f000000bfbfbffff8ff00fffffffff8ffbfb8bf000000000000101010b0b7b0fffffffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fff0f7f0f0f0f0f0f7f0fffffffffffffff8ffe0e0e0cfcfcf0000000f070f000000cfc8cfffffff00f0f0f0f0f0f0c0c0c00f080f0f080f1010108f878fafa8afbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbf
b8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8
bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bf
bfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbf
b8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8
bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb8bfbfb7bfb0b7b0b0b7b0bfb8bfc0c0c0bfb8bfafa8af9f989f0f080f000000000000bfb8bfefefef00ffffffffffffc0c7c00f070f0000000000001010100f070f0000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f070f0000000000000f070f0f080f0f080f0000000000001f171f000000000000cfc8cfffffff00ffffffffffffbfb7bf100f100000000000001010100000000f080f0f080f
0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f
080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f08
0f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f
0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f
080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0f080f0000000000000000000000000000000000000000000000000f070f000000000000cfcfcfffffff00fffffffff8ffb0b0b02f282f1010100f080f100f1000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f080f100f10000000101010afafafdfd8df00f0f7f0ffffffe0e7e0b0b7b0bfb8bfbfbfbfbfb8
bfb0b7b0bfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbf
bfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbf
bfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbf
bfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbf
bfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfbfb0b0b0bfb7bfbfb8bfbfb8bfb0b0b0b0b0b0bfb8bfc0bfc0bfbfbfbfbfbfb0b0b0ffffffffffff00ffffffffffffffffffffffffffffff
fff8ffe0e7e0f0f7f0fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8
fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ff
fff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8ffff
f8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffff8fffffffffffffffffffffffffffffffffffffffffffffffffff0f0f0fffffff0f0f0f0f7f0ffffff00efefeff0f0f0efe8efff
fffffffffffffffff0f0f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f0f0fffffffffffffffffffff8fff0f0f0f0f0f0fff8ffe0e0e0ffffffffffffdfdfdfffffff000300000000
00}
</xsl:template>

<!-- ================================================  -->
<!-- Primary borrower's address on separate lines                                    -->
<!-- ================================================  -->
<xsl:template name="PrimaryBorrowerAddress">
	<xsl:for-each select="//Deal/Borrower">
		<xsl:if test="./primaryBorrowerFlag='Y'">  
			<xsl:for-each select="./BorrowerAddress">
				<xsl:if test="borrowerAddressTypeId='0'"><!-- current -->
					<xsl:value-of select="./Addr/addressLine1"/><xsl:text>\line </xsl:text>
					<xsl:if test="./Addr/addressLine2 and string-length(./Addr/addressLine2)>0">
						<xsl:value-of select="./Addr/addressLine2"/><xsl:text>\line </xsl:text>
					</xsl:if>
					<xsl:value-of select="./Addr/city"/><xsl:text>, </xsl:text><xsl:value-of select="./Addr/province"/><xsl:text> </xsl:text>
					<xsl:value-of select="concat(./Addr/postalFSA,' ', ./Addr/postalLDU)"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
   <xsl:template name="BorrowersListedBlocks">
	<xsl:for-each select="//Deal/Borrower">
		<xsl:if test="./borrowerTypeId=0">
			<xsl:text>{</xsl:text>
			    <xsl:call-template name="BorrowerFullName"/>
			<xsl:text>}</xsl:text>
			<xsl:if test="position() !=last()">\line </xsl:if>
		</xsl:if>			
	</xsl:for-each>
   </xsl:template>   
   
<!-- ================================================  -->    
<xsl:template name="GuarantorNames">
	<xsl:for-each select="//Deal/Borrower">
		<xsl:if test="./borrowerTypeId=1">
			<xsl:text>{</xsl:text>
			    <xsl:call-template name="BorrowerFullName"/>
			<xsl:text>}</xsl:text>
			<xsl:if test="position() !=last()">\line </xsl:if>
		</xsl:if>			
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->    
  <xsl:template name="BorrowerFullName">
    	<xsl:if test="salutation"><xsl:value-of select="salutation"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="borrowerFirstName"/><xsl:text> </xsl:text>
	<xsl:if test="borrowerMiddleInitial"><xsl:value-of select="borrowerMiddleInitial"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="borrowerLastName"/>     
</xsl:template>

<!-- ================================================  -->
<xsl:template name="PrimaryBorrowerName">
  <xsl:for-each select="//Deal/Borrower">
	<xsl:if test="./primaryBorrowerFlag='Y'">  
		<xsl:text>{</xsl:text>
	  		<xsl:call-template name="BorrowerFullName"/>
		<xsl:text>}</xsl:text>
  	</xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="DealUnderwriter">
	<xsl:for-each select="//Deal/UserProfile">
		<xsl:if test="./userProfileId=//Deal/underwriterUserId">
			<xsl:value-of select="concat(./Contact/contactFirstName,' ', ./Contact/contactLastName)"/>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
<!-- Source Of businessProfile 											         -->
<!-- ================================================  -->

<xsl:template name="BrokerAddress">
	<xsl:for-each select="//Deal/SourceOfBusinessProfile/Contact">
		<xsl:value-of select="./Addr/addressLine1"/><xsl:text>\line </xsl:text>
		<xsl:if test="./Addr/addressLine2">
			<xsl:value-of select="./Addr/addressLine2"/><xsl:text>\line </xsl:text>
		</xsl:if>
		<xsl:value-of select="./Addr/city"/><xsl:text>, </xsl:text>
		<xsl:value-of select="./Addr/province"/><xsl:text> </xsl:text>
		<xsl:value-of select="concat(./Addr/postalFSA, ' ',./Addr/postalLDU) "/><xsl:text> </xsl:text>		
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
<!-- Current user                        											         -->
<!-- ================================================  -->

<xsl:template name="CurrentUserAddress">
	<xsl:for-each select="//specialRequirementTags/currentUser/Contact">
		<xsl:value-of select="./Addr/addressLine1"/><xsl:text>\line </xsl:text>
		<xsl:if test="./Addr/addressLine2">
			<xsl:value-of select="./Addr/addressLine2"/><xsl:text>\line </xsl:text>
		</xsl:if>
		<xsl:value-of select="./Addr/city"/><xsl:text>, </xsl:text>
		<xsl:value-of select="./Addr/province"/><xsl:text> </xsl:text>
		<xsl:value-of select="concat(./Addr/postalFSA, ' ',./Addr/postalLDU) "/><xsl:text> </xsl:text>		
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
 <!-- Subject property, English formatting of address, one line -->
<xsl:template name="SubjectPropertyLines">
	<xsl:for-each select="//Deal/Property">
		<xsl:value-of select="propertyStreetNumber"/><xsl:text>\caps  </xsl:text><xsl:value-of select="propertyStreetName"/><xsl:text> </xsl:text>
		<xsl:if test="streetType"><xsl:text>\caps </xsl:text><xsl:value-of select="streetType"/><xsl:text> </xsl:text></xsl:if>
		<xsl:if test="streetDirection"><xsl:text>\caps </xsl:text><xsl:value-of select="streetDirection"/></xsl:if>
		<xsl:if test="unitNumber">
			<xsl:if test="string-length(unitNumber) > 0">
				<xsl:text>\caps , unit </xsl:text><xsl:value-of select="unitNumber"/>
			</xsl:if>	
		</xsl:if>
		<xsl:text>\line </xsl:text>
		<xsl:text>\caps </xsl:text><xsl:value-of select="concat(propertyCity,', ',province)"/><xsl:text>\line </xsl:text>
		<xsl:text>\caps </xsl:text><xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>	
	</xsl:for-each>	
</xsl:template>

<!-- ================================================  -->
<xsl:template name="FormatPhone">
	<xsl:param name="pnum"/>
	<xsl:if test="string-length($pnum)=10">
		<xsl:text>(</xsl:text>
		<xsl:value-of select="substring($pnum, 1, 3)"/>
		<xsl:text>) </xsl:text>
		<xsl:value-of select="substring($pnum, 4, 3)"/>
		<xsl:text>-</xsl:text>	
		<xsl:value-of select="substring($pnum, 7, 4)"/>
	</xsl:if>		
</xsl:template>

<!-- ================================================  -->
<xsl:template name="FormatLTV">
	<xsl:variable name="temp">
		<xsl:value-of select="substring-before(//Deal/combinedLTV,'%')"/>
	</xsl:variable>
	<!-- // debug
	<xsl:text>temp = </xsl:text><xsl:value-of select="$temp"/>
	<xsl:text>\line </xsl:text>
	-->
	<xsl:if test="contains(//Deal/combinedLTV, '%')">
		<xsl:value-of select="concat(substring($temp, 1, string-length($temp)-1),'%')"/>
	</xsl:if>
</xsl:template>

<!--  ============================= SolicitorName ====================================== -->
<xsl:template name="SolicitorName">
	<xsl:for-each select="//Deal/PartyProfile">
		<xsl:if test="./partyTypeId='50'">		
			<xsl:text>{</xsl:text>
				<xsl:value-of select="./Contact/contactFirstName"/><xsl:text> </xsl:text>
				<xsl:if test="./Contact/contactMiddleInitial"><xsl:value-of select="./Contact/contactMiddleInitial"/><xsl:text> </xsl:text></xsl:if>
				<xsl:value-of select="./Contact/contactLastName"/>
			<xsl:text>}</xsl:text>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!--  ============================= SolicitorAddress ====================================== -->
<xsl:template name="SolicitorAddress">
	<xsl:for-each select="//Deal/PartyProfile">
		<xsl:if test="./partyTypeId='50'">		
			<xsl:text>{</xsl:text>
				<xsl:value-of select="./Contact/Addr/addressLine1"/><xsl:text>\line </xsl:text>
				<xsl:if test="./Contact/Addr/addressLine2"><xsl:value-of select="./Contact/Addr/addressLine2"/><xsl:text>\line </xsl:text></xsl:if>
				<xsl:value-of select="./Contact/Addr/city"/><xsl:text>, </xsl:text>
				<xsl:value-of select="./Contact/Addr/province"/><xsl:text>\line </xsl:text>
				<xsl:value-of select="concat(./Contact/Addr/postalFSA,' ', ./Contact/Addr/postalLDU)"/>
			<xsl:text>}</xsl:text>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="SolicitorPhoneAndFax">
	<xsl:for-each select="//Deal/PartyProfile">
		<xsl:if test="./partyTypeId='50'">		
			<xsl:text>{Phone: \tab </xsl:text>
				<xsl:call-template name="FormatPhone">
					<xsl:with-param name="pnum" select="./Contact/contactPhoneNumber"/>
				</xsl:call-template>	
				<xsl:if test="./Contact/contactPhoneNumberExtension and string-length(./Contact/contactPhoneNumberExtension)!=0">
					<xsl:text> Ext. </xsl:text><xsl:value-of select="./Contact/contactPhoneNumberExtension"/>
				</xsl:if>
				<xsl:text>\line </xsl:text>
				<xsl:if test="./Contact/contactFaxNumber">
					<xsl:text>Fax: \tab </xsl:text>
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="./Contact/contactFaxNumber"/>
					</xsl:call-template>	
				</xsl:if>
			<xsl:text>}</xsl:text>			
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- ************************************************************************ -->
<!-- rtf file start and rtf file end                                                                    -->
<!-- ************************************************************************ -->
<!-- ================================================  -->
<xsl:template name="RTFFileEnd4">  
	<xsl:text>}}</xsl:text>
</xsl:template>  

<!-- ================================================  -->
<!-- letter size  (216 × 279 mm or  612pt  x 792pt); margings: top=36pt, bottom =36pt, left=21.6pt, right=21.6pt-->
<xsl:template name="RTFFileStart4">  
<xsl:text>
{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\info {\author Zivko Radulovic, Catherine Rutgaizer}{\operator filogix automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company filogix inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \paperw12240 \paperh15840 \margl432\margr432\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
</xsl:text>	
</xsl:template>  
	
</xsl:stylesheet>  
