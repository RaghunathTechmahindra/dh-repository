<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
  26/Jan/2006 DVG #DG386 #2685  Open  Solicitor Package - Total Loan Amount  
	28/Oct/2005 DVG #DG350 #2304  Solicitor package/Commitment Letter text changes  
	05/Oct/2005 DVG #DG330 #2199  Solicitor Package (Lender profile)  
	06/Sep/2005 DVG #DG308 #2027  Change to the solicitor package  
	10/Aug/2005 DVG #DG288 #1888  Solicitor package changes  
	05/Aug/2005 DVG #DG286  #1861  - improvements for Merix included here too
	13/Jul/2005 DVG #DG258 #1793  Add Fee Tag to next build  #1791
	24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package 
	- Many changes to become the Xprs standard	-->
	<!--  Import all of the logic from the Commitment Letter template so that the 2 documents 
	(standalone C/L and embedded C/L) remain the same -->
	<xsl:import href="CommitmentLetterGENX.xsl"/>
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LanguageEnglish">
				<xsl:call-template name="EnglishFooter"/><!--#DG286 -->
				<xsl:call-template name="EnglishSolicitorInstructions"/>
				<xsl:call-template name="EnglishGuide"/>
				<xsl:call-template name="EnglishCommitmentLetter"/>
				<xsl:call-template name="EnglishRequestForFunds"/>
				<xsl:call-template name="EnglishFinalReport"/>
				<xsl:text>\page </xsl:text><!--#DG288 -->
				<xsl:call-template name="EnglishConfClos"/><!--#DG288 -->
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LanguageFrench">
				<!--				<xsl:call-template name="FrenchTemplate"/>-->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishSolicitorInstructions">
		<!--#DG238-->
		<xsl:call-template name="LogoAddress"/>
		<xsl:text>\pard\highlight{\f1\fs20 THE CLOSING INSTRUCTION WORKSHEET IS NOT INTENDED TO ACT AS A LEGAL DOCUMENT.  IT IS THE LENDER\rquote S RESPONSIBILITY TO ENSURE THAT THE WORKSHEET IS REVIEWED AND AMENDED AS REQUIRED PRIOR TO ITS ISSUANCE.\highlight0}
<!--#DG288 {\f1\fs24  \f2\fs18 _______________________________________________________________________________________________________\f1\fs24\par}--></xsl:text>
		<xsl:call-template name="HorizLine"/>
		<xsl:text>\tab\tab\tab\tab\tab\tab\tab\tab\tab\tab\tab
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CurrentDate"/>
		<xsl:text>
\par }
\pard \ql<!--#DG288 --> \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18
\par 
\par </xsl:text>
		<!--#DG288 -->
		<xsl:call-template name="Solicitor"/>
		<xsl:text>
\par 
\par Dear  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>,\par 
\par }

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 RE:\cell BORROWER(S):\cell </xsl:text>
		<xsl:for-each select="//SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trbrdrh
\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, if any:\cell 
</xsl:text>
		<xsl:for-each select="//SolicitorsPackage/GuarantorNames">
			<xsl:value-of select="./Name"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell REFERENCE NUMBER: \cell </xsl:text>		<!--#DG238-->
		<!-- xsl:value-of select="//SolicitorsPackage/ReferenceNum"/-->
		<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell SECURITY ADDRESS:\cell 
</xsl:text><!--#DG288 -->
		<xsl:call-template name="PropertyAddr"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell CLOSING DATE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell APPROVED LOAN AMOUNT:\cell </xsl:text>
		<!--#DG364 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\pard \qj \widctlpar\tx540\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 {\pict{\*\picprop\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn alignHR}{\sv 1}}{\sp{\sn dxHeightHR}{\sv 30}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fStandardHR}{\sv 1}}{\sp{\sn fHorizRule}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}}\picscalex1053\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag908016038\blipupi1439{\*\blipuid 361f39a6bcdb98f4598df072e3f44282}
010009000003310000000000070000000000050000000b0200000000050000000c02f401e803070000001b04f401e8030000000005000000140200000000050000001302f401e803050000001402f40100000500000013020000e80303000000000000}}

{\f1\fs18 \par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 A Mortgage Loan has been approved for the above-noted Borrower(s), in accordance with the terms and conditions outlined in the Commitment Letter attached. We have been advised that you will be acting for </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> (hereinafter called the \'93Lender\'94) in attending to the preparation, execution and registration of the Mortgage against the property and ensuring that the interests of the Lender as Mortgagee are valid and appropriately secured. Please note that the Lender will not require or approve an interim report on title or draft documentation, including the Mortgage. The Lender will rely solely on you to ensure that the Mortgage is prepared in accordance with the instructions outlined herein. Any amendments required due to errors, omissions or non-compliance on your part will be for your account and your responsibility to correct. The Lender will also rely solely on you to confirm the identity of the Borrower(s) and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, if any, and to retain the evidence used to confirm their identity in your file. The Mortgage may be registered electronically, where available. </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/eRegistration"/>
		<xsl:text>\par \par }

\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 Any material facts which may adversely affect the Lender\rquote s position as a First Charge on the property are to be disclosed to the Lender prior to the advance of any funds. Similarly, any issues which may affect the Lender\rquote s security or any subsequent amendments to the Agreement of Purchase and Sale are to be referred to this office for direction prior to the release of funds. Our prior consent will be required relative to any requests for additional or secondary financing. If for any reason you are unable to register our Mortgage on the scheduled closing date, we must be notified at least 24 hours prior to that date and advised of the reason for the delay. No advance can be made after the scheduled closing date without the Lender\rquote s written authorization.\par \par}

\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 <!-- #DG238 You must submit the Solicitor\rquote s Request for Funds together with the required documentation to our Financial Services Centre }{\f1\fs18\ul no later than three (3) days prior to closing}{\f1\fs18 . The Solicitor\rquote s Request for Funds should contain confirmation that all our requirements have been or will be met and should specifically outline any qualifications that will appear in your Final Report and certification of title.
\par 
\par The mortgage funds will be sent to you by PREPAID courier or by Electronic Fund Transfer and should be disbursed without delay provided you are satisfied that all requirements have been met and adequate precaution has been taken to ensure that the mortgage security will retain priority.  
\par \par -->Legal fees and all other costs, charges and expenses associated with this transaction 
are payable by the Borrower(s) whether or not the Mortgage proceeds are advanced.
\par }
\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
{\f1\fs18 In connection with this transaction we enclose the following documents:
\par 
\par 
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Mortgage Commitment Letter}{\f1\b0  - }
{\f1\b0 Please ensure that any outstanding conditions outlined in the Commitment Letter that are the responsibility of 
the Solicitor/Notary, are duly noted and that full compliance is confirmed prior to advance and included in your final report.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Solicitor/Notary\rquote s Instructions and Guidelines}<!-- #DG238 {\f1\b0  - }{\f1\b0 Self explanatory}-->\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
<!-- #DG238 \pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Mortgage}{\f1\b0  - </xsl:text>
		<xsl:for-each select="//SolicitorsPackage/ProvincialClause/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Schedules}{\f1\b0 - Schedule A\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Disclosure Statement\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}-->
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Solicitor/Notary\rquote s Request for Funds\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
<!--#DG288 added -> Final report on Title-->
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Solicitor/Notary\rquote s Final report on Title\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}

\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Confirmation of Closing\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}

}
{\f1\fs18 \par }
{\f1\fs18 Other required security/Special Conditions:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SolicitorSpecialInstructions"/>
		
		<xsl:text>
\par
\par
{\pard
\b\f1\fs18 You must access our website {\ul\b0 http://www.bridgewaterbank.ca/solicitordocs} to obtain our current Solicitors Instructions and the Mortgage and other Forms that you must use to complete this transaction.  The Instructions and Forms on our website supplement and override this letter and enclosures.
\par}
		</xsl:text>
<!--
		<xsl:text>
\par
\par
{\pard
\brdrt \brdrs \brdrw10 \brsp20
\brdrl \brdrs \brdrw10 \brsp80
\brdrb \brdrs \brdrw10 \brsp20
\brdrr \brdrs \brdrw10 \brsp80
\b\f1\fs18 You must access our website http://www.bridgewaterbank.ca/solicitordocs to obtain our current Solicitors Instructions and the Mortgage and other Forms that you must use to complete this transaction.  The Instructions and Forms on our website supplement and override this letter and enclosures.
\par}
		</xsl:text>
-->
		
		<xsl:text>
\par }

{\f1\fs18\par All mortgages are to be registered in the name of </xsl:text>

		<!--#DG330 rewritten
		<xsl:value-of select="//SolicitorsPackage/LenderName"/-->
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/RegisteredName">
				<xsl:value-of select="//SolicitorsPackage/RegisteredName"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>				
		
	 	<xsl:text>. Our address for service is </xsl:text>
	 	
		<!--#DG330 rewritten-->
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/RegisteredAddress">

				<xsl:for-each select="/*/SolicitorsPackage/RegisteredAddress/Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text> </xsl:text>
					</xsl:if>
				</xsl:for-each>				

			</xsl:when>
			<xsl:otherwise>

		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line1">
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		
			</xsl:otherwise>
		</xsl:choose>				
		
		<xsl:text>.  You can contact our office, if you require a copy of our mortgage schedules and standard charge terms.\par }

{\f1\fs18\par }
\pard\plain \s22\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 We consent to your acting for the Lender as well as the Borrower(s) and/or </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> provided that you disclose this fact to the Borrower(s) and/or </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> and obtain their consent in writing and that you disclose to each party all information you possess or obtain which is or may be relevant to this transaction. 
\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
<!--#DG238 \pard\plain \s2\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\b\f1\fs18\ul FINANCIAL SERVICES CENTRE\par }-->
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18\par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\f1\fs18 <!--#DG238 Documents and funding requests should be directed to:-->\b Final Reports and supporting documentation should be mailed to:} \par \par 
{\f1\fs18 </xsl:text>

		<!--#DG330 rewritten
		<xsl:value-of select="//SolicitorsPackage/LenderName"/-->
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/RegisteredName">
				<xsl:value-of select="//SolicitorsPackage/RegisteredName"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>		
		
		<xsl:text>
\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 </xsl:text>

		<!--#DG330 rewritten-->
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/RegisteredAddress">

				<xsl:for-each select="/*/SolicitorsPackage/RegisteredAddress/Line">
					<xsl:value-of select="."/>
					<xsl:text>\par </xsl:text>
				</xsl:for-each>				

			</xsl:when>
			<xsl:otherwise>

				<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/>
				<xsl:text>\par</xsl:text>
				<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/>
				<xsl:if test="//SolicitorsPackage/BranchAddress/Line2">
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/>
				<xsl:text>\par </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/>		
				<xsl:text>\par </xsl:text>
		
			</xsl:otherwise>
		</xsl:choose>				
		
		<xsl:text>}
{\f1\fs18\b <!--#DG238 \par Contact:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>\par -->Attention:  Servicing Department\par}
{\f1\fs18 </xsl:text>

		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/RegisteredPhoneFax">

				<xsl:for-each select="/*/SolicitorsPackage/RegisteredPhoneFax/Line">
					<xsl:value-of select="."/>
					<xsl:text>\par </xsl:text>
				</xsl:for-each>

			</xsl:when>
			<xsl:otherwise>

				<xsl:text>Phone:\tab </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/BranchAddress/Phone"/>
				<xsl:text> Ext. </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/FunderExtension"/>
				<xsl:text>\par Fax:\tab </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
				<xsl:text>\par </xsl:text>
		
			</xsl:otherwise>
		</xsl:choose>				
		<xsl:text>}		
\pard \ql \fi-360\li360\ri0\widctlpar\aspalpha\aspnum\faauto\ilvl12\adjustright\rin0\lin360\itap0 
{\f1\fs18 \par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 If you have any question or concerns, please feel free to call.
\par 
\par Sincerely,\tab \tab 
\par 
\par 
\par 
\par 
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>\par <!--#DG238 Financial Services Administrator}--></xsl:text>		
		<!--xsl:value-of select="//SolicitorsPackage/Administrator/userDescription"/-->
		<xsl:text>}</xsl:text>
	</xsl:template>
	
	<xsl:template name="EnglishGuide">
		<xsl:text>\par{<!--#DG286 \sectd-->\sect}
\pard\plain \s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\b\ul\f1\fs28 SOLICITOR/NOTARY\rquote S GUIDE}
{\f1\fs18\par\par\par }</xsl:text>

		<!--#DG308 rewritten  --> 
		<xsl:if test="//SolicitorsPackage/OpenStatem">
			<xsl:text>\pard\plain \s16\qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs18 <!--#DG308 If you are satisfied that the Mortgagor(s) (also known as the \'93Borrower(s)\'94) has or will acquire a good and marketable title to the property described in our Commitment Letter, complete our requirements as set out below, and prepare a Mortgage as outlined herein.--></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/OpenStatem/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

<!--#DG238 \pard\plain \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
{\b\f1\fs18\ul Documentation:}{\f1\fs18  The Mortgage Form Number identified in our cover letter together with the applicable Schedule(s) must be registered. }
{\f1\fs18 \par \par }
{\b\f1\fs18\ul Mortgage Requirements:}{\f1\fs18  The Mortgage Document is to be registered as follows:\par \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Approved Loan Amount:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Interest Rate (Compounded </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CompoundingFrequency"/>
		<xsl:text>):\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Principal and Interest Payment:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PandIPaymentMonthly"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Interest Adjustment Date:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/IADDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 First Monthly Payment Date:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FirstPaymentDateMonthly"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Maturity/Renewal Date:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}\par-->

		<xsl:if test="//SolicitorsPackage/BorIdTxt">
			<xsl:text>\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Borrower Identification:}{\f1\fs18  <!--#DG308 You are required to confirm the identity of the Borrower(s) and Guarantor(s), if any.  The Lender also requires you to forward a photocopy of the evidence used to confirm their identity together prior to your release of funds. --></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/BorIdTxt/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/FundingTxt">
			<xsl:text>{\b\f1\fs18\ul Funding:}{\f1\fs18  <!--#DG308 The mortgage funds will be made available to you no later than 24 hours before the scheduled closing and should be disbursed without delay provided you are satisfied that all requirements have been met and adequate precaution has been taken to ensure that the mortgage security will retain priority. Interest will accrue from this date unless the Bank receives written notification from your office at least 24 hours prior to the scheduled closing date.  If the Bank does not receive adequate notice from your office, you assume responsibility for the accrued interest. \par \par
If the closing of this transaction is delayed more than 5 business days, all funds must be returned to our office immediately.--></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/FundingTxt/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/ConfirmClosing">
			<xsl:text>{\b\f1\fs18\ul Confirmation of Closing:}{\f1\fs18  <!--#DG308 We require that you complete and return the enclosed Confirmation of Closing within 24 hours of the scheduled closing date to confirm that this transaction has closed, that funds were disbursed. --></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/ConfirmClosing/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/TitleInsReq">
			<xsl:text>{\b\f1\fs18\ul Title Insurance Requirement:}{\f1\fs18  <!--#DG308 If this is a non-purchase mortgage transaction in any province or a purchase transaction in Ontario, the Lender requires the Borrower to purchase a lender title insurance policy.  Please contact our office to ensure the Title Insurance Firm is acceptable to us. --></xsl:text>

			<xsl:for-each select="/*/SolicitorsPackage/TitleInsReq/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/Searches">
			<xsl:text>{\b\f1\fs18\ul Searches:}
<!--#DG238 {\f1\fs18  You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Execution"/>
		<xsl:text>. It is your responsibility to ensure that there are no work orders or deficiency notices outstanding against the property, that all realty taxes and levies which have or will become due and payable up to the interest adjustment date are paid to the Municipality. If your search reveals title has been transferred since the date of the Offer to Purchase and before the closing date you must notify us immediately and discontinue all work on this transaction unless notified otherwise. Immediately prior to registration of the mortgage, you are to obtain a Sheriff\rquote s Certificate/General Registry Search indicating that there are no writs of execution on file against the mortgagor(s), </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>(s), if any, or any previous owner, which would adversely affect our security.
\par }-->
{\f1\fs18  <!--#DG308 You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. Complete searches on a timely basis to ensure there are no executions against the applicant(s). It is your responsibility to ensure that there are no work orders or deficiency notices outstanding against the property that all realty taxes and levies which have or will become due and payable up to the interest adjustment date are paid to the Municipality. If your search reveals title has been transferred since the date of the Offer to Purchase and before the closing date you must notify us immediately and discontinue all work on this transaction unless notified otherwise.--></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/Searches/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/SurveyReq">
			<xsl:text>{\b\f1\fs18\ul Survey Requirements:}{\f1\fs18  <!--#DG308 You must obtain and review a survey or a surveyor's certificate / Real Property Report (AB, NL, and SK)/Certificate of Location (PQ) completed by a recognized land surveyor and dated within the last twenty (20) years. Satisfy yourself from the survey that the position of the buildings on the land complies with all municipal, provincial and other government requirements. Where an addition has been made since the date of the survey, an updated survey is required unless there is no doubt that the addition is also clearly within the lot lines and meets all setback requirements. If you are unable to comply with our survey requirements, please contact our office as soon as possible. --></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/SurveyReq/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/CondoStrataTitle">
			<xsl:text>\pard \qj \widctlpar\tx1800\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Condo/Strata Title:}{\f1\fs20  <!--#DG238 You are to confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. You are to review the Condominium Corporation\rquote s Declaration and Bylaws and confirm they contain nothing derogatory to our security. You are to assign the voting rights to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> (if applicable).--> 
<!--#DG308 You are to review the condominium/strata documentation to ensure that it is satisfactory and identify all requirements applicable to us (including notice provisions) which are to be set out in your Report on Title and obtain a clear Estoppel certificate and certificate of insurance. --></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/CondoStrataTitle/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>
		
<!--#DG238 \pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs18 \par} 
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Survey:}{\f1\fs18  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SurveyClause"/>
		<xsl:text>. \par\par }-->

		<xsl:if test="//SolicitorsPackage/FireInsur">
			<xsl:text>\pard \qj \widctlpar\tx1440\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Fire Insurance:}{\f1\fs18  <!--#DG308 You must ensure, prior to advancing funds, that fire insurance coverage for building(s) on the property is in place for not less than their full replacement value with first loss payable to </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text> as Mortgagee at the address shown on the Charge/Mortgage and including a duly completed standard mortgage clause approved by the Insurance Bureau of Canada. Policies must not contain a co-insurance or similar clause that may limit the amount payable. Unless otherwise noted, we will not require a copy of this policy.--></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/FireInsur/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/NewConstructionTxt">
			<xsl:text>{\b\f1\fs18\ul New Construction:}{\f1\fs18  <!--#DG308 If this transaction is a new construction, you must ensure that an acceptable Certificate of Completion and New Home Warranty Certificate are obtained prior to advancing the mortgage proceeds. --></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/NewConstructionTxt/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

<!--#DG238 {\b\f1\fs18\ul Matrimonial Property Act:}
{\f1\fs18  You are to certify that all requirements under the Matrimonial Property Act in your province, where applicable, have been met and that the status does not in any way affect our mortgage/charge.\par \par \par }-->

		<xsl:text>\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18\ul Mortgage Advances:}{\f1\fs18  When all conditions precedent to this transaction have been met, funds in the amount of  </xsl:text>
		<!--#DG364 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>
		<xsl:text> will be advanced with the following costs being deducted from the advance by the Lender;\par }
\pard \qj \fi720\li720\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin720\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Mortgage Insurance Premium\par (Included in Total Loan Amount)\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Premium"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Provincial Sales Tax on Premium\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
		<xsl:for-each select="//SolicitorsPackage/Fees/Fee">
			<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 TOTAL\tab \cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TotalDeductionsAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Interest Adjustment (daily per diem)\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PerDiemInterestAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par <!--#DG238 The net Mortgage proceeds, payable to you, in trust, will be sent to your office prior to the scheduled closing date, which is anticipated to be </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>. 
-->
The net Mortgage proceeds in the amount of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/NetAdvanceAmount"/><!--#DG258 -->
		<xsl:text>, payable to you, in trust, will be available for pick up five (5) days prior to the scheduled closing date at the address shown on the Charge/Mortgage.\par \par }</xsl:text>
		<!--  Internal refi -->
		<xsl:if test="//SolicitorsPackage/InternalRefi">
			<xsl:text>
\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0
{\b\f1\fs18\ul Refinances:}{\f1\fs18 If the purpose of this Mortgage is to refinance existing debt with </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text>, we will retain the following additional funds to discharge existing mortgage(s) owing to us;\par }
\pard \qj \keep\keepn\fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Existing </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b Other Debts to be Paid:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
</xsl:text>
			</xsl:for-each>
			<xsl:text>\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance, if any, is to be paid to the Borrower(s).
\par }
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\par \par}
</xsl:text>
		</xsl:if>
		<!--  External refi -->
		<xsl:if test="//SolicitorsPackage/ExternalRefi">
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 \ul Refinances:}{\f1\fs18  If the purpose of this Mortgage is to refinance existing debt, the balance
 of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Existing </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LPDescription"/>
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b Other Debts to be Paid:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance, if any, is to be paid to the Borrower(s).
\par \par \par}</xsl:text>
		</xsl:if>
		
		<xsl:if test="//SolicitorsPackage/ConstructionProgrAdv">
			<xsl:text>{\b\f1\fs18\ul Construction/Progress Advances:}{\f1\fs18  <!--#DG238 If the security for this Mortgage is a new property and we a
re advancing by way of progress advances, the advances will be done on a cost-to-complete basis.-->
<!--#DG308 If the security for this Mortgage is a new property and we are advancing by way of progress advances, we will require your written request for each advance together with a direction executed by the Mortgagor(s) directing us to pay the funds to you, in trust, at least five (5) days prior to the advance of funds.
You are responsible for the disbursement of all loan advances made payable to you in trust and to ensure the priority of each advance by conducting all necessary searches and ensure that the requirements of the appropriate Provincial Construction Lien Act have been met before each advance is made. You are to retain appropriate amounts from the Mortgage proceeds to comply with Provincial lien holdback requirements. Unless otherwise noted, </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text> will require a copy of the executed Certificate of Completion and Possession form and, where applicable, the Occupancy Certificate.--></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/ConstructionProgrAdv/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/AssignmentRents">
			<xsl:text>{\b\f1\fs18\ul Assignment Of Rents:}{\f1\fs18  <!--#DG308 If the Mortgage Approval provides for rental of units in the Property, an Assignment of Rents must be included in the Standard Charge Terms.  In addition, </xsl:text> <!- -#DG238 - ->
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text> will require registration of Financing Statement under the Personal Property Security Act for the term of the mortgage. --></xsl:text>		
			<xsl:for-each select="/*/SolicitorsPackage/AssignmentRents/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/Disclosure">
			<xsl:text>{\b\f1\fs18\ul Disclosure:}{\f1\fs18  <!--#DG308 The Mortgagor(s) and the </xsl:text><!- -#DG238 - ->
			<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
			<xsl:text> (if any) must sign a Statement of Disclosure. One copy is to be given to the Borrower(s) and a second copy is to be forwarded to </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text>with the Solicitor/Notary\rquote s Final Report on Title. <!- -#DG238 Please ensure that the applicable provincial legislation and timeframes regarding Disclosure Statements are adhered to.- -> If a Statement of Disclosure cannot be signed, you must obtain a waiver from the Mortgagor(s) and the </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
			<xsl:text> (if any). --></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/Disclosure/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/PlanningAct">
			<xsl:text>\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Planning Act:}{\f1\fs18  <!--#DG308 You must ensure that the Mortgage does not contravene the provisions of the Planning Act as amended from time to time, and that the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.--></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/PlanningAct/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/MatrimonialPropAct">
			<xsl:text>\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Matrimonial Property Act:}{\f1\fs18  <!--#DG308 You are to certify that all requirements under the Matrimonial Property Act in your province, where applicable, have been met and that the status does not in any way affect our mortgage/charge. --></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/MatrimonialPropAct/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/Renew4Guarantors">
			<xsl:text>\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Renewal for Guarantor(s):}{\f1\fs18  <!--#DG308 If applicable, the following wording must be included in a Schedule to the Charge/Mortgage: 
"In addition to the Guarantor's promises and agreements contained in this Mortgage, the Guarantor(s) also agree(s) that at the sole discretion of </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text>, the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s). Any such renewal of this Mortgage binds the Guarantor(s), whether or not the renewal agreement has been signed by the Guarantor(s)." --></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/Renew4Guarantors/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/CorporationAsBor">
			<xsl:text>{\b\f1\fs18\ul\cf1 Corporation as a Borrower:}{\f1\fs18\cf1  <!--#DG308 If the Mortgagor(s) or </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>(s) is a corporation, you must obtain a certified copy of the directors\rquote resolution and the borrowing by-law of the corporation authorizing the Charge/Mortgage and ensure that the corporation is duly incorporated under applicable Provincial or Federal laws with all necessary power to charge its interest in the property and include these documents(s) with the Solicitor\rquote s Final Report on Title.--></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/CorporationAsBor/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>

		<xsl:if test="//SolicitorsPackage/SolicitorRpt">
			<xsl:text>{\b\f1\fs18\ul Solicitor/Notary\rquote s Report:}{\f1\fs18  <!--#DG308 You must submit the Solicitor/Notary\rquote s Final Report on Title, on the enclosed form and with the stated enclosures, within 30 days after the final advance is made. If your report cannot be submitted in this time frame, you must provide us with a letter explaining the reason(s) for the delay. Failure to comply with any of the above instructions may 
result in discontinued future dealings with you or your firm.--></xsl:text>
			<xsl:for-each select="/*/SolicitorsPackage/SolicitorRpt/Line">
				<xsl:value-of select="."/>
				<xsl:text>\par </xsl:text>
			</xsl:for-each>							
			<xsl:text>\par }</xsl:text>
		</xsl:if>
		<xsl:text>{\pard \par \page }</xsl:text>
		<!--#DG308 end -->
	</xsl:template>

	<xsl:template name="EnglishRequestForFunds">
		<xsl:text>\par{<!--#DG286 \sectd-->\sect}</xsl:text>
		<!--#DG238 added whole new table with logo and branch address -->
		<xsl:call-template name="LogoAddress"/>
		<xsl:text>\pard \ql \fi720\li5040\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin5040\itap0 
{\f1\fs18  \par}

\trowd \trgaph108\trrh719\trleft1800
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx8820
\pard\plain \s6\qc \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\ul\f1\fs28 SOLICITOR/NOTARY\rquote S REQUEST FOR FUNDS\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh719\trleft1800
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx8820
\row 
}
\pard \ql \fi720\li5040\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin5040\itap0 
{\f1\fs18\par\par\par}
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 
\cellx4745\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 FAX TO:\cell \b0</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 FROM:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:value-of select="//SolicitorsPackage/SolicitorAddress1"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 
\cellx4745\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ATTENTION:\cell \b0
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>\cell \cell }
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>
}
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 DATE:\cell }{\f1\fs18 ________________\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><!--#DG288 -->
		<xsl:call-template name="SolicitorPhoneAndFax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\pard \ql \widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\b\f1\fs18 {\pict{\*\picprop\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn 
fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}
{\sp{\sn fLine}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}
\picscalex1080\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0
\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag907321276\blipupi1439{\*\blipuid 
36149fbcc681ce015720994253f9dec0}010009000003310000000000070000000000050000000b0200000000050000000c02f401e80307
0000001b04f401e80300000000050000001402000000000500
00001302f401e803050000001402f40100000500000013020000e803030000000000}
\par }
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
<!-- {\b\f1\fs18 MORTGAGE REFERENCE NUMBER:\cell \b0 </xsl:text><xsl:value-of select="//SolicitorsPackage/MortgageNum"/><xsl:text>\cell } -->
{\b\f1\fs18 MORTGAGE REFERENCE NUMBER:\cell \b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/ReferenceNum"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 <!--#DG350 MORTGAGOR-->BORROWER(S):\cell \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MUNICIPAL ADDRESS:\cell \b0 
</xsl:text>
		<!--#DG288 -->
		<xsl:call-template name="PropertyAddr"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGE AMOUNT:\cell \b0 </xsl:text>
		<!--#DG364 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 CLOSING DATE:\cell \b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 {\pict{\*\picprop\shplid1026{\sp{\sn 
shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}\picscalex1080\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag907321276\blipupi1439{\*\blipuid 
36149fbcc681ce015720994253f9dec0}010009000003310000000000070000000000050000000b0200000000050000000c02f401e803070000001b04f401e80300000000050000001402000000000500
00001302f401e803050000001402f40100000500000013020000e803030000000000}}
{\f1\fs18 \par }
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs18 \par }
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 I/We request funds in the amount of </xsl:text>
		<!--#DG364 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>
		<xsl:text> for closing on </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>.}
{\f1\fs18 \par }
\pard \s15\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 I/We acknowledge that there are no amendments and/or schedules to the Agreement of Purchase and Sale which 
affect the purchase price or deposit amount as set out in your Mortgage Commitment Letter.\par }
{\f1\fs18 \par }
{\b\f1\fs18 I/We confirm that:\par \par }

\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 \cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }{\f1\fs18 all terms and conditions set out in your instructions have been satisfied}{\cell }\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }{\f1\fs18 all CMHC / GEMICO terms and conditions have been satisfied}{\cell }\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\pard \ql \widctlpar\faauto\itap0 {
\par }{\b\f1\fs18 DELIVERY OF FUNDS IS REQUESTED AS FOLLOWS}{
\par }\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 \cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }{\f1\fs18 I / We will arrange for pick-up of funds from your office five (5) days prior to the scheduled closing date.}{\cell }\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }{\f1\fs18 
Deposit funds to my/our </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> Trust Account # ______________________ at Branch # __________.}{\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd 
\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 \cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\pard 
\ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }{\f1\fs18 Deliver funds courier collect via _________________________ courier account # _________________.}{\cell }\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }{\f1\fs18 * Wire funds to : 
______________________________________________________________________________}{
\par }{\f1\fs18 ____________________________________________________________________________________________}{
\par }{\f1\fs18 ____________________________________________________________________________________________}{
\par \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth540 \cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\pard \ql \widctlpar\faauto\itap0 {
\par }{\b\f1\fs18 * If this last option is chosen, I/We understand that you will retain sufficient funds to cover all wire costs.}{
\par 
\par }\pard \ql \widctlpar\tx5000\tx7000\aspalpha\aspnum\faauto\adjustright\itap0 {\tab }{\f1\fs18 ___________________________________________________ }{
\par \tab \tab }{\f1\fs18 Signature of Solicitor/Notary}{
\par }\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 { 
\par }
<!--#DG238 {\pard \par \page }
{\f1\fs18 DOCUMENTS REQUIRED PRIOR TO RELEASE OF THE MORTGAGE:}\par\par\pard \ql \widctlpar\tx50\tx200\tx400\aspalpha\aspnum\faauto\adjustright\itap0{\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Pre-Authorized Chequing (PAC) form together with "VOID" cheque } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Signed Statement of Disclosure } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Signed Statutory Declaration } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Well Water Test/Certificate, indicating that the water is potable and fit for human consumption } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Septic Tank Certificate } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 New Home Warranty Certificate of Possession } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 New Home Warranty Builder's Registration Number and Unit Enrollment Number } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Assignment of Condominium Voting Rights } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Assignment of Proceeds } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Signed Authorization to Send Assessment and Tax Bill (#45-06-20) PEI } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Other (specify) _______________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par}\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0{ \par }--></xsl:text>
	</xsl:template>

	<!--#DG286 using the one from cmt template now - ->
	<!- -  We are using the build logic from the commitment letter template that already exists instead of replicating it here... - ->
	<xsl:template name="EnglishCommitmentLetter">
		<xsl:call-template name="EnglishPage1"/>
		<!- -xsl:call-template name="EnglishPage2"/- ->
		<!- -xsl:call-template name="EnglishPage3"/- ->		
		<xsl:call-template name="EnglishPage4"/>
		<xsl:call-template name="EnglishPage5"/>
	</xsl:template-->

	<xsl:template name="EnglishFinalReport">
		<xsl:text>\par{<!--#DG286 \sectd-->\sect}
\trowd \trgaph108\trleft1500\trbrdrt\brdrs\brdrw15\brdrcf1 
\trbrdrl\brdrs\brdrw15\brdrcf1 
\trbrdrb\brdrs\brdrw15\brdrcf1 
\trbrdrr\brdrs\brdrw15\brdrcf1 
\trbrdrh\brdrs\brdrw15\brdrcf1 
\trbrdrv\brdrs\brdrw15\brdrcf1 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw15\brdrcf1 
\clbrdrl\brdrs\brdrw15\brdrcf1 
\clbrdrb\brdrs\brdrw15\brdrcf1 
\clbrdrr\brdrs\brdrw15\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth6500 \cellx8000
\pard \qc \widctlpar\intbl\faauto 
{\par }{\b\f1\fs28\ul SOLICITOR/NOTARY\rquote S FINAL REPORT ON TITLE\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1500\trbrdrt\brdrs\brdrw15\brdrcf1 
\trbrdrl\brdrs\brdrw15\brdrcf1 
\trbrdrb\brdrs\brdrw15\brdrcf1 
\trbrdrr\brdrs\brdrw15\brdrcf1 
\trbrdrh\brdrs\brdrw15\brdrcf1 
\trbrdrv\brdrs\brdrw15\brdrcf1 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw15\brdrcf1 
\clbrdrl\brdrs\brdrw15\brdrcf1 
\clbrdrb\brdrs\brdrw15\brdrcf1 
\clbrdrr\brdrs\brdrw15\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth6500 \cellx8000
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\par \par }
\pard \ql \widctlpar\tx100\tx7000\aspalpha\aspnum\faauto\adjustright\itap0 
{\tab }{\b\f1\fs18 Solicitor/Notary\rquote s Reference No. ____________________________ }{\tab }{\b\f1\fs18 _________________________}
{\par \tab \tab }{\b\f1\fs18 Date}
{\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{ \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth918 \cellx810
\cltxlrtb\clftsWidth3\clwWidth10098 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 TO:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>
\par \par Attention: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdministratorName"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth918 \cellx810
\cltxlrtb\clftsWidth3\clwWidth10098 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\fs18 \par }
{\f1\fs18 In accordance with your instructions, we have acted as your solicitors in the following transactions. We have registered a Mortgage/Charge (or Deed of Loan, if applicable) the "Mortgage" on the appropriate form in the appropriate </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> and make our final report as follows:}
{\fs18 \par \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 <!--#DG350 Mortgagor-->Borrower(s)\cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>\cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Municipal Address of property \cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Legal Address of Property \cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \par }
{\b\f1\fs18 It is our opinion that:}
{\f1\fs18 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (1)\cell A valid and legally binding First Mortgage in favour of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> for the full amount of the monies advanced was registered on _____________________________________________ in the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> Division of _____________________________________________________ as Instrument No. ___________________________.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (2)\cell The <!--#DG350 Mortgagor-->Borrower(s) have good and marketable title in fee 
simple to the property free and clear of any prior encumbrances, other than the minor defects 
listed below which }{\b\f1\fs18 do not}{\f1\fs18  affect the priority of the Mortgage or the 
marketability of the property. All lien holdback/retention period requirements have been met. 
Easements,<!--#DG238--> Encroachments and Restrictions etc. are listed below:
\par ________________________________________________________________________________________________ 
\par ________________________________________________________________________________________________ 
\par ________________________________________________________________________________________________ 
\par ________________________________________________________________________________________________ 
\par ________________________________________________________________________________________________ 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\cell }{\b\f1\fs18 Condominium/strata unit(s) if applicable:}{\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {
\trowd 
\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\f1\fs18 \cell 2 a)\cell We confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. We have reviewed the Condominium Corporation's Declaration and Bylaws and confirm they contain nothing derogatory to your security. We have assigned the voting rights to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>, if applicable.\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell b)\cell All necessary steps have been taken to confirm your right to vote should you wish to do so.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908
\row 
}
<!--
\pard \ql \widctlpar\intbl\faauto {\f1\fs18 \cell c )\cell 
All necessary steps have been taken to confirm your right to vote should you wish to do so.\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908
\row 
}
-->
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\f1\fs18 \cell c)\cell Applicable notice provision, if any: __________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 
\cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (3)\cell All restrictions have been complied with in full and there are no work orders or deficiency notices outstanding against the property.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (4)\cell All taxes and levies due and payable on the property to the Municipality have been paid up to _______________________________________.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (5)\cell The Mortgage does not contravene the provisions of the Planning Act as amended 
from time to time, because the <!--#DG350 Mortgagor-->Borrower(s) do not retain the fee or the 
equity of redemption in, or a power or right to grant, assign or exercise a power of appointment 
with respect to any land abutting the land secured by the Mortgage.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (6)\cell A true copy of the Mortgage (including Standard Charge Terms, all Schedules to 
it, and the <!--#DG350 Mortgagor-->Borrower(s) Acknowledgement and Direction, if applicable) has 
been given to each <!--#DG350 Mortgagor-->Borrower. \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (7)\cell Fire Insurance coverage has been arranged in accordance with your instructions for full replacement cost(s) with loss payable to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> in accordance with the I.B.C. standard mortgage clause.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \par \page \par \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908\pard 
\ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell Insurance Company: ______________________________ 
\par \cell Broker: _______________________________________ 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell Amount of Insurance: _____________________________ 
\par \cell Policy No: _____________________________________ 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell Effective Date: ___________________________________ 
\par \cell Expiry Date: ____________________________________ 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \par \par}
{\f1\fs18\b The following documents are enclosed for your file:}
{\f1\fs18 \par \par }

\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Duplicate registered copy 
of the Charge/Mortgage, or Deed of Loan including Standard Charge Terms, all Schedules to it and 
acknowledgement of receipt of <!--#DG350 Mortgagor-->Borrower(s) and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>(s), if any}\cell \pard \intbl \row\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Electronic Charge and Acknowledgement and Direction<!--#DG238  {\f1\fs18\b (Ontario electronic registration counties only)}-->, if applicable in the Province/Jurisdiction}\cell \pard \intbl \row 
<!--#DG238 \pard \intbl {\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Instrument Number 
{\f1\fs18 (Teranet Registration) }}\cell 
\pard \intbl \row -->

\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl {\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Guarantee Agreement<!--#DG238  {\f1\fs18\b (Ontario electronic registration counties only)}-->, if applicable}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
<!--#DG238 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Certificate of Title (or Provincial Comparable) }\cell \pard \intbl \row -->
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Survey or Surveyor\rquote s Certificate}\cell \pard \intbl \row
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
<!--#DG238 \pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Survey or Surveyor's Certificate or Title Insurance in lieu of a Survey, if applicable }\cell \pard \intbl \row -->
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Title Insurance Policy and Schedules A &amp; B to Policy}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Registered Amending Agreement }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Execution Certificate }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Estoppel Certificate and Certificate of Insurance }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Municipal Tax Certificate}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Certificate of Completion and Possession}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Final Inspection }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 New Home Warranty Certificate of Possession }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Occupancy Certificate/Permit}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Personal Guarantee and Letter of Independent Legal Advice}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Statement of Disclosure}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Certified copy of directors\rquote\~resolution and the borrowing by-law of the corporation authorizing the Charge/Mortgage}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Sheriff's Certificate/GR Search}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Signed Statutory Declaration}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Fire Insurance Policy}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Condominium Corporation Insurance Binder}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Assignment of Site/Lot Lease to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Zoning Certificate/Memorandum (Ontario properties only)}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Declaration as to Possession (Manitoba properties only)}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Registered Assignment of Rents}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 General Security Agreement (GSA) Registered under PPSA}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Statement of Funds Received and Disbursed }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Verification of Payout of Debts }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Copy of lease, if applicable }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Other (specify)}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 _______________________________________________________________________________________________}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell
{\f1\fs18 ]}\cell \pard \intbl
{\f1\fs18 _______________________________________________________________________________________________}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 _______________________________________________________________________________________________}\cell \pard \intbl \row
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908\pard \intbl
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 _______________________________________________________________________________________________}\cell \pard \intbl \row 
\pard\par\par\pard \ql \widctlpar\tx5000\tx7000\aspalpha\aspnum\faauto\adjustright\itap0
{\tab 
{\f1\fs18 ___________________________________________________ }\par\tab\tab 
{\f1\fs18 Signature of Solicitor/Notary}\par}\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{ \par }
</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- French template section                                             			-->
	<!-- ************************************************************************ 	-->
	<!--	<xsl:template name="FrenchTemplate">
	</xsl:template>-->


	<!--#DG238 table with logo and branch address -->
	<xsl:template name="LogoAddress">
		<xsl:text><!--#DG286 \trowd \trgaph108\trrh1079\trleft-108\trftsWidth2\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth2\clwWidth30 \cellx30-->\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{\f1 </xsl:text>  
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell }
\pard \ql \clvertalc\widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{\f1\fs18 </xsl:text>

		<!--#DG330 rewritten
		<xsl:value-of select="//SolicitorsPackage/LenderName"/-->
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/RegisteredName">
				<xsl:value-of select="//SolicitorsPackage/RegisteredName"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>		
		
		<xsl:text>\par }
{\f1\fs18 </xsl:text>

		<!--#DG330 rewritten-->
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/RegisteredAddress">

				<xsl:for-each select="/*/SolicitorsPackage/RegisteredAddress/Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>				

			</xsl:when>
			<xsl:otherwise>

				<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
				<xsl:if test="//CommitmentLetter/BranchAddress/Line1">
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
				<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
				<xsl:text>  </xsl:text>
				<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>

			</xsl:otherwise>
		</xsl:choose>		

		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{
\trowd \trrh1266\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\cltxlrtb\clftsWidth3\clwWidth3377 \cellx5269
\clvertalc\cltxlrtb\clftsWidth3\clwWidth7522 \cellx15646
\row 
}</xsl:text>	
	</xsl:template>

	<!--#DG288 add CONFIRMATION OF CLOSING page - copied from CV-->
	<xsl:template name="EnglishConfClos">	
		<xsl:text>\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }</xsl:text>
		<xsl:text>{\pard\sa240
\brdrt \brdrs \brdrw10 \brsp10
\brdrl \brdrs \brdrw10 \brsp10
\brdrb \brdrs \brdrw10 \brsp10
\brdrr \brdrs \brdrw10 \brsp10
\qc\f1\fs20 \line\b\ul {\fs28 CONFIRMATION OF CLOSING}\line
\par}</xsl:text>
		<xsl:call-template name="FaxTo"/>
		<xsl:call-template name="LenderRef"/>

		<xsl:text>{\pard\sa180\f1\fs20 </xsl:text>
		<xsl:text>\sa360 </xsl:text>
		<xsl:text>We confirm that the above referenced transaction has closed.  Our Solicitor/Notary\rquote s Final report will follow.</xsl:text>
		<xsl:text>\par}</xsl:text>
		<!-- Signature -->		
		<xsl:text>\pard \qr \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 
{\f1\fs20\sb720 ______________________________________\par}
{\f1\fs20 Signature of Solicitor/Notary}</xsl:text>

	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="FaxTo">
		<!-- row 1 -->
		<xsl:call-template name="FaxTo_row_type1"/>
		<xsl:text>{\f1\fs20\b FAX TO:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>}\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b FROM:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 </xsl:text>
		<xsl:call-template name="Solicitor"/>
		<xsl:text>}\cell \row }</xsl:text>
		<!-- row 2 -->
		<xsl:call-template name="FaxTo_row_type1"/>
		<xsl:text>{\f1\fs20\b ATTENTION:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<xsl:text>Originations </xsl:text>
		<!-- #554 <xsl:call-template name="CurrentUserName"/> -->
		<xsl:text>}\cell \pard \intbl </xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 </xsl:text>
		<xsl:text>}\cell \row }</xsl:text>
		<!-- row 3 -->
		<xsl:call-template name="FaxTo_row_type1"/>
		<xsl:text>{\f1\fs20\b DATE:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CurrentDate"/>
		<xsl:text>}\cell \pard \intbl </xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 </xsl:text>
		<xsl:call-template name="SolicitorPhoneAndFax"/>
		<xsl:text>}\cell \row }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="LenderRef">
		<xsl:call-template name="HorizLine"/>
		<!-- row 1 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b LENDER REFERENCE NUMBER:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/ReferenceNum"/>
		<xsl:text>}\cell \row }</xsl:text>
		<!-- row 2 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b <!--#DG350 MORTGAGOR-->BORROWER(S):}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b\caps </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/>
		<xsl:text>}\cell \row }</xsl:text>
		<!-- row 3 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b MUNICIPAL ADDRESS:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<xsl:call-template name="PropertyAddr"/>
		<xsl:text>}\cell \row }</xsl:text>
		<!-- row 4 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b MORTGAGE AMOUNT:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<!--#DG364 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>
		<xsl:text>}\cell \row }</xsl:text>
		<!-- row 5 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b CLOSING DATE:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<!-- row 6 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b MORTGAGE REGISTRATION NO:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b </xsl:text>____________________________________________________________<xsl:text>}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="HorizLine"/>
	</xsl:template>
	<!-- ================================================  -->

	<xsl:template name="PropertyAddr">
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text>\line </xsl:text>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
			<xsl:text>\line </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
	</xsl:template>

	<!--  ============================= SolicitorAddress ====================================== -->
	<xsl:template name="Solicitor">
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>\line </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\line </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\line </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="SolicitorPhoneAndFax">
		<xsl:text>{Phone: \tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Phone"/>
		<xsl:text>\line </xsl:text>
		<xsl:text>Fax: \tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Fax"/>
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- Utility templates 														                -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="HorizLine">
		<xsl:text>{\pard\sa180\brdrb \brdrs \brdrw30 \brsp10 \par}</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- Row definitions 														                -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="FaxTo_row_type1">
		<xsl:text>{\trowd \trgaph55\trleft-55\ql
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10840 
\pard \intbl</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="FaxTo_row_type2">
		<xsl:text>{\trowd \trgaph55\trleft-55
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5000 
\pard \intbl</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="LenderRef_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx4000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10840 
\pard \intbl</xsl:text>
	</xsl:template>
	<!--#DG288 end -->

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff63\deflang1033\deflangfe1033
{\fonttbl
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f14\fnil\fcharset2\fprq2{\*\panose 05000000000000000000}Wingdings;}
}
{\colortbl;
\red0\green0\blue0;
\red0\green0\blue255;
\red0\green255\blue255;
\red0\green255\blue0;
\red255\green0\blue255;
\red255\green0\blue0;
\red255\green255\blue0;
\red255\green255\blue255;
\red0\green0\blue128;
\red0\green128\blue128;
\red0\green128\blue0;
\red128\green0\blue128;
\red128\green0\blue0;
\red128\green128\blue0;
\red128\green128\blue128;
\red192\green192\blue192;
\red255\green255\blue255;
}
{\stylesheet
{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \snext0 Normal;}
{\s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 \sbasedon0 \snext0 heading 1;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext15 header;}
{\s16\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext16 footer;}
}
\margl720\margr720\margt720\margb720 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0<!--#DG286 -->\headery0\footery400\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>
</xsl:stylesheet>
