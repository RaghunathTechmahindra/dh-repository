<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- 
  28/Sep/2006 DVG #DG520 #3371  HFS CR#36 Branding Changes  
	20/Oct/2005 DVG #DG340 #2306  CR#131 Removal of text from wire sheet  
	01/Feb/2005 DVG #DG132 scr#909 Xceed - Wire Transfer Document-refundableFee line break 
  -->

	<xsl:output method="text"/>
	
  <!--#DG520 -->
	<xsl:variable name="lendName" select="translate(/*/LenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />	
	<xsl:variable name="lendNameLang">	
		<xsl:choose>
			<xsl:when test="/*/LanguageFrench">
		    <xsl:value-of select="concat($lendName,'_Fr')"/>
      </xsl:when>
			<xsl:otherwise>
		    <xsl:value-of select="concat($lendName,'_En')"/>
      </xsl:otherwise>
		</xsl:choose>
	</xsl:variable>	
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendNameLang,'.xsl'))" />
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />

	<xsl:template match="/">
    <!--#DG520 -->
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>

		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//LanguageEnglish">
				<xsl:call-template name="EnglishMain"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//LanguageFrench">
				<!--				<xsl:call-template name="FrenchTemplate"/>-->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- English template section                                                 	   -->
	<!-- ************************************************************************ -->
	<xsl:template name="EnglishMain">
		<xsl:text>\trowd \trgaph108\trleft-1080\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx3960
\cltxlrtb\clftsWidth3\clwWidth5760 \cellx9720
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 STANDARD WIRE INSTRUCTIONS\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{</xsl:text>
		<xsl:call-template name="XceedLogo"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx3960
\cltxlrtb\clftsWidth3\clwWidth5760 \cellx9720
\row 
}
\pard \ql \li-1080\ri-720\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-720\lin-1080\itap0 
{\b\f1\fs24 Attention: </xsl:text>
		<xsl:value-of select="//Solicitor/Name"/>
		<xsl:text>\par }
\pard\plain \s1\ql \li-1080\ri-1080\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\rin-1080\lin-1080\itap0 
{\b\f1\fs24 Fax: </xsl:text>
		<xsl:value-of select="//Solicitor/Fax"/>
		<xsl:text>\tab Phone: </xsl:text>
		<xsl:value-of select="//Solicitor/Phone"/>
		<xsl:text>\par }
\pard\plain \qj \li-720\ri-720\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-720\lin-720\itap0 
{\b\f1\fs24 \par }
\pard\plain \s17\qj \li-1080\ri-1080\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-1080\lin-1080\itap0 
{\f1\fs24 </xsl:text>
		<xsl:value-of select="//LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//LenderNameFund"/>
		<xsl:text> has initiated the wire transfer<!--#DG334  from Bank of Montreal-->. The net proceeds will be deposited into your trust account.
\par }
\pard\plain \qj \li-1080\ri-1080\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-1080\lin-1080\itap0 
{\f1\fs24 \par }
{\b\f1\fs28 PRIOR TO RELEASE OF FUNDS, THE FOLLOWING ITEMS AND VERBAL AUTHORIZATION IS REQUIRED:\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 FUNDING DATE:\cell </xsl:text>
		<xsl:value-of select="//CurrentDate"/>
		<xsl:text>\cell }
{\f1\fs20 DATE CLOSED:\cell </xsl:text>
		<xsl:value-of select="//EstClosingDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\row 
}
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 LOAN NUMBER:\cell </xsl:text>
		<xsl:value-of select="//ServicingMortgageNumber"/>
		<xsl:text>\cell PROGRAM TYPE:\cell </xsl:text>
		<xsl:value-of select="//Product"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 BROKER:\cell </xsl:text>
		<xsl:value-of select="//BrokerFirmName"/>
		<xsl:text>\cell GRADE:\cell </xsl:text>
		<xsl:value-of select="//LOBDescription"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 AGENT:\cell </xsl:text>
		<xsl:value-of select="//BrokerAgentName"/>
		<xsl:text>\cell CHARGE PRIORITY:\cell </xsl:text>
		<xsl:value-of select="//ChargePriority"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 CLIENT:\cell </xsl:text>
		<xsl:value-of select="//BorrowerLastName"/>
		<xsl:text>\cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\row 
}
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 COMMITMENT DATE:\cell </xsl:text>
		<xsl:value-of select="//ApprovalDate"/>
		<xsl:text>\cell MATURITY DATE:\cell </xsl:text>
		<xsl:value-of select="//MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx4500
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx6840
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2880 \cellx9720
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{ \par }
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\clbrdrt\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Mortgage Loan:\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//LoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Interest Rate:\cell </xsl:text>
		<xsl:value-of select="//NetInterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\clbrdrt\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Term:\cell </xsl:text>
		<xsl:value-of select="//PaymentTerm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amortization:\cell </xsl:text>
		<xsl:value-of select="//Amortization"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard 
\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment:\cell </xsl:text>
		<xsl:value-of select="//PandIPayment"/>
		<xsl:text>\cell }</xsl:text>
		<xsl:text>
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard 
\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Frequency:\cell </xsl:text>
		<xsl:value-of select="//PaymentFrequency"/>
		<xsl:text>\cell }</xsl:text>
		<!--  Repeating rows for fees  -->
		<xsl:for-each select="//Fees/Fee">
			<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Less </xsl:text>
			<xsl:value-of select="./Verb"/>
			<xsl:text>:\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
			<xsl:value-of select="./Amount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell \cell }</xsl:text>
		</xsl:for-each>
		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Appraised/purchase:\cell </xsl:text>
		<xsl:value-of select="//ActualAppraisalValue"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:for-each select="//Fees/refundableFee">
			<xsl:text>Add </xsl:text>
			<xsl:value-of select="./Verb"/>
			<!-- #DG132 -->
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text> \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:for-each select="//Fees/refundableFee">
			<xsl:value-of select="./Amount"/>
			<!-- #DG132 -->
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 LTV:\cell </xsl:text>
		<xsl:value-of select="//LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4222 \cellx2777
\cltxlrtb\clftsWidth3\clwWidth1111 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Less Accrued Interest:\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//IADAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\clbrdrt\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Net Proceeds Wire:\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//NetWire"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-1080
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trftsWidth3\trwWidth10800\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2269 \cellx4429
\clbrdrt\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2051 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx9720
\row 
}
\pard\ql \li-1080\ri-1080\sa120\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-1080\lin-1080\itap0 
{\f1\fs22\b YOU WILL BE CONTACTED WITHIN 24 HOURS OF CLOSING TO CONFIRM THIS TRANSACTION HAS CLOSED AND FUNDS WERE DISBURSED. IF THIS IS NOT THE CASE, WE WILL REQUIRE THE IMMEDIATE RETURN OF THESE FUNDS AT YOUR EXPENSE AND WITH APPROPRIATE PENALTIES. }
{\par }
{\par }
\pard \qc \li-1080\ri-1080\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-1080\lin-1080\itap0 
{\f1 STANDARD WIRE INSTRUCTIONS FOR MORTGAGE FINANCING\par }
\pard \ql \li-720\ri-720\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-720\lin-720\itap0 
{\par }
\pard \ql \li-1080\ri-1080\sa120\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-1080\lin-1080\itap0 
{\f1\fs22 FUNDER: </xsl:text>
		<xsl:value-of select="//LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//LenderNameFund"/>
		<xsl:text>}
{\par }
{\f1\fs22 PAYEE: </xsl:text>
		<xsl:value-of select="//Solicitor/Firm"/>
		<xsl:text> "In Trust"}
{\par }
\pard \ql \li-1080\ri-1080\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-1080\lin-1080\itap0 
{\f1\fs22 FINANCIAL INSTITUTION
\par ADDRESS:
\par BANK and BRANCH #:
\par ACCOUNT:
\par }
\pard \ql \li-720\ri-720\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-720\lin-720\itap0 
{\par }
\pard \ql \li-1080\ri-1080\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-1080\lin-1080\itap0 
{\f1\fs18 Prepared by: \tab ____________________________\tab \tab Officer: \tab __________________________
\par \tab \tab \tab </xsl:text>
		<xsl:value-of select="//Administrator/contactFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//Administrator/contactLastName"/>
		<xsl:text>\par }
\pard \ql \li-720\ri-720\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-720\lin-720\itap0 
{\f1\fs18 \par }
\pard \ql \li-1080\ri-1080\widctlpar\aspalpha\aspnum\faauto\adjustright\rin-1080\lin-1080\itap0 
{\f1\fs18 Authorizing Officer: \tab ____________________________\par }</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- French template section                                                  -->
	<!-- ************************************************************************ -->
	<!--	<xsl:template name="FrenchTemplate">
		</xsl:template>-->

	<!-- ************************************************************************ -->
	<!-- rtf file start and rtf file end                                          -->
	<!-- ************************************************************************ -->
	<xsl:template name="RTFFileEnd">
		<xsl:text>\par }}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\stylesheet {\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}}
\margt720\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin720\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead \pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta.}}{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta)}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta)}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
</xsl:text>
	</xsl:template>

	<xsl:template name="XceedLogo">
    <!--#DG520 delete old, use external -->
		<xsl:value-of select="$logoRtf"/>
	</xsl:template>
	
</xsl:stylesheet>
