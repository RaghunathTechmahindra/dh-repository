<?xml version="1.0" encoding="UTF-8"?>
<!--
18/Dec/2006 DVG #DG560 reverse some DJ templates to 3.0 - document-location() is empty
05/Dec/2006 DVG #DG552 #5310  CR 055 - waived conditions appear on the document  
18/Aug/2006 DVG #DG488 #4342  Words missing from pre-authorization document  
22/Feb/2005 DVG DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik
-->
<!-- created by Catherine Rutgaizer -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions"
	xmlns:File="xalan://java.io.File">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>

  <!--#DG552 relative logo file name-->
  <!-- eg. file:///C:/mosdocprep/admin_CTF/docgen/templates/WFCommitmentLetterFO.xsl -->
  <!-- #DG560 <xsl:variable name="logoPatha" select="document-location()" /> -->
  <xsl:variable name="logoPatht" select="document-location()" />
  <xsl:variable name="logoPatha">
   <xsl:choose>
    <xsl:when test="$logoPatht">
      <xsl:value-of select="$logoPatht" />
    </xsl:when>
    <xsl:otherwise>\mos_docprep\admin_DJ\docgen\templates\.</xsl:otherwise>
   </xsl:choose>
  </xsl:variable>  
	<xsl:variable name="logoPathf" select="File:new($logoPatha)"/>
  <xsl:variable name="logoPath" select="File:getParent($logoPathf)"/>  
	<xsl:variable name="logoFilea" select="'\DJLogo_fr.gif'" />
	<xsl:variable name="logoFile" select="concat($logoPath,$logoFilea)" />

	<xsl:template match="/">
	  <xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>

		<xsl:call-template name="FOStart"/>
	</xsl:template>

	<!-- define attribute sets -->
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr">
		<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ObjectAttr" use-attribute-sets="MainFontAttr SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr" use-attribute-sets="SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	<!-- end attribute sets -->

  <!-- #DG552 -->
	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<xsl:template name="BorrAddress">
		<xsl:element name="fo:block">
			<xsl:value-of select="addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="city"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="province"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="postalFSA"/>
			<xsl:text>  </xsl:text>
			<xsl:value-of select="postalLDU"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="ContactName">
		<xsl:param name="saluta" select="''"/>	
		<xsl:if test="$saluta != ''">
			<xsl:value-of select="$saluta"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="contactFirstName"/>
		<xsl:text> </xsl:text>							
		<xsl:if test="contactMiddleInitial!= ''">
			<xsl:value-of select="contactMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>							
		<xsl:value-of select="contactLastName"/>
	</xsl:template>

  <!-- #DG552 end -->

	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateSalesRepLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">143mm</xsl:attribute><!--#DG552 -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="SpaceBAOptimum"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<!-- #DG552 xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element-->
							<fo:external-graphic src="{$logoFile}" height="28mm" width="73mm"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="margin-left">2mm</xsl:attribute>
			<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">20mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">100mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TitleFontAttr"/>Avis de pré-autorisation d'un prêt hypothécaire</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">143mm</xsl:attribute><!--#DG552 -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
						Le  <xsl:value-of select="/*/General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateSalesRepLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">70mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">73mm</xsl:attribute><!--#DG552 -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Représentant hypothécaire :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<!-- #DG552 
							xsl:if test="/*/Deal/SourceOfBusinessProfile/Contact/salutation">
								<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/salutation"/>
							</xsl:if>
							<xsl:value-of select="concat(/*/Deal/SourceOfBusinessProfile/Contact/contactFirstName , '   ', /*/Deal/SourceOfBusinessProfile/Contact/contactLastName ) "/-->
        		  <xsl:for-each select="/*/Deal/SourceOfBusinessProfile/Contact">
         		    <xsl:call-template name="ContactName">
                  <xsl:with-param name="saluta" select="salutation"/>
                </xsl:call-template>
        		  </xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>Téléphone : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>
							<xsl:if test="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension">
								<xsl:value-of select="concat('  	poste : ', /*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension) "/>
							</xsl:if>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>Télécopieur : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactFaxNumber"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>Courriel :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactEmailAddress"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">30mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">113mm</xsl:attribute><!--#DG552 -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Objet :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<!-- #DG552 
						<xsl:element name="fo:block">Nom de l'emprunteur : 							
							<xsl:if test="/*/specialRequirementTags/OrderedBorrowers/Borrower/primaryBorrowerFlag='Y'">
								<xsl:value-of select="/*/specialRequirementTags/OrderedBorrowers/Borrower/borrowerFirstName"/>
								<xsl:text> </xsl:text>
								<xsl:if test="/*/specialRequirementTags/OrderedBorrowers/Borrower/borrowerMiddleInitial">
									<xsl:value-of select="/*/specialRequirementTags/OrderedBorrowers/Borrower/borrowerMiddleInitial"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:value-of select="/*/specialRequirementTags/OrderedBorrowers/Borrower/borrowerLastName"/>
							</xsl:if>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:if test="position()=2">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:text> </xsl:text>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">Nom du 1er co-emprunteur : 
								<xsl:value-of select="./borrowerFirstName"/>
									<xsl:text> </xsl:text>
									<xsl:if test="./borrowerMiddleInitial">
										<xsl:value-of select="./borrowerMiddleInitial"/>
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:if>
					<xsl:if test="position()=3">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:text> </xsl:text>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">Nom du 2ème co-emprunteur : 
								<xsl:value-of select="./borrowerFirstName"/>
									<xsl:text> </xsl:text>
									<xsl:if test="./borrowerMiddleInitial">
										<xsl:value-of select="./borrowerMiddleInitial"/>
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:if>
					<xsl:if test="position()=4">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:text> </xsl:text>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">Nom du 3ème co-emprunteur : 
								<xsl:value-of select="./borrowerFirstName"/>
									<xsl:text> </xsl:text>
									<xsl:if test="./borrowerMiddleInitial">
										<xsl:value-of select="./borrowerMiddleInitial"/>
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:if>
				</xsl:for-each-->
    		    <xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[primaryBorrowerFlag='Y'][1]">
    					<xsl:element name="fo:block">Nom de l'emprunteur :  							
    			     <xsl:call-template name="BorrowerFullName"/>
    					</xsl:element>
            </xsl:for-each>						
    		    <xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower">
            	<xsl:choose>
            		<xsl:when test="position()=2">
    						  <xsl:element name="fo:block">
                   Nom du 1er co-emprunteur :
    			         <xsl:call-template name="BorrowerFullName"/>								  
    						  </xsl:element>
            		</xsl:when>
            		<xsl:when test="position()=3">
    						  <xsl:element name="fo:block">
            		    Nom du 2ème co-emprunteur :
    			          <xsl:call-template name="BorrowerFullName"/>								  
    						  </xsl:element>
            		</xsl:when>
            		<xsl:when test="position()=4">
    						  <xsl:element name="fo:block">
            		    Nom du 3ème co-emprunteur :
    			          <xsl:call-template name="BorrowerFullName"/>								  
    						  </xsl:element>
            		</xsl:when>
            		<xsl:otherwise>
            		  <xsl:text></xsl:text>
            		</xsl:otherwise>
            	</xsl:choose>
    		    </xsl:for-each>
					</xsl:element>
				</xsl:element>
				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:text> </xsl:text>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Numéro de dossier : <xsl:text> </xsl:text>
							<xsl:value-of select="/*/Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:text> </xsl:text>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Montant pré-autorisé : <xsl:text> </xsl:text>
							<xsl:value-of select="/*/Deal/totalLoanAmount"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>			
			Monsieur (Madame),
	    </xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
			La présente a pour but de vous confirmer la pré-autorisation du prêt des personnes mentionnées en titre, aux conditions suivantes :
		</xsl:element>
		<!--#DG488 rewritten 
    xsl:element name="fo:block">
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:element name="fo:list-block">
				<xsl:attribute name="margin-left">5mm</xsl:attribute>
				<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
				<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:apply-templates select="/*/Deal/DocumentTracking"/>
			</xsl:element>
		</xsl:element-->
		<fo:table xsl:use-attribute-sets="TableLeftFixed">
			<fo:table-column column-width="6mm"/>
			<fo:table-column column-width="137mm"/>
			<fo:table-body xsl:use-attribute-sets="MainFontAttr">
				<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
				<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
				<xsl:apply-templates select="/*/Deal/DocumentTracking[documentStatusId = 0 or documentStatusId = 1]"/><!-- #DG552 -->
			</fo:table-body>
		</fo:table>
		
		<!-- 
		<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainFontAttr"/>
				Veuillez prendre note que vous devez faire suivre les documents originaux au nom du soussigné.
		</xsl:element>
		-->
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			Nous vous remercions de votre collaboration. 
		</xsl:element>
	</xsl:template>

  <!--#DG488 rewritten -->
	<xsl:template match="DocumentTracking">
		<xsl:variable name="condTId" select="Condition/conditionTypeId"/>
		<!-- #DG552 xsl:if test="$condTId = 0 or $condTId = 2"-->
		<xsl:if test="$condTId = 0 or ($condTId = 2 and documentResponsibilityRoleId != 60)">
			<fo:table-row keep-together="always">
				<fo:table-cell padding-top="2mm">
						<fo:block text-align="right"> &#x2022;</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-top="2mm" keep-together="always">
					<fo:block text-align="justify" start-indent="6mm" keep-together="always">
						<xsl:value-of select="./text"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
	</xsl:template>

	<xsl:template name="CreateUnderwriterLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">143mm</xsl:attribute><!--#DG552 -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
              <!--#DG552 
							xsl:value-of select=" concat(/*/Deal/underwriter/Contact/contactFirstName , '   ', /*/Deal/underwriter/Contact/contactLastName )   "/-->
        		  <xsl:for-each select="/*/Deal/underwriter/Contact">
         		    <xsl:call-template name="ContactName"/>
        		  </xsl:for-each>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="/*/Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">30mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">100mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:copy use-attribute-sets="PaddingAll0mm"/>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Téléphone :	
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumber"/>
											<xsl:choose>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='173'"> ou 1-888-281-4420 poste : <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<!-- <xsl:when test="/*/Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999 poste : <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when> -->
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='175'"> ou 1-800-728-2728 poste : <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text> </xsl:text>poste : <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Télécopieur :
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="/*/Deal/underwriter/Contact/contactFaxNumber"/>
											<xsl:choose>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='173'"> ou 1-888-888-7777</xsl:when>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999</xsl:when>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='175'"> ou 1-888-888-8888</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="FOStart">
		<!-- DG#140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- DG#140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<!--#DG552 adjust width 
					xsl:attribute name="page-height">11in</xsl:attribute-->
					<xsl:attribute name="page-height">279mm</xsl:attribute>					
					<!--#DG552 adjust width 
          xsl:attribute name="page-width">8.5in</xsl:attribute-->
				  <xsl:attribute name="page-width">216mm</xsl:attribute>
					<xsl:attribute name="margin-top">0mm</xsl:attribute>
					<xsl:attribute name="margin-bottom">10mm</xsl:attribute>
					<xsl:attribute name="margin-left">0mm</xsl:attribute>
					<xsl:attribute name="margin-right">25mm</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">38mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">15mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
