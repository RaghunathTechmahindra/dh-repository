<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' >  
<xsl:output method="text"/>

<xsl:template match="/">
<xsl:call-template name="RTFFileStart"/>

<xsl:choose>
<xsl:when test='//LanguageEnglish'>
<!-- <xsl:call-template name="EnglishPageNumber"/> -->
<xsl:call-template name="EnglishDocumentTitle"/>
<xsl:call-template name="EnglishHeaderTable"/>
<xsl:call-template name="EnglishSubheader"/>
<xsl:call-template name="EnglishConditionList"/>
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test='//LanguageFrench'>
	<xsl:call-template name="FrenchDocumentTitle"/>
	<xsl:call-template name="FrenchHeaderTable"/>
	<xsl:call-template name="FrenchSubheader"/>
	<xsl:call-template name="FrenchConditionList"/>
</xsl:when>
</xsl:choose>


<xsl:call-template name="RTFFileEnd"/> 
</xsl:template>  

<!-- ************************************************************************ -->
<!-- English template section                                                 -->
<!-- ************************************************************************ -->
<xsl:template name="EnglishConditionList"> 

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs 
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx9000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard \intbl 

{\f1\fs22\b \cell Documents reviewed by:\par </xsl:text><xsl:value-of select="//CurrentUser/Name"/><xsl:text> </xsl:text><xsl:value-of select="//CurrentUser/Extension"/><xsl:text>\par \par 
**ALL CONDITIONS MUST BE MET </xsl:text><xsl:value-of select="//ConditionsMetDays"/><xsl:text> BUSINESS DAYS PRIOR TO CLOSING OR CLOSING WILL BE DELAYED**\par\par
We will instruct this file once the following conditions are {\ul satisfied}: Original Appraisal, Signed Commitment, Income Verification and Solicitor’s Information (see below for complete conditions)\cell \cell }\pard \intbl\row </xsl:text>

<xsl:for-each select="//Conditions/Condition">

<xsl:if test="not(contains(./Description, '(Solicitor)') or contains(./Description, '(Notaire)'))">
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx9000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 __}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text>
<xsl:for-each select="./Description/Line">
<xsl:value-of select="."/><xsl:text> \par </xsl:text>
</xsl:for-each>
<xsl:text>}\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="./Status"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>
</xsl:if>
</xsl:for-each>

<!-- Catherine for pvcs #817 -->
<xsl:text>\pard \par \par {\f1\fs22\b </xsl:text><xsl:value-of select="//ConditionNote"/><xsl:text>}</xsl:text>
<!-- Catherine for pvcs #817 -->

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 



<xsl:template name="EnglishSubheader"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx7000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Conditions of Approval Outstanding For: }{\f1\fs22 </xsl:text><xsl:value-of select="//ClientName"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Closing Date: }{\f1\fs22 </xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 



<xsl:template name="EnglishHeaderTable"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx7500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 To:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFirmName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 From:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//LenderNameShort"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx7500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Attn:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Loan #:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx7500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Phone:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Lender Reference #:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//DealNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx7500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Fax:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Date:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx7500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Insurance Reference #:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIPolicyNumber"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Insurance Status:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIStatus"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>




<xsl:text>\pard \par</xsl:text> 

</xsl:template> 


<xsl:template name="EnglishPageNumber"> 
<xsl:text>\pard {\f1\fs22 Page: }{\field {\*\fldinst {\cs16 PAGE }}}{\f1\fs22 \~\~ of\~\~ }{\field {\*\fldinst {\cs16 NUMPAGES }}}\pard\par</xsl:text>
</xsl:template> 


<xsl:template name="EnglishDocumentTitle"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \qr\intbl </xsl:text>

<xsl:text>{\f1\fs24\b Conditions Outstanding}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 

<xsl:template name="EnglishFooter">
<xsl:text>
{\qc\f1\fs22 Phone: </xsl:text><xsl:value-of select="//BranchPhone"/><xsl:text>   Fax: </xsl:text><xsl:value-of select="//BranchFax"/><xsl:text>\par}
{\qc\f1\fs22 Toll-free Phone: </xsl:text><xsl:value-of select="//BranchTollPhone"/><xsl:text>   Toll-free Fax: </xsl:text><xsl:value-of select="//BranchTollFax"/><xsl:text>\par}
</xsl:text>
</xsl:template>

<!-- ************************************************************************ -->
<!-- French template section                                                  -->
<!-- ************************************************************************ -->
<xsl:template name="FrenchDocumentTitle"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \qr \intbl </xsl:text>

<xsl:text>{\f1\fs24\b Conditions non remplies}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 

<xsl:template name="FrenchHeaderTable"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 À:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFirmName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 De:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//LenderNameShort"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Attention:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 N{\super o} du prêt:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Téléphone:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 N{\super o} de référence du prêteur:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//DealNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Télécopieur:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Date:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx3000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 N{\super o} de référence assurance:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIPolicyNumber"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Statut de l’assurance:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIStatus"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>
<xsl:text>\pard \par</xsl:text> 

</xsl:template> 

<xsl:template name="FrenchSubheader"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx7500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Conditions non remplies de l’approbation de: }{\f1\fs22</xsl:text><xsl:value-of select="//ClientName"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Date de clôture: }{\f1\fs22 </xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template>

<xsl:template name="FrenchConditionList"> 

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs 
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx9000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard \intbl 

{\f1\fs22\b \cell Documents examinés par:\par </xsl:text><xsl:value-of select="//CurrentUser/Name"/><xsl:text> </xsl:text><xsl:value-of select="//CurrentUser/Extension"/><xsl:text>\par \par 
**TOUTES LES CONDITIONS DOIVENT ÊTRE REMPLIES DANS LES </xsl:text><xsl:value-of select="//ConditionsMetDays"/><xsl:text> JOURS OUVRABLES PRÉCÉDANT LA CLÔTURE À DÉFAUT DE QUOI LA CLÔTURE SERA RETARDÉE**\par\par
Nous traiterons ce dossier dès que les conditions suivantes auront été {\ul remplies}: Copie originale de l’évaluation, lettre d’engagement signée, vérification du revenu et informations sur le notaire mandaté(e) à cette transaction (voir ci-dessous la liste complète des conditions) \cell \cell }\pard \intbl\row </xsl:text>

<xsl:for-each select="//Conditions/Condition">

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx500

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx9000

\clbrdrt\brdrs
\clbrdrl\brdrs
\clbrdrb\brdrs
\clbrdrr\brdrs\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 __}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text>
<xsl:for-each select="./Description/Line">
<xsl:value-of select="."/><xsl:text> \par </xsl:text>
</xsl:for-each>
<xsl:text>}\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="./Status"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>
</xsl:for-each>

<!-- Catherine for pvcs #817 -->
<xsl:text>\pard \par \par {\f1\fs22\b </xsl:text><xsl:value-of select="//ConditionNote"/><xsl:text>}</xsl:text>
<!-- Catherine for pvcs #817 -->

<xsl:text>\pard \par</xsl:text> 
</xsl:template>

<xsl:template name="FrenchFooter">
<xsl:text>
{\qc\f1\fs22 Téléphone: </xsl:text><xsl:value-of select="//BranchPhone"/><xsl:text>   Télécopieur: </xsl:text><xsl:value-of select="//BranchFax"/><xsl:text>\par}
{\qc\f1\fs22 Téléphone sans frais: </xsl:text><xsl:value-of select="//BranchTollPhone"/><xsl:text>   Télécopieur sans frais: </xsl:text><xsl:value-of select="//BranchTollFax"/><xsl:text>\par}
</xsl:text>
</xsl:template>

<!-- ************************************************************************ -->
<!-- rtf file start and rtf file end                                          -->
<!-- ************************************************************************ -->
<xsl:template name="RTFFileEnd">  
<xsl:text>}}</xsl:text>
</xsl:template>  

<xsl:template name="RTFFileStart">  
 <!-- #DG670 -->
<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\stylesheet {\ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}}
{\info {\author Zivko Radulovic}{\operator BASIS100 automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company Basis100 Inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \margl720\margr720\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\footer 
{\qc\f1\fs22 ______________________________________________________________________________________\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//LenderName"/><xsl:text>\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//BranchAddressComplete"/><xsl:text>\par}
</xsl:text>
<xsl:choose>
	<xsl:when test="//LanguageFrench"><xsl:call-template name="FrenchFooter"/></xsl:when>
	<xsl:otherwise><xsl:call-template name="EnglishFooter"/></xsl:otherwise>
</xsl:choose>
<xsl:text>
\pard\plain \s15\ql \li0\ri0\widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha
\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033

{\cs16 {\f1\fs22  }}
{\field {\*\fldinst {\cs16  }}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}}}
{\f1\fs16 {\f1\fs22 }}
{\field {\*\fldinst {\cs16 }}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}}}
{\cs16 \par }
\pard \s15\ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }}

{\header \pard\plain \qr \li0\ri0\widctlpar\faauto\rin0\lin0\itap0 \f108\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs22 Page: }{\field{\*\fldinst {PAGE }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{\f1\fs22 \~\~ </xsl:text><xsl:choose><xsl:when test="//LanguageFrench"><xsl:text>de</xsl:text></xsl:when><xsl:otherwise><xsl:text>of</xsl:text></xsl:otherwise></xsl:choose><xsl:text>\~\~ }{\field{\*\fldinst {NUMPAGES }}{\fldrslt {\lang1024\langfe1024\noproof 4}}}{\par }\pard \ql \li0\ri0\widctlpar\faauto\rin0\lin0\itap0 {\f1\fs22 \par }}
</xsl:text>

</xsl:template>  

</xsl:stylesheet>  
