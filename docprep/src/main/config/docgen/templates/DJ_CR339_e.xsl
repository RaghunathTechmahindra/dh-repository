<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
31/Jan/2007 DVG #DG574 #5634  CR 039 - changes for PPI  
12/Jan/2007 DVG #DG566 Upgrading to FOP0.9x 
18/Dec/2006 DVG #DG560 reverse some DJ templates to 3.0 - document-location() is empty
09/Nov/2006 DVG #DG540 #5103  DJ_CR039_e.xsl  - change existing logo  
06/Apr/2005 DVG #DG176 #1182  DJ documents - comma between city and province  
22/Feb/2005 DVG #DG140 # conform templates to jdk1.4 xalan, new xerces, fop, batik

created by Catherine Rutgaizer 

## Xue Bin Zhao 20070413, updated for DJ Credit Scoring Delivery 2 (CR039)
-->
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:fox="http://xml.apache.org/fop/extensions"
	xmlns:File="xalan://java.io.File">
	<!--	xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->

	<xsl:output method="xml" version="1.0" encoding="UTF-8"/>

  <!--#DG540 relative logo file name-->
  <!-- eg. file:///C:/mosdocprep/admin_CTF/docgen/templates/WFCommitmentLetterFO.xsl -->
  <xsl:variable name="logoPatha" select="document-location()" />
  <xsl:variable name="logoPathf" select="File:new($logoPatha)"/>
  <xsl:variable name="logoPath" select="File:getParent($logoPathf)"/>  
  <xsl:variable name="logoFilea" select="'\DJLogo_en.gif'" />
  <xsl:variable name="logoFile" select="concat($logoPath,$logoFilea)" />

	<!-- define attribute sets -->
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr">
		<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ObjectAttr" use-attribute-sets="MainFontAttr SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr" >
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>
	<!-- end attribute sets -->
	
	<xsl:template match="/">
	  <xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>

		<xsl:call-template name="FOStart"/>
	</xsl:template>

	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
					<xsl:attribute name="text-align">center</xsl:attribute>
					Page <fo:page-number/> of <fo:page-number-citation ref-id="{generate-id(/)}"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateSalesRepLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
				<fo:block id="{generate-id(/)}" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:block">
			<!--#DG540 xsl:element name="fo:external-graphic">
				<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
				<xsl:attribute name="height">1.1in</xsl:attribute>
				<xsl:attribute name="width">2.86in</xsl:attribute>
			</xsl:element-->
      <fo:external-graphic src="{$logoFile}" height="28mm" width="73mm"/>							
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<!-- <xsl:attribute name="margin-left">2mm</xsl:attribute> -->
			<xsl:attribute name="space-before.optimum">0mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">142mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					 <!--<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="border">solid black 1px</xsl:attribute> 
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TitleFontAttr"/>
						Notice - Conditional acceptance of mortgage loan
						</xsl:element>
					</xsl:element>-->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="TitleFontAttr"/>Notice - Conditional acceptance of mortgage loan
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">142mm</xsl:attribute><!--#DG540 adjust width -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="/*/General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateSalesRepLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">67mm</xsl:attribute><!--#DG540 adjust width -->
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">75mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Mortgage Representative:  </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:if test="/*/Deal/SourceOfBusinessProfile/Contact/salutation">
								<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/salutation"/>
							</xsl:if>
							<xsl:value-of select="concat(//Deal/SourceOfBusinessProfile/Contact/contactFirstName , '   ', //Deal/SourceOfBusinessProfile/Contact/contactLastName ) "/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
						Fax: </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactFaxNumber"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
						E-mail: </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactEmailAddress"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">18mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">124mm</xsl:attribute><!--#DG540 adjust width -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Subject: </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Mortgage loan application </xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!--<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
						File number: <xsl:value-of select="/*/Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
			<!--xsl:call-template name="PropertyDescription"/-->
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			This is to confirm acceptance of the mortgage loan requested requested by 
      		<xsl:call-template name="BorrowersListed"/>
       		for the purchase of the below-mentioned property, under the following conditions:
   	    </xsl:element>

        <xsl:element name="fo:block">
              <xsl:copy use-attribute-sets="MainBlockAttr"/>
	      <xsl:attribute name="text-align">justify</xsl:attribute>
              <xsl:element name="fo:list-item">        
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element> 
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">Obtain acceptance from a mortgage insurer (CMHC, Genworth) if mortgage insurance is required by the caisse, as indicated below.</xsl:element>
                    </xsl:element>                                   
              </xsl:element>    
              <xsl:element name="fo:list-item"> 
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element> 
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">If not, the caisse will require a certified appraisal of the property's market value.</xsl:element>                              
              		</xsl:element>
               </xsl:element>
               <xsl:element name="fo:list-item"> 
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element> 
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">Receipt of declaration and consent form duly signed by the borrower(s).</xsl:element>                              
              		</xsl:element>
               </xsl:element>
               <xsl:element name="fo:list-item"> 
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element>
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">Receipt of copies of investment statements showing, among other things, the source(s) or accumulation of the entire downpayment amount specified below. If the caisse requires that the loan be insured by a mortgage insurer, the investment statements must show the accumulation of the downpayment over a minimum period of six months.</xsl:element>                              
              		</xsl:element>
               </xsl:element>
               <xsl:element name="fo:list-item"> 
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element> 
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">Receipt of written confirmation from the employer of each borrower specifying the hiring date, position occupied, job status (regular, full-time, temporary, part-time, seasonal) and annual base salary, and also stating that the employee is not on probation. The borrower(s) will also need to provide a recent pay stub showing year-to-date earnings. The annual base salary indicated on the employer's written confirmation must match the salary declared in the loan application.</xsl:element>                              
              		</xsl:element>
               </xsl:element>
               <xsl:element name="fo:list-item"> 
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element> 
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">If the borrower's salary is paid by an enterprise belonging to the borrower, the latter must provide the caisse with his enterprise's financial statements for the three previous years along with his personal notice of assessment for the past three years. The average annual salary for the previous three years paid to the borrower by his enterprise, if applicable, must match the salary declared in the loan application. This information is used to confirm the applicant's borrowing capacity. The salary to be considered by the caisse will be the average salary of these three years or the salary of the past year if lower.</xsl:element>                              
              		</xsl:element>
               </xsl:element> 
               <xsl:element name="fo:list-item"> 
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element> 
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">Receipt of a copy of the offer to purchase and any attachments, duly signed by all the parties. The document must include the names of the sellers, their address and telephone number, as well as the name of the real estate broker, if applicable.</xsl:element>                              
              		</xsl:element>
               </xsl:element>    
               <xsl:element name="fo:list-item"> 
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element> 
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">All required documents, and their contents, must be verifiable and satisfy the caisse.</xsl:element>                              
              		</xsl:element>
               </xsl:element>
               <xsl:element name="fo:list-item"> 
                    <xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
                    <xsl:element name="fo:list-item-label"> 
                         <xsl:element name="fo:block">&#x2022;</xsl:element> 
                    </xsl:element> 
                    <xsl:element name="fo:list-item-body">                         
                         <xsl:attribute name="start-indent">body-start()</xsl:attribute> 
                         <xsl:element name="fo:block">All information contained in the loan application must be verifiable and satisfy the caisse.</xsl:element>                              
              		</xsl:element>
               </xsl:element>   
        </xsl:element>    
               
	    <xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">6mm</xsl:attribute>
			This acceptance and the related conditions have been issued on the basis of the information and 
			documents provided. Any change to this information or to the documents will result in a new 
			examination of the loan application and a new decision by the caisse. 
			This also applies if the caisse becomes aware that additional information or documents which 
			would have generated a different decision or if the financial situation of the borrower(s) has 
			deteriorated significantly since the loan application was first submitted. 
	    </xsl:element>

		<xsl:call-template name="PropertyDescription"/>	
		
		<xsl:element name="fo:block">
 			<!--#DG566 xsl:attribute name="keep-together">always</xsl:attribute--> 
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:element name="fo:list-block">
				<xsl:attribute name="margin-left">5mm</xsl:attribute>
				<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
				<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
				<!--#DG574 guarantee 1 item minimum -->  
  			<fo:list-item>
  				<fo:list-item-label>
  					<fo:block/>
  				</fo:list-item-label>
  				<fo:list-item-body>
  					<fo:block/>
  				</fo:list-item-body>
  			</fo:list-item>				
				
				<!-- #DG574 xsl:apply-templates select="/*/Deal/DocumentTracking"/-->
				<!-- <xsl:apply-templates select="/*/Deal/DocumentTracking[documentStatusId = 0 or documentStatusId = 1]"/> -->
			</xsl:element>
		</xsl:element>
		
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:call-template name="LoanDescription"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:call-template name="BottomText"/>
			Sincerely,
		</xsl:element>
	</xsl:template>

	<!-- PVCS # 611, new condition for conditions -->
	<xsl:template match="DocumentTracking">
		<xsl:if test="Condition/conditionTypeId='0' or (Condition/conditionTypeId='2' and documentResponsibilityRoleId != '60')">
			<xsl:element name="fo:list-item">
 				<xsl:attribute name="space-before.optimum">2mm</xsl:attribute> 
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">&#x2022;</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
 						<xsl:attribute name="text-align">justify</xsl:attribute>
 						<!--#DG566 xsl:attribute name="keep-together">always</xsl:attribute-->  
						<xsl:value-of select="./text"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template name="BorrowersListed">
		<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='0']">
			<xsl:choose>
				<xsl:when test="position()=1">
					<xsl:call-template name="BorrowerFullName"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="position()=last()">
							<xsl:text> and </xsl:text>
							<xsl:call-template name="BorrowerFullName"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>, </xsl:text>
							<xsl:call-template name="BorrowerFullName"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BorrowerFullName">
		<xsl:value-of select="salutation"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<xsl:template name="ColumnWidths">
		<!-- <xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">18mm</xsl:attribute>
		</xsl:element> -->
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">75mm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">40mm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">61mm</xsl:attribute><!--#DG540 adjust width -->
		</xsl:element>
	</xsl:template>

	<xsl:template name="PropertyDescription">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="padding-top">1mm</xsl:attribute>
			<xsl:call-template name="ColumnWidths"/>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Property:</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
 						<fo:block/><!--#DG574 guarantee 1 item minimum -->  
						<xsl:apply-templates select="/*/DealAdditional/Property"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="LoanDescription">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="padding-top">0mm</xsl:attribute> <!--#DG574 -->
			<xsl:call-template name="ColumnWidths"/>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>

				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Purchase price : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/totalPurchasePrice"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				
				<!--#DG574 add REFIIMPROVEMENTAMOUNT -->  
        <xsl:if test="/*/Deal/dealPurposeId = 17">
   				<fo:table-row>
  					<!-- <fo:table-cell>
  						<fo:block/>
  					</fo:table-cell> -->
  					<fo:table-cell>
  						<fo:block>Improvement Value : </fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block text-align="right">
  							<xsl:value-of select="/*/Deal/refiImprovementAmount"/>
  						</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block/>
  					</fo:table-cell>
   				</fo:table-row>
   				<fo:table-row>
  					<!-- <fo:table-cell>
  						<fo:block/>
  					</fo:table-cell> -->
  					<fo:table-cell>
  						<fo:block>As Improved Value : </fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block text-align="right">
  							<xsl:value-of select="/*/specialRequirementTags/asImprovedAmount"/>
  						</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block/>
  					</fo:table-cell>
   				</fo:table-row>
        </xsl:if>
				
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Down payment : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<!-- Begin Changes for ticket FXP17787 -->
							<xsl:value-of select="/*/Deal/requiredDownPayment"/>							
							<!-- End Changes for ticket FXP17787 -->		
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Type of financing : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:call-template name="TypeOfFinancing"/>
						</xsl:element>					
					</xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Amount of loan :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/netLoanAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					
					<xsl:element name="fo:table-cell">
					<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
						<xsl:element name="fo:block">Amount of advance for purchase (if revolving credit) : </xsl:element>
					</xsl:element>
					<!--<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select=""/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Mortgage insurance : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:call-template name="MortgageInsurance"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Amount of mortgage insurance premium : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/MIPremiumAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
					<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
						<xsl:element name="fo:block">Total amount of financing (or of advance for purchase if revolving credit) : <xsl:value-of select="/*/Deal/totalLoanAmount"/>
						</xsl:element>
					</xsl:element>
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/totalLoanAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Interest rate : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/netInterestRate"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Term* : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">left</xsl:attribute>
							<!-- Commented and added line below - Venkata - FXP17318 - begin -->
							<!-- <xsl:value-of select="/*/Deal/actualPaymentTerm"/> -->
							<xsl:variable name="actualTerm" select="/*/Deal/actualPaymentTerm" />
							<xsl:value-of select="($actualTerm - ($actualTerm mod 12))div 12"/><xsl:text> Year(s) </xsl:text> <xsl:value-of select="$actualTerm mod 12"/><xsl:text> Month(s)</xsl:text>
							<!-- Commented and added line below - Venkata - FXP17318 - end -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Amortization period* : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">left</xsl:attribute>
							<!-- Commented and added line below - Venkata - FXP17318 - begin -->
							<!-- <xsl:value-of select="/*/Deal/amortizationTerm"/> -->
							<xsl:variable name="totalAmortTerm" select="/*/Deal/amortizationTerm" />
							<xsl:value-of select="($totalAmortTerm - ($totalAmortTerm mod 12))div 12"/><xsl:text> Year(s) </xsl:text> <xsl:value-of select="$totalAmortTerm mod 12"/><xsl:text> Month(s)</xsl:text>
							<!-- Commented and added line below - Venkata - FXP17318 - end -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Security : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">See conditions</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Mortgage insurer file : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/Deal/MIPolicyNumber"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<!-- <xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element> -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Financing : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
  					<fo:block/><!--#DG574 guarantee 1 item minimum -->
						<xsl:for-each select="/*/DealAdditional/originationBranch/partyProfile">
							<xsl:element name="fo:block">
								<xsl:call-template name="PartyNameAndCompany"/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:apply-templates select="/*/DealAdditional/originationBranch/partyProfile/Contact/Address"/>
							</xsl:element>
						</xsl:for-each>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PartyNameAndCompany">
		<xsl:element name="fo:block">
			<xsl:value-of select="./partyName"/>
		</xsl:element>
		<!-- <xsl:element name="fo:block"><xsl:value-of select="./partyCompanyName"/></xsl:element> -->
	</xsl:template>

	<xsl:template match="Address">
		<xsl:element name="fo:block">
			<xsl:value-of select="./addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./city,', ',./province)"/> <!--#DG176-->
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./postalFSA,'  ', ./postalLDU)"/>
		</xsl:element>
	</xsl:template>


	<!--Logic implemented based on Process 1 in CR407-->
	<xsl:template name="TypeOfFinancing">
		<xsl:choose>
			<xsl:when test="/*/Deal/mortgageInsurerId='0'">Conventional</xsl:when>
			<xsl:otherwise>Insured</xsl:otherwise>	
		</xsl:choose>			
	</xsl:template>
	<!--Logic implemented based on Process 2 in CR407-->
	<xsl:template name="MortgageInsurance">
		<xsl:choose>
			<xsl:when test="/*/Deal/mortgageInsurerId='0'" />
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="/*/Deal/mortgageInsurerId='1'">CMHC</xsl:when>
					<xsl:when test="/*/Deal/mortgageInsurerId='2' and /*/Deal/MITypeId='6'">Genworth 50/50</xsl:when>
					<xsl:otherwise>Genworth 100</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!-- Subject property, French formatting of address -->
	<!--
		<xsl:template match="Property">
			<xsl:element name="fo:block"><xsl:attribute name="keep-together">always</xsl:attribute>			
				<xsl:value-of select="propertyStreetNumber"/><xsl:text>, </xsl:text>
				<xsl:if test="streetType"><xsl:value-of select="streetType"/><xsl:text>  </xsl:text></xsl:if>
				<xsl:value-of select="propertyStreetName"/>
				<xsl:if test="streetDirection"><xsl:text>  </xsl:text><xsl:value-of select="streetDirection"/></xsl:if>
				<xsl:if test="unitNumber">, unit� <xsl:value-of select="unitNumber"/></xsl:if>
			</xsl:element>
			<xsl:element name="fo:block"><xsl:value-of select="concat(propertyCity,', ',province)"/></xsl:element>
			<xsl:element name="fo:block"><xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/></xsl:element>
		</xsl:template>
     -->
	<!-- Subject property, English formatting of address -->

	<xsl:template match="Property">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:value-of select="propertyStreetNumber"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="propertyStreetName"/>
			<xsl:text> </xsl:text>
			<xsl:if test="streetType">
				<xsl:value-of select="streetType"/>
				<xsl:text>  </xsl:text>
			</xsl:if>
			<xsl:if test="streetDirection">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="streetDirection"/>
			</xsl:if>
			<xsl:if test="unitNumber">, unit <xsl:value-of select="unitNumber"/>
			</xsl:if>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyCity,', ',province)"/> <!--#DG176-->
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateUnderwriterLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">142mm</xsl:attribute><!--#DG540 adjust width -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="/*/Deal/underwriter/userType"/>
						</xsl:element>
						
						<!-- NOTE : take off the userType, tel, and fax sections as required -->
						<!--xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="/*/Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">30mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">100mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:copy use-attribute-sets="PaddingAll0mm"/>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Telephone:	
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumber"/>
											<xsl:choose>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='173'"> or 1-888-281-4420 ext:  <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>												
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='175'"> or 1-800-728-2728 ext:  <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text> </xsl:text>ext:  <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Fax:
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="/*/Deal/underwriter/Contact/contactFaxNumber"/>
											<xsl:choose>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='173'"> or 1-888-888-7777</xsl:when>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999</xsl:when>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='175'"> or 1-888-888-8888</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element-->
						
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="FOStart">
		<!-- DG#140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- DG#140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<!--#DG574  
					xsl:attribute name="page-height">11in</xsl:attribute>
          xsl:attribute name="page-width">8.5in</xsl:attribute-->
					<xsl:attribute name="page-height">279mm</xsl:attribute>					
				  <xsl:attribute name="page-width">216mm</xsl:attribute>					
					<xsl:attribute name="margin-top">0mm</xsl:attribute>
					<xsl:attribute name="margin-bottom">10mm</xsl:attribute>
					<xsl:attribute name="margin-left">0mm</xsl:attribute>
					<xsl:attribute name="margin-right">0mm</xsl:attribute>
 					<!-- #DG566 moved down <xsl:element name="fo:region-before"> ....-->
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">10mm</xsl:attribute>
						<xsl:attribute name="margin-top">25mm</xsl:attribute>
						<xsl:attribute name="margin-left">18mm</xsl:attribute>
						<xsl:attribute name="margin-right">18mm</xsl:attribute>
					</xsl:element>
 					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">15mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="BottomText">
		<xsl:element name="fo:block">
		<xsl:attribute name="text-align">justify</xsl:attribute>
		<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
		* The above interest rate is guaranteed for a period of 30 days. The borrower may wish to discuss other types of interest rates with the caisse (regular variable rates, reduced or capped variable rates, yearly revisable rate), other terms (the interest rate differs according to the selected term) or other amortization periods.
		</xsl:element>
		
		<xsl:element name="fo:block">
		<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
		We trust you will find this to be satisfactory.
		</xsl:element>
	</xsl:template>	
	
		
</xsl:stylesheet>
