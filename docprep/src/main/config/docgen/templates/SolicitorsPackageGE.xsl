<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
24/Oct/2006 DVG #DG530 #5012  GE Money - CR 16  French Solicitor Package  
23/Oct/2006 DVG #DG528 #5009  RTF File for Solicitor's Package Not Being Created
15/Mar/2005 SL #1902 Reverse part change in #1855 - display Lender Name in Clause(1) on Page 10
28/Oct/2005 DVG #DG348 #1807  GE - E2E - Correcting values appearing in the solicitor's package  
25/Aug/2005 DVG #DG300 #1902  GE Solicitor Package change  
11/Aug/2005 DVG #DG292 #1902  GE Solicitor Package change  - create new templates
- created based on MX
11/Aug/2005 DVG #DG290 #1855  Solicitor package changes  
05/Aug/2005 DVG #DG286  #1861  Merix - commitment letter Page 2 blank 
29/Jul/2005 DVG #DG270 #1855  Solicitor package changes  - New
13/Jul/2005 DVG #DG258 #1793  Add Fee Tag to next build  #1791
24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package 
- Many changes to become the Xprs standard	
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--  Import all of the logic from the Commitment Letter template so that the 2 documents 
	(standalone C/L and embedded C/L) remain the same -->

	<!--#DG286 xsl:import href="CommitmentLetterGENX.xsl"/-->
	<xsl:import href="CommitmentLetterGE.xsl"/>

	<xsl:output method="text"/>

	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/LanguageEnglish">
				<!--#DG528 xsl:call-template name="EnglishFooter"/><!- -#DG286 -->
				<xsl:call-template name="FooterEn"/>				
				<xsl:call-template name="SolicitorInstructionsEn"/>
				<xsl:call-template name="GuideEn"/>
				<xsl:call-template name="CommitmentLetterEn"/>
				<xsl:call-template name="RequestForFundsEn"/>
				<xsl:call-template name="FinalReportEn"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/LanguageFrench">
				<xsl:call-template name="FooterFr"/>				
				<xsl:call-template name="SolicitorInstructionsFr"/>
				<xsl:call-template name="GuideFr"/>
				<xsl:call-template name="CommitmentLetterFr"/>
				<xsl:call-template name="RequestForFundsFr"/>
				<xsl:call-template name="FinalReportFr"/>
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="SolicitorInstructionsEn">
		<!--#DG238-->
		<xsl:call-template name="LogoAddress"/>
		<!--#DG292 xsl:text>\pard\highlight{\f1\fs20 THE CLOSING INSTRUCTION WORKSHEET IS NOT INTENDED TO ACT AS A LEGAL DOCUMENT.  IT IS THE LENDER\rquote S RESPONSIBILITY TO ENSURE THAT THE WORKSHEET IS REVIEWED AND AMENDED AS REQUIRED PRIOR TO ITS ISSUANCE.\highlight0}
{\f1\fs24  \f2\fs18 _______________________________________________________________________________________________________\f1\fs24\par}</xsl:text-->
		<xsl:call-template name="HorizLine"/>
		<xsl:text><!--#DG530 \tab\tab\tab\tab\tab\tab\tab\tab\tab\tab\tab-->\par 
\pard \qr<!--#DG292 --> \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CurrentDate"/>
		<xsl:text>
\par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18
\par 
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Name">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SolicitorAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SolicitorAddress/Postal"/>
		<xsl:text>
\par 
\par Dear  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>,
\par 
\par }
</xsl:text>

<xsl:choose>
	<xsl:when test="$isProductHeloc">
	<xsl:text>
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 RE:\cell LENDER:\cell </xsl:text>
<xsl:value-of select="$lenderName"/>
<xsl:text> (the \'93Lender\'94)</xsl:text>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trbrdrh
\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}


\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell BORROWER(S):\cell </xsl:text>
		<xsl:for-each select="//SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trbrdrh
\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 RE:\cell BORROWER(S):\cell </xsl:text>
		<xsl:for-each select="//SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trbrdrh
\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}</xsl:text>
</xsl:otherwise>
</xsl:choose>

<xsl:text>
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>, if any:\cell 
</xsl:text>
		<xsl:for-each select="//SolicitorsPackage/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell REFERENCE NUMBER: \cell </xsl:text>		<!--#DG238-->
		<!-- xsl:value-of select="//SolicitorsPackage/ReferenceNum"/-->
		<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell SECURITY ADDRESS:\cell 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}
{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/PostalCode"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell CLOSING DATE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell APPROVED LOAN AMOUNT:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\pard \qj \widctlpar\tx540\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 </xsl:text>
		<xsl:call-template name="ThinLine"/><!--#DG530 -->
		<xsl:text>}

{\f1\fs18 \par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 A Mortgage Loan has been approved for the above-noted Borrower(s), in accordance with the terms and conditions outlined in the Commitment Letter attached. We have been advised that you will be acting for </xsl:text>
<xsl:choose>
	<xsl:when test="$isProductHeloc">
		<xsl:text>the Lender</xsl:text>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> (hereinafter called the \'93Lender\'94)</xsl:text>
	</xsl:otherwise>
</xsl:choose>
		
		<xsl:text> in attending to the preparation, execution and registration of the Mortgage against the property and ensuring that the interests of the Lender as Mortgagee are valid and appropriately secured. Please note that the Lender will not require or approve an interim report on title or draft documentation, including the Mortgage. The Lender will rely solely on you to ensure that the Mortgage is prepared in accordance with the instructions outlined herein. Any amendments required due to errors, omissions or non-compliance on your part will be for your account and your responsibility to correct. The Lender will also rely solely on you to confirm the identity of the Borrower(s) and </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>, if any, and to retain the evidence used to confirm their identity in your file. The Mortgage may be registered electronically, where available. </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/eRegistration"/>
		<xsl:text>\par \par }

\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 Any material facts which may adversely affect the Lender\rquote s position as a First Charge on the property are to be disclosed to the Lender prior to the advance of any funds. Similarly, any issues which may affect the Lender\rquote s security or any subsequent amendments to the Agreement of Purchase and Sale are to be referred to this office for direction prior to the release of funds. Our prior consent will be required relative to any requests for additional or secondary financing. If for any reason you are unable to register our Mortgage on the scheduled closing date, we must be notified at least 24 hours prior to that date and advised of the reason for the delay. No advance can be made after the scheduled closing date without the Lender\rquote s written authorization.\par \par}<!--#DG270 -->
{\f1\fs18 A postdated draft payable to your order or to your firm's order, in trust, in the amount of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/NetAdvanceAmount"/>
		<xsl:text> will be available for pick up four (4) days prior to the scheduled closing date at the office noted above. We will automatically advance this Mortgage to our mortgage system on the scheduled closing date and interest will accrue from that date unless we are notified by your office at least 24 hours prior to that date that the closing has been delayed. In the event that we are not given adequate notification of any delay in the scheduled closing date, you assume all responsibility for the interest accrued on this Mortgage.}\par\par

\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 <!-- #DG238 You must submit the Solicitor\rquote s Request for Funds together with the required documentation to our Financial Services Centre }{\f1\fs18\ul no later than three (3) days prior to closing}{\f1\fs18 . The Solicitor\rquote s Request for Funds should contain confirmation that all our requirements have been or will be met and should specifically outline any qualifications that will appear in your Final Report and certification of title.
\par 
\par The mortgage funds will be sent to you by PREPAID courier or by Electronic Fund Transfer and should be disbursed without delay provided you are satisfied that all requirements have been met and adequate precaution has been taken to ensure that the mortgage security will retain priority.  
\par \par -->Legal fees and all other costs, charges and expenses associated with this transaction 
are payable by the Borrower(s) whether or not the Mortgage proceeds are advanced.
\par }
\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
{\f1\fs18 In connection with this transaction we enclose the following documents:
\par 
\par 
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Mortgage Commitment Letter}{\f1\b0  - }
{\f1\b0 Please ensure that any outstanding conditions outlined in the Commitment Letter that are the responsibility of 
the Solicitor<!--#DG292 -->, are duly noted and that full compliance is confirmed prior to advance and included in your final report.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Solicitor<!--#DG292 -->\rquote s Instructions and Guidelines}<!-- #DG238 {\f1\b0  - }{\f1\b0 Self explanatory}-->\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
<!--#DG292 \pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Mortgage}{\f1\b0  - </xsl:text>
		<xsl:for-each select="//SolicitorsPackage/ProvincialClause/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}-->
<!--#DG270 \pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Schedules}{\f1\b0 - Schedule A\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Disclosure Statement\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}-->
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Solicitor<!--#DG292 -->\rquote s Request for Funds\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}

<!--#DG270 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Confirmation of Closing\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}-->

<!--#DG290 added -> Final report on Title-->
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs18}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Solicitor<!--#DG292 -->\rquote s Final report on Title\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}

}
{\f1\fs18 \par }
{\f1\fs18 Other required security/Special Conditions:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SolicitorSpecialInstructions"/>
		<xsl:text>
\par }

<!--#DG270 {\f1\fs18\par All mortgages are to be registered in the name of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
	 	<xsl:text>. Our address for service is </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line1">
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>.  You can contact our office, if you require a copy of our mortgage schedules and standard charge terms.\par }-->

{\f1\fs18\par }
\pard\plain \s22\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 We consent to your acting for the Lender as well as the Borrower(s) and/or </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text> provided that you disclose this fact to the Borrower(s) and/or </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text> and obtain their consent in writing and that you disclose to each party all information you possess or obtain which is or may be relevant to this transaction. 
\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
<!--#DG238 \pard\plain \s2\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\b\f1\fs18\ul FINANCIAL SERVICES CENTRE\par }-->
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18\par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
<!--#DG238 {\f1\fs18 Documents and funding requests should be directed to:\b Final Reports and supporting documentation should be mailed to:} \par \par 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>
\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/>
		<xsl:text>\par</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/>
		<xsl:text>
\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\f1\fs18\b 
\par Contact:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>
\par Attention:  Servicing Department\par}-->
<!--#DG292 -->{\f1\fs18 If you require any additional information, please feel free to call us at Tel: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Phone"/>
		<!--#DG292 xsl:text> Ext. </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderExtension"/-->
		<xsl:text>, or fax us at Fax: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>.\par }
<!--#DG292 \pard \ql \fi-360\li360\ri0\widctlpar\aspalpha\aspnum\faauto\ilvl12\adjustright\rin0\lin360\itap0 
{\f1\fs18 \par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 If you have any question or concerns, please feel free to call.-->{\f1\fs18 
\par 
\par Sincerely,\tab \tab 
\par 
\par 
\par 
\par 
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdministratorName"/>
		<xsl:text>\par <!--#DG238 Financial Services Administrator}--></xsl:text>		
		<xsl:value-of select="//SolicitorsPackage/Administrator/userDescription"/>
		<xsl:text>}</xsl:text>
	</xsl:template>
	
	<xsl:template name="GuideEn">
		<xsl:text>\par{<!--#DG286 \sectd-->\sect}
\pard\plain \s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\b\ul\f1\fs28 SOLICITOR<!--#DG292 -->\rquote S GUIDE}
{\f1\fs18\par\par\par }
\pard\plain \s16\qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs18 If you are satisfied that the Mortgagor(s) (also known as the \'93Borrower(s)\'94) has or will acquire a good and marketable title to the property described in our Commitment Letter, complete our requirements as set out below, and prepare a Mortgage as outlined herein.\par\par }

<!--#DG270 -->{\f1\fs18\b\ul Documentation:}{\f1\fs18  The Mortgage Form Number identified in our cover letter together with the applicable Schedule(s) must be registered. For Provinces with standard charge terms, complete the Charge/Mortgage using the instructions below together with the instructions indicated on the form sample.}
\par
\trowd \trgaph108\trleft-108 \cellx630\cellx10908 
\pard\plain \intbl{\f1\fs18 (a)}\cell 
{\f1\fs18\b Prepayment Privileges:} {\f1\fs18 </xsl:text>
		<!--#DG292 xsl:value-of select="//CommitmentLetter/Privilege/Line"/-->
		<xsl:text>Annual 10% Prepayment. When not in default, on each anniversary on the Interest Adjustment Date, the client may prepay without notice or penalty an amount not greater than 10% of the original principal amount of the Initial Loan or the amended, renewed or additional loan.
}\cell 
\pard \intbl \row 

\trowd \trgaph108\trleft-108 \cellx630\cellx10908 
<!--#DG292 -->\pard \intbl{\f1\fs18 (b)}\cell 
{\f1\fs18\b Leasehold Interest:} {\f1\fs18 If the Mortgagor(s) hold or will hold a leasehold interest in the property, the following wording must be included in a Schedule to the Charge/Mortgage:}{\par\par}{\f1\fs18 "By a lease dated the ______ day of __________________ between _________________________________ }{\f1\fs18 ________________, as Lessor, and the Mortgagor(s), as Lesee, notice of which was registered on }{\f1\fs18 ________________________________________________ in the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> Division of }{\f1\fs18 ______________________________________________________________________as Instrument No. }{\f1\fs18 ______________________________, the Lessor did lease unto the Mortgagor(s), his heirs, executors, administrators, }{\f1\fs18 successors and assigns, the property described in Box 5 of the Charge/Mortgage for a term of ________ (    ) years }{\f1\fs18 commencing from the ______ day of ____________________, subject to the rents, covenants and conditions contained }{\f1\fs18 within the lease. The Mortgagor(s) hereby charge(s) all of his leasehold interest in the property to </xsl:text>
		<xsl:choose>
			<xsl:when test="$isProductHeloc">
				<xsl:value-of select="$lenderName"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text>, }{\f1\fs18 together with all of his rights under the lease, including his option, if any, to purchase the said property, subject to any }{\f1\fs18 proviso for redemption." }\cell \pard \intbl \row 
\pard \par
<!--#DG238 \pard\plain \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
{\b\f1\fs18\ul Documentation:}{\f1\fs18  The Mortgage Form Number identified in our cover letter together with the applicable Schedule(s) must be registered. }
{\f1\fs18 \par \par }
{\b\f1\fs18\ul Mortgage Requirements:}{\f1\fs18  The Mortgage Document is to be registered as follows:\par \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Approved Loan Amount:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Interest Rate (Compounded </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CompoundingFrequency"/>
		<xsl:text>):\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Principal and Interest Payment:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PandIPaymentMonthly"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Interest Adjustment Date:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/IADDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 First Monthly Payment Date:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FirstPaymentDateMonthly"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \sa240\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Maturity/Renewal Date:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}\par-->

<!--#DG270 \pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Borrower Identification:}{\f1\fs18  You are required to confirm the identity of the Borrower(s) and Guarantor(s), if any.  The Lender also requires you to forward a photocopy of the evidence used to confirm their identity together prior to your release of funds. \par \par}

{\b\f1\fs18\ul Funding:}{\f1\fs18  The mortgage funds will be made available to you no later than 24 hours before the scheduled closing and should be disbursed without delay provided you are satisfied that all requirements have been met and adequate precaution has been taken to ensure that the mortgage security will retain priority. Interest will accrue from this date unless the Bank receives written notification from your office at least 24 hours prior to the scheduled closing date.  If the Bank does not receive adequate notice from your office, you assume responsibility for the accrued interest. \par \par
If the closing of this transaction is delayed more than 5 business days, all funds must be returned to our office immediately.\par \par}

{\b\f1\fs18\ul Confirmation of Closing:}{\f1\fs18  We require that you complete and return the enclosed Confirmation of Closing within 24 hours of the scheduled closing date to confirm that this transaction has closed, that funds were disbursed. \par \par}

{\b\f1\fs18\ul Title Insurance Requirement:}{\f1\fs18  If this is a non-purchase mortgage transaction in any province or a purchase transaction in Ontario, the Lender requires the Borrower to purchase a lender title insurance policy.  Please contact our office to ensure the Title Insurance Firm is acceptable to us. \par \par}
-->
{\b\f1\fs18\ul Searches:}
{\f1\fs18  You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. Prior to releasing any mortgage proceeds, the solicitor will carry out the necessary searches with respect to any liens, encumbrances, executions; that may be registered against the property.</xsl:text>
		<!--#DG270 xsl:value-of select="//SolicitorsPackage/Execution"/-->
		<xsl:text> It is your responsibility to ensure that there are no work orders or deficiency notices outstanding against the property, that all realty taxes and levies which have or will become due and payable up to the interest adjustment date are paid to the Municipality. If your search reveals title has been transferred since the date of the Offer to Purchase and before the closing date you must notify us immediately and discontinue all work on this transaction unless notified otherwise. <!--#DG270 Immediately prior to registration of the mortgage, you are to obtain a Sheriff\rquote s Certificate/General Registry Search indicating that there are no writs of execution on file against the mortgagor(s), </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>(s), if any, or any previous owner, which would adversely affect our security.
\par }
{\f1\fs18  You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. Complete searches on a timely basis to ensure there are no executions against the applicant(s). It is your responsibility to ensure that there are no work orders or deficiency notices outstanding against the property that all realty taxes and levies which have or will become due and payable up to the interest adjustment date are paid to the Municipality. If your search reveals title has been transferred since the date of the Offer to Purchase and before the closing date you must notify us immediately and discontinue all work on this transaction unless notified otherwise.-->\par\par}

{\b\f1\fs18\ul Survey Requirements:}{\f1\fs18  You must obtain and review a survey or a surveyor's certificate / Real Property Report (AB, NL, and SK)/Certificate of Location (PQ) completed by a recognized land surveyor and dated within the last twenty (20) years. Satisfy yourself from the survey that the position of the buildings on the land complies with all municipal, provincial and other government requirements. Where an addition has been made since the date of the survey, an updated survey is required unless there is no doubt that the addition is also clearly within the lot lines and meets all setback requirements. If you are unable to comply with our survey requirements, please contact our office as soon as possible. \par }

<!--#DG292 \pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs18 \par }
\pard \qj \widctlpar\tx1800\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Condo/Strata Title:}{\f1\fs20 <!- -#DG238 You are to confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. You are to review the Condominium Corporation\rquote s Declaration and Bylaws and confirm they contain nothing derogatory to our security. You are to assign the voting rights to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> (if applicable).- -> You are to review the condominium/strata documentation to ensure that it is satisfactory and identify all requirements applicable to us (including notice provisions) which are to be set out in your Report on Title and obtain a clear Estoppel certificate and certificate of insurance. \par}-->

<!--#DG238 \pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs18 \par} 
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Survey:}{\f1\fs18  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SurveyClause"/>
		<xsl:text>. \par }-->
		
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs18 \par }
\pard \qj \widctlpar\tx1440\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Fire Insurance:}{\f1\fs18  You must ensure, prior to advancing funds, that fire insurance coverage for building(s) on the property is in place for not less than their full replacement value with first loss payable to </xsl:text>
		<xsl:choose>
			<xsl:when test="$isProductHeloc">
				<xsl:text>us</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> as Mortgagee at the address shown on the Charge/Mortgage and including a duly completed standard mortgage clause approved by the Insurance Bureau of Canada. Policies must not contain a co-insurance or similar clause that may limit the amount payable. Unless otherwise noted, we will not require a copy of this policy.
\par\par}

<!--#DG292 \pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18\ul New Construction:}{\f1\fs18  If this transaction is a new construction, you must ensure that an acceptable Certificate of Completion and New Home Warranty Certificate are obtained prior to advancing the mortgage proceeds. \par \par}-->

<!--#DG238 {\b\f1\fs18\ul Matrimonial Property Act:}
{\f1\fs18  You are to certify that all requirements under the Matrimonial Property Act in your province, where applicable, have been met and that the status does not in any way affect our mortgage/charge.\par \par \par }-->
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18\ul <!--#DG292 Mortgage -->Advances:}{\f1\fs18   When all conditions precedent to this transaction have been met, funds in the amount of  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text> will be advanced with the following costs being deducted from the advance by the Lender;\par }
\pard \qj \fi720\li720\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin720\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Mortgage Insurance Premium\par (Included in Total Loan Amount)\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Premium"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Provincial Sales Tax on Premium\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
		<xsl:for-each select="//SolicitorsPackage/Fees/Fee">
			<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 TOTAL\tab \cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TotalDeductionsAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
<!--#DG300 \pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}-->
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Interest Adjustment (daily per diem)\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PerDiemInterestAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par <!--#DG238 The net Mortgage proceeds, payable to you, in trust, will be sent to your office prior to the scheduled closing date, which is anticipated to be </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>. 
-->
The net Mortgage proceeds in the amount of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/NetAdvanceAmount"/><!--#DG270 -->
		<xsl:text>, payable to you, in trust, will be available for pick up four (4) days prior to the scheduled closing date at the address shown on the Charge/Mortgage.\par \par }</xsl:text>
		<!--  Internal refi -->
		<!--#DG292 xsl:if test="//SolicitorsPackage/InternalRefi">
			<xsl:text>
\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0
{\b\f1\fs18\ul Refinances:}{\f1\fs18 If the purpose of this Mortgage is to refinance existing debt with </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text>, we will retain the following additional funds to discharge existing mortgage(s) owing to us;\par }
\pard \qj \keep\keepn\fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Existing </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b Other Debts to be Paid:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
</xsl:text>
			</xsl:for-each>
			<xsl:text>\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance, if any, is to be paid to the Borrower(s).
\par }
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\par \par}
</xsl:text>
		</xsl:if>
		<!- -  External refi - ->
		<xsl:if test="//SolicitorsPackage/ExternalRefi">
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 \ul Refinances:}{\f1\fs18  If the purpose of this Mortgage is to refinance existing debt, the balance
 of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Existing </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LPDescription"/>
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b Other Debts to be Paid:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance, if any, is to be paid to the Borrower(s).
\par \par \par}</xsl:text>
		</xsl:if-->
		
		<xsl:text>{\b\f1\fs18\ul Construction/Progress Advances:}{\f1\fs18   <!--#DG238 If the security for this Mortgage is a new property and we a
re advancing by way of progress advances, the advances will be done on a cost-to-complete basis.-->
If the security for this Mortgage is a new property and we are advancing by way of progress advances, we will require your written request for each advance together with a direction executed by the Mortgagor(s) directing us to pay the funds to you, in trust, at least five (5) days prior to the advance of funds.
You are responsible for the disbursement of all loan advances made payable to you in trust and to ensure the priority of each advance by conducting all necessary searches and ensure that the requirements of the appropriate Provincial Construction Lien Act have been met before each advance is made. You are to retain appropriate amounts from the Mortgage proceeds to comply with Provincial lien holdback requirements. Unless otherwise noted, </xsl:text>
		<xsl:choose>
			<xsl:when test="$isProductHeloc">
				<xsl:text>we</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> will require a copy of the executed Certificate of Completion and Possession form and, where applicable, the Occupancy Certificate.\par \par }
<!--#DG270 {\b\f1\fs18\ul Assignment Of Rents:}{\f1\fs18  If the Mortgage Approval provides for rental of units in the Property, an Assignment of Rents must be included in the Standard Charge Terms.  In addition, </xsl:text> 
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> will require registration of Financing Statement under the Personal Property Security Act for the term of the mortgage. \par \par }-->

{\b\f1\fs18\ul Disclosure:}{\f1\fs18  The Mortgagor(s) and the </xsl:text><!--#DG238 -->
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text> (if any) must sign a Statement of Disclosure. One copy is to be given to the Borrower(s) and a second copy is to be forwarded to </xsl:text>
		<xsl:choose>
			<xsl:when test="$isProductHeloc">
				<xsl:text>us</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> with the Solicitor<!--#DG292 -->\rquote s Final Report on Title. <!--#DG238 Please ensure that the applicable provincial legislation and timeframes regarding Disclosure Statements are adhered to.--> If a Statement of Disclosure cannot be signed, you must obtain a waiver from the Mortgagor(s) and the </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text> (if any).\par\par}

\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Planning Act:}{\f1\fs18  You must ensure that the Mortgage does not contravene the provisions of the Planning Act as amended from time to time, and that the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.\par \par}

<!--#DG292 \pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Matrimonial Property Act:}{\f1\fs18 You are to certify that all requirements under the Matrimonial Property Act in your province, where applicable, have been met and that the status does not in any way affect our mortgage/charge.\par\par}-->

\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs18\ul Renewal for Guarantor(s):}{\f1\fs18 If applicable, the following wording must be included in a Schedule to the Charge/Mortgage: 
"In addition to the Guarantor's promises and agreements contained in this Mortgage, the Guarantor(s) also agree(s) that at the sole discretion of </xsl:text>
		<xsl:choose>
			<xsl:when test="$isProductHeloc">
				<xsl:value-of select="$lenderName"/>  
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>, the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s). Any such renewal of this Mortgage binds the Guarantor(s), whether or not the renewal agreement has been signed by the Guarantor(s)."  \par }

\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs18 \par }{\b\f1\fs18\ul\cf1 Corporation as a Borrower:}{\f1\fs18\cf1  If the Mortgagor(s) or </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>(s) is a corporation, you must obtain a certified copy of the directors\rquote  resolution and the borrowing by-law of the corporation authorizing the Charge/Mortgage and ensure that the corporation is duly incorporated under applicable Provincial or Federal laws with all necessary power to charge its interest in the property and include these documents(s) with the Solicitor\rquote s Final Report on Title.\par }{\f1\fs20 
\par }

{\b\f1\fs18\ul Solicitor<!--#DG292 -->\rquote s Report:}{\f1\fs18  You must submit the Solicitor<!--#DG292 -->\rquote s Final Report on Title, on the enclosed form and with the stated enclosures, within 30 days after the final advance is made. If your report cannot be submitted in this time frame, you must provide us with a letter explaining the reason(s) for the delay. Failure to comply with any of the above instructions may 
result in discontinued future dealings with you or your firm.\par \page}</xsl:text><!--#DG238 -->
	</xsl:template>

	<xsl:template name="RequestForFundsEn">
		<xsl:text>\par{<!--#DG286 \sectd-->\sect}</xsl:text>
		<!--#DG238 added whole new table with logo and branch address -->
		<xsl:call-template name="LogoAddress"/>
		<xsl:text>\par 
\pard \ql \fi720\li5040\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin5040\itap0 

\trowd \trgaph108\trrh719\trleft1800
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx8820
\pard\plain \s6\qc \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\ul\f1\fs28 SOLICITOR<!--#DG292 -->\rquote S REQUEST FOR FUNDS\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh719\trleft1800
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx8820
\row 
}
\pard \ql \fi720\li5040\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin5040\itap0 
{\f1\fs18\par\par\par}
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 
\cellx4745\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 FAX TO:\cell \b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 FROM:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:value-of select="//SolicitorsPackage/SolicitorAddress1"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 
\cellx4745\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ATTENTION:\cell \b0
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>\cell \cell }
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>
}
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 DATE:\cell }{\f1\fs18 ________________\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Phone: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Phone"/>
		<xsl:text>\par Fax: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\pard \ql \widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\b\f1\fs18 </xsl:text>
		<xsl:call-template name="HorizLine"/><!--#DG530 -->
		<xsl:text>}
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
<!-- {\b\f1\fs18 MORTGAGE REFERENCE NUMBER:\cell \b0 </xsl:text><xsl:value-of select="//SolicitorsPackage/MortgageNum"/><xsl:text>\cell } -->
{\b\f1\fs18 MORTGAGE REFERENCE NUMBER:\cell \b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/ReferenceNum"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
</xsl:text>

<xsl:choose>
<xsl:when test="$isProductHeloc">
<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 LENDER:\cell \b0 </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
</xsl:text>
</xsl:when>
</xsl:choose>

<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGOR(S):\cell \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MUNICIPAL ADDRESS:\cell \b0 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGE AMOUNT:\cell \b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 CLOSING DATE:\cell \b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 </xsl:text>
		<xsl:call-template name="HorizLine"/><!--#DG530 -->
		<xsl:text>}
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs18 \par }
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 I/We request funds in the amount of </xsl:text>
		<!--#DG348 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/SolicitorsPackage/NetAdvanceAmount"/>
		<xsl:text> for closing on </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>.}
{\f1\fs18 \par }
\pard \s15\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 I/We acknowledge that there are no amendments and/or schedules to the Agreement of Purchase and Sale which 
affect the purchase price or deposit amount as set out in your Mortgage Commitment Letter.\par }
{\f1\fs18 \par }
{\b\f1\fs18 I/We confirm that:\par \par }

\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 \cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }{\f1\fs18 all terms and conditions set out in your instructions have been satisfied}{\cell }\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }{\f1\fs18 all CMHC / Genworth terms and conditions have been satisfied}{\cell }\pard 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\pard \ql \widctlpar\faauto\itap0 {
\par }{\b\f1\fs18 DELIVERY OF FUNDS IS REQUESTED AS FOLLOWS}{
\par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 \cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }
{\f1\fs18 I / We will arrange for pick-up of funds from your office five (5) days prior to the scheduled closing date.}{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }
{\f1\fs18 ]}{\cell }{\f1\fs18 
Deposit funds to my/our </xsl:text>
		<xsl:choose>
			<xsl:when test="$isProductHeloc">
				<xsl:text>Lender</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> Trust Account # ______________________ at Branch # __________.}{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 \cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }

\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }
{\f1\fs18 Deliver funds courier collect via _________________________ courier account # _________________.}{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }

<!--#DG292 \trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }
{\f1\fs18 EFT - Electronic Fund Transfer. Instituiton # _____________, Transit # __________, Account # __________.}
\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }-->

\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360\cltxlrtb\clftsWidth3\clwWidth540 
\cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\f1\fs18 [}{\cell }{\f1\fs18 ]}{\cell }
{\f1\fs18 * Wire funds to : 
______________________________________________________________________________}{
\par }{\f1\fs18 ____________________________________________________________________________________________}{
\par }{\f1\fs18 ____________________________________________________________________________________________}{
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth540 \cellx900\cltxlrtb\clftsWidth3\clwWidth10008 \cellx10908\row }\pard \ql \widctlpar\faauto\itap0 {
\par }
{\b\f1\fs18 * If this last option is chosen, I/We understand that you will retain sufficient funds to cover all wire costs.}{
\par 
\par }\pard \ql \widctlpar\tx5000\tx7000\aspalpha\aspnum\faauto\adjustright\itap0 {\tab }{\f1\fs18 ___________________________________________________ }{
\par \tab \tab }{\f1\fs18 Signature of Solicitor<!--#DG292 -->}{
\par }\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 { 
\par }
<!--#DG238 {\pard \par \page }
{\f1\fs18 DOCUMENTS REQUIRED PRIOR TO RELEASE OF THE MORTGAGE:}\par\par\pard \ql \widctlpar\tx50\tx200\tx400\aspalpha\aspnum\faauto\adjustright\itap0{\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Pre-Authorized Chequing (PAC) form together with "VOID" cheque } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Signed Statement of Disclosure } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Signed Statutory Declaration } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Well Water Test/Certificate, indicating that the water is potable and fit for human consumption } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Septic Tank Certificate } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 New Home Warranty Certificate of Possession } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 New Home Warranty Builder's Registration Number and Unit Enrollment Number } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Assignment of Condominium Voting Rights } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Assignment of Proceeds } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Signed Authorization to Send Assessment and Tax Bill (#45-06-20) PEI } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Other (specify) _______________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 _______________________________________________________________________________________ } \par}\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0{ \par }--></xsl:text>
	</xsl:template>

	<xsl:template name="FinalReportEn">
		<xsl:text>\par{<!--#DG286 \sectd-->\sect}
\trowd \trgaph108\trleft1500\trbrdrt\brdrs\brdrw15\brdrcf1 
\trbrdrl\brdrs\brdrw15\brdrcf1 
\trbrdrb\brdrs\brdrw15\brdrcf1 
\trbrdrr\brdrs\brdrw15\brdrcf1 
\trbrdrh\brdrs\brdrw15\brdrcf1 
\trbrdrv\brdrs\brdrw15\brdrcf1 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw15\brdrcf1 
\clbrdrl\brdrs\brdrw15\brdrcf1 
\clbrdrb\brdrs\brdrw15\brdrcf1 
\clbrdrr\brdrs\brdrw15\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth6500 \cellx8000
\pard \qc \widctlpar\intbl\faauto 
{\par }{\b\f1\fs28\ul SOLICITOR<!--#DG292 -->\rquote S FINAL REPORT ON TITLE\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1500\trbrdrt\brdrs\brdrw15\brdrcf1 
\trbrdrl\brdrs\brdrw15\brdrcf1 
\trbrdrb\brdrs\brdrw15\brdrcf1 
\trbrdrr\brdrs\brdrw15\brdrcf1 
\trbrdrh\brdrs\brdrw15\brdrcf1 
\trbrdrv\brdrs\brdrw15\brdrcf1 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw15\brdrcf1 
\clbrdrl\brdrs\brdrw15\brdrcf1 
\clbrdrb\brdrs\brdrw15\brdrcf1 
\clbrdrr\brdrs\brdrw15\brdrcf1 
\cltxlrtb\clftsWidth3\clwWidth6500 \cellx8000
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\par \par }
\pard \ql \widctlpar\tx100\tx7000\aspalpha\aspnum\faauto\adjustright\itap0 
{\tab }{\b\f1\fs18 Solicitor<!--#DG292 -->\rquote s Reference No. ____________________________ }{\tab }{\b\f1\fs18 _________________________}
{\par \tab \tab }{\b\f1\fs18 Date}
{\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{ \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth918 \cellx810
\cltxlrtb\clftsWidth3\clwWidth10098 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 TO:\cell </xsl:text>
<xsl:choose>
	<xsl:when test="$isProductHeloc">
		<xsl:value-of select="$lenderName"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
	</xsl:otherwise>
</xsl:choose>
		
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>
\par \par Attention: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdministratorName"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth918 \cellx810
\cltxlrtb\clftsWidth3\clwWidth10098 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\fs18 \par }
{\f1\fs18 In accordance with your instructions, we have acted as your solicitors in the following transactions. We have registered a Mortgage/Charge (or Deed of Loan, if applicable) the "Mortgage" on the appropriate form in the appropriate </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> and make our final report as follows:}
{\fs18 \par \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Mortgagor(s)\cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>\cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Municipal Address of property \cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Legal Address of Property \cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \par }
{\b\f1\fs18 It is our opinion that:}
{\f1\fs18 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (1)\cell A valid and legally binding First Mortgage in favour of </xsl:text>
<xsl:choose>
	<xsl:when test="$isProductHeloc">
		<xsl:value-of select="$lenderName"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
	</xsl:otherwise>
</xsl:choose>
		<xsl:text> for the full amount of the monies advanced was registered on _____________________________________________ in the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> Division of _____________________________________________________ as Instrument No. ___________________________.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (2)\cell The Mortgagor(s) have good and marketable title in fee simple to the property free and clear of any prior encumbrances, other than the minor defects listed below which }{
\b\f1\fs18 do not}{\f1\fs18  affect the priority of the Mortgage or the marketability of the property. All lien holdback/retention period requirements have been met. Easements,<!--#DG238--> Encroachments and Restrictions etc. are listed below:
\par ________________________________________________________________________________________________ 
\par ________________________________________________________________________________________________ 
\par ________________________________________________________________________________________________ 
\par ________________________________________________________________________________________________ 
\par ________________________________________________________________________________________________ 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\cell }{\b\f1\fs18 Condominium/strata unit(s) if applicable:}{\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\f1\fs18 \cell a)\cell <!--#DG292 We confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. We have reviewed the Condominium Corporation's Declaration and Bylaws and confirm they contain nothing derogatory to your security. We have assigned the voting rights to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>, if applicable.-->
The condominium/strata corporation has been notified of the Mortgage and of your voting rights and has been requested to advise you if the Mortgagor(s) default in paying common expenses.\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908
\row 
}
<!--#DG292 -->
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell b )\cell <!--#DG300 We confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. We have reviewed the Condominium Corporation's Declaration and Bylaws and confirm they contain nothing derogatory to your security. We have assigned the voting rights to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>, if applicable.-->We have obtained an Estoppel Certificate and Certificate of Insurance in accordance with your instructions and have determined that both documents are satisfactory. We further confirm that any arrears noted in the Estoppel Certificate have been paid. 
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908
\row 
}

\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell c )\cell 
All necessary steps have been taken to confirm your right to vote should you wish to do so.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell d)\cell Applicable notice provision, if any: __________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth1109 \cellx1739
\cltxlrtb\clftsWidth3\clwWidth9169 
\cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (3)\cell All restrictions have been complied with in full and there are no work orders or deficiency notices outstanding against the property.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (4)\cell All taxes and levies due and payable on the property to the Municipality have been paid up to _______________________________________.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (5)\cell The Mortgage does not contravene the provisions of the Planning Act as amended from time to time, because the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (6)\cell A true copy of the Mortgage (including Standard Charge Terms, all Schedules to it, and the Mortgagor(s) Acknowledgement and Direction, if applicable) has been given to each Mortgagor. \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 (7)\cell Fire Insurance coverage has been arranged in accordance with your instructions for full replacement cost(s) with loss payable to </xsl:text>
<xsl:choose>
	<xsl:when test="$isProductHeloc">
		<xsl:value-of select="$lenderName"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
	</xsl:otherwise>
</xsl:choose>
		<xsl:text> in accordance with the I.B.C. standard mortgage clause.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth10278 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \par \page \par \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908\pard 
\ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell Insurance Company: ______________________________ 
\par \cell Broker: _______________________________________ 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell Amount of Insurance: _____________________________ 
\par \cell Policy No: _____________________________________ 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell Effective Date: ___________________________________ 
\par \cell Expiry Date: ____________________________________ 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth738 \cellx630
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx5769
\cltxlrtb\clftsWidth3\clwWidth5139 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \par \par}
{\f1\fs18\b The following documents are enclosed for your file:}
{\f1\fs18 \par \par }

\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Duplicate registered copy of the Charge/Mortgage, or Deed of Loan including Standard Charge Terms, all Schedules to it and acknowledgement of receipt of Mortgagor(s) and </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>(s), if any}\cell \pard \intbl \row\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell 
\pard \intbl{\f1\fs18 Electronic Charge and Acknowledgement and Direction {\f1\fs18\b (Ontario electronic registration counties only)}<!--#DG292 , if applicable-->}\cell 
\pard \intbl \row 
<!--#DG238 \pard \intbl {\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Instrument Number 
{\f1\fs18 (Teranet Registration) }}\cell 
\pard \intbl \row -->

\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl {\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Guarantee Agreement {\f1\fs18\b (Ontario electronic registration counties only)}<!--#DG292 , if applicable-->}\cell \pard \intbl \row 
<!--#DG238 \pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Certificate of Title (or Provincial Comparable) }\cell \pard \intbl \row -->
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Survey or Surveyor\rquote s Certificate}\cell \pard \intbl \row
<!--#DG238 \pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Survey or Surveyor's Certificate or Title Insurance in lieu of a Survey, if applicable }\cell \pard \intbl \row -->
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Title Insurance Policy and Schedules A &amp; B to Policy}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Registered Amending Agreement }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Execution Certificate }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Estoppel Certificate and Certificate of Insurance }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Municipal Tax Certificate}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Certificate of Completion and Possession}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Final Inspection }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 New Home Warranty Certificate of Possession }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Occupancy Certificate/Permit}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Certificate of Independent Legal Advice}\cell \pard \intbl \row <!--#DG292 -->
<!--#DG292 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Personal Guarantee and Letter of Independent Legal Advice}\cell \pard \intbl \row -->
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Statement of Disclosure}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Certified copy of directors\rquote\~resolution and the borrowing by-law of the corporation authorizing the Charge/Mortgage}\cell \pard \intbl \row 
<!--#DG292 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Sheriff's Certificate/GR Search}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Signed Statutory Declaration}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Fire Insurance Policy}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Condominium Corporation Insurance Binder}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Assignment of Site/Lot Lease to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Zoning Certificate/Memorandum (Ontario properties only)}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Declaration as to Possession (Manitoba properties only)}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Registered Assignment of Rents}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 General Security Agreement (GSA) Registered under PPSA}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Statement of Funds Received and Disbursed }\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!- -#DG286 - ->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Verification of Payout of Debts }\cell \pard \intbl \row -->
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Copy of lease (leasehold properties only)<!--#DG292 , if applicable -->}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Other (specify)}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 _______________________________________________________________________________________________}\cell \pard \intbl \row 
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
\pard \intbl{\f1\fs18 [}\cell
{\f1\fs18 ]}\cell \pard \intbl
{\f1\fs18 _______________________________________________________________________________________________}\cell \pard \intbl \row \pard \intbl
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908 \pard \intbl<!--#DG286 -->
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 _______________________________________________________________________________________________}\cell \pard \intbl \row
\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908\pard \intbl
{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 _______________________________________________________________________________________________}\cell \pard \intbl \row \pard\par\par\pard \ql \widctlpar\tx5000\tx7000\aspalpha\aspnum\faauto\adjustright\itap0
{\tab 
{\f1\fs18 ___________________________________________________ }\par\tab\tab 
{\f1\fs18 Signature of Solicitor<!--#DG292 -->}\par}\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{ \par }
</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- French template section                                             			-->
	<!-- ************************************************************************ 	-->
	<!--#DG530 brand new copy of English templates translated to French -->

	<xsl:template name="SolicitorInstructionsFr">
		<xsl:call-template name="LogoAddress"/>
		<xsl:call-template name="HorizLine"/>
		<xsl:text>\pard\qr\f1\fs18\par 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/CurrentDate"/>
		<xsl:text>\par
\pard\qj\par\par

</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Name"/>
		<xsl:if test="/*/SolicitorsPackage/Solicitor/Name">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Address1"/>
		<xsl:if test="/*/SolicitorsPackage/Solicitor/Address1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="/*/SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/SolicitorAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/SolicitorAddress/Postal"/>
		<xsl:text>\par\par 
Ma�tre </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>,\par\par 

\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx900\cellx3870\cellx11006
\pard\intbl
\b OBJET\~:\cell 
EMPRUNTEUR(S)\~:\cell 
</xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell\row

\pard\intbl
\cell </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>, le cas �ch�ant :\cell 
</xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell \row 

\pard\intbl
\cell NUM�RO DE R�F�RENCE :\cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell\row 

\pard\intbl
\cell ADRESSE DE S�CURIT� :\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:if test="/*/SolicitorsPackage/PropertyAddress/Line2">
		  <xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Line2"/>
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/PostalCode"/>
		<xsl:text>\cell \row 

\pard\intbl
\cell DATE DE CL�TURE :\cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell \row 

\pard\intbl
\cell MONTANT DU PR�T APPROUV� :\cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell \row 

\b0\pard 
</xsl:text>
		<xsl:call-template name="ThinLine"/>
		<xsl:text>\pard \qj \f1\fs18 
Un pr�t hypoth�caire a �t� approuv� pour le ou les emprunteur(s) susmentionn�(s), conform�ment 
aux modalit�s et aux conditions d�crites dans la Lettre d'engagement hypoth�caire ci-jointe. Nous 
avons �t� inform�s que vous agirez au nom de </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> (ci-apr�s appel� le � pr�teur �) pour aider � la pr�paration, � l'ex�cution et � 
l'enregistrement de l'hypoth�que sur la propri�t� et pour vous assurer que les int�r�ts du 
pr�teur en tant que cr�ancier hypoth�caire sont valables et garantis de fa�on appropri�e. 
Veuillez noter que le pr�teur n'exigera pas, ni n'approuvera de rapport interm�diaire sur les 
titres ou les documents provisoires, y compris le pr�t hypoth�caire. Le pr�teur se fiera 
uniquement � vous pour s'assurer que le pr�t hypoth�caire est pr�par� conform�ment aux 
instructions d�crites aux pr�sentes. Toute modification requise en raison d'erreurs, d'omissions 
ou de non-conformit� de votre part sera � votre charge et la correction vous incombera. Le 
pr�teur se fiera uniquement � vous pour confirmer l'identit� de l'emprunteur ou des emprunteurs 
et du </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>, le cas �ch�ant, et pour conserver la preuve utilis�e pour confirmer leur identit� 
dans vos dossiers. L'hypoth�que peut �tre enregistr�e par voie �lectronique, s'il y a lieu. </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/eRegistration"/>
		<xsl:text>\par \par 

Les faits importants, le cas �ch�ant, susceptibles de toucher de fa�on d�favorable la position du 
pr�teur � titre de charge de premier rang sur la propri�t� doivent �tre divulgu�s au pr�teur 
avant le d�caissement de tout fonds. Pareillement, toute question qui peut toucher la s�ret� du 
pr�teur ou toute modification subs�quente � l'entente d'achat et de vente doit �tre transmise � 
ce bureau afin d'obtenir des indications avant le d�blocage des fonds. Notre consentement 
pr�alable sera requis pour toutes demandes de financement suppl�mentaires ou secondaires. Si pour 
une raison quelconque, vous n'�tes pas en mesure d'enregistrer notre pr�t hypoth�caire � la date 
de cl�ture pr�vue, nous devons en �tre avis�s au moins 24 heures avant cette date et �tre 
inform�s du motif du retard. Aucun d�caissement ne peut �tre effectu� apr�s la date de cl�ture 
pr�vue sans l'autorisation �crite du pr�teur.\par\par

Vous pourrez aller chercher une traite postdat�e payable � votre ordre ou � l'ordre de votre 
�tude, en fiducie, au montant de </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/NetAdvanceAmount"/>
		<xsl:text> quatre (4) jours avant la date de cl�ture pr�vue au bureau indiqu� ci-dessus. Nous 
d�bourserons automatiquement ce pr�t hypoth�caire dans notre syst�me hypoth�caire � la date de 
cl�ture pr�vue et l'int�r�t courra � compter de cette date � moins que nous ne soyons avis�s par 
votre bureau au moins 24 heures avant cette date que la cl�ture a �t� report�e. Si nous ne 
recevons pas d'avis appropri� de tout retard de la date de cl�ture pr�vue, vous assumerez toute 
la responsabilit� pour l'int�r�t couru sur ce pr�t hypoth�caire.\par\par

Les frais juridiques et les autres co�ts, frais et d�penses associ�s � cette op�ration sont 
payables par le ou les emprunteur(s), que les produits hypoth�caires soient d�bours�s 
ou non.\par\par

Relativement � cette op�ration, nous joignons les documents suivants :\par\par

\trowd\trgaph108\trleft180\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx555\cellx10908

\pard\intbl
\f3\'d8\f1 \cell 
\pard\intbl
\qj\ul\b\i Lettre d'engagement hypoth�caire\ulnone\b0\i0   - Veuillez vous assurer que toutes les 
conditions en suspens stipul�es dans la lettre d'engagement qui incombent au notaire ou � 
l'avocat, sont d�ment not�es et que l'enti�re conformit� est confirm�e avant le d�caissement de 
l'avance et inclus dans votre rapport final.\cell\row 

\pard\intbl
\f3\'d8\f1 \cell 
\pard\intbl
\qj\ul\b\i Instructions et directives � l'intention du notaire ou de l'avocat\ulnone\b0\i0\cell\row 

\pard\intbl
\f3\'d8\f1 \cell 
\pard\intbl
\qj\ul\b\i Demande de d�boursement de fonds de la part du notaire ou de l'avocat\ulnone\b0\i0\cell\row 

\pard\intbl
\f3\'d8\f1 \cell 
\pard\intbl
\qj\ul\b\i Rapport final sur les titres du notaire ou de l'avocat\ulnone\b0\i0\cell\row 

\pard\qj\par
Autres garanties requises/Conditions sp�ciales :\tab </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/SolicitorSpecialInstructions"/>
		<xsl:text>\par\par 

Nous consentons � ce que vous agissiez au nom du pr�teur ainsi que de l'emprunteur ou des 
emprunteurs ou du </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text> pourvu que vous d�voiliez ce fait � l'emprunteur ou aux emprunteurs ou au </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text> et obteniez leur consentement par �crit et que vous divulguiez � chaque partie tous 
les renseignements que vous d�tenez ou obtenez, lesquels  sont ou peuvent �tre pertinents � cette 
op�ration.\par 
\pard\par\par 

\pard\qj 
Si vous avez besoin d'autres renseignements, n'h�sitez pas � communiquer avec nous au num�ro </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/BranchAddress/Phone"/>
		<xsl:text>, ou � nous t�l�copier au num�ro </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>.\par\par\par 


Nous vous pr�sentons, Ma�tre, nos sinc�res salutations.,\par\par\par\par\par 



</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/AdministratorName"/>
		<xsl:text>\par 
</xsl:text>		
		<!-- qk fix xsl:value-of select="/*/SolicitorsPackage/Administrator/userDescription"/-->
		<xsl:text>Administratrice</xsl:text>
		<xsl:text>\par 
</xsl:text>
	</xsl:template>
	
	<xsl:template name="GuideFr">
		<xsl:text>\sect


\pard\keepn\s1\ul\b\fs28 
GUIDE � L'INTENTION DU NOTAIRE OU DE L'AVOCAT\ulnone\b0\fs18\par\par\par


\pard\qj\tx4140 Si vous �tes assur� que le ou les d�biteur(s) hypoth�caire(s) (�galement 
appel�(s) le ou les � emprunteur(s)) � d�tient (d�tiennent) ou acquerra (acquerront) un bien ou 
un titre n�gociable sur la propri�t� d�crite dans notre Lettre d'engagement hypoth�caire, 
veuillez inscrire les renseignements demand�s ci-dessous, et pr�parer un pr�t hypoth�caire comme 
il est d�crit aux pr�sentes.\par\par 

\b\ul Documentation :\ulnone\b0  le num�ro de formulaire hypoth�caire indiqu� dans notre lettre 
d'accompagnement ainsi que le ou les annexe(s) applicable(s) doit �tre enregistr�. Pour les 
provinces ayant des conditions standards relatives aux frais, veuillez remplir le formulaire 
Charge/Pr�t hypoth�caire � l'aide des instructions ci-dessous ainsi que celles indiqu�es dans le 
formulaire type.\par

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx630\cellx10908
\pard\intbl
a)\cell\b Privil�ges de remboursement anticip� :\b0 
remboursement anticip� de 10 % par ann�e. Si le pr�t est en r�gle, � chaque 
anniversaire � la date de redressement des int�r�ts, le client peut rembourser sans avis ni 
p�nalit� un montant maximal de 10 % du capital initial du pr�t initial ou du pr�t modifi�, 
renouvel� ou suppl�mentaire.\cell\row 

\pard\intbl (b)\cell\b 
Int�r�t � bail :\b0  si le ou les d�biteur(s) hypoth�caire(s) d�tient (d�tiennent) ou d�tiendra (
d�tiendront) un int�r�t � bail sur la propri�t�, le libell� suivant doit �tre inclus dans une 
annexe relative au formulaire Charte/Pr�t hypoth�caire :\par\par 

� Par l'interm�diaire d'un bail dat� du ______ jour de __________________ entre
_________________________________ ________________, en tant que bailleur et le ou les d�biteurs 
hypoth�caires, en tant que ________________________________________________ dont l'avis a �t� 
enregistr� le ______________________________________________________________________au </xsl:text>
		<!-- xsl:value-of select="/*/SolicitorsPackage/LandOfficeClause"/-->
		<xsl:text>registre foncier</xsl:text>
		<xsl:text> de
_____________________________________ en tant que no d'instrument ______________________________, 
le bailleur a lou� � bail aux d�biteurs hypoth�caires, ses h�ritiers, ex�cuteurs, 
administrateurs, successeurs et mandataires, la propri�t� d�crite � la case 5 du formulaire 
Charge/Pr�t hypoth�caire pour une dur�e de ________ (    ) ans � compter du  ______ jour de 
____________________, sous r�serve de loyers, clauses et conditions contenus dans le bail. Le ou 
les d�biteurs hypoth�caires, par la pr�sente, imputent la totalit� de leurs int�r�ts � bail sur 
la propri�t� � </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text>, ainsi que la totalit� de leurs droits aux termes du bail, y compris leur option, le 
cas �ch�ant, d'acheter ladite propri�t� sous r�serve de toute provision de rachat �.\cell\row

\pard \par

\ul\b Recherches\~:\ulnone\b0  vous devez mener toutes les recherches pertinentes et habituelles 
en ce qui a trait � la propri�t� afin de prot�ger nos int�r�ts. Avant la lib�ration de tout 
produit hypoth�caire, l'avocat ou le notaire effectuera les recherches n�cessaires en ce qui a 
trait � tout privil�ge, gr�vement, souscription; qui peuvent �tre enregistr�s contre la 
propri�t�. Il vous incombe d'assurer qu'il n'existe aucun ordre de travail ou avis de d�faut en 
cours contre la propri�t�, que toutes les taxes fonci�res et droits qui sont ou deviendront 
exigibles d'ici la date de redressement des int�r�ts sont pay�s � la municipalit�. Si votre 
recherche r�v�le que le titre a �t� c�d� depuis la date de l'offre d'achat et avant la date de 
cl�ture, vous devez nous en aviser imm�diatement et interrompre tout travail relatif � cette 
op�ration, � moins d'avis contraire.\par\par

\ul\b Exigences relatives � l'arpentage :\ulnone\b0  vous devez obtenir et �valuer un certificat 
d'arpentage ou d'arpenteur / rapport sur le bien immobilier (Alberta, Terre Neuve et 
Saskatchewan) / certificat de localisation (Qu�bec) rempli par un arpenteur reconnu et dat� dans 
les vingt (20) derni�res ann�es. � partir du certificat ou du rapport, v�rifiez que l'emplacement 
des immeubles sur le terrain est conforme � toutes les exigences municipales, provinciale et 
d'autres gouvernements. Assurez vous, lorsqu'un ajout a �t� effectu� depuis la date de 
l'arpentage, qu'un arpentage � jour est requis � moins qu'il n'y ait aucun doute que l'ajout est 
� pr�sent dans les limites du lot et respecte toutes les exigences relatives aux limites de 
construction. Si vous �tes dans l'impossibilit� de vous conformer � nos exigences relatives � 
l'arpentage, veuillez communiquer avec notre bureau au plus t�t.\par\par 

\ul\b Assurance incendie :\ulnone\b0  vous devez vous assurer avant l'avance des fonds, que la 
garantie d'assurance incendie pour le ou les immeubles sur la propri�t� est en vigueur pour une 
valeur qui n'est pas inf�rieure � leur valeur de remplacement complet, le montant du premier 
sinistre �tant payable � </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> � titre de cr�ancier hypoth�caire � l'adresse indiqu�e dans le formulaire de Charge/
Pr�t hypoth�caire et incluant une clause hypoth�caire standard d�ment remplie approuv�e par le 
Bureau des assurances du Canada. Les polices ne doivent pas contenir de clause de co-assurance ou 
de clause similaire qui peut limiter le montant � verser. � moins d'indication contraire, nous 
n'aurons pas besoin d'un exemplaire de cette police.\par\par

\ul\b Avances :\ulnone\b0  lorsque toutes les conditions pr�c�dant cette op�ration sont remplies, 
des fonds au montant de </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LoanAmount"/>
		<xsl:text> seront avanc�s, les co�ts suivants �tant d�duits de l'avance par le pr�teur :\par\par 

\trowd\trgaph108\trleft1980\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx6300\cellx10620
\pard\intbl\qj Primes d'assurance hypoth�caire\par
(incluses dans le montant total de l'emprunt)\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Premium"/>
		<xsl:text>\cell \row 

\pard\intbl 
Taxe de vente provinciale sur la prime\cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PST"/>
		<xsl:text>\cell\row

</xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/Fees/Fee">
			<xsl:text>\pard\intbl 
</xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell \row

</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \intbl 
TOTAL\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/TotalDeductionsAmount"/>
		<xsl:text>\cell\row 

\pard \intbl 
Redressement des int�r�ts (taux quotidien)\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PerDiemInterestAmount"/>
		<xsl:text>\cell \row 

\pard\qj\par
Vous pourrez aller chercher les produits nets de l'hypoth�que de </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/NetAdvanceAmount"/>
		<xsl:text>, payables en votre nom, en fiducie, quatre (4) jours avant la date de cl�ture pr�vue 
� l'adresse indiqu�e sur le formulaire Charge/Pr�t hypoth�caire.\par \par
 
</xsl:text>

		<!--  Internal refi would go here ...-->
		
		<xsl:text>\ul\b Avances sur la construction/progressives :\ulnone\b0  si la garantie pour ce 
pr�t hypoth�caire est constitu�e d'une nouvelle propri�t� et que nous d�caissons les fonds par 
avances progressives, vous devrez nous fournir une demande �crite pour chaque avance ainsi qu'un 
ordre ex�cut� par le d�biteur hypoth�caire nous demandant de vous verser des fonds, en fiducie, 
au moins cinq (5) jours avant l'avance des fonds. Vous �tes charg� du d�caissement de toutes les 
avances sur le pr�t qui vous ont �t� vers�es en fiducie et de vous assurer de la priorit� de 
chaque avance en effectuant toutes les recherches n�cessaires et en vous assurant que les 
exigences de la loi provinciale sur le privil�ge des constructeurs et des fournisseurs de 
mat�riaux ont �t� respect�es avant que chaque avance vous soit vers�e. Vous devez conserver les 
montants appropri�s sur les produits hypoth�caires afin de vous conformer aux exigences 
provinciales relatives aux retenues pour privil�ges. Sauf avis contraire, </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> aura besoin d'un exemplaire du certificat d'ach�vement et de possession et, le cas 
�ch�ant, du certificat d'occupation.\par \par 

\ul\b Information :\ulnone\b0  Le ou les d�biteurs hypoth�caires et le </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text> (le cas �ch�ant) doivent signer une d�claration. Un exemplaire doit �tre remis � 
l'emprunteur ou aux emprunteurs et le deuxi�me exemplaire doit �tre achemin� � </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> avec le rapport final sur les titres �mis par le notaire ou l'avocat. Si aucune 
d�claration n'est sign�e, vous devez obtenir une renonciation aupr�s du d�biteur hypoth�caire ou 
des d�biteurs hypoth�caires et du </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text> (s'il y a lieu).\par\par

\ul\b Loi sur l'am�nagement du territoire :\ulnone\b0  vous devez vous assurer que le pr�t 
hypoth�caire ne contrevient pas aux dispositions de la Loi sur l'am�nagement du territoire, 
modifi�e de temps � autre, et que le ou les d�biteurs hypoth�caires ne d�tiennent pas un droit en 
fief ou en �quit� de rachat, ou ne conservent pas un pouvoir ou un droit d'accorder, de c�der ou 
d'exercer un pouvoir de d�signation en ce qui a trait � tout terrain contigu au terrain garanti 
par le pr�t hypoth�caire.\par \par

\ul\b Renouvellement pour le ou les garant(s) :\ulnone\b0  le cas �ch�ant, le libell� suivant 
doit �tre inclus dans une annexe au formulaire de Charge/Pr�t hypoth�caire : � En plus des 
promesses et des ententes du ou des garant(s) contenus dans ce pr�t hypoth�caire, le ou les 
garant(s) convient ou conviennent �galement au seul gr� de </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text>, que le pr�t hypoth�caire puisse �tre renouvel� � l'�ch�ance pour n'importe quel 
terme, avec ou sans modification des taux d'int�r�ts payables aux termes du pr�t hypoth�caire, en 
concluant une ou plusieurs ententes �crites avec le d�biteur ou les d�biteurs hypoth�caires. Tout 
renouvellement dudit pr�t hypoth�caire lie le ou les garant(s), que l'entente de renouvellement 
ait �t� sign�e ou non par le ou les d�biteur(s) hypoth�caire(s) �.\par\par

\ul\b Soci�t� � titre d'emprunteur :\ulnone\b0  si un d�biteur ou des d�biteurs hypoth�caires ou 
</xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>(s) est une soci�t�, vous devez obtenir une copie certifi�e de la r�solution du 
conseil d'administration et du r�glement d'emprunt de la soci�t� autorisant la charge ou le pr�t 
hypoth�caire et vous devez vous assurer que la soci�t� est d�ment constitu�e aux termes des lois 
provinciales ou f�d�rales avec tous les pouvoirs n�cessaires d'imputer sa participation dans la 
propri�t� et inclut ces documents avec le rapport final sur les titres du notaire ou de 
l'avocat.\par\par

\ul\b Rapport du notaire ou de l'avocat :\ulnone\b0  vous devez soumettre le rapport final sur 
les titres �mis par le notaire et l'avocat, sur le formulaire ci-joint et les documents indiqu�s, 
dans les 30 jours suivant le versement de la derni�re avance. Si votre rapport ne peut �tre 
soumis dans ce d�lai, vous devez nous fournir une lettre explicative. Le d�faut de vous conformer 
� l'une des instructions ci-dessus peut entra�ner l'interruption des relations d'affaires futures 
avec vous ou votre �tude.\par\pard\page 


</xsl:text>
	</xsl:template>

	<xsl:template name="RequestForFundsFr">
		<xsl:text>\sect

\f1\fs18 </xsl:text>
		<xsl:call-template name="LogoAddress"/>
		<xsl:text>\par 
\f1\fs18 
\trowd\trgaph108\trleft1440\trrh719
\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalc
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx9180
\pard\intbl\keepn
\qc\ul\b\fs28 DEMANDE DE D�CAISSEMENT DE FONDS PAR LE NOTAIRE OU L'AVOCAT\cell\row

\pard\fi720\li5040\ulnone\b0\fs18\par\par\par

\trowd\trgaph57\trleft-115\trpaddl57\trpaddr57\trpaddfl3\trpaddfr3
\cellx1312\cellx4598\cellx5976\cellx10800
\pard\intbl
\sa120\b T�L�COPIER � :\cell\b0 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>\cell 
\b EXP�DITEUR :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Name"/>
		<xsl:value-of select="/*/SolicitorsPackage/SolicitorAddress1"/>
		<xsl:text>\cell\row 

\pard\intbl
\b ATTENTION:\cell \b0
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/FunderName"/>
		<xsl:text>\cell \cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="/*/SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/AddressPostal"/>
		<xsl:text>\cell \row 

\pard\intbl
\b DATE :\cell \b0
________________\cell \cell 
T�l�phone :</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Phone"/>
		<xsl:text>\par 
T�l�copieur :</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Fax"/>
		<xsl:text>\cell \row 
\pard 
</xsl:text>
		<xsl:call-template name="HorizLine"/>
		<xsl:text>\par 
\pard\keepn\tx1260
\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx4320\cellx10980
\pard\intbl
\b NUM�RO DE R�F�RENCE DU PR�T HYPOTH�CAIRE :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/ReferenceNum"/>
		<xsl:text>\cell \row 

\pard\intbl
\b D�BITEUR(S) HYPOTH�CAIRE(S) :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BorrowerNamesLine"/>
		<xsl:text>\cell \row 

\pard\intbl
\b ADRESSE CIVILE :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:if test="/*/SolicitorsPackage/PropertyAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="/*/SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text>\cell \row 

\pard\intbl
\b MONTANT DU PR�T HYPOTH�CAIRE :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell \row 

\pard\intbl
\b DATE DE CL�TURE :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell \row 

\pard 
</xsl:text>
		<xsl:call-template name="HorizLine"/>
		<xsl:text>\par 

\b Je demande/Nous demandons des fonds d'un montant de  montant </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/NetAdvanceAmount"/>
		<xsl:text> pour la date de cl�ture </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/AdvanceDate"/>
		<xsl:text>.\par\par 

Je reconnais/Nous reconnaissons qu'il n'existe aucune modification ou annexe � l'entente d'achat 
et de vente qui touche le prix d'achat ou le montant du d�p�t indiqu� dans votre lettre 
d'engagement hypoth�caire.\par\par 

Je confirme/Nous confirmons que :\b0\par\par 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx360\cellx900\cellx10908
\pard\intbl
[\cell ]\cell 
toutes les modalit�s et les conditions �nonc�es dans vos instructions ont �t� remplies\cell\row 

\pard\intbl
[\cell ]\cell 
toutes les modalit�s et les conditions de la SCHL ou de Genworth ont �t� remplies\cell\row 

\pard\par

\b LA LIVRAISON DES FONDS EST REQUISE COMME SUIT\b0\par

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx360\cellx900\cellx10908
\pard\intbl
[\cell ]\cell 
Je prends les dispositions (Nous prenons les dispositions) pour prendre livraison des fonds � 
votre bureau cinq (5) jours avant la date de cl�ture pr�vue.\cell\row 

\pard\intbl
[\cell ]\cell 
D�poser les fonds du d�p�t � mon ou � notre compte en fiducie de </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> n� ______________________ � la succursale n� __________.\cell\row 

\pard\intbl
[\cell ]\cell 
Livrer les fonds par messagerie � nos frais  _________________________ compte de messagerie n� 
_________________.\cell\row 

\pard\intbl
[\cell ]\cell 
* Virer les fonds par voie �lectronique � : 
______________________________________________________________________________
____________________________________________________________________________________________
____________________________________________________________________________________________
\cell\row 

\pard\par

\b * Si cette derni�re option est choisie, je comprends/nous comprenons que vous retiendrez les 
fonds suffisants pour couvrir tous les frais de virement �lectronique.\b0\par\par 

\pard\tx5000\tx7000\tab ___________________________________________________\par

\tab\tab Signature du notaire / de l'avocat\par 
</xsl:text>
	</xsl:template>

	<xsl:template name="FinalReportFr">
		<xsl:text>\sect 


\trowd\trgaph108\trleft360
\trbrdrl\brdrs\brdrw15\brdrcf1 \trbrdrt\brdrs\brdrw15\brdrcf1 
\trbrdrr\brdrs\brdrw15\brdrcf1 \trbrdrb\brdrs\brdrw15\brdrcf1 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw15\brdrs\brdrcf1\clbrdrt\brdrw15\brdrs\brdrcf1
\clbrdrr\brdrw15\brdrs\brdrcf1\clbrdrb\brdrw15\brdrs\brdrcf1 \cellx10440
\pard\intbl\qc\par
\ul\b\fs28 RAPPORT FINAL SUR LES TITRES D�LIVR�S PAR LE NOTAIRE OU L'AVOCAT\b0\ulnone\par
\cell\row

\pard \par\fs18 

\par

\pard\tx100\tx7000\tab\b No de r�f�rence du notaire ou de l'avocat  
____________________________\tab _________________________\par
\tab\tab Date\b0\par
\pard  \par

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1800\cellx11898
\pard\intbl 
DESTINATAIRE :\cell </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\par \par 
� l'attention de </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/AdministratorName"/>
		<xsl:text>\cell \row 

\pard \par 

Conform�ment � vos instructions, nous avons agi � titre de vos notaires ou avocats dans les 
op�rations suivantes. Nous avons enregistr� un pr�t hypoth�caire/charge (ou acte de pr�t, le cas 
�ch�ant), le � pr�t hypoth�caire � sur le formulaire appropri� au bureau d'enregistrement 
appropri� et r�dig� notre rapport suivant comme suit :\par \par 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx3150\cellx10908
\pard\intbl
D�biteur(s) hypoth�caire(s)\cell 
______________________________________________________________________ \cell\row 

\pard\intbl 
</xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>\cell 
______________________________________________________________________ \cell\row 

\pard\intbl 
Adresse civile de la propri�t�\cell 
______________________________________________________________________ \cell \row 

\pard\intbl 
Adresse l�gale de la propri�t�\cell 
______________________________________________________________________ \cell \row 

\pard \par \par 

\b � notre avis :\b0 \par 
\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx720\cellx10908
\pard\intbl 
(1)\cell Un pr�t hypoth�caire de premier rang valide et ayant force obligatoire en faveur de </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> pour le montant complet des sommes avanc�es a �t� enregistr� sur 
_____________________________________________ au bureau d'enregistrement de 
_____________________________________________________ comme no d'instrument 
___________________________.\cell \row 

\pard\intbl 
(2)\cell 
Le ou les d�biteur(s) hypoth�caire(s) ont un titre marchand et en bonne et due forme en fief 
simple de la propri�t� libre et exempte de tout gr�vement pr�c�dent, autre que des d�fectuosit�s 
mineures �num�r�es ci-dessous qui \b ne touchent pas\b0  le rang du pr�t hypoth�caire et le 
caract�re n�gociable de la propri�t�. Toutes exigences relatives � la retenue pour privil�ges et 
� la p�riode de retenue ont �t� satisfaites. Les servitudes, empi�tements et restrictions, etc. 
sont �num�r�s ci-dessous :\par
________________________________________________________________________________________________ \par 
________________________________________________________________________________________________ \par 
________________________________________________________________________________________________ \par 
________________________________________________________________________________________________ \par 
________________________________________________________________________________________________ \par 
________________________________________________________________________________________________ \par 
\cell \row 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx630\cellx10908
\pard\intbl
\cell \b Unit�(s) en copropri�t�, s'il y a lieu :\b0\cell \row 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx630\cellx1739\cellx10908
\pard\intbl
\cell a)\cell La soci�t� en copropri�t� a �t� avis�e du pr�t hypoth�caire et de vos droits de 
vote et est inform�e de vous aviser si le d�biteur ou les d�biteurs hypoth�caire(s) n'acquittent 
pas les d�penses communes.\cell\row 

\pard\intbl
\cell b)\cell Nous avons obtenu un certificat de pr�clusion et une attestation d'assurance 
conform�ment � vos instructions et avons d�termin� que ces deux documents sont satisfaisants. 
Nous confirmons par ailleurs que tous arr�rages indiqu�s dans les certificats de pr�clusion ont 
�t� acquitt�s.\cell\row 

\pard\intbl
\cell c)\cell Toutes les �tapes n�cessaires ont �t� prises pour confirmer votre droit de voter si 
vous d�sirez le faire.\cell\row 

\pard\intbl
\cell d)\cell Disposition relative au pr�avis applicable, le cas �ch�ant : 
__________________________________________________\cell\row 

\pard \par 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx630\cellx10908
\pard\intbl 
(3)\cell Toutes les restrictions ont �t� respect�es en entier et il n'existe aucun ordre de 
travail ni avis de d�faillance en cours contre la propri�t�.\cell \row 

\pard\intbl 
(4)\cell Tous les taxes et les droits exigibles et payables sur la propri�t� � la municipalit� 
ont �t� vers�s jusqu'au _______________________________________.\cell \row 

\pard\intbl 
(5)\cell Le pr�t hypoth�caire ne contrevient pas aux dispositions de la \i Loi de l'am�nagement 
des territoires \i0 modifi�e de temps � autre, et le ou les d�biteurs hypoth�caires ne d�tiennent 
pas un droit en fief ou en �quit� de rachat, ou ne conservent pas un pouvoir ou un droit 
d'accorder, de c�der ou d'exercer un pouvoir de d�signation en ce qui a trait � tout terrain 
contigu au terrain garanti par le pr�t hypoth�caire.\cell \row 

\pard\intbl 
(6)\cell Une copie certifi�e conforme du pr�t hypoth�caire (y compris les modalit�s standard 
relatives � la charge ou au pr�t hypoth�caire, toutes les annexes y aff�rentes, et la 
Reconnaissance de d�bit hypoth�caire et directives, le cas �ch�ant) ont �t� donn�es � chaque 
d�biteur hypoth�caire.\cell \row 

\pard\intbl 
(7)\cell La garantie d'assurance incendie a �t� souscrite conform�ment � vos instructions pour la 
valeur totale de remplacement en raison d'un sinistre payable � </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> conform�ment � la clause hypoth�caire normalis�e du BAC.\cell \row 

\pard \par \par \page 


\par \par 
\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx630\cellx6120\cellx11259
\pard\intbl
\cell 
Compagnie d'assurance : ______________________________\par \cell 
Courtier : _______________________________________\par \cell \row 

\pard\intbl
\cell 
Montant de l'assurance : _____________________________\par \cell 
No de police : _____________________________________\par \cell \row 

\pard\intbl
\cell 
Date d'entr�e en vigueur : _______________________________\par \cell 
Date d'expiration : ___________________________\par \cell \row 

\pard\par
\par
\par
\b Les documents suivants ont �t� joints pour vos dossiers :\b0\par \par 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx270\cellx720\cellx10908
\pard\intbl 
[\cell ]\cell
Double de l'exemplaire enregistr� du formulaire de Charge/Pr�t hypoth�caire ou de l'acte de pr�t 
y compris les modalit�s standard relatives � la charge ou au pr�t hypoth�caire, toutes les 
annexes y aff�rant et l'accus� de r�ception du ou des d�biteur(s) hypoth�caire(s) et du ou des </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>(s) s'il y a lieu\cell\row

\pard\intbl 
[\cell ]\cell
Reconnaissance �lectronique de d�bit hypoth�caire et directives \b (comt�s d'enregistrement 
�lectronique de l'Ontario seulement)\b0\cell\row

\pard\intbl 
[\cell ]\cell
Entente de garantie \b (comt�s d'enregistrement �lectronique de l'Ontario seulement)\b0\cell\row

\pard\intbl 
[\cell ]\cell
Certificat d'arpentage\cell\row

\pard\intbl 
[\cell ]\cell
Police d'assurance des titres et Annexes A et B � la police\cell\row

\pard\intbl 
[\cell ]\cell
Entente de modification enregistr�e\cell\row

\pard\intbl 
[\cell ]\cell
Certificat d'ex�cution\cell\row

\pard\intbl 
[\cell ]\cell
Certificat de pr�clusion et attestation d'assurance\cell\row

\pard\intbl 
[\cell ]\cell
Certificat des taxes fonci�res\cell\row

\pard\intbl 
[\cell ]\cell
Certificat d'ach�vement\cell\row

\pard\intbl 
[\cell ]\cell
Inspection finale\cell\row

\pard\intbl 
[\cell ]\cell
Certificat de garantie de nouvelle demeure\cell\row

\pard\intbl 
[\cell ]\cell
Certificat d'occupation/permis\cell\row

\pard\intbl 
[\cell ]\cell
Certificat d'avis juridique ind�pendant\cell\row

\pard\intbl 
[\cell ]\cell
D�claration\cell\row

\pard\intbl 
[\cell ]\cell
Copie certifi�e de la r�solution des administrateurs et du r�glement d'emprunt de la soci�t� 
autorisant la charge / le pr�t hypoth�caire\cell\row

\pard\intbl 
[\cell ]\cell
Copie du bail (propri�t�s locatives seulement)\cell\row

\pard\intbl 
[\cell ]\cell
Autre (pr�cisez)\cell\row

\pard\intbl 
[\cell ]\cell
_______________________________________________________________________________________________\cell\row

\pard\intbl 
[\cell ]\cell
_______________________________________________________________________________________________\cell\row

\pard\intbl 
[\cell ]\cell
_______________________________________________________________________________________________\cell\row

\pard\intbl 
[\cell ]\cell
_______________________________________________________________________________________________\cell\row

\pard\par

\par
\pard\tx5000\tx7000
\tab ___________________________________________________ \par
\tab\tab Signature du notaire / de l\rquote avocat\par

</xsl:text>
	</xsl:template>


	<!--#DG238 table with logo and branch address -->
	<xsl:template name="LogoAddress">
		<xsl:text><!--#DG286 \trowd \trgaph108\trrh1079\trleft-108\trftsWidth2\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth2\clwWidth30 \cellx30-->\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{\f1 </xsl:text>
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell }
\pard \ql \clvertalc\widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line1"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>
}
{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{
\trowd \trrh1266\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\cltxlrtb\clftsWidth3\clwWidth3377 \cellx5269
\clvertalc\cltxlrtb\clftsWidth3\clwWidth7522 \cellx15646
\row 
}
\f1\fs18 \pard 
</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- Utility templates 														                -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="HorizLine">
		<xsl:text>{\pard<!--#DG530 \sa180-->\brdrb\brdrs\brdrw30\brsp10\par}</xsl:text>
	</xsl:template>

  <!--#DG530 -->
	<xsl:template name="ThinLine">
		<xsl:text>{\pard\brdrb \brdrs \brdrw15 \brsp10 \par}</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>{\f1\par }}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff63\deflang1033\deflangfe1033
{\fonttbl
{\f0\froman\fprq2\fcharset0 Times New Roman;}
{\f1\fswiss\fprq2\fcharset0 Arial;}
{\f2\fswiss\fcharset0 Arial;}
{\f3\fnil\fprq2\fcharset2 Wingdings;}
}
{\colortbl;
\red0\green0\blue0;
\red0\green0\blue255;
\red0\green255\blue255;
\red0\green255\blue0;
\red255\green0\blue255;
\red255\green0\blue0;
\red255\green255\blue0;
\red255\green255\blue255;
\red0\green0\blue128;
\red0\green128\blue128;
\red0\green128\blue0;
\red128\green0\blue128;
\red128\green0\blue0;
\red128\green128\blue0;
\red128\green128\blue128;
\red192\green192\blue192;
\red255\green255\blue255;
}
{\stylesheet
{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \snext0 Normal;}
{\s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 \sbasedon0 \snext0 heading 1;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext15 header;}
{\s16\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext16 footer;}
}
\margl720\margr720\margt720\margb720 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0
<!--#DG286 -->\headery0\footery400\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>
</xsl:stylesheet>
