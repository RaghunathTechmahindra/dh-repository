<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' >  
<xsl:output method="text"/>

<xsl:template match="/">
<xsl:call-template name="RTFFileStart"/>

<xsl:choose>
<xsl:when test='//LanguageEnglish'>
<!-- <xsl:call-template name="EnglishPageNumber"/> -->
<xsl:call-template name="EnglishDocumentTitle"/>
<xsl:call-template name="EnglishHeaderTable"/>
<xsl:call-template name="EnglishSubheader"/>
<xsl:call-template name="EnglishConditionList"/>
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test='//LanguageFrench'>
<xsl:call-template name="FrenchDocumentTitle"/>
<xsl:call-template name="FrenchHeaderTable"/>
<xsl:call-template name="FrenchSubheader"/>
<xsl:call-template name="FrenchConditionList"/>
</xsl:when>
</xsl:choose>


<xsl:call-template name="RTFFileEnd"/> 
</xsl:template>  

<!-- ************************************************************************ -->
<!-- English template section                                                 -->
<!-- ************************************************************************ -->
<xsl:template name="EnglishConditionList"> 

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx9000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl 

{\f1\fs22\b \cell Documents reviewed by:\par </xsl:text><xsl:value-of select="//CurrentUser/Name"/><xsl:text>\par Phone: </xsl:text><xsl:value-of select="//CurrentUser/Phone"/><xsl:text>\par Fax: </xsl:text><xsl:value-of select="//CurrentUser/Fax"/>
<xsl:text>\par Email: </xsl:text><xsl:value-of select="//CurrentUser/EMailAddress"></xsl:value-of>
<xsl:text>\par\par **ALL CONDITIONS MUST BE MET </xsl:text><xsl:value-of select="//ConditionsMetDays"/><xsl:text> BUSINESS DAYS PRIOR TO CLOSING OR COMMITMENT MAY BE CANCELLED**\par\cell \cell }\pard \intbl\row </xsl:text>

<xsl:for-each select="//Conditions/Condition">
<xsl:if test="not(  contains(./Description, '(Solicitor)') or contains(./Description, '(Notaire)')  )"> <!-- Xue Bin Zhao, fixed FXP14441 i.e. #5581 -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx9000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 __}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text>
<xsl:for-each select="./Description/Line">
<xsl:value-of select="."/><xsl:text> \par </xsl:text>
</xsl:for-each>
<xsl:text>}\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="./Status"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>
</xsl:if> <!-- Xue Bin Zhao, fixed FXP14441 i.e. #5581 -->
</xsl:for-each>

<!-- Catherine for pvcs #817 -->
<xsl:text>\pard \par \par {\f1\fs22\b </xsl:text><xsl:value-of select="//ConditionNote"/><xsl:text>}</xsl:text>
<!-- Catherine for pvcs #817 -->

<xsl:text>\pard \par \par {\f1\fs22\b\ul </xsl:text><xsl:value-of select="//BrokerConditionFooter"/><xsl:text>}</xsl:text>
</xsl:template> 



<xsl:template name="EnglishSubheader"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrnone\cellx7000

\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Conditions of Approval Outstanding For: }{\f1\fs22 </xsl:text><xsl:value-of select="//ClientName"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Closing Date: }{\f1\fs22 </xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 



<xsl:template name="EnglishHeaderTable"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 To:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFirmName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 From:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//LenderName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Attn:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Loan #:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Phone:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Lender Reference #:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//DealNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Fax:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Date:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Insurance Reference #:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIPolicyNumber"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Insurance Status:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIStatus"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>




<xsl:text>\pard \par</xsl:text> 

</xsl:template> 


<xsl:template name="EnglishPageNumber"> 
<xsl:text>\pard {\f1\fs22 Page: }{\field {\*\fldinst {\cs16 PAGE }}}{\f1\fs22 \~\~ of\~\~ }{\field {\*\fldinst {\cs16 NUMPAGES }}}\pard\par</xsl:text>
</xsl:template> 


<xsl:template name="EnglishDocumentTitle"> 
<!-- row 1 -->
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrnone
\clbrdrl\brdrs\brdrnone
\clbrdrb\brdrs\brdrnone
\clbrdrr\brdrs\brdrnone\cellx10908
\pard \ql\intbl </xsl:text>
<xsl:text>{\f1\fs24\b </xsl:text><xsl:call-template name="LogoImage_e"/><xsl:text>}\cell \pard \intbl\row </xsl:text>
<xsl:text>\pard \par</xsl:text> 

<!-- row 2 -->
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \qr\intbl </xsl:text>
<xsl:text>{\f1\fs24\b Conditions Outstanding}\cell \pard \intbl\row </xsl:text>
<xsl:text>\pard \par</xsl:text> 

</xsl:template> 

<xsl:template name="EnglishHeader">
<xsl:text>
{\header \pard\plain \qr \widctlpar\faauto\itap0 {\f1\fs22 Page: }{\field{\*\fldinst {PAGE }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{\f1\fs22 \~\~ of\~\~ }{\field{\*\fldinst {NUMPAGES }}{\fldrslt {\lang1024\langfe1024\noproof 4}}}{\par }\pard \ql \widctlpar\faauto\itap0 {\f1\fs22 \par }}
</xsl:text>
</xsl:template>

<xsl:template name="EnglishFooter">
<xsl:text>
{\footer 
{\qc\f1\fs22 ______________________________________________________________________________________\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//LenderName"/><xsl:text>\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//BranchAddressComplete"/><xsl:text>\par}
{\qc\f1\fs22 Toll-free Phone: </xsl:text><xsl:value-of select="//BranchTollPhone"/><xsl:text>   Toll-free Fax: </xsl:text><xsl:value-of select="//BranchTollFax"/><xsl:text>\par}
</xsl:text>
</xsl:template>

<!-- ************************************************************************ -->
<!-- French template section                                                  -->
<!-- ************************************************************************ -->
<xsl:template name="FrenchConditionList"> 

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx9000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl 

{\f1\fs22\b \cell Documents examinés par:\par </xsl:text><xsl:value-of select="//CurrentUser/Name"/><xsl:text>\par Téléphone: </xsl:text><xsl:value-of select="//CurrentUser/Phone"/><xsl:text>\par Télécopieur: </xsl:text><xsl:value-of select="//CurrentUser/Fax"/>
<xsl:text>\par Email: </xsl:text><xsl:value-of select="//CurrentUser/EMailAddress"></xsl:value-of>
<xsl:text>\par \par **TOUTES LES CONDITIONS DOIVENT ÊTRE REMPLIES </xsl:text><xsl:value-of select="//ConditionsMetDays"/><xsl:text> JOURS OUVRABLES AVANT LA DATE DE CLÔTURE, SANS QUOI L'OFFRE D'ENGAGEMENT SERA ANNULÉE**\par\cell \cell }
\pard \intbl\row </xsl:text>

<xsl:for-each select="//Conditions/Condition">
<xsl:if test="not(  contains(./Description, '(Solicitor)') or contains(./Description, '(Notaire)')  )"> <!-- Xue Bin Zhao, fixed FXP14441 i.e. #5581 -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx9000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 __}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text>
<xsl:for-each select="./Description/Line">
<xsl:value-of select="."/><xsl:text> \par </xsl:text>
</xsl:for-each>
<xsl:text>}\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="./Status"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>
</xsl:if> <!-- Xue Bin Zhao, fixed FXP14441 i.e. #5581 -->
</xsl:for-each>

<!-- Catherine for pvcs #817 -->
<xsl:text>\pard \par \par {\f1\fs22\b </xsl:text><xsl:value-of select="//ConditionNote"/><xsl:text>}</xsl:text>
<!-- Catherine for pvcs #817 -->

<xsl:text>\pard \par \par {\f1\fs22\b\ul </xsl:text><xsl:value-of select="//BrokerConditionFooter"/><xsl:text>}</xsl:text>
</xsl:template> 



<xsl:template name="FrenchSubheader"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrnone\cellx7000

\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Conditions d’approbation non remplies pour: }{\f1\fs22</xsl:text><xsl:value-of select="//ClientName"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Date de clôture: }{\f1\fs22 </xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 



<xsl:template name="FrenchHeaderTable"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 À:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFirmName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 De:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//LenderNameShort"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Attn:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 No du prêt:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Téléphone:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 No de référence du prêteur:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//DealNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Télécopieur:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Date:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx3240

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 No de référence de l’assurance:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIPolicyNumber"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Statut de l’assurance:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIStatus"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>




<xsl:text>\pard \par</xsl:text> 

</xsl:template> 


<xsl:template name="FrenchPageNumber"> 
<xsl:text>\pard {\f1\fs22 Page: }{\field {\*\fldinst {\cs16 PAGE }}}{\f1\fs22 \~\~ de\~\~ }{\field {\*\fldinst {\cs16 NUMPAGES }}}\pard\par</xsl:text>
</xsl:template> 


<xsl:template name="FrenchDocumentTitle"> 
<!-- row 1 -->
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrnone
\clbrdrl\brdrs\brdrnone
\clbrdrb\brdrs\brdrnone
\clbrdrr\brdrs\brdrnone\cellx10908
\pard \ql\intbl </xsl:text>
<xsl:text>{\f1\fs24\b </xsl:text><xsl:call-template name="LogoImage_f"/><xsl:text>}\cell \pard \intbl\row </xsl:text>
<xsl:text>\pard \par</xsl:text> 

<!-- row 2 -->
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \qr\intbl </xsl:text>
<xsl:text>{\f1\fs24\b Conditions non remplies}\cell \pard \intbl\row </xsl:text>
<xsl:text>\pard \par</xsl:text> 

</xsl:template> 

<xsl:template name="FrenchHeader">
<xsl:text>
{\header \pard\plain \qr \widctlpar\faauto\itap0 {\f1\fs22 Page: }{\field{\*\fldinst {PAGE }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{\f1\fs22 \~\~ de\~\~ }{\field{\*\fldinst {NUMPAGES }}{\fldrslt {\lang1024\langfe1024\noproof 4}}}{\par }\pard \ql \widctlpar\faauto\itap0 {\f1\fs22 \par }}
</xsl:text>
</xsl:template>

<xsl:template name="FrenchFooter">
<xsl:text>
{\footer 
{\qc\f1\fs22 ______________________________________________________________________________________\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//LenderName"/><xsl:text>\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//BranchAddressComplete"/><xsl:text>\par}
{\qc\f1\fs22 Téléphone: </xsl:text><xsl:value-of select="//BranchPhone"/><xsl:text>   Télécopieur: </xsl:text><xsl:value-of select="//BranchFax"/><xsl:text>\par}
{\qc\f1\fs22 Téléphone sans frais: </xsl:text><xsl:value-of select="//BranchTollPhone"/><xsl:text>   Télécopieur sans frais: </xsl:text><xsl:value-of select="//BranchTollFax"/><xsl:text>\par}
</xsl:text>
</xsl:template>

<!-- ************************************************************************ -->
<!-- rtf file start and rtf file end                                          -->
<!-- ************************************************************************ -->
<xsl:template name="RTFFileEnd">  
<xsl:text>}}</xsl:text>
</xsl:template>  

<xsl:template name="RTFFileStart">  
 <!-- #DG670 -->
<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\stylesheet {\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}}
{\info {\author Zivko Radulovic}{\operator BASIS100 automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company Basis100 Inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \margl720\margr720\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
</xsl:text>

<xsl:choose>
	<xsl:when test="//LanguageFrench">
		<xsl:call-template name="FrenchFooter"/>
	</xsl:when>
	
	<xsl:otherwise>
		<xsl:call-template name="EnglishFooter"/>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\pard\plain \s15\ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha
\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033

{\cs16 {\f1\fs22  }}
{\field {\*\fldinst {\cs16  }}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}}}
{\f1\fs16 {\f1\fs22 }}
{\field {\*\fldinst {\cs16 }}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}}}
{\cs16 \par }
\pard \s15\ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }}</xsl:text>

<xsl:choose>
	<xsl:when test="//LanguageFrench">
		<xsl:call-template name="FrenchHeader"/>
	</xsl:when>
	
	<xsl:otherwise>
		<xsl:call-template name="EnglishHeader"/>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>  

<xsl:template name="LogoImage_e">  
<xsl:text>
{\pict\wmetafile8\picwgoal2137\pichgoal1511 
0100090000038623000000006123000000000400000003010800050000000b0200000000050000
000c0271009700030000001e00040000000701040061230000410b2000cc007000960000000000
700096000000000028000000960000007000000001000800000000000000000000000000000000
00000000000000000000000000ffffff00fefefe00edf9fd0028b3ef0020b0ed0020b0ee0021b0
ee0025b1ee001aaeed0016aded00f8f8f800fcfcfc00cececf0089888900d9d9d900f4f4f400f5
f5f500f1f1f100a7a7a8009f9e9f00aaa9aa00bfbebf00737374006b6a6c008383840043424400
63626400bbbabb00f7f7f700dcdcdc004b4a4c0031303200201f2100d5d5d6002625270029282a
0068676900585759002f2e3000807f7f0028272900fdfdfd00212022002e2d2f00363537003b3a
3c008d8c8d005e5d5f0094939400b2b2b3002d2c2e0023222400c1c1c200dbdbdb00e3e3e300c3
c3c300e5e5e500919192006e6e6f008080810035343600b4b3b400e4e4e4002f303100a4a3a400
48474900f9f9f900c7c7c700605f6100dfdfdf00ececec00e0e0e000717071003e3d3f00403f41
00a5a5a600787779002a292b009c9b9c00efefef00c5c5c600cdcdcd003433350033323400dddd
dd009d9d9e00e6e7e60038373900bcbbbc00e8e7e800b5b5b50056565700c9c9c900a7a6a700fa
fafa008b8a8b00666567009a999a002b2a2c0046454700cbcbcb007e7d7f0053525400b1b1b200
1f1e200098979800c0bfc000fefefd00ababac009a9a9b00bdbdbd00eeeeee007b7b7c001b1a1d
0086858700f3f3f300d4d4d400acabac004e4d4f009f9fa000eeedee00ebebeb0076767700eae9
ea0093929300f6f6f600eaeaea00b8b7b800c8c8c800d7d7d700f2f2f20039383a00e7e7e700fb
fbfb00d3d3d300ededed00afaeaf00cfcfcf0096959600e9e9e900dedede00d8d8d800f0f0f000
b0afb000a3a2a30034333400a1a1a200e1e1e100d1d1d200e8e8e8005b5a5c00504f5100b7b6b7
0097969700b9b9ba00cccbcc008f8e8f0088878800d0d0d0008b8b8c00c4c4c400f0eff000fafa
f900fefdfe00cccccc00f1f2f100adadad00f8f7f800f6f5f600dcdbdc00cbedfb00c4eafa002a
b3ef002cb4ef002db5ee002eb4ef0023b1ee002fb5ef002eb5ef0022b1ee0027b2ee0024b1ef00
47bef000b5e4f9002fb4ef002eb4f0005ac4f200afe3f900f4fbfe00d9f1fc009bdbf70052c2f2
00b9e6f90024b2ee00d1effb0033b7ef004dc0f100e1f4fd002eb6ef0020b1ee00aae1f800fcfe
fe00bde8fa003dbaf00020b0ef00f7fcfe006bcaf40072cdf400a2def800b0e3f900f2fafe00a5
e0f80085d4f50066c8f4002ab4ef0061c7f30022b1ef0094d9f700f9fdfe00a8e0f9007bd0f400
8dd7f600ade2f9001dafee0031b6ef0065c9f300e7f7fd002cb6ee0022b0ee00908f9000ecebec
00deddde00edeeee00fcfcfb00fcfbfc00fbfcfb00f4f4f3000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202024776150202020202020202020202020202020202020202020202020202020202
020202020202998e02020202020000020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
0202029f5427020202020202020202020202020202020202020202020202020202020202020202
027153230202020202000002020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
02020202020202020202020202020202020202020202020202020202020202020202020202029f
534002020202020202020202020202020202020202020202020202020202020202020202021a2d
450202020200000202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202029f53400202
02020202020202020202020202020202020202020202020202020202020202020202242ced0202
020000020202ea5feb0202020202025f5f0202020202020202eca583022a86860202020202027f
9b9b1002020202020202529b0202020202020202020202020202020202399b5ba5020202020202
0202485b440202020202020202865f0202025f5f0202025f8602020202029f5340391552020202
0202020202575ba2022a82020202020286862a02022a5f86020202020202e62d45020202000002
0202672c300202020202022b23430202020202027529532c676a2c980202020244292020202c1b
02020202026b203d4e71020202020202020202020202023e29203d3d5333980202020286332054
3d296802020202023663230202023434020202344e0f020202029f542d20535329740202020202
19403d3349232ce902020202982c2f020291334b0202020202020263290202020000020202972d
1b020202020202242c430202020202025c2d4b4f33202d5c0202020202213b4855202039020202
02e62d2d924c0202020202020202020202020e542d2027644a2460020202024e2d4a7519542c94
020202021e542c020202634e020202335436020202029f54202fa54b2d290202020202292d3c81
532d207f020202025c2d3a02025e2d420202020202020263534402020000020202972d1b020202
020202242c43020202020202772d4f027f242d5c02020202022a020274405395020202023a3d19
02020202020202020202020202962c2d4e57020202020b020202525324020202302d1f02020202
1e542c020202634e020202335436020202029f53400202024e2d90020202022954e8025b3d1a02
020202025c2d3a02025e2d42020202020202562d2d1f02020000020202972d1b02020202020224
2c5f020202020202772d6202028b2d5c0202020202029b582c2d2302020202023a2d1902020202
0202020202020202021f2d6302020202020202020202732d42020202515333020202021e542c02
0202634e020202335436020202029f5340020202252d97020202028029334a7b2d1a0202020202
5c2d3a02025e2d42020202020202633d2d2402020000020202972d1b0202020202022454820202
02020202772d6202023a2d5c0202020202673d3d29710202020202023a2d190202020202020202
0202020202292d45020202020202020202023c2d1f0202025d5363020202021e542c0202026324
020202335436020202029f53400202027b2d770202020202029b67332d1a02020202025c2d9a02
025e2d4202020202029f5427452d32020000020202972d1b020202020202242d332a1102020202
772d6202023a2d5c020202025f33406b02020202020202023a2d19020202020202020202020202
02633d8902020202020202020202383d24020202182d64020202021e5453780202632d1b020263
200f020202029f5333020202582d1702020202515f0202803d1a02020202025c2d244302913d42
02020202024a2d8b9f544a020000020202972d1b0202020202024e5358334b02020202772d6202
023a2d5c020202022a632c39026f21020202020d662d17525002020202020202020202024e2d6d
0202020202020202020202292d77e791202022020202021e542d2049a7532d20649c2c20850202
02029f542d64463c53277f020202024b3393023b2d7702020202025c2d20241c773d7702020202
74274e020224636c0000020202972d1b0202020202022323462c23020202021f546a0202e62067
0202020202492020333d335902020220543d54276f02020202020202020202024e3d6702020202
0202020202020270293d403d633a02020202023627334f33533d4b9f29533d3402020202028a40
3d84202d403c0202020202222920243d33a502020202026720622e202d27590202020230541f02
0226546a0000020202972d1b0202020202028d4602aa0f020202023f4850020270483f02020202
02023fa028a74302020202029e2d71481102020202020202020202027b2d40aa02020202020202
0202020202382f132a0202020202024346460243a738020202995b0202020202020b464602903a
5a020202020202020280e656a20202020202023948700282153902020202021e467e02027e4655
0000020202972d1b02020202020202020202020202020202020202020202020202020202020202
02020202020202023a2d190202020202020202020202020202232d33600b0250179f0202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020200003333
33533d533333231002020202020202020202020202020202020202020202020202020202020202
02020202318473020202020202020202020202020288232d2d2c4e402d40020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020000848484848484
848454100202020202020202020202020202020202020202020202020202020202020202020202
02020202020202020202020202020202020202173329244e774702020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202000002020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020200000202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020000020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202000002020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020200000202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020000020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202000002020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020200
000202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020000020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202000002020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020200000202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020000020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202000002020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020200000202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020000020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
0202020202020000020202020202020202020202020202020202ced1d4d4d4d4d4d4d4d4d4d4d4
d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4
d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4
d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d4d102020202020202020202020202020202
020200000202020202020202020202020202020202020304b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2080202020202020202020202020202020202020000
02020202020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b208020202020202020202020202020202020202000002020202
020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b20802020202020202020202020202020202020200000202020202020202
0202020202020202020203adb2b2b2b2b2b4b4b4b4b4b4b4b4b4b4b4b1b3b2b2b2b2b2b2b2b2b2
b2b2b2c2e5b4b4b4b4b4b4b4b4b4b4b5b2b2b2b2b2b2b2b2b2b2b3aeb1b5ccd8d0ddddddcfbbcc
afb4c2adb2b3b3b3b2b2b2b2b2e5b4b4b4b4b4b4b4b4b4b4c2c7b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2080202020202020202020202020202020202020000020202020202020202020202
02020202020203adb2b2b2b2b5020202020202020202020202c2b2b2b2b2b2b2b2b2b2b2b2d703
0202020202020202020202d1b3b2b2b2b2b2b2b2afb1cfbe0202020202020202020202020202c6
bfb7b4aeb2b2b2b2b40202020202020202020202c3afb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b208020202020202020202020202020202020202000002020202020202020202020202020202
020203adb2b2b2b2b3d50202020202020202020202b5b2b2b2b2b2b2b2b2b2b2b2b40202020202
02020202020202e4b2b2b2b2b2b3b2d7ac020202020202020202020202020202020202020202e2
b2b2b2b2b10202020202020202020202abaeb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b20802
0202020202020202020202020202020202000002020202020202020202020202020202020203ad
b2b2b2b2b2080202020202020202020202d0b2b2b2b2b2b2b2b2b2b2c7bb020202020202020202
0202ceb6b2b2b2b2b2cdd20202020202020202020202020202020202020202020202d6b2b2b2b2
b10202020202020202020202abaeb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2080202020202
02020202020202020202020202000002020202020202020202020202020202020203adb2b2b2b2
b2ad0302020202020202020202beaeafb2b2b2b2b2b2b2b2aeab0202020202020202020202ddb3
b3b2b2b30402020202020202020202020202020202020202020202020202d6b2b2b2b2b1020202
0202020202020202abaeb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b208020202020202020202
020202020202020202000002020202020202020202020202020202020203adb2b2b2b2b2b2cf02
0202020202020202020205b3b2b2b2b2b2b2b2b207020202020202020202020202c2b2b2b2b2e1
0202020202020202020202020202020202020202020202020202d6b2b2b2b2b102020202020202
02020202abaeb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b20802020202020202020202020202
0202020202000002020202020202020202020202020202020203adb2b2b2b2b2b2b40202020202
020202020202ccb3b2b2b2b2b2b2b2b2cc0202020202020202020202e3d7b2b2c7c20202020202
02020202020202020202020202020202020202020202d6b2b2b2b2b10202020202020202020202
abaeb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2080202020202020202020202020202020202
02000002020202020202020202020202020202020203adb2b2b2b2b2b2d7c60202020202020202
0202d2c0c0c0c0c0c0c0c0c0c10202020202020202020202e2b2b2b2c2d3020202020202020202
020202020202020202020202020202020202d6b2b2b2b2b10202020202020202020202abaeb2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b208020202020202020202020202020202020202000002
020202020202020202020202020202020203adb2b2b2b2b2b2b2bb020202020202020202020202
0202020202020202020202020202020202020202c8b3b2b2dd02020202020202020202020202b8
c5e1e1ccc6020202020202020202d6b2b2b2b2b10202020202020202020202abaeb2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b20802020202020202020202020202020202020200000202020202
0202020202020202020202020203adb2b2b2b2b2b2b2c802020202020202020202020202020202
020202020202020202020202020202c3aeb2b306020202020202020202020202dbb4b3afb3b3ae
ab020202020202020202d6b2b2b2b2b10202020202020202020202abaeb2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2080202020202020202020202020202020202020000020202020202020202
02020202020202020203adb2b2b2b2b2b2b2aeab02020202020202020202020202020202020202
0202020202020202020202c5b2b2b2d8020202020202020202020202e0b3b2b2b2b2aeab020202
020202020202d6b2b2b2b2b10202020202020202020202abb3b3b3b3b3b3b3b3b3b3b3b3b2afb2
b2b2b2b2b208020202020202020202020202020202020202000002020202020202020202020202
020202020203adb2b2b2b2b2b2b2b2c50202020202020202020202020202020202020202020202
02020202020202c8b2b2aec60202020202020202020202d0b3b3b2b2b2b2aeab02020202020202
0202d6b2b2b2b2b10202020202020202020202abd908080808080808080808080804b3b2b2b2b2
b20802020202020202020202020202020202020200000202020202020202020202020202020202
0203adb2b2b2b2b2b2b2b205020202020202020202020202020202020202020202020202020202
0202cbaeb2b20502020202020202020202020207b2b2b2b2b2b2aeab020202020202020202d6b2
b2b2b2b102020202020202020202020202020202020202020202020202b8b3b2b2b2b2b2080202
02020202020202020202020202020202000002020202020202020202020202020202020203adb2
b2b2b2b2b2b2b2afcb02020202020202020202020202020202020202020202020202020202ccc7
b2b2b30202020202020202020202d304b2b2b2b2b2b2aeab020202020202020202d6b2b2b2b2b1
02020202020202020202020202020202020202020202020202d2b3b2b2b2b2b208020202020202
020202020202020202020202000002020202020202020202020202020202020203adb2b2b2b2b2
b2b2b2b3cc02020202020202020202020202020202020202020202020202020202b4b3b2b2c502
02020202020202020202cbafb2b2b2b2b2b2aeab020202020202020202d6b2b2b2b2b102020202
020202020202020202020202020202020202020202bcb3b2b2b2b2b20802020202020202020202
0202020202020202000002020202020202020202020202020202020203adb2b2b2b2b2b2b2b2b3
b402020202020202020202c0b1b1b1b1b1c402020202020202020202d1b3b3b2b2d60202020202
020202020202dcc7b2b2b2b2b2b2aeab020202020202020202d6b2b2b2b2b10202020202020202
0202020202020202020202020202020202bcb3b2b2b2b2b2080202020202020202020202020202
02020202000002020202020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b3dc0202
02020202020202c6d7b2b2b2b2c102020202020202020202c7b2b2b2b2dd020202020202020202
0202dcb3b2b2b2b2b2b2b3dedfdfdfdfdfdfdfdfd2c0b2b2b2b2b1020202020202020202020202
02020202020202020202020202bcb3b2b2b2b2b208020202020202020202020202020202020202
000002020202020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b2c4020202020202
0202020205b2b2b3b402020202020202020202db08b2b2b2b2d00202020202020202020202acb3
b2b2b2b2b2b2afb3b2b2b2b2b2b2b2b2b3b2b2b2b2b2b102020202020202020202020202020202
020202020202020202bcb3b2b2b2b2b20802020202020202020202020202020202020200000202
0202020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b2c202020202020202020202
d8b2b3bacc02020202020202020202d5b3b2b2b2b2bb0202020202020202020202dbb5b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b10202020202020202020202020202020202020202
0202020202d2b3b2b2b2b2b2080202020202020202020202020202020202020000020202020202
02020202020202020202020203adb2b2b2b2b2b2b2b2b2b2afda020202020202020202c6aeb2b3
b802020202020202020202c2b3b2b2b2b2c4020202020202020202020202aeb2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b10202020202020202020202c3c0c0c0c0c0c0c0c0c0c0c0c0
c0ccb2b2b2b2b2b208020202020202020202020202020202020202000002020202020202020202
020202020202020203adb2b2b2b2b2b2b2b2b2b2b2d702020202020202020202b4b30502020202
0202020202020304b2b2b2b2b2d9020202020202020202020202abaeb3b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b10202020202020202020202abaeb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b20802020202020202020202020202020202020200000202020202020202020202020202
0202020203adb2b2b2b2b2b2b2b2b2b2b2b5bd020202020202020202d6c7cc0202020202020202
0202cfb2b3b2b2b2b2d703020202020202020202020202d8b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b10202020202020202020202abaeb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
080202020202020202020202020202020202020000020202020202020202020202020202020202
03adb2b2b2b2b2b2b2b2b2b2b2b2d5020202020202020202c604d202020202020202020202b4b2
b2b2b2b2b2b2cf02020202020202020202020202d0b5b3b3b3b3b3b2b3b3b3b3b3b3c8c7b3b2b2
b2b2b10202020202020202020202abaeb2b2b2b2b2b2b2b2b2b2b2b2b3b3b2b2b2b2b208020202
020202020202020202020202020202000002020202020202020202020202020202020203adb2b2
b2b2b2b2b2b2b2b2b2b2c2020202020202020202020a02020202020202020202beaeb2b2b2b2b2
b2af06020202020202020202020202020203c00604b3b2b2b3aeb5b4ccd402aeb9b2b2b2b2b102
02020202020202020202abb504040404040404040404040404b2b2b2b2b2b20802020202020202
0202020202020202020202000002020202020202020202020202020202020203adb2b2b2b2b2b2
b2b2b2b2b2b2ad03020202020202020202d002020202020202020202bbc7b2b2b2b2b2b2b2b2d0
0202020202020202020202020202020203cbd1bfd2c3ce02020202aeb2b2b2b2b2b10202020202
020202020202cad3bdbdbdbdbdbdbdbdbdbdbdbdce06b2b2b2b2b2080202020202020202020202
02020202020202000002020202020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b2
b2b2b2cf020202020202020202020202020202020202020205b3b2b2b2b2b2b2b2b2b5c6020202
0202020202020202020202020202020202020202020202aeb2b2b2b2b2b1020202020202020202
020202020202020202020202020202020206b2b2b2b2b208020202020202020202020202020202
020202000002020202020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b2b2b2b2b4
02020202020202020202020202020202020202acb3b2b2b2b2b2b2b2b2b2b206ca020202020202
02020202020202020202020202020202020202aeb2b2b2b2b2b102020202020202020202020202
0202020202020202020202020206b2b2b2b2b20802020202020202020202020202020202020200
0002020202020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b2b2b2b3aec6020202
020202020202020202020202020202ccb2b2b2b2b2b2b2b2b2b2b2b2cdce020202020202020202
020202020202020202020202020202aeb2b2b2b2b2b10202020202020202020202020202020202
02020202020202020206b2b2b2b2b2080202020202020202020202020202020202020000020202
02020202020202020202020202020203adb2b2b2b2b2b2b2b2b2b2b2b2b2b2bb02020202020202
0202020202020202020202b4b2b2b2b2b2b2b2b2b2b2b2b2b2b4cb020202020202020202020202
0202020202020202020202aeb2b2b2b2b2b1020202020202020202020202020202020202020202
020202020206b2b2b2b2b208020202020202020202020202020202020202000002020202020202
020202020202020202020203adb2b2b2b2b2b2b2b2b2b2b2b2b2b2b40202020202020202020202
020202020202c9b2b2b2b2b2b2b2b2b2b2b2b2b2b2b3afb7ca0202020202020202020202020202
02020202020202aeb2b2b2b2b2b102020202020202020202020202020202020202020202020202
0206b2b2b2b2b20802020202020202020202020202020202020200000202020202020202020202
0202020202020203adb2b2b2b2b2b2b2b2b2b2b2b2b2b2aec30202020202020202020202020202
0202c4b3b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2c2c5c602020202020202020202020202020202
020202aec7b2b2b2b2b60202020202020202020202020202020202020202020202020202c8b2b2
b2b2b2080202020202020202020202020202020202020000020202020202020202020202020202
02020203adb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b7b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b5b9
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2bab2ae05bbbc0302020202020202020202bdbebfc0b5b2
b3b2b2b2b2b5c1b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8b8c1c2b2b2b2b2b208
020202020202020202020202020202020202000002020202020202020202020202020202020203
adb2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2adb4b4b5b5b5b508b106b604aeb2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b20802020202
0202020202020202020202020202000002020202020202020202020202020202020203adb2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b3b3b2b2b2b2b2b3b3b2b2b2b2b2b2b2b2b2b2b2b2b2b2
b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2b2080202020202020202
02020202020202020202000002020202020202020202020202020202020203adb2b3b3b3b3b3b3
b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3
b3b3b3b3b3b3b3b3b3b3b3b3b2b2b3b3b3b3b3b2b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3
b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b3b2b1020202020202020202020202
020202020202000002020202020202020202020202020202020203adaeafb0b0b0b0b0b0b0b0b0
b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0
b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0
b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0aeb102020202020202020202020202020202
020200000202020202020202020202020202020202020305abacacacacacacacacacacacacacac
acacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacac
acacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacacac
acacacacacacacacacacacacacacacacacac090202020202020202020202020202020202020000
020202020202020202020202020202020202030902020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
02020202020202020202020202020a020202020202020202020202020202020202000002020202
020202020202020202020202020203090202020202020202378f02020202020202020202020202
020202020202020202020202020202020202020c9683128e83025b7b9602020202020202020202
02020202020202020202020202020202020202020202020202020202020202020294aa02020202
020270020202020202020902020202020202020202020202020202020200000202020202020202
02020202020202020202030902020202020247910c8f0f78186064520202020202020202020202
020202020202020202020202020259725ba5a58f02020202260202020202020202020202020202
0202021e72203423180297022602020202020202020202020202020247191e9a493043024b0291
880202020202090202020202020202020202020202020202020000020202020202020202020202
0202020202020309020202020202a1020202020202990242020202020202020202020202020202
0202020202020202021a2c6d189002020202460249020202020202020202020202020202020272
0c020202020202024b02020202020202020202020202020264020202020202026b026b37020202
020209020202020202020202020202020202020202000002020202020202020202020202020202
02020309020202020202980202480202021a024202020202020202020202020202020202020202
020202022e4b1f02029c0fa902a194858c020202020202020202020202020202024ba102020202
122a0244530202020202020202020202021d66214e5f193c5f0202020202340202020202020902
020202020202020202020202020202020200000202020202020202020202020202020202020309
02020202020297a802027b248b731a020202020202020202020202020202020202020202020225
24026d512c02023f9d334d7302020202020202020202020202020202176302905b1f9b3fa5614e
73020202020202020202027a1a8b55020202023a3e687d5c634f43020202020202090202020202
020202020202020202020202020000020202020202020202020202020202020202030902020202
020202342e236433020202020202020202020202020202020202020202020202020e0271296f72
a9020202020202020202020202020202020202020202020223a25f3b3302020202020202020202
0202020202024f231702021c269002020202020202020202020202020209020202020202020202
02020202020202020200000202020202020202020202020202020202020309020202020202021d
728c5e344602020202020202020202020202020202020202020202024563820202020202020202
02020202020202020202020202020202020202024e5c1b025e0202020202020202020202020202
68340202722e0e1467020202020202020202020202020202020902020202020202020202020202
020202020200000202020202020202020202020202020202020309020202020202020202020202
290b02020202020202020202020202020202020202022a88027f34020223020202020202020202
02020202020202020202020202020202210f02020f020202020202020202020202021a24240202
0202022a0202020202020202020202020202020202090202020202020202020202020202020202
020000020202020202020202020202020202020202030902020202020202020211450202727f02
02020202020202020202020202020202026f2302020259347b9802020202020202020202020202
0202020202020202020202027b02023b18020202020202020202020268344a021f0c1d47621d02
020202020202020202020202020202020209020202020202020202020202020202020202000002
020202020202020202020202020202020203090202020202020202022a30021864341a7156155b
809f4702020202020202020212334e7795020250197b0202020202020202020202020202020202
0202020202020239377a3429330202020202020202024f631747026f1d0278772d020202020202
020202020202020202020202020902020202020202020202020202020202020200000202020202
0202020202020202020202020203090202020202020202020295481502022e2c0246243b2c632f
2b1b94020202020270340296a033a002024b779602020202020202020202020202020202020202
0202029a8d0202029e8a02020202020202342d3da002024238440b1e0202020202020202020202
020202020202020202090202020202020202020202020202020202020000020202020202020202
020202020202020202030902020202020202020202021f6f02021d4002024d02628e0250406320
6502020202682f020202730f020202832e510202020202020202020202020202020202106b4d63
7a0202020229632c1b0202862e2d2d3d4d0202a89b645c02020202020202020202020202020202
020202020209020202020202020202020202020202020202000002020202020202020202020202
02020202020309020202020202020202020225200202024b025b4b025f020202697ea1632ea602
02028b550202026490430202023369021e020202020202020202435b613d24273d2d4e02020212
27532d3d2d4264332d2d2d3da70202313b2a020202020202020202020202020202020202020202
020902020202020202020202020202020202020200000202020202020202020202020202020202
02030902020202020202020202024c3d7b020202021b59020202867123020202642c6602020229
02020202020251020228140297a002020202020c7133333d3d54631a204002020291305e9c8b29
40272d2d542d2d2d33160233610202020202020202020202020202020202020202020202090202
020202020202020202020202020202020000020202020202020202020202020202020202030902
0202020202020202020202897b335f020202219c029934528c020202021a2d638e02604e176402
02021f02023aa10262207502023967332d2d2d2d406b02029d1a02020202020202020212109a69
a5292d3d3d3d630e020202020202020202020253340d0202020202020202020209020202020202
020202020202020202020202000002020202020202020202020202020202020203090202020202
02020202020202020202020202025e341002914102023f020202242d204054639e860202648202
02458a020e2d536429203d2d53842d2d260202021a5454020f0e182c2b1802020202a4257e232d
2d2d2902020202020202020202028444021a020202020202020202020902020202020202020202
020202020202020200000202020202020202020202020202020202020309020202020202020202
0202020202020202020202504e0202a2590272021002a02d2d3d330202023a3310020202210202
4567972d2d2d2d5341020c2c5b0202742b8702020202020202184e4c0e74020b868658333d5338
0202020202020202a32d7a96496502020202020202020202090202020202020202020202020202
020202020000020202020202020202020202020202020202030902020202020202020202020202
020202020202020202302a027202420291029f533d3d2902020f5402020202151f02022d8c4d40
642d2d2d02020229870202183602020202025c9302023f229b847a0202020202a0203344126280
020202a12d02021b5617653c380202020202020209020202020202020202020202020202020202
000002020202020202020202020202020202020203090202020202020202020202020202020202
02020202020202712d916a020c9737202d2d280202976f02020282340202022b021a3202243d91
460202615602024b02020202024b90020202020202028a02980b0202020f1f53338a6a21983b70
0202694588020202200c0202020202020902020202020202020202020202020202020200000202
020202020202020202020202020202020309020202020202020202020202020202020202020202
020202024b77395d024a303d2d64020202870202021f9a02020218027c342a024b58020e020295
3b0202230202020213290202020202129b350202024b4b9c02020f2402020202029d70027a9e02
0c7a99024d02020202020202090202020202020202020202020202020202020000020202020202
020202020202020202020202030902020202020202020202020202020202020202020202020202
022f022b0255202d4002020f0202020287020202020202102b7802027775026702020c73023e46
314a83442c25020259257b2f45294e730202384e7a020e2002023102020202228c8d4c993e668d
7e5702020202020209020202020202020202020202020202020202000002020202020202020202
020202020202020203090202020202020202020202020202020202020202020202020202020258
1f87402d2d1b3c02020202020296110202022f4e370202022302027702025a10024f7458656197
4402020202020202020267407002022b0202952902024b3736020298557402020202025c020202
020202020902020202020202020202020202020202020200000202020202020202020202020202
020202020309020202020202020202020202020202020202020202020202020202020220542d2d
2c3e0202020202661f11026826667002020202832d020262020255020202020202020202020202
94950202020202632502025c020234300202592c900202190202762a0202951c02020202020202
090202020202020202020202020202020202020000020202020202020202020202020202020202
0309020202020202020202020202020202020202020202020202020202020202243d925b020202
02182d7002029302020202020202022078021757028602020202020202020202023b726e020276
3f02026a290202027a022e38020202181b0274907f417f1202695c020202020202020209020202
020202020202020202020202020202000002020202020202020202020202020202020203090202
02020202020202867402020202020202020202020202020202020202672d4a0202027d34380202
020202028f66020290026b600202230202900202029102425c26674b4d3602020202282939028d
23020202020202810202023c4202025b70331b3073277102020202020202020902020202020202
020202020202020202020200000202020202020202020202020202020202020309020202020202
0218203455342d1a165f0202020202020202020202020202153d8902028b3d2f02020202028c53
8b0202745102134602022d0b023b0202022002028d520c02020202020211023621027564020202
8e020202200202862e7e0266023c2e6723395f0202020202020202090202020202020202020202
020202020202020000020202020202020202020202020202020202030902020202021218020202
0242025c028384160202020202020202020202021529026402612085020202024540110202027d
8602023402021811022e020202690c020202020202870202026639028839195d025d02021b0202
341d0c8929440250027e7702025f8a020202020202020209020202020202020202020202020202
020202000002020202020202020202020202020202020203090202020202344b7b19220c7a7c2b
4602773d29301d020202020202020202691502405f52400202020233260202027d022f02020242
56026b0202347e020260247f0202020202488002020269020c2a1302263f02026902026e728102
022b02023502024333820202020202020202020902020202020202020202020202020202020200
000202020202020202020202020202020202020309020202023602020202020202020274020218
2d424370457502020202767102026577025b53020202203302020235780265160202643d020c02
794068020202244a02020202027a64020202643a0202024333650202210202020c610202021a02
1e64520c0f02020202020202020202090202020202020202020202020202020202020000020202
020202020202020202020202020202030902020202684234020202020202020202020269020202
026a2329211a6502026b3a020220420202383d41020202696c026d6702026e2402020252546f02
0202452770020202025c5c020202242e020202024e710202203c02020c71025502020202727302
020202020202020202020209020202020202020202020202020202020202000002020202020202
020202020202020202020203090202020202024202020202020202020202020259020202021733
5a0202025b4d02435c535d020248230202023b5302025c2902025e4e0202025f33600202021f2d
61020202183d6202020c334e02020202634e02020c33640202652a660202026702020202020202
020202020202020902020202020202020202020202020202020200000202020202020202020202
0202020202020203090202020222334243020202020202020202020202020244452d4402463a3b
47020248494a02022a4b2402021233450202263d4c024d20370202024e4e02024f27202c020250
273d5102022a333d4f02025253545502022c2d560202575802114f020202020202020202020202
020202090202020202020202020202020202020202020000020202020202020202020202020202
020202030902020202022329120202020202020202020202020202020202022a02020202020202
020202022b2c2302021f2d2e02022f2d2902302d310202323334020235363738020239342c3a02
0202242d3b02023c2d3d3e02023f27400202021b02410202020202020202020202020202020209
020202020202020202020202020202020202000002020202020202020202020202020202020203
090202020202021002020202020202020202020202020202020202020202020202020202020202
02110f1202131415020216171802191a1b02021c1d02020202020202020202020b1e02020b1f20
21020222232425020202262721280218151a020202020202020202020202020202020902020202
020202020202020202020202020200000202020202020202020202020202020202020309020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020b0c0202020d0e0f020202020202020202020202020202090202020202020202
020202020202020202020000020202020202020202020202020202020202030902020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020209020202020202020202020202
020202020202000002020202020202020202020202020202020203090202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020a02020202020202020202020202020202
020200000202020202020202020202020202020202020304050506060606060606060606060606
060606060606060606060606060606060606060606060606060606060606060606060606060606
060606060606060606060606060606060606060606060606060606060606060606060606060606
060606060606060606060606060606060607080202020202020202020202020202020202020000
040000002701ffff030000000000
}
</xsl:text>
</xsl:template>

<xsl:template name="LogoImage_f">
<xsl:text>
{\pict\wmetafile8\picwgoal2070\pichgoal1201 
010009000003b21c000000008d1c000000000400000003010800050000000b0200000000050000
000c025a009700030000001e0004000000070104008d1c0000410b2000cc005900960000000000
590096000000000028000000960000005900000001000800000000000000000000000000000000
00000000000000000000000000ffffff00fefefe00fafdfe0024b2ee003dbaf00022b1ee0022b2
ee00f3fbfe00eff9fe0016aded00fdfdfd00fafafa00dcdcdc00f2f1f200e3e3e300bdbdbd00bc
bbbc00efefef00a1a1a200a9a9aa00f3f3f3005b5a5c00414042003a393b00c5c5c500f7f7f700
4b4a4c0035343600fcfcfc0023222400202022001a191b001e1d1f00383739002b2a2d00787879
0066666700e1e1e1002a292b00282729003433350057565700d0d0d000dfdfdf009e9e9f005353
540098979800e4e4e400818081009c9b9c00b7b6b7009f9fa000c3c3c300d1d1d100737274007e
7d7f0029282a003e3d3f0043424400717072006e6e6f0075747600eeedee0046454700e7e7e700
3130320033323400504f5100b4b4b4009b9a9b00bab9ba00c0c0c000cdcdce00f5f5f500eeeeee
0089888a00f9f9f900636264005f5e6000ababac00abaaab008d8c8e00e0e0e000a8a8a9008b8a
8b0068676900d5d5d600f8f8f8007b7a7b00c9c9ca00363537006b6b6c002e2d2f00ededed00eb
ebeb00adacad009d9d9e00e5e5e600908f9000dbdbdb00dbdbdc00cfcfd00091919200cbcacb00
93929300807f800026252700a6a5a600fbfbfb00e8e7e800dad9da00b3b2b30074737500959596
007776770085858600a8a7a800f4f4f400f6f6f60048474900969697005857590049484a008786
8700b1b1b200e9e9ea009493940084838500d4d4d400b4b4b500fefdfe00bfbebf00a7a7a70070
6f7100d4d3d400ddddde00b7b7b8002d2c2e00c1c1c20087878800cccbcc008e8e8f00aeaeaf00
5e5d5f00ececec00e8e8e800302f3100a3a2a300c7c6c7009a999a00edeeee0082828300616062
00b0afb000fdfefd004e4d4f00d7d7d700d3d3d300d8d8d800bfbfbf00c7c7c800a4a4a400dfdf
e000f0f0f000ebeceb00d8d7d800bbbabb00acacac00c9ecfb00c5eafb0027b3ee002eb4ef002f
b5ef0022b0ee002fb5ee002bb4ef0024b1ee002db4ef0028b3ef009ddcf8009bdcf7005ac5f200
e7f7fd00d2effb0094d9f60067c9f3002ab3ef004ec1f0002fb6ef00cfeefb0020b0ee0089d6f6
00b8e6f900abe1f900c8ebfb00fcfefe0051c1f200a5dff700d9f1fc00e1f4fd00b0e3f80039b9
ef0035b8ef0035b8f00032b6ef001dafee0077cff400bee8fa007dd1f5008fd7f70025b1ee0046
bdf10096daf700b4e5f900b2e4f900a3def90024b0ef008fd8f600ceedfb0020b0ed0027b3ef00
21b2ee0082d3f500b5b5b600c4c4c400f3f4f400f4f3f400fbfcfb00262829008c8b8d00323435
00c3c3c40000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000202020202020202020202020202020202020202
0202020202020202020202499d0202020202020202020202020202026f2f8f0202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020000020202020202020202020202020202020202020202020202
02020202020202392a020202020202020202020202020244428a23279802020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202000002020202020202020202020202020202020202020202020202020202
020202272a020202020202020202020202020227900202465d1502020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
0202020202000002020202415f0202020202022602020202020202020202020202020202020227
165e5f0202020202025f02020b02024a1e60147943e80202020202020202020202020202020202
125f020202020202020202024d5e020202020202024a6202020202020202020202020202020202
020202020230020202020202025802020202020202020b53020202020202020202020253020202
020000020278274242394c020296396b234f02020231210202252002029c1f020202271c285de7
02020288392822231e0202222223272329020202219c0202211602024b1e7002026d395d28393d
02020202020202598a6b7a1e4f020202298a28281b020202020202027f210202027f1e76020295
39274223280202023b5d1e841e740202021b2323234402022818020202902328395b0202000002
232243526c3c81027e5d2e02e6299a02028c2302023d2802022a8a020202272202a1429002025c
8a0235422f02029f2753580202020202272a0202274e02021293330202188a8702964802020202
02023f5d3b023e5b56020273437d02a76a02020202020202962702020296934a02026b234d7d42
290202022744876b1c520202722225588f9d0202293b02028b293e026f2e02020000135b3b0202
020202025523020202399c02028c2302023d2802022a8a020202232a0202402802024923995e23
850202e4428a28e51a020202272a0202274e02021293330202285c020202020202020202027042
5302022756020243220202020202020202020202962702020296934a0276937202025d29020202
274e025e425202023b39020202020202293b02023c2802020202020200001b426e020202020202
3e23020202234002028c2302023d2802022a8a020202232a020225270202020e385d5b85020259
93531a8a16020202277a0202274e020212933302022842212139230202020202027d9312020223
560202435b20216b2102020202020202962702020296934a027693a202025d29020202274e026d
5d5202021823020202020202293b0202862220201e1ee302000093390202020202020235298902
e2422502028c5b2a023d5b3e024423020202271c0202236b020236450202236c02024f39020223
17020202275d5702274e02021293330202425b0202239c0202020202027e425c0202235602023d
270202444202020202020202322702020296934a02026b1c02022329020202274e026d5d520202
3c432b0202020202293b02027d934d026927020200003a23020202020202020229932a271e0202
028c421b4342433a5b292202020227188a181c520202648a865c43e10202a65d78161c18250202
27401e3a5b8c0202129389020262271b2e5d9e0202020202020216433b8a5b560202021e182593
320202020202028c3a1c407d02965d4a0202854229185b29020202394e026d5d520202021e5d17
237f0202433b020202438a3c5d2e020200004c226a0202020202020202322e3102020202681302
94254b02725c580202021333a744e0020202020d992e6002020202a390163584a7020213826d3c
7c02020258a22c02020258567a0f020202020202020202511b6c235602020202382ee102020202
0202027e3d5d8564029ea20c0202029e4438432902020213a7020b944902020202809c3d150202
6c6002020202612e320202020000026b295c369e6a7e0202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
024902020202020202020202020202020202020202022756020202020202020202020202020202
96270202020236020202020202025d29020202020202020202020202020202020202020e580202
02020202020202020000020c5d424343234e020202020202020202020202020202020202020202
02020202020202020202020202020202020202020202020202020202020202020202029d936c02
020202020202020202020202020202020202275602020202020202020202020202020268298f02
648c93260202020202025d29020202020202020202020202020202020202022893020202020202
020202020000020202929a896d0202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202024a218d0202020202
02020202020202020202020202021e4e020202020202020202020202020202022293281e601f4d
020202020202288a02020202020202020202020202020202020202402e02020202020202020202
000002020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202024a580202020202020202020202020202020202622b4a02020202020202
02024a4a0202020202020202020202020202020202020202020202020202020202020200000202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020000020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202000002020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020200000202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020000020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202000002020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020200000202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020000020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202000002020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020200
000202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020000020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202000002020202020202
02020202020202020202020202020202020202020202030aaeaeaeaeaeaeaeaeaeaeaeaeaeaeae
aeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeae
aeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeaeae0a0202020202
020202020202020202020202020202020202020202020202020200000202020202020202020202
02020202020202020202020202020202020203abadadadadadadadadadadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadadadadadadadadae020202020202020202
020202020202020202020202020202020202020202020000020202020202020202020202020202
020202020202020202020202020203abadadadadadadadadadadadadadadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadb0b304abb3b0adadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadadadadae02020202020202020202020202
020202020202020202020202020202020202000002020202020202020202020202020202020202
0202020202020202020203abadadadb0d1cfcfcfcfcfcfcfd1bbadadadadadadadadadccd1cfcf
cfcfcfcfcfcfd4adadadadadb2bd04b0cfd8c809030809b7c3b5d1d40604adadadadb0d1cfcfcf
cfcfcfcfd1bdafadadadadadadadadadadadadadadae0202020202020202020202020202020202
020202020202020202020202020200000202020202020202020202020202020202020202020202
02020202020203abadadadde020202020202020202d4bdadadadadadadadadd802020202020202
020206adadadadadaedf020202020202020202020202020202c4d1adadaddc0202020202020202
02cdafadadadadadadadadadadadadadadae020202020202020202020202020202020202020202
020202020202020202020000020202020202020202020202020202020202020202020202020202
020203abadadadadda0202020202020202b4adadadadadadadaddd080202020202020202c8b0ad
adadb3ba020202020202020202020202020202020202d8adadadbf020202020202020202cdadad
adadadadadadadadadadadadadae02020202020202020202020202020202020202020202020202
0202020202020000020202020202020202020202020202020202020202020202020202020203ab
adadadadcd0202020202020202c404adadadadadadadb0020202020202020202baadadadddaa02
020202020202020202020202020202020202d8adadadbf020202020202020202cdadadadadadad
adadadadadadadadadae0202020202020202020202020202020202020202020202020202020202
02020000020202020202020202020202020202020202020202020202020202020203abadadadad
b309020202020202020204adadadadadadadcf020202020202020202dcadad04be020202020202
0202020202020202020202020202d8adadadbf020202020202020202cdadadadadadadadadadad
adadadadadae020202020202020202020202020202020202020202020202020202020202020000
020202020202020202020202020202020202020202020202020202020203abadadadadadda0202
020202020202bacdcdcdcdcdcdb0090202020202020202dbb2adbdb90202020202020202020202
02020202020202020202d8adadadbf020202020202020202cdadadadadadadadadadadadadadad
adae02020202020202020202020202020202020202020202020202020202020202000002020202
0202020202020202020202020202020202020202020202020203abadadadadad06020202020202
02020202020202020202020202020202020202b6b2adb002020202020202020202a9c5bdcdd102
020202020202d8adadadbf020202020202020202cdadadadadadadadadadadadadadadadae0202
020202020202020202020202020202020202020202020202020202020200000202020202020202
02020202020202020202020202020202020202020203abadadadadadb308020202020202020202
020202020202020202020202020202aeadb2d0020202020202020202b6b2adadadb60202020202
0202d8adadadbf020202020202020202cdadadadadadadadadadadadadadadadae020202020202
020202020202020202020202020202020202020202020202020000020202020202020202020202
020202020202020202020202020202020203abadadadadadadba02020202020202020202020202
02020202020202020202aab2adae020202020202020202d6b2adadadadb602020202020202d8ad
adadbf020202020202020202b3d30404040404040404d3adadadadadae02020202020202020202
020202020202020202020202020202020202020202000002020202020202020202020202020202
0202020202020202020202020203abadadadadadad040202020202020202020202020202020202
020202020202cbadb2d4020202020202020202afafadadadadb602020202020202d8adadadbf02
0202020202020202020202020202020202020204adadadadae0202020202020202020202020202
020202020202020202020202020202020200000202020202020202020202020202020202020202
02020202020202020203abadadadadadadb2c70202020202020202020202020202020202020202
0202aeadadd5020202020202020202d9adadadadadb602020202020202d8adadadbf0202020202
02020202020202020202020202020204adadadadae020202020202020202020202020202020202
020202020202020202020202020000020202020202020202020202020202020202020202020202
020202020203abadadadadadadadba0202020202020203d6c1c1c1d70202020202020202b5adad
add60202020202020202b7b0adadadadadb602020202020202d8adadadbf020202020202020202
020202020202020202020204adadadadae02020202020202020202020202020202020202020202
020202020202020202000002020202020202020202020202020202020202020202020202020202
0203abadadadadadadadae0202020202020202bfadadadd40202020202020202cbadadadd00202
020202020202c8b0adadadadadb6b7b7b7b7b7b7b7d5acadadbf02020202020202020202020202
0202020202020204adadadadae0202020202020202020202020202020202020202020202020202
02020202020000020202020202020202020202020202020202020202020202020202020203abad
adadadadadadb0b802020202020202baadadacc30202020202020208abadadadd0020202020202
020208b3adadadadadacb0bbbbbbbbbbbbb0adadadbf0202020202020202020202020202020202
02020204adadadadae020202020202020202020202020202020202020202020202020202020202
020000020202020202020202020202020202020202020202020202020202020203abadadadadad
adadbdbc02020202020202c8b0adbf0202020202020202d2adadadadc9020202020202020202bf
b2adadadadadadadadadadadadadadadadbf0202020202020202020202020202020202020202d3
adadadadae02020202020202020202020202020202020202020202020202020202020202000002
0202020202020202020202020202020202020202020202020202020203abadadadadadadadafae
020202020202020206adbc020202020202020206adadadadd1020202020202020202d1adadadad
adadadadadadadadadadadadadbf0202020202020202020406060606060606060606adadadadad
ae0202020202020202020202020202020202020202020202020202020202020200000202020202
02020202020202020202020202020202020202020202020203abadadadadadadadadadc1020202
02020202cfadd002020202020202b7b3adadadadb30202020202020202020206adadadadadadad
adadadadadadadadadbf020202020202020202cdadadadadadadadadadadadadadadadae020202
020202020202020202020202020202020202020202020202020202020000020202020202020202
020202020202020202020202020202020202020203abadadadadadadadadad0502020202020202
b7ce0202020202020202cfadadadadad040202020202020202020202cbb0adadadadadadadad06
05adadadadbf020202020202020202cdadadadadadadadadadadadadadadadae02020202020202
020202020202020202020202020202020202020202020202000002020202020202020202020202
0202020202020202020202020202020203abadadadadadadadadadbf0202020202020202050202
020202020202aeadadadadadadb60202020202020202020202b8bab3bfbfaeb0b6c902a9acadad
adbf020202020202020202cacbcbcbcbcbcbcbcbcccbcdadadadadae0202020202020202020202
020202020202020202020202020202020202020200000202020202020202020202020202020202
02020202020202020202020203abadadadadadadadadadadc60202020202020208020202020202
02c7b0adadadadadadb3c80202020202020202020202020202020202020202c3acadadadbf0202
020202020202020202020202020202020202c0adadadadae020202020202020202020202020202
020202020202020202020202020202020000020202020202020202020202020202020202020202
020202020202020203abadadadadadadadadadad05020202020202020202020202020202c5adad
adadadadadadbf0202020202020202020202020202020202020202c3acadadadbf020202020202
0202020202020202020202020202c0adadadadae02020202020202020202020202020202020202
020202020202020202020202000002020202020202020202020202020202020202020202020202
0202020203abadadadadadadadadadaf06030202020202020202020202020202bfadadadadadad
adadadaec4020202020202020202020202020202020202c3acadadadbf02020202020202020202
02020202020202020202c0adadadadae0202020202020202020202020202020202020202020202
020202020202020200000202020202020202020202020202020202020202020202020202020202
03abadadadadadadadadadadadb502020202020202020202020202c1acadadadadadadadadadad
aec20202020202020202020202020202020202c3acadadadbf0202020202020202020202020202
020202020202c0adadadadae020202020202020202020202020202020202020202020202020202
020202020000020202020202020202020202020202020202020202020202020202020203abadad
adadadadadadadadadab02020202020202020202020202bcbdadadadadadadadadadadadadadbe
020202020202020202020202020202beacadadadbf020202020202020202020202020202020202
0202c0adadadadae02020202020202020202020202020202020202020202020202020202020202
0000020202020202020202020202020202020202020202020202020202020203abadadadadadad
adadadadadb3b4b5b5b5b5b5b5b5b5b5b5b5b4abadadadadadadadadadadadadadadb2aeb6b4b7
0202020202020208b8b9babbadadadad04b4b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b6ad
adadadae0202020202020202020202020202020202020202020202020202020202020200000202
02020202020202020202020202020202020202020202020202020203abadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadadadadadadadadafadadadadb004aeaeae
0606b1abb2adadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadae
020202020202020202020202020202020202020202020202020202020202020000020202020202
020202020202020202020202020202020202020202020203abadadadadadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadae02020202
020202020202020202020202020202020202020202020202020202000002020202020202020202
0202020202020202020202020202020202020203abacadadadadadadadadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadadad
adadadadadadadadadadadadadadadadadadadadadadadadadadadadadacae0202020202020202
020202020202020202020202020202020202020202020200000202020202020202020202020202
0202020202020202020202020202020304a9aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa90a020202020202020202020202
020202020202020202020202020202020202020000020202020202020202020202020202020202
020202020202020202020203040202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020a02020202020202020202020202020202
020202020202020202020202020202000002020202020202020202020202020202020202020202
020202020202020304020202020255345194557271020202020202020202020202020202020202
02021d1c3e864c020f687402020202020202020202020202024f3d8e50022ba802020202020202
020202020248738e3861024f3660020202020a0202020202020202020202020202020202020202
020202020202020202020200000202020202020202020202020202020202020202020202020202
02020203040202020215020202020287a802020202020202020202020202020202020202541e10
5102020288028e020202020202020202020202021e0c02020202833c0202020202020202020202
0b5502020202028b029c020202020a020202020202020202020202020202020202020202020202
020202020202020000020202020202020202020202020202020202020202020202020202020203
0402020202a70202130c0e7b6c1d0202020202020202020202020202020202337b2f0272968957
4e5a0b020202020202020202020202595a02022685811b1602020202020202020241251e2f0f78
8b8b02026166020202020a02020202020202020202020202020202020202020202020202020202
020202000002020202020202020202020202020202020202020202020202020202020304020202
02023a925738a61d7e0202020202020202020202020202020202413a67436b0202024b7a640202
020202020202020202022c29025d3a020202689d02020202020202912941021a58020202029e24
0202020202020a0202020202020202020202020202020202020202020202020202020202020200
000202020202020202020202020202020202020202020202020202020202030402020202020e20
7f288b020202020202020202020202020202020202351f02a50257020202020202020202020202
02020202020268383d0e02020202020202020202020259026a5c8d7e3a02020202020202020202
02020a020202020202020202020202020202020202020202020202020202020202020000020202
020202020202020202020202020202020202020202020202020203040202020202020202021676
020202020202020202020202020202022b02116702180202020202020202020202020202020202
02024738025302020202020202020202a31e8a1d02020262020202020202020202020202020a02
020202020202020202020202020202020202020202020202020202020202000002020202020202
02020202020202020202020202020202020202020202030402020202020202858e02208d020202
0202020202020202020202229d02027a5b25020202020202020202020202020202020202023102
76431502020202020202023d1e4b2e6232731902020202020202020202020202020a0202020202
020202020202020202020202020202020202020202020202020200000202020202020202020202
020202020202020202020202020202020202030402020202020202020e1f7e16286c17235d173e
6f0202020202a2184f4274020223150202020202020202020202020202020202022a02a03d7302
02020202022e2314020c6f02022e0202020202020202020202020202020a020202020202020202
020202020202020202020202020202020202020202020000020202020202020202020202020202
020202020202020202020202020203040202020202020202888c02022802026c8146027b1e2402
0202021802024b440202536a6f0202020202020202020202020202a48517020202592a732c0202
445b5b90020b591e14020202020202020202020202020202020a02020202020202020202020202
020202020202020202020202020202020202000002020202020202020202020202020202020202
02020202020202020202030402020202020202020e1e02025a027f520202027466491f52020202
16020291251d0202905ba102020202020202767d2e42399343a20202706b5d5b5d9c23221c1c96
02a32e7602020202020202020202020202020202020a0202020202020202020202020202020202
020202020202020202020202020200000202020202020202020202020202020202020202020202
02020202020203040202020202020202021e227e0202386f02028e1e9f0202189326029647020c
02024702a01902204d020202138a935b5b40322a9802023266024a3d182329235b1c937c444602
02020202020202700202020202020202020a020202020202020202020202020202020202020202
020202020202020202020000020202020202020202020202020202020202020202020202020202
02020304020202020202020202020226020202219d7e18020202020239937a7523936f027d4902
606f02397a1044275b29285b440202138a460b5f888e9e0202027d62175b1c5b4e020202020202
02028b592412020202020202020a02020202020202020202020202020202020202020202020202
020202020202000002020202020202020202020202020202020202020202020202020202020304
0202020202020202020202020202021d441d0238025d021d10295b2949029b3b8d02021602021e
37295b5b938b02393302021f4802027e62028c9c608d022b027a275b4002020202020202981165
2f36020202020202020a0202020202020202020202020202020202020202020202020202020202
020200000202020202020202020202020202020202020202020202020202020202030402020202
02020202020202020202020202610296844e02715f425b2702025d02020277180297567244225b
1b02021619028889020202584058021d778f2d0c020202982399192e360202419a0d40322b1366
02020202020a020202020202020202020202020202020202020202020202020202020202020000
020202020202020202020202020202020202020202020202020202020203040202020202020202
020202020202020202020293782d02240f425b5502026a02025a7802023b023b0c105d0d6c0281
940213580202021e0b02020202020202409502020b8a72021985200202204b020b021e02020202
020a02020202020202020202020202020202020202020202020202020202020202000002020202
020202020202020202020202020202020202020202020202020304020202020202020202020202
0202020202020202906d50026b5b220291020202451a02021d028f17021a1b024f020272027b0e
11024417026f8c4e1821221202501f02022102026d020202774a9248365a4702020202020a0202
020202020202020202020202020202020202020202020202020202020200000202020202020202
020202020202020202020202020202020202020202030402020202020202020202020202020202
02020202020220495d295c1602020202022b02021d2e790202498d028e02766202571c472e2d02
02020202020224390202220202200283866502128f88020202023e02020202020a020202020202
02020202020202020202020202020202020202020202020202feff020202020202020202020202
020202020202020202020202020202020203040202020202020202020202020202020202020202
02024d275b5b7a0202026d2a79028586870202020240028812020b020202020202020202897f02
0202028a8b027202864402027b24022c02028c025e8c0202020202020a02020202020202020202
020202020202020202020202020202020202020202feff02020202020202020202020202020202
020202020202020202020202020304020202020202020202020202020202020202020202020254
1c290202027a8002020c020253024a025a310244020230024102355f120d56448102021f820234
3802020202590202831c0238025d84591b7f0202020202020a0202020202020202020202020202
0202020202020202020202020202020202feff0202020202020202020202020202020202020202
020202020202020202030402020202020d782e1b18794d0202020202020202020202022345021d
1e13020202023618020259027a02027b020b640235150b317c1202020202024d3902337d02020e
02023d02023b0b7e1a7f27793e360202020202020a020202020202020202020202020202020202
02020202020202020202020202feff020202020202020202020202020202020202020202020202
02020202020304020202025c020202660225025c52020202020202020202022102246f276d0202
021c7002020271020e720273022b65026f6a0202020202660202744d02752e0259025a0f022102
363b490b02764e02776f0202020202020a02020202020202020202020202020202020202020202
020202020202020202feff02020202020202020202020202020202020202020202020202020202
020304020202626337640b0b0225650e5d2318020b02020202024e660240024202021a205f0202
67025602022002680238690202436a02020202550202026b02026c6d4e026e2d02412102024002
155e023712020202020202020a0202020202020202020202020202020202020202020202020202
0202020202feff0202020202020202020202020202020202020202020202020202020202030402
0202555657020202020202025821020249223c382e5902022e025a1702025b5c0202314b025d02
02285e02023b4e02024739020202024e5f02021f020202604402363a0202610202024902212e02
02020202020202020a020202020202020202020202020202020202020202020202020202020202
02feff020202020202020202020202020202020202020202020202020202020203040202021d1d
480202020202020202494a020202273702020218024b234c02021f02021a2102021e4d023a1002
024e4f02023629500202132802023042510202523902024e1b0202532e0202544b020202020202
020202020a02020202020202020202020202020202020202020202020202020202020202feff02
0202020202020202020202020202020202020202020202020202020203040202022d2e2f020202
020202020202020230313202333435020236370202383902023a3b0202273c02233d02023e2802
3f1e39400202283902024142430202441c45022b43460202220247020202020202020202020202
0a02020202020202020202020202020202020202020202020202020202020202feff0202020202
020202020202020202020202020202020202020202020202030402020202181902020202020202
020202020202020202020202020202021a1b1c151d1e1f02022020022122020223240202020202
020225180e02262728020218291302022a232b0217022c0202020202020202020202020a020202
02020202020202020202020202020202020202020202020202020202feff020202020202020202
020202020202020202020202020202020202020203040202020202020202020202020202020202
020202020202020202020202020202020202020202020202020b02020202020202020202020202
0202020c0d0e020f10110202121314150216170202020202020202020202020a02020202020202
020202020202020202020202020202020202020202020202feff02020202020202020202020202
020202020202020202020202020202020304020202020202020202020202020202020202020202
020202020202020202020202020202020202020202020202020202020202020202020202020202
0202020202020202020202020202020202020202020202020202020a0202020202020202020202
0202020202020202020202020202020202020202feff0202020202020202020202020202020202
020202020202020202020202030708090909090909090909090909090909090909090909090909
090909090909090909090909090909090909090909090909090909090909090909090909090909
09090909090909090909090909090909090909090909080a020202020202020202020202020202
02020202020202020202020202020202feff020202020202020202020202020202020202020202
020202020202020203040505050505050505050505050505050505050505050505050505050505
050505050505050505050505050505050505050505050505050505050505050505050505050505
050505050505050505050505050505050505050602020202020202020202020202020202020202
020202020202020202020202feff040000002701ffff030000000000
}</xsl:text>
</xsl:template>

</xsl:stylesheet>  
