<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--  Import all of the logic from the Commitment Letter template so that the 2 documents 
	(standalone C/L and embedded C/L) remain the same -->
	<xsl:import href="CommitmentLetterGENXFO.xsl"/>
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LanguageEnglish">
				<xsl:call-template name="EnglishSolicitorInstructions"/>
				<xsl:call-template name="EnglishGuide"/>
				<xsl:call-template name="EnglishCommitmentLetter"/>
				<xsl:call-template name="EnglishRequestForFunds"/>
				<xsl:call-template name="EnglishFinalReport"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LanguageFrench">
				<!--				<xsl:call-template name="FrenchTemplate"/>-->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="FOEnd"/>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishSolicitorInstructions">
	<xsl:text>
	&lt;fo:page-sequence master-reference="main"&gt;
		&lt;fo:static-content flow-name="xsl-region-after"&gt;
			&lt;fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="right"&gt;Page &lt;fo:page-number/&gt; of &lt;fo:page-number-citation ref-id="endofdoc"/&gt;
			&lt;/fo:block&gt;
		&lt;/fo:static-content&gt;
		&lt;fo:flow flow-name="xsl-region-body"&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="268.85pt"/&gt;
				&lt;fo:table-column column-width="268.85pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block&gt;
								&lt;fo:external-graphic src="\mos_docprep\admin\docgen\templates\basis.gif"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="end"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="end"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/><xsl:text>&lt;/fo:block&gt;
							</xsl:text><xsl:if test="//CommitmentLetter/BranchAddress/Line2">
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="end"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
							</xsl:if><xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="end"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/City"/><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/><xsl:text>  </xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/CurrentDate"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/><xsl:text> </xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/><xsl:text> </xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Dear </xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/><xsl:text>,&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="32.4pt"/&gt;
				&lt;fo:table-column column-width="148.5pt"/&gt;
				&lt;fo:table-column column-width="356.8pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;RE:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;BORROWER(S):&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
						</xsl:text>
						<xsl:for-each select="//SolicitorsPackage/BorrowerNames">
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="./Name"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
						</xsl:for-each>
						<xsl:text>
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/GuarantorClause"/><xsl:text>, if any:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;SECURITY ADDRESS:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/><xsl:text>&lt;/fo:block&gt;
							</xsl:text>
							<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/><xsl:text>
&lt;/fo:block&gt;
							</xsl:text>
							</xsl:if>
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/><xsl:text>, </xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/><xsl:text>  </xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/PostalCode"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;CLOSING DATE:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/AdvanceDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;APPROVED LOAN AMOUNT:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/LoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
				&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;
				&lt;fo:leader line-height="9pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;A Mortgage Loan has been approved for the above noted Borrower(s), in accordance with the terms and conditions outlined in the Commitment Letter attached. We have been advised that you will be acting for </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text> (hereinafter called the "Lender") in attending to the preparation, execution and registration of the Mortgage against the property and ensuring that the interests of the Lender as Mortgagee are valid and appropriately secured. Please note that the Lender will not require or approve an interim report on title or draft documentation, including the Mortgage. The Lender will rely solely on you to ensure that the Mortgage is prepared in accordance with the instructions outlined herein. Any amendments required due to errors, omissions or non-compliance on your part will be for your account and your responsibility to correct. The Lender will also rely solely on you to confirm the identity of the Borrower(s) and </xsl:text><xsl:value-of select="//SolicitorsPackage/GuarantorClause"/><xsl:text>, if any, and to retain the evidence used to confirm their identity in your file. The Mortgage may be registered electronically, where available. </xsl:text><xsl:value-of select="//SolicitorsPackage/eRegistration"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;Any material facts which may adversely affect the Lender's position as a First Change on the property are to be disclosed to the Lender prior to the advance of any funds. Similarly, any issues which may affect the Lender's security or any subsequent amendments to the Agreement of Purchase and Sale are to be referred.to this office for direction prior to the release of funds. Our prior consent will be required relative to any requests for additional or secondary financing. If for any reason you are unable to register our Mortgage on the scheduled closing date, we must be notified at least 24 hours prior to that date and advised of the reason for the delay. No advance can be made after the scheduled closing date without the Lender's written authorization.&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;You must submit the Solicitor's Request for Funds together with the required documentation to our Financial Services Centre &lt;fo:inline text-decoration="underline"&gt;no later than three (3) days prior to closing&lt;/fo:inline&gt;. The Solicitor's Request for Funds should contain confirmation that all our requirements have been or will be met and should specifically outline any qualifications that will appear in your Final Report and certification of title.&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;The mortgage funds will be sent to you by PREPAID courier or by Electronic Fund Transfer and should be disbursed without delay provided you are satisfied that all requirements have been met and adequate precaution has been taken to ensure that the mortgage security will retain priority.  &lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;Legal fees and all other costs, charges and expenses associated with this transaction are payable by the Borrower(s) whether or not the Mortgage proceeds are advanced.&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;In connection with this transaction we enclose the following documents:&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:list-block space-before="6pt" space-after="6pt" font-family="Arial, Helvetica"&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;*&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-style="italic" font-weight="bold" text-decoration="underline"&gt;Mortgage Commitment Letter&lt;/fo:inline&gt; - Please ensure that any outstanding conditions outlined in the Commitment Letter that are the responsibility of the Solicitor, are duly noted and that full compliance is confirmed prior to advance and included in your final report.&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;*&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-style="italic" font-weight="bold" text-decoration="underline"&gt;Solicitors Instructions and Guidelines&lt;/fo:inline&gt; - Self explanatory&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;*&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-style="italic" font-weight="bold" text-decoration="underline"&gt;Mortgage&lt;/fo:inline&gt;&lt;/fo:block&gt;
						</xsl:text>
						<xsl:for-each select="//SolicitorsPackage/ProvincialClause/Line">
						<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text>
						</xsl:for-each>
						<xsl:text>
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;*&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-style="italic" font-weight="bold" text-decoration="underline"&gt;Schedules&lt;/fo:inline&gt;- </xsl:text><xsl:value-of select="//SolicitorsPackage/ScheduleText"/><xsl:text>&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;*&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-style="italic" font-weight="bold" text-decoration="underline"&gt;Disclosure Statement&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
			&lt;/fo:list-block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Other required security/Special Conditions: </xsl:text><xsl:value-of select="//SolicitorsPackage/SolicitorSpecialInstructions"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;We consent to your acting for the Lender as well as the Borrower(s) and/or </xsl:text><xsl:value-of select="//SolicitorsPackage/GuarantorClause"/><xsl:text> provided that you disclose this fact to the Borrower(s) and/or </xsl:text><xsl:value-of select="//SolicitorsPackage/GuarantorClause"/><xsl:text> and obtain their consent in writing and that you disclose to each party all information you possess or obtain which is or may be relevant to this transaction. &lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-decoration="underline"&gt;FINANCIAL SERVICES CENTRE&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Documents and funding requests should be directed to:&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/><xsl:text>&lt;/fo:block&gt;
			</xsl:text>
			<xsl:if test="//SolicitorsPackage/BranchAddress/Line2">
			<xsl:text>
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/><xsl:text>&lt;/fo:block&gt;
			</xsl:text>
			</xsl:if>
			<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/><xsl:text> </xsl:text><xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/><xsl:text>  </xsl:text><xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Contact: </xsl:text><xsl:value-of select="//SolicitorsPackage/FunderName"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Phone: </xsl:text><xsl:value-of select="//SolicitorsPackage/BranchAddress/Phone"/><xsl:text> Ext. </xsl:text><xsl:value-of select="//SolicitorsPackage/FunderExtension"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Fax: </xsl:text><xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" margin-left="18.0pt" text-indent="-18.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;If you have any question or concerns, please feel free to call.&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader leader-length="72.0pt" leader-pattern-width="72.0pt"&gt;Sincerely,&lt;/fo:leader&gt;
				&lt;fo:leader leader-length="36.0pt" leader-pattern="space"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/AdministratorName"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Financial Services Administrator&lt;/fo:block&gt;
		&lt;/fo:flow&gt;
	&lt;/fo:page-sequence&gt;
	</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishGuide">
	<xsl:text>
	&lt;fo:page-sequence master-reference="main"&gt;
		&lt;fo:static-content flow-name="xsl-region-after"&gt;
			&lt;fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="right"&gt;Page &lt;fo:page-number/&gt; of &lt;fo:page-number-citation ref-id="endofdoc"/&gt;
			&lt;/fo:block&gt;
		&lt;/fo:static-content&gt;
		&lt;fo:flow flow-name="xsl-region-body"&gt;
			&lt;fo:block font-size="14.0pt" font-weight="bold" text-decoration="underline"&gt;SOLICITOR'S GUIDE&lt;/fo:block&gt;
			&lt;fo:block font-size="9.0pt" font-weight="normal" text-decoration="none"&gt;
				&lt;fo:leader line-height="9pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="9.0pt" font-weight="normal" text-decoration="none"&gt;
				&lt;fo:leader line-height="9pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;If you are satisfied that the Mortgagor(s) (also known as the "Borrower(s)") has or will acquire a good and marketable title to the property described in our Commitment Letter, complete our requirements as set out below, and prepare a Mortgage as outlined herein.&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Documentation:&lt;/fo:inline&gt; The Mortgage Form Number identified in our cover letter together with the applicable Schedule(s) must be registered.
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Mortgage Requirements:&lt;/fo:inline&gt; The Mortgage Document is to be registered as follows:
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed" margin-left="50pt"&gt;
				&lt;fo:table-column column-width="284.4pt"/&gt;
				&lt;fo:table-column column-width="252.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;Approved Loan Amount:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/LoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;Interest Rate (Compounded </xsl:text><xsl:value-of select="//CommitmentLetter/CompoundingFrequency"/><xsl:text>):&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/InterestRate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;Principal and Interest Payment:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/PandIPayment"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;Interest Adjustment Date:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/IADDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;First Monthly Payment Date:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/FirstPaymentDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;Maturity/Renewal Date:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt" font-weight="normal" space-after="12.0pt" text-align="justify" text-decoration="none"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/MaturityDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Searches:&lt;/fo:inline&gt; You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. </xsl:text><xsl:value-of select="//SolicitorsPackage/Execution"/><xsl:text>  It is your responsibility to ensure that there are no work orders or deficiency notices outstanding against the property, that all realty taxes and levies which have or will become due and payable up to the interest adjustment date are paid to the Municipality. If your search reveals title has been transferred since the date of the Offer to Purchase and before the closing date you must notify us immediately and discontinue all work on this transaction unless notified otherwise. Immediately prior to registration of the mortgage, you are to obtain a Sheriff's Certificate/General Registry Search indicating that there are no writs of execution on file against the mortgagor(s), guarantor(s), if any, or any previous owner, which would adversely affect our security.
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Condo/Strata Title:&lt;/fo:inline&gt; You are to confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. You are to review the Condominium Corporation's Declaration and Bylaws and confirm they contain nothing derogatory to our security. You are to assign the voting rights to </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text> (if applicable).
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Survey:&lt;/fo:inline&gt; </xsl:text><xsl:value-of select="//SolicitorsPackage/SurveyClause"/><xsl:text>.
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Fire Insurance:&lt;/fo:inline&gt; You must ensure, prior to advancing funds, that fire insurance coverage for building(s) on the property is in place for not less than their full replacement value with first loss payable to </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text> as Mortgagee at the address shown on the Charge/Mortgage and including a duly completed standard mortgage clause approved by the Insurance Bureau of Canada. Policies must not contain a co-insurance or similar clause that may limit the amount payable. Unless otherwise noted, we will not require a copy of this policy.
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" text-align="justify"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Matrimonial Property Act:&lt;/fo:inline&gt; You are to certify that all requirements under the Matrimonial Property Act in your province, where applicable, have been met and that the status does not in any way affect our mortgage/charge.
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-size="10.0pt" font-weight="normal" text-align="justify" text-decoration="none"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;			
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Advances:&lt;/fo:inline&gt;  When all conditions precedent to this transaction have been met, funds in the amount of </xsl:text><xsl:value-of select="//SolicitorsPackage/LoanAmount"/><xsl:text> will be advanced with the following costs being deducted from the advance by the Lender;
			&lt;/fo:block&gt;
			&lt;fo:block margin-left="36.0pt" text-indent="36.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed" margin-left="50pt"&gt;
				&lt;fo:table-column column-width="230.0pt"/&gt;
				&lt;fo:table-column column-width="216.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Mortgage Insurance Premium&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;(Included in Total Loan Amount)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/Premium"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Provincial Sales Tax on Premium&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/PST"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>

					<xsl:for-each select="//SolicitorsPackage/Fees/Fee">
					<xsl:text>
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="./FeeVerb"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="./FeeAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					</xsl:text>
					</xsl:for-each>

					<xsl:text>
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;TOTAL&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/TotalDeductionsAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Interest Adjustment (daily per diem)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/IADAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;The net Mortgage proceeds, payable to you, in trust, will be sent to your office prior to the scheduled closing date, which is anticipated to be </xsl:text><xsl:value-of select="//SolicitorsPackage/AdvanceDate"/><xsl:text>&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			</xsl:text>
			
			<!--  Internal refi -->
			<xsl:if test="//SolicitorsPackage/InternalRefi">
			<xsl:text>
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Refinances:&lt;/fo:inline&gt;  If the purpose of this Mortgage is to refinance existing debt with </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text>, we will retain the following additional funds to discharge existing mortgage(s) owing to us;
			&lt;/fo:block&gt;
			&lt;fo:block text-indent="36.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed" margin-left="50pt"&gt;
				&lt;fo:table-column column-width="216.0pt"/&gt;
				&lt;fo:table-column column-width="216.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Existing </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text> Mortgage&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;The balance of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed" margin-left="50pt"&gt;
				&lt;fo:table-column column-width="216.0pt"/&gt;
				&lt;fo:table-column column-width="216.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Other Debts to be Paid:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Amount:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					</xsl:text>

					<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
					<xsl:text>
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="./Description"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="./Amount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					</xsl:text>
					</xsl:for-each>
					
				<xsl:text>
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;The balance, if any, is to be paid to the Borrower(s).&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			</xsl:text>
			</xsl:if>
			
			<!--  External refi -->
			<xsl:if test="//SolicitorsPackage/ExternalRefi">
			<xsl:text>
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Refinances:&lt;/fo:inline&gt; If the purpose of this Mortgage is to refinance existing debt, the balance of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed" margin-left="50pt"&gt;
				&lt;fo:table-column column-width="216.0pt"/&gt;
				&lt;fo:table-column column-width="216.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Existing </xsl:text><xsl:value-of select="//SolicitorsPackage/LPDescription"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Other Debts to be Paid:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;Amount:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					</xsl:text>
					
					<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
					<xsl:text>
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="./Description"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="./Amount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					</xsl:text>
					</xsl:for-each>

					<xsl:text>
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;The balance, if any, is to be paid to the Borrower(s).&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			</xsl:text>
			</xsl:if>
			
			<xsl:text>
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Construction/Progress Advances:&lt;/fo:inline&gt;  If the security for this Mortgage is a new property and we are advancing by pay of progress advances, the advances will be done on a cost-to-complete basis. You are responsible for the disbursement of all loan advances made payable to you in trust and to ensure the priority of each advance by conducting all necessary searches and ensure that the requirements of the appropriate Provincial Construction Lien Act have been met before each advance is made. You are to retain appropriate amounts from the Mortgage proceeds to comply with Provincial lien holdback requirements. Unless otherwise noted, </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text> will require a copy of the executed Certificate of Completion and Possession form and/or the Occupancy Certificate.
			&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Disclosure:&lt;/fo:inline&gt;  The Mortgagor(s) and the </xsl:text><xsl:value-of select="//SolicitorsPackage/GuarantorClause"/><xsl:text> (if any) must sign a Statement of Disclosure. One copy is to be given to the Borrower(s) and a second copy is to be forwarded to </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text>. Please ensure that the applicable provincial legislation and timeframes regarding Disclosure Statements are adhered to. If a Statement of Disclosure cannot be signed, you must obtain a waiver from the Mortgagor(s) and the </xsl:text><xsl:value-of select="//SolicitorsPackage/GuarantorClause"/><xsl:text> (if any).
			&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Planning Act:&lt;/fo:inline&gt;  You must ensure that the Mortgage does not contravene the provisions of the Planning Act as amended from time to time, and that the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.
			&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Corporation as a Borrower:&lt;/fo:inline&gt; If the Mortgagor(s) or </xsl:text><xsl:value-of select="//SolicitorsPackage/GuarantorClause"/><xsl:text>(s) is a corporation, you must obtain a certified copy of the directors' resolution and the borrowing by-law of the corporation authorizing the Charge/Mortgage and ensure that the corporation is duly incorporated under applicable Provincial or Federal laws with all necessary power to charge its interest in the property and include these documents(s) with the Solicitor's Final Report on Title.
			&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;&lt;fo:inline font-weight="bold" text-decoration="underline"&gt;Solicitor's Report:&lt;/fo:inline&gt; You must submit the Solicitor's Final Report on Title, on the enclosed form and with the stated enclosures, within 30 days after the final advance is made. If your report cannot be submitted in this time frame, you must provide us with a letter explaining the reason(s) for the delay. Failure to comply with any of the above instrustions may result in discontinued future dealings with you or your firm.
			&lt;/fo:block&gt;
			&lt;fo:block font-size="12.0pt"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;
		&lt;/fo:flow&gt;
	&lt;/fo:page-sequence&gt;
	</xsl:text>
	</xsl:template>

<!--  We are using the build logic from the commitment letter template that already exists instead of replicating it here... -->
	<xsl:template name="EnglishCommitmentLetter">
		<xsl:call-template name="EnglishPage1"/>
  	</xsl:template>

	<xsl:template name="EnglishRequestForFunds">
	<xsl:text>
	&lt;fo:page-sequence master-reference="main"&gt;
		&lt;fo:static-content flow-name="xsl-region-after"&gt;
			&lt;fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="right"&gt;Page &lt;fo:page-number/&gt; of &lt;fo:page-number-citation ref-id="endofdoc"/&gt;
			&lt;/fo:block&gt;
		&lt;/fo:static-content&gt;
		&lt;fo:flow flow-name="xsl-region-body"&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="555pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row height="35.95pt"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" display-align="center" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="14.0pt" font-weight="bold" text-align="center" text-decoration="underline"&gt;SOLICITOR'S REQUEST FOR FUNDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" margin-left="252.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" margin-left="252.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" margin-left="252.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="72.0pt"/&gt;
				&lt;fo:table-column column-width="171.0pt"/&gt;
				&lt;fo:table-column column-width="45.0pt"/&gt;
				&lt;fo:table-column column-width="257.4pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" space-after="6.0pt" text-align="start"&gt;FAX TO:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" space-after="6.0pt" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="end"&gt;FROM:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/SolicitorName"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/SolicitorAddress1"/><xsl:text>&lt;/fo:block&gt;						
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" space-after="6.0pt" text-align="start"&gt;ATTENTION:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" space-after="6.0pt" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/Administrator"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" space-after="6.0pt" text-align="start"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;</xsl:text>

						<xsl:if test="//SolicitorsPackage/SolicitorAddress2">
							<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/SolicitorAddress2"/><xsl:text>&lt;/fo:block&gt;		
							</xsl:text>
						</xsl:if>

						<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/><xsl:text> </xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/><xsl:text>  </xsl:text><xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/><xsl:text>&lt;/fo:block&gt;									
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" space-after="6.0pt" text-align="start"&gt;DATE:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="start"&gt;
								&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt; 
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="start"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;Phone: </xsl:text><xsl:value-of select="//SolicitorsPackage/SolicitorPhone"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;Fax: </xsl:text><xsl:value-of select="//SolicitorsPackage/SolicitorFax"/>
<xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;
				&lt;fo:leader leader-pattern="rule" rule-thickness="2pt" color="black" line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="212.4pt"/&gt;
				&lt;fo:table-column column-width="333.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;LENDER REFERENCE NUMBER:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/MortgageNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;MORTGAGOR(S):&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;MUNICIPAL ADDRESS:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
<xsl:text>&lt;/fo:block&gt;</xsl:text>

					<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
						<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
<xsl:text>&lt;/fo:block&gt;
						</xsl:text>
					</xsl:if>
					
					<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/><xsl:text> </xsl:text><xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/><xsl:text>&lt;/fo:block&gt;

						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;MORTGAGE AMOUNT:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/LoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;CLOSING DATE:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/AdvanceDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;
				&lt;fo:leader leader-pattern="rule" rule-thickness="2pt" color="black"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;I/We request funds for closing on &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt" leader-length="100pt"/&gt;.&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;I/We acknowledge that there are no amendments and/or schedules to the Agreement on Purchase and Sale which affect the purchase price or deposit amount as set out in your Mortgage Commitment Letter.&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;I/We confirm that:&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			
			&lt;fo:list-block space-before="6pt" space-after="6pt" font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;All terms and conditions set out in your instructions have been satisfied&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;All CMHC / GEMICO terms and conditions have been satisfied&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;All documents required prior to release of the mortgage proceeds have been forwarded with the Solicitor's Request for Funds for </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text> to review.  Written authorization from </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text> will be required in order to release the mortgage proceeds.&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
			&lt;/fo:list-block&gt;
						
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;

			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;DELIVERY OF FUNDS IS REQUESTED AS FOLLOWS:&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			
			&lt;fo:list-block space-before="6pt" space-after="6pt" font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Deposit funds to my/our Trust Account # &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt" leader-length="200pt"/&gt;at&lt;/fo:block&gt; 
						&lt;fo:block&gt;Bank Name &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
						&lt;fo:block&gt;Route and Transit &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Deliver funds courier collect via &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt" leader-length="100pt"/&gt; courier account #&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt" leader-length="100pt"/&gt;.&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;*Wire funds to:&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
						&lt;fo:block&gt;&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
						&lt;fo:block&gt;&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
			&lt;/fo:list-block&gt;
			
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;* If this last option is chosen, I/We understand that you will retain sufficient funds to cover all wire costs.&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" margin-left="252.0pt" text-align="end"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" margin-left="252.0pt" text-align="end"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" margin-left="252.0pt" text-align="end"&gt;&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" margin-left="252.0pt" text-align="end"&gt;Signature of Solicitor&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="start"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;

			&lt;fo:block break-before="page" font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;DOCUMENTS REQUIRED PRIOR TO RELEASE OF THE MORTGAGE PROCEEDS:&lt;/fo:block&gt;
			
			&lt;fo:block&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			
			&lt;fo:list-block space-before="6pt" space-after="6pt" font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Pre-Authorized Chequing (PAC) form together with "VOID" cheque &lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Signed Statement of Disclosure&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Signed Statutory Declaration&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Well Water Test/Certificate, indicating that the water is potable and fit for human consumption&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Septic Tank Certificate&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;New Home Warranty Certificate of Possession&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;New Home Warranty Builder's Registration Number and Unit Enrollment Number&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Assignment of Condominium Voting Rights&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Assignment of Proceeds&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Signed Authorization to Send Assessment and Tax Bill (#45-06-20) PEI&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Other (specify) &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" line-height="10pt"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
			&lt;/fo:list-block&gt;			
		&lt;/fo:flow&gt;
	&lt;/fo:page-sequence&gt;	
	</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishFinalReport">
	<xsl:text>
	&lt;fo:page-sequence master-reference="main"&gt;
		&lt;fo:static-content flow-name="xsl-region-after"&gt;
			&lt;fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="right"&gt;Page &lt;fo:page-number/&gt;
 of &lt;fo:page-number-citation ref-id="endofdoc"/&gt;
			&lt;/fo:block&gt;
		&lt;/fo:static-content&gt;
		&lt;fo:flow flow-name="xsl-region-body"&gt;
			&lt;fo:block&gt;
				&lt;fo:table table-layout="fixed"&gt;
					&lt;fo:table-column column-width="555pt"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="14.0pt" font-weight="bold" space-after="12.0pt" space-before="12.0pt" text-align="center" text-decoration="underline"&gt;SOLICITOR'S FINAL REPORT ON TITLE&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
					&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="36.0pt"/&gt;
				&lt;fo:table-column column-width="377.0pt"/&gt;
				&lt;fo:table-column column-width="140.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Solicitor's Reference No.&lt;fo:inline font-weight="normal"/&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="50%"/&gt;&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica"&gt;TO:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" text-align="justify"&gt;</xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" text-align="justify"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/><xsl:text>&lt;/fo:block&gt;
							</xsl:text>
							<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" text-align="justify"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
							</xsl:if>
							<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" text-align="justify"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/City"/><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/><xsl:text>  </xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica"&gt;
								&lt;fo:leader line-height="12pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica"&gt;Attention: Joe Brancati&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica" text-align="justify" text-indent="36.0pt"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;In accordance with your instructions, we have acted as your solicitors in the following transaction. We have registered a Mortgage/Charge (or Deed of Loan, if applicable) the "Mortgage" on the appropriate form in the appropriate Land Registry Office and make our final report as follows:&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block space-after="6pt" font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;Mortgagor(s) &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
			&lt;fo:block space-after="6pt" font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;Guarantor &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
			&lt;fo:block space-after="6pt" font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;Municipal Address of Property &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
			&lt;fo:block space-after="6pt" font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;Legal Address of Property &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="41.0pt"/&gt;
				&lt;fo:table-column column-width="27.0pt"/&gt;
				&lt;fo:table-column column-width="245.5pt"/&gt;
				&lt;fo:table-column column-width="235.5pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="4" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="justify"&gt;It is our opinion that:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;(1)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="3" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="12.0pt" text-align="justify"&gt;A valid and legally binding </xsl:text><xsl:value-of select="//SolicitorsPackage/LPDescription"/><xsl:text> in favour of </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text>, for the full amount of the monies advanced was registered on &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="100pt"/&gt; in the </xsl:text><xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/><xsl:text> Division of &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="100pt"/&gt; as Instrument No. &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="100pt"/&gt;.&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;(2)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="3" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;The Mortgagor(s) have good and marketable first charge in fee simple to the property free and clear of any prior encumbrances, other than the minor defects listed below which do not affect the priority of the Mortgage or the marketability of the property. All lien holdback/retention period requirements have been met. Easements, Encroachments and Restrictions etc. are listed below:&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="9.0pt" text-align="justify"&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
					&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="4" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="justify"&gt;Condominium/strata unit(s) if applicable:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" number-rows-spanned="3" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" text-align="justify"&gt;
								&lt;fo:leader line-height="12pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;2a)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;We confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. We have reviewed the Condominium Corporation's Declaration and Bylaws and confirm they contain nothing derogatory to your security. We have assigned the voting rights to </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text>, if applicable.&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;b)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;All necessary steps have been taken to confirm your right to vote should you wish to do so.&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;c)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="12.0pt" text-align="justify"&gt;Applicable notice provisions, if any: &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;(3)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="3" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="12.0pt" text-align="justify"&gt;All restrictions have been complied with in full and there are no work orders or deficiency notices outstanding against the property.&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;(4)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="3" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="12.0pt" text-align="justify"&gt;All taxes and levies due and payable on the property to the Municipality have been paid up to &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="75pt"/&gt;.&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;(5)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="3" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="12.0pt" text-align="justify"&gt;The Mortgage does not contravene the provisions of the Planning Act as amended from time to time, because the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;(6)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="3" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="12.0pt" text-align="justify"&gt;A true copy of the Mortgage (including Standard Charge Terms, all Schedules to it, and the Mortgagor(s) Acknowledgement and Direction, if applicable) has been given to each Mortgagor.&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" number-rows-spanned="2" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;(7)&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="3" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="justify"&gt;Fire Insurance coverage has been arranged in accordance with your instructions for full replacement cost(s) with loss payable to </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text> in accordance with the I.B.C. standard mortgage clause.&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="justify"&gt;Insurance Company: &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="justify"&gt;Broker: &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="justify"&gt;Amount of Insurance: &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="justify"&gt;Policy No: &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
								&lt;fo:leader line-height="10pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="justify"&gt;Effective Date: &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="2.85pt" padding-right="2.85pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-after="6.0pt" text-align="justify"&gt;Expiry Date: &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="justify"&gt;The following documents are enclosed for your file:&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
				&lt;fo:leader line-height="10pt"/&gt;
			&lt;/fo:block&gt;

			&lt;fo:list-block space-before="6pt" space-after="6pt" font-family="Arial, Helvetica" font-size="10.0pt" text-align="justify"&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Duplicate registered copy of the Charge/Mortgage, or Deed of Loan including Standard Charge Terms, all Schedules to it and acknowledgement of receipt of Mortgagor(s) and </xsl:text><xsl:value-of select="//SolicitorsPackage/GuarantorClause"/><xsl:text>(s), if any&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Electronic Charge and Acknowledgement and Direction (&lt;fo:inline font-weight="bold"&gt;Ontario electronic registration counties only&lt;/fo:inline&gt;)&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Instrument Number (Teranet Registration)&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Guarantee Agreement (&lt;fo:inline font-weight="bold"&gt;Ontario electronic registration counties only&lt;/fo:inline&gt;)&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Certificate of Title (or Provincial Comparable)&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Survey or Surveyor's Certificate or Title Insurance in lieu of a Survey, if applicable&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Title Insurance Policy and Schedules A and B to Policy&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Registered Amendment Agreement&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Execution Certificate&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Municipal Tax Certificate&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Certificate of Completion and Possession&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Final Inspection&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;New Home Warranty Certificate of Possession&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Occupancy Certificate/Permit&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Personal Guarantee and Letter of Independent Legal Advice&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Statement of Disclosure&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Certified copy of directors' resolution and the borrowing by-law of the corporation authorizing the Charge/Mortgage&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Sheriff's Certificate/GR Search&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Signed Statutory Declaration&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Fire Insurance Policy&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Condominium Corporation Insurance Binder&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Assignment of Site/Lot Lease to </xsl:text><xsl:value-of select="//SolicitorsPackage/LenderName"/><xsl:text>&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Zoning Certificate/Memorandum (Ontario properties only)&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Declaration as to Possession (Manitoba properties only)&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Registered Assignment of Rents&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;General Security Agreement (GSA) Registered under PPSA&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Statement of Funds Received and Disbursed&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Verification of Payout of Debts&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;Other (specify)  &lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
				&lt;fo:list-item&gt;
					&lt;fo:list-item-label end-indent="label-end()"&gt;
						&lt;fo:block&gt;[    ]&lt;/fo:block&gt;
					&lt;/fo:list-item-label&gt;
					&lt;fo:list-item-body start-indent="body-start()"&gt;
						&lt;fo:block&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
					&lt;/fo:list-item-body&gt;
				&lt;/fo:list-item&gt;
			&lt;/fo:list-block&gt;
			
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="end"&gt;
				&lt;fo:leader line-height="20pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="end"&gt;&lt;fo:leader line-height="9pt" leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="50%"/&gt;&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" text-align="end"&gt;Signature of Solicitor&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" text-align="end"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;	
			&lt;fo:block id="endofdoc"/&gt;
		&lt;/fo:flow&gt;
	&lt;/fo:page-sequence&gt;	
	</xsl:text>
	</xsl:template>

	
	<!-- ************************************************************************ 	-->
	<!-- French template section                                             			-->
	<!-- ************************************************************************ 	-->
	<!--	<xsl:template name="FrenchTemplate">
	</xsl:template>-->
	<!-- ************************************************************************ 	-->
	<!-- fo file start and fo file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="FOStart">
		<xsl:text>
&lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;
&lt;fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://xml.apache.org/fop/extensions"&gt;
	&lt;fo:layout-master-set&gt;
		&lt;fo:simple-page-master master-name="main" page-height="11in" page-width="8.5in" margin-top="1cm" margin-bottom="1cm" margin-left="1cm" margin-right="1cm"&gt;
			&lt;fo:region-body margin-bottom="10mm" margin-top="5mm"/&gt;
			&lt;fo:region-before extent="10mm"/&gt;
			&lt;fo:region-after extent="10mm"/&gt;
		&lt;/fo:simple-page-master&gt;
	&lt;/fo:layout-master-set&gt;
		</xsl:text>
	</xsl:template>
	<xsl:template name="FOEnd">
		<xsl:text>&lt;/fo:root&gt;</xsl:text>
	</xsl:template>
</xsl:stylesheet>