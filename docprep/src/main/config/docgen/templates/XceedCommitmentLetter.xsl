<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
  12/Jul/2006 DVG #DG460 #3874  Xceed Solicitors' Package  Borrower Name Template change  
	09/Sep/2005 DVG #DG312 #1965  Xeed Solicitor Package Middle Name initial is not shown  
	21/Mar/2005 DVG #DG170 #1106  Xceed - Commitment letter/solicitors package commitment letter
	07/Dec/2004 DVG #DG112 	scr#787 Xceed - Template change  
	-->
	<xsl:output method="text"/>
	
	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageEnglish">
				<xsl:call-template name="EnglishHeader"/>
				<xsl:call-template name="EnglishFooter"/>
				<xsl:call-template name="EnglishPage1"/>
				<xsl:call-template name="EnglishPage2Start"/>
				<xsl:call-template name="EnglishPage2"/>
				<!-- #DG312 xsl:call-template name="EnglishPage3Start"/-->
				<xsl:call-template name="EnglishPage3"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageFrench">
				<xsl:call-template name="FrenchHeader"/>
				<xsl:call-template name="FrenchFooter"/>
				<xsl:call-template name="FrenchPage1"/>
				<xsl:call-template name="FrenchPage2Start"/>
				<xsl:call-template name="FrenchPage2"/>
				<xsl:call-template name="FrenchPage3Start"/>
				<xsl:call-template name="FrenchPage3"/>
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishPage1">
		<xsl:text>
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \s4\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel3\adjustright\itap0 
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6030 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx11340
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:call-template name="XceedLogo"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6030 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr
\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:if test="//CommitmentLetter/BranchAddress/Phone">
			<xsl:text>Telephone:</xsl:text>
		</xsl:if>
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:if test="//CommitmentLetter/BranchAddress/Fax">
			<xsl:text>Fax:</xsl:text>
		</xsl:if>
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8730
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 TO:\cell </xsl:text>
		<!--#DG312 xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName"/>
		
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\b\fs20\ul\f1 Underwriter\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>\cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\b\fs20\ul Date\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchCurrentTime"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8730
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11340
\row 
}
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CareOf"/>
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="//BrokerFirmName"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAgentName"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line1"/>
		<xsl:if test="//CommitmentLetter/BrokerAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BrokerAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/City"/>
		<xsl:if test="//CommitmentLetter/BrokerAddress/City">
			<xsl:text>{ }</xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Province"/>
		<xsl:text>{  }</xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/PostalCode"/>
		<xsl:if test="//CommitmentLetter/BrokerEmail">
			<xsl:text>\par </xsl:text>
			<xsl:value-of select="//CommitmentLetter/BrokerEmail"/>
		</xsl:if>
		<xsl:text>\cell }
{\b\f1\ul\fs20 Broker Reference Number:\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmgf\clbrdrt
\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trrh230\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell \cell }
{\b\f1\ul\fs20 Lender Reference Number:\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh230\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell Telephone:\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Phone"/>
		<xsl:text>\cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\fs20 \cell }
\pard\plain \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell Fax:\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Fax"/>
		<xsl:text>\cell \cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\fs20 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s15\ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\i\f1\fs28 We are pleased to confirm that your application for a Mortgage Loan has been approved under the following terms and conditions:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5386 \cellx11340
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\b\fs20\ul Mortgagor(s):\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<!--#DG312 xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<xsl:call-template name="BorrowerFullName"/>
		
		<xsl:text>\cell }{\b\f1\fs20\ul Security Address:\par }</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Properties/Property">
			<xsl:text>{\f1\fs20 </xsl:text>
			<xsl:value-of select="./AddressLine1"/>
			<xsl:text>\par </xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>  </xsl:text>
			<xsl:value-of select="./PostalCode"/>
			<xsl:text>}</xsl:text>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5386 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20\ul </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>:\cell }
{\f1\fs20 </xsl:text>
    <!--#DG460 oooops, should have been done since #DG312 ...
    xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="borType" select="1"/>
    </xsl:call-template>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 LOAN TYPE: }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Product"/>
		<xsl:text>\tab\tab }
{\b\f1\fs22 LTV: }{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 COMMITMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 FIRST PAYMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 INTEREST ADJUSTMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/IADDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 CLOSING DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 MATURITY DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 INTEREST ADJUSTMENT AMOUNT:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/IADAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22\ul LOAN\cell }
\pard\plain \s5\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel4\adjustright 
{\b\f1\fs22\ul TERMS\cell }
\pard\plain \s6\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\f1\fs22\ul INSTALLMENT - </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentFrequency"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 

\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl
\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt
\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Amount:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f1\fs20 Interest Rate: \cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/InterestRate"/>
		<xsl:text>\cell }
{\b\f1\fs20 Principal and Interest:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PandIPayment"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Insurance:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs20 Term:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentTerm"/>
		<xsl:text>\cell }
{\b\f1\fs20 Taxes (estimated):\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TaxPortion"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Admin Fee:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdminFee"/>
		<xsl:text>\cell }
{\b\f1\fs20 Amortization:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Amortization"/>
		<xsl:text>\cell }
{\b\f1\fs20 Total Installment:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalPayment"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Total Loan:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell \cell \cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s18\ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\b Please be advised the solicitor acting on behalf of the transaction, will be:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Solicitor Name:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Address:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Phone / Fax:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\pard\plain \s18\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl
\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb
\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> after which time if not accepted, shall be considered null and void.\par }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/ClosingText/Line">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s20\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishPage2Start">
		<xsl:text>{\sect } \linex0\headery706\footery260\endnhere\sectdefaultcl</xsl:text>
	</xsl:template>
	
	<xsl:template name="EnglishPage2">
		<xsl:text>\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \ql \sb120\sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul TERMS AND CONDITIONS:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING:\par }
{\b\f1\fs18 The following conditions must be met, and the requested documents must be received in form and content satisfactory to </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
no later than ten (10) business days prior to the advance of the mortgage. Failure to do so will delay current closing and/or void this commitment.\par }{\f1\fs18 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/Conditions/Condition">
			<xsl:value-of select="position()"/>
			<xsl:text>.  </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\caps\f1\fs18\ul Standard Conditions:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 1.\cell </xsl:text>

		<!-- #DG112 -->
		<xsl:text>Receipt of a void cheque for mortgage payment account.</xsl:text>
		<!--xsl:value-of select="//CommitmentLetter/VoidChq"/-->
		<!-- #DG112 end -->
		
		<xsl:text> \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 2.\cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/TitleInsurance/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 3.\cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/FireInsurance/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 4.\cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/TaxesPaid/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- #DG112 added the 5th item below-->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}

<!-- #DG170 commented out
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 5.\cell All Xceed Mortgages are closed to prepayment in full except in the case of a proven bona fide sale and payment of the applicable penalty.
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}-->
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0

</xsl:text>
	</xsl:template>

	<!-- #DG312 not needed since footer is already defined earlier
  xsl:template name="EnglishPage3Start">
		<xsl:text>{\sect }\sectd \linex0\headery706\footery260\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Page {\field{\*\fldinst {\cs22  PAGE }}
{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}{\f1\fs20  of }{\field{\*\fldinst 
{\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
	</xsl:template-->

	<xsl:template name="EnglishPage3">
		<xsl:text>\trowd\trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth11440\cellx11350
\pard\plain\s17\qj\sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\caps\f1\fs18\ul Rate Guarantee and Rate Adjustment Policies:\par }
{\f1\fs18 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par}

{\f1\fs18\b\ul\caps Payment Policy:\par }
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:for-each select="//CommitmentLetter/PrivilegePayment/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="//CommitmentLetter/InterestCalc/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>
\par }
\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18 Closing:\par }
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:for-each select="//CommitmentLetter/Closing/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par }
\pard \s17\qj \sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18 ACCEPTANCE:\par }
\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:for-each select="//CommitmentLetter/Acceptance/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par \par }
\pard \s17\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>\par \par }
\pard\plain \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 Authorized by: __________</xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>_____________\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/Signature/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par }
{\f1 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 APPLICANT\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 CO-APPLICANT\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\pard <!-- \ql \widctlpar\tx9644\aspalpha\aspnum\faauto\adjustright\itap0 -->
		</xsl:text>
	</xsl:template>
	<xsl:template name="XceedLogo">
		<xsl:text>{\pict\wmetafile8\picw6879\pich2170\picwgoal3900\pichgoal1230 010009000003f42b00000000c62b00000000050000000b0200000000050000000c0252000401050000000b0200000000050000000c025200040105000000090200000000050000000102ffffff000400000007010300c62b0000430f2000cc0000005200040100000000520004010000000028000000040100005200000001
000800000000004853000000000000000000000000000000000000000000000101010002020200030303000404040005050500060606000707070008080800090909000a0a0a000b0b0b000c0c0c000d0d0d000e0e0e000f0f0f00101010001111110012121200131313001414140015151500161616001717170018181800
191919001a1a1a001b1b1b001c1c1c001d1d1d001e1e1e001f1f1f00202020002121210022222200232323002424240025252500262626002727270028282800292929002a2a2a002b2b2b002c2c2c002d2d2d002e2e2e002f2f2f003030300031313100323232003333330034343400353535003636360037373700383838
00393939003a3a3a003b3b3b003c3c3c003d3d3d003e3e3e003f3f3f00404040004141410042424200434343004444440045454500464646004747470048484800494949004a4a4a004b4b4b004c4c4c004d4d4d004e4e4e004f4f4f0050505000515151005252520053535300545454005555550056565600575757005858
5800595959005a5a5a005b5b5b005c5c5c005d5d5d005e5e5e005f5f5f00606060006161610062626200636363006464640065656500666666006767670068686800696969006a6a6a006b6b6b006c6c6c006d6d6d006e6e6e006f6f6f00707070007171710072727200737373007474740075757500767676007777770078
787800797979007a7a7a007b7b7b007c7c7c007d7d7d007e7e7e007f7f7f00808080008181810082828200838383008484840085858500868686008787870088888800898989008a8a8a008b8b8b008c8c8c008d8d8d008e8e8e008f8f8f009090900091919100929292009393930094949400959595009696960097979700
98989800999999009a9a9a009b9b9b009c9c9c009d9d9d009e9e9e009f9f9f00a0a0a000a1a1a100a2a2a200a3a3a300a4a4a400a5a5a500a6a6a600a7a7a700a8a8a800a9a9a900aaaaaa00ababab00acacac00adadad00aeaeae00afafaf00b0b0b000b1b1b100b2b2b200b3b3b300b4b4b400b5b5b500b6b6b600b7b7b7
00b8b8b800b9b9b900bababa00bbbbbb00bcbcbc00bdbdbd00bebebe00bfbfbf00c0c0c000c1c1c100c2c2c200c3c3c300c4c4c400c5c5c500c6c6c600c7c7c700c8c8c800c9c9c900cacaca00cbcbcb00cccccc00cdcdcd00cecece00cfcfcf00d0d0d000d1d1d100d2d2d200d3d3d300d4d4d400d5d5d500d6d6d600d7d7
d700d8d8d800d9d9d900dadada00dbdbdb00dcdcdc00dddddd00dedede00dfdfdf00e0e0e000e1e1e100e2e2e200e3e3e300e4e4e400e5e5e500e6e6e600e7e7e700e8e8e800e9e9e900eaeaea00ebebeb00ececec00ededed00eeeeee00efefef00f0f0f000f1f1f100f2f2f200f3f3f300f4f4f400f5f5f500f6f6f600f7
f7f700f8f8f800f9f9f900fafafa00fbfbfb00fcfcfc00fdfdfd00fefefe00ffffff00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ebfdfffffffffffffffffffffffffffffffffffffae9f7fffffffffffffffffffffffff5eafcfffffffffffffffffffffffffffffceaf7ff
fffffffff1f0fffffffffffffffffffffffffffffffffffffceaf6fffffffffffffffffffffffffffffffffffffffffffffffffffffffff7eafcffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb682ffffffff2ffffff57832413db5ffffa478fffa49adffffa478fffffffe943145316effa96fffffff9d89fff46e3243329dffff37363737e7ffffffff
ffa037433261faffdb56373a55dbffff37e5ffb345f7c06dffffffffffb23843327cfaffff44ffffac5e9286ffffff9a8effff37efffffff44fffffc8232433cb7ffffb180ffffff7e5bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb682ffffffff2fffff7e6ff3ffcf37c2ffa478ff9f65feffffa478ffffffa455ebfff73cfff739eaffff4fe5ff667efaffba73ffff37efff
ffffffffffffa448dffff9aef8f13aa4fdfda439f0ff37e5ef39ccffc06dffffffffce40dcfff66c80ffff44ffe936d8ea38f4fffb3fe0ffff37efffffff44ffff8a66f4ffd436c4ffb180ffffae355cffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb682ffddf8ff2ffff938f5ffffffa461ffa478d93fe7ffffffa478ffffff41ceffda803cffff8c2c372f64ffdd32f7ffb566
73ffff37efffffffffffffff40c8ffffffffffad66ffffffff65adff37e17686ffffc067edf8ffff6ea7ffffffea2bf2ff44f45f8dffff752c372d61ffffff37efffffff44fff82de5ffffffa165ffb180ffda45b35bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb682da3d66fa2fffeb45ffffffffc64bffa43d584ddeffffffa478ffffff28ebffeab8c0ffffea4efe61bdff
c54cffffd6b8d1ffff3750545bffffffffff28e9ffffffffff9988ffffffff879aff37584393ffffc039634b94ff56c5fffffffe31deff395d3f99ffffd944fa4cc9ffffff37efffffff44ffe82bfdffffffc34dffb180e145cec85bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb66d40c57c842ffffe41d9ffffff7c7bffa478ff9e83ffffffa478ffffff5aaeffffffffffff
ff729743fdffef2fdfffffffffffff37efffffffffffffff59a2ffffffffffc446f9fffff845c4ff37e5f82debffc06dfff93bff8787ffffffca39fbff44fff42af4ffff678657ffffffff37efffffff44fffd41c4ffffff797effb16f45bbffc85cffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb625aafffb5a2fffffb23cafcd813fe8ffa466cf6a9affd8c88463c8cdffd43c
a3d0bc8affffffc226a0ffffffa347b8d0ab9fffff37bdc8c8fcffffffffd33693cebb76fffd685bc1c45a68fdff37bcb232f7ffc05bc89d52ffee4390ceb33fb9ffff41d1a934fbffffc625b7ffebc8c832bdc8e8ff44ffffc03eafcf873feaffb1259bffffc85bffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5a4ffffffe989ffffffcd83749befffffcb8283a3faffa68282
82828cffffdd8b737fbefffffffc8bf4ffffffffc67e738ad5ffff8c828282f9ffffffffffe593747fb4fffffcb27979b1fcffff8c828cd7ffffdb828292e4ffffeb947481cfffffff8b828edafffffffe93fcffd28282828282cbff94ffffffd4827397f0ffffd2a2ffffffdfa1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5dbcec9d4e8fdffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff965b5b5b5b5b5b5bb5ffffffffffffffffffffffffdc5c5b5b5b5b5b5b5cdaffffffffffffffcc7f482725252525252d548ed5ffffffffffffff
ffc15b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5bc8ffffffffff5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b5b84ffffffffff8a5b5b5b5b5b5b5b5b5b5b5b616d81a3d6fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e494949494956ffffffffffffffffffffffffffff
ffffffffff95494949494950ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffec462525252525252cc7ffffffffffffffffffffe64025252525252526a1fffffffffffcae4d2525252525252525252525252545
94edffffffffffad2525252525252525252525252525252525b6ffffffffff25252525252525252525252525252525255bffffffffff64252525252525252525252525252525253a8cecffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff78252525252534ffffffffffffffff
ffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe43925252525252531ddfffffffffffffffff64b2525252525252596ffffffffffe85425252525252525252525
2525252525252531ecffffffffad2525252525252525252525252525252525b6ffffffffff25252525252525252525252525252525255cffffffffff6425252525252525252525252525252525252530a9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff78252525252535ffff
ffffffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcf2c25252525252546f3ffffffffffffff7f252525252525257bfeffffffffea462525252525
2525252525252525252525252525e5ffffffffad2525252525252525252525252525252525b6ffffffffff25252525252525252525252525252525255bffffffffff64252525252525252525252525252525252525252578fbffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff782525
25252534ffffffffffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbe292525252525256cfdffffffffffa72525252525252567fbffffffffed4a
2525252525252525252525252525252525252525e5ffffffffad2525252525252525252525252525252525b6ffffffffff25252525252525252525252525252525255bffffffffff6425252525252525252525252525252525252525252574feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff78252525252535ffffffffffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c2525252525252592ffffffffbf272525252525254bf4ff
fffffffe6325252525252525255ea5cbdad6bf9a662b25252525e5ffffffffad252525252525dcf5f5f5f5f5f5f5f5f5f5fbffffffffff2525252525258df5f5f5f5f5f5f5f5f5f5f8ffffffffff6425252525252ba4a4a4a4a397753a252525252525252597ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff78252525252535ffffffffffffffffffffffffffffffffffffff8025252525252efffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe6b25252525252526baffffdd3125252525
25253ce6ffffffffffb82525252525252545cbffffffffffffffffee983a2525e5ffffffffad252525252525e5ffffffffffffffffffffffffffffffff25252525252592ffffffffffffffffffffffffffffffff6425252525252ffffffffffffffffba52e25252525252529dbffffffffffffffffffffffffffffffffffff
fffffffffffffffffdb2adadadadad5825252525252fadadadadadade5ffffffffffffffffffffffff8025252525252efffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff55125252525252530daf3
452525252525252ed5fffffffffffe4d2525252525254af1fffffffffffffffffffffff7982de5ffffffffad252525252525e5ffffffffffffffffffffffffffffffff25252525252592ffffffffffffffffffffffffffffffff6425252525252fffffffffffffffffffd6382525252525256affffffffffffffffffffffff
ffffffffffffffffffffffffffffffad25252525252525252525252525252525256dfbffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeb432525
252525253f6425252525252527baffffffffffffd12525252525252bdaffffffffffffffffffffffffffffdff2ffffffffad252525252525e5ffffffffffffffffffffffffffffffff25252525252592ffffffffffffffffffffffffffffffff6425252525252fffffffffffffffffffffd32b252525252526ddffffffffff
ffffffffffffffffffffffffffffffffffffffffffffad2a25252525252525252525252525257ffbffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffd62c25252525252525252525252525a4ffffffffffffff9325252525252580ffffffffffffffffffffffffffffffffffffffffffad252525252525e5ffffffffffffffffffffffffffffffff25252525252592ffffffffffffffffffffffffffffffff6425252525252fffffffffffffffffffffff7c252525252525
91ffffffffffffffffffffffffffffffffffffffffffffffffffffffffbe2b2525252525252525252525258effffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffb426252525252525252525252589ffffffffffffffff6a252525252525cfffffffffffffffffffffffffffffffffffffffffffad252525252525aec0c0c0c0c0c0c0c0c0c0edffffffffff25252525252572c0c0c0c0c0c0c0c0c0c0d0ffffffffff6425252525252fffffffffffffffffffffffc6
2525252525255effffffffffffffffffffffffffffffffffffffffffffffffffffffffffbe2a252525252525252525257fffffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff93252525252525252525256efdffffffffffffffff4f25252525252cfbffffffffffffffffffffffffffffffffffffffffffad2525252525252525252525252525252525c0ffffffffff25252525252525252525252525252525255bffffffffff6425252525252fffffffffffff
fffffffffff52625252525253bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffad2525252525252525256dfbffffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffe8125252525252525255ef9ffffffffffffffffff3e252525252544ffffffffffffffffffffffffffffffffffffffffffffad2525252525252525252525252525252525c0ffffffffff25252525252525252525252525252525255cffffffffff6425252525252f
ffffffffffffffffffffffff3b252525252528ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffad2a2525252525257ffbffffffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc9252525252525252599ffffffffffffffffffff3c252525252548ffffffffffffffffffffffffffffffffffffffffffffad2525252525252525252525252525252525c0ffffffffff25252525252525252525252525252525255bffffffffff64
25252525252fffffffffffffffffffffffff40252525252525ffffffffffffffffffffffffffffffffffffffb3f4ffffffffffffffffffffffbe2a252525257fffffffffffffffffffffffff96ffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe13525252525252525252ac9ffffffffffffffffff46252525252538ffffffffffffffffffffffffffffffffffffffffffffad2525252525252525252525252525252525c0ffffffffff25252525252525252525252525252525255c
ffffffffff6425252525252ffffffffffffffffffffffffe2f25252525252fffffffffffffffffffffffffffffffffffffff8258f4ffffffffffffffffffffffbe2a25256afbffffffffffffffffffffff9625ffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe7422525252525252525252531ceffffffffffffffff5d252525252525e6ffffffffffffffffffffffffffffffffffffffffffad2525252525257f8b8b8b8b8b8b8b8b8b8bddffffffffff252525252525588b8b8b8b8b
8b8b8b8b8ba8ffffffffff6425252525252fffffffffffffffffffffffdf2525252525254bffffffffffffffffffffffffffffffffffffff822550ecffffffffffffffffffffffb82a77faffffffffffffffffffffff952525ffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff44a25252525252525252525252532daffffffffffffff832525252525259dffffffffffffffffffffffffffffffffffffffffffad252525252525e5ffffffffffffffffffffffffffffffff252525252525
92ffffffffffffffffffffffffffffffff6425252525252fffffffffffffffffffffff9b2525252525257cffffffffffffffffffffffffffffffffffffff82252550f4ffffffffffffffffffffffdafeffffffffffffffffffffff96252525ffffffffffff8025252525252effffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7625252525252525322525252525253febffffffffffffb52525252525253df7ffffffffffffffffffffffffffffffffffffffffad252525252525e5ffffffffffffffffffffffffffffffff
25252525252592ffffffffffffffffffffffffffffffff6425252525252ffffffffffffffffffffff942252525252525bbffffffffffffffffffffffffffffffffffffff8225252558f4ffffffffffffffffffffffffffffffffffffffffffff9425252525ffffffffffff8025252525252effffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa62525252525252589c02625252525252561fcfffffffffff12b2525252525258bffffffffffffffffffffffffffffceffffffffffad252525252525e5ffffffffffffffffffff
ffffffffffff25252525252592ffffffffffffffffffffffffffffffff6425252525252ffffffffffffffffffffe7d25252525252539f9ffffffffffffffffffffffffffffffffffffff822525252558f4ffffffffffffffffffffffffffffffffffffffff8e2525252525ffffffffffff8025252525252effffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb62825252525252571fdff9c2525252525252577fdffffffffff862525252525252687f4fffffffffffffffffffcc2643cffffffffffad252525252525e5ffffffff
ffffffffffffffffffffffff25252525252592ffffffffffffffffffffffffffffffff6425252525252fffffffffffffffffea6d25252525252525a8fffffffffffffffffffffffffff0aaaaaaaaaaaa5e252525252550ecffffffffffffffffffffffffffffffffffff8f252525252525b1b1b1b1b1b15f25252525252eff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcf2925252525252559faffffff832525252525252581fffffffffff33e252525252525253797e1fffffffff8cf8e3d25253cffffffffffad2525252525
25e5ffffffffffffffffffffffffffffffff25252525252592ffffffffffffffffffffffffffffffff6425252525252eededededede2c083312525252525252561feffffffffffffffffffffffffffda2525252525252525252525252551f4ffffffffffffffffffffffffffffffff8e252525252525252525252525252525
252525252efffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0422525252525253aebfffffffffb5e25252525252525aaffffffffffc8282525252525252525253246493f2825252525253cffffffffff
ad2525252525256e78787878787878787878d1ffffffffff2525252525254e787878787878787878789affffffffff642525252525252525252525252525252525252525253cecffffffffffffffffffffffffffffda252525252525252525252525252558f4ffffffffffffffffffffffffffff8e25252525252525252525
252525252525252525252efffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff4562525252525252ac9fffffffffffff1482525252525252cc4ffffffffffb92c252525252525252525252525252525252525
3cffffffffffad2525252525252525252525252525252525b6ffffffffff25252525252525252525252525252525255bffffffffff6425252525252525252525252525252525252525253dd9ffffffffffffffffffffffffffffffda25252525252525252525252525252551f1ffffffffffffffffffffffff842525252525
252525252525252525252525252525252efffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb6225252525252525abffffffffffffffffda2e2525252525252dd0ffffffffffc6332525252525252525252525
2525252525253cffffffffffad2525252525252525252525252525252525b6ffffffffff25252525252525252525252525252525255bffffffffff642525252525252525252525252525252525252561edffffffffffffffffffffffffffffffffda25252525252525252525252525252585feffffffffffffffffffffffff
be2a25252525252525252525252525252525252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff802525252525252582ffffffffffffffffffffb32525252525252538e2ffffffffffe76e26252525
25252525252525252525252569ffffffffffad2525252525252525252525252525252525b6ffffffffff25252525252525252525252525252525255bffffffffff6425252525252525252525252525252525253db0feffffffffffffffffffffffffffffffffffda252525252525252525252525252585feffffffffffffff
ffffffffffffffbe2a252525252525252525252525252525252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb0252525252525255cfbffffffffffffffffffffff8c2525252525252546f1ffffffff
ffffd7752e252525252525252525253b7fd1ffffffffffffad2525252525252525252525252525252525b6ffffffffff25252525252525252525252525252525255bffffffffff6425252525252525252525252525253d72b3f8ffffffffffffffffffffffffffffffffffffffda2525252525252525252525252577feffff
ffffffffffffffffffffffffffffbe2a2525252525252525252525252525252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd3c9c9c9c9c9c9c9f2fffffffffffffffffffffffffdcbc9c9c9c9c9c9
c9e9fffffffffffffffff8cfa78b7b73758195b1ddfeffffffffffffffffedd0d0d0d0d0d0d0d0d0d0d0d0d0d0d0d0d0efffffffffffd0d0d0d0d0d0d0d0d0d0d0d0d0d0d0d0d0dcffffffffffddd0d0d0d0d0d0d0d0d0d0d0d5e1f4fffffffffffffffffffffffffffffffffffffffffffffff7d0d0d0d0d0d06825252525
2577faffffffffffffffffffffffffffffffffffffb82a2525252525d0d0d0d0d0d06c25252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ff7a2525252585feffffffffffffffffffffffffffffffffffffffffbe2a25252525ffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffff7a25252585feffffffffffffffffffffffffffffffffffffffffffffbe2a252525ffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a252587fefffffffffffffffffffffda0f0ffffffffffffffffffffffc7312525ffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffff7a257efeffffffffffffffffffffff86254deeffffffffffffffffffffffc72a25ffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffff7a7dfdffffffffffffffffffffff9f2525254df0ffffffffffffffffffffffbe2affffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc2fdffffffffffffffffffffff9f25252525255df9ffffffffffffffffffffffbeffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe8e252525252525255df0ffffffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e2525252525252525254df0ffffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f25252525252525252525255df9ffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f252525252525252525252525255ff7ffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe8e25252525252525252525252525252557edffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff95252525252525252525252525252525252547edffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffedbdadadadada6a252525252532dadadadadadaefffffffffffffffffffffffff8025252525252effffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff78252525252535ffffffffffffffffffffffffffffffffffffff8025252525252effff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff78252525252535ffffffffffffffffffffffffffffffffffffff802525
2525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff78252525252535ffffffffffffffffffffffffffffffff
ffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff78252525252534ffffffffffffffffffff
ffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7825252525253affffffff
ffffffffffffffffffffffffffffff8025252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefa4a4a4a4a4a4a4a4a4a4a4a4a4a4a4a4a4a4a45525252525
2532a4a4a4a4a4a4a4a4a4a4a4a4a4a4a4a4a4a4a45a25252525252effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffda252525252525252525252525252525252525
2525252525252525252525252525252525252525252525252525252525252525252effffffffffffffffe0faffe9f1fcebffdcffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffda252525252525252525252525
2525252525252525252525252525252525252525252525252525252525252525252525252525252effffffffffffffff49e5ff80adb755ff2fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffda252525252525
2525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252effffffffffffffff49e5ff80ad5e5bdb2fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffda
2525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252effffffffffffffff49e5ff809058bf872fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffda2525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252effffffffffffffff49e5ff803fabf9482cffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffda2525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252525252effffffffffffc96b5e69a49f60f6ff9d5bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffcededededededededededededededededededededededededededededededededededededededededededededededededededededffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffff030000000000000000000000000000000000}
</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishHeader">
		<xsl:text>{\header \pard\plain \s20\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\b\i\f1\fs28 MORTGAGE COMMITMENT}}</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishFooter">
		<xsl:text>{\footer 
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone 
\clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3810 \cellx3720
\clbrdrt\brdrnone \clbrdrl\brdrnone 
\clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3810 \cellx7530
\clbrdrt\brdrnone \clbrdrl\brdrnone 
\clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3810 \cellx11340
\pard\plain \s21\qr \widctlpar\intbl
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright {\f1\fs20\cell }
\pard \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Page 
{\field{\*\fldinst {\cs22  PAGE }}
{\fldrslt {\cs22\lang1024\langfe1024\noproof 1}}}{\f1  of }
{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}\cell }
\pard \s21\qr \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Initials _____ Date _______________}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone 
\clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3810 \cellx3720
\clbrdrt\brdrnone \clbrdrl\brdrnone 
\clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3810 \cellx7530
\clbrdrt\brdrnone \clbrdrl\brdrnone 
\clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3810 \cellx11340\row }

\pard\plain \s21\ql \widctlpar
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\fs20\par }}</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="FrenchHeader">
		<xsl:text>{\header \pard\plain \s20\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\b\i\f1\fs28 LETTRE D’ENGAGEMENT}}</xsl:text>
	</xsl:template>

	<xsl:template name="FrenchFooter">
		<xsl:text>{\footer \trowd \trgaph108\trleft-90\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx3720\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb
\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx7530\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx11340\pard\plain \s21\qr \widctlpar\intbl
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright {\f1\fs20\cell }\pard \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright {\f1\fs20 Page 
{\field{\*\fldinst {\cs22  PAGE }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 1}}}{\f1  de }{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}\cell }\pard \s21\qr \widctlpar\intbl
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright {\f1\fs20 Initiales _____ Date _______________}{\cell }\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx3720\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb
\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx7530\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx11340\row }\pard\plain \s21\ql \widctlpar
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\fs20\par }}</xsl:text>
	</xsl:template>
	<xsl:template name="FrenchPage1">
		<xsl:text>
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \s4\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel3\adjustright\itap0 
\trowd \trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr
\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5671 \cellx5580\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5759 \cellx11339
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:call-template name="XceedLogo"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs24 \trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalt\clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5671 \cellx5580\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5759 \cellx11339\row }\trowd \trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5671 \cellx5580\clvertalt\clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1720 \cellx7300\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4039 
\cellx11339\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1 \cell }
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:if test="//CommitmentLetter/BranchAddress/Phone">
			<xsl:text>Téléphone:</xsl:text>
		</xsl:if>
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs24 \trowd 
\trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5671 \cellx5580\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1720 \cellx7300\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4039 \cellx11339\row }\trowd \trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl
\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5671 \cellx5580\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1720 \cellx7300\clvertalt\clbrdrt\brdrnone 
\clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4039 \cellx11339\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\f1 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:if test="//CommitmentLetter/BranchAddress/Fax">
			<xsl:text>Télécopieur:</xsl:text>
		</xsl:if>
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs24 \trowd \trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth5671 \cellx5580\clvertalt\clbrdrt
\brdrnone \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1720 \cellx7300\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4039 \cellx11339\row 
}\trowd \trgaph108\trleft-91\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3870 \cellx5580\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\f1\fs20 DESTINATAIRE:\cell </xsl:text>
		<!--#DG312 xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
    <!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName"/>

		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\b\fs20\ul\f1 Souscripteur\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>\cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\b\fs20\ul Date\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchCurrentTime"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs24 \trowd \trgaph108\trleft-91\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv
\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvertalt
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3870 \cellx5580\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\row }\trowd \trgaph108\trleft-91\trkeep\trbrdrt
\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvmgf\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3870 
\cellx5580\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CareOf"/>
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="//BrokerFirmName"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAgentName"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line1"/>
		<xsl:if test="//CommitmentLetter/BrokerAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BrokerAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/City"/>
		<xsl:if test="//CommitmentLetter/BrokerAddress/City">
			<xsl:text>{ }</xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Province"/>
		<xsl:text>{  }</xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/PostalCode"/>
		<xsl:if test="//CommitmentLetter/BrokerEmail">
			<xsl:text>\par </xsl:text>
			<xsl:value-of select="//CommitmentLetter/BrokerEmail"/>
		</xsl:if>
		<xsl:text>\cell }
{\b\f1\ul\fs20 Numéro de référence du courtier:\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs24 \trowd \trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvmgf\clvertalt
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3870 \cellx5580\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\row }\trowd \trgaph108\trrh276\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl
\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3870 \cellx5580
\clvmgf\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvmgf\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1 \cell \cell }
{\b\f1\ul\fs20 Numéro de référence du prêteur:\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealNum"/>
		<xsl:text>\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 {\fs24 \trowd \trgaph108\trrh276\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl
\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth3870 \cellx5580
\clvmgf\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvmgf\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\row }\trowd \trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvertalt\clbrdrt
\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1350 \cellx3060\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580
\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1\fs20 \cell Téléphone:\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Phone"/>
		<xsl:text>\cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\fs20 \cell }
\pard\plain \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs24 \trowd 
\trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1350 \cellx3060\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\row }\trowd 
\trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1350 \cellx3060\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\pard 
\ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
{\f1\fs20 \cell Télécopieur:\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerAddress/Fax"/>
		<xsl:text>\cell \cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\fs20 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs24 \trowd 
\trgaph108\trleft-91\trkeep\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1801 \cellx1710\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1350 \cellx3060\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2520 \cellx5580\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3420 \cellx9000\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth2339 \cellx11339\row }\trowd 
\trgaph108\trleft-91\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvertalt
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11430 \cellx11339\pard\plain \s15\ql \li0\ri0\sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 
\fs28\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\b\i\f1\fs28 Nous sommes heureux de confirmer que votre demande de prêt hypothécaire a été approuvée selon les conditions stipulées ci-dessous:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5386 \cellx11340
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\b\fs20\ul EMPRUNTEUR(s):\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<!--#DG312 xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<!--#DG460 only main applicants--> 
		<xsl:call-template name="BorrowerFullName"/>
		
		<xsl:text>\cell }{\b\f1\fs20\ul ADRESSE DE LA PROPRIÉTÉ HYPOTHÉQUÉE:\par }</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Properties/Property">
			<xsl:text>{\f1\fs20 </xsl:text>
			<xsl:value-of select="./AddressLine1"/>
			<xsl:text>\par </xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>  </xsl:text>
			<xsl:value-of select="./PostalCode"/>
			<xsl:text>}</xsl:text>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5386 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20\ul </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>:\cell }
{\f1\fs20 </xsl:text>
    <!--#DG460 oooops, should have been done since #DG312 ...
		<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="borType" select="1"/>
    </xsl:call-template>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 TYPE DE PRÊT: }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Product"/>
		<xsl:text>\tab\tab }
{\b\f1\fs22 QUOTITÉ DE FINANCEMENT: }{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}

<!--  NEW STUFF -->
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3421 \cellx3330
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3599 \cellx6929
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE D\rquote ENGAGEMENT:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE DU PREMIER VERSEMENT:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE D\rquote AJUSTEMENT DES INT\'c9R\'caTS:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/IADDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3421 \cellx3330
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3599 \cellx6929
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11339
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE DE CL\'d4TURE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE D\rquote \'c9CH\'c9ANCE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 MONTANT DE L\rquote AJUSTEMENT DES INT\'c9R\'caTS:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/IADAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3421 \cellx3330
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3599 \cellx6929
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3421 \cellx3330
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3599 \cellx6929
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11339
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22\ul PR\'caT\cell }
\pard\plain \s5\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel4\adjustright 
{\b\f1\fs22\ul TERMES\cell }
\pard\plain \s6\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\f1\fs22\ul P\'c9RIODICIT\'c9 - </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentFrequency"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3421 \cellx3330
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3599 \cellx6929
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1530 \cellx3330
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1800 \cellx5130
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1799 \cellx6929
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9269
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Montant:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f1\fs20 Taux d\rquote int\'e9r\'eat: \cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/InterestRate"/>
		<xsl:text>\cell }
{\b\f1\fs20 Capital et int\'e9r\'eat:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PandIPayment"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1530 \cellx3330
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1800 \cellx5130
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1799 \cellx6929
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9269
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 
\cellx11339
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Assurance:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs20 Terme:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentTerm"/>
		<xsl:text>\cell }
{\b\f1\fs20 Taxes (estimatives):\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TaxPortion"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1530 \cellx3330
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1800 \cellx5130
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1799 \cellx6929
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9269
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11339
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Frais d\rquote administration:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdminFee"/>
		<xsl:text>\cell }
{\b\f1\fs20 Amortissement:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Amortization"/>
		<xsl:text>\cell }
{\b\f1\fs20 Versement total:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalPayment"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd 
\trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1530 \cellx3330
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1800 \cellx5130
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1799 \cellx6929
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9269
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1530 \cellx3330
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1800 \cellx5130
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1799 \cellx6929
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9269
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Total du pr\'eat:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell \cell \cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1530 \cellx3330
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1800 \cellx5130
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1799 \cellx6929
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9269
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11339
\row 
}
\trowd 
\trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11339
\pard\plain \s18\ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f28\fs20\f1 Veuillez noter que le notaire agissant pour cette transaction sera:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11339
\row 
}
\trowd 
\trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9539 \cellx11339
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f28\fs20\f1 Nom du notaire:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9539 \cellx11339
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f28\fs20\f1 Adresse:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9539 \cellx11339
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f28\fs20\f1 \cell ___________________________________________________\cell }
\pard\plain 
\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9539 \cellx11339
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs20\f1 T\'e9l\'e9phone / T\'e9l\'e9copieur:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9539 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9539 \cellx11339
\pard\plain \s18\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\fs20\f1 \cell \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1891 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9539 \cellx11339
\row 
}
<!--  END NEW STUFF -->
			<!--
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE D’ENGAGEMENT:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE DU PREMIER VERSEMENT:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE D’AJUSTEMENT DES INTÉRÊTS:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/IADDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE DE CLÔTURE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/AdvanceDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 DATE D’ÉCHÉANCE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/MaturityDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 MONTANT DE L’AJUSTEMENT DES INTÉRÊTS:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/IADAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22\ul PRÊT\cell }
\pard\plain \s5\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel4\adjustright 
{\b\f1\fs22\ul TERMES\cell }
\pard\plain \s6\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\f1\fs22\ul PÉRIODICITÉ - </xsl:text><xsl:value-of select="//CommitmentLetter/PaymentFrequency"/><xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 

\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl
\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt
\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Montant:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/LoanAmount"/><xsl:text>\cell }
{\b\f1\fs20 Taux d’intérêt: \cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/InterestRate"/><xsl:text>\cell }
{\b\f1\fs20 Capital et intérêt:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/PandIPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Assurance:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/Premium"/><xsl:text>\cell }
{\b\f1\fs20 Terme:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/PaymentTerm"/><xsl:text>\cell }
{\b\f1\fs20 Taxes (estimatives):\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/TaxPortion"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Frais d’administration:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/AdminFee"/><xsl:text>\cell }
{\b\f1\fs20 Amortissement:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/Amortization"/><xsl:text>\cell }
{\b\f1\fs20 Versement total:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/TotalPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Total du prêt:\cell }
{\f1\fs20 </xsl:text><xsl:value-of select="//CommitmentLetter/TotalAmount"/><xsl:text>\cell \cell \cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s18\ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\b Veuillez noter que le notaire agissant pour cette transaction sera:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Nom du notaire:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Adresse:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Téléphone / Télécopieur:\cell ___________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\pard\plain \s18\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
-->

\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 La signature de la présente Lettre d’engagement par les demandeurs atteste l’acceptation des conditions. Cette Lettre doit nous être retournée au plus tard le </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> à défaut de quoi l’offre devient nulle et non avenue.\par }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/ClosingText/Line">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s20\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
</xsl:text>
	</xsl:template>
	<xsl:template name="FrenchPage2Start">
		<xsl:text>{\sect } \linex0\headery706\footery260\endnhere\sectdefaultcl</xsl:text>
	</xsl:template>
	<xsl:template name="FrenchPage2">
		<xsl:text>\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \ql \sb120\sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul CONDITIONS:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 LA PRÉSENTE LETTRE D’ENGAGEMENT EST ASSUJETTIE AUX CONDITIONS SUIVANTES:\par }
{\f1\fs18 Les conditions suivantes doivent être remplies et les documents indiqués doivent être reçus et jugés acceptables quant au fond et à la forme par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> au plus tard dix (10) jours avant l’octroi du prêt hypothécaire à défaut de quoi la présente Lettre d’engagement pourrait être annulée.\par </xsl:text>
		<xsl:for-each select="//CommitmentLetter/Conditions/Condition">
			<xsl:value-of select="position()"/>
			<xsl:text>.  </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\caps\f1\fs18\ul CONDITIONS STANDARD:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 1.\cell </xsl:text>

		<!-- #DG112 -->
		<xsl:text>Réception d'un chèque portant la mention annulé ainsi qu'une autorisation dûment remplie et signée pour permettre à la Corporation Hypothécaire Xceed de prélever automatiquement les versements hypothécaires du compte bancaire.</xsl:text>
		<!--xsl:value-of select="//CommitmentLetter/VoidChq"/-->
		<!-- #DG112 end -->
		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 2.\cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/TitleInsurance/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 3.\cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/FireInsurance/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 4.\cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/TaxesPaid/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>

		<!-- #DG112 added the 5th item below-->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}

</xsl:text><!-- #DG170 commented out xsl:text>
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 5.\cell Toutes les prêts hypothécaires d'Xceed sont fermés. Le paiement du solde en totalité n'est pas permis sauf dans le cas d'une vente véritable prouvée et paiement de la pénalité applicable.
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth450 \cellx360
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx11340
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0</xsl:text-->

	</xsl:template>
	<xsl:template name="FrenchPage3Start">
		<xsl:text>{\sect }\sectd \linex0\headery706\footery260\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Page {\field{\*\fldinst {\cs22  PAGE }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}{\f1\fs20  de }{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
	</xsl:template>
	<xsl:template name="FrenchPage3">
		<xsl:text>\trowd\trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth11440\cellx11350
\pard\plain\s17\qj\sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\caps\f1\fs18\ul TAUX GARANTI ET POLITIQUE DE RAJUSTEMENT DE TAUX:\par }
{\f1\fs18 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par}

{\f1\fs18\b\ul\caps POLITIQUE DE PAIEMENT:\par }
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:for-each select="//CommitmentLetter/PrivilegePayment/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="//CommitmentLetter/InterestCalc/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>
\par }
\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18 CLÔTURE:\par }
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:for-each select="//CommitmentLetter/Closing/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par }
\pard \s17\qj \sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18 ACCEPTATION:\par }
\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:for-each select="//CommitmentLetter/Acceptance/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par \par }
\pard \s17\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>\par \par }
\pard\plain \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 Autorisé par: __________</xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>_____________\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/Signature/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par }
{\f1 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 TÉMOIN\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DEMANDEUR\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 TÉMOIN\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 CO-DEMANDEUR\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 TÉMOIN\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\pard \ql \widctlpar\tx9644\aspalpha\aspnum\faauto\adjustright\itap0</xsl:text>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>\par }}</xsl:text>
	</xsl:template>
	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033{\fonttbl{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f5\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Helvetica;}{\f28\fswiss\fcharset0\fprq0{\*\panose 00000000000000000000}N Helvetica Narrow;}{\f35\froman\fcharset238\fprq2 Times New Roman CE;}{\f36\froman\fcharset204\fprq2 Times New Roman Cyr;}
{\f38\froman\fcharset161\fprq2 Times New Roman Greek;}{\f39\froman\fcharset162\fprq2 Times New Roman Tur;}{\f40\froman\fcharset177\fprq2 Times New Roman (Hebrew);}{\f41\froman\fcharset178\fprq2 Times New Roman (Arabic);}
{\f42\froman\fcharset186\fprq2 Times New Roman Baltic;}{\f43\fswiss\fcharset238\fprq2 Arial CE;}{\f44\fswiss\fcharset204\fprq2 Arial Cyr;}{\f46\fswiss\fcharset161\fprq2 Arial Greek;}{\f47\fswiss\fcharset162\fprq2 Arial Tur;}
{\f48\fswiss\fcharset177\fprq2 Arial (Hebrew);}{\f49\fswiss\fcharset178\fprq2 Arial (Arabic);}{\f50\fswiss\fcharset186\fprq2 Arial Baltic;}{\f75\fswiss\fcharset238\fprq2 Helvetica CE;}{\f76\fswiss\fcharset204\fprq2 Helvetica Cyr;}
{\f78\fswiss\fcharset161\fprq2 Helvetica Greek;}{\f79\fswiss\fcharset162\fprq2 Helvetica Tur;}{\f80\fswiss\fcharset177\fprq2 Helvetica (Hebrew);}{\f81\fswiss\fcharset178\fprq2 Helvetica (Arabic);}{\f82\fswiss\fcharset186\fprq2 Helvetica Baltic;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;
\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;\red255\green255\blue255;}{\stylesheet{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext0 heading 1;}{
\s2\ql \keepn\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \b\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext0 heading 2;}{\s3\ql \keepn\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\b\fs20\ul\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext0 heading 3;}{\s4\ql \keepn\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \b\i\fs28\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext0 
heading 4;}{\s5\ql \keepn\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \b\f1\fs22\ul\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext0 heading 5;}{
\s6\qc \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel5\adjustright\itap0 \b\f1\fs22\ul\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext0 heading 6;}{\*\cs10 \additive Default Paragraph Font;}{
\s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs28\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext15 Body Text;}{\s16\qj \widctlpar\brdrt\brdrs\brdrw10\brsp20 \brdrl\brdrs\brdrw10 \brdrb
\brdrs\brdrw10\brsp620 \brdrr\brdrs\brdrw10 \aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext16 Body Text 2;}{\s17\ql \widctlpar\brdrt\brdrs\brdrw10\brsp20 \brdrl
\brdrs\brdrw10 \brdrb\brdrs\brdrw10\brsp620 \brdrr\brdrs\brdrw10 \aspalpha\aspnum\faauto\adjustright\itap0 \b\fs24\ul\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext17 Body Text 3;}{
\s18\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \b\f28\fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext18 Subhead 2;}{\s19\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\f5\fs17\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext19 Body text;}{\s20\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
\sbasedon0 \snext20 header;}{\s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext21 footer;}{\*\cs22 \additive \sbasedon10 
page number;}}{\*\listtable{\list\listtemplateid140558232\listhybrid{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698703\'02\'00.;}{\levelnumbers\'01;}\chbrdr
\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1\fbias0 \fi-360\li720\jclisttab\tx720 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698713\'02\'01.;}{\levelnumbers\'01;}
\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li1440\jclisttab\tx1440 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698715\'02\'02.;}{\levelnumbers\'01;}
\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li2160\jclisttab\tx2160 }{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698703\'02\'03.;}{\levelnumbers\'01;}
\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li2880\jclisttab\tx2880 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698713\'02\'04.;}{\levelnumbers\'01;}
\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li3600\jclisttab\tx3600 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698715\'02\'05.;}{\levelnumbers\'01;}
\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li4320\jclisttab\tx4320 }{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698703\'02\'06.;}{\levelnumbers\'01;}
\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li5040\jclisttab\tx5040 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698713\'02\'07.;}{\levelnumbers\'01;}
\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li5760\jclisttab\tx5760 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698715\'02\'08.;}{\levelnumbers\'01;}
\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li6480\jclisttab\tx6480 }{\listname ;}\listid417363440}{\list\listtemplateid849922510\listhybrid{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0
\levelindent0{\leveltext\'02\'00);}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1\fbias0 \fi-720\li1080\jclisttab\tx1080 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0
{\leveltext\'02\'01.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li1440\jclisttab\tx1440 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'02.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li2160\jclisttab\tx2160 }{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'03.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li2880\jclisttab\tx2880 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'04.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li3600\jclisttab\tx3600 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'05.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li4320\jclisttab\tx4320 }{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'06.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li5040\jclisttab\tx5040 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'07.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li5760\jclisttab\tx5760 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'08.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li6480\jclisttab\tx6480 }{\listname ;}\listid1630238392}{\list\listtemplateid261500848\listhybrid{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0
\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698703\'02\'00.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li720\jclisttab\tx720 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0
\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698713\'02\'01.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li1440\jclisttab\tx1440 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698715\'02\'02.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li2160\jclisttab\tx2160 }{\listlevel\levelnfc0\levelnfcn0\leveljc0
\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698703\'02\'03.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li2880\jclisttab\tx2880 }{\listlevel\levelnfc4\levelnfcn4
\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698713\'02\'04.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li3600\jclisttab\tx3600 }{\listlevel\levelnfc2
\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698715\'02\'05.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li4320\jclisttab\tx4320 }{\listlevel
\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698703\'02\'06.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li5040\jclisttab\tx5040 }
{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698713\'02\'07.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li5760
\jclisttab\tx5760 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\leveltemplateid67698715\'02\'08.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 
\fi-180\li6480\jclisttab\tx6480 }{\listname ;}\listid1839271392}{\list\listtemplateid1295274388\listhybrid{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\leveltemplateid67698703
\'02\'00.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1\fbias0 \fi-360\li720\jclisttab\tx720 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\leveltemplateid67698713\'02\'01.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li1440\jclisttab\tx1440 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\leveltemplateid67698715\'02\'02.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li2160\jclisttab\tx2160 }{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\leveltemplateid67698703\'02\'03.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li2880\jclisttab\tx2880 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\leveltemplateid67698713\'02\'04.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li3600\jclisttab\tx3600 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\leveltemplateid67698715\'02\'05.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li4320\jclisttab\tx4320 }{\listlevel\levelnfc0\levelnfcn0\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\leveltemplateid67698703\'02\'06.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li5040\jclisttab\tx5040 }{\listlevel\levelnfc4\levelnfcn4\leveljc0\leveljcn0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\leveltemplateid67698713\'02\'07.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-360\li5760\jclisttab\tx5760 }{\listlevel\levelnfc2\levelnfcn2\leveljc2\leveljcn2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\leveltemplateid67698715\'02\'08.;}{\levelnumbers\'01;}\chbrdr\brdrnone\brdrcf1 \chshdng0\chcfpat1\chcbpat1 \fi-180\li6480\jclisttab\tx6480 }{\listname ;}\listid1922450526}}{\*\listoverridetable{\listoverride\listid1630238392\listoverridecount0\ls1}
{\listoverride\listid417363440\listoverridecount0\ls2}{\listoverride\listid1839271392\listoverridecount0\ls3}{\listoverride\listid1922450526\listoverridecount0\ls4}}{\info{\title  }{\author dan conte}{\operator John Schisler}
{\creatim\yr2002\mo4\dy16\hr16\min39}{\revtim\yr2002\mo4\dy16\hr16\min39}{\printim\yr2002\mo4\dy15\hr14\min58}{\version3}{\edmins0}{\nofpages2}{\nofwords498}{\nofchars2844}{\*\company  }{\nofcharsws0}{\vern8283}}\margl432\margr432\margt720\margb850 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dghspace180\dgvspace180\dghorigin1701\dgvorigin1984\dghshow0\dgvshow0\jexpand\viewkind1\viewscale104\pgbrdrhead\pgbrdrfoot\nolnhtadjtbl \fet0
\sectd \linex0\headery706\footery260\endnhere\sectdefaultcl </xsl:text>
	</xsl:template>
	
	<!--#DG312 --><!--#DG460 filter applicants-->
	<!-- ================================================  -->
	<xsl:template name="BorrowerFullName">
		<xsl:param name="separa" select="'\par '"/>
		<xsl:param name="borType" select="0"/>
		<xsl:for-each select="/*/Deal/Borrower[borrowerTypeId = $borType]"> 
  		<xsl:if test="salutation">
  			<xsl:value-of select="salutation"/>
  			<xsl:text> </xsl:text>
  		</xsl:if>
  		<xsl:value-of select="borrowerFirstName"/>
  		<xsl:text> </xsl:text>
  		<xsl:if test="borrowerMiddleInitial != ''">
  			<xsl:value-of select="borrowerMiddleInitial"/>
  			<xsl:text>. </xsl:text>
  		</xsl:if>
  		<xsl:value-of select="borrowerLastName"/>
  		<xsl:if test="position() !=last()">
        <xsl:value-of select="$separa"/>
      </xsl:if>
		</xsl:for-each>
	</xsl:template>
	
</xsl:stylesheet>
