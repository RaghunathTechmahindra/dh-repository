<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output version="1.0" method="html" encoding="UTF-8" indent="no"/>
<xsl:variable name="image-dir" select="//Document/Logo_Image"></xsl:variable>	
	<xsl:variable name="fo:layout-master-set">
	
		<fo:layout-master-set>
			<fo:simple-page-master master-name="default-page" page-height="14in" page-width="8.5in" margin-left="0.5in" margin-right="0.48in">
				<fo:region-body margin-top="0.5in" margin-bottom="0.59in"/>
				<fo:region-after extent="0.59in"/>
			</fo:simple-page-master>
		</fo:layout-master-set>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:variable name="maxwidth" select="7.52000"/>
		<fo:root>
			<xsl:copy-of select="$fo:layout-master-set"/>
			<fo:page-sequence master-reference="default-page" initial-page-number="1" format="1">
				<xsl:call-template name="footerall"/>
				<fo:flow flow-name="xsl-region-body">
					<fo:block>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth0" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths0" select="2.08333 + 1.32292 + 4.39583 + 0.20833 + 2.79167"/>
						<xsl:variable name="factor0">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths0 &gt; 0.00000 and $sumcolumnwidths0 &gt; $tablewidth0">
									<xsl:value-of select="$tablewidth0 div $sumcolumnwidths0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth0_0" select="1.92333 * $factor0"/>
						<xsl:variable name="columnwidth0_1" select="1.38292 * $factor0"/>
						<xsl:variable name="columnwidth0_2" select="4.39583 * $factor0"/>
						<xsl:variable name="columnwidth0_3" select="0.20833 * $factor0"/>
						<xsl:variable name="columnwidth0_4" select="2.85167 * $factor0"/>
						<fo:table width="{$tablewidth0}in" space-before.optimum="1pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth0_0}in"/>
							<fo:table-column column-width="{$columnwidth0_1}in"/>
							<fo:table-column column-width="{$columnwidth0_2}in"/>
							<fo:table-column column-width="{$columnwidth0_3}in"/>
							<fo:table-column column-width="{$columnwidth0_4}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo_fr.jpeg" />	
											</fo:inline>
											</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block >
											<fo:inline font-weight="bold">
												<xsl:text>MANDAT D&apos;ÉVALUATION ET D&apos;INSPECTION</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="7pt" font-weight="bold">
												<xsl:text>Service Évaluation-inspection</xsl:text>
											</fo:inline>
											<fo:block font-size="7pt">
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="7pt">
												<xsl:text>Montréal (1689-1) Télécopieur: (514) 394-8648 ou (800) 634-9591</xsl:text>
											</fo:inline>
											<fo:block font-size="7pt">
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="7pt">
												<xsl:text>Site: https://bncevaluation.com</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="after" number-rows-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth1" select="$columnwidth0_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths1" select="2.09375"/>
											<xsl:variable name="factor1">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths1 &gt; 0.00000 and $sumcolumnwidths1 &gt; $tablewidth1">
														<xsl:value-of select="$tablewidth1 div $sumcolumnwidths1"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth1_0" select="2.09375 * $factor1"/>
											<fo:table width="{$tablewidth1}in" space-before.optimum="1pt" space-after.optimum="1pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth1_0}in"/>
												<fo:table-body font-size="10pt">
												<fo:table-row >
													
														<fo:table-cell height="0.20222in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02084in">
															<fo:block padding-top="5pt"  display-align="center">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 4pt" text-align="left">
																	<xsl:text>Référence de la demande #:  </xsl:text>
																	<xsl:value-of select="//RefSourceApplicationNo"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
													
														<fo:table-cell height="0.31333in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02084in" border-left-style="solid" border-left-color="black" border-left-width="0.02084in" border-right-style="solid" border-right-color="black" border-right-width="0.02084in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
															
																<fo:inline font-size="6pt">
																
																	<xsl:text>&#160; No de dossier évaluation-inspection</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="4">
										<fo:block>
										<xsl:variable name="tablewidth2" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths2" select="0.26042 + 0.54167 + 0.20833 + 0.87500 + 0.20833 + 7.40625"/>
						<xsl:variable name="factor2">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths2 &gt; 0.00000 and $sumcolumnwidths2 &gt; $tablewidth2">
									<xsl:value-of select="$tablewidth2 div $sumcolumnwidths2"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns2" select="1 + 1"/>
						<xsl:variable name="defaultcolumnwidth2">
							<xsl:choose>
								<xsl:when test="$factor2 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns2 &gt; 0">
									<xsl:value-of select="($tablewidth2 - $sumcolumnwidths2) div $defaultcolumns2"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
							<xsl:variable name="columnwidth2_0" select="0.20833 * $factor2"/>
						<xsl:variable name="columnwidth2_1" select="0.43333 * $factor2"/>
						<xsl:variable name="columnwidth2_2" select="0.20833 * $factor2"/>
						<xsl:variable name="columnwidth2_3" select="0.75625 * $factor2"/>
						<xsl:variable name="columnwidth2_4" select="0.20833 * $factor2"/>
						<xsl:variable name="columnwidth2_5" select="7.40625 * $factor2"/>
						<xsl:variable name="columnwidth2_6" select="$defaultcolumnwidth2"/>
						<xsl:variable name="columnwidth2_7" select="$defaultcolumnwidth2"/>
						<fo:table width="{$tablewidth2}in" space-before.optimum="1pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth2_0}in"/>
							<fo:table-column column-width="{$columnwidth2_1}in"/>
							<fo:table-column column-width="{$columnwidth2_2}in"/>
							<fo:table-column column-width="{$columnwidth2_3}in"/>
							<fo:table-column column-width="{$columnwidth2_4}in"/>
							<fo:table-column column-width="{$columnwidth2_5}in"/>
							<fo:table-column column-width="{$columnwidth2_6}in"/>
							<fo:table-column column-width="{$columnwidth2_7}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="1pt">
											<xsl:choose>
												<xsl:when test="//mtgCheck = '1'">
												   <xsl:call-template name="CheckedCheckbox"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:call-template name="UnCheckedCheckbox"/>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>									
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold"  >
												<xsl:text>PH</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="5pt">
														
															<xsl:choose>
																  <xsl:when test="//locCheck = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">				
												<xsl:text>Marges</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="5pt">
													
															<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
												<fo:inline font-size="8pt" font-weight="bold">												<xsl:text>Autres produits de crédits</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									
									
									
								</fo:table-row>
							</fo:table-body>
						</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						
						<xsl:variable name="tablewidth3" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths3" select="3.28125 + 0.79167"/>
						<xsl:variable name="factor3">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths3 &gt; 0.00000 and $sumcolumnwidths3 &gt; $tablewidth3">
									<xsl:value-of select="$tablewidth3 div $sumcolumnwidths3"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns3" select="1"/>
						<xsl:variable name="defaultcolumnwidth3">
							<xsl:choose>
								<xsl:when test="$factor3 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns3 &gt; 0">
									<xsl:value-of select="($tablewidth3 - $sumcolumnwidths3) div $defaultcolumns3"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth3_0" select="3.56042 * $factor3"/>
						<xsl:variable name="columnwidth3_1" select="0.81667 * $factor3"/>
						<xsl:variable name="columnwidth3_2" select="2.96042 * $factor3"/>
						<fo:table  width="{$tablewidth3}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center"  border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" >
							<fo:table-column column-width="{$columnwidth3_0}in"/>
							<fo:table-column column-width="{$columnwidth3_1}in"/>
							<fo:table-column column-width="{$columnwidth3_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell display-align="center" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" height="0.22292in">
										<fo:block >
											<fo:inline font-size="7.5pt" font-weight="bold">
												<xsl:text>TRAVAIL REQUIS</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="before" text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" height="0.02292in">
										<fo:block text-align="center" line-height="0.1411in" >
											<fo:inline font-size="7.5pt" font-weight="bold">
												<xsl:text>SECTIONS</xsl:text>
											</fo:inline>
										</fo:block>
										<fo:block line-height="0.14555in">
											<fo:inline font-size="7.5pt" font-weight="bold" padding-bottom="2pt">
												<xsl:text>à remplir</xsl:text>
											</fo:inline>
										</fo:block >
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in"  height="0.22292in">
										<fo:block >
											<fo:inline font-size="7.5pt" font-weight="bold">
												<xsl:text>DOCUMENTS REQUIS</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.63750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" display-align="before">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth4" select="$columnwidth3_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths4" select="0.20833 + 0.92708 + 0.15625 + 0.98958 + 0.16667 + 0.87500"/>
											<xsl:variable name="factor4">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths4 &gt; 0.00000 and $sumcolumnwidths4 &gt; $tablewidth4">
														<xsl:value-of select="$tablewidth4 div $sumcolumnwidths4"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns4" select="1"/>
											<xsl:variable name="defaultcolumnwidth4">
												<xsl:choose>
													<xsl:when test="$factor4 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns4 &gt; 0">
														<xsl:value-of select="($tablewidth4 - $sumcolumnwidths4) div $defaultcolumns4"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth4_0" select="0.20833 * $factor4"/>
											<xsl:variable name="columnwidth4_1" select="1.17708 * $factor4"/>
											<xsl:variable name="columnwidth4_2" select="$defaultcolumnwidth4"/>
											<xsl:variable name="columnwidth4_3" select="0.15625 * $factor4"/>
											<xsl:variable name="columnwidth4_4" select="0.89958 * $factor4"/>
											<xsl:variable name="columnwidth4_5" select="0.16667 * $factor4"/>
											<xsl:variable name="columnwidth4_6" select="0.87500 * $factor4"/>
											<fo:table width="{$tablewidth4}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth4_0}in"/>
												<fo:table-column column-width="{$columnwidth4_1}in"/>
												<fo:table-column column-width="{$columnwidth4_2}in"/>
												<fo:table-column column-width="{$columnwidth4_3}in"/>
												<fo:table-column column-width="{$columnwidth4_4}in"/>
												<fo:table-column column-width="{$columnwidth4_5}in"/>
												<fo:table-column column-width="{$columnwidth4_6}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="right">
<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="5pt">
<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text>&#160;&#160;  </xsl:text>
																					</fo:inline>															
																   <xsl:call-template name="CheckedCheckbox"/>
															
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7.5pt" font-weight="bold">
																	<xsl:text>&#160;&#160;Évaluation</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
												
																   <xsl:call-template name="CheckedCheckbox"/>
														
																   
																
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>A</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text> Financement</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>B</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text> Réalisation&#160;&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="right">
<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="5pt">
<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text>&#160;&#160;  </xsl:text>
																					</fo:inline>															
															<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt">
																	<xsl:text>&#160;&#160;Amendement</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>C</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text> Rénovations</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>						<fo:block padding-top="1pt" padding-bottom="1pt"/>					</fo:table-cell>
														<fo:table-cell display-align="before" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt" vertical-align="super" text-align="right">
																	<xsl:text>&#160;&#160;&#160;Recouvrement</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt">
																	<xsl:text>&#160;# dossier antérieur</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="3">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth5" select="$columnwidth4_4 * 1.00000 + $columnwidth4_5 * 1.00000 + $columnwidth4_6 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths5" select="0.60417 + 0.39583 + 0.37500 + 0.73958 + 1.92708"/>
																<xsl:variable name="factor5">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths5 &gt; 0.00000 and $sumcolumnwidths5 &gt; $tablewidth5">
																			<xsl:value-of select="$tablewidth5 div $sumcolumnwidths5"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth5_0" select="0.60417 * $factor5"/>
																<xsl:variable name="columnwidth5_1" select="0.39583 * $factor5"/>
																<xsl:variable name="columnwidth5_2" select="0.30500 * $factor5"/>
																<xsl:variable name="columnwidth5_3" select="0.73958 * $factor5"/>
																<xsl:variable name="columnwidth5_4" select="1.92708 * $factor5"/>
																<fo:table width="{$tablewidth5}in" space-before.optimum="1pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth5_0}in"/>
																	<fo:table-column column-width="{$columnwidth5_1}in"/>
																	<fo:table-column column-width="{$columnwidth5_2}in"/>
																	<fo:table-column column-width="{$columnwidth5_3}in"/>
																	<fo:table-column column-width="{$columnwidth5_4}in"/>
																	<fo:table-body font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="7pt">
																						<xsl:text>Valeur:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell><fo:block padding-top="1pt" padding-bottom="1pt" padding-left="5pt">
																				<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text>&#160;&#160;  </xsl:text>
																				</fo:inline>															
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="smallCheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="smallUnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>Future&#160;</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="5pt" text-align="right">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text>&#160;&#160;&#160;&#160; </xsl:text>
																					</fo:inline>																				
																					<xsl:choose>
																						<xsl:when test="//alwUnchkd = '1'">
																						   <xsl:call-template name="smallCheckedCheckbox"/>
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:call-template name="smallUnCheckedCheckbox"/>
																						</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>																	</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Actuelle et future</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>														</fo:table-cell>
														<fo:table-cell number-columns-spanned="3">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>D</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text>&#160;Copropriété indivise</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														
														
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell  number-columns-spanned="2" border-top-style="solid" border-top-color="black" border-top-width="0.0012in" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.63750in" text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="left">
											<fo:inline font-size="7pt">
												<xsl:text>&#160;&#160;(1, 3, 8 et 9)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.63750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" font-size="7pt">
											<fo:inline font-size="7pt" >
												<xsl:text>Certificat de localisation - Comptes de taxes municipales et scolaires</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline >
												<xsl:text>Inscription (MLS) - Liste des baux et dépenses d&apos;exploitation -</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="7pt">
												<xsl:text>Copie de la garantie (réalisation) - Liste et coût détaillé des travaux</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="7pt">
												<xsl:text>Croquis démontrant espaces occupés</xsl:text>
											</fo:inline>
											<fo:block>
												<fo:leader leader-pattern="space"></fo:leader>
											</fo:block>
											<fo:block>
												<fo:table  display-align="before">
													<fo:table-column column-width="0.99999in"/>
													<fo:table-column column-width="2.40007in"/>
													
													<fo:table-body>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="2">
															<fo:block>
																<xsl:text></xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
														<fo:table-row>
															<fo:table-cell >
																<fo:block>	
																	<fo:inline font-size="7pt" font-weight="bold">
																		<xsl:text>Rénovations:</xsl:text>
																	</fo:inline>	
																</fo:block>
															</fo:table-cell>
													
															<fo:table-cell >
																<fo:block font-size="7pt">
																	<fo:inline >
																		 <xsl:text>Description et coût des travaux par catégorie </xsl:text>
																	</fo:inline>	
																	<fo:block>
																		<xsl:text>&#xA;</xsl:text>
																	</fo:block>
																	<fo:inline >
																		<xsl:text>d&apos;entrepreneurs</xsl:text>
																	</fo:inline>
																</fo:block>
																
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:block>	
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.44792in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth6" select="$columnwidth3_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths6" select="0.22917 + 1.20833 + 0.16667 + 2.17708 + 0.06250"/>
											<xsl:variable name="factor6">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths6 &gt; 0.00000 and $sumcolumnwidths6 &gt; $tablewidth6">
														<xsl:value-of select="$tablewidth6 div $sumcolumnwidths6"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns6" select="1"/>
											<xsl:variable name="defaultcolumnwidth6">
												<xsl:choose>
													<xsl:when test="$factor6 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns6 &gt; 0">
														<xsl:value-of select="($tablewidth6 - $sumcolumnwidths6) div $defaultcolumns6"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth6_0" select="0.17708 * $factor6"/>
											<xsl:variable name="columnwidth6_1" select="1.16667 * $factor6"/>
											<xsl:variable name="columnwidth6_2" select="$defaultcolumnwidth6"/>
											<xsl:variable name="columnwidth6_3" select="0.12750 * $factor6"/>
											<xsl:variable name="columnwidth6_4" select="1.41667 * $factor6"/>
											<xsl:variable name="columnwidth6_5" select="0.06250 * $factor6"/>	<fo:table width="{$tablewidth6}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth6_0}in"/>
												<fo:table-column column-width="{$columnwidth6_1}in"/>
												<fo:table-column column-width="{$columnwidth6_2}in"/>
												<fo:table-column column-width="{$columnwidth6_3}in"/>
												<fo:table-column column-width="{$columnwidth6_4}in"/>
												<fo:table-column column-width="{$columnwidth6_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="right">
<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="5pt">
<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text>&#160;&#160;  </xsl:text>
																					</fo:inline>															
															<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7.5pt" font-weight="bold">
																	<xsl:text>&#160;&#160;Estimé sur plans</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>A</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text> Constructeur accrédité</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="right">
<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="5pt">
<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text>&#160;&#160;  </xsl:text>
																					</fo:inline>															
															<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt">
																	<xsl:text>&#160;&#160;Amendement</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>B&#160;</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text> Particulier par constructeur accrédité</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.17708in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.17708in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt">
																	<xsl:text># dossier antérieur</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.17708in">
														<fo:block padding-top="1pt" padding-bottom="1pt">
<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>													
															</fo:table-cell>
														<fo:table-cell display-align="before" height="0.17708in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>C&#160;</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text> Autoconstructeur</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.17708in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.11111in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.11111in" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.0012in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.11111in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.11111in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.11111in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.44792in" text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-bottom="10pt" >
											<fo:inline font-size="7pt">
												<xsl:text>&#160;&#160;(2, 3, 8 and 9)</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="7pt">
												<xsl:text>&#160;&#160;(1, 2, 3, 8 and 9)</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="7pt">
												<xsl:text>&#160;&#160;(1, 3, 8 and 9)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.44792in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt" font-size="7pt">
											<fo:inline font-size="7pt" font-weight="bold">
												<xsl:text>Construction neuve:</xsl:text>
											</fo:inline>
											<fo:inline >
												<xsl:text>&#160; Modèle - Plans et devis - Plan du terrain - </xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Offre d&apos;achat/contrat - Liste et coût des extras </xsl:text>
											
											<fo:block>
												<xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;et crédits</xsl:text>
											</fo:block>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.17708in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth7" select="$columnwidth3_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths7" select="0.17708"/>
											<xsl:variable name="factor7">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths7 &gt; 0.00000 and $sumcolumnwidths7 &gt; $tablewidth7">
														<xsl:value-of select="$tablewidth7 div $sumcolumnwidths7"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns7" select="1"/>
											<xsl:variable name="defaultcolumnwidth7">
												<xsl:choose>
													<xsl:when test="$factor7 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns7 &gt; 0">
														<xsl:value-of select="($tablewidth7 - $sumcolumnwidths7) div $defaultcolumns7"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth7_0" select="0.17708 * $factor7"/>
											<xsl:variable name="columnwidth7_1" select="$defaultcolumnwidth7"/>
											<fo:table width="{$tablewidth7}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center">
												<fo:table-column column-width="0.1000in"/>
												<fo:table-column column-width="{$columnwidth7_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.18750in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.18750in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt" font-weight="bold"><xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															<fo:inline font-size="7.5pt">
																	<xsl:text>&#160;&#160;Certificat de visite</xsl:text>
																</fo:inline>	
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>							</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" text-align="left" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-top="0pt" padding-bottom="1pt">
											<fo:inline font-size="7pt">
												<xsl:text>&#160;&#160;(1, 3, 4, 8 et 9)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-top="0pt" padding-bottom="1pt">
											<fo:inline font-size="7pt">
												<xsl:text>Compte de taxes municipales détaillé de l&apos;année courante</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth8" select="$columnwidth3_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths8" select="0.18750 + 1.46875 + 0.16667 + 0.84375"/>
											<xsl:variable name="factor8">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths8 &gt; 0.00000 and $sumcolumnwidths8 &gt; $tablewidth8">
														<xsl:value-of select="$tablewidth8 div $sumcolumnwidths8"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns8" select="1"/>
											<xsl:variable name="defaultcolumnwidth8">
												<xsl:choose>
													<xsl:when test="$factor8 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns8 &gt; 0">
														<xsl:value-of select="($tablewidth8 - $sumcolumnwidths8) div $defaultcolumns8"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth8_0" select="0.18750 * $factor8"/>
											<xsl:variable name="columnwidth8_1" select="1.48875 * $factor8"/>
											<xsl:variable name="columnwidth8_2" select="0.13667 * $factor8"/>
											<xsl:variable name="columnwidth8_3" select="0.84375 * $factor8"/>
											<xsl:variable name="columnwidth8_4" select="$defaultcolumnwidth8"/>
											<fo:table width="{$tablewidth8}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
												<fo:table-column column-width="0.0890in"/>
												<fo:table-column column-width="{$columnwidth8_1}in"/>
												<fo:table-column column-width="{$columnwidth8_2}in"/>
												<fo:table-column column-width="{$columnwidth8_3}in"/>
												<fo:table-column column-width="{$columnwidth8_4}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell display-align="before" height="0.01042in" number-rows-spanned="2" text-align="right" vertical-align="sub">
															
														</fo:table-cell>
														<fo:table-cell height="0.01042in" number-rows-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" font-weight="bold"><xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																<fo:inline font-size="7.5pt">
																	<xsl:text>&#160;&#160;Vérification du rapport</xsl:text>
																</fo:inline>
															</fo:inline>	
															</fo:block>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="0pt">
																<fo:inline>
																	<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="smallCheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="smallUnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="2pt">
																<fo:inline font-size="7pt">
																	<xsl:text>Immobilier</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
													
													
													
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="0pt" line-height="0.0214in">
																<fo:inline>
																	<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="smallCheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="smallUnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="3pt" line-height="0.0214in">
																<fo:inline font-size="7pt">
																	<xsl:text>Environnemental</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.04167in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													
													
													</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-top="0pt" padding-bottom="1pt">
											<fo:inline font-size="7pt">
												<xsl:text>&#160;&#160;(1, 3, 8 et 9)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="justify" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-top="0pt" padding-bottom="1pt">
											<fo:inline font-size="7pt" font-weight="bold">
												<xsl:text>Rapport d&apos;évaluation</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.58333in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth9" select="$columnwidth3_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths9" select="0.29167 + 2.18750 + 1.68750"/>
											<xsl:variable name="factor9">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths9 &gt; 0.00000 and $sumcolumnwidths9 &gt; $tablewidth9">
														<xsl:value-of select="$tablewidth9 div $sumcolumnwidths9"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth9_0" select="0.22167 * $factor9"/>
											<xsl:variable name="columnwidth9_1" select="1.20050 * $factor9"/>
											<xsl:variable name="columnwidth9_2" select="1.68750 * $factor9"/>
											<fo:table width="{$tablewidth9}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth9_0}in"/>
												<fo:table-column column-width="{$columnwidth9_1}in"/>
												<fo:table-column column-width="{$columnwidth9_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="2pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																<xsl:text>&#160;</xsl:text>
															</fo:inline>															
															<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>	
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="left">
																<fo:inline font-size="7.5pt" font-weight="bold">
																	<xsl:text>Inspection immobilière</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"><xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>&#160;&#160;A</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text>&#160;&#160;&#160;Construction neuve</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														
														<fo:table-cell number-columns-spanned="2" display-align="center" >
															<fo:block  padding-left="5pt" text-align="right">
																<fo:table width="95%">
																	<fo:table-column column-width="proportional-column-width(20)"/>
																	<fo:table-column column-width="proportional-column-width(65)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block text-align="right">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" >
																							<xsl:text> </xsl:text>
																						<xsl:choose>
																						<xsl:when test="//alwUnchkd = '1'">
																						   <xsl:call-template name="smallCheckedCheckbox"/>
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:call-template name="smallUnCheckedCheckbox"/>
																						</xsl:otherwise>
																						</xsl:choose>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block text-align="left" padding-bottom="2pt">
																					<fo:inline font-size="7pt" >
																						<xsl:text>&#160;&#160;1 - Débours</xsl:text>
																					</fo:inline>	
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"><xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>&#160;&#160;B</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text>&#160;&#160;&#160;Rénovations</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-left="5pt" text-align="center">
																<fo:table width="95%">
																	<fo:table-column column-width="proportional-column-width(20)"/>
																	<fo:table-column column-width="proportional-column-width(65)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block text-align="right">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" >
																							<xsl:text> </xsl:text>
																						<xsl:choose>
																						<xsl:when test="//alwUnchkd = '1'">
																						   <xsl:call-template name="smallCheckedCheckbox"/>
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:call-template name="smallUnCheckedCheckbox"/>
																						</xsl:otherwise>
																						</xsl:choose>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block text-align="left" padding-bottom="2pt">
																					<fo:inline font-size="7pt" >
																						<xsl:text>&#160;&#160;2 - Fin légale</xsl:text>
																					</fo:inline>	
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>	
															</fo:block>	
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"><xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																<fo:inline font-size="7pt" font-weight="bold">
																	<xsl:text>&#160;&#160;C</xsl:text>
																</fo:inline>
																<fo:inline font-size="7pt">
																	<xsl:text>&#160;&#160;&#160;Incendie</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.58333in" text-align="left" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-top="0pt" padding-bottom="1pt">
											<fo:inline font-size="7pt">
												<xsl:text>&#160;&#160;(1, 3, 5, 8 et 9)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.58333in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-top="4pt" padding-bottom="1pt" font-size="7pt">
												<fo:inline font-size="7pt" font-weight="bold">
													<xsl:text>Construction neuve:</xsl:text>
												</fo:inline>
												<fo:inline font-size="7pt">
													<xsl:text>&#160;&#160;&#160;Estimé sur plans</xsl:text>
												</fo:inline>			
											</fo:block>		
											<fo:block>
												<fo:inline font-size="7pt" font-weight="bold">
													<xsl:text>Rénovations:&#160;&#160;&#160;</xsl:text>
												</fo:inline>
												<fo:inline font-size="7pt">
													<xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Liste et coût détaillé des travaux - Évaluation</xsl:text>
												</fo:inline>
											</fo:block>
											<fo:block>
												<fo:inline font-size="7pt" font-weight="bold">
													<xsl:text>Incendie:&#160;</xsl:text>
												</fo:inline>
												<fo:inline font-size="7pt">
													<xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; Soumission ou contrat</xsl:text>
												</fo:inline>
											</fo:block>	
										
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.07292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth10" select="$columnwidth3_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths10" select="0.30208"/>
											<xsl:variable name="factor10">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths10 &gt; 0.00000 and $sumcolumnwidths10 &gt; $tablewidth10">
														<xsl:value-of select="$tablewidth10 div $sumcolumnwidths10"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns10" select="1"/>
											<xsl:variable name="defaultcolumnwidth10">
												<xsl:choose>
													<xsl:when test="$factor10 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns10 &gt; 0">
														<xsl:value-of select="($tablewidth10 - $sumcolumnwidths10) div $defaultcolumns10"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth10_0" select="0.23208 * $factor10"/>
											<xsl:variable name="columnwidth10_1" select="$defaultcolumnwidth10"/>
											<fo:table width="{$tablewidth10}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth10_0}in"/>
												<fo:table-column column-width="{$columnwidth10_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
<fo:block padding-top="1pt" padding-bottom="1pt" >
<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text>&#160;&#160;</xsl:text>
																					</fo:inline>															
															<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="left">
																<fo:inline font-size="7.5pt" font-weight="bold">
																	<xsl:text>&#160;Rapport de consultation (spécifier travail section 7)</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.07292in" text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" display-align="before">
										<fo:block padding-top="1pt" padding-bottom="2pt">
											<fo:inline font-size="7pt">
												<xsl:text>&#160;&#160;(1, 3, 7, 8 et 9)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.07292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in"  display-align="before">
										<fo:block padding-top="1pt" padding-bottom="2pt">
											<fo:inline font-size="7pt" font-weight="bold">
												<xsl:text>Selon vos instructions</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth11" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths11" select="0.16667"/>
						<xsl:variable name="factor11">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths11 &gt; 0.00000 and $sumcolumnwidths11 &gt; $tablewidth11">
									<xsl:value-of select="$tablewidth11 div $sumcolumnwidths11"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns11" select="1"/>
						<xsl:variable name="defaultcolumnwidth11">
							<xsl:choose>
								<xsl:when test="$factor11 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns11 &gt; 0">
									<xsl:value-of select="($tablewidth11 - $sumcolumnwidths11) div $defaultcolumns11"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth11_0" select="0.16667 * $factor11"/>
						<xsl:variable name="columnwidth11_1" select="$defaultcolumnwidth11"/>
						<fo:table width="{$tablewidth11}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" >
							<fo:table-column column-width="{$columnwidth11_0}in"/>
							<fo:table-column column-width="{$columnwidth11_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>IDENTIFICATION DU CLIENT</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block />
									</fo:table-cell>
									<fo:table-cell>
										<fo:block >
											<xsl:variable name="tablewidth12" select="$columnwidth11_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths12" select="4.46875 + 0.01042 + 3.01042 + 2.47917 + 2.41667"/>
											<xsl:variable name="factor12">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths12 &gt; 0.00000 and $sumcolumnwidths12 &gt; $tablewidth12">
														<xsl:value-of select="$tablewidth12 div $sumcolumnwidths12"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth12_0" select="4.46875 * $factor12"/>
											<xsl:variable name="columnwidth12_1" select="0.01042 * $factor12"/>
											<xsl:variable name="columnwidth12_2" select="3.01042 * $factor12"/>
											<xsl:variable name="columnwidth12_3" select="2.47917 * $factor12"/>
											<xsl:variable name="columnwidth12_4" select="2.41667 * $factor12"/>
											<fo:table width="{$tablewidth12}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
												<fo:table-column column-width="{$columnwidth12_0}in"/>
												<fo:table-column column-width="{$columnwidth12_1}in"/>
												<fo:table-column column-width="{$columnwidth12_2}in"/>
												<fo:table-column column-width="{$columnwidth12_3}in"/>
												<fo:table-column column-width="{$columnwidth12_4}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.32292in"  padding-top="1pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>&#160;</xsl:text>
																</fo:inline>
																<fo:inline font-size="6pt">
																	<xsl:text>Nom du client</xsl:text> 
																</fo:inline>
															</fo:block>
															<fo:block>
																<xsl:text> </xsl:text>	
															</fo:block>
															<fo:block>
																<xsl:text>&#160;</xsl:text>	<xsl:value-of select="//borName1"/>		
															</fo:block>
															
														</fo:table-cell>
														<fo:table-cell height="0.32292in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in">

														<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																
																</fo:inline>
															</fo:block>
														</fo:table-cell >
														<fo:table-cell height="0.32292in" display-align="before">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Résidence</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="//cltTelNoHome"/>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.32292in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Emploi</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="//cltTelNoWork"/>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.32292in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Cellulaire</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;</xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
													<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" padding-top="1pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>&#160;</xsl:text>
																</fo:inline>
																<fo:inline font-size="6pt">
																	<xsl:text>Nom du propriétaire (</xsl:text> 
																</fo:inline>
																<fo:inline font-size="6pt" font-style="italic">
																	<xsl:text>Si différent</xsl:text>
																</fo:inline>
																<fo:inline font-size="6pt">
																	<xsl:text>)</xsl:text> 
																</fo:inline>
															</fo:block>
															<fo:block>
																<xsl:text> </xsl:text>	
															</fo:block>
															<fo:block>
																<xsl:text>&#160;</xsl:text>	<xsl:value-of select="//ownerName"/>		
															</fo:block>
															
														</fo:table-cell>
														<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">

														<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in"  display-align="before">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Résidence</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text></xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Emploi</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text></xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Cellulaire</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text></xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.33292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" padding-top="1pt" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>&#160;</xsl:text>
																</fo:inline>
																<fo:inline font-size="6pt">
																	<xsl:text>Personne ressource (</xsl:text> 
																</fo:inline>
																<fo:inline font-size="6pt" font-style="italic">
																	<xsl:text>Pour visite, si différent</xsl:text>
																</fo:inline>
																<fo:inline font-size="6pt">
																	<xsl:text>)</xsl:text> 
																</fo:inline>
															</fo:block>
															<fo:block>
																<xsl:text> </xsl:text>	
															</fo:block>
															<fo:block>
																<xsl:text>&#160;</xsl:text>	<xsl:value-of select="//ctPerOwnerName"/>		
															</fo:block>
															
														</fo:table-cell>
														<fo:table-cell height="0.33292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">

														<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.33292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in"  display-align="before">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Résidence</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="//ctPerOwnTelHome"/>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.33292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Emploi</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="//ctPerOwnTelWork"/>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.33292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="2pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Cellulaire</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block>
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="//ctPerOwnTelCell"/>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth22" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths22" select="0.16667"/>
						<xsl:variable name="factor22">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths22 &gt; 0.00000 and $sumcolumnwidths22 &gt; $tablewidth22">
									<xsl:value-of select="$tablewidth22 div $sumcolumnwidths22"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns22" select="1"/>
						<xsl:variable name="defaultcolumnwidth22">
							<xsl:choose>
								<xsl:when test="$factor22 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns22 &gt; 0">
									<xsl:value-of select="($tablewidth22 - $sumcolumnwidths22) div $defaultcolumns22"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth22_0" select="0.16667 * $factor22"/>
						<xsl:variable name="columnwidth22_1" select="$defaultcolumnwidth22"/>
						<fo:table width="{$tablewidth22}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth22_0}in"/>
							<fo:table-column column-width="{$columnwidth22_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.04167in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.04167in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>IDENTIFICATION DU CONSTRUCTEUR ACCRÉDITÉ</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth23" select="$columnwidth22_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths23" select="2.48958 + 1.53125 + 1.52083 + 1.25000 + 2.18750"/>
											<xsl:variable name="factor23">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths23 &gt; 0.00000 and $sumcolumnwidths23 &gt; $tablewidth23">
														<xsl:value-of select="$tablewidth23 div $sumcolumnwidths23"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns23" select="1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1"/>
											<xsl:variable name="defaultcolumnwidth23">
												<xsl:choose>
													<xsl:when test="$factor23 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns23 &gt; 0">
														<xsl:value-of select="($tablewidth23 - $sumcolumnwidths23) div $defaultcolumns23"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth23_0" select="2.48958 * $factor23"/>
											<xsl:variable name="columnwidth23_1" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_2" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_3" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_4" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_5" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_6" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_7" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_8" select="1.53125 * $factor23"/>
											<xsl:variable name="columnwidth23_9" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_10" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_11" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_12" select="$defaultcolumnwidth23"/>
											<xsl:variable name="columnwidth23_13" select="1.52083 * $factor23"/>
											<xsl:variable name="columnwidth23_14" select="1.25000 * $factor23"/>
											<xsl:variable name="columnwidth23_15" select="2.18750 * $factor23"/>
											<fo:table width="{$tablewidth23}in" space-before.optimum="2pt" space-after.optimum="1pt" border-collapse="separate" font-size="8pt" color="black" display-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in"   border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
												<fo:table-column column-width="1.59009in"/>
												<fo:table-column column-width="1.10999in"/>
												<fo:table-column column-width="0.07999in"/>
												<fo:table-column column-width="1.65400in"/>
												<fo:table-body>
													<fo:table-row  >
														<fo:table-cell number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in"  padding-left="0.03000in" height="0.34375in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline  font-size="6pt">
																	<xsl:text>Nom du constructeur</xsl:text>
																</fo:inline>
															</fo:block>
															<fo:block>
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" height="0.34375in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>&#160;&#160;&#160;No d&apos;accréditation</xsl:text>
																</fo:inline>
															</fo:block>
															<fo:block>
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.34375in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="0pt" padding-bottom="0pt">
																	<fo:block padding-top="1pt" padding-bottom="0pt">
																		<fo:inline font-size="6pt">
																			<xsl:text>&#160;No de téléphone (</xsl:text>
																		</fo:inline>
																		<fo:inline font-size="6pt" font-style="italic">
																			<xsl:text>Résidence</xsl:text>
																		</fo:inline>
																		<fo:inline font-size="6pt">
																			<xsl:text>)</xsl:text>
																		</fo:inline>
																	</fo:block>
																	<fo:block padding-top="1pt" padding-bottom="1pt">
																		<fo:inline  font-size="6">
																			<xsl:text></xsl:text>
																		</fo:inline>
																</fo:block>
	
																</fo:block>
																</fo:table-cell>
														<fo:table-cell height="0.34375in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="1pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																
																<fo:inline font-size="6pt" font-style="italic">
																			<xsl:text>Emploi</xsl:text>
																		</fo:inline>
																		<fo:inline font-size="6pt">
																			<xsl:text>)</xsl:text>
																		</fo:inline>
																	</fo:block>
																<fo:block padding-top="1pt" padding-bottom="1pt">
																	<fo:inline  font-size="6">
																		<xsl:text></xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.34375in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block padding-top="1pt" padding-bottom="0pt">
																	<fo:inline font-size="6pt">
																		<xsl:text>&#160;No de téléphone (</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt" font-style="italic">
																		<xsl:text>Cellulaire</xsl:text>
																	</fo:inline>
																	<fo:inline font-size="6pt">
																		<xsl:text>)</xsl:text>
																	</fo:inline>
																</fo:block>
																<fo:block padding-top="1pt" padding-bottom="1pt">
																	<fo:inline  font-size="6">
																		<xsl:text></xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth27" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths27" select="0.16667"/>
						<xsl:variable name="factor27">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths27 &gt; 0.00000 and $sumcolumnwidths27 &gt; $tablewidth27">
									<xsl:value-of select="$tablewidth27 div $sumcolumnwidths27"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns27" select="1"/>
						<xsl:variable name="defaultcolumnwidth27">
							<xsl:choose>
								<xsl:when test="$factor27 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns27 &gt; 0">
									<xsl:value-of select="($tablewidth27 - $sumcolumnwidths27) div $defaultcolumns27"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth27_0" select="0.16667 * $factor27"/>
						<xsl:variable name="columnwidth27_1" select="$defaultcolumnwidth27"/>
						<fo:table width="{$tablewidth27}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth27_0}in"/>
							<fo:table-column column-width="{$columnwidth27_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>3</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>ADRESSE DE LA PROPRIÉTÉ À ÉVALUER OU À INSPECTER</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block >
											<xsl:variable name="tablewidth28" select="$columnwidth27_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths28" select="0.83333 + 1.47917 + 1.79167 + 1.10417 + 0.15625 + 0.66667 + 0.85417 + 0.67708 + 1.14583 + 0.01042"/>
											<xsl:variable name="factor28">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths28 &gt; 0.00000 and $sumcolumnwidths28 &gt; $tablewidth28">
														<xsl:value-of select="$tablewidth28 div $sumcolumnwidths28"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns28" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth28">
												<xsl:choose>
													<xsl:when test="$factor28 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns28 &gt; 0">
														<xsl:value-of select="($tablewidth28 - $sumcolumnwidths28) div $defaultcolumns28"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth28_0" select="0.83333 * $factor28"/>
											<xsl:variable name="columnwidth28_1" select="1.47917 * $factor28"/>
											<xsl:variable name="columnwidth28_2" select="1.79167 * $factor28"/>
											<xsl:variable name="columnwidth28_3" select="$defaultcolumnwidth28"/>
											<xsl:variable name="columnwidth28_4" select="$defaultcolumnwidth28"/>
											<xsl:variable name="columnwidth28_5" select="1.10417 * $factor28"/>
											<xsl:variable name="columnwidth28_6" select="0.15625 * $factor28"/>
											<xsl:variable name="columnwidth28_7" select="0.66667 * $factor28"/>
											<xsl:variable name="columnwidth28_8" select="0.85417 * $factor28"/>
											<xsl:variable name="columnwidth28_9" select="0.67708 * $factor28"/>
											<xsl:variable name="columnwidth28_10" select="1.14583 * $factor28"/>
											<xsl:variable name="columnwidth28_11" select="0.01042 * $factor28"/>
											<fo:table width="{$tablewidth28}in"  border-collapse="separate" font-size="6pt" color="black" display-align="center" border-top-style="double" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" space-after.optimum="1pt" space-before.optimum="1pt">
												<fo:table-column column-width="0.29500in"/>
												<fo:table-column column-width="0.29500in"/>
												<fo:table-column column-width="1.00009in"/>
												<fo:table-column column-width="1.00009in"/>
												<fo:table-column column-width="0.20000in"/>
												<fo:table-column column-width="0.20000in"/>
												<fo:table-column column-width="0.42700in"/>
												<fo:table-column column-width="0.42700in"/>
												<fo:table-column column-width="0.35000in"/>
												<fo:table-column column-width="0.55000in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.12750in" number-columns-spanned="2" padding-left="0.03000in" padding-top="2pt">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:text>No civique</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														
														<fo:table-cell height="0.12750in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in"  padding-left="0.03000in" number-columns-spanned="2" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="2pt">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:text>Rue</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12750in" padding-left="0.03000in" number-columns-spanned="2" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="2pt">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:text>No app.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														
														<fo:table-cell height="0.12750in" padding-left="0.03000in" number-columns-spanned="2" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="2pt">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:text>Ville</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12750in" padding-left="0.03000in" number-columns-spanned="2"  padding-top="2pt">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:text>Arrondissement</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.09750in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in"  padding-left="0.03000in" number-columns-spanned="2" >
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//propNo"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09750in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in"  padding-left="0.03000in" number-columns-spanned="2"  border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
                                                             <fo:block ><fo:inline font-size="6pt" >
																	<xsl:value-of select="//propStreet"/>	</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09750in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in"  padding-left="0.03000in" number-columns-spanned="2">
                                                            <fo:block  >
																 <fo:inline font-size="6pt">																	
																	<xsl:value-of select="//propApt"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09750in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" number-columns-spanned="2">
														<fo:block >
															<fo:inline font-size="6pt">
																	<xsl:value-of select="//propCity"/>
															</fo:inline>
															</fo:block>
														</fo:table-cell>	
														<fo:table-cell height="0.09750in" border-bottom-color="black" border-bottom-width="0.01042in" border-bottom-style="solid"  padding-left="0.03000in" number-columns-spanned="2">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
														<xsl:variable name="tablewidth29" select="$columnwidth28_0 * 1.00000 + $columnwidth28_1 * 1.00000 + $columnwidth28_2 * 1.00000 + $columnwidth28_3 * 1.00000 + $columnwidth28_4 * 1.00000 + $columnwidth28_5 * 1.00000 + $columnwidth28_6 * 1.00000 + $columnwidth28_7 * 1.00000 - 0.03000"/>
																<xsl:variable name="sumcolumnwidths29" select="2.29167 + 2.10417 + 7.11458 + 2.44792"/>
																<xsl:variable name="factor29">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths29 &gt; 0.00000 and $sumcolumnwidths29 &gt; $tablewidth29">
																			<xsl:value-of select="$tablewidth29 div $sumcolumnwidths29"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth29_0" select="2.29167 * $factor29"/>
																<xsl:variable name="columnwidth29_1" select="2.10417 * $factor29"/>
																<xsl:variable name="columnwidth29_2" select="7.11458 * $factor29"/>
																<xsl:variable name="columnwidth29_3" select="2.44792 * $factor29"/>
													<fo:table-row>
														<fo:table-cell height="0.09750in" number-columns-spanned="3"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in"  padding-top="2pt">
																	<fo:block >
																		<fo:block>
																			<fo:inline font-size="6pt">
																				<xsl:text>No de lot</xsl:text>
																			</fo:inline>
																		</fo:block>
																	</fo:block>
																</fo:table-cell>	
																<fo:table-cell height="0.09750in" number-columns-spanned="3"   border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in"  padding-top="2pt">	
																	<fo:block >
																		<fo:inline font-size="6pt" >
																			<xsl:text>Paroisse cadastrale</xsl:text>
																		</fo:inline>
																	</fo:block>
																</fo:table-cell>
														<fo:table-cell height="0.09750in"  padding-left="0.03000in"    number-columns-spanned="2" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="2pt">
															<fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>Code postal</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.07750in"  padding-left="0.03000in"   number-columns-spanned="2" >
															<fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>Province</xsl:text>
																</fo:inline>
															</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
														<fo:table-cell height="0.07750in" number-columns-spanned="3" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
																	<fo:block >
																		<fo:inline font-size="6pt">
																			<xsl:value-of select="//propLotNo"/>	
																		</fo:inline>
																	</fo:block>
																</fo:table-cell>	
													<fo:table-cell height="0.07750in" number-columns-spanned="3" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">	
														<fo:block >
															<fo:inline font-size="6pt" >
															</fo:inline>
														</fo:block>
																		
														</fo:table-cell>
														<fo:table-cell height="0.07750in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in"   number-columns-spanned="2" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//propPostCode"/>	</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.07750in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in"  padding-top="0.05000in"  number-columns-spanned="2">
															<fo:block>
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//propProvince"/>
																</fo:inline>
															</fo:block>
													</fo:table-cell>
												</fo:table-row>		
													
<xsl:variable name="tablewidth30" select="$columnwidth28_0 * 1.00000 + $columnwidth28_1 * 1.00000 + $columnwidth28_2 * 1.00000 + $columnwidth28_3 * 1.00000 + $columnwidth28_4 * 1.00000 + $columnwidth28_5 * 1.00000 + $columnwidth28_6 * 1.00000 + $columnwidth28_7 * 1.00000 + $columnwidth28_8 * 1.00000 + $columnwidth28_9 * 1.00000 + $columnwidth28_10 * 1.00000 + $columnwidth28_11 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths30" select="0.86458 + 0.18750 + 0.61458 + 0.09375 + 0.55208 + 0.10417 + 0.71875 + 0.16667 + 0.77083"/>
																<xsl:variable name="factor30">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths30 &gt; 0.00000 and $sumcolumnwidths30 &gt; $tablewidth30">
																			<xsl:value-of select="$tablewidth30 div $sumcolumnwidths30"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns30" select="1 + 1"/>
																<xsl:variable name="defaultcolumnwidth30">
																	<xsl:choose>
																		<xsl:when test="$factor30 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns30 &gt; 0">
																			<xsl:value-of select="($tablewidth30 - $sumcolumnwidths30) div $defaultcolumns30"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth30_0" select="0.54458 * $factor30"/>
																<xsl:variable name="columnwidth30_1" select="0.09375 * $factor30"/>
																<xsl:variable name="columnwidth30_2" select="0.60458 * $factor30"/>
																<xsl:variable name="columnwidth30_3" select="0.09375 * $factor30"/>
																<xsl:variable name="columnwidth30_4" select="0.54208 * $factor30"/>
																<xsl:variable name="columnwidth30_5" select="0.09375 * $factor30"/>
																<xsl:variable name="columnwidth30_6" select="0.70875 * $factor30"/>
																<xsl:variable name="columnwidth30_7" select="0.09375 * $factor30"/>
																<xsl:variable name="columnwidth30_8" select="0.54083 * $factor30"/>
																<xsl:variable name="columnwidth30_9" select="$defaultcolumnwidth30"/>
																<xsl:variable name="columnwidth30_10" select="$defaultcolumnwidth30"/>
																<xsl:variable name="tablewidth31" select="$columnwidth30_9 * 1.00000 + $columnwidth30_10 * 1.00000 - 0.03000"/>
																					<xsl:variable name="sumcolumnwidths31" select="0.60417 + 0.72917 + 0.12500 + 1.17708"/>
																					<xsl:variable name="factor31">
																						<xsl:choose>
																							<xsl:when test="$sumcolumnwidths31 &gt; 0.00000 and $sumcolumnwidths31 &gt; $tablewidth31">
																								<xsl:value-of select="$tablewidth31 div $sumcolumnwidths31"/>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="1.000"/>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:variable>
																					<xsl:variable name="defaultcolumns31" select="1"/>
																					<xsl:variable name="defaultcolumnwidth31">
																						<xsl:choose>
																							<xsl:when test="$factor31 &lt; 1.000">
																								<xsl:value-of select="0.000"/>
																							</xsl:when>
																							<xsl:when test="$defaultcolumns31 &gt; 0">
																								<xsl:value-of select="($tablewidth31 - $sumcolumnwidths31) div $defaultcolumns31"/>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="0.000"/>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:variable>
																						<xsl:variable name="columnwidth31_0" select="0.30208 * $factor31"/>
																					<xsl:variable name="columnwidth31_1" select="0.72917 * $factor31"/>
																					<xsl:variable name="columnwidth31_2" select="0.12500 * $factor31"/>
																					<xsl:variable name="columnwidth31_3" select="1.17708 * $factor31"/>
																					<xsl:variable name="columnwidth31_4" select="$defaultcolumnwidth31"/>													
													<fo:table-row>
														<fo:table-cell number-columns-spanned="4" border-bottom-style="double" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-bottom="2pt" padding-right="5pt" >
																<fo:table width="{$tablewidth30}in" space-before.optimum="0pt" space-after.optimum="4pt" border-collapse="separate" font-size="6pt" color="black" display-align="after">
																	<fo:table-column column-width="{$columnwidth30_0}in"/>
																	<fo:table-column column-width="{$columnwidth30_1}in"/>
																	<fo:table-column column-width="{$columnwidth30_2}in"/>
																	<fo:table-column column-width="{$columnwidth30_3}in"/>
																	<fo:table-column column-width="{$columnwidth30_4}in"/>
																	<fo:table-column column-width="{$columnwidth30_5}in"/>
																	<fo:table-column column-width="{$columnwidth30_6}in"/>
																	<fo:table-column column-width="{$columnwidth30_7}in"/>
																	<fo:table-column column-width="{$columnwidth30_8}in"/>
																	<fo:table-column column-width="{$columnwidth30_9}in"/>
																	<fo:table-column column-width="{$columnwidth30_10}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" >
																					<fo:inline font-size="7pt">
																						<xsl:text>Résidentiel</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																				<xsl:choose>
																					<xsl:when test="//resSingleUnit = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt"  text-align="left">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Unifamilial</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																					<fo:block padding-top="1pt" padding-bottom="1pt">
																				<xsl:choose>
																					<xsl:when test="//resDuplex = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt"  text-align="left">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Duplex</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																				<xsl:choose>
																					<xsl:when test="//resTriplex = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt"  text-align="left">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Triplex</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																				<xsl:choose>
																					<xsl:when test="//resQplex = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04250in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt"  text-align="left">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Quadruplex</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		</fo:table-body>
																	</fo:table>
																</fo:block>
															</fo:table-cell>
															<fo:table-cell height="0.04250in" number-columns-spanned="4" padding-left="0.03000in"  border-left-style="solid" border-left-color="black" border-left-width="0.01042in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" border-bottom-color="black" border-bottom-width="0.01042in" border-bottom-style="solid">
																	<fo:block padding-top="1pt" >
																		<fo:table space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center" >
																						<fo:table-column column-width="{$columnwidth31_0}in"/>
																						<fo:table-column column-width="{$columnwidth31_2}in"/>
																						<fo:table-column column-width="1.35in"/>
																						<fo:table-body>
																							<fo:table-row>
																								<fo:table-cell height="0.04250in">
																									<fo:block padding-top="1pt" padding-bottom="1pt">
																										<fo:inline font-size="6pt">
																											<xsl:text>Superficie</xsl:text>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																								<fo:table-cell height="0.04250in">
																									<fo:block padding-top="1pt" />
																								</fo:table-cell>
																								<fo:table-cell height="0.04250in" border-bottom-color="black" border-bottom-width="0.01042in" border-bottom-style="solid">
																									<fo:block padding-top="1pt" />
																								</fo:table-cell>
																							</fo:table-row>
																						</fo:table-body>
																				</fo:table>
																		</fo:block>
																</fo:table-cell>
																<fo:table-cell height="0.04250in" number-columns-spanned="2" padding-left="0.03000in" border-bottom-color="black" border-bottom-width="0.01042in" border-bottom-style="solid">
																	<fo:block padding-top="1pt" padding-bottom="5pt">
																		<fo:table space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
																			<fo:table-column column-width="0.5in"/>
																			<fo:table-column column-width="0.655in"/>
																			<fo:table-column column-width="0.0011in"/>
																				<fo:table-body>
																					<fo:table-row>
																						<fo:table-cell height="0.04250in"  >
																							<fo:block padding-top="1pt" padding-bottom="1pt">
																								<fo:inline font-size="6pt">
																									<xsl:text>Année de construction</xsl:text>
																								</fo:inline>
																							</fo:block>
																						</fo:table-cell>
																						<fo:table-cell height="0.04250in"  border-bottom-color="black" border-bottom-width="0.01042in" border-bottom-style="solid" >
																									<fo:block padding-top="1pt" padding-bottom="1pt">
																										
																									</fo:block>
																							</fo:table-cell>
																								<fo:table-cell height="0.04250in" border-bottom-color="black" border-bottom-width="0.01042in" border-bottom-style="solid">
																									<fo:block padding-top="1pt" />
																								</fo:table-cell>
																					</fo:table-row>
																				</fo:table-body>
																		</fo:table>
																	</fo:block>
																</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.01142in" number-columns-spanned="12" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth32" select="$columnwidth28_0 * 1.00000 + $columnwidth28_1 * 1.00000 + $columnwidth28_2 * 1.00000 + $columnwidth28_3 * 1.00000 + $columnwidth28_4 * 1.00000 + $columnwidth28_5 * 1.00000 + $columnwidth28_6 * 1.00000 + $columnwidth28_7 * 1.00000 + $columnwidth28_8 * 1.00000 + $columnwidth28_9 * 1.00000 + $columnwidth28_10 * 1.00000 + $columnwidth28_11 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths32" select="0.52083 + 0.16667 + 0.96875 + 0.19792 + 0.98958 + 0.15625 + 1.33333 + 0.33333 + 0.75000 + 0.21875 + 0.72917 + 0.19792 + 0.60417 + 0.25000 + 0.20833 + 0.26042 + 0.65625 + 1.61458"/>
																<xsl:variable name="factor32">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths32 &gt; 0.00000 and $sumcolumnwidths32 &gt; $tablewidth32">
																			<xsl:value-of select="$tablewidth32 div $sumcolumnwidths32"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns32" select="1"/>
																<xsl:variable name="defaultcolumnwidth32">
																	<xsl:choose>
																		<xsl:when test="$factor32 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns32 &gt; 0">
																			<xsl:value-of select="($tablewidth32 - $sumcolumnwidths32) div $defaultcolumns32"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth32_0" select="0.84678 * $factor32"/>
																<xsl:variable name="columnwidth32_1" select="0.09875 * $factor32"/>
																<xsl:variable name="columnwidth32_2" select="0.93458 * $factor32"/>
																<xsl:variable name="columnwidth32_3" select="0.09875 * $factor32"/>
																<xsl:variable name="columnwidth32_4" select="0.84958 * $factor32"/>
																<xsl:variable name="columnwidth32_5" select="0.09875 * $factor32"/>
																<xsl:variable name="columnwidth32_6" select="1.09333 * $factor32"/>
																<xsl:variable name="columnwidth32_7" select="0.09875 * $factor32"/>
																<xsl:variable name="columnwidth32_8" select="0.83000 * $factor32"/>
																<xsl:variable name="columnwidth32_9" select="0.09875 * $factor32"/>
																<xsl:variable name="columnwidth32_10" select="0.93108 * $factor32"/>
																<xsl:variable name="columnwidth32_11" select="0.09875 * $factor32"/>
																<xsl:variable name="columnwidth32_12" select="0.68934 * $factor32"/>
																<xsl:variable name="columnwidth32_13" select="0.09875 * $factor32"/>
																<xsl:variable name="columnwidth32_14" select="0.26542 * $factor32"/>
																<xsl:variable name="columnwidth32_15" select="0.20000 * $factor32"/>
																<xsl:variable name="columnwidth32_16" select="0.65625 * $factor32"/>
																<xsl:variable name="columnwidth32_17" select="1.72408 * $factor32"/>
																<xsl:variable name="columnwidth32_18" select="$defaultcolumnwidth32"/>
																<fo:table width="{$tablewidth32}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth32_0}in"/>
																	<fo:table-column column-width="{$columnwidth32_1}in"/>
																	<fo:table-column column-width="{$columnwidth32_2}in"/>
																	<fo:table-column column-width="{$columnwidth32_3}in"/>
																	<fo:table-column column-width="{$columnwidth32_4}in"/>
																	<fo:table-column column-width="{$columnwidth32_5}in"/>
																	<fo:table-column column-width="{$columnwidth32_6}in"/>
																	<fo:table-column column-width="{$columnwidth32_7}in"/>
																	<fo:table-column column-width="{$columnwidth32_8}in"/>
																	<fo:table-column column-width="{$columnwidth32_9}in"/>
																	<fo:table-column column-width="{$columnwidth32_10}in"/>
																	<fo:table-column column-width="{$columnwidth32_11}in"/>
																	<fo:table-column column-width="{$columnwidth32_12}in"/>
																	<fo:table-column column-width="{$columnwidth32_13}in"/>
																	<fo:table-column column-width="{$columnwidth32_14}in"/>
																	<fo:table-column column-width="{$columnwidth32_15}in"/>
																	<fo:table-column column-width="{$columnwidth32_16}in"/>
																	<fo:table-column column-width="{$columnwidth32_17}in"/>
																	<fo:table-column column-width="0.0611in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell height="0.04167in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="7pt">
																						<xsl:text>Autres</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in">
																				<xsl:choose>
																<xsl:when test="//otherMultiUnit= '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Multifamilial</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in">
																	<xsl:choose>
																<xsl:when test="//commUnit = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Commercial</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in">
																			
																				<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;S/Commercial</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in">
																				<xsl:choose>
																				
																<xsl:when test="//indUnit = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Industriel</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in">
																				<xsl:choose>
																<xsl:when test="//vacLot = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Terrain</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in">
																			<xsl:choose>
																<xsl:when test="//farmUnit = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Ferme</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in">
																			<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" padding-left="0.03000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Autres</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in">
																				<fo:block padding-top="1pt" />
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" display-align="after" >
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt" font-style="italic">
																						<xsl:text>&#160;&#160;&#160;&#160;&#160;Préciser</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.04167in" padding-right="4pt" border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.01042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block></fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth33" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths33" select="0.16667"/>
						<xsl:variable name="factor33">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths33 &gt; 0.00000 and $sumcolumnwidths33 &gt; $tablewidth33">
									<xsl:value-of select="$tablewidth33 div $sumcolumnwidths33"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns33" select="1"/>
						<xsl:variable name="defaultcolumnwidth33">
							<xsl:choose>
								<xsl:when test="$factor33 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns33 &gt; 0">
									<xsl:value-of select="($tablewidth33 - $sumcolumnwidths33) div $defaultcolumns33"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth33_0" select="0.16667 * $factor33"/>
						<xsl:variable name="columnwidth33_1" select="$defaultcolumnwidth33"/>
						<fo:table width="{$tablewidth33}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth33_0}in"/>
							<fo:table-column column-width="{$columnwidth33_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.07292in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>4</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.07292in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>MONTANT DU PRÊT </xsl:text>
											</fo:inline>
											<fo:inline font-size="7pt">
												<xsl:text>(Hypothèque et garantie) </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth34" select="$columnwidth33_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths34" select="2.63542 + 4.12500 + 4.17708 + 2.46875"/>
											<xsl:variable name="factor34">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths34 &gt; 0.00000 and $sumcolumnwidths34 &gt; $tablewidth34">
														<xsl:value-of select="$tablewidth34 div $sumcolumnwidths34"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns34" select="1"/>
											<xsl:variable name="defaultcolumnwidth34">
												<xsl:choose>
													<xsl:when test="$factor34 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns34 &gt; 0">
														<xsl:value-of select="($tablewidth34 - $sumcolumnwidths34) div $defaultcolumns34"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth34_0" select="2.63542 * $factor34"/>
											<xsl:variable name="columnwidth34_1" select="4.12500 * $factor34"/>
											<xsl:variable name="columnwidth34_2" select="4.17708 * $factor34"/>
											<xsl:variable name="columnwidth34_3" select="2.46875 * $factor34"/>
											<xsl:variable name="columnwidth34_4" select="$defaultcolumnwidth34"/>
											<fo:table width="{$tablewidth34}in" space-before.optimum="1pt" space-after.optimum="1pt" border-collapse="separate" font-size="6pt" color="black" display-align="center" border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
												<fo:table-column column-width="{$columnwidth34_0}in"/>
												<fo:table-column column-width="{$columnwidth34_1}in"/>
												<fo:table-column column-width="{$columnwidth34_2}in"/>
												<fo:table-column column-width="{$columnwidth34_3}in"/>
												<fo:table-column column-width="{$columnwidth34_4}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell display-align="before" height="0.10625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in"> 
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:text>Prix d&apos;achat </xsl:text>
																</fo:inline>
																<fo:inline font-style="italic">
																	<xsl:text>(si applicable)</xsl:text>	
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.10625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in">
															<fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>1er rang ou solde des prêts</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.10625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:text>2e rang ou évolutif avance add.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.10625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in">
															<fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>Évaluation municipale</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.10625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" padding-top="0.05000in">
															<fo:block />
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell display-align="after" height="0.12417in"   border-right-style="solid" border-right-color="black" border-right-width="0.01042in"  padding-left="0.03000in">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//purPrice"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.12417in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//firstMtg"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.12417in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.07000in" padding-left="0.03000in">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//secondMtg"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.12417in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.07000in" padding-left="0.03000in">
															<fo:block >
																<fo:inline font-size="6pt">
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" height="0.12417in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth35" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths35" select="0.16667"/>
						<xsl:variable name="factor35">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths35 &gt; 0.00000 and $sumcolumnwidths35 &gt; $tablewidth35">
									<xsl:value-of select="$tablewidth35 div $sumcolumnwidths35"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns35" select="1"/>
						<xsl:variable name="defaultcolumnwidth35">
							<xsl:choose>
								<xsl:when test="$factor35 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns35 &gt; 0">
									<xsl:value-of select="($tablewidth35 - $sumcolumnwidths35) div $defaultcolumns35"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth35_0" select="0.16667 * $factor35"/>
						<xsl:variable name="columnwidth35_1" select="$defaultcolumnwidth35"/>
						<fo:table width="{$tablewidth35}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth35_0}in"/>
							<fo:table-column column-width="{$columnwidth35_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.06250in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>5</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.06250in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>INSPECTION</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" />
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" >
											<xsl:variable name="tablewidth36" select="$columnwidth35_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths36" select="0.86458 + 0.37500 + 1.02083 + 1.43750 + 2.32292 + 0.54167"/>
											<xsl:variable name="factor36">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths36 &gt; 0.00000 and $sumcolumnwidths36 &gt; $tablewidth36">
														<xsl:value-of select="$tablewidth36 div $sumcolumnwidths36"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns36" select="1"/>
											<xsl:variable name="defaultcolumnwidth36">
												<xsl:choose>
													<xsl:when test="$factor36 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns36 &gt; 0">
														<xsl:value-of select="($tablewidth36 - $sumcolumnwidths36) div $defaultcolumns36"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth36_0" select="0.86458 * $factor36"/>
											<xsl:variable name="columnwidth36_1" select="0.37500 * $factor36"/>
											<xsl:variable name="columnwidth36_2" select="1.02083 * $factor36"/>
											<xsl:variable name="columnwidth36_3" select="1.61750 * $factor36"/>
											<xsl:variable name="columnwidth36_4" select="$defaultcolumnwidth36"/>
											<xsl:variable name="columnwidth36_5" select="2.14292 * $factor36"/>
											<xsl:variable name="columnwidth36_6" select="0.54167 * $factor36"/>
											<fo:table width="{$tablewidth36}in" space-before.optimum="1pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
												<fo:table-column column-width="{$columnwidth36_0}in"/>
												<fo:table-column column-width="{$columnwidth36_1}in"/>
												<fo:table-column column-width="{$columnwidth36_2}in"/>
												<fo:table-column column-width="{$columnwidth36_3}in"/>
												<fo:table-column column-width="{$columnwidth36_4}in"/>
												<fo:table-column column-width="{$columnwidth36_5}in"/>
												<fo:table-column column-width="{$columnwidth36_6}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.31875in" padding-left="0.03000in"  >
															<fo:block padding-top="1pt" >
																<fo:inline font-size="6pt">
																	<xsl:text>No d&apos;inspection</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.31875in" >
															<fo:block padding-top="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.31875in" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth37" select="$columnwidth36_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths37" select="0.10417 + 0.29167 + 0.13542"/>
																<xsl:variable name="factor37">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths37 &gt; 0.00000 and $sumcolumnwidths37 &gt; $tablewidth37">
																			<xsl:value-of select="$tablewidth37 div $sumcolumnwidths37"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns37" select="1"/>
																<xsl:variable name="defaultcolumnwidth37">
																	<xsl:choose>
																		<xsl:when test="$factor37 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns37 &gt; 0">
																			<xsl:value-of select="($tablewidth37 - $sumcolumnwidths37) div $defaultcolumns37"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth37_0" select="0.10417 * $factor37"/>
																<xsl:variable name="columnwidth37_1" select="0.29167 * $factor37"/>
																<xsl:variable name="columnwidth37_2" select="0.10417 * $factor37"/>
																<xsl:variable name="columnwidth37_3" select="$defaultcolumnwidth37"/>
																<fo:table width="{$tablewidth37}in" space-before.optimum="0pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="center" height="0.3000in">
																	<fo:table-column column-width="{$columnwidth37_0}in"/>
																	<fo:table-column column-width="{$columnwidth37_1}in"/>
																	<fo:table-column column-width="{$columnwidth37_2}in"/>
																	<fo:table-column column-width="{$columnwidth37_3}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="4" >
																				<fo:block padding-top="1pt" >
																					<fo:inline font-size="6pt" font-weight="bold">
																						<xsl:text>Inspection finale</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell>
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" >
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Oui</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																			<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" text-align="left">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Non</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.31875in" padding-left="0.03000in">
															<fo:block padding-top="1pt" >
																<fo:inline font-size="6pt">
																	<xsl:text>Date requise de la visite</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.31875in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.31875in" padding-left="0.03000in">
															<fo:block padding-top="1pt" >
																<fo:inline font-size="6pt">
																	<xsl:text>No de dossier estimé sur les plans</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.31875in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" />
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth38" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths38" select="0.16667 + 7.94792"/>
						<xsl:variable name="factor38">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths38 &gt; 0.00000 and $sumcolumnwidths38 &gt; $tablewidth38">
									<xsl:value-of select="$tablewidth38 div $sumcolumnwidths38"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth38_0" select="0.16667 * $factor38"/>
						<xsl:variable name="columnwidth38_1" select="7.94792 * $factor38"/>
						<fo:table width="{$tablewidth38}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth38_0}in"/>
							<fo:table-column column-width="{$columnwidth38_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>6</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>RECOUVREMENT / RÉALISATION</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth39" select="$columnwidth38_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths39" select="7.97917 + 4.57292"/>
											<xsl:variable name="factor39">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths39 &gt; 0.00000 and $sumcolumnwidths39 &gt; $tablewidth39">
														<xsl:value-of select="$tablewidth39 div $sumcolumnwidths39"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth39_0" select="7.97917 * $factor39"/>
											<xsl:variable name="columnwidth39_1" select="4.57292 * $factor39"/>
											<fo:table width="{$tablewidth39}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in"  border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" >
												<fo:table-column column-width="{$columnwidth39_0}in"/>
												<fo:table-column column-width="{$columnwidth39_1}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell  number-columns-spanned="2"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth40" select="$columnwidth39_0 * 1.00000 + $columnwidth39_1 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths40" select="0.33333 + 1.12500 + 0.25000 + 0.70833 + 0.31250 + 1.10417 + 0.22917 + 1.14583 + 2.86458"/>
																<xsl:variable name="factor40">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths40 &gt; 0.00000 and $sumcolumnwidths40 &gt; $tablewidth40">
																			<xsl:value-of select="$tablewidth40 div $sumcolumnwidths40"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns40" select="1"/>
																<xsl:variable name="defaultcolumnwidth40">
																	<xsl:choose>
																		<xsl:when test="$factor40 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns40 &gt; 0">
																			<xsl:value-of select="($tablewidth40 - $sumcolumnwidths40) div $defaultcolumns40"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth40_0" select="0.19875 * $factor40"/>
																<xsl:variable name="columnwidth40_1" select="0.65500 * $factor40"/>
																<xsl:variable name="columnwidth40_2" select="0.19875 * $factor40"/>
																<xsl:variable name="columnwidth40_3" select="0.42500 * $factor40"/>
																<xsl:variable name="columnwidth40_4" select="0.19875 * $factor40"/>
																<xsl:variable name="columnwidth40_5" select="0.70500 * $factor40"/>
																<xsl:variable name="columnwidth40_6" select="0.19875 * $factor40"/>
																<xsl:variable name="columnwidth40_7" select="0.80500 * $factor40"/>
																<xsl:variable name="columnwidth40_8" select="2.86458 * $factor40"/>
																<xsl:variable name="columnwidth40_9" select="$defaultcolumnwidth40"/>
																<fo:table width="{$tablewidth40}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth40_0}in"/>
																	<fo:table-column column-width="{$columnwidth40_1}in"/>
																	<fo:table-column column-width="{$columnwidth40_2}in"/>
																	<fo:table-column column-width="{$columnwidth40_3}in"/>
																	<fo:table-column column-width="{$columnwidth40_4}in"/>
																	<fo:table-column column-width="{$columnwidth40_5}in"/>
																	<fo:table-column column-width="{$columnwidth40_6}in"/>
																	<fo:table-column column-width="{$columnwidth40_7}in"/>
																	<fo:table-column column-width="{$columnwidth40_8}in"/>
																	<fo:table-column column-width="{$columnwidth40_9}in"/>
																	<fo:table-body>
																		<fo:table-row height="0.21000in">
																			<fo:table-cell  >
																				<fo:block padding-bottom="1pt" padding-left="9pt" text-align="right">
<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>	
																				<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>
																			</fo:table-cell>
																			<fo:table-cell  >
																				<fo:block padding-top="4pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Nouvelle demande</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell  >
																				<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="9pt" text-align="right">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>	
																				<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																				</fo:block>
																			</fo:table-cell >
																			<fo:table-cell  >
																				<fo:block padding-top="4pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Mise à jour</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell >
																			
																			<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="9pt" text-align="right">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>	
																				<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell  >
																				<fo:block padding-top="4pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Propriété occupée</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			
																			
																			<fo:table-cell >
																			
																			<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="9pt" text-align="right">
<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>	
																			<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell  >
																				<fo:block padding-top="4pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Propriété vacante</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			
																			<fo:table-cell  >
																				<fo:block padding-top="4pt" >
																					<fo:inline font-size="6pt" font-weight="bold">
																						<xsl:text>Délai à respecter:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell >
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.21875in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>&#160;&#160;&#160;&#160; Évaluation antérieure</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<xsl:variable name="tablewidth41" select="$columnwidth39_0 * 1.00000 - 0.03000"/>
																<xsl:variable name="sumcolumnwidths41" select="0.13542 + 0.26042 + 0.12500 + 0.26042 + 1.22917 + 0.52083 + 1.25000"/>
																<xsl:variable name="factor41">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths41 &gt; 0.00000 and $sumcolumnwidths41 &gt; $tablewidth41">
																			<xsl:value-of select="$tablewidth41 div $sumcolumnwidths41"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth41_0" select="0.18875 * $factor41"/>
																<xsl:variable name="columnwidth41_1" select="0.21312 * $factor41"/>
																<xsl:variable name="columnwidth41_2" select="0.19875 * $factor41"/>
																<xsl:variable name="columnwidth41_3" select="0.23312 * $factor41"/>
																<xsl:variable name="columnwidth41_4" select="1.08999 * $factor41"/>
																<xsl:variable name="columnwidth41_5" select="0.89583 * $factor41"/>
																<xsl:variable name="columnwidth41_6" select="1.05000 * $factor41"/>
																<fo:table width="{$tablewidth41}in" space-before.optimum="0pt" space-after.optimum="0pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth41_0}in"/>
																	<fo:table-column column-width="{$columnwidth41_1}in"/>
																	<fo:table-column column-width="{$columnwidth41_2}in"/>
																	<fo:table-column column-width="{$columnwidth41_3}in"/>
																	<fo:table-column column-width="{$columnwidth41_4}in"/>
																	<fo:table-column column-width="{$columnwidth41_5}in"/>
																	<fo:table-column column-width="{$columnwidth41_6}in"/>
																	<fo:table-body font-size="10pt">
																		<fo:table-row>
																			<fo:table-cell height="0.25000in" >
																				<fo:block padding-top="3pt" padding-bottom="1pt" padding-left="3pt" text-align="center">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text>&#160;&#160; </xsl:text>
																					</fo:inline>	
																			<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.25000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Non</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			
																			
																			<fo:table-cell height="0.25000in" >
																			
																			<fo:block padding-top="3pt" padding-bottom="1pt" padding-left="9pt" text-align="right">
<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>	
																				<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.25000in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="2pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Oui</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.01042in" text-align="right">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt" font-style="italic">
																						<xsl:text>Si oui, indiquer </xsl:text>
																					</fo:inline>
																					<fo:inline font-size="6pt">
																						<xsl:text>No de dossier</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.01042in"  display-align="center">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<xsl:text> ______________</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.01042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>ou annexer la copie.</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.21875in">
															<fo:block padding-top="2pt"  >
															 
																<fo:inline font-size="6.5pt" font-weight="bold">
																	<xsl:text>&#160;&#160;Compléter le nom de la personne ressource à la section 1</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth42" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths42" select="0.16667 + 0.86458 + 5.91667"/>
						<xsl:variable name="factor42">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths42 &gt; 0.00000 and $sumcolumnwidths42 &gt; $tablewidth42">
									<xsl:value-of select="$tablewidth42 div $sumcolumnwidths42"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns42" select="1"/>
						<xsl:variable name="defaultcolumnwidth42">
							<xsl:choose>
								<xsl:when test="$factor42 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns42 &gt; 0">
									<xsl:value-of select="($tablewidth42 - $sumcolumnwidths42) div $defaultcolumns42"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth42_0" select="0.16667 * $factor42"/>
						<xsl:variable name="columnwidth42_1" select="0.86458 * $factor42"/>
						<xsl:variable name="columnwidth42_2" select="5.91667 * $factor42"/>
						<xsl:variable name="columnwidth42_3" select="$defaultcolumnwidth42"/>
						<fo:table width="{$tablewidth42}in" space-before.optimum="0pt" border-collapse="separate" color="black" display-align="center" space-after.optimum="0pt" padding-top="1pt" padding-bottom="1pt">
							<fo:table-column column-width="{$columnwidth42_0}in"/>
							<fo:table-column column-width="{$columnwidth42_1}in"/>
							<fo:table-column column-width="{$columnwidth42_2}in"/>
							<fo:table-column column-width="{$columnwidth42_3}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>7</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>INSTRUCTIONS</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in " display-align="after">
										<fo:block padding-top="1pt" font-size="6pt">
										<xsl:text>&#160;</xsl:text><xsl:value-of select="//specInstrn"/></fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row>
									<fo:table-cell number-columns-spanned="4">
										<fo:block>
												<xsl:variable name="tablewidth43" select="$maxwidth * 1.00000"/>
												<xsl:variable name="sumcolumnwidths43" select="0.13542 + 4.11458 + 0.12500 + 1.94792 + 0.29167"/>
												<xsl:variable name="factor43">
													<xsl:choose>
														<xsl:when test="$sumcolumnwidths43 &gt; 0.00000 and $sumcolumnwidths43 &gt; $tablewidth43">
															<xsl:value-of select="$tablewidth43 div $sumcolumnwidths43"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="1.000"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:variable>
												<xsl:variable name="defaultcolumns43" select="1"/>
												<xsl:variable name="defaultcolumnwidth43">
													<xsl:choose>
														<xsl:when test="$factor43 &lt; 1.000">
															<xsl:value-of select="0.000"/>
														</xsl:when>
														<xsl:when test="$defaultcolumns43 &gt; 0">
															<xsl:value-of select="($tablewidth43 - $sumcolumnwidths43) div $defaultcolumns43"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="0.000"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:variable>
												<xsl:variable name="columnwidth43_0" select="0.13542 * $factor43"/>
												<xsl:variable name="columnwidth43_1" select="4.11458 * $factor43"/>
												<xsl:variable name="columnwidth43_2" select="0.12500 * $factor43"/>
												<xsl:variable name="columnwidth43_3" select="1.94792 * $factor43"/>
												<xsl:variable name="columnwidth43_4" select="0.29167 * $factor43"/>
												<xsl:variable name="columnwidth43_5" select="$defaultcolumnwidth43"/>
												<fo:table width="{$tablewidth43}in" border-collapse="separate" font-size="6pt" color="black" display-align="after" padding-bottom="0pt" padding-top="1pt">
													<fo:table-column column-width="0.1797in"/>
													<fo:table-column column-width="4.59230in"/>
													<fo:table-column column-width="0.19875in"/>
													<fo:table-column column-width="1.75000in"/>
													<fo:table-column column-width="0.12461in"/>
													<fo:table-column column-width="0.70923in"/>
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell height="0.11334in">
																<fo:block padding-top="1pt" padding-bottom="2pt"/>
															</fo:table-cell>
															<fo:table-cell height="0.11334in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" display-align="after">
																<fo:block>
																	
																</fo:block>	
															</fo:table-cell>
															<fo:table-cell height="0.11334in" >
																				<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="9pt" text-align="right">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>	
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.11334in" padding-top="8pt" >
																				<fo:block padding-top="1pt" padding-bottom="0pt" >
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Inclure photocopie de la garantie</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
														<fo:table-cell height="0.11334in" >
																				<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="9pt" text-align="right">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>	
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.11334in" padding-top="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="0pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;VOIR VERSO</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
														</fo:table-row>
													
													</fo:table-body>
												</fo:table>
												
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
													<fo:table-cell height="0.21875in">
													<fo:block padding-top="1pt" padding-bottom="1pt"/>
														
													</fo:table-cell>
													<fo:table-cell height="0.21875in" number-columns-spanned="4" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
														<fo:block padding-top="1pt" padding-bottom="1pt"/>
													</fo:table-cell>
												</fo:table-row>		
							</fo:table-body>
						</fo:table>	
								
						
						
						<xsl:variable name="tablewidth44" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths44" select="0.16667 + 7.30208"/>
						<xsl:variable name="factor44">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths44 &gt; 0.00000 and $sumcolumnwidths44 &gt; $tablewidth44">
									<xsl:value-of select="$tablewidth44 div $sumcolumnwidths44"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns44" select="1 + 1 + 1 + 1 + 1 + 1"/>
						<xsl:variable name="defaultcolumnwidth44">
							<xsl:choose>
								<xsl:when test="$factor44 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns44 &gt; 0">
									<xsl:value-of select="($tablewidth44 - $sumcolumnwidths44) div $defaultcolumns44"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth44_0" select="0.16667 * $factor44"/>
						<xsl:variable name="columnwidth44_1" select="7.30208 * $factor44"/>
						<xsl:variable name="columnwidth44_2" select="$defaultcolumnwidth44"/>
						<xsl:variable name="columnwidth44_3" select="$defaultcolumnwidth44"/>
						<xsl:variable name="columnwidth44_4" select="$defaultcolumnwidth44"/>
						<xsl:variable name="columnwidth44_5" select="$defaultcolumnwidth44"/>
						<xsl:variable name="columnwidth44_6" select="$defaultcolumnwidth44"/>
						<xsl:variable name="columnwidth44_7" select="$defaultcolumnwidth44"/>
						<fo:table width="{$tablewidth44}in" space-before.optimum="2pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center" padding-before="3pt" >
							<fo:table-column column-width="{$columnwidth44_0}in"/>
							<fo:table-column column-width="{$columnwidth44_1}in"/>
							<fo:table-column column-width="{$columnwidth44_2}in"/>
							<fo:table-column column-width="{$columnwidth44_3}in"/>
							<fo:table-column column-width="{$columnwidth44_4}in"/>
							<fo:table-column column-width="{$columnwidth44_5}in"/>
							<fo:table-column column-width="{$columnwidth44_6}in"/>
							<fo:table-column column-width="{$columnwidth44_7}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>8</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.01042in" number-columns-spanned="7">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>DEMANDANT / PAIEMENT</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.01042in" number-columns-spanned="7"  >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth45" select="$columnwidth44_1 * 1.00000 + $columnwidth44_2 * 1.00000 + $columnwidth44_3 * 1.00000 + $columnwidth44_4 * 1.00000 + $columnwidth44_5 * 1.00000 + $columnwidth44_6 * 1.00000 + $columnwidth44_7 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths45" select="1.45833 + 3.67708 + 1.15625"/>
											<xsl:variable name="factor45">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths45 &gt; 0.00000 and $sumcolumnwidths45 &gt; $tablewidth45">
														<xsl:value-of select="$tablewidth45 div $sumcolumnwidths45"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns45" select="1"/>
											<xsl:variable name="defaultcolumnwidth45">
												<xsl:choose>
													<xsl:when test="$factor45 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns45 &gt; 0">
														<xsl:value-of select="($tablewidth45 - $sumcolumnwidths45) div $defaultcolumns45"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth45_0" select="1.45833 * $factor45"/>
											<xsl:variable name="columnwidth45_1" select="3.67708 * $factor45"/>
											<xsl:variable name="columnwidth45_2" select="1.15625 * $factor45"/>
											<xsl:variable name="columnwidth45_3" select="$defaultcolumnwidth45"/>
											<fo:table width="{$tablewidth45}in" space-before.optimum="1pt" space-after.optimum="1pt" border-collapse="separate" font-size="6pt" color="black" display-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
												<fo:table-column column-width="{$columnwidth45_0}in"/>
												<fo:table-column column-width="{$columnwidth45_1}in"/>
												<fo:table-column column-width="{$columnwidth45_2}in"/>
												<fo:table-column column-width="{$columnwidth45_3}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.12792in"   padding-top="0.05000in">
															<fo:block  padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>&#160;&#160; But de la demande</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
															<fo:block>
																<xsl:text>
																</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12792in"   border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in">
															<fo:block  padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.12792in"  padding-top="0.05000in">
															<fo:block  padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>&#160;&#160; Date de la demande</xsl:text>
																</fo:inline>
															</fo:block>
															<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12792in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in">
															<fo:block  padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.12792in"  padding-top="0.05000in">
															<fo:block  padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	
																</fo:inline>
																<fo:block>
																	
																</fo:block>
															</fo:block>
															<fo:block>
																<xsl:text>
																</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12792in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in">
															<fo:block  padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.12792in" padding-top="0.05000in">
															<fo:block  padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	
																</fo:inline>
															</fo:block>
															<fo:block>
																	
																</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12792in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in">
															<fo:block  padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						
						<fo:block break-after="page"/>	
						
						<fo:inline font-size="8pt" font-weight="bold">
							<xsl:text>9&#160;&#160; PAIEMENT DES FRAIS - OBLIGATOIRE</xsl:text>
						</fo:inline>
						<fo:block>
							<xsl:text>&#xA;</xsl:text>
						</fo:block>
						<xsl:variable name="tablewidth46" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths46" select="0.14583"/>
						<xsl:variable name="factor46">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths46 &gt; 0.00000 and $sumcolumnwidths46 &gt; $tablewidth46">
									<xsl:value-of select="$tablewidth46 div $sumcolumnwidths46"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns46" select="1 + 1"/>
						<xsl:variable name="defaultcolumnwidth46">
							<xsl:choose>
								<xsl:when test="$factor46 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns46 &gt; 0">
									<xsl:value-of select="($tablewidth46 - $sumcolumnwidths46) div $defaultcolumns46"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth46_0" select="0.14583 * $factor46"/>
						<xsl:variable name="columnwidth46_1" select="$defaultcolumnwidth46"/>
						<xsl:variable name="columnwidth46_2" select="$defaultcolumnwidth46"/>
						<fo:table width="{$tablewidth46}in" space-before.optimum="0pt" space-after.optimum="0pt" border-collapse="separate" color="black">
							<fo:table-column column-width="{$columnwidth46_0}in"/>
							<fo:table-column column-width="{$columnwidth46_1}in"/>	
							<fo:table-column column-width="{$columnwidth46_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2"  display-align="before" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block >
											<xsl:variable name="tablewidth47" select="$columnwidth46_1 * 1.00000 + $columnwidth46_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths47" select="4.41667 + 2.44792 + 4.56250 + 1.82292 + 0.39583 + 1.73958"/>
											<xsl:variable name="factor47">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths47 &gt; 0.00000 and $sumcolumnwidths47 &gt; $tablewidth47">
														<xsl:value-of select="$tablewidth47 div $sumcolumnwidths47"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth47_0" select="4.41667 * $factor47"/>
											<xsl:variable name="columnwidth47_1" select="2.44792 * $factor47"/>
											<xsl:variable name="columnwidth47_2" select="4.56250 * $factor47"/>
											<xsl:variable name="columnwidth47_3" select="1.82292 * $factor47"/>
											<xsl:variable name="columnwidth47_4" select="0.39583 * $factor47"/>
											<xsl:variable name="columnwidth47_5" select="1.73958 * $factor47"/>
											<fo:table width="{$tablewidth47}in" space-before.optimum="0pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="before">
												<fo:table-column column-width="{$columnwidth47_0}in"/>
												<fo:table-column column-width="{$columnwidth47_1}in"/>
												<fo:table-column column-width="{$columnwidth47_2}in"/>
												<fo:table-column column-width="{$columnwidth47_3}in"/>
												<fo:table-column column-width="{$columnwidth47_4}in"/>
												<fo:table-column column-width="{$columnwidth47_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell number-columns-spanned="2" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.03000in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block >
																<xsl:variable name="tablewidth48" select="$columnwidth47_0 * 1.00000 + $columnwidth47_1 * 1.00000 - 0.03000"/>
																<xsl:variable name="sumcolumnwidths48" select="0.15625 + 4.03125"/>
																<xsl:variable name="factor48">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths48 &gt; 0.00000 and $sumcolumnwidths48 &gt; $tablewidth48">
																			<xsl:value-of select="$tablewidth48 div $sumcolumnwidths48"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth48_0" select="0.15625 * $factor48"/>
																<xsl:variable name="columnwidth48_1" select="4.03125 * $factor48"/>
																<fo:table width="{$tablewidth48}in" space-before.optimum="0pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="before">
																	<fo:table-column column-width="{$columnwidth48_0}in"/>
																	<fo:table-column column-width="{$columnwidth48_1}in"/>
																	<fo:table-body font-size="10pt" >
																		<fo:table-row>
																			<fo:table-cell text-align="right" padding-top="3pt" padding-left="0pt">
																				<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="0pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;&#160;Payé par le client</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<xsl:variable name="tablewidth49" select="$columnwidth48_1 * 1.00000"/>
																					<xsl:variable name="sumcolumnwidths49" select="0.14583 + 1.71875 + 0.59375 + 0.78125 + 0.82292"/>
																					<xsl:variable name="factor49">
																						<xsl:choose>
																							<xsl:when test="$sumcolumnwidths49 &gt; 0.00000 and $sumcolumnwidths49 &gt; $tablewidth49">
																								<xsl:value-of select="$tablewidth49 div $sumcolumnwidths49"/>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="1.000"/>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:variable>
																					<xsl:variable name="defaultcolumns49" select="1"/>
																					<xsl:variable name="defaultcolumnwidth49">
																						<xsl:choose>
																							<xsl:when test="$factor49 &lt; 1.000">
																								<xsl:value-of select="0.000"/>
																							</xsl:when>
																							<xsl:when test="$defaultcolumns49 &gt; 0">
																								<xsl:value-of select="($tablewidth49 - $sumcolumnwidths49) div $defaultcolumns49"/>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="0.000"/>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:variable>
																					<xsl:variable name="columnwidth49_0" select="0.10583 * $factor49"/>
																					<xsl:variable name="columnwidth49_1" select="0.71195 * $factor49"/>
																					<xsl:variable name="columnwidth49_2" select="0.60375 * $factor49"/>
																					<xsl:variable name="columnwidth49_3" select="0.48125 * $factor49"/>
																					<xsl:variable name="columnwidth49_4" select="0.45292 * $factor49"/>
																					<xsl:variable name="columnwidth49_5" select="$defaultcolumnwidth49"/>
																					<fo:table width="{$tablewidth49}in" space-before.optimum="0pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="before">
																						<fo:table-column column-width="{$columnwidth49_0}in"/>
																						<fo:table-column column-width="{$columnwidth49_1}in"/>
																						<fo:table-column column-width="{$columnwidth49_2}in"/>
																						<fo:table-column column-width="{$columnwidth49_3}in"/>
																						<fo:table-column column-width="{$columnwidth49_4}in"/>
																						<fo:table-column column-width="0.0511in"/>
																						<fo:table-body font-size="10pt">
																							<fo:table-row>
																								<fo:table-cell><fo:block padding-left="1pt">
																										<fo:inline>
																											<xsl:choose>
																												<xsl:when test="//alwUnchkd = '1'">
																												   <xsl:call-template name="CheckedCheckbox"/>
																												</xsl:when>
																												<xsl:otherwise>
																													<xsl:call-template name="UnCheckedCheckbox"/>
																												</xsl:otherwise>
																											</xsl:choose>									
																										</fo:inline>
																									</fo:block>													
																								</fo:table-cell>
																								<fo:table-cell>
																									<fo:block padding-top="1pt" >
																										<fo:inline font-size="6pt">
																													<xsl:text>No de compte à débiter:</xsl:text>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																								<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																									<fo:block padding-top="1pt"/>
																								</fo:table-cell>
																								<fo:table-cell>
																									<fo:block padding-top="1pt" >
																										<fo:inline font-size="6pt">
																											<xsl:text>&#160;&#160;No de transit:</xsl:text>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																								<fo:table-cell number-columns-spanned="1" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																									<fo:block padding-top="1pt" />
																								</fo:table-cell>
																								<fo:table-cell >
																									<fo:block padding-top="1pt" />
																								</fo:table-cell>
																							</fo:table-row>
																						</fo:table-body>
																					</fo:table>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row line-height="0.005in">
																			<fo:table-cell  >
																				<fo:block >
																					<fo:inline font-size="6pt">
																						<xsl:text>OU</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block />
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell display-align="before">
																				<fo:block padding-top="1pt" />
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" >
																					<xsl:variable name="tablewidth50" select="$columnwidth48_1 * 1.00000"/>
																					<xsl:variable name="sumcolumnwidths50" select="0.07292 + 3.48958"/>
																					<xsl:variable name="factor50">
																						<xsl:choose>
																							<xsl:when test="$sumcolumnwidths50 &gt; 0.00000 and $sumcolumnwidths50 &gt; $tablewidth50">
																								<xsl:value-of select="$tablewidth50 div $sumcolumnwidths50"/>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="1.000"/>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:variable>
																					<xsl:variable name="columnwidth50_0" select="0.07292 * $factor50"/>
																					<xsl:variable name="columnwidth50_1" select="3.68958 * $factor50"/>
																					<fo:table width="{$tablewidth50}in" space-before.optimum="1pt" space-after.optimum="0pt" border-collapse="separate" color="black" display-align="before" >
																						<fo:table-column column-width="{$columnwidth50_0}in"/>
																						<fo:table-column column-width="{$columnwidth50_1}in"/>
																						<fo:table-body font-size="10pt">
																							<fo:table-row>
																								<fo:table-cell number-rows-spanned="2" >
																									<fo:block padding-top="2pt" padding-bottom="1pt" padding-left="1pt">		
																										<xsl:choose>
																											<xsl:when test="//alwUnchkd = '1'">
																											   <xsl:call-template name="CheckedCheckbox"/>
																											</xsl:when>
																											<xsl:otherwise>
																												<xsl:call-template name="UnCheckedCheckbox"/>
																											</xsl:otherwise>
																										</xsl:choose>
																									</fo:block>
																								</fo:table-cell>
																								<fo:table-cell>
																									<fo:block padding-top="1pt" >
																										<fo:inline font-size="6pt" >
																											<xsl:text>&#160;&#160; </xsl:text>
																											<xsl:text>Rubrique suspens à débiter : 4-21 au secteur 01</xsl:text>
																										</fo:inline>
																								
																									<fo:block line-height="0.0951in" display-align="before" padding-bottom="2pt">
																										<fo:inline font-size="5.5pt" >
																											<xsl:text>&#160;&#160;&#160;</xsl:text>				
																											<xsl:text>(ex.: si client paye par chèque ou en argent en succursale)</xsl:text>
																										</fo:inline>	
																									</fo:block>
																										</fo:block>
																								</fo:table-cell>
																							</fo:table-row>
																						</fo:table-body>
																					</fo:table>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="4"  border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
															<fo:block>
																<xsl:variable name="tablewidth51" select="$columnwidth47_2 * 1.00000 + $columnwidth47_3 * 1.00000 + $columnwidth47_4 * 1.00000 + $columnwidth47_5 * 1.00000 - 0.03000"/>
																<xsl:variable name="sumcolumnwidths51" select="0.14583 + 1.07292 + 0.15625 + 1.48958 + 0.14583 + 0.44792 + 0.13542 + 0.82292"/>
																<xsl:variable name="factor51">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths51 &gt; 0.00000 and $sumcolumnwidths51 &gt; $tablewidth51">
																			<xsl:value-of select="$tablewidth51 div $sumcolumnwidths51"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns51" select="1"/>
																<xsl:variable name="defaultcolumnwidth51">
																	<xsl:choose>
																		<xsl:when test="$factor51 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns51 &gt; 0">
																			<xsl:value-of select="($tablewidth51 - $sumcolumnwidths51) div $defaultcolumns51"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth51_0" select="0.14583 * $factor51"/>
																<xsl:variable name="columnwidth51_1" select="0.97292 * $factor51"/>
																<xsl:variable name="columnwidth51_2" select="0.15625 * $factor51"/>
																<xsl:variable name="columnwidth51_3" select="1.55958 * $factor51"/>
																<xsl:variable name="columnwidth51_4" select="0.14583 * $factor51"/>
																<xsl:variable name="columnwidth51_5" select="0.54792 * $factor51"/>
																<xsl:variable name="columnwidth51_6" select="0.13542 * $factor51"/>
																<xsl:variable name="columnwidth51_7" select="0.82292 * $factor51"/>
																<xsl:variable name="columnwidth51_8" select="$defaultcolumnwidth51"/>
																<fo:table width="{$tablewidth51}in" border-collapse="separate" color="black" display-align="center" padding-bottom="5pt">
																	<fo:table-column column-width="{$columnwidth51_0}in"/>
																	<fo:table-column column-width="{$columnwidth51_1}in"/>
																	<fo:table-column column-width="{$columnwidth51_2}in"/>
																	<fo:table-column column-width="{$columnwidth51_3}in"/>
																	<fo:table-column column-width="{$columnwidth51_4}in"/>
																	<fo:table-column column-width="{$columnwidth51_5}in"/>
																	<fo:table-column column-width="{$columnwidth51_6}in"/>
																	<fo:table-column column-width="{$columnwidth51_7}in"/>
																	<fo:table-column column-width="{$columnwidth51_8}in"/>
																	<fo:table-body font-size="10pt">
																		<fo:table-row>
																			<fo:table-cell padding-left="1pt">
																				<xsl:choose>
																<xsl:when test="//alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
																			</fo:table-cell>
																			<fo:table-cell height="0.05208in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Payé par la banque</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.05208in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.05208in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.05208in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.05208in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.05208in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.05208in" number-columns-spanned="2">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row  line-height="0.1091in">
																			<fo:table-cell >
																				<fo:block />
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="2pt">
																				<fo:block >
																					<fo:inline font-size="6pt">
																						<xsl:text>&#160;Rubrique à débiter</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																			<fo:block>
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																			</fo:block>	
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="2pt">
																				<fo:block>
																					<fo:inline font-size="6pt">
																						<xsl:text>456-26 </xsl:text>
																					</fo:inline>
																					<fo:inline font-weight="bold" font-size="6pt"><xsl:text>&#160;PH :</xsl:text></fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																			<fo:block>
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="1pt">
																				<fo:block>
																					<fo:inline font-size="6pt">
																						<xsl:text>Réseau</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell >
																			<fo:block >
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell  number-columns-spanned="2" padding-bottom="1pt">
																				<fo:block>
																					<fo:inline font-size="6pt">
																						<xsl:text>Commercial</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row line-height="0.1091in">
																			<fo:table-cell >
																				<fo:block />
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:choose>
																						<xsl:when test="//alwUnchkd = '1'">
																						   <xsl:call-template name="CheckedCheckbox"/>
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:call-template name="UnCheckedCheckbox"/>
																						</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>	
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="1pt">
																				<fo:block>
																					<fo:inline font-size="6pt">
																						<xsl:text>456-79 </xsl:text>
																					</fo:inline>
																					<fo:inline font-weight="bold" font-size="6pt"><xsl:text>&#160;Marge :</xsl:text></fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																			<fo:block>
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="1pt">
																				<fo:block>
																					<fo:inline font-size="6pt">
																						<xsl:text>Réseau</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell >
																			<fo:block >
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell  number-columns-spanned="2" padding-bottom="1pt">
																				<fo:block>
																					<fo:inline font-size="6pt">
																						<xsl:text>Commercial</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row  line-height="0.1091in">
																			<fo:table-cell >
																				<fo:block />
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell >
																			<fo:block padding-top="1pt">
																			<xsl:choose>
																				<xsl:when test="//alwUnchkd = '1'">
																				   <xsl:call-template name="CheckedCheckbox"/>
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																				</xsl:otherwise>
																			</xsl:choose>
																			</fo:block>	
																			</fo:table-cell>
																			<fo:table-cell >
																				<fo:block>
																					<fo:inline font-size="6pt" padding-bottom="2pt">
																						<xsl:text>456-94 </xsl:text>
																					</fo:inline>
																					<fo:inline font-weight="bold" font-size="6pt"><xsl:text>&#160;Autres produits de crédits :</xsl:text></fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell >
																			<fo:block padding-top="2pt">
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell >
																				<fo:block>
																					<fo:inline font-size="6pt" padding-bottom="0pt">
																						<xsl:text>Réseau</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell >
																			<fo:block padding-top="2pt">
																				<xsl:choose>
																					<xsl:when test="//alwUnchkd = '1'">
																					   <xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																				</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell  number-columns-spanned="2" padding-bottom="0pt">
																				<fo:block>
																					<fo:inline font-size="6pt">
																						<xsl:text>Commercial</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.36458in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.03000in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in"  >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>No de prêt</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.36458in" number-columns-spanned="3" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.03000in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt" >
																<fo:inline font-size="6pt">
																	<xsl:text>Adresse de l&apos;unité à débiter</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.36458in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.36458in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>No de transit</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
													<fo:table-cell number-columns-spanned="6">
														<fo:block>
															<fo:table width="95%" border-collapse="separate" color="black" >
																<fo:table-column column-width="proportional-column-width(44.5)"/>
																<fo:table-column column-width="proportional-column-width(15.5)"/>
																<fo:table-column column-width="proportional-column-width(20)"/>
																<fo:table-column column-width="proportional-column-width(20)"/>
																<fo:table-body>
																<fo:table-row>
																<fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.01042in" display-align="before">
																	<fo:block  padding-bottom="1pt" padding-top="2pt">
																		<fo:inline font-size="6pt">
																			<xsl:text>&#160;Demandé par</xsl:text>
																		</fo:inline>
																	</fo:block>
																	</fo:table-cell>
																	<fo:table-cell   border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
																	<fo:block>
																		<fo:inline font-size="6pt">
																			<xsl:text>&#160;No de transit</xsl:text>
																		</fo:inline>
																	</fo:block>
																	</fo:table-cell>
																	<fo:table-cell   border-left-style="solid" border-left-color="black" border-left-width="0.01042in" display-align="before">
																		<fo:block>
																			<fo:inline font-size="6pt">
																				<xsl:text>&#160;No de téléphone</xsl:text>
																			</fo:inline>
																		</fo:block>
																	</fo:table-cell>
																	<fo:table-cell   border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" display-align="before">
																		<fo:block>
																			<fo:inline font-size="6pt">
																				<xsl:text>&#160;No de télécopieur</xsl:text>
																			</fo:inline>
																		</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																<fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.01042in" display-align="before">
																	<fo:block  padding-bottom="1pt" padding-top="2pt">
																		<fo:inline font-size="6pt">
																			<xsl:text>&#160;</xsl:text>
																		</fo:inline>
																	</fo:block>
																	</fo:table-cell>
																	<fo:table-cell   border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
																	<fo:block>
																		<fo:inline font-size="6pt">
																			<xsl:text></xsl:text>
																		</fo:inline>
																	</fo:block>
																	</fo:table-cell>
																	<fo:table-cell   border-left-style="solid" border-left-color="black" border-left-width="0.01042in" display-align="before">
																		<fo:block>
																			<fo:inline font-size="6pt">
																				<xsl:text></xsl:text>
																			</fo:inline>
																		</fo:block>
																	</fo:table-cell>
																	<fo:table-cell   border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" display-align="before">
																		<fo:block>
																			<fo:inline font-size="6pt">
																				<xsl:text></xsl:text>
																			</fo:inline>
																		</fo:block>
																	</fo:table-cell>
																</fo:table-row>
															</fo:table-body>
														</fo:table>
													
														</fo:block>
													</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth54" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths54" select="0.16667"/>
						<xsl:variable name="factor54">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths54 &gt; 0.00000 and $sumcolumnwidths54 &gt; $tablewidth54">
									<xsl:value-of select="$tablewidth54 div $sumcolumnwidths54"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns54" select="1 + 1"/>
						<xsl:variable name="defaultcolumnwidth54">
							<xsl:choose>
								<xsl:when test="$factor54 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns54 &gt; 0">
									<xsl:value-of select="($tablewidth54 - $sumcolumnwidths54) div $defaultcolumns54"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth54_0" select="0.16667 * $factor54"/>
						<xsl:variable name="columnwidth54_1" select="$defaultcolumnwidth54"/>
						<xsl:variable name="columnwidth54_2" select="$defaultcolumnwidth54"/>
						<fo:table width="{$tablewidth54}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth54_0}in"/>
							<fo:table-column column-width="{$columnwidth54_1}in"/>
							<fo:table-column column-width="{$columnwidth54_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.08333in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.08333in" number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth55" select="$columnwidth54_1 * 1.00000 + $columnwidth54_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths55" select="0.16667"/>
											<xsl:variable name="factor55">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths55 &gt; 0.00000 and $sumcolumnwidths55 &gt; $tablewidth55">
														<xsl:value-of select="$tablewidth55 div $sumcolumnwidths55"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns55" select="1 + 1 + 1"/>
											<xsl:variable name="defaultcolumnwidth55">
												<xsl:choose>
													<xsl:when test="$factor55 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns55 &gt; 0">
														<xsl:value-of select="($tablewidth55 - $sumcolumnwidths55) div $defaultcolumns55"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth55_0" select="0.16667 * $factor55"/>
											<xsl:variable name="columnwidth55_1" select="$defaultcolumnwidth55"/>
											<xsl:variable name="columnwidth55_2" select="$defaultcolumnwidth55"/>
											<xsl:variable name="columnwidth55_3" select="$defaultcolumnwidth55"/>
											<fo:table width="{$tablewidth55}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth55_0}in"/>
												<fo:table-column column-width="{$columnwidth55_1}in"/>
												<fo:table-column column-width="{$columnwidth55_2}in"/>
												<fo:table-column column-width="{$columnwidth55_3}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.08333in" number-columns-spanned="4">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt" font-weight="bold">
																	<xsl:text>EXPÉDITION</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.50000in" number-columns-spanned="4">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth56" select="$columnwidth55_0 * 1.00000 + $columnwidth55_1 * 1.00000 + $columnwidth55_2 * 1.00000 + $columnwidth55_3 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths56" select="1.76042 + 7.18750 + 1.60417 + 0.88542"/>
																<xsl:variable name="factor56">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths56 &gt; 0.00000 and $sumcolumnwidths56 &gt; $tablewidth56">
																			<xsl:value-of select="$tablewidth56 div $sumcolumnwidths56"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth56_0" select="0.76042 * $factor56"/>
																<xsl:variable name="columnwidth56_1" select="3.58750 * $factor56"/>
																<xsl:variable name="columnwidth56_2" select="1.00417 * $factor56"/>
																<xsl:variable name="columnwidth56_3" select="0.08542 * $factor56"/>
																<fo:table width="{$tablewidth56}in"  border-collapse="separate" font-size="6pt" color="black" display-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
																	<fo:table-column column-width="{$columnwidth56_0}in"/>
																	<fo:table-column column-width="{$columnwidth56_1}in"/>
																	<fo:table-column column-width="{$columnwidth56_2}in"/>
																	<fo:table-column column-width="{$columnwidth56_3}in"/>
																	<fo:table-body font-size="10pt">
																		<fo:table-row>
																			<fo:table-cell display-align="before" height="0.02417in"   padding-top="0.05000in" padding-left="0.03000in" number-columns-spanned="2" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt" >
																					<fo:inline font-size="6pt" font-weight="bold">
																						<xsl:text>Original du rapport</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell display-align="before" height="0.02417in" padding-top="0.05000in" padding-left="0.03000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt">
																						<xsl:text>No de transit</xsl:text>
																					</fo:inline>
																					<fo:block>
																						<xsl:text>&#xA;</xsl:text>
																					</fo:block>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell display-align="before" height="0.02417in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell   height="0.55417in">
																				<fo:block></fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.55417in"  border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in" >
																				<fo:block padding-bottom="1pt">
																					<xsl:variable name="tablewidth57" select="$columnwidth56_1 * 1.00000 - 0.03000"/>
																					<xsl:variable name="sumcolumnwidths57" select="0.81250 + 5.44792"/>
																					<xsl:variable name="factor57">
																						<xsl:choose>
																							<xsl:when test="$sumcolumnwidths57 &gt; 0.00000 and $sumcolumnwidths57 &gt; $tablewidth57">
																								<xsl:value-of select="$tablewidth57 div $sumcolumnwidths57"/>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="1.000"/>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:variable>
																					<xsl:variable name="columnwidth57_0" select="0.91250 * $factor57"/>
																					<xsl:variable name="columnwidth57_1" select="12.04792 * $factor57"/>
																					<fo:table width="{$tablewidth57}in"  border-collapse="separate" font-size="8pt" color="black" display-align="center">
																						<fo:table-column column-width="{$columnwidth57_0}in"/>
																						<fo:table-column column-width="{$columnwidth57_1}in"/>
																						
																						<fo:table-body>
																							
																							<fo:table-row>
																								<fo:table-cell display-align="after" height="0.14417in" number-columns-spanned="1" >
																									<fo:block  padding-bottom="1pt" padding-right="5pt" text-align="center">  
																										<fo:inline font-size="6pt">
																											<xsl:text>Adresse</xsl:text>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																								<fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="1pt">
																									<fo:block>
																									</fo:block>
																								</fo:table-cell>
																							</fo:table-row>
																							<fo:table-row>
																								<fo:table-cell display-align="before" height="0.16417in" number-columns-spanned="2">
																									<fo:block padding-bottom="1pt"/>
																								</fo:table-cell>
																							</fo:table-row>
																							<fo:table-row>
																								<fo:table-cell display-align="after" height="0.16417in" number-columns-spanned="1">
																									<fo:block padding-top="1pt" padding-bottom="1pt" padding-right="5pt" text-align="left">  
																										<fo:inline font-size="6pt">
																											<xsl:text>A/S</xsl:text>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																								<fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="1pt">
																									<fo:block>
																									</fo:block>
																								</fo:table-cell>
																							</fo:table-row>
																						</fo:table-body>
																					</fo:table>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell display-align="before" height="0.55417in"  padding-top="0.05000in" padding-left="0.03000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell display-align="before" height="0.55417in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row font-size="6pt">
																			<fo:table-cell display-align="before" height="0.18542in" number-columns-spanned="2" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="6pt" font-weight="bold">
																						<xsl:text>Copie du rapport</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell display-align="before" height="0.18542in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" number-columns-spanned="2" text-align="left" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<xsl:text> No de transit</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																			<fo:table-row font-size="6pt">
																			<fo:table-cell display-align="before" height="0.18542in" number-columns-spanned="2"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																					<fo:inline font-size="8pt">
																						<xsl:text>600, rue de La Gauchetière Ouest, 9</xsl:text>
																					</fo:inline>
																					<fo:inline font-size="7pt" vertical-align="super">
																						<xsl:text> e </xsl:text>
																					</fo:inline>
																					<fo:inline font-size="8pt">
																						<xsl:text>étage, Montréal (Québec) H3B 4L2</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell display-align="before" height="0.18542in" number-columns-spanned="2" text-align="left" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-top="0.05000in" padding-left="0.03000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center" font-size="8pt">
																					<xsl:text> 1698-1</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	<xsl:template name="footerall">
		<xsl:variable name="maxwidth" select="7.52000"/>
		<fo:static-content flow-name="xsl-region-after">
			<fo:block>
				<xsl:variable name="tablewidth59" select="$maxwidth * 1.00000"/>
				<xsl:variable name="sumcolumnwidths59" select="0.04167 + 0.04167"/>
				<xsl:variable name="defaultcolumns59" select="1 + 1"/>
				<xsl:variable name="defaultcolumnwidth59">
					<xsl:choose>
						<xsl:when test="$defaultcolumns59 &gt; 0">
							<xsl:value-of select="($tablewidth59 - $sumcolumnwidths59) div $defaultcolumns59"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="0.000"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="columnwidth59_0" select="$defaultcolumnwidth59"/>
				<xsl:variable name="columnwidth59_1" select="$defaultcolumnwidth59"/>
				<fo:table width="{$tablewidth59}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black" display-align="center">
					<fo:table-column column-width="{$columnwidth59_0}in"/>
					<fo:table-column column-width="{$columnwidth59_1}in"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell height="0.31250in" number-columns-spanned="2" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:inline font-size="6pt">
										<xsl:text>10683-001 (2004-11-22)</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>
	<!-- ************************************************************* -->
	<!-- Checked Check Box                                                  -->
	<!--                                                                                   -->
	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="7px" height="7px">
			<svg xmlns="http://www.w3.org/2000/svg" width="7px" height="7px">
				<rect x="0" y="0" width="7" height="7" style="fill: none; stroke: black"/>
				<line x1="0" y1="0" x2="7" y2="7" style="stroke:black"/>
				<line x1="0" y1="7" x2="7" y2="0" style="stroke:black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	
	<!-- ************************************************************* -->
	<!-- Unchecked Check Box                                              -->
	<!--                                                                                   -->
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="7px" height="7px">
			<svg xmlns="http://www.w3.org/2000/svg" width="7px" height="7px">
				<rect x="0" y="0" width="7" height="7" style="fill: none; stroke: black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	
<xsl:template name="smallCheckedCheckbox">
		<fo:instream-foreign-object width="6px" height="6px">
			<svg xmlns="http://www.w3.org/2000/svg" width="6px" height="6px">
				<rect x="0" y="0" width="6" height="6" style="fill: none; stroke: black"/>
				<line x1="0" y1="0" x2="6" y2="6" style="stroke:black"/>
				<line x1="0" y1="6" x2="6" y2="0" style="stroke:black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	
	<!-- ************************************************************* -->
	<!-- Unchecked Check Box                                              -->
	<!--                                                                                   -->
	<xsl:template name="smallUnCheckedCheckbox">
		<fo:instream-foreign-object width="6px" height="6px">
			<svg xmlns="http://www.w3.org/2000/svg" width="6px" height="6px">
				<rect x="0" y="0" width="6" height="6" style="fill: none; stroke: black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>	
	
	
</xsl:stylesheet>
