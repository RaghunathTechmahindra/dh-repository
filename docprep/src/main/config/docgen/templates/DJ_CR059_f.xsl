<?xml version="1.0" encoding="UTF-8"?>
<!--
19/Oct/2005 DVG #DG334 #2264  DJ docs - Change phone numbers in CR047, CR050, CR059, CR060, CR061  
14/Apr/2005 DVG #DG184 #1211 DJ - CR059 - change to spec -->
<!--09/Mar/2005 DVG #DG166 #1063  DJ - miscellaneous DocPrep fixes  -->
<!--22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik-->
<!-- created by Catherine Rutgaizer -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- define attribute sets -->
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll1mm">
		<xsl:attribute name="padding-top">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
		<xsl:attribute name="padding-right">1mm</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">9pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="DisclaimerFontAttr">
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">9pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="RetourFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">11pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr">
		<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="DateFontAttr">
		<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="BottomLine">
		<xsl:attribute name="border-bottom-style">solid</xsl:attribute>
		<xsl:attribute name="border-bottom-color">black</xsl:attribute>
		<xsl:attribute name="border-bottom-width">0.5px</xsl:attribute>
	</xsl:attribute-set>
	<!-- end attribute sets -->

	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:call-template name="CreateLogoLine"/>
					<xsl:call-template name="PreTitleLines"/>
					<xsl:call-template name="CreateTitleLine"/>
					<xsl:call-template name="BranchInfo"/>
					<xsl:call-template name="FIInfo"/>
					<xsl:call-template name="BorrowerInfo"/>
					<xsl:call-template name="RetourInfo"/>
					<xsl:call-template name="Certification"/>
					<xsl:call-template name="Disclaimer"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">170mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="SpaceBAOptimum"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">C:\Dev\Desjardins\data\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PreTitleLines">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="TitleFontAttr"/>
			<xsl:attribute name="space-before">2mm</xsl:attribute>
			<xsl:element name="fo:block">Nous retourner dans L’HEURE QUI SUIT</xsl:element>
			<xsl:element name="fo:block">à la réception de ce document. Merci!</xsl:element>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="DateFontAttr"/>
			(Envoyé  <xsl:value-of select="//General/CurrentDateTime"/>)
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">175mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TitleFontAttr"/>
						Demande d’expérience de crédit - Dossier <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="BranchInfo">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:copy use-attribute-sets="PaddingAll1mm"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">83mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">92mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:element name="fo:block">
							<!-- <xsl:value-of select="//Deal/branchProfile/branchName"/> -->
							<!--#DG184 
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyCompanyName"/-->
							Service de financement au point de vente Desjardins<!--#DG184 -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:call-template name="UnderwriterInfo"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="UnderwriterInfo">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">25mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">67mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">De :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Téléphone :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<!--#DG334 xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumber"/>
							<xsl:choose>
								<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> ou 1-888-888-7777 <!- - #DG166 poste : <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/- ->
								</xsl:when>
								<xsl:when test="//Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999 <!- - #DG166 poste : <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/- ->
								</xsl:when>
								<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> ou 1-888-888-8888 <!- - #DG166 poste : <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/- ->
								</xsl:when>
								<!- - #DG166 xsl:otherwise>
									<xsl:text> </xsl:text>poste : <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
								</xsl:otherwise- ->
							</xsl:choose-->514-376-7181 ou sans frais 1-877-376-7181
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<!--#DG334 xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Poste :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
						</xsl:element>
					</xsl:element>
				</xsl:element-->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Télécopieur :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<!--#DG334 xsl:value-of select="//Deal/underwriter/Contact/contactFaxNumber"/>
							<xsl:choose>
								<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> ou 1-888-888-7777</xsl:when>
								<xsl:when test="//Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999</xsl:when>
								<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> ou 1-888-888-8888</xsl:when>
							</xsl:choose-->514-322-2364 ou sans frais 1-866-243-2364
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="FIInfo">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">175mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">171mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">Numéro de transit : </xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:attribute name="padding-top">1mm</xsl:attribute>
										<xsl:element name="fo:block">Nom :</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:attribute name="padding-top">1mm</xsl:attribute>
										<xsl:element name="fo:block">Personne ressource :</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:attribute name="padding-top">1mm</xsl:attribute>
										<xsl:element name="fo:block">Adresse :</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:attribute name="padding-top">5mm</xsl:attribute>
										<xsl:element name="fo:block">
											<xsl:text> </xsl:text>
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:attribute name="padding-top">1mm</xsl:attribute>
										<xsl:element name="fo:block">No. téléphone :</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:attribute name="padding-top">1mm</xsl:attribute>
										<xsl:element name="fo:block">No. télécopieur :</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="BorrowerInfo">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:copy use-attribute-sets="PaddingAll1mm"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">175mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:element name="fo:block">
							<!--#DG140 xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
								<xsl:if test="position()=1">
									<xsl:apply-templates select="." mode="Borrowera"/>
								</xsl:if>
							</xsl:for-each-->
							<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower[1]">
								<xsl:call-template name="BorrData"/>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!--#DG140 xsl:template match="Borrower" -->
	<xsl:template name="BorrData">	
 		<fo:table  xsl:use-attribute-sets="TableLeftFixed">

		<!--#DG140 xsl:element name="fo:table">
			<would not work like this !! xsl:copy use-attribute-sets="TableLeftFixed"/-->
			
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">38mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">129mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Nom de l'emprunteur :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:value-of select="./borrowerFirstName"/>
							<xsl:text> </xsl:text>
							<xsl:if test="borrowerMiddleInitial">
								<xsl:value-of select="./borrowerMiddleInitial"/>
								<xsl:text> </xsl:text>
							</xsl:if>
							<xsl:value-of select="./borrowerLastName"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Adresse :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:for-each select="./BorrowerAddress">
								<xsl:if test="borrowerAddressTypeId= 0">
									<xsl:value-of select="./Address/addressLine1"/>
									<xsl:text> </xsl:text>
									<xsl:if test="./Address/addressLine2">
										<xsl:value-of select="./Address/addressLine2"/>
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="./Address/city"/>, <xsl:value-of select="./Address/province"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="./Address/postalFSA"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="./Address/postalLDU"/>
								</xsl:if>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Date de naissance :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="./borrowerBirthDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">N.A.S. :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:if test="./socialInsuranceNumber">
								<xsl:value-of select="./socialInsuranceNumber"/>
							</xsl:if>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">But de la demande :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="//Deal/dealPurpose"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Bureau de crédit Date :</xsl:element>
					</xsl:element>
					<!-- #644 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<!--<xsl:value-of select="./creditBureauOnFileDate"/>-->
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</fo:table><!--#DG140 -->
	</xsl:template>

	<xsl:template name="RetourInfo">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">83mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">92mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="RetourFontAttr"/>RETOUR</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:call-template name="RetourDetailsLeft"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:call-template name="RetourDetailsRight"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="RetourDetailsLeft">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">29mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">50mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="PaddingAll1mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">-3mm</xsl:attribute>
						<xsl:element name="fo:block">ÉPARGNE </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"/>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Date ouv. compte :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Compte de chèque :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Épargnes :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Placements :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Chèques NSF :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="RetourDetailsRight">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">30mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">58mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="PaddingAll1mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">-3mm</xsl:attribute>
						<xsl:element name="fo:block">PRÊT </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"/>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Date d'autorisation :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Montant autorisé :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Solde :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Versement :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Fréquence :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">But :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Commentaires :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">8mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">7mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">MARGE DE CRÉDIT</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Date d'autorisation :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">Montant autorisé :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:element name="fo:block">% d'utilisation :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">4mm</xsl:attribute>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Certification">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:copy use-attribute-sets="PaddingAll1mm"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">175mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="DisclaimerFontAttr"/>
				<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:element name="fo:block">
			Nous certifions, par la présente, avoir obtenu les consentements requis par la Loi sur la protection des renseignements personnels dans le secteur privé.
			</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Disclaimer">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">175mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="DisclaimerFontAttr"/>
				<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll1mm"/>
						<xsl:element name="fo:block">
			Ce message est strictement réservé à l'usage de l'individu ou la personne à qui il est adressé et contient des informations privilégiées et confidentielles. Si le lecteur de ce message n'est pas le destinataire désigné vous êtes, par les présentes, avisé que toute diffusion, distribution ou copie de cette communication est strictement prohibée. Si vous avez reçu cette communication par erreur, veuillez nous téléphoner immédiatement et nous retourner le message original, par la poste, à l'adresse mentionnée ci-dessus. Merci.
			</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- FO Start -->
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">5mm</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">1in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">5mm</xsl:attribute>
						<xsl:attribute name="margin-left">15mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">2mm</xsl:attribute>
						<xsl:attribute name="margin-top">2mm</xsl:attribute>
						<xsl:attribute name="margin-left">20mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">2mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
