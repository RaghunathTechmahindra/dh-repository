<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' >  
<xsl:output method="text"/>

<xsl:template match="/">
<xsl:call-template name="RTFFileStart"/>

<xsl:choose>
<xsl:when test='//LanguageEnglish'>
<!-- <xsl:call-template name="EnglishPageNumber"/> -->
<xsl:call-template name="EnglishDocumentTitle"/>
<xsl:call-template name="EnglishHeaderTable"/>
<xsl:call-template name="EnglishSubheader"/>
<xsl:call-template name="EnglishConditionList"/>
</xsl:when>
</xsl:choose>

<xsl:choose>
<xsl:when test='//LanguageFrench'>
<xsl:call-template name="FrenchDocumentTitle"/>
<xsl:call-template name="FrenchHeaderTable"/>
<xsl:call-template name="FrenchSubheader"/>
<xsl:call-template name="FrenchConditionList"/>
</xsl:when>
</xsl:choose>


<xsl:call-template name="RTFFileEnd"/> 
</xsl:template>  

<!-- ************************************************************************ -->
<!-- English template section                                                 -->
<!-- ************************************************************************ -->
<xsl:template name="EnglishConditionList"> 

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx9000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl 

{\f1\fs22\b \cell Documents reviewed by:\par </xsl:text><xsl:value-of select="//CurrentUser/Name"/><xsl:text>\par Phone: </xsl:text><xsl:value-of select="//CurrentUser/Phone"/><xsl:text>\par Fax: </xsl:text><xsl:value-of select="//CurrentUser/Fax"/>
<xsl:text>\par Email: </xsl:text><xsl:value-of select="//CurrentUser/EMailAddress"></xsl:value-of>
<xsl:text>\par\par All conditions must be met before instructing the solicitor. \par \cell \cell }\pard \intbl\row </xsl:text>

<xsl:for-each select="//Conditions/Condition">

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx9000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 __}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text>
<xsl:for-each select="./Description/Line">
<xsl:value-of select="."/><xsl:text> \par </xsl:text>
</xsl:for-each>
<xsl:text>}\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="./Status"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>
</xsl:for-each>

<!-- Catherine for pvcs #817 -->
<xsl:text>\pard \par \par {\f1\fs22\b </xsl:text>
<!-- Midori for pvcs #2815 -->
<xsl:for-each select="//ConditionNote/Line"><xsl:value-of select="." /><xsl:text> \par </xsl:text></xsl:for-each>
<!-- Midori for pvcs #2815 end -->
<xsl:text>}</xsl:text>
<!-- Catherine for pvcs #817 -->

<xsl:text>\pard \par \par {\f1\fs22\b\ul </xsl:text><xsl:value-of select="//BrokerConditionFooter"/><xsl:text>}</xsl:text>
</xsl:template> 



<xsl:template name="EnglishSubheader"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrnone\cellx7000

\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Conditions of Approval Outstanding For: }{\f1\fs22 </xsl:text><xsl:value-of select="//ClientName"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Closing Date: }{\f1\fs22 </xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 



<xsl:template name="EnglishHeaderTable"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 To:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFirmName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 From:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//LenderNameShort"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Attn:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Loan #:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Phone:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Lender Reference #:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//DealNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Fax:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Date:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Insurance Reference #:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIPolicyNumber"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Insurance Status:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIStatus"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>




<xsl:text>\pard \par</xsl:text> 

</xsl:template> 


<xsl:template name="EnglishPageNumber"> 
<xsl:text>\pard {\f1\fs22 Page: }{\field {\*\fldinst {\cs16 PAGE }}}{\f1\fs22 \~\~ of\~\~ }{\field {\*\fldinst {\cs16 NUMPAGES }}}\pard\par</xsl:text>
</xsl:template> 


<xsl:template name="EnglishDocumentTitle"> 
<!--  insert logo here # 561 -->
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone \brdrw10
\clbrdrl\brdrnone \brdrw10
\clbrdrb\brdrnone \brdrw10
\clbrdrr\brdrnone \brdrw10\cellx10908
\pard \ql\intbl </xsl:text>
<xsl:call-template name="logo-en"/>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 

<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \qr\intbl </xsl:text>

<xsl:text>{\f1\fs24\b Conditions Outstanding}\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 

</xsl:template> 

<xsl:template name="EnglishHeader">
<xsl:text>
{\header \pard\plain \qr \widctlpar\faauto\itap0 {\f1\fs22 Page: }{\field{\*\fldinst {PAGE }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{\f1\fs22 \~\~ of\~\~ }{\field{\*\fldinst {NUMPAGES }}{\fldrslt {\lang1024\langfe1024\noproof 4}}}{\par }\pard \ql \widctlpar\faauto\itap0 {\f1\fs22 \par }}
</xsl:text>
</xsl:template>

<xsl:template name="EnglishFooter">
<xsl:text>
{\footer 
{\qc\f1\fs22 ______________________________________________________________________________________\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//LenderName"/><xsl:text>\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//BranchAddressComplete"/><xsl:text>\par}
{\qc\f1\fs22 Phone: </xsl:text><xsl:value-of select="//BranchPhone"/><xsl:text>   Fax: </xsl:text><xsl:value-of select="//BranchFax"/><xsl:text>\par}
{\qc\f1\fs22 Toll-free Phone: </xsl:text><xsl:value-of select="//BranchTollPhone"/><xsl:text>   Toll-free Fax: </xsl:text><xsl:value-of select="//BranchTollFax"/><xsl:text>\par}
</xsl:text>
</xsl:template>

<!-- ************************************************************************ -->
<!-- French template section                                                  -->
<!-- ************************************************************************ -->
<xsl:template name="FrenchConditionList"> 

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx9000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl 

{\f1\fs22\b \cell Documents examinés par:\par </xsl:text><xsl:value-of select="//CurrentUser/Name"/><xsl:text>\par Téléphone: </xsl:text><xsl:value-of select="//CurrentUser/Phone"/><xsl:text>\par Télécopieur: </xsl:text><xsl:value-of select="//CurrentUser/Fax"/>
<xsl:text>\par Email: </xsl:text><xsl:value-of select="//CurrentUser/EMailAddress"></xsl:value-of>
<xsl:text>\par \par Toutes les conditions doivent être remplies pour finaliser le dossier.</xsl:text><xsl:text> \par\cell \cell }
\pard \intbl\row </xsl:text>

<xsl:for-each select="//Conditions/Condition">

<xsl:text>\trowd\trkeep\trgaph108\trleft-108 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx9000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 __}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text>
<xsl:for-each select="./Description/Line">
<xsl:value-of select="."/><xsl:text> \par </xsl:text>
</xsl:for-each>
<xsl:text>}\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="./Status"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>
</xsl:for-each>

<!-- Catherine for pvcs #817 -->
<xsl:text>\pard \par \par {\f1\fs22\b </xsl:text>
<!-- Midori for pvcs #2815 -->
<xsl:for-each select="//ConditionNote/Line"><xsl:value-of select="." /><xsl:text> \par </xsl:text></xsl:for-each>
<!-- Midori for pvcs #2815 end -->
<xsl:text>}</xsl:text>
<!-- Catherine for pvcs #817 -->

<xsl:text>\pard \par \par {\f1\fs22\b\ul </xsl:text><xsl:value-of select="//BrokerConditionFooter"/><xsl:text>}</xsl:text>
</xsl:template> 



<xsl:template name="FrenchSubheader"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrnone\cellx7000

\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Conditions d’approbation non remplies pour: }{\f1\fs22</xsl:text><xsl:value-of select="//ClientName"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs20\b Date de clôture: }{\f1\fs22 </xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text> }</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 



<xsl:template name="FrenchHeaderTable"> 
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 À:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFirmName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 De:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//LenderNameShort"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Attn:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceName"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 No du prêt:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Téléphone:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 No de référence du prêteur:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//DealNum"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx1440

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 Télécopieur:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Date:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>


<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx3240

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5040

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx8100

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908
\pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 No de référence de l’assurance:}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIPolicyNumber"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl</xsl:text>

<xsl:text>{\f1\fs22 Statut de l’assurance:}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>

<xsl:text>{\f1\fs22 </xsl:text><xsl:value-of select="//MIStatus"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl\row </xsl:text>




<xsl:text>\pard \par</xsl:text> 

</xsl:template> 


<xsl:template name="FrenchPageNumber"> 
<xsl:text>\pard {\f1\fs22 Page: }{\field {\*\fldinst {\cs16 PAGE }}}{\f1\fs22 \~\~ de\~\~ }{\field {\*\fldinst {\cs16 NUMPAGES }}}\pard\par</xsl:text>
</xsl:template> 


<xsl:template name="FrenchDocumentTitle"> 
<!--  insert logo here # 561 -->
<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone \brdrw10
\clbrdrl\brdrnone \brdrw10
\clbrdrb\brdrnone \brdrw10
\clbrdrr\brdrnone \brdrw10\cellx10908
\pard \ql\intbl </xsl:text>
<xsl:call-template name="logo-fr"/>
<xsl:text>\cell \pard \intbl\row </xsl:text>

<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \qr\intbl </xsl:text>

<xsl:text>{\f1\fs24\b Conditions non remplies}\cell \pard \intbl\row </xsl:text>

<xsl:text>\pard \par</xsl:text> 
</xsl:template> 

<xsl:template name="FrenchHeader">
<xsl:text>
{\header \pard\plain \qr \widctlpar\faauto\itap0 {\f1\fs22 Page: }{\field{\*\fldinst {PAGE }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{\f1\fs22 \~\~ de\~\~ }{\field{\*\fldinst {NUMPAGES }}{\fldrslt {\lang1024\langfe1024\noproof 4}}}{\par }\pard \ql \widctlpar\faauto\itap0 {\f1\fs22 \par }}
</xsl:text>
</xsl:template>

<xsl:template name="FrenchFooter">
<xsl:text>
{\footer 
{\qc\f1\fs22 ______________________________________________________________________________________\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//LenderName"/><xsl:text>\par}
{\qc\f1\fs22 </xsl:text><xsl:value-of select="//BranchAddressComplete"/><xsl:text>\par}
{\qc\f1\fs22 Téléphone: </xsl:text><xsl:value-of select="//BranchPhone"/><xsl:text>   Télécopieur: </xsl:text><xsl:value-of select="//BranchFax"/><xsl:text>\par}
{\qc\f1\fs22 Téléphone sans frais: </xsl:text><xsl:value-of select="//BranchTollPhone"/><xsl:text>   Télécopieur sans frais: </xsl:text><xsl:value-of select="//BranchTollFax"/><xsl:text>\par}
</xsl:text>
</xsl:template>

<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<xsl:template name="logo-en">
<xsl:text>
{\pict\wmetafile8\picwgoal2423\pichgoal982 
010009000003c69d00000000a19d000000000400000003010800050000000b0200000000050000
000c026600f600030000001e000400000007010400a19d0000410b2000cc006900ff0000000000
6500f5000000000028000000ff000000690000000100180000000000003b010000000000000000
000000000000000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffff
ffffff8c8c8c393939000000000000636363c6c6c6fffffffffffff7f7f7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffa5a5a5000000adadadefefef
f7f7f7949494212121adadadffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff525252000000ffffffffffffffffffffffff393939636363
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
f7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffbdbdbd000000a5a5a5ffffffffffffcecece181818000000ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff000000fffffffffffffffffffffffff7f7f7dedede4a4a4a525252cececeffffffffffff
ffffffffffffffffffdedede848484000000424242101010525252cececeffffffffffff7b7b7b
525252adadadefefef7b7b7b4a4a4a9c9c9cf7f7f7ffffff5252524a4a4ab5b5b5ffffffefefef
525252000000101010949494ffffffffffffffffffffffffffffffffffffffffffefefef5a5a5a
0000004242420000006b6b6bd6d6d6ffffffffffffa5a5a54a4a4a9c9c9cf7f7f7ffffffffffff
ffffffffffffffffffffffffdedede5a5a5a080808000000080808393939d6d6d6ffffffffffff
6b6b6b0000000808085252526363630000005a5a5aefefef393939424242c6c6c6ffffffffffff
949494292929000000212121424242b5b5b5ffffff6363631010100000000808088c8c8cffffff
ffffffffffffffffffffffffffffff8c8c8c5a5a5ab5b5b5ffffffffffffffffffffffffcecece
5a5a5a8c8c8cffffffd6d6d65252527b7b7bffffffffffffc6c6c65a5a5a8c8c8cffffffdedede
292929000000525252949494292929424242d6dedeadadad4a4a4aa5a5a5ffffffffffff9c9c9c
525252adadadffffffffffffd6d6d66363630808080000004a4a4aadadadffffff5252525a5a5a
dededef7f7f7525252636363e7e7e7ffffffffffff4a4a4a424242ffffffffffff4a4a4a080808
000000000000000000000000737373ffffffffffffffffffffffffffffff848484212121000008
0000004a4a4abdbdbdffffffffffffffffffcecece6b6b6b101010000000313131949494ffffff
ffffff636363525252e7e7e7ffffffffffffffffffffffffdedede3939394a4a4affffffffffff
ffffffdedede525252636363efefefffffffefefef7b7b7b000000000000101010848484ffffff
ffffffbdbdbd4242420000000000004a4a4abdbdbdffffff949494101010080808000000848484
f7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffff
fffffffffffffffffff7f7f7000000000000d6d6d6ffffffffffffffffffffffffffffff6b6b6b
000000adadadffffffd6d6d6080808292929e7e7e7ffffff525252000000b5b5b5ffffff525252
000000adadadffffffffffff525252101010bdbdbdffffffd6d6d6000000393939efefefd6d6d6
ffffffffffffffffffffffffffffffffffffffffff292929080808cececeffffffcecece000000
4a4a4af7f7f7ffffff8c8c8c0000008c8c8cffffffffffffffffffffffffffffffffffffffffff
c6c6c6181818d6d6d6ffffffdedede313131000000dededee7e7e7000000101010adadad8c8c8c
101010000000d6d6d6ffffff393939000000dededeffffff949494000000393939d6d6d6dedede
9c9c9c848484ffffff525252c6c6c6ffffff8c8c8c292929c6c6c6ffffffffffffffffffffffff
ffffff4a4a4a0000009c9c9cffffffffffffffffffffffffcecece000000525252ffffffd6d6d6
0000005a5a5affffffffffffbdbdbd0000005a5a5affffff525252000000848484adadad424242
0000008c8c8cf7f7f79c9c9c0000008c8c8cffffffffffff8c8c8c000000949494ffffffdedede
181818000000adadade7e7e79494948c8c8cf7f7f7000000000000ffffffffffff000000000000
ffffffffffffffffff000000101010fffffff7f7f70000000808088484849c9c9c949494adadad
e7e7e7ffffffffffffffffffffffffffffff0000007b7b7bffffffefefef525252000000c6c6c6
ffffffd6d6d6101010000000a5a5a5d6d6d6adadad292929f7f7f7ffffff080808000000ffffff
ffffffffffffffffffffffffadadad000000000000e7e7e7ffffffffffffcecece000000080808
fffffff7f7f7525252000000737373cececec6c6c64a4a4ae7e7e7c6c6c6000000101010bdbdbd
c6c6c6a5a5a54a4a4affffff9494946b6b6bffffffadadad000000b5b5b5ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffff
000000101010cececeffffffffffffffffffffffffdedede000000000000f7f7f7ffffffffffff
636363000000949494ffffff636363000000cececeffffff4a4a4a000000d6d6d6ffffffffffff
4a4a4a000008cececeffffffd6d6d60000004a4a4affffffffffffffffffffffffffffffffffff
ffffffffffffb5b5b50000004a4a4affffffffffffffffff313131000000adadadffffff949494
000000949494fffffffffffff7f7f7fffffffffffffffffffffffff7f7f79c9c9cffffffffffff
ffffff9c9c9c000000949494cecece080808393939ffffffffffff000000101010ffffffffffff
636363000000cececef7f7f7000000080808e7efe7fffffffffffffffffff7f7f7fffffff7f7f7
f7f7f7efefef7373730000009c9c9cffffffffffffffffffffffffffffff5a5a5a0000009c9c9c
fffffffffffff7f7f7ffffffd6d6d6080808424242ffffffcecece0000004a4a4affffffffffff
d6d6d60000005a5a5affffff4a4a4a000000cececeffffff4a4a4a000000d6d6d6ffffff8c8c8c
000000949494ffffffffffff9494940000008c8c8cffffff7373730000005a5a5aefefefffffff
ffffffffffffffffff000000000000ffffffffffff000000000000ffffffffffffffffff000000
000000ffffffffffff525252393939bdbdbdd6d6d6ffffffffffffffffffffffffffffffffffff
ffffffffffffadadadffffffffffffffffffd6d6d6000000393939ffffff8484840000007b7b7b
f7f7f7fffffffffffff7f7f7ffffffffffff000000000000ffffffffffffffffffffffffffffff
4a4a4a0000002929297b7b7bffffffffffffd6d6d6000000212121ffffffcecece000000080808
e7e7e7ffffffffffffffffffffffff212121000000b5b5b5ffffffffffffffffffffffffffffff
e7e7e7ffffffffffff9494940000005a5a5af7f7f7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff000000ffffffffffffffffffffffffffffffffffff000000000000c6c6c6ffffff
ffffffffffffffffffb5b5b5101010525252fffffff7f7f7ffffffa5a5a5080808525252ffffff
525252000000d6d6d6ffffff6b6b6b000000cececeffffffffffff525252000000d6d6d6ffffff
d6d6d60000085a5a5affffffffffffbdbdbd5252525252525a5a5a4a4a4affffff949494000000
525252fffffffffffff7f7f76363630808088c8c8cffffff9494940808088c8c8cffffffe7e7e7
5a5a5a5252525252525a5a5adededeffffffffffffffffffffffffffffff6363630000085a5a5a
f7f7f75a5a5a0000009c9c9cd6d6d6000000000000ffffffffffff4a4a4a000000cececed6d6d6
000000000000d6d6d6cececed6d6d6cececed6d6d6ffffffcecece6b6b6b000000000000000000
adadadffffffffffffffffffffffffffffff5252520000008c8c8cffffffffffffffffffffffff
cecece0000005a5a5affffffd6d6d6000000636363ffffffffffffcecece0808084a4a4affffff
b5b5b50000004a4a52bdbdbd636363080808cececeffffffa5a5a50000009c9c9cffffffffffff
9c9c9c000000949494ffffff4242420000009c9c9cffffffffffffffffffffffffffffff000000
000000ffffffffffff000000000000ffffffffffffffffff000000000000ffffffffffff6b6b6b
0000006b6b6b212121292929949494ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffa5a5a5000000080808ffffff4a4a4a0000009c9c9cd6d6d6cececed6d6d6cecece
f7f7f7ffffff000000000000ffffffffffffffffffffffffdedede000000181818a5a5a5181818
ffffffffffffcecece000000525252ffffff9494940000004a4a52ffffffffffffffffffffffff
ffffff000000000000adadadd6d6d6d6d6d6d6d6d6c6c6c6fffffff7f7f77b7b7b000000080808
000000848484ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffff
ffffffffffffffffffffffffffffff000000000000adadaddededeffffffffffffffffffb5b5b5
0000004a4a4affffffffffffffffff949494000000525252ffffff525252000000d6d6d6ffffff
8c8c8c000000c6c6c6ffffffffffff525252000000cececeffffffcecece000000424242ffffff
ffffff9c9c9c080808000000000000080808f7f7f7949494000000525252ffffffffffffffffff
5252520000009c9c9cffffff8c8c8c0000009c9c9cffffffdedede080808000000000000000000
c6c6c6fffffffffffff7f7f7c6c6c65a5a5a0000000000007b7b7bffffffffffffc6c6c66b6b6b
52525a000000000000ffffffffffff4a4a4a000000cececec6c6c6000000000000525252525252
4a4a4a000000000000ffffff3939390000000000003939398c8c8cffffffffffffffffffffffff
ffffffffffff5a5a5a0000009c9c9cfffffffffffff7f7f7ffffffcecece000000525252ffffff
c6c6c6000000424242ffffffffffffc6c6c6080808424242ffffffffffffdedede9c9c9c4a4a4a
000000000000cececeffffff9494940000009c9c9cffffffffffff9494941010108c8c8cffffff
4a4a4a0000008c8c8cffffffffffffffffffffffffffffff000000000000ffffffffffff000000
000000efefefffffffffffff000000000000ffffffbdbdbd0808084a4a4affffffe7e7e7000000
101010ffffffffffffffffffffffffffffffffffffffffffffffffefefef949494181818000000
393939ffffff5252520000000000000000000808080000000000008c8c8cffffff000000000000
f7f7f7ffffffffffffffffff737373000000737373ffffff181818adadadffffffcecece000000
6b6b6bffffff9c9c9c000000525252ffffffffffffffffffffffffffffff000000000000181818
000000000000000000000000cecece7373730000001010100000007b7b7be7e7e7ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffff
ffffff000000000000080808181818080808737373e7e7e7dee7e7101010000000ffffffffffff
ffffff6363631010109c9c9cf7f7f75a5a5a000000bdbdbdffffff6b6b6b0000008c8c8cffffff
ffffff525252000000d6d6d6ffffffd6d6d6181818636363fffffffffffff7f7f7ffffffffffff
ffffffffffffffffffc6c6c60000004a4a4affffffffffffffffff292929000000b5b5b5ffffff
8c8c8c000000949494ffffffffffffefefeffffffffffffff7f7f7ffffffffffffcecece4a4a4a
000000080808000000181818f7f7f7ffffffdededef7f7f7ffffffffffff000000000000ffffff
ffffff636363000000d6d6d6f7f7f7000000181818f7f7f7ffffffffffff000000181818ffffff
000000000000cececeffffffe7e7e7fffffff7f7f7ffffffffffffffffffffffff4a4a4a000000
5a5a5a9494947b7b7b6b6b6bf7f7f7dedede000000525252ffffffd6d6d6000000292929efefef
ffffffb5b5b5080808424242ffffffffffffe7e7e7fffffff7f7f7737373101010cececeffffff
8c8c8c0000005a5a5affffffffffff7b7b7b0000009c9c9cffffff737373080808848484f7f7f7
ffffffffffffefefefffffff000000000000ffffffffffff080808080808bdbdbdffffffffffff
101010000000ffffffa5a5a50000004a4a4affffffffffff000000080808ffffffffffffffffff
fffffffffffffffffff7f7f78c8c8c000000000000000000000000bdbdbdffffff6b6b6b000000
8c8c8cffffffffffff636363000000b5b5b5ffffff000000000000b5b5b5efefefffffffffffff
181818000000b5b5b5ffffff7b7b7b424242ffffffd6d6d6000000292929ffffffc6c6c6000000
393939ffffffffffffffffffdee7e7ffffff292929080808c6c6c6ffffffffffff212121181818
e7e7e7636363000000adadadffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff000000ffffffffffffffffffffffffffffffffffff000000000000d6d6d6
f7f7f7636363000000636363ffffff7b7b7b000000848484ffffffdedede000000101010e7e7e7
ffffff525252000000b5b5b5ffffff5252520000000000008c8c8c848484000000080808e7e7e7
f7f7f7a5a5a5000000424242d6d6d6dededeffffffffffffffffffffffffffffffffffffffffff
393939000000b5b5b5ffffffadadad000000424242ffffffd6d6d68484840000007b7b7bc6c6c6
fffffffffffffffffffffffffffffffffffff7f7f7212121000000000000212121949494efefef
ffffffffffff848484848484bdbdbd7b7b7b000000292929ffffffffffff424242080808c6c6c6
ffffff848484000000adadadffffff9c9c9c0000007b7b7bffffff5a5a5a212129e7e7e7c6c6c6
393939f7f7f7ffffffffffffffffffffffffffffff52525200000052525284848494949c7b7b7b
ffffffd6d6d60000005a5a5affffffcecece000000000000737373adadad4a4a4a0000008c8c8c
ffffffd6d6d6313131bdbdbdc6c6c6212121000000dededeffffff9c9c9c0000000000009c9c9c
b5b5b5181818000000adadadffffffdedede000008101010cececeffffffadadad737373ffffff
000000000000efefefffffff080808000000424242c6c6c6636363000000181818ffffffcecece
000008292929ffffffd6d6d6000000080808d6d6d6f7f7f7ffffffffffffffffffffffff6b6b6b
000000000000000000636363cececeffffffffffffc6c6c6080808424242fffffff7f7f7393939
080808f7f7f7ffffff000000000000000000181818848484b5b5b5000000212121ffffffffffff
bdbdbd000000d6d6d6cecece000000000000ffffffffffff424242000000949494ffffffd6d6d6
212129f7f7f7adadad000000737373ffffffdedede0000004a4a4affffff6b6b6b000000e7e7e7
f7f7f7181818f7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000
ffffffffffffffffffffffffffffffffffff000000000000d6d6d6ffffffcecece000000000000
efefefffffff9494942929291010100000005a5a5aefefefffffffffffff8c8c8c525252b5b5b5
ffffff848484525252a5a5a5636363000000313131b5b5b5ffffffbdbdbd525252000000212121
525252dededef7f7f7fffffffffffffffffffffffffffffffffffff7f7f77b7b7b212121000000
101010848484efefefffffff8484843939390000003131315a5a5affffffffffffffffffffffff
ffffffffffffd6d6d6000000000000b5b5b5fffffffffffffffffffffffff7f7f7e7efe75a5a5a
000000000000636363cececeffffffffffff636363000000d6d6d6ffffffffffff9c9c9c313131
0000001010108c8c8cf7f7f7ffffffd6d6d65a5a5a0000000000007b7b7befefefffffffffffff
ffffffffffffffffff525252000000949494ffffffffffffffffffffffffb5b5b53939395a5a5a
ffffffdedede3939397373737373731818180000005a5a5aefefeffffffff7f7f7737373080808
000000212121949494ffffffffffff9c9c9c4a4a4a7b7b7b5a5a5a000000000000949494ffffff
ffffffffffffd6d6d65a5a5a0000000000001010109c9c9cffffff212121313131cececeffffff
292929212121adadad181818000000424242d6d6d6ffffffffffffb5b5b5393939212121000000
313131525252848484f7f7f7ffffffffffffffffffffffff0808080000007b7b7befefefffffff
ffffffffffffffffffffffffb5b5b53131312121211010104a4a4ac6c6c6ffffffffffff181818
3939399c9c9c3131315252524a4a4a424242848484fffffffffffff7f7f7393939636363cecece
292929212121ffffffffffffefefef949494080808000000000000525252efefefffffff949494
2929292929290000005a5a5acececeffffffefefef6b6b6b000000000000313131efefeff7f7f7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffff
ffffffffffff000000000000bdbdbdffffffd6d6d6000008000000dededeffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffb5b5b5000000424242fffffff7f7f7ffffffffffff
fffffffffffffffffffffffffffffffffffff7f7f7f7f7f7ffffffffffffffffffffffffffffff
ffffffb5b5b5000000d6d6d6ffffffffffffffffffffffffffffffffffffffffffd6d6d6000000
5a5a5affffffffffffffffffcececeffffffffffffffffffffffffffffffffffffffffffffffff
fffffff7f7f75a5a5a080808c6c6c6ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff4a4a4a
0000009c9c9cfffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffff
fffffffffffffffffff7f7f7fffffffffffffffffffffffffffffffffffffffffff7f7f7ffffff
fffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffff
fffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff000000000000fffffffffffffffffff7f7f7ffffffffffffffffff
fffffffffffffffffffffffff7f7f7ffffffffffffffffffefefefffffffffffffffffffffffff
fffffffffffff7f7f7ffffffffffffffffffefefefffffffffffffffffffefefefffffffffffff
fffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffff000000000000
dededeefefef848484000000525252f7f7f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffff848484313131bdbdbdffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffefefef4242424a4a4affffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedede101010f7f7f7
fffffffffffffffffffffffffffffffffffffffffff7f7f7393939313131efefefffffffc6c6c6
6b6b6bfffffffffffff7f7f7ffffffffffffffffffffffffffffffffffffffffff4a4a4a000000
cececeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff4a4a52000000949494ffffffffffff
d6d6d6f7f7f7cecece6363638c8c8cffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff636363737373efefefffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
737373000000dededeffffffefefef737373dededeffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffdedede5a5a5a7b7b7bffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
000000ffffffffffffffffffffffffffffffe7e7e7080808000000000000181818000000313131
c6c6c6ffffffffffffffffffffffffffffffffffffffffffffffffffffffefefef424242000000
adadadfffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffdedede
bdbdbdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff6b6b6b4a4a4a313131d6d6d6ffffffffffff
ffffffffffffffffffffffffbdbdbd393939212121424242000000212121dededeffffffffffff
ffffffffffffffffffffffffffffffffffffffffff080808000000cececeffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363000000000000000000000000000000c6c6c6bdbdbd000000
212121ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffe7e7e7000000000000dedede
fffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffe7e7e7424242000000525252
292929000000bdbdbdfffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
cecece000000313131fffffffffffff7f7f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffff
fffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffdededeb5b5b5efefefffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffefefef525252080808b5b5b5ffffffffffffffffffffffffffffffffffff
ffffffffffffd6d6d6d6d6d6efefeff7f7f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff212121292929cececeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
e7e7e7f7f7f7ffffffffffffffffffcececefffffff7f7f79c9c9cbdbdbdffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffadadada5a5a5ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffced6d6dededecececef7f7f7ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffefefefadadadb5b5b5ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefef
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff7fff7ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffff
fffffffffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffff
fff7fffffffffffffffffffffffff7fffffffffffffffffffffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffff
fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffff7f7f7fffffffffffff7f7f7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff000000fffffffffffffffffffffffffffffffffffffffff7ffffef
ffffefffffe7ffffefffffefffffefffffeffffff7ffffeffffff7ffffeffffff7fffff7fffff7
ffffeffffff7fffff7fffff7ffffeffffff7fffff7fffffffffff7fffffffffffffffffffffff7
fffffffffff7fffff7fffff7fffff7fffff7fffff7ffffeffffff7fffff7fffff7ffffeffffff7
fffff7fffff7ffffefffffefffffefffffefffffefffffefffffeffffff7ffffefffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffefefeffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffff
fffffffffffff7f7f7fffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffff
fffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
000000ffffffffffffffffffffffffffffffffffffced6adc6d694bdd68cc6d68cbdd68cc6d68c
bdd68cc6d694bdce8cc6d694bdce94c6d69cbdce94c6d694bdce94c6d694bdce94c6d694bdd694
c6d694bdce94c6d694bdce94c6d69cbdce9cc6ce9cbdce9cc6d69cbdce94c6d69cbdce94c6d694
bdce94c6d694bdd694c6d694bdce94c6d694bdce94c6d69cbdce94c6d694bdd694c6d694bdd68c
c6d68cbdd68cc6d694bdd68cc6d68cbdce94cedeb5fffff7ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefefa5a5a5636363
4242420000000000000000004a4a4a8c8c8cdededeffffffffffffffffffffffffffffffffffff
ffffff8484848c8c8c949494bdbdbdffffffffffffffffffbdbdbd8c8c8c8c8c8c848484ffffff
ffffffffffff8c8c8c9494948c8c8cc6c6c6ffffffffffffe7e7e77373738484848c8c8cbdbdbd
f7f7f7ffffff9c9c9c9494948c8c8cadadadffffffffffffffffffe7e7e78c8c8c8c8c8c848484
dededef7f7f7fffffffffffff7f7f7bdbdbd8484844a4a4a101010525252737373bdbdbdffffff
fffffffffffffffffffffffffffffffffffffffffffffffff7f7f7b5b5b55a5a5a181818000000
1010103939398c8c8cdededeffffffffffffffffffffffffffffffffffffd6ded68484845a5a5a
181818313131737373b5b5b5efefefffffffffffffffffffffffffadadad8c8c8c8c8c8ca5a5a5
ffffffffffffffffffffffffa5a5a54a4a4a2929293939399c9c9cefefef9c9c9c949494949494
8c8c8cffffffffffffffffffffffff949494313131212129636363949494ffffffe7e7e7737373
949494737373c6c6c6f7f7f7ffffffffffffffffffd6d6d69494944a4a4a1818184a4a4a636363
adadadffffffffffffffffffffffffffffff7b7b7b8c8c8c8c8c8cc6c6c6f7f7f7ffffffffffff
bdbdbd8c8c8c9c9c9c8c8c8cffffffffffffc6c6c68484844242420000005252527b7b7be7e7e7
ffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffff
ffffffffffffffffe79cad6b738c217394106b8c087394086b8c087394086b8c087394086b8c08
7394106b8c10738c106b8c10738c186b8c10738c106b8c087394086b8c087394086b8c08739408
6b8c08738c106b8c107394106b8c087394086b8c087394086b8c087394086b8c087394086b8c08
738c106b8c10738c186b8c10738c106b8c107394106b8c087394086b8c087394106b8c08739408
6b8c087b94219cad6bffffefffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff9494944242420000000000000808083131315a5a5a080808
1010100000000000006b6b6bcececeffffffffffffffffffffffffffffff080808000000000000
949494ffffffffffffffffff949494000000000000080808ffffffffffffffffff000000000000
000000949494ffffffffffffcecece0000000000000808088c8c8cffffffffffff5a5a5a000000
000000525252ffffffffffffffffffcecece080808000000000000cececefffffffffffff7f7f7
7b7b7b181818000000080808000000000000000000212121adadadffffffffffffffffffffffff
ffffffffffffffffffffffffcecece000000000000080808313131424242212121000000313131
adadadffffffffffffffffffffffffadadad3939390000001010108c8c8c4a4a4a000000000000
737373dededeffffffffffffffffff5a5a5a0000000000005a5a5affffffffffffffffff6b6b6b
0808080000000000000000000000004a4a4a3939390808080000004a4a4affffffffffffffffff
9494940808080808080000003939394a4a4affffffd6d6d6000000000000000000949494ffffff
ffffffffffff9c9c9c3939390000001818187b7b7b424242080808000000393939e7e7e7ffffff
ffffffffffff080808000000000000949494ffffffffffffffffff8c8c8c000000000000000000
ffffffffffff1010100000003131314a4a4a4a4a4a0000002121219c9c9cffffffffffffffffff
ffffffffffffffffffffffffffffff000000fffffffffffffffffffffffffffff7ffffef94ad5a
6b8c086b9400739c006b9400739c006b9400739c006b9400739c006b94007394006b9400739c08
6b94007394006b9400739c006b9400739c006b9c00739c006b9400739c006b9400739c006b9400
739c006b9400739c006b9c00739c006b9c00739c006b9400739c006b9400739c086b9400739400
6b9400739c006b9400739c006b9400739c006b9400739c006b9400739c006b84089cad63ffffef
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
737373000000000000000000525252dededeffffffffffffffffffb5b5b5181818000000000000
313131cececeffffffffffffffffffffffff0000000808080000008c8c8cffffffffffffffffff
9c9c9c000000000000000000ffffffffffffffffff0000000000000000008c8c8cffffffffffff
d6d6d60000000000000000008c8c8cffffffffffff4a4a4a000000000000525252ffffffffffff
ffffffcecece000000000000000000d6d6d6ffffffffffff949494000000000000101010636363
efefefffffffe7e7e76b6b6b737373fffffffffffffffffffffffffffffffffffff7f7f7ffffff
e7e7e7000000212121bdbdbdffffffffffffe7e7e7636363000000080808c6c6c6ffffffffffff
c6c6c6181818000000292929cececefffffff7f7f7313131000000000000525252ffffffffffff
ffffff4a4a4a000000000000525252fffffff7f7f7e7e7e70000000000000000000000007b7b7b
7b7b7b2121210000000000000000005a5a5affffffffffffffffff292929000000000000393939
ffffffffffffffffffc6c6c6000000000000000000949494ffffffffffffc6c6c6212121000000
080808b5b5b5ffffffffffff525252000000000000525252ffffffffffffffffff000000000000
0000009c9c9cffffffffffffffffff949494000000000000000000ffffffffffff181818313131
dededeffffffffffff636363000000212121f7f7f7ffffffffffffffffffffffffffffffffffff
ffffff000000ffffffffffffffffffffffffffffffffffef9cb563739408739c006b9c00739c00
739c00739c006b9c00739c00739c00739c006b9400739c00739c007ba5006b9400739c00739c00
6b9c00739c0073a5006b9c007ba500739c00739c006b94006b94006b94007ba500739c006b9c00
6b9c007ba5006b9c0073a5006b9c00739c00739c00739c006b9400739c00739c00739c006b9c00
739c00739c00739c006b9c00739c00739c0073940894ad5afffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff949494000000000000000000636363
f7f7f7ffffffffffffffffffffffffffffffdedede080808000000000000292929ced6d6ffffff
ffffffffffff000000080808000000949494ffffffffffffffffff9c9c9c000000000000000000
ffffffffffffffffff0000000000000000009c9c9cffffffffffffcecece000000000000000000
949494ffffffffffff5a5a5a000000000000525252ffffffffffffffffffcecece000000000000
000000d6d6d6ffffffefefef080808000000000000313131ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff313131a5a5a5ffffff
fffffffffffffffffff7f7f7000000000000212121ffffffffffff212121000000000000292929
ffffffffffffffffff9c9c9c000000000000000000d6d6d6ffffffffffff525252000000000000
525252ffffffffffff9c9c9c0000000808080000008c8c8cffffffffffffcecece000000000000
0000005a5a5affffffffffffffffff000000000000000000525252ffffffffffffffffffcecece
0000000000000000009c9c9cffffffffffff393939000000000000181818ffffffffffffffffff
b5b5b5000000000000000000b5b5b5ffffffffffff000000000000000000949494ffffffffffff
ffffff949494101010080808000000ffffffffffff8c8c8cd6d6d6ffffffffffffffffff8c8c8c
000000080808d6d6d6ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffff
fffffffffffffffffffffff794ad5a7394086b9c00739c006b9400739c006b9c00739c006b9400
739c00739c00739c006394006b94006394006b94006394006b9c0073a5007ba500739c00739c00
739c006b94006b94007b9c107b9c107394086b8c007b9c00739c007ba500739c00739c006b9400
6b9400638c006b94006b94006b94006b9400739c006b9c00739c006b9400739c006b9c00739c00
6b9c00739c006b94009cad5affffefffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffdedede424242000000000000080808bdbdbdffffffffffffffffffffffff
ffffffffffffffffff9c9c9c0000000000000000005a5a5affffffffffffffffff000000000000
000000949494ffffffffffffffffff9c9c9c000000000000000000ffffffffffffffffff000000
000000000000949494ffffffffffffcecece000000000000000000949494ffffffffffff525252
0000000000005a5a5affffffffffffffffffcecece000000000000000000cececeffffffb5b5b5
000000000000000000adadadffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffb5b5b5fffffffffffffffffffffffffffffff7f7f7
080808000000080808dededededede000000000000000000636363ffffffffffffffffffd6d6d6
000000000000000000848484ffffffffffff525252000000000000525252ffffffffffff9c9c9c
000000000000000000bdbdbdfffffffffffff7f7f7000000000000000000525252ffffffffffff
ffffff0000000000000000005a5a5affffffffffffffffffd6d6d6000000000000000000949494
ffffffefefef1010100000000000004a4a4affffffffffffffffffe7e7e7000000000000000000
636363ffffffffffff080808000000000000949494ffffffffffffffffff949494000000000000
000000ffffffffffffffffffffffffffffffcecece6b6b6b000000000000000000c6c6c6ffffff
ffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffef
9cb55a6b9408739c00739c00739c006b9400739c00739c00739c006b9c00739c00739c00739c00
739c00739c00739c00739c006b9c006b9c006b9400739c086b8c00638408738c21adc673d6efa5
e7f7b5adbd6b7394216384086b9400739c00739c00638c007ba500739c00739c006b9400739c00
739c00739c006b9400739c00739c00739c006b9400739c00739c00739c006b9c0073940894ad5a
fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa5a5a5
0000000000000000004a4a4afffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
000000000000000000000000c6c6c6ffffffffffff000000000000000000949494ffffffffffff
ffffff9c9c9c000000000000000000ffffffffffffffffff0000000000000000009c9c9cffffff
ffffffcecece000000000000000000949494ffffffffffff525252000000000000525252ffffff
ffffffffffffc6c6c6101010000000000000cececeffffff7b7b7b000000080808000000cecece
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffb5b5b5000000000000000000cecece
d6d6d6000000000000000000949494ffffffffffffffffffffffff000000000000000000313131
ffffffffffff525252000000000000525252ffffffffffff9c9c9c000000000000080808d6d6d6
ffffffffffffffffff0000000000000000005a5a5affffffffffffffffff000000000000000000
525252ffffffffffffffffffcecece0000000000000000008c8c8cffffffcecece000000000000
0000007b7b7bffffffffffffffffffffffff000000000000080808080808ffffffffffff000000
0000000000008c8c8cffffffffffffffffff9c9c9c000000000000101010ffffffffffffffffff
d6d6d6525252080808000000000000000000000000dededeffffffffffffffffffffffffffffff
ffffffffffff000000ffffffffffffffffffffffffffffffffffef94ad527394086b9c00739c00
6b9c00739c006b9400739c006b9400739c00638c006b9400739c00739c006b9400739c006b9400
6b9400739c007394006b8c08738c188ca54acede9cffffdeffffe7ffffe7ffffd6cede9c9cad5a
6384106b8c08739c08739c086b9400739c006b9c00739c006b94006b94006b94006b94006b9400
739c006b9400739c006b9400739c006b9c00739c006b94009cad5affffefffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff5a5a5a0000000000000000008c8c8c
ffffffffffffffffffffffffffffffffffffffffffffffffffffff424242000000080808000000
6b6b6bffffffffffff000000000000080808949494ffffffffffffffffff949494000000000000
000000ffffffffffffffffff000000000000000000949494ffffffffffffd6d6d6000000000000
000000949494ffffffffffff5252520000000000004a4a4affffffffffffffffffd6d6d6080808
000000000000d6d6d6ffffff5a5a5a0000000000000000006b6b6b949494a5a5a59c9c9c949494
9c9c9c949494e7e7e7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff949494000000000000080808000000dededed6d6d6000000000000000000
9c9c9cffffffffffffffffffffffff000000000000000000000000ffffffffffff525252000000
000000525252ffffffffffff9c9c9c000000000000000000d6d6d6ffffffffffffffffff000000
000000000000525252ffffffffffffffffff00000000000000000052525affffffffffffffffff
cecece0000000000000000009c9c9cffffffc6c6c60000000808080000008c8c8cffffffffffff
ffffffffffff000000000000000000000000ffffffffffff0000000000000000009c9c9cffffff
ffffffffffff949494000000000000000000ffffffffffffb5b5b5000000000000000000000000
0000000000005a5a5affffffffffffffffffffffffffffffffffffffffffffffff000000ffffff
ffffffffffffffffffffffffffffef9cb55a739400739c00739c00739c00739c00739c006b9400
739c00739c00739c00739c00739c006b9c00739c00739c00739c007394006b8c086b8c108ca542
bdd68cffffd6ffffe7f7ffd6bdce94bdce94eff7c6ffffe7ffffd6bdd68c8ca542739418638400
739408739c007ba500739c00739c006b9400739c00739c00739c006b9c00739c00739c00739c00
6b9c0073a500739c00739c0094ad52fffff7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff181818000000000000000000cececeffffffffffffffffffffffff
ffffffffffffffffffffffffffffff949494000000101010000000313131ffffffffffff000000
080808101010848484ffffffffffffffffff8c8c8c000000000000000000ffffffffffffffffff
0000000000000000009c9c9cffffffffffffcecece000000000000000000949494ffffffffffff
5a5a5a000000000000212121ffffffffffffffffffcecece000000000000000000cececeffffff
6363630000000000000000006363634a4a4a5a5a5a313131000000000000000000cececeffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb5b5b5313131101010
000000080808000000000000efefefdedede0000000000000000008c8c8cffffffffffffffffff
ffffff000000000000000000101010ffffffffffff525252000000000000525252ffffffffffff
9c9c9c000000080808000000cececeffffffffffffffffff0000000000000000005a5a5affffff
ffffffffffff000000000000000000525252ffffffffffffffffffcecece000000000000000000
9c9c9cffffffdedede000000080808000000737373ffffffffffffffffffffffff000000000000
000000080808ffffffffffff000000000000000000848484fffffff7f7f7ffffff7b7b7b181818
000000000000ffffffffffff3939390000000000000808080000000808087b7b7bf7f7f7ffffff
ffffffffffffffffffffffffffffffffffffffffff000000fffff7ffffffffffffffffffffffff
fffff794ad527394006b9c0073a5006b9c00739c006b9400739c006b9400739c00739c00739c00
6b9c00739c00739c006b9400638c006b8c107b9429adc673e7f7bdffffdeffffd6d6e7a594ad5a
637b18637b1894a552cede9cffffdeffffdeefffc6adc67b7b94296b8c106b8c086b8c00739c00
739c00739c006b9400739c006b9c00739c006b9400739c006b9400739c006b9c00739c006b9400
9cb552ffffefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
000000000000000000000000cececeffffffffffffffffffffffffffffffffffffffffffffffff
ffffffbdbdbd000000000000000000000000ffffffffffff0000000000000000004a4a4affffff
ffffffffffff6b6b6b000000000000080808ffffffffffffffffff000000000000000000949494
ffffffffffffcecece000000000000000000949494ffffffffffff525252000000000000080808
f7f7f7ffffffffffffb5b5b5000000000000000000cececeffffff9c9c9c000000000000000000
bdbdbdffffffffffff8c8c8c181818000000000000cececeffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd6d6d6393939000000000000000000000000000000000000848484
ffffffe7e7e7080808000000000000636363ffffffffffffffffffffffff000000000000000000
424242ffffffffffff525252000000000000525252ffffffffffff9c9c9c000000000000000000
d6d6d6ffffffffffffffffff000000000000000000525252ffffffffffffffffff000000000000
0000005a5a5affffffffffffffffffd6d6d6000000000000000000949494ffffffffffff000000
000000000000525252ffffffffffffffffffffffff080808000000000000393939ffffffffffff
000000000000000000636363ffffffffffffffffff525252000000000000000000ffffffffffff
0000000000000000002121217b7b7be7e7e7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff000000fffffffffffffffffffffffffffffffffff79cb55a6b940073a500
739c00739c006b9400739c00739c00739c006b9c00739c006b94006b9c00739c006b9400638400
7b942194ad5ae7f7bdffffdeffffe7e7f7b5a5bd636b8c106b8c00739c0084a510638c00738c10
9cb55ae7f7bdffffe7ffffe7deefb5a5b5637b9421638400638c00739c006b94006b9c006b9c00
739c00739c00739c006b9400739c00739c0073a500739c00739c0894ad5afffff7ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffff000000000000080808000000
b5b5b5ffffffffffffffffffffffffffffffffffffffffffffffffffffffc6c6c6000000000000
000000000000ffffffffffff000000000000000000292929e7e7e7ffffffefefef4a4a4a000000
000000080808ffffffffffffffffff0000000000000000009c9c9cffffffffffffcecece000000
000000000000949494ffffffffffff525252000000000000080808bdbdbdffffffffffff848484
080808000000000000d6d6d6ffffffd6d6d6181818000000000000adadadffffffffffff848484
000000000000212121f7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffcecece
2121210000000000000000001010100000000000007b7b7bffffffffffffffffff5a5a5a000000
000000393939ffffffffffffffffffcecece000000000000000000adadadffffffffffff525252
000000000000525252ffffffffffff9c9c9c000000000000000000cececeffffffffffffffffff
0000000000000000005a5a5affffffffffffffffff000000000000000000525252ffffffffffff
ffffffcecece000000000000000000949494ffffffffffff7b7b7b000000080808181818ffffff
ffffffffffffdedede000000000000000000848484ffffffffffff000000000000000000393939
dededeffffffefefef313131000000000000181818ffffffffffff0000000000004a4a4af7f7f7
ffffffffffffa5a5a5bdbdbdffffffffffffffffffffffffffffffffffffffffffffffff000000
fffffffffffffffffffffffffffffffffff794ad527394086b9c00739c006b9c00739c006b9400
739c006b9400739c00739c006b94006b94007394086b8c10849c39c6de94ffffdeffffdef7ffce
adc67b738c215a7b00739400739c006b94006b94007ba500739c006384006b8418adc67befffc6
ffffe7ffffdecede9c84a5397394106b94006b94006b9400739c006b9400739c006b9400739c00
6b9400739c006b9c00739c006b94009cad52ffffefffffffffffffffffffffffffffffffffffff
fffffffffffffffffff7f7f7ffffff101010080808000000000000949494ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffb5b5b5000000000000000000000000ffffffffffff
0000000000001010100000005a5a5a8c8c8c7b7b7b000000000000080808292929ffffffffffff
ffffff000000000000000000949494ffffffffffffd6d6d6000000000000000000949494ffffff
ffffff525252000000080808000000212121949494949494000000000000000000000000f7f7f7
ffffffffffff4a4a4a0000000000004a4a52ffffffffffff525252000000101010636363ffffff
ffffffffffffffffffffffffffffffffffffffffffffffff424242000000000008000000000000
000000212121adadadfffffffffffff7fff7ffffffbdbdbd181818000000000000d6d6d6ffffff
ffffff7b7b7b000000080808101010f7f7f7ffffffffffff525252000000000000525252ffffff
ffffff9c9c9c000000000000000000cececeffffffffffffffffff000000000000000000525252
ffffffffffffffffff00000000000000000052525affffffffffffffffffcecece000000000000
0000009c9c9cffffffffffffd6d6d6000000000000000000bdbdbdffffffffffffa5a5a5000000
000000080808efefefffffffffffff0000000808080000000000006363639494946b6b6b000000
000000000000424242ffffffffffff4a4a4a0000007b7b7bffffffffffffcecece1010105a5a5a
ffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffff
ffffffffffef9cb5527394007ba5006b9c0073a50073a5007ba500739c00739c007b9c00739c00
7394086b8c107b9431bdce8cf7ffd6ffffe7ffffd6c6de8c738c107b9c10739c00739c00739c00
6b9400739c00739c006b94007ba5006b9400739c005a84008ca539bdce8cffffdeffffe7ffffde
b5c67b739418638400739c08739c00739c086b9400739c00739c0073a500739c0073a500739c00
739c0094b54afffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
f7f7f76b6b6b000000080808000000737373ffffffffffffffffffffffffffffffffffffffffff
ffffffffffff8c8c8c0000000000000000004a4a4affffffffffff0000000000000000084a4a4a
212121000000000000000000000000292929b5b5b5ffffffffffffffffff000000000000000000
949494ffffffffffffcecece000000000000080808949494ffffffffffff636363000000080808
2929294a4a4a00000000000010101000000000000073737bffffffffffffffffffefefe7525252
000000000000737373737373080808000000394239e7e7e7ffffffffffffffffffffffffffffff
ffffffffffffffffffd6d6d60000000000000808080000002121219c9c9cf7f7f7ffffffffffff
ffffffffffffffffffffffffadadad1010100000001818189c9c9c737373101010000000393939
cececeffffffffffffffffff4a4a4a000000000000525252ffffffffffff949494080808000000
000000cececeffffffffffffffffff000000000000000000525252ffffff4a4a4a000000080808
000000000000000000000000525252ffffffcecece000000000000080808848484ffffffffffff
ffffffb5b5b51010100000002121217b7b7b848484181818000000393939cececeffffffffffff
ffffff000000000000000000424242313131000000000000000000000000181818adadadffffff
ffffffdedede2121210000005a5a5a737373313131000000424242f7f7f7ffffffffffffffffff
ffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffef94ad4a739400
6b9400739c006b94006b94006b9c007ba5006b9400739410638400738c189cad63e7f7bdffffe7
ffffe7c6de948cad4a6b8c087394006b94006b9400739c007ba500739c00739c00739c007b9c00
739c006b9c00739c006b9c006b8c007394188c9c4aced6a5ffffe7ffffe7e7f7bdadbd6b738c18
6384006b8c08739408739c00739c006b94006b9c006b94006b94006b94009cb54affffefffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c9c9c000000000000
080808393939f7fff7fffffffffffffffffffffffffffffffffffffffffff7f7f75a5a5a080808
000000000000949494ffffffffffff5252524a4a4a5a5a5ab5b5b5dedede525252080808000000
4a4a4aa5a5a5ffffffffffffffffffffffff0000000000000000009c9c9cffffffffffffe7e7e7
5252524a4a4a525252b5b5b5fffffff7f7f78c8c8c52525a4a4a4aa5a5a5dedede949494101010
000000292929848484fffffffffffff7f7f7ffffffffffffe7e7e794949c52525a000000080808
2929299c9c9cdededeffffffffffffffffffffffffffffffffffffffffffffffffffffffd6d6d6
0000000000000000005a5a5adededeffffffffffffffffffffffffffffffffffffffffffffffff
ffffffcecece737373212121000000080808424242a5a5a5e7e7e7ffffffffffffffffffffffff
5252520000000000004a4a4affffffffffffbdbdbd5252525a5a5a5a5a5adededeffffffffffff
ffffff6363635a5a5a636363848484f7f7f7949494737373000000000000000000181818525252
8c8c8cf7f7f7dedede5a5a5a4a4a4a5a5a5abdbdbdffffffffffffffffffffffffdedede7b7b7b
1818181818180000002929298c8c8ce7e7e7ffffffffffffffffffffffff5252524a4a4a636363
b5b5b5b5b5b57b7b7b0000000808084a4a4ab5b5b5ffffffffffffffffffffffffd6d6d6737373
1818180000003131318c8c8cc6c6c6ffffffffffffffffffffffffffffffffffffffffffffffff
000000ffffffffffffffffffffffffffffffffffef9cb5526b94006b9400739c00739c006b9c00
6b9c00638c006b8c08738c219cad63d6e7adffffe7ffffdee7f7b5a5bd637b9c216b8c086b8c00
7ba5006b94006b94007ba500639400739c00739c00638c00739400739408739400739c00739c00
6b94006b94006b8c087b9429a5b573deefb5ffffe7ffffe7deefb594ad5a7b94216b8c08638c00
6b9400739c006b9c00739c006b9400739c0094b54affffefffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffefefef292929000000000000000000bdbdbdffffff
ffffffffffffffffffffffffefefefffffffdedede000000000000000000000000efefefffffff
fffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff0000000000000000009c9c9cfffffffffffff7f7f7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffff7f7f7fffffffffffff7f7f7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdedede000000000000000000f7f7f7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffff525252000000000000525252
ffffffffffffefefeffffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffff
fffffffffffffffffff7f7f70000000000000000005a5a5affffffffffffffffffffffffffffff
fffffffffffff7f7f7fffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffff7f7f7ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffff
ffffffffffffffffef94ad4a7394086b9400739c00739c007ba5006b94006b8c0094ad4abdd68c
ffffdeffffe7efffc6bdce847b9418638400638c007ba508638c007ba5006b9c00739c00739c00
739c007394006b8c007394087b94105a7b007394106b9400739c006b94007ba5006b94006b8c00
5a7b087b8c31adbd84efffc6ffffdeffffe7bdd68c9cb5526b8c006b9400739c00739c00739c00
739c006b94009cb552ffffefffffffffffffffffffffffffffffffffffffffffffffffffffffff
f7f7f7ffffffffffffadadad0000000000000000006b6b6be7e7e7ffffffffffffffffffffffff
ffffffe7e7e7737373101010000000000000848484fffffffffffff7f7f7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000000000
000000949494fffffffffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe7e7e7080808000000424242f7f7f7ffffffffffffffffffefefef
7b7b7bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff5a5a5a0000000000004a4a4affffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
0000000000000000004a4a4affffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffff7f7f7fffffff7f7f7fffffffffffffffffffffffff7fff7ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffef9cb55a
739400739c00739c006b9c006b9400739c089cb54aefffbdffffe7ffffe7c6d68c849c216b8c08
7b9c08739c00739c007ba500739c00739c007b9c006b94006b8c009cb542d6ef94ffffd6ffffce
def7ada5b5637b9421739408638c00739c006b9c00739c006b94007b9c087394086b8c087b9429
c6d694ffffdeffffe7efffbda5bd5a739c08739c006b9400739c006b940073940894ad52fffff7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
7b7b7b181818000000000000848484efefeffffffffffffffffffff7f7f78c8c8c000000000000
0000005a5a5af7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff000000000000000000949494ffffffffffff
ffffff7b7b7b2121216b6b6bf7f7f7fffffff7f7f7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff6b6b6b000000000008d6d6d6ffffffffffffffffff737373000000efefefffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff5a5a5a0000000000005a5a5affffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffff7f7f75a5a5a0808080808084a4a4a
fffffffffffffffffff7f7f78c8c8c0808086b6b6be7e7e7ffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff000000fffffffffffffffffffffffffffffffffff794ad527394006b94007b9c006b9400
6b9400739408c6d684ffffdef7ffd694ad52849c216b8c00739c007ba50073a5006b9c00739c00
6b9400739c006b94006b8c006b8c10a5bd6b94a55a9cad6bf7ffceffffe7f7ffd6c6de949cb55a
6b8c08739c006b9c006b9400739c006b9c00739c006b94007394087394218c9c52efffceffffe7
bdd684739408638c00739c00739c006b94006b94009cb552ffffefffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff7f7f7ffffffffffffa5a5a5313131000000
0000001010107373738c8c8c7b7b7b2121290000000000000000005a5a5adededeffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff0000000000000000008c8c8cffffffffffffb5b5b5000000000000000000
adadadffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedede181818000000
0000007b7b7b8484845a5a5a000000000000b5b5b5f7f7f7ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5a5a5a000000000000
525252ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffefefefcecece4242426b6b6bf7f7f7ffffffffffffbdbdbd
000000000000000000848484fffffffffffff7f7f7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffff
fffffffffffffffffffffff79cb5526b9400739400739c007ba5006b9400739410c6d68cffffe7
bdc6946b84107394087ba5006b9c00739c00739c00739c00638c007b9c007b9c006b8c00849c18
b5ce7bffffdeffffe7bdce9c849452cedeadffffefffffefefffce738c217b9c086b9400639400
73a5007ba5006394006b9c007ba5007b9c08638408bdce9cffffdec6de8c6b8c086b9400739c00
7ba5006b940073940894ad52fffff7ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffefefef9c9c9c393939000000080808000000
1010100000000808086b6b6bd6d6d6f7f7f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000
000000000000949494fffffff7f7f7a5a5a5000000080808000000737373ffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7fffff7ffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffff
ffffffffffffffffffffffffffffffffffffffffffffffff6b6b6b080808000000101010000000
000000737373e7e7e7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffff5252520000000000005a5a5affffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe7e7e7ffffffffffffffffffadadad0000000808080000005a5a5a
fffffffffffffffffffffffff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffff000000fffffffffffffffffffffffffffffffffff7
94ad527394086b9400739c00739c00739c006b8c00c6d68cffffe7c6ce9c6b8c106b94006b9400
73a5006b9c006b94006b94007ba5086b9400638c007b9c10deef9ca5b56b9cad73dee7c6ffffef
bdce94ced6a5f7ffe7fffff7ffffe7738c31739408739c00739c00739c006b9c00739c00739c00
73a5006b9c00739408bdc694ffffefbdd6846b9408739c00739c006b9400739c006b94009cb552
ffffefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffcececececececececedededeffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff0000000000000000008c8c8cffffff
ffffffe7e7e7101010000000101010c6c6c6ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7f7efdee7cee7efceffffdefffff7ffffeffffff7ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffd6d6d6c6c6c6cececeffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff4a4a4a000000000000525252ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe7e7e7212121000000080808bdbdbdffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff000000fffffffffffffffffffffffffffffffffff79cb552739400739c00739c00
739c00739c00739408bdd684ffffefbdce9c7394106b9400739c006b9c00739c00739c00739400
6b94006b8c007394086b8c10ffffd6ffffdee7efc6848c6badb594adbd7bbdce94ffffeffffff7
ffffef7b8c427b94186b94007b9c08739c00739c006b9c00739c00739c00739c00739408c6ce9c
ffffefc6d68c6b8c087b9c006b9c0073a5006b9c00739c0094ad52fffff7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff8484849494949c9c9cb5b5b5ffffffffffffffffffe7e7e7dedede
efefeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
a5a5947384426b84296b84219cb56bd6e7a5ffffdefffff7ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c9c9c949494
8c8c8cadadadffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffff7f7f7d6d6d6f7f7f7f7f7f7ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffff
fffffffffffffffffffffffffffff794ad4a7394006b9400739c006b9c00739c006b8c00c6d684
ffffe7bdce9c6b8c08739c00739c00739c006b94006b94007394088cad299cb5399cbd42739418
bdd68cefffceffffeffffff7bdc6a57b8c42bdd694ffffe7fffff7ffffe794a56b94ad527b9418
6b8c106b8c08638c00739c00739c00739c006b9400739410bdce9cffffefbdd6846b9408739c00
739c00739c00739c006b94009cb552ffffefffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
f7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7e7cedead9cb563638410
6b8c106b8c18a5bd6bdeefbdfffff7ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff7f7f7fffffffffffff7f7f7ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffff
fffff79cb5526b9400739c00739c00739c00739c00739408bdd684ffffefbdce946b94006b9c00
84ad007ba5006b940063840073941894ad52e7ffadffffce738c21a5b56b8c9c63ced6b5fffff7
ffffefb5c684c6d69cffffe7adbd94cedeadf7ffd6ffffe7ffffdebdce948ca55a738c18638c00
739c0073a500739c00739410c6d69cffffe7c6d6846b8c087ba5006b9c0073a5006b9c00739c00
94b54afffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffefefffbd9cbd52638400638c00738c10a5bd73
ffffdeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff000000fffffffffffffffffffffffffffffffffff794ad4a7394006b9400
739c006b9c00739c006b8c00bdd684ffffe7c6d6946b94006b9c006b9c006b94006b8c0094ad4a
b5c684d6e7a58494399cb552849c39ffffdef7ffce9cad7b94a56bdeefb5c6d6948c9c5a7b8c52
a5b584b5c68c8c9c5ac6d69cffffefffffefdee7c6adbd7b84a5216b940073a500739c00739410
bdce94ffffe7bdd684739408739c00739c00739c00739c006b94009cb552ffffefffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffefffffe7c6de8c739410739c086b8c00738c18d6e7adffffefffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000
ffffffffffffffffffffffffffffffffffef9cb552739400739c00739c00739c00739c00739408
bdd684ffffefbdce947394086b9c00739c006b8c009cbd52deefb5ffffe7ffffefd6e7a5738c18
739418b5ce6bffffceffffd6d6e79c7b94428c9c528c9c63ffffe7ffffefffffd6bdd68c738c31
9cad6bfffff7fffff7ffffdeb5c67b7394086b9400739c006b9400c6d694ffffe7c6d68c6b8c08
7b9c006b9c00739c006b9400739c0094ad52fffff7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffef
efffbd7394186b94006b8c00739421a5b573fffff7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffff
fffffffffff794ad527394006b9400739c006b9400739c006b8c08c6d68cffffe7bdce946b8c00
739c00739c007b9c10bdd68cffffefffffefffffef738431738c107394087394007b9c21deef9c
a5bd6b849442e7efbdffffe7ffffdee7f7bdadc66b7b94186b84106b8429ffffefffffffffffef
c6d694739408739c006b9c006b9400bdce94ffffefbdce846b8c08739400739c006b9400739400
6b94009cb552ffffefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd6738c186b94007ba508
6b8c08849c4affffefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff000000fffffffffffffffffffffffffffffffffff79cb55a6b9408
739c00739c00739c00739408739418bdd68cffffefbdce94739408739c006b94006b8c08bdd68c
ffffeffffff7ffffef7394216b94007ba500739c007b9c186b8c18b5c68cffffeffffff7f7ffe7
bdce947b9418638c007ba5006b9400738c18ffffeffffff7ffffefb5c68c739410739c007ba500
6b8c10c6d69cffffefc6d68c6b8c107b9c086b9400739c006b940073940894ad52fffff7ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffff7ffffe76b8c107b9c087ba5006384006b8421ffffefffffff
fffffffffffffffffffffffffffffffffff7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
000000fffffffffffffffffffffffffffffffffff794ad527394086b9400739c006b9400739c08
6b8c10bdd68cffffe7c6ce9c638c00739c006b9400739410bdce8cffffe7ffffefffffef738c29
739408639400739c00638c00739421e7f7c6ffffeffffff7e7efce6b8429739408739c0073a500
638c00739418ffffeffffff7ffffefc6d69c738c106b94006b9400739410bdce94ffffefbdce8c
738c107394007394007394007394006b94009cb552ffffefffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffff7fffff7ffffeffffff7ffffef
fffff7fffff7fffffffffff7fffff7ffffeffffff7ffffeffffff7fffff7ffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7fffff7fffff7
fffff7e7efcee7efcedee7cee7efcef7ffe7fffff7ffffefffffffffffffffffffffffffffffff
fffff7fffff7f7ffdee7efd6dee7cedee7ceefefd6fffff7fffff7fffff7ffffffffffffffffff
ffffefffffde7b9418638c00739c00739c00738c21ffffeffffffffffffffffffffffff7ffffef
eff7cee7efc6e7efceffffeffffff7fffffffffff7fffff7ffffe7eff7d6ffffe7fffff7fffff7
fffffffffff7fffffffffff7fffff7fffff7fffff7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffefffffefeff7dee7efd6dee7c6e7efd6ffffefffffffffffef
fffff7ffffeffffff7ffffeffffff7fffffffffffffffffffffffffffff7fffff7ffffeffffff7
fffff7fffffffffffffffffffffffffffffffffff7ffffefffffeffffffffffff7ffffffffffff
fffffffffffffffffffffff7fffff7ffffeffffff7fffff7fffffffffffffffffffffff7fffff7
ffffeff7ffe7efefd6e7efd6dee7ceeff7deffffeffffffffffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffff
fffffffffffffffff79cb552739400739c00739c00739c00739c00739410bdd684ffffefbdce94
7394086b94006b94006b8c08adc663eff7c6fffff7fffff7c6d6a5849c396b9400638c0084a508
739410d6e7b5fffff7ffffffdeefc6738c216b8c00739c007b9c006384086b8431ffffeffffff7
ffffefbdce947b94296b8c00739c006b9400c6d694ffffe7c6d68c6b8c08739c086b9400739c00
6b9400739c0094ad52fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffb5bd9c9cad6394ad4a9cbd529cb5529cb55294ad5a9cad6b94ad639cb563
9cb5529cb54aa5bd5abdd684cee79cefffc6ffffeffffffffffffffffffffffffffffffff7fff7
fffffffffffffffffffffffffffffffffff7ffffe7bdce94a5b5636b8418849c296b8c106b8c10
738c087b94188cad42d6e7a5ffffefffffffffffffffffffe7efd6c6d69c8ca54a738c18738c18
738421738c31738c21739421c6de8cffffd6fffffffffffffffffffffff7ffffe7738c18739c00
739c00739c00738c18fffff7fffff7ffffffffffefc6d6946b8c107b9c107394087b9c18738c21
adbd84ffffdeffffe7b5c67b7b94216b8c087b9c216b84189cad6bdeefbdffffef94a5639cb563
9cb55a9cb55a8ca552cedeb5fffff7fffffffffffffffffffffffffffffffffffffffffff7ffde
bdd684738c187b9c21738c107394185a7b08849c42bdce8cffffdec6d68c94ad4a94b54a9cb55a
8ca552d6deb5fffff7ffffffffffef94ad5a9cb54a9cb54a94ad529cad6bcedeb5fffffffffff7
ffffffadbd949cb56394ad529cb55a8ca552c6cea5fffff7fffffffffffffffffffffff794a563
9cb55a9cb5529cb5529cad63d6deb5ffffffffffffffffe7cede9484a5316384087b94296b8421
738c216b8418738c29adc67befffceffffe7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff000000fffffffffffffffffffffffffffffffffff794ad4a
7394006b9400739c006b9400739c006b8c08c6d684ffffe7bdce946b8c08739c08739c006b9400
638400849c39bdce94f7ffdeffffe7ffffd6b5d66b739c08638400739418e7f7bdffffefffffef
eff7ce7b9429739c08638c006b8c08a5b56be7f7c6ffffe7ffffe7cede9c94ad52739410739408
6b9c006b9400bdce8cffffe7bdd6846b8c08739400739c006b9c00739c006b94009cb552ffffef
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7bdc69c
6b8c10739400739400739400638c00739418738c216b84216b8c187b94186b8c086384006b8c00
7394107b94218ca552ced6adffffe7fffff7fffffffffffffffffffffffffffffff7f7f7ffffff
fffff7d6deb594a5527b94186384007394005a84006b9408739408739400638c006b8408738c29
84945affffeffffffff7ffe7bdce9c738c296b84086b8c0884a5299cb55a8ca54a8cad31739408
5a84009cb552d6e7bdfffffffffffffffff7ffffde738c186b9400739c006b9400739418ffffef
ffffffffffefbdce9484a5296b9400739c00739c005a7b006b8c10738c319cad63bdce7b84a529
638c00739c00638c0073941894ad63d6deadf7ffd68c9c4a738c106384007ba5087b9c18bdce94
fffff7fffff7ffffffffffffffffffffffffffffffe7efcea5bd73739410739400739c006b8c00
6b8c007b9c187b9429738c2194ad4a8cad317b9c08739400739c00739418c6ce9cffffefffffef
ffffe7739418739c00739400739408739418bdce94fffff7ffffffffffefa5b57b6b8c10739408
739400738c18adbd84ffffeffffff7ffffffffffffffffef738c297394086b94007394006b8c18
ced6a5fffff7fffff7c6d6946b8c106b8c006b8c00738c1894ad529cb55a8ca542739418637b10
8ca54ae7f7bdffffefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff000000fffffffffffffffffffffffffffffffffff79cb5526b9400739c00739c00739c00
739c00739410bdd684ffffe7bdce947b9c186b94006b9400739c0873940863840073941894ad4a
f7ffc6ffffd6ffffdee7ffb59cb55a637b18b5c67bf7ffceffffefffffdeadc6736b841894ad4a
deefadffffd6ffffe7e7f7bdb5ce7b84a5186b9400739c00739c00739c006b8c00c6d694ffffe7
c6d68c6b8c087b9c086b9c00739c006b9400739c0094b54afffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffbdce9c7394106b9400739c006b8c00
739410a5bd5abdd68cbdce94bdce9cb5c68ca5b573849c39738c186384086b8c087b9418739421
9cb563d6e7adf7ffdefffffffffffffffffffffffffffffffffff7e7f7c68ca54a638400739c00
638c00739c08739418adbd6bd6efa5deefadd6efa5bdd68c94ad63637331d6debdfffff7fffff7
c6ce9c526b08849c39c6d68cefffc6ffffe7ffffe7efffbd9cb5527394086b8c0894ad63ffffef
ffffffffffffffffef6b8c18739408739c00739400738c10fffff7fffff7f7ffd6849c42638c00
7394007394006b8c00a5bd5abdd68cadbd7b8c9c527b9c296b8c006b9c00739c00739410c6d68c
f7ffdeffffefffffef8c9c527b9418739c00739c006b8c00bdce9cfffff7ffffffffffffffffff
ffffffffffffffffefb5c6846384105a8400739c007394087b9418c6de94e7f7bdcedea594a563
6b84187394107ba508638c006b94006b8408cedea5fffff7ffffffffffef7b9421638c006b9400
6b8c00738c18bdce8cfffff7fffff7fffff794a56b7394106b9400739c08738c10bdce94fffff7
fffffffffffffffffffffff77b94296b8c006b94006b8c00739418deefb5fffffffffff7e7f7bd
6384107b9c29adc67befffceffffefffffefffffefadbd736b84186384108ca552f7ffdefffff7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffff
fffffffffffffffffffffff794ad527394006b9400739c006b9400739c086b8c08bdd68cffffe7
c6ce946b8c086b94006b8c007394087394007394006b8c006b9400527300a5b563c6d69ca5b584
94a56bcedea5bdd68c849c42b5c684cede9c94a563b5c684ffffd6ffffe7ffffd6c6d69c7b9c31
739410638c006b94006b9c00739c006b9400739408bdce94ffffe7bdd684738c10739400739c00
739c00739c006b94009cb552fffff7ffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffff7ced6a56b8c08739c006b94006b94007b9410d6efa5ffffdefffff7
fffff7fffff7ffffeff7ffd6c6d6948ca5426b8c087394086b8c005a84007b9421c6de94ffffe7
fffffffff7ffffffffffffffffffe794ad5a6b8c086b940084ad00638c007b9c18d6e79cf7ffd6
ffffeffffff7ffffefffffefeff7d6ced6b5adb59cffffefffffefcedead637321c6d694f7ffd6
fffff7fffff7fffff7ffffe7cede8c7394086384007b9421cedea5fffff7ffffffffffe7738c21
6b9400739c006b9400739418ffffe7fffff7deefb57394216b8c00739c00638c007b9418f7ffce
ffffefffffe7d6e7ad739418638c007ba5006b9c00638408deefb5fffff7ffffffffffef94ad63
739410739c006b9c00739408c6d6a5fffffffffffffffffffffffffffffffffff7dee7bd7b9431
6b8c08739400739c086b8c10c6d694ffffe7fffff7ffffefe7f7ce8ca552738c10739c006b9400
6b9400739410d6e7b5fffffffffffffffff7849c39739408739400739c00738c10c6d694ffffef
fffff7ffffef9cad6b739408739c006b9400739410b5c68cfffff7fffffffffffffffffffffff7
738c216b9400739c00739c006b8c10efffc6fffff7ffffffe7f7c66b8431adbd7bf7ffd6fffff7
fffffffffffffffff7d6e7a5849c31638400739418adbd84fffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffff000000fffffffffffffffffffffffffffffffffff7
9cb552739400739c00739c00739c00739c00739410bdd68cffffefbdce946b9408739c007b9c08
6b94006b94006b94007ba5007ba500739c08739418738439cedeb5ffffe7ffffe7f7ffc68ca542
7b9421a5b56be7f7c6ffffe7ffffefbdce9c8c9c4a7394186b8c006b9400739c00739c00739c00
6b9c00739c00739400c6d694ffffe7c6d68c6b8c10739c086b9400739c006b9c00739c0094ad52
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
cedead7394086b9400739c00638c00739418deefadffffeffffffffffffffffffffffffffffff7
ffffefdeefb5a5bd636b8c007ba5086b94006b8c007b9c18c6d69cffffeffffffffffffffffff7
cedea57b9418739c0073a5006b94007b9c10bdd68cffffeffffffffffffff7f7ffffffffffffff
fffffffffff7ffffffffffffffffffdee7c6a5b58cf7ffe7ffffffffffffffffffffffffffffe7
bdd67b7b9c086b9400739410adbd73ffffffffffffffffef738c18739408739c00739c00739418
ffffefffffefcede9c738c107ba5086b9400739c0094ad4afffff7ffffffffffffffffe7849c39
638c007ba500638c006b8c18e7efc6fffffff7f7f7fffff79cad6b739410638c007ba500739408
dee7bdfffffffffffffffffffffffffffffffffff7bdce8c6b8c10739c007ba5086b94008ca539
ffffe7ffffffffffffffffffffffefbdd68c6b8c086b94006b94007ba500739410deefbdffffff
fffffffffff794ad527394086b94006b9400738c18bdce94ffffeffffff7fffff7adbd7b739408
6b94006b94006b8c08bdce94fffff7fffffffffffffffffffffff77b94216b94007ba5006b9400
638408deefb5fffff7ffffffeff7d68c946beff7d6fffff7ffffffffffffffffffffffefd6e7a5
739410739c08739408849c42ffffefffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff000000fffffffffffffffffffffffffffffffffff794ad527394006b9400739c00
6b9400739c006b8c10c6d68cffffe7bdce946b9400739c006b94007394006b9400739c006b9400
6b94006b94007394087b8c39ffffe7ffffefffffe7bdce8c739421637b08cede94ffffe7fffff7
ffffe7738442738c186b9400739c007ba500739c006b94006b9400739c00739c006b9400bdce94
ffffefbdce846b8c10739400739c006b9c00739c006b94009cb552fffff7ffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffff7dee7b56b8c00739c00739c00
6b9400738c18e7efbdfffff7fffffffffffffffffffffff7fffff7fffff7fffff7deefb58ca542
638400739c006b9400638c00849c31dee7bdffffffffffffffffef9cad63739408739c00739c00
739c0094ad42ffffdeffffeffffffffffffffffffffffffffffffffffffffffff7fffff7ffffff
fffff7fffff7e7efd6fffff7fffff7fffff7ffffffffffe7c6de9c7b9c21739400739c005a8400
9cad5afffff7ffffffffffe7738c186b9400739c006b9400739418ffffeffffff7c6d69c6b8c10
6b94006b9400739c009cb552fffff7fffffff7fff7ffffef8ca54a7394086b9c006b9400739421
f7ffdeffffffffffffffffef9cad6b739408739c00739c00739408dee7bdffffffffffffffffff
ffffffffffffffffe7a5b56b6b8c08739c006b94007394089cb563ffffefffffffffffffffffff
fffff7deefb57394186b9400739c006b9400739410dee7bdfffffffffffffffff78cad4a739400
6b9400739c086b8c10cede9cffffefffffffffffefbdce947394087b9c006b9400739408bdd694
fffff7fffffffffffffffffffffff77394186b9400739c007b9c086b8410e7f7bdfffff7ffffff
fffff7d6dec6ffffefffffffffffffffffffffffefeff7c694ad42638c0084ad00638c006b8c18
ffffe7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffff
fffffffffffffffffffffffffffff79cb5526b9400739c00739c00739c00739c00739410bdd68c
ffffefbdce94739c086b9c00739c006b9400739c006b9400739c00739c00739c00638c00738c21
ffffdeffffefffffefbdce8c6b84217b9421b5c67bffffeffffff7ffffef738c31638c00739c00
739c00739c007394006b8c00739408739c00739c006b8c00c6d694ffffefc6d68c6b8c087b9c08
6b9c00739c006b9400739c0094ad52ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffdeefbd6b8c086b9400739c006b9400738c21e7f7c6ffffff
ffffffffffffffffffffffffffffffffffffffffffffffefd6e7ad739418739c007ba5007ba500
6b9408a5b56bffffeffffff7ffffef738c296b9400638c00739c00739c08b5ce7bffffe7fffff7
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7
ffffefffffe7ced6a58ca5527b9c106b8c00638c00739c0084a51894a552ffffffffffffffffe7
6b8c10739c00739c00739c00738c18fffff7fffff7e7f7c67b9431739408739c00739c0084a521
fffff7ffffffffffffffffef9cb563739c08739c00739400738c21e7efc6ffffffffffffffffef
94ad637394086b9c00739c006b8c08e7efc6fffffffffffffffffffffffffffff7ffffe7849442
7b9c08739c006b9c00739408b5ce84ffffeffffffffffffffffffffffff7ffffde7394187b9c08
739c00739c00738c10e7efc6fffff7fffffffffff79cb5526b9400739c00739c087b9421dee7b5
ffffffffffffffffefbdce94739408739c00739c006b8c00c6d694ffffefffffffffffffffffff
ffffe7849c21638c006b9400739400738c21e7efc6fffffffffffffffffffffff7fffffffffff7
ffffe7cedea5a5b5636b84086b940052840084ad007b9c006b8410ffffe7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffff
fffff794ad527394006b9400739c006b9400739c086b8c08bdd68cffffe7c6ce946b8c08739c00
739c00739c00739c007ba500739c006b9400638c007ba508637b08ffffdeffffe7ffffe7cedea5
6b84296b8421bdce8cffffeffffff7ffffe7738c296b9400739c006394006b9c00739400739c08
6b94006b94006b9400739408bdce94ffffe7bdd684739408739400739c00739c00739c006b9400
9cb552fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffff7e7efc6739408739c006b94007394086b8418e7efc6ffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffef7b94317394086b940084ad005a7b007b9421e7f7c6ffffef
e7efbd6b8c18739400739c006b9400739408d6efa5ffffe7ffffe7fffff7fffff7fffffffffff7
fffff7fffff7fffffffffffffffffffffffffffffffffff7ffffe7b5c68c7b94397b94186b9400
638c00739c007ba5006b94006b8c10adbd7bfffff7ffffffffffde7394186b940073a5006b9400
739418fffff7ffffffffffe794a5637394107b9c08638c00739410e7efc6fffff7ffffefffffe7
8ca5427394086b9400739c006b8410deefbdfffff7ffffffffffe794ad5a6b8c00739c006b9400
739410e7efc6ffffffffffffffffffffffffffffffffffe76b84216b9400739c006b9400739408
c6d694fffff7fffffffffffffffffffffff7ffffde6b8c186b8c00739c006b9400738c10deefbd
fffff7fffff7ffffef94b54a739400638c006b9400738c18deefbdfffff7ffffffffffefbdce94
638c00739c006b9c00739408bdce94fffff7ffffffffffffffffffffffef94ad427b9c086b8c00
7394086b8c18deefbdfffffffffffffffffffffffff7ffe7bdce94849c397b9c186b9400739c00
73a50073a500739c006b8c0094ad5affffefffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff000000fffffffffffffffffffffffffffffffffff79cb55a7394007ba508
6b94007ba500739c00739408c6de8cffffdec6ce9c738c29739408739c00739c007ba500739c00
739c00739c006b9c006b940073941894ad52e7f7bdffffdeefffc69cad638c9c5acedeadffffef
ffffefe7f7bd84a5396b9400739c0073a50073a5007ba5006b9c006b94006b94007394106b8c18
c6d69cffffdecee78c6b8c08739c00739c006b9400739c00739c0094ad52ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe7efc6739410739c00
739c00739400739418deefbdffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffefc6d68c6b8c086b9400739c00739400739418adbd84ffffe7efffc66b84106b94006b9c00
739c00739c08cee794deefade7f7bddeefb5e7f7bddeefbde7f7c6dee7bdeff7cee7efc6dee7c6
f7ffe7fffffffffff7ced6ad8494426b8c087b9c007ba5006b940084b5006b94006b94006b8c08
7b9431dee7bdfffffffffff7ffffe7739418739c00739c00739c009cb552ffffffffffffffffff
f7ffdeadbd7b738c187394186b8c107b9439c6d69cffffe7ffffd6a5bd4a638c00739c00739c00
738c18e7f7bdfffff7fffff7ffffef94ad52739408739c007394006b8c08efffceffffffffffff
fffffffffffffffff7ffffef738c296b9400739c006b94007b9c10b5c68cfffff7ffffffffffff
ffffffffffefffffe7849c427b9c106b9400739c006b8c10e7f7bdfffff7ffffffffffefa5bd5a
6b9400739c00739400738c21e7f7c6fffff7ffffffffffffbdce947394106b94007ba508739408
bdce94f7ffe7ffffffffffffffffffffffef9cb5526b8c00739c006b8c00739421deefbdffffff
ffffffffffffdeefc68c9c5a7394216b8c006b94006b9400739c00739c006b94006384007b9c31
deefbdfffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000
ffffffffffffffffffffffffffffffffffff94ad527394006b94006b94006b9c007394006b8c00
bdd673ffffd6ffffe7bdce8c94a54a6384006b8c00739c00739c006b9400739c006b9400739c00
6b8c00738c107b943194ad5a9cb563a5bd6bf7ffceffffe7ffffdecedea58ca54a6b8c08638c00
6b9400739c006b9400639400739c006b8c006b8c088c9c4abdce8cffffdeffffdeb5ce6b739408
6b9400739c006394006b94006b94009cb552fffff7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffe7efce6b8c10739c006b9400739c006b8c18e7f7bd
fffff7fffffffffffffffffffffffffffffffffffffffffffffffffffff7deefad7394106b9400
7ba5006b9400738c1094a563ffffe7d6e7ad738c18739c00739c0073a500638c006b8c18738429
7384317384316b84217b94316b8421738c216b84216b842184944adeefbdffffe7dee7b58c9c4a
6b8c086b94006b9c006b9c007ba5006b9400638c006b8c008ca542bdce9cfffff7fffff7fffff7
ffffd6738c186b9400739c006b94009cb54afffff7ffffffffffffffffffffffe7deefb5a5b573
8c9c52738c31738c317b9439adbd6b8ca521739c00638c006b94006b8410e7f7bdffffeffffff7
ffffe79cb5636b94006b9c006b94006b8c08cedea5fffff7fffffffffffffffff7fffff7ffffe7
6b84216b8c006b94006394007b9c10bdce94fffff7fffffffffffffffffffffff7ffffe7849c42
6b8c00739400739400739410deefbdfffff7fffff7ffffef9cb5526b94006b9400739c006b8c10
e7f7c6fffff7fffffffffff7bdce946b8c086b94006b9400739410bdce94fffff7ffffffffffff
fffffffffff78ca54a739c08739c006b94006b8c10e7f7c6fffff7fffffff7ffde94a563638410
638c007ba5006b9c00739c007b9c006b8c00738c10849c42c6d6a5ffffefffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffff
fffffffffff79cb55a6b9400739400739c00739c006b940073940084a521c6de8cf7ffceffffe7
efffc6bdce847b94186b8c007394007ba5086b9400739c00739c007ba50073940073941094ad39
def794ffffceffffdedef7b59cad5a6384086b8c006b94007ba5006b94007ba508739c08739408
638408849c29b5c67befffceffffe7ffffd6c6de8c8ca5296b9400739c00739c00739c006b9400
739c0894ad52ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffe7efce739418739c00739c00739400739418e7f7b5ffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff7ffffde7394187394007ba500739c006b84087b9442
ffffdeefffc6738c21739c086b9c00739c0073940094ad4a9cad6b9cad7b94a57394ad63adbd6b
6384087394086b94007394086b8c18deefadffffe7adbd73738c18638c0084ad00739c006b9400
6b8c006b8c10849c39cede94f7ffd6fffff7fffff7fffffffffff7ffffde6b8c18739408739c00
739c0094ad4afffff7fffffffffffffffffffffffffffff7ffffefffffefdeefc6b5c694849c52
7b94316b8c00739c007ba500739c00739421e7efbdfffff7fffff7ffffef9cb5637394086b9400
739c006b8c00b5c684ffffeffffffffffff7fffff7ffffefffffef6b8421739c086b9400739c00
739408c6d69cfffff7ffffffffffffffffffffffffffffef7b94396b8c007394007b9c00739408
e7f7bdfffff7ffffffffffef9cb55a6b8c006b9c006b9400739418e7f7bdfffff7fffffffffff7
bdce947394107394007394006b8c10bdce94fffff7fffffffffffffffffffffff77b9c31739400
739c006b94006b8c08e7f7bdffffffffffffced6a56b84186b94086b940084ad08638c007b9c18
638410738429adbd84f7ffdefffff7ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff000000fffffffffffffffffffffffffffffffffff794ad52739408
7394007b9c006b9c00739c006b94006384006b8c109cad5adeefadffffe7ffffdee7ffb59cb55a
7394186b8c086b94086b94006b9c006b9c00739c006b9400739c006b8c006b8c08738c18738c18
6b8c087b9c08739c00739c006b9c007b9c006b8c007394107394219cad63deefadffffe7ffffe7
deefbd9cad5a739418638400739c006b9400739c00739c00739c006b94009cad52fffff7ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe7efce6b8c10
739c006b9400739c086b8c08e7f7b5fffff7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffe7738c216b8c00739c007394006b8c086b8429ffffe7ffffde7b94316b8c00
739c005a8c007ba508deefa5ffffe7fffff7fffff7ffffdeffffd67394087ba5006b9400739c00
6b8c10efffbdffffd6849c396b8c086b94006b9c006b9400738c10a5bd6bd6e7ade7f7c6f7ffd6
ffffeffffffffffffffffffffffff7ffffde738c216b8c007394006b94009cb552ffffefffffff
fffffffffffffffff7fffffffffffffffffffffff7fffff7f7ffd6cede9c7394086b9400739c00
6b9400738c21e7f7c6fffff7fffff7ffffe79cad5a6b8c00739c006b94006b94008ca54af7ffd6
ffffeffffff7ffffefffffefffffe78ca54a7394007ba500739c006b9400adbd7bfffff7ffffff
ffffffffffffffffffffffde738c29638c00739c00739c00739408deefbdfffff7fffffffffff7
94ad527394086b9400739c006b8c10deefadffffeffffff7fffff7bdce9c6b8c08739c006b9400
739410a5bd7bfffff7fffff7ffffffffffffffffef6b84186b9400639400739c006b8c00e7f7b5
fffff7fffff78ca5527b94186b94006b94005a8400739418849c4acedeadf7ffdefffff7fffff7
fffffff7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
000000fffffffffffffffffffffffffffffffffff79cb55a739400739c006b9400739c00739c00
7ba5006b94006b8c006b8c107b94219cb563e7f7b5ffffdeffffe7cede9c94ad52738c18739408
6b9400739c006b9c006b9c006b9400739c087394086b8c086b94007b9c006b94006b9c006b9c00
739c006b94006b8c108ca54acedea5ffffe7ffffefe7f7c6adc67b7b94297394186b8c08739c08
739c00739c006b9400739c006b940073940894ad52ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffe7efce739418739c00739c00739400739410
def7b5fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffef6b8421
6b94006b9c00739c006b9408738c21ffffe7ffffe794a5636b8c087ba5006b9400638c00deefa5
ffffeffffffffffffffffff7ffffde6b8c08739c0073a5007b9c006b8c18ffffd6ffffe7738c29
7b9c107394006b940094b539e7f7c6f7ffe7fffff7fffff7ffffeff7ffe7ffffffffffffffffff
fffff7ffffef7b94297394086b9400739c009cb552fffff7fffffffffffffffffffffff7fffff7
fffffffffffffffffffffff7fffff7ffffd68ca5295a84006b9c006b9400738c21e7efc6ffffff
fffffffffff794a55a739408739c0073a500638c00739418bdd68cefffc6ffffdeffffdedee7bd
ffffefadc67b6b8c08739c007ba5006b94008ca54affffe7fffffffffffffffffffffff7e7efb5
6b8410739c00739c00739c006b8c08e7f7c6fffff7fffffffffff79cb552739c00739c00739c00
739410cee79cfffff7fffff7fffff7bdc6947394086b9400739c00739408a5b56bffffe7fffff7
fffff7fffffff7ffd6739410739c00739c00739c00739410e7f7bdfffffffffff794ad5a7b9c08
6b94006b8c0094ad39cedea5ffffe7fffff7fffffffffff7fffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffff
fffffffffffffffff794ad527394006b8c00739c006b9c006b9c006b94007ba500739c00739408
6b8c006b8c08739429b5c684f7ffceffffe7f7ffcecede9c849c427394186b8c00739c00739c00
6b9c006b94007b9c007ba5086b94006b9400739c006b9c00739c006b8c08739421bdd68cefffc6
ffffe7ffffdebdce948494396384086b8c086b94007b9c08739c00739c006b9400739c006b9400
6b94006b94009cb552ffffefffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff7e7efc66b8c10739c006b9400739c006b8c10e7f7b5fffff7ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffef738c296b94006b94006b9c00739408
6b8418ffffe7ffffefced6a5738c186b8c00739c006b8c00b5c67bffffeffffffffffffffffff7
f7ffce6b8c00739c006b9c007394008ca552ffffefffffe79cad63739408638c007b9c08f7ffc6
f7ffe7fffffff7f7effffff7e7f7ce9cad7bdee7ceffffffffffffffffffffffde7b94216b9400
6b9c006b940094b54afffff7fffffffffffffffff7b5c69cd6debdffffe7ffffffffffffffffff
fffff7e7f7bd739408639400739c00739c006b8421e7efceffffffffffffffffef9cad6b6b8c08
739c007ba500739c007b9c106b8c106b8c187b94316b7b31637331ffffe7d6e7ad6b84106b8c00
639400739c006b8c10efffbdffffefffffffffffefffffe7b5ce7b6b8c08739c00739c006b9400
6b8c10dee7bdfffffffffffffffff78ca542739c00739c00739c006b8c08c6d694ffffeffffff7
ffffefadbd846b8c00739c00739c006b8c007b9429d6e7adffffe7ffffeff7ffd6cede946b8c00
739c00739c006b94006b8c10eff7c6ffffffffffffbdd68c6384007394007b9c10deefa5ffffef
fffff7fffffffffff7ffffefb5bd9cc6ceadfffff7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff000000fffffffffffffffffffffffffffffffffff79cb552
6b9408739c08739c007ba5006b9c006b9c00739c00739c006b94007ba5007ba500739408638408
8ca542cee79cffffdeffffe7f7ffdebdce947b94395a7b007394007ba500739c00638c006b9400
6384007ba500739c00638c00739418b5c684eff7ceffffefffffded6efa594ad427394086b9400
7ba500739c00739c006b9400739c006b9400739c007b9c00739c0073940073940894ad52fffff7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe7efc6
739410739c00739c00739400739418e7f7b5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffe7738c29739c086b9c00739c006b9400738c21ffffe7fffff7f7ffde
adc6735a8400739c00739c009cb55af7ffd6fffffffffffffffff7c6d694739410739c006b9400
6b8c10d6debdfffff7ffffefc6d69c7394185a8400739410ffffd6fffff7ffffffffffffffffe7
c6de947b9431c6cea5fffff7fffffffffffff7ffd66b8c107ba500739c00739c0084a529fffff7
ffffffffffffffffde849c528ca552deefadffffe7ffffeffffff7f7ffdeb5ce7b7394007ba500
73a5006b940094ad52f7ffdefffffffffffffffff79cad6b739408739c006b94006b9400b5d65a
7394086b94086b8c107394219cad6bffffefffffdea5bd636b8c006b9400739c006b9400adc663
ffffdeffffefffffe7deefad9cb54273940073a5006b9400739c006b8c10e7efceffffffffffff
ffffef8cad316b9400739c00739c00739410bdce8cfffff7fffff7fffff79cad6b739408739c00
7ba5006b94007394108ca54acede9cdeefadbdd67384a518739c006b9400739c006b8c00849c39
f7ffd6ffffffffffffe7f7bd6b8408739408739418e7efbdfffff7fffffffffffffffff7d6dead
7b944aa5b57bfffff7ffffefffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff000000fffffffffffffffffffffffffffffffffff794ad52739408739c00739c00739c00
739c00739c00739c006394007ba5006b9c006b94006b94007b9c086b8c006b8c109cb563deefb5
ffffe7ffffefdeefbda5b5636b8c08739400739c007394087394086b8c006b8c006b940894ad42
def7adffffdeffffefdeefb5a5bd6b6b8c106b94006b9c00739c006b9c0073a500739c006b9400
739c00739c006b9400739c006b9400739c006b94009cad52ffffefffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffff7e7f7c66b8c10739c006b9400739c00
6b8c08e7f7b5fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffefffd6
738c216b9400739c006b9c006b94006b8421ffffeffffff7ffffffeff7c6849c31638400739408
637b00b5ce84ffffe7ffffeff7ffce94ad5a73941073941073941094a552ffffeffffffffffff7
f7ffd6849c397b9c18637b08c6d68cffffefffffefffffefbdce946b84186b841894a56bfffff7
ffffffffffffeff7c66b8410739c0073a5006b8c007b9429fffff7fffffffffff7f7ffde7b8c42
637b187b9431adbd73cedea5cedea594ad52738c10638c00739c00528400739c08adc67bfffff7
ffffffffffffffffef9cad6b6b8c106b9400739400739400cee77b7b9c10739408739408638410
9cad63ffffe7ffffefd6e7a5849c296b94007b9c00638c00738c109cb55abdce84b5c67b8ca542
5a7b00739400739c00739c00739c00738c18dee7c6ffffffffffffffffef849c296b94006b8c00
7394086b8c18bdce94ffffeffffff7ffffef94a5636b8c086b9400638c007ba50884a5296b8418
738c29738c187394085a8c00739c006b94006b8c007b9c21a5b573fffff7ffffffffffffffffde
adbd6b638408638410b5c68cf7ffdeffffefffffefbdce948c9c4a5a73108ca55af7ffdeffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffff
ffffffffffffffffffffffef9cb55a739408739c006b9c00739c00739c00739c006b9400739c00
739c00739c006b9c00739c00739c00739c006b9400739408738c10b5c673e7f7bdffffe7ffffde
cede9c8ca54a738c216b8418738c216b8421849c42cede9cffffe7ffffdeefffbdb5c673739410
6b9400739c00739c0073a500739c0073a500739c00739c006b9c00739c00739400739c006b9c00
739c00739c0073940894ad52fffff7ffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe7efc67b9421638c00739c006b94006b8c08e7f7b5fffff7ffffff
fffffffffffffffffffffffffffffffffffffffffffffff7e7f7bd738c106b9400739c00739c00
6b8c08a5b563ffffefffffffffffffffffffe7efc6a5bd6b738c186b8c106b8c0894ad3994b542
849c216384086b8418738c29adbd7bf7ffd6fffffffffffffffffffffff7deefbd94a5526b8c18
738c1894a54a94ad5a7b9c316b8418738c29637b2194a56beff7deffffffffffffdeefbd738c21
6b8c086b8c00738c18738439fffffffffffffffffffffffff7ffdeadbd7b7b9429638408739410
7394087b9c107394086b8c006384008ca531adbd73ffffefffffeffffffffffff7ffffe7849c52
7394187394107394106b8c18e7f7b5efffbd8ca542638408738c1894a552ffffeffffff7ffffe7
cedea594ad426384006b8c087b9c187394087394086b84107b9421bdd67363840084a5006b9c00
739c00739418e7efcefffffffffffffffff7738c316b8c10739410738c18738439b5bd94ffffff
fffff7fffff794a56b6b8c186b8c086b8c087b9418def7a5b5ce847b94396384107394006b9400
7b9c086b8c08738c219cb573e7efceffffffffffffffffffffffffe7efcea5b56b6b8421738c21
7b9c318ca5398ca5397b9421637b106b84218c9c63dee7bdfffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffff000000fffffffffffffffffffffffffffffffffff7
94ad527394086b9c00739c006b9400739c006b9400739c006b9400739c006b9c00739c006b9c00
739c006b9c00739c006b94007394006b8c008ca539b5ce84efffc6ffffdeffffdebdce8c7b8c42
738c42bdce94f7ffd6ffffe7f7ffcebdd68c849c316384006b9400739c006b9c00739c006b9c00
739c006b9400739c006b9c00739c006b94007394006b9400739c006b9c00739c006b94009cad5a
ffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7
e7f7c6738c186b9400739c00739c006b8c08efffbdfffff7ffffffffffffffffffffffffffffff
fffffffffffffffff7ffffefb5ce847b9c10638c00739c006b9400738c18b5c68cffffefffffff
fffffffffffffffff7efffd6cedea5a5bd63849c296384007394186b84107b9439adbd84d6e7b5
eff7d6fffff7ffffffffffffffffffffffffffffeff7ffd6c6d6949cb5636b8418738c18637b10
7b9439adbd7bced6ade7efcefffff7ffffffffffffeff7d6b5c684bdd684d6e794bdd68ca5b584
fffff7fffffffffffffffffffffff7f7ffded6e7adb5ce7b7394186b8c086384006b8c107b9421
a5b563cedea5ffffe7fffff7fffffffffff7fffff7ffffe7b5c694b5c684bdd684b5c67badbd84
efffd6ffffefd6e7adb5c67b738c31637b29fffff7fffffffffffffffff7deefb5b5c673849c42
738c216384007394188ca54acede9cf7ffc67394186b94006b9c006b9400739418dee7bdffffff
ffffffffffffadbd84bdd684bdd684bdce8cbdc69cdedec6fffff7fffffffffff7c6ceadc6d69c
c6de94b5ce84bdce8cf7ffd6f7ffd6cedeadb5c684849c297394106b8c188ca552bdce94f7ffde
fffff7fffffffffffffffffffffffffffffff7ffe7d6e7ad94ad636384187394187394186b8c18
9cad63c6d6a5f7ffdefffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff000000ffffffffffffffffffffffffffffffffffef9cb55a6b9408739c00739c00
739c006b9400739c00739400739c006b9c00739c00739c00739c006b9c00739c00739c00739c00
6b9400739c086b8c0073941094ad4ad6efa5ffffd6ffffe7f7ffd6f7ffd6ffffe7ffffd6d6e7a5
9cb5526b8c106b8c08739400739c00739c00739c006b9c00739c00739c00739c006b9c00739c00
739c00739c006b9400739c00739c00739c006b9c0073940894ad5afffff7ffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffff7e7efbd738c186b8c007b9c00
6b94006b8c10deefb5ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe7
8ca5397394006394007ba5006b8c087b8c31e7efcefffffffffffffff7ffffffffffffffffffff
fffff7fffff7ffffdeffffefffffefffffefffffe7fffff7fffff7ffffffffffffffffffffffff
fffffffffffffffffffffff7fffff7f7ffdeffffefffffefffffefffffe7fffff7ffffffffffff
fffffffffffffffffffffff7ffffe7ffffe7f7ffd6ffffe7ffffefffffffffffffffffffffffff
ffffffffffffffffffffffefffffeff7ffd6f7ffd6f7ffd6ffffdeffffe7fffff7ffffffffffff
fffffffffffffffffffffffff7f7e7fffff7ffffefffffefffffeffffffffffffffffff7ffffef
ffffeffffff7fffffffffffffffffffffffffffff7ffffefffffefffffe7ffffefffffe7ffffef
ffffefffffe7738c186b94007ba5006b9400738c10e7efceffffffffffffffffffffffe7ffffe7
ffffe7ffffe7fffff7ffffffffffffffffffffffffffffffffffefffffeffffff7f7f7e7ffffff
fffffffffffff7ffeffffff7ffffefffffefffffeffffff7fffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffefffffefffffefffffefffffefffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffff
ffffffffffffffffffffffffffffef94ad527394086b9400739c006b9400739c006b9400739c00
6b9c00739c006b9c00739c006b9400739c006b9400739c006b9400739c006b9c00739c006b9400
6b8c006b8c10a5bd63deefadffffe7ffffdedef7b59cb5637394186384006b9400739c007ba500
6b9400739c006b9400739c006b9400739c006b9c00739c006b9c00739c006b9400739c006b9400
739c006b9c00739c006b8c009cad5affffefffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffefdeefb56b8c10739c00739c00739c006b8c10deefb5fffff7
fffffffffffffffffffffffffffffffffffffffffff7ffded6e7a56b8c00739c00739c00739c00
6384089cb56bffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffff7fff7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffff7ffeffffff7fffffffffffffffff7fffffffffffffffffffffffffffffffffff7
eff7cebdce8c9cad63b5c68ceff7d6fffff7ffffffffffffffffffffffffffffffffffffffffff
fffffffffff7ffffeffffffffffff7fffffffffff7ffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffff7fffffffffffffffffffffffffffffffffff7ffffffffffffffffff
fffffffffffffffff7fffff7fffff7fffffffffff7fffffffffff7ffffffffffe76b84186b8c00
7ba500739c00739418e7efceffffffffffffffffffffffefffffd6e7f7bdeff7c6ffffe7ffffff
fffffffffffffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffff
ffffef9cb55a739408739c006b9c00739c00739c00739c006b9c0073a500739c0073a500739c00
739c00739c00739c006b9c00739c00739c006b94006b9c0073a500739c006b94006b8c088ca531
9cb55aa5bd5a849c296b9408638c00739c00739c00739c00639400739c006b9c00739c00739c00
739c006b9c0073a500739c0073a500739c00739c00739c00739c006b9c00739c00739c00739408
94ad5afffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffff7cedea5738c107394007ba5006b9400739410d6e7a5fffff7ffffffffffffffffffffffff
fffffffffff7ffffe7cee79c7b94187394007ba500739c005a84008ca539deefb5ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffff7ffd68c9c526b84105a7b006b8c18
84944affffe7fffff7fffffffffffffffffffff7ffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffef738c297394006b9400739c006b8c10deefc6
fffff7ffffffffffffe7f7c694ad5a6b8c187b9429adbd7beff7d6ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffff000000ffffffffffffffffffffffffffffffffffef94ad5a7394086b9400
739c006b9400739c006b9400739c006b9c00739c006b9c00739c006b9400739c006b9400739c00
6b9400739c00739c006b9c00639400739c006b94006b94006b8c007394106b8c086b94086b9400
739c006b9c006b9c006b940073a5006b9c00739c006b9400739c006b9400739c006b9c00739c00
6b9c00739c006b9400739c006b9400739c006b9400739c006b8c089cad5affffefffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe7c6d6946b8c106b9400
6b9400639400739408c6de8cffffdefffff7fffff7fffff7ffffefffffefefffc6c6de8c84a531
5a8400739c006b94005a8400739410cede9cffffeffffff7fffffffffffffffffffffff7ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffffffffffffffffff
fff7ffffffffffffffffffffcedead6b84187394087ba5086b8c08738c18ced6a5fffff7ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffef738c3173940073a500638c00739410deefbdfffff7ffffffffffefa5bd73
5a7b00638c006b8c08738c29c6d6a5ffffefffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000
ffffffffffffffffffffffffffffffffffef9cad636b8c10739c08739400739c006b9400739c00
739c00739c006b9c00739c00739c00739c006b9400739c00739c00739c006b9400739c00739c00
73a500739c007b9c006b9400739c007ba508739c00739c007ba5087ba5007ba500739c00739c00
6b9c00739c00739c00739c006b9400739c00739c00739c006b9c00739c00739c00739c006b9400
739c00739400739c006b940073941894ad63ffffefffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffefbdce947394187394007ba500739c007b9c08adc663
deefb5deefbde7f7c6d6e7adc6d694a5bd6384a531638c006b8c007b9c08739c086b8c008ca539
c6d69cffffefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffe7
c6de946b8c087ba500739c006b94006b8c08bdce8cffffefffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffef738c29
7394086b9400739c007b9c10eff7c6ffffefffffffffffe78ca5426384007ba5086b94006b8c10
a5b56bffffe7fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffff
fffffffffff794a563738c216b8c087394086b8c087394086b8c087394086b94007394006b9400
7394086b94007394086b8c007394086b94007394086b8c006b8c006b8c007394086b94006b9400
6b8c006b94086b8c006b94007394006b8c00638c007394086b8c007394086b8c007394086b9400
7394086b8c007394086b94007394086b94007394006b8c007394086b8c087394086b8c08739410
6b84189cad6bffffefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffefc6d69c6b8418739410739c00739c005a84006b8c107b94397384396b84297b9439
738c216b84106b8c087394086b94007394086b84108ca54acedeadfffff7ffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffff7fffffffffffffffffffffff7ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffff7d6e7ad738c18638c00739c00
739400738c18d6e7adfffff7fffffffffffffffffffffffffffff7fffff7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffe7738431739410739c08739c006b8c10
ced6a5f7ffe7fffff7ffffef7b94397b9c187394006b9400738c18a5b573ffffefffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffeff7deadbd8c738431
738c216b8c18738c21738421738c216b8421738c21738c18738c216b8429738c29738c21738c21
6b8c18738c21738c21738c216b8421738c29738421738c216b8421738c21738c21738c216b8c18
738c21738421738c216b8421738c29738c21738c216b8c18738c21738c21738c216b8421738c29
738421738c216b8c18738c21738421738c216b8421738c21738421738c31adb584f7ffe7ffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffe7b5c694738439
6b8421739418738c10738c186b8421738c396b84396b84317384317b8c317384216b8c187b9429
9cb55aadc67bf7ffcef7ffdeffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffff7ffdeb5c684738c215a7b08849442adbd84fffff7ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffff7ffe77b8c4a738c217394107b94186b8418c6cea5fffff7ffffffffffef
dee7b5738c316b8c105a7b0894ad63e7efc6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
000000ffffffffffffffffffffffffffffffffffffffffefffffe7ffffe7ffffe7ffffe7ffffef
ffffe7ffffefffffe7ffffe7ffffe7ffffefffffe7ffffefffffe7ffffe7ffffe7ffffe7ffffe7
ffffe7ffffe7ffffefffffe7ffffefffffe7ffffefffffe7ffffefffffe7ffffefffffe7ffffef
ffffe7ffffe7ffffe7ffffe7ffffe7ffffe7ffffe7ffffefffffe7ffffefffffe7ffffe7ffffe7
ffffefffffe7ffffe7ffffe7ffffe7ffffdeffffeffffff7ffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffff7fffffffffff7f7ffe7ffffefffffe7ffffdeffffdeffffde
ffffe7ffffe7ffffefffffe7ffffe7ffffe7ffffe7ffffdeffffe7ffffe7ffffeff7ffe7fffff7
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffff7f7ffded6deb5d6deb5dee7c6ffffefffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
dee7c6deefb5e7f7b5efffbddeefb5eff7d6ffffffffffffffffffffffefefffcec6d69cc6d69c
dee7bdfffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffffffffff
fffffffffffffffffffffffffffff7fffff7fffff7fffffffffff7fffffffffffffffffffffff7
fffffffffff7fffffffffff7fffffffffff7fffff7fffff7fffff7fffff7fffff7fffff7ffffff
fffff7fffffffffffffffffffffffffffffffffff7fffff7fffff7fffff7fffff7fffff7fffff7
fffff7fffff7fffffffffff7fffffffffff7fffffffffff7fffffffffff7fffffffffff7fffff7
fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff7fffff7ffffeffffff7fffff7fffffffffff7ffffff
fffff7fffffffffffffffffffffffffffffffffff7ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffffffffeffffff7
fffff7fffffffffffffffffffffffffffffffffff7ffffefffffefffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7fffff7fffff7ffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000ffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffff7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffff000000ffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffff000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7ffffffff
fffffffffffffffffffffffffffffffffff7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff000000040000
002701ffff030000000000
}</xsl:text>
</xsl:template>
<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<!-- =================================================================================================================================== -->
<xsl:template name="logo-fr">
<xsl:text>
{\pict\wmetafile8\picwgoal2970\pichgoal1141 
010009000003d44600000000af46000000000400000003010800050000000b0200000000050000
000c0276002d01030000001e000400000007010400af460000410b2000cc0075002c0100000000
75002c0100000000280000002c0100007500000001000800000000000000000000000000000000
00000000000000000000000000ffffff00ffffdb00ffdbdb00aadbb600ffdbff00aaff92005592
24000092000055920000aadbdb0000490000aab66d00aab69200aaffff00aab62400aa924900ff
dbb600aab64900556d0000aa922400aadb6d00ffffb600aadb9200aa926d00006d000055b60000
55924900aa920000aadb4900aab60000aaffdb00aaffb600aadbff00aab6b600ffdb9200aadb24
00aa92920055b69200ffdb6d0055b66d00ffb6920055db6d0055b6240055db920000242400556d
6d00554949005524240000494900aa6d6d00556d490055926d0055244900559292000000240000
492400aa92b600aa6d920000240000aab6db0055492400556d92000024490055496d00ffb6b600
ffb6db000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010102010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101033625010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010122002f010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101050101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101050101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010101010101010101010101010103002f
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101020101010101030101010101010101010301010103010101010101010101
010101010101010101010101010101010101010101010101010101010101010301010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101050101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010103002e010101010101010101010101
010101010101010101010101010101010101010101010e01010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010103010101010101010101010101010101013c2f302f2501010101032f3d2f2201022525
0301010101032e22010101222522010102253d2f25010101253d2f320101222f302e0301010301
0301012e302f222522010103363035340101010101010a183601012525220125250a0103253e01
012e302e012e25012536220101342e010101222f3d2f0a0101222f3d2f22010525250301033e25
010125360301010a2e302f2501012525030103362e0101032f302e02010102012e30250a322501
0122302f0a2525010101010103002e222f2e0301010101252f3d2e0301014125220122180a0101
252e030101322f2e030101050101252d2f222525010101252f2d2e0a01010101030101222e2201
010301222f2d2f220103252e01012225250101222f30250103252d302e03010103010101010101
01010101010101010a00320a25003801010d002d25362d25012f00220101010125002d0101012e
372501052d003a223022012d002e25333101302e0a2f000201010101012d00252e2d002f010500
002e252f2f010101010122003001013500220130000301010000012e002f25000025032f002201
010000010136002d2236300125002d323a2d250135000a010a003001012f00220122002d253630
250131000a01010000010140002e220a0101012e002f2e0000250125002d2e2d00000101010101
0100002f252d002201012f00220330000a01250025012e002201012d0005013600402203010101
012f002e252d002d01012f002e252e000101030101010133002e010101320030252e2d03010000
010122002f0101000025250130002f252f2f01010101010101010101010101010101010a2e0101
012500220100000501010103012f0003010101012f0000030101360025012f0025010102032e00
2501010105010301012500220101010125002e010118002e012e00250101010301010101010300
2f01012e000a012f000301010000012f0003012f0003012e000301010000010300000101010301
00000301010103012f00030103002e01012e0022012d000301010103012f000a01010030010131
000301010101012f000101310001012f00220122002d010101010103002d01010a00300125002e
010122002f01250036012e002201012d0003012e002201010201010a002d010122002f0122002f
010101030101010101010100002d03010a00300101010301010000010141002f0103002d010125
002e0101010a0101010101010101010101010101010e0101010101012500250a002d0103020502
012f003c01010103003d252f010125000d0100000301010101300003010501030101253d00000a
010101012f00220101220025012d00030103010101010101010a002f010125000a012e00030101
00000122002f012e0001012e000a010100000125002f010101010300300101010101012f000301
01002e01012f000a03002d0301030101013200030101000001012f0001010101010125002e012f
0001012f000101030030010101010101002f0101010000012f0022010101000001250036012500
22010138000201320002010101010139002e0101012d35012e0025010101010101010105012200
2e2f250122002e0103030103012d30010104002e0101002d01012f002201030103010101010101
0101010101010101010103010501032500002503000030302d002e012f00110101011800250200
030122002501000001010101013d002d302d303b032e0000002e01010101012d00220101220036
012d002d302d2d30030101010103002f01012e0022012f0003010100000101032e30000003012e
000a010100000125002f010101010300002d302d0025012f00030101002e01012f002203000030
2d30002f012e00040101000001012f00020101010101010a2e30000001012f000101013b370101
01010103002f0101010000012f00220101010000012500360125002201012f0003012e00030101
03010125002e0101012d30013e002d2d302d002501010101012f0022222d012500002d2d00002e
010000010122002f0101000001012f002d2d2d0000010103010101010101010101010101010101
012e2d00000005010037010122002e012f00340101012d000a012d25013200250130000a010101
012f002201033100012d0022050101010101012e00250101220025012f000a01032d2d03010501
0101002f01012e0042012f000a0101000001050201012f0003012e002201010000010a00300101
010301002d010122002e012f000a0103002d01012e000a03003001010a002f012e002201010000
01012f00030101010101030101012f0001012f000301012d00010101010103003d010101000001
2e002501010a002d01250025012500250101350003012e0003010101010122002d010103003d01
25003201012e00220101010103003003012d0a0a002f010125002501000001013c002f0101002d
01012e002201012d000101020101010101010101010101010103012f00002d320101012e000401
320022012f0000003222003d01012500013200220122002f03012f030a002f0104002f01332d01
222e01010301010a00000a3c2d00360122002e0122002e010101010122002f01012f0022012f00
2d0a2e002f01032d0303000001012e002d2236002d01012f002501222f012f000a012e00220130
002d222f002d3222000003012e002201250022012f002d042e002d01033000220a010101010300
0a03000001012f00040105000001010101010a000025052e002e0101002d01012f002501250025
012e00002225002d05012f0025030101010101300022032e003001032d0001012d000101010101
2e002e0101312f0131000a012f000a0100002f0a2d002e010a00000a010a002f010a002f010301
0101010101010101010101010101032d00250101010101032e2d3037250101402f25372e2e2d3a
01010a000a252d360101222d00002d0a01222d302d2f0301033100002f0101010103012231002f
2e00250101252d302d2f03010101010330002d2f032e2d25012f2f360000300a01012e00002d25
01014030252d0030220101032e0000002e01012e002d002501012f2f2500002f032f0000250101
012e2d30002e01012f303600002f0a012e3b002f2f01010101014000002d2201012f2d2201032d
2d0101010101222d2f2d000033010101222f2d302d250101252d25012e2d2e30002d22013e3b00
3f2d0102010201032e003b2e2d3001010a2f2d302d2201010101012d2d2201012500030a2f2d2d
2d22010a2d2f2e00002f030a2f00002f3601222d2d002f0a010501210101010101010101010101
0101032d2f01010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101220025010101010101010101
010101010100250101010101010101010101010101010101010101010101010101010101010101
010101010101010101050101010101010101010101010101010101010101010101010101010101
01010101012f000a01010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101012e00220101010101010101010101
2d3001010101010101010103010101010101010101010101010101010101010101010101010101
01002d01010101010101010101010101010101010101010101010101010e2f000101032f010101
010101010101010101010101010101010101010125352201010101010101010101010101010101
01010101010101010101010301010122002e01010101010101010101010101012d22010125380a
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010101010101010101012e000401010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
222d220101010101010101010125002501010101010101010101012d3501010101010101010101
010101010101010101010101010101010101010101010101010101012f00010101010101010101
0101010101010101010101010101010101010130002d003d050101010101010101010101010101
01010101010101012f002e01010101010101010101010101010101010101010101010101010101
02010122002f0101010101010101010101010101220030252d0025010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010a0301010101010101010101010101010101
0101010101010101010101010101010101010101010101010101012e002e010101010101010201
0101030a0101010101010101010103002d01010101010101010101010101010101010101010101
010101010101010101010101010101010122030101010101010101010101010101010101010101
01010101010501010a220a0101010101010101010101010101010101010101010101010a220301
01010101010101010101010101010101010101010101010101010101030122002e010101010101
01010101010101010125002e032205010103010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010105010105010101010101010101010101010101010101010101010101010101
01010101010101010101010101010103220a010101010101010101010101010101010101010101
01010a302d02010101010101010101010101010101010101010101010101010101010101010101
010101010201030101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101030e0101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101030101010101010101
010101010101010101010101010101010101010101010101010501010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101020101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101012101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101030425
040501010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101052f00002d30002f030101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101012d00303c01012200000a01010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101010101010101010101010101010a002d
030105010125003b05010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010103002d010101010122000022010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010201010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010103030301
010101010101010101010101010101010101010101010101010101010101010101020101010101
01010101010101010101010101330022010101012e00003a010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101030101010205020102020201
020102010202020103010301020202010301030203020502020102010202030103010301030205
020301010101010101010101010e010103222e2f2e3c0101010101010101222536250e01010101
0104030a030101010122183e030104020a03010101012536250a01010401220101010101012532
2e2201010101010a0a03220101010a03033c0101013e3837003b2f030101010101010101010101
0a2534250301010103030322010101030a030a010e0101010101010122010a0301014103032201
0101250032252e2f00000025010a05220101010503050a05010101010325323622010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010102071a0909091a09091a091a09090909090909090909
1a09091c091a090909090909091a091a091a091c091c09091a0909090701010101010101010101
010101220000002d2d000022010101012200003b2f00002f010101012d00002e0101012d000000
0036300000250101053f00000000010a00000001010101330000302d0000250101012e00002d01
01012e00002d010122000032222f00000a01010101010101010a2d000000000030010122000000
0a0101220000000a01010101010101012d00002201012e00002d01010337000000000000003b01
040000000a01010a0000000a0101032f0000000000000301010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101030709091a09090909090909091c090909091a091a0909091c091c1a091c091a0909
090909091c09091a1c091a090909091c100101020e01010101010101010122002d22051f250037
2201013c3b002d03012500002f0101023f00003601012e0037002f2e000037000d01010a000000
010301050000000101012e00002e01013000000d0101180000380101012e00002d010103002501
01012f002f01010101010101012d0000303c03222d25013c00002d030101030000300501010101
010101013100000d01012e00003001012200000000302f2d2e030103000000030101223b003b03
01013000002d220303303401010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101010101010101010101051b0909091a09
1a1c0909091a091a091c0909090909091a090909090909091c091c091e091a091a091a091a1c1a
1c1a1c0914010101010101010101010103010100030101010140000001012d00002e0101010000
002501013d00002501013f00002f0101220000002201010d000030010101010000000101220000
000301012200000001013600002d0101011800002d0101012e0101010130003b03010101010101
2e000030010101010101012200002d0301010400002d0301010101010101013000000d01012e00
003801010300002501010101010101040000000301010a00002d0301250000000a010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101071a091c091c091c091e090909091c1a1c09
1c091c090909090909091a09091c091c091c0909091c0909091c091a1c1a092b01010105010101
010101010101012201010101012e00000301000000220101012f00002f01013500002501013000
00250101032d000039010122000030010101030000000101250000370101010a37000039012500
003501010132000030010101010103363700002d030101010101013000002e0101010101010122
0000370301012200002d0301010101010101013000003901012e00002d01010122002e22220101
010101220000000a01010300002d03012f00002f01010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010102071a0909091c091c09090909090909091e09091a09090909231509090909
0909091c0909090909090909090909091c09091401010101010101010101010101010101010101
033b00002241000000030101012e00000001013100003601022d00003601010330000025010125
0000300101010300000001012f00002d010101032d00002e011800003001010132000030010301
012e00000000002f010101010101030000002201010101010101220000000301010a0000300301
010101010101013000002501012e00002d0101032e00003d00002f030101220000002201012200
002d03032d00002e01010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101010101010101010101010101031b0909
09091a091a0909091e091a09090909090909091501010102180909091a091a091a091a091a0909
1a0909090909091401010101010101010101010101010101010141300000002222000000030101
01250000000a013d00002e01032d0000340101013000000d01012500002d010101010000000101
2f00002d010101023000002e013e00003d0101012e0000380101012f0000000000002201010101
01010a3b0000002d002d302d30012200003b0301010a00002d0101010101010101013000000d01
012e00003001012e00002e012200003001010a0000000301010a37002d030a2d00002d003b372d
2d37030102010e0101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010102071a1c0909090909091e0909091c09
091a091e091203010123170101231b09091a0909090909091c091c090909090909092401010101
010101010101010101010101222f00000000000103000000220101012500000001013f00003401
01300000360101013500002501012500002d0101010300000001013200002d010101032d000032
011800002f0101012e00003001013c0000000000400a01010101010101032d00002f25252f0000
00012200002d0301012200002d0301010101010101012f00003901012e00003001013700002201
032d00002201030000000a0101220000370a012d00002f25252e00000003010301030101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010507091e090909090909090909091c0909090907040101112b090914
0301012907090909091c091c091c091c091e090909091b0101030101010101010101010101012f
000000000000250101000000250101012f00003001013f00002501012d00002501010131000039
010125000030010101030000000101220000000101010a2d000025012500002d0301012e000035
01012200002d2501010101010101010101033500002501012500002d01220000000a0101250000
300301010101010101013100003901012e00003001020000002201032d000025010a0000002201
012500003703012f00002e01010300000001010501010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010105
07091c091c090909091c091c1a1c091a09150201030f090909091c092301010215091c091c0909
091a09091a0909091a091401010101010101010101010101012f000000000000220101012e0000
2f0101013b00002501012f00002501032d0000360101013000002501010d00002d010101030000
000101032d0000220101220000000101250000002e01030000002f0101220000220101013d0301
0101010101011800002f01012e00002e01220000002f01013d0000300101010101010101012d00
002501013200002d01010000002201032d00002201220000002d01012d00003703012200002d01
012500002d01010301010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010102070909091e091c091e091e09
0909091501010115091a0909091c0909091a1701010310090909091a091a090909091e091e1401
01010e01010101010301010103000000002d2e01010101010331000025013600002e2101013d00
003e01012d00003e01010130000025012e2f00002d2e2e0103000000010101220000300a033500
002201012e0000370000000000002201010100002501012e002501010101010101012d00002203
2d002d0301220000370000000000002e0101010101010101013000000d01012e00002d01013e00
002f012200002d0301220000002d00000000002e0101012f000025012d00002201010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101050709091a1c091c090909091c090904010104070909090909
09090909091a1c1b03010123091c0909090909091c091c09091401010101010101010101010101
22000000330301010101010101032e000000000036010101012f00003a0103003b00320101022d
00002501000000000000000103002d00010101010a3000000000300a0101012e0000300a000000
002e01010201220000382d00002e0101010101010101032e00000000300a01012200002d0a3000
00002f010101010101010101012d00000d01012f0000000101011800002d00000000002222003b
003c2f0000003802010101012e0000000000220101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101031b1a090909091a090909090723010103141a091c091c091c1a091a1c091c091c09180201
011509090909091a1c090909090701010301010101010101010501250000330101010101010101
010101010a0303010101010105380000180101010101010101010101010101012200002d010101
0101010101010101010101030a010101010101010101010101030a01010101010101032e2f2f2e
03010101010101010102010101030a0101010101010101010101030a0101010103010101010101
012d0000390101010101010101010101010a030a030a0301010101010101010a01010101010101
01010a030201010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010101010101010507091c091a091a0909
0915030101150909091c09090909090909091c09090909090909230101030f091a09091a09091a
0910010101010101010101010101010a0000250101050122010101010101010101010101010101
01300000180101010101010101010101010101012500002d010101010101010101010101010101
010101010101010101010102010101010101010101010101010101010101010101010101010101
050101010101010103010101010101010101010101010101010101012d00002501010101010101
010101010101010101010101010101010101010101030101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010102071c091e091c0909150a010117091a090909091a09
0907071b1b0909091a09090909091a0911010111071c0909091a1c091001010101010101010101
010101012d00350101012200220101010101010101010101010101012f00003601010101010101
01010101010201012237002d01010101362f250101010101010101020502010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101012f00000d01010a2e2f0a01010101010101010101010101
0101010102010301030e0103010a01050101012101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101021b091c0909091a170101042b091a1c091c09091a09090923010104090909091a09
09091c1a092b01010115091a091a091a100101010101010101010101010101040000302e330000
2e0101010101010101010101010301032d00002e0101010101010101010101010105010103342d
010101220000000401010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101013000002501012f00002d01010101010101010101010101010101010102050101050101
030101030101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101010101010101010101051b090909091a
090301181c091c0909091c1a0909091b151b0f0301010c1b1c1a091e090909090909150104091a
1c09091a07010101010101010101010101010101032e2d00002f2e0a0101010101010101010101
010101033100003201010101010101010101010101010101010101010101250000002201010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101013000002501013000
000001010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010207091a091a0907020a091a090909091c0909
1a091a15010115090401011709090909090909091c090901041c091c0909091001010101010101
010101010101010101010203030301010101010101010101010101010101032d00002e01010101
010101010101010101010101010101010101022f002f0101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101013000002501012200002501010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101032b091a0909090903210909091a091c0909091c1b23180904011823010104
09090909091c1a1c09091a01041c0909090909140101030e010101010101010101010101010101
010101010101010101010101010101010101012222220a01010101010101010101010101010101
02010101010101010a0d1704010101010105010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101220a220301010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101071a09
091a1c0903050909091c0909090909091c03010a07100723010123080909090909090909090701
04090909090909140101010a010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010103090909
071503010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101031b1c091c1a1c090202091a091c091a
091504040914040101170917010103170d0909090909091a1c09010a091a091e091c0701010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010407091a07040101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101070909091c09090301091a1c0909091c0715010915141503011817
0a09150101011509091c09091c090111091c091c09091401010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101020104091a092b0201010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010101010101010101010101010101010e
070909090909090305091c090909090d0315070901021509170f09090415072301010407091c09
09090111091c0909091a1001010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101090909092301010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010101010507091c090909090203091c1a
09070101011509092b110104090904010103071c15010102071c091c090111091c1a1c091c2401
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010101010101010107091c091401010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010103071a091e09091a031f09091a0918010101091c1a09092409
1505010115090909150101051b1a091a090104091a090909090701010301010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010f09091a0701010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101011b0909090909090305090909090f0101011c09090909090a010104091c09090915010102
071a091a1c0104091a091a1c1a1401010101010101010101010101010101010101010101010101
050101010101010101010101010101010101010101010101010101010101050101010101010101
01010101010118091a090901010101010101010101010101020101010101010101010101010101
010101010101010101010101010101010101010101010201010101020101010101010101010101
010101010101010101020102010101010101010101010101010501010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101010101010101021b090909091c090301
0909090924010101070909091c0903010123091e0909091501010207091c091c0104091c091e09
1e140101010e010101010101012101020101010102030303030101010101010101010101010101
01010101010104231515170a010101010101010102170c150c1701010101010101150909090901
010101010215170d20010101022317040101010501010101030101010101010101010111151515
170501010201010102010201020101010101010101020101010103010105010501010101030101
010102230c150c1502010101010105010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010709091a1c0909030109090909091d200101120909
09090301012909090909091701011f2b091a09090104090909091a091401010101010101010101
010103090909090909090909080909070c17030101010101010101010101011509090909090908
091804010101171c0909090909091a15010101010117091a0909010101010709090909091d0103
091c09090807010c091a090910010101010101010123071c0909090809150413091a1314010301
090909090915010105070909090904010101051809090908140101030709090909090909070401
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010507091c091a09090303091c091e091e07230101031d090904010104090909140301
01171b09090909090111091a090909090701010103010101010101010102090909090909090909
090909091a09091516010103010301050103070909090909090707090909010101140909070403
04240909180101010117091a1c0902010115081a0909090909241b090909091d0d012309091a09
15010101010101011109090909090707090915091c09090c0105010709091a092301010107091a
091e040101010117091a09091501010a1a09091704030d09091a04010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101010101010101010101021b1e091a091a
090201090909091a091a092b0401041a1b091701021b2b230101042b091a091a09090901041a09
1a091a090f010101010101010101010301031b1a091a0915010101012217070909090909170101
010101010207090909091b040101010123091701010c091501010101011b09090d0101010d0909
1c0901010a09090909090401011b09090909070101010d09090909150101010101010107090909
092301010107091a091a0915010101240909090904010101070909090911010101012309090909
0c010102090703010101010d090907010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101032b091a091c091a0301091a09090909091a09
0907071701030709070c01010218091c1a1c091e091a09010409091c0909091401010101010101
010102010103071a1c091a15010101010101030c0909091c0915010101010115091c1a09070301
010101010103030101280901010101010114091c070101010d090909090101041c091c09150101
0104091e09091501010117090909091501010101010104091a091c10010101010a0909091a0915
01010115090909090401010107091c09090a010e01010d090909090c010105090d010101010104
090909230101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010102071a1c09091a09030a09091c091e091e091a1c1a03010117090917010120
1c1a091c091c091c09090901041c1a091c090910010101010101010101010e01030709091a0915
01010101010201010d09091e091c2301010102091a091e09040101010101010501010201040401
0101010103090909090a03012c09091c07010104090909091501010103091a0909150101010309
0909091701010101010124091a090917010101050114090909091701010112091e090904010101
070909092b050102010117091a1c091501010115050101010101150909090c0101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101010101010101010101010101031b091c
09090909031f09091c09091c0909090909030102150909170101290909090909090909091c0901
040909091a1c1a1b010103010101010101010101011b090909090c0101010101010101010c0909
091a0901010104090909091c01010101010101010101010101020101010217141a090909040101
1709091c090101040909090915010201030909091c15010101041c1a1c09170101010101010709
1c091a04010101010115091c09091701010115091a1c0904010101140909090903010101012309
1c1a1c170101010101010101112b1c091c09150101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010307091e091c1a1c0202091c1a1c0909
090909091c0301011d1c09230101040909090909091c091c0909011109091c0909091401010101
0101010101050201012409091c0915010101010101010105030909090909150101150909090914
01010101010101010101030101010103150909091a0909090a010115091c090701020107090909
0701010103090909091501010104090909091701010101010a070909090903010101010123091c
090917010501280909090904010101150909090703010101010409090909150101010102011714
0909090909091d0101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010709091a091a09030309091a091a091a1c091c1a15010103090804
010117091a1c1a1c091a1c0909090123091e09090909140101010e010101010105010101140909
1a0915010101010101010101011509091c0907010115091a1c0915010101010101010101010101
010215091a090909090909140101010c09091a09010101200709090915010102091c09090c0101
010409090909290101010101031b091a090903010101020104091a0909170101010c0909090902
01010115091a091a030101010104090909091701010101170709090909091a09090a0101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010105
1b0909090909070201151909090909090909090909140415142301010307090909090909090909
1a270104090909091a09140101010301010101010301010e24090909091d010101010101010101
010d090907090903010c09090909071d1412141d141d2424181f01030709090909090909090903
0101010f09091a07010105011f150909090f040309091c090f0101010a09090909230101010101
01070909090903010102010103091e0909150101012a0909091c0a01010115091a090703010101
01041c0909091501010123091a1c09090909090917010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010103071a1c091c091c1501010a14
09090909091a09090909091501010215091a1c09090909090909150101020c0909091c09090701
0105010101010101010101012409091c0915010101010101010101010409091a090923010c0909
0909090709071b070909090909030124091c070909090909150101010101150909090902010105
0101010a0c070909091a091a2401010104091c091a150101010101030709091c09200101010101
04091c0909170101011d0909091a0401010115091c09090301010101041c1a0909170101030909
1e0909090909140a01010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101050709091c09091c0907110101041c0909091c09091c090924
141007091c09091c09091c09070201012307091c09091c09091001010101010101010101010101
0f09091c0915010101010101010101010309091c090915010c090909090c020303030107090909
090301070909090909071703010101010101180909091b01010101030101010103040709090909
2401010104090909090701010301010307090909090401010101012909091c09170101010c0909
1a09040101010f090909090a030101010d090909091701010d0909090909091504010101030101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101021b0909091a0909091a09140301012309090909091a091c091c1a091a1c09090909090711
0101202b091a091a091a091a1c0f0101020101010101010e0101010f091a090915010101010101
0101010101091c091c091501030909091a15010101010114091a09070501070909091004010101
01010101010115091a1c09010101010101010101010102091c09091401010111091a0909091701
01010101150909091a23010101010115090909090d0101010c091a0909040105010709091a1c04
010101010c09091c09170101150909090917010101010101010201010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101010101010101010101010207091a1c1a1c091e09
1e091c150101030c1c09090909091a09090909090909091c15020102151c091c1a1c091c1a1c09
0924010101010101010101010101010f091a1c0915010101010101010101010209091e09091501
010709090907010201020107090909140101070909070201010201030401010101150909090701
0101010403010101020104090909091501010104091a09090709140c1407010d09090909150101
0101010709090909150101010c090909090401010107090909092a010101010709090909170101
0d0909090401010101011703010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010507091a1c09091a1c091c1a09090717010103140909
09091a09091c09090909180201011d0909091c091c091c091c0909090701010105010101010101
0101011d09091c091501010101010101010101031a090909091501011709090909020101010209
090909170101280909150101010101240701010101140909090901020e01072401010101011409
090909290101011709091c0923091a091e070102090909090903010101040909091e0917010101
15091c09090401010107091c09090923030217091e090909040101110809090101010101040904
010501010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101070909090909090909090909090909240401011707090909090909090907110101
2307090909090909090909090909090914010101010101010101010101010f090909091d010101
010101020102011f09090909090c02050110090909120101011209090907010101200909070301
01030c09090301010107090909070105010309090704010a0f0909090909110103010d09090909
0d140909090f01010d090909090704030409090909090917010101240909090904010201070909
090907090907090909090907010501010c09090d0105010a07090c010103010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010103071c0909091c
09090909091c091c091c09150301011509091c0909092301010324090909090909090909090909
09091c09100101020e01010101010101010114090909090c010101010101010101010d09091a09
091701010103260909090f170c090909140a010101010409091b150c0709091904010101070909
0909010101011f1808090909090909090904010105010d0909090804030909091b010121170909
09090909090715090909091701010107090909091701010107090908071f070809090909090704
01010101011b08090f271209090907010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101011b0909090909090909090909090909090909
092301010314090915030102150909090909090909090909090909090909091401010101010101
0101010101011d0909090915010101010101010101011509090909090301030103010418090909
0909070d0101030101010111180909090909120403010101180c150f1802010102010517070909
090907182001010301010d18120c240401041409070101030104140909090907031f090909090c
010201151815150f230101010c240c0f2501030c070909091820050205010101030d0709090909
070d03010103010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101011f1b1a091a091a091a091a091a091a091a091a091a07040101040401011707
1a091e091a091e091a091e091a091e091a091a0701010501010101010105010101071a091e0915
01010101010101010102071e091c091401010a0101010101010a0a0a0301010101010301010101
0103200a0a0501010101010501010101010103010103010101030a04030e0101010e0101010101
010101012101010a22010101010101030a030a010103091a09091701010101010101010101010a
0101010101030101030a0a0301010101022102050101010a0a0a05010101010e01010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101070909
090909090909090909090909090909090909090702010104070909090909090909090909090909
090909090909091401010101010101010101010101070909090915010101010101010101170909
090909040101010101010101010101010101010101010101010101010101010101010101010101
01170c170301010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010a0909090917010101010103030101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0101010101010101010101010101010101010101010101031b091c091c091c091c091c09091c09
091c09091c09091c09151d091c09091c0909091c0909091c0909091c0909091c09091401010101
01010101010e010101071c090909170101010101010101041e0909090907010101050201010101
0103010101010e03010101010101030101010101010101030101011509091a070101010101010e
01010101030101010101010e0301010e0301010103010101010101030105010e010101011f0909
1c0915010101011509091501010101010e02010101010101010101010e01050101010101010101
010103010101030101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010303070909090909090909090909090909090909090909090909090909
090909090909090909090909090909090909090909091401030105010101010103010103070909
09090d010101010101030d0909090909070a010101010101010101010101010101010101010101
01010101010101010101010101010319090909080d010101010101010101010101010101010101
0101010101010101010101010101010101010101010101010a091a091a15010101120809090917
010101020101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010103
070909090909090909090909090909090909090909090909090909090909090909090909090909
09090909090909090909140101020e01010101010101010307090909091504160304170c070909
090909070a01010101010101010101010101010101010101010101010101010101010101010101
010102090909090915010101010101010101010101010101010101010101010101010101010101
01010101010101010101010117090909090c010101070909090918010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010103070909090909090909090909
090909090909090909090909090909090909090909090909090909090909090909090909090f01
01020501010101010205010a090909090909090909090909090909090912010103010101010101
010101010101010101010101010101010101010101010101010301010103090909091304010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
0d090909090f0101010c090909090c010105020101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010103070809090909090909090909090909090909090909090909
09090909090909090909090909090909090909090909090909070101050101010101010103010a
0b070707070909090809090909070c0d0a01010301010101010101010101010101010101010101
0101010101010101010101010101010e03010d0909090f0a010101010101010101010101010101
0101010101010101010101010101010101010101010101010101030d08090908100105010a0909
090711010103010501010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010102010101010101010101030101010101010101010101
010101010101030101010101010101010101010101010101010101010101010101010101010101
010101010301010104030101010101010101010101010101010101010101010101010101010101
010101010101010101010101010502030303050303010501010106040501010301050101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
010101010101010101010101010101010101010101010101010101010101010101010101010101
01010101010101010101010101010101010101040000002701ffff030000000000
} \par </xsl:text>
</xsl:template>

<!-- ************************************************************************ -->
<!-- rtf file start and rtf file end                                          -->
<!-- ************************************************************************ -->
<xsl:template name="RTFFileEnd">  
<xsl:text>}}</xsl:text>
</xsl:template>  

<xsl:template name="RTFFileStart">  
 <!-- #DG670 -->
<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\stylesheet {\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}}
{\info {\author Zivko Radulovic}{\operator BASIS100 automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company Basis100 Inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \margl720\margr720\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
</xsl:text>

<xsl:choose>
	<xsl:when test="//LanguageFrench">
		<xsl:call-template name="FrenchFooter"/>
	</xsl:when>
	
	<xsl:otherwise>
		<xsl:call-template name="EnglishFooter"/>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>
\pard\plain \s15\ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha
\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033

{\cs16 {\f1\fs22  }}
{\field {\*\fldinst {\cs16  }}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}}}
{\f1\fs16 {\f1\fs22 }}
{\field {\*\fldinst {\cs16 }}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}}}
{\cs16 \par }
\pard \s15\ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }}</xsl:text>

<xsl:choose>
	<xsl:when test="//LanguageFrench">
		<xsl:call-template name="FrenchHeader"/>
	</xsl:when>
	
	<xsl:otherwise>
		<xsl:call-template name="EnglishHeader"/>
	</xsl:otherwise>
</xsl:choose>

</xsl:template>  

</xsl:stylesheet>  
