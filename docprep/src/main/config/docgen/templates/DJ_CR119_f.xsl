<?xml version="1.0" encoding="UTF-8"?>
<!-- Author Zivko Radulovic -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xmlns="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- ================================================================== -->
	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="display-align">center</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">18pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">3pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:attribute name="border-before-style">solid</xsl:attribute><xsl:attribute name="border-before-width">1mm</xsl:attribute>
							<xsl:attribute name="border-after-style">solid</xsl:attribute><xsl:attribute name="border-after-width">1mm</xsl:attribute>
								DEMANDE DE CRÉDIT
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								OBJECT DU DOSSIER :
							</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Services financiers d'épargne, de crédit et services complémentaires
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ================================================================== -->
	<!-- ### -->
	<!-- ================================================================== -->	
	<xsl:template name="CreateActifPassifSection">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">2mm</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>					
		</xsl:element>	
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">8cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10.5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:call-template name="CreateActifSection"></xsl:call-template> 
						</xsl:element>
					</xsl:element>							
					<!-- end of cell -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>							
					<!-- end of cell -->					
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:element name="fo:block">
							 <xsl:call-template name="CreatePasifSection"></xsl:call-template> 
						</xsl:element>
					</xsl:element>		
					<!-- end of cell -->					
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ================================================================== -->	
	
	<!-- Catherine -->
	<xsl:template name="actifColumns">
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">25mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">30mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">25mm</xsl:attribute></xsl:element>
	</xsl:template>
	
	<xsl:template name="passifColumns">
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">20mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">28mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">19mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">22mm</xsl:attribute></xsl:element>
	</xsl:template>	
	<!-- end Catherine -->
	
	<xsl:template name="CreateActifSection">
		<xsl:element name="fo:table">	<xsl:attribute name="text-align">left</xsl:attribute><xsl:attribute name="table-layout">fixed</xsl:attribute>
				<xsl:call-template name="actifColumns"/> <!-- Catherine -->
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									ACTIF
							</xsl:element>
					</xsl:element>		
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Institution/détail
							</xsl:element>
					</xsl:element>		
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Montant
							</xsl:element>
				</xsl:element></xsl:element>
			</xsl:element></xsl:element>
		<xsl:call-template name="ActifDetail"/>
	</xsl:template>
	<!-- ================================================================== -->	
	<xsl:template name="CreatePasifSection">
		<xsl:element name="fo:table">	<xsl:attribute name="text-align">left</xsl:attribute><xsl:attribute name="table-layout">fixed</xsl:attribute>
				<xsl:call-template name="passifColumns"/>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									PASSIF
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Institution/détail
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->					
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Limite
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->					
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Mens.
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->					
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Soldes
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:call-template name="PassifDetail"/>
	</xsl:template>
	
	<!-- ===================    Catherine ============================ -->
	<xsl:attribute-set name="tableFont">
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>		
	</xsl:attribute-set>
	<xsl:attribute-set name="BottFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>		
	</xsl:attribute-set>		
	<xsl:attribute-set name="rowHeight">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="rowAttr" use-attribute-sets="rowHeight">
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="rowAttrNar">
		<xsl:attribute name="padding-top">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="padding-right">1mm</xsl:attribute>
		<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="rowEmpty" use-attribute-sets="rowAttrNar">
		<xsl:attribute name="padding-top">6mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="DollarAmntFormat">
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>    
			
	<xsl:template name="ActifDetail">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:call-template name="actifColumns"/>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="tableFont"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">Dépôts, épargne (avec # de compte)</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotalAssetCash"/></xsl:element>
					</xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row"><!-- ACT 4, 5-->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
					<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
						<xsl:element name="fo:block">Institution financière (nom et adresse)</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="padding-top">3mm</xsl:attribute>						
							<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute>
								<xsl:element name="fo:table-column"><xsl:attribute name="column-width">20mm</xsl:attribute></xsl:element>
								<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10mm</xsl:attribute></xsl:element>
								<xsl:element name="fo:table-column"><xsl:attribute name="column-width">50mm</xsl:attribute></xsl:element>
								<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Caisse</xsl:text></xsl:element></xsl:element>
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:text>	</xsl:text></xsl:element></xsl:element>
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Banque</xsl:text></xsl:element></xsl:element>
								</xsl:element>
							</xsl:element></xsl:element>							
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- ACT 7, 8, 9-->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">Biens immobiliers</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="padding-top">2mm</xsl:attribute>Biens mobiliers</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
							<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotAssetProperty"/></xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
							<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotAssetVehicle"/></xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="border-bottom">solid white 0px</xsl:attribute>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute>					
						<xsl:element name="fo:block">Autres actifs</xsl:element>
						<xsl:element name="fo:block">Placements</xsl:element>
						<xsl:element name="fo:block">Obligations</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="padding-top">5mm</xsl:attribute>
							RÉER</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="padding-bottom">11mm</xsl:attribute>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotAssetStocks"/></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
							<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotAssetRrsp"/></xsl:element></xsl:element>
				</xsl:element>

				<xsl:call-template name="ListedAssets"/>
				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>					
						<xsl:element name="fo:block">TOTAL</xsl:element><xsl:element name="fo:block">ACTIF</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:attribute name="number-columns-spanned">2</xsl:attribute>
					<xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotalAssetSide"/></xsl:element></xsl:element>
				</xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>	
	
	<xsl:template name="ListedAssets">
		<xsl:for-each select="//specialRequirementTags/ListedAsset">
			<xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell">
					<xsl:attribute name="padding-top">1mm</xsl:attribute>
					<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
					<xsl:attribute name="padding-left">1mm</xsl:attribute>
					<xsl:attribute name="border-left">solid black 0.5px</xsl:attribute>				
						<xsl:element name="fo:block"></xsl:element></xsl:element>						
				<xsl:element name="fo:table-cell">
					<xsl:attribute name="padding-top">1mm</xsl:attribute>
					<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
					<xsl:attribute name="padding-left">1mm</xsl:attribute>
					<xsl:attribute name="border">solid black 0.5px</xsl:attribute>				
						<xsl:element name="fo:block"><xsl:value-of select="ListedAssetTitle"/></xsl:element></xsl:element>
				<xsl:element name="fo:table-cell">
					<xsl:attribute name="padding-top">1mm</xsl:attribute>
					<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
					<xsl:attribute name="padding-left">1mm</xsl:attribute>
					<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
		 			<xsl:attribute name="text-align">right</xsl:attribute>
					<xsl:attribute name="padding-right">2mm</xsl:attribute>
					<xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="ListedAssetValue"/></xsl:element></xsl:element>				
			</xsl:element>								
		</xsl:for-each>	
	</xsl:template>
	
	<xsl:template name="PassifDetail">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:call-template name="passifColumns"/>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="tableFont"/>
				<xsl:element name="fo:table-row"><!-- PAS 4, 5 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">Cartes et</xsl:element><xsl:element name="fo:block">marges de </xsl:element><xsl:element name="fo:block">crédit</xsl:element>
						</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotLiabCreditCardMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totLiabCreditCard"/></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 6-9 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 11-14 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">5</xsl:attribute>
						<xsl:element name="fo:block">Prêts</xsl:element><xsl:element name="fo:block"> hypothécaires</xsl:element><xsl:element name="fo:block">et personnels</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotLiabMtgMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totLiabMtg"/></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 15 -18 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 19 -22 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 23 -26 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 27 -30 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:attribute name="number-columns-spanned">5</xsl:attribute>
						<xsl:element name="fo:block">Autres </xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row"><!-- PAS 32 -35 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>					
						<xsl:element name="fo:block">Location</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotLiabLeaseMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totLiabLease"/></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 36 -39 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>					
						<xsl:element name="fo:block">Pension</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotLiabChildMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totLiabChild"/></xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row"><!-- PAS 40 -43 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>					
						<xsl:element name="fo:block">Caution</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 51 -55 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>					
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotOtherLiabMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totOtherLiab"/></xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">TOTAL MENSUALITÉS 
							<xsl:element name="inline"><xsl:copy use-attribute-sets="DollarAmntFormat"/><xsl:text>                          </xsl:text>
								<xsl:value-of select="//specialRequirementTags/TotalMonthlyLiabilities"/></xsl:element>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute><xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">TOTAL PASSIF</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totalLiabilitySide"/></xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">ABD SOMMAIRE
						<xsl:element name="inline"><xsl:copy use-attribute-sets="DollarAmntFormat"/><xsl:text>                          </xsl:text>
							<xsl:value-of select="//Deal/combinedGDS"/></xsl:element>
					</xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="border-after-width">0px</xsl:attribute>
					<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">ATD SOMMAIRE 
						<xsl:element name="inline"><xsl:copy use-attribute-sets="DollarAmntFormat"/><xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/combinedTDS"/></xsl:element>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="border-after-width">0px</xsl:attribute>
					<xsl:attribute name="number-columns-spanned">2</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:element name="fo:block">VALEUR NETTE</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="border-after-width">0px</xsl:attribute>
						<xsl:copy use-attribute-sets="DollarAmntFormat"/>
							<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totalNetWorth"/></xsl:element></xsl:element>
				</xsl:element>								
		</xsl:element></xsl:element>		
	</xsl:template>
	
<!-- #########################################################################################################  -->		

	<xsl:template name="CreateBottomTable">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute><xsl:attribute name="padding-top">4mm</xsl:attribute>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">93mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">93mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-body">
			<xsl:element name="fo:table-row"><xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-cell">
				    <xsl:call-template name="QuestionsGuarantorOther"/>		
				 </xsl:element>   
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
				<xsl:element name="fo:table-cell">				    
				    <xsl:call-template name="QuestionsBankruptcyStatus"/>							 
			</xsl:element></xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>
	
	<xsl:template name="QuestionsGuarantorOther">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute><xsl:copy use-attribute-sets="tableFont"/>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">65mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">12mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">12mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="rowAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="padding-bottom">1mm</xsl:attribute>
						Êtes vous actuellement caution?
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
							<xsl:value-of select="concat(borrowerFirstName,' ', borrowerLastName)"/>
						</xsl:element></xsl:element>
						<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="space-before.optimum">1mm</xsl:attribute></xsl:element></xsl:element>
						<xsl:call-template name="IsGuarantorChkBox"><!-- <xsl:with-param name="YN_var" select="guarantorOtherLoans"/> --> </xsl:call-template>
					</xsl:element>
				</xsl:for-each>	
		</xsl:element></xsl:element>
	</xsl:template>			
	
	<xsl:template name="QuestionsBankruptcyStatus">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute><xsl:copy use-attribute-sets="tableFont"/>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">65mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">24mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="rowAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="padding-bottom">1mm</xsl:attribute>
						Statut de fiallite
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
							<xsl:value-of select="concat(borrowerFirstName,' ', borrowerLastName)"/>
						</xsl:element></xsl:element>
						<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element><xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
						<xsl:call-template name="BankruptcyStatusFr"/>
					</xsl:element>
				</xsl:for-each>	
		</xsl:element></xsl:element>
	</xsl:template>			

	<!-- this template returns two cells (columns) -->
	<xsl:template name="IsGuarantorChkBox">
		<xsl:choose>
			<xsl:when test="guarantorOtherLoans='Y'">
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="CheckedCheckbox"/>  Oui</xsl:element></xsl:element>	
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="UnCheckedCheckbox"/> Non</xsl:element></xsl:element>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="UnCheckedCheckbox"/>  Oui</xsl:element></xsl:element>	
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="CheckedCheckbox"/> Non</xsl:element></xsl:element>	
			</xsl:otherwise>
		</xsl:choose>				
	</xsl:template>
	
	<!-- this template returns 1 cells (column) -->
	<xsl:template name="BankruptcyStatusFr">
		<xsl:choose>
			<xsl:when test="bankruptcyStatusID='0'">
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Aucune faillite</xsl:element></xsl:element>	
			</xsl:when>
			<xsl:when test="bankruptcyStatusID='1'">
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Failli entièrement</xsl:element></xsl:element>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Réhabilité en partie</xsl:element></xsl:element>	
			</xsl:otherwise>
		</xsl:choose>				
	</xsl:template>
	
<!-- #########################################################################################################  -->		

	<xsl:template name="OriginRow">
		<xsl:element name="fo:table-row"><xsl:copy use-attribute-sets="tableFont"/>
			<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="number-columns-spanned">14</xsl:attribute>
				<xsl:element name="fo:block">Origine :</xsl:element></xsl:element>
			<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">Folio : </xsl:element></xsl:element>
			<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">Transit :	</xsl:element></xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ===================   end Catherine ============================ -->	
	
	<!-- ================================================================== -->
	<xsl:template name="CreateEmploymentHistorySection">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
			<xsl:attribute name="font-size">8pt</xsl:attribute><xsl:attribute name="space-before.optimum">2mm</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>	
				RENSEIGNEMENTS SUR L'EMPLOI
		</xsl:element>
		<!-- Line 1 -->
		<xsl:element name="fo:table"><xsl:attribute name="text-align">left</xsl:attribute><xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body">
				<!-- &&& Repeat for all sorted borrowers. There should be 4 of them. -->
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:if test="./borrowerTypeId=0">
						<xsl:for-each select="./EmploymentHistory">							
								<xsl:if test="./employmentHistoryStatusId = 0">
									<xsl:call-template name="EmploymentHistoryTableRowOne"></xsl:call-template>
								</xsl:if>
						</xsl:for-each>
					</xsl:if>					
					<xsl:if test="./borrowerTypeId=0">
						<xsl:for-each select="./EmploymentHistory">							
								<xsl:if test="./employmentHistoryStatusId = 1">
									<xsl:if test="./monthsAtCurrentEmployment &lt; 36">
										<xsl:call-template name="EmploymentHistoryTableRowTwo"></xsl:call-template>
									</xsl:if>
								</xsl:if>
								<xsl:call-template name="EmploymentHistoryTableRowThree"></xsl:call-template>	
						</xsl:for-each>
					</xsl:if>																						
				</xsl:for-each>
				<!-- <xsl:call-template name="BorrowerAddressTableRow"></xsl:call-template>	
				<xsl:call-template name="BorrowerTableRowMaritalStatus"></xsl:call-template>		
				<xsl:call-template name="BorrowerTableRowResidentialStatus"></xsl:call-template>	
				<xsl:call-template name="BorrowerTableRowResidentialStatusTwo"></xsl:call-template>
				<xsl:call-template name="PreviousAddressesList"></xsl:call-template> -->
			</xsl:element>
		</xsl:element>
		<!-- ====================== -->
	</xsl:template>	
	<!-- ================================================================== -->
	<xsl:template name="EmploymentHistoryTableRowOne">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Employeur actuel du demandeur
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./employerName"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						No. de téléphone
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./Contact/contactPhoneNumber"></xsl:value-of><xsl:if test="./Contact/contactPhoneNumberExtension"><xsl:text> x </xsl:text><xsl:value-of select="./Contact/contactPhoneNumberExtension"></xsl:value-of></xsl:if>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Poste occupé
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./jobTitle"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Depuis
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>					
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="../monthsAtCurrentEmployment"></xsl:value-of> 
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:choose>
					<xsl:when test="./employmentHistoryStatusId=0">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Complet</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Partiel</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Autre</xsl:text> 
						</xsl:element>																	
					</xsl:when>
					<xsl:when test="./employmentHistoryStatusId=1">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Complet</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Partiel</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Autre</xsl:text> 
						</xsl:element>					
					</xsl:when>					
					<xsl:otherwise>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Complet</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Partiel</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Autre</xsl:text> 
						</xsl:element>					
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<!-- end of cell -->	
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Revenu brut mensuel
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./Income/monthlyIncomeAmount"></xsl:value-of> 
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->								
		</xsl:element>	
	</xsl:template>
	<!-- ================================================================== -->
	<xsl:template name="EmploymentHistoryTableRowTwo">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Employeur precedent
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./employerName"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						No. de téléphone
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./Contact/contactPhoneNumber"></xsl:value-of><xsl:text> x </xsl:text><xsl:value-of select="./Contact/contactPhoneNumberExtension"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Poste occupé
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./jobTitle"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Depuis
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>					
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="../monthsAtCurrentEmployment"></xsl:value-of> 
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:choose>
					<xsl:when test="./employmentHistoryStatusId=0">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Complet</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Partiel</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Autre</xsl:text> 
						</xsl:element>																	
					</xsl:when>
					<xsl:when test="./employmentHistoryStatusId=1">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Complet</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Partiel</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Autre</xsl:text> 
						</xsl:element>					
					</xsl:when>					
					<xsl:otherwise>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Complet</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Partiel</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Autre</xsl:text> 
						</xsl:element>					
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<!-- end of cell -->	
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Revenu brut mensuel
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./Income/monthlyIncomeAmount"></xsl:value-of> 
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->								
		</xsl:element>	
	</xsl:template>
	<!-- ================================================================== -->
	<!-- ================================================================== -->
	<xsl:template name="EmploymentHistoryTableRowThree">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">19</xsl:attribute>
				
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
							Autre revenus (pension, etc.)
					</xsl:element>
									
					<xsl:for-each select="./Income">
						<xsl:choose>
							<xsl:when test="./incomeTypeId=0"></xsl:when>
							<xsl:when test="./incomeTypeId=1"></xsl:when>
							<xsl:when test="./incomeTypeId=2"></xsl:when>
							<xsl:otherwise>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:value-of select="./incomeDescription"></xsl:value-of><xsl:text>   </xsl:text><xsl:value-of select="./incomeAmount"></xsl:value-of>
								</xsl:element>							
							</xsl:otherwise>
						</xsl:choose>					
					</xsl:for-each>
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	<!-- ================================================================== -->		
	<xsl:template name="EmploymentHistoryTableRowTwoZZCommented">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">19</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
							Adresse	antérieure (si moins de 3 ans) :
				</xsl:element>
				<xsl:if test="//DealAdditional/primBorMonthsAtCurrentAddress &lt; 36">
					<xsl:for-each select="//DealAdditional/Borrower/BorrowerAddress">
						<xsl:if test="./borrowerAddressTypeId = 1">
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
								<xsl:attribute name="font-size">9pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="./Address/addressLine1"></xsl:value-of>
									<xsl:if test="./Address/addressLine2">
										<xsl:text>, </xsl:text><xsl:value-of select="./Address/addressLine2"></xsl:value-of>
									</xsl:if>
									<xsl:text>, </xsl:text><xsl:value-of select="./Address/city"></xsl:value-of>
									<xsl:text>, </xsl:text><xsl:value-of select="./Address/province"></xsl:value-of>
									<xsl:text>, </xsl:text><xsl:value-of select="./Address/postalFSA"></xsl:value-of>
									<xsl:text>  </xsl:text><xsl:value-of select="./Address/postalLDU"></xsl:value-of>
							</xsl:element>														
						</xsl:if>
					</xsl:for-each>
				</xsl:if>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="CreateBorrowerSection">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>			
			<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>	
				RENSEIGNEMENTS SUR LE(S) DEMANDEUR(S)
		</xsl:element>
		<!-- Line 1 -->
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:call-template name="OriginRow"></xsl:call-template>	
				<!-- &&& Repeat for all sorted borrowers. There should be 4 of them. -->
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:call-template name="BorrowerTableRowOne"></xsl:call-template>					
				</xsl:for-each>
				<xsl:call-template name="BorrowerAddressTableRow"></xsl:call-template>	
				<xsl:call-template name="BorrowerTableRowMaritalStatus"></xsl:call-template>		
				<xsl:call-template name="BorrowerTableRowResidentialStatus"></xsl:call-template>	
				<xsl:call-template name="BorrowerTableRowResidentialStatusTwo"></xsl:call-template>
				<xsl:call-template name="PreviousAddressesList"></xsl:call-template>
			</xsl:element>
		</xsl:element>
		<!-- ====================== -->
	</xsl:template>	

	<!-- ===========================  -->
	<xsl:template name="PreviousAddressesList">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">19</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Adresse antérieure (si moins de 3 ans) :
				</xsl:element>
				<xsl:if test="//DealAdditional/primBorMonthsAtCurrentAddress &lt; 36">
					<xsl:for-each select="//DealAdditional/Borrower/BorrowerAddress">
						<xsl:if test="./borrowerAddressTypeId = 1">
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="./Address/addressLine1"></xsl:value-of>
									<xsl:if test="./Address/addressLine2">
										<xsl:text>, </xsl:text><xsl:value-of select="./Address/addressLine2"></xsl:value-of>
									</xsl:if>
									<xsl:if test="./Address/city"><xsl:text>, </xsl:text><xsl:value-of select="./Address/city"/></xsl:if>
									<xsl:if test="./Address/province"><xsl:text>, </xsl:text><xsl:value-of select="./Address/province"/></xsl:if>
									<xsl:if test="./Address/postalFSA"><xsl:text>, </xsl:text><xsl:value-of select="./Address/postalFSA"/></xsl:if>
									<xsl:if test="./Address/postalLDU"><xsl:text>  </xsl:text><xsl:value-of select="./Address/postalLDU"/></xsl:if>
							</xsl:element>														
						</xsl:if>
					</xsl:for-each>
				</xsl:if>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->
	<xsl:template name="BorrowerAddressTableRow">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">10</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Adresse	
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:value-of select="//DealAdditional/BorrowerAddress/Address/addressLine1"/>
					<xsl:if test="//DealAdditional/BorrowerAddress/Address/addressLine2">
						<xsl:text>, </xsl:text>
						<xsl:value-of select="//DealAdditional/BorrowerAddress/Address/addressLine2"></xsl:value-of>					
					</xsl:if>
					<xsl:text>, </xsl:text><xsl:value-of select="//DealAdditional/BorrowerAddress/Address/city"></xsl:value-of>
					<xsl:text>, </xsl:text><xsl:value-of select="//DealAdditional/BorrowerAddress/Address/province"></xsl:value-of>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Code Postal</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="//DealAdditional/BorrowerAddress/Address/postalFSA"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="//DealAdditional/BorrowerAddress/Address/postalLDU"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>No. de téléphone</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="//DealAdditional/Borrower/borrowerHomePhoneNumber"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->
	<xsl:template name="BorrowerTableRowMaritalStatus">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">10</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						État civil	
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="//DealAdditional/Borrower/maritalStatusId=0">
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Célibataire &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Marié(e) &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Divorcé(e) &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Autre &#160;&#160;</xsl:text>
						</xsl:when>
						<xsl:when test="//DealAdditional/Borrower/maritalStatusId=2">
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Célibataire &#160;&#160;</xsl:text>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Marié(e) &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Divorcé(e) &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Autre &#160;&#160;</xsl:text>
						</xsl:when>						
						<xsl:when test="//DealAdditional/Borrower/maritalStatusId=5">
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Célibataire &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Marié(e) &#160;&#160;</xsl:text>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Divorcé(e) &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Autre &#160;&#160;</xsl:text>
						</xsl:when>						
						<xsl:otherwise>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Célibataire &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Marié(e) &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Divorcé(e) &#160;&#160;</xsl:text>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Autre &#160;&#160;</xsl:text>						
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">9</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Personne à charge</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="//DealAdditional/Borrower/numberOfDependents"></xsl:value-of>						
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->
	<xsl:template name="BorrowerTableRowResidentialStatusTwo">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="//DealAdditional/BorrowerAddress/residentialStatusId=1">
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Locataire </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Locataire </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Depuis</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:if test="//DealAdditional/BorrowerAddress/residentialStatusId != 1">					
						<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
						     <xsl:choose><xsl:when test="position()=1">
									<xsl:value-of select="./monthsAtCurrentAddress"/>
							</xsl:when>	</xsl:choose>						
						</xsl:for-each>	
					</xsl:if>
				</xsl:element>				
			</xsl:element>						
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Loyer</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:for-each select="//DealAdditional/Borrower/Liability">
							<xsl:if test="./liabilityTypeId=16">
								<xsl:value-of select="./liabilityMonthlyPayment"></xsl:value-of>
							</xsl:if>
						</xsl:for-each>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Propriétaire</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:for-each select="//DealAdditional/Borrower/Liability">
							<xsl:if test="./liabilityTypeId=16">
								<xsl:value-of select="./liabilityDescription"></xsl:value-of>
							</xsl:if>
						</xsl:for-each>
				</xsl:element>				
			</xsl:element>			
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>No. de téléphone</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text> </xsl:text>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->					
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->
	<xsl:template name="BorrowerTableRowResidentialStatus">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="//DealAdditional/BorrowerAddress/residentialStatusId=1">
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Propriétaire </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Propriétaire </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Depuis</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:if test="//DealAdditional/BorrowerAddress/residentialStatusId = 1">
						<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
						     <xsl:choose><xsl:when test="position()=1">
								<xsl:value-of select="./monthsAtCurrentAddress"/>
							</xsl:when>	</xsl:choose>						
						</xsl:for-each>	
					</xsl:if>	
				</xsl:element>				
			</xsl:element>						
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Versement hypothécaire</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:for-each select="//DealAdditional/Borrower/Liability">
							<xsl:if test="./liabilityTypeId=0">
								<xsl:value-of select="./liabilityMonthlyPayment"></xsl:value-of>
							</xsl:if>
						</xsl:for-each>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Créancier</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:for-each select="//DealAdditional/Borrower/Liability">
							<xsl:if test="./liabilityTypeId=0">
								<xsl:value-of select="./liabilityDescription"></xsl:value-of>
							</xsl:if>
						</xsl:for-each>
				</xsl:element>				
			</xsl:element>			
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>No. de téléphone</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text> </xsl:text>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->					
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->	
	
	<xsl:template name="BorrowerTableRowTwo">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">8</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Prénom(s) et nom
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
					<xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:value-of select="//DealAdditional/Borrower/borrowerFirstName"/>
					<xsl:text/>
					<xsl:value-of select="//DealAdditional/Borrower/borrowerLastName"/>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="display-align">center</xsl:attribute>
				<xsl:attribute name="border">solid black 0.5px</xsl:attribute><xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="./borrowerGenderId='1'">
							<xsl:call-template name="CheckedCheckbox"/><xsl:text>  M &#160;&#160;  </xsl:text><xsl:call-template name="UnCheckedCheckbox"/><xsl:text> F</xsl:text>
						</xsl:when>
						<xsl:when test="./borrowerGenderId='2'">
							<xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  M &#160;&#160;  </xsl:text><xsl:call-template name="CheckedCheckbox"/><xsl:text> F </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  M &#160;&#160;  </xsl:text><xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  F </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Date de naissance</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./borrowerBirthDate"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>					
					<xsl:text>Numéro d'assurance social</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./socialInsuranceNumber"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	<!-- ===========================  -->
	<xsl:template name="BorrowerTableRowOne">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">9</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Prénom(s) et nom	
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:value-of select="./borrowerFirstName"/><xsl:text> </xsl:text><xsl:value-of select="./borrowerLastName"/>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="display-align">center</xsl:attribute>
				<xsl:attribute name="border">solid black 0.5px</xsl:attribute><xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="./borrowerGenderId='1'">
							<xsl:call-template name="CheckedCheckbox"/><xsl:text> &#160; M   </xsl:text><xsl:call-template name="UnCheckedCheckbox"/><xsl:text>&#160; F</xsl:text>
						</xsl:when>
						<xsl:when test="./borrowerGenderId='2'">
							<xsl:call-template name="UnCheckedCheckbox"/><xsl:text> &#160; M   </xsl:text><xsl:call-template name="CheckedCheckbox"/><xsl:text>&#160; F</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  &#160;M    </xsl:text><xsl:call-template name="UnCheckedCheckbox"/><xsl:text> &#160; F</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Date de naissance</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./borrowerBirthDate"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Numéro d'assurance social</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./socialInsuranceNumber"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	
	<!-- ================================================================== -->
	<!-- ### -->
	<!-- ================================================================== -->	
	<xsl:template name="CreatePageOne">
		<xsl:element name="page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-before</xsl:attribute>
				<!-- <xsl:call-template name="CreateLogoLine"/> -->
			</xsl:element>
			<xsl:element name="fo:static-content"><xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block"><xsl:attribute name="space-before.optimum">3mm</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					Page <fo:page-number/> de <fo:page-number-citation ref-id="{generate-id(/)}"/> </xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<!-- <xsl:call-template name="CreateLogoLine"></xsl:call-template>	 -->
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateBorrowerSection"/>
				<xsl:call-template name="CreateEmploymentHistorySection"/>
				<xsl:call-template name="CreateActifPassifSection"/>
				<xsl:call-template name="CreateBottomTable"/>
				<fo:block id="{generate-id(/)}" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ================================================================== -->	

	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="FOStart">
		<xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:root">
			<xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute>
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">40mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">10mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
