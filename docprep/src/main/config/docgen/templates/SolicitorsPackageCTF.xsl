<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
20.Nov.2008 DVG #DG778 FXP23556: CTFS CR 264 - UAT B - Solictor's letter   
17/Oct/2008 DVG #DG768 FXP22148: CTFS CR 264 - Document clean-up
16April2007 Xue Bin Zhao changed this template based on CTFS One-And-Only Account CR BRD.
22/Sep/2006 DVG #DG514 #4770 Solicitor Package - Revisions to existing
30/Jun/2006 DVG #DG454 #3559 CTFS - Customization of Express Documents
  - created from Genx 
-->		
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<!--#DG514 add comit letter>
	<xsl:variable name="lendName" select="translate(/*/CommitmentLetter/LenderName, '/ \\:*?&quot;&lt;&gt;\|', '')"/>
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendName,'.xsl'))"/>
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*"/-->	
	<xsl:import href="CommitmentLetterCTF.xsl"/>
	<xsl:output method="text"/>

	<xsl:template match="/">
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/>
		</xsl:message>
		<xsl:message>This is a Canadian Tire One-and-OnlyTM account: <xsl:value-of select="$IsOneAndOnly"/>
		</xsl:message>

		<xsl:call-template name="RTFFileStart"/>
		<!-- #DG768 -->
		<xsl:choose>
			<xsl:when test="/*/SolicitorsPackage/LanguageFrench">
				<xsl:call-template name="EnglishSolicitorInstructions"/>
			</xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$IsOneAndOnly">
            <xsl:call-template name="EnglishOneAndOnlyInstructions"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="EnglishSolicitorInstructions"/>
          </xsl:otherwise>
        </xsl:choose>
        <!--#DG514 add comit letter-->
        <xsl:text>{\pard \par \page }</xsl:text>
        <xsl:call-template name="EnglishCommitmentLetter"/>
      </xsl:otherwise>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishSolicitorInstructions">
		<!--#DG238-->
		<xsl:call-template name="LogoAddress"/>
		<xsl:text>\pard\fs16\par
\qc\f1\fs20\b SOLICITOR INSTRUCTIONS LETTER\b0\par
\f1\fs18\ql \par\par 

</xsl:text>
		<xsl:call-template name="Solicitor"/>
		<xsl:text>
\par 
\par Dear </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>,\par 
\par 

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3370 \cellx3910
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6736 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs18 RE:\cell BORROWER(S):\cell </xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, if any:\cell 
</xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/GuarantorNames">
			<xsl:value-of select="./Name"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell MORTGAGE REFERENCE NUMBER:\cell </xsl:text>		
		<xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell PROPERTY ADDRESS:\cell 
</xsl:text>
		<xsl:call-template name="PropertyAddr"/>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell CLOSING DATE:\cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell LOAN AMOUNT:\cell </xsl:text>
		<!--#DG364 xsl:value-of select="/*/SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell PRIORITY:\cell </xsl:text>
		<xsl:value-of select="/*/Deal/lienPosition"/>
		<xsl:text>\cell }
\row 

{\pard\fs10\sa100\brdrb \brdrs \brdrw10 \brsp10 \brdrcf16\par}

\pard \qj \widctlpar\tx540\aspalpha\aspnum\faauto\adjustright\itap0 

\f1\fs18 A Mortgage Loan has been approved for the above-noted Borrower, in accordance with the 
terms and conditions outlined in the Mortgage Commitment attached. We understand that you have 
agreed to act for </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text> in the preparation, execution and registration of the Mortgage against the Property 
and ensuring that the interests of </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text>, as </xsl:text>
		<xsl:value-of select="/*/Deal/lienPosition"/>
		<xsl:text> Mortgagee are validly and 
appropriately secured.\par\par

We consent to you also acting for the Borrower and/or the Guarantor provided that you disclose to 
the Borrower and/or the Guarantor that you also act for us and obtain their consent in writing 
and that you disclose to each party all information you possess or obtain which is relevant to 
this transaction.\par\par

\b Mortgage Details:\b0\par\par

Please refer to the enclosed Mortgage Commitment for the mortgage details, 
terms and conditions.\par\par

\b Mortgage Advances:\b0\par\par

When all conditions precedent to this transaction have been satisfied in full, funds in the 
amount of  </xsl:text>
		<!--#DG514 xsl:value-of select="/*/Deal/netLoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>
		<xsl:text> will be advanced to your trust account, save for the following costs and expenses 
which we will deduct from that amount:\par\par

<!-- #DG768 -->
\trowd\trgaph108\trleft284\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx5812\cellx8505
\pard\intbl\nowidctlpar\qj Mortgage Insurance Premium (Included in Total Loan Amount)\cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Premium"/>
		<xsl:text>\cell 
\row 

{\f1\fs18 Provincial Sales Tax on Premium\cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PST"/>
		<xsl:text>\cell }
\row 

</xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/Fees/Fee">
			<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\row 

</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 TOTAL DEDUCTED\cell </xsl:text><!-- #DG768 -->
		<xsl:value-of select="/*/SolicitorsPackage/TotalDeductionsAmount"/>
		<xsl:text>\cell }
\row 

<!--#DG778 {\f1\fs18 \cell \cell }
\row 
{\f1\fs18 Interest Adjustment (daily per diem)\cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PerDiemInterestAmount"/>
		<xsl:text>\cell }
\row
-->
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
\par The net Mortgage proceeds in the amount of \b </xsl:text><!-- #DG768 -->
		<!--#DG514 xsl:value-of select="/*/SolicitorsPackage/NetAdvanceAmount"/-->
		<xsl:value-of select="/*/SolicitorsPackage/AdvanceAmount"/>
		<xsl:text>\b0, will be deposited to your trust account by electronic funds transfer on the 
scheduled closing date.
<!-- #DG768 The banking information we have for your firm is as follows -->
 If the closing of this Mortgage Loan is delayed past </xsl:text>
    <!-- #DG778 funding -->
		<xsl:value-of select="/*/Deal/estimatedClosingDate"/>
		<xsl:text>, a per diem of </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PerDiemInterestAmount"/>
		<xsl:text> will be deducted from the net Mortgage Proceeds.\par\par 

The banking information we have for your firm is as follows:\par <!-- #DG778 removed underline -->

</xsl:text>
    <xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50 and bankName != '']">
			<xsl:text>Bank: </xsl:text>
			<xsl:value-of select="./bankName"/>
			<xsl:text>\par 
Branch: </xsl:text>
			<xsl:call-template name="BankTransitWoInstitution"/>
			<xsl:text>\par 
Account Number: </xsl:text>
			<xsl:call-template name="AccountNumWStars"/>
			<xsl:text>\par\par </xsl:text>
    </xsl:for-each>
<xsl:text>

Please note that for your protection we have removed certain numbers from your account 
information above.  If you suspect we have the wrong account information for your firm, please 
contact us immediately.\par\par

\b Solicitor Registration Guidelines:\b0\par\par

<!--  ticket 16089 cr110 Mar082007  -->
Please visit our website at 
{\field{\*\fldinst{HYPERLINK "http://www.myCTFS.com/solicitors" }}
{\fldrslt{\cf1\ul www.myCTFS.com/solicitors}}}
 for instructions and documentation necessary to complete a mortgage transaction on behalf of </xsl:text>
		 	<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		 	<xsl:text>.\par\par

</xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text>\par
</xsl:text>
	</xsl:template>


	<!-- ************************************************************************ 	-->
	<!-- English One-and-Only template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishOneAndOnlyInstructions">
		<!--#DG238-->
		<xsl:call-template name="LogoAddress"/>
		<xsl:text>\pard\fs16\par
\qc\f1\fs20\b SOLICITOR INSTRUCTIONS LETTER\b0\par
\f1\fs18\ql \par\par 

</xsl:text>
		<xsl:call-template name="Solicitor"/>
		<xsl:text>
\par 
\par Dear </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>,\par 
\par 

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3370 \cellx3910
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6736 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\f1\fs18 RE:\cell BORROWER(S):\cell </xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, if any:\cell 
</xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/GuarantorNames">
			<xsl:value-of select="./Name"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell PRODUCT REFERENCE NUMBER:\cell </xsl:text>		
		<xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell PROPERTY ADDRESS:\cell 
</xsl:text>
		<xsl:call-template name="PropertyAddr"/>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell LOAN TYPE:\cell </xsl:text>
		<xsl:text>Canadian Tire One-and-Only account</xsl:text>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell PRIORITY:\cell </xsl:text>
		<xsl:value-of select="/*/Deal/lienPosition"/>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell ACCOUNT ACTIVATION/ADVANCE DATE:\cell </xsl:text>
		\line <xsl:value-of select="/*/SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\row 

{\b\f1\fs18 \cell MAXIMUM AMOUNT OF COLLATERAL MORTGAGE:\cell </xsl:text>
		<!--#DG364 xsl:value-of select="/*/SolicitorsPackage/LoanAmount"/-->
		\line<xsl:value-of select="/*/Deal/totalLoanAmount"/>
		<xsl:text>\cell }
\row 

{\pard\fs10\sa100\brdrb \brdrs \brdrw10 \brsp10 \brdrcf16\par}
\pard \qj \widctlpar\tx540\aspalpha\aspnum\faauto\adjustright\itap0 

\f1\fs18 A Canadian Tire One-and-Only{\super TM} account, to be secured by a COLLATERAL MORTGAGE registered against the above property, has been approved for the above-noted Borrower, in accordance with the 
terms and conditions outlined in the Canadian Tire One-and-Only Commitment attached. We understand that you have 
agreed to act for </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text> in connection with the preparation, execution and registration of the Collateral Mortgage against title to the Property 
and ensuring that the interests of </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text>, as </xsl:text>
		<xsl:value-of select="/*/Deal/lienPosition"/>
		<xsl:text> Mortgagee are validly and 
appropriately secured.\par\par

We consent to you also acting for the Borrower and/or the Guarantor provided that you disclose to 
the Borrower and/or the Guarantor that you also act for us and obtain their consent in writing 
so to act and that you disclose to each party all information you possess or obtain which is relevant to 
this transaction.\par\par

\b Account Details:\b0\par\par

Please refer to the enclosed Canadian Tire One-and-Only Commitment for the mortgage details, 
terms and conditions.\par\par

\b Account Advances:\b0\par\par

When the Collateral Mortgage has been registered and all other conditions precedent to this transaction have been satisfied in full, 
the Borrower will be entitled to obtain advances up to the amount of </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LoanAmount"/>
<xsl:text>. A mortgage insurance premium in the amount of </xsl:text>
		<xsl:choose>
		<!--
		MIPayorId: 1 = Borrower, 2 = Lender
		-->
		<xsl:when test="/*/Deal/MIPayorId=1 and /*/Deal/MIUpfront='N'">	
			<xsl:value-of select="/*/Deal/MIPremiumAmount"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>$0.00</xsl:text>
		</xsl:otherwise>
		</xsl:choose>
<xsl:text> will be added to the maximum permitted advances for a total collateral mortgage of </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TotalAmount"/>
<xsl:text> (the Maximum Amount of Collateral Mortgage).

\par\par Funds to be disbursed as follows:	
\par\par

\pard \qj \fi720\li720\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin720\itap0 
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 Initial Borrower Advance \cell </xsl:text>
		 <xsl:choose>
		    <xsl:when test="translate($InitialBorrowerAdvanceFeeAmount, '$,', '') &gt; 0">	
			<xsl:value-of select="$InitialBorrowerAdvanceFeeAmount"/>
		    </xsl:when>
		    <xsl:otherwise>
			    <xsl:text>$0.00</xsl:text>
		    </xsl:otherwise>
		  </xsl:choose>
 		<xsl:text>\cell }
\row

{\f1\fs18 Mortgage Insurance Premium \cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Premium"/>
		<xsl:text>\cell }
\row 

{\f1\fs18 Provincial Sales Tax on Premium\cell </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PST"/>
		<xsl:text>\cell }
\row 

</xsl:text>
		<xsl:for-each select="/*/SolicitorsPackage/Fees/Fee">
			<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\row 

</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 TOTAL\tab \cell </xsl:text>
		<!--xsl:value-of select="/*/SolicitorsPackage/TotalDeductionsAmount"/-->
	    <xsl:choose>
	    <xsl:when test="translate($InitialBorrowerAdvanceFeeAmount, '$,', '') &gt; 0">	
   	        <xsl:value-of select="format-number(translate($InitialBorrowerAdvanceFeeAmount, '$,', '') + translate(/*/SolicitorsPackage/TotalDeductionsAmount, '$,', ''),'$##,000.00')"/>
	    </xsl:when>
	    <xsl:otherwise>
		    <xsl:value-of select="/*/SolicitorsPackage/TotalDeductionsAmount"/>
	    </xsl:otherwise>
	    </xsl:choose>	
		<xsl:text>\cell }
\row 

{\f1\fs18 \cell \cell }
\row 
<!--
{\f1\fs18 Interest Adjustment (daily per diem)\cell </xsl:text>
		< xsl:value-of select="/*/SolicitorsPackage/PerDiemInterestAmount" />
		< xsl:text >\cell }
\row 
-->
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The Borrower has requested an initial advance in the amount of </xsl:text>
		<!-- xsl:value-of select="$InitialBorrowerAdvanceFeeAmount"/ -->
		<xsl:choose>
		    <xsl:when test="translate($InitialBorrowerAdvanceFeeAmount, '$,', '') &gt; 0">	
			<xsl:value-of select="$InitialBorrowerAdvanceFeeAmount"/>
		    </xsl:when>
		    <xsl:otherwise>
			    <xsl:text>$0.00</xsl:text>
		    </xsl:otherwise>
		  </xsl:choose>
<xsl:text>. This amount will be deposited to your trust account by electronic funds transfer on 
the Account Activation/Advance Date.  The banking information we have for your firm is as follows:\par \par }

</xsl:text>
    <xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50 and bankName != '']">
			<xsl:text>Bank: </xsl:text>
			<xsl:value-of select="./bankName"/>
			<xsl:text>\par 
Branch: </xsl:text>
			<xsl:call-template name="BankTransitWoInstitution"/>
			<xsl:text>\par 
Account Number: </xsl:text>
			<xsl:call-template name="AccountNumWStars"/>
			<xsl:text>\par\par </xsl:text>
    </xsl:for-each>
<xsl:text>

Please note that for your protection we have removed certain numbers from your account 
information above.  If you suspect we have the wrong account information for your firm, please 
contact us immediately.\par\par

Note:  In the case of any inconsistency in the amount of the Initial Borrower Advance above 
and the amount of the advance shown on the Solicitor's Requisition of Funds, the amount on 
the Requisition of Funds will govern.\par\par

\b Solicitor Registration Guidelines:\b0\par\par

Please visit our website at {\ul myCTFS.com/solicitors} for instructions and documentation 
necessary to complete a Canadian Tire One-and-Only account transaction on behalf of </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>.
    <xsl:text>\par\par

</xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text>\par
Encl.</xsl:text>
	</xsl:template>

	<xsl:template name="LogoAddress">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{\f1 </xsl:text>  
		<xsl:value-of select="$logoRtf"/>
		<xsl:text>\cell }
\pard \ql \clvertalc\widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text>\par }
{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line1"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
\row
 
</xsl:text>	
	</xsl:template>

	<xsl:template name="PropertyAddr">
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text>\line </xsl:text>
		<xsl:if test="/*/SolicitorsPackage/PropertyAddress/Line2">
			<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Line2"/>
			<xsl:text>\line </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PropertyAddress/Province"/>
	</xsl:template>

	<!--  ============================= SolicitorAddress ====================================== -->
	<xsl:template name="Solicitor">
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>\line </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\line </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="/*/SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\line </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Solicitor/AddressPostal"/>
	</xsl:template>

	<xsl:template name="BankTransitWoInstitution">
		<xsl:if test="contains(./bankTransitNum, '-')">
			<xsl:variable name="transNum" select="substring-after(./bankTransitNum, '-')"/>
			<xsl:text>*</xsl:text>
			<xsl:value-of select="substring($transNum, 2, (string-length($transNum) -2))"/>
			<xsl:text>*</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template name="AccountNumWStars">
		<xsl:text>*</xsl:text>
		<xsl:value-of select="substring(./bankAccNum, 2, (string-length(./bankAccNum) -2))"/>
		<xsl:text>*</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff63\deflang1033\deflangfe1033
{\fonttbl
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f14\fnil\fcharset2\fprq2{\*\panose 05000000000000000000}Wingdings;}
}
{\colortbl;
\red0\green0\blue0;
\red0\green0\blue255;
\red0\green255\blue255;
\red0\green255\blue0;
\red255\green0\blue255;
\red255\green0\blue0;
\red255\green255\blue0;
\red255\green255\blue255;
\red0\green0\blue128;
\red0\green128\blue128;
\red0\green128\blue0;
\red128\green0\blue128;
\red128\green0\blue0;
\red128\green128\blue0;
\red128\green128\blue128;
\red192\green192\blue192;
\red255\green255\blue255;
}
{\stylesheet
{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \snext0 Normal;}
{\s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 \sbasedon0 \snext0 heading 1;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext15 header;}
{\s16\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext16 footer;}
}
\margl720\margr720\margt720\margb720 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul
\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720
\dghshow1\dgvshow1\jexpand
\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp
\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0
<!--#DG286 -->\headery0\footery400\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>
</xsl:stylesheet>
