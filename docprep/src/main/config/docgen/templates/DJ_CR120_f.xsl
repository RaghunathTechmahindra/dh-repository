<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
31/Jan/2007 DVG #DG578 #5713  CR 309 (120) - changes for PPI  
19/Jul/2006 DVG #DG466 #3021  CR 309 - combination of DJ deal summary (DJ_DealSummaryLite.xsl) and CR 120. 
16/Sep/2005 DVG #DG318 #2108  DJ - CR120 - Line beside section titles - make them a variable length  
	#2112  DJ - CR120 - increase "et al." font to 12 pts  
01/Jun/2005 DVG #DG218 #1488  DJ - CR120 - Disability percentage coverage required 
25/May/2005 DVG #DG214 #1440  CR120 - no data in the document since last build in QA
	xsl syntax error, also recreated new, copied and pasted from notepad to correct french chars 
25/Apr/2005 DVG #DG200 #1231  DJ - CR120 - change reference of deal.firstpaymentdatemonthly to deal.firstpaymentdate 
25/Apr/2005 DVG #DG198 #1212 DJ - CR120 - need to add addressline2 for each borrower
25/Apr/2005 DVG #DG196 #1221  DJ - CR120 - Property address information  
15/Mar/2005 DVG #DG168 #1075  DJ - document change - CR120  
28/Feb/2005 DVG #DG148 #1014 CR120 - Life % coverage displayed  
22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik
 Modified by Catherine Rutgaizer, Mar 8, 04
 Author Zivko Radulovic 
 -->
<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:fo="http://www.w3.org/1999/XSL/Format" 
  xmlns:fox="http://xml.apache.org/fop/extensions"	

  xmlns:xsltc-extension="http://xml.apache.org/xalan/xsltc"
  xmlns:xalan="http://xml.apache.org/xalan"
  xmlns:exslt="http://exslt.org/common">
  
  <!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	<xsl:preserve-space elements="text"/>
	<xsl:preserve-space elements="fo:block"/>

	<!-- ================================================================== -->
	<!-- define attribute sets -->
	<xsl:attribute-set name="TableCellAttributes">
		<xsl:attribute name="padding-top">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="border">solid black 1px</xsl:attribute>
		<xsl:attribute name="text-align">start</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableBlockAttributes">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		<!-- #DG578 xsl:attribute name="keep-together">always</xsl:attribute-->
	</xsl:attribute-set>
	
	<!--#DG466 -->
	<xsl:attribute-set name="TableBlockAttribA" use-attribute-sets="TableBlockAttributes">
		<xsl:attribute name="space-before.optimum">0.5pt</xsl:attribute>
	</xsl:attribute-set>

	<xsl:attribute-set name="TitleA">
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">16pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-style">normal</xsl:attribute>
		<xsl:attribute name="space-before.optimum">12pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">6pt</xsl:attribute>
		<!-- #DG578 xsl:attribute name="keep-together">always</xsl:attribute-->
	</xsl:attribute-set>
	
	<xsl:attribute-set name="TitleB">
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">14pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-style">italic</xsl:attribute>
		<xsl:attribute name="space-before.optimum">12pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">6pt</xsl:attribute>
		<!-- #DG578 xsl:attribute name="keep-together">always</xsl:attribute-->
	</xsl:attribute-set>
	
	<xsl:attribute-set name="TitleC">
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-style">normal</xsl:attribute>
		<xsl:attribute name="space-before.optimum">12pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">6pt</xsl:attribute>
		<!-- #DG578 xsl:attribute name="keep-together">always</xsl:attribute-->
	</xsl:attribute-set>
	
	<xsl:attribute-set name="TextBoxed">
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="font-weight">normal</xsl:attribute>
		<xsl:attribute name="font-style">normal</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:attribute-set name="TableHeaderCellAttributes">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="text-align">start</xsl:attribute>
		<xsl:attribute name="display-align">after</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableHeaderBlockAttributes">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		<!-- #DG578 xsl:attribute name="keep-together">always</xsl:attribute-->
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="LiabTable">
		<xsl:attribute name="padding-top">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">1mm</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="text-align">end</xsl:attribute>
		<xsl:attribute name="border">solid black 1px</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">9pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr SpaceBAOptimum">
		<!-- #DG578 xsl:attribute name="keep-together">always</xsl:attribute-->
	</xsl:attribute-set>
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="eletitregrp">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">2pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">2pt</xsl:attribute>
		<xsl:attribute name="color">#C0C0C0</xsl:attribute>
		<xsl:attribute name="background-color">#fff5ff</xsl:attribute>
		<xsl:attribute name="font-family">Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="padding-top">1pt</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">1pt</xsl:attribute>
		<xsl:attribute name="padding-right">0pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="enteteAR">
		<xsl:attribute name="color">#ffffff</xsl:attribute>
		<xsl:attribute name="background-color">#0052ba</xsl:attribute>
		<xsl:attribute name="font-family">Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="eletitregauche">
		<xsl:attribute name="font-family">Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="border-style">solid</xsl:attribute>
		<xsl:attribute name="border-width">0.1mm</xsl:attribute>
		<xsl:attribute name="background-color">#e2e2e2</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute> 
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="eletitregauche2" use-attribute-sets="eletitregauche">
		<xsl:attribute name="background-color">#ffffff</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="elesubtitre">
		<xsl:attribute name="border-style">solid</xsl:attribute>
		<xsl:attribute name="border-width">0.1mm</xsl:attribute>
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">2pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">2pt</xsl:attribute>
		<xsl:attribute name="color">#0052ba</xsl:attribute>
		<xsl:attribute name="background-color">#EEEEEE</xsl:attribute>
		<xsl:attribute name="font-family">Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="padding-top">1pt</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">1pt</xsl:attribute>
		<xsl:attribute name="padding-right">0pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="elemonaie">
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="padding-top">1pt</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">1pt</xsl:attribute>
		<xsl:attribute name="padding-right">0pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
		<xsl:attribute name="font-family">Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="border-style">solid</xsl:attribute>
		<xsl:attribute name="border-width">0.1pt</xsl:attribute>
		<xsl:attribute name="display-align">after</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="color">#0052ba</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="table_values">
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="padding-top">1pt</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">1pt</xsl:attribute>
		<xsl:attribute name="padding-right">0pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
		<xsl:attribute name="font-family">Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="border-style">solid</xsl:attribute>
		<xsl:attribute name="border-width">0.1pt</xsl:attribute>
		<xsl:attribute name="display-align">after</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr" use-attribute-sets="SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableLeftFixedCR309">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
		<xsl:attribute name="border-top-style">outset</xsl:attribute>
		<xsl:attribute name="border-top-color">black</xsl:attribute>
		<xsl:attribute name="border-left-style">outset</xsl:attribute>
		<xsl:attribute name="border-left-color">black</xsl:attribute>
		<xsl:attribute name="border-bottom-style">outset</xsl:attribute>
		<xsl:attribute name="border-bottom-color">black</xsl:attribute>
		<xsl:attribute name="border-right-style">outset</xsl:attribute>
		<xsl:attribute name="border-right-color">black</xsl:attribute>
		<xsl:attribute name="background-color">#ff0000</xsl:attribute>
		<xsl:attribute name="border-style">solid</xsl:attribute>
		<xsl:attribute name="border-width">1pt</xsl:attribute>
		<xsl:attribute name="cellspacing">0</xsl:attribute>
		<xsl:attribute name="cellpadding">0</xsl:attribute>
	</xsl:attribute-set>
	<!-- end attribute sets -->

	<!-- ================================================================== -->
	<!-- Common templates -->
	<xsl:template name="TableOf3Columns">
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">5.67cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">5.67cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">5.67cm</xsl:attribute>
		</xsl:element>
	</xsl:template>

	<xsl:template name="TableOf3sColumns">
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">3.4cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">3.4cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">3.4cm</xsl:attribute>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="TableOf4Columns">
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">4.25cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">4.25cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">4.25cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">4.25cm</xsl:attribute>
		</xsl:element>
	</xsl:template>

	<xsl:template name="TableOf5Columns">
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">3.4cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">3.4cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">3.4cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">3.4cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">3.4cm</xsl:attribute>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="TableOf6Columns">
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">2.83cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">2.83cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">2.83cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">2.83cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">2.83cm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">2.83cm</xsl:attribute>
		</xsl:element>
	</xsl:template>

  <!--#DG466 -->
	<xsl:template name="solidLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<fo:table-column column-width="170mm"/>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<fo:block border-bottom-style="solid" border-bottom-color="black" border-bottom-width="1px">
  							<xsl:text/>
						</fo:block>
		      </xsl:element>
		    </xsl:element>
  		</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- ================================================================== -->
	<xsl:variable name="EfxTest" select="/*/specialRequirementTags/CreditResponseBlock3/CreditBureauReports/EfxTransmit"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>

	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">15mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<!--#DG578 moved from up -->
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">40mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">15mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
			<!-- <xsl:call-template name="CreateBorrowerSection"/>
			 <xsl:call-template name="CreatePageThree"/>
			<xsl:call-template name="CreatePageFour"/>  -->						
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
      <xsl:attribute name="hyphenate">true</xsl:attribute><!--#DG466 -->
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<!-- <xsl:call-template name="CreateLogoLine"/> -->
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					<fo:page-number/> de <fo:page-number-citation ref-id="page_1"/>
			    </xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<!-- <xsl:call-template name="CreateLogoLine"></xsl:call-template>	 -->
				<xsl:call-template name="CreateTitleLine"/>
                <xsl:call-template name="CreateCSResponseBlock1"/> <!--Xue Bin Zhao added-->
				<xsl:call-template name="CreateBorrowerSection"/>
				<xsl:call-template name="CreateSummarySection"/>
				<xsl:call-template name="CreateDetailsSection"/>
				<!-- Vandana commented - FXP16511 - Begin 4/3/2007-->
				<!-- Ref: Functional Specifications_ExpressDFS LinkINTERNAL v4.5.doc - Section: 12.5.2 -->
				<!-- <xsl:call-template name="CreateCBReportSection"/> -->
				<!-- Vandana commented - FXP16511 - End, 4/3/2007 -->
				<xsl:call-template name="CreateCSResponseBlock2"/> <!--Xue Bin Zhao added-->
				<!-- Vandana Start 1-->
				<!-- <fo:block break-before="page"  /> -->
				<xsl:for-each select="$EfxTest">
					<xsl:variable name="counter" select="position()"/>
					<xsl:variable name="count" select="last()"/>
					<xsl:text>Fiche de cr�dit</xsl:text>
					<xsl:if test = "$counter &lt;= $count">
						<fo:block break-before="page"  />
					</xsl:if>
					<xsl:if test = "$counter &lt;= 1">
						<xsl:element name="fo:block">
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">12pt</xsl:attribute> 
							<xsl:attribute name="space-after.optimum">12pt</xsl:attribute> 
							<xsl:attribute name="keep-together">always</xsl:attribute>	  								
							<fo:inline font-weight="bold">
							<xsl:text>Fiche de cr�dit</xsl:text>
							</fo:inline>
						</xsl:element>
					</xsl:if>
					<xsl:call-template name="CreateTitleLineReport"/>
					<xsl:call-template name="DealInfo"/>
					<xsl:call-template name="Identification"/>
					<xsl:call-template name="Pointage"/>
					<xsl:call-template name="FraudWarnings"/>
					<xsl:call-template name="Employments"/>
					<xsl:call-template name="OtherIncomes"/>
					<xsl:call-template name="BankruptciesOrActs"/>
					<xsl:call-template name="Collections"/>
					<xsl:call-template name="LegalItems"/>
					<xsl:call-template name="SecuredLoans"/>
					<xsl:call-template name="NonResponsabilityNotices"/>
					<xsl:call-template name="MaritalItems"/>
					<xsl:call-template name="NSFs"/>
					<xsl:call-template name="Garnishments"/>
					<xsl:call-template name="Trades"/> 
					<xsl:call-template name="NonMemberTrades"/>
					<xsl:call-template name="BankAccounts"/>
					<xsl:call-template name="LocalInquiries"/>
					<xsl:call-template name="ForeignInquiries"/>
					<xsl:call-template name="SpecialServices"/>
					<xsl:call-template name="ConsumerStatements"/>
					<xsl:call-template name="EndOfReport"/>
					
					
					
					<!--<fo:block id="{generate-id(/)}" line-height="0pt"/> -->
				
			</xsl:for-each>
				<!-- Vandana End 1-->
                <fo:block id="page_1" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">4px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
		        <xsl:element name="fo:block" use-attribute-sets="TitleA"><!--#DG466 -->
              <xsl:attribute name="text-decoration">underline</xsl:attribute>							
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>Formulaire de demande de cr�dit et d'analyse</xsl:text>
						</xsl:element>
		        <xsl:element name="fo:block" use-attribute-sets="TitleA"><!--#DG466 -->
              <xsl:attribute name="text-decoration">underline</xsl:attribute>							
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>Hypoth�que immobili�re</xsl:text>
						</xsl:element>
		        <xsl:element name="fo:block" use-attribute-sets="TitleA"><!--#DG466 -->
              <xsl:attribute name="text-decoration">underline</xsl:attribute>							
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>no. &#160;</xsl:text>
							<xsl:value-of select="/*/Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateBorrowerSection">
		<!--#DG466 rewritten --><!--#DG466 -->
		<xsl:element name="fo:block" use-attribute-sets="TitleA"><!--#DG466 -->
			<xsl:attribute name="space-before.optimum">7mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">7mm</xsl:attribute>
			Renseignements sur le(s) demandeur(s)
		</xsl:element>
    <xsl:call-template name="solidLine"/>	
		<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId &lt;= 1]">
				<xsl:element name="fo:block" use-attribute-sets="TitleB"><!--#DG466 -->
         	<xsl:choose>
        		<xsl:when test="position()=1">
        		  <xsl:text>Demandeur principal</xsl:text>
              <xsl:call-template name="listBorrower"/>
        		</xsl:when>

        		<xsl:when test="position() = 5">
        		  <xsl:text>et al.</xsl:text>
        		</xsl:when>
        		
        		<xsl:when test="position() > 5">
        		</xsl:when>
        		<xsl:otherwise>
  					  <xsl:attribute name="break-before">page</xsl:attribute>
  					  <xsl:attribute name="space-before.optimum">0mm</xsl:attribute>
  					  <xsl:attribute name="space-after.optimum">0mm</xsl:attribute>
              <xsl:call-template name="coAppHeader"/>
              <xsl:call-template name="listBorrower"/>
        		</xsl:otherwise>        		
        	</xsl:choose>
				</xsl:element>
		</xsl:for-each>
	</xsl:template>
	
  <!--#DG466 rewritten -->
	<xsl:template name="CreateSummarySection">
		<xsl:element name="fo:block" use-attribute-sets="TitleA">
      <xsl:attribute name="break-before">page</xsl:attribute>
			<xsl:attribute name="space-after.optimum">7mm</xsl:attribute>
			<fo:inline font-weight="bold">R�sum� combin� des emprunteurs</fo:inline>
		</xsl:element>
    <xsl:call-template name="solidLine"/>	
		<xsl:call-template name="SummaryTableTypeOne"/>
	</xsl:template>

	<!-- ======================================================================== -->
	<!-- ZZZ &&& -->
	<!-- ======================================================================== -->
	<!--#DG466 rewritten -->
	<xsl:template name="CreateDetailsSection">
		<xsl:element name="fo:block" use-attribute-sets="TitleA">
			<xsl:attribute name="space-before.optimum">7mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">7mm</xsl:attribute>
			<fo:inline font-weight="bold">D�tails de la demande</fo:inline>
		</xsl:element>
    <xsl:call-template name="solidLine"/>	

		<xsl:call-template name="DetailsTableTypeOne"/>
		<xsl:call-template name="DetailsTableTypeTwo"/>
		<xsl:call-template name="DetailsTableTypeThree"/>
		<xsl:call-template name="DetailsTableTypeFour"/>
		<xsl:call-template name="DetailsTableTypeFive"/>
		<xsl:call-template name="InsuranceTableTypeOne"/>
		<xsl:call-template name="InsuranceTableTypeTwo"/>
		<xsl:call-template name="InsuranceTableTypeThree"/>
		<xsl:call-template name="InsuranceTableTypeFour"/>
		<xsl:call-template name="InsuranceTableTypeFive"/>
		<xsl:call-template name="InsuranceTableTypeSix"/>
		<xsl:call-template name="InsuranceTableTypeSeven"/>
		<xsl:call-template name="InsuranceIITableTypeOne"/>
		<xsl:call-template name="InsuranceIIITableTypeOne"/>
		<xsl:call-template name="InsuranceIIITableTypeTwo"/>
		<xsl:call-template name="InsuranceIIITableTypeThree"/>
		<xsl:call-template name="InsuranceIIITableTypeFour"/>
		<xsl:call-template name="InsuranceIIITableTypeFive"/>
		<xsl:call-template name="LiabilitiesTableTypeOne"/>
		<xsl:call-template name="LiabilitiesTableTypeTwo"/>
		<xsl:call-template name="LiabilitiesTableTypeThree"/>
		<xsl:call-template name="LiabilitiesTableTypeThreeA"/>
		<xsl:call-template name="LiabilitiesTableTypeFourA"/>
		<xsl:call-template name="LiabilitiesTableTypeFourB"/>
		<xsl:call-template name="DealNotesTableTypeOne"/>
	</xsl:template>

	<xsl:template name="CreateCBReportSection">
		<xsl:element name="fo:block" use-attribute-sets="TitleC">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute><!--#DG466 -->
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			Rapport de Bureau de cr�dit
		</xsl:element>
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">4mm</xsl:attribute><!--#DG466 -->
			<xsl:attribute name="border">solid black 1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:call-template name="GetAllCBReports"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="DealNotesTableTypeOne">
    <!--#DG466 rewritten -->
		<xsl:element name="fo:block" use-attribute-sets="TitleA"><!--#DG466 -->
      <xsl:attribute name="break-before">page</xsl:attribute>
			<xsl:attribute name="space-after.optimum">7mm</xsl:attribute>
			<fo:inline font-weight="bold">Analyse de la demande</fo:inline>
		</xsl:element>
    <xsl:call-template name="solidLine"/>
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">7mm</xsl:attribute><!--#DG466 -->
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
			  <xsl:attribute name="space-before">1mm</xsl:attribute><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
    			  <!--#DG466 if empty add blank line -->
    				<xsl:variable name="t" select="/*/Deal/DealNotes/DealNote[dealNotesCategoryId = '3']"/>
            <xsl:if test="not($t)">
  						<xsl:element name="fo:block">
				        <xsl:text> &#160;</xsl:text>
  						</xsl:element>
            </xsl:if>
    				<xsl:for-each select="$t">
              <xsl:sort select="dealNotesId" order="ascending" /><!--#DG466 sorted -->
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="./dealNotesText"/>
							</xsl:element>
						</xsl:for-each>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateMIResponseSection">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			R�ponse de l'assureur
		</xsl:element>
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:attribute name="border">solid black 1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="white-space-collapse">false</xsl:attribute>
							<xsl:value-of select="/*/Deal/mortgageInsuranceResponse"/>
		          <xsl:text>&#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="GetAllCBReports">
		<xsl:for-each select="/*/Deal/CreditBureauReport">
			<xsl:for-each select="./creditReport/Line">
				<xsl:element name="fo:block">
					<xsl:value-of select="."/>
				</xsl:element>
			</xsl:for-each>
			<xsl:element name="fo:block">
				<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
				<xsl:text> </xsl:text>
			</xsl:element>
		</xsl:for-each>
		<xsl:text>&#160;</xsl:text><!--#DG466 -->
	</xsl:template>
	<!-- ========================= end Catherine  ======================================= -->
	
	<!-- ==================================================================================================================================== -->
	<xsl:template name="LiabilitiesTableTypeFourA">
	  <!--#DG466 -->
		<xsl:element name="fo:block" use-attribute-sets="TitleB"><!--#DG466 -->
		  <xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
				Ratios d'endettement
		</xsl:element>

		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4cm</xsl:attribute>
			</xsl:element>
			
			<!--#DG466 use headers-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
						  <xsl:attribute name="wrap-option">no-wrap</xsl:attribute><!--#DG466 -->
								Ratios combin�s sur le taux contractuel 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>Ratio ATD  </xsl:text>
							<xsl:value-of select="/*/specialRequirementTags/combinedTDS"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>Ratio ABD  </xsl:text>
							<xsl:value-of select="/*/specialRequirementTags/combinedGDS"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="LiabilitiesTableTypeFourB">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4cm</xsl:attribute>
			</xsl:element>

      <!--#DG466 use header -->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
						  <xsl:attribute name="wrap-option">no-wrap</xsl:attribute><!--#DG466 -->
								Ratios combin�s sur 3 ans
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>

			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>Ratio ATD  </xsl:text>
							<xsl:value-of select="/*/specialRequirementTags/CombinedTDS3Year"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>Ratio ABD  </xsl:text>
							<xsl:value-of select="/*/specialRequirementTags/CombinedGDS3Year"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- ==================================================================================================================================== -->
	<xsl:template name="LiabilitiesTableTypeThreeA">
	  <!--#DG466 moved -->
		<xsl:element name="fo:block" use-attribute-sets="TitleB"><!--#DG466 -->
			    Revenu mensuel brut consid�r�
		</xsl:element>

		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:call-template name="TableOf3Columns"/>
			<xsl:call-template name="LiabilitiesTableHeaderTypeThreeB"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='0']">
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							    <xsl:choose>
								    <xsl:when test="primaryBorrowerFlag='Y'">
									    <xsl:text>Emprunteur principal</xsl:text>
								    </xsl:when>
								    <xsl:otherwise>
									    <xsl:text>Co-emprunteur</xsl:text>
								     </xsl:otherwise>
							    </xsl:choose>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							    <xsl:value-of select="./borrowerFirstName"/>
							    <xsl:text>&#160;</xsl:text>
							    <xsl:value-of select="./borrowerMiddleInitial"/>
							    <xsl:text>&#160;</xsl:text>
							    <xsl:value-of select="./borrowerLastName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<!-- #DG466  xsl:attribute name="text-align">left</xsl:attribute-->
			        <xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="totalMonthlySumIncludeInTDS"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="LiabilitiesTableHeaderTypeThreeB">
			<xsl:element name="fo:table-header">
			  <xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
				    <xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
							    <xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
				    <xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
								Pr�nom et nom de l'emprunteur
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
				    <xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
								Revenu mensuel brut consid�r�
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>

	<!-- ==================================================================================================================================== -->
	<xsl:template name="LiabilitiesTableTypeThree">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">13cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="LiabilitiesTableHeaderTypeThree"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">end</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/specialRequirementTags/variousLiabilities/variousLiabilityTotal"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="LiabilitiesTableHeaderTypeThree">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Total des mensualit�s
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ==================================================================================================================================== -->
	<xsl:template name="LiabilitiesTableTypeTwo">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">3mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">30mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">26.6mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="LiabilitiesTableHeaderTypeTwo"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
      			  <!--#DG466 if empty add blank line -->
      				<xsl:variable name="t" select="/*/specialRequirementTags/variousLiabilities/creditCardLiabilityTotal"/>
              <xsl:if test="not($t)">
      				        <xsl:text> &#160;</xsl:text>
              </xsl:if>
      				<xsl:value-of select="$t"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/variousLiabilities/loansLiabilityTotal"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/variousLiabilities/otherLiabilityTotal"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/variousLiabilities/monthlyHeatingExp"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
						<!-- Taxes mensuelles -->
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/variousLiabilities/monthlyTaxExp"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/variousLiabilities/monthlyCondoExp"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="LiabilitiesTableHeaderTypeTwo">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Total cartes de cr�dit
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Total pr�ts
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
								 Total autres obligations
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
								Frais de chauffage mensuels
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
								Taxes mensuelles
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
								Frais de condo consid�r�
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ==================================================================================================================================== -->
	<xsl:template name="LiabilitiesTableTypeOne">
	  <!--#DG466 rewritten -->
		<xsl:element name="fo:block" use-attribute-sets="TitleA">
      <xsl:attribute name="break-before">page</xsl:attribute>
			<xsl:attribute name="space-after.optimum">7mm</xsl:attribute>
			<fo:inline font-weight="bold">Donn�es sur les obligations financi�res</fo:inline>
		</xsl:element>
    <xsl:call-template name="solidLine"/>	

		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">7mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.83cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">35mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			<xsl:call-template name="LiabilitiesTableHeaderTypeOne"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
						<xsl:attribute name="text-align">end</xsl:attribute>
						<xsl:choose>
							<xsl:when test="/*/specialRequirementTags/variousLiabilities/creditCardLiabilities/creditCardLiability">
								<xsl:for-each select="/*/specialRequirementTags/variousLiabilities/creditCardLiabilities/creditCardLiability">
									<xsl:element name="fo:block">
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:value-of select="./amount"/>
									</xsl:element>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:text>&#160;</xsl:text>
								</xsl:element>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
 						<fo:block/><!--#DG578 guarantee 1 item minimum -->  
						<xsl:for-each select="/*/specialRequirementTags/variousLiabilities/loanLiabilities/loanLiability">
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="./amount"/>
							</xsl:element>
						</xsl:for-each>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="LiabTable"/>
 						<fo:block/><!--#DG578 guarantee 1 item minimum -->  
						<xsl:for-each select="/*/specialRequirementTags/variousLiabilities/otherLiabilities/otherLiability">
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="./amount"/>
							</xsl:element>
						</xsl:for-each>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="LiabilitiesTableHeaderTypeOne">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Cartes de cr�dit
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Pr�ts
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
								Autres obligations
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ==================================================================================================================================== -->
	<xsl:template name="InsuranceIIITableTypeFive">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
		  <xsl:call-template name="InsuranceIIITableHeaderTypeFive"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="/*/DealAdditional/appraiserData/partyProfile/ptPShortName">
									<xsl:value-of select="/*/DealAdditional/appraiserData/partyProfile/ptPShortName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/appraiserData/partyProfile/ptPBusinessId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceIIITableHeaderTypeFive">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<!- - <xsl:attribute name="space-before">5px</xsl:attribute> - ->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								�valuateur agr��
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								No d'identification
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ==================================================================================================================================== -->
	<xsl:template name="InsuranceIIITableTypeFour">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="InsuranceIIITableHeaderTypeFour"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="starts-with(//DealAdditional/referralSource/partyProfile/ptPBusinessId, 'B')">
									<xsl:value-of select="/*/DealAdditional/referralSource/partyProfile/ptPShortName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="starts-with(//DealAdditional/referralSource/partyProfile/ptPBusinessId, 'B')">
									<xsl:value-of select="/*/DealAdditional/referralSource/partyProfile/ptPBusinessId"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceIIITableHeaderTypeFour">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								Constructeur
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								No d'identification
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ==================================================================================================================================== -->
	<xsl:template name="InsuranceIIITableTypeThree">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="InsuranceIIITableHeaderTypeThree"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="starts-with(//DealAdditional/referralSource/partyProfile/ptPBusinessId, 'C')">
									<xsl:value-of select="/*/DealAdditional/referralSource/partyProfile/partyCompanyName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:text>&#160;</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceIIITableHeaderTypeThree">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								Courtier immobilier
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								No d'identification
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ==================================================================================================================================== -->
	<xsl:template name="InsuranceIIITableTypeTwo">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="InsuranceIIITableHeaderTypeTwo"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="starts-with(//DealAdditional/referralSource/partyProfile/ptPBusinessId, 'C')">
									<xsl:value-of select="/*/DealAdditional/referralSource/partyProfile/ptPShortName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="starts-with(//DealAdditional/referralSource/partyProfile/ptPBusinessId, 'C')">
									<xsl:value-of select="/*/DealAdditional/referralSource/partyProfile/ptPBusinessId"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceIIITableHeaderTypeTwo">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								Agent immobilier
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								No d'identification
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="InsuranceIIITableTypeOne">
	  <!--#DG466 rewritten --> 
		<xsl:element name="fo:block" use-attribute-sets="TitleA"><!--#DG466 -->
			<xsl:attribute name="space-before.optimum">7mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">7mm</xsl:attribute>
			<fo:inline font-weight="bold">Autres donn�es sur le pr�t</fo:inline>
		</xsl:element>
    <xsl:call-template name="solidLine"/>	
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">7mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="InsuranceIIITableHeaderTypeOne"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactFirstName"/>
							<xsl:text>  </xsl:text>
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactLastName"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/alternativeId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceIIITableHeaderTypeOne">
	  <!--#DG466 block moved to body -->
	  
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								Repr�sentant hypoth�caire
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								No d'identification
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="createInsuranceRows">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">40mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">40mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							Assurance pr�t
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							% Couverture
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='0' or borrowerTypeId='1']">
					<xsl:element name="fo:table-row">
						<!-- 1 -->
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:attribute name="text-align">start</xsl:attribute>
              <xsl:call-template name="coAppHead"/><!--#DG466 -->	
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:choose>
									<xsl:when test="./disabilityStatusId=1">
										<xsl:text>Invalidit� &amp; vie</xsl:text>
									</xsl:when>
									<xsl:when test="./disabilityStatusId=2 or ./disabilityStatusId=3">
										<xsl:choose>
											<xsl:when test="./lifeStatusId=1">
												<xsl:text>Vie seulement</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>Aucune</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>Aucune</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>
						</xsl:element>
						<!-- 2 -->
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:attribute name="text-align">start</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:text>Assurance vie</xsl:text>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<!-- #DG148 -->
								<xsl:choose>
									<xsl:when test="./lifeStatusId =1">
										<xsl:value-of select="./lifePercentCoverageReq"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>0,00%</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<!-- #DG148 end -->
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:text>Assurance invalidit�</xsl:text>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:choose>
									<xsl:when test="./disabilityStatusId=1">
										<!-- #DG218 xsl:choose>
											<xsl:when test="./insuranceProportionsId=1">
												<xsl:text>150,00%</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>100,00%</xsl:text>
											</xsl:otherwise>
										</xsl:choose-->
										<xsl:value-of select="disPercentCoverageReq"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>0,00%</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>
						</xsl:element>
						<!--    end of cell -->
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>

  <!--#DG466 -->
	<xsl:template name="coAppHead">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">9pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:if test="position()=1">
				<xsl:text>Demandeur principal</xsl:text>
			</xsl:if>
			<xsl:if test="position()=2">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 1</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">er </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="position()=3">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 2</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ieme </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="position()=4">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 3</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ieme </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="position()=5">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 4</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ieme </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="position()=6">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 5</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ieme </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="position()=7">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 6</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ieme </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="position()=8">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 7</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ieme </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="position()=9">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 8</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ieme </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="position()=10">
				<xsl:choose>
					<xsl:when test="./borrowerTypeId=1">
						<xsl:text>Endosseur</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<fo:inline font-weight="bold"> 9</fo:inline>
						<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ieme </fo:inline>
						<fo:inline font-weight="bold">co-demandeur</fo:inline>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="createInsuranceRowsB">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">45mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">40mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
            <xsl:attribute name="hyphenate">false</xsl:attribute>
							Protection propri�taire
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute>
							<xsl:text>Option Multiprojets</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			
		  <!--#DG466 rewritten -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
				    <xsl:call-template name="ProtectionCheckBox"/>
					</xsl:element>
					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
						<xsl:call-template name="MultiprojectOptionsCheckBox"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="InsuranceIITableTypeOne">
	  <!--#DG466 rewritten --> 
		<xsl:element name="fo:block" use-attribute-sets="TitleA"><!--#DG466 -->
      <xsl:attribute name="break-before">page</xsl:attribute>
			<xsl:attribute name="space-after.optimum">7mm</xsl:attribute>
			<fo:inline font-weight="bold">Assurance pr�t et Protection propri�taire</fo:inline>
		</xsl:element>
    <xsl:call-template name="solidLine"/>	
	
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">7mm</xsl:attribute><!--#DG466 -->
      <!--#DG466 rewritten to fit 3 cols -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">80mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">05mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">85mm</xsl:attribute>
			</xsl:element>
			<!--xsl:call-template name="InsuranceIITableHeaderTypeOne"/-->
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<!-- #DG466  xsl:copy use-attribute-sets="TableCellAttributes"/-->
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<!-- <xsl:element name="fo:block"><xsl:copy use-attribute-sets="TableBlockAttributes"/> -->
			      <!--#DG466 insert a table with 2 cols -->
						<xsl:call-template name="createInsuranceRows"/>
						<!-- </xsl:element> -->
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<!-- #DG466  xsl:copy use-attribute-sets="TableBlockAttributes"/-->
							<!-- space b/w tables -->
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
		      <!--#DG466 insert a table with 2 cols -->
					<xsl:element name="fo:table-cell">
						<!-- #DG466  xsl:copy use-attribute-sets="TableCellAttributes"/-->
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
					  <xsl:call-template name="createInsuranceRowsB"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="MultiprojectOptionsCheckBox">
		<xsl:choose>
			<xsl:when test="/*/Deal/multiProject='N'">
				<xsl:element name="fo:block">
					<xsl:call-template name="CheckedCheckbox"/>
					<xsl:text> non </xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:call-template name="UnCheckedCheckbox"/>
					<xsl:text> oui - nouveau pr�t</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:call-template name="UnCheckedCheckbox"/>
					<xsl:text> oui - refinancement</xsl:text>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="/*/Deal/dealPurposeId='4' or //Deal/dealPurposeId='5' or //Deal/dealPurposeId='12' or //Deal/dealPurposeId='14' ">
						<xsl:element name="fo:block">
							<xsl:call-template name="UnCheckedCheckbox"/>
							<xsl:text> non </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:call-template name="UnCheckedCheckbox"/>
							<xsl:text> oui - nouveau pr�t</xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:call-template name="CheckedCheckbox"/>
							<xsl:text> oui - refinancement</xsl:text>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="fo:block">
							<xsl:call-template name="UnCheckedCheckbox"/>
							<xsl:text> non </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:call-template name="CheckedCheckbox"/>
							<xsl:text> oui - nouveau pr�t</xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:call-template name="UnCheckedCheckbox"/>
							<xsl:text> oui - refinancement</xsl:text>
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- returns cell with 2 lines -->
	<xsl:template name="ProtectionCheckBox">
		<xsl:choose>
			<xsl:when test="/*/Deal/proprietairePlus='Y'">
				<xsl:element name="fo:block">
					<xsl:call-template name="UnCheckedCheckbox"/>
					<xsl:text>  non </xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:call-template name="CheckedCheckbox"/>
					<xsl:text> oui </xsl:text>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:block">
					<xsl:call-template name="CheckedCheckbox"/>
					<xsl:text>  non </xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:call-template name="UnCheckedCheckbox"/>
					<xsl:text> oui </xsl:text>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
    <!--#DG466 now, in the same cell -->			
		<xsl:element name="fo:block">
		  <xsl:attribute name="space-before.optimum">10mm</xsl:attribute> 
	    <xsl:text>&#160;</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			Limite de Cr�dit
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="/*/Deal/proprietairePlusLOC"/>
		</xsl:element>
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="InsuranceTableTypeSeven">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute><!--#DG466 -->
			<xsl:call-template name="TableOf3sColumns"/>
			<xsl:call-template name="InsuranceTableHeaderTypeSeven"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/Deal/MIPolicyNumber"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/MIStatus"/>
							</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/SpecialFeature"/>
							<xsl:text> &#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceTableHeaderTypeSeven">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">5px</xsl:attribute>
			<xsl:call-template name="TableOf3sColumns"/>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								No de r�f�rence de l'assurance
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								Statut de l'assurance
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
							    Traitement sp�cial
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="InsuranceTableTypeSix">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute><!--#DG466 -->
			<xsl:call-template name="TableOf3Columns"/>

			<!--#DG466 rewritten -->
			<xsl:call-template name="InsuranceTableHeaderTypeSix"/>
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed">
				<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
				<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
				<xsl:variable name="t" select="/*/specialRequirementTags/DownPayments/DownPayment"/>
        <xsl:if test="not($t)">
  				<xsl:element name="fo:table-row">
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
  						<xsl:element name="fo:block">
				        <xsl:text> &#160;</xsl:text>
  						</xsl:element>
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
 						   <fo:block/><!--#DG578 guarantee 1 item minimum -->  
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
 						   <fo:block/><!--#DG578 guarantee 1 item minimum -->  
  					</xsl:element>
  				</xsl:element>
        </xsl:if>
				<xsl:for-each select="$t">
  				<xsl:element name="fo:table-row">
            <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
  						<xsl:element name="fo:block">
  							<xsl:value-of select="downPaymentSourceType"/>
  						</xsl:element>
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
  						<xsl:element name="fo:block">
  							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
  							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
  							<xsl:value-of select="dPSDescription"/> 
  							</xsl:element>
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
  						<xsl:element name="fo:block">
  							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
  							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
  							<xsl:value-of select="amount"/>
  						</xsl:element>
  					</xsl:element>
  				</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceTableHeaderTypeSix">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">5px</xsl:attribute>
			<xsl:call-template name="TableOf3Columns"/>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Source de la mise de fonds
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								Description de la mise de fonds
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
							    Montant de la mise de fonds
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="InsuranceTableTypeFive">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute><!--#DG466 -->
			<xsl:call-template name="TableOf4Columns"/>
			<xsl:call-template name="InsuranceTableHeaderTypeFive"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/Deal/paymentFrequency"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/Deal/actualPaymentTerm"/> mois
							</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<!-- #DG466  xsl:attribute name="text-align">start</xsl:attribute-->
		        <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/Deal/escrowPayment/escrowPaymentAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<!-- #DG466  xsl:attribute name="text-align">start</xsl:attribute-->
		        <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/Deal/PandiPaymentAmount"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceTableHeaderTypeFive">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">5px</xsl:attribute>
			<xsl:call-template name="TableOf4Columns"/>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Fr�quence
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								Terme
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
							    Taxes selon la fr�quence
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
							    Versement C&amp;I 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="InsuranceTableTypeFour">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">7mm</xsl:attribute><!--#DG466 -->
			<xsl:call-template name="TableOf4Columns"/>
			<xsl:call-template name="InsuranceTableHeaderTypeFour"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
					<!--#DG466 xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TableBlockAttributes"/>							
							Voir conditions <!- - <xsl:value-of select="/*/Deal/rateGuarantyPeriod"></xsl:value-of> - ->
						</xsl:element>
					</xsl:element-->
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/Deal/estimatedClosingDate"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<!-- #DG200 xsl:value-of select="/*/Deal/firstPaymentDateMonthly"/-->
							<xsl:value-of select="/*/specialRequirementTags/firstPaymentDate"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="/*/Deal/paymentFrequencyId='0'">
									<!-- monthly -->
									<xsl:text>� tous les mois suivant</xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/paymentFrequencyId='2'">
									<!-- Bi-weekly -->
									<xsl:text>� tous les 14 jours suivant</xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/paymentFrequencyId='4'">
									<!-- Weekly -->
									<xsl:text>� tous les 7 jours suivant</xsl:text>
								</xsl:when>
								<xsl:otherwise><!--#DG466 -->
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="/*/Deal/paymentFrequencyId='0'">
									<!-- monthly -->
									<xsl:text>mois </xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/paymentFrequencyId='2'">
									<!-- Bi-weekly -->
									<xsl:text>deux semaines</xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/paymentFrequencyId='4'">
									<!-- Weekly -->
									<xsl:text>semaine </xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceTableHeaderTypeFour">
	  <!--#DG466 use table-headers 
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">5px</xsl:attribute>
			<xsl:call-template name="TableOf4Columns"/>
			<xsl:element name="fo:table-body"-->
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							Date pr�vue du d�bours�
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
							Date estim�e du 1er remboursement
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
							Jours des prochains remboursements
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							de chaque
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="InsuranceTableTypeThree">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">9mm</xsl:attribute><!--#DG466 -->
			<xsl:call-template name="TableOf6Columns"/>
			<xsl:call-template name="InsuranceTableHeaderTypeThree"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/Deal/amortizationTerm"/> mois
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<!--#DG168 xsl:value-of select="/*/Deal/interestType"/-->
							<xsl:value-of select="/*/Deal/MtgProd/mtgProdName"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="/*/Deal/lienPositionId='0'">1er</xsl:when>
								<xsl:when test="/*/Deal/lienPositionId='1'">2i�me</xsl:when>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
			        <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							<xsl:value-of select="/*/Deal/netInterestRate"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
			        <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							<xsl:value-of select="/*/Deal/premium"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<!--#DG466 Voir  conditions-->
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceTableHeaderTypeThree">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							Dur�e
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							<!--#DG168 Taux-->Produit
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							 Rang
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							Taux annuel
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
							Pct de majoration
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							Taux plafond
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="InsuranceTableTypeTwo">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute><!--#DG466 -->
			<xsl:call-template name="TableOf6Columns"/>
			<xsl:call-template name="InsuranceTableHeaderTypeTwo"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
			        <xsl:attribute name="text-align">right</xsl:attribute>
							<!-- #DG578 -->
            	<xsl:choose>
            		<xsl:when test="/*/Deal/dealPurposeId = 17">
 							    <xsl:value-of select="/*/specialRequirementTags/asImprovedAmount"/>
            		</xsl:when>
            		<xsl:otherwise>
                  <xsl:value-of select="/*/Deal/totalPurchasePrice"/>
            		</xsl:otherwise>
            	</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
			        <xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/downPaymentAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
			        <xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/netLoanAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
			        <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							<xsl:choose>
								<xsl:when test="/*/Deal/mortgageInsurerId=0">
									<xsl:text>&#160;</xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/mortgageInsurerId=1">
									<xsl:value-of select="/*/Deal/MIPremiumAmount"/>
								</xsl:when>
								<xsl:when test="/*/Deal/mortgageInsurerId=2">
									<xsl:value-of select="/*/Deal/MIPremiumAmount"/>
								</xsl:when>
								<xsl:otherwise><!--#DG466 -->
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
			        <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							<xsl:value-of select="/*/Deal/totalLoanAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableCellAttributes"/>
						<xsl:element name="fo:block">
			        <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							<xsl:value-of select="/*/specialRequirementTags/CombinedLTV"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceTableHeaderTypeTwo">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
							Montant du projet
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
							Mise de fonds
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
							 Montant financement requis
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
							<xsl:choose>
								<xsl:when test="/*/Deal/mortgageInsurerId=0">
									<xsl:text>&#160;</xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/mortgageInsurerId=1">
									<xsl:text>Prime SCHL</xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/mortgageInsurerId=2">
									<xsl:text>Prime Genworth Financial Canada</xsl:text>
								</xsl:when>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
							Montant emprunt
 						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="TableHeaderCellAttributes"/>
						<xsl:element name="fo:block">
							RPV
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="InsuranceTableTypeOne">
	  <!--#DG466 moved from header -->
		<xsl:element name="fo:block" use-attribute-sets="TitleA"><!--#DG466 -->
      <xsl:attribute name="break-before">page</xsl:attribute>
			<xsl:attribute name="space-after.optimum">7mm</xsl:attribute>
			<fo:inline font-weight="bold">Donn�es sur le pr�t</fo:inline>
		</xsl:element>
    <xsl:call-template name="solidLine"/>	
	
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">7mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="InsuranceTableHeaderTypeOne"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/Deal/dealPurpose"/>
		          <xsl:text>&#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="InsuranceTableHeaderTypeOne">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute> 
								Nature du projet
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!--#DG466 /xsl:element-->
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="DetailsTableTypeFive">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">65mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">35mm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="DetailsTableHeaderTypeFive"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/Property/occupancyType"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/Property/structureAge"/>
		          <xsl:text>&#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="DetailsTableHeaderTypeFive">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
								Occupation de la propri�t�
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
								�ge de la structure
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!--#DG466 /xsl:element-->
	</xsl:template>

	<xsl:template name="DetailsTableTypeFour">
	   <!--#DG466 moved from header -->
		<xsl:element name="fo:block" use-attribute-sets="TitleB"><!--#DG466 -->
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute><!--#DG466 -->
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
				D�signation
		</xsl:element>
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">4mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">65mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">45mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">40mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">20mm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="DetailsTableHeaderTypeFour"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="white-space-collapse">false</xsl:attribute>
							<xsl:value-of select="/*/DealAdditional/Property/propertyStreetNumber"/>
							<xsl:text>, </xsl:text>
							<xsl:value-of select="/*/DealAdditional/Property/streetType"/>
							<!--#DG196-->
							<xsl:text> </xsl:text>
							<xsl:value-of select="/*/DealAdditional/Property/propertyStreetName"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="/*/DealAdditional/Property/streetDirection"/>
							<!--#DG196-->
							<xsl:if test="/*/DealAdditional/Property/unitNumber">
								<!--#DG196-->
								<xsl:text>, unit� </xsl:text>
								<!--#DG214-->
								<xsl:value-of select="/*/DealAdditional/Property/unitNumber"/>
							</xsl:if>
							<xsl:if test="/*/DealAdditional/Property/propertyAddressLine2">
								<!--#DG196-->
								<xsl:text> &#xA;</xsl:text>
								<xsl:value-of select="/*/DealAdditional/Property/propertyAddressLine2"/>
							</xsl:if>
							<xsl:if test="/*/DealAdditional/Property/legalLine1">
								<xsl:text> &#xA;</xsl:text>
								<xsl:value-of select="/*/DealAdditional/Property/legalLine1"/>
							</xsl:if>
							<xsl:if test="/*/DealAdditional/Property/legalLine2">
								<xsl:text> &#xA;</xsl:text>
								<xsl:value-of select="/*/DealAdditional/Property/legalLine2"/>
							</xsl:if>
							<xsl:if test="/*/DealAdditional/Property/legalLine3">
								<xsl:text> &#xA;</xsl:text>
								<xsl:value-of select="/*/DealAdditional/Property/legalLine3"/>
							</xsl:if>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/Property/propertyCity"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/Property/province"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/Property/propertyPostalFSA"/>
							<xsl:text>&#160; </xsl:text>
							<xsl:value-of select="/*/DealAdditional/Property/propertyPostalLDU"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="DetailsTableHeaderTypeFour">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
								Adresse et num�ro de cadastre
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
								Ville
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
								 Province
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:element name="fo:block">
								Code postal
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!--#DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="DetailsTableTypeThree">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="DetailsTableHeaderTypeThree"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/Property/newConstruction"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/Property/dwellingType"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
		          <xsl:text>&#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="DetailsTableHeaderTypeThree">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								Type immeuble
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								Description de l'immeuble
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!--#DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="DetailsTableTypeTwo">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
		  <xsl:call-template name="DetailsTableHeaderTypeTwo"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/DealAdditional/originationBranch/partyProfile/ptPBusinessId"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<!-- <xsl:value-of select="/*/Deal/branchProfile/branchName"></xsl:value-of> -->
							<xsl:value-of select="/*/DealAdditional/originationBranch/partyProfile/partyCompanyName"/>
							<!-- Roger, Sept 2 -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:text> &#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/Deal/commisionCode"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="DetailsTableHeaderTypeTwo">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								No de transit
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								Caisse
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								Code de commissionnement
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="DetailsTableTypeOne">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">7mm</xsl:attribute><!--#DG466 -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="DetailsTableHeaderTypeOne"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/Deal/dealId"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:choose>
								<xsl:when test="/*/Deal/mortgageInsurerId=0">
									<xsl:text>Conventionnel</xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/mortgageInsurerId=1">
									<xsl:text>SCHL</xsl:text>
								</xsl:when>
								<xsl:when test="/*/Deal/mortgageInsurerId=2">
									<xsl:choose>
										<xsl:when test="/*/Deal/MITypeId=5">
											<xsl:text>Genworth Financial Canada 50/50</xsl:text>
										</xsl:when>
										<xsl:when test="/*/Deal/MITypeId=6">
											<xsl:text>Genworth Financial Canada 50/50</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>Genworth Financial Canada 100</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		          <xsl:text>&#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/Deal/applicationDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="DetailsTableHeaderTypeOne">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								No de demande
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<!-- <xsl:attribute name="border">solid black 1px</xsl:attribute> -->
						<xsl:element name="fo:block">
								Type de pr�t
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								Date de la demande
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!--#DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="SummaryTableHeaderTypeOne">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Revenu total annuel
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Total du passif
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Total de l'actif
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Valeur nette totale
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="SummaryTableTypeOne">
		<xsl:element name="fo:table">
			<xsl:attribute name="space-before.optimum">7mm</xsl:attribute><!--#DG466 --> 
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4.25cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4.25cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4.25cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4.25cm</xsl:attribute>
			</xsl:element>
			<xsl:call-template name="SummaryTableHeaderTypeOne"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<!--#DG466 xsl:attribute name="text-align">start</xsl:attribute-->
			      <xsl:attribute name="text-align">right</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		          <xsl:text>&#160;</xsl:text><!--#DG466 -->
							<xsl:value-of select="/*/specialRequirementTags/totalSumIncome"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<!--#DG466 xsl:attribute name="text-align">start</xsl:attribute-->
			      <xsl:attribute name="text-align">right</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/totalSumLiability"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<!--#DG466 xsl:attribute name="text-align">start</xsl:attribute-->
			      <xsl:attribute name="text-align">right</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/totalSumAsset"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<!--#DG466 xsl:attribute name="text-align">start</xsl:attribute-->
			      <xsl:attribute name="text-align">right</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="/*/specialRequirementTags/totalNetWorth"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!--#DG466 -->
	<xsl:template name="coAppHeader">
		<xsl:choose>
			<xsl:when test="./borrowerTypeId=1">
				<xsl:text>Endosseur</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="ordina">
					<xsl:choose>
		        <xsl:when test="position() = 2">
							<xsl:text>er </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>ieme </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<fo:inline font-weight="bold">
				  <xsl:text> </xsl:text>
		      <xsl:value-of select="position()-1"/>
				  <fo:inline font-size="8" baseline-shift="super">
		         <xsl:value-of select="$ordina"/>
          </fo:inline>
				  co-demandeur
        </fo:inline>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!--#DG466 rewritten to use table-headers -->
	<xsl:template name="listBorrower">
		<xsl:call-template name="BorrowerTableTypeName"/>
		<xsl:call-template name="BorrowerTableTypeTele"/>
		<xsl:call-template name="BorrowerTableTypeLang"/>
		<xsl:call-template name="BorrowerTableTypeAsset"/>
		<xsl:call-template name="BorrowerTableTypeLiab"/>
		<xsl:call-template name="BorrowerTableTypeRefer"/>
		<xsl:call-template name="BorrowerTableTypeIncom"/>
		<xsl:call-template name="BorrowerTableTypeOthInc"/>
		<xsl:call-template name="BorrowerTableTypeOther"/>
	</xsl:template>
	
	<!--#DG466 rewritten --> 
	<xsl:template name="BorrowerTableTypeOther">
		<xsl:element name="fo:block" use-attribute-sets="TitleB"><!--#DG466 -->
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute><!--#DG466 --> 
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<fo:inline font-weight="bold">Autres informations</fo:inline>
		</xsl:element>
		<xsl:call-template name="BorrowerTableTypeBankrpt"/>
	</xsl:template>
	
	<xsl:template name="BorrowerTableHeaderTypeNineSub">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Statut de faillite
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Actuellement caution?
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="BorrowerTableTypeBankrpt">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 --> 
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
  		<xsl:call-template name="BorrowerTableHeaderTypeNineSub"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="bankruptcyStatus"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="guarantorOtherLoans"/>
		          <xsl:text>&#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="BorrowerTableHeaderTypeEight">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Autres revenus
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Description des autres revenus
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Revenu annuel
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="BorrowerTableTypeOthInc">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 --> 
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4.9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.1cm</xsl:attribute>
			</xsl:element>
  		<xsl:call-template name="BorrowerTableHeaderTypeEight"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
			  <!--#DG466 list only the ones not listed yet, if empty add blank line -->
				<xsl:variable name="eh" select="./EmploymentHistory/Income"/>
				<xsl:variable name="othIncIds">
          <xsl:for-each select="Income">
  				  <xsl:variable name="incId" select="incomeId"/>
            <xsl:if test="not($eh[incomeId = $incId])">
							 <xsl:value-of select="$incId"/>
				        <xsl:text>|</xsl:text>
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>
				<xsl:variable name="t" select="./Income[contains($othIncIds,concat(incomeId,'|'))]"/><!--#DG466 exclude employment income-->
        <xsl:if test="not($t)">
  				<xsl:element name="fo:table-row">
            <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
  						<xsl:element name="fo:block">
				        <xsl:text> &#160;</xsl:text>
  						</xsl:element>
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
 						   <fo:block/><!--#DG578 guarantee 1 item minimum -->  
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
 						   <fo:block/><!--#DG578 guarantee 1 item minimum -->  
  					</xsl:element>
  				</xsl:element>
        </xsl:if>
				<xsl:for-each select="$t">
					<xsl:element name="fo:table-row">
            <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="incomeType"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="incomeDescription"/>
				        <xsl:text> &#160;</xsl:text>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
			        <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="incomeAmount"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="BorrowerTableHeaderTypeSeven">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Nom de l'employeur
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Statut
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Titre/Poste
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Depuis combien de temps
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Revenu annuel
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="BorrowerTableTypeIncom">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">4mm</xsl:attribute><!--#DG466 --> 
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5.2cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.1cm</xsl:attribute>
			</xsl:element>
  		<xsl:call-template name="BorrowerTableHeaderTypeSeven"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
			  <!--#DG466 if empty add blank line -->
				<xsl:variable name="t" select="./EmploymentHistory"/>
        <xsl:if test="not($t)">
  				<xsl:element name="fo:table-row">
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
  						<xsl:element name="fo:block">
				        <xsl:text> &#160;</xsl:text>
  						</xsl:element>
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
 						   <fo:block/><!--#DG578 guarantee 1 item minimum -->  
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
 						   <fo:block/><!--#DG578 guarantee 1 item minimum -->  
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
 						   <fo:block/><!--#DG578 guarantee 1 item minimum -->  
  					</xsl:element>
  					<xsl:element name="fo:table-cell" use-attribute-sets="TableCellAttributes">
 						   <fo:block/><!--#DG578 guarantee 1 item minimum -->  
  					</xsl:element>
  				</xsl:element>
        </xsl:if>
				<xsl:for-each select="$t">
					<xsl:element name="fo:table-row">
            <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="employerName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="employmentHistoryStatus"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="jobTitle"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
							<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="timeAtJob"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">0mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							<xsl:attribute name="padding-right">0mm</xsl:attribute>
							<xsl:attribute name="padding-left">1mm</xsl:attribute>
  						<!--#DG466 xsl:attribute name="text-align">left</xsl:attribute-->
  			      <xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:attribute name="border">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:value-of select="Income/incomeAmount"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="BorrowerTableHeaderTypeSix">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Type de r�f�rence  de cr�dit
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Description de la r�f�rence
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Nom de l'institution
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								No. de compte
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Solde
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="BorrowerTableTypeRefer">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 --> 
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
  		<xsl:call-template name="BorrowerTableHeaderTypeSix"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
          <xsl:choose>
             <xsl:when test="./CreditReference">
				       <xsl:for-each select="./CreditReference">
					       <xsl:element name="fo:table-row">
                   <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							 	       <xsl:value-of select="creditRefType"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="creditReferenceDescription"/>
							       </xsl:element>
						        </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="institutionName"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="accountNumber"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="currentBalance"/>
							       </xsl:element>
						       </xsl:element>
					       </xsl:element>
				       </xsl:for-each>
                   </xsl:when>
                   <xsl:otherwise>
 					    <xsl:element name="fo:table-row">
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
					    </xsl:element>
                   </xsl:otherwise>
               </xsl:choose>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="BorrowerTableHeaderTypeFive">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Type de passif
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Description du passif
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Montant du passif
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Paiement du passif
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Pay�
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:attribute name="wrap-option">no-wrap</xsl:attribute><!--#DG466 -->
						<xsl:element name="fo:block">
								% en ABD/ATD
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="BorrowerTableTypeLiab">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 --> 
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">26mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">44mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">20mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">21mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">27mm</xsl:attribute><!--#DG466 -->
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15mm</xsl:attribute>
			</xsl:element>
  		<xsl:call-template name="BorrowerTableHeaderTypeFive"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
          <xsl:choose>
             <xsl:when test="./Liability">
 				       <xsl:for-each select="./Liability">
 					       <xsl:element name="fo:table-row">
		               <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="liabilityType"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="liabilityDescription"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="liabilityAmount"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="liabilityMonthlyPayment"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							           <xsl:choose>
								           <xsl:when test="liabilityPayoffTypeId=1">
									           <xsl:text>Sera pay� et ferm� avant le d�bours</xsl:text>
								           </xsl:when>
								           <xsl:otherwise>
								           	   <xsl:value-of select="liabilityPayoffType"/>
                                           </xsl:otherwise>
							           </xsl:choose>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="percentIncludedInGDS"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">right</xsl:attribute><!--#DG466 -->
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="percentIncludedInTDS"/>
							       </xsl:element>
						       </xsl:element>
					       </xsl:element>
				       </xsl:for-each>
             </xsl:when>
             <xsl:otherwise>
 					    <xsl:element name="fo:table-row">
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
					    </xsl:element>
                   </xsl:otherwise>
               </xsl:choose>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="BorrowerTableHeaderTypeFour">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Type d'actif
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Description de l'actif
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Valeur de l'actif
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="BorrowerTableTypeAsset">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 --> 
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4cm</xsl:attribute>
			</xsl:element>
  		<xsl:call-template name="BorrowerTableHeaderTypeFour"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
          <xsl:choose>
             <xsl:when test="./Asset">
				       <xsl:for-each select="./Asset">
					       <xsl:element name="fo:table-row">
                   <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="assetType"/>
							       </xsl:element>
						       </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <xsl:attribute name="text-align">left</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="assetDescription"/>
							        </xsl:element>
						        </xsl:element>
						       <xsl:element name="fo:table-cell">
							       <xsl:attribute name="padding-top">0mm</xsl:attribute>
							       <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							       <xsl:attribute name="padding-right">0mm</xsl:attribute>
							       <xsl:attribute name="padding-left">1mm</xsl:attribute>
							       <!--#DG466 xsl:attribute name="text-align">left</xsl:attribute-->
        			       <xsl:attribute name="text-align">right</xsl:attribute>
							       <xsl:attribute name="border">solid black 1px</xsl:attribute>
							       <xsl:element name="fo:block">
								       <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								       <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								       <xsl:value-of select="assetValue"/>
							       </xsl:element>
						       </xsl:element>
					       </xsl:element>
				       </xsl:for-each>
             </xsl:when>
             <xsl:otherwise>
 					    <xsl:element name="fo:table-row">
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
						    <xsl:element name="fo:table-cell">
							    <xsl:attribute name="padding-top">0mm</xsl:attribute>
							    <xsl:attribute name="padding-bottom">0mm</xsl:attribute>
							    <xsl:attribute name="padding-right">0mm</xsl:attribute>
							    <xsl:attribute name="padding-left">1mm</xsl:attribute>
							    <xsl:attribute name="text-align">left</xsl:attribute>
							    <xsl:attribute name="border">solid black 1px</xsl:attribute>
							    <xsl:element name="fo:block">
								    <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								    <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								    <xsl:text>&#160;</xsl:text>
							    </xsl:element>
						    </xsl:element>
					    </xsl:element>
                   </xsl:otherwise>
               </xsl:choose>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="BorrowerTableTypeLang">
		<xsl:element name="fo:table">
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 --> 
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
  		<xsl:call-template name="BorrowerTableHeaderTypeThree"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="./languagePreference"/>
		          <xsl:text>&#160;</xsl:text><!--#DG466 -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="./maritalStatus"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="./numberOfDependents"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<!--#DG466 xsl:attribute name="text-align">start</xsl:attribute-->
			      <xsl:attribute name="text-align">right</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="./totalIncomeAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<!--#DG466 xsl:attribute name="text-align">start</xsl:attribute-->
			      <xsl:attribute name="text-align">right</xsl:attribute>
						<xsl:attribute name="display-align">before</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<!--#DG466 xsl:value-of select="/*/totalNetWorth"/-->
							<xsl:value-of select="netWorth"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="BorrowerTableHeaderTypeThree">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
								Langue de correspondance
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								�tat civil
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Personnes � charge
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Revenu annuel
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Valeur nette
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!-- #DG466 /xsl:element-->
	</xsl:template>
	
	<xsl:template name="BorrowerTableTypeTele">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">6mm</xsl:attribute><!--#DG466 --> 
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3.4cm</xsl:attribute>
			</xsl:element>
  		<xsl:call-template name="BorrowerTableHeaderTypeTwo"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>R�s </xsl:text>
							<xsl:value-of select="./borrowerHomePhoneNumber"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:text>Bur </xsl:text>
							<xsl:value-of select="./borrowerWorkPhoneNumber"/>
							<xsl:if test="./borrowerWorkPhoneExtension">
									poste: <xsl:value-of select="./borrowerWorkPhoneExtension"/>
							</xsl:if>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="./socialInsuranceNumber"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="./borrowerBirthDate"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="./age"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:choose>
								<xsl:when test="./borrowerGenderId=1">
									<xsl:text>Masculin</xsl:text>
								</xsl:when>
								<xsl:when test="./borrowerGenderId=2">
									<xsl:text>F�minin</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text> </xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="BorrowerTableHeaderTypeTwo">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								T�l�phone
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
              <xsl:attribute name="hyphenate">false</xsl:attribute><!--#DG466 -->
								# Assurance sociale
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Date de naissance
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Age
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Sexe
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!--#DG466 /xsl:element-->
	</xsl:template>

	<xsl:template name="BorrowerTableHeaderTypeName">
			<xsl:element name="fo:table-header" use-attribute-sets="TitleC"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
								Nom et pr�nom
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="display-align">after</xsl:attribute>
						<xsl:element name="fo:block">
								Adresse actuelle
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		<!--#DG466 /xsl:element-->
	</xsl:template>

	<xsl:template name="BorrowerTableTypeName">
		<xsl:element name="fo:table">
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before.optimum">5.5mm</xsl:attribute><!--#DG466 --> 
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
		  <xsl:call-template name="BorrowerTableHeaderTypeName"/><!--#DG466 -->
			<xsl:element name="fo:table-body" use-attribute-sets="TextBoxed"><!--#DG466 -->
				<xsl:element name="fo:table-row">
          <xsl:attribute name="keep-together">always</xsl:attribute><!--#DG466 -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<!-- <xsl:attribute name="display-align">after</xsl:attribute> -->
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="./borrowerLastName"/>
							<xsl:text>&#160;</xsl:text>
							<xsl:value-of select="./borrowerFirstName"/>
							<xsl:text>&#160;</xsl:text>
							<xsl:value-of select="./borrowerMiddleInitial"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:for-each select="./BorrowerAddress">
								<xsl:if test="position()=1">
						      <xsl:element name="fo:block">
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:value-of select="./Address/addressLine1"/>
									</xsl:element>
									<xsl:if test="Address/addressLine2">
										<!--#DG198-->
						        <xsl:element name="fo:block">
											<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:value-of select="Address/addressLine2"/>
										</xsl:element>
									</xsl:if>
					        <xsl:element name="fo:block">
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:value-of select="./Address/city"/>,&#160;<xsl:value-of select="./Address/province"/>
									</xsl:element>
									<xsl:element name="fo:block">
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:value-of select="./Address/postalFSA"/>&#160;<xsl:value-of select="./Address/postalLDU"/>
									</xsl:element>
								</xsl:if>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	
		    
	<!--Xue Bin Zhao 20070308, added for DJ Credit Scoring Delivery 2, START-->
    <xsl:template name="CreateCSResponseBlock1">
   		<xsl:element name="fo:block">
   		<xsl:attribute name="font-size">12pt</xsl:attribute>   		
   		<xsl:attribute name="space-before.optimum">12pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">12pt</xsl:attribute> 
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<fo:inline font-weight="bold"><xsl:text>�valuation SCOR - R�sultat d'analyse</xsl:text></fo:inline>	
		</xsl:element>
					    
		<xsl:element name="fo:block">
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
		<xsl:attribute name="keep-together">always</xsl:attribute>					
		<fo:inline font-weight="bold"><xsl:text>Donn�es du r�sultat</xsl:text></fo:inline>
		</xsl:element>
						        
        <xsl:element name="fo:table">
        <xsl:copy use-attribute-sets="TableLeftFixed"/>
        <xsl:attribute name="font-size">8pt</xsl:attribute>
        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
         <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
        	<xsl:attribute name="text-align">left</xsl:attribute>
        	<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">50mm</xsl:attribute> <!-- NOTE: Apache FOP does not process and support % -->
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">50mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">50mm</xsl:attribute>
			</xsl:element>			
    	 	<xsl:element name="fo:table-body">    				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
	        			<xsl:element name="fo:block">		
						<xsl:text>R�sultat score : </xsl:text>
						<xsl:value-of select="//CreditScoring/AdjudicationResponse/AdjudicationStatusID"/>
						</xsl:element>
				   	</xsl:element>
				   	<xsl:element name="fo:table-cell">
	        			<xsl:element name="fo:block">		
						<xsl:text>Analys� le : </xsl:text>
						<xsl:value-of select="//CreditScoring/AdjudicationResponse/ResponseDate"/>
						</xsl:element>
				   	</xsl:element>
	    		   	<xsl:element name="fo:table-cell">
	        			<xsl:element name="fo:block">		
						<xsl:text>No DFP : </xsl:text>
						<xsl:value-of select="//CreditScoring/AdjudicationResponse/AdjudicationConfirmNo"/>
						</xsl:element>
				   	</xsl:element>
				</xsl:element>														
			</xsl:element>
		</xsl:element>
				
		<xsl:element name="fo:block">
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
		<xsl:attribute name="keep-together">always</xsl:attribute>	  								
		<fo:inline font-weight="bold"><xsl:text>Motifs</xsl:text></fo:inline>	
		</xsl:element>			
     	<xsl:for-each select="//CreditScoring/DJResultMotiveReason/Reason">	
			<xsl:element name="fo:block">
				<xsl:attribute name="font-size">8pt</xsl:attribute>
				<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
		        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 										   
				<xsl:value-of select="."/>							
			</xsl:element>
        </xsl:for-each>
		
		<xsl:element name="fo:block">
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
		<xsl:attribute name="keep-together">always</xsl:attribute>	  								
		<fo:inline font-weight="bold"><xsl:text>Conditions</xsl:text></fo:inline>	
		</xsl:element>
		<xsl:for-each select="//CreditScoring/DJResultMotiveCondition/Condition">							   			
			<xsl:element name="fo:block">
				<xsl:attribute name="font-size">8pt</xsl:attribute>
				<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
		        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 				
				<xsl:value-of select="."/>  							
			</xsl:element>
		</xsl:for-each>

		<xsl:element name="fo:block">
		<xsl:attribute name="font-size">8pt</xsl:attribute>   
		<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
		<xsl:attribute name="keep-together">always</xsl:attribute>	  								
		<fo:inline font-weight="bold"><xsl:text>Pratiques</xsl:text></fo:inline>	
		</xsl:element>
		<xsl:for-each select="//CreditScoring/DJResultMotivePractice/Practice">							   			
			<xsl:element name="fo:block">
				<xsl:attribute name="font-size">8pt</xsl:attribute>
				<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
		        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 				
				<xsl:value-of select="."/>  							
			</xsl:element>
		</xsl:for-each>	
				
        <xsl:element name="fo:block">
		<xsl:attribute name="font-size">8pt</xsl:attribute>   		
		<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
		<xsl:attribute name="keep-together">always</xsl:attribute>					
		<fo:inline font-weight="bold"><xsl:text>Informations compl�mentaires</xsl:text></fo:inline>
		</xsl:element>
        <xsl:element name="fo:table">
        	<xsl:copy use-attribute-sets="TableLeftFixed"/>
            <xsl:attribute name="font-size">8pt</xsl:attribute>
            <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
            <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
            <xsl:attribute name="text-align">left</xsl:attribute>
            <xsl:element name="fo:table-column">
                <xsl:attribute name="column-width">70mm</xsl:attribute>
            </xsl:element>
            <xsl:element name="fo:table-column">
                <xsl:attribute name="column-width">70mm</xsl:attribute>
            </xsl:element>		
            <xsl:element name="fo:table-body">    				
                <xsl:element name="fo:table-row">
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">		
                        <xsl:text>Ratio pr�t/valeur : </xsl:text>
                        <xsl:value-of select="//CreditScoring/DJResultMotive/LTVRatio"/>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">		
                        <xsl:text>Engag. total le + �lev� : </xsl:text>
                        <xsl:value-of select="//CreditScoring/DJResultMotive/HighestTotalCommitment"/>
                        </xsl:element>
                    </xsl:element>
                 </xsl:element>
            </xsl:element>
        </xsl:element>

        <xsl:element name="fo:block">
        <xsl:attribute name="font-size">8pt</xsl:attribute>            
        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
		<xsl:attribute name="keep-together">always</xsl:attribute>					
		<fo:inline font-weight="bold"><xsl:text>Messages d'erreurs</xsl:text></fo:inline>
		</xsl:element>	
		<xsl:for-each select="//CreditScoring/DJResultMotiveAppMessage/ApplicationMessage">							   			
			<xsl:element name="fo:block">
				<xsl:attribute name="font-size">8pt</xsl:attribute>
				<xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
		        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 				
				<xsl:value-of select="."/>  							
			</xsl:element>
		</xsl:for-each>

    </xsl:template>
    <!--Xue Bin Zhao 20070308, added for DJ Credit Scoring Delivery 2, END-->
    
   <!--Xue Bin Zhao 20070315, added for DJ Credit Scoring Delivery 2, START-->
    <xsl:template name="CreateCSResponseBlock2">
       	<xsl:element name="fo:block">
   		<xsl:attribute name="font-size">12pt</xsl:attribute>
   		<xsl:attribute name="space-before.optimum">12pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">12pt</xsl:attribute> 
		<xsl:attribute name="keep-together">always</xsl:attribute>	  								
		<fo:inline font-weight="bold">
		<!--
			Updated for Ticket # FXP16511, 25th  May, 2007
		-->
		<xsl:text>Donn�es additionnelles sur les demandeurs</xsl:text>
		<!--<xsl:text> Donn�es sur les emprunteurs</xsl:text> --></fo:inline> <!--This french means: Data on the borrowers -->	
		</xsl:element>

		<xsl:for-each select="//CreditScoring/DJBorrowerGuarantorInfo">
		<xsl:element name="fo:block">
  		<xsl:attribute name="font-size">8pt</xsl:attribute>  		
  		<xsl:attribute name="space-before.optimum">6pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">6pt</xsl:attribute>  		
        <xsl:element name="fo:table">
        <xsl:copy use-attribute-sets="TableLeftFixed"/>
        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
         <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
            <xsl:attribute name="text-align">left</xsl:attribute>
            <xsl:element name="fo:table-column">
                <xsl:attribute name="column-width">15mm</xsl:attribute>
            </xsl:element>
            <xsl:element name="fo:table-column">
                <xsl:attribute name="column-width">25mm</xsl:attribute>
            </xsl:element>
            <xsl:element name="fo:table-column">
                <xsl:attribute name="column-width">50mm</xsl:attribute>
            </xsl:element>		
            <xsl:element name="fo:table-column">
                <xsl:attribute name="column-width">50mm</xsl:attribute>
            </xsl:element>
            <xsl:element name="fo:table-body">    				
                <xsl:element name="fo:table-row">
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">		                       
                        <xsl:value-of select="./GivenName"/>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">		
                        <xsl:value-of select="./FamilyName"/>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">		
                        <xsl:if test="./IsMainBorrower='T'">
                        <xsl:text>Emprunteur principal</xsl:text>
                        </xsl:if>                     
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">		                       
                        <xsl:text>Niveau de risqu� : </xsl:text>
                        <xsl:value-of select="./RiskLevel"/>                 
                        </xsl:element>
                    </xsl:element>
                </xsl:element>														
            </xsl:element>
        </xsl:element>

		<!-- SECTION : Donn�es personnelles de base AND Bilan, START -->
        <xsl:element name="fo:table">
        <xsl:copy use-attribute-sets="TableLeftFixed"/>
        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:element name="fo:table-column">
        <xsl:attribute name="column-width">70mm</xsl:attribute>
        </xsl:element>
        <xsl:element name="fo:table-column">
        <xsl:attribute name="column-width">100mm</xsl:attribute>
        </xsl:element>
        <xsl:element name="fo:table-body">    				
                <xsl:element name="fo:table-row">
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">
                        <xsl:text>Donn�es personnelles de base</xsl:text>                        
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">
                        <xsl:text>Bilan</xsl:text>                        
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="fo:table-row">
                  <xsl:element name="fo:table-cell">
                    <xsl:element name="fo:table">
                        <xsl:copy use-attribute-sets="TableLeftFixed"/>                        
                        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
                        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
                        <xsl:attribute name="text-align">left</xsl:attribute>
                        <xsl:element name="fo:table-column">
                            <xsl:attribute name="column-width">30mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-column">
                            <xsl:attribute name="column-width">40mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-body">    				
                            <xsl:element name="fo:table-row">
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">
                                        <xsl:text>R�le : </xsl:text>                        
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">                                       
                                        <xsl:value-of select="./Role"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="fo:table-row">
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">
                                        <xsl:text>Emploi : </xsl:text>                        
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">                                       
                                        <xsl:value-of select="./Occupation"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>                           
                             <xsl:element name="fo:table-row">
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">
                                        <xsl:text>ATD personnel : </xsl:text>                        
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">                                       
                                        <xsl:value-of select="./Tds"/>%
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="fo:table-row">
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">
                                        <xsl:text>ABD personnel : </xsl:text>                        
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">                                       
                                        <xsl:value-of select="./Gds"/>%
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="fo:table-row">
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">
                                        <xsl:text>Engagement total : </xsl:text>                        
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="fo:table-cell">
                                    <xsl:element name="fo:block">                                       
                                        <xsl:value-of select="./TotalCommitment"/>$
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                  </xsl:element>
                  <xsl:element name="fo:table-cell">
                      <!-- draw a bordered table for Bilan -->
                      <xsl:element name="fo:table">
                        <xsl:copy use-attribute-sets="TableLeftFixed"/>
                        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
                        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
                        <xsl:attribute name="text-align">left</xsl:attribute>
                        <xsl:attribute name="border-style">solid</xsl:attribute>
                        <xsl:attribute name="border-width">0.1pt</xsl:attribute>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">20mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">20mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">20mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">20mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-body">
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block></fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Caisse ($)</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Ailleurs ($)</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Total ($)</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Actif</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./AssetInCaisse"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./AssetElsewhere"/>
                                    </fo:block>                                   
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./AssetTotalAmount"/>
                                    </fo:block>                                    
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Passif</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./LiabilityInCaisse"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./LiabilityElsewhere"/>
                                    </fo:block>                                   
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./LiabilityTotalAmount"/>
                                    </fo:block>                                    
                                </fo:table-cell>
                            </fo:table-row>                           
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Valeur nette</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NetWorthInCaisse"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NetWorthElsewhere"/>
                                    </fo:block>                                   
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NetWorthTotalAmount"/>
                                    </fo:block>                                    
                                </fo:table-cell>
                            </fo:table-row>                                                          
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>V. nette tangible</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>                                   	
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>                                    	
                                    </fo:block>                                   
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./TangibleNetWorth"/>
                                    </fo:block>                                    
                                </fo:table-cell>
                            </fo:table-row>                                                
                        </xsl:element>

                     </xsl:element>
                  	</xsl:element>                        
                  </xsl:element>
        </xsl:element>														
        </xsl:element>
        <!-- SECTION : Donn�es personnelles de base AND Bilan, END -->
        
        <!-- SECTION : Historique de credit � la caisse AND Ch�ques retourn�s, START --> 
        <xsl:element name="fo:table">
        <xsl:copy use-attribute-sets="TableLeftFixed"/>
        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:element name="fo:table-column">
        <xsl:attribute name="column-width">100mm</xsl:attribute>
        </xsl:element>
        <xsl:element name="fo:table-column">
        <xsl:attribute name="column-width">70mm</xsl:attribute>
        </xsl:element>
        <xsl:element name="fo:table-body">    				
                <xsl:element name="fo:table-row">
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">
                        <xsl:text>Historique de credit � la caisse</xsl:text>                        
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="fo:table-cell">
                        <xsl:element name="fo:block">
                        <xsl:text>Ch�ques retourn�s</xsl:text>                        
                        </xsl:element>
                    </xsl:element>
                </xsl:element>                              
                <xsl:element name="fo:table-row">
                  <xsl:element name="fo:table-cell">
                      <!-- draw a bordered table for Historique de credit � la caisse -->
                        <xsl:element name="fo:table">
                        <xsl:copy use-attribute-sets="TableLeftFixed"/>                        
                        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
                        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
                        <xsl:attribute name="text-align">left</xsl:attribute>
                        <xsl:attribute name="border-style">solid</xsl:attribute>
                        <xsl:attribute name="border-width">0.1pt</xsl:attribute>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">20mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">20mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">50mm</xsl:attribute>
                        </xsl:element>                 
                        <xsl:element name="fo:table-body">
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block></fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Cote actuelle</fo:block>
                                </fo:table-cell>  
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Historique</fo:block>
                                </fo:table-cell>       
                            </fo:table-row>
                            <fo:table-row>                      
                             	<fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>PAT en cours</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">                              
                                	<fo:block>
                                    	<xsl:value-of select="./CurrentRatingForTermLoans"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                	    <xsl:text>Cote depuis ouverture : </xsl:text>
                                    	<xsl:value-of select="./RatingSinceOpeningForTermLoans"/>
                                    </fo:block>                                   
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                            	<fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>MC en cours</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./CurrentRatingForLinesOfCredit"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                		<xsl:text>Cumulatif des retards : </xsl:text>
                                    	<fo:block text-align="right"><xsl:value-of select="./NbDelinquenciesLOC60"/>&#160;&#160;&#160;&#160;31 � 60 jours</fo:block>
                                    	<fo:block text-align="right"><xsl:value-of select="./NbDelinquenciesLOC90"/>&#160;&#160;&#160;&#160;61 � 90 jours</fo:block>
                                    	<fo:block text-align="right"><xsl:value-of select="./NbDelinquenciesLOCPlus90"/>&#160;&#160;&#160;&#160;91 jours ou +</fo:block>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>                                                                                                                                                                             
                        </xsl:element>
                       </xsl:element>                    
                  </xsl:element>                 
                  <xsl:element name="fo:table-cell">
                      <!-- draw a bordered table for Ch�ques retourn�s -->
                      <xsl:element name="fo:table">
                        <xsl:copy use-attribute-sets="TableLeftFixed"/>                        
                        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
                        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
                        <xsl:attribute name="text-align">left</xsl:attribute>
                        <xsl:attribute name="border-style">solid</xsl:attribute>
                        <xsl:attribute name="border-width">0.1pt</xsl:attribute>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">20mm</xsl:attribute>
                        </xsl:element>
                        <xsl:element name="fo:table-column">
                        <xsl:attribute name="column-width">20mm</xsl:attribute>
                        </xsl:element>                 
                        <xsl:element name="fo:table-body">
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Ann�e</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                    <fo:block>Nombre</fo:block>
                                </fo:table-cell>        
                            </fo:table-row>
                            <fo:table-row>                      
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NfsYr1"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NfsQt1"/>
                                    </fo:block>                                   
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NfsYr2"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NfsQt2"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>                           
                            <fo:table-row>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NfsYr3"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell border-style="solid" border-width="0.1pt">
                                	<fo:block>
                                    	<xsl:value-of select="./NfsQt3"/>
                                    </fo:block>                                   
                                </fo:table-cell>                   
                            </fo:table-row>                                                                                                                              
                        </xsl:element>

                     </xsl:element>
                  	</xsl:element>                        
                  </xsl:element>
        </xsl:element>														
        </xsl:element>      
        <!-- SECTION : Historique de credit � la caisse AND Ch�ques retourn�s, END -->

        <!-- SECTION : PDO, START -->
        <xsl:element name="fo:table">
        <xsl:copy use-attribute-sets="TableLeftFixed"/>
        <xsl:attribute name="space-before.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="space-after.optimum">3pt</xsl:attribute> 
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:element name="fo:table-column">
        <xsl:attribute name="column-width">70mm</xsl:attribute>
        </xsl:element>
        <xsl:element name="fo:table-column">
        <xsl:attribute name="column-width">70mm</xsl:attribute>
        </xsl:element>
        <xsl:element name="fo:table-body">    				
                <xsl:element name="fo:table-row">
                    <xsl:element name="fo:table-cell">
        				<xsl:element name="fo:block">
			            <xsl:text>Indicateur de PDO multiple : </xsl:text>
			            <xsl:if test="./IsMultiplePDO='T'">
			            <xsl:text>oui</xsl:text>
			            </xsl:if>
			            <xsl:if test="./IsMultiplePDO='F'">
			            <xsl:text>non</xsl:text>
			            </xsl:if>
			        	</xsl:element>
			       </xsl:element> 
			       <xsl:element name="fo:table-cell">	
			       		<xsl:element name="fo:block">
			            <xsl:text>Indicateur de cr�ation de PDO : </xsl:text>
			            <xsl:if test="./IsPDOCreation='T'">
			            <xsl:text>oui</xsl:text>
			            </xsl:if>
			            <xsl:if test="./IsPDOCreation='F'">
			            <xsl:text>non</xsl:text>
			            </xsl:if>
			       		</xsl:element>
			       </xsl:element>
			    </xsl:element>
		</xsl:element>
		</xsl:element>	       		
        <!-- SECTION : PDO, END -->
	  		  	
		</xsl:element>
	  </xsl:for-each>	 

    </xsl:template>
    <!--Xue Bin Zhao 20070315, added for DJ Credit Scoring Delivery 2, END-->
	<!-- Vandana Start 2 -->
	<xsl:template name="CreateTitleLineReport">
		<xsl:element name="fo:table">
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">185mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<fo:block xsl:use-attribute-sets="enteteAR">
						Equifax Canada Inc. 
						</fo:block>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- Start of Header Section -->
	<xsl:template name="DealInfo">
		<fo:table table-layout="fixed">
			<fo:table-column column-width="28mm"/>
			<fo:table-column column-width="33mm"/>
			<fo:table-column column-width="35mm"/>
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="40mm"/>
			<xsl:element name="fo:table-body">
				<fo:table-row>
					<fo:table-cell padding-top="0.75pt" padding-left="0.75pt" padding-bottom="0.75pt" padding-right="0.75pt" number-columns-spanned="6" display-align="center">
						<fo:block>
							<fo:leader leader-pattern="rule" leader-length="185mm" rule-thickness="0.5pt" rule-style="solid" color="gray"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<!-- ROW 1 -->
				<xsl:element name="fo:table-row">
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Identification</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Emplois</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Jugement</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Pr�t garanti</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Information bancaire</xsl:text>
						</fo:block>
					</fo:table-cell>
				</xsl:element>
				<!-- ROW 2 -->
				<xsl:element name="fo:table-row">
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Pointage</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Recouvrements</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Demandes</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Responsabilit�s</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Services sp�ciaux</xsl:text>
						</fo:block>
					</fo:table-cell>
				</xsl:element>
				<!-- ROW 3 -->
				<xsl:element name="fo:table-row">
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Fraudes</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Faillites et autres</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Saisie-Arr�t</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Op�r. non mbres</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Ch�ques sans provision</xsl:text>
						</fo:block>
					</fo:table-cell>
				</xsl:element>
				<!-- ROW 4 -->
				<xsl:element name="fo:table-row">
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Op�rations</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>D�clarations</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Situation familiale</xsl:text>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregrp">
						<fo:block>
							<xsl:text>Demandes bureaux �trangers</xsl:text>
						</fo:block>
					</fo:table-cell>
				</xsl:element>
				<fo:table-row>
					<fo:table-cell padding-top="0.75pt" padding-left="0.75pt" padding-bottom="0.75pt" padding-right="0.75pt" number-columns-spanned="6" display-align="center">
						<fo:block>
							<fo:leader leader-pattern="rule" leader-length="185mm" rule-thickness="0.5pt" rule-style="solid" color="gray"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:element>
		</fo:table>
	</xsl:template>
	<!--End of Header Section -->

	<!-- Start of Identification Section -->
	<xsl:template name="Identification">
		<xsl:variable name="CNHeader" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNHeader"/>
		<xsl:for-each select="$CNHeader">
			<xsl:variable name="CNHeader.Request" select="Request"/>
			<xsl:variable name="CNHeader.CreditFile" select="CreditFile"/>
			<xsl:variable name="CNHeader.Subject" select="Subject"/>
			<xsl:element name="fo:table">
				<xsl:copy use-attribute-sets="TableLeftFixedCR309"/>
				<fo:table-column column-width="162mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNHeader.Subject">
								<xsl:element name="fo:table">
									<xsl:copy use-attribute-sets="TableLeftFixedCR309"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="12mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="40mm"/>
									<fo:table-column column-width="40mm"/>
									<fo:table-column column-width="23mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="elesubtitre">
												<fo:block >Identification</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
										<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
											<fo:block>
													<xsl:text>Nom</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Pr�nom</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>NAS</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>�tat civil</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Date de naissance</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Date de d�c�s</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Suffixe</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:attribute name="padding-top">1mm</xsl:attribute>
													<xsl:variable name="CNHeader.Subject.SubjectName.LastName" select="SubjectName/LastName"/>
													<xsl:value-of select="$CNHeader.Subject.SubjectName.LastName"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:variable name="CNHeader.Subject.SubjectName.FirstName" select="SubjectName/FirstName"/>
													<xsl:variable name="CNHeader.Subject.SubjectName.MiddleName" select="SubjectName/MiddleName"/>
													<xsl:value-of select="concat($CNHeader.Subject.SubjectName.FirstName, '  ', $CNHeader.Subject.SubjectName.MiddleName)"/>	
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:variable name="CNHeader.Subject.SubjectId.SocialInsuranceNumber" select="SubjectId/SocialInsuranceNumber"/>
													<xsl:value-of select="$CNHeader.Subject.SubjectId.SocialInsuranceNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:variable name="CNHeader.Subject.SubjectId.MaritalStatus.description" select="SubjectId/MaritalStatus/@description"/>
													<xsl:value-of select="$CNHeader.Subject.SubjectId.MaritalStatus.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:variable name="CNHeader.Subject.SubjectId.DateOfBirth" select="SubjectId/DateOfBirth"/>
													<xsl:value-of select="$CNHeader.Subject.SubjectId.DateOfBirth"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:variable name="CNHeader.Subject.SubjectId.DateOfDeath" select="SubjectId/DateOfDeath"/>
													<xsl:value-of select="$CNHeader.Subject.SubjectId.DateOfDeath"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:variable name="CNHeader.Subject.SubjectName.Suffix.description" select="SubjectName/Suffix/@description"/>
													<xsl:value-of select="$CNHeader.Subject.SubjectName.Suffix.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="5">
							<xsl:for-each select="$CNHeader.CreditFile">
								<xsl:variable name="CNHeader.CreditFile.UniqueNumber" select="UniqueNumber"/>
								<xsl:variable name="CNHeader.CreditFile.FileSinceDate" select="FileSinceDate"/>
								<xsl:variable name="CNHeader.CreditFile.DateOfRequest" select="DateOfRequest"/>
								<xsl:variable name="CNHeader.CreditFile.DateOfLastActivity" select="DateOfLastActivity"/>
								<xsl:variable name="CNHeader.CreditFile.DataWarningMessages" select="DataWarningMessages"/>
								<xsl:variable name="CNHeader.CreditFile.CardAlertWarningMessage" select="CardAlertWarningMessage"/>
								<xsl:for-each select="$CNHeader.Request">
									<xsl:element name="fo:table">
										<fo:table-column column-width="28mm"/>
										<fo:table-column column-width="32mm"/>
										<fo:table-column column-width="25mm"/>
										<fo:table-column column-width="55mm"/>
										<fo:table-column column-width="45mm"/>
										<xsl:element name="fo:table-body">
											<xsl:attribute name="start-indent">0pt</xsl:attribute>
											<xsl:element name="fo:table-row">
												<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
													<fo:block>
														<xsl:text>Langue</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
													<fo:block>
														<xsl:text>No unique</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
													<fo:block>
														<xsl:text>Ouverture</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
													<fo:block>
														<xsl:text>Date de la demande</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
													<fo:block>
														<xsl:text>Derni�re activit�</xsl:text>
													</fo:block>
												</fo:table-cell>
											</xsl:element>
											<xsl:element name="fo:table-row">
												<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
															<xsl:variable name="CNHeader.Request.LanguageIndicator" select="LanguageIndicator"/>
															<xsl:value-of select="$CNHeader.Request.LanguageIndicator"/>
															<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="table_values">
													<fo:block>
															<xsl:value-of select="$CNHeader.CreditFile.UniqueNumber"/>
															<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="table_values">
													<fo:block>
															<xsl:value-of select="$CNHeader.CreditFile.FileSinceDate"/>
															<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="table_values">
													<fo:block>
															<xsl:value-of select="$CNHeader.CreditFile.DateOfRequest"/>
															<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="table_values">
													<fo:block>
															<xsl:value-of select="$CNHeader.CreditFile.DateOfLastActivity"/>
															<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:for-each>
								<xsl:for-each select="$CNHeader.CreditFile.DataWarningMessages">
									<xsl:variable name="CNHeader.CreditFile.DataWarningMessages.DataWarningMessage" select="DataWarningMessage"/>
									<xsl:for-each select="$CNHeader.CreditFile.DataWarningMessages.DataWarningMessage">
										<xsl:variable name="CNHeader.CreditFile.DataWarningMessages.DataWarningMessage.code" select="@code"/>
										<xsl:variable name="CNHeader.CreditFile.DataWarningMessages.DataWarningMessage.description" select="@description"/>
										<xsl:element name="fo:table">
											<fo:table-column column-width="35mm"/>
											<fo:table-column column-width="150mm"/>
											<xsl:element name="fo:table-body">
												<xsl:attribute name="start-indent">0pt</xsl:attribute>
												<xsl:element name="fo:table-row">
													<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
														<fo:block>
															<xsl:text>Code</xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
														<fo:block>
															<xsl:text>Messages / Donn�es</xsl:text>
														</fo:block>
													</fo:table-cell>
												</xsl:element>
												<xsl:element name="fo:table-row">
													<fo:table-cell  xsl:use-attribute-sets="table_values">
														<fo:block>
																<xsl:value-of select="$CNHeader.CreditFile.DataWarningMessages.DataWarningMessage.code"/>
																<xsl:text>&#160;</xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell  xsl:use-attribute-sets="table_values">
														<fo:block>
																<xsl:value-of select="$CNHeader.CreditFile.DataWarningMessages.DataWarningMessage.description"/>
																<xsl:text>&#160;</xsl:text>
														</fo:block>
													</fo:table-cell>
												</xsl:element>
											</xsl:element>
										</xsl:element>
									</xsl:for-each>
								</xsl:for-each>
								<xsl:for-each select="$CNHeader.CreditFile.CardAlertWarningMessage">
									<xsl:variable name="CNHeader.CreditFile.CardAlertWarningMessage.code" select="@code"/>
									<xsl:variable name="CNHeader.CreditFile.CardAlertWarningMessage.description" select="@description"/>
									<xsl:element name="fo:table">
										<fo:table-column column-width="35mm"/>
										<fo:table-column column-width="150mm"/>
										<xsl:element name="fo:table-body">
											<xsl:attribute name="start-indent">0pt</xsl:attribute>
											<xsl:element name="fo:table-row">
												<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
													<fo:block>
														<xsl:text>Code</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
													<fo:block>
														<xsl:text>Messages / Carte de cr&#233;di</xsl:text>
													</fo:block>
												</fo:table-cell>
											</xsl:element>
											<xsl:element name="fo:table-row">
												<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
															<xsl:value-of select="$CNHeader.CreditFile.CardAlertWarningMessage.code"/>
															<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
															<xsl:value-of select="$CNHeader.CreditFile.CardAlertWarningMessage.description"/>
															<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:for-each>
							</xsl:for-each>
						</fo:table-cell>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
		<xsl:variable name="CNAddresses" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNAddresses"/>
		<xsl:for-each select="$CNAddresses">
			<xsl:variable name="CNAddresses.CNAddress" select="CNAddress"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="50mm"/>
				<fo:table-column column-width="32mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="18mm"/>
				<fo:table-column column-width="15mm"/>
				<fo:table-column column-width="12mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="18mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="8" xsl:use-attribute-sets="elesubtitre">
							<fo:block>Adresses</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Description</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Adresse</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Ville</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Province</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>C.P.</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Source</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Mise � jour</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Depuis</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="8">
							<xsl:for-each select="$CNAddresses.CNAddress">
								<xsl:variable name="CNAddresses.CNAddress.description" select="@description"/>
								<xsl:variable name="CNAddresses.CNAddress.CivicNumber" select="CivicNumber"/>
								<xsl:variable name="CNAddresses.CNAddress.StreetName" select="StreetName"/>
								<xsl:variable name="CNAddresses.CNAddress.City" select="City"/>
								<xsl:variable name="CNAddresses.CNAddress.Province.description" select="Province/@description"/>
								<xsl:variable name="CNAddresses.CNAddress.PostalCode" select="PostalCode"/>
								<xsl:variable name="CNAddresses.CNAddress.UpdateSource.description" select="UpdateSource/@description"/>
								<xsl:variable name="CNAddresses.CNAddress.DateReported" select="DateReported"/>
								<xsl:variable name="CNAddresses.CNAddress.Occupancy.Date" select="Occupancy/Date"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="50mm"/>
									<fo:table-column column-width="32mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="18mm"/>
									<fo:table-column column-width="15mm"/>
									<fo:table-column column-width="12mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="18mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNAddresses.CNAddress.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										<fo:table-cell xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="concat($CNAddresses.CNAddress.CivicNumber, '  ', $CNAddresses.CNAddress.StreetName)"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNAddresses.CNAddress.City"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNAddresses.CNAddress.Province.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
										</fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="substring ($CNAddresses.CNAddress.PostalCode, 0,4)"/>&#09;<xsl:value-of select="substring ($CNAddresses.CNAddress.PostalCode, 4,3)"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNAddresses.CNAddress.UpdateSource.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNAddresses.CNAddress.DateReported"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNAddresses.CNAddress.Occupancy.Date"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</fo:table-cell>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
		<!--  Other Names -->
		<xsl:variable name="CNOtherNames" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNOtherNames"/>
		<xsl:for-each select="$CNOtherNames">
			<xsl:variable name="CNOtherNames.CNOtherName" select="CNOtherName"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="60mm"/>
				<fo:table-column column-width="35mm"/>
				<fo:table-column column-width="40mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="5">
							<fo:block xsl:use-attribute-sets="elesubtitre">Autres noms</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Description</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Nom</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Pr&#233;nom</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>2e nom</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Suffixe</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="5">
							<xsl:for-each select="$CNOtherNames.CNOtherName">
								<xsl:variable name="CNOtherNames.CNOtherName.description" select="@description"/>
								<xsl:variable name="CNOtherNames.CNOtherName.LastName" select="LastName"/>
								<xsl:variable name="CNOtherNames.CNOtherName.FirstName" select="FirstName"/>
								<xsl:variable name="CNOtherNames.CNOtherName.MiddleName" select="MiddleName"/>
								<xsl:variable name="CNOtherNames.CNOtherName.Suffix.description" select="Suffix/@description"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="60mm"/>
									<fo:table-column column-width="35mm"/>
									<fo:table-column column-width="40mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNOtherNames.CNOtherName.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNOtherNames.CNOtherName.LastName"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNOtherNames.CNOtherName.FirstName"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNOtherNames.CNOtherName.MiddleName"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNOtherNames.CNOtherName.Suffix.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</fo:table-cell>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of Identification Section -->
		
	<!--  Pointage -->
	<xsl:template name="Pointage">
		<xsl:variable name="CNScores" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNScores"/>
		<xsl:for-each select="$CNScores">
			<xsl:variable name="CNScores.CNScore" select="CNScore"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="185mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell xsl:use-attribute-sets="elesubtitre">
							<xsl:element name="fo:block">Pointage</xsl:element>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">F
							<xsl:for-each select="$CNScores.CNScore">
								<xsl:variable name="CNScores.CNScore.description" select="@description"/>
								<xsl:variable name="CNScores.CNScore.Result" select="Result"/>
								<xsl:variable name="CNScores.CNScore.Reasons" select="Reasons"/>
								<xsl:variable name="CNScores.CNScore.RejectCodes" select="RejectCodes"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="185mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="elesubtitre">
												<fo:block>
															Pointage => <xsl:value-of select="position()"/>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNScores.CNScore.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNScores.CNScore.Result">
													<xsl:variable name="CNScores.CNScore.Result.SignIndicator" select="SignIndicator"/>
													<xsl:variable name="CNScores.CNScore.Result.Value" select="Value"/>
													<xsl:variable name="CNScores.CNScore.Result.ScoreNarratives.ScoreNarrative" select="ScoreNarratives/ScoreNarrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="185mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																	Valeur	
																</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
															<fo:table-cell  xsl:use-attribute-sets="table_values">
																<fo:block>
																		<xsl:value-of select="$CNScores.CNScore.Result.SignIndicator"/>
																		<xsl:value-of select="$CNScores.CNScore.Result.Value"/>
																		<xsl:text>&#160;</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
										<!-- Start of Reasons -->
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNScores.CNScore.Reasons">
													<xsl:variable name="CNScores.CNScore.Reasons.Reason" select="Reason"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="185mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Description</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNScores.CNScore.Reasons.Reason">
																		<xsl:variable name="CNScores.CNScore.Reasons.Reason.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="185mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNScores.CNScore.Reasons.Reason.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
										<!-- End of Reasons -->
										<!-- Reject Codes -->
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNScores.CNScore.RejectCodes">
													<xsl:variable name="CNScores.CNScore.RejectCodes.RejectCode" select="RejectCode"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="185mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Description</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNScores.CNScore.RejectCodes.RejectCode">
																		<xsl:variable name="CNScores.CNScore.RejectCodes.RejectCode.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="185mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNScores.CNScore.RejectCodes.RejectCode.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
										<!-- End of Reject Codes -->
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!--  End of Pointage -->
	
	<!--  Fraud Warning -->
	<xsl:template name="FraudWarnings">
		<xsl:variable name="CNFraudWarnings" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNFraudWarnings"/>
		<xsl:for-each select="$CNFraudWarnings">
			<xsl:variable name="CNFraudWarnings.CNFraudWarning" select="CNFraudWarning"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="35mm"/>
				<fo:table-column column-width="45mm"/>
				<fo:table-column column-width="105mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="3"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell xsl:use-attribute-sets="elesubtitre" number-columns-spanned="3">
							<xsl:element name="fo:block">Fraudes</xsl:element>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Code r�f�rence</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Description</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Messages d'avertissement</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNFraudWarnings.CNFraudWarning">
								<xsl:variable name="CNFraudWarnings.CNFraudWarning.productId" select="@productId"/>
								<xsl:variable name="CNFraudWarnings.CNFraudWarning.description" select="@description"/>
								<xsl:variable name="CNFraudWarnings.CNFraudWarning.Messages.Message" select="Messages/Message"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="35mm"/>
									<fo:table-column column-width="45mm"/>
									<fo:table-column column-width="105mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNFraudWarnings.CNFraudWarning.productId"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNFraudWarnings.CNFraudWarning.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNFraudWarnings.CNFraudWarning.Messages.Message">
													<xsl:variable name="CNFraudWarnings.CNFraudWarning.Messages.Message.description" select="@description"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="105mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="table_values">
																	<fo:block>
																		<xsl:value-of select="$CNFraudWarnings.CNFraudWarning.Messages.Message.description"/>
																		<xsl:text>&#160;</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!--  End of Fraud Warning -->
	
	<!-- Start of Employment -->
	<xsl:template name="Employments">
		<xsl:variable name="CNEmployments" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNEmployments"/>
		<xsl:for-each select="$CNEmployments">
			<xsl:variable name="CNEmployments.CNEmployment" select="CNEmployment"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="40mm"/>
				<fo:table-column column-width="10mm"/>
				<fo:table-column column-width="55mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="6"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell xsl:use-attribute-sets="elesubtitre" number-columns-spanned="6">
							<xsl:element name="fo:block">Emplois</xsl:element>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Description</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche" number-columns-spanned="2">
							<fo:block>
								<xsl:text>Employeur</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Occupation</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Ville</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Province</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Entr&#233;e</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Fin</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>V&#233;rification</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date v&#233;rifi&#233;e</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Salaire</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Code</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNEmployments.CNEmployment">
								<xsl:variable name="CNEmployments.CNEmployment.description" select="@description"/>
								<xsl:variable name="CNEmployments.CNEmployment.Employer" select="Employer"/>
								<xsl:variable name="CNEmployments.CNEmployment.Occupation" select="Occupation"/>
								<xsl:variable name="CNEmployments.CNEmployment.City" select="City"/>
								<xsl:variable name="CNEmployments.CNEmployment.Province.description" select="Province/@description"/>
								<xsl:variable name="CNEmployments.CNEmployment.DateEmployed" select="DateEmployed"/>
								<xsl:variable name="CNEmployments.CNEmployment.DateLeft" select="DateLeft"/>
								<xsl:variable name="CNEmployments.CNEmployment.Verification.description" select="Verification/@description"/>
								<xsl:variable name="CNEmployments.CNEmployment.Verification.Date" select="Verification/Date"/>
								<xsl:variable name="CNEmployments.CNEmployment.Salary.SalaryAmount" select="Salary/SalaryAmount"/>
								<xsl:variable name="CNEmployments.CNEmployment.Salary.SalaryVerification.description" select="Salary/SalaryVerification/@description"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="40mm"/>
									<fo:table-column column-width="10mm"/>
									<fo:table-column column-width="55mm"/>
									<fo:table-column column-width="30mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="2">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.Employer"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.Occupation"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.City"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.Province.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.DateEmployed"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.DateLeft"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.Verification.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.Verification.Date"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="elemonaie">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNEmployments.CNEmployment.Salary.SalaryAmount!=''">
															<xsl:value-of select="format-number($CNEmployments.CNEmployment.Salary.SalaryAmount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNEmployments.CNEmployment.Salary.SalaryVerification.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of Employment -->
	
	<!-- Start of OtherIncomes -->
	<xsl:template name="OtherIncomes">
		<xsl:variable name="CNOtherIncomes" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNOtherIncomes"/>
		<xsl:for-each select="$CNOtherIncomes">
			<xsl:variable name="CNOtherIncomes.CNOtherIncome" select="CNOtherIncome"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="40mm"/>
				<fo:table-column column-width="50mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="65mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="4"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell xsl:use-attribute-sets="elesubtitre" number-columns-spanned="4">
							<xsl:element name="fo:block">Autres revenus</xsl:element>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Mise &#224; jour</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Revenu</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Employeur</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>V&#233;rification</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNOtherIncomes.CNOtherIncome">
								<xsl:variable name="CNOtherIncomes.CNOtherIncome.DateReported" select="DateReported"/>
								<xsl:variable name="CNOtherIncomes.CNOtherIncome.Amount" select="Amount"/>
								<xsl:variable name="CNOtherIncomes.CNOtherIncome.IncomeSource" select="IncomeSource"/>
								<xsl:variable name="CNOtherIncomes.CNOtherIncome.VerificationDate" select="VerificationDate"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="30mm"/>
									<fo:table-column column-width="40mm"/>
									<fo:table-column column-width="50mm"/>
									<fo:table-column column-width="65mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNOtherIncomes.CNOtherIncome.DateReported"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNOtherIncomes.CNOtherIncome.Amount!=''">
															<xsl:value-of select="format-number($CNOtherIncomes.CNOtherIncome.Amount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNOtherIncomes.CNOtherIncome.IncomeSource"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>	
													<xsl:value-of select="$CNOtherIncomes.CNOtherIncome.VerificationDate"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of OtherIncomes -->
	
	<!-- Start of BankruptciesOrActs -->
	<xsl:template name="BankruptciesOrActs">
		<xsl:variable name="CNBankruptciesOrActs" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNBankruptciesOrActs"/>
		<xsl:for-each select="$CNBankruptciesOrActs">
			<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct" select="CNBankruptcyOrAct"/>
			<xsl:element name="fo:table">
					<fo:table-column column-width="20mm"/>
					<fo:table-column column-width="10mm"/>
					<fo:table-column column-width="25mm"/>
					<fo:table-column column-width="25mm"/>
					<fo:table-column column-width="35mm"/>
					<fo:table-column column-width="35mm"/>
					<fo:table-column column-width="35mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="7"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell xsl:use-attribute-sets="elesubtitre" number-columns-spanned="7">
							<xsl:element name="fo:block">Faillites et autres</xsl:element>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNBankruptciesOrActs.CNBankruptcyOrAct">
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.DateFiled" select="DateFiled"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.CourtId.CustomerNumber" select="CourtId/CustomerNumber"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.CourtId.Name" select="CourtId/Name"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.CaseNumberAndTrustee" select="CaseNumberAndTrustee"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.Type.description" select="Type/@description"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.Filer.description" select="Filer/@description"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.UpdateSource.description" select="UpdateSource/@description"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.IntentOrDisposition.description" select="IntentOrDisposition/@description"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.LiabilityAmount" select="LiabilityAmount"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.IntentOrDisposition.Date" select="IntentOrDisposition/Date"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.AssetAmount" select="AssetAmount"/>
								<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.Narratives" select="Narratives"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="10mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="35mm"/>
									<fo:table-column column-width="35mm"/>
									<fo:table-column column-width="35mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Date enreg.</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>No de cour</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Cour</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>No cause / Syndic</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Type faillite</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>D&#233;p&#244;t</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Source</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.DateFiled"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.CourtId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.CourtId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.CaseNumberAndTrustee"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.Type.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.Filer.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.UpdateSource.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="4">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.IntentOrDisposition.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Passif
														</fo:block>
											</fo:table-cell>
										<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNBankruptciesOrActs.CNBankruptcyOrAct.LiabilityAmount!=''">
															<xsl:value-of select="format-number($CNBankruptciesOrActs.CNBankruptcyOrAct.LiabilityAmount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="4">
												<fo:block>
													<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.IntentOrDisposition.Date"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Actif
														</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNBankruptciesOrActs.CNBankruptcyOrAct.AssetAmount!=''">
															<xsl:value-of select="format-number($CNBankruptciesOrActs.CNBankruptcyOrAct.AssetAmount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-row">
										<xsl:element name="fo:table-cell">
											<fo:table-cell number-columns-spanned="7">
												<xsl:for-each select="$CNBankruptciesOrActs.CNBankruptcyOrAct.Narratives">
													<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="185mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNBankruptciesOrActs.CNBankruptcyOrAct.Narratives.Narrative">
																		<xsl:variable name="CNBankruptciesOrActs.CNBankruptcyOrAct.Narratives.Narrative.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="185mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																							<fo:block>	Description : 
																						<xsl:value-of select="$CNBankruptciesOrActs.CNBankruptcyOrAct.Narratives.Narrative.description"/>
																						<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of BankruptciesOrActs -->
	
	<!-- Start of Collections -->
	<xsl:template name="Collections">
		<xsl:variable name="CNCollections" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNCollections"/>
		<xsl:for-each select="$CNCollections">
			<xsl:variable name="CNCollections.CNCollection" select="CNCollection"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="92mm"/>
				<fo:table-column column-width="93mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="2"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell xsl:use-attribute-sets="elesubtitre" number-columns-spanned="2">
							<xsl:element name="fo:block">Recouvrements</xsl:element>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="2">
							<xsl:for-each select="$CNCollections.CNCollection">
								<xsl:variable name="CNCollections.CNCollection.description" select="@description"/>
								<xsl:variable name="CNCollections.CNCollection.AssignedDate" select="AssignedDate"/>
								<xsl:variable name="CNCollections.CNCollection.AgencyId.CustomerNumber" select="AgencyId/CustomerNumber"/>
								<xsl:variable name="CNCollections.CNCollection.AgencyId.Name" select="AgencyId/Name"/>
								<xsl:variable name="CNCollections.CNCollection.LedgerNumber" select="LedgerNumber"/>
								<xsl:variable name="CNCollections.CNCollection.UpdateSource.description" select="UpdateSource/@description"/>
								<xsl:variable name="CNCollections.CNCollection.CollectionCreditor.AccountNumberAndOrName" select="CollectionCreditor/AccountNumberAndOrName"/>
								<xsl:variable name="CNCollections.CNCollection.OriginalAmount" select="OriginalAmount"/>
								<xsl:variable name="CNCollections.CNCollection.DateOfLastPayment" select="DateOfLastPayment"/>
								<xsl:variable name="CNCollections.CNCollection.CollectionCreditor.Industry.description" select="CollectionCreditor/Industry/@description"/>
								<xsl:variable name="CNCollections.CNCollection.BalanceAmount" select="BalanceAmount"/>
								<xsl:variable name="CNCollections.CNCollection.DatePaid" select="DatePaid"/>
								<xsl:variable name="CNCollections.CNCollection.Reason.description" select="Reason/@description"/>
								<xsl:variable name="CNCollections.CNCollection.VerificationDate" select="VerificationDate"/>
								<xsl:variable name="CNCollections.CNCollection.Narratives" select="Narratives"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="30mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="45mm"/>
									<fo:table-column column-width="35mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell number-columns-spanned="6" xsl:use-attribute-sets="eletitregauche">
												<fo:block >Recouvrement => <xsl:value-of select="position()"/>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block >
													<xsl:text>Description</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block >
													<xsl:text>Date enregistr&#233;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block >
													<xsl:text>No de membre</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block >
													<xsl:text>Agence</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block >
													<xsl:text>No grand livre</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block >
													<xsl:text>Source</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.AssignedDate"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.AgencyId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.AgencyId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.LedgerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.UpdateSource.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Cr&#233;ancier/Compte
														</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.CollectionCreditor.AccountNumberAndOrName"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Montant
														</fo:block>
											</fo:table-cell>
										<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNCollections.CNCollection.OriginalAmount!=''">
															<xsl:value-of select="format-number($CNCollections.CNCollection.OriginalAmount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Dernier paiement
														</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.DateOfLastPayment"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Description
														</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.CollectionCreditor.Industry.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Solde
														</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNCollections.CNCollection.BalanceAmount!=''">
															<xsl:value-of select="format-number($CNCollections.CNCollection.BalanceAmount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Date pay&#233;e
														</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.DatePaid"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
										<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Raison
														</fo:block>
											</fo:table-cell>
										<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="3">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.Reason.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
															Date v&#233;rifi&#233;e
														</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNCollections.CNCollection.VerificationDate"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNCollections.CNCollection.Narratives">
													<xsl:variable name="CNCollections.CNCollection.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="92mm"/>
														<fo:table-column column-width="93mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																		<fo:block>
																			Code
																		</fo:block>
																</fo:table-cell>
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																		<fo:block>
																			Narratives
																		</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNCollections.CNCollection.Narratives.Narrative">
																		<xsl:variable name="CNCollections.CNCollection.Narratives.Narrative.code" select="@code"/>
																		<xsl:variable name="CNCollections.CNCollection.Narratives.Narrative.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="92mm"/>
																			<fo:table-column column-width="93mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNCollections.CNCollection.Narratives.Narrative.code"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																							<fo:block>
																							<xsl:value-of select="$CNCollections.CNCollection.Narratives.Narrative.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</fo:table-cell>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of Collections -->
	
	<!-- Start of Legal Items -->
	<xsl:template name="LegalItems">
		<xsl:variable name="CNLegalItems" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNLegalItems"/>
		<xsl:for-each select="$CNLegalItems">
			<xsl:variable name="CNLegalItems.CNLegalItem" select="CNLegalItem"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="45mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="30mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="5"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="5">
												<fo:block>Jugement</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNLegalItems.CNLegalItem">
								<xsl:variable name="CNLegalItems.CNLegalItem.description" select="@description"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.DateFiled" select="DateFiled"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.CourtId.CustomerNumber" select="CourtId/CustomerNumber"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.CourtId.Name" select="CourtId/Name"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.CaseNumber" select="CaseNumber"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.Defendant" select="Defendant"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.Plaintiff" select="Plaintiff"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.Amount" select="Amount"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.VerificationDate" select="VerificationDate"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.Status.description" select="Status/@description"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.DateSatisfied" select="DateSatisfied"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.LawyerNameAddress" select="LawyerNameAddress"/>
								<xsl:variable name="CNLegalItems.CNLegalItem.Narratives" select="Narratives"/>
								<xsl:variable name="montant">
									<xsl:choose>
										<xsl:when test="$CNLegalItems.CNLegalItem.Amount!=''">
											<xsl:value-of select="format-number($CNLegalItems.CNLegalItem.Amount,'$#,##0.00')"/>
										</xsl:when>
										<xsl:otherwise>$0.00</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:element name="fo:table">
										<fo:table-column column-width="45mm"/>
										<fo:table-column column-width="30mm"/>
										<fo:table-column column-width="30mm"/>
										<fo:table-column column-width="30mm"/>
										<fo:table-column column-width="30mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche" number-columns-spanned="5">
												<fo:block>Note => <xsl:value-of select="position()"/>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Description</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Date</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>No de cour</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Nom de la cour</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>No de cause</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLegalItems.CNLegalItem.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLegalItems.CNLegalItem.DateFiled"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLegalItems.CNLegalItem.CourtId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLegalItems.CNLegalItem.CourtId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLegalItems.CNLegalItem.CaseNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>D&#233;fendeur</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Demandeur</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Montant</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>&#201;tatr</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Avocat</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLegalItems.CNLegalItem.Defendant"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLegalItems.CNLegalItem.Plaintiff"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="concat($montant, ' / ', $CNLegalItems.CNLegalItem.VerificationDate)"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="concat($CNLegalItems.CNLegalItem.Status.description, ' / ', $CNLegalItems.CNLegalItem.DateSatisfied)"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLegalItems.CNLegalItem.LawyerNameAddress"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNLegalItems.CNLegalItem.Narratives">
													<xsl:variable name="CNLegalItems.CNLegalItem.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="185mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		Narratives
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNLegalItems.CNLegalItem.Narratives.Narrative">
																		<xsl:variable name="CNLegalItems.CNLegalItem.Narratives.Narrative.code" select="@code"/>
																		<xsl:variable name="CNLegalItems.CNLegalItem.Narratives.Narrative.description" select="@description"/>
																		<fo:table-column column-width="185mm"/>
																		<xsl:element name="fo:table">
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNLegalItems.CNLegalItem.Narratives.Narrative.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of Legal Items -->
	
	<!-- Start of SecuredLoans -->
	<xsl:template name="SecuredLoans">
		<xsl:variable name="CNSecuredLoans" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNSecuredLoans"/>
		<xsl:for-each select="$CNSecuredLoans">
			<xsl:variable name="CNSecuredLoans.CNSecuredLoan" select="CNSecuredLoan"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="40mm"/>
				<fo:table-column column-width="40mm"/>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="6"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="6">
												<fo:block>Pr&#234;t garanti</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No de cour</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Nom de la cour</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>&#201;ch&#233;ance</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Cr&#233;ancier</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Industrie</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNSecuredLoans.CNSecuredLoan">
								<xsl:variable name="CNSecuredLoans.CNSecuredLoan.DateFiled" select="DateFiled"/>
								<xsl:variable name="CNSecuredLoans.CNSecuredLoan.CourtId.CustomerNumber" select="CourtId/CustomerNumber"/>
								<xsl:variable name="CNSecuredLoans.CNSecuredLoan.CourtId.Name" select="CourtId/Name"/>
								<xsl:variable name="CNSecuredLoans.CNSecuredLoan.MaturityDate" select="MaturityDate"/>
								<xsl:variable name="CNSecuredLoans.CNSecuredLoan.SecuredLoanCreditorId" select="SecuredLoanCreditorId"/>
								<xsl:variable name="CNSecuredLoans.CNSecuredLoan.Narratives" select="Narratives"/>
								<xsl:variable name="CNSecuredLoans.CNSecuredLoan.SecuredLoanCreditorId.NameAddressAndAmount" select="SecuredLoanCreditorId/NameAddressAndAmount"/>
								<xsl:variable name="CNSecuredLoans.CNSecuredLoan.SecuredLoanCreditorId.Industry.description" select="SecuredLoanCreditorId/Industry/@description"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="40mm"/>
									<fo:table-column column-width="40mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSecuredLoans.CNSecuredLoan.DateFiled"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSecuredLoans.CNSecuredLoan.CourtId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSecuredLoans.CNSecuredLoan.CourtId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSecuredLoans.CNSecuredLoan.MaturityDate"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSecuredLoans.CNSecuredLoan.SecuredLoanCreditorId.NameAddressAndAmount"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSecuredLoans.CNSecuredLoan.SecuredLoanCreditorId.Industry.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell number-columns-spanned="6">
												<fo:block>
													<xsl:for-each select="$CNSecuredLoans.CNSecuredLoan.Narratives">
														<xsl:variable name="CNSecuredLoans.CNSecuredLoan.Narratives.Narrative" select="Narrative"/>
														<xsl:element name="fo:table">
															<fo:table-column column-width="185mm"/>
															<xsl:element name="fo:table-body">
																<xsl:element name="fo:table-row">
																	<xsl:element name="fo:table-cell">
																		<xsl:for-each select="$CNSecuredLoans.CNSecuredLoan.Narratives.Narrative">
																			<xsl:variable name="CNSecuredLoans.CNSecuredLoan.Narratives.Narrative.description" select="@description"/>
																			<xsl:element name="fo:table">
																				<fo:table-column column-width="185mm"/>
																				<xsl:element name="fo:table-body">
																					<xsl:element name="fo:table-row">
																						<fo:table-cell  xsl:use-attribute-sets="table_values">
																							<fo:block>
																								Description : <xsl:value-of select="$CNSecuredLoans.CNSecuredLoan.Narratives.Narrative.description"/>
																							</fo:block>
																						</fo:table-cell>
																					</xsl:element>
																				</xsl:element>
																			</xsl:element>
																		</xsl:for-each>
																	</xsl:element>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:for-each>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of SecuredLoans -->
	
	<!-- Start of NonResponsabilityNotices -->
	<xsl:template name="NonResponsabilityNotices">
		<xsl:variable name="CNNonResponsabilityNotices" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNNonResponsabilityNotices"/>
		<xsl:for-each select="$CNNonResponsabilityNotices">
			<xsl:variable name="CNNonResponsabilityNotices.CNNonResponsabilityNotice" select="CNNonResponsabilityNotice"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="22mm"/>
				<fo:table-column column-width="33mm"/>
				<fo:table-column column-width="135mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="3"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="3">
							<fo:block>Responsabilit&#233;s</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date rapport&#233;e</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Code</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Description</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNNonResponsabilityNotices.CNNonResponsabilityNotice">
								<xsl:variable name="CNNonResponsabilityNotices.CNNonResponsabilityNotice.DateReported" select="DateReported"/>
								<xsl:variable name="CNNonResponsabilityNotices.CNNonResponsabilityNotice.Filer.code" select="Filer/@code"/>
								<xsl:variable name="CNNonResponsabilityNotices.CNNonResponsabilityNotice.Filer.description" select="Filer/@description"/>
								<xsl:variable name="CNNonResponsabilityNotices.CNNonResponsabilityNotice.Narratives" select="Narratives"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="22mm"/>
									<fo:table-column column-width="33mm"/>
									<fo:table-column column-width="135mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche" number-columns-spanned="3">
												<fo:block>Note => <xsl:value-of select="position()"/>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNonResponsabilityNotices.CNNonResponsabilityNotice.DateReported"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNonResponsabilityNotices.CNNonResponsabilityNotice.Filer.code"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNonResponsabilityNotices.CNNonResponsabilityNotice.Filer.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNNonResponsabilityNotices.CNNonResponsabilityNotice.Narratives">
													<xsl:variable name="CNNonResponsabilityNotices.CNNonResponsabilityNotice.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="20mm"/>
														<fo:table-column column-width="165mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Code</xsl:text>
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Narratives</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNNonResponsabilityNotices.CNNonResponsabilityNotice.Narratives.Narrative">
																		<xsl:variable name="CNNonResponsabilityNotices.CNNonResponsabilityNotice.Narratives.Narrative.code" select="@code"/>
																		<xsl:variable name="CNNonResponsabilityNotices.CNNonResponsabilityNotice.Narratives.Narrative.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="20mm"/>
																			<fo:table-column column-width="165mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNNonResponsabilityNotices.CNNonResponsabilityNotice.Narratives.Narrative.code"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNNonResponsabilityNotices.CNNonResponsabilityNotice.Narratives.Narrative.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of NonResponsabilityNotices -->
	
	<!-- Start of MaritalItems -->
	<xsl:template name="MaritalItems">
		<xsl:variable name="CNMaritalItems" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNMaritalItems"/>
		<xsl:for-each select="$CNMaritalItems">
			<xsl:variable name="CNMaritalItems.CNMaritalItem" select="CNMaritalItem"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="35mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="35mm"/>
				<fo:table-column column-width="20mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="7"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
					<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="7">
							<fo:block>Situation familiale</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Statut</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date d&#233;pos&#233;e</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No r&#233;f&#233;rence</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Cour</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Jugement</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Conjoint(e)</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date v&#233;rif.</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNMaritalItems.CNMaritalItem">
								<xsl:variable name="CNMaritalItems.CNMaritalItem.DivorceStatus.description" select="DivorceStatus/@description"/>
								<xsl:variable name="CNMaritalItems.CNMaritalItem.DateFiled" select="DateFiled"/>
								<xsl:variable name="CNMaritalItems.CNMaritalItem.CourtId.CustomerNumber" select="CourtId/CustomerNumber"/>
								<xsl:variable name="CNMaritalItems.CNMaritalItem.CourtId.Name" select="CourtId/Name"/>
								<xsl:variable name="CNMaritalItems.CNMaritalItem.CaseNumber" select="CaseNumber"/>
								<xsl:variable name="CNMaritalItems.CNMaritalItem.SpouseName" select="SpouseName"/>
								<xsl:variable name="CNMaritalItems.CNMaritalItem.VerificationDate" select="VerificationDate"/>
								<xsl:variable name="CNMaritalItems.CNMaritalItem.Narratives" select="Narratives"/>
								<xsl:element name="fo:table">
								<fo:table-column column-width="35mm"/>
								<fo:table-column column-width="20mm"/>
								<fo:table-column column-width="20mm"/>
								<fo:table-column column-width="25mm"/>
								<fo:table-column column-width="30mm"/>
								<fo:table-column column-width="35mm"/>
								<fo:table-column column-width="20mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
													<fo:block>
													<xsl:value-of select="$CNMaritalItems.CNMaritalItem.DivorceStatus.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
													<fo:block>
													<xsl:value-of select="$CNMaritalItems.CNMaritalItem.DateFiled"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNMaritalItems.CNMaritalItem.CourtId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNMaritalItems.CNMaritalItem.CourtId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNMaritalItems.CNMaritalItem.CaseNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNMaritalItems.CNMaritalItem.SpouseName"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNMaritalItems.CNMaritalItem.VerificationDate"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNMaritalItems.CNMaritalItem.Narratives">
													<xsl:variable name="CNMaritalItems.CNMaritalItem.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="20mm"/>
														<fo:table-column column-width="165mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Code</xsl:text>
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Narratives</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNMaritalItems.CNMaritalItem.Narratives.Narrative">
																		<xsl:variable name="CNMaritalItems.CNMaritalItem.Narratives.Narrative.code" select="@code"/>
																		<xsl:variable name="CNMaritalItems.CNMaritalItem.Narratives.Narrative.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="20mm"/>
																			<fo:table-column column-width="165mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNMaritalItems.CNMaritalItem.Narratives.Narrative.code"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNMaritalItems.CNMaritalItem.Narratives.Narrative.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of MaritalItems -->
	
	<!-- Start of NSFs -->
	<xsl:template name="NSFs">
		<xsl:variable name="CNNSFs" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNNSFs"/>
		<xsl:for-each select="$CNNSFs">
			<xsl:variable name="CNNSFs.CNNSF" select="CNNSF"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="10mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="45mm"/>
				<fo:table-column column-width="20mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="7"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="7">
							<fo:block>Ch&#232;que(s) sans provision</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
					<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
						<fo:block>
								<xsl:text>Date</xsl:text>
							</fo:block>
						</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
						<fo:block>
								<xsl:text>No de membre</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Nom du membre</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No t&#233;l. (ext)</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Montant</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>D&#233;tails</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>V&#233;rif.</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNNSFs.CNNSF">
								<xsl:variable name="CNNSFs.CNNSF.DateReported" select="DateReported"/>
								<xsl:variable name="CNNSFs.CNNSF.CreditorId.CustomerNumber" select="CreditorId/CustomerNumber"/>
								<xsl:variable name="CNNSFs.CNNSF.CreditorId.Name" select="CreditorId/Name"/>
								<xsl:variable name="CNNSFs.CNNSF.CreditorId.Telephone.ParsedTelephone.AreaCode" select="CreditorId/Telephone/ParsedTelephone/AreaCode"/>
								<xsl:variable name="CNNSFs.CNNSF.CreditorId.Telephone.ParsedTelephone.Number" select="CreditorId/Telephone/ParsedTelephone/Number"/>
								<xsl:variable name="CNNSFs.CNNSF.CreditorId.Telephone.ParsedTelephone.Extension" select="CreditorId/Telephone/ParsedTelephone/Extension"/>
								<xsl:variable name="CNNSFs.CNNSF.NSFAmount" select="NSFAmount"/>
								<xsl:variable name="CNNSFs.CNNSF.Details" select="Details"/>
								<xsl:variable name="CNNSFs.CNNSF.VerificationDate" select="VerificationDate"/>
								<xsl:variable name="CNNSFs.CNNSF.Narratives" select="Narratives"/>
								<xsl:element name="fo:table">
								<fo:table-column column-width="30mm"/>
								<fo:table-column column-width="20mm"/>
								<fo:table-column column-width="10mm"/>
								<fo:table-column column-width="30mm"/>
								<fo:table-column column-width="30mm"/>
								<fo:table-column column-width="45mm"/>
								<fo:table-column column-width="20mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNSFs.CNNSF.DateReported"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNSFs.CNNSF.CreditorId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNSFs.CNNSF.CreditorId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:call-template name="format-telephone-extention">
														<xsl:with-param name="area" select="$CNNSFs.CNNSF.CreditorId.Telephone.ParsedTelephone.AreaCode"/>
														<xsl:with-param name="number" select="$CNNSFs.CNNSF.CreditorId.Telephone.ParsedTelephone.Number"/>
														<xsl:with-param name="xtn" select="$CNNSFs.CNNSF.CreditorId.Telephone.ParsedTelephone.Extension"/>
													</xsl:call-template>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNSFs.CNNSF.NSFAmount"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNSFs.CNNSF.Details"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNSFs.CNNSF.VerificationDate"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNNSFs.CNNSF.Narratives">
													<xsl:variable name="CNNSFs.CNNSF.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="20mm"/>
														<fo:table-column column-width="165mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Code</xsl:text>
																	</fo:block>
																</fo:table-cell>
															<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																<fo:block>
																		<xsl:text>Narratives</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNNSFs.CNNSF.Narratives.Narrative">
																		<xsl:variable name="CNNSFs.CNNSF.Narratives.Narrative.code" select="@code"/>
																		<xsl:variable name="CNNSFs.CNNSF.Narratives.Narrative.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="20mm"/>
																			<fo:table-column column-width="165mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNNSFs.CNNSF.Narratives.Narrative.code"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNNSFs.CNNSF.Narratives.Narrative.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of NSFs -->
	
	<!-- Start of Garnishments -->
	<xsl:template name="Garnishments">
		<xsl:variable name="CNGarnishments" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNGarnishments"/>
		<xsl:for-each select="$CNGarnishments">
			<xsl:variable name="CNGarnishments.CNGarnishment" select="CNGarnishment"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="15mm"/>
				<fo:table-column column-width="10mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="10"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="10">
								<fo:block>Saisie-Arr&#234;t</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No de cour</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Nom de la cour</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No de cause</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Montant</xsl:text>
							</fo:block>
						</fo:table-cell>
					<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Demandeur</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>D&#233;fendeur</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date v&#233;rif.</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date pay&#233;e</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Saisie</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNGarnishments.CNGarnishment">
								<xsl:variable name="CNGarnishments.CNGarnishment.DateFiled" select="DateFiled"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.CourtId.CustomerNumber" select="CourtId/CustomerNumber"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.CourtId.Name" select="CourtId/Name"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.CaseNumber" select="CaseNumber"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.Amount" select="Amount"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.Plaintiff" select="Plaintiff"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.Defendant" select="Defendant"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.VerificationDate" select="VerificationDate"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.DateSatisfied" select="DateSatisfied"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.Garnishee" select="Garnishee"/>
								<xsl:variable name="CNGarnishments.CNGarnishment.Narratives" select="Narratives"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="15mm"/>
									<fo:table-column column-width="10mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="20mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.DateFiled"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.CourtId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.CourtId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.CaseNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNGarnishments.CNGarnishment.Amount!=''">
															<xsl:value-of select="format-number($CNGarnishments.CNGarnishment.Amount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.Plaintiff"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.Defendant"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.VerificationDate"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.DateSatisfied"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNGarnishments.CNGarnishment.Garnishee"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNGarnishments.CNGarnishment.Narratives">
													<xsl:variable name="CNGarnishments.CNGarnishment.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="185mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Narratives</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNGarnishments.CNGarnishment.Narratives.Narrative">
																		<xsl:variable name="CNGarnishments.CNGarnishment.Narratives.Narrative.code" select="@code"/>
																		<xsl:variable name="CNGarnishments.CNGarnishment.Narratives.Narrative.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="185mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNGarnishments.CNGarnishment.Narratives.Narrative.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of Garnishments -->
	
<!-- Start of Trades -->
	<xsl:template name="Trades">
		<xsl:variable name="CNTrades" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNTrades"/>
		<xsl:for-each select="$CNTrades">
		<xsl:variable name="CNTrades.CNTrade" select="CNTrade"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="185mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre">
							<fo:block>Op�rations</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNTrades.CNTrade">
								<xsl:variable name="CNTrades.CNTrade.DateReported" select="DateReported"/>
								<xsl:variable name="CNTrades.CNTrade.CreditorId.CustomerNumber" select="CreditorId/CustomerNumber"/>
								<xsl:variable name="CNTrades.CNTrade.CreditorId.Name" select="CreditorId/Name"/>
								<xsl:variable name="CNTrades.CNTrade.CreditorId.Telephone.ParsedTelephone.AreaCode" select="CreditorId/Telephone/ParsedTelephone/AreaCode"/>
								<xsl:variable name="CNTrades.CNTrade.CreditorId.Telephone.ParsedTelephone.Number" select="CreditorId/Telephone/ParsedTelephone/Number"/>
								<xsl:variable name="CNTrades.CNTrade.CreditorId.Telephone.ParsedTelephone.Extension" select="CreditorId/Telephone/ParsedTelephone/Extension"/>
								<xsl:variable name="CNTrades.CNTrade.AccountNumber" select="AccountNumber"/>
								<xsl:variable name="CNTrades.CNTrade.Association.description" select="Association/@description"/>
								<xsl:variable name="CNTrades.CNTrade.UpdateSource.description" select="UpdateSource/@description"/>
								<xsl:variable name="CNTrades.CNTrade.HighCreditAmount" select="HighCreditAmount"/>
								<xsl:variable name="CNTrades.CNTrade.DateOpened" select="DateOpened"/>
								<xsl:variable name="CNTrades.CNTrade.PortfolioType.description" select="PortfolioType/@description"/>
								<xsl:variable name="CNTrades.CNTrade.PortfolioType.code" select="PortfolioType/@code"/>
								<xsl:variable name="CNTrades.CNTrade.BalanceAmount" select="BalanceAmount"/>
								<xsl:variable name="CNTrades.CNTrade.DateLastActivityOrPayment" select="DateLastActivityOrPayment"/>
								<xsl:variable name="CNTrades.CNTrade.MonthsReviewed" select="MonthsReviewed"/>
								<xsl:variable name="CNTrades.CNTrade.PaymentTermAmount" select="PaymentTermAmount"/>
								<xsl:variable name="CNTrades.CNTrade.HistoryDerogatoryCounters.Count30DayPastDue" select="HistoryDerogatoryCounters/Count30DayPastDue"/>
								<xsl:variable name="CNTrades.CNTrade.HistoryDerogatoryCounters.Count60DayPastDue" select="HistoryDerogatoryCounters/Count60DayPastDue"/>
								<xsl:variable name="CNTrades.CNTrade.HistoryDerogatoryCounters.Count90DayPastDue" select="HistoryDerogatoryCounters/Count90DayPastDue"/>
								<xsl:variable name="CNTrades.CNTrade.Status.description" select="Status/@description"/>
								<xsl:variable name="CNTrades.CNTrade.PastDueAmount" select="PastDueAmount"/>
								<xsl:variable name="CNTrades.CNTrade.PaymentRate.description" select="PaymentRate/@description"/>
								<xsl:variable name="CNTrades.CNTrade.PaymentRate.code" select="PaymentRate/@code"/>
								<xsl:variable name="CNTrades.CNTrade.PreviousHighPaymentRates" select="PreviousHighPaymentRates"/>
								<xsl:variable name="CNTrades.CNTrade.Narratives" select="Narratives"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="30mm"/>
									<fo:table-column column-width="40mm"/>
									<fo:table-column column-width="20mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
										<fo:table-cell  xsl:use-attribute-sets="eletitregauche" number-columns-spanned="7">
											<fo:block>
												Op�rations => <xsl:value-of select="position()"/>
											</fo:block>
										</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													Date
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													No de membre
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													Nom du membre
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													No t&#233;l. (ext)
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													No compte
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													Association
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													Source
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.DateReported"/>	
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.CreditorId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.CreditorId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:call-template name="format-telephone-extention">
														<xsl:with-param name="area" select="$CNTrades.CNTrade.CreditorId.Telephone.ParsedTelephone.AreaCode"/>
														<xsl:with-param name="number" select="$CNTrades.CNTrade.CreditorId.Telephone.ParsedTelephone.Number"/>
														<xsl:with-param name="xtn" select="$CNTrades.CNTrade.CreditorId.Telephone.ParsedTelephone.Extension"/>
													</xsl:call-template>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.AccountNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.Association.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.UpdateSource.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
										<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
													Limite (h/c)
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="elemonaie">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNTrades.CNTrade.HighCreditAmount!=''"><xsl:value-of select="format-number($CNTrades.CNTrade.HighCreditAmount,'$#,##0.00')"/></xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
												</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
														Ouverture
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
														<xsl:value-of select="$CNTrades.CNTrade.DateOpened"/>
														<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
														Classification
													</fo:block>
												</fo:table-cell>
												<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="2">
												<fo:block>
														<xsl:value-of select="$CNTrades.CNTrade.PortfolioType.code"/><xsl:value-of select="$CNTrades.CNTrade.PaymentRate.code"/>
														<xsl:text>&#160;</xsl:text>
													</fo:block>
												</fo:table-cell>
											</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
													Solde
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="elemonaie">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNTrades.CNTrade.BalanceAmount!=''"><xsl:value-of select="format-number($CNTrades.CNTrade.BalanceAmount,'$#,##0.00')"/></xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>	
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
													Dernier paiement
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.DateLastActivityOrPayment"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
													Nb/mois
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="2">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.MonthsReviewed"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
													Paiement
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="elemonaie">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNTrades.CNTrade.PaymentTermAmount!=''"><xsl:value-of select="format-number($CNTrades.CNTrade.PaymentTermAmount,'$#,##0.00')"/></xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>	
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
													30/60/90+ 
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNTrades.CNTrade.HistoryDerogatoryCounters.Count30DayPastDue!=''"><xsl:value-of select="concat($CNTrades.CNTrade.HistoryDerogatoryCounters.Count30DayPastDue,'&#160;')"/></xsl:when>
														<xsl:otherwise>00</xsl:otherwise>
													</xsl:choose>/	
													<xsl:choose>
														<xsl:when test="$CNTrades.CNTrade.HistoryDerogatoryCounters.Count60DayPastDue!=''"><xsl:value-of select="concat($CNTrades.CNTrade.HistoryDerogatoryCounters.Count60DayPastDue,'&#160;')"/></xsl:when>
														<xsl:otherwise>00</xsl:otherwise>
													</xsl:choose>/
													<xsl:choose>
														<xsl:when test="$CNTrades.CNTrade.HistoryDerogatoryCounters.Count90DayPastDue!=''"><xsl:value-of select="concat($CNTrades.CNTrade.HistoryDerogatoryCounters.Count90DayPastDue,'&#160;')"/></xsl:when>
														<xsl:otherwise>00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
													&#201;tat
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="2">
												<fo:block>
													<xsl:value-of select="$CNTrades.CNTrade.Status.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche2">
												<fo:block>
													Arr&#233;rages
												</fo:block>
											</fo:table-cell>
											<fo:table-cell xsl:use-attribute-sets="elemonaie">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNTrades.CNTrade.PastDueAmount!=''"><xsl:value-of select="format-number($CNTrades.CNTrade.PastDueAmount,'$#,##0.00')"/></xsl:when>
															<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>	
														<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="5">
												<fo:block>	<xsl:text>&#160;</xsl:text></fo:block>
											</fo:table-cell>
											</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:for-each select="$CNTrades.CNTrade.PreviousHighPaymentRates">
												<xsl:variable name="CNTrades.CNTrade.PreviousHighPaymentRates.PreviousHighPaymentRate" select="PreviousHighPaymentRate"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="20mm"/>
														<fo:table-column column-width="165mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Date</xsl:text>
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>D&#233;tails</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
															<xsl:element name="fo:table-cell">
																<xsl:element name="fo:table">
																	<fo:table-column column-width="20mm"/>
																	<fo:table-column column-width="165mm"/>
																	<xsl:for-each select="$CNTrades.CNTrade.PreviousHighPaymentRates.PreviousHighPaymentRate">
																	<xsl:variable name="CNTrades.CNTrade.PreviousHighPaymentRates.PreviousHighPaymentRate.Date" select="Date"/>
																	<xsl:variable name="CNTrades.CNTrade.PreviousHighPaymentRates.PreviousHighPaymentRate.code" select="@code"/>
																	<xsl:element name="fo:table-body">
																		<xsl:element name="fo:table-row">
																			<fo:table-cell  xsl:use-attribute-sets="table_values">
																				<fo:block>
																					<xsl:value-of select="$CNTrades.CNTrade.PreviousHighPaymentRates.PreviousHighPaymentRate.Date"/>		
																					<xsl:text>&#160;</xsl:text>						
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell  xsl:use-attribute-sets="table_values">
																				<fo:block>																					
																					<xsl:value-of select="$CNTrades.CNTrade.PortfolioType.code"/>
																					<xsl:value-of select="$CNTrades.CNTrade.PreviousHighPaymentRates.PreviousHighPaymentRate.code"/>	
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</xsl:element>
																	</xsl:element>
																	</xsl:for-each>
															</xsl:element>
															</xsl:element>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																<SCRIPT>
																	varTemp = 0;
																</SCRIPT>
																	<xsl:for-each select="$CNTrades.CNTrade.Narratives">
																	<xsl:variable name="CNTrades.CNTrade.Narratives.Narrative" select="Narrative"/>
																	<xsl:for-each select="$CNTrades.CNTrade.Narratives.Narrative">
																	<xsl:variable name="CNTrades.CNTrade.Narratives.Narrative.description" select="@description"/>
																	<xsl:if test="$CNTrades.CNTrade.Narratives.Narrative.description!=''">
																	<SCRIPT>
																	if (varTemp==0) {
																		document.write('<TR bgcolor="#e2e2e2">');
																		document.write('<TD class="eletitregauche"  colspan="7" >Description</TD>');
																		document.write('</TR>');
																	}
																	varTemp = 1;
																		</SCRIPT>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="185mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNTrades.CNTrade.Narratives.Narrative.description"/>	
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																		</xsl:if>
																	</xsl:for-each>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</xsl:element>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
<!-- End of Trades -->	
	
	<!-- Start of NonMemberTrades -->
	<xsl:template name="NonMemberTrades">
		<xsl:variable name="CNNonMemberTrades" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNNonMemberTrades"/>
		<xsl:for-each select="$CNNonMemberTrades">
			<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade" select="CNNonMemberTrade"/>
			<xsl:variable name="CNGarnishments.CNGarnishment" select="CNGarnishment"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="35mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="7"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="7">
							<fo:block>Op&#233;rations non membres</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNNonMemberTrades.CNNonMemberTrade">
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.DateReported" select="DateReported"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.CreditorName" select="CreditorName"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.HighCreditAmount" select="HighCreditAmount"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.BalanceAmount" select="BalanceAmount"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.PastDueAmount" select="PastDueAmount"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.DateOpened" select="DateOpened"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.PortfolioType.description" select="PortfolioType/@description"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.PaymentRate" select="PaymentRate"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.Status.description" select="Status/@description"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.PaymentRate.description" select="PaymentRate/@description"/>
								<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.Narratives" select="Narratives"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="30mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="20mm"/>
									<fo:table-column column-width="35mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche" number-columns-spanned="7">
													<fo:block>Op&#233;ration non membres => <xsl:value-of select="position()"/>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Date</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Cr&#233;diteur</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Limite</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Balance</xsl:text>
												</fo:block>
											</fo:table-cell>
										<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Montant d&#251;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Ouverture</xsl:text>
												</fo:block>
											</fo:table-cell>
										<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Portefeuille</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNonMemberTrades.CNNonMemberTrade.DateReported"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNonMemberTrades.CNNonMemberTrade.CreditorName"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNNonMemberTrades.CNNonMemberTrade.HighCreditAmount!=''">
															<xsl:value-of select="format-number($CNNonMemberTrades.CNNonMemberTrade.HighCreditAmount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNNonMemberTrades.CNNonMemberTrade.BalanceAmount!=''">
															<xsl:value-of select="format-number($CNNonMemberTrades.CNNonMemberTrade.BalanceAmount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:choose>
														<xsl:when test="$CNNonMemberTrades.CNNonMemberTrade.PastDueAmount!=''">
															<xsl:value-of select="format-number($CNNonMemberTrades.CNNonMemberTrade.PastDueAmount,'$#,##0.00')"/>
														</xsl:when>
														<xsl:otherwise>$0.00</xsl:otherwise>
													</xsl:choose>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNonMemberTrades.CNNonMemberTrade.PastDueAmount"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNonMemberTrades.CNNonMemberTrade.PortfolioType.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Profile</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche" number-columns-spanned="6">
												<fo:block>Statut</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNNonMemberTrades.CNNonMemberTrade.Status.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="6">
												<fo:block>
													<xsl:value-of select="$CNNonMemberTrades.CNNonMemberTrade.PaymentRate.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell number-columns-spanned="7">
												<xsl:for-each select="$CNNonMemberTrades.CNNonMemberTrade.Narratives">
													<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="20mm"/>
														<fo:table-column column-width="165mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Code</xsl:text>
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Narratives</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNNonMemberTrades.CNNonMemberTrade.Narratives.Narrative">
																		<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.Narratives.Narrative.code" select="@code"/>
																		<xsl:variable name="CNNonMemberTrades.CNNonMemberTrade.Narratives.Narrative.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="20mm"/>
																			<fo:table-column column-width="165mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNNonMemberTrades.CNNonMemberTrade.Narratives.Narrative.code"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							<xsl:value-of select="$CNNonMemberTrades.CNNonMemberTrade.Narratives.Narrative.description"/>
																							<xsl:text>&#160;</xsl:text>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of NonMemberTrades -->
	
	<!-- Start of BankAccounts -->
	<xsl:template name="BankAccounts">
		<xsl:variable name="CNBankAccounts" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNBankAccounts"/>
		<xsl:for-each select="$CNBankAccounts">
			<xsl:variable name="CNBankAccounts.CNBankAccount" select="CNBankAccount"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="20mm"/>
				<fo:table-column column-width="30mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="8"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="8">
							<fo:block>Op&#233;rations non membres</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNBankAccounts.CNBankAccount">
								<xsl:variable name="CNBankAccounts.CNBankAccount.description" select="@description"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.DateReported" select="DateReported"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.CreditorId.CustomerNumber" select="CreditorId/CustomerNumber"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.CreditorId.Name" select="CreditorId/Name"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.CreditorId.Telephone.ParsedTelephone.AreaCode" select="CreditorId/Telephone/ParsedTelephone/AreaCode"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.CreditorId.Telephone.ParsedTelephone.Number" select="CreditorId/Telephone/ParsedTelephone/Number"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.CreditorId.Telephone.ParsedTelephone.Extension" select="CreditorId/TelephoneParsedTelephone/Extension"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.DateOpened" select="DateOpened"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.FigureAmount" select="FigureAmount"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.NumberOfNSF" select="NumberOfNSF"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.AccountType" select="AccountType"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.Narratives" select="Narratives"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.AccountNumber" select="AccountNumber"/>
								<xsl:variable name="CNBankAccounts.CNBankAccount.AccountType.description" select="AccountType/@description"/>
								<xsl:element name="fo:table">
										<fo:table-column column-width="20mm"/>
										<fo:table-column column-width="20mm"/>
										<fo:table-column column-width="25mm"/>
										<fo:table-column column-width="25mm"/>
										<fo:table-column column-width="25mm"/>
										<fo:table-column column-width="20mm"/>
										<fo:table-column column-width="20mm"/>
										<fo:table-column column-width="30mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche" number-columns-spanned="8">
												<fo:block>Compte => <xsl:value-of select="position()"/>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Type compte</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Date report&#233;e</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>No de membre</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Banque</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>No t&#233;l. (ext)</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Ouverture</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>Montant Fig.</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
												<fo:block>
													<xsl:text>NSF</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankAccounts.CNBankAccount.description"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankAccounts.CNBankAccount.DateReported"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankAccounts.CNBankAccount.CreditorId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankAccounts.CNBankAccount.CreditorId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:call-template name="format-telephone-extention">
														<xsl:with-param name="area" select="$CNBankAccounts.CNBankAccount.CreditorId.Telephone.ParsedTelephone.AreaCode"/>
														<xsl:with-param name="number" select="$CNBankAccounts.CNBankAccount.CreditorId.Telephone.ParsedTelephone.Number"/>
														<xsl:with-param name="xtn" select="$CNBankAccounts.CNBankAccount.CreditorId.Telephone.ParsedTelephone.Extension"/>
													</xsl:call-template>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankAccounts.CNBankAccount.DateOpened"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankAccounts.CNBankAccount.FigureAmount"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNBankAccounts.CNBankAccount.NumberOfNSF"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<xsl:element name="fo:table-cell">
												<xsl:if test="$CNBankAccounts.CNBankAccount.AccountType.description!=''">
													<xsl:element name="fo:table">
														<fo:table-column column-width="20mm"/>
														<fo:table-column column-width="165mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>Genre de compte</xsl:text>
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:text>D&#233;tail compte</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
															<xsl:element name="fo:table-row">
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:value-of select="$CNBankAccounts.CNBankAccount.AccountNumber"/>
																		<xsl:text>&#160;</xsl:text>
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
																	<fo:block>
																		<xsl:value-of select="$CNBankAccounts.CNBankAccount.AccountType.description"/>
																		<xsl:text>&#160;</xsl:text>
																	</fo:block>
																</fo:table-cell>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:if>
											</xsl:element>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell number-columns-spanned="8">
												<xsl:for-each select="$CNBankAccounts.CNBankAccount.Narratives">
													<xsl:variable name="CNBankAccounts.CNBankAccount.Narratives.Narrative" select="Narrative"/>
													<xsl:element name="fo:table">
														<fo:table-column column-width="185mm"/>
														<xsl:element name="fo:table-body">
															<xsl:element name="fo:table-row">
																<xsl:element name="fo:table-cell">
																	<xsl:for-each select="$CNBankAccounts.CNBankAccount.Narratives.Narrative">
																		<xsl:variable name="CNBankAccounts.CNBankAccount.Narratives.Narrative.code" select="@code"/>
																		<xsl:variable name="CNBankAccounts.CNBankAccount.Narratives.Narrative.description" select="@description"/>
																		<xsl:element name="fo:table">
																			<fo:table-column column-width="185mm"/>
																			<xsl:element name="fo:table-body">
																				<xsl:element name="fo:table-row">
																					<fo:table-cell  xsl:use-attribute-sets="table_values">
																						<fo:block>
																							Information : 	<xsl:value-of select="$CNBankAccounts.CNBankAccount.Narratives.Narrative.description"/>
																						</fo:block>
																					</fo:table-cell>
																				</xsl:element>
																			</xsl:element>
																		</xsl:element>
																	</xsl:for-each>
																</xsl:element>
															</xsl:element>
														</xsl:element>
													</xsl:element>
												</xsl:for-each>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of BankAccounts -->
	
	<!-- Start of LocalInquiries -->
	<xsl:template name="LocalInquiries">
		<xsl:variable name="CNLocalInquiries" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNLocalInquiries"/>
		<xsl:for-each select="$CNLocalInquiries">
			<xsl:variable name="CNLocalInquiries.CNLocalInquiry" select="CNLocalInquiry"/>
			<xsl:element name="fo:table">
				<xsl:copy use-attribute-sets="TableLeftFixedCR309"/>
				<fo:table-column column-width="30mm"/>
				<fo:table-column column-width="35mm"/>
				<fo:table-column column-width="40mm"/>
				<fo:table-column column-width="40mm"/>
				<fo:table-column column-width="40mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="5"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="5">
												<fo:block>Demandes</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No de membre</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Nom de membre</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No t&#233;l. (ext)</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Indicateur</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNLocalInquiries.CNLocalInquiry">
								<xsl:variable name="CNLocalInquiries.CNLocalInquiry.date" select="@date"/>
								<xsl:variable name="CNLocalInquiries.CNLocalInquiry.CustomerId.CustomerNumber" select="CustomerId/CustomerNumber"/>
								<xsl:variable name="CNLocalInquiries.CNLocalInquiry.CustomerId.Name" select="CustomerId/Name"/>
								<xsl:variable name="CNLocalInquiries.CNLocalInquiry.CustomerId.Telephone.ParsedTelephone.AreaCode" select="CustomerId/Telephone/ParsedTelephone/AreaCode"/>
								<xsl:variable name="CNLocalInquiries.CNLocalInquiry.CustomerId.Telephone.ParsedTelephone.Number" select="CustomerId/Telephone/ParsedTelephone/Number"/>
								<xsl:variable name="CNLocalInquiries.CNLocalInquiry.CustomerId.Telephone.ParsedTelephone.Extension" select="CustomerId/Telephone/ParsedTelephone/Extension"/>
								<xsl:variable name="CNLocalInquiries.CNLocalInquiry.ConsumerOnlyDisplayIndicator" select="ConsumerOnlyDisplayIndicator"/>
								<xsl:element name="fo:table">
									<xsl:copy use-attribute-sets="TableLeftFixedCR309"/>
										<fo:table-column column-width="30mm"/>
										<fo:table-column column-width="35mm"/>
										<fo:table-column column-width="40mm"/>
										<fo:table-column column-width="40mm"/>
										<fo:table-column column-width="40mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLocalInquiries.CNLocalInquiry.date"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLocalInquiries.CNLocalInquiry.CustomerId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLocalInquiries.CNLocalInquiry.CustomerId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:call-template name="format-telephone-extention">
														<xsl:with-param name="area" select="$CNLocalInquiries.CNLocalInquiry.CustomerId.Telephone.ParsedTelephone.AreaCode"/>
														<xsl:with-param name="number" select="$CNLocalInquiries.CNLocalInquiry.CustomerId.Telephone.ParsedTelephone.Number"/>
														<xsl:with-param name="xtn" select="$CNLocalInquiries.CNLocalInquiry.CustomerId.Telephone.ParsedTelephone.Extension"/>
													</xsl:call-template>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNLocalInquiries.CNLocalInquiry.ConsumerOnlyDisplayIndicator"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of LocalInquiries -->
	
	<!-- Start of ForeignInquiries -->
	<xsl:template name="ForeignInquiries">
		<xsl:variable name="CNForeignInquiries" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNForeignInquiries"/>
		<xsl:for-each select="$CNForeignInquiries">
			<xsl:variable name="CNForeignInquiries.CNForeignInquiry" select="CNForeignInquiry"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="50mm"/>
				<fo:table-column column-width="50mm"/>
				<fo:table-column column-width="85mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="3"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="3">							
							<xsl:element name="fo:block">Demandes par bureaux &#233;trangers</xsl:element>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Nom / No r&#233;f&#233;rence</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Description / Province</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNForeignInquiries.CNForeignInquiry">
								<xsl:variable name="CNForeignInquiries.CNForeignInquiry.date" select="@date"/>
								<xsl:variable name="CNForeignInquiries.CNForeignInquiry.CityNameOrInquiryNumber" select="CityNameOrInquiryNumber"/>
								<xsl:variable name="CNForeignInquiries.CNForeignInquiry.InquiryProvince" select="InquiryProvince"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="50mm"/>
									<fo:table-column column-width="50mm"/>
									<fo:table-column column-width="85mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNForeignInquiries.CNForeignInquiry.date"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNForeignInquiries.CNForeignInquiry.CityNameOrInquiryNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNForeignInquiries.CNForeignInquiry.InquiryProvince"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of ForeignInquiries -->
	
	<!-- Start of SpecialServices -->
	<xsl:template name="SpecialServices">
		<xsl:variable name="CNSpecialServices" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNSpecialServices"/>
		<xsl:for-each select="$CNSpecialServices">
			<xsl:variable name="CNSpecialServices.CNSpecialService" select="CNSpecialService"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="25mm"/>
				<fo:table-column column-width="40mm"/>
				<fo:table-column column-width="60mm"/>
				<fo:table-column column-width="60mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="4"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="4">
							<xsl:element name="fo:block">Services sp&#233;ciaux</xsl:element>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No de membre</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Nom du membre</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>No t&#233;l. (ext)</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNSpecialServices.CNSpecialService">
								<xsl:variable name="CNSpecialServices.CNSpecialService.DateReported" select="DateReported"/>
								<xsl:variable name="CNSpecialServices.CNSpecialService.CustomerId.CustomerNumber" select="CustomerId/CustomerNumber"/>
								<xsl:variable name="CNSpecialServices.CNSpecialService.CustomerId.Name" select="CustomerId/Name"/>
								<xsl:variable name="CNSpecialServices.CNSpecialService.CustomerId.Telephone.ParsedTelephone.AreaCode" select="CustomerId/Telephone/ParsedTelephone/AreaCode"/>
								<xsl:variable name="CNSpecialServices.CNSpecialService.CustomerId.Telephone.ParsedTelephone.Number" select="CustomerId/Telephone/ParsedTelephone/Number"/>
								<xsl:variable name="CNSpecialServices.CNSpecialService.CustomerId.Telephone.ParsedTelephone.Extension" select="CustomerId/Telephone/ParsedTelephone/Extension"/>
								<xsl:variable name="CNSpecialServices.CNSpecialService.description" select="@description"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="25mm"/>
									<fo:table-column column-width="40mm"/>
									<fo:table-column column-width="60mm"/>
									<fo:table-column column-width="60mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSpecialServices.CNSpecialService.DateReported"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSpecialServices.CNSpecialService.CustomerId.CustomerNumber"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNSpecialServices.CNSpecialService.CustomerId.Name"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:call-template name="format-telephone-extention">
														<xsl:with-param name="area" select="$CNSpecialServices.CNSpecialService.CustomerId.Telephone.ParsedTelephone.AreaCode"/>
														<xsl:with-param name="number" select="$CNSpecialServices.CNSpecialService.CustomerId.Telephone.ParsedTelephone.Number"/>
														<xsl:with-param name="xtn" select="$CNSpecialServices.CNSpecialService.CustomerId.Telephone.ParsedTelephone.Extension"/>
													</xsl:call-template>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values" number-columns-spanned="4">
												<fo:block>
														Raison : <xsl:value-of select="$CNSpecialServices.CNSpecialService.description"/>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- End of SpecialServices -->
	
	<!-- Start of ConsumerStatements -->
	<xsl:template name="ConsumerStatements">
		<xsl:variable name="CNConsumerStatements" select="EfxReport/CNConsumerCreditReports/CNConsumerCreditReport/CNConsumerStatements"/>
		<xsl:for-each select="$CNConsumerStatements">
			<xsl:variable name="CNConsumerStatements.CNConsumerStatement" select="CNConsumerStatement"/>
			<xsl:element name="fo:table">
				<fo:table-column column-width="50mm"/>
				<fo:table-column column-width="50mm"/>
				<fo:table-column column-width="85mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell number-columns-spanned="3"><fo:block>
							<xsl:text>&#xa;&#160;</xsl:text> </fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="elesubtitre" number-columns-spanned="3">
							<fo:block>D&#233;claration du consommateur</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date report&#233;e</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>Date d'annulation</xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell  xsl:use-attribute-sets="eletitregauche">
							<fo:block>
								<xsl:text>D&#233;tails</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:for-each select="$CNConsumerStatements.CNConsumerStatement">
								<xsl:variable name="CNConsumerStatements.CNConsumerStatement.DateReported" select="DateReported"/>
								<xsl:variable name="CNConsumerStatements.CNConsumerStatement.DateToBePurged" select="DateToBePurged"/>
								<xsl:variable name="CNConsumerStatements.CNConsumerStatement.Statement" select="Statement"/>
								<xsl:element name="fo:table">
									<fo:table-column column-width="50mm"/>
									<fo:table-column column-width="50mm"/>
									<fo:table-column column-width="85mm"/>
									<xsl:element name="fo:table-body">
										<xsl:element name="fo:table-row">
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNConsumerStatements.CNConsumerStatement.DateReported"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNConsumerStatements.CNConsumerStatement.DateToBePurged"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
											<fo:table-cell  xsl:use-attribute-sets="table_values">
												<fo:block>
													<xsl:value-of select="$CNConsumerStatements.CNConsumerStatement.Statement"/>
													<xsl:text>&#160;</xsl:text>
												</fo:block>
											</fo:table-cell>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="EndOfReport">
		<xsl:element name="fo:table">
			<fo:table-column column-width="185mm"/>
				<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
						<fo:table-cell>
							<fo:block>
								<xsl:text>&#xa;&#160;</xsl:text>
								<xsl:text>&#xa;&#160;</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
					<xsl:element name="fo:table-row">
						<fo:table-cell   xsl:use-attribute-sets="TitleFontAttr">
							<fo:block>
								<xsl:text>Fin du rapport</xsl:text>
							</fo:block>
						</fo:table-cell>
					</xsl:element>
				</xsl:element>
			</xsl:element>
	</xsl:template>
	<xsl:template name="format-telephone-extention">
		<xsl:param name="area"/>
		<xsl:param name="number"/>
		<xsl:param name="xtn"/>
		<xsl:if test="$area">
			<xsl:value-of select="$area"/>-
		</xsl:if>
		<xsl:if test="$number">
			<xsl:value-of select="$number"/>
		</xsl:if>
		<xsl:if test="$xtn">
			(<xsl:value-of select="$xtn"/>)
		 </xsl:if>
	</xsl:template>
	<!-- Vandana End 2 -->
    
</xsl:stylesheet>
