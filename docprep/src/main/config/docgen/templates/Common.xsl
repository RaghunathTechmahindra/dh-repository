<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
09/May/2008 DVG #DG720 CTFS - CR203 - new form for requisiton of Funds
04/Mar/2008 DVG #DG704 FXP20631: DJ POS, express doc prep - deal with 7 applicants, only 4 show on some docs 
05/Feb/2008 DVG #DG696 FXP20239: Doc CR039 property address uses french format in english doc 
05/Feb/2008 DVG #DG692 FXP20231: Annexe � l�offre de financement all check boxes are checked in english and french
03/Jan/2008 DVG #DG678 FXP19663: Desjardins - CR334B - Changes for emili low ratio program - Formulaire d�analyse 
10/Dec/2007 DVG #DG672 DJ Branch docs./Sophy  project
27/Nov/2006 DVG #DG542 #5223  CR053 - English document  
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:fox="http://xml.apache.org/fop/extensions"
	xmlns:File="xalan://java.io.File"
  xmlns:java="http://xml.apache.org/xalan/java" 
  >

  <!-- relative logo file name
   eg. file:///C:/mosdocprep/admin_CTF/docgen/templates/WFCommitmentLetterFO.xsl 
   -->
  <xsl:variable name="logoPatha" select="document-location()" />
	<xsl:variable name="logoPathf" select="File:new($logoPatha)"/>
  <xsl:variable name="logoPath" select="File:getParent($logoPathf)"/>

	<!-- ================  Attribute sets ============== -->
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBA1.5Optimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ListLabelIndent">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="padding-left">-3mm</xsl:attribute>
	</xsl:attribute-set>
	<!-- ================  Attribute sets ============== -->


  <!-- relative logo file name
  
  input: 
  $language     : language id (global var)
  $lenderName   : lender name 
  $imageType    : logo image type: gif, jpg, ... 

  output
  $logoFile     : this is the logo file name complete with path, language and extension
    
   eg. file:///C:/mosdocprep/admin_CTF/docgen/templates/WFCommitmentLetterFO.xsl 
   -->
  <xsl:template name="getLogoFileName">
		<xsl:param name="lenderName" select="'DJLogo'"/><!-- default is DJ just cuz has many templates -->
		<xsl:param name="imageType" select="'jpg'"/>
  
    <!-- remove undesirable file name chars  'Ge Money Financial' -->	
  	<xsl:variable name="lendName" select="translate($lenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />
    <!-- add language and extension  'GeMoneyFinancial' -->	
  	<xsl:variable name="lendNameLang">	
  		<xsl:choose>
  			<xsl:when test="$language = 1">
  		    <xsl:value-of select="concat($lendName,'_Fr')"/>
        </xsl:when>
  			<xsl:otherwise>
  		    <xsl:value-of select="concat($lendName,'_En')"/>
        </xsl:otherwise>
  		</xsl:choose>
  	</xsl:variable>	
    <!-- add \ and . 'GeMoneyFinancial_Fr' -->	
  	<xsl:variable name="logoFilea" select="concat('\',concat($lendNameLang,'.'))" />
    <!-- add image type 'GeMoneyFinancial_Fr.' -->	
    <xsl:variable name="logoFileb" select="concat($logoFilea,$imageType)"/>
    <!-- add full path 'GeMoneyFinancial_Fr.jpg' -->
    <xsl:variable name="logoFile" select="concat($logoPath,$logoFileb)"/>
    <!-- return 'file:///C:/Dev/MOS/Development/Framework/../admin/docgen/templates/GeMoneyFinancial_Fr.jpg' -->
    <xsl:value-of select="$logoFile"/>
  </xsl:template>

	<xsl:template name="ContactFullName">
		<xsl:param name="saluta" select="''"/>	
		<xsl:if test="$saluta != ''">
			<xsl:value-of select="$saluta"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="contactFirstName"/>
		<xsl:text> </xsl:text>							
		<xsl:if test="contactMiddleInitial!= ''">
			<xsl:value-of select="contactMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>							
		<xsl:value-of select="contactLastName"/>
	</xsl:template>

  <!-- list main borrowers and primary borrower address -->
	<xsl:template name="CreateBorrowerLines">
		<fo:table xsl:use-attribute-sets="TableLeftFixed">
			<fo:table-column column-width="142mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="PaddingAll2mm MainFontAttr" padding-left="0mm" 
            space-after="2pt" space-before="2pt" keep-together="always">
							<xsl:call-template name="BorrowersListedBlocks4only"/>
							<xsl:call-template name="PrimBorrAddress"/>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
	  </fo:table>
	</xsl:template>

  <!-- list main borrowers in separate lines -->
	<xsl:template name="BorrowersListedBlocks4only">
		<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId=0]">
  		<fo:block><!-- #DG704 minor -->
  		  <xsl:choose>     
         <xsl:when test="position()  &lt; 5">
  				<xsl:element name="fo:block">
  					<xsl:call-template name="BorrowerFullName"/>
  				</xsl:element>
         </xsl:when>
         <xsl:when test="position() = 5">       
        		<xsl:choose>
        			<xsl:when test="$language = 1">
                <xsl:text> et al.</xsl:text>        
              </xsl:when>
        			<xsl:otherwise>
      					<xsl:text> and others</xsl:text>
              </xsl:otherwise>
        		</xsl:choose>				       
         </xsl:when>
        </xsl:choose>
  		</fo:block>
		</xsl:for-each>
	</xsl:template>

  <!-- list main borrowers in same line 
  input: 
  $language     : language id (global var)
  -->
	<xsl:template name="BorrowersListed">
    <xsl:variable name="conector">     
  		<xsl:choose>
  			<xsl:when test="$language = 1">
					<xsl:text> et </xsl:text>
        </xsl:when>
  			<xsl:otherwise>
					<xsl:text> and </xsl:text>
        </xsl:otherwise>
  		</xsl:choose>				
    </xsl:variable>	
		<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId=0]">
			<xsl:choose>
				<xsl:when test="position() =1"/>
				<xsl:when test="position()=last()">
				  <xsl:value-of select="$conector" />      
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>, </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial!= ''"><!-- #DG720 -->
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

  <!-- @todo replace occurences, if any, with 'ContactFullName' -->
	<xsl:template name="ContactName">
    <xsl:call-template name="ContactFullName"/>
	</xsl:template>

	<xsl:template name="PrimBorrAddress">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:if test="position()=1">
				<xsl:for-each select="BorrowerAddress[borrowerAddressTypeId=0]">
  				<xsl:call-template name="doAddress"/>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

  <!-- #DG720 allow use of 'Addr'-->
	<xsl:template name="doAddress">
		<xsl:for-each select="./Address">
			<xsl:call-template name="doAddres"/>
    </xsl:for-each>
	</xsl:template>

  <!-- output 1 address -->
	<xsl:template name="doAddres">
		<fo:block>
			<xsl:value-of select="addressLine1"/>
		</fo:block>
		<xsl:if test="addressLine2!=''">
  		<fo:block>
  			<xsl:value-of select="addressLine2"/>
  		</fo:block>
    </xsl:if>
		<fo:block>
			<xsl:value-of select="city"/>
			<xsl:text> (</xsl:text>
			<xsl:value-of select="province"/>
			<xsl:text>)&#160;&#160;</xsl:text>
			<xsl:value-of select="postalFSA"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="postalLDU"/>
		</fo:block>
	</xsl:template>

	<!-- Subject property, formatting of address
  input: 
  $language     : language id (global var)
  -->
	<xsl:template match="Property">
		<fo:block keep-together="always">
      <!-- #DG696 -->		
      <xsl:choose>
        <xsl:when test="$language=1">
    			<xsl:if test="propertyStreetNumber!=''">
      			<xsl:value-of select="propertyStreetNumber"/>
      			<xsl:text>, </xsl:text>
    			</xsl:if>
    			<xsl:if test="streetType!=''">
    				<xsl:text>  </xsl:text>
    				<xsl:value-of select="streetType"/>
    			</xsl:if>
   				<xsl:text>  </xsl:text>
    			<xsl:value-of select="propertyStreetName"/>
    			<xsl:if test="streetDirection!=''">
    				<xsl:text>  </xsl:text>
    				<xsl:value-of select="streetDirection"/>
    			</xsl:if>
    			<xsl:if test="unitNumber">
            <xsl:text>, unit� </xsl:text>
            <xsl:value-of select="unitNumber"/>
    			</xsl:if>
        </xsl:when>
        <xsl:otherwise>
    			<xsl:if test="propertyStreetNumber!=''">
      			<xsl:value-of select="propertyStreetNumber"/>
   				  <xsl:text>  </xsl:text>
    			</xsl:if>
    			<xsl:value-of select="propertyStreetName"/>
    			<xsl:if test="streetDirection!=''">
    				<xsl:text>  </xsl:text>
    				<xsl:value-of select="streetDirection"/>
    			</xsl:if>
    			<xsl:if test="streetType!=''">
    				<xsl:text>  </xsl:text>
    				<xsl:value-of select="streetType"/>
    			</xsl:if>
    			<xsl:if test="unitNumber">
            <xsl:text>, unit </xsl:text>
            <xsl:value-of select="unitNumber"/>
    			</xsl:if>
        </xsl:otherwise> 
      </xsl:choose> 
        <xsl:if test="propertyAddressLine2!=''">
         <fo:block>
          <xsl:value-of select="propertyAddressLine2"/>
         </fo:block>
        </xsl:if>
  		<fo:block>
  			<xsl:value-of select="propertyCity"/>
  			<xsl:text> (</xsl:text>
  			<xsl:value-of select="province"/>
  			<xsl:text>)&#160;&#160;</xsl:text>
  			<xsl:value-of select="propertyPostalFSA"/>
  			<xsl:text>&#160;&#160;</xsl:text>
  			<xsl:value-of select="propertyPostalLDU"/>
  		</fo:block>
 		</fo:block>
	</xsl:template>

  <!-- return nr. of years and months  
  input: 
  $language     : language id (global var)
  $amortize   : amortize term in months
   -->
  <xsl:template name="getYearMonth">
		<xsl:param name="amortize"/>
  
  	<xsl:variable name="montha" select="$amortize mod 12" />
  	<xsl:variable name="yeara" select="($amortize - $montha) div 12" />
  	
  	<xsl:variable name="ret">	
  		<xsl:choose>
  			<xsl:when test="$language = 1">
         	<xsl:value-of select="$yeara"/>
  			  <xsl:text> ans et </xsl:text>
        	<xsl:value-of select="$montha"/>
        	<xsl:text> mois</xsl:text>
        </xsl:when>
  			<xsl:otherwise>
         	<xsl:value-of select="$yeara"/>
  			  <xsl:text> year(s) and </xsl:text>
        	<xsl:value-of select="$montha"/>
        	<xsl:text> month(s)</xsl:text>
        </xsl:otherwise>
  		</xsl:choose>
  	</xsl:variable>	
    <xsl:value-of select="$ret"/>
  </xsl:template>
  <!-- #DG672 end -->

  <!-- #DG678 -->
  <xsl:template name="checkBox">
		<xsl:param name="chek" select="true"/>
		<xsl:param name="boxSize" select="9"/>	
		<fo:instream-foreign-object width="{$boxSize}px" height="{$boxSize}px">
        <!-- content-height="scale-to-fit" content-width="scale-to-fit" > -->
			<svg xmlns="http://www.w3.org/2000/svg" width="{$boxSize}px" height="{$boxSize}px">
				<rect x="0" y="0" width="{$boxSize}" height="{$boxSize}" style="fill: none; stroke: black;"/>
				<xsl:if test="$chek">
  				<line x1="0" y1="0" x2="{$boxSize}" y2="{$boxSize}" style="stroke:black;"/>
  				<line x1="0" y1="{$boxSize}" x2="{$boxSize}" y2="0" style="stroke:black;"/>     
        </xsl:if>
			</svg>
		</fo:instream-foreign-object>
  </xsl:template>
  
  <!-- kept just for optimization and back compatibility -->
	<xsl:template name="CheckedCheckbox">
		<xsl:param name="boxSize" select="9"/>	
		<fo:instream-foreign-object width="{$boxSize}px" height="{$boxSize}px">        
			<svg xmlns="http://www.w3.org/2000/svg" width="{$boxSize}px" height="{$boxSize}px">
				<rect x="0" y="0" width="{$boxSize}" height="{$boxSize}" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="{$boxSize}" y2="{$boxSize}" style="stroke:black;"/>
				<line x1="0" y1="{$boxSize}" x2="{$boxSize}" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
  
  <!-- kept just for optimization and back compatibility -->
	<xsl:template name="UnCheckedCheckbox">
		<xsl:param name="boxSize" select="9"/>	
		<fo:instream-foreign-object width="{$boxSize}px" height="{$boxSize}px">
			<svg xmlns="http://www.w3.org/2000/svg" width="{$boxSize}px" height="{$boxSize}px">
				<rect x="0" y="0" width="{$boxSize}" height="{$boxSize}" style="fill: none; stroke: black;"/>
				<!-- #DG692 -->
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>    
  <!-- #DG678 end -->

</xsl:stylesheet>
