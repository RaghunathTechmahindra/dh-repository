<?xml version="1.0" encoding="UTF-8"?>
<!--
27/Nov/2006 DVG #DG550 #5239  CR 057E - English document  
22/Feb/2005 DVG DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik
  #4931  DJ 3.1 rollout - Deny deal document not sent to GM and sales Force 
prepared by Catherine Rutgaizer 
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions"
	xmlns:File="xalan://java.io.File">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	
	<xsl:output method="xml" version="1.0" encoding="UTF-8"/>
	
  <!--#DG550 relative logo file name-->
  <!-- eg. file:///C:/mosdocprep/admin_CTF/docgen/templates/WFCommitmentLetterFO.xsl -->
  <xsl:variable name="logoPatha" select="document-location()" />
	<xsl:variable name="logoPathf" select="File:new($logoPatha)"/>
  <xsl:variable name="logoPath" select="File:getParent($logoPathf)"/>  
	<xsl:variable name="logoFilea" select="'\DJLogo_en.gif'" />
	<xsl:variable name="logoFile" select="concat($logoPath,$logoFilea)" />

	<xsl:template match="/">
	  <xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	
	<!-- define attribute sets -->
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>

  <!-- #DG550 -->
	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<xsl:template name="BorrAddress">
		<xsl:element name="fo:block">
			<xsl:value-of select="addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="city"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="province"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="postalFSA"/>
			<xsl:text>  </xsl:text>
			<xsl:value-of select="postalLDU"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="ContactName">
		<xsl:param name="saluta" select="''"/>	
		<xsl:if test="$saluta != ''">
			<xsl:value-of select="$saluta"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="contactFirstName"/>
		<xsl:text> </xsl:text>							
		<xsl:if test="contactMiddleInitial!= ''">
			<xsl:value-of select="contactMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>							
		<xsl:value-of select="contactLastName"/>
	</xsl:template>

  <!-- #DG550 end -->

	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateNameAddressLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
				<!-- #DG550 xsl:call-template name="CreateCommentLines"/-->
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<!--#DG550
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">138mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="SpaceBAOptimum"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
              xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element-->
		<fo:block>
			<xsl:copy use-attribute-sets="SpaceBAOptimum"/>
			<xsl:attribute name="keep-together">always</xsl:attribute>
		  <fo:external-graphic src="{$logoFile}" height="28mm" width="73mm"/>
		</fo:block>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
    <!--#DG550 
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">10mm</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">45mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">80mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								Notice – Mortgage Refusal 
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element-->
		<xsl:element name="fo:block">
			<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="padding-top">4mm</xsl:attribute>
			<xsl:attribute name="padding-bottom">4mm</xsl:attribute>
			<xsl:attribute name="margin-left">21mm</xsl:attribute>
			<xsl:attribute name="margin-right">21mm</xsl:attribute>
			<xsl:attribute name="text-align">center</xsl:attribute>
			<xsl:attribute name="border">solid black 1px</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="font-family">Times Roman</xsl:attribute>
			<xsl:attribute name="font-size">12pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
					Notice – Mortgage Refusal as submitted
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">138mm</xsl:attribute><!-- #DG550 -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
				    <xsl:copy use-attribute-sets="MainFontAttr"/>
				    <xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-after">2em</xsl:attribute>
							<xsl:attribute name="space-before">2em</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="/*/General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="space-before">0.5cm</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">121mm</xsl:attribute><!-- #DG550 -->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
				    <xsl:copy use-attribute-sets="PaddingAll2mm"/>
				    <xsl:copy use-attribute-sets="MainFontAttr"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Subject: 
							</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
				    <xsl:copy use-attribute-sets="PaddingAll2mm"/>
				    <xsl:copy use-attribute-sets="MainFontAttr"/>
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Mortgage financing application 
							</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>								 
								File number:  <xsl:text> </xsl:text>
							<xsl:value-of select="/*/Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateNameAddressLines">
     <!--#DG550 
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">138mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<!- - Borrowers - ->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">0.5em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower">
								xsl:value-of select="./salutation"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="./borrowerFirstName"/>
								<xsl:text> </xsl:text>
								<xsl:if test="./borrowerMiddleInitial">
									<xsl:value-of select="./borrowerMiddleInitial"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:value-of select="./borrowerLastName"/>
								<xsl:element name="fo:block">
									<xsl:text> </xsl:text>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
						<!- - prim Borrower Address - ->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">0.5em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower">
								<xsl:if test="position()=1">
									<xsl:for-each select="./BorrowerAddress">
										<xsl:if test="./borrowerAddressTypeId='0'">
											<xsl:value-of select="./Address/addressLine1"/>
											<xsl:text> </xsl:text>
											<xsl:if test="./Address/addressLine2">
												<xsl:element name="fo:block">
													<xsl:value-of select="./Address/addressLine2"/>
													<xsl:text> </xsl:text>
												</xsl:element>
											</xsl:if>
											<xsl:element name="fo:block">
												<xsl:value-of select="./Address/city"/>, <xsl:value-of select="./Address/province"/>
												<xsl:text> </xsl:text>
											</xsl:element>
											<xsl:element name="fo:block">
												<xsl:value-of select="./Address/postalFSA"/>
												<xsl:text> </xsl:text>
												<xsl:value-of select="./Address/postalLDU"/>
											</xsl:element>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element-->		
		<fo:block>
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">0.5em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
  		<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower">
  			<xsl:element name="fo:block">
  	      <xsl:call-template name="BorrowerFullName"/>
  			</xsl:element>
  		</xsl:for-each>
  		<fo:block>
    		<xsl:attribute name="space-before.optimum">0.5em</xsl:attribute>
    		<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
    		<xsl:attribute name="keep-together">always</xsl:attribute>
    		<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[1]
    			/BorrowerAddress[borrowerAddressTypeId=0]/Address">
    	    <xsl:call-template name="BorrAddress"/>
    		</xsl:for-each>
  		</fo:block>
		</fo:block>
		
	</xsl:template>

	<!-- #DG550 not used 
  xsl:template name="CreateCommentLines">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="space-before.optimum">18pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<!- - Commentaire(s) - ->
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:element name="fo:list-block">
				<xsl:attribute name="margin-left">5mm</xsl:attribute>
				<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
				<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
				<xsl:apply-templates select="/*/Deal/DealNotes/DealNote"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="DealNote">
		<xsl:if test="dealNotesCategoryId='2'">
			<xsl:if test="dealNotesDate=/*/General/CurrentDate">
				<xsl:element name="fo:list-item">
					<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
					<xsl:element name="fo:list-item-label">
						<xsl:element name="fo:block">&#x2022;</xsl:element>
					</xsl:element>
					<xsl:element name="fo:list-item-body">
						<xsl:attribute name="start-indent">body-start()</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">justify</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="dealNotesText"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:if>
		</xsl:if>
	</xsl:template-->

	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">15pt</xsl:attribute><!--#DG550 -->
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>								 
		  Dear Sir or Madam:
	    </xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">15pt</xsl:attribute><!--#DG550 -->
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			We regret to inform you that after studying your file, we unfortunately cannot authorize your 
      mortgage financing application <!--#DG550 -->as submitted.
	    </xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">7pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			For any information regarding your file, please contact your mortgage representative,    
      <!--#DG550 
      xsl:if test="/*/Deal/SourceOfBusinessProfile/Contact/salutation">
				<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/salutation"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:value-of select="concat(/*/Deal/SourceOfBusinessProfile/Contact/contactFirstName , '   ', /*/Deal/SourceOfBusinessProfile/Contact/contactLastName ) "/-->
		  <xsl:for-each select="/*/Deal/SourceOfBusinessProfile/Contact">
 		    <xsl:call-template name="ContactName">
          <xsl:with-param name="saluta" select="salutation"/>
        </xsl:call-template>
		  </xsl:for-each>
      , at the following telephone number: 
			<!--#DG550 xsl:element name="fo:block">
				<xsl:attribute name="keep-together">always</xsl:attribute-->
				<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>
        .
      <!--#DG550 /xsl:element-->
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">22pt</xsl:attribute><!--#DG550 -->
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
  		Yours truly,
    </xsl:element>
	</xsl:template>

	<xsl:template name="CreateUnderwriterLines">
    <!--#DG550 
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">138mm</xsl:attribute><!- - #DG550 - ->
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
              <xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select=" concat(/*/Deal/underwriter/Contact/contactFirstName , '   ', /*/Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="/*/Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">30mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">100mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:copy use-attribute-sets="PaddingAll0mm"/>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Telephone:	
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumber"/>
											<xsl:choose>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='173'"> or 1-888-281-4420 ext:  <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<!- - <xsl:when test="/*/Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999 ext:  <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when> - ->
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='175'"> or 1-800-728-2728 ext:  <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text> </xsl:text>ext:  <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Fax:
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="/*/Deal/underwriter/Contact/contactFaxNumber"/>
											<xsl:choose>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='173'"> or 1-888-888-7777</xsl:when>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999</xsl:when>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='175'"> or 1-888-888-8888</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element-->
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">0mm</xsl:attribute>
			Claudine Otis, Manager, Credit Administration
		</xsl:element>
	</xsl:template>

	<xsl:template name="FOStart">
		<!-- DG#140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- DG#140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<!--#DG550 adjust width 
					xsl:attribute name="page-height">11in</xsl:attribute-->
					<xsl:attribute name="page-height">279mm</xsl:attribute>					
					<!--#DG550 adjust width 
          xsl:attribute name="page-width">8.5in</xsl:attribute-->
					<xsl:attribute name="margin-top">0mm</xsl:attribute>
					<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
					<xsl:attribute name="margin-left">0mm</xsl:attribute>
					<xsl:attribute name="margin-right">25mm</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">30mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
