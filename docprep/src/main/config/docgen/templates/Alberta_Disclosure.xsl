<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output version="1.0" method="html" encoding="UTF-8" indent="no"/>
	<xsl:variable name="fo:layout-master-set">
		<fo:layout-master-set>
			<fo:simple-page-master master-name="default-page" page-height="11in" page-width="8.5in" margin-left="0.5in" margin-right="0.48in">
			    <fo:region-before extent="0.79in" />
				<fo:region-body margin-top="0.1in" margin-bottom="0.1in"/>
			</fo:simple-page-master>
		</fo:layout-master-set>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:variable name="maxwidth" select="7.52000"/>
		<fo:root>
			<xsl:copy-of select="$fo:layout-master-set"/>
			<fo:page-sequence master-reference="default-page" initial-page-number="1" format="1">
  			    <xsl:call-template name="headerall" />
				<fo:flow flow-name="xsl-region-body">
					<fo:block>					
						<xsl:variable name="tablewidth0" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths0" select="0.05208 + 9.42708"/>
						<xsl:variable name="factor0">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths0 &gt; 0.00000 and $sumcolumnwidths0 &gt; $tablewidth0">
									<xsl:value-of select="$tablewidth0 div $sumcolumnwidths0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns0" select="1"/>
						<xsl:variable name="defaultcolumnwidth0">
							<xsl:choose>
								<xsl:when test="$factor0 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns0 &gt; 0">
									<xsl:value-of select="($tablewidth0 - $sumcolumnwidths0) div $defaultcolumns0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth0_0" select="0.05208 * $factor0"/>
						<xsl:variable name="columnwidth0_1" select="9.42708 * $factor0"/>
						<xsl:variable name="columnwidth0_2" select="$defaultcolumnwidth0"/>
						<fo:table width="{$tablewidth0}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth0_0}in"/>
							<fo:table-column column-width="{$columnwidth0_1}in"/>
							<fo:table-column column-width="{$columnwidth0_2}in"/>
							<fo:table-body font-size="11pt">
								<fo:table-row>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" text-align="center" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">										
										<fo:block padding-top="1pt" padding-bottom="1pt"/>											
									</fo:table-cell>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>								
								<fo:table-row>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" text-align="center" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">										
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>FORM 2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" text-align="center" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell text-align="center" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="11pt">
												<xsl:text>GOVERNMENT OF THE PROVINCE OF ALBERTA</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="11pt">
												<xsl:text>STATEMENT OF DISCLOSURE</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="11pt">
												<xsl:text>FOR MORTGAGE ON REAL PROPERTY</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="11pt">
												<xsl:text>UNDER THE CONSUMER CREDIT TRANSACTION ACT</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.07292in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.07292in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.07292in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth1" select="$columnwidth0_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths1" select="1.32292 + 4.67708"/>
											<xsl:variable name="factor1">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths1 &gt; 0.00000 and $sumcolumnwidths1 &gt; $tablewidth1">
														<xsl:value-of select="$tablewidth1 div $sumcolumnwidths1"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns1" select="1"/>
											<xsl:variable name="defaultcolumnwidth1">
												<xsl:choose>
													<xsl:when test="$factor1 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns1 &gt; 0">
														<xsl:value-of select="($tablewidth1 - $sumcolumnwidths1) div $defaultcolumns1"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth1_0" select="1.32292 * $factor1"/>
											<xsl:variable name="columnwidth1_1" select="4.67708 * $factor1"/>
											<xsl:variable name="columnwidth1_2" select="$defaultcolumnwidth1"/>
											<fo:table width="{$tablewidth1}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth1_0}in"/>
												<fo:table-column column-width="{$columnwidth1_1}in"/>
												<fo:table-column column-width="{$columnwidth1_2}in"/>
												<fo:table-body font-size="9pt">
													<fo:table-row>
														<fo:table-cell padding-left="0.15000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>DATE:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DISC_DATA_1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-left="0.15000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>LENDER:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DSCL_LNDR_OFFCE_1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-left="0.15000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>ADDRESS:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DSCL_LENDADDR_1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-left="0.15000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>BORROWER(S):</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DSCL_BOR_COBOR_1"/>
																</fo:inline>
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>&#160;&#160;</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-left="0.15000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>PROPERTY:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DSCL_ADRSPRPTY_1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-left="0.15000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DSCL_DWELLTYPE_1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.14583in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.14583in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.14583in">
										
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth2" select="$columnwidth0_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths2" select="0.41667 + 4.67708"/>
											<xsl:variable name="factor2">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths2 &gt; 0.00000 and $sumcolumnwidths2 &gt; $tablewidth2">
														<xsl:value-of select="$tablewidth2 div $sumcolumnwidths2"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns2" select="1"/>
											<xsl:variable name="defaultcolumnwidth2">
												<xsl:choose>
													<xsl:when test="$factor2 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns2 &gt; 0">
														<xsl:value-of select="($tablewidth2 - $sumcolumnwidths2) div $defaultcolumns2"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth2_0" select="0.41667 * $factor2"/>
											<xsl:variable name="columnwidth2_1" select="4.67708 * $factor2"/>
											<xsl:variable name="columnwidth2_2" select="$defaultcolumnwidth2"/>
											<fo:table width="{$tablewidth2}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth2_0}in"/>
												<fo:table-column column-width="{$columnwidth2_1}in"/>
												<fo:table-column column-width="{$columnwidth2_2}in"/>
												<fo:table-body font-size="9pt">
													<fo:table-row>
														<fo:table-cell display-align="center" text-align="center">
															<fo:block >
																<fo:inline font-size="9pt">
																	<xsl:text>1.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="before" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth3" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths3" select="1.31042 + 0.9 + 2.80000 + 0.78125 + 0.11458"/>
																<xsl:variable name="factor3">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths3 &gt; 0.00000 and $sumcolumnwidths3 &gt; $tablewidth3">
																			<xsl:value-of select="$tablewidth3 div $sumcolumnwidths3"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns3" select="1"/>
																<xsl:variable name="defaultcolumnwidth3">
																	<xsl:choose>
																		<xsl:when test="$factor3 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns3 &gt; 0">
																			<xsl:value-of select="($tablewidth3 - $sumcolumnwidths3) div $defaultcolumns3"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth3_0" select="1.31042 * $factor3"/>
																<xsl:variable name="columnwidth3_1" select="0.9 * $factor3"/>
																<xsl:variable name="columnwidth3_2" select="2.80000 * $factor3"/>
																<xsl:variable name="columnwidth3_3" select="0.78125 * $factor3"/>
																<xsl:variable name="columnwidth3_4" select="0.11458 * $factor3"/>
																<xsl:variable name="columnwidth3_5" select="$defaultcolumnwidth3"/>
																<fo:table width="{$tablewidth3}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth3_0}in"/>
																	<fo:table-column column-width="{$columnwidth3_1}in"/>
																	<fo:table-column column-width="{$columnwidth3_2}in"/>
																	<fo:table-column column-width="{$columnwidth3_3}in"/>
																	<fo:table-column column-width="{$columnwidth3_4}in"/>
																	<fo:table-column column-width="{$columnwidth3_5}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="3">
																				<fo:block>
																					<fo:inline font-size="9pt">
																						<xsl:text>Principal amount of the</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DISC_TYPMTG_2">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DISC_TYPMTG_2"/>&#160;
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>mortgage to be repaid by the borrower:</xsl:text>
																					</fo:inline>																					
																				</fo:block>
																			</fo:table-cell>																			
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DSCL_MTGAMT_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block >
																<fo:inline font-size="9pt">
																	<xsl:text>2.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth4" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths4" select="2.79167 + 3.00000 + 0.11458"/>
																<xsl:variable name="factor4">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths4 &gt; 0.00000 and $sumcolumnwidths4 &gt; $tablewidth4">
																			<xsl:value-of select="$tablewidth4 div $sumcolumnwidths4"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns4" select="1"/>
																<xsl:variable name="defaultcolumnwidth4">
																	<xsl:choose>
																		<xsl:when test="$factor4 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns4 &gt; 0">
																			<xsl:value-of select="($tablewidth4 - $sumcolumnwidths4) div $defaultcolumns4"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth4_0" select="2.79167 * $factor4"/>
																<xsl:variable name="columnwidth4_1" select="3.00000 * $factor4"/>
																<xsl:variable name="columnwidth4_2" select="0.11458 * $factor4"/>
																<xsl:variable name="columnwidth4_3" select="$defaultcolumnwidth4"/>
																<fo:table width="{$tablewidth4}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth4_0}in"/>
																	<fo:table-column column-width="{$columnwidth4_1}in"/>
																	<fo:table-column column-width="{$columnwidth4_2}in"/>
																	<fo:table-column column-width="{$columnwidth4_3}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>Deduct charges, fees, etc. where applicable:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block />
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DSCL_FEES_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block />
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" padding-left="4pt">
															<fo:block >
																<fo:inline font-size="9pt">
																	<xsl:text>(This amount must equal the total of the amount under section 9.)</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center" padding-top="4pt">
															<fo:block >
																<fo:inline font-size="9pt">
																	<xsl:text>3.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block >
																<xsl:variable name="tablewidth5" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths5" select="5.79167 + 0.11458"/>
																<xsl:variable name="factor5">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths5 &gt; 0.00000 and $sumcolumnwidths5 &gt; $tablewidth5">
																			<xsl:value-of select="$tablewidth5 div $sumcolumnwidths5"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns5" select="1"/>
																<xsl:variable name="defaultcolumnwidth5">
																	<xsl:choose>
																		<xsl:when test="$factor5 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns5 &gt; 0">
																			<xsl:value-of select="($tablewidth5 - $sumcolumnwidths5) div $defaultcolumns5"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth5_0" select="5.79167 * $factor5"/>
																<!-- xsl:variable name="columnwidth5_1" select="0.12458 * $factor5"/-->
																<xsl:variable name="columnwidth5_2" select="0.11458 * $factor5"/>
																<xsl:variable name="columnwidth5_3" select="$defaultcolumnwidth5"/>
																<fo:table width="{$tablewidth5}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth5_0}in"/>
																	<!-- fo:table-column column-width="{$columnwidth5_1}in"/-->
																	<fo:table-column column-width="{$columnwidth5_2}in"/>
																	<fo:table-column column-width="{$columnwidth5_3}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell padding-top="4pt">
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>Amount of money to be paid to the borrower or the amount to be disbursed on his direction:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="4pt">
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="4pt">
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DSCL_AMOUNT_2"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center" padding-top="4pt">
															<fo:block  >
																<fo:inline font-size="9pt">
																	<xsl:text>4.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" padding-top="4pt">
															<fo:block >
																<xsl:variable name="tablewidth6" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths6" select="2.73083 + 0.64375 + 0.45958 + 0.50000"/>
																<xsl:variable name="factor6">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths6 &gt; 0.00000 and $sumcolumnwidths6 &gt; $tablewidth6">
																			<xsl:value-of select="$tablewidth6 div $sumcolumnwidths6"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns6" select="1"/>
																<xsl:variable name="defaultcolumnwidth6">
																	<xsl:choose>
																		<xsl:when test="$factor6 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns6 &gt; 0">
																			<xsl:value-of select="($tablewidth6 - $sumcolumnwidths6) div $defaultcolumns6"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth6_0" select="2.73083 * $factor6"/>
																<xsl:variable name="columnwidth6_1" select="0.64375 * $factor6"/>
																<xsl:variable name="columnwidth6_2" select="0.45958 * $factor6"/>
																<xsl:variable name="columnwidth6_3" select="0.50000 * $factor6"/>
																<xsl:variable name="columnwidth6_4" select="$defaultcolumnwidth6"/>
																<fo:table width="{$tablewidth6}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth6_0}in"/>
																	<fo:table-column column-width="{$columnwidth6_1}in"/>
																	<fo:table-column column-width="{$columnwidth6_2}in"/>
																	<fo:table-column column-width="{$columnwidth6_3}in"/>
																	<fo:table-column column-width="{$columnwidth6_4}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="5">
																				<fo:block>
																					<fo:inline font-size="9pt">
																						<xsl:text>The annual percentage rate of the mortgage of $&#160;</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_MTGAMT_1">
																							<fo:inline font-size="9pt">
																								<xsl:value-of select="//DSCL_MTGAMT_1"/>&#160;
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>will be</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_RATE_1">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_NETINTRATE_1"/>
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>
																					</xsl:choose>
																					<fo:inline font-size="9pt" >
																						<xsl:text>%</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>																			
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block />
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block >
																<xsl:variable name="tablewidth7" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths7" select="5.7 + 0.41667"/>
																<xsl:variable name="factor7">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths7 &gt; 0.00000 and $sumcolumnwidths7 &gt; $tablewidth7">
																			<xsl:value-of select="$tablewidth7 div $sumcolumnwidths7"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns7" select="1"/>
																<xsl:variable name="defaultcolumnwidth7">
																	<xsl:choose>
																		<xsl:when test="$factor7 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns7 &gt; 0">
																			<xsl:value-of select="($tablewidth7 - $sumcolumnwidths7) div $defaultcolumns7"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth7_0" select="5.7 * $factor7"/>
																<xsl:variable name="columnwidth7_1" select="0.41667 * $factor7"/>
																<xsl:variable name="columnwidth7_2" select="$defaultcolumnwidth7"/>
																<fo:table width="{$tablewidth7}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth7_0}in"/>
																	<fo:table-column column-width="{$columnwidth7_1}in"/>
																	<fo:table-column column-width="{$columnwidth7_2}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell padding-left="4pt" number-columns-spanned="3">
																				<fo:block>
																					<fo:inline font-size="9pt">
																						<xsl:text>or, where the annual percentage rate is subject to variations, the initial annual percentage rate will be</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_RATE_2">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_NETINTRATE_2"/>
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt" >
																						<xsl:text>%.</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>																			
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block />
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block >
																<xsl:variable name="tablewidth8" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths8" select="4.25 + 0.5"/>
																<xsl:variable name="factor8">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths8 &gt; 0.00000 and $sumcolumnwidths8 &gt; $tablewidth8">
																			<xsl:value-of select="$tablewidth8 div $sumcolumnwidths8"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns8" select="1"/>
																<xsl:variable name="defaultcolumnwidth8">
																	<xsl:choose>
																		<xsl:when test="$factor8 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns8 &gt; 0">
																			<xsl:value-of select="($tablewidth8 - $sumcolumnwidths8) div $defaultcolumns8"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth8_0" select="4.25 * $factor8"/>
																<xsl:variable name="columnwidth8_1" select="0.5 * $factor8"/>
																<xsl:variable name="columnwidth8_2" select="$defaultcolumnwidth8"/>
																<fo:table width="{$tablewidth8}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth8_0}in"/>
																	<fo:table-column column-width="{$columnwidth8_1}in"/>
																	<fo:table-column column-width="{$columnwidth8_2}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell padding-left="4pt" number-columns-spanned="3">
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>The effective annual rate including brokerage, lender&apos;s and insurance fee is</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_RATE_3">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_RATE_3"/>
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt" >
																						<xsl:text>%.</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>																			
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block />
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" padding-left="4pt">
															<fo:block>
																<fo:inline font-size="9pt">
																	<xsl:text>* Accurate to the nearest 1/8%.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="4pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>5.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="4pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>The principal amount and the cost of borrowing based on the annual percentage rate disclosed in section 4 will be&#160;</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth9" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths9" select="0.68917 + 0.63750 + 0.889 + 0.85417 + 2.32 + 0.77292"/>
																<xsl:variable name="factor9">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths9 &gt; 0.00000 and $sumcolumnwidths9 &gt; $tablewidth9">
																			<xsl:value-of select="$tablewidth9 div $sumcolumnwidths9"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns9" select="1"/>
																<xsl:variable name="defaultcolumnwidth9">
																	<xsl:choose>
																		<xsl:when test="$factor9 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns9 &gt; 0">
																			<xsl:value-of select="($tablewidth9 - $sumcolumnwidths9) div $defaultcolumns9"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth9_0" select="0.68917 * $factor9"/>
																<xsl:variable name="columnwidth9_1" select="0.63750 * $factor9"/>
																<xsl:variable name="columnwidth9_2" select="0.889 * $factor9"/>
																<xsl:variable name="columnwidth9_3" select="0.85417 * $factor9"/>
																<xsl:variable name="columnwidth9_4" select="2.32 * $factor9"/>
																<xsl:variable name="columnwidth9_5" select="0.77292 * $factor9"/>
																<xsl:variable name="columnwidth9_6" select="$defaultcolumnwidth9"/>
																<fo:table width="{$tablewidth9}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth9_0}in"/>
																	<fo:table-column column-width="{$columnwidth9_1}in"/>
																	<fo:table-column column-width="{$columnwidth9_2}in"/>
																	<fo:table-column column-width="{$columnwidth9_3}in"/>
																	<fo:table-column column-width="{$columnwidth9_4}in"/>
																	<fo:table-column column-width="{$columnwidth9_5}in"/>
																	<fo:table-column column-width="{$columnwidth9_6}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell display-align="after" padding-left="4pt" padding-right="4pt" number-columns-spanned="7">
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>payable in</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_PAYMENTS_1">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_PAYMENTS_1"/>&#160;
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>payments of $</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_MTGPAYMTAMT_1">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_MTGPAYMTAMT_1"/>&#160;
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>with the first payment becoming due on</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_PYMTDT_1">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_PYMTDT_1"/>
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>.</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>																			
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>6.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth10" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths10" select="2.6+ 0.30417"/>
																<xsl:variable name="factor10">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths10 &gt; 0.00000 and $sumcolumnwidths10 &gt; $tablewidth10">
																			<xsl:value-of select="$tablewidth10 div $sumcolumnwidths10"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns10" select="1"/>
																<xsl:variable name="defaultcolumnwidth10">
																	<xsl:choose>
																		<xsl:when test="$factor10 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns10 &gt; 0">
																			<xsl:value-of select="($tablewidth10 - $sumcolumnwidths10) div $defaultcolumns10"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth10_0" select="2.6 * $factor10"/>
																<xsl:variable name="columnwidth10_1" select="0.30417 * $factor10"/>
																<xsl:variable name="columnwidth10_2" select="$defaultcolumnwidth10"/>
																<fo:table width="{$tablewidth10}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth10_0}in"/>
																	<fo:table-column column-width="{$columnwidth10_1}in"/>
																	<fo:table-column column-width="{$columnwidth10_2}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="3">
																				<fo:block>
																					<fo:inline font-size="9pt">
																						<xsl:text>The mortgage will become due and payable in</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_MTGTERM_2">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_MTGTERM_2"/>&#160;
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>yrs, based on the annual percentage rate disclosed in</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>																			
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block />
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth11" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths11" select="5.5 + 0.73958"/>
																<xsl:variable name="factor11">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths11 &gt; 0.00000 and $sumcolumnwidths11 &gt; $tablewidth11">
																			<xsl:value-of select="$tablewidth11 div $sumcolumnwidths11"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns11" select="1"/>
																<xsl:variable name="defaultcolumnwidth11">
																	<xsl:choose>
																		<xsl:when test="$factor11 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns11 &gt; 0">
																			<xsl:value-of select="($tablewidth11 - $sumcolumnwidths11) div $defaultcolumns11"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth11_0" select="5.5 * $factor11"/>
																<xsl:variable name="columnwidth11_1" select="0.73958 * $factor11"/>
																<xsl:variable name="columnwidth11_2" select="$defaultcolumnwidth11"/>
																<fo:table width="{$tablewidth11}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth11_0}in"/>
																	<fo:table-column column-width="{$columnwidth11_1}in"/>
																	<fo:table-column column-width="{$columnwidth11_2}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell padding-left="4pt" number-columns-spanned="3">
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>section 4, at which time the borrower, if all payments have been made on the due date, will owe $</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_BLNCEENDTRM_1">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_BLNCEENDTRM_1"/>
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>.</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>																			
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>7.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth12" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths12" select="1.57083 + 0.30852 + 2.35625 + 0.30852"/>
																<xsl:variable name="factor12">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths12 &gt; 0.00000 and $sumcolumnwidths12 &gt; $tablewidth12">
																			<xsl:value-of select="$tablewidth12 div $sumcolumnwidths12"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns12" select="1"/>
																<xsl:variable name="defaultcolumnwidth12">
																	<xsl:choose>
																		<xsl:when test="$factor12 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns12 &gt; 0">
																			<xsl:value-of select="($tablewidth12 - $sumcolumnwidths12) div $defaultcolumns12"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth12_0" select="1.57083 * $factor12"/>
																<xsl:variable name="columnwidth12_1" select="0.30852 * $factor12"/>
																<xsl:variable name="columnwidth12_2" select="2.35625 * $factor12"/>
																<xsl:variable name="columnwidth12_3" select="0.30852 * $factor12"/>
																<xsl:variable name="columnwidth12_4" select="$defaultcolumnwidth12"/>
																<fo:table width="{$tablewidth12}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth12_0}in"/>
																	<fo:table-column column-width="{$columnwidth12_1}in"/>
																	<fo:table-column column-width="{$columnwidth12_2}in"/>
																	<fo:table-column column-width="{$columnwidth12_3}in"/>
																	<fo:table-column column-width="{$columnwidth12_4}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="5">
																				<fo:block>
																					<fo:inline font-size="9pt">
																						<xsl:text>The term of the mortgage is</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_MTGTERM_2">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_MTGTERM_2"/>&#160;
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>years.&#160;&#160;Amortization period of mortgage :</xsl:text>
																					</fo:inline>
																					<xsl:choose>
																						<xsl:when test="//DSCL_MTGAMRTIZTON_1">
																							<fo:inline font-size="9pt">
																								&#160;<xsl:value-of select="//DSCL_MTGAMRTIZTON_1"/>&#160;
																							</fo:inline>
																						</xsl:when>
																						<xsl:otherwise>
																							<fo:inline font-size="9pt" >
																								<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
																							</fo:inline>
																						</xsl:otherwise>																						
																					</xsl:choose>
																					<fo:inline font-size="9pt">
																						<xsl:text>years.</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>																			
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="4pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>8.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="2pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>Where the term of the mortgage is subject to variations, it shall vary in the following manner :</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.01042in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.01042in" number-columns-spanned="2" padding-left="4pt">
															<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">
																<!-- fo:inline font-size="9pt">
																	<xsl:text>&#160;&#160;</xsl:text>
																</fo:inline-->
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DISC_CNSTRCTINMTGINTST_2"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="5pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>9.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="5pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>The charges, fees, etc. under section 2 are made up as follows:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth13" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths13" select="2.10833 + 3.68334 + 0.11458"/>
																<xsl:variable name="factor13">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths13 &gt; 0.00000 and $sumcolumnwidths13 &gt; $tablewidth13">
																			<xsl:value-of select="$tablewidth13 div $sumcolumnwidths13"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns13" select="1"/>
																<xsl:variable name="defaultcolumnwidth13">
																	<xsl:choose>
																		<xsl:when test="$factor13 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns13 &gt; 0">
																			<xsl:value-of select="($tablewidth13 - $sumcolumnwidths13) div $defaultcolumns13"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth13_0" select="2.10833 * $factor13"/>
																<xsl:variable name="columnwidth13_1" select="3.68334 * $factor13"/>
																<xsl:variable name="columnwidth13_2" select="0.11458 * $factor13"/>
																<xsl:variable name="columnwidth13_3" select="$defaultcolumnwidth13"/>
																<fo:table width="{$tablewidth13}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth13_0}in"/>
																	<fo:table-column column-width="{$columnwidth13_1}in"/>
																	<fo:table-column column-width="{$columnwidth13_2}in"/>
																	<fo:table-column column-width="{$columnwidth13_3}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>&#160;&#160;(a) Mortgage Insurance Fees</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DSCL_INSURFEES_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth14" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths14" select="2.28958 + 3.50209 + 0.11458"/>
																<xsl:variable name="factor14">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths14 &gt; 0.00000 and $sumcolumnwidths14 &gt; $tablewidth14">
																			<xsl:value-of select="$tablewidth14 div $sumcolumnwidths14"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns14" select="1"/>
																<xsl:variable name="defaultcolumnwidth14">
																	<xsl:choose>
																		<xsl:when test="$factor14 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns14 &gt; 0">
																			<xsl:value-of select="($tablewidth14 - $sumcolumnwidths14) div $defaultcolumns14"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth14_0" select="2.38958 * $factor14"/>
																<xsl:variable name="columnwidth14_1" select="3.40209 * $factor14"/>
																<xsl:variable name="columnwidth14_2" select="0.11458 * $factor14"/>
																<xsl:variable name="columnwidth14_3" select="$defaultcolumnwidth14"/>
																<fo:table width="{$tablewidth14}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth14_0}in"/>
																	<fo:table-column column-width="{$columnwidth14_1}in"/>
																	<fo:table-column column-width="{$columnwidth14_2}in"/>
																	<fo:table-column column-width="{$columnwidth14_3}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>&#160;&#160;(b) Inspection and Appraisal Fees</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DISC_INSPCTNFEE_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth15" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths15" select="2.85208 + 2.93959 + 0.11458"/>
																<xsl:variable name="factor15">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths15 &gt; 0.00000 and $sumcolumnwidths15 &gt; $tablewidth15">
																			<xsl:value-of select="$tablewidth15 div $sumcolumnwidths15"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns15" select="1"/>
																<xsl:variable name="defaultcolumnwidth15">
																	<xsl:choose>
																		<xsl:when test="$factor15 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns15 &gt; 0">
																			<xsl:value-of select="($tablewidth15 - $sumcolumnwidths15) div $defaultcolumns15"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth15_0" select="2.95208 * $factor15"/>
																<xsl:variable name="columnwidth15_1" select="2.83959 * $factor15"/>
																<xsl:variable name="columnwidth15_2" select="0.11458 * $factor15"/>
																<xsl:variable name="columnwidth15_3" select="$defaultcolumnwidth15"/>
																<fo:table width="{$tablewidth15}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth15_0}in"/>
																	<fo:table-column column-width="{$columnwidth15_1}in"/>
																	<fo:table-column column-width="{$columnwidth15_2}in"/>
																	<fo:table-column column-width="{$columnwidth15_3}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>&#160;&#160;(c) Legal Fees and Estimated Disbursements</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block />
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block >
																					<fo:inline font-size="9pt">
																						
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block />
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block >
																<fo:inline font-size="9pt">
																	<xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;&#160; of not more than:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="4pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="4pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>&#160;&#160;(d) Other charges (list items)</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth16" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths16" select="0.34167 + 1.40625 + 3.23124 + 0.1"/>
																<xsl:variable name="factor16">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths16 &gt; 0.00000 and $sumcolumnwidths16 &gt; $tablewidth16">
																			<xsl:value-of select="$tablewidth16 div $sumcolumnwidths16"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns16" select="1"/>
																<xsl:variable name="defaultcolumnwidth16">
																	<xsl:choose>
																		<xsl:when test="$factor16 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns16 &gt; 0">
																			<xsl:value-of select="($tablewidth16 - $sumcolumnwidths16) div $defaultcolumns16"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth16_0" select="0.34167 * $factor16"/>
																<xsl:variable name="columnwidth16_1" select="1.40625 * $factor16"/>
																<xsl:variable name="columnwidth16_2" select="3.23124 * $factor16"/>
																<xsl:variable name="columnwidth16_3" select="0.1 * $factor16"/>
																<xsl:variable name="columnwidth16_4" select="$defaultcolumnwidth16"/>
																<fo:table width="{$tablewidth16}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth16_0}in"/>
																	<fo:table-column column-width="{$columnwidth16_1}in"/>
																	<fo:table-column column-width="{$columnwidth16_2}in"/>
																	<fo:table-column column-width="{$columnwidth16_3}in"/>
																	<fo:table-column column-width="{$columnwidth16_4}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell height="0.10417in" text-align="right">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>(i)</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>&#160; Bonus or Discount</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DSCL_DISCFEES_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth17" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths17" select="0.35208 + 1.92708 + 2.7 + 0.11458"/>
																<xsl:variable name="factor17">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths17 &gt; 0.00000 and $sumcolumnwidths17 &gt; $tablewidth17">
																			<xsl:value-of select="$tablewidth17 div $sumcolumnwidths17"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns17" select="1"/>
																<xsl:variable name="defaultcolumnwidth17">
																	<xsl:choose>
																		<xsl:when test="$factor17 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns17 &gt; 0">
																			<xsl:value-of select="($tablewidth17 - $sumcolumnwidths17) div $defaultcolumns17"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth17_0" select="0.35208 * $factor17"/>
																<xsl:variable name="columnwidth17_1" select="1.92708 * $factor17"/>
																<xsl:variable name="columnwidth17_2" select="2.7 * $factor17"/>
																<xsl:variable name="columnwidth17_3" select="0.11458 * $factor17"/>
																<xsl:variable name="columnwidth17_4" select="$defaultcolumnwidth17"/>
																<fo:table width="{$tablewidth17}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth17_0}in"/>
																	<fo:table-column column-width="{$columnwidth17_1}in"/>
																	<fo:table-column column-width="{$columnwidth17_2}in"/>
																	<fo:table-column column-width="{$columnwidth17_3}in"/>
																	<fo:table-column column-width="{$columnwidth17_4}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell height="0.10417in" text-align="right">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>(ii)</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>&#160; Agent/Broker Commission</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DSCL_BROKFEES_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth18" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths18" select="0.36250 + 1.02083 + 3.59583 + 0.11458"/>
																<xsl:variable name="factor18">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths18 &gt; 0.00000 and $sumcolumnwidths18 &gt; $tablewidth18">
																			<xsl:value-of select="$tablewidth18 div $sumcolumnwidths18"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns18" select="1"/>
																<xsl:variable name="defaultcolumnwidth18">
																	<xsl:choose>
																		<xsl:when test="$factor18 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns18 &gt; 0">
																			<xsl:value-of select="($tablewidth18 - $sumcolumnwidths18) div $defaultcolumns18"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth18_0" select="0.36250 * $factor18"/>
																<xsl:variable name="columnwidth18_1" select="1.02083 * $factor18"/>
																<xsl:variable name="columnwidth18_2" select="3.59583 * $factor18"/>
																<xsl:variable name="columnwidth18_3" select="0.11458 * $factor18"/>
																<xsl:variable name="columnwidth18_4" select="$defaultcolumnwidth18"/>
																<fo:table width="{$tablewidth18}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth18_0}in"/>
																	<fo:table-column column-width="{$columnwidth18_1}in"/>
																	<fo:table-column column-width="{$columnwidth18_2}in"/>
																	<fo:table-column column-width="{$columnwidth18_3}in"/>
																	<fo:table-column column-width="{$columnwidth18_4}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell height="0.11458in" text-align="right">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>(iii)</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.11458in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>&#160;Other costs</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.11458in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.11458in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.11458in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DSCL_OTHERFEES_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth19" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths19" select="0.36250 + 1.01042 + 4.41875 + 0.11458"/>
																<xsl:variable name="factor19">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths19 &gt; 0.00000 and $sumcolumnwidths19 &gt; $tablewidth19">
																			<xsl:value-of select="$tablewidth19 div $sumcolumnwidths19"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns19" select="1"/>
																<xsl:variable name="defaultcolumnwidth19">
																	<xsl:choose>
																		<xsl:when test="$factor19 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns19 &gt; 0">
																			<xsl:value-of select="($tablewidth19 - $sumcolumnwidths19) div $defaultcolumns19"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth19_0" select="0.36250 * $factor19"/>
																<xsl:variable name="columnwidth19_1" select="1.01042 * $factor19"/>
																<xsl:variable name="columnwidth19_2" select="4.41875 * $factor19"/>
																<xsl:variable name="columnwidth19_3" select="0.11458 * $factor19"/>
																<xsl:variable name="columnwidth19_4" select="$defaultcolumnwidth19"/>
																<fo:table width="{$tablewidth19}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth19_0}in"/>
																	<fo:table-column column-width="{$columnwidth19_1}in"/>
																	<fo:table-column column-width="{$columnwidth19_2}in"/>
																	<fo:table-column column-width="{$columnwidth19_3}in"/>
																	<fo:table-column column-width="{$columnwidth19_4}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell height="0.10417in" text-align="right">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>(iv)</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>&#160;Total</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//DSCL_TOTALFEES_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth20" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths20" select="2.12500 + 3.66667 + 0.11458"/>
																<xsl:variable name="factor20">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths20 &gt; 0.00000 and $sumcolumnwidths20 &gt; $tablewidth20">
																			<xsl:value-of select="$tablewidth20 div $sumcolumnwidths20"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns20" select="1"/>
																<xsl:variable name="defaultcolumnwidth20">
																	<xsl:choose>
																		<xsl:when test="$factor20 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns20 &gt; 0">
																			<xsl:value-of select="($tablewidth20 - $sumcolumnwidths20) div $defaultcolumns20"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth20_0" select="2.27500 * $factor20"/>
																<xsl:variable name="columnwidth20_1" select="3.51667 * $factor20"/>
																<xsl:variable name="columnwidth20_2" select="0.11458 * $factor20"/>
																<xsl:variable name="columnwidth20_3" select="$defaultcolumnwidth20"/>
																<fo:table width="{$tablewidth20}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth20_0}in"/>
																	<fo:table-column column-width="{$columnwidth20_1}in"/>
																	<fo:table-column column-width="{$columnwidth20_2}in"/>
																	<fo:table-column column-width="{$columnwidth20_3}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>&#160;&#160;(e) TOTAL as shown in section 2</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="9pt">
																						<xsl:value-of select="//AB_DSCL_FEES_1"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>10.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>Where the annual percentage rate is subject to variations, it shall vary in the following manner, based on the</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>following conditions:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.05208in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.05208in" number-columns-spanned="2" padding-left="4pt">
															<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">																
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DSCL_ANNLPERCRTEVRTNS_3"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="4pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>11.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="4pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>The terms and conditions of repayment before maturity of the loan contract are as follows:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.20833in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.20833in" number-columns-spanned="2" padding-left="4pt">
															<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">																
																<fo:inline font-size="9pt">
																	<xsl:value-of select="//DSCL_PYMTVRTNS_3"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<!-- fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="10pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2">
															<fo:block padding-top="30pt" padding-bottom="1pt">
																<fo:inline font-size="9pt">
																	<xsl:text>Credit Grantor</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row-->
													<fo:table-row>
														<fo:table-cell height="0.20833in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.20833in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<!-- fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell-->
														<fo:table-cell number-columns-spanned="2" padding-left="10pt">
															<fo:block keep-together="always">
																<xsl:variable name="tablewidth21" select="$columnwidth2_1 * 1.00000 + $columnwidth2_2 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths21" select="2.8 + 1.00000 + 2.8 "/>
																<xsl:variable name="factor21">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths21 &gt; 0.00000 and $sumcolumnwidths21 &gt; $tablewidth21">
																			<xsl:value-of select="$tablewidth21 div $sumcolumnwidths21"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns21" select="1"/>
																<xsl:variable name="defaultcolumnwidth21">
																	<xsl:choose>
																		<xsl:when test="$factor21 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns21 &gt; 0">
																			<xsl:value-of select="($tablewidth21 - $sumcolumnwidths21) div $defaultcolumns21"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth21_0" select="2.8  * $factor21"/>
																<xsl:variable name="columnwidth21_1" select="1.00000 * $factor21"/>
																<xsl:variable name="columnwidth21_2" select="2.8 * $factor21"/>
																<xsl:variable name="columnwidth21_3" select="$defaultcolumnwidth21"/>
																<fo:table width="{$tablewidth21}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth21_0}in"/>
																	<fo:table-column column-width="{$columnwidth21_1}in"/>
																	<fo:table-column column-width="{$columnwidth21_2}in"/>
																	<fo:table-column column-width="{$columnwidth21_3}in"/>
																	<fo:table-body font-size="9pt">
																		<fo:table-row keep-with-next="always">
																			<fo:table-cell padding-top="17pt" height="0.42in" padding-left="10pt" number-rows-spanned="2" display-align="center">
																				<fo:block >
																					<fo:inline font-size="9pt">
																						<xsl:text>Credit Grantor</xsl:text>
																					</fo:inline>
																				</fo:block>																				
																			</fo:table-cell>
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" />
																			</fo:table-cell>
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" />
																			</fo:table-cell>
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" />
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row keep-with-next="always">
																			<!-- fo:table-cell height="0.42in" padding-left="10pt">
																				<fo:block padding-top="30pt" padding-bottom="1pt"/>																																								
																			</fo:table-cell-->
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row keep-with-next="always">
																			<fo:table-cell height="0.42in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell height="0.42in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<!--  xsl:for-each select="//borrowerloop/Borrower">
																			<xsl:variable name="count" select="count"/>	
																			<xsl:when test="$count mod 2 = 0">																					
																				<fo:table-row>
																					<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline font-size="9pt">
																								<xsl:value-of select="$count"/>
																							</fo:inline>
																							<fo:inline font-size="9pt">
																								<xsl:value-of select="name"/>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell>
																						<fo:block padding-top="1pt" padding-bottom="1pt"/>
																					</fo:table-cell>
																			</xsl:when>
																			<xsl:otherwise>
																				<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline font-size="9pt">
																								<xsl:value-of select="$count"/>
																							</fo:inline>
																							<fo:inline font-size="9pt">
																								<xsl:value-of select="name"/>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																			</xsl:otherwise>																				
																		</xsl:for-each>
																		<xsl:when test="$count mod 2 != 0">	
																					<fo:table-cell>
																						<fo:block padding-top="1pt" padding-bottom="1pt"/>
																					</fo:table-cell>
																			</fo:table-row>
																		</xsl:when-->
																		<fo:table-row keep-with-next="always">
																			<fo:table-cell>
																				<fo:table>
																					<fo:table-column column-width="proportional-column-width(100)"/>
																					<fo:table-body font-size="9pt">
																						<xsl:for-each select="//borrowerloop1/Borrower">																							
																							<fo:table-row keep-with-next="always">
																								<fo:table-cell height="0.42in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
																									<fo:block padding-top="1pt" padding-bottom="1pt">																										
																										<fo:inline font-size="9pt">
																											<xsl:value-of select="name"/>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																							</fo:table-row>
																						</xsl:for-each>
																					</fo:table-body>
																				</fo:table>
																			</fo:table-cell>																			
																			<fo:table-cell height="0.35in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell display-align="before">
																				<fo:table>
																					<fo:table-column column-width="proportional-column-width(100)"/>
																					<fo:table-body font-size="9pt">
																						<xsl:for-each select="//borrowerloop2/Borrower">																							
																							<fo:table-row>
																								<fo:table-cell height="0.42in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
																									<fo:block padding-top="1pt" padding-bottom="1pt">																										
																										<fo:inline font-size="9pt">
																											<xsl:value-of select="name"/>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																							</fo:table-row>
																						</xsl:for-each>
																					</fo:table-body>
																				</fo:table>
																			</fo:table-cell>																				
																			<fo:table-cell height="0.35in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.35in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.35in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	 <xsl:template name="headerall">
        <xsl:variable name="maxwidth" select="7.55000" />
        <fo:static-content flow-name="xsl-region-before">
            <fo:block>
                <xsl:variable name="tablewidth0" select="$maxwidth * 1.00000" />
                <xsl:variable name="sumcolumnwidths0" select="0.04167 + 0.04167" />
                <xsl:variable name="defaultcolumns0" select="1 + 1" />
                <xsl:variable name="defaultcolumnwidth0">
                    <xsl:choose>
                        <xsl:when test="$defaultcolumns0 &gt; 0">
                            <xsl:value-of select="($tablewidth0 - $sumcolumnwidths0) div $defaultcolumns0" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="0.000" />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="columnwidth0_0" select="$defaultcolumnwidth0" />
                <xsl:variable name="columnwidth0_1" select="$defaultcolumnwidth0" />
                <fo:table table-layout="fixed" width="{$tablewidth0}in" border-collapse="separate" border-separation="0.04167in" color="black" display-align="center">
                    <fo:table-column column-width="{$columnwidth0_0}in" />
                    <fo:table-column column-width="{$columnwidth0_1}in" />
                    <fo:table-body>
                    	<fo:table-row>
                            <fo:table-cell number-columns-spanned="2" padding-top="0.0in" padding-bottom="0.00000in" padding-left="0.02in" padding-right="0.00000in">
                                <fo:block>
                                    <fo:block text-align="center" space-before.optimum="-8pt">
                                       
                                    </fo:block>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell number-columns-spanned="2" padding-top="0.0in" padding-bottom="0.00000in" padding-left="0.02in" padding-right="0.00000in">
                                <fo:block>
                                    <fo:block text-align="center" space-before.optimum="-8pt">
                                        <fo:leader leader-length="100%" leader-pattern="rule" rule-thickness="0.01042in" color="black" />
                                    </fo:block>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>
</xsl:stylesheet>
