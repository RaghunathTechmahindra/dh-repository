<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
13/Oct/2006 DVG #DG522 #4922  Cervus CR # 106 Rebranding  
12/Aug/2005 DVG #DG294 #662  Cervus - docs to be created for PII - reformat 
06/Jul/2005 DVG #DG246 #662  Cervus - docs to be created for PII - reformat - reorganized
prepared by: Catherine Rugaizer 
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- Legal size paper -->
	<xsl:output method="text"/>

	<xsl:variable name="lenderName" select="/*/Deal/LenderProfile/lenderName"/><!--#DG522 -->	

	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
		<!--	<xsl:call-template name="DocumentHeader"/> -->
		<xsl:call-template name="ToSection"/>
		<xsl:call-template name="ReSection"/>
		<!--#DG246 xsl:call-template name="MainText"/-->
		<xsl:call-template name="MainTextFurth"/>
		<!--#DG294 xsl:call-template name="MainTextPlsForw"/-->
		<xsl:call-template name="MainTextToCv"/>
		<xsl:call-template name="MainTextThxAdv"/>
		<!--#DG246 end-->		
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- templates section                                                                                -->
	<!-- ************************************************************************ -->
	<!-- this is a place holder for logo -->
	<xsl:template name="DocumentHeader">
		<xsl:text>{\pard\sa240
\brdrt \brdrs \brdrnone \brsp10
\brdrl \brdrs \brdrnone \brsp10
\brdrb \brdrs \brdrnone \brsp10
\brdrr \brdrs \brdrnone \brsp10
\qc\f1\fs20 \line\b\ul {\fs28} \line
\par }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="ToSection">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa360 </xsl:text>
		<xsl:value-of select="//General/CurrentDate"/>
		<xsl:text>\par }</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa80 </xsl:text>
		<xsl:call-template name="SolicitorName"/>
		<xsl:text>\line }</xsl:text><!--#DG246 -->
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa80</xsl:text>
		<xsl:call-template name="SolicitorAddressAndCompany"/>
		<xsl:text>\par }</xsl:text>
		<!--#DG246 xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa80</xsl:text>
		<xsl:call-template name="SolicitorPhoneAndFax"/>
		<xsl:text>\line\line}</xsl:text --><xsl:text>\line</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 Dear </xsl:text>
		<xsl:for-each select="//Deal/PartyProfile[partyTypeId=50][1]"><!--#DG246 -->
			<xsl:value-of select="./Contact/contactFirstName"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="./Contact/contactLastName"/>
			<xsl:text>, </xsl:text>
			<xsl:text>\par }</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<!--#DG294 -->
	<!-- ================================================  -->
	<xsl:template name="FrToSection">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa360 </xsl:text>
		<xsl:value-of select="//General/CurrentDate"/>
		<xsl:text>\par }</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa80 </xsl:text>
		<xsl:call-template name="SolicitorName"/>
		<xsl:text>\line }</xsl:text><!--#DG246 -->
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa80</xsl:text>
		<xsl:call-template name="SolicitorAddressAndCompany"/>
		<xsl:text>\par }</xsl:text>
		<!--#DG246 xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa80</xsl:text>
		<xsl:call-template name="SolicitorPhoneAndFax"/>
		<xsl:text>\line\line}</xsl:text --><xsl:text>\line</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 Cher </xsl:text>
		<xsl:for-each select="//Deal/PartyProfile[partyTypeId=50][1]"><!--#DG246 -->
			<xsl:value-of select="./Contact/contactFirstName"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="./Contact/contactLastName"/>
			<xsl:text>, </xsl:text>
			<xsl:text>\par }</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="ReSection">
		<!-- row 1 -->
		<xsl:call-template name="Re_row"/>
		<xsl:text>{\f1\fs20\b {Re:}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:text>Borrowers: </xsl:text>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 \b \caps {</xsl:text>
		<xsl:call-template name="BorrowersListedBlocks"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<!-- row 2 -->
		<xsl:call-template name="Re_row"/>
		<xsl:text>{\f1\fs20\b }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:text>Security Address:</xsl:text>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 \b {</xsl:text>
		<xsl:call-template name="SecurityAddress"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<!-- row 3 -->
		<xsl:call-template name="Re_row"/>
		<xsl:text>{\f1\fs20\b }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:text>Closing Date:</xsl:text>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 \b {</xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="Re_row"/>
		<xsl:text>{\f1\fs20\b }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:text>Lender Reference Number:</xsl:text>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 \b {</xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
	</xsl:template>

	<!--#DG294 -->
	<!-- ================================================  -->
	<xsl:template name="FrReSection">
		<!-- row 1 -->
		<xsl:call-template name="Re_row"/>
		<xsl:text>{\f1\fs20\b {Object:}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:text>Emprunteur(s): </xsl:text>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 \b \caps {</xsl:text>
		<xsl:call-template name="BorrowersListedBlocks"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<!-- row 2 -->
		<xsl:call-template name="Re_row"/>
		<xsl:text>{\f1\fs20\b }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:text>Adresse de la Propriété:</xsl:text>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 \b {</xsl:text>
		<xsl:call-template name="SecurityAddress"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<!-- row 3 -->
		<xsl:call-template name="Re_row"/>
		<xsl:text>{\f1\fs20\b }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:text>Date de Conclusion:</xsl:text>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 \b {</xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="Re_row"/>
		<xsl:text>{\f1\fs20\b }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:text>Numéro de Référence du Prêteur:</xsl:text>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 \b {</xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="MainTextFurth">
		<xsl:call-template name="HorizLine"/>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa360 \qj </xsl:text>
		<!--#DG294 xsl:text>Further to the closing of the above-referenced transaction, a review of our file indicates that our office has yet to receive the Solicitor’s Final Report and supporting documentation.</xsl:text-->
		<xsl:text>Further to our solicitor instructions, please fax the required Confirmation of 
Closing with the mortgage registration number to our office as soon as possible.  
<!--#DG522 Cervus-->Macquarie Financial requires 
this form to be completed and returned within 48 hours of registration.  If you have already 
faxed it in, thank you and please disregard this email.</xsl:text>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!--#DG294 -->
	<!-- ================================================  -->
	<xsl:template name="FrMainTextFurth">
		<xsl:call-template name="HorizLine"/>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa360 \qj </xsl:text>
		<xsl:text>Suite à nos instructions au notaire, veuillez envoyer à notre bureau par télécopie la 
confirmation de la conclusion requise incluant le numéro d'enregistrement de l'hypothèque 
aussitôt que possible. 
<!--#DG522 Cervus -->Macquarie Financial exige que ce formulaire soit complété et retourné  
dans les 48 heures  qui suivent l'enregistrement du prêt. Veuillez ne pas tenir 
compte de ce courriel si vous l'avez déjà envoyé.</xsl:text>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="MainTextPlsForw">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa360 </xsl:text>
		<xsl:text>Please forward the required report together with the supporting documentation outlined in our Solicitor’s Guide to:</xsl:text>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="MainTextToCv">		
		<xsl:text>{\pard\sa180\f1\fs22 \sa80 \qc {
<!--#DG522 Cervus Financial Corp.--></xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text>\par 
<!--#DG294 20 Toronto Street, Suite 830-->20 Toronto Street, 10{\super th} Floor \par 
Toronto, Ontario, M5C 2B8 \par 
Attention: Mortgage Servicing Department \par 
<!--#DG294 -->Fax:  1-888-753-5842\par
Phone: 1-877-462-3788\par
}</xsl:text>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!--#DG294 -->
	<!-- ================================================  -->
	<xsl:template name="FrMainTextToCv">		
		<xsl:text>{\pard\sa180\f1\fs22 \sa80 \qc {
<!--#DG522 Corporation Financière Cervus-->Financière Macquarie Ltée\par  
20, rue Toronto, 10{\super e} étage \par 
Toronto, Ontario, M5C 2B8 \par 
Attention : Service de l’administration \par 
Téléc.:  1-888-753-5842\par
Tél.: 1-877-462-3788\par
}</xsl:text>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="MainTextThxAdv">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 </xsl:text>
		<xsl:text>Thank you in advance for your assistance and cooperation.</xsl:text>
		<xsl:text>\par }</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 </xsl:text>
		<xsl:text>Yours truly, </xsl:text>
		<xsl:text>\par }</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 </xsl:text>
		<xsl:text><!--#DG522 Cervus Financial Corp.-->Macquarie Financial Ltd.</xsl:text>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!--#DG294 -->
	<!-- ================================================  -->
	<xsl:template name="FrMainTextThxAdv">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 </xsl:text>
		<xsl:text>Merci,</xsl:text>
		<xsl:text>\par }</xsl:text>
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa240 </xsl:text>
    <!--#DG522 xsl:text>Corporation Financière Cervus.</xsl:text-->
		<xsl:text>Financière Macquarie Ltée.</xsl:text>		
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!--  ===================================== Utility templates ====================================== -->

	<!--  ============================= SolicitorNameAndAddress ====================================== -->
	<xsl:template name="SolicitorName">
		<xsl:for-each select="//Deal/PartyProfile[partyTypeId='50'][1]"> <!--#DG246 -->
			<xsl:text>{</xsl:text>
			<xsl:value-of select="./Contact/contactFirstName"/>
			<xsl:text> </xsl:text>
			<xsl:if test="./Contact/contactMiddleInitial">
				<xsl:value-of select="./Contact/contactMiddleInitial"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:value-of select="./Contact/contactLastName"/>
			<xsl:text>}</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="SolicitorAddressAndCompany">
		<xsl:for-each select="//Deal/PartyProfile[partyTypeId='50'][1]"> <!--#DG246 -->
			<xsl:text>{</xsl:text>
			<xsl:value-of select="./partyCompanyName"/>
			<xsl:text>\line </xsl:text><!--#DG246 -->
			<xsl:value-of select="./Contact/Addr/addressLine1"/>
			<xsl:if test="./Contact/Addr/addressLine2">
				<xsl:text>\~</xsl:text><!--#DG246 -->
				<xsl:value-of select="./Contact/Addr/addressLine2"/>
			</xsl:if>
			<xsl:text>\line </xsl:text>
			<xsl:value-of select="./Contact/Addr/city"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Contact/Addr/province"/>
			<xsl:text>, </xsl:text><!--#DG246 -->
			<xsl:value-of select="concat(./Contact/Addr/postalFSA,' ', ./Contact/Addr/postalLDU)"/>
			<xsl:text>}</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="SolicitorPhoneAndFax">
		<!--#DG246 xsl:for-each select="//Deal/PartyProfile">
			<xsl:if test="./partyTypeId='50'"-->
		<xsl:for-each select="//Deal/PartyProfile[partyTypeId='50'][1]"> <!--#DG246 -->
				<xsl:text>{Phone: \tab </xsl:text>
				<xsl:call-template name="FormatPhone">
					<xsl:with-param name="pnum" select="./Contact/contactPhoneNumber"/>
				</xsl:call-template>
				<xsl:if test="./Contact/contactPhoneNumberExtension and string-length(./Contact/contactPhoneNumberExtension)!=0">
					<xsl:text> Ext. </xsl:text>
					<xsl:value-of select="./Contact/contactPhoneNumberExtension"/>
				</xsl:if>
				<xsl:text>\line </xsl:text>
				<xsl:if test="./Contact/contactFaxNumber">
					<xsl:text>Fax: \tab </xsl:text>
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="./Contact/contactFaxNumber"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:text>}</xsl:text>
			<!--#DG246 /xsl:if-->
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="//Deal/Borrower">
			<xsl:if test="./borrowerTypeId=0">
				<xsl:text>{</xsl:text>
				<xsl:call-template name="BorrowerFullName"/>
				<xsl:text>}</xsl:text>
				<xsl:if test="position() !=last()">\line </xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<!-- ================================================  -->
	<!-- Subject property, English formatting of address, upper case -->
	<xsl:template name="SecurityAddress">
		<xsl:for-each select="//Deal/Property">
			<xsl:if test="./primaryPropertyFlag='Y'">
				<xsl:if test="./unitNumber != ''">
					<xsl:text>\caps </xsl:text>
					<xsl:value-of select="./unitNumber"/>
					<xsl:text> - </xsl:text>
				</xsl:if>
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="./propertyStreetNumber"/>
				<xsl:text> </xsl:text>
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="./propertyStreetName"/>
				<xsl:text> </xsl:text>
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="./streetType"/>
				<xsl:text> </xsl:text>
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="./streetDirection"/>
				<xsl:text>\par </xsl:text>
				<xsl:if test="./propertyAddressLine2 != ''">
					<xsl:text>\caps </xsl:text>
					<xsl:value-of select="./propertyAddressLine2"/>
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:text>}{\b\f1\fs18 </xsl:text>
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="./propertyCity"/>
				<xsl:text>, </xsl:text>
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="./province"/>
				<xsl:text> </xsl:text>
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="./propertyPostalFSA"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="./propertyPostalLDU"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- Row definitions 														                -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="ToSection_row_type1">
		<xsl:text>{\trowd \trgaph55\trleft-55\ql
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10840 
\pard \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="ToSection_row_type2">
		<xsl:text>{\trowd \trgaph55\trleft-55
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5000 
\pard \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<!-- row with 3 columns -->
	<xsl:template name="Chk_list_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \sa180 \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<!-- row with 3 columns, default between rows -->
	<xsl:template name="Chk_list_row2">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="produce-empty-rows">
		<xsl:param name="count"/>
		<xsl:if test="$count != 0">
			<xsl:call-template name="Chk_list_empty_row"/>
			<xsl:call-template name="produce-empty-rows">
				<xsl:with-param name="count" select="$count - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="Chk_list_empty_row">
		<xsl:call-template name="Chk_list_row"/>
		<xsl:text>{\f1\fs20 [    ]}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>\cell \pard \intbl {\f1\fs20 _________________________________________________________________________________________ }
</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="FormatPhone">
		<xsl:param name="pnum"/>
		<xsl:if test="string-length($pnum)=10">
			<xsl:text>(</xsl:text>
			<xsl:value-of select="substring($pnum, 1, 3)"/>
			<xsl:text>) </xsl:text>
			<xsl:value-of select="substring($pnum, 4, 3)"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="substring($pnum, 7, 4)"/>
		</xsl:if>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="WeConfirm_line">
		<xsl:call-template name="Chk_list_row"/>
		<xsl:text>{\f1\fs20    }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 _________________________________________________________________________________________}
</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- row with 3 columns -->
	<xsl:template name="Re_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 <!--#DG294 -->
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx745
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx4045
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8820 
\pard \sa80 \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<!-- row with 2 columns -->
	<xsl:template name="Signature_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6400
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<!-- ================================================  -->
	<xsl:template name="CurrentUserName">
		<xsl:for-each select="//specialRequirementTags/currentUser">
			<xsl:value-of select="concat(./Contact/contactFirstName,' ', ./Contact/contactLastName)"/>
		</xsl:for-each>
	</xsl:template>
	<!-- ************************************************************************ -->
	<!-- Utility templates 														                -->
	<!-- ************************************************************************ -->
	<!-- ================================================  -->
	<xsl:template name="HorizLine">
		<xsl:text>{\pard\sa180\brdrb \brdrs \brdrw30 \brsp10 \par }</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="Prgr_Arial_10_bold">
		<xsl:text>{\pard\sa180\f1\fs20\b
</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="Prgr_Arial_10">
		<xsl:text>{\pard\sa180\f1\fs20
</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- rtf file start and rtf file end                                                                    -->
	<!-- ************************************************************************ -->
	<!-- ================================================  -->
	<xsl:template name="RTFFileEnd">
		<xsl:text>}}</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- letter size  (216 × 279 mm or  612pt  x 792pt); margings: top=72pt, bottom =72pt, left=90pt, right=90pt-->
	<xsl:template name="RTFFileStart">
	 <!-- #DG670 -->
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\info {\author Catherine Rutgaizer}{\operator filogix automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company filogix inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl 
\paperw12240 \paperh15840 \margl1650<!--#DG294 -->\margr1800\margt1440\margb1440\deftab720
 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
</xsl:text>
	</xsl:template>
</xsl:stylesheet>
