<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
  12/Jul/2006 DVG #DG460 #3874  Xceed Solicitors' Package  Borrower Name Template change  
	17/Nov/2005 DVG #DG362 #2324  CR#134 Solicitors Package changes for ARM  
	09/Sep/2005 DVG #DG312 #1965  Xeed Solicitor Package Middle Name initial is not shown  
	Import all of the logic from the Commitment Letter template so that the 2 documents 
	(standalone C/L and embedded C/L) remain the same -->
	<xsl:import href="XceedCommitmentLetter.xsl"/>
	<xsl:output method="text"/>

  <!--#DG362 this is used all over-->
  <xsl:variable name="secondMtg" select="/*/SolicitorsPackage/LienPosition = 'SECOND'"/>

	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LanguageEnglish">
				<xsl:call-template name="EnglishSolicitorInstructions"/>
				<xsl:call-template name="EnglishGuide"/>
				<xsl:call-template name="EnglishCommitmentLetter"/>
				<xsl:call-template name="EnglishRequestForFunds"/>
				<xsl:call-template name="EnglishFinalReport"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LanguageFrench">
				<xsl:call-template name="FrenchSolicitorInstructions"/>
				<xsl:call-template name="FrenchGuide"/>
				<xsl:call-template name="FrenchCommitmentLetter"/>
				<xsl:call-template name="FrenchRequestForFunds"/>
				<xsl:call-template name="FrenchFinalReport"/>
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishSolicitorInstructions">
		<xsl:text>\paperh20160 \trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
		<xsl:call-template name="XceedLogo"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<!-- <xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose>	-->
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>{  }</xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CurrentDate"/>
		<xsl:text>
\par 
\par
\par}
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth7938 \cellx7830
\cltxlrtb\clftsWidth3\clwWidth2818 \cellx10648
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \fs20
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/>
		<xsl:text>
\par }
{\f1\fs20 
</xsl:text>
		<xsl:if test="//SolicitorsPackage/Solicitor/Phone">
			<xsl:text>Phone: </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/Solicitor/Phone"/>
			<xsl:text>\tab </xsl:text>
		</xsl:if>
		<xsl:if test="//SolicitorsPackage/Solicitor/Fax">
			<xsl:text>Fax: </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/Solicitor/Fax"/>
		</xsl:if>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Number of Pages ______\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth7938 \cellx7830
\cltxlrtb\clftsWidth3\clwWidth2818 \cellx10648
\row 
}
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 
\par 
Dear  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>,
\par 
\par }

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 RE:\cell BORROWER(S):\cell </xsl:text>
		<!--#DG312 xsl:for-each select="//SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trbrdrh
\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, if any:\cell 
</xsl:text>
    <!--#DG460 oooops, should have been done since #DG312 ...
		xsl:for-each select="//SolicitorsPackage/GuarantorNames">
			<xsl:value-of select="./Name"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="borType" select="1"/>
    </xsl:call-template>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell SECURITY ADDRESS:\cell 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}
{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/PostalCode"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell CLOSING DATE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell APPROVED LOAN AMOUNT:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell LENDER REFERENCE NUMBER:\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}

\pard \qj \widctlpar\tx540\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 {\pict{\*\picprop\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn alignHR}{\sv 1}}{\sp{\sn dxHeightHR}{\sv 30}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fStandardHR}{\sv 1}}{\sp{\sn fHorizRule}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}}\picscalex1053\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag908016038\blipupi1439{\*\blipuid 361f39a6bcdb98f4598df072e3f44282}
010009000003310000000000070000000000050000000b0200000000050000000c02f401e803070000001b04f401e8030000000005000000140200000000050000001302f401e803050000001402f40100000500000013020000e80303000000000000}}

{\f1\fs18 \par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 A Mortgage Loan has been approved for the above-noted Borrower(s), in accordance with the terms and conditions outlined in the Commitment Letter attached. We have been advised that you will be acting for </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> and Xceed Funding Corporation </xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> and </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
			</xsl:when>
		</xsl:choose>
		<xsl:text> (hereinafter called the “Lender”) in attending to the preparation, execution and 
registration of the Mortgage against the property and ensuring that the interests of the Lender 
as Mortgagee are valid and appropriately secured. Any amendments required due to errors, 
omissions or non-compliance on your part will be for your account and your responsibility to 
correct. The Lender will also rely solely on you to confirm the identity of the Borrower(s) 
and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, if any, and to retain the evidence used to confirm their identity in your file. The Mortgage may be registered electronically, where available.\par \par }

\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Any material facts which may adversely affect the Lender’s position as a First Charge on the property are to be disclosed to the Lender prior to the advance of any funds. Similarly, any issues which may affect the Lender’s security or any subsequent amendments to the Agreement of Purchase and Sale are to be referred to this office for direction prior to the release of funds. Our prior consent will be required relative to any requests for additional or secondary financing. If for any reason you are unable to register our Mortgage on the scheduled closing date, we must be notified at least 24 hours prior to that date and advised of the reason for the delay. No advance can be made after the scheduled closing date without the Lender’s written authorization.\par \par}
{\f1\fs20\b You must submit the Solicitor\rquote s request for Funds together with the required 
documentation to our Financial Service Centre {\ul no later than 12pm EST four (4) business days 
prior to closing}. Failure to do so will delay closing from the currently scheduled closing date. 
The Solicitor\rquote s Request for Funds should contain confirmation that all our requirements 
have been or will be met and should specifically outline any qualifications that will appear in 
your Final Report and certification of title. You are to ensure when submitting your Request for 
Funds that this transaction will close on the date requested. In the case of refinances, all 
payout and discharge statements are to be in your possession prior to requesting funds if this is 
not the case, do not submit your Request for Funds. For purchase transactions, you must be in a 
position to have this mortgage registered on the request closing date. \par \par}

{\f1\fs20\b The mortgage funds will be sent to you by Electronic Fund Transfer and should be 
disbursed without delay provided you are satisfied that all requirements have been met and 
adequate precaution has been taken to ensure that the mortgage security will retain priority. 
Should the closing of this transaction be delayed, interest will be charged to the borrower 
beginning on the originally requested closing date.\par \par}

{\f1\fs20\b You will be contacted by noon EST on the business day after closing to confirm that this transaction has closed and funds were disbursed. If this is not the case, we will require the immediate return of our funds at your expense (and with appropriate penalties). If this will close at a later date, you will be required to resubmit the Solicitor\rquote s Request for Funds {\ul no later than 12pm EST four (4) business days prior to the revised closing date}. If you are within this four day window, the earliest the transaction would then close would be on the fourth business day. \par \par}

\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Legal fees and all other costs, charges and expenses associated with this transaction 
are payable by the Borrower(s) whether or not the Mortgage proceeds are advanced.\par }
\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
{\f1\fs20 In connection with this transaction we enclose the following documents:
\par 
\par 
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Mortgage Commitment Letter}{\f1\b0  - }
{\f1\b0 Please ensure that any outstanding conditions outlined in the Commitment Letter that are the responsibility of the Solicitor, 
are duly noted and that full compliance is confirmed prior to advance and included in your final report.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Solicitors Instructions and Guidelines}{\f1\b0  - }{\f1\b0 Self explanatory\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Mortgage}{\f1\b0  - </xsl:text>
		<xsl:for-each select="//SolicitorsPackage/ProvincialClause/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Schedules}{\f1\b0 - Schedule A\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\f1 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Disclosure Statement}{\f1\fs20   (if applicable)\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
}
{\f1\fs20 \par }
{\f1\fs20 Other required security/Special Conditions:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SolicitorSpecialInstructions"/>
		<xsl:text>
\par }
{\f1\fs20\par }
\pard\plain \s22\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 We consent to your acting for the Lender as well as the Borrower(s) and/or </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> provided that you disclose this fact to the Borrower(s) and/or </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> and obtain their consent in writing and that you disclose to each party all information you possess or obtain which is or may be relevant to this transaction. 
\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\pard\plain \s2\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\b\f1\fs20\ul FINANCIAL SERVICES CENTRE\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\f1\fs20 Documents and funding requests should be directed to:
\par 
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> / Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text>
\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/>
		<xsl:text>\par
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/>
		<xsl:text>
\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\f1\fs20 
\par Contact: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Administrator/contactFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Administrator/contactLastName"/>
		<xsl:text>
\par 
\par Phone:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Phone"/>
		<xsl:text> Ext. </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Administrator/contactPhoneNumberExtension"/>
		<xsl:text>
\par Fax:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>
\par Email:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Administrator/contactEmailAddress"/>
		<xsl:text>		
\par }
\pard \ql \fi-360\li360\widctlpar\aspalpha\aspnum\faauto\ilvl12\adjustright\lin360\itap0 
{\f1\fs20 \par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 If you have any question or concerns, please feel free to call.
\par 
\par Sincerely,\tab \tab 
\par 
\par 
\par 
\par 
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Administrator/contactFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Administrator/contactLastName"/>
		<xsl:text>
\par Mortgage Administrator}
</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishGuide">
		<xsl:text>{\sectd\sect} \pghsxn20160\psz5
\pard\plain \s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\b\ul\f1\fs28 SOLICITOR\rquote S GUIDE}
{\f1\fs18\par\par\par }
\pard\plain \s16\qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs20 If you are satisfied that the Mortgagor(s) (also known as the \'93Borrower(s)\'94) has 
or will acquire a good and marketable title to the property described in our Commitment Letter, 
complete our requirements as set out below, and prepare a Mortgage as outlined herein.\par }
\pard\plain \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
{\b\f1\fs20\ul Documentation:}{\f1\fs20  The Mortgage Form Number identified in our cover letter together with the applicable Schedule(s) must be registered. }
{\f1\fs20 \par \par }
{\b\f1\fs20\ul Mortgage Requirements:}{\f1\fs20  The Mortgage Document is to be registered MONTHLY as follows:\par \par }

{\b0\fs20
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Title to be registered as:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SoleJointTenants"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Approved Loan Amount:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmountPreFee"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Application Fee:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdminFee"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Total Loan Amount:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Interest Rate:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Principal &amp; Interest Payment:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PandIPaymentMonthly"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Tax Payment:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TaxPortionMonthly"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Interest Adjustment Date:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/IADDateNextMonthFirst"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Interest Adjustment Amount:\cell }
{\f1 </xsl:text>To be determined based on payment frequency
		<!-- <xsl:value-of select="//SolicitorsPackage/IADAmount"/> -->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 First Payment Date:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FirstPaymentDateNextMonth"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Maturity/Renewal Date:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/maturityDateAdjusted"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Interest Calculated:\cell }
{\f1 Semi-annually, not in advance\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
}

\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 Note: If the closing date gets moved from one month to the next, ALL dates shown 
above must be changed accordingly. \par\par }

{\b\f1\fs20\ul Searches:}
{\f1\fs20  You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Execution"/>
		<xsl:text> It is your responsibility to ensure that there are no work orders or deficiency 
notices outstanding against the property, that all realty taxes and levies which have or will 
become due and payable up to the closing date are paid to the Municipality. If your search 
reveals title has been transferred since the date of the Offer to Purchase and before the closing 
date you must notify us immediately and discontinue all work on this transaction unless notified 
otherwise. Immediately prior to registration of the mortgage, you are to obtain a 
Sheriff\rquote s Certificate/General Registry Search indicating that there are no writs of 
execution on file against the mortgagor(s), guarantor(s), if any, or any previous owner, which 
would adversely affect our security.\par }
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }

\pard \qj \widctlpar\tx1800\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Condo/Strata Title:}{\f1\fs20  You are to confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. You are to review the Condominium Corporation\rquote s Declaration and Bylaws and confirm they contain nothing derogatory to our security. You are to assign the voting rights to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> and Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> and </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text> and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> (if applicable).
\par }\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Survey:}{\f1\fs20  If an up-to-date survey or Surveyor’s Location Certificate/RPR (including Certificate of Compliance), which must be no older than 30 years, is not available, we will accept Title Insurance issued by First Canadian Title, Stewart Title, Title Plus or Land Canada. You should obtain and follow the instructions of the Title Insurance Company in this regard. The mortgagor(s) are responsible for the cost of this Title Insurance premium.
\par }\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \qj \widctlpar\tx1440\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs20\ul Fire Insurance:}{\f1\fs20  You must ensure, prior to advancing funds, that fire insurance coverage for building(s) on the property is in place for not less than their full replacement value with</xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderProfileId = '0'"> first </xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//SolicitorsPackage/LienPosition = 'FIRST'"> first </xsl:when>
					<xsl:otherwise> second </xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<!-- <xsl:text> first </xsl:text> -->
		<!-- <xsl:choose>
	<xsl:when test="//SolicitorsPackage/LienPosition = 'SECOND'"> first </xsl:when>	
	<xsl:otherwise> second </xsl:otherwise>
</xsl:choose> -->
		<xsl:text> payable to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> and Xceed Funding Corporation </xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> and </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
			</xsl:when>
		</xsl:choose>
		<!-- <xsl:text> and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> as Mortgagee at the address shown on the Charge/Mortgage and including a duly completed standard mortgage clause approved by the Insurance Bureau of Canada. Policies must not contain a co-insurance or similar clause that may limit the amount payable.
\par }
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
{\b\f1\fs20\ul Matrimonial Property Act:}
{\f1\fs20  You are to certify that all requirements under the Matrimonial Property Act in your province, where applicable, have been met and that the status does not in any way affect our mortgage/charge.\par \par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Advances:}{\f1\fs20  When all conditions precedent to this transaction have been met, funds in the amount of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text> will be advanced with the following costs being deducted from the advance by the 
Lender;\par }
\pard \qj \fi720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 
{\f1\fs20 \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Application Fee	\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdminFee"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Interest Adjustment Amount\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/IADAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}</xsl:text>
		<xsl:for-each select="//SolicitorsPackage/Fees/Fee">
			<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 TOTAL DEDUCTIONS\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TotalDeductionsAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}
</xsl:text>
		<xsl:call-template name="createRefundableFeeBlankRow"/>
		<xsl:for-each select="//SolicitorsPackage/Fees/refundableFee">
			<xsl:call-template name="createRefundableFeeRows"/>
		</xsl:for-each>
		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Interest Adjustment (daily per diem)\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PerDiem"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par The net Mortgage proceeds, payable to you, in trust, will be deposited into your Trust account on the scheduled closing date, which is anticipated to be </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>. 
\par \par }</xsl:text>
		<xsl:if test="//SolicitorsPackage/Refi">
			<xsl:text>\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Refinances:}
{\f1\fs20  If the purpose of this Mortgage is to refinance an existing debt, the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \sa120\keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Existing </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LPDescription"/>
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Debts to be Paid and Accounts Closed:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
		</xsl:if>
		<xsl:text>
{\b\f1\fs20\ul Construction/Progress Advances:}{\b\f1\fs20  }{\f1\fs20 If the security for this Mortgage is a new property and we are advancing by way of progress advances, the advances will be done on a cost-to-complete basis. You are responsible for the disbursement of all loan advances made payable to you in trust and to ensure the priority of each advance by conducting all necessary searches and 
ensure that the requirements of the appropriate Provincial Construction Lien Act have been met 
before each advance is made. You are to retain appropriate amounts from the Mortgage proceeds to 
comply with Provincial lien holdback requirements. Unless otherwise noted, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> /  Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> will require a copy of the executed Certificate of Completion and Possession form and/or the Occupancy Certificate. 
\par
\par }{\b\f1\fs20\ul Disclosure:}{\f1\fs20  The Mortgagor(s) and the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> (if any) must sign a Statement of Disclosure. One copy is to be given to the 
Borrower(s) and a second copy is to be forwarded to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<!-- <xsl:text> and Xceed Funding Corporation </xsl:text> -->
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text>. Please ensure that the applicable provincial legislation and timeframes regarding 
Disclosure Statements are adhered to. If a Statement of Disclosure cannot be signed, you must 
obtain a waiver from the Mortgagor(s) and the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> (if any). 
\par 
\par }\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs20\ul\cf1 Planning Act:}{\b\f1\fs20\cf1  }{\f1\fs20\cf1 You must ensure that t}{\f1\fs20 
he Mortgage does not contravene the provisions of the Planning Act as amended from time to time, 
and that the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or 
right to grant, assign or exercise a power of appointment with respect to any land abutting the 
land secured by the Mortgage.
\par }\pard\plain \s2\qj \keepn\widctlpar\tx720\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs20 \par }\pard\plain \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul\cf1 Renewal for Guarantor(s):}{\f1\fs20  If applicable, the following wording must be included in a Schedule to the Charge/Mortgage: 
\par 
\par \'93In addition to the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>\rquote s promises and agreements contained in this Mortgage, the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>(s) also agree(s) that a}{\f1\fs20 t the sole discretion of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> /  Xceed Funding Corporation </xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
			</xsl:when>
		</xsl:choose>
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text>, the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s).\'93
\par
\par }
{\b\f1\fs20\ul\cf1 Corporation as a Borrower:}
{\f1\fs20\cf1  If the Mortgagor(s) or </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>(s) is a corporation, you must obtain a certified copy of the directors\rquote 
 resolution and the borrowing by-law of the corporation authorizing the Charge/Mortgage and ensure that the corporation is duly incorporated under applicable Provincial or Federal laws with all necessary power to charge its interest in the property and include these documents(s) with the Solicitor\rquote s Final Report on Title. \par }{\f1\fs20 \par }
{\b\f1\fs20\ul Solicitor\rquote s Report:}{\f1\fs20  You must submit the Solicitor\rquote s Final Report on Title, on the enclosed form and with 
the stated enclosures, within 30 days after the final advance is made. If your report cannot be submitted in this time frame, you must provide us with a letter explaining the reason(s) for the delay. Failure to comply with any of the above instructions may result in discontinued future dealings with you or your firm.
\par }</xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/PrimaryPropertyProvince = 'AB'">
				<xsl:call-template name="EnglishScheduleA2"/>
			</xsl:when>
			<xsl:when test="//SolicitorsPackage/PrimaryPropertyProvince = 'NS'">
				<xsl:call-template name="EnglishScheduleA3"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="EnglishScheduleA1"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--  Primary property province != AB or NS -->
	<xsl:template name="EnglishScheduleA1">
		<xsl:text>{\f1\fs20 \sect }\sectd \pghsxn20160\psz5 \linex0\endnhere\sectlinegrid360\sectdefaultcl 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3576 \cellx3468
\cltxlrtb\clftsWidth3\clwWidth3576 \cellx7044
\cltxlrtb\clftsWidth3\clwWidth3576 \cellx10620
\pard\plain \qj \sl240\slmult0\widctlpar\intbl
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs20 \page }{\f1\fs22 \cell }\pard\plain \s3\qc \sl240\slmult0\keepn\widctlpar\intbl
\tx-5016\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel2\adjustright
 \b\f1\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {SCHEDULE "A"\cell }\pard\plain \qr \sl240\slmult0\widctlpar\intbl
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs22 (ON, MB, SK, BC, NB, NF, PEI)}{\f1\fs22 \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs22 \trowd 
\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth3576 \cellx3468\cltxlrtb\clftsWidth3\clwWidth3576 \cellx7044\cltxlrtb\clftsWidth3\clwWidth3576 \cellx10620
\row }\pard \qj \sl240\slmult0\widctlpar
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0{\f1\fs22 

\par }\pard \ql \sl240\slmult0\widctlpar
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel0\adjustright
\itap0 </xsl:text>
		<!--Ticket#2324 xsl:choose>
			<xsl:when test="//SolicitorsPackage/LienPosition = 'SECOND'">
				<xsl:text>{\b\f1\fs20\ul Please insert clauses #1 - #5 into every mortgage and where applicable, please also insert clauses #6 &amp; #7.\par }</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\b\f1\fs20\ul Please insert clauses #1 to #5 and #9 into every mortgage and where applicable, please also insert clause #6 and #7 and #8.\par }</xsl:text>
			</xsl:otherwise>
		</xsl:choose-->
		<xsl:text>
{\f1\fs22 \par }
\pard \ql \sl240\slmult0\widctlpar
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0{\f1\fs22 
_(1)_ }{\b\f1\fs22 SALE OF CHARGE/CHANGE OF LOAN SERVICER: }{\f1\fs22 
The charge or a partial interest in the Charge may be sold and/or assigned one or more times without prior notice to the Chargor. A sale may result in a change in the entity (known as the \'93Loan Servicer\'94) that collec
ts monthly payments due under the Charge. There also may be one or more changes of the Loan Servicer unrelated to a sale of the Charge. The Chargor will not be required to deal with the new Loan Servicer until the Chargor has been given written notice of 
change.
\par 
\par _(2)_ }{\b\f1\fs22 APPLICATION OF PAYMENTS: }{\f1\fs22 If the Charge is repayable by blended 
installments of principal and interest the installments payable under the Charge are to be 
applied firstly to interest calculated as provided in the Charge on the principal monies from 
time to time outstanding and the balance of the said installments shall be applied on account of 
principal; after default by the Chargor, 
the Chargee may then apply any payments received during the period of default in whatever order it may elect as to principal, taxes, interest, repairs, insurance premiums or other advances made on behalf of the Chargor.
\par 
\par _(3)_ }{\b\f1\fs22 PREPAYMENT PRIVILEGES: }{\f1\fs22 You may prepay up to 20% of the original principal amount of your mortgage without notice, bonus or penalty.  If you do not use this privilege in a calendar year, you cannot carry forward this right of prepayment for that calendar year to any following calendar year.  This right of prepayment without notice, bonus or penalty does not apply if you prepay the principal amount of the mortgage in full.  This is the case even if you have not yet used this right of prepayment in the calendar year during which the mortgage is prepaid in full.  If you choose to exercise this right prior to prepaying the principal amount of the mortgage in full, the payment must be made on a scheduled payment date at least 30 days prior to the date of paying the mortgage in full.\par
In addition, provided the mortgage is not in default, the mortgagor(s) have the privilege of 
increasing their regular payment amount up to a maximum of 20% of the current principal and 
interest payment *once per calendar year.  These privileges are non-cumulative from year to year.
\par 
\par _(4)_ }{\b\f1\fs22 P.I.T. AND FREQUENCY CLAUSE: }{\f1\fs22 (This clause is applicable to all products)
\par Mortgage is to be registered monthly; however client(s) have chosen a payment frequency of </xsl:text>
		<xsl:value-of select="//PaymentFrequency"/>
		<xsl:text>, the principal and interest payments are </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PandIPayment"/>
		<xsl:text> plus a tax portion of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TaxPortion"/>
		<xsl:text> 
 (estimated). \par }\pard \ql \sl240\slmult0\widctlpar
\tx-1440\tx-1260\tx576\tx1296\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\tx27360\aspalpha\aspnum\faauto\adjustright
\itap0 {\f1\fs22 
\par _(5)_ }{\b\f1\fs22 FEES: }{\f1\fs22 The following Fees are in effect at the time of this Mortgage/Charge:
\par }\pard \ql \sl240\slmult0\widctlpar\tx-1440\tx-1260\tx540\tqdec\tx4752\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 \tab NSF Fee:\tab $75.00\tab \tab Stop Payment Fee:\tab $25.00
\par \tab Renewal Fee:\tab $100.00\tab \tab Transfer/Switch:\tab $250.00
\par \tab Assumption Fee:\tab $250.00\tab \tab Hold Payment:\tab $50.00
\par \tab Discharge Fee:\tab $125.00
\par 
\par _(6)_ }{\b\f1\fs22 CONDOMINIUM/STRATA: }{\f1\fs22 If the Chargor sells, transfers, assigns or conveys any parking unit(s) encumbered by this Charge while retaining title to (or ownership of) the dwelling unit so encumbered by this Charge, or if the Chargor sells, transfers, assigns or conveys the parking unit(s) as well as the dwelling unit but to different purchasers, transferees or assignees, then in either case the total outstanding principal and interest indebtedness secured by this Charge, at the option of the Chargee, immediately become due and payable.
\par }</xsl:text>
		<!--#DG362 rewritten -->
		
		<!--set format for this whole block-->
		<xsl:text>\f1\fs22 </xsl:text>
		<xsl:call-template name="Ticket_739"/>
		<xsl:if test="$secondMtg">
		  <xsl:call-template name="secondMtgClause"/>
		</xsl:if>
		
    <xsl:variable name="cntFromA">
  		<xsl:choose>
  			<xsl:when test="$secondMtg">10</xsl:when>
  			<xsl:otherwise>9</xsl:otherwise>
  		</xsl:choose>
    </xsl:variable>

		<xsl:text>\par 
_(</xsl:text>
		<xsl:value-of select="$cntFromA"/>
		<xsl:text>)_ </xsl:text>
		<xsl:call-template name="privacyClause"/>
		
  	<xsl:call-template name="armClauses">
  		<xsl:with-param name="cntFrom" select="$cntFromA+1"/>
  	</xsl:call-template>
		<!--set default format -->
		<xsl:text>\f1\fs22 </xsl:text>
		
		<!--#DG362 end -->
		
		<!-- <xsl:if test="//SolicitorsPackage/LienPosition = 'SECOND'">
			<xsl:text>{\par _(7)_ }{\b\f1\fs22 SECOND MORTGAGE: }{\f1\fs22 The Chargor acknowledges and agrees that in the event of default under the existing first mortgage, such default constitutes default under this Charge and entitles the Chargee to exercise its remedies hereunder.\par }</xsl:text>
		</xsl:if> -->
	</xsl:template>


	<!--  Primary property province = AB -->
	<xsl:template name="EnglishScheduleA2">
		<xsl:text>{\f1\fs20\sect }\sectd \pghsxn20160\psz5 \linex0\endnhere\sectlinegrid360
\sectdefaultcl \pard\plain \ql \sl240\slmult0\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440
\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum
\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs22 \cell }\pard\plain \s3\qc 
\sl240\slmult0\keepn\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440
\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum
\faauto\outlinelevel2\adjustright 
\b\f1\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {SCHEDULE "A"\cell }\pard\plain \qr 
\sl240\slmult0\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440
\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum
\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs22 (ALBERTA ONLY)}
{\f1\fs22 \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs22 
\trowd 
\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3576 \cellx3468\cltxlrtb\clftsWidth3\clwWidth3576 \cellx7044
\cltxlrtb\clftsWidth3\clwWidth3576 \cellx10620
\row }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720
\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640
\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22  
\par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720
\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640
\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0</xsl:text>
		<!--Ticket#2324xsl:choose>
			<xsl:when test="//SolicitorsPackage/LienPosition = 'SECOND'">
				<xsl:text>{\b\f1\fs20\ul Please insert clauses #1 - #5 into every mortgage and where applicable, please also insert clauses #6 &amp; #7.\par }</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\b\f1\fs20\ul Please insert clauses #1 to #5 and #9 into every mortgage and where applicable, please also insert clause #6 and #7 and #8.\par }</xsl:text>
			</xsl:otherwise>
		</xsl:choose-->
		<xsl:text>{\f1\fs22 \par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 _(1)_ }{
\b\f1\fs22 SALE OF CHARGE/CHANGE OF LOAN SERVICER: }{\f1\fs22 The charge or a partial interest in the Charge may be sold and/or assigned one or more times without prior notice to the Chargor. A sale may result in a change in the entity (known as the \'93
Loan Servicer\'94) that collects monthly payments due under the Charge. There also may be one or more changes of the Loan Servicer unrelated to a sale of the Charge. The C
hargor will not be required to deal with the new Loan Servicer until the Chargor has been given written notice of change.
\par 
\par _(2)_ }{\b\f1\fs22 APPLICATION OF PAYMENTS: }{\f1\fs22 If the Charge is repayable by blended installments of principal and interest the installments payable 
under the Charge are to be applied firstly to interest calculated as provided in the Charge on 
the principal monies from time to time outstanding and the balance of the said installments shall 
be applied on account of principal; after default by the Chargor, the Chargee may then apply any 
payments received during the period of default in whatever order it may elect as to principal, 
taxes, interest, repairs, insurance premiums or other advances made on behalf of the Chargor.
\par 
\par _(3)_ }{\b\f1\fs22 PREPAYMENT PRIVILEGES: }{\f1\fs22 You may prepay up to 20% of the original principal amount of your mortgage without notice, bonus or penalty.  If you do not use this privilege in a calendar year, you cannot carry forward this right of prepayment for that calendar year to any following calendar year.  This right of prepayment without notice, bonus or penalty does not apply if you prepay the principal amount of the mortgage in full.  This is the case even if you have not yet used this right of prepayment in the calendar year during which the mortgage is prepaid in full.  If you choose to exercise this right prior to prepaying the principal amount of the mortgage in full, the payment must be made on a scheduled payment date at least 30 days prior to the date of paying the mortgage in full.\par
In addition, provided the mortgage is not in default, the mortgagor(s) have the privilege of 
increasing their regular payment amount up to a maximum of 20% of the current principal and 
interest payment *once per calendar year.  These privileges are non-cumulative from year to year.
\par 
\par _(4)_ }{\b\f1\fs22 P.I.T.  AND FREQUENCY CLAUSE: }{\f1\fs22 (This clause is applicable to all products)
\par Mortgage is to be registered monthly; however client(s) have chosen a payment frequency of </xsl:text>
		<xsl:value-of select="//PaymentFrequency"/>
		<xsl:text>, the principal and interest payments are </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PandIPayment"/>
		<xsl:text> plus a tax portion of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TaxPortion"/>
		<xsl:text> 
 (estimated). \par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\tx27360\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 
\par _(5)_ }{\b\f1\fs22 FEES: }{\f1\fs22 The following Fees are in effect at the time of this Mortgage/Charge:
\par }
\pard \ql \sl240\slmult0\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 \tab NSF Fee:\tab $75.00
\par \tab Transfer/Switch Fee:\tab $250.00
\par 
\par _(6)_ }{\b\f1\fs22 CONDOMINIUM/STRATA: }{\f1\fs22 If the Chargor sells, transfers, assigns or conveys any parking unit(s) encumbered by this Charge while retaining title to (or ownership of) the dwelling unit so encumbered by this Charge, or if the Chargor sells, transfers, assigns or conveys the parking unit(s) as well as the dwelling unit but to different purchasers, transferees or assignees, then in either case the total outstanding principal and interest indebtedness secured by this Charge, at the option of the Chargee, immediately become due and payable.
\par }</xsl:text>
		
		<!--#DG362 rewritten -->
		
		<!--set format for this whole block-->
		<xsl:text>\f1\fs22 </xsl:text>
		<xsl:call-template name="Ticket_739"/>
		<xsl:if test="$secondMtg">
		  <xsl:call-template name="secondMtgClause"/>
		</xsl:if>
		
    <xsl:variable name="cntFromA">
  		<xsl:choose>
  			<xsl:when test="$secondMtg">10</xsl:when>
  			<xsl:otherwise>9</xsl:otherwise>
  		</xsl:choose>
    </xsl:variable>

		<xsl:text>\par 
_(</xsl:text>
		<xsl:value-of select="$cntFromA"/>
		<xsl:text>)_ </xsl:text>
		<xsl:call-template name="privacyClause"/>
		
  	<xsl:call-template name="armClauses">
  		<xsl:with-param name="cntFrom" select="$cntFromA+1"/>
  	</xsl:call-template>
		<!--set default format -->
		<xsl:text>\f1\fs22 </xsl:text>
		
		<!--#DG362 end -->

		<!-- <xsl:if test="//SolicitorsPackage/LienPosition = 'SECOND'">
			<xsl:text>{\par _(7)_ }{\b\f1\fs22 SECOND MORTGAGE: }{\f1\fs22 The Chargor acknowledges and agrees that in the event of default under the existing first mortgage, such default constitutes default under this Charge and entitles the Chargee to exercise its remedies hereunder.\par }</xsl:text>
		</xsl:if> -->
	</xsl:template>

	<!--  Primary property province = NS -->
	<xsl:template name="EnglishScheduleA3">
		<xsl:text>{\sect }\sectd \pghsxn20160\psz5 \linex0\endnhere\sectlinegrid360\sectdefaultcl \pard\plain \ql \sl240\slmult0\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs20 \cell }\pard\plain \s3\qc \sl240\slmult0\keepn\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
\b\f1\fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {SCHEDULE "A"\cell }\pard\plain \qr \sl240\slmult0\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs20 (NOVA SCOTIA)}{\f1\fs20 \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs20 \trowd 
\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth3576 \cellx3468\cltxlrtb\clftsWidth3\clwWidth3576 \cellx7044\cltxlrtb\clftsWidth3\clwWidth3576 \cellx10620
\row }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs20 \par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0</xsl:text>
		<!--Ticket#2324xsl:choose>
			<xsl:when test="//SolicitorsPackage/LienPosition = 'SECOND'">
				<xsl:text>{\b\f1\fs20\ul Please insert clauses #1 - #5 into every mortgage and where applicable, please also insert clauses #6 &amp; #7.\par }</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\b\f1\fs20\ul Please insert clauses #1 to #5 and #9 into every mortgage and where applicable, please also insert clause #6 and #7 and #8.\par }</xsl:text>
			</xsl:otherwise>
		</xsl:choose-->
		<xsl:text>{\f1\fs20 \par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 _(1)_ }{
\b\f1\fs20 SALE OF CHARGE/CHANGE OF LOAN SERVICER: }{\f1\fs20 The charge or a partial interest in the Charge may be sold and/or assigned one or more times without prior notice to the Chargor. A sale may result in a change in the entity (known as the \'93L
oan Servicer\'94
) that collects monthly payments due under the Charge. There also may be one or more changes of the Loan Servicer unrelated to a sale of the Charge. The Chargor will not be required to deal with the new Loan Servicer until the Chargor has been given written notice of change.
\par 
\par _(2)_ }{\b\f1\fs20 APPLICATION OF PAYMENTS: }{\f1\fs20 If the Charge is repayable by blended 
installments of principal and interest the installments payable under the Charge are to be 
applied firstly to interest calculated as provided in the Charge on the principal monies from 
time to time outstanding and the balance of the said installments shall be applied on account of 
principal; after default by the Chargor, the Chargee may then apply any payments received during 
the period of default in whatever order it may elect as to principal, taxes, interest, repairs, 
insurance premiums or other advances made on behalf of the Chargor.
\par 
\par _(3)_ }{\b\f1\fs20 PREPAYMENT PRIVILEGES: }{\f1\fs22 You may prepay up to 20% of the original principal amount of your mortgage without notice, bonus or penalty.  If you do not use this privilege in a calendar year, you cannot carry forward this right of prepayment for that calendar year to any following calendar year.  This right of prepayment without notice, bonus or penalty does not apply if you prepay the principal amount of the mortgage in full.  This is the case even if you have not yet used this right of prepayment in the calendar year during which the mortgage is prepaid in full.  If you choose to exercise this right prior to prepaying the principal amount of the mortgage in full, the payment must be made on a scheduled payment date at least 30 days prior to the date of paying the mortgage in full.\par
In addition, provided the mortgage is not in default, the mortgagor(s) have the privilege of 
increasing their regular payment amount up to a maximum of 20% of the current principal and 
interest payment *once per calendar year.  These privileges are non-cumulative from year to year.
\par 
\par _(4)_ }{\b\f1\fs20 P.I.T.  AND FREQUENCY CLAUSE: }{\f1\fs20 (This clause is applicable to all products)
\par Mortgage is to be registered monthly; however client(s) have chosen a payment frequency of </xsl:text>
		<xsl:value-of select="//PaymentFrequency"/>
		<xsl:text>, the principal and interest payments are </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PandIPayment"/>
		<xsl:text> plus a tax portion of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TaxPortion"/>
		<xsl:text> 
 (estimated). \par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\tx27360\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par _(5)_ }{\b\f1\fs20 FEES: }{\f1\fs20 The following Fees are in effect at the time of this Mortgage/Charge:
\par }

\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\pard \ql \sl240\slmult0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 NSF Fee:\tab \tab $75.00\cell Stop Payment Fee:\tab $25.00\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\row 
}
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\pard \ql \sl240\slmult0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Renewal Fee:\tab \tab $100.00\cell Transfer/Switch:\tab $25.00\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\row 
}
\pard \ql \sl240\slmult0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Assumption Fee:\tab $250.00\cell Hold Payment:\tab $50.00\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\row 
}
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\pard \ql \sl240\slmult0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Discharge Fee:\tab $125.00\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\row 
}
\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par _(6)_ }{
\b\f1\fs20 CONDOMINIUM PROVISIONS: }{\f1\fs20 If the Mortgage Land is condominium unit or 
includes a condominium unit, the following provisions shall apply:\par\par 

a) the Mortgage shall observe and perform each of the covenants and provisions required to be 
observed and performed pursuant to the Mortgage, the Condominium Act, all amendments thereto, all 
legislation passed in substitution therefor, and the declaration, by-laws and rules, as amended 
from time to time, of the condominium corporation of which the Mortgagor is a member by virtue of 
the Mortgagor’s ownership of the condominium unit charged by the Mortgage:\par\par 

b) the Mortgagor shall pay promptly when due any and all unpaid common expenses, common element 
expenses, assessments, levies, instalments, payments or any other amounts due to the applicable 
condominium corporation or any agent thereof by the Mortgagor and, at the Mortgagee’s request, 
deliver to the Mortgagee evidence of the payment thereof:\par\par 

c) upon Default herein and notwithstanding any other right or action of such condominium 
corporation or the Mortgagee, the Mortgagee may distrain for arrears of any assessments, levies, 
instalments, payments or any other amounts due to the Mortgagee or arising pursuant to this 
provision; and\par\par 

d) (i) Notwithstanding any authorization, including a provision in any agreement, mortgage or 
charge that is made on, before or after the coming into force of this subsection, the holder of a 
mortgage or charge on a unit may not exercise any right that another person has, by reason of 
being the owner of a unit, to vote or consent, unless the holder of the mortgage or charge is a 
mortgagee in possession.\par\par 

     (ii) For greater certainty, nothing in subsection (i) invalidates any vote that was cast or 
any consent that was given before the coming into force of this subsection.\par\par }

</xsl:text>

		<!--#DG362 rewritten -->
		
		<!--set format for this whole block-->
		<xsl:text>\f1\fs20 </xsl:text>
		<xsl:call-template name="Ticket_739"/>
		<xsl:if test="$secondMtg">
		  <xsl:call-template name="secondMtgClause"/>
		</xsl:if>
		
    <xsl:variable name="cntFromA">
  		<xsl:choose>
  			<xsl:when test="$secondMtg">10</xsl:when>
  			<xsl:otherwise>9</xsl:otherwise>
  		</xsl:choose>
    </xsl:variable>

		<xsl:text>\par 
_(</xsl:text>
		<xsl:value-of select="$cntFromA"/>
		<xsl:text>)_ </xsl:text>
		<xsl:call-template name="privacyClause"/>
		
  	<xsl:call-template name="armClauses">
  		<xsl:with-param name="cntFrom" select="$cntFromA+1"/>
  	</xsl:call-template>
		<!--set default format -->
		<xsl:text>\f1\fs22 </xsl:text>
		
		<!--#DG362 end -->

		<!-- <xsl:if test="//SolicitorsPackage/LienPosition = 'SECOND'">
			<xsl:text>{\par _(7)_ }{\b\f1\fs22 SECOND MORTGAGE: }{\f1\fs22 The Chargor acknowledges and agrees that in the event of default under the existing first mortgage, such default constitutes default under this Charge and entitles the Chargee to exercise its remedies hereunder.\par }</xsl:text>
		</xsl:if> -->
	</xsl:template>

	<xsl:template name="EnglishRequestForFunds">
		<xsl:text>{\sect\sectd } \pghsxn20160\psz5
{\header \pard\plain \s20\ql \sl240\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\par }}
\trowd \trgaph108\trrh719\trleft1800
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx8820
\pard\plain \s6\qc \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\ul\i\f1\fs28 SOLICITOR\rquote S REQUEST FOR FUNDS\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh719\trleft1800
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx8820
\row 
}
\pard \ql \fi720\li5040\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5040\itap0 
{\f1\fs20\par\par}

\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\clvmgf\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 FAX TO:\cell }
{\b\f1\fs20 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 FROM:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/>
		<xsl:text>\par }
{\f1\fs20 
Phone: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\clvmgf\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\clvmrg\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 ATTENTION:\cell }
{\b\f1\fs20 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs14 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\clvmrg\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\clvmrg\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 DATE:\cell }
{\f1\fs20 ________________\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\clvmrg\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}

\pard \ql \widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\b\f1\fs20 {\pict{\*\picprop\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn 
fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}
{\sp{\sn fLine}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}
\picscalex1080\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0
\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag907321276\blipupi1439{\*\blipuid 
36149fbcc681ce015720994253f9dec0}010009000003310000000000070000000000050000000b0200000000050000000c02f401e80307
0000001b04f401e80300000000050000001402000000000500
00001302f401e803050000001402f40100000500000013020000e803030000000000}
\par }
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs20 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 LENDER REFERENCE NUMBER:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/MortgageNum"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 MORTGAGOR(S):\cell </xsl:text>
		<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 MUNICIPAL ADDRESS:\cell 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 MORTGAGE AMOUNT:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 CLOSING DATE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 {\pict{\*\picprop\shplid1026{\sp{\sn 
shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}\picscalex1080\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag907321276\blipupi1439{\*\blipuid 
36149fbcc681ce015720994253f9dec0}010009000003310000000000070000000000050000000b0200000000050000000c02f401e803070000001b04f401e80300000000050000001402000000000500
00001302f401e803050000001402f40100000500000013020000e803030000000000}}
{\f1\fs20 \par }
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs20 \par }
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 I/We request funds for closing on ________________.\par }
\pard \s15\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 I/We acknowledge that there are no amendments and/or schedules to the Agreement of Purchase and Sale which 
affect the purchase price or deposit amount as set out in your Mortgage Commitment Letter.\par }
{\f1\fs20 \par }
{\b\f1\fs20 I/We confirm that:\par }
{\f1\fs20 \par }

\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 [    ]\tab All terms and conditions set out in your instructions have been satisfied.
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 [    ]\tab We are of the opinion that the mortgage has been prepared and executed in accordance with your instructions and when registered the mortgagor(s) will have good and marketable title free and clear of all judgements and other charges and you will have a valid }{\b\f1\fs20 </xsl:text>
		<!-- <xsl:value-of select="//SolicitorsPackage/LienPosition"/> -->
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderProfileId = '0'"> FIRST </xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//SolicitorsPackage/LienPosition = 'FIRST'"> FIRST </xsl:when>
					<xsl:otherwise> SECOND </xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<!-- <xsl:text> FIRST </xsl:text> -->
		<!-- <xsl:choose>
	<xsl:when test="//SolicitorsPackage/LienPosition = 'SECOND'"> FIRST </xsl:when>	
	<xsl:otherwise> SECOND </xsl:otherwise>
</xsl:choose>		-->
		<xsl:text>}{\f1\fs20  against the property subject to the exceptions listed below.
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 Easements, encroachments, reservations, etc, which do not affect the priority of the mortgage or the marketability of the property.
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par 
\par }\pard \s15\ql \li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 
Easements, encroachments, reservations, mortgages, executions or other encumbrances which do affect the priority of the mortgage or the marketability of the property.
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 [    ]\tab }{\b\f1\fs20 We confirm that all taxes due have been paid or will be paid on closing up to and including </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>.}{\f1\fs20 
\par 
\par [    ]\tab We have obtained a surveyor\rquote 
s certificate/Real Property Report with Certificate of Compliance (RPR) dated _______________. The survey/RPR indicates that the building and structure compromising the mortgaged property are situated wholly within the limits 
of the lot lines, and that there are no encroachments, easements, or right of ways except as disclosed herein.
\par 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 
\par [    ]\tab All documents required prior to release of the mortgage proceeds have been forwarded with the Solicitor\rquote s Request for Funds for </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> /  Xceed Funding Corporation </xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
			</xsl:when>
		</xsl:choose>
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> to review.  {\b Verbal authorization from </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> /  Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose>		 -->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> will be required in order to release the mortgage proceeds. Should this mortgage not close as scheduled, Xceed Mortgage Corporation/Xceed Funding Corporation will require that funds be returned by wire, within three (3) business days of the originally scheduled closing date.}
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\b\f1\fs20 DELIVERY OF FUNDS:
\par }{\f1\fs20 
\par [    ]\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> / Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> and </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> does }{\f1\fs20\ul not}{\f1\fs20  issue cheques. Please make sure \'93Solicitor\rquote s Request For Funds\'94 is }{\f1\fs20\ul received}{\f1\fs20  by 12:00 pm EST four (4) business days prior to closing. Failure to do so will delay the currently scheduled closing date.
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 Wire funds to my/our Trust Account:
\par }\pard \s15\ql \fi-720\li720\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 \tab Name of Trust Account _____________________________________________________
\par \tab Branch # (5 digits)_______________ Institution # _______________ Account # _______________
\par \tab Branch Name and Address __________________________________________________
\par \tab \tab \tab \tab       __________________________________________________
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\b\f1\fs20 
\par }\pard \qr \fi720\li5040\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5040\itap0 {\f1\fs20\cf1 _____________________________________________
\par }\pard \qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 Signature of Solicitor}{\b\f1\fs20 
\par }\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs18\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs20 \page 
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\b\f1\fs20 DOCUMENTS REQUIRED PRIOR TO RELEASE OF THE MORTGAGE PROCEEDS:
\par 
\par }\pard\plain \s16\qj \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 \fs16\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{\f1\fs20 [ X ]\tab Draft Mortgage with applicable schedule(s)
\par [ X ]\tab Consumer Pre-Authorized Debit Plan (Consumer PAD) form together with \'93VOID\'94 cheque
\par }\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs20 [    ]\tab Signed Statement of Disclosure
\par }{\f1\fs20\cf1 [ X ]\tab Signed Statutory Declaration (re: owner-occupied, no secondary financing, downpayment)}{\f1\fs20 
\par [    ]\tab Well Water Test/Certificate, indicating that the water is potable and fit for human consumption
\par [    ]\tab New Home Warranty Certificate of Possession
\par [    ]\tab New Home Warranty Builder\rquote s Registration Number and Unit Enrollment Number
\par [    ]\tab Assignment of Condominium Voting Rights
\par [    ]\tab Solicitor\rquote s undertaking to payout }{\f1\fs20\ul and close}{\f1\fs20  debts (if applicable, as per Mortgage Commitment)
\par [    ]\tab Signed Authorization to Send Assessment and  Tax Bill (#45-06-20) PEI
\par [ X ]\tab Survey or Title Insurance (if applicable)
\par [ X ]\tab Wire instructions for Trust Account
\par [    ]\tab Up-to-date Status Certificate (if applicable)
\par [    ]\tab Personal Guarantee and Letter of Independent Legal Advice
\par }
\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\cf1 [    ]\tab Other (specify) _____________________________________________________________________________
\par [    ]\tab __________________________________________________________________________________________
\par [    ]\tab __________________________________________________________________________________________
\par [    ]\tab __________________________________________________________________________________________
\par [    ]\tab __________________________________________________________________________________________
\par }{\f1\fs20\cf1 \page 
\par 
\par }\pard\plain \s6\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel5\adjustright\itap0 \b\f1\fs28\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {STATUTORY DECLARATION
\par }\pard\plain \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs20\cf1 
\par 
\par }{\b\f1\cf1 CANADA\tab \tab \tab \tab ) IN THE MATTER OF a charge
\par }\pard \qj \fi720\li2880\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin2880\itap0 
{\b\f1\cf1 ) from </xsl:text>
		<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>
\par ) to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> / Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text>
\par )\par }

\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\cf1 PROVINCE OF\tab \tab \tab )\par\par }

\pard\plain \s7\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel6\adjustright\itap0 
\b\f1\fs24\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {I/We </xsl:text>
		<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>, of the City of
\par }\pard\plain \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\cf1 In the Province of\tab \tab \tab \tab \tab , do solemnly declare:
\par 
\par \tab 1.\tab The property will be owner-occupied by myself after closing.
\par \tab 2.\tab There is no secondary financing being registered on the property.
\par \tab 3.\tab The funds for my downpayment are not borrowed.
\par }\pard \qj \li360\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin360\itap0 {\b\f1\cf1 
\par }\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\cf1 AND I/WE MAKE THIS SOLEMN DECLARATION, conscientiously, believe it to be true and knowing that it is of the same effect as if made under oath.
\par 
\par DECLARED before me at the City of
\par }\pard \qj \fi720\li2160\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin2160\itap0 {\b\f1\cf1 , in the
\par }\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\cf1 Province of
\par This\tab \tab day of\tab \tab \tab , </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CurrentYear"/>
		<xsl:text>.
\par 
\par }\pard \qj \li360\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin360\itap0 {\b\f1\cf1 
\par }\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\cf1 (Solicitor)\tab \tab \tab \tab \tab \tab \tab \tab ____________________________
\par }\pard \qj \fi720\li5760\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5760\itap0 {\b\f1\cf1 (Mortgagor\rquote s Signature)
\par }
{\b\f1\fs20\cf1 __________________________________\par \par }</xsl:text>
		<xsl:if test="//SolicitorsPackage/UndertakingFlag">
			<xsl:text>
{\b\fs20 \page }
\pard\plain \s17\qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{ \b\f1\fs24 UNDERTAKING\par \par }
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth10374 \cellx10908
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 TO:\cell }{\f1\fs22 </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text> / Xceed Funding Corporation </xsl:text>
			<xsl:choose>
				<xsl:when test="//SolicitorsPackage/LenderNameFund">
					<xsl:text> / </xsl:text>
					<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
				</xsl:when>
			</xsl:choose>
			<!-- <xsl:text>/</xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth10374 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 RE:\cell }
{\b\f1\fs22 MORTGAGE LOAN #:\cell }
{\b\f1\fs22 </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/MortgageNum"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 NAME(S):\cell </xsl:text>
			<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		  <!--#DG460 only main applicants-->
  		<xsl:call-template name="BorrowerFullName">
        <xsl:with-param name="separa" select="', '"/>
      </xsl:call-template>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 PROPERTY ADDRESS:\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
			<xsl:text/>
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
			<xsl:text>\par </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
			<xsl:text/>
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs22 \par \par \par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs22 IN CONSIDERATION }{\f1\fs22 of and notwithstanding the closing of the above noted transaction, I hereby undertake as follows:
\par 
\par }
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 1.\cell }
{\f1\fs22 TO pay from the mortgage advance proceeds the following debts as 
per account payment Statement for each and to }{\b\f1\fs22 
instruct accounts to be closed:}{\f1\fs22 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\row 
}
</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/LiabilitiesClose/Liability">
				<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx4320
\cltxlrtb\clftsWidth3\clwWidth6588 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 \cell }
{\f1\fs22 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx4320
\cltxlrtb\clftsWidth3\clwWidth6588 \cellx10908
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs22 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 2.\cell }
{\f1\fs22 TO pay from the mortgage advance proceeds the following debts as per account Payment 
statement for each and }{\b\f1\fs22 accounts to remain open:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\row 
}
</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/LiabilitiesOpen/Liability">
				<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx4320
\cltxlrtb\clftsWidth3\clwWidth6588 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 \cell }
{\f1\fs22 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx4320
\cltxlrtb\clftsWidth3\clwWidth6588 \cellx10908
\row 
}
</xsl:text>
			</xsl:for-each>
			<xsl:text>
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs22 
\par 
\par 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs22 
\par 
\par 
\par 
\par 
\par 
\par }
{\b\f1\fs22 DATED }{\f1\fs22 at                                        , this                day of                             , </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/CurrentYear"/>
			<xsl:text>.
\par 
\par }
\pard \ql \li780\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin780\itap0 
{\f1\fs22 \par \par }
\pard \ql \fi720\li5040\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin5040\itap0 
{\f1\fs22 _________________________________________}
{\b\f1\fs22 
\par </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/SolicitorName"/>
			<xsl:text>
\par Barrister &amp; Solicitor
\par }
</xsl:text>
		</xsl:if>
		<xsl:text>\pard\plain \s6\qc \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel5\adjustright\itap0 
{ \page }{\f1\fs28\b\ul STATEMENT OF FUNDS RECEIVED &amp; DISBURSED\par }
\pard\plain \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\cf1 \par }
{\b\f1\cf1 Property: }{\b\f1\cf1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/PostalCode"/>
		<xsl:text>}{\b\f1\cf1 
\par }\pard\plain \s7\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel6\adjustright\itap0 \b\f1\fs24\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {Mortgage Number: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/MortgageNum"/>
		<xsl:text>
\par }
\pard\plain \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{\b\f1\cf1 Mortgagor(s): </xsl:text>
		<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>
\par }\pard\plain \s7\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel6\adjustright\itap0 \b\f1\fs24\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {
________________________________________________________________________________
\par FUNDS RECEIVED:
\par From:
\par }\pard\plain \ql \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> / Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose>		 -->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text>\tab \tab \tab \tab $
\par \tab \tab \tab \tab \tab \tab \tab \tab \tab \tab \tab \tab $
\par }\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 
\par }\pard\plain \s7\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel6\adjustright\itap0 
{\b\f1\fs24 FUNDS DISBURSED:\par }
\pard\plain \ql \li5760\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5760\itap0 
{\f1\fs24 $\par }
\pard \ql \fi720\li5040\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5040\itap0 
{\f1 $
\par $
\par $
\par $
\par $
\par $
\par $
\par $
\par }\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 \tab \tab \tab \tab \tab \tab \tab \tab ____________________\tab ________________
\par }{\b\f1\cf1 TOTAL:}{\f1 \tab \tab \tab \tab \tab \tab \tab $\tab \tab \tab \tab $
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par ___________________________________________\tab \tab ________________
\par }{\f1\lang1036\langfe1033\langnp1036 Solicitor\rquote s Signature\tab \tab \tab \tab \tab \tab \tab \tab Date
\par 
\par }

\pard \qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\cf1\lang1036\langfe1033\langnp1036 \sect }\sectd \pghsxn15840\marglsxn432\margrsxn432\psz1\linex0\headery547\footery475\endnhere\sectlinegrid360\sectdefaultcl 
\pard\plain \qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
</xsl:text>
		<xsl:call-template name="XceedLogo"/>
		<xsl:text>
{\par }\pard \s17\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }\pard \s17\qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs24 AUTHORIZATION FOR CONSUMER\par }
\pard\plain \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs24 PRE-AUTHORIZED DEBIT PLAN
\par Authorization of the Payor to the Payee to Direct Debit an Account
\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1 \par }{\f1 Instructions:\par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 1.\cell Please complete all sections in order to instruct your financial institution to 
make payments directly from your account.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 2.\cell Please sign the Terms and Conditions attached to this document.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 3.\cell Return the completed form with a blank cheque marked \'93VOID\'94 to the payee at the address noted below.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 4.\cell If you have any questions, please write or call the payee.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs28\b 5.\cell ****PLEASE ENSURE YOUR ACCOUNT IS OPEN, HAS CHEQUING PRIVILEDGES AND THE ACCOUNT NUMBERS ARE CLEARLY DISPLAYED*** \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par PAYOR INFORMATION }{\scaps\f1 (Please type or print clearly)}{\f1 
\par }
\trowd \trgaph108\trrh528\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Payor Name:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh528\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\row 
}
\trowd \trgaph108\trrh529\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Address:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh529\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\row 
}
\trowd \trgaph108\trrh528\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Telephone:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1 \trowd \trgaph108\trrh528\trleft10\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861\row }\trowd \trgaph108\trrh529\trleft10\trbrdrt
\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1 Signature 
of Payor(s):                                                                           Date:\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1 \trowd \trgaph108\trrh529\trleft10\trbrdrt\brdrs\brdrw10 \trbrdrl
\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861\row }\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 
\par PAYOR FINANCIAL INSTITUTION/BANKING INFORMATION
\par }
\trowd \trgaph108\trrh494\trleft10\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\lang1024\langfe1024\noproof 
{\shp{\*\shpinst\shpleft8280\shptop200\shpright8280\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz18\shplid1026{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8210\dpline\dpptx0\dppty0\dpptx0\dppty288
\dpx8280\dpy200\dpxsize0\dpysize288\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft7848\shptop200\shpright7848\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz17\shplid1027
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8209\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx7848\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft7416\shptop200\shpright7416\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz16\shplid1028
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8208\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx7416\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft6984\shptop200\shpright6984\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz15\shplid1029
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8207\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx6984\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft6552\shptop200\shpright6552\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz14\shplid1030
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8206\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx6552\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft6120\shptop200\shpright6120\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz13\shplid1031
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8205\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx6120\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft5688\shptop200\shpright5688\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz12\shplid1032
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8204\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx5688\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft5256\shptop200\shpright5256\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz11\shplid1033
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8203\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx5256\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft4824\shptop200\shpright4824\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz10\shplid1034
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8202\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx4824\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft4392\shptop200\shpright4392\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz9\shplid1035
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8201\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx4392\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft3960\shptop200\shpright3960\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz8\shplid1036
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8200\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx3960\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft2520\shptop200\shpright2520\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz6\shplid1037
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8198\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx2520\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft2952\shptop200\shpright2952\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz7\shplid1038
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8199\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx2952\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft792\shptop200\shpright792\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz3\shplid1039
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8195\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx792\dpy200\dpxsize0\dpysize288\dplinew15\dplinecor0\dplinecog0\dplinecob0}
}}{\shp{\*\shpinst\shpleft360\shptop200\shpright360\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz2\shplid1040{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8194\dpline\dpptx0\dppty0\dpptx0\dppty288
\dpx360\dpy200\dpxsize0\dpysize288\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft1224\shptop200\shpright1224\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz4\shplid1041
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8196\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx1224\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft1656\shptop200\shpright1656\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz5\shplid1042
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8197\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx1656\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft3528\shptop56\shpright3528\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz1\shplid1043
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8193\dpline\dpptx0\dppty0\dpptx0\dppty432\dpx3528\dpy56\dpxsize0\dpysize432\dplinew15\dplinecor0\dplinecog0\dplinecob0}
}}{\shp{\*\shpinst\shpleft2088\shptop56\shpright2088\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz0\shplid1044{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8192\dpline\dpptx0\dppty0\dpptx0\dppty432
\dpx2088\dpy56\dpxsize0\dpysize432\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}}
{\f1\fs24 Branch Number      Institution #   Account Number\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh494\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\trowd \trgaph108\trrh462\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 Name of Financial Institution:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh462\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\trowd \trgaph108\trrh463\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 Branch:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh463\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\trowd \trgaph108\trrh462\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 Branch Address:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh462\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\trowd \trgaph108\trrh463\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 City/Province:                                                                    Postal Code:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh463\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par  PAYEE INFORMATION\par }
\trowd \trgaph108\trrh449\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Payee Name:\par\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> / Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh449\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\row 
}
\trowd \trgaph108\trrh449\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Address:\par\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/>
		<xsl:text>
\par\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh449\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\row 
}
\trowd \trgaph108\trrh450\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Telephone:\par\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Phone"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh450\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1 \page AUTHORIZATION FOR CONSUMER PRE-AUTHORIZED DEBIT PLAN\par }
\pard\plain \s8\qc \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel7\adjustright\itap0 
{\b\f1\fs22 Terms &amp; Conditions\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs24 \par }
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 1.\cell In this Authorization, \'93I\'94, \'93me\'94 and \'93my\'94 refers to each Account Holder who signs below.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 2.\cell I agree to participate in this Pre-Authorized Debit Plan for personal/household or consumer purposes and I authorize the Payee indicated on the reverse hereof and any successor or assign of the Payee to draw a debit in paper, electronic or other form for the purpose of making payment for consumer goods or services (a \'93Consumer PAD\'94), on my account indicated on the reverse hereof (the \'93Account\'94) at the financial institution indicated on the reverse hereof  (the \'93Financial Institution\'94) and I authorize the Financial Institution to honour and pay such debits. This Authorization is provided for the benefit of the Payee and my Financial Institution and is provided in consideration of my Financial Institution agreeing to process debits against my Account in accordance with the Rules of the Canadian Payments Association. I agree that any direction I may provide to draw a Consumer PAD and any Consumer PAD drawn in accordance with this Authorization, shall be binding on me as if signed by me, and, in the case of paper debits, as if they were cheques signed by me.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 3.\cell I may revoke this Authorization at any time by delivering a written notice of revocation to the Payee. This Authorization applies only to the method of payment and I agree that revocation of this Authorization does not terminate or otherwise have any bearing on any contract that exists between the Payee and me.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 4.\cell I agree that my Financial Institution is not required to verify that any Consumer PAD has been drawn in accordance with this Authorization, including the amount, frequency and fulfillment of any purpose of any Consumer PAD.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 5.\cell I agree that delivery of this Authorization to the Payee constitutes delivery by me to my Financial Institution. I agree that the Payee may deliver this Authorization to the Payee\rquote s financial institution and agree to the disclosure of any personal information that may be contained in this Authorization to such financial institution.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 6.\cell I understand that with respect to:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth716 \cellx1513
\cltxlrtb\clftsWidth3\clwWidth9395 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell \cell (i)\cell fixed amount Consumer PADs, we shall receive written notice from the Payee of the amount to be debited and the due date(s) of debiting, at least ten (10) calendar days before the due date of the first Consumer PAD, and such notice shall be received every time there is a change in the amount or payment date(s);\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth716 \cellx1513
\cltxlrtb\clftsWidth3\clwWidth9395 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell \cell (ii)\cell variable amount Consumer PADs, we shall receive written notice from the Payee of the amount to be debited and the due date(s) of debiting, at least ten (10) calendar days before the due date of every Consumer PAD; and\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth716 \cellx1513
\cltxlrtb\clftsWidth3\clwWidth9395 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell \cell (iii)\cell a Consumer PAD Plan that provides for the issuance of a Consumer PAD in response to my direct action (such as, but not limited to, a telephone instruction) requesting the Payee to issue a Consumer PAD in full or partial payment of a billing received by us, the ten (10) day pre-notification is waived.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth716 \cellx1513
\cltxlrtb\clftsWidth3\clwWidth9395 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 7.\cell I may dispute a Consumer PAD by providing a signed declaration to my Financial Institution under the following conditions:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth10111 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell (a)\cell the Consumer PAD was not drawn in accordance with this Authorization;\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth10111 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell (b)\cell this Authorization was revoked;\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth10111 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell (c)\cell any pre-notification required by section 6 was not received by me;\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth10111 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell I acknowledge that in order to obtain reimbursement from my Financial Institution for the amount of a disputed Consumer PAD, I must sign a declaration to the effect that either (a), (b) or (c) above took place and present it to my Financial Institution up to and including but not later than ninety (90) calendar days after the date on which the disputed Consumer PAD was posted to the Account. I acknowledge that, after this ninety (90) day period, I shall resolve any dispute regarding a Consumer PAD solely with the Payee, and that my Financial Institution shall have no liability to me respecting any such disputed Consumer PAD.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 8.\cell I certify that all information provided with respect to the Account is accurate and I agree to inform the Payee, in writing, of any change in the Account information provided in this Authorization at least ten (10) business days prior to the next due date of a Consumer Pad. In the event of any such change, this Authorization shall continue in respect of any new account to be used for Consumer PADs.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 9.\cell I warrant and guarantee that all persons whose signatures are required to sign on the Account have signed this Authorization below.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 10.\cell I understand and agree to the foregoing terms and conditions.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 11.\cell I agree to comply with the Rules of the Canadian Payments Association or any other rules or regulations which may affect the services described herein, as may be introduced in the future or are currently in effect and I agree to execute any further documentation which may be prescribed from time to time by the Canadian Payments Association in respect of the services described herein.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 12.\cell }
\pard\plain \s18\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\ul Applicable to the Province of Quebec only}{\f1\fs18 : It is the express wish of the parties that this Authorization and any related documents be drawn up and executed in English. Les parties conviennent que la pr\'e9sente autorisation et tous les documents s\rquote y ratachant soient r\'e9dig\'e9s et sign\'e9s en anglais.\par }
{\f1\par \par }
\pard \s18\qj \widctlpar\intbl\tqc\tx4770\tx8640\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
________________________________________________________________________
\par Name of Account Holder                   Signature                                              Date\par \par 
________________________________________________________________________
\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Name of Account Holder                   Signature                                              Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
</xsl:text>
	</xsl:template>
	<!--  We are using the build logic from the commitment letter template that already exists instead of replicating it here... -->
	<xsl:template name="EnglishCommitmentLetter">
		<xsl:text>{\sect\sectd} \pghsxn15840
\marglsxn432\margrsxn432\linex0\headery547\footery475\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
		<xsl:call-template name="EnglishHeader"/>
		<xsl:call-template name="EnglishPage1"/>
		<xsl:text>{\sect\sectd} \pghsxn15840
\marglsxn432\margrsxn432\linex0\headery547\footery475\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
		<xsl:call-template name="EnglishPage2"/>
		<xsl:text>{\sect\sectd} \pghsxn15840\marglsxn432\margrsxn432\linex0\sectdefaultcl </xsl:text>
		<xsl:call-template name="EnglishPage3"/>
	</xsl:template>
	<xsl:template name="EnglishFinalReport">
		<xsl:text>{\sect\sectd} \pghsxn20160\psz5
{\header \pard\plain \s20\ql \sl240\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\par }}	
\trowd \trgaph108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11520 \cellx11520
\pard\plain \s4\qc \sb240\sa240\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel3\adjustright 
{\b\f1\fs28\ul SOLICITOR\rquote S FINAL REPORT ON TITLE\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11520 \cellx11520
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }
\trowd \trgaph108
\trbrdrh\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth8460 \cellx8460
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx11520
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 Solicitor\rquote s Reference No.}{\f1  }{\b\f1 ____________________\cell }
{\b\f1 __________________\par \b0\f1 Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108
\trbrdrh\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth8460 \cellx8460
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx11520
\row 
}
\trowd \trgaph108\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth720 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10800 \cellx11520
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 TO:\cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> / Xceed Funding Corporation </xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
			</xsl:when>
		</xsl:choose>
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/>
		<xsl:text>{  }</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/>
		<xsl:text>
\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \par Attention: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth720 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10800 \cellx11520
\row 
}
\pard \qj \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }
\pard\plain \s20\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs20 In accordance with your instructions, we have acted as your solicitors in the following transaction. We have registered a Mortgage/Charge (or Deed of Loan, if applicable) the \'93Mortgage\'94
 on the appropriate form in the appropriate </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> and make our final report as follows:\par }
\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Mortgagor(s) ______________________________________________________________________________________
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> ___________________________________________________________________________________
\par }
\pard\plain \s2\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs20 Municipal Address of Property _________________________________________________________________________
\par Legal Address of Property ____________________________________________________________________________\par\par }

\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx10980
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 It is our opinion that:\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 

{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx10980
\row 
}

\trowd \trgaph57\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980

\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (1)\cell }

\pard \qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
A valid and legally binding </xsl:text>
		<!-- <xsl:value-of select="//SolicitorsPackage/LPDescription"/> -->
		<!-- <xsl:text> First </xsl:text>  By Billy 28Oct2003 -->
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderProfileId = '0'"> first </xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//SolicitorsPackage/LienPosition = 'FIRST'"> first </xsl:when>
					<xsl:otherwise> second </xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LienPosition = 'Second'"> first </xsl:when>	
			<xsl:otherwise> First </xsl:otherwise>
		</xsl:choose> -->
		<xsl:text> Mortgage in favour of </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> / Xceed Funding Corporation </xsl:text>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> / </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose>	-->
		<!-- <xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text>, for the full amount of the monies advanced was registered on _______________________________________ in the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> Division of __________________________________________________________as Instrument No. ______________________________.}
{\f1\ul \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}

\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (2)\cell }
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 The Mortgagor(s) have good and marketable first charge to the property free and clear of any prior encumbrances, other than the minor defe
cts listed below which }{\b\f1\fs20\ul do not}{\f1\fs20 
 affect the priority of the Mortgage or the marketability of the property. All lien holdback/ retention period requirements have been met. Easements, Encroachments and Restrictions etc. are listed below:\par } 
</xsl:text>
		<xsl:if test="//SolicitorsPackage/LienPosition = 'SECOND'">A first mortgage balance shall not exceed <xsl:value-of select="//SolicitorsPackage/equityMtgLiability"/>	with monthly payments of no more than </xsl:if>
		<xsl:text> 
\pard\plain \qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20
_________________________________________________________________________________________________________________________________________________________________________________________________________
_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
___________________________________________________________\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx10980
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Condominium/strata unit(s) if applicable:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmgf\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 2a)\cell }
\pard\plain \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 
We confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. We have reviewed the Condominium Corporation\rquote s
 Declaration and Bylaws and confirm they contain nothing derogatory to your security. We have assigned the voting rights to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>, if applicable.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmgf\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 \clvmrg
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 b)\cell 
}
\pard\plain \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 All necessary steps have been taken to confirm your right to vote should you wish to do so.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmrg\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\row 
}
\pard 
\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 c)\cell 
}
\pard\plain \s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
Applicable notice provisions, if any: ________________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmrg\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (3)\cell }
\pard\plain \s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
All restrictions have been complied with in full and there are no work orders or deficiency notices outstanding against the property.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (4)\cell }
\pard\plain \s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 All taxes and levies due and payable on the property to the Municipality have been paid up to _______________.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 (5)\cell }
\pard\plain 
\s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20
The Mortgage does not contravene the provisions of the Planning Act as amended from time to time, because the Mortgagor(s) do not retain the f
ee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (6)\cell }
\pard\plain \s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 A true copy of the Mortgage (including Standard Charge Terms, all Schedules 
to it, and the Mortgagor(s) Acknowledgement and Direction, if applicable) has been given to each Mortgagor.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmgf\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\pard \qj \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (7)\cell }
\pard\plain \s20\qj \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Fire Insurance coverage has been arranged in accordance with your instructions for full replacement cost(s) with loss payable to </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>, Xceed Funding Corporation </xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> and </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
			</xsl:when>
		</xsl:choose>
		<!-- <xsl:text> and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> in accordance with the I.B.C. standard mortgage clause.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmgf\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmrg\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\pard \qj \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Insurance Company:_____________________________\cell Broker: ___________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmrg\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\pard \qj \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amount of Insurance: ____________________________\cell Policy No: ________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\pard \qj \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Effective Date: __________________________________\cell 
Expiry Date: _______________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\row 
}
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs20 \par }
{\b\f1\fs20 The following documents are enclosed for your file:}
{\f1\fs20 \par \par }
\pard\plain\s19\qj \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0
{\f1\fs20 [ X ] \tab Solicitor\rquote s Final Report on Title
\par [ X ] \tab Duplicate registered copy of the Charge/Mortgage, or Deed of Loan including Standard Charge Terms, all Schedules to it and acknowledgement of receipt of Mortgagor(s) and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>(s), if any
\par [ X ]\tab Electronic Charge and Acknowledgement and Direction (Ontario electronic registration counties only)
\par [    ]\tab Instrument Number (Teranet Registration)
\par [    ]\tab Guarantee Agreement ( Ontario electronic registration counties only )
\par [    ]\tab Certificate of Title (or Provincial Comparable)
</xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/PropertyAddress/ProvinceId = 11"/>
			<xsl:otherwise>\par [ X ]\tab Survey or Surveyor\rquote s Certificate or Title Insurance in lieu of a Survey, if applicable</xsl:otherwise>
		</xsl:choose>
		<xsl:text>
\par [ X ]\tab Title Insurance Policy and Schedules A &amp; B to Policy
\par [    ]\tab Registered Amendment Agreement (if applicable)
\par [ X ]\tab Execution Certificate
\par [ X ]\tab Municipal Tax Certificate
\par [    ]\tab Certificate of Completion and Possession
\par [    ]\tab New Home Warranty Certificate of Possession
\par [    ]\tab Occupancy Certificate/Permit
\par [    ]\tab Personal Guarantee and Letter of Independent Legal Advice
\par [ X ]\tab Sheriff\rquote s Certificate/GR Search
\par [ X ]\tab Signed Statutory Declaration
\par [ X ]\tab Fire Insurance Policy
\par [    ]\tab Condominium Corporation Insurance Binder
\par [    ]\tab Zoning Certificate/Memorandum (Ontario properties only)
\par [    ]\tab Declaration as to Possession (Manitoba properties only)
\par [    ]\tab Registered Assignment of Rents
\par [    ]\tab General Security Agreement (GSA) Registered under PPSA
\par [    ]\tab Statement of Funds Received and Disbursed
\par [    ]\tab Verification of Payout of Debts
\par [    ]\tab Other (specify) ________________________________________________________________________________
\par [    ]\tab _____________________________________________________________________________________________
\par [    ]\tab _____________________________________________________________________________________________
\par [    ]\tab _____________________________________________________________________________________________
\par [    ]\tab _____________________________________________________________________________________________
\par \par }
\pard\plain \s20\qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\par _____________________________________________________\par }
\pard\plain \qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\cf1 Signature of Solicitor\par }</xsl:text>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- French template section                                             			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="FrenchSolicitorInstructions">
		<xsl:text>\paperh20160 \trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
		<xsl:call-template name="XceedLogo"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CurrentDate"/>
		<xsl:text>
\par 
\par
\par}
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth7938 \cellx7830
\cltxlrtb\clftsWidth3\clwWidth2818 \cellx10648
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \fs20
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/>
		<xsl:text>
\par }
{\f1\fs20 
</xsl:text>
		<xsl:if test="//SolicitorsPackage/Solicitor/Phone">
			<xsl:text>Téléphone: </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/Solicitor/Phone"/>
			<xsl:text>\tab </xsl:text>
		</xsl:if>
		<xsl:if test="//SolicitorsPackage/Solicitor/Fax">
			<xsl:text>Télécopieur: </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/Solicitor/Fax"/>
		</xsl:if>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Nombre de pages ______\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth7938 \cellx7830
\cltxlrtb\clftsWidth3\clwWidth2818 \cellx10648
\row 
}
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 
\par 
Maître  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>,
\par 
\par }

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 RE:\cell OBJET:  EMPRUNTEUR(S):\cell </xsl:text>
		<!--#DG312 xsl:for-each select="//SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trbrdrh
\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, s’il y a lieu:\cell 
</xsl:text>
    <!--#DG460 oooops, should have been done since #DG312 ...
		xsl:for-each select="//SolicitorsPackage/GuarantorNames">
			<xsl:value-of select="./Name"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="borType" select="1"/>
    </xsl:call-template>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell ADRESSE DE LA PROPRIÉTÉ HYPOTHÉQUÉE:\cell 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}
{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/PostalCode"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell DATE DE CLÔTURE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell MONTANT DU PRÊT APPROUVÉ:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell NUMÉRO DE RÉFÉRENCE DU PRÊTEUR:\cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx3960
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth6686 \cellx10646
\row 
}
\pard \qj \widctlpar\tx540\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 {\pict{\*\picprop\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn alignHR}{\sv 1}}{\sp{\sn dxHeightHR}{\sv 30}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fStandardHR}{\sv 1}}{\sp{\sn fHorizRule}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}}\picscalex1053\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag908016038\blipupi1439{\*\blipuid 361f39a6bcdb98f4598df072e3f44282}
010009000003310000000000070000000000050000000b0200000000050000000c02f401e803070000001b04f401e8030000000005000000140200000000050000001302f401e803050000001402f40100000500000013020000e80303000000000000}}

{\f1\fs18 \par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Un prêt hypothécaire a été approuvé au nom du ou des emprunteurs susmentionnés, conformément aux conditions énoncées dans la Lettre d’engagement ci-jointe. On nous a avisé que vous agirez pour le compte de </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> et </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text> (ci-après dénommé le Prêteur) dans la préparation, l’exécution et l’enregistrement de l’hypothèque sur la propriété, en vous assurant que les intérêts du Prêteur – le créancier hypothécaire – sont valides et convenablement protégés. Veuillez prendre note que le Prêteur n’exigera pas et n’approuvera pas, de rapport intérimaire sur les titres ou les ébauches de documents, y compris l’hypothèque. Le Prêteur fait appel à vous uniquement pour vous assurer que le prêt hypothécaire est établi en conformité avec les instructions mentionnées aux présentes, pour confirmer l’identité du ou des emprunteurs et </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, selon le cas, et pour conserver en dossier la preuve ayant servi à confirmer leur identité. L’hypothèque peut être enregistrée par voie électronique, s’il y a lieu.\par \par }

\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Tout fait d’importance risquant de modifier la position du Prêteur en tant que dépositaire de la première sûreté sur la propriété doit être portée à son attention avant l’avancement des fonds. De même, toute question susceptible de modifier la garanti du Prêteur ou toute modification ultérieure apportée à la Convention d’achat-vente doit être soumise à nos bureaux, qui communiqueront leurs instructions, avant le dégagement des fonds. Il faudra notre consentement préalable pour toute demande de financement supplémentaire ou de financement par prêt hypothécaire de second rang. Si pour un motif quelconque vous ne pouvez enregistrer notre hypothèque à la date de clôture prévue, il faudra nous en aviser au moins 24 heures avant cette date et nous signifier le motif du retard. Nulle avance ne pourra être consentie après la date de clôture prévue sans l’autorisation écrite du Prêteur.\par \par}

\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Vous devez soumettre la Demande de fonds du notaire avec tous les documents requis à notre Centre de services financiers }{\f1\fs20\ul au moins trois (3) jours ouvrables avant la date de clôture}{\f1\fs20 . La Demande de fonds du notaire doit contenir confirmation que toutes nos dispositions ont été satisfaites ou seront satisfaites et doit faire mention spécifique de toute qualification qui apparaîtra sur votre Rapport final et le certificat de titres.
\par 
\par Les fonds hypothécaires seront envoyés par transfert de fonds électronique et seront déboursés sans délai, à condition que toutes les dispositions aient été satisfaites et que les précautions nécessaires aient été apportées pour que la garantie de l’hypothèque demeure prioritaire.  
\par 
\par Les frais juridiques et tous les autres frais et débours afférents à cette transaction sont à la charge du ou des emprunteurs, que le produit du prêt hypothécaire ait ou non été avancé.
\par }
\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
{\f1\fs20 En relation avec cette transaction, nous joignons les documents suivants:
\par 
\par 
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Lettre d’engagement}{\f1\b0  - }
{\f1\b0 Veuillez vous assurer que le toute condition pendante énoncée dans la Lettre d’engagement, qui relève de la responsabilité du notaire, soit dûment notée, que la conformité pleine et entière soit confirmée avant l’avancement des fonds et qu’il en soit fait état dans votre rapport final.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Instructions et Directives destinées au notaire}{\f1\b0  - }{\f1\b0 Explicite\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Prêt hypothécaire}{\f1\b0  - </xsl:text>
		<xsl:for-each select="//SolicitorsPackage/ProvincialClause/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Annexes}{\f1\b0 - Annexe A\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\f1 \trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 {\field{\*\fldinst SYMBOL 216 \\f "Wingdings" \\s 10}{\fldrslt\f14\fs20}}}{\f1 \cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\i\f1\ul Déclaration de divulgation}{\f1\fs20   (s’il y a lieu)\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft180\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth375 \cellx555
\cltxlrtb\clftsWidth3\clwWidth10353 \cellx10908
\row 
}
}
{\f1\fs20 \par }
{\f1\fs20 Autres garanties ou conditions particulières requises:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SolicitorSpecialInstructions"/>
		<xsl:text>
\par }
{\f1\fs20\par }
\pard\plain \s22\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Nous consentons que vous agissiez pour le compte du Prêteur et du ou des emprunteurs et/ou </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> à condition que vous en avisiez le ou les emprunteur(s) et/ou </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> que vous obteniez leur consentement par écrit et que vous divulguiez à chacune des parties toute l’information en votre possession relative à la présente transaction. 
\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\pard\plain \s2\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\b\f1\fs20\ul CENTRE DE SERVICES FINANCIERS\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\f1\fs20 Les documents et demandes de fonds doivent être envoyés à:
\par 
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>
\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/>
		<xsl:text>\par
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/>
		<xsl:text>
\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\f1\fs20 
\par Contact: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>
\par 
\par Téléphone:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Phone"/>
		<xsl:text> Ext. </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderExtension"/>
		<xsl:text>
\par Télécopieur:\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>
\par }
\pard \ql \fi-360\li360\widctlpar\aspalpha\aspnum\faauto\ilvl12\adjustright\lin360\itap0 
{\f1\fs20 \par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Pour tout autre renseignement, n’hésitez pas à nous contacter.
\par 
\par Cordialement,\tab \tab 
\par 
\par 
\par 
\par 
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>
\par Administrateur des services financiers -  Préposé à l’avance de fonds pour l’hypothèque}
</xsl:text>
	</xsl:template>
	<xsl:template name="FrenchGuide">
		<xsl:text>{\sectd\sect} \pghsxn20160\psz5
\pard\plain \s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\b\ul\f1\fs28 GUIDE À L’INTENTION DU NOTAIRE}
{\f1\fs18\par\par\par }
\pard\plain \s16\qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs20 Si vous avez la conviction que le ou les emprunteurs hypothécaires (également appelé(s) le ou les « emprunteurs ») ont acquis ou acquerront un titre valable et marchand sur la propriété décrite dans notre Lettre d’engagement, veuillez remplir nos conditions stipulées ci-dessous et préparer une hypothèque tel qu’énoncé aux présents.\par }
\pard\plain \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
{\b\f1\fs20\ul Documents:}{\f1\fs20  Le numéro de formule de l’hypothèque indiqué dans notre Lettre d’accompagnement ainsi que la ou les annexes y afférentes doivent être enregistrées. }
{\f1\fs20 \par \par }
{\b\f1\fs20\ul Exigences hypothécaires:}{\f1\fs20  Le document hypothécaire doit être enregistré comme suit:\par \par }

{\b0\fs20
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Le titre doit être enregistré au nom de:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SoleJointTenants"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Montant du prêt approuvé:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmountPreFee"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Frais de demande:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdminFee"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Montant total du prêt:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Taux d’intérêt:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Remboursement du capital &amp; Intérêt:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PandIPayment"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Paiement de taxes:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TaxPortion"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Date d’ajustement des intérêts:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/IADDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Montant de l’ajustement des intérêts:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/IADAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Date du premier remboursement:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Date d’échéance/Renouvellement:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Intérêt calculé:\cell }
{\f1 Semestriel, pas en avance\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
}

\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Recherche de titres:}
{\f1\fs20  Vous devez faire les recherches nécessaires sur les titres de propriété afin de protéger nos intérêts. </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Execution"/>
		<xsl:text> Il vous incombe de vérifier qu’il n’y a pas d’ordres de travail ni d’avis de défaut en souffrance contre la propriété et que les taxes et les droits immobiliers dus et exigibles jusqu’à la date de rajustement de l’intérêt ont tous été payés à la municipalité. Si vos recherches révèlent que le titre a été transféré entre la date de l’offre d’achat et la date de conclusion, vous devez nous aviser sans délai et interrompre toutes les démarches concernant cette transaction à moins d’avis contraire.  
Avant l’enregistrement de l’hypothèque, vous devez obtenir un certificat du shérif /une recherche de registre général indiquant qu’il n’y a pas de brefs d’exécution contre le ou les emprunteur(s), garant(s), au cas où il y en aurait, ceci affecterait notre sécurité.
\par }
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\pard \qj \widctlpar\tx1800\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Titre d’unité de condominium:}{\f1\fs20  Vous devez confirmer que le syndicat de co-propriété est enregistré et possède une couverture assurance- incendie adéquate. Vous devez examiner la déclaration et les arrêtés du syndicat de co-propriété et confirmer qu’ils ne contiennent aucune clause dérogatoire à notre sécurité. Vous devez affecter les droits de vote au </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> et à </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text> (s’il y a lieu).
\par }\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Localisation:}{\f1\fs20  Si un certificat de localisation /RPR (y compris un certificat de conformité), qui ne doit pas dater de plus de 30 an, n’est pas disponible, nous accepterons une assurance titres émise par First Canadian Title, Stewart Title, Title Plus ou Land Canada. Vous devez obtenir et suivre les instructions de Title Insurance Company à cet égard. Le ou les emprunteur(s) sont responsables du coût de la prime d’assurance titres.
\par }\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \qj \widctlpar\tx1440\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs20\ul Assurance incendie:}{\f1\fs20  Avant que nous n’avancions les fonds, vous devez vérifier que la couverture d’assurance incendie du ou des bâtiments situés sur la propriété est en vigueur pour un montant non inférieur à leur pleine valeur de remplacement, avec pertes payables à </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> et </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text> à titre de prêteur hypothécaire à l’adresse indiquée sur le prêt hypothécaire, et qu’elle comporte une clause relative aux garanties hypothécaires dûment remplies et approuvée par le Bureau d’assurance du Canada. Les polices ne doivent pas contenir de coassurance ni de clause similaire susceptible de limiter le montant payable.
\par }
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
{\b\f1\fs20\ul Loi sur les biens matrimoniaux:}
{\f1\fs20  Vous devez certifier que toutes les exigences de la Loi sur les biens matrimoniaux de votre province, s’il y a lieu, ont été satisfaites et que le statut  n’affecte en aucune façon que ce soit notre hypothèque.\par \par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Avances:}{\f1\fs20  Lorsque toutes les conditions relatives à cette transaction auront été remplies, un montant de </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text> sera avancé et les coûts suivants seront déductibles de l’avance hypothécaire;\par }
\pard \qj \fi720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 
{\f1\fs20 \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Frais de demande\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdminFee"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant total de l’ajustement des intérêts\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/IADAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}</xsl:text>
		<xsl:for-each select="//SolicitorsPackage/Fees/Fee">
			<xsl:text>\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 TOTAL DES DÉDUCTIONS\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TotalDeductionsAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Ajustement des intérêts (par jour)\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PerDiem"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\row 
}
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par Le produit net du prêt hypothécaire, payable à vous en fiducie sera envoyé à votre bureau et déposé dans votre compte fiduciaire à la date de clôture prévue, qui est prévue pour être </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>. 
\par \par }</xsl:text>
		<xsl:if test="//SolicitorsPackage/Refi">
			<xsl:text>\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Refinancement:}
{\f1\fs20  Si le présent prêt hypothécaire vise à refinancer une dette existante, le solde du produit hypothécaire vous sera remis en fiducie pour être déboursé tel qu’énoncé ci-dessous. Vous devez confirmer le paiement dans la Déclaration de fonds reçus et déboursés et/ou une lettre d’accompagnement.\par \par }
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \sa120\keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Hypothèque </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LPDescription"/>
			<xsl:text> existante\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Autres dettes à payer:\cell Montant:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
		</xsl:if>
		<xsl:text>
{\b\f1\fs20\ul Avances provisoires:}{\b\f1\fs20  }{\f1\fs20 Si la garantie offerte en contrepartie de ce prêt hypothécaire est un nouveau bien et que nous avancions de l’argent sous forme d’avances provisoires, les avances seront faites sur une base de coût à l’achèvement. Vous êtes responsable du déboursement de toutes les avances faites à votre ordre en fiducie, garantir le rang de chaque avance en procédant à toutes les recherches nécessaires sur les titres et vérifier que les dispositions de la Loi provinciale sur le privilège dans l’industrie de la construction ont été respectées avant le déboursement de chacune des avances. Vous devez retenir les sommes appropriées du produit du prêt hypothécaire en conformité avec les prescriptions de la province relatives à la retenue pour privilège. À moins d’indication contraire, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text> auront besoin d’une copie du Certificat de fin des travaux et de prise de possession dûment signé et/ou du Certificat d’occupation.
\par
\par }{\b\f1\fs20\ul Divulgation:}{\f1\fs20  Le ou les emprunteurs et le </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> (s’il y a lieu) doivent signer une Déclaration de divulgation, dont une copie sera remise au(x) emprunteur(s) et une seconde copie envoyée à </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>. Veuillez vous assurer que la législation provinciale et les délais d’exécution applicables relatifs aux Déclarations de divulgation ont été appliqué. À défaut d’obtenir une déclaration de divulgation, vous devrez obtenir une renonciation du ou des emprunteurs et du </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> (s’il y a lieu). 
\par 
\par }\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs20\ul\cf1 Loi sur l’aménagement du territoire:}{\b\f1\fs20\cf1  }{\f1\fs20 Vous devez vérifier que l’hypothèque est conforme aux dispositions de la Loi sur l’aménagement du territoire, modifiée au gré du Législateur, que le ou les emprunteurs ne conservent pas le fief ni la valeur nette de rachat de tout terrain adjacent à la propriété donnée en garantie du prêt hypothécaire, et qu’ils n’ont ni le droit ni le pouvoir d’accorder ou de céder ce terrain, ni celui d’exercer un pouvoir de nomination relativement à ce terrain.
\par }\pard\plain \s2\qj \keepn\widctlpar\tx720\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs20 \par }\pard\plain \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul\cf1 Renouvellement pour le ou les garant(s):}{\f1\fs20  S’il y a lieu, on inclura le paragraphe suivant dans une Annexe du prêt hypothécaire: 
\par 
\par « En plus des promesses du ou des </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> contenues dans le présent prêt hypothécaire, le ou les </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> conviennent que le prêt hypothécaire peut être renouvelé à l’échéance pour n’importe quel terme, à la discrétion du </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>, avec ou sans modification du taux d’intérêt payable en vertu du prêt, sous réserve de l’exécution d’une ou plusieurs ententes avec le ou les emprunteurs. »\par
\par }
{\b\f1\fs20\ul\cf1 L’emprunteur est une entreprise:}
{\f1\fs20\cf1  Si le ou les emprunteurs ou </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> sont une entreprise, vous devez obtenir une copie certifiée conforme de la résolution du conseil d’administration et du règlement d’emprunt de l’entreprise autorisant le prêt hypothécaire et vous assurer que l’entreprise est dûment constituée en société en vertu des lois provinciales ou fédérales pertinentes et qu’elle a le pouvoir de céder en garantie son intérêt dans la propriété. Vous devez annexer ce ou ces documents à votre Rapport final sur les titres.
\par }</xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/PrimaryPropertyProvince = 'AB'">
				<xsl:call-template name="FrenchScheduleA2"/>
			</xsl:when>
			<xsl:when test="//SolicitorsPackage/PrimaryPropertyProvince = 'NS'">
				<xsl:call-template name="FrenchScheduleA3"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="FrenchScheduleA1"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--  Primary property province != AB or NS -->
	<xsl:template name="FrenchScheduleA1">
		<xsl:text>{\f1\fs20 \sect }\sectd \pghsxn20160\psz5 \linex0\endnhere\sectlinegrid360\sectdefaultcl 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3576 \cellx3468
\cltxlrtb\clftsWidth3\clwWidth3576 \cellx7044
\cltxlrtb\clftsWidth3\clwWidth3576 \cellx10620
\pard\plain \qj \sl240\slmult0\widctlpar\intbl
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs20 \page }{\f1\fs22 \cell }\pard\plain \s3\qc \sl240\slmult0\keepn\widctlpar\intbl
\tx-5016\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel2\adjustright
 \b\f1\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {ANNEXE « A »\cell }\pard\plain \qr \sl240\slmult0\widctlpar\intbl
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs22 (ON, MB, SK, BC, NB, NF, PEI)}{\f1\fs22 \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs22 \trowd 
\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth3576 \cellx3468\cltxlrtb\clftsWidth3\clwWidth3576 \cellx7044\cltxlrtb\clftsWidth3\clwWidth3576 \cellx10620
\row }\pard \qj \sl240\slmult0\widctlpar
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0{\f1\fs22 

\par }\pard \ql \sl240\slmult0\widctlpar
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel0\adjustright
\itap0 </xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LienPosition = 'SECOND'">
				<xsl:text>{\b\f1\fs20\ul Veuillez insérer les clauses n{\super o} 1 - 5 dans chaque hypothèque et s’il y lieu, veuillez aussi insérer les clauses n{\super o} 6 et n{\super o} 7\par }</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\b\f1\fs20\ul Veuillez insérer les clauses n{\super o} 1 - 5 dans chaque hypothèque et s’il y a lieu, veuillez aussi insérer la clause n{\super o} 6.\par }</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>
{\f1\fs22 \par }
\pard \ql \sl240\slmult0\widctlpar
\tx-1440\tx-1260\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0{\f1\fs22 
_(1)_ }{\b\f1\fs22 VENTE DE L’HYPOTHÈQUE /CHANGEMENT DE FOURNISSEUR DE PRÊT: }{\f1\fs22 
L’hypothèque ou un intérêt partiel de l’hypothèque peut être vendue et/ou affectée une ou plusieurs fois sans préavis à l’emprunteur. Une vente peut résulter en un changement dans 
l’entité (connu aussi sous le nom de « Agent de prêt ») qui perçoit les remboursements mensuels dus pour l’hypothèque. Il peut aussi y avoir un ou des changements relatifs à 
l’Agent de prêt, mais non relatifs à une vente de la charge hypothécaire. L’emprunteur n’aura pas à prendre contact avec le nouvel agent du prêt à moins qu’un avis de changement écrit lui ait été remis.
\par 
\par _(2)_ }{\b\f1\fs22 APPLICATION DES PAIEMENTS: }{\f1\fs22 Si la charge hypothécaire est remboursable en versements mixtes du capital et de l’intérêt, les versements payables au sens de la charge 
doivent d’abord être appliqués à l’intérêt calculé sur les sommes du capital parfois impayées comme indiqué dans la charge et le solde des versements indiqués doit être appliqué au titre 
de capital; après défaut de paiement par l’emprunteur, le titulaire de la charge hypothécaire peut appliquer comme il le souhaite tout paiement reçu durant la période du défaut pour le 
capital, les taxes, l’intérêt, les réparations, les primes d’assurance ou toute autre avance faite pour le compte de l’emprunteur.
\par 
\par _(3)_ }{\b\f1\fs22 PRIVILÈGES DE REMBOURSEMENT ANTICIPÉ: }{\f1\fs22 Sous réserve que l’hypothèque ne soit pas défaut, le ou les emprunteur(s) peuvent rembourser jusqu’à 20 % du 
montant original du capital de l’hypothèque, sans préavis, bonus ou pénalité chaque année civile. Chaque remboursement sera fait à une date de remboursement régulière et ne sera 
pas inférieure à100.00 $. De plus, sous réserve que I’hypothèque n’est pas en défaut, le ou les emprunteurs ont le privilège d’augmenter leur montant de remboursement régulier jusqu’à un maximum de 
20 % du remboursement du capital et intérêt actuel {\b une fois} par année civile. Ces privilèges ne sont pas cumulatifs d’année en année.
\par 
\par _(4)_ }{\b\f1\fs22 CLAUSE C.I.T: }{\f1\fs22 (cette clause s’applique à tous les produits)
\par Une portion mensuelle des taxes de </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TaxPortion"/>
		<xsl:text> (estimation) sera ajoutée au paiement de votre capital et intérêt.
\par }\pard \ql \sl240\slmult0\widctlpar
\tx-1440\tx-1260\tx576\tx1296\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\tx27360\aspalpha\aspnum\faauto\adjustright
\itap0 {\f1\fs22 
\par _(5)_ }{\b\f1\fs22 FRAIS: }{\f1\fs22 Les frais suivants sont en vigueur au moment de cette hypothèque:
\par }\pard \ql \sl240\slmult0\widctlpar\tx-1440\tx-1260\tx540\tqdec\tx4752\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 \tab Frais de chèque sans provision:\tab 
$50.00\tab \tab Frais d’arrêt de paiement:\tab $25.00
\par \tab Frais de renouvellement:\tab $100.00\tab \tab Transfert/Changement:\tab $250.00
\par \tab Frais de prise en charge:\tab $250.00\tab \tab Bloquer paiement:\tab $25.00
\par \tab Frais de décharge:\tab $125.00
\par 
\par _(6)_ }{\b\f1\fs22 UNITÉS DE CONDOMINIUM: }{\f1\fs22 Si l’emprunteur vend, transfère, affecte ou cède tout unité de parking  relative à cette hypothèque tout en gardant le titre (ou la possession) de l’unité d’habitation liée à cette hypothèque ou si l’emprunteur vend, transfère, affecte ou cède tout unité de parking ainsi que l’unité d’habitation mais à des acheteurs, bénéficiaires du transfert ou de la cession différents, alors  dans les deux cas, le total du capital impayé et de l’endettement de l’intérêt sécurisé par cette hypothèque, selon l’option du titulaire de la charge hypothécaire, devient immédiatement due et payable.
\par } </xsl:text>
		<xsl:if test="//SolicitorsPackage/LienPosition = 'SECOND'">
			<xsl:text>{\par _(7)_ }{\b\f1\fs22 SECONDE HYPOTHÈQUE: }{\f1\fs22 L’emprunteur reconnaît et est d’avis qu’en cas de défaut de paiement de la première hypothèque existante, un tel défaut constitue un défaut pour cette hypothèque et qu’il donne le droit au titulaire de la charge hypothécaire d’exercer les recours ci-dessous.\par }</xsl:text>
		</xsl:if>
	</xsl:template>

	<!--  Primary property province = AB -->
	<xsl:template name="FrenchScheduleA2">
		<xsl:text>{\f1\fs20\sect }\sectd \pghsxn20160\psz5 \linex0\endnhere\sectlinegrid360\sectdefaultcl \pard\plain \ql \sl240\slmult0\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs22 \cell }\pard\plain \s3\qc \sl240\slmult0\keepn\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
\b\f1\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {ANNEXE « A »\cell }\pard\plain \qr \sl240\slmult0\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs22 (UNIQUEMENT POUR L’ALBERTA)}{\f1\fs22 \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs22 \trowd 
\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth3576 \cellx3468\cltxlrtb\clftsWidth3\clwWidth3576 \cellx7044\cltxlrtb\clftsWidth3\clwWidth3576 \cellx10620
\row }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22  
\par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0</xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LienPosition = 'SECOND'">
				<xsl:text>{\b\f1\fs20\ul Veuillez insérer les clauses n{\super o} 1 - 5 dans chaque hypothèque et s’il y lieu, veuillez aussi insérer les clauses n{\super o} 6 et n{\super o} 7.\par }</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\b\f1\fs20\ul Veuillez insérer les clauses n{\super o} 1 - 5 dans chaque hypothèque et s’il y a lieu, veuillez aussi insérer la clause n{\super o} 6.\par }</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>{\f1\fs22 \par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 _(1)_ }{
\b\f1\fs22 VENTE DE L’HYPOTHÈQUE /CHANGEMENT DE FOURNISSEUR DE PRÊT: }{\f1\fs22 L’hypothèque ou un intérêt partiel de l’hypothèque peut être vendue et/ou affectée une ou plusieurs fois sans préavis à l’emprunteur. Une vente peut résulter en un changement dans l’entité (connu aussi sous le nom de « Agent de prêt ») qui perçoit les remboursements mensuels dus pour l’hypothèque. Il peut aussi y avoir un ou des changements relatifs à l’Agent de prêt, mais non relatifs à une vente de la charge hypothécaire. L’emprunteur n’aura pas à prendre contact avec le nouvel agent du prêt à moins qu’un avis de changement écrit lui ait été remis.
\par 
\par _(2)_ }{\b\f1\fs22 APPLICATION DES PAIEMENTS: }{\f1\fs22 Si la charge hypothécaire est remboursable en versements mixtes du capital et de l’intérêt, les versements payables au sens de la charge doivent d’abord être appliqués à l’intérêt calculé sur les sommes du capital parfois impayées comme indiqué dans la charge et le solde des versements indiqués doit être appliqué au titre de capital; après défaut de paiement par l’emprunteur, le titulaire de la charge hypothécaire peut appliquer comme il le souhaite tout paiement reçu durant la période du défaut pour le capital, les taxes, l’intérêt, les réparations, les primes d’assurance ou toute autre avance faite pour le compte de l’emprunteur.
\par 
\par _(3)_ }{\b\f1\fs22 PRIVILÈGES DE REMBOURSEMENT ANTICIPÉ: }{\f1\fs22 
Sous réserve que l’hypothèque ne soit pas défaut, le ou les emprunteur(s) peuvent rembourser jusqu’à 20 % du montant original du capital de l’hypothèque, sans préavis, bonus ou pénalité chaque année civile. Chaque remboursement sera fait à une date de remboursement régulière et ne sera pas inférieure à100.00 $. De plus, sous réserve que I’hypothèque n’est pas en défaut, le ou les emprunteurs ont le privilège d’augmenter leur montant de remboursement régulier jusqu’à un maximum de 20 % du remboursement du capital et intérêt actuel {\b une fois} par année civile. Ces privilèges ne sont pas cumulatifs d’année en année.
\par 
\par _(4)_ }{\b\f1\fs22 CLAUSE C.I.T: }{\f1\fs22 (cette clause s’applique à tous les produits)
\par Une portion mensuelle des taxes de </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TaxPortion"/>
		<xsl:text> (estimation) sera ajoutée au paiement de votre capital et intérêt.
\par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\tx27360\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 
\par _(5)_ }{\b\f1\fs22 FRAIS: }{\f1\fs22 Les frais suivants sont en vigueur au moment de cette hypothèque:
\par }
\pard \ql \sl240\slmult0\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 \tab Frais de chèque sans provision:\tab $50.00
\par \tab Frais de transfert/Changement:\tab $250.00
\par 
\par _(6)_ }{\b\f1\fs22 UNITÉS DE CONDOMINIUM: }{\f1\fs22 Si l’emprunteur vend, transfère, affecte ou cède tout unité de parking  relative à cette hypothèque tout en gardant le titre (ou la possession) de l’unité d’habitation liée à cette hypothèque ou si l’emprunteur vend, transfère, affecte ou cède tout unité de parking ainsi que l’unité d’habitation mais à des acheteurs, bénéficiaires du transfert ou de la cession différents, alors  dans les deux cas, le total du capital impayé et de l’endettement de l’intérêt sécurisé par cette hypothèque, selon l’option du titulaire de la charge hypothécaire, devient immédiatement due et payable.
\par }</xsl:text>
		<xsl:if test="//SolicitorsPackage/LienPosition = 'SECOND'">
			<xsl:text>{\par _(7)_ }{\b\f1\fs22 SECONDE HYPOTHÈQUE: }{\f1\fs22 L’emprunteur reconnaît et est d’avis qu’en cas de défaut de paiement de la première hypothèque existante, un tel défaut constitue un défaut pour cette hypothèque et qu’il donne le droit au titulaire de la charge hypothécaire d’exercer les recours ci-dessous.\par }</xsl:text>
		</xsl:if>
	</xsl:template>
	<!--  Primary property province = NS -->
	<xsl:template name="FrenchScheduleA3">
		<xsl:text>{\sect }\sectd \pghsxn20160\psz5 \linex0\endnhere\sectlinegrid360\sectdefaultcl \pard\plain \ql \sl240\slmult0\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs22 \cell }\pard\plain \s3\qc \sl240\slmult0\keepn\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
\b\f1\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {ANNEXE « A »\cell }\pard\plain \qr \sl240\slmult0\widctlpar\intbl
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs22 (NOVA SCOTIA)}{\f1\fs22 \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs22 \trowd 
\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \cltxlrtb\clftsWidth3\clwWidth3576 \cellx3468\cltxlrtb\clftsWidth3\clwWidth3576 \cellx7044\cltxlrtb\clftsWidth3\clwWidth3576 \cellx10620
\row }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\fs22 \par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0</xsl:text>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LienPosition = 'SECOND'">
				<xsl:text>{\b\f1\fs20\ul Veuillez insérer les clauses n{\super o} 1 - 5 dans chaque hypothèque et s’il y lieu, veuillez aussi insérer les clauses n{\super o} 6 et n{\super o} 7.\par }</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\b\f1\fs20\ul Veuillez insérer les clauses n{\super o} 1 - 5 dans chaque hypothèque et s’il y a lieu, veuillez aussi insérer la clause n{\super o} 6.\par }</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>{\f1\fs22 \par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 _(1)_ }{
\b\f1\fs22 VENTE DE L’HYPOTHÈQUE /CHANGEMENT DE FOURNISSEUR DE PRÊT: }{\f1\fs22 L’hypothèque ou un intérêt partiel de l’hypothèque peut être vendu(e) et/ou affecté(e) une ou plusieurs fois sans qu’un préavis ne doit donné à l’emprunteur. Une vente peut résulter en un changement dans l’entité (connu aussi sous le nom de « Agent de prêt ») qui perçoit les remboursements mensuels dus pour l’hypothèque. Il peut aussi y avoir un ou des changements relatifs à l’Agent de prêt, mais non relatifs à une vente de la charge hypothécaire. L’emprunteur n’aura pas à prendre contact avec le nouvel agent du prêt à moins qu’un avis de changement écrit lui ait été remis.
\par 
\par _(2)_ }{\b\f1\fs22 APPLICATION DES PAIEMENTS: }{\f1\fs22 Si la charge hypothécaire est remboursable en versements mixtes du capital et de l’intérêt, les versements payables au sens de la charge doivent d’abord être appliqués à l’intérêt calculé sur les sommes du capital parfois impayées comme indiqué dans la charge et le solde des versements indiqués doit être appliqué au titre de capital; après défaut de paiement par l’emprunteur, le titulaire de la charge hypothécaire peut appliquer comme il le souhaite tout paiement reçu durant la période du défaut pour le capital, les taxes, l’intérêt, les réparations, les primes d’assurance ou toute autre avance faite pour le compte de l’emprunteur.
\par 
\par _(3)_ }{\b\f1\fs22 PRIVILÈGES DE REMBOURSEMENT ANTICIPÉ: }{\f1\fs22 Sous réserve que l’hypothèque ne soit pas défaut, le ou les emprunteur(s) peuvent rembourser jusqu’à 20 % du montant original du capital de l’hypothèque, sans préavis, bonus ou pénalité chaque année civile. Chaque remboursement sera fait à une date de remboursement régulière et ne sera pas inférieure à100.00 $. De plus, sous réserve que I’hypothèque n’est pas en défaut, le ou les emprunteurs ont le privilège d’augmenter leur montant de remboursement régulier jusqu’à un maximum de 20 % du remboursement du capital et intérêt actuel {\b une fois} par année civile. Ces privilèges ne sont pas cumulatifs d’année en année.
\par 
\par _(4)_ }{\b\f1\fs22 CLAUSE C.I.T: }{\f1\fs22 (cette clause s’applique à tous les produits)
\par Une portion mensuelle des taxes de </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TaxPortion"/>
		<xsl:text> (estimation) sera ajoutée au paiement de votre capital et intérêt.
\par }\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\tx27360\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs22 
\par _(5)_ }{\b\f1\fs22 FRAIS: }{\f1\fs22 Les frais suivants sont en vigueur au moment de cette hypothèque:
\par }

\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\pard \ql \sl240\slmult0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs22 Frais de chèque sans provision:\tab \tab $50.00\cell Frais d’arrêt de paiement:\tab $25.00\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\row 
}
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\pard \ql \sl240\slmult0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs22 Frais de renouvellement:\tab \tab $100.00\cell Transfert/Changement:\tab $25.00\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\row 
}
\pard \ql \sl240\slmult0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs22 Frais de prise en charge:\tab $250.00\cell Bloquer paiement:\tab $25.00\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\row 
}
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\pard \ql \sl240\slmult0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs22 Frais de décharge:\tab $125.00\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft540\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx4860
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx8460
\row 
}
\pard \ql \sl240\slmult0\widctlpar
\tx11520\tx12240\tx12960\tx13680\tx14400\tx15120\tx15840\tx16560\tx17280\tx18000\tx18720\tx19440\tx20160\tx20880\tx21600\tx22320\tx23040\tx23760\tx24480\tx25200\tx25920\tx26640\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs22 \par _(6)_ }{
\b\f1\fs22 CLAUSES LIÉES AU CONDOMINIUM: }{\f1\fs22 Si l’hypothèque immobilière concerne une unité de condominium ou comprend une unité de condominium, les clauses suivantes s’appliquent:
\par\par 
a) l’hypothèque doit observer et se conformer à chaque clause restrictive et disposition qui doit être observer et conforme en vertu de l’hypothèque, de la Loi sur les condominiums, de tous les amendements à cet égard, de toute loi présentée en substitution, et de la déclaration, des règlements et règles, modifiés de temps à autre, du syndicat de copropriété dont le débiteur hypothécaire est membre en vertu de la possession de l’unité de condominium dont il est question dans l’hypothèque:
\par\par 
b) Le débiteur hypothécaire doit payer dans les moindres délais toute dépense commune, dépense d’élément commun, évaluation, cotisation, versement, paiement non payé(e) ou tout autre montant dû au syndicat de copropriété applicable ou à tout agent du débiteur et, à la demande du créancier hypothécaire, fournir au créancier la preuve du paiement:
\par\par 
c) en cas de défaut de paiement aux présentes et par dérogation à tout autre doit ou action du syndicat de co-propriété ou du créancier hypothécaire, le créancier hypothécaire peut opérer une saisie pour arriérés sur toute évaluation, cotisation, versement, paiement ou tout autre montant dû par le débiteur hypothécaire ou se révélant dû en vertu de cette clause; et
\par\par 
d) (i) Par dérogation à toute autorisation, y compris une clause dans tout accord, hypothèque ou charge faite lors de, avant ou après l’entrée en vigueur de ce paragraphe, le détenteur d’une hypothèque ou d’une charge sur une unité peut ne pas exercer le droit de vote ou de consentement appartenant à une autre personne se trouvant être le détenteur d’une unité, à moins que le détenteur de l’hypothèque ou de la charge soit un créancier en possession.
\par\par 
     (ii) Il demeure entendu que rien dans ce paragraphe (i) n’annule les votes ou consentements donnés avant l’entrée en vigueur de ce paragraphe.\par\par }
</xsl:text>
		<xsl:if test="//SolicitorsPackage/LienPosition = 'SECOND'">
			<xsl:text>{\par _(7)_ }{\b\f1\fs22 SECONDE HYPOTHÈQUE: }{\f1\fs22 L’emprunteur reconnaît et est d’avis qu’en cas de défaut de paiement de la première hypothèque existante, un tel défaut constitue un défaut pour cette hypothèque et qu’il donne le droit au titulaire de la charge hypothécaire d’exercer les recours ci-dessous.\par }</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template name="FrenchRequestForFunds">
		<xsl:text>{\sect\sectd } \pghsxn20160\psz5
{\header \pard\plain \s20\ql \sl240\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\par }}
\trowd \trgaph108\trrh719\trleft1800
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx8820
\pard\plain \s6\qc \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\ul\i\f1\fs28 DEMANDE DE FONDS DU NOTAIRE\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh719\trleft1800
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth7020 \cellx8820
\row 
}
\pard \ql \fi720\li5040\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5040\itap0 
{\f1\fs20\par\par\par}

\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth2095 \cellx1980
\cltxlrtb\clftsWidth3\clwWidth2765 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth1460 \cellx6205
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx11353
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 TÉLÉCOPIER À:\cell }
{\b\f1\fs20 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 EXPÉDITEUR:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/>
		<xsl:text>\par }
{\f1\fs20 
Téléphone: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Phone"/>
		<xsl:text>\par 
Télécopieur: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth2095 \cellx1980
\cltxlrtb\clftsWidth3\clwWidth2765 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth1460 \cellx6205
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx11353
\row 
}
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth2095 \cellx1980
\cltxlrtb\clftsWidth3\clwWidth2765 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth1460 \cellx6205
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx11353
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 À L’ATTENTION DE:\cell }
{\b\f1\fs20 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs14 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth2095 \cellx1980
\cltxlrtb\clftsWidth3\clwWidth2765 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth1460 \cellx6205
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx11353
\row 
}
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth2095 \cellx1980
\cltxlrtb\clftsWidth3\clwWidth2765 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth1460 \cellx6205
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx11353
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 DATE:\cell }
{\f1\fs20 ________________\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trkeep\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth2095 \cellx1980
\cltxlrtb\clftsWidth3\clwWidth2765 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth1460 \cellx6205
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx11353
\row 
}

\pard \ql \widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
{\b\f1\fs20 {\pict{\*\picprop\shplid1025{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn 
fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}
{\sp{\sn fLine}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}
\picscalex1080\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0
\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag907321276\blipupi1439{\*\blipuid 
36149fbcc681ce015720994253f9dec0}010009000003310000000000070000000000050000000b0200000000050000000c02f401e80307
0000001b04f401e80300000000050000001402000000000500
00001302f401e803050000001402f40100000500000013020000e803030000000000}
\par }
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs20 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 NUMÉRO DU PRÊT HYPOTHÉCAIRE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/MortgageNum"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 EMPRUNTEUR(S):\cell </xsl:text>
		<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 ADRESSE CIVIQUE:\cell 
</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 MONTANT DU PRÊT HYPOTHÉCAIRE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LoanAmount"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 DATE DE CLÔTURE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 {\pict{\*\picprop\shplid1026{\sp{\sn 
shapeType}{\sv 1}}{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn fillColor}{\sv 8421504}}{\sp{\sn fFilled}{\sv 1}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 1}}}\picscalex1080\picscaley6\piccropl0\piccropr0\piccropt0\piccropb0\picw1764\pich882\picwgoal1000\pichgoal500\wmetafile8\bliptag907321276\blipupi1439{\*\blipuid 
36149fbcc681ce015720994253f9dec0}010009000003310000000000070000000000050000000b0200000000050000000c02f401e803070000001b04f401e80300000000050000001402000000000500
00001302f401e803050000001402f40100000500000013020000e803030000000000}}
{\f1\fs20 \par }
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs20 \par }
\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 Je demande/Nous demandons des fonds pour conclure la transaction le ________________.\par }
\pard \s15\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 Je/Nous reconnaissons qu’il n’y a pas de modifications ni d’annexes à la Convention d’achat-vente qui risqueraient de modifier le prix d’achat ou le montant du dépôt tel qu’énoncé dans votre Lettre d’engagement.\par }
{\f1\fs20 \par }
{\b\f1\fs20 Je/Nous confirmons que:\par }
{\f1\fs20 \par }

\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 [    ]\tab toutes les conditions énoncées dans vos instructions ont été remplies.
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 [    ]\tab Nous sommes d’avis que l’hypothèque a été préparée et exécutée selon vos instructions et qu’une fois l’hypothèque enregistrée, le ou les emprunteur(s) auront un bon titre marchand libre et quitte de tous jugements et autres, et vous aurez un }{\b\f1\fs20 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LienPosition"/>
		<xsl:text>}{\f1\fs20  valide contre la propriété sujette aux exceptions figurant ci-dessous.
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 Les servitudes, empiètements, restrictions, etc, qui n’affectent pas la priorité de l’hypothèque ou la valeur marchande de la propriété sont énumérées ci-dessous.
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par 
\par }\pard \s15\ql \li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 
Les servitudes, empiètements, restrictions, hypothèques, exécutions ou autre charge qui affectent la priorité de l’hypothèque ou la valeur marchande de la propriété sont énumérées ci-dessous.
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par 
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 [    ]\tab }{\b\f1\fs20 Nous confirmons que toutes les taxes dues ont été payées ou seront payées à la clôture le </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>.}{\f1\fs20 
\par 
\par [    ]\tab Nous avons obtenus un Certificat de localisation/Rapport de biens immobiliers avec un Certificat de conformité (RPR) datés du _______________. Le certificat /RPR indique que l’immeuble et la structure composant la propriété hypothéquée sont situés pleinement à l’intérieur des limites des lignes de la parcelle et qu’il n’y a ni servitudes, empiètements ou de droits de passage à l’exception de ceux indiqués ci-dessous.
\par 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 __________________________________________________________________________________________
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 
\par [    ]\tab Tous les documents requis avant l’octroi du prêt hypothécaire ont été envoyés avec la Demande de fonds pour examen au notaire au </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text> {\b Une autorisation verbale du </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text> sera requise afin d’obtenir le produit du prêt hypothécaire.}
\par }\pard \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\b\f1\fs20 LIVRAISON DES FONDS:
\par }{\f1\fs20 
\par [    ]\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text> n’émettent }{\f1\fs20\ul pas}{\f1\fs20 de chèques. Veuillez vous assurer que la « Demande de fonds au notaire » soit télécopiée au moins trois (3) jours ouvrables avant la date de clôture.
\par }\pard \s15\ql \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 
\par }\pard \s15\ql \fi720\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 Veuillez télégraphier les fonds à mon /notre compte en fiducie:
\par }\pard \s15\ql \fi-720\li720\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\f1\fs20 \tab Nom du compte en fiducie _____________________________________________________
\par \tab N{\super o} de succursale _______________ N{\super o} de l’institution _______________ N{\super o} de compte _______________
\par \tab Nom et adresse de la succursale __________________________________________________
\par \tab \tab \tab \tab       __________________________________________________
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\b\f1\fs20 
\par }\pard\plain \qj \fi720\li5040\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5040\itap0 {\f1\fs20
\par }\pard \qr \fi720\li5040\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5040\itap0 {\f1\fs20\cf1 _____________________________________________
\par }\pard \qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs20 Signature du notaire}{\b\f1\fs20 
\par }\pard\plain \s15\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs18\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs20 \page 
\par }\pard \s15\ql \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 {\b\f1\fs20 CI-JOINT LES DOCUMENTS ANNEXÉS REQUIS POUR L’OBTENTION DU PRODUIT DU PRÊT HYPOTHÉCAIRE :
\par 
\par }\pard\plain \s16\qj \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 \fs16\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{\f1\fs20 [     ]\tab Projet de l’hypothèque avec annexe(s) applicable(s)
\par [     ]\tab Formulaire d’Autorisation de débits autorisés par le client (DPA client) ainsi qu’un chèque portant la mention « Nul »
\par }\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1\fs20 [     ]\tab Déclaration de divulgation signée
\par }{\f1\fs20\cf1 [     ]\tab Déclaration officielle signée (objet: occupée par le propriétaire, pas de second financement, mise de fonds)}{\f1\fs20 
\par [     ]\tab Test/Certificat eau de puits indiquant que l’eau est potable
\par [     ]\tab Certificat de possession de garantie de maison neuve
\par [     ]\tab Numéro d’enregistrement du constructeur de garantie de maison de neuve ainsi que le numéro d’inscription de l’unité
\par [     ]\tab Affectation des droits de votre pour le condominium
\par [     ]\tab Engagement du notaire de payer }{\f1\fs20\ul et clore}{\f1\fs20 les dettes (s’il y a lieu d’après la Lettre d’engagement)
\par [     ]\tab Autorisation signée d’envoyer une évaluation et un mémoire de dépense (#45-06-20) PEI
\par [     ]\tab Certificat de localisation ou assurance titre (s’il y a lieu)
\par [     ]\tab Virement télégraphique des instructions pour un compte en fiducie
\par [     ]\tab Certificat de statut à jour (s’il y a lieu)
\par [     ]\tab Garantie personnelle et Attestation d’avis juridique indépendant
\par }
\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\cf1 [     ]\tab Autre (précisez) _____________________________________________________________________________
\par [     ]\tab __________________________________________________________________________________________
\par [     ]\tab __________________________________________________________________________________________
\par [     ]\tab __________________________________________________________________________________________
\par [     ]\tab __________________________________________________________________________________________
\par }{\f1\fs20\cf1 \page 
\par 
\par }\pard\plain \s6\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel5\adjustright\itap0 \b\f1\fs28\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{DÉCLARATION OFFICIELLE\par }\pard\plain \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\fs20\cf1 
\par 
\par }{\b\f1\cf1 CANADA\tab \tab \tab \tab ) concernant une hypothèque entre
\par }
\pard \qj \fi720\li2880\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin2880\itap0 {\b\f1\cf1 ) </xsl:text>
		<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>
\par ) et </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>
\par )
\par }\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\cf1 PROVINCE DE\tab \tab \tab )
\par 
\par }\pard\plain \s7\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel6\adjustright\itap0 \b\f1\fs24\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {Je/Nous </xsl:text>
		<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>, de la ville de
\par }\pard\plain \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{\b\f1\cf1 De la province de\tab \tab \tab \tab \tab , déclarons solennellement:
\par 
\par \tab 1.\tab La propriété sera occupée par moi-même après la clôture.
\par \tab 2.\tab Il n’y a pas de second financement enregistré sur la propriété.
\par \tab 3.\tab Les fonds de ma mise de fonds n’ont pas été empruntés.
\par }\pard \qj \li360\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin360\itap0 {\b\f1\cf1 
\par }\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\cf1 ET JE/NOUS FAIS/FAISONS CETTE DÉCLARATION SOLENNELLE, et la considérons véridique comme si elle avait été  faite sous serment.
\par 
\par DÉCLARÉ devant moi dans la ville de
\par }\pard \qj \fi720\li2160\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin2160\itap0 {\b\f1\cf1 , dans la
\par }\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\cf1 province de
\par En ce jour \tab\tab de\tab\tab \tab , </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CurrentYear"/>
		<xsl:text>.
\par 
\par }\pard \qj \li360\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin360\itap0 {\b\f1\cf1 
\par }\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\b\f1\cf1 (Notaire)\tab \tab \tab \tab \tab \tab \tab \tab ____________________________
\par }\pard \qj \fi720\li5760\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5760\itap0 {\b\f1\cf1 (Signature de l’emprunteur)
\par }
{\b\f1\fs20\cf1 __________________________________\par \par }</xsl:text>
		<xsl:if test="//SolicitorsPackage/UndertakingFlag">
			<xsl:text>
{\b\fs20 \page }
\pard\plain \s17\qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{ \b\f1\fs24 ENGAGEMENT\par \par }
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth10374 \cellx10908
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 À:\cell }{\f1\fs22 </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text>/</xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth10374 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 OBJET:\cell }
{\b\f1\fs22 N{\super o} DU PRÊT HYPOTHÉCAIRE:\cell }
{\b\f1\fs22 </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/MortgageNum"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 NOM(S):\cell </xsl:text>
			<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
			<!--#DG460 only main applicants-->
    	<xsl:call-template name="BorrowerFullName">
        <xsl:with-param name="separa" select="', '"/>
      </xsl:call-template>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 ADRESSE DE LA PROPRIÉTÉ:\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
			<xsl:text/>
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
			<xsl:text>\par </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
			<xsl:text/>
			<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth642 \cellx534
\cltxlrtb\clftsWidth3\clwWidth3066 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs22 \par \par \par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs22 EN CONSIDÉRATION }{\f1\fs22 de et malgré la clôture de la transaction citée ci-dessus, je m’engage par la présente à:
\par 
\par }
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 1.\cell }{\f1\fs22 payer avec le produit de l’avance du prêt hypothécaire les dettes suivantes comme déclaration de paiement de compte pour chaque dette et à }{\b\f1\fs22 
instruire la clôture des comptes:}{\f1\fs22 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\row 
}
</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/LiabilitiesClose/Liability">
				<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx4320
\cltxlrtb\clftsWidth3\clwWidth6588 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 \cell }
{\f1\fs22 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx4320
\cltxlrtb\clftsWidth3\clwWidth6588 \cellx10908
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs22 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 2.\cell }
{\f1\fs22 payer avec le produit de l’avance du prêt hypothécaire les dettes suivantes comme déclaration de paiement de compte pour chaque dette et }{\b\f1\fs22 à garder les comptes ouverts:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10188 \cellx10908
\row 
}
</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/LiabilitiesOpen/Liability">
				<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx4320
\cltxlrtb\clftsWidth3\clwWidth6588 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 \cell }
{\f1\fs22 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth828 \cellx720
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx4320
\cltxlrtb\clftsWidth3\clwWidth6588 \cellx10908
\row 
}
</xsl:text>
			</xsl:for-each>
			<xsl:text>
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs22 
\par 
\par 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs22 
\par 
\par 
\par 
\par 
\par 
\par }
{\b\f1\fs22 DATÉ }{\f1\fs22 à                                        , ce                jour de                             , </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/CurrentYear"/>
			<xsl:text>.
\par 
\par }
\pard \ql \li780\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin780\itap0 
{\f1\fs22 \par \par }
\pard \ql \fi720\li5040\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin5040\itap0 
{\f1\fs22 _________________________________________}
{\b\f1\fs22 
\par </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/SolicitorName"/>
			<xsl:text>
\par Avocat et notaire
\par }
</xsl:text>
		</xsl:if>
		<xsl:text>\pard\plain \s6\qc \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel5\adjustright\itap0 
{ \page }{\f1\fs28\b\ul DÉCLARATION DE FONDS REÇUS ET DÉBOURSÉS\par }
\pard\plain \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\cf1 \par }
{\b\f1\cf1 Propriété: }{\b\f1\cf1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/PostalCode"/>
		<xsl:text>}{\b\f1\cf1 
\par }\pard\plain \s7\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel6\adjustright\itap0 \b\f1\fs24\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{Numéro d’hypothèque: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/MortgageNum"/>
		<xsl:text>
\par }\pard\plain \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\b\f1\cf1 Emprunteur(s): </xsl:text>
		<!--#DG312 xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/-->
		<!--#DG460 only main applicants-->
		<xsl:call-template name="BorrowerFullName">
      <xsl:with-param name="separa" select="', '"/>
    </xsl:call-template>
		<xsl:text>
\par }\pard\plain \s7\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel6\adjustright\itap0 \b\f1\fs24\cf1\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {
________________________________________________________________________________
\par FONDS REÇUS:
\par Expéditeur:
\par }\pard\plain \ql \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>\tab \tab \tab \tab $
\par \tab \tab \tab \tab \tab \tab \tab \tab \tab \tab \tab \tab $
\par }\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 
\par }\pard\plain \s7\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel6\adjustright\itap0 
{\b\f1\fs24 FONDS DÉBOURSÉS:\par }
\pard\plain \ql \li5760\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5760\itap0 
{\f1\fs24 $\par }
\pard \ql \fi720\li5040\sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\lin5040\itap0 
{\f1 $
\par $
\par $
\par $
\par $
\par $
\par $
\par $
\par }\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 \tab \tab \tab \tab \tab \tab \tab \tab ____________________\tab ________________
\par }{\b\f1\cf1 TOTAL:}{\f1 \tab \tab \tab \tab \tab \tab \tab $\tab \tab \tab \tab $
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par 
\par ___________________________________________\tab \tab ________________
\par }{\f1\lang1036\langfe1033\langnp1036 Signature du notaire\tab \tab \tab \tab \tab \tab \tab \tab Date
\par 
\par }

\pard \qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\cf1\lang1036\langfe1033\langnp1036 \sect }\sectd \pghsxn15840\marglsxn432\margrsxn432\psz1\linex0\headery547\footery475\endnhere\sectlinegrid360\sectdefaultcl 
\pard\plain \qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
</xsl:text>
		<xsl:call-template name="XceedLogo"/>
		<xsl:text>
{\par }\pard \s17\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }\pard \s17\qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs24 AUTORISATION DE\par }
\pard\plain \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs24 DÉBITS AUTORISÉS
\par Autorisation de débits préautorisés accordée au bénéficiaire par le payeur
\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1 \par }{\f1 Instructions:\par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 1.\cell Veuillez remplir toutes les sections afin de demander à votre institution financière de faire des paiements tirés sur votre compte.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 2.\cell Veuillez signer la section Conditions en pièce jointe de ce document.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 3.\cell Retournez le formulaire dûment rempli et un chèque portant la mention « Nul » au bénéficiaire à l’adresse indiquée ci-dessous.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 4.\cell Si vous avez des questions, veuillez écrire au bénéficiaire ou l’appeler.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth11124 \cellx11484
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par \par INFORMATION SUR LE PAYEUR }{\scaps\f1 (VEUILLEZ TAPER OU ÉCRIRE LISBLEMENT EN CARACTÈRES D’IMPRIMERIE)}{\f1 
\par }
\trowd \trgaph108\trrh528\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Nom du payeur:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh528\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\row 
}
\trowd \trgaph108\trrh529\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Adresse:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh529\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\row 
}
\trowd \trgaph108\trrh528\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Téléphone:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1 \trowd \trgaph108\trrh528\trleft10\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861\row }\trowd \trgaph108\trrh529\trleft10\trbrdrt
\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb
\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1 Signature(s) du ou des payeur(s):                                                                           Date:\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1 \trowd \trgaph108\trrh529\trleft10\trbrdrt\brdrs\brdrw10 \trbrdrl
\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr
\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth8851 \cellx8861\row }\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\f1 
\par 
\par INFORMATION SUR L’INSTITUTION FINANCIÈRE DU PAYEUR/BANCAIRE
\par }
\trowd \trgaph108\trrh494\trleft10\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 \trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt
\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\lang1024\langfe1024\noproof 
{\shp{\*\shpinst\shpleft8280\shptop200\shpright8280\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz18\shplid1026{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8210\dpline\dpptx0\dppty0\dpptx0\dppty288
\dpx8280\dpy200\dpxsize0\dpysize288\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft7848\shptop200\shpright7848\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz17\shplid1027
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8209\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx7848\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft7416\shptop200\shpright7416\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz16\shplid1028
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8208\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx7416\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft6984\shptop200\shpright6984\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz15\shplid1029
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8207\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx6984\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft6552\shptop200\shpright6552\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz14\shplid1030
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8206\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx6552\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft6120\shptop200\shpright6120\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz13\shplid1031
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8205\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx6120\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft5688\shptop200\shpright5688\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz12\shplid1032
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8204\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx5688\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft5256\shptop200\shpright5256\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz11\shplid1033
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8203\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx5256\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft4824\shptop200\shpright4824\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz10\shplid1034
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8202\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx4824\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft4392\shptop200\shpright4392\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz9\shplid1035
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8201\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx4392\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft3960\shptop200\shpright3960\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz8\shplid1036
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8200\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx3960\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft2520\shptop200\shpright2520\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz6\shplid1037
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8198\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx2520\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft2952\shptop200\shpright2952\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz7\shplid1038
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8199\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx2952\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft792\shptop200\shpright792\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz3\shplid1039
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8195\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx792\dpy200\dpxsize0\dpysize288\dplinew15\dplinecor0\dplinecog0\dplinecob0}
}}{\shp{\*\shpinst\shpleft360\shptop200\shpright360\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz2\shplid1040{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8194\dpline\dpptx0\dppty0\dpptx0\dppty288
\dpx360\dpy200\dpxsize0\dpysize288\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft1224\shptop200\shpright1224\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz4\shplid1041
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8196\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx1224\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft1656\shptop200\shpright1656\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz5\shplid1042
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8197\dpline\dpptx0\dppty0\dpptx0\dppty288\dpx1656\dpy200\dpxsize0\dpysize288
\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}{\shp{\*\shpinst\shpleft3528\shptop56\shpright3528\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz1\shplid1043
{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8193\dpline\dpptx0\dppty0\dpptx0\dppty432\dpx3528\dpy56\dpxsize0\dpysize432\dplinew15\dplinecor0\dplinecog0\dplinecob0}
}}{\shp{\*\shpinst\shpleft2088\shptop56\shpright2088\shpbottom488\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz0\shplid1044{\sp{\sn shapeType}{\sv 20}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}{\sp{\sn fFillOK}{\sv 0}}{\sp{\sn fFilled}{\sv 0}}{\sp{\sn fArrowheadsOK}{\sv 1}}{\sp{\sn fLayoutInCell}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 0}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8192\dpline\dpptx0\dppty0\dpptx0\dppty432
\dpx2088\dpy56\dpxsize0\dpysize432\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}}
{\f1\fs24 Numéro de succursale      N{\super o} de l’institution   Numéro de compte\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh494\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\trowd \trgaph108\trrh462\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 Nom de l’institution financière:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh462\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\trowd \trgaph108\trrh463\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 Succursale:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh463\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\trowd \trgaph108\trrh462\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 Adresse de la succursale:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh462\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\trowd \trgaph108\trrh463\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs24 Ville/Province:                                                                    Code postal:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh463\trleft10
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8914
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par \par INFORMATION SUR LE BÉNÉFICIAIRE\par }
\trowd \trgaph108\trrh449\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Nom du bénéficiaire:\par\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh449\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\row 
}
\trowd \trgaph108\trrh449\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Adresse:\par\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/>
		<xsl:text>
\par\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh449\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\row 
}
\trowd \trgaph108\trrh450\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Téléphone:\par\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Phone"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh450\trleft-3
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8904 \cellx8901
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1 \page AUTORISATION DE DÉBITS PRÉAUTORISÉS\par }
\pard\plain \s8\qc \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel7\adjustright\itap0 
{\b\f1\fs22 Conditions\par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs24 \par }
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 1.\cell Dans cette autorisation, « Je », « moi » et « mon » réfèrent à chaque détenteur de compte qui signe ci-dessous.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 2.\cell J’accepte de participer à ce plan de débits préautorisés à des fins personnelles/maison ou client et j’autorise le bénéficiaire indiqué au verso et tout successeur à émettre un débit sous forme papier, électronique ou une autre forme dans le but d’effectuer un paiement pour des biens ou services client (un « DPA client »), tiré sur mon compte indiqué au verso (le « Compte ») à l’institution financière indiquée au verso (l’« institution financière ») et j’autorise l’institution financière à honorer et à payer ce débit. Il est entendu que cette autorisation est accordée à l’intention du bénéficiaire et de notre institution financière et sous réserve de l’acceptation par notre institution financière d’effectuer les débits dans mon compte en conformité avec les règles de l’Association canadienne des paiements. Je reconnais le caractère exécutoire de toute directive que je pourrais donner d’effectuer un DPA client, et de tout DPA client effectué aux termes de la présente autorisation, comme si je l’avais signée, et dans le cas d’un DPA papier, comme s’il s’agissait d’un chèque signé que j’avais signé.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 3.\cell Je me réserve le droit de résilier la présente autorisation en tout temps en faisant parvenir un avis d’annulation écrit au bénéficiaire. La présente autorisation s’applique uniquement à la méthode de paiement, et j’accepte que l’annulation de l’autorisation ne met fin d’aucune façon à quelque contrat qui existe entre le bénéficiaire et moi, ou n’a aucun effet sur un tel contrat.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 4.\cell Je conviens que mon institution financière n’est pas tenue de vérifier que tous les DPA clients sont effectués conformément à la présente autorisation, y compris le montant, la fréquence et l’exécution de l’objet de tout DPA client.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 5.\cell Je conviens que la délivrance de la présente autorisation au bénéficiaire équivaut à sa délivrance par moi à mon institution financière. Je conviens que le bénéficiaire peut délivrer la présente autorisation à ma  propre institution financière et je consens à ce que toute information contenue dans la présente autorisation soit divulguée à cette institution financière.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 6.\cell Je comprends que dans le cas:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth716 \cellx1513
\cltxlrtb\clftsWidth3\clwWidth9395 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell \cell (i)\cell D’un DPA client de montant fixe, nous recevrons un préavis écrit du bénéficiaire avec le montant à prélever et le(s) date(s) de prélèvement, au moins dix (10) jours civils avant la date d’échéance du premier DPA client, et que chaque préavis sera reçu à chaque fois qu’il y aura un changement dans le montant ou la ou les date(s) de paiement;\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth716 \cellx1513
\cltxlrtb\clftsWidth3\clwWidth9395 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell \cell (ii)\cell D’un DPA client de montant variable, nous recevrons un préavis écrit du bénéficiaire avec le montant à prélever et la ou les date(s) de prélèvement, au moins dix (10) jours civils avant la date d’échéance du premier DPA client; et\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth716 \cellx1513
\cltxlrtb\clftsWidth3\clwWidth9395 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell \cell (iii)\cell D’un plan de DPA client qui prévoit l’émission d’un DPA client en réponse à une demande directe de ma part (comme, mais sans s’y limiter, une instruction téléphonique) pour que le bénéficiaire émette un DPA client en paiement total ou partiel d’une facture que nous aurions reçue, le préavis de dix (10) jours est annulé.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth716 \cellx1513
\cltxlrtb\clftsWidth3\clwWidth9395 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 7.\cell Je peux contester un DPA client en présentant à mon institution financière une déclaration signée, aux conditions suivantes:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth10111 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell (a)\cell Le DPA client n’a pas été fait conformément avec les dispositions de cette autorisation;\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth10111 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell (b)\cell Cette autorisation a été annulée;\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth10111 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell (c)\cell Je n’ai pas reçu de préavis requis par la section 6;\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth437 \cellx797
\cltxlrtb\clftsWidth3\clwWidth10111 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell Je reconnais qu’afin de me faire rembourser le montant d’un PDA client litigieux par mon institution financière, je dois signer une déclaration indiquant que les faits mentionnés en (a), (b) ou en (c) ci-dessus se sont produits, et la présenter à mon institution financière dans les quatre-vingt dix (90) jours ouvrables suivant la date de l’inscription au compte d’un DPA client litigieux. Je reconnais qu’au terme de cette période de quatre-vingt (90) dix jours ouvrables, je dois régler tout différend concernant un PDA client uniquement avec le bénéficiaire, et que mon institution financière n’assumera aucune responsabilité à mon égard au sujet d’un tel DPA client.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 8.\cell Je certifie que toute l’information fournie relativement au compte est exact et je conviens d’informer le bénéficiaire, par écrit, de tout changement de l’information sur le compte, contenue dans la présente autorisation au moins dans les dix (10) jours ouvrables précédant la prochaine date d’échéance d’un DPA client. Dans l’éventualité d’un tel changement, la présente autorisation demeurera en vigueur relativement à toute nouvelle exigence que l’Association canadienne des paiements pourrait édicter relativement aux services décrits aux présentes.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 9.\cell Je garantis que toutes les personnes dont la signature est nécessaire pour le compte ont signé la présente autorisation ci-dessous.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 10.\cell Je confirme par les présentes que j’ai Iu et compris toutes les conditions décrites ci-dessus, et que je les accepte.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 11.\cell Je conviens d’observer les règles de l’Association canadienne des paiements, ou toute autre réglementation susceptible d’influer sur les services décrits aux présentes, maintenant en vigueur ou pouvant être édictée à l’avenir, et je conviens de me conformer à toute nouvelle exigence que l’Association canadienne des paiements pourrait édicter relativement aux services décrits aux présentes.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 12.\cell }
\pard\plain \s18\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\ul S‘applique au Québec seulement}{\f1\fs18 : Les parties conviennent que la présente autorisation et tous les documents s’y rattachant soient rédigés et signés en anglais.\par }
{\f1\par \par }
\pard \s18\qj \widctlpar\intbl\tqc\tx4770\tx8640\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
________________________________________________________________________
\par Nom du détenteur du compte	                   Signature                                              Date\par \par 
________________________________________________________________________
\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Nom du détenteur du compte                   Signature                                              Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
</xsl:text>
	</xsl:template>
	<!--  We are using the build logic from the commitment letter template that already exists instead of replicating it here... -->
	<xsl:template name="FrenchCommitmentLetter">
		<xsl:text>{\sect\sectd} \pghsxn15840
\marglsxn432\margrsxn432\linex0\headery547\footery475\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
		<xsl:call-template name="FrenchHeader"/>
		<xsl:call-template name="FrenchPage1"/>
		<xsl:text>{\sect\sectd} \pghsxn15840
\marglsxn432\margrsxn432\linex0\headery547\footery475\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
		<xsl:call-template name="FrenchPage2"/>
		<xsl:text>{\sect\sectd} \pghsxn15840\marglsxn432\margrsxn432\linex0\sectdefaultcl </xsl:text>
		<xsl:call-template name="FrenchPage3"/>
	</xsl:template>
	<xsl:template name="FrenchFinalReport">
		<xsl:text>{\sect\sectd} \pghsxn20160\psz5
{\header \pard\plain \s20\ql \sl240\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {\par }}	
\trowd \trgaph108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11520 \cellx11520
\pard\plain \s4\qc \sb240\sa240\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel3\adjustright 
{\b\f1\fs28\ul RAPPORT FINAL DU NOTAIRE SUR LES TITRES\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11520 \cellx11520
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }
\trowd \trgaph108
\trbrdrh\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth8460 \cellx8460
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx11520
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1 N{\super o} de référence du notaire.}{\f1  }{\b\f1 ____________________\cell }
{\b\f1 __________________\par \b0\f1 Date\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108
\trbrdrh\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth8460 \cellx8460
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3060 \cellx11520
\row 
}
\trowd \trgaph108\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth720 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10800 \cellx11520
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Destinataire:\cell }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/City"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Province"/>
		<xsl:text/>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Postal"/>
		<xsl:text>
\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \par À l’attention de: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/FunderName"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth720 \cellx720
\cltxlrtb\clftsWidth3\clwWidth10800 \cellx11520
\row 
}
\pard \qj \fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1 \par }
\pard\plain \s20\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs20 Conformément à vos instructions, nous avons agi en qualité de notaire dans la transaction ci-après. Nous avons enregistré un prêt hypothécaire (ou un acte de prêt, selon le cas) l’« hypothèque », au moyen de la formule pertinente auprès du </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> approprié et nous vous présentons notre rapport final:\par }
\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\pard \qj \sl360\slmult1\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 Emprunteur(s) ______________________________________________________________________________________
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> ___________________________________________________________________________________
\par }
\pard\plain \s2\qj \sl360\slmult1\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs20 Adresse civique de la propriété _________________________________________________________________________
\par Adresse légale de la propriété ____________________________________________________________________________\par\par }

\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx10980
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nous sommes d’avis que:\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 

{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx10980
\row 
}

\trowd \trgaph57\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980

\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (1)\cell }

\pard \qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
Une hypothèque </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LPDescription"/>
		<xsl:text> valide et légalement exécutoire en faveur de </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text>, pour l’intégralité des sommes avancées, a été enregistrée le _______________________________ auprès de la Division </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LandOfficeClause"/>
		<xsl:text> de _____________________________________________________ sous le N{\super o} d’instrument ______________________________.}
{\f1\ul \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}

\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (2)\cell }
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Le ou les emprunteur (s) détiennent sur la propriété un titre valable et réalisable en toute propriété, libre de toute servitude, mis à part les défauts mineurs énumérés ci-dessous, qui risquerait de modifier le rang de l’hypothèque ou la qualité marchande de la propriété. Toutes les conditions ont été remplies concernant la retenue pour privilège / le délai de conservation. Les servitudes, empiètements, restrictions, etc. sont énumérées ci-dessous:\par }
\pard\plain \qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20
_________________________________________________________________________________________________________________________________________________________________________________________________________
_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
___________________________________________________________\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx10980
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Unité(s) de condominium (selon le cas):\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth10980 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmgf\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 2a)\cell }
\pard\plain \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Le syndicat de copropriétaires a été avisé de l’existence de l’hypothèque et de vos droits de vote et il a été prié de vous en informer 
si le ou les emprunteur(s) sont en défaut de paiement des frais de co-propriété. Nous avons obtenu une attestation pour préclusion et un certificat 
d’assurance conformément à vos instructions et nous estimons que les deux documents sont satisfaisants. Nous confirmons également que les arrérages 
notés sur l’attestation pour préclusion ont été payés.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmgf\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 \clvmrg
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 b)\cell 
}
\pard\plain \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Toutes les mesures nécessaires ont été prises pour confirmer votre droit de vote si tel était votre souhait de l’exercer.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmrg\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\row 
}
\pard 
\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 c)\cell 
}
\pard\plain \s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Dispositions concernant les avis, s’il y en a: __________________________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmrg\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth540 \cellx1360
\cltxlrtb\clftsWidth3\clwWidth9620 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (3)\cell }
\pard\plain \s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Toutes les restrictions ont été respectées dans leur inégalité et il n’y a pas d’ordres de travail ni d’avis de défaut en instance à l’encontre de la propriété.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (4)\cell }
\pard\plain \s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Les taxes et autres droits municipaux dus et exigibles sur la propriété ont été entièrement payés à _______________.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 (5)\cell }
\pard\plain 
\s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 L’hypothèque est conforme aux dispositions de la Loi sur l’aménagement du territoire, modifiée au gré du Législateur, 
car le ou les emprunteurs ne conservent pas le fief ni la valeur nette de rachat de tout terrain adjacent à la propriété donnée en garantie du prêt 
hypothécaire, et ils ont ni le droit ni le pouvoir d’accorder ou de céder ce terrain, ni celui d’exercer un pouvoir de nomination relativement à ce terrain.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (6)\cell }
\pard\plain \s17\qj \sa240\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Une copie certifiée conforme de l’hypothèque (y compris les conditions standard d’octroi de prêt hypothécaire, les annexes y afférentes, 
ainsi que l’Accusé de réception et les Directives du ou des emprunteurs, s’il y a lieu) a été remise à chaque emprunteur.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmgf\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\pard \qj \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (7)\cell }
\pard\plain \s20\qj \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 (7)	Une assurance incendie a été souscrite conformément à vos instructions avec pleine valeur de remplacement et les pertes seront payables à </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text> et </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>
		<xsl:text> conformément à la clause hypothécaire type du BAC.\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmgf\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth10160 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmrg\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\pard \qj \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Compagnie d’assurance:____________________________\cell Courtier: _________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\clvmrg\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\pard \qj \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant de l’assurance: ____________________________\cell Police n{\super o}: ________________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\row 
}
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\pard \qj \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard\plain \s20\qj \sa120\keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Date d’effet: __________________________________\cell 
Date d’expiration: __________________________\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trkeep\trftsWidth3\trwWidth10980\trpaddr115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth820 \cellx820
\cltxlrtb\clftsWidth3\clwWidth5450 \cellx6270
\cltxlrtb\clftsWidth3\clwWidth4710 \cellx10980
\row 
}
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs20 \par }
{\b\f1\fs20 Ci-joint les documents annexés pour vos dossiers:}
{\f1\fs20 \par \par }
\pard\plain\s19\qj \fi-720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0
{\f1\fs20 [     ] \tab Rapport final du notaire sur les titres
\par [     ] \tab Copie enregistrée du prêt hypothécaire (ou de l’acte de prêt), y compris les standard d’octroi de prêt hypothécaire, les annexes y afférentes, ainsi que l’Accusé de réception du ou des emprunteurs et </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, s’il y a lieu
\par [     ]\tab Charge, Accusé de réception et Directives sur support électronique (uniquement les comtés de l’Ontario qui disposent de l’enregistrement électronique)
\par [     ]\tab Numéro d’instrument (Enregistrement Teranet)
\par [     ]\tab Convention de garantie (uniquement les comtés de l’Ontario qui disposent de l’enregistrement électronique)
\par [     ]\tab Certificat de titre de propriété (ou son équivalent pour la province)
\par [     ]\tab Certificat de localisation ou assurance titre s’il y a lieu
\par [     ]\tab Police d’assurance sur titres et Annexes A &amp; B de la police
\par [     ]\tab Convention d’amendement enregistrée (s’il y a lieu)
\par [     ]\tab Certificat d’exécution
\par [     ]\tab Attestation de taxes municipales
\par [     ]\tab Certificat de fin de travaux et de prise de possession
\par [     ]\tab Certificat de possession de garantie de maison neuve
\par [     ]\tab Certificat d’occupation/Permis
\par [     ]\tab Garantie personnelle et Attestation d’avis juridique indépendant
\par [     ]\tab Certificat du shérif/Recherche RG
\par [     ]\tab Déclaration officielle signée
\par [     ]\tab Police d’assurance- incendie
\par [     ]\tab Note de couverture d’assurance du syndicat de copropriété
\par [     ]\tab Certificat de zonage/Mémorandum (uniquement les propriétés de l’Ontario)
\par [     ]\tab Déclaration de possession (uniquement les propriétés du Manitoba)
\par [     ]\tab Cession de loyers enregistrée
\par [     ]\tab Accord de sécurité générale (GSA) enregistré sous PPSA
\par [     ]\tab Déclaration de fonds reçus et déboursés
\par [     ]\tab Vérification du paiement intégral des dettes
\par [     ]\tab Autres documents (préciser)______________________________________________________________________
\par [     ]\tab _____________________________________________________________________________________________
\par [     ]\tab _____________________________________________________________________________________________
\par [     ]\tab _____________________________________________________________________________________________
\par [     ]\tab _____________________________________________________________________________________________
\par \par }
\pard\plain \s20\qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\par _____________________________________________________\par }
\pard\plain \qr \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\cf1 Signature du notaire\par }</xsl:text>
	</xsl:template>


	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->

	<xsl:template name="RTFFileEnd">
		<xsl:text>{\f1\par }}</xsl:text>
	</xsl:template>


	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff63\deflang1033\deflangfe1033
{\fonttbl
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f14\fnil\fcharset2\fprq2{\*\panose 05000000000000000000}Wingdings;}
}
{\colortbl;
\red0\green0\blue0;
\red0\green0\blue255;
\red0\green255\blue255;
\red0\green255\blue0;
\red255\green0\blue255;
\red255\green0\blue0;
\red255\green255\blue0;
\red255\green255\blue255;
\red0\green0\blue128;
\red0\green128\blue128;
\red0\green128\blue0;
\red128\green0\blue128;
\red128\green0\blue0;
\red128\green128\blue0;
\red128\green128\blue128;
\red192\green192\blue192;
\red255\green255\blue255;
}
{\stylesheet
{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \snext0 Normal;}
{\s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 \sbasedon0 \snext0 heading 1;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext15 header;}
{\s16\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext16 footer;}
}

\margl720\margr720\margt720\margb720 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\headery540\footery480\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>
	
	
	<xsl:template name="createRefundableFeeRows">
		<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>Add <xsl:value-of select="./FeeVerb"/>
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="./FeeAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620\row 
}
</xsl:text>
	</xsl:template>


	<xsl:template name="createRefundableFeeBlankRow">
		<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:text>\cell </xsl:text>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620\row 
}
</xsl:text>
	</xsl:template>


	<xsl:template name="Ticket_739">
    <!--#DG362 remove local font change let the calling context prevail -->
		<xsl:text>
\par _(7)_ \b INTEREST RATE \b0 (Applicable only to Variable Rate Mortgages):\par 
a) The interest rate chargeable under the mortgage at any particular time is called the "Payment 
Rate". Interest is calculated semi-annually, not in advance and is payable monthly. Interest is 
payable on this variable rate mortgage on the loan amount both before and after the final payment 
date, default and judgment. The Payment Rate is a floating rate and will vary automatically as 
set out hereunder.\par \par 
 
b) The initial Payment Rate will be set seven (7) days prior to the advance of funds under this 
mortgage and shall be based on the then current Bankers Acceptance Rate ("BA Rate") as determined 
by Xceed Mortgage Corporation, plus an interest differential as indicated on the registered 
mortgage document. Whenever this mortgage refers to the Payment Rate payable on the loan amount, 
that expression means the BA Rate as determined by Xceed Mortgage Corporation plus an interest 
differential as aforesaid and is payable as set out in this paragraph.\par \par 
 
c) The Payment Rate will be adjusted on each three (3) month anniversary of the Interest 
Adjustment Date of this mortgage. The Payment Rate for each three (3) month period shall be the 
BA Rate in effect on each such anniversary date as determined by Xceed Mortgage Corporation, plus 
the interest differential as indicated on the registered mortgage document.\par\par 
 
_(8)_ \b CONVERSION TO A FIXED RATE TERM  \b0 (Applicable only to Variable Rate Mortgages):
Provided that the mortgage is not in default, the mortgagor(s) shall, subject to a review of a 
current credit bureau report on the mortgagor(s) and approval of the same by Xceed Mortgage 
Corporation, upon written request, be able to convert the variable rate mortgage to a fixed rate 
mortgage product offered by Xceed Mortgage Corporation for a new term that is at least equal to 
the remaining term of the existing variable rate mortgage.\par \par 
 
In the event that the mortgage is amended to extend the term hereof, the date of the mortgage for 
the purpose of the application of Section 10 of the Interest Act (Canada) or any similar Federal 
or Provincial legislation permitting prepayment, shall be conclusively deemed to be the first day 
of the last extension of time for payment and the mortgage as amended shall not, at any time or 
times, be subject to prepayment, in whole or in part, save as may be herein provided.\par 
	</xsl:text>
	</xsl:template>


  <!--#DG362 avoid repetition -->
	<xsl:template name="secondMtgClause">
				<xsl:text>\par _(9)_ \b SECOND MORTGAGE: \b0 The Chargor acknowledges and 
agrees that in the event of default under the existing first mortgage, such default constitutes 
default under this Charge and entitles the Chargee to exercise its remedies hereunder.\par
</xsl:text>
	</xsl:template>
	

	<xsl:template name="privacyClause">
		<xsl:text>\b PRIVACY CLAUSE: \b0 
\rdblquote You\rdblquote  and \rdblquote your\rdblquote  means the 
applicant and any joint applicant. \rdblquote We\rdblquote , \rdblquote our\rdblquote  and 
\rdblquote us\rdblquote  means Xceed Mortgage Corporation. \par 
You authorize us to collect personal information (including credit, employment and other 
financially-related information) about you, from you and from credit bureaus, credit reporting 
agencies, financial institutions, references you have provided to us and from persons who may 
have referred your mortgage business to us. \par You acknowledge that the personal information 
collected from you and others may be used for the following purposes: (i) to determine your 
financial situation; (ii) to determine your initial and ongoing eligibility for mortgage 
services; (iii) to administer or service your mortgage; (iv) to arrange for and in connection 
with the financing of our mortgage business; and (v) as otherwise necessary for the provision of 
mortgage services. \par You acknowledge that the personal information collected from you and 
others may be disclosed to the following people for the following purposes: (i) to credit 
bureaus, credit reporting agencies, mortgage insurers and financial institutions to confirm your 
financial situation and your initial and ongoing eligibility for mortgage services; (ii) to 
persons retained to administer or service your mortgage for the purpose of such administration or 
servicing; (iii) to persons (or their permitted assignees) involved in the financing or 
securitizing, or facilitation of the financing or securitizing, of our mortgage business for the 
purpose of their providing or facilitating such financing or securitizing (which may include the 
administration or servicing of your mortgage by them or their agents); and (iv) to other persons 
as necessary for the provision of mortgage services to you.\par
</xsl:text>
	</xsl:template>
    

	<xsl:template name="armClauses">
		<xsl:param name="cntFrom" select="10"/>	
	
		<xsl:text>\par 
_(</xsl:text>
		<xsl:value-of select="$cntFrom"/>
		<xsl:text>)_ \b  INTEREST RATE\b0  (Applicable only to Adjustable Rate Mortgages):\par\par

a) The interest rate chargeable under the mortgage at any particular time is called the "Interest 
Rate". Interest is calculated semi-annually, not in advance and is payable 
monthly. Interest is payable on this adjustable rate mortgage on the loan amount both before and 
after the final payment date, default and judgment. The Interest Rate is a floating rate until it 
is set 7 days prior closing based on the CDOR rate and will not vary as set out hereunder.\par\par

b) The initial Interest Rate will be set seven (7) days prior to the advance of funds under this 
mortgage and shall be based on the then current CDOR Rate as determined by Xceed Mortgage 
Corporation, plus an interest differential as indicated on the registered mortgage document. 
Whenever this mortgage refers to the Interest  Rate payable on the loan amount, that expression 
means the CDOR Rate as determined by Xceed Mortgage Corporation plus an interest differential as 
aforesaid and is payable as set out in this paragraph.\par\par

c) The Interest differential may be decreased annually on the anniversary date of the interest 
adjustment date ONLY if your Beacon Score has increased as per attached table.  Xceed Mortgage 
Corporation will review your Equifax Credit Bureau report to determine if you Beacon Score has 
increased.\par\par

<!-- use braces so as not to affect the default font set in the calling context -->
\trowd\trgaph108\trrh315\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\cellx6860\pard\intbl\qc\cf1{\b\fs24 Spreads based on Beacon Score \par
(not including CDOR  Rate)}\cell\row

\trowd\trgaph108\trrh255\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\clcbpat1\cellx594
\clvertalb\clcbpat1\cellx1840
\clvertalb\clcbpat1\cellx2956
\clvertalb\clcbpat1\cellx3932
\clvertalb\clcbpat1\cellx4908
\clvertalb\clcbpat1\cellx5884
\clvertalb\clcbpat1\cellx6860
\pard\intbl\qc\cf17{\fs20 LTV\cell &lt;550\cell 550 - 599\cell 600 - 639\cell 
640 - 679\cell 680 - 699\cell 700+\cell\row}

\trowd\trgaph108\trrh255\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\cellx594\clvertalb\cellx1840\clvertalb\cellx2956\clvertalb\cellx3932
\clvertalb\cellx4908\clvertalb\cellx5884\clvertalb\cellx6860
\pard\intbl\qc\cf0\b0{\fs20 100\cell 5.00%\cell 4.70%\cell 4.35%\cell 3.75%\cell 
3.35%\cell 3.00%\cell\row}

\trowd\trgaph108\trrh255\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\cellx594\clvertalb\cellx1840\clvertalb\cellx2956
\clvertalb\cellx3932\clvertalb\cellx4908\clvertalb\cellx5884
\clvertalb\cellx6860
\pard\intbl\qc{\fs20 95\cell 4.60%\cell 4.30%\cell 3.95%\cell 3.35%\cell 
2.95%\cell 2.60%\cell\row}

\trowd\trgaph108\trrh255\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\cellx594\clvertalb\cellx1840\clvertalb\cellx2956\clvertalb\cellx3932
\clvertalb\cellx4908\clvertalb\cellx5884\clvertalb\cellx6860
\pard\intbl\qc{\fs20 90\cell 4.40%\cell 4.10%\cell 
3.75%\cell 3.15%\cell 2.75%\cell 2.40%\cell\row}

\trowd\trgaph108\trrh255\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\cellx594\clvertalb\cellx1840\clvertalb\cellx2956\clvertalb\cellx3932
\clvertalb\cellx4908\clvertalb\cellx5884\clvertalb\cellx6860
\pard\intbl\qc{\fs20 85\cell 4.20%\cell 3.90%\cell 
3.55%\cell 2.95%\cell 2.55%\cell 2.20%\cell\row}

\trowd\trgaph108\trrh255\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\cellx594\clvertalb\cellx1840\clvertalb\cellx2956\clvertalb\cellx3932
\clvertalb\cellx4908\clvertalb\cellx5884\clvertalb\cellx6860
\pard\intbl\qc{\fs20 80\cell 4.00%\cell 3.70%\cell 
3.35%\cell 2.75%\cell 2.35%\cell 1.99%\cell\row}

\trowd\trgaph108\trrh255\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\cellx594\clvertalb\cellx1840\clvertalb\cellx2956\clvertalb\cellx3932
\clvertalb\cellx4908\clvertalb\cellx5884\clvertalb\cellx6860
\pard\intbl\qc{\fs20 75\cell 3.85%\cell 3.55%\cell 
3.20%\cell 2.60%\cell 2.20%\cell 1.99%\cell\row}

\pard\f1\par

\f1\fs22 _(</xsl:text>
		<xsl:value-of select="$cntFrom+1"/>
		<xsl:text>)_ \b CONVERSION TO A FIXED RATE TERM\b0\ (Applicable only to Adjustable Rate Mortgages):\par\par
Provided that the mortgage is not in default, the mortgagor(s) shall, subject to a review of a current 
credit bureau report on the mortgagor(s) and approval of the same by Xceed Mortgage Corporation, 
in its sole discretion, upon written request, be able to convert the adjustable rate mortgage to a fixed rate
 mortgage product offered by Xceed Mortgage Corporation for a new term that is at least equal to the remaining term of the existing adjustable rate mortgage.\par\par

In the event that the mortgage is amended to extend the term hereof, the date of the mortgage for 
the purpose of the application of Section 10 of the \i Interest Act (Canada) \i0 or any 
similar Federal or Provincial legislation permitting prepayment, shall be conclusively deemed to 
be the first day of the last extension of time for payment and the mortgage as amended shall not, 
at any time or times, be subject to prepayment, in whole or in part, save as may be herein 
provided.\par\par

_(</xsl:text>
		<xsl:value-of select="$cntFrom+2"/>
		<xsl:text>)_ \b PREPAYMENT IN FULL\b0 (Applicable only to Adjustable Rate Mortgages):\par\par
This mortgage can be paid in full upon payment of an\~additional\~amount equal to 
\b 6 months interest\b0  on the principal balance outstanding as at the proposed pay out date, 
without any credit being given for any sum that may have been able to have been paid pursuant to 
any annual prepayment privileges contained in the charge.\~ The rate used to calculate this 
amount is the\~posted Xceed Mortgage Corporation\~rate\~for adjustable rate mortgages in effect 
at the time the discharge statement is requested.\par\par

_(</xsl:text>
		<xsl:value-of select="$cntFrom+3"/>
		<xsl:text>)_ \b PREPAYMENT IN FULL\b0 (Applicable only to Variable Rate Mortgages):\par\par
This mortgage can be paid in full upon payment of an additional amount equal to \b 6 months interest\b0  on 
the principal balance outstanding as at the proposed pay out date, without any credit being given for 
any sum that may have been able to have been paid pursuant to any annual prepayment privileges 
contained in the charge.  The rate used to calculate this amount is the posted Xceed Mortgage Corporation 
rate for variable rate mortgages in effect at the time the discharge statement is requested.\par\par

_(</xsl:text>
		<xsl:value-of select="$cntFrom+4"/>
		<xsl:text>)_ \b PREPAYMENT IN FULL\b0 (For Fixed Rate Mortgages only):\par\par
This mortgage is closed to prepayment in full, except in certain exceptional circumstances (e.g. bona 
fide sale of the property).  Xceed will only discharge the mortgage in these circumstances upon 
payment of a penalty which at the very least shall be the greater of three months interest and 
the present value of the forgone profit that would have been enjoyed by Xceed for the remainder 
of the term of the mortgage, had the mortgage not been discharged.  As a proxy for the forgone 
profit, Xceed will compare the interest rate on the existing mortgage to the interest rate on 
alternative investments such as residential mortgages offered by Canada’s chartered Banks 
including any rate discounting offered by those banks on or about the time the mortgage discharge 
statement is requested. \par

</xsl:text>
	</xsl:template>
  <!--#DG362 end -->
	
</xsl:stylesheet>
