<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!--	
17/Jul/2006 DVG #DG462 #3947  GE-QA - Minor Change  to IA Data Sheet  
  18/May/2006  GE Multi-Investor Data Sheet Generation Style Sheet By David Sun-->
	
	<!-- Developer's Notes: 
		  Please Note that for output of date, this stylesheet expects the date in the xml file in one of the following formats:
			 1. MMM D[D], YYYY (example: Jun 5, 2006)
			 2. MMM* D[D], YYYY (example: June 5, 2006)
		 For date input in other format, this stylesheet will not guarantee ouputting them correctly.
	-->
	<!-- David Sun: Aug.10, 2006
		Changes were made to the output logic for col 4 and col20 based on the requests in GE-Multi-Investor Project - CR #4 , 
		tickte #4181
	-->

	<xsl:output method="text"/>
	
	<!-- ****************************************************************Global Variables ***************************************************************** -->
	  
	<xsl:variable name="newline">
<xsl:text>
</xsl:text>
	</xsl:variable>
	
	<!--********************* Property information ********************** -->	
	<xsl:variable name="pun" select="//Deal/Property/unitNumber"/>
	<xsl:variable name="psn" select="//Deal/Property/propertyStreetNumber"/>
	<xsl:variable name="psnm" select="//Deal/Property/propertyStreetName"/>
	<xsl:variable name="pst" select="//Deal/Property/streetType"/>
	<xsl:variable name="psd" select="//Deal/Property/streetDirection"/>
	<xsl:variable name="pcity" select="//Deal/Property/propertyCity"/>
	<xsl:variable name="pprovince" select="//Deal/Property/province"/>
	<xsl:variable name="pprovinceAbbr">
		<xsl:call-template name="getProvinceAbbr">
			<xsl:with-param name="provinceId" select="//Deal/Property/provinceId"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="pFSA" select="//Deal/Property/propertyPostalFSA"/>
	<xsl:variable name="pLDU" select="//Deal/Property/propertyPostalLDU"/>
	
	<!--********************* Borrower current address information ********************** -->
	<xsl:variable name="curBorrAddrLine1" select="//Deal/Borrower[borrowerNumber=1]/BorrowerAddress[borrowerAddressTypeId=0]/Addr/addressLine1"/>
	<xsl:variable name="curBorrCity" select="//Deal/Borrower[borrowerNumber=1]/BorrowerAddress[borrowerAddressTypeId=0]/Addr/city"/>
	<xsl:variable name="curBorrProvAbbr">
		<xsl:call-template name="getProvinceAbbr">
			<xsl:with-param name="provinceId" select="//Deal/Borrower[borrowerNumber=1]/BorrowerAddress[borrowerAddressTypeId=0]/Addr/provinceId"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="curBorrFSA" select="//Deal/Borrower[borrowerNumber=1]/BorrowerAddress[borrowerAddressTypeId=0]/Addr/postalFSA"/>
	<xsl:variable name="curBorrLDU" select="//Deal/Borrower[borrowerNumber=1]/BorrowerAddress[borrowerAddressTypeId=0]/Addr/postalLDU"/>
	
	<!-- ***************************************************************** End of Global Variables *********************************************************** -->
	
	<!-- the Main -->
	<xsl:template match="/">
			<xsl:call-template name="header"/>
			<xsl:value-of select="$newline"/>
			<xsl:call-template name="content"/>
	</xsl:template>
	
	<!-- ************************************************************************ 	-->
	<!-- Generate the header                                       				           	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="header">
		<!--col: 01 -->
		<xsl:text>Vendor Loan number,</xsl:text>
		<xsl:text>Insurer,</xsl:text>
		<xsl:text>Insurer number,</xsl:text>
		<xsl:text>Original date,</xsl:text>
		<xsl:text>Original amount,</xsl:text>
		<xsl:text>Property appraisal,</xsl:text>
		<xsl:text>Property address line 1,</xsl:text>
		<xsl:text>Property address line 2,</xsl:text>
		<xsl:text>Property address line 3,</xsl:text>
		<xsl:text>Property postal code,</xsl:text>
		<!--col: 10 -->
		<xsl:text>Borrower first name 1,</xsl:text>
		<xsl:text>Borrower last name 1,</xsl:text>
		<xsl:text>Borrower first name 2,</xsl:text>
		<xsl:text>Borrower last name 2,</xsl:text>
		<xsl:text>Interest rate,</xsl:text>
		<xsl:text>Remaining term according to payment frequency,</xsl:text>
		<xsl:text>Payment frequency,</xsl:text>
		<xsl:text>Principal-interest payment,</xsl:text>
		<xsl:text>Principal balance on transaction date,</xsl:text>
		<xsl:text>Date of next payment,</xsl:text>
		<!--col: 20 -->
		<xsl:text>Present value,</xsl:text>
		<xsl:text>Property province code,</xsl:text>
		<xsl:text>Phone correspondence name,</xsl:text>
		<xsl:text>Home phone number,</xsl:text>
		<xsl:text>Office phone number,</xsl:text>
		<xsl:text>Mailing correspondence language,</xsl:text>
		<xsl:text>Mailing correspondence name(s),</xsl:text>
		<xsl:text>Correspondence address line 1,</xsl:text>
		<xsl:text>Correspondence address line 2,</xsl:text>
		<xsl:text>Correspondence address line 3,</xsl:text>
		<!--col: 30 -->
		<xsl:text>Correspondence address line 4,</xsl:text>
		<xsl:text>Correspondence  postal code,</xsl:text>
		<xsl:text>Correspondence province code,</xsl:text>
		<xsl:text>Tax payment,</xsl:text>
		<xsl:text>Tax reserve at the sale transaction,</xsl:text>
		<xsl:text>Accrue interest on tax reserve at the sale transaction,</xsl:text>
		<xsl:text>Legal Description,</xsl:text>
		<xsl:text>Bank code,</xsl:text>
		<xsl:text>Transit number (branch),</xsl:text>
		<xsl:text>Account number,</xsl:text>
		<!--col:40 -->
		<xsl:text>Type of account,</xsl:text>
		<xsl:text>Signatory’s name,</xsl:text>
		<xsl:text>Matching funds number IA,</xsl:text>
		<xsl:text>Term type,</xsl:text>
		<xsl:text>Product interest rate base point,</xsl:text>
		<xsl:text>Adjustment interest rate base point,</xsl:text>
		<xsl:text>Mortgage deed number,</xsl:text>
		<xsl:text>Registration mortgage deed date,</xsl:text>
		<xsl:text>Signature mortgage deed date,</xsl:text>
		<xsl:text>"Registration number, notice, address"</xsl:text>
		<xsl:text>,</xsl:text>
		<!--col:50 comma removed in-btw -->
		<xsl:text>Expiry date mortgage insurance,</xsl:text>
		<xsl:text>Expiry date for fire insurance,</xsl:text>
		<xsl:text>Expiry date boiler insurance,</xsl:text>
		<xsl:text>Expiry date rental insurance,</xsl:text>
		<xsl:text>Expiry date collateral insurance,</xsl:text>
		<xsl:text>Expiry date lease insurance,</xsl:text>
		<xsl:text>Guarantor code,</xsl:text>
		<xsl:text>Property type,</xsl:text>
		<xsl:text>Number of units,</xsl:text>
		<xsl:text>Type of use,</xsl:text>
		<!--col: 60 -->
		<xsl:text>Construction year,</xsl:text>
		<xsl:text>Year at which the property was last renovated,</xsl:text>
		<xsl:text>Year acquired,</xsl:text>
		<xsl:text>Acquisition cost,</xsl:text>
		<xsl:text>Number of properties,</xsl:text>
		<xsl:text>Condominium code,</xsl:text>
		<xsl:text>Date last appraisal,</xsl:text>
		<xsl:text>Building value,</xsl:text>
		<xsl:text>Social insurance number,</xsl:text>
		<xsl:text>Prepayment by anticipation code,</xsl:text>
		<!--col: 70 -->
		<xsl:text>Broker code,</xsl:text>
		<xsl:text>Broker name,</xsl:text>
		<xsl:text>Source Application ID,</xsl:text>
		<xsl:text>Name of municipality where taxes are paid,</xsl:text>
		<xsl:text>Tax roll number,</xsl:text>
		<xsl:text>Lender Profile ID</xsl:text>
		<!-- col:76 -->
	</xsl:template>
	
    <!-- ************************************************************************ 	-->
	<!-- Generate the content                                                           	   	-->
	<!-- ************************************************************************ 	-->	
	<xsl:template name="content">
		<!-- ********************** col:1 **********************  -->
		<xsl:variable name="dealId" select="//Deal/dealId"/>		
		<xsl:if test="string-length($dealId)=0">
			<xsl:value-of select="0"/>		
		</xsl:if>
		<xsl:value-of select="$dealId"/>
		<xsl:text>,</xsl:text>
		<!-- ********************** col: 2 **********************  -->
		<!--xsl:variable name="MIid" select="//Deal/mortgageInsurerId"/-->
		<xsl:call-template name="condionalPop">
			<xsl:with-param name="orig_val" select="//Deal/mortgageInsurerId"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:3 **********************  -->
		<xsl:variable name="MIPN" select="//Deal/MIPolicyNumber"/>
		<!--xsl:value-of select="$MIPN"/-->
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select ="10"/> 
			<xsl:with-param name="orig_str" select="$MIPN"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:4 **********************  -->
		<xsl:call-template name="formatDate">
			<xsl:with-param name="date" select="//Deal/actualClosingDate"/>
			<xsl:with-param name="shift" select="true()"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:5 **********************  -->
		<xsl:variable name="tla" select="translate(//Deal/totalLoanAmount, '$, ', '')"/>		
		<xsl:if test="string-length($tla)=0">
			<xsl:value-of select="0"/>
		</xsl:if>
		<xsl:value-of select="format-number($tla, '#########.##')"/>		
		<!--xsl:value-of select="$tla"/-->	
		<xsl:text>,</xsl:text>
		<!-- ********************** col:6 **********************  -->
		<xsl:variable name="tav" select="translate(//Deal/totalActAppraisedValue, '$, ', '')"/>		
		<xsl:if test="string-length($tav)=0">
			<xsl:value-of select="format-number(0, '#########.##')"/>
		</xsl:if>
		<xsl:value-of select="format-number($tav, '#########.##')"/>		
		<xsl:text>,</xsl:text>
		<!-- ********************** col:7 **********************  -->
	
		<xsl:variable name="pal1">
			<xsl:variable name="tempAddr" select="normalize-space(concat($pun,'-',$psn,' ',$psnm,' ',$pst,' ',$psd))"/>
			<xsl:choose>
				<xsl:when test="starts-with($tempAddr, '-')">
					<xsl:value-of select="substring-after($tempAddr, '-')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$tempAddr"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="30"/>
			<xsl:with-param name="orig_str" select="$pal1"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:8 **********************  -->
		<xsl:call-template name="outputString">
				<xsl:with-param name="length" select="30"/>
				<xsl:with-param name="orig_str" select="$pcity"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:9 **********************  -->
		<xsl:call-template name="outputString">
				<xsl:with-param name="length" select="25"/>
				<xsl:with-param name="orig_str" select="$pprovince"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:10 **********************  -->
		<xsl:value-of select="$pFSA"/>
		<xsl:value-of select="$pLDU"/>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:11 **********************  -->
		<xsl:variable name="borrower1_fn" select="//Deal/Borrower[borrowerNumber=1]/borrowerFirstName"/>
			<xsl:call-template name="outputString">
				<xsl:with-param name="length" select="20"/>
				<xsl:with-param name="orig_str" select="$borrower1_fn"/>
			</xsl:call-template>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:12 **********************  -->
		<xsl:variable name="borrower1_ln" select="//Deal/Borrower[borrowerNumber=1]/borrowerLastName"/>
			<xsl:call-template name="outputString">
				<xsl:with-param name="length" select="20"/>
				<xsl:with-param name="orig_str" select="$borrower1_ln"/>
			</xsl:call-template>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:13 **********************  -->
		<xsl:variable name="borrower2_fn" select="//Deal/Borrower[borrowerNumber=2]/borrowerFirstName"/>
			<xsl:call-template name="outputString">
				<xsl:with-param name="length" select="20"/>
				<xsl:with-param name="orig_str" select="$borrower2_fn"/>
			</xsl:call-template>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:14 **********************  -->
		<xsl:variable name="borrower2_ln" select="//Deal/Borrower[borrowerNumber=2]/borrowerLastName"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="20"/>
			<xsl:with-param name="orig_str" select="$borrower2_ln"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:15 **********************  -->
		<xsl:variable name="netIRate" select="translate(//Deal/netInterestRate, '%,', '')"/>				
		<xsl:choose>
			<xsl:when test="string-length($netIRate)=0">
				<xsl:value-of select="0"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="format-number($netIRate, '##.###')"/>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:16 **********************  -->
		<xsl:variable name="apt" select="//Deal/actualPaymentTerm"/>		
		<xsl:choose>
			<xsl:when test="string-length($apt)=0">
				<xsl:value-of select="0"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$apt"/>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:17 **********************  -->
		<xsl:variable name="pfId" select="//Deal/paymentFrequencyId"/>		
		<xsl:choose>
			<xsl:when test="$pfId = 0">
				<xsl:text>12</xsl:text>
			</xsl:when>
			<xsl:when test="$pfId = 1 or $pfId = 2 or $pfId = 3">
				<xsl:text>26</xsl:text>
			</xsl:when>
			<xsl:when test="$pfId = 4 or $pfId = 5">
				<xsl:text>52</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>""</xsl:text>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:18 **********************  -->
		<xsl:variable name="pipa" select="translate(//Deal/PandiPaymentAmount, '$, ', '')"/>				
		<xsl:choose>
			<xsl:when test="string-length($pipa)=0">
				<xsl:value-of select="format-number(0, '#########.##')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="format-number($pipa, '#########.##')"/>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:text>,</xsl:text>					
		<!-- ********************** col:19 **********************  -->
		<xsl:variable name="tlat" select="translate(//Deal/totalLoanAmount, '$, ', '')"/>				
		<xsl:choose>
			<xsl:when test="string-length($tlat)=0">
				<xsl:value-of select="format-number(0, '#########.##')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="format-number($tlat, '#########.##')"/>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:text>,</xsl:text>					
		<!-- ********************** col:20 **********************  -->
		<xsl:choose>
			<xsl:when test="starts-with(normalize-space(translate(//Deal/paymentFrequency, 'MONTHLY', 'monthly')), 'monthly')">
				<xsl:call-template name="formatDate">
					<xsl:with-param name="date"	select="//Deal/firstPaymentDate"/>
					<xsl:with-param name="shift" select="true()"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="formatDate">
					<xsl:with-param name="date"	select="//Deal/firstPaymentDate"/>
					<xsl:with-param name="shift" select="false()"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:21 **********************  -->		
		<xsl:value-of select="format-number(0, '#########.##')"/>		
		<xsl:text>,</xsl:text>
		<!-- ********************** col:22 **********************  -->
		<xsl:value-of select="$pprovinceAbbr"/>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:23 **********************  -->
		<xsl:variable name="borrowerName" select="concat(//Deal/Borrower[borrowerNumber=1]/borrowerFirstName,' ', //Deal/Borrower[borrowerNumber=1]/borrowerLastName)"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="20"/>
			<xsl:with-param name="orig_str" select="$borrowerName"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:24 **********************  -->
		<xsl:variable name="homePhone" select="//Deal/Borrower[borrowerNumber=1]/borrowerHomePhoneNumber"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="10"/>
			<xsl:with-param name="orig_str" select="$homePhone"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:25 **********************  -->
		<xsl:variable name="workPhone" select="//Deal/Borrower[borrowerNumber=1]/borrowerWorkPhoneNumber"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="10"/>
			<xsl:with-param name="orig_str" select="$workPhone"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:26 **********************  -->
		<xsl:variable name="languageId" select="//Deal/Borrower[borrowerNumber=1]/languagePreferenceId"/>
		<xsl:choose>
			<xsl:when test="$languageId=0">
				<xsl:text>"A"</xsl:text>
			</xsl:when>
			<xsl:when test="$languageId=1">
				<xsl:text>"F"</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>""</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:27 **********************  -->
		<xsl:variable name="borrowerNameMail" select="normalize-space(concat(//Deal/Borrower[borrowerNumber=1]/borrowerFirstName,' ', //Deal/Borrower[borrowerNumber=1]/borrowerLastName))"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="40"/>
			<xsl:with-param name="orig_str" select="$borrowerNameMail"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:28 **********************  -->
		<xsl:variable name="occType" select="//Deal/Property/occupancyTypeId"/>
		<xsl:choose>
			<xsl:when test="$occType=0">
			<xsl:text></xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="contains(translate($curBorrAddrLine1, 'CO','co'), 'c/o')">
					<xsl:call-template name="outputString">
						<xsl:with-param name="length" select="40"/>
						<xsl:with-param name="orig_str" select="$curBorrAddrLine1"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:29 **********************  -->
		<xsl:variable name="occType2" select="//Deal/Property/occupancyTypeId"/>
		<xsl:choose>
			<xsl:when test="$occType2=0">
				<xsl:variable name="corrAddrLine2">
					<xsl:variable name="tempAddr" select="normalize-space(concat($pun,'-',$psn,' ',$psnm,' ',$pst,' ',$psd))"/>
					<xsl:choose>
						<xsl:when test="starts-with($tempAddr, '-')">
							<xsl:value-of select="substring-after($tempAddr, '-')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$tempAddr"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:call-template name="outputString">
					<xsl:with-param name="length" select="30"/>
					<xsl:with-param name="orig_str" select="$corrAddrLine2"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="contains(translate($curBorrAddrLine1, 'CO','co'), 'c/o')">
						<xsl:call-template name="outputString">
							<xsl:with-param name="length" select="30"/>
							<xsl:with-param name="orig_str" select="//Deal/Borrower[borrowerNumber=1]/BorrowerAddress[borrowerAddressTypeId=0]/Addr/addressLine2"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="outputString">
							<xsl:with-param name="length" select="30"/>
							<xsl:with-param name="orig_str" select="$curBorrAddrLine1"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>				
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:30 **********************  -->
		<xsl:variable name="occType3" select="//Deal/Property/occupancyTypeId"/>
		<xsl:choose>
			<xsl:when test="$occType3=0">
				<xsl:call-template name="outputString">
					<xsl:with-param name="length" select="30"/>
					<xsl:with-param name="orig_str" select="$pcity"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputString">
					<xsl:with-param name="length" select="30"/>
					<xsl:with-param name="orig_str" select="$curBorrCity"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:31 **********************  -->
		<xsl:variable name="occType4" select="//Deal/Property/occupancyTypeId"/>
		<xsl:choose>
			<xsl:when test="$occType4=0">
				<xsl:call-template name="outputString">
					<xsl:with-param name="length" select="25"/>
					<xsl:with-param name="orig_str" select="$pprovince"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputString">
					<xsl:with-param name="length" select="25"/>
					<xsl:with-param name="orig_str" select="//Deal/Borrower[borrowerNumber=1]/BorrowerAddress[borrowerAddressTypeId=0]/Addr/province"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:32 **********************  -->
		<xsl:choose>
			<xsl:when test="$occType=0">
				<xsl:variable name="postalCode" select="normalize-space(concat($pFSA,$pLDU))"/>
				<xsl:call-template name="outputString">
					<xsl:with-param name="length" select="6"/>
					<xsl:with-param name="orig_str" select="$postalCode"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="currAddrPostalCode" select="normalize-space(concat($curBorrFSA,$curBorrLDU))"/>
				<xsl:call-template name="outputString">
					<xsl:with-param name="length" select="6"/>
					<xsl:with-param name="orig_str" select="$currAddrPostalCode"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:33 **********************  -->		
			<xsl:choose>
			<xsl:when test="$occType=0">
				<xsl:value-of select="$pprovinceAbbr"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$curBorrProvAbbr"/>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:text>,</xsl:text>
		<!-- ********************** col:34 **********************  -->		
		<xsl:variable name="escrowPay" select="translate(//Deal/escrowPaymentAmount, '$, ', '')"/>		
		<xsl:choose>
			<xsl:when test="$escrowPay &lt; 0">
				<xsl:value-of select="format-number($escrowPay,'########.##')"/>
			</xsl:when>
			<xsl:when test="$escrowPay &gt;= 0">
				<xsl:value-of select="format-number($escrowPay,'#########.##')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="0"/>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:35 **********************  -->		
		<xsl:value-of select="0"/>		
		<xsl:text>,</xsl:text>
		<!-- ********************** col:36 **********************  -->		
		<xsl:value-of select="0"/>		
		<xsl:text>,</xsl:text>
		<!-- ********************** col:37 **********************  -->
		<xsl:variable name="legalDes" select="//Deal/Property/legalLine1"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="40"/>
			<xsl:with-param name="orig_str" select="$legalDes"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:38 **********************  -->
		<xsl:variable name="bankCode" select="//Deal/bankABANumber"/>
		<xsl:variable name="firstPart" select="substring-before($bankCode, '-')"/>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="$firstPart"/>
		<xsl:text>"</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:39 **********************  -->
		<xsl:variable name="secondPart" select="substring-after($bankCode, '-')"/>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="substring($secondPart,1,5)"/>
		<xsl:text>"</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:40 **********************  -->		
		<xsl:value-of select="//Deal/bankAccountNumber"/>		
		<xsl:text>,</xsl:text>
		<!-- ********************** col:41**********************  -->
		<xsl:text>"1"</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:42 **********************  -->
		<xsl:variable name="signatory" select="normalize-space(concat(//Deal/Borrower[borrowerNumber=1]/borrowerLastName, ' ', //Deal/Borrower[borrowerNumber=1]/borrowerFirstName))"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="38"/>
			<xsl:with-param name="orig_str" select="$signatory"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
 		<!-- ********************** col:43 **********************  -->
 		<xsl:text>1401</xsl:text>
 		<xsl:text>,</xsl:text>
		<!-- ********************** col:44 **********************  -->
		<xsl:variable name="MtgProdID" select="//Deal/mtgProdId"/>
		<xsl:choose>
			<xsl:when test="$MtgProdID=5 or $MtgProdID=6 or $MtgProdID=7 or $MtgProdID=8 or $MtgProdID=9 or $MtgProdID=10 or $MtgProdID=11">
				<xsl:text>"F"</xsl:text>
			</xsl:when>
			<xsl:when test="$MtgProdID=12">
				<xsl:text>"W"</xsl:text>
			</xsl:when>
			<xsl:when test="$MtgProdID=13">
				<xsl:text>"D"</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>""</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:45 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:46 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:47 **********************  -->
		<xsl:text>""</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:48 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:49 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:50 **********************  -->
		<xsl:text>""</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:51 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:52 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:53 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:54 **********************  -->
		<xsl:text>0</xsl:text>
			<xsl:text>,</xsl:text>
		<!-- ********************** col:55 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:56 **********************  -->
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:57 **********************  -->
		<xsl:variable name="numOfGuar" select="//Deal/numberOfGuarantors"/>
		<xsl:choose>
			<xsl:when test="$numOfGuar &gt; 0">
				<xsl:text>"O"</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>"N"</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:58 **********************  -->
		<xsl:variable name="propTypeId" select="//Deal/Property/propertyTypeId"/>
		<xsl:call-template name="getPropType">
			<xsl:with-param name="propType" select="$propTypeId"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:59 **********************  -->
		<xsl:choose>
			<xsl:when test="$propTypeId=1">
				<xsl:value-of select="//Deal/Property/numberOfUnits"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>1</xsl:text>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:text>,</xsl:text>		
		<!-- ********************** col:60 **********************  -->
		<xsl:variable name="occupancyTID" select="//Deal/Property/occupancyTypeId"/>		
			<xsl:choose>
				<xsl:when test="$occupancyTID=1 or $occupancyTID=2">
					<xsl:text>"L"</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>"O"</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		<xsl:text>,</xsl:text>			
		<!-- ********************** col:61 **********************  -->		
		<xsl:text>0</xsl:text>	
		<xsl:text>,</xsl:text>
		<!-- ********************** col:62 **********************  -->		
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:63 **********************  -->		
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:64 **********************  -->		
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:65 **********************  -->		
		<xsl:text>1,</xsl:text><!--#DG462 -->
		<!-- ********************** col:66 **********************  -->
		<xsl:variable name="propertyTID" select="//Deal/Property/propertyTypeId"/>		
			<xsl:choose>
				<xsl:when test="$propertyTID=2 or $propertyTID=14 or $propertyTID=15 or $propertyTID=16">
					<xsl:text>"O"</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>"N"</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		<xsl:text>,</xsl:text>	
		<!-- ********************** col:67 **********************  -->	
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:68 **********************  -->		
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:69 **********************  -->		
		<xsl:text>0</xsl:text>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:70 **********************  -->
		<xsl:variable name="MtgPID" select="//Deal/mtgProdId"/> 
			<xsl:choose>
				<xsl:when test="$MtgPID=11">    <!--*****5 years no frills **** -->
					<xsl:text>"C"</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>"W"</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		<xsl:text>,</xsl:text>	
		<!-- ********************** col:71 **********************  -->
		<xsl:variable name="brokerCode" select="//Deal/SourceOfBusinessProfile/sourceOfBusinessCode"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="11"/>
			<xsl:with-param name="orig_str" select="$brokerCode"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:72 **********************  -->
		<xsl:variable name="brokerName" select="normalize-space(concat(//Deal/SourceOfBusinessProfile/Contact/contactFirstName,' ',//Deal/SourceOfBusinessProfile/Contact/contactLastName))"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="50"/>
			<xsl:with-param name="orig_str" select="$brokerName"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:73 **********************  -->
		<xsl:variable name="sourceAID" select="//Deal/sourceApplicationId"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="16"/>
			<xsl:with-param name="orig_str" select="$sourceAID"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:74 **********************  -->
		<xsl:variable name="partyName" select="//Deal/PartyProfile[partyTypeId=1]/partyName"/>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="30"/>
			<xsl:with-param name="orig_str" select="$partyName"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:75 **********************  -->                                                                                 
		<xsl:variable name="taxRollNum">
			<xsl:call-template name="numberExtractor">
				<xsl:with-param name="mixedString" select="normalize-space(//Deal/Property/PropertyExpense[propertyExpenseTypeId=0]/propertyExpenseDescription)"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:call-template name="outputString">
			<xsl:with-param name="length" select="15"/>
			<xsl:with-param name="orig_str" select="$taxRollNum"/>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<!-- ********************** col:76 **********************  -->
		<xsl:value-of select="//Deal/lenderProfileId"/>				
			
	</xsl:template>
		
		
	<!-- ************************************** Fuction that selectively output text *******************************-->
	<xsl:template name="condionalPop">
		<xsl:param name="orig_val"/>
		<xsl:choose>
			<xsl:when test="$orig_val= 0">
				<xsl:text>"NO"</xsl:text>
			</xsl:when>
			<xsl:when test="$orig_val= 1">
				<xsl:text>"CMHC"</xsl:text>
			</xsl:when>
			<xsl:when test="$orig_val= 2">
				<xsl:text>"GE"</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>""</xsl:text>
			</xsl:otherwise>			
		</xsl:choose>		
	</xsl:template>
	<!-- ******************************* Fuction emitting zeros ****************************************************-->
	<xsl:template name="outputZeros">
		<xsl:param name="zeros"/>
		<xsl:if test="$zeros &gt; 0">
			<xsl:text>0</xsl:text>
			<xsl:call-template name="outputZeros">
				<xsl:with-param name="length" select="$zeros -1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- ******************************* Fuction that output formatted string *************************************-->
	<xsl:template name="outputString">
		<xsl:param name="length"/>
		<xsl:param name="orig_str"/>		
		<xsl:variable name="numOfSpaces" select="$length - string-length($orig_str)"/>
		<xsl:choose>
			<xsl:when test="$numOfSpaces &lt;= 0">
				<xsl:text>"</xsl:text>
				<xsl:value-of select="translate(substring($orig_str, 1, $length),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
				<xsl:text>"</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>"</xsl:text>
				<xsl:value-of select="translate($orig_str,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
				<xsl:text>"</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- ************************************* Fuction that parses and transates month info **********************************-->
	<xsl:template name="parseMonth">
		<xsl:param name="p1"/>
		<xsl:param name="p2"/>
		
		<xsl:choose>
			<xsl:when test="string-length($p1) &lt;= 2">  <!--p2 contains month info-->
				<xsl:call-template name="getMonth">
					<xsl:with-param name="mon_abbr" select="$p2"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>       <!--p1 contains month info-->
				<xsl:call-template name="getMonth">
					<xsl:with-param name="mon_abbr" select="$p1"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<!-- ************************************* Fuction that parses and transates day info **********************************-->
	<xsl:template name="parseDay">
		<xsl:param name="p1"/>
		<xsl:param name="p2"/>
		
		<xsl:choose>
			<xsl:when test="string-length($p1) &lt;= 2">  <!--p1 contains day info-->
				<xsl:value-of select="$p1"/>
			</xsl:when>
			<xsl:otherwise>       <!--p1 contains month info-->
				<xsl:value-of select="$p2"/>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
		
	<!-- ************************************* Fuction that format and ouput date as is**********************************-->
	<xsl:template name="outputDateNormal">
		<xsl:param name="year"/>
		<xsl:param name="month"/>
		<xsl:param name="day"/>
		
		<xsl:value-of select="$year"/>
		<xsl:value-of select="$month"/>
		<xsl:call-template name="outputZeros">
			<xsl:with-param name="zeros" select="2-string-length($day)"/>
		</xsl:call-template>
		<xsl:value-of select="normalize-space($day)"/>
		
	</xsl:template>
	
	<!-- ************************************* Fuction that ormat and ouput date shifted**********************************-->
	<xsl:template name="outputDateShift">
		<xsl:param name="year1"/>
		<xsl:param name="month1"/>
		<xsl:param name="day1"/>
		<xsl:choose>
			<xsl:when test="starts-with($day1, '29') or starts-with($day1, '30') or starts-with($day1, '31')">
				<xsl:choose>
					<xsl:when test="starts-with($month1, '12')">
						<xsl:value-of select="$year1+1"/>
						<xsl:text>0</xsl:text>
						<xsl:value-of select="1"/> <!-- Test This -->
						<xsl:text>0</xsl:text>
						<xsl:value-of select="1"/> <!-- Test This -->
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$year1"/>
						<xsl:call-template name="outputZeros">
							<xsl:with-param name="zeros" select="2-string-length($month1+1)"/>
						</xsl:call-template>
						<xsl:value-of select="$month1+1"/> <!-- Test This -->
						<xsl:text>0</xsl:text>
						<xsl:value-of select="1"/> <!-- Test This -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputDateNormal">
					<xsl:with-param name="year" select="$year1"/>
					<xsl:with-param name="month" select="$month1"/>
					<xsl:with-param name="day" select="$day1"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	

	<!-- **************************************** Fuction that format the date *************************************-->
	<xsl:template name="formatDate">
		<xsl:param name="date"/>
		<xsl:param name="shift"/>
		
		<xsl:variable name="normalized_date" select="normalize-space(translate($date, ',.-/', ' '))"/>
		<xsl:variable name="part1" select="substring-before($normalized_date,' ')"/>
		<xsl:variable name="remaining" select="substring-after($normalized_date,' ')"/>
		<xsl:variable name="part2" select="substring-before($remaining,' ')"/>
		<xsl:variable name="yr" select="substring-after($remaining,' ')"/>
		
		<xsl:variable name="mon">
			<xsl:call-template name="parseMonth">
				<xsl:with-param name="p1" select="$part1"/>
				<xsl:with-param name= "p2" select="$part2"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name="dy">
			<xsl:call-template name="parseDay">
				<xsl:with-param name="p1" select="$part1"/>
				<xsl:with-param name="p2" select="$part2"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$shift = false()">
				<xsl:call-template name="outputDateNormal">
					<xsl:with-param name="year" select="$yr"/>
					<xsl:with-param name="month" select="$mon"/>
					<xsl:with-param name="day" select="$dy"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="outputDateShift">
					<xsl:with-param name="year1" select="$yr"/>
					<xsl:with-param name="month1" select="$mon"/>
					<xsl:with-param name="day1" select="$dy"/>
				</xsl:call-template>
			</xsl:otherwise>	
		</xsl:choose>
	</xsl:template>
	
	<!-- ************** Fuction that accepts month abbriviation and output month in mumber****************-->
	<xsl:template name="getMonth">
		<xsl:param name="mon_abbr"/>
		<xsl:choose>
			<xsl:when test="starts-with(translate($mon_abbr,'JAN','jan'), 'jan')">
				<xsl:text>01</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr, 'FEB','feb'), 'feb')">
				<xsl:text>02</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'MAR', 'mar'), 'mar')">
				<xsl:text>03</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'APR','apr'), 'apr')">
				<xsl:text>04</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'MAY','may'), 'may')">
				<xsl:text>05</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'JUN','jun'), 'jun')">
				<xsl:text>06</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'JUL','jul'), 'jul')">
				<xsl:text>07</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'AUG','aug'), 'aug')">
				<xsl:text>08</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'SEP','sep'), 'sep')">
				<xsl:text>09</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'OCT','oct'), 'oct')">
				<xsl:text>10</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'NOV','nov'), 'nov')">
				<xsl:text>11</xsl:text>
			</xsl:when>
			<xsl:when test="starts-with(translate($mon_abbr,'DEC','dec'), 'dec')">
				<xsl:text>12</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>""</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- ************************************ Fuction that get Province Abbreviation******************************-->
	<xsl:template name="getProvinceAbbr">
		<xsl:param name="provinceId"/>
		<xsl:choose>
			<xsl:when test="$provinceId=0">
				<xsl:text>"OT"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=1">
				<xsl:text>"AB"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=2">
				<xsl:text>"BC"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=3">
				<xsl:text>"MB"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=4">
				<xsl:text>"NB"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=5">
				<xsl:text>"NF"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=6">
				<xsl:text>"NT"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=7">
				<xsl:text>"NS"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=8">
				<xsl:text>"NU"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=9">
				<xsl:text>"ON"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=10">
				<xsl:text>"PE"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=11">
				<xsl:text>"QC"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=12">
				<xsl:text>"SK"</xsl:text>
			</xsl:when>
			<xsl:when test="$provinceId=13">
				<xsl:text>"YT"</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>""</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
		
    <!-- ************************************ Fuction that gets the Property Type*********************************-->
	<xsl:template name="getPropType">
		<xsl:param name="propType"/>
		<xsl:choose>
			<xsl:when test="$propType=0 or $propType=6">
				<xsl:text>"1"</xsl:text>
			</xsl:when>
			<xsl:when test="$propType=1 and //Deal/Property/numberOfUnits &lt; 10">
				<xsl:text>"1"</xsl:text>
			</xsl:when>
			<xsl:when test="$propType=1 and //Deal/Property/numberOfUnits &gt;= 10">
				<xsl:text>"12"</xsl:text>
			</xsl:when>
			<xsl:when test="$propType=2 or $propType=14 or $propType=15 or $propType=16 or $propType=17">
				<xsl:text>"2"</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>""</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	 <!-- ************************************ Fuction that extracts only numbers from a String *********************************-->
	<xsl:template name="numberExtractor">
	<xsl:param name="mixedString"/>
	<xsl:choose>
		<xsl:when test="string-length($mixedString) &lt;=1 ">
			<xsl:if test="starts-with($mixedString,'1') or starts-with($mixedString,'2') or starts-with($mixedString,'3') or starts-with($mixedString,'4') or starts-with($mixedString,'5') or starts-with($mixedString,'6') or starts-with($mixedString,'7') or starts-with($mixedString,'8') or starts-with($mixedString,'9') or starts-with($mixedString,'0')">
				<xsl:value-of select="$mixedString"/>
			</xsl:if>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="remaining">
				<xsl:call-template name="numberExtractor">
					<xsl:with-param name="mixedString" select="substring($mixedString,2)"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>				
				<xsl:when test="starts-with($mixedString,'1') or starts-with($mixedString,'2') or starts-with($mixedString,'3') or starts-with($mixedString,'4') or starts-with($mixedString,'5') or starts-with($mixedString,'6') or starts-with($mixedString,'7') or starts-with($mixedString,'8') or starts-with($mixedString,'9') or starts-with($mixedString,'0')">					
					<xsl:value-of select="concat(substring($mixedString,1,1), $remaining)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$remaining"/>
				</xsl:otherwise>			
		</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
	
</xsl:stylesheet>
