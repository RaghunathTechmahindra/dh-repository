<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageEnglish">
				<xsl:call-template name="EnglishPage1"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageFrench">
				<!--				<xsl:call-template name="FrenchTemplate"/>-->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="FOEnd"/>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishPage1">
		<xsl:text>
	&lt;fo:page-sequence master-reference="main"&gt;
		&lt;fo:static-content flow-name="xsl-region-after"&gt;
			&lt;fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="right"&gt;Page &lt;fo:page-number/&gt;
 of &lt;fo:page-number-citation ref-id="endofdoc"/&gt;
			&lt;/fo:block&gt;
		&lt;/fo:static-content&gt;
		&lt;fo:flow flow-name="xsl-region-body"&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="14.0pt" font-weight="bold"&gt;MORTGAGE COMMITMENT&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="23.4pt"/&gt;
				&lt;fo:table-column column-width="63.0pt"/&gt;
				&lt;fo:table-column column-width="61.55pt"/&gt;
				&lt;fo:table-column column-width="35.6pt"/&gt;
				&lt;fo:table-column column-width="63.15pt"/&gt;
				&lt;fo:table-column column-width="120.45pt"/&gt;
				&lt;fo:table-column column-width="12.0pt"/&gt;
				&lt;fo:table-column column-width="13.25pt"/&gt;
				&lt;fo:table-column column-width="18.7pt"/&gt;
				&lt;fo:table-column column-width="139.7pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" display-align="center" number-columns-spanned="5" number-rows-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
							<!--  Change this to be the relative path (from the root).  The drive letter isn't needed -->
								&lt;fo:external-graphic src="\mos_docprep\admin\docgen\templates\basis.gif"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/><xsl:text>&lt;/fo:block&gt;</xsl:text>

						<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
						</xsl:if>
						<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/City"/><xsl:text>, </xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;Tel: </xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;Fax: </xsl:text><xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;Underwriter&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/Underwriter"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;Date&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/CurrentDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row height="25.9pt"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" number-rows-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;</xsl:text>
							
					<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
						<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text>
					</xsl:for-each>
							
					<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
							
					<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
						<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
					</xsl:if>
					<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/ClientAddress/City"/><xsl:text>, </xsl:text><xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;Mortgage Reference Number:&lt;fo:inline font-weight="normal"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row height="25.85pt"&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;Deal Number:&lt;fo:inline font-weight="normal"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/DealNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="10" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-style="italic"&gt;We are pleased to confirm that your application for a Mortgage Loan has been approved under the following terms and conditions:&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row height="10.5pt"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;MORTGAGOR(S):&lt;fo:inline font-weight="normal"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;</xsl:text>

						<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text>
						</xsl:for-each>

						<xsl:text>
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" number-rows-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;PURPOSE: &lt;fo:inline font-weight="normal"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/DealPurpose"/><xsl:text>&lt;/fo:inline&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row height="10.5pt"&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/GuarantorClause"/><xsl:text>(s):&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;</xsl:text>

						<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
							<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text>
						</xsl:for-each>
						
						<xsl:text>
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="5"&gt;
							&lt;fo:block space-before="6pt" font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold" text-decoration="underline"&gt;SECURITY ADDRESS:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block space-before="6pt" font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold" text-decoration="underline"&gt;LEGAL DESCRIPTION:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
					
					<xsl:for-each select="//CommitmentLetter/Properties/Property">
					<xsl:text>
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./AddressLine1"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
							
							<xsl:if test="./AddressLine2">
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./AddressLine2"/><xsl:text>&lt;/fo:block&gt;</xsl:text>									</xsl:if>
							
							<xsl:text>
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./City"/><xsl:text>, </xsl:text><xsl:value-of select="./Province"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./PostalCode"/><xsl:text>&lt;/fo:block&gt;

							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;</xsl:text>

							<xsl:if test="./LegalLine1">
								<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./LegalLine1"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
							</xsl:if>
			
							<xsl:if test="./LegalLine2">
								<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./LegalLine2"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
							</xsl:if>
			
							<xsl:if test="./LegalLine3">
								<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./LegalLine3"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
							</xsl:if>

						<xsl:text>
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					</xsl:text>
					</xsl:for-each>

					<xsl:text>
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;COMMITMENT DATE:&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;INTEREST ADJUSTMENT DATE:&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/IADDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold" text-align="center"&gt;COMMITMENT EXPIRY DATE:&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="center"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/CommitmentExpiryDate"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="center"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold" text-align="center"&gt;FIRST PAYMENT DATE:&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="center"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold" text-align="center"&gt;ADVANCE DATE:&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="center"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/AdvanceDate"/><xsl:text>&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="center"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold" text-align="center"&gt;MATURITY DATE:&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="center"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/MaturityDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;LOAN TYPE:&lt;fo:inline font-weight="normal"&gt;  </xsl:text><xsl:value-of select="//CommitmentLetter/Product"/><xsl:text>&lt;/fo:inline&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;LTV:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/LTV"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;BASIC LOAN AMOUNT:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/LoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;INTEREST RATE:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/InterestRate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;MTG INSURANCE PREMIUM:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/Premium"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;TERM:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/Term"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;TOTAL LOAN AMOUNT:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/TotalAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;AMORTIZATION PERIOD:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/Amortization"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;BASIC PAYMENT AMOUNT:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/TotalPayment"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;PAYMENT FREQUENCY:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/PaymentFrequency"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;EST. ANNUAL PROP. TAXES:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/AnnualTaxes"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;TAXES TO BE PAID BY:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/TaxPayor"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;PST PAYABLE ON INSURANCE PREMIUM:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/PST"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;MORTGAGE FEES:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
					
					<xsl:for-each select="//CommitmentLetter/Fees/Fee">
					<xsl:text>
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;
								&lt;fo:leader leader-length="9.0pt" leader-pattern="space"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-style="none" border-top-style="none" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="./FeeVerb"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./FeeAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					</xsl:text>
					</xsl:for-each>
					
					<xsl:text>
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="10" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block space-before= "6pt" font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;In this approval and any schedule(s) to this approval, &lt;fo:inline font-weight="bold"&gt;you and your&lt;/fo:inline&gt; mean the Borrower, Co-Borrower and </xsl:text><xsl:value-of select="//CommitmentLetter/GuarantorClause"/><xsl:text>, if any, and &lt;fo:inline font-weight="bold"&gt;we, our and us &lt;/fo:inline&gt; mean </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text> &lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;All of our normal requirements and, if applicable, those of the mortgage insurer must be met. All costs, including legal, survey, mortgage insurance etc. are for the account of the applicant(s). The mortgage insurance premium (if applicable) will be added to the mortgage. Any fees specified herein may be deducted from the Mortgage advance. If for any reason the loan is not advanced, you agree to pay all application, legal, appraisal and survey costs incurred in this transaction.&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;This Mortgage Commitment is subject to the details and terms outlined herein as well as the conditions described on the attached schedules.&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than </xsl:text><xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/><xsl:text> after which time if not accepted, shall be considered null and void.&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;Thank you for choosing </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text> for your financing.&lt;/fo:block&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
								&lt;fo:leader line-height="9pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
				&lt;fo:leader line-height="9pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;
				&lt;fo:leader line-height="9pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:block break-before="page" font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;
				&lt;fo:table table-layout="fixed"&gt;
					&lt;fo:table-column column-width="550.8pt"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block space-before="6pt" font-weight="bold" text-decoration="underline"&gt;TERMS AND CONDITIONS:&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Mortgage:&lt;/fo:block&gt;
								&lt;fo:block&gt;This Mortgage will be subject to all extended terms set forth in our standard form of mortgage contract or in the mortgage contract prepared by our solicitors, whichever the case may be, and insured mortgage loans will be subject to the provisions of the National Housing Act (N.H.A.) and the regulations thereunder.&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Rate Guarantee and Rate Adjustment Policies:&lt;/fo:block&gt;
								&lt;fo:block&gt;Provided that the Mortgage Loan is advanced by </xsl:text><xsl:value-of select="//CommitmentLetter/AdvanceDate"/><xsl:text>, the interest rate will be </xsl:text><xsl:value-of select="//CommitmentLetter/InterestRate"/><xsl:text> per annum, calculated half yearly, not in advance. In the event that the effective rate of interest is lower at closing, the interest rate may be adjusted.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Title Requirements / Title Insurance:&lt;/fo:block&gt;
								&lt;fo:block&gt;Title to the Property must be acceptable to our Solicitor who will ensure that the Mortgagor(s) have good and marketable title in fee simple to the property and that it is clear of any encumbrances which might affect the priority of the </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text> Mortgage. Alternatively, we will accept title insurance from a title insurance provider acceptable to </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text>. The Mortgage must be a first charge on the property.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Survey Requirements / Title Insurance:&lt;/fo:block&gt;
								&lt;fo:block&gt;A survey or surveyors certificate completed by a recognized land surveyor and dated within the last ten (10) years is to be furnished to our Solicitor prior to any advance. The survey must confirm that the location of the building(s) on the property complies with all municipal, provincial and other government requirements. A survey older than ten (10) years, but not older than twenty (20) years, may be accepted provided that it is accompanied by a Declaration of Possession from the then present owners of the property. Alternatively, we will accept title insurance from a title insurance provider acceptable to </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text>.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
					&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
			&lt;/fo:block&gt;
			&lt;fo:block break-before="page" font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;
				&lt;fo:table table-layout="fixed"&gt;
					&lt;fo:table-column column-width="550.8pt"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block space-before="6pt" font-weight="bold" text-decoration="underline"&gt;Privilege Payment Policies: &lt;/fo:block&gt;
								&lt;fo:block&gt;This mortgage is CLOSED. The Mortgagor (borrower), when not in default of any of the terms, covenants, conditions or provisions contained in the mortgage, shall have the following privileges for payment of extra principal amounts: 1) During each 12 month period following the interest adjustment date the borrower may, without penalty: i) make prepayments totaling up to twenty percent (20%) of the original principal amount of the mortgage; ii) increase regular monthly payment by a total up to twenty percent (20%) of the original monthly payment amount so as to reduce the amortization of the mortgage without changing term. 2) If the mortgaged property is sold pursuant to a bona fide arms length agreement, the borrower may repay the mortgage in full upon payment of three (3) months interest penalty calculated at the current interest rate of the mortgage. 3) In addition to the prepayment privileges described in paragraph 1, at any time after the third anniversary of the interest adjustment date, the borrower may prepay the mortgage in whole or in part upon payment of three (3) months interest penalty calculated at the current interest rate of the mortgage.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Interest Adjustment Payment &lt;/fo:block&gt;
								&lt;fo:block&gt;Prior to commencement of the regular monthly loan payments interest will accrue at the rate payable on the Mortgage loan, on all funds advanced to the Mortgagor(s). Interest will be computed from the advance date and will become due and payable on the first day of the month next following the advance date.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Prepayment Policies:&lt;/fo:block&gt;
								&lt;fo:block&gt;You can prepay this mortgage on payment of 3 months simple interest on the principal amount owing at the prepayment date, or the interest rate differential,whichever is greater.TBD&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Renewal:&lt;/fo:block&gt;
								&lt;fo:block&gt;At the sole discretion of BasisXPressLender, the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s).&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Early Renewal: &lt;/fo:block&gt;
								&lt;fo:block&gt;Provided that the Mortgage is not in default and that the building on the lands described in the Mortgage is used only as residence and comprises no more than four (4) dwelling units, you may renegotiate the term and payment provisions of the Mortgage prior to the end of the original term of the Mortgage, provided that, &lt;/fo:block&gt;
								&lt;fo:list-block space-before="6pt" space-after="6pt" font-family="Arial, Helvetica" font-size="9.0pt"&gt;
									&lt;fo:list-item&gt;
										&lt;fo:list-item-label end-indent="label-end()"&gt;
											&lt;fo:block&gt;a)&lt;/fo:block&gt;
										&lt;/fo:list-item-label&gt;
										&lt;fo:list-item-body start-indent="body-start()"&gt;
											&lt;fo:block text-align="justify"&gt;the Mortgage may only be renegotiated for a closed term&lt;/fo:block&gt;
										&lt;/fo:list-item-body&gt;
									&lt;/fo:list-item&gt;
									&lt;fo:list-item&gt;
										&lt;fo:list-item-label end-indent="label-end()"&gt;
											&lt;fo:block&gt;b)&lt;/fo:block&gt;
										&lt;/fo:list-item-label&gt;
										&lt;fo:list-item-body start-indent="body-start()"&gt;
											&lt;fo:block text-align="justify"&gt;the Mortgagor(s) select a renewal option that results in the new Maturity Date being equal to or greater than the original Maturity Date of the current term; and &lt;/fo:block&gt;
										&lt;/fo:list-item-body&gt;
									&lt;/fo:list-item&gt;
									&lt;fo:list-item&gt;
										&lt;fo:list-item-label end-indent="label-end()"&gt;
											&lt;fo:block&gt;c)&lt;/fo:block&gt;
										&lt;/fo:list-item-label&gt;
										&lt;fo:list-item-body start-indent="body-start()"&gt;
											&lt;fo:block text-align="justify"&gt;payment of the interest rate differential and an early renewal processing fee is made upon execution of such renewal or amending agreement.&lt;/fo:block&gt;
										&lt;/fo:list-item-body&gt;
									&lt;/fo:list-item&gt;
								&lt;/fo:list-block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;NSF Fee: &lt;/fo:block&gt;
								&lt;fo:block&gt;There may be a fee charged for any payment returned due to insufficient funds or stopped payments.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Assumption Policies:&lt;/fo:block&gt;
								&lt;fo:block&gt;In the event that you subsequently enter into an agreement to sell, convey or transfer the property, any subsequent purchaser(s) who wish to assume this Mortgage, must be approved by </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text>, otherwise this Mortgage Loan will become due and payable upon your vacating or selling the property.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Portability Policies:&lt;/fo:block&gt;
								&lt;fo:block&gt;Should you decide to sell the mortgaged property and purchase another property, you may transfer the remaining mortgage balance together with the interest rate set out therein to the new property provided that the Mortgage Loan is not in default and a new mortgage loan application has been accepted by </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text> and all terms and conditions set out in the subsequent Mortgage Loan approval have been met.&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
					&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
			&lt;/fo:block&gt;
			&lt;fo:block break-before="page" font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;
				&lt;fo:table table-layout="fixed"&gt;
					&lt;fo:table-column column-width="550.8pt"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;The following conditions must be met, and the requested documents must be received in form and content satisfactory to </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text> no later than ten (10) days prior to the advance of the mortgage. Failure to do so may delay or void this commitment.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:list-block space-before="6pt" space-after="6pt" font-family="Arial, Helvetica"&gt;
								</xsl:text>
								
								<xsl:for-each select="//CommitmentLetter/Conditions/Condition">
									<xsl:text>
									&lt;fo:list-item&gt;
										&lt;fo:list-item-label end-indent="label-end()"&gt;
											&lt;fo:block&gt;</xsl:text><xsl:value-of select="position()"/><xsl:text>.&lt;/fo:block&gt;
										&lt;/fo:list-item-label&gt;
										&lt;fo:list-item-body start-indent="body-start()"&gt;
											&lt;fo:block keep-together="always"&gt;</xsl:text>
		
											<xsl:for-each select="./Line">
												<xsl:text>&lt;fo:block&gt;</xsl:text>
												<xsl:choose>
												<!--  When there is a node with text in it (non-empty), then we display it normally.  
													However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
													<xsl:when test="* | text()"><xsl:value-of select="."/>
														</xsl:when>
													<xsl:otherwise><xsl:text>&lt;fo:leader line-height="9pt"/&gt;</xsl:text></xsl:otherwise>
												</xsl:choose>
												<xsl:text>&lt;/fo:block&gt;</xsl:text>												
											</xsl:for-each>
		
											<xsl:text>
											&lt;/fo:block&gt;
											&lt;fo:block&gt;
												&lt;fo:leader line-height="9pt"/&gt;
											&lt;/fo:block&gt;
										&lt;/fo:list-item-body&gt;
									&lt;/fo:list-item&gt;
									</xsl:text>									
								</xsl:for-each>
								<xsl:text>
								&lt;/fo:list-block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;Requests for any changes to the terms and conditions of the Commitment will only be considered if your request is received by us in writing at least 5 business days prior to the Advance Date.&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;ACCEPTANCE:&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold" text-decoration="underline"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text><xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/><xsl:text> after which time, if not accepted, shall be considered null and void. Furthermore, the mortgage must be advanced by no later than </xsl:text><xsl:value-of select="//CommitmentLetter/AdvanceDate"/><xsl:text> at which time it expires.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader leader-length="200.0pt" leader-pattern="space"/&gt;
									&lt;fo:leader leader-length="42.5pt" leader-pattern="space"/&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text>&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader leader-length="200.0pt" leader-pattern="space"/&gt;Authorized by: &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader leader-length="200.0pt" leader-pattern="space"/&gt;
									&lt;fo:leader leader-length="42.5pt" leader-pattern="space"/&gt;
									&lt;fo:leader leader-length="32.5pt" leader-pattern="space"/&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/Underwriter"/><xsl:text>&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
					&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
			&lt;/fo:block&gt;
			&lt;fo:block break-before="page" font-family="Arial, Helvetica" font-size="9.0pt" text-align="justify"&gt;
				&lt;fo:table table-layout="fixed"&gt;
					&lt;fo:table-column column-width="167.4pt"/&gt;
					&lt;fo:table-column column-width="171.0pt"/&gt;
					&lt;fo:table-column column-width="212.4pt"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block space-before="6pt" font-weight="bold"&gt;I / We, hereby authorize </xsl:text><xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text> to debit the account indicated below with payments on the Mortgage Loan (including taxes and life insurance premiums, if applicable)&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-weight="bold"&gt;[ ] Chequing/Savings Account&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-weight="bold"&gt;[ ] Chequing Account &lt;fo:inline font-weight="normal"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-weight="bold"&gt;[ ] Current Account No.
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;Bank 	&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="225pt"/&gt; 
								Branch &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;Address &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									Transit No.&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="200pt"/&gt; 
									Telephone No. &lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;A sample &quot;VOID&quot; cheque is enclosed.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;I / We </xsl:text><xsl:value-of select="//CommitmentLetter/Underwriter"/><xsl:text> warrant that all representations made by me/us and all the information submitted to you in connection with my/our mortgage application are true and accurate. I / We fully understand that any misrepresentations of fact contained in my/our mortgage application or other documentation entitles you to decline to advance a portion or all of the loan proceeds, or to demand immediate repayment of all monies secured by the Mortgage.&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;I / We, the undersigned applicants accept the terms of this Mortgage Commitment as stated above and agree to fulfill the conditions of approval outlined herein.&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;WITNESS&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;APPLICANT&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;DATE&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;WITNESS&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;CO-APPLICANT&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;DATE&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-style="none" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;WITNESS&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-style="none" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;</xsl:text><xsl:value-of select="//CommitmentLetter/GuarantorClause"/><xsl:text>&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" border-left-style="none" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-size="9pt"&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;DATE&lt;/fo:block&gt;
								&lt;fo:block font-weight="bold"&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
								&lt;fo:block&gt;
									&lt;fo:leader line-height="9pt"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
					&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
			&lt;/fo:block&gt;
	<!--		&lt;fo:block id="endofdoc"/&gt; -->
		&lt;/fo:flow&gt;
	&lt;/fo:page-sequence&gt;
		</xsl:text>
	</xsl:template>
	<xsl:template name="FOStart">
		<xsl:text>
		&lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;
		&lt;fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://xml.apache.org/fop/extensions"&gt;
			&lt;fo:layout-master-set&gt;
				&lt;fo:simple-page-master master-name="main" page-height="11in" page-width="8.5in" margin-top="1cm" margin-bottom="1cm" margin-left="1cm" margin-right="1cm"&gt;
					&lt;fo:region-body margin-bottom="10mm" margin-top="5mm"/&gt;
					&lt;fo:region-before extent="10mm"/&gt;
					&lt;fo:region-after extent="10mm"/&gt;
				&lt;/fo:simple-page-master&gt;
			&lt;/fo:layout-master-set&gt;
	</xsl:text>
	</xsl:template>
	<xsl:template name="FOEnd">
		<xsl:text>&lt;/fo:root&gt;</xsl:text>
	</xsl:template>
</xsl:stylesheet>
