<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:fo="http://www.w3.org/1999/XSL/Format" 
  xmlns:fox="http://xml.apache.org/fop/extensions">
	<!--	xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->

	<!--
  28/Sep/2006 DVG #DG520 #3371  HFS CR#36 Branding Changes  	
  31/Mar/2005 DVG #DG174 #1155  HFC Name Change to Household Financial Solutions /added French logo
	   for now, only the regular (english) logo is in production and is checked in.
  11/Feb/2005 DVG #DG138 #956  Blank Commitment letter for deal ids 383,385 (HFC E2E),
	   major surgery to convert template to regular <xsl:element name= format  	 
	
  edited with XML Spy v4.3 U (http://www.xmlspy.com) by Brad Hadfield (Basis100 Inc) 
  -->

	<xsl:output method="xml" indent="yes" version="1.0" encoding="ISO-8859-1"/>

  <!--#DG520 -->
	<xsl:variable name="lenderName" select="/*/CommitmentLetter/LenderName"/>
	
	<xsl:template match="/">
		<xsl:element name="fo:root">
			<!--xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:call-template name="FOStart"/>
			<xsl:choose>
				<xsl:when test="//CommitmentLetter/LanguageEnglish">
					<xsl:call-template name="EnglishPage1"/>
					<xsl:call-template name="EnglishPage2"/>
					<xsl:call-template name="EnglishPage3"/>
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="//CommitmentLetter/LanguageFrench">
					<xsl:call-template name="FrenchPage1"/>
					<xsl:call-template name="FrenchPage2"/>
					<xsl:call-template name="FrenchPage3"/>
				</xsl:when>
			</xsl:choose>
			<!--xsl:call-template name="FOEnd"/-->
		</xsl:element>
		<!-- fo:root -->
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishHeader">
		<xsl:element name="fo:static-content">
			<xsl:attribute name="flow-name">xsl-region-before</xsl:attribute>
			<xsl:element name="fo:block">
				<xsl:attribute name="line-height">12pt</xsl:attribute>
				<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
				<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:inline">
					<xsl:attribute name="font-style">italic</xsl:attribute>
					<xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-size">14pt</xsl:attribute>
					<xsl:text>MORTGAGE COMMITMENT</xsl:text>
				</xsl:element>
				<!-- fo:inline -->
			</xsl:element>
			<!-- fo:block -->
		</xsl:element>
		<!-- fo:static-content -->
	</xsl:template>

	<xsl:template name="EnglishFooter">
		<xsl:element name="fo:static-content">
			<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
			<xsl:element name="fo:table">
				<xsl:attribute name="text-align">left</xsl:attribute>
				<xsl:attribute name="table-layout">fixed</xsl:attribute>
				<xsl:element name="fo:table-column">
					<xsl:attribute name="column-width">8cm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:table-column">
					<xsl:attribute name="column-width">3.55cm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:table-column">
					<xsl:attribute name="column-width">8cm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">2mm</xsl:attribute>
							<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:attribute name="padding-right">2mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						</xsl:element>
						<!-- fo:table-cell -->
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">2mm</xsl:attribute>
							<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:attribute name="padding-right">2mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="font-size">10pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:attribute name="text-align">center</xsl:attribute>
								<xsl:text>Page </xsl:text>
								<xsl:element name="fo:page-number"/>
								<xsl:text> of </xsl:text>
								<xsl:element name="fo:page-number-citation">
									<xsl:attribute name="ref-id">endofdoc</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:block -->
						</xsl:element>
						<!-- fo:table-cell -->
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">2mm</xsl:attribute>
							<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:attribute name="padding-right">2mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="font-size">10pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:attribute name="text-align">right</xsl:attribute>
								<xsl:text>Initials </xsl:text>
								<xsl:element name="fo:leader">
									<xsl:attribute name="leader-pattern">rule</xsl:attribute>
									<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
									<xsl:attribute name="color">black</xsl:attribute>
									<xsl:attribute name="leader-length">50pt</xsl:attribute>
								</xsl:element>
								<xsl:text> Date </xsl:text>
								<xsl:element name="fo:leader">
									<xsl:attribute name="leader-pattern">rule</xsl:attribute>
									<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
									<xsl:attribute name="color">black</xsl:attribute>
									<xsl:attribute name="leader-length">100pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:block -->
						</xsl:element>
						<!-- fo:table-cell -->
					</xsl:element>
					<!-- fo:table-row -->
				</xsl:element>
				<!-- fo:table-body -->
			</xsl:element>
			<!-- fo:table -->
		</xsl:element>
		<!-- fo:static-content -->
	</xsl:template>

	<xsl:template name="EnglishPage1">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:call-template name="EnglishHeader"/>
			<xsl:call-template name="EnglishFooter"/>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:element name="fo:table">
					<xsl:attribute name="text-align">left</xsl:attribute>
					<xsl:attribute name="table-layout">fixed</xsl:attribute>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">1.25cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.80cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">5.5cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.5cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.5cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">5cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-body">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">2mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
								<xsl:attribute name="border">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">3</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<!--  Change this to be the relative path (from the root).  The drive letter isn't needed -->
								<xsl:choose>
									<xsl:when test="//CommitmentLetter/LenderProfileId=0">
										<xsl:element name="fo:external-graphic">
											<xsl:attribute name="src">\mos_docprep\admin_HF\docgen\templates\HFCLogo.gif</xsl:attribute>
											<xsl:attribute name="height">1.10in</xsl:attribute>
											<xsl:attribute name="width">2.5in</xsl:attribute>
										</xsl:element>
									</xsl:when>
									<xsl:otherwise>
										<xsl:element name="fo:external-graphic">
											<xsl:attribute name="src">\mos_docprep\admin_HF\docgen\templates\XCD2.GIF</xsl:attribute>
											<xsl:attribute name="height">.84in</xsl:attribute>
											<xsl:attribute name="width">2.7in</xsl:attribute>
										</xsl:element>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
                  <xsl:value-of select="$lenderName"/><!--#DG520 -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt	</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt	</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt	</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:if>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:attribute name="margin-bottom">6.0pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
									<xsl:text>  </xsl:text>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:text>Telephone:</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:text>Fax:</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:text>TO:</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<!--/xsl:element> < fo:table-cell -->
								<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt	</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:if>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
									<xsl:text>  </xsl:text>
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>Underwriter</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/Underwriter"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:text>Date</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
									</xsl:element>
									<!-- fo:block -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/BranchCurrentTime"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">4</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/CareOf"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerFirmName"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAgentName"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line1"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:if test="//CommitmentLetter/BrokerAddress/Line2">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line2"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:if>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAddress/City"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="//CommitmentLetter/BrokerAddress/Province"/>
									<xsl:text>  </xsl:text>
									<xsl:value-of select="//CommitmentLetter/BrokerAddress/PostalCode"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:if test="//CommitmentLetter/BrokerEmail">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/BrokerEmail"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:if>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Broker Reference Number:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">3</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Lender Reference Number:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/DealNum"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:text>Telephone:</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAgentContactData/BrokerAgentPhoneNumber"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:text>Fax:</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAgentContactData/BrokerAgentFaxNumber"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-style">italic</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">14pt</xsl:attribute>
										<xsl:text>We are pleased to confirm that your application for a Mortgage Loan has been approved under the following terms and conditions:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Mortgagor(s):</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
									<xsl:element name="fo:block">
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Security Address:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:for-each select="//CommitmentLetter/Properties/Property">
										<xsl:element name="fo:block">
											<xsl:attribute name="font-size">10pt</xsl:attribute>
											<xsl:attribute name="line-height">12pt</xsl:attribute>
											<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:value-of select="./AddressLine1"/>
										</xsl:element>
										<!-- fo:block -->
										<xsl:if test="./AddressLine2">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">10pt</xsl:attribute>
												<xsl:attribute name="line-height">12pt</xsl:attribute>
												<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
												<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
												<xsl:value-of select="./AddressLine2"/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:if>
										<xsl:element name="fo:block">
											<xsl:attribute name="font-size">10pt</xsl:attribute>
											<xsl:attribute name="line-height">12pt</xsl:attribute>
											<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:value-of select="./City"/>
											<xsl:text> </xsl:text>
											<xsl:value-of select="./Province"/>
											<xsl:text>  </xsl:text>
											<xsl:value-of select="./PostalCode"/>
										</xsl:element>
										<!-- fo:block -->
									</xsl:for-each>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
										<xsl:text>:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>LOAN TYPE: </xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:value-of select="//CommitmentLetter/Product"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>LTV: </xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:value-of select="//CommitmentLetter/LTV"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
					</xsl:element>
					<!-- fo:table-body -->
				</xsl:element>
				<!-- fo:table -->
				<xsl:element name="fo:table">
					<xsl:attribute name="text-align">left</xsl:attribute>
					<xsl:attribute name="table-layout">fixed</xsl:attribute>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.5cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3.25cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.75cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3.8cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">4.25cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-body">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>COMMITMENT DATE:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>FIRST PAYMENT DATE:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>INTEREST ADJUSTMENT DATE:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/IADDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>CLOSING DATE:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>MATURITY DATE:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/MaturityDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>INTEREST ADJUSTMENT AMOUNT:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/IADAmount"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>LOAN</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>TERMS</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>INSTALLMENT - </xsl:text>
										<xsl:value-of select="//CommitmentLetter/PaymentFrequency"/>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Amount:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Interest Rate:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/InterestRate"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Principal and Interest:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/PandIPayment"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Insurance:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/Premium"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Term:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/PaymentTerm"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Taxes (estimated):</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/TaxPortion"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Admin Fee:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/AdminFee"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Amortization:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/Amortization"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Total Installment:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/TotalPayment"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Total Loan:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:text>Please be advised the solicitor acting on behalf of the transaction, will be:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:text>Name:</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:text>Address:</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:text>Phone / Fax:</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:text>To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than </xsl:text>
									<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
									<xsl:text> after which time if not accepted, shall be considered null and void.</xsl:text>
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/ClosingText/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
											However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">10pt</xsl:attribute>
												<xsl:attribute name="line-height">12pt</xsl:attribute>
												<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
												<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">10pt</xsl:attribute>
												<xsl:attribute name="line-height">12pt</xsl:attribute>
												<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
												<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
												<!-- fo:block -->
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
					</xsl:element>
					<!-- fo:table-body -->
				</xsl:element>
				<!-- fo:table -->
				<xsl:element name="fo:block">
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:attribute name="id">LastPage</xsl:attribute>
					<xsl:attribute name="line-height">1pt</xsl:attribute>
					<xsl:attribute name="font-size">1pt</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<!-- fo:flow -->
		</xsl:element>
		<!-- fo:page-sequence -->
	</xsl:template>
	<xsl:template name="EnglishPage2">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:call-template name="EnglishHeader"/>
			<xsl:call-template name="EnglishFooter"/>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:element name="fo:table">
					<xsl:attribute name="text-align">left</xsl:attribute>
					<xsl:attribute name="table-layout">fixed</xsl:attribute>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">1cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">18.55cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-body">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-top">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:block">
										<xsl:attribute name="space-after">6.0pt</xsl:attribute>
										<xsl:element name="fo:inline">
											<xsl:attribute name="text-decoration">underline</xsl:attribute>
											<xsl:attribute name="font-weight">bold</xsl:attribute>
											<xsl:attribute name="font-size">9pt</xsl:attribute>
											<xsl:text>TERMS AND CONDITIONS:</xsl:text>
										</xsl:element>
										<!-- fo:inline -->
									</xsl:element>
									<!-- fo:block -->
									<xsl:element name="fo:block">
										<xsl:attribute name="space-after">6.0pt</xsl:attribute>
										<xsl:element name="fo:inline">
											<xsl:attribute name="font-weight">bold</xsl:attribute>
											<xsl:attribute name="font-size">9pt</xsl:attribute>
											<xsl:text>THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING:</xsl:text>
										</xsl:element>
										<!-- fo:inline -->
									</xsl:element>
									<!-- fo:block -->
									<xsl:element name="fo:block">
										<xsl:attribute name="space-after">6.0pt</xsl:attribute>
										<xsl:text>The following conditions must be met, and the requested documents must be received in form and content satisfactory to </xsl:text>
                    <xsl:value-of select="$lenderName"/><!--#DG520 -->
										<xsl:text> no later than ten (10) days prior to the advance of the mortgage. Failure to do so may delay or void this commitment.</xsl:text>
									</xsl:element>
									<!-- fo:block -->
									<xsl:for-each select="//CommitmentLetter/Conditions/Condition">
										<xsl:element name="fo:list-item">
											<xsl:element name="fo:list-item-label">
												<xsl:attribute name="end-indent">label-end()</xsl:attribute>
												<xsl:element name="fo:block">
													<xsl:value-of select="position()"/>
													<xsl:text>.</xsl:text>
												</xsl:element>
												<!-- fo:block -->
											</xsl:element>
											<!-- fo:list-item-label -->
											<xsl:element name="fo:list-item-body">
												<xsl:attribute name="start-indent">body-start()</xsl:attribute>
												<xsl:element name="fo:block">
													<xsl:attribute name="keep-together">always</xsl:attribute>
													<xsl:for-each select="./Line">
														<xsl:element name="fo:block">
															<xsl:choose>
																<!--  When there is a node with text in it (non-empty), then we display it normally.  
													However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
																<xsl:when test="* | text()">
																	<xsl:value-of select="."/>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:element name="fo:leader">
																		<xsl:attribute name="line-height">9pt</xsl:attribute>
																	</xsl:element>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:element>
														<!-- fo:block -->
													</xsl:for-each>
												</xsl:element>
												<!-- fo:block -->
												<xsl:element name="fo:block">
													<xsl:element name="fo:leader">
														<xsl:attribute name="line-height">9pt</xsl:attribute>
													</xsl:element>
												</xsl:element>
												<!-- fo:block -->
											</xsl:element>
											<!-- fo:list-item-body -->
										</xsl:element>
										<!-- fo:list-item -->
									</xsl:for-each>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">9pt</xsl:attribute>STANDARD CONDITIONS:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>1.</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/VoidChq"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>2.</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/TitleInsurance/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt	</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>3.</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/FireInsurance/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>4.</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/TaxesPaid/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt	</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<!--					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">1mm</xsl:attribute> 
							<xsl:attribute name="padding-left">2mm</xsl:attribute> 
							<xsl:attribute name="padding-right">2mm</xsl:attribute> 
							<xsl:attribute name="border-left">solid Black</xsl:attribute> 
							<xsl:attribute name="border-right">solid Black</xsl:attribute> 
							<xsl:attribute name="border-bottom">solid Black</xsl:attribute> 
							<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before">12pt</xsl:attribute> 
								<xsl:attribute name="space-after">6pt</xsl:attribute> 
								<xsl:attribute name="font-size">9pt</xsl:attribute>
								<xsl:element name="fo:inline">
									<xsl:attribute name="text-decoration">underline</xsl:attribute> 
									<xsl:attribute name="font-weight">bold</xsl:attribute>RATE GUARANTEE AND RATE ADJUSTMENT POLICIES:</xsl:element> 
							</xsl:element> <! - - fo:block>
							
							<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
								<xsl:choose>
									<xsl:when 
									<xsl:when test="* | text()">	<xsl:element name="fo:block">
										<xsl:attribute name="font-size">9pt</xsl:attribute> 
										<xsl:attribute name="space-after">6pt</xsl:attribute><xsl:value-of 
										select="."/></xsl:element> <! - - fo:block >
									</xsl:when>
									<xsl:otherwise><xsl:element name="fo:block">
										<xsl:attribute name="font-size">9pt</xsl:attribute> 
										<xsl:attribute name="space-after">6pt</xsl:attribute><xsl:element name="fo:leader 
										<xsl:attribute name="line-height">9pt</xsl:attribute></xsl:element></xsl:element> <!- - fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
							
						</xsl:element> <!- - fo:table-cell >
					</xsl:element> <!- - fo:table-row >-->
					</xsl:element>
					<!-- fo:table-body -->
				</xsl:element>
				<!-- fo:table -->
			</xsl:element>
			<!-- fo:flow -->
		</xsl:element>
		<!-- fo:page-sequence -->
	</xsl:template>
	<xsl:template name="EnglishPage3">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:call-template name="EnglishHeader"/>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="font-size">10pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:attribute name="text-align">center</xsl:attribute>Page <xsl:element name="fo:page-number"/>
					<xsl:text> of </xsl:text>
					<xsl:element name="fo:page-number-citation">
						<xsl:attribute name="ref-id">endofdoc</xsl:attribute>
					</xsl:element>
				</xsl:element>
				<!-- fo:block -->
			</xsl:element>
			<!-- fo:static-content -->
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:element name="fo:table">
					<xsl:attribute name="text-align">left</xsl:attribute>
					<xsl:attribute name="table-layout">fixed</xsl:attribute>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">6cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">10cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3.55cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-body">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-top">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">6pt</xsl:attribute>
									<xsl:attribute name="space-after">6pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>RATE GUARANTEE AND RATE ADJUSTMENT POLICIES:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
									<xsl:choose>
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>PAYMENT POLICY:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/PrivilegePayment/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
										However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:for-each select="//CommitmentLetter/InterestCalc/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
										However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>CLOSING:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/Closing/Line">
									<xsl:element name="fo:block">
										<xsl:attribute name="font-size">9pt</xsl:attribute>
										<xsl:attribute name="space-after">6pt</xsl:attribute>
										<xsl:attribute name="text-align">justify</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>ACCEPTANCE:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/Acceptance/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
										However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="text-align">right</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:attribute name="space-before">6pt</xsl:attribute>
									<xsl:attribute name="space-after">6pt</xsl:attribute>
                  <xsl:value-of select="$lenderName"/><!--#DG520 -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="text-align">right</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:attribute name="space-before">6pt</xsl:attribute>
									<xsl:attribute name="space-after">6pt</xsl:attribute>
									<xsl:text>Authorized by: </xsl:text>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">75pt</xsl:attribute>
									</xsl:element>
									<xsl:value-of select="//CommitmentLetter/Underwriter"/>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">100pt</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/Signature/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
										However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="text-align">right</xsl:attribute>
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-before">6pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="text-align">right</xsl:attribute>
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-before">6pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">30pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>WITNESS</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">30pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>APPLICANT</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">30pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>DATE</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>WITNESS</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>CO-APPLICANT</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>DATE</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">12pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>WITNESS</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">12pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">12pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>DATE</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
					</xsl:element>
					<!-- fo:table-body -->
				</xsl:element>
				<!-- fo:table -->
				<xsl:element name="fo:block">
					<xsl:attribute name="id">endofdoc</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<!-- fo:flow -->
		</xsl:element>
		<!-- fo:page-sequence -->
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="FrenchHeader">
		<xsl:element name="fo:static-content">
			<xsl:attribute name="flow-name">xsl-region-before</xsl:attribute>
			<xsl:element name="fo:block">
				<xsl:attribute name="line-height">12pt</xsl:attribute>
				<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
				<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:inline">
					<xsl:attribute name="font-style">italic</xsl:attribute>
					<xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-size">14pt</xsl:attribute>
					<xsl:text>LETTRE D'ENGAGEMENT</xsl:text>
				</xsl:element>
				<!-- fo:inline -->
			</xsl:element>
			<!-- fo:block -->
		</xsl:element>
		<!-- fo:static-content -->
	</xsl:template>
	<xsl:template name="FrenchFooter">
		<xsl:element name="fo:static-content">
			<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
			<xsl:element name="fo:table">
				<xsl:attribute name="text-align">left</xsl:attribute>
				<xsl:attribute name="table-layout">fixed</xsl:attribute>
				<xsl:element name="fo:table-column">
					<xsl:attribute name="column-width">8cm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:table-column">
					<xsl:attribute name="column-width">3.55cm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:table-column">
					<xsl:attribute name="column-width">8cm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:table-body">
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">2mm</xsl:attribute>
							<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:attribute name="padding-right">2mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						</xsl:element>
						<!-- fo:table-cell -->
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">2mm</xsl:attribute>
							<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:attribute name="padding-right">2mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="font-size">10pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:attribute name="text-align">center</xsl:attribute>
								<xsl:text>Page </xsl:text>
								<xsl:element name="fo:page-number"/>
								<xsl:text> de </xsl:text>
								<xsl:element name="fo:page-number-citation">
									<xsl:attribute name="ref-id">endofdoc</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:block -->
						</xsl:element>
						<!-- fo:table-cell -->
						<xsl:element name="fo:table-cell">
							<xsl:attribute name="padding-top">2mm</xsl:attribute>
							<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:attribute name="padding-right">2mm</xsl:attribute>
							<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="font-size">10pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:attribute name="text-align">right</xsl:attribute>
								<xsl:text>Initiales </xsl:text>
								<xsl:element name="fo:leader">
									<xsl:attribute name="leader-pattern">rule</xsl:attribute>
									<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
									<xsl:attribute name="color">black</xsl:attribute>
									<xsl:attribute name="leader-length">50pt</xsl:attribute>
								</xsl:element>
								<xsl:text> Date </xsl:text>
								<xsl:element name="fo:leader">
									<xsl:attribute name="leader-pattern">rule</xsl:attribute>
									<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
									<xsl:attribute name="color">black</xsl:attribute>
									<xsl:attribute name="leader-length">100pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:block -->
						</xsl:element>
						<!-- fo:table-cell -->
					</xsl:element>
					<!-- fo:table-row -->
				</xsl:element>
				<!-- fo:table-body -->
			</xsl:element>
			<!-- fo:table -->
		</xsl:element>
		<!-- fo:static-content -->
	</xsl:template>
	<xsl:template name="FrenchPage1">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:call-template name="FrenchHeader"/>
			<xsl:call-template name="FrenchFooter"/>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:element name="fo:table">
					<xsl:attribute name="text-align">left</xsl:attribute>
					<xsl:attribute name="table-layout">fixed</xsl:attribute>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.75cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.05cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">4.75cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3.0cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3.0cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">4cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-body">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">2mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
								<xsl:attribute name="border">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">3</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<!--  Change this to be the relative path (from the root).  The drive letter isn't needed -->
								<xsl:choose>
									<xsl:when test="//CommitmentLetter/LenderProfileId=0">
										<xsl:element name="fo:external-graphic">
											<xsl:attribute name="src">\mos_docprep\admin_HF\docgen\templates\HFCLogoFr.gif</xsl:attribute> <!--#DG174-->
											<xsl:attribute name="height">1.10in</xsl:attribute>
											<xsl:attribute name="width">2.5in</xsl:attribute>
										</xsl:element>
									</xsl:when>
									<xsl:otherwise>
										<xsl:element name="fo:external-graphic">
											<xsl:attribute name="src">\mos_docprep\admin_HF\docgen\templates\XCD2.GIF</xsl:attribute>
											<xsl:attribute name="height">.84in</xsl:attribute>
											<xsl:attribute name="width">2.7in</xsl:attribute>
										</xsl:element>
									</xsl:otherwise>
								</xsl:choose>
								<!-- <xsl:element name="fo:external-graphic">
									 	<xsl:attribute name="src">\mos_docprep\admin_XD\docgen\templates\XCD2F.GIF</xsl:attribute> 
									 	<xsl:attribute name="height">.84in</xsl:attribute> 
									 	<xsl:attribute name="width">2.7in</xsl:attribute></xsl:element> -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
                  <!--#DG520 xsl:text>Corporation Hypothécaire Xceed</xsl:text-->
                  <xsl:value-of select="$lenderName"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:if>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:attribute name="margin-bottom">6.0pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
									<xsl:text>  </xsl:text>
									<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>Téléphone:</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:text>1-888-257-8889</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>Télécopieur:</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:text>1-888-482-6544</xsl:text>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">8pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>EMPRUNTEUR(S):</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:if>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
									<xsl:text>  </xsl:text>
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>Souscripteur</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/Underwriter"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>Date</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
									</xsl:element>
									<!-- fo:block -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/BranchCurrentTime"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">4</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<!--</xsl:text><xsl:value-of 
										select="//CommitmentLetter/CareOf"/><xsl:text>-->A/S</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerFirmName"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAgentName"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line1"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:if test="//CommitmentLetter/BrokerAddress/Line2">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line2"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:if>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAddress/City"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="//CommitmentLetter/BrokerAddress/Province"/>
									<xsl:text>  </xsl:text>
									<xsl:value-of select="//CommitmentLetter/BrokerAddress/PostalCode"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:if test="//CommitmentLetter/BrokerEmail">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/BrokerEmail"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:if>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Numéro de référence du courtier:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">3</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Numéro de référence du prêteur:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/DealNum"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>Téléphone:</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAgentContactData/BrokerAgentPhoneNumber"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>Télécopieur:</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/BrokerAgentContactData/BrokerAgentFaxNumber"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-style">italic</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">14pt</xsl:attribute>Nous sommes heureux de confirmer que votre demande de prêt hypothécaire a été approuvée selon les conditions mentionnées ci-dessous:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>EMPRUNTEUR(s):</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
									<xsl:element name="fo:block">
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>ADRESSE DE LA PROPRIÉTÉ:</xsl:element>
									<!-- fo:inline -->
									<xsl:for-each select="//CommitmentLetter/Properties/Property">
										<xsl:element name="fo:block">
											<xsl:attribute name="font-size">10pt</xsl:attribute>
											<xsl:attribute name="line-height">12pt</xsl:attribute>
											<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:value-of select="./AddressLine1"/>
										</xsl:element>
										<!-- fo:block -->
										<xsl:if test="./AddressLine2">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">10pt</xsl:attribute>
												<xsl:attribute name="line-height">12pt</xsl:attribute>
												<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
												<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
												<xsl:value-of select="./AddressLine2"/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:if>
										<xsl:element name="fo:block">
											<xsl:attribute name="font-size">10pt</xsl:attribute>
											<xsl:attribute name="line-height">12pt</xsl:attribute>
											<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:value-of select="./City"/>
											<xsl:text> </xsl:text>
											<xsl:value-of select="./Province"/>
											<xsl:text>  </xsl:text>
											<xsl:value-of select="./PostalCode"/>
										</xsl:element>
										<!-- fo:block -->
									</xsl:for-each>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
										<xsl:text>:</xsl:text>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
									<xsl:element name="fo:block">
										<xsl:attribute name="line-height">12pt</xsl:attribute>
										<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
										<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>PRODUIT: </xsl:element>
									<!-- fo:inline -->
									<xsl:value-of select="//CommitmentLetter/Product"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>RATIO PRÊT VALEUR: </xsl:element>
									<!-- fo:inline -->
									<xsl:value-of select="//CommitmentLetter/LTV"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
					</xsl:element>
					<!-- fo:table-body -->
				</xsl:element>
				<!-- fo:table -->
				<xsl:element name="fo:table">
					<xsl:attribute name="text-align">left</xsl:attribute>
					<xsl:attribute name="table-layout">fixed</xsl:attribute>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3.00cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.75cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">2.85cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3.7cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">4.25cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-body">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>DATE D'ENGAGEMENT:</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>DATE DU PREMIER VERSEMENT:</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>DATE D'AJUSTEMENT DES INTÉRÊTS:</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/IADDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>DATE DE CLÔTURE:</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>DATE D'ÉCHÉANCE:</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/MaturityDate"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>MONTANT DE L'AJUSTEMENT DES INTÉRÊTS:</xsl:element>
									<!-- fo:inline -->
									<xsl:element name="fo:block">
										<xsl:attribute name="keep-together">always</xsl:attribute>
										<xsl:value-of select="//CommitmentLetter/IADAmount"/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>PRÊT</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>TERMES</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">11pt</xsl:attribute>
										<xsl:text>PAIEMENT - </xsl:text>
										<xsl:value-of select="//CommitmentLetter/PaymentFrequency"/>
									</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Montant:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Taux d'intérêt:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/InterestRate"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Capital et intérêts:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/PandIPayment"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Assurance:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/Premium"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Terme:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/PaymentTerm"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Taxes (approximatives):</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/TaxPortion"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Frais d'acceptation:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/AdminFee"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Amortissement:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/Amortization"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Versement total:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/TotalPayment"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Total du prêt:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">10pt</xsl:attribute>Veuillez indiquer le notaire mandaté(e) pour cette transaction:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>Nom du notaire:</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>Adresse:</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="font-size">10pt</xsl:attribute>Téléphone / Télécopieur:</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								</xsl:element>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">10pt</xsl:attribute>
									<xsl:attribute name="line-height">12pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:text>La signature de la présente Lettre d'engagement par le(s) emprunteur(s) atteste l'acceptation des conditions mentionnées dans celle-ci. La présente lettre d'engagement doit nous être retournée dûment signée au plus tard le </xsl:text>
									<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
									<xsl:text> à défaut de quoi  ladite lettre d'engagement devient nulle at non avenue.</xsl:text>
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/ClosingText/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
											However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">10pt</xsl:attribute>
												<xsl:attribute name="line-height">12pt</xsl:attribute>
												<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
												<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">10pt</xsl:attribute>
												<xsl:attribute name="line-height">12pt</xsl:attribute>
												<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
												<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
					</xsl:element>
					<!-- fo:table-body -->
				</xsl:element>
				<!-- fo:table -->
				<xsl:element name="fo:block">
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:attribute name="id">LastPage</xsl:attribute>
					<xsl:attribute name="line-height">1pt</xsl:attribute>
					<xsl:attribute name="font-size">1pt</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<!-- fo:flow -->
		</xsl:element>
		<!-- fo:page-sequence -->
	</xsl:template>
	<xsl:template name="FrenchPage2">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:call-template name="FrenchHeader"/>
			<xsl:call-template name="FrenchFooter"/>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:element name="fo:table">
					<xsl:attribute name="text-align">left</xsl:attribute>
					<xsl:attribute name="table-layout">fixed</xsl:attribute>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">1cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">18.55cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-body">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-top">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:block">
										<xsl:attribute name="space-after">6.0pt</xsl:attribute>
										<xsl:element name="fo:inline">
											<xsl:attribute name="text-decoration">underline</xsl:attribute>
											<xsl:attribute name="font-weight">bold</xsl:attribute>
											<xsl:attribute name="font-size">9pt</xsl:attribute>TERMES ET CONDITIONS:</xsl:element>
										<!-- fo:inline -->
									</xsl:element>
									<!-- fo:block -->
									<xsl:element name="fo:block">
										<xsl:attribute name="space-after">6.0pt</xsl:attribute>
										<xsl:element name="fo:inline">
											<xsl:attribute name="font-weight">bold</xsl:attribute>
											<xsl:attribute name="font-size">9pt</xsl:attribute>CETTE LETTRE D'ENGAGEMENT EST ASSUJETTIE AUX CONDITIONS SUIVANTES:</xsl:element>
										<!-- fo:inline -->
									</xsl:element>
									<!-- fo:block -->
									<xsl:element name="fo:block">
										<xsl:attribute name="space-after">6.0pt</xsl:attribute>
                    <xsl:text>Les conditions suivantes doivent être complétées 
                    et jugées satisfaisantes par la </xsl:text> 
                    <!--#DG520 xsl:text>Corporation 	Hypothécaire XCEED</xsl:text-->
                    <xsl:value-of select="$lenderName"/>
                    <xsl:text> au plus tard dix (10) jours avant le date de clôture du déboursé 
                      hypothécaire à défaut de quoi la présente lettre d'engagement pourrait 
                      être annulée.</xsl:text> 
                  </xsl:element>
									<!-- 	fo:block -->
									<xsl:for-each select="//CommitmentLetter/Conditions/Condition">
										<xsl:element name="fo:list-item">
											<xsl:element name="fo:list-item-label">
												<xsl:attribute name="end-indent">label-end()</xsl:attribute>
												<xsl:element name="fo:block">
													<xsl:value-of select="position()"/>
													<xsl:text>.</xsl:text>
												</xsl:element>
												<!-- fo:block -->
											</xsl:element>
											<!-- fo:list-item-label -->
											<xsl:element name="fo:list-item-body">
												<xsl:attribute name="start-indent">body-start()</xsl:attribute>
												<xsl:element name="fo:block">
													<xsl:attribute name="keep-together">always</xsl:attribute>
													<xsl:for-each select="./Line">
														<xsl:element name="fo:block">
															<xsl:choose>
																<!--  When there is a node with text in it (non-empty), then we display it normally.  
														However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
																<xsl:when test="* | text()">
																	<xsl:value-of select="."/>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:element name="fo:leader">
																		<xsl:attribute name="line-height">9pt</xsl:attribute>
																	</xsl:element>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:element>
														<!-- fo:block -->
													</xsl:for-each>
												</xsl:element>
												<!-- fo:block -->
												<xsl:element name="fo:block">
													<xsl:element name="fo:leader">
														<xsl:attribute name="line-height">9pt</xsl:attribute>
													</xsl:element>
												</xsl:element>
												<!-- fo:block -->
											</xsl:element>
											<!-- fo:list-item-body -->
										</xsl:element>
										<!-- fo:list-item -->
									</xsl:for-each>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:attribute name="font-size">9pt</xsl:attribute>CONDITIONS STANDARD:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>1.</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/VoidChq"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>2.</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/TitleInsurance/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
										However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>3.</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/FireInsurance/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
										However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>4.</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/TaxesPaid/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
										However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<!--					<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute> 
								<xsl:attribute name="padding-left">2mm</xsl:attribute> 
								<xsl:attribute name="padding-right">2mm</xsl:attribute> 
								<xsl:attribute name="border-left">solid Black</xsl:attribute> 
								<xsl:attribute name="border-right">solid Black</xsl:attribute> 
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute> 
								<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">12pt</xsl:attribute> 
									<xsl:attribute name="space-after">6pt</xsl:attribute> 
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute> 
										<xsl:attribute name="font-weight">bold</xsl:attribute>RATE GUARANTEE AND RATE ADJUSTMENT POLICIES:</xsl:element> <!- - fo:inline - ->
								</xsl:element> <!- - fo:block - ->
								
								<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
									<xsl:choose>
										<xsl:when 
										<xsl:when test="* | text()">	<xsl:element name="fo:block">
											<xsl:attribute name="font-size">9pt</xsl:attribute> 
											<xsl:attribute name="space-after">6pt</xsl:attribute><xsl:value-of 
											select="."/></xsl:element> <!- - fo:block - ->
										</xsl:when>
										<xsl:otherwise><xsl:element name="fo:block">
											<xsl:attribute name="font-size">9pt</xsl:attribute> 
											<xsl:attribute name="space-after">6pt</xsl:attribute><xsl:element name="fo:leader 
											<xsl:attribute name="line-height">9pt</xsl:attribute></xsl:element></xsl:element> <!- - fo:block - ->			
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								
							</xsl:element> <!- - fo:table-cell - ->
						</xsl:element> <!- - fo:table-row - ->-->
					</xsl:element>
					<!-- fo:table-body -->
				</xsl:element>
				<!-- fo:table -->
			</xsl:element>
			<!-- fo:flow -->
		</xsl:element>
		<!-- fo:page-sequence -->
	</xsl:template>
	<xsl:template name="FrenchPage3">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:call-template name="FrenchHeader"/>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="font-size">10pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:text>Page </xsl:text>
					<xsl:element name="fo:page-number"/>
					<xsl:text> de </xsl:text>
					<xsl:element name="fo:page-number-citation">
						<xsl:attribute name="ref-id">endofdoc</xsl:attribute>
					</xsl:element>
				</xsl:element>
				<!-- fo:block -->
			</xsl:element>
			<!-- fo:static-content -->
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:element name="fo:table">
					<xsl:attribute name="text-align">left</xsl:attribute>
					<xsl:attribute name="table-layout">fixed</xsl:attribute>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">6cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">10cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-column">
						<xsl:attribute name="column-width">3.55cm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:table-body">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-top">solid Black</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">6pt</xsl:attribute>
									<xsl:attribute name="space-after">6pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>TAUX GARANTI ET POLITIQUE DE RAJUSTEMENT DE TAUX:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
									<xsl:choose>
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>POLITIQUE DE PAIEMENT ANTICIPÊ:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/PrivilegePayment/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:for-each select="//CommitmentLetter/InterestCalc/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>CLÔTURE:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/Closing/Line">
									<xsl:element name="fo:block">
										<xsl:attribute name="font-size">9pt</xsl:attribute>
										<xsl:attribute name="space-after">6pt</xsl:attribute>
										<xsl:attribute name="text-align">justify</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:element>
									<!-- fo:block -->
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:inline">
										<xsl:attribute name="text-decoration">underline</xsl:attribute>
										<xsl:attribute name="font-weight">bold</xsl:attribute>ACCEPTATION:</xsl:element>
									<!-- fo:inline -->
								</xsl:element>
								<!-- fo:block -->
								<xsl:for-each select="//CommitmentLetter/Acceptance/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:attribute name="text-align">justify</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:element name="fo:block">
									<xsl:attribute name="text-align">right</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:attribute name="space-before">6pt</xsl:attribute>
									<xsl:attribute name="space-after">6pt</xsl:attribute>
                  <!--#DG520 Corporation Hypothécaire Xceed</xsl:text-->
                  <xsl:value-of select="$lenderName"/>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="text-align">right</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:attribute name="space-before">6pt</xsl:attribute>
									<xsl:attribute name="space-after">6pt</xsl:attribute>
									<xsl:text>Autorisé par: </xsl:text>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">75pt</xsl:attribute>
									</xsl:element>
									<xsl:value-of select="//CommitmentLetter/Underwriter"/>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">100pt</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
								<xsl:for-each select="//CommitmentLetter/Signature/Line">
									<xsl:choose>
										<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
										<xsl:when test="* | text()">
											<xsl:element name="fo:block">
												<xsl:attribute name="text-align">right</xsl:attribute>
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-before">6pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:value-of select="."/>
											</xsl:element>
											<!-- fo:block -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:element name="fo:block">
												<xsl:attribute name="text-align">right</xsl:attribute>
												<xsl:attribute name="font-size">9pt</xsl:attribute>
												<xsl:attribute name="space-before">6pt</xsl:attribute>
												<xsl:attribute name="space-after">6pt</xsl:attribute>
												<xsl:element name="fo:leader">
													<xsl:attribute name="line-height">9pt</xsl:attribute>
												</xsl:element>
											</xsl:element>
											<!-- fo:block -->
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">30pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>TÉMOIN</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">30pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>EMPRUNTEUR(S)</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">30pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>DATE</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>TÉMOIN</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>CO-EMPRUNTEUR(S)</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="font-size">9pt</xsl:attribute>DATE</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-left">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">12pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>TÉMOIN</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">12pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
								</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
							<xsl:element name="fo:table-cell">
								<xsl:attribute name="padding-top">1mm</xsl:attribute>
								<xsl:attribute name="padding-left">2mm</xsl:attribute>
								<xsl:attribute name="padding-right">2mm</xsl:attribute>
								<xsl:attribute name="border-right">solid Black</xsl:attribute>
								<xsl:attribute name="border-bottom">solid Black</xsl:attribute>
								<xsl:element name="fo:block">
									<xsl:attribute name="space-before">24pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>
									<xsl:element name="fo:leader">
										<xsl:attribute name="leader-pattern">rule</xsl:attribute>
										<xsl:attribute name="rule-thickness">1pt</xsl:attribute>
										<xsl:attribute name="color">black</xsl:attribute>
										<xsl:attribute name="leader-length">90%</xsl:attribute>
									</xsl:element>
								</xsl:element>
								<!-- fo:block -->
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">12pt</xsl:attribute>
									<xsl:attribute name="font-size">9pt</xsl:attribute>DATE</xsl:element>
								<!-- fo:block -->
							</xsl:element>
							<!-- fo:table-cell -->
						</xsl:element>
						<!-- fo:table-row -->
					</xsl:element>
					<!-- fo:table-body -->
				</xsl:element>
				<!-- fo:table -->
				<xsl:element name="fo:block">
					<xsl:attribute name="id">endofdoc</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<!-- fo:flow -->
		</xsl:element>
		<!-- fo:page-sequence -->
	</xsl:template>

	<xsl:template name="FOStart">
		<!-- #DG138 &lt;?xml version="1.0" encoding="ISO-8859-1"? >
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->		
		<xsl:element name="fo:layout-master-set">
			<xsl:element name="fo:simple-page-master">
				<xsl:attribute name="master-name">main</xsl:attribute>
				<xsl:attribute name="page-height">11in</xsl:attribute>
				<xsl:attribute name="page-width">8.5in</xsl:attribute>
				<xsl:attribute name="margin-top">1cm</xsl:attribute>
				<xsl:attribute name="margin-bottom">1cm</xsl:attribute>
				<xsl:attribute name="margin-left">1cm</xsl:attribute>
				<xsl:attribute name="margin-right">1cm</xsl:attribute>
				<xsl:element name="fo:region-before">
					<xsl:attribute name="extent">10mm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:region-body">
					<xsl:attribute name="margin-bottom">10mm</xsl:attribute>
					<xsl:attribute name="margin-top">5mm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:region-after">
					<xsl:attribute name="extent">10mm</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<!-- fo:simple-page-master -->
		</xsl:element>
		<!-- fo:layout-master-set -->
	</xsl:template>
	<xsl:template name="FOEnd">
		<!--/xsl:element> <!- - fo:root -->
	</xsl:template>
</xsl:stylesheet>
