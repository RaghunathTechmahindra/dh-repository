<?xml version="1.0" encoding="UTF-8"?>
<!--22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik-->
<!-- Author Zivko Radulovic -->
<!-- edited by Catherine Rutgaizer, March 2004 -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- ================  Attribute sets ============== -->
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<!-- ================  Attribute sets ============== -->
	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<!-- ========================== top level ================================  -->
	<xsl:template name="CreatePageThree">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					<!-- Page <fo:page-number/> of <fo:page-number-citation ref-id="page_3"/></xsl:element> -->
					Page 1 of 1</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage3"/>
				<xsl:call-template name="Page_3_Item_1"/>
				<fo:block id="page_3" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_3_Item_1">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>The loan will be granted under the following terms and conditions, in addition to those in use at the Caisse.</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute> -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 1. DOCUMENTS TO BE GIVEN TO THE NOTARY</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Upon receipt hereof, you are required to send the notary the following documents if they are available, and any other document the notary may ask for:</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<fo:list-block>
							<xsl:attribute name="start-indent">1mm</xsl:attribute>
							<xsl:attribute name="provisional-distance-between-starts">12mm</xsl:attribute>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										your marriage contract, where applicable;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ======================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										your deed of acquisition of the property, where applicable;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										-	a copy of the preliminary contract (purchase offer) and memorandum, where applicable;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										a certificate of location accepted by the Caisse or meeting the specifications on the first side of this form, where applicable;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										an insurance policy against fire and other perils for an amount sufficient to cover the loan, and the line of credit if applicable, or a coverage note, or in the case of a condominium, the certificate establishing that an insurance covering the co-owned property has been subscribed by the administrators;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										municipal and school tax receipts; and
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										in the case of a legal person, proof of its juridical existence and its power to take out the planned loan.
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
						</fo:list-block>
						<!-- =============================================================================-->
						<!-- GROUP 2  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 2. SIGNING OF THE DEED OF HYPOTHEC AND DISBURSEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>You shall be required to sign a deed of hypothecary loan based on the forms in use at the Caisse. The loan will be disbursed once the Caisse, tjas received satisfactory evidence that:</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<fo:list-block>
							<xsl:attribute name="start-indent">1mm</xsl:attribute>
							<xsl:attribute name="provisional-distance-between-starts">12mm</xsl:attribute>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										it holds a good and valid hypothec of the rank required;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ======================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>
										the property is insured against fire and other perils for an amount sufficient to cover the claim of the Caisse, and that the policy contains the'standard mortgage clause in favor of the Caisse;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										all taxes and charges that may affect the property have been paid;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="text-align">justify</xsl:attribute>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										any irregularity noted in the certificate of location has been corrected;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										the certificate of authorization required under the <fo:inline font-style="italic"> Environment Quality Act </fo:inline>(R.S.Q., c. Q-2) has been issued in the Borrower's name
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										all the terms and conditions applicable to the loan have been fulfilled.
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
						</fo:list-block>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>The line of credit, if any, shall be disbursed only if you avail yourself of the privilege conferred by the "Owner Protection" clause in accordance with the conditions set in the deed of hypothec.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 3  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 3. CONSTRUCTION, REPAIRS OR IMPROVEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>If the purpose of the loan is to build, repair or improve a property, the loan shall be advanced in successive disbursements as the work progresses. You will be required, prior to each disbursement, to provide the Caisse or the notary with a solemn declaration on legal construction hypothecs. If the work has been entrusted with a general contractor, disbursements may be effected through joint cheques made to your order jointly with the general contractor and, if applicable, with those persons who gave you notice of their contract with the general contractor, or to your order jointly with the persons with whom you have directly entered into a contract. If the work has not been entrusted with a general contractor, disbursements may be effected through cheques made jointly to your order and to that of any person with whom you have directly entered into a contract! Instead of ascertaining that the persons who may hold a legal construction hypothec have been paid as stipulated above, the Caisse may require that you provide it, prior to every disbursement, with a "Consent to Priority of Hypothec" signed in its favor by each and every such person. In all cases, the Caisse may make withholdings so that the value of the work exceed by 15%, in each case, the total of the disburse-. ments requested and the disbursements already advanced, with the last disbursement of 15% to be made 35 days after the completion of the work, all of which at the discretion of the Caisse.
							</xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							You should also note that, according to the Act <fo:inline font-style="italic">respecting labour relations, vocational training and manpower management in the construction industry </fo:inline>
							(R.S.Q., c. R-20), you will be held solidarity liable for the payment of any wages due to construction employees   working on the site, unless you make sure that the contractor or the general contractor with whom you contracted, directly or through an intermediary, holds the prescribed license issued by the Régie du bâtiment du Quebec.
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 4  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 4. PAYMENT OF INTEREST</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>If the loan is advanced in successive disbursements, the Caisse may at any time require that the accrued interest be paid regularty and in full. The interest accrued on the date of each disbursement may be deducted from the disbursement by the Caisse.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 5  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 5. DEADLINE FOR DISBURSEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>If the loan cannot be fully disbursed within three months of the dale of signing of the deed of hypothec, the Caisse may, upon giving written notice to that effect, refuse to make any further disbursement, but shall retain all its recourses with respect to the sums already advanced.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 6  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 6. COSTS</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>You shall pay the fees of the notary, surveyor and assessor, as well as publication charges, inspection costs, environmental verification costs and other costs of the same type. The Caisse may retain sufficient sums from the proceeds of the loan to pay them. You shall also pay, when the loan is repaid, with or without subrogation, the costs and fees of the notarial cancellation.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 7  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 7. DEBIT AUTHORIZATION</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Any sum due may be debited directly from one of your savings accounts at the Caisse.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 8  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 8. SALE PRICE OF THE PROPERTY</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>If the loan is used for the acquisition of a property, the Caisse reserves the right to reduce the amount of the loan if the sale price is lower than the one stipulated in the offer to buy.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 9  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 9. CHANGES IN YOUR FINANCIAL SITUATION</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							The Caisse reserves the right to withdraw this offer to finance should it find that your financial situation has significantly deteriorated since you filed your loan application.
						</xsl:element>
						<!-- =============================================================================-->
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="CreateTitleLinePage3">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">11cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								TERMS AND CONDITIONS
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!--   ================================================================= -->
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageThree"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
