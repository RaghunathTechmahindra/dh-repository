<!-- Venkata, FXP16891 - NBC CR007 Commitment Letter Changes  -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output version="1.0" method="html" encoding="UTF-8" indent="no"/>
	<xsl:variable name="fo:layout-master-set">
		<fo:layout-master-set>
			<fo:simple-page-master master-name="default-page" page-height="14in" page-width="8.5in" margin-left="0.5in" margin-right="0.48in">
				<fo:region-body margin-top="0.5in" margin-bottom="0.59in"/>
				<fo:region-after extent="0.59in"/>
			</fo:simple-page-master>
			<fo:simple-page-master master-name="CL_First_Page" page-height="14in" page-width="8.5in" margin-left="0.5in" margin-right="0.48in">
				<fo:region-body margin-top="1.4in" margin-bottom="0.59in"/>
				<fo:region-before region-name="RegionBeforeFirstPage" extent="1.4in"/>
				<fo:region-after extent="0.59in"/>
			</fo:simple-page-master>
			<fo:simple-page-master master-name="CL_Other_Page" page-height="14in" page-width="8.5in" margin-left="0.5in" margin-right="0.48in">
				<fo:region-body margin-top="1.2in" margin-bottom="0.59in"/>
				<fo:region-before region-name="RegionBeforeOtherPage" extent="1.2in"/>
				<fo:region-after extent="0.59in"/>
			</fo:simple-page-master>
			<fo:page-sequence-master master-name="CL_Page_Master">
				<fo:repeatable-page-master-alternatives>
					<fo:conditional-page-master-reference master-reference="CL_First_Page" page-position="first"/>
					<fo:conditional-page-master-reference master-reference="CL_Other_Page" page-position="rest"/>
				</fo:repeatable-page-master-alternatives>
			</fo:page-sequence-master>
		</fo:layout-master-set>
	</xsl:variable>
	<!-- ************************************************************* -->
	<!--  /CommitmentLetter :  NBC Solicitor package - English_Version -->
	<!-- ************************************************************* -->
	<xsl:template match="/">
		<fo:root>
			<xsl:variable name="maxwidth" select="7.52000"/>
			<xsl:variable name="docName" select="//CommitmentLetter/docName"/>
			<xsl:call-template name="CommitmentLetterEn"/><!-- will be called from NBC solicit.pkg too -->
		</fo:root>
	</xsl:template>
	<!-- *************************************************************** -->
	<!--  Document :  MORTGAGE COMMITMENT LETTER - 17975-->
	<!-- *************************************************************** -->
	<xsl:template name = "CommitmentLetterEn">
		<xsl:variable name="maxwidth" select="7.30000"/>
		<xsl:variable name="image-dir" select="//CommitmentLetter/Logo_Image"></xsl:variable>
			<xsl:copy-of select="$fo:layout-master-set"/>
			<xsl:variable name="page-sequenceId" select="concat('EOR', 'block1')" />
			<fo:page-sequence master-reference="CL_Page_Master" initial-page-number="1" format="1" force-page-count="no-force">
				<xsl:call-template name="footerall">
					<xsl:with-param name="footerText" select="'FX17975-002 (2006-10-01) (Canada)'"/>
					<xsl:with-param name="page-sequence" select="$page-sequenceId" />
				</xsl:call-template>
				<xsl:call-template name="firstpageheader">
					<xsl:with-param name="headingText" select="'MORTGAGE COMMITMENT LETTER'"/>
				</xsl:call-template>
				<xsl:call-template name="nonfirstpageheader">
					<xsl:with-param name="headingText" select="''"/>
				</xsl:call-template>
				<fo:flow flow-name="xsl-region-body">
					<fo:block>
						<xsl:variable name="tablewidth1" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths1" select="0.40625 + 0.04167 + 2.53125 + 0.04167 + 0.04167"/>
						<xsl:variable name="factor1">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths1 &gt; 0.00000 and $sumcolumnwidths1 &gt; $tablewidth1">
									<xsl:value-of select="$tablewidth1 div $sumcolumnwidths1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns1" select="1"/>
						<xsl:variable name="defaultcolumnwidth1">
							<xsl:choose>
								<xsl:when test="$factor1 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns1 &gt; 0">
									<xsl:value-of select="($tablewidth1 - $sumcolumnwidths1) div $defaultcolumns1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth1_0" select="0.40625 * $factor1"/>
						<xsl:variable name="columnwidth1_1" select="2.53125 * $factor1"/>
						<xsl:variable name="columnwidth1_2" select="$defaultcolumnwidth1"/>
						<fo:table width="{$tablewidth1}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth1_0}in"/>
							<fo:table-column column-width="{$columnwidth1_1}in"/>
							<fo:table-column column-width="{$columnwidth1_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding-top="0.02083in" padding-bottom="0.02083in" padding-right="0.02083in" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Date</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:value-of select="//CommitmentLetter/date1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth2" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths2" select="0.04167"/>
						<xsl:variable name="defaultcolumns2" select="1"/>
						<xsl:variable name="defaultcolumnwidth2">
							<xsl:choose>
								<xsl:when test="$defaultcolumns2 &gt; 0">
									<xsl:value-of select="($tablewidth2 - $sumcolumnwidths2) div $defaultcolumns2"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth2_0" select="$defaultcolumnwidth2"/>
						<fo:table width="{$tablewidth2}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth2_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt" text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-weight="bold" text-align="center">
												<xsl:text> Conditional approval of your Mortgage Application</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth3" select="$maxwidth * 1.00000 - 0.00521 - 0.00521"/>
						<xsl:variable name="sumcolumnwidths3" select="4.51042"/>
						<xsl:variable name="factor3">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths3 &gt; 0.00000 and $sumcolumnwidths3 &gt; $tablewidth3">
									<xsl:value-of select="$tablewidth3 div $sumcolumnwidths3"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns3" select="1"/>
						<xsl:variable name="defaultcolumnwidth3">
							<xsl:choose>
								<xsl:when test="$factor3 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns3 &gt; 0">
									<xsl:value-of select="($tablewidth3 - $sumcolumnwidths3) div $defaultcolumns3"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth3_0" select="4.51042 * $factor3"/>
						<xsl:variable name="columnwidth3_1" select="$defaultcolumnwidth3"/>
						<fo:table width="{$maxwidth}in">
							<fo:table-column column-width="{0.00521}in"/>
							<fo:table-column column-width="{$tablewidth3}in"/>
							<fo:table-column column-width="{0.00521}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<fo:table width="{$tablewidth3}in" space-before.optimum="1pt" space-after.optimum="2pt" border-top-style="solid" border-top-color="black" border-top-width="0.00521in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.00521in" border-left-style="solid" border-left-color="black" border-left-width="0.00521in" border-right-style="solid" border-right-color="black" border-right-width="0.00521in" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth3_0}in"/>
												<fo:table-column column-width="{$columnwidth3_1}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell font-size="10pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth4" select="$columnwidth3_0 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths4" select="1.26042"/>
																<xsl:variable name="factor4">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths4 &gt; 0.00000 and $sumcolumnwidths4 &gt; $tablewidth4">
																			<xsl:value-of select="$tablewidth4 div $sumcolumnwidths4"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns4" select="1"/>
																<xsl:variable name="defaultcolumnwidth4">
																	<xsl:choose>
																		<xsl:when test="$factor4 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns4 &gt; 0">
																			<xsl:value-of select="($tablewidth4 - $sumcolumnwidths4) div $defaultcolumns4"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth4_0" select="1.26042 * $factor4"/>
																<xsl:variable name="columnwidth4_1" select="$defaultcolumnwidth4"/>
																<fo:table width="{$tablewidth4}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth4_0}in"/>
																	<fo:table-column column-width="{$columnwidth4_1}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>National Bank</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Address of  MPC:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt">
																			<fo:block padding-top="1pt" padding-bottom="1pt" font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																					<fo:inline>
																						<xsl:value-of select="//branchAddress"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" height="0.26042in" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt" height="0.26042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell font-size="10pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth5" select="$columnwidth3_1 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths5" select="1.34375"/>
																<xsl:variable name="factor5">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths5 &gt; 0.00000 and $sumcolumnwidths5 &gt; $tablewidth5">
																			<xsl:value-of select="$tablewidth5 div $sumcolumnwidths5"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns5" select="1"/>
																<xsl:variable name="defaultcolumnwidth5">
																	<xsl:choose>
																		<xsl:when test="$factor5 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns5 &gt; 0">
																			<xsl:value-of select="($tablewidth5 - $sumcolumnwidths5) div $defaultcolumns5"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth5_0" select="1.34375 * $factor5"/>
																<xsl:variable name="columnwidth5_1" select="$defaultcolumnwidth5"/>
																<fo:table width="{$tablewidth5}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth5_0}in"/>
																	<fo:table-column column-width="{$columnwidth5_1}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Branch</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Address of branch: </xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" height="0.26042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>

																					</fo:inline>
																				</fo:block>

																			</fo:table-cell>
																			<fo:table-cell font-size="10pt" height="0.26042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell font-size="10pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth6" select="$columnwidth3_0 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths6" select="0.000"/>
																<xsl:variable name="defaultcolumns6" select="1"/>
																<xsl:variable name="defaultcolumnwidth6">
																	<xsl:choose>
																		<xsl:when test="$defaultcolumns6 &gt; 0">
																			<xsl:value-of select="($tablewidth6 - $sumcolumnwidths6) div $defaultcolumns6"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth6_0" select="$defaultcolumnwidth6"/>
																<fo:table width="{$tablewidth6}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth6_0}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Name of borrower: </xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:value-of select="//CommitmentLetter/borName5"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell font-size="10pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth7" select="$columnwidth3_1 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths7" select="0.000"/>
																<xsl:variable name="defaultcolumns7" select="1"/>
																<xsl:variable name="defaultcolumnwidth7">
																	<xsl:choose>
																		<xsl:when test="$defaultcolumns7 &gt; 0">
																			<xsl:value-of select="($tablewidth7 - $sumcolumnwidths7) div $defaultcolumns7"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth7_0" select="$defaultcolumnwidth7"/>
																<fo:table width="{$tablewidth7}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth7_0}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Address of the property:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:value-of select="//CommitmentLetter/propAddress"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell font-size="10pt" text-align="center" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Loan particulars</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell font-size="10pt" height="0.16667in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth8" select="$columnwidth3_0 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths8" select="1.04167 + 0.52083 + 0.15625"/>
																<xsl:variable name="factor8">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths8 &gt; 0.00000 and $sumcolumnwidths8 &gt; $tablewidth8">
																			<xsl:value-of select="$tablewidth8 div $sumcolumnwidths8"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns8" select="1"/>
																<xsl:variable name="defaultcolumnwidth8">
																	<xsl:choose>
																		<xsl:when test="$factor8 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns8 &gt; 0">
																			<xsl:value-of select="($tablewidth8 - $sumcolumnwidths8) div $defaultcolumns8"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth8_0" select="1.04167 * $factor8"/>
																<xsl:variable name="columnwidth8_1" select="0.52083 * $factor8"/>
																<xsl:variable name="columnwidth8_2" select="0.15625 * $factor8"/>
																<xsl:variable name="columnwidth8_3" select="$defaultcolumnwidth8"/>
																<fo:table width="{$tablewidth8}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth8_0}in"/>
																	<fo:table-column column-width="{$columnwidth8_1}in"/>
																	<fo:table-column column-width="{$columnwidth8_2}in"/>
																	<fo:table-column column-width="{$columnwidth8_3}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Loan amount:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:value-of select="//CommitmentLetter/princAmtBor"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell font-size="10pt" height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth9" select="$columnwidth3_1 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths9" select="1.97917"/>
																<xsl:variable name="factor9">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths9 &gt; 0.00000 and $sumcolumnwidths9 &gt; $tablewidth9">
																			<xsl:value-of select="$tablewidth9 div $sumcolumnwidths9"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns9" select="1"/>
																<xsl:variable name="defaultcolumnwidth9">
																	<xsl:choose>
																		<xsl:when test="$factor9 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns9 &gt; 0">
																			<xsl:value-of select="($tablewidth9 - $sumcolumnwidths9) div $defaultcolumns9"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth9_0" select="1.97917 * $factor9"/>
																<xsl:variable name="columnwidth9_1" select="$defaultcolumnwidth9"/>
																<fo:table width="{$tablewidth9}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth9_0}in"/>
																	<fo:table-column column-width="{$columnwidth9_1}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>This Commitment is valid until:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:value-of select="//CommitmentLetter/commitValUntil"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell font-size="10pt" height="0.16667in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth10" select="$columnwidth3_0 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths10" select="1.04167 + 0.52083 + 0.15625"/>
																<xsl:variable name="factor10">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths10 &gt; 0.00000 and $sumcolumnwidths10 &gt; $tablewidth10">
																			<xsl:value-of select="$tablewidth10 div $sumcolumnwidths10"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns10" select="1"/>
																<xsl:variable name="defaultcolumnwidth10">
																	<xsl:choose>
																		<xsl:when test="$factor10 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns10 &gt; 0">
																			<xsl:value-of select="($tablewidth10 - $sumcolumnwidths10) div $defaultcolumns10"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth10_0" select="1.04167 * $factor10"/>
																<xsl:variable name="columnwidth10_1" select="0.52083 * $factor10"/>
																<xsl:variable name="columnwidth10_2" select="0.15625 * $factor10"/>
																<xsl:variable name="columnwidth10_3" select="$defaultcolumnwidth10"/>
																<fo:table width="{$tablewidth10}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth10_0}in"/>
																	<fo:table-column column-width="{$columnwidth10_1}in"/>
																	<fo:table-column column-width="{$columnwidth10_2}in"/>
																	<fo:table-column column-width="{$columnwidth10_3}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="4">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<xsl:variable name="tablewidth11" select="$columnwidth10_0 * 1.00000 + $columnwidth10_1 * 1.00000 + $columnwidth10_2 * 1.00000 + $columnwidth10_3 * 1.00000"/>
																					<xsl:variable name="sumcolumnwidths11" select="3.71875 + 0.97917"/>
																					<xsl:variable name="factor11">
																						<xsl:choose>
																							<xsl:when test="$sumcolumnwidths11 &gt; 0.00000 and $sumcolumnwidths11 &gt; $tablewidth11">
																								<xsl:value-of select="$tablewidth11 div $sumcolumnwidths11"/>
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="1.000"/>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:variable>
																					<xsl:variable name="columnwidth11_0" select="3.71875 * $factor11"/>
																					<xsl:variable name="columnwidth11_1" select="0.97917 * $factor11"/>
																					<fo:table width="{$tablewidth11}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																						<fo:table-column column-width="{$columnwidth11_0}in"/>
																						<fo:table-column column-width="{$columnwidth11_1}in"/>
																						<fo:table-body>
																							<fo:table-row>
																								<fo:table-cell padding-left="0.05000in">
																									<fo:block padding-top="1pt" padding-bottom="1pt">
																										<fo:inline>
																											<xsl:text>Registered / published amount of the charge/hypothec: $</xsl:text>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																								<fo:table-cell>
																									<fo:block padding-top="1pt" padding-bottom="1pt">
																										<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																											<xsl:value-of select="//CommitmentLetter/regAmtCharge"/>
																										</fo:inline>
																									</fo:block>
																								</fo:table-cell>
																							</fo:table-row>
																						</fo:table-body>
																					</fo:table>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Purchase price:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:value-of select="//CommitmentLetter/purPrice"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Down payment:</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>$</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:value-of select="//CommitmentLetter/downPayment"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell font-size="10pt" height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth12" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths12" select="0.000"/>
						<xsl:variable name="defaultcolumns12" select="1"/>
						<xsl:variable name="defaultcolumnwidth12">
							<xsl:choose>
								<xsl:when test="$defaultcolumns12 &gt; 0">
									<xsl:value-of select="($tablewidth12 - $sumcolumnwidths12) div $defaultcolumns12"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth12_0" select="$defaultcolumnwidth12"/>
						<fo:table width="{$tablewidth12}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth12_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" height="0.18750in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-weight="bold">
												<xsl:text>Information to solicitor: Refer to Financing Conditions on page 4.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.25000in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:value-of select="//CommitmentLetter/infoSol"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.26042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.25000in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.26042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth13" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths13" select="0.000"/>
						<xsl:variable name="defaultcolumns13" select="1"/>
						<xsl:variable name="defaultcolumnwidth13">
							<xsl:choose>
								<xsl:when test="$defaultcolumns13 &gt; 0">
									<xsl:value-of select="($tablewidth13 - $sumcolumnwidths13) div $defaultcolumns13"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth13_0" select="$defaultcolumnwidth13"/>
						<fo:table width="{$tablewidth13}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth13_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-weight="bold">
												<xsl:text>If you have elected to have the loan divided into portions (the "Portions"), your loan has a Multi-Choice Option. If the loan is insured and your loan insurance premium is financed, it will be included in the loan amount.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-weight="bold">
												<xsl:text>The loan amount is the maximum amount made available to you by the Bank, notwithstanding any amount that will be registered/published on title, as more fully described in the table on page 2 of this Commitment.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth14" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths14" select="0.000"/>
						<xsl:variable name="defaultcolumns14" select="1"/>
						<xsl:variable name="defaultcolumnwidth14">
							<xsl:choose>
								<xsl:when test="$defaultcolumns14 &gt; 0">
									<xsl:value-of select="($tablewidth14 - $sumcolumnwidths14) div $defaultcolumns14"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth14_0" select="$defaultcolumnwidth14"/>
						<fo:table width="{$tablewidth14}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" text-align="center" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth14_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>LOAN AMOUNT BREAKDOWN</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth15" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths15" select="1.03125 + 2.25000 + 0.10417 + 0.93750 + 0.15625 + 0.59375"/>
						<xsl:variable name="factor15">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths15 &gt; 0.00000 and $sumcolumnwidths15 &gt; $tablewidth15">
									<xsl:value-of select="$tablewidth15 div $sumcolumnwidths15"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth15_0" select="1.03125 * $factor15"/>
						<xsl:variable name="columnwidth15_1" select="2.25000 * $factor15"/>
						<xsl:variable name="columnwidth15_2" select="0.10417 * $factor15"/>
						<xsl:variable name="columnwidth15_3" select="0.93750 * $factor15"/>
						<xsl:variable name="columnwidth15_4" select="0.15625 * $factor15"/>
						<xsl:variable name="columnwidth15_5" select="0.59375 * $factor15"/>
						<fo:table width="{$tablewidth15}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth15_0}in"/>
							<fo:table-column column-width="{$columnwidth15_1}in"/>
							<fo:table-column column-width="{$columnwidth15_2}in"/>
							<fo:table-column column-width="{$columnwidth15_3}in"/>
							<fo:table-column column-width="{$columnwidth15_4}in"/>
							<fo:table-column column-width="{$columnwidth15_5}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.15625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.16667in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.16667in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.05000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Basic Financing Amount:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.16667in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.16667in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:value-of select="//CommitmentLetter/availAmt"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.16667in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.16667in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.04167in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.04167in" number-columns-spanned="4" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.04167in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.18750in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.18750in" number-columns-spanned="4" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.05000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Additional Financing Amounts:</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.18750in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.03125in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.03125in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.03125in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.03125in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.03125in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.03125in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.16667in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.16667in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.05000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>&#x2022;</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>&#160;&#160;&#160;Loan Insurance Premium (CMHC or Genworth):&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.16667in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.16667in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:value-of select="//CommitmentLetter/amtDedPrinAmtBor"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.16667in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.16667in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.06250in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.06250in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.06250in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.06250in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.06250in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.06250in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.14583in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.14583in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth16" select="$columnwidth15_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths16" select="0.88542 + 1.77083 + 1.11458"/>
											<xsl:variable name="factor16">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths16 &gt; 0.00000 and $sumcolumnwidths16 &gt; $tablewidth16">
														<xsl:value-of select="$tablewidth16 div $sumcolumnwidths16"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth16_0" select="0.88542 * $factor16"/>
											<xsl:variable name="columnwidth16_1" select="1.77083 * $factor16"/>
											<xsl:variable name="columnwidth16_2" select="1.11458 * $factor16"/>
											<fo:table width="{$tablewidth16}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth16_0}in"/>
												<fo:table-column column-width="{$columnwidth16_1}in"/>
												<fo:table-column column-width="{$columnwidth16_2}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.06250in" padding-left="0.05000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>&#x2022;</xsl:text>
																</fo:inline>
																<fo:inline>
																	<xsl:text>&#160;&#160;&#160;Other: </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.14583in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.14583in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.14583in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.14583in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.05208in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.05208in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.12500in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.12500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.05000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text> Loan Amount:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.12500in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.12500in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="8pt">
																						<xsl:value-of select="//CommitmentLetter/loanAmount"/>
																					</fo:inline>
																				</fo:block>										</fo:table-cell>
									<fo:table-cell height="0.12500in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.12500in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.05000in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="after" height="0.15625in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell height="0.15625in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth17" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths17" select="7.47917"/>
						<xsl:variable name="factor17">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths17 &gt; 0.00000 and $sumcolumnwidths17 &gt; $tablewidth17">
									<xsl:value-of select="$tablewidth17 div $sumcolumnwidths17"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth17_0" select="7.47917 * $factor17"/>
						<fo:table width="{$tablewidth17}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth17_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>The table below contains the terms and conditions that will be applicable to your loan. The loan or each Portion, if the loan has been divided into Portions, will be governed by, and repayable in accordance with, the terms and conditions set out in the following table. </xsl:text>
											</fo:inline>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>(Please review the following information to make sure it reflects the decisions and choices you made when completing your mortgage application form with our representative. If you have any questions, please contact us at (1-888-483-5628).&#160; We will be pleased to assist you.)</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth18" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths18" select="0.28125 + 2.02083 + 2.44792 + 2.44792 + 2.43750"/>
						<xsl:variable name="factor18">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths18 &gt; 0.00000 and $sumcolumnwidths18 &gt; $tablewidth18">
									<xsl:value-of select="$tablewidth18 div $sumcolumnwidths18"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth18_0" select="0.28125 * $factor18"/>
						<xsl:variable name="columnwidth18_1" select="2.02083 * $factor18"/>
						<xsl:variable name="columnwidth18_2" select="2.44792 * $factor18"/>
						<xsl:variable name="columnwidth18_3" select="2.44792 * $factor18"/>
						<xsl:variable name="columnwidth18_4" select="2.43750 * $factor18"/>
						<fo:table width="{$tablewidth18}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth18_0}in"/>
							<fo:table-column column-width="{$columnwidth18_1}in"/>
							<fo:table-column column-width="{$columnwidth18_2}in"/>
							<fo:table-column column-width="{$columnwidth18_3}in"/>
							<fo:table-column column-width="{$columnwidth18_4}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>

									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center" >
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Terms and Conditions of your Financing</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 3</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.62500in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 1.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.62500in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Term - open/closed</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.22917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth19" select="$columnwidth18_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths19" select="0.53125 + 0.33333 + 0.39583 + 0.62500 + 0.20833 + 1.22917"/>
											<xsl:variable name="factor19">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths19 &gt; 0.00000 and $sumcolumnwidths19 &gt; $tablewidth19">
														<xsl:value-of select="$tablewidth19 div $sumcolumnwidths19"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth19_0" select="0.53125 * $factor19"/>
											<xsl:variable name="columnwidth19_1" select="0.33333 * $factor19"/>
											<xsl:variable name="columnwidth19_2" select="0.39583 * $factor19"/>
											<xsl:variable name="columnwidth19_3" select="0.62500 * $factor19"/>
											<xsl:variable name="columnwidth19_4" select="0.20833 * $factor19"/>
											<xsl:variable name="columnwidth19_5" select="1.22917 * $factor19"/>
											<fo:table width="{$tablewidth19}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth19_0}in"/>
												<fo:table-column column-width="{$columnwidth19_1}in"/>
												<fo:table-column column-width="{$columnwidth19_2}in"/>
												<fo:table-column column-width="{$columnwidth19_3}in"/>
												<fo:table-column column-width="{$columnwidth19_4}in"/>
												<fo:table-column column-width="{$columnwidth19_5}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/term2"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.13542in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.13542in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/openRateLoan = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.13542in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline >
																	<xsl:text>open</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.13542in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/closedRateLoan = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.13542in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
															<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
																<fo:inline>
																	<xsl:text>closed</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.22917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth20" select="$columnwidth18_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths20" select="0.53125 + 0.33333 + 0.39583 + 0.62500 + 0.20833 + 1.22917"/>
											<xsl:variable name="factor20">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths20 &gt; 0.00000 and $sumcolumnwidths20 &gt; $tablewidth20">
														<xsl:value-of select="$tablewidth20 div $sumcolumnwidths20"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth20_0" select="0.53125 * $factor20"/>
											<xsl:variable name="columnwidth20_1" select="0.33333 * $factor20"/>
											<xsl:variable name="columnwidth20_2" select="0.39583 * $factor20"/>
											<xsl:variable name="columnwidth20_3" select="0.62500 * $factor20"/>
											<xsl:variable name="columnwidth20_4" select="0.20833 * $factor20"/>
											<xsl:variable name="columnwidth20_5" select="1.22917 * $factor20"/>
											<fo:table width="{$tablewidth20}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth20_0}in"/>
												<fo:table-column column-width="{$columnwidth20_1}in"/>
												<fo:table-column column-width="{$columnwidth20_2}in"/>
												<fo:table-column column-width="{$columnwidth20_3}in"/>
												<fo:table-column column-width="{$columnwidth20_4}in"/>
												<fo:table-column column-width="{$columnwidth20_5}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		   <xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																		    <xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>open</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
															</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
																<fo:inline>
																	<xsl:text>closed</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.22917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth21" select="$columnwidth18_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths21" select="0.53125 + 0.33333 + 0.39583 + 0.62500 + 0.20833 + 1.22917"/>
											<xsl:variable name="factor21">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths21 &gt; 0.00000 and $sumcolumnwidths21 &gt; $tablewidth21">
														<xsl:value-of select="$tablewidth21 div $sumcolumnwidths21"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth21_0" select="0.53125 * $factor21"/>
											<xsl:variable name="columnwidth21_1" select="0.33333 * $factor21"/>
											<xsl:variable name="columnwidth21_2" select="0.39583 * $factor21"/>
											<xsl:variable name="columnwidth21_3" select="0.62500 * $factor21"/>
											<xsl:variable name="columnwidth21_4" select="0.20833 * $factor21"/>
											<xsl:variable name="columnwidth21_5" select="1.22917 * $factor21"/>
											<fo:table width="{$tablewidth21}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth21_0}in"/>
												<fo:table-column column-width="{$columnwidth21_1}in"/>
												<fo:table-column column-width="{$columnwidth21_2}in"/>
												<fo:table-column column-width="{$columnwidth21_3}in"/>
												<fo:table-column column-width="{$columnwidth21_4}in"/>
												<fo:table-column column-width="{$columnwidth21_5}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.14583in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
				<fo:block padding-top="1pt" padding-bottom="1pt">
					<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>open</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.16667in">
															<fo:block padding-top="1pt" padding-bottom="1pt">

																<fo:inline>
																	<xsl:text>&#160; closed</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.17708in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 2.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Amount</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth22" select="$columnwidth18_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths22" select="0.10417"/>
											<xsl:variable name="factor22">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths22 &gt; 0.00000 and $sumcolumnwidths22 &gt; $tablewidth22">
														<xsl:value-of select="$tablewidth22 div $sumcolumnwidths22"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns22" select="1"/>
											<xsl:variable name="defaultcolumnwidth22">
												<xsl:choose>
													<xsl:when test="$factor22 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns22 &gt; 0">
														<xsl:value-of select="($tablewidth22 - $sumcolumnwidths22) div $defaultcolumns22"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth22_0" select="0.10417 * $factor22"/>
											<xsl:variable name="columnwidth22_1" select="$defaultcolumnwidth22"/>
											<fo:table width="{$tablewidth22}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth22_0}in"/>
												<fo:table-column column-width="{$columnwidth22_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.13542in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.13542in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text><xsl:value-of select="//CommitmentLetter/princAmtBor"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth23" select="$columnwidth18_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths23" select="0.10417"/>
											<xsl:variable name="factor23">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths23 &gt; 0.00000 and $sumcolumnwidths23 &gt; $tablewidth23">
														<xsl:value-of select="$tablewidth23 div $sumcolumnwidths23"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns23" select="1"/>
											<xsl:variable name="defaultcolumnwidth23">
												<xsl:choose>
													<xsl:when test="$factor23 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns23 &gt; 0">
														<xsl:value-of select="($tablewidth23 - $sumcolumnwidths23) div $defaultcolumns23"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth23_0" select="0.10417 * $factor23"/>
											<xsl:variable name="columnwidth23_1" select="$defaultcolumnwidth23"/>
											<fo:table width="{$tablewidth23}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth23_0}in"/>
												<fo:table-column column-width="{$columnwidth23_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth24" select="$columnwidth18_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths24" select="0.10417"/>
											<xsl:variable name="factor24">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths24 &gt; 0.00000 and $sumcolumnwidths24 &gt; $tablewidth24">
														<xsl:value-of select="$tablewidth24 div $sumcolumnwidths24"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns24" select="1"/>
											<xsl:variable name="defaultcolumnwidth24">
												<xsl:choose>
													<xsl:when test="$factor24 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns24 &gt; 0">
														<xsl:value-of select="($tablewidth24 - $sumcolumnwidths24) div $defaultcolumns24"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth24_0" select="0.10417 * $factor24"/>
											<xsl:variable name="columnwidth24_1" select="$defaultcolumnwidth24"/>
											<fo:table width="{$tablewidth24}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth24_0}in"/>
												<fo:table-column column-width="{$columnwidth24_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.15625in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 3.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Interest adjustment date</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text><xsl:value-of select="//CommitmentLetter/intAdjDate"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>&#160; </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.15625in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>&#160; </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 4.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Amortization period</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth25" select="$columnwidth18_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths25" select="0.39583 + 0.39583 + 0.57292"/>
											<xsl:variable name="factor25">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths25 &gt; 0.00000 and $sumcolumnwidths25 &gt; $tablewidth25">
														<xsl:value-of select="$tablewidth25 div $sumcolumnwidths25"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth25_0" select="0.14 * $factor25"/>
											<xsl:variable name="columnwidth25_1" select="0.39583 * $factor25"/>
											<xsl:variable name="columnwidth25_2" select="0.57292 * $factor25"/>
											<fo:table width="{$tablewidth25}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth25_0}in"/>
												<fo:table-column column-width="{$columnwidth25_1}in"/>
												<fo:table-column column-width="{$columnwidth25_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.19792in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
														<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/amortPeriod"/>
																</fo:inline>
															</fo:block>														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth26" select="$columnwidth18_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths26" select="0.39583 + 0.39583 + 0.57292"/>
											<xsl:variable name="factor26">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths26 &gt; 0.00000 and $sumcolumnwidths26 &gt; $tablewidth26">
														<xsl:value-of select="$tablewidth26 div $sumcolumnwidths26"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth26_0" select="0.14 * $factor26"/>
											<xsl:variable name="columnwidth26_1" select="0.39583 * $factor26"/>
											<xsl:variable name="columnwidth26_2" select="0.57292 * $factor26"/>
											<fo:table width="{$tablewidth26}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth26_0}in"/>
												<fo:table-column column-width="{$columnwidth26_1}in"/>
												<fo:table-column column-width="{$columnwidth26_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.19792in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth27" select="$columnwidth18_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths27" select="0.39583 + 0.39583 + 0.57292"/>
											<xsl:variable name="factor27">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths27 &gt; 0.00000 and $sumcolumnwidths27 &gt; $tablewidth27">
														<xsl:value-of select="$tablewidth27 div $sumcolumnwidths27"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth27_0" select="0.14 * $factor27"/>
											<xsl:variable name="columnwidth27_1" select="0.39583 * $factor27"/>
											<xsl:variable name="columnwidth27_2" select="0.57292 * $factor27"/>
											<fo:table width="{$tablewidth27}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth27_0}in"/>
												<fo:table-column column-width="{$columnwidth27_1}in"/>
												<fo:table-column column-width="{$columnwidth27_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.19792in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 5.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>First payment date</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>&#160;&#160;&#160;&#160; </xsl:text><xsl:value-of select="//CommitmentLetter/dateFirstPayment"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>&#160; </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>&#160; </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<xsl:text>&#xA;</xsl:text>
						</fo:block>
						<xsl:variable name="tablewidth31" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths31" select="0.91667 + 10.50000"/>
						<xsl:variable name="factor31">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths31 &gt; 0.00000 and $sumcolumnwidths31 &gt; $tablewidth31">
									<xsl:value-of select="$tablewidth31 div $sumcolumnwidths31"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns31" select="1 + 1 + 1 + 1"/>
						<xsl:variable name="defaultcolumnwidth31">
							<xsl:choose>
								<xsl:when test="$factor31 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns31 &gt; 0">
									<xsl:value-of select="($tablewidth31 - $sumcolumnwidths31) div $defaultcolumns31"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth31_0" select="0.91667 * $factor31"/>
						<xsl:variable name="columnwidth31_1" select="$defaultcolumnwidth31"/>
						<xsl:variable name="columnwidth31_2" select="$defaultcolumnwidth31"/>
						<xsl:variable name="columnwidth31_3" select="$defaultcolumnwidth31"/>
						<xsl:variable name="columnwidth31_4" select="$defaultcolumnwidth31"/>
						<xsl:variable name="columnwidth31_5" select="10.50000 * $factor31"/>
						<fo:table width="{$tablewidth31}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth31_0}in"/>
							<fo:table-column column-width="{$columnwidth31_1}in"/>
							<fo:table-column column-width="{$columnwidth31_2}in"/>
							<fo:table-column column-width="{$columnwidth31_3}in"/>
							<fo:table-column column-width="{$columnwidth31_4}in"/>
							<fo:table-column column-width="{$columnwidth31_5}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
								</fo:table-row>
								<fo:table-row>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block break-after="page">
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth33" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths33" select="0.33333 + 2.02083 + 2.44792 + 2.44792 + 2.43750"/>
						<xsl:variable name="factor33">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths33 &gt; 0.00000 and $sumcolumnwidths33 &gt; $tablewidth33">
									<xsl:value-of select="$tablewidth33 div $sumcolumnwidths33"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth33_0" select="0.33333 * $factor33"/>
						<xsl:variable name="columnwidth33_1" select="2.02083 * $factor33"/>
						<xsl:variable name="columnwidth33_2" select="2.44792 * $factor33"/>
						<xsl:variable name="columnwidth33_3" select="2.44792 * $factor33"/>
						<xsl:variable name="columnwidth33_4" select="2.43750 * $factor33"/>
						<fo:table width="{$tablewidth33}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth33_0}in"/>
							<fo:table-column column-width="{$columnwidth33_1}in"/>
							<fo:table-column column-width="{$columnwidth33_2}in"/>
							<fo:table-column column-width="{$columnwidth33_3}in"/>
							<fo:table-column column-width="{$columnwidth33_4}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>

									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center" >
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Terms and Conditions of your Financing</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 3</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell height="0.39583in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 6.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.39583in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Payment frequency</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt" vertical-align="super">
												<xsl:text>1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.39583in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth28" select="$columnwidth18_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths28" select="0.84375 + 0.21875"/>
											<xsl:variable name="factor28">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths28 &gt; 0.00000 and $sumcolumnwidths28 &gt; $tablewidth28">
														<xsl:value-of select="$tablewidth28 div $sumcolumnwidths28"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns28" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth28">
												<xsl:choose>
													<xsl:when test="$factor28 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns28 &gt; 0">
														<xsl:value-of select="($tablewidth28 - $sumcolumnwidths28) div $defaultcolumns28"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth28_0" select="0.14 * $factor28"/>
											<xsl:variable name="columnwidth28_1" select="0.21875 * $factor28"/>
											<xsl:variable name="columnwidth28_2" select="$defaultcolumnwidth28"/>
											<xsl:variable name="columnwidth28_3" select="$defaultcolumnwidth28"/>
											<fo:table width="{$tablewidth28}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth28_0}in"/>
												<fo:table-column column-width="{$columnwidth28_1}in"/>
												<fo:table-column column-width="{$columnwidth28_2}in"/>
												<fo:table-column column-width="{$columnwidth28_3}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/paymentFreq1 = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>monthly</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
													<fo:table-cell text-align="center">
														<fo:block padding-top="1pt" padding-bottom="1pt"/>
													</fo:table-cell>
													<fo:table-cell text-align="center">
														<fo:block padding-top="1pt" padding-bottom="1pt">
															<fo:inline >
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/paymentFreq2 = '1' ">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
						</xsl:choose></fo:inline ></fo:block>														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>every 2 weeks</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
		<fo:block padding-top="1pt" padding-bottom="1pt">
																				<fo:inline >
															<xsl:choose>
							<xsl:when test="//CommitmentLetter/paymentFreq3 = '1' ">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose></fo:inline ></fo:block>														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>weekly</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.39583in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth29" select="$columnwidth18_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths29" select="0.84375 + 0.21875"/>
											<xsl:variable name="factor29">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths29 &gt; 0.00000 and $sumcolumnwidths29 &gt; $tablewidth29">
														<xsl:value-of select="$tablewidth29 div $sumcolumnwidths29"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns29" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth29">
												<xsl:choose>
													<xsl:when test="$factor29 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns29 &gt; 0">
														<xsl:value-of select="($tablewidth29 - $sumcolumnwidths29) div $defaultcolumns29"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth29_0" select="0.14 * $factor29"/>
											<xsl:variable name="columnwidth29_1" select="0.21875 * $factor29"/>
											<xsl:variable name="columnwidth29_2" select="$defaultcolumnwidth29"/>
											<xsl:variable name="columnwidth29_3" select="$defaultcolumnwidth29"/>
											<fo:table width="{$tablewidth29}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth29_0}in"/>
												<fo:table-column column-width="{$columnwidth29_1}in"/>
												<fo:table-column column-width="{$columnwidth29_2}in"/>
												<fo:table-column column-width="{$columnwidth29_3}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>monthly</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>every 2 weeks</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>weekly</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.39583in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth30" select="$columnwidth18_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths30" select="0.84375 + 0.21875"/>
											<xsl:variable name="factor30">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths30 &gt; 0.00000 and $sumcolumnwidths30 &gt; $tablewidth30">
														<xsl:value-of select="$tablewidth30 div $sumcolumnwidths30"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns30" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth30">
												<xsl:choose>
													<xsl:when test="$factor30 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns30 &gt; 0">
														<xsl:value-of select="($tablewidth30 - $sumcolumnwidths30) div $defaultcolumns30"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth30_0" select="0.14 * $factor30"/>
											<xsl:variable name="columnwidth30_1" select="0.21875 * $factor30"/>
											<xsl:variable name="columnwidth30_2" select="$defaultcolumnwidth30"/>
											<xsl:variable name="columnwidth30_3" select="$defaultcolumnwidth30"/>
											<fo:table width="{$tablewidth30}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth30_0}in"/>
												<fo:table-column column-width="{$columnwidth30_1}in"/>
												<fo:table-column column-width="{$columnwidth30_2}in"/>
												<fo:table-column column-width="{$columnwidth30_3}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>monthly</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>every 2 weeks</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>weekly</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 7.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Expiry date of term</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
	<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>&#160;&#160;&#160;&#160; </xsl:text><xsl:value-of select="//CommitmentLetter/matDate"/>
											</fo:inline>
										</fo:block>									</fo:table-cell>
									<fo:table-cell text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 8.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Periodic payment for property taxes(at frequency stated on Line 6)</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth34" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths34" select="0.21875"/>
											<xsl:variable name="factor34">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths34 &gt; 0.00000 and $sumcolumnwidths34 &gt; $tablewidth34">
														<xsl:value-of select="$tablewidth34 div $sumcolumnwidths34"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns34" select="1"/>
											<xsl:variable name="defaultcolumnwidth34">
												<xsl:choose>
													<xsl:when test="$factor34 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns34 &gt; 0">
														<xsl:value-of select="($tablewidth34 - $sumcolumnwidths34) div $defaultcolumns34"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth34_0" select="0.21875 * $factor34"/>
											<xsl:variable name="columnwidth34_1" select="$defaultcolumnwidth34"/>
											<fo:table width="{$tablewidth34}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth34_0}in"/>
												<fo:table-column column-width="{$columnwidth34_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/propTaxInstall"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth35" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths35" select="0.10417"/>
											<xsl:variable name="factor35">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths35 &gt; 0.00000 and $sumcolumnwidths35 &gt; $tablewidth35">
														<xsl:value-of select="$tablewidth35 div $sumcolumnwidths35"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns35" select="1"/>
											<xsl:variable name="defaultcolumnwidth35">
												<xsl:choose>
													<xsl:when test="$factor35 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns35 &gt; 0">
														<xsl:value-of select="($tablewidth35 - $sumcolumnwidths35) div $defaultcolumns35"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth35_0" select="0.10417 * $factor35"/>
											<xsl:variable name="columnwidth35_1" select="$defaultcolumnwidth35"/>
											<fo:table width="{$tablewidth35}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth35_0}in"/>
												<fo:table-column column-width="{$columnwidth35_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth36" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths36" select="0.10417"/>
											<xsl:variable name="factor36">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths36 &gt; 0.00000 and $sumcolumnwidths36 &gt; $tablewidth36">
														<xsl:value-of select="$tablewidth36 div $sumcolumnwidths36"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns36" select="1"/>
											<xsl:variable name="defaultcolumnwidth36">
												<xsl:choose>
													<xsl:when test="$factor36 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns36 &gt; 0">
														<xsl:value-of select="($tablewidth36 - $sumcolumnwidths36) div $defaultcolumns36"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth36_0" select="0.10417 * $factor36"/>
											<xsl:variable name="columnwidth36_1" select="$defaultcolumnwidth36"/>
											<fo:table width="{$tablewidth36}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth36_0}in"/>
												<fo:table-column column-width="{$columnwidth36_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text> 9.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Date of first interest rate revision (if applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:value-of select="//CommitmentLetter/intRevFreq"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>&#160; </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>&#160; </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>10.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Periodic administration fees (at frequency stated on Line&#160; 6)</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth37" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths37" select="0.20833"/>
											<xsl:variable name="factor37">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths37 &gt; 0.00000 and $sumcolumnwidths37 &gt; $tablewidth37">
														<xsl:value-of select="$tablewidth37 div $sumcolumnwidths37"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns37" select="1"/>
											<xsl:variable name="defaultcolumnwidth37">
												<xsl:choose>
													<xsl:when test="$factor37 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns37 &gt; 0">
														<xsl:value-of select="($tablewidth37 - $sumcolumnwidths37) div $defaultcolumns37"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth37_0" select="0.20833 * $factor37"/>
											<xsl:variable name="columnwidth37_1" select="$defaultcolumnwidth37"/>
											<fo:table width="{$tablewidth37}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth37_0}in"/>
												<fo:table-column column-width="{$columnwidth37_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/other1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth38" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths38" select="0.10417"/>
											<xsl:variable name="factor38">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths38 &gt; 0.00000 and $sumcolumnwidths38 &gt; $tablewidth38">
														<xsl:value-of select="$tablewidth38 div $sumcolumnwidths38"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns38" select="1"/>
											<xsl:variable name="defaultcolumnwidth38">
												<xsl:choose>
													<xsl:when test="$factor38 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns38 &gt; 0">
														<xsl:value-of select="($tablewidth38 - $sumcolumnwidths38) div $defaultcolumns38"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth38_0" select="0.10417 * $factor38"/>
											<xsl:variable name="columnwidth38_1" select="$defaultcolumnwidth38"/>
											<fo:table width="{$tablewidth38}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth38_0}in"/>
												<fo:table-column column-width="{$columnwidth38_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth39" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths39" select="0.10417"/>
											<xsl:variable name="factor39">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths39 &gt; 0.00000 and $sumcolumnwidths39 &gt; $tablewidth39">
														<xsl:value-of select="$tablewidth39 div $sumcolumnwidths39"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns39" select="1"/>
											<xsl:variable name="defaultcolumnwidth39">
												<xsl:choose>
													<xsl:when test="$factor39 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns39 &gt; 0">
														<xsl:value-of select="($tablewidth39 - $sumcolumnwidths39) div $defaultcolumns39"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth39_0" select="0.10417 * $factor39"/>
											<xsl:variable name="columnwidth39_1" select="$defaultcolumnwidth39"/>
											<fo:table width="{$tablewidth39}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth39_0}in"/>
												<fo:table-column column-width="{$columnwidth39_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.45833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>11.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.45833in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Insurance premium</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="10pt">
												<xsl:text>(at frequency stated on line&#160; 6)</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth40" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths40" select="0.20833"/>
											<xsl:variable name="factor40">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths40 &gt; 0.00000 and $sumcolumnwidths40 &gt; $tablewidth40">
														<xsl:value-of select="$tablewidth40 div $sumcolumnwidths40"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns40" select="1"/>
											<xsl:variable name="defaultcolumnwidth40">
												<xsl:choose>
													<xsl:when test="$factor40 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns40 &gt; 0">
														<xsl:value-of select="($tablewidth40 - $sumcolumnwidths40) div $defaultcolumns40"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth40_0" select="0.20833 * $factor40"/>
											<xsl:variable name="columnwidth40_1" select="$defaultcolumnwidth40"/>
											<fo:table width="{$tablewidth40}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth40_0}in"/>
												<fo:table-column column-width="{$columnwidth40_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/totInsPrem"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth41" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths41" select="0.10417"/>
											<xsl:variable name="factor41">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths41 &gt; 0.00000 and $sumcolumnwidths41 &gt; $tablewidth41">
														<xsl:value-of select="$tablewidth41 div $sumcolumnwidths41"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns41" select="1"/>
											<xsl:variable name="defaultcolumnwidth41">
												<xsl:choose>
													<xsl:when test="$factor41 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns41 &gt; 0">
														<xsl:value-of select="($tablewidth41 - $sumcolumnwidths41) div $defaultcolumns41"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth41_0" select="0.10417 * $factor41"/>
											<xsl:variable name="columnwidth41_1" select="$defaultcolumnwidth41"/>
											<fo:table width="{$tablewidth41}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth41_0}in"/>
												<fo:table-column column-width="{$columnwidth41_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth42" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths42" select="0.10417"/>
											<xsl:variable name="factor42">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths42 &gt; 0.00000 and $sumcolumnwidths42 &gt; $tablewidth42">
														<xsl:value-of select="$tablewidth42 div $sumcolumnwidths42"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns42" select="1"/>
											<xsl:variable name="defaultcolumnwidth42">
												<xsl:choose>
													<xsl:when test="$factor42 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns42 &gt; 0">
														<xsl:value-of select="($tablewidth42 - $sumcolumnwidths42) div $defaultcolumns42"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth42_0" select="0.10417 * $factor42"/>
											<xsl:variable name="columnwidth42_1" select="$defaultcolumnwidth42"/>
											<fo:table width="{$tablewidth42}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth42_0}in"/>
												<fo:table-column column-width="{$columnwidth42_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>12.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Promotional interest rate, promotion duration and corresponding payment amount (if applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth43" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths43" select="0.20833 + 0.68750 + 0.29167 + 0.41667 + 0.57292"/>
											<xsl:variable name="factor43">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths43 &gt; 0.00000 and $sumcolumnwidths43 &gt; $tablewidth43">
														<xsl:value-of select="$tablewidth43 div $sumcolumnwidths43"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth43_0" select="0.20833 * $factor43"/>
											<xsl:variable name="columnwidth43_1" select="0.68750 * $factor43"/>
											<xsl:variable name="columnwidth43_2" select="0.29167 * $factor43"/>
											<xsl:variable name="columnwidth43_3" select="0.41667 * $factor43"/>
											<xsl:variable name="columnwidth43_4" select="0.57292 * $factor43"/>
											<fo:table width="{$tablewidth43}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth43_0}in"/>
												<fo:table-column column-width="{$columnwidth43_1}in"/>
												<fo:table-column column-width="{$columnwidth43_2}in"/>
												<fo:table-column column-width="{$columnwidth43_3}in"/>
												<fo:table-column column-width="{$columnwidth43_4}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:value-of select="//CommitmentLetter/teaserRate"/>
																					</fo:inline>
																				</fo:block>														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
	<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:value-of select="//CommitmentLetter/teaserTerm"/>
																					</fo:inline>
																				</fo:block>														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="3">
	<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:value-of select="//CommitmentLetter/teaserPayAmt"/>
																					</fo:inline>
																				</fo:block>														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth44" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths44" select="0.20833 + 0.68750 + 0.29167 + 0.41667 + 0.57292"/>
											<xsl:variable name="factor44">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths44 &gt; 0.00000 and $sumcolumnwidths44 &gt; $tablewidth44">
														<xsl:value-of select="$tablewidth44 div $sumcolumnwidths44"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth44_0" select="0.20833 * $factor44"/>
											<xsl:variable name="columnwidth44_1" select="0.68750 * $factor44"/>
											<xsl:variable name="columnwidth44_2" select="0.29167 * $factor44"/>
											<xsl:variable name="columnwidth44_3" select="0.41667 * $factor44"/>
											<xsl:variable name="columnwidth44_4" select="0.57292 * $factor44"/>
											<fo:table width="{$tablewidth44}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth44_0}in"/>
												<fo:table-column column-width="{$columnwidth44_1}in"/>
												<fo:table-column column-width="{$columnwidth44_2}in"/>
												<fo:table-column column-width="{$columnwidth44_3}in"/>
												<fo:table-column column-width="{$columnwidth44_4}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="right" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="3">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth45" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths45" select="0.20833 + 0.68750 + 0.29167 + 0.41667 + 0.57292"/>
											<xsl:variable name="factor45">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths45 &gt; 0.00000 and $sumcolumnwidths45 &gt; $tablewidth45">
														<xsl:value-of select="$tablewidth45 div $sumcolumnwidths45"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth45_0" select="0.20833 * $factor45"/>
											<xsl:variable name="columnwidth45_1" select="0.68750 * $factor45"/>
											<xsl:variable name="columnwidth45_2" select="0.29167 * $factor45"/>
											<xsl:variable name="columnwidth45_3" select="0.41667 * $factor45"/>
											<xsl:variable name="columnwidth45_4" select="0.57292 * $factor45"/>
											<fo:table width="{$tablewidth45}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth45_0}in"/>
												<fo:table-column column-width="{$columnwidth45_1}in"/>
												<fo:table-column column-width="{$columnwidth45_2}in"/>
												<fo:table-column column-width="{$columnwidth45_3}in"/>
												<fo:table-column column-width="{$columnwidth45_4}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="right" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="3">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.01042in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>13.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Automatic renewal</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-left="5pt" height="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth46" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths46" select="0.44792"/>
											<xsl:variable name="factor46">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths46 &gt; 0.00000 and $sumcolumnwidths46 &gt; $tablewidth46">
														<xsl:value-of select="$tablewidth46 div $sumcolumnwidths46"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns46" select="0.5 + 0.3 + 0.5 + 0.3"/>
											<xsl:variable name="defaultcolumnwidth46">
												<xsl:choose>
													<xsl:when test="$factor46 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns46 &gt; 0">
														<xsl:value-of select="($tablewidth46 - $sumcolumnwidths46) div $defaultcolumns46"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>

											<xsl:variable name="columnwidth46_1" select="0.4 * $factor46"/>
											<xsl:variable name="columnwidth46_2" select="0.4 * $factor46"/>
											<xsl:variable name="columnwidth46_3" select="0.2 * $factor46"/>
											<xsl:variable name="columnwidth46_4" select="0.6 * $factor46"/>
											<fo:table width="{$tablewidth46}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">

												<fo:table-column column-width="{$columnwidth46_1}in"/>
												<fo:table-column column-width="{$columnwidth46_2}in"/>
												<fo:table-column column-width="{$columnwidth46_3}in"/>
												<fo:table-column column-width="{$columnwidth46_4}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
											  					<xsl:choose>
																	<xsl:when test="//CommitmentLetter/intRateRenewAuto1 = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt" >
																	<xsl:text>Yes</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/intRateRenNotAuto1 = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>No</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-left="5pt" height="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth47" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths47" select="0.44792"/>
											<xsl:variable name="factor47">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths47 &gt; 0.00000 and $sumcolumnwidths47 &gt; $tablewidth47">
														<xsl:value-of select="$tablewidth47 div $sumcolumnwidths47"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns47" select="0.5 + 0.3 + 0.5 + 0.3"/>
											<xsl:variable name="defaultcolumnwidth47">
												<xsl:choose>
													<xsl:when test="$factor47 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns47 &gt; 0">
														<xsl:value-of select="($tablewidth47 - $sumcolumnwidths47) div $defaultcolumns47"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>

											<xsl:variable name="columnwidth47_1" select="0.4 * $factor47"/>
											<xsl:variable name="columnwidth47_2" select="0.4 * $factor47"/>
											<xsl:variable name="columnwidth47_3" select="0.2 * $factor47"/>
											<xsl:variable name="columnwidth47_4" select="0.6 * $factor47"/>
											<fo:table width="{$tablewidth47}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth47_1}in"/>
												<fo:table-column column-width="{$columnwidth47_2}in"/>
												<fo:table-column column-width="{$columnwidth47_3}in"/>
												<fo:table-column column-width="{$columnwidth47_4}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>Yes</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
															  	<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>No</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-left="2pt" height="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth48" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths48" select="0.44792"/>
											<xsl:variable name="factor48">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths48 &gt; 0.00000 and $sumcolumnwidths48 &gt; $tablewidth48">
														<xsl:value-of select="$tablewidth48 div $sumcolumnwidths48"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns48" select="0.5 + 0.3 + 0.5 + 0.3"/>
											<xsl:variable name="defaultcolumnwidth48">
												<xsl:choose>
													<xsl:when test="$factor48 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns48 &gt; 0">
														<xsl:value-of select="($tablewidth48 - $sumcolumnwidths48) div $defaultcolumns48"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth48_1" select="0.4 * $factor48"/>
											<xsl:variable name="columnwidth48_2" select="0.4 * $factor48"/>
											<xsl:variable name="columnwidth48_3" select="0.2 * $factor48"/>
											<xsl:variable name="columnwidth48_4" select="0.6 * $factor48"/>
											<fo:table width="{$tablewidth48}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth48_1}in"/>
												<fo:table-column column-width="{$columnwidth48_2}in"/>
												<fo:table-column column-width="{$columnwidth48_3}in"/>
												<fo:table-column column-width="{$columnwidth48_4}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
													 			<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>&#160;Yes</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
													   			<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>No</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.18750in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>14.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.18750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Cash back (if applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.18750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth49" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths49" select="0.32292 + 2.38542"/>
											<xsl:variable name="factor49">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths49 &gt; 0.00000 and $sumcolumnwidths49 &gt; $tablewidth49">
														<xsl:value-of select="$tablewidth49 div $sumcolumnwidths49"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns49" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth49">
												<xsl:choose>
													<xsl:when test="$factor49 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns49 &gt; 0">
														<xsl:value-of select="($tablewidth49 - $sumcolumnwidths49) div $defaultcolumns49"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth49_0" select="0.32292 * $factor49"/>
											<xsl:variable name="columnwidth49_1" select="$defaultcolumnwidth49"/>
											<xsl:variable name="columnwidth49_2" select="2.38542 * $factor49"/>
											<xsl:variable name="columnwidth49_3" select="$defaultcolumnwidth49"/>
											<fo:table width="{$tablewidth49}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth49_0}in"/>
												<fo:table-column column-width="{$columnwidth49_1}in"/>
												<fo:table-column column-width="{$columnwidth49_2}in"/>
												<fo:table-column column-width="{$columnwidth49_3}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.15625in" number-columns-spanned="2" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/cashbackAmt1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.18750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth49" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths49" select="0.32292 + 2.38542"/>
											<xsl:variable name="factor49">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths49 &gt; 0.00000 and $sumcolumnwidths49 &gt; $tablewidth49">
														<xsl:value-of select="$tablewidth49 div $sumcolumnwidths49"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns49" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth49">
												<xsl:choose>
													<xsl:when test="$factor49 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns49 &gt; 0">
														<xsl:value-of select="($tablewidth49 - $sumcolumnwidths49) div $defaultcolumns49"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth49_0" select="0.32292 * $factor49"/>
											<xsl:variable name="columnwidth49_1" select="$defaultcolumnwidth49"/>
											<xsl:variable name="columnwidth49_2" select="2.38542 * $factor49"/>
											<xsl:variable name="columnwidth49_3" select="$defaultcolumnwidth49"/>
											<fo:table width="{$tablewidth49}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth49_0}in"/>
												<fo:table-column column-width="{$columnwidth49_1}in"/>
												<fo:table-column column-width="{$columnwidth49_2}in"/>
												<fo:table-column column-width="{$columnwidth49_3}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.15625in" number-columns-spanned="2" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160;&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.18750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth49" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths49" select="0.32292 + 2.38542"/>
											<xsl:variable name="factor49">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths49 &gt; 0.00000 and $sumcolumnwidths49 &gt; $tablewidth49">
														<xsl:value-of select="$tablewidth49 div $sumcolumnwidths49"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns49" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth49">
												<xsl:choose>
													<xsl:when test="$factor49 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns49 &gt; 0">
														<xsl:value-of select="($tablewidth49 - $sumcolumnwidths49) div $defaultcolumns49"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth49_0" select="0.32292 * $factor49"/>
											<xsl:variable name="columnwidth49_1" select="$defaultcolumnwidth49"/>
											<xsl:variable name="columnwidth49_2" select="2.38542 * $factor49"/>
											<xsl:variable name="columnwidth49_3" select="$defaultcolumnwidth49"/>
											<fo:table width="{$tablewidth49}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth49_0}in"/>
												<fo:table-column column-width="{$columnwidth49_1}in"/>
												<fo:table-column column-width="{$columnwidth49_2}in"/>
												<fo:table-column column-width="{$columnwidth49_3}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.15625in" number-columns-spanned="2" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160;&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="center" height="0.22917in" number-columns-spanned="5" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="3pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>The following are additional terms and conditions only applicable to a loan with a Fixed Interest Rate</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.21875in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>15.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.52083in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Fixed interest rate (calculated semi-annually and not in advance)</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.34375in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth50" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths50" select="0.46875 + 0.17708 + 0.46875 + 0.10417 + 0.46875 + 0.13542"/>
											<xsl:variable name="factor50">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths50 &gt; 0.00000 and $sumcolumnwidths50 &gt; $tablewidth50">
														<xsl:value-of select="$tablewidth50 div $sumcolumnwidths50"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth50_0" select="0.46875 * $factor50"/>
											<xsl:variable name="columnwidth50_1" select="0.17708 * $factor50"/>
											<xsl:variable name="columnwidth50_2" select="0.46875 * $factor50"/>
											<xsl:variable name="columnwidth50_3" select="0.10417 * $factor50"/>
											<xsl:variable name="columnwidth50_4" select="0.46875 * $factor50"/>
											<xsl:variable name="columnwidth50_5" select="0.13542 * $factor50"/>
											<fo:table width="{$tablewidth50}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth50_0}in"/>
												<fo:table-column column-width="{$columnwidth50_1}in"/>
												<fo:table-column column-width="{$columnwidth50_2}in"/>
												<fo:table-column column-width="{$columnwidth50_3}in"/>
												<fo:table-column column-width="{$columnwidth50_4}in"/>
												<fo:table-column column-width="{$columnwidth50_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
	<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="10pt">
																						<xsl:value-of select="//CommitmentLetter/intRateFixed"/>
																					</fo:inline>
																				</fo:block>														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text> -</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
	<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="10pt">
																						<xsl:value-of select="//CommitmentLetter/discount"/>
																					</fo:inline>
																				</fo:block>														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text> =</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
	<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="10pt">
																						<xsl:value-of select="//CommitmentLetter/fxRate"/>
																					</fo:inline>
																				</fo:block>														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Posted&#160; </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>rate</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Rate </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>reduction</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.34375in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth51" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths51" select="0.46875 + 0.17708 + 0.46875 + 0.10417 + 0.46875 + 0.13542"/>
											<xsl:variable name="factor51">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths51 &gt; 0.00000 and $sumcolumnwidths51 &gt; $tablewidth51">
														<xsl:value-of select="$tablewidth51 div $sumcolumnwidths51"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth51_0" select="0.46875 * $factor51"/>
											<xsl:variable name="columnwidth51_1" select="0.17708 * $factor51"/>
											<xsl:variable name="columnwidth51_2" select="0.46875 * $factor51"/>
											<xsl:variable name="columnwidth51_3" select="0.10417 * $factor51"/>
											<xsl:variable name="columnwidth51_4" select="0.46875 * $factor51"/>
											<xsl:variable name="columnwidth51_5" select="0.13542 * $factor51"/>
											<fo:table width="{$tablewidth51}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth51_0}in"/>
												<fo:table-column column-width="{$columnwidth51_1}in"/>
												<fo:table-column column-width="{$columnwidth51_2}in"/>
												<fo:table-column column-width="{$columnwidth51_3}in"/>
												<fo:table-column column-width="{$columnwidth51_4}in"/>
												<fo:table-column column-width="{$columnwidth51_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text> -</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text> =</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Posted&#160; </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>rate</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Rate </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>reduction</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.34375in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth52" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths52" select="0.46875 + 0.17708 + 0.46875 + 0.10417 + 0.46875 + 0.13542"/>
											<xsl:variable name="factor52">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths52 &gt; 0.00000 and $sumcolumnwidths52 &gt; $tablewidth52">
														<xsl:value-of select="$tablewidth52 div $sumcolumnwidths52"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth52_0" select="0.46875 * $factor52"/>
											<xsl:variable name="columnwidth52_1" select="0.17708 * $factor52"/>
											<xsl:variable name="columnwidth52_2" select="0.46875 * $factor52"/>
											<xsl:variable name="columnwidth52_3" select="0.10417 * $factor52"/>
											<xsl:variable name="columnwidth52_4" select="0.46875 * $factor52"/>
											<xsl:variable name="columnwidth52_5" select="0.13542 * $factor52"/>
											<fo:table width="{$tablewidth52}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth52_0}in"/>
												<fo:table-column column-width="{$columnwidth52_1}in"/>
												<fo:table-column column-width="{$columnwidth52_2}in"/>
												<fo:table-column column-width="{$columnwidth52_3}in"/>
												<fo:table-column column-width="{$columnwidth52_4}in"/>
												<fo:table-column column-width="{$columnwidth52_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text> -</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text> =</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Posted&#160; </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>rate</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Rate </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline font-size="6pt">
																	<xsl:text>reduction</xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell font-size="8pt" height="0.21875in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>15.1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.52083in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt" >
											<fo:inline>
												<xsl:text>Blended rate (calculated semi-annually and not in advance), amount of indemnity financed by applying blended rate and corresponding payment amount (if applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.34375in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth53" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths53" select="0.32292 + 0.87500 + 0.76042 + 0.22917 + 0.94792 + 0.29167"/>
											<xsl:variable name="factor53">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths53 &gt; 0.00000 and $sumcolumnwidths53 &gt; $tablewidth53">
														<xsl:value-of select="$tablewidth53 div $sumcolumnwidths53"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth53_0" select="0.32292 * $factor53"/>
											<xsl:variable name="columnwidth53_1" select="0.87500 * $factor53"/>
											<xsl:variable name="columnwidth53_2" select="0.76042 * $factor53"/>
											<xsl:variable name="columnwidth53_3" select="0.22917 * $factor53"/>
											<xsl:variable name="columnwidth53_4" select="0.94792 * $factor53"/>
											<xsl:variable name="columnwidth53_5" select="0.29167 * $factor53"/>
											<fo:table width="{$tablewidth53}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth53_0}in"/>
												<fo:table-column column-width="{$columnwidth53_1}in"/>
												<fo:table-column column-width="{$columnwidth53_2}in"/>
												<fo:table-column column-width="{$columnwidth53_3}in"/>
												<fo:table-column column-width="{$columnwidth53_4}in"/>
												<fo:table-column column-width="{$columnwidth53_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.02083in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>&#160;&#160;&#160; %</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell display-align="after" height="0.20833in" text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell display-align="after" height="0.05208in" text-align="right" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="left" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="10pt">
																						<xsl:value-of select="//CommitmentLetter/refPaymentIC"/>
																					</fo:inline>
																				</fo:block>															</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.34375in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth54" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths54" select="0.32292 + 0.87500 + 0.76042 + 0.22917 + 0.94792 + 0.29167"/>
											<xsl:variable name="factor54">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths54 &gt; 0.00000 and $sumcolumnwidths54 &gt; $tablewidth54">
														<xsl:value-of select="$tablewidth54 div $sumcolumnwidths54"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth54_0" select="0.32292 * $factor54"/>
											<xsl:variable name="columnwidth54_1" select="0.87500 * $factor54"/>
											<xsl:variable name="columnwidth54_2" select="0.76042 * $factor54"/>
											<xsl:variable name="columnwidth54_3" select="0.22917 * $factor54"/>
											<xsl:variable name="columnwidth54_4" select="0.94792 * $factor54"/>
											<xsl:variable name="columnwidth54_5" select="0.29167 * $factor54"/>
											<fo:table width="{$tablewidth54}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth54_0}in"/>
												<fo:table-column column-width="{$columnwidth54_1}in"/>
												<fo:table-column column-width="{$columnwidth54_2}in"/>
												<fo:table-column column-width="{$columnwidth54_3}in"/>
												<fo:table-column column-width="{$columnwidth54_4}in"/>
												<fo:table-column column-width="{$columnwidth54_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.02083in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>&#160;&#160;&#160; %</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell display-align="after" height="0.20833in" text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell display-align="after" height="0.05208in" text-align="right" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="left" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.34375in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth55" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths55" select="0.32292 + 0.87500 + 0.76042 + 0.22917 + 0.94792 + 0.29167"/>
											<xsl:variable name="factor55">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths55 &gt; 0.00000 and $sumcolumnwidths55 &gt; $tablewidth55">
														<xsl:value-of select="$tablewidth55 div $sumcolumnwidths55"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth55_0" select="0.32292 * $factor55"/>
											<xsl:variable name="columnwidth55_1" select="0.87500 * $factor55"/>
											<xsl:variable name="columnwidth55_2" select="0.76042 * $factor55"/>
											<xsl:variable name="columnwidth55_3" select="0.22917 * $factor55"/>
											<xsl:variable name="columnwidth55_4" select="0.94792 * $factor55"/>
											<xsl:variable name="columnwidth55_5" select="0.29167 * $factor55"/>
											<fo:table width="{$tablewidth55}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth55_0}in"/>
												<fo:table-column column-width="{$columnwidth55_1}in"/>
												<fo:table-column column-width="{$columnwidth55_2}in"/>
												<fo:table-column column-width="{$columnwidth55_3}in"/>
												<fo:table-column column-width="{$columnwidth55_4}in"/>
												<fo:table-column column-width="{$columnwidth55_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.02083in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text>&#160;&#160;&#160; %</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.02083in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell display-align="after" height="0.20833in" text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.20833in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell display-align="after" height="0.05208in" text-align="right" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="left" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" text-align="center" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.05208in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.21875in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>16.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.52083in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Amount of each periodic payment </xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline>
												<xsl:text>(principal + interest)</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth56" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths56" select="0.20833"/>
											<xsl:variable name="factor56">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths56 &gt; 0.00000 and $sumcolumnwidths56 &gt; $tablewidth56">
														<xsl:value-of select="$tablewidth56 div $sumcolumnwidths56"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns56" select="1"/>
											<xsl:variable name="defaultcolumnwidth56">
												<xsl:choose>
													<xsl:when test="$factor56 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns56 &gt; 0">
														<xsl:value-of select="($tablewidth56 - $sumcolumnwidths56) div $defaultcolumns56"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth56_0" select="0.20833 * $factor56"/>
											<xsl:variable name="columnwidth56_1" select="$defaultcolumnwidth56"/>
											<fo:table width="{$tablewidth56}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth56_0}in"/>
												<fo:table-column column-width="{$columnwidth56_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/monPayment"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth57" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths57" select="0.10417"/>
											<xsl:variable name="factor57">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths57 &gt; 0.00000 and $sumcolumnwidths57 &gt; $tablewidth57">
														<xsl:value-of select="$tablewidth57 div $sumcolumnwidths57"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns57" select="1"/>
											<xsl:variable name="defaultcolumnwidth57">
												<xsl:choose>
													<xsl:when test="$factor57 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns57 &gt; 0">
														<xsl:value-of select="($tablewidth57 - $sumcolumnwidths57) div $defaultcolumns57"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth57_0" select="0.10417 * $factor57"/>
											<xsl:variable name="columnwidth57_1" select="$defaultcolumnwidth57"/>
											<fo:table width="{$tablewidth57}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth57_0}in"/>
												<fo:table-column column-width="{$columnwidth57_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth58" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths58" select="0.10417"/>
											<xsl:variable name="factor58">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths58 &gt; 0.00000 and $sumcolumnwidths58 &gt; $tablewidth58">
														<xsl:value-of select="$tablewidth58 div $sumcolumnwidths58"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns58" select="1"/>
											<xsl:variable name="defaultcolumnwidth58">
												<xsl:choose>
													<xsl:when test="$factor58 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns58 &gt; 0">
														<xsl:value-of select="($tablewidth58 - $sumcolumnwidths58) div $defaultcolumns58"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth58_0" select="0.10417 * $factor58"/>
											<xsl:variable name="columnwidth58_1" select="$defaultcolumnwidth58"/>
											<fo:table width="{$tablewidth58}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth58_0}in"/>
												<fo:table-column column-width="{$columnwidth58_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="center" height="0.21875in" number-columns-spanned="5" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="3pt" padding-bottom="1pt">
											<fo:inline font-weight="bold">
												<xsl:text>The following are additional terms and conditions only applicable to a loan with a Variable Interest Rate</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.21875in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>17.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.52083in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Mortgage Rate (MR)</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>3</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt">
												<xsl:text>&#160;</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>: (based on a closed term mortgage loan of :)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth59" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths59" select="0.39583 + 0.39583 + 0.57292"/>
											<xsl:variable name="factor59">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths59 &gt; 0.00000 and $sumcolumnwidths59 &gt; $tablewidth59">
														<xsl:value-of select="$tablewidth59 div $sumcolumnwidths59"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth59_0" select="0.39583 * $factor59"/>
											<xsl:variable name="columnwidth59_1" select="0.39583 * $factor59"/>
											<xsl:variable name="columnwidth59_2" select="0.57292 * $factor59"/>
											<fo:table width="{$tablewidth59}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth59_0}in"/>
												<fo:table-column column-width="{$columnwidth59_1}in"/>
												<fo:table-column column-width="{$columnwidth59_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.19792in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="right" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth60" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths60" select="0.39583 + 0.39583 + 0.57292"/>
											<xsl:variable name="factor60">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths60 &gt; 0.00000 and $sumcolumnwidths60 &gt; $tablewidth60">
														<xsl:value-of select="$tablewidth60 div $sumcolumnwidths60"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth60_0" select="0.39583 * $factor60"/>
											<xsl:variable name="columnwidth60_1" select="0.39583 * $factor60"/>
											<xsl:variable name="columnwidth60_2" select="0.57292 * $factor60"/>
											<fo:table width="{$tablewidth60}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth60_0}in"/>
												<fo:table-column column-width="{$columnwidth60_1}in"/>
												<fo:table-column column-width="{$columnwidth60_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.19792in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="right" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth61" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths61" select="0.39583 + 0.39583 + 0.57292"/>
											<xsl:variable name="factor61">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths61 &gt; 0.00000 and $sumcolumnwidths61 &gt; $tablewidth61">
														<xsl:value-of select="$tablewidth61 div $sumcolumnwidths61"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth61_0" select="0.39583 * $factor61"/>
											<xsl:variable name="columnwidth61_1" select="0.39583 * $factor61"/>
											<xsl:variable name="columnwidth61_2" select="0.57292 * $factor61"/>
											<fo:table width="{$tablewidth61}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth61_0}in"/>
												<fo:table-column column-width="{$columnwidth61_1}in"/>
												<fo:table-column column-width="{$columnwidth61_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.19792in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="right" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.19792in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>months</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.72917in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>18.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.72917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Variable Interest Rate</xsl:text>
											</fo:inline>
											<xsl:variable name="tablewidth62" select="$columnwidth33_1 * 1.00000 - 0.03000"/>
											<xsl:variable name="sumcolumnwidths62" select="1.76042 + 1.76042"/>
											<xsl:variable name="factor62">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths62 &gt; 0.00000 and $sumcolumnwidths62 &gt; $tablewidth62">
														<xsl:value-of select="$tablewidth62 div $sumcolumnwidths62"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns62" select="1"/>
											<xsl:variable name="defaultcolumnwidth62">
												<xsl:choose>
													<xsl:when test="$factor62 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns62 &gt; 0">
														<xsl:value-of select="($tablewidth62 - $sumcolumnwidths62) div $defaultcolumns62"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth62_0" select="1.76042 * $factor62"/>
											<xsl:variable name="columnwidth62_1" select="1.76042 * $factor62"/>
											<xsl:variable name="columnwidth62_2" select="$defaultcolumnwidth62"/>
											<fo:table width="{$tablewidth62}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth62_0}in"/>
												<fo:table-column column-width="{$columnwidth62_1}in"/>
												<fo:table-column column-width="{$columnwidth62_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>(VIR) (as at</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/intRateActDate"/>
																</fo:inline>
																</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.17708in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt" font-size="10pt">
																<fo:inline>
																	<xsl:text>(calculated monthly and not in advance)</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.72917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth64" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths64" select="0.05208 + 1.33333 + 0.22917 + 0.20833 + 0.23958 + 1.27083 + 0.16667 + 1.56250"/>
											<xsl:variable name="factor64">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths64 &gt; 0.00000 and $sumcolumnwidths64 &gt; $tablewidth64">
														<xsl:value-of select="$tablewidth64 div $sumcolumnwidths64"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth64_0" select="0.05208 * $factor64"/>
											<xsl:variable name="columnwidth64_1" select="1.33333 * $factor64"/>
											<xsl:variable name="columnwidth64_2" select="0.22917 * $factor64"/>
											<xsl:variable name="columnwidth64_3" select="0.20833 * $factor64"/>
											<xsl:variable name="columnwidth64_4" select="0.23958 * $factor64"/>
											<xsl:variable name="columnwidth64_5" select="1.27083 * $factor64"/>
											<xsl:variable name="columnwidth64_6" select="0.16667 * $factor64"/>
											<xsl:variable name="columnwidth64_7" select="1.56250 * $factor64"/>
											<fo:table width="{$tablewidth64}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth64_0}in"/>
												<fo:table-column column-width="{$columnwidth64_1}in"/>
												<fo:table-column column-width="{$columnwidth64_2}in"/>
												<fo:table-column column-width="{$columnwidth64_3}in"/>
												<fo:table-column column-width="{$columnwidth64_4}in"/>
												<fo:table-column column-width="{$columnwidth64_5}in"/>
												<fo:table-column column-width="{$columnwidth64_6}in"/>
												<fo:table-column column-width="{$columnwidth64_7}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/varIntRate"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/intRateSpread"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/intRateSpreadVal"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/diffMorRateNSpread"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.12500in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>MR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in" number-columns-spanned="1">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																<fo:inline font-size="8pt">
																	<xsl:text>+/-</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>Spread</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>VIR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.72917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth64" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths64" select="0.05208 + 1.33333 + 0.22917 + 0.20833 + 0.23958 + 1.27083 + 0.16667 + 1.56250"/>
											<xsl:variable name="factor64">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths64 &gt; 0.00000 and $sumcolumnwidths64 &gt; $tablewidth64">
														<xsl:value-of select="$tablewidth64 div $sumcolumnwidths64"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth64_0" select="0.05208 * $factor64"/>
											<xsl:variable name="columnwidth64_1" select="1.33333 * $factor64"/>
											<xsl:variable name="columnwidth64_2" select="0.22917 * $factor64"/>
											<xsl:variable name="columnwidth64_3" select="0.20833 * $factor64"/>
											<xsl:variable name="columnwidth64_4" select="0.23958 * $factor64"/>
											<xsl:variable name="columnwidth64_5" select="1.27083 * $factor64"/>
											<xsl:variable name="columnwidth64_6" select="0.16667 * $factor64"/>
											<xsl:variable name="columnwidth64_7" select="1.56250 * $factor64"/>
											<fo:table width="{$tablewidth64}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth64_0}in"/>
												<fo:table-column column-width="{$columnwidth64_1}in"/>
												<fo:table-column column-width="{$columnwidth64_2}in"/>
												<fo:table-column column-width="{$columnwidth64_3}in"/>
												<fo:table-column column-width="{$columnwidth64_4}in"/>
												<fo:table-column column-width="{$columnwidth64_5}in"/>
												<fo:table-column column-width="{$columnwidth64_6}in"/>
												<fo:table-column column-width="{$columnwidth64_7}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="10pt">
																						<xsl:text>=</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.12500in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>MR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																<fo:inline font-size="8pt">
																	<xsl:text>+/-</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>Spread</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>VIR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>

									<fo:table-cell height="0.72917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth64" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths64" select="0.05208 + 1.33333 + 0.22917 + 0.20833 + 0.23958 + 1.27083 + 0.16667 + 1.56250"/>
											<xsl:variable name="factor64">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths64 &gt; 0.00000 and $sumcolumnwidths64 &gt; $tablewidth64">
														<xsl:value-of select="$tablewidth64 div $sumcolumnwidths64"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth64_0" select="0.05208 * $factor64"/>
											<xsl:variable name="columnwidth64_1" select="1.33333 * $factor64"/>
											<xsl:variable name="columnwidth64_2" select="0.22917 * $factor64"/>
											<xsl:variable name="columnwidth64_3" select="0.20833 * $factor64"/>
											<xsl:variable name="columnwidth64_4" select="0.23958 * $factor64"/>
											<xsl:variable name="columnwidth64_5" select="1.27083 * $factor64"/>
											<xsl:variable name="columnwidth64_6" select="0.16667 * $factor64"/>
											<xsl:variable name="columnwidth64_7" select="1.56250 * $factor64"/>
											<fo:table width="{$tablewidth64}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth64_0}in"/>
												<fo:table-column column-width="{$columnwidth64_1}in"/>
												<fo:table-column column-width="{$columnwidth64_2}in"/>
												<fo:table-column column-width="{$columnwidth64_3}in"/>
												<fo:table-column column-width="{$columnwidth64_4}in"/>
												<fo:table-column column-width="{$columnwidth64_5}in"/>
												<fo:table-column column-width="{$columnwidth64_6}in"/>
												<fo:table-column column-width="{$columnwidth64_7}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="10pt">
																						<xsl:text></xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>

														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="10pt">
																						<xsl:text>=</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																						<xsl:text></xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.12500in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="8pt">
																						<xsl:text>MR</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
														<fo:table-cell height="0.12500in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																					<fo:inline font-size="8pt">
																						<xsl:text>+/-</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
														<fo:table-cell height="0.12500in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="8pt">
																						<xsl:text>Spread</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="8pt">
																						<xsl:text>VIR</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
										<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
										</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:inline>
							<xsl:text>_____________________</xsl:text>
						</fo:inline>
						<fo:block>
							<xsl:text>&#xA;</xsl:text>
						</fo:block>
						<xsl:variable name="tablewidth75" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths75" select="0.50000"/>
						<xsl:variable name="factor75">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths75 &gt; 0.00000 and $sumcolumnwidths75 &gt; $tablewidth75">
									<xsl:value-of select="$tablewidth75 div $sumcolumnwidths75"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns75" select="1"/>
						<xsl:variable name="defaultcolumnwidth75">
							<xsl:choose>
								<xsl:when test="$factor75 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns75 &gt; 0">
									<xsl:value-of select="($tablewidth75 - $sumcolumnwidths75) div $defaultcolumns75"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth75_0" select="0.50000 * $factor75"/>
						<xsl:variable name="columnwidth75_1" select="$defaultcolumnwidth75"/>
						<fo:table width="{$tablewidth75}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth75_0}in"/>
							<fo:table-column column-width="{$columnwidth75_1}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>1</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>The payment frequency is used to determine when the periodic payments are to be made starting from the interest adjustment date.&#160; For example, if the payment frequency is monthly, then the periodic payments are to be made monthly, with the first payment to be made one month from the interest adjustment date.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>2</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt"  text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>Please note that you may benefit from a decline in interest rates occuring up to 3 days before the legal documents are signed with your legal advisor. Therefore, the interest rate indicated in this Commitment is subject to change. </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>3</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="before" height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt"  text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>"Mortgage Rate" or "MR" means the annual interest rate published by the Bank from time to time and which is used to determine the interest rates applicable to fixed-rate residential mortgages with a closed term as shown on Line 17 above.
</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

							</fo:table-body>
						</fo:table>
						<fo:block break-after="page"/>
						<xsl:variable name="tablewidth77" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths77" select="0.38542 + 2.82292 + 2.44792 + 2.44792 + 2.43750"/>
						<xsl:variable name="factor77">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths77 &gt; 0.00000 and $sumcolumnwidths77 &gt; $tablewidth77">
									<xsl:value-of select="$tablewidth77 div $sumcolumnwidths77"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth77_0" select="0.38542 * $factor77"/>
						<xsl:variable name="columnwidth77_1" select="2.82292 * $factor77"/>
						<xsl:variable name="columnwidth77_2" select="2.44792 * $factor77"/>
						<xsl:variable name="columnwidth77_3" select="2.44792 * $factor77"/>
						<xsl:variable name="columnwidth77_4" select="2.43750 * $factor77"/>
						<fo:table width="{$tablewidth77}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth77_0}in"/>
							<fo:table-column column-width="{$columnwidth77_1}in"/>
							<fo:table-column column-width="{$columnwidth77_2}in"/>
							<fo:table-column column-width="{$columnwidth77_3}in"/>
							<fo:table-column column-width="{$columnwidth77_4}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>

									<fo:table-cell height="0.20833in" number-columns-spanned="2" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">>
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Terms and Conditions of your Financing</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.20833in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text>Portion No. 3</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>18.1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.93750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt" >
											<fo:inline>
												<xsl:text>Blended rate (calculated monthly and not in advance), amount of indemnity financed by applying blended rate and corresponding payment amount (if applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.93750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth66" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths66" select="0.05208 + 0.2 + .10000 + 0.2 + 0.10000 + 0.20000 + 0.1 +  0.10000"/>
											<xsl:variable name="factor66">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths66 &gt; 0.00000 and $sumcolumnwidths66 &gt; $tablewidth66">
														<xsl:value-of select="$tablewidth66 div $sumcolumnwidths66"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns66" select="1"/>
											<xsl:variable name="defaultcolumnwidth66">
												<xsl:choose>
													<xsl:when test="$factor75 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns66 &gt; 0">
														<xsl:value-of select="($tablewidth66 - $sumcolumnwidths66) div $defaultcolumns66"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth66_0" select="0.05208 * $factor66"/>
											<xsl:variable name="columnwidth66_1" select="0.2 * $factor66"/>
											<xsl:variable name="columnwidth66_2" select="0.10000 * $factor66"/>
											<xsl:variable name="columnwidth66_3" select="0.2 * $factor66"/>
											<xsl:variable name="columnwidth66_4" select="0.10000 * $factor66"/>
											<xsl:variable name="columnwidth66_5" select="0.20000 * $factor66"/>
											<xsl:variable name="columnwidth66_6" select="0.1 * $factor66"/>
											<xsl:variable name="columnwidth66_7" select="0.10000 * $factor66"/>
											<fo:table width="{$tablewidth66}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth66_0}in"/>
												<fo:table-column column-width="{$columnwidth66_1}in"/>
												<fo:table-column column-width="{$columnwidth66_2}in"/>
												<fo:table-column column-width="{$columnwidth66_3}in"/>
												<fo:table-column column-width="{$columnwidth66_4}in"/>
												<fo:table-column column-width="{$columnwidth66_5}in"/>
												<fo:table-column column-width="{$columnwidth66_6}in"/>
												<fo:table-column column-width="{$columnwidth66_7}in"/>
												<fo:table-body font-size="8pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>+</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%&#160;=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<!-- fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell-->
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell font-size="8pt" height="0.01042in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt" font-size="8pt">
																<fo:inline>
																	<xsl:text>VIR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.01042in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block>
																	<fo:leader leader-pattern="space"/>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<!-- fo:table-cell height="0.01042in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell-->
														<fo:table-cell height="0.01042in" text-align="center" font-size="8pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Blended </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline>
																	<xsl:text>rate</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.09375in" text-align="center" font-size="10pt" padding-left="2pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160;&#160;$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" text-align="center" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" text-align="right" font-size="10pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.93750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth67" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths67" select="0.05208 + 0.2 + .10000 + 0.2 + 0.10000 + 0.20000 + 0.1 +  0.10000"/>
											<xsl:variable name="factor67">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths67 &gt; 0.00000 and $sumcolumnwidths67 &gt; $tablewidth67">
														<xsl:value-of select="$tablewidth67 div $sumcolumnwidths67"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth67_0" select="0.05208 * $factor67"/>
											<xsl:variable name="columnwidth67_1" select="0.2 * $factor67"/>
											<xsl:variable name="columnwidth67_2" select="0.1 * $factor67"/>
											<xsl:variable name="columnwidth67_3" select="0.2 * $factor67"/>
											<xsl:variable name="columnwidth67_4" select="0.1 * $factor67"/>
											<xsl:variable name="columnwidth67_5" select="0.2* $factor67"/>
											<xsl:variable name="columnwidth67_6" select="0.1* $factor67"/>
											<xsl:variable name="columnwidth67_7" select="0.1 * $factor67"/>
											<fo:table width="{$tablewidth67}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth67_0}in"/>
												<fo:table-column column-width="{$columnwidth67_1}in"/>
												<fo:table-column column-width="{$columnwidth67_2}in"/>
												<fo:table-column column-width="{$columnwidth67_3}in"/>
												<fo:table-column column-width="{$columnwidth67_4}in"/>
												<fo:table-column column-width="{$columnwidth67_5}in"/>
												<fo:table-column column-width="{$columnwidth67_6}in"/>
												<fo:table-column column-width="{$columnwidth67_7}in"/>
												<fo:table-body font-size="8pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>+</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%&#160;=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<!-- fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell-->
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell font-size="8pt" height="0.01042in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt" font-size="8pt">
																<fo:inline>
																	<xsl:text>VIR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.01042in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block>
																	<fo:leader leader-pattern="space"/>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<!-- fo:table-cell height="0.01042in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell-->
														<fo:table-cell height="0.01042in" text-align="center" font-size="8pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Blended </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline>
																	<xsl:text>rate</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.09375in" text-align="center" font-size="10pt" padding-left="2pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160;&#160;$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" text-align="center" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" text-align="right" font-size="10pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.93750in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth68" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths68" select="0.05208 + 0.2 + .10000 + 0.2 + 0.10000 + 0.20000 + 0.1 +  0.10000"/>
											<xsl:variable name="factor68">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths68 &gt; 0.00000 and $sumcolumnwidths68 &gt; $tablewidth68">
														<xsl:value-of select="$tablewidth68 div $sumcolumnwidths68"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth68_0" select="0.05208 * $factor68"/>
											<xsl:variable name="columnwidth68_1" select="0.2 * $factor68"/>
											<xsl:variable name="columnwidth68_2" select="0.1 * $factor68"/>
											<xsl:variable name="columnwidth68_3" select="0.2 * $factor68"/>
											<xsl:variable name="columnwidth68_4" select="0.1 * $factor68"/>
											<xsl:variable name="columnwidth68_5" select="0.2 * $factor68"/>
											<xsl:variable name="columnwidth68_6" select="0.1 * $factor68"/>
											<xsl:variable name="columnwidth68_7" select="0.1 * $factor68"/>
											<fo:table width="{$tablewidth68}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth68_0}in"/>
												<fo:table-column column-width="{$columnwidth68_1}in"/>
												<fo:table-column column-width="{$columnwidth68_2}in"/>
												<fo:table-column column-width="{$columnwidth68_3}in"/>
												<fo:table-column column-width="{$columnwidth68_4}in"/>
												<fo:table-column column-width="{$columnwidth68_5}in"/>
												<fo:table-column column-width="{$columnwidth68_6}in"/>
												<fo:table-column column-width="{$columnwidth68_7}in"/>
												<fo:table-body font-size="8pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>+</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%&#160;=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<!-- fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell-->
														<fo:table-cell height="0.10417in" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="left" border-left-color="red">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell font-size="8pt" height="0.01042in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt" font-size="8pt">
																<fo:inline>
																	<xsl:text>VIR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.01042in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:block>
																	<fo:leader leader-pattern="space"/>
																</fo:block>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<!-- fo:table-cell height="0.01042in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell-->
														<fo:table-cell height="0.01042in" text-align="center" font-size="8pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Blended </xsl:text>
																</fo:inline>
																<fo:block>
																	<xsl:text>&#xA;</xsl:text>
																</fo:block>
																<fo:inline>
																	<xsl:text>rate</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.09375in" text-align="center" font-size="10pt" padding-left="2pt">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160;&#160;$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" text-align="center" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" text-align="right" font-size="10pt" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.09375in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.09375in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.22917in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>19.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.22917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Revision frequency of Variable Interest Rate</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.22917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth69" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths69" select="0.98958 + 0.85417 + 1.16667 + 0.28125"/>
											<xsl:variable name="factor69">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths69 &gt; 0.00000 and $sumcolumnwidths69 &gt; $tablewidth69">
														<xsl:value-of select="$tablewidth69 div $sumcolumnwidths69"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns69" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth69">
												<xsl:choose>
													<xsl:when test="$factor69 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns69 &gt; 0">
														<xsl:value-of select="($tablewidth69 - $sumcolumnwidths69) div $defaultcolumns69"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth69_0" select="0.98958 * $factor69"/>
											<xsl:variable name="columnwidth69_1" select="0.85417 * $factor69"/>
											<xsl:variable name="columnwidth69_2" select="$defaultcolumnwidth69"/>
											<xsl:variable name="columnwidth69_3" select="1.16667 * $factor69"/>
											<xsl:variable name="columnwidth69_4" select="$defaultcolumnwidth69"/>
											<xsl:variable name="columnwidth69_5" select="0.28125 * $factor69"/>
											<fo:table width="{$tablewidth69}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth69_0}in"/>
												<fo:table-column column-width="{$columnwidth69_1}in"/>
												<fo:table-column column-width="{$columnwidth69_2}in"/>
												<fo:table-column column-width="{$columnwidth69_3}in"/>
												<fo:table-column column-width="{$columnwidth69_4}in"/>
												<fo:table-column column-width="{$columnwidth69_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Every </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/intRateRevFreq1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>months or</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.08333in" text-align="right" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Every </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in">
<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/intRateRevFreq2"/>
																</fo:inline>
															</fo:block>															</fo:table-cell>
														<fo:table-cell height="0.08333in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>weeks</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.22917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth70" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths70" select="0.98958 + 0.85417 + 1.16667 + 0.28125"/>
											<xsl:variable name="factor70">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths70 &gt; 0.00000 and $sumcolumnwidths70 &gt; $tablewidth70">
														<xsl:value-of select="$tablewidth70 div $sumcolumnwidths70"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns70" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth70">
												<xsl:choose>
													<xsl:when test="$factor70 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns70 &gt; 0">
														<xsl:value-of select="($tablewidth70 - $sumcolumnwidths70) div $defaultcolumns70"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth70_0" select="0.98958 * $factor70"/>
											<xsl:variable name="columnwidth70_1" select="0.85417 * $factor70"/>
											<xsl:variable name="columnwidth70_2" select="$defaultcolumnwidth70"/>
											<xsl:variable name="columnwidth70_3" select="1.16667 * $factor70"/>
											<xsl:variable name="columnwidth70_4" select="$defaultcolumnwidth70"/>
											<xsl:variable name="columnwidth70_5" select="0.28125 * $factor70"/>
											<fo:table width="{$tablewidth70}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth70_0}in"/>
												<fo:table-column column-width="{$columnwidth70_1}in"/>
												<fo:table-column column-width="{$columnwidth70_2}in"/>
												<fo:table-column column-width="{$columnwidth70_3}in"/>
												<fo:table-column column-width="{$columnwidth70_4}in"/>
												<fo:table-column column-width="{$columnwidth70_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Every </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>months or</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.08333in" text-align="right" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Every </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>weeks</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.22917in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth71" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths71" select="0.98958 + 0.85417 + 1.16667 + 0.28125"/>
											<xsl:variable name="factor71">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths71 &gt; 0.00000 and $sumcolumnwidths71 &gt; $tablewidth71">
														<xsl:value-of select="$tablewidth71 div $sumcolumnwidths71"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns71" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth71">
												<xsl:choose>
													<xsl:when test="$factor71 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns71 &gt; 0">
														<xsl:value-of select="($tablewidth71 - $sumcolumnwidths71) div $defaultcolumns71"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth71_0" select="0.98958 * $factor71"/>
											<xsl:variable name="columnwidth71_1" select="0.85417 * $factor71"/>
											<xsl:variable name="columnwidth71_2" select="$defaultcolumnwidth71"/>
											<xsl:variable name="columnwidth71_3" select="1.16667 * $factor71"/>
											<xsl:variable name="columnwidth71_4" select="$defaultcolumnwidth71"/>
											<xsl:variable name="columnwidth71_5" select="0.28125 * $factor71"/>
											<fo:table width="{$tablewidth71}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth71_0}in"/>
												<fo:table-column column-width="{$columnwidth71_1}in"/>
												<fo:table-column column-width="{$columnwidth71_2}in"/>
												<fo:table-column column-width="{$columnwidth71_3}in"/>
												<fo:table-column column-width="{$columnwidth71_4}in"/>
												<fo:table-column column-width="{$columnwidth71_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Every </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>months or</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.08333in" text-align="right" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Every </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>weeks</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:variable name="tablewidth72" select="$columnwidth33_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths72" select="0.54167 + 2.89583 + 1.16667 + 0.28125"/>
											<xsl:variable name="factor72">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths72 &gt; 0.00000 and $sumcolumnwidths72 &gt; $tablewidth72">
														<xsl:value-of select="$tablewidth72 div $sumcolumnwidths72"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns72" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth72">
												<xsl:choose>
													<xsl:when test="$factor72 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns72 &gt; 0">
														<xsl:value-of select="($tablewidth72 - $sumcolumnwidths72) div $defaultcolumns72"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth72_0" select="0.54167 * $factor72"/>
											<xsl:variable name="columnwidth72_1" select="2.89583 * $factor72"/>
											<xsl:variable name="columnwidth72_2" select="0.0000000002"/>
											<xsl:variable name="columnwidth72_3" select="1.12 * $factor72"/>
											<xsl:variable name="columnwidth72_4" select="$defaultcolumnwidth72"/>
											<xsl:variable name="columnwidth72_5" select="0.9 * $factor72"/>
											<xsl:variable name="tablewidth73" select="$columnwidth33_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths73" select="0.54167 + 2.89583 + 1.16667 + 0.28125"/>
											<xsl:variable name="factor73">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths73 &gt; 0.00000 and $sumcolumnwidths73 &gt; $tablewidth73">
														<xsl:value-of select="$tablewidth73 div $sumcolumnwidths73"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns73" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth73">
												<xsl:choose>
													<xsl:when test="$factor73 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns73 &gt; 0">
														<xsl:value-of select="($tablewidth73 - $sumcolumnwidths73) div $defaultcolumns73"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth73_0" select="0.54167 * $factor73"/>
											<xsl:variable name="columnwidth73_1" select="2.89583 * $factor73"/>
											<xsl:variable name="columnwidth73_2" select="$defaultcolumnwidth73"/>
											<xsl:variable name="columnwidth73_3" select="1.16667 * $factor73"/>
											<xsl:variable name="columnwidth73_4" select="$defaultcolumnwidth73"/>
											<xsl:variable name="columnwidth73_5" select="0.28125 * $factor73"/>
											<xsl:variable name="tablewidth74" select="$columnwidth33_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths74" select="0.54167 + 2.89583 + 1.16667 + 0.28125"/>
											<xsl:variable name="factor74">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths74 &gt; 0.00000 and $sumcolumnwidths74 &gt; $tablewidth74">
														<xsl:value-of select="$tablewidth74 div $sumcolumnwidths74"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns74" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth74">
												<xsl:choose>
													<xsl:when test="$factor74 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns74 &gt; 0">
														<xsl:value-of select="($tablewidth74 - $sumcolumnwidths74) div $defaultcolumns74"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth74_0" select="0.54167 * $factor74"/>
											<xsl:variable name="columnwidth74_1" select="2.89583 * $factor74"/>
											<xsl:variable name="columnwidth74_2" select="$defaultcolumnwidth74"/>
											<xsl:variable name="columnwidth74_3" select="1.16667 * $factor74"/>
											<xsl:variable name="columnwidth74_4" select="$defaultcolumnwidth74"/>
											<xsl:variable name="columnwidth74_5" select="0.28125 * $factor74"/>
								<fo:table-row>
									<fo:table-cell height="0.32292in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>20.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Capped Rate</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>4</xsl:text>
											</fo:inline>
											<fo:inline vertical-align="super">
												<xsl:text>&#160;</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>(if applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">

											<fo:table width="{$tablewidth72}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth72_0}in"/>
												<fo:table-column column-width="{$columnwidth72_1}in"/>
												<fo:table-column column-width="{$columnwidth72_2}in"/>
												<fo:table-column column-width="{$columnwidth72_3}in"/>
												<fo:table-column column-width="{$columnwidth72_4}in"/>
												<fo:table-column column-width="{$columnwidth72_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/cappedRate"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">

											<fo:table width="{$tablewidth73}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth73_0}in"/>
												<fo:table-column column-width="{$columnwidth73_1}in"/>
												<fo:table-column column-width="{$columnwidth73_2}in"/>
												<fo:table-column column-width="{$columnwidth73_3}in"/>
												<fo:table-column column-width="{$columnwidth73_4}in"/>
												<fo:table-column column-width="{$columnwidth73_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">

											<fo:table width="{$tablewidth74}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth74_0}in"/>
												<fo:table-column column-width="{$columnwidth74_1}in"/>
												<fo:table-column column-width="{$columnwidth74_2}in"/>
												<fo:table-column column-width="{$columnwidth74_3}in"/>
												<fo:table-column column-width="{$columnwidth74_4}in"/>
												<fo:table-column column-width="{$columnwidth74_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

										<fo:table-row>
									<fo:table-cell height="0.32292in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>21.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Critical Rate</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>5</xsl:text>
											</fo:inline>
											<fo:inline vertical-align="super">
												<xsl:text>&#160;</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>(if applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">

											<fo:table width="{$tablewidth72}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth72_0}in"/>
												<fo:table-column column-width="{$columnwidth72_1}in"/>
												<fo:table-column column-width="{$columnwidth72_2}in"/>
												<fo:table-column column-width="{$columnwidth72_3}in"/>
												<fo:table-column column-width="{$columnwidth72_4}in"/>
												<fo:table-column column-width="{$columnwidth72_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/cricRate"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">

											<fo:table width="{$tablewidth73}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth73_0}in"/>
												<fo:table-column column-width="{$columnwidth73_1}in"/>
												<fo:table-column column-width="{$columnwidth73_2}in"/>
												<fo:table-column column-width="{$columnwidth73_3}in"/>
												<fo:table-column column-width="{$columnwidth73_4}in"/>
												<fo:table-column column-width="{$columnwidth73_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.32292in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">

											<fo:table width="{$tablewidth74}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth74_0}in"/>
												<fo:table-column column-width="{$columnwidth74_1}in"/>
												<fo:table-column column-width="{$columnwidth74_2}in"/>
												<fo:table-column column-width="{$columnwidth74_3}in"/>
												<fo:table-column column-width="{$columnwidth74_4}in"/>
												<fo:table-column column-width="{$columnwidth74_5}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text></xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" number-columns-spanned="2" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>22.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Regular fixed payment amount (principal + interest) </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>6</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth81" select="$columnwidth77_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths81" select="0.21875"/>
											<xsl:variable name="factor81">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths81 &gt; 0.00000 and $sumcolumnwidths81 &gt; $tablewidth81">
														<xsl:value-of select="$tablewidth81 div $sumcolumnwidths81"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns81" select="1"/>
											<xsl:variable name="defaultcolumnwidth81">
												<xsl:choose>
													<xsl:when test="$factor81 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns81 &gt; 0">
														<xsl:value-of select="($tablewidth81 - $sumcolumnwidths81) div $defaultcolumns81"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth81_0" select="0.21875 * $factor81"/>
											<xsl:variable name="columnwidth81_1" select="$defaultcolumnwidth81"/>
											<fo:table width="{$tablewidth81}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth81_0}in"/>
												<fo:table-column column-width="{$columnwidth81_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text><xsl:value-of select="//CommitmentLetter/fixedPayAmt"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth82" select="$columnwidth77_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths82" select="0.21875"/>
											<xsl:variable name="factor82">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths82 &gt; 0.00000 and $sumcolumnwidths82 &gt; $tablewidth82">
														<xsl:value-of select="$tablewidth82 div $sumcolumnwidths82"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns82" select="1"/>
											<xsl:variable name="defaultcolumnwidth82">
												<xsl:choose>
													<xsl:when test="$factor82 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns82 &gt; 0">
														<xsl:value-of select="($tablewidth82 - $sumcolumnwidths82) div $defaultcolumns82"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth82_0" select="0.21875 * $factor82"/>
											<xsl:variable name="columnwidth82_1" select="$defaultcolumnwidth82"/>
											<fo:table width="{$tablewidth82}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth82_0}in"/>
												<fo:table-column column-width="{$columnwidth82_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth83" select="$columnwidth77_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths83" select="0.21875"/>
											<xsl:variable name="factor83">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths83 &gt; 0.00000 and $sumcolumnwidths83 &gt; $tablewidth83">
														<xsl:value-of select="$tablewidth83 div $sumcolumnwidths83"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns83" select="1"/>
											<xsl:variable name="defaultcolumnwidth83">
												<xsl:choose>
													<xsl:when test="$factor83 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns83 &gt; 0">
														<xsl:value-of select="($tablewidth83 - $sumcolumnwidths83) div $defaultcolumns83"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth83_0" select="0.21875 * $factor83"/>
											<xsl:variable name="columnwidth83_1" select="$defaultcolumnwidth83"/>
											<fo:table width="{$tablewidth83}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth83_0}in"/>
												<fo:table-column column-width="{$columnwidth83_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>23.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Amount of first variable regular payment (principal + interest), calculated with the interest rate indicated on Line 18(if applicable) (provided for information purposes only if a promotion applies)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth84" select="$columnwidth77_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths84" select="0.21875"/>
											<xsl:variable name="factor84">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths84 &gt; 0.00000 and $sumcolumnwidths84 &gt; $tablewidth84">
														<xsl:value-of select="$tablewidth84 div $sumcolumnwidths84"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns84" select="1"/>
											<xsl:variable name="defaultcolumnwidth84">
												<xsl:choose>
													<xsl:when test="$factor84 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns84 &gt; 0">
														<xsl:value-of select="($tablewidth84 - $sumcolumnwidths84) div $defaultcolumns84"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth84_0" select="0.21875 * $factor84"/>
											<xsl:variable name="columnwidth84_1" select="$defaultcolumnwidth84"/>
											<fo:table width="{$tablewidth84}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth84_0}in"/>
												<fo:table-column column-width="{$columnwidth84_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text><xsl:value-of select="//CommitmentLetter/empProgPay"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth85" select="$columnwidth77_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths85" select="0.21875"/>
											<xsl:variable name="factor85">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths85 &gt; 0.00000 and $sumcolumnwidths85 &gt; $tablewidth85">
														<xsl:value-of select="$tablewidth85 div $sumcolumnwidths85"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns85" select="1"/>
											<xsl:variable name="defaultcolumnwidth85">
												<xsl:choose>
													<xsl:when test="$factor85 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns85 &gt; 0">
														<xsl:value-of select="($tablewidth85 - $sumcolumnwidths85) div $defaultcolumns85"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth85_0" select="0.21875 * $factor85"/>
											<xsl:variable name="columnwidth85_1" select="$defaultcolumnwidth85"/>
											<fo:table width="{$tablewidth85}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth85_0}in"/>
												<fo:table-column column-width="{$columnwidth85_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth86" select="$columnwidth77_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths86" select="0.21875"/>
											<xsl:variable name="factor86">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths86 &gt; 0.00000 and $sumcolumnwidths86 &gt; $tablewidth86">
														<xsl:value-of select="$tablewidth86 div $sumcolumnwidths86"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns86" select="1"/>
											<xsl:variable name="defaultcolumnwidth86">
												<xsl:choose>
													<xsl:when test="$factor86 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns86 &gt; 0">
														<xsl:value-of select="($tablewidth86 - $sumcolumnwidths86) div $defaultcolumns86"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth86_0" select="0.21875 * $factor86"/>
											<xsl:variable name="columnwidth86_1" select="$defaultcolumnwidth86"/>
											<fo:table width="{$tablewidth86}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth86_0}in"/>
												<fo:table-column column-width="{$columnwidth86_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell height="0.21875in" number-columns-spanned="5" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="3pt" padding-bottom="1pt">
											<fo:inline font-weight="bold">
												<xsl:text>The following are additional terms and conditions only applicable to a line of credit</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.83333in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>24.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.83333in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Variable Annual Interest</xsl:text>
											</fo:inline>
											<xsl:variable name="tablewidth87" select="$columnwidth77_1 * 1.00000 - 0.03000"/>
											<xsl:variable name="sumcolumnwidths87" select="1.90625 + 1.76042"/>
											<xsl:variable name="factor87">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths87 &gt; 0.00000 and $sumcolumnwidths87 &gt; $tablewidth87">
														<xsl:value-of select="$tablewidth87 div $sumcolumnwidths87"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns87" select="1"/>
											<xsl:variable name="defaultcolumnwidth87">
												<xsl:choose>
													<xsl:when test="$factor87 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns87 &gt; 0">
														<xsl:value-of select="($tablewidth87 - $sumcolumnwidths87) div $defaultcolumns87"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth87_0" select="1.90625 * $factor87"/>
											<xsl:variable name="columnwidth87_1" select="1.78042 * $factor87"/>
											<xsl:variable name="columnwidth87_2" select="$defaultcolumnwidth87"/>
											<fo:table width="{$tablewidth87}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth87_0}in"/>
												<fo:table-column column-width="{$columnwidth87_1}in"/>
												<fo:table-column column-width="{$columnwidth87_2}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.18750in" padding-left="0.03000in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Rate (VAIR) as at</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>

														<fo:table-cell height="0.18750in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell font-size="8pt"  height="0.18750in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt" >
																	<xsl:value-of select="//CommitmentLetter/dateT26a"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell font-size="8pt" height="0.18750in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt" font-weight="bold">
																	<xsl:text>(provided for </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell  height="0.18750in" >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell font-size="8pt" height="0.18750in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt" font-weight="bold">
																	<xsl:text> information purposes only)</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell  height="0.18750in">
															<fo:block padding-top="1pt" padding-bottom="1pt" />
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.83333in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth88" select="$columnwidth77_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths88" select="0.20833 + 1.33333 + 0.05 + 0.16667 + 0.05 + 1.27083 + 0.16667 + 1.56250"/>
											<xsl:variable name="factor88">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths88 &gt; 0.00000 and $sumcolumnwidths88 &gt; $tablewidth88">
														<xsl:value-of select="$tablewidth88 div $sumcolumnwidths88"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth88_0" select="0.20833 * $factor88"/>
											<xsl:variable name="columnwidth88_1" select="1.33333 * $factor88"/>
											<xsl:variable name="columnwidth88_2" select="0.05 * $factor88"/>
											<xsl:variable name="columnwidth88_3" select="0.16667 * $factor88"/>
											<xsl:variable name="columnwidth88_4" select="0.05 * $factor88"/>
											<xsl:variable name="columnwidth88_5" select="1.27083 * $factor88"/>
											<xsl:variable name="columnwidth88_6" select="0.16667 * $factor88"/>
											<xsl:variable name="columnwidth88_7" select="1.56250 * $factor88"/>
											<fo:table width="{$tablewidth88}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth88_0}in"/>
												<fo:table-column column-width="{$columnwidth88_1}in"/>
												<fo:table-column column-width="{$columnwidth88_2}in"/>
												<fo:table-column column-width="{$columnwidth88_3}in"/>
												<fo:table-column column-width="{$columnwidth88_4}in"/>
												<fo:table-column column-width="{$columnwidth88_5}in"/>
												<fo:table-column column-width="{$columnwidth88_6}in"/>
												<fo:table-column column-width="{$columnwidth88_7}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:value-of select="//CommitmentLetter/intRatePrime"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in" number-columns-spanned="1">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:value-of select="//CommitmentLetter/spreadGT0"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:value-of select="//CommitmentLetter/varRateToPrime"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																<fo:inline font-size="10pt">
																	<xsl:text>=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																	<xsl:value-of select="//CommitmentLetter/calcField"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.12500in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Prime</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in" number-columns-spanned="1">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>+/-</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Spread</xsl:text>
																</fo:inline>
																</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>VAIR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.83333in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth89" select="$columnwidth77_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths89" select="0.20833 + 1.33333 + .05 + 0.16667 + .05 + 1.27083 + 0.16667 + 1.56250"/>
											<xsl:variable name="factor89">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths89 &gt; 0.00000 and $sumcolumnwidths89 &gt; $tablewidth89">
														<xsl:value-of select="$tablewidth89 div $sumcolumnwidths89"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth89_0" select="0.20833 * $factor89"/>
											<xsl:variable name="columnwidth89_1" select="1.33333 * $factor89"/>
											<xsl:variable name="columnwidth89_2" select="0.05 * $factor89"/>
											<xsl:variable name="columnwidth89_3" select="0.16667 * $factor89"/>
											<xsl:variable name="columnwidth89_4" select="0.05 * $factor89"/>
											<xsl:variable name="columnwidth89_5" select="1.27083 * $factor89"/>
											<xsl:variable name="columnwidth89_6" select="0.16667 * $factor89"/>
											<xsl:variable name="columnwidth89_7" select="1.56250 * $factor89"/>
											<fo:table width="{$tablewidth89}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth89_0}in"/>
												<fo:table-column column-width="{$columnwidth89_1}in"/>
												<fo:table-column column-width="{$columnwidth89_2}in"/>
												<fo:table-column column-width="{$columnwidth89_3}in"/>
												<fo:table-column column-width="{$columnwidth89_4}in"/>
												<fo:table-column column-width="{$columnwidth89_5}in"/>
												<fo:table-column column-width="{$columnwidth89_6}in"/>
												<fo:table-column column-width="{$columnwidth89_7}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.12500in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Prime</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>+/-</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Spread</xsl:text>
																</fo:inline>
																</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>VAIR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.83333in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth90" select="$columnwidth77_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths90" select="0.20833 + 1.33333 + 0.05 + 0.16667 + 0.05 + 1.27083 + 0.16667 + 1.46250"/>
											<xsl:variable name="factor90">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths90 &gt; 0.00000 and $sumcolumnwidths90 &gt; $tablewidth90">
														<xsl:value-of select="$tablewidth90 div $sumcolumnwidths90"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth90_0" select="0.20833 * $factor90"/>
											<xsl:variable name="columnwidth90_1" select="1.33333 * $factor90"/>
											<xsl:variable name="columnwidth90_2" select="0.05 * $factor90"/>
											<xsl:variable name="columnwidth90_3" select="0.16667 * $factor90"/>
											<xsl:variable name="columnwidth90_4" select="0.05 * $factor90"/>
											<xsl:variable name="columnwidth90_5" select="1.27083 * $factor90"/>
											<xsl:variable name="columnwidth90_6" select="0.16667 * $factor90"/>
											<xsl:variable name="columnwidth90_7" select="1.46250 * $factor90"/>
											<fo:table width="{$tablewidth90}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth90_0}in"/>
												<fo:table-column column-width="{$columnwidth90_1}in"/>
												<fo:table-column column-width="{$columnwidth90_2}in"/>
												<fo:table-column column-width="{$columnwidth90_3}in"/>
												<fo:table-column column-width="{$columnwidth90_4}in"/>
												<fo:table-column column-width="{$columnwidth90_5}in"/>
												<fo:table-column column-width="{$columnwidth90_6}in"/>
												<fo:table-column column-width="{$columnwidth90_7}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.10417in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>=</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.12500in" padding-left="0.03000in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Prime</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>+/-</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>Spread</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="6pt">
																	<xsl:text>VAIR</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>25.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Optional payment</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth91" select="$columnwidth77_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths91" select="0.21875"/>
											<xsl:variable name="factor91">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths91 &gt; 0.00000 and $sumcolumnwidths91 &gt; $tablewidth91">
														<xsl:value-of select="$tablewidth91 div $sumcolumnwidths91"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns91" select="1"/>
											<xsl:variable name="defaultcolumnwidth91">
												<xsl:choose>
													<xsl:when test="$factor91 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns91 &gt; 0">
														<xsl:value-of select="($tablewidth91 - $sumcolumnwidths91) div $defaultcolumns91"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth91_0" select="0.21875 * $factor91"/>
											<xsl:variable name="columnwidth91_1" select="$defaultcolumnwidth91"/>
											<fo:table width="{$tablewidth91}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth91_0}in"/>
												<fo:table-column column-width="{$columnwidth91_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth92" select="$columnwidth77_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths92" select="0.21875"/>
											<xsl:variable name="factor92">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths92 &gt; 0.00000 and $sumcolumnwidths92 &gt; $tablewidth92">
														<xsl:value-of select="$tablewidth92 div $sumcolumnwidths92"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns92" select="1"/>
											<xsl:variable name="defaultcolumnwidth92">
												<xsl:choose>
													<xsl:when test="$factor92 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns92 &gt; 0">
														<xsl:value-of select="($tablewidth92 - $sumcolumnwidths92) div $defaultcolumns92"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth92_0" select="0.21875 * $factor92"/>
											<xsl:variable name="columnwidth92_1" select="$defaultcolumnwidth92"/>
											<fo:table width="{$tablewidth92}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth92_0}in"/>
												<fo:table-column column-width="{$columnwidth92_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.17708in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth93" select="$columnwidth77_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths93" select="0.21875"/>
											<xsl:variable name="factor93">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths93 &gt; 0.00000 and $sumcolumnwidths93 &gt; $tablewidth93">
														<xsl:value-of select="$tablewidth93 div $sumcolumnwidths93"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns93" select="1"/>
											<xsl:variable name="defaultcolumnwidth93">
												<xsl:choose>
													<xsl:when test="$factor93 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns93 &gt; 0">
														<xsl:value-of select="($tablewidth93 - $sumcolumnwidths93) div $defaultcolumns93"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth93_0" select="0.21875 * $factor93"/>
											<xsl:variable name="columnwidth93_1" select="$defaultcolumnwidth93"/>
											<fo:table width="{$tablewidth93}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth93_0}in"/>
												<fo:table-column column-width="{$columnwidth93_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10pt">
																	<xsl:text>$</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.08333in" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>&#160; </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<xsl:variable name="tablewidth86" select="$columnwidth18_0 * 1.00000 + $columnwidth18_1 * 1.00000 + $columnwidth18_2 * 1.00000 + $columnwidth18_3 * 1.00000 + $columnwidth18_4* 1.00000"/>
									<xsl:variable name="sumcolumnwidths86" select="0.31250 + 2.56250 + 0.13542 + 1.15625 + 0.18750 + 1.85417 + 0.17708 + 1.93750"/>
									<xsl:variable name="factor86">
										<xsl:choose>
											<xsl:when test="$sumcolumnwidths86 &gt; 0.00000 and $sumcolumnwidths86 &gt; $tablewidth86">
												<xsl:value-of select="$tablewidth86 div $sumcolumnwidths86"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="1.000"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<xsl:variable name="defaultcolumns86" select="1"/>
									<xsl:variable name="defaultcolumnwidth86">
										<xsl:choose>
											<xsl:when test="$factor86 &lt; 1.000">
												<xsl:value-of select="0.000"/>
											</xsl:when>
											<xsl:when test="$defaultcolumns86 &gt; 0">
												<xsl:value-of select="($tablewidth86 - $sumcolumnwidths86) div $defaultcolumns86"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="0.000"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<xsl:variable name="columnwidth86_0" select="0.31250 * $factor86"/>
									<xsl:variable name="columnwidth86_1" select="2.56250 * $factor86"/>
									<xsl:variable name="columnwidth86_2" select="0.13542 * $factor86"/>
									<xsl:variable name="columnwidth86_3" select="1.15625 * $factor86"/>
									<xsl:variable name="columnwidth86_4" select="0.18750 * $factor86"/>
									<xsl:variable name="columnwidth86_5" select="1.85417 * $factor86"/>
									<xsl:variable name="columnwidth86_6" select="0.17708 * $factor86"/>
									<xsl:variable name="columnwidth86_7" select="1.93750 * $factor86"/>
									<xsl:variable name="columnwidth86_8" select="$defaultcolumnwidth86"/>
									<fo:table-cell number-columns-spanned="5" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:table>
											<fo:table-column column-width="{$columnwidth86_0}in"/>
											<fo:table-column column-width="{$columnwidth86_1}in"/>
											<fo:table-column column-width="{$columnwidth86_2}in"/>
											<fo:table-column column-width="{$columnwidth86_3}in"/>
											<fo:table-column column-width="{$columnwidth86_4}in"/>
											<fo:table-column column-width="{$columnwidth86_5}in"/>
											<fo:table-column column-width="{$columnwidth86_6}in"/>
											<fo:table-column column-width="{$columnwidth86_7}in"/>
											<fo:table-column column-width="{$columnwidth86_8}in"/>
											<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.12500in" padding-right="4pt" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>All the Portions of your Loan will be:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.12500in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																		<xsl:when test="//CommitmentLetter/loanInsType1 = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																</xsl:choose>
															</fo:block>
															</fo:table-cell>

															<fo:table-cell height="0.12500in">
																<fo:block  padding-top="1pt" padding-bottom="1pt">
																	<fo:inline>
																		<xsl:text>&#160;&#160;Uninsured</xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:table-cell>
															<fo:table-cell height="0.12500in" padding-left="6pt">
																<fo:block  padding-top="1pt" padding-bottom="1pt">
																	<xsl:choose>
																				<xsl:when test="//CommitmentLetter/loanInsType2 = '1' ">
																					<xsl:call-template name="CheckedCheckbox"/>
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																				</xsl:otherwise>
																	</xsl:choose>
																</fo:block>
															</fo:table-cell>
															<fo:table-cell height="0.12500in">
																<fo:block padding-top="1pt" padding-bottom="1pt">
																	<fo:inline>
																		<xsl:text>&#160;&#160;insured by CMHC</xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:table-cell>
															<fo:table-cell height="0.12500in">
																<fo:block padding-top="1pt" padding-bottom="1pt">
																	<xsl:choose>
																			<xsl:when test="//CommitmentLetter/loanInsType3 = '1' ">
																				<xsl:call-template name="CheckedCheckbox"/>
																			</xsl:when>
																			<xsl:otherwise>
																				<xsl:call-template name="UnCheckedCheckbox"/>
																			</xsl:otherwise>
																	</xsl:choose>
																</fo:block>
															</fo:table-cell>
															<fo:table-cell height="0.12500in">
																<fo:block padding-top="1pt" padding-bottom="1pt">
																	<fo:inline>
																		<xsl:text>&#160;&#160;insured by Genworth</xsl:text>
																	</fo:inline>
																</fo:block>
															</fo:table-cell>
													</fo:table-row>
											</fo:table-body>
										</fo:table>

								</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-top="3pt" display-align="center" height="0.27083in" text-align="center" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
	<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/budgetOpt = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>									</fo:table-cell>
									<fo:table-cell display-align="center" padding-top="3pt" height="0.27083in" number-columns-spanned="4" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Budget option </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>7</xsl:text>
											</fo:inline>
											<fo:inline vertical-align="super">
												<xsl:text>&#160; </xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>(tick if applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth97" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths97" select="0.000"/>
						<xsl:variable name="defaultcolumns97" select="1"/>
						<xsl:variable name="defaultcolumnwidth97">
							<xsl:choose>
								<xsl:when test="$defaultcolumns97 &gt; 0">
									<xsl:value-of select="($tablewidth97 - $sumcolumnwidths97) div $defaultcolumns97"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth97_0" select="$defaultcolumnwidth97"/>
<fo:table width="{$tablewidth97}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth97_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>

						</fo:block>
						</fo:table-cell>
						</fo:table-row>
						</fo:table-body>
						</fo:table>

						<fo:inline>
							<xsl:text>_____________________</xsl:text>
						</fo:inline>
						<fo:block>
							<xsl:text>&#xA;</xsl:text>
						</fo:block>
						<xsl:variable name="tablewidth98" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths98" select="0.50000"/>
						<xsl:variable name="factor98">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths98 &gt; 0.00000 and $sumcolumnwidths98 &gt; $tablewidth98">
									<xsl:value-of select="$tablewidth98 div $sumcolumnwidths98"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns98" select="1"/>
						<xsl:variable name="defaultcolumnwidth98">
							<xsl:choose>
								<xsl:when test="$factor98 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns98 &gt; 0">
									<xsl:value-of select="($tablewidth98 - $sumcolumnwidths98) div $defaultcolumns98"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth98_0" select="0.50000 * $factor98"/>
						<xsl:variable name="columnwidth98_1" select="$defaultcolumnwidth98"/>
						<fo:table width="{$tablewidth98}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="inherited-property-value(&apos;font-size&apos;) - 2pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth98_0}in"/>
							<fo:table-column column-width="{$columnwidth98_1}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>4</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>"Capped Rate" means the maximum interest rate, if any, applicable to your loan.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>5</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>"Critical Rate" means the rate above which the Fixed Periodic Payment indicated on Line 22 would not be sufficient to pay the interest due and payable on the Loan principal.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>6</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>This fixed payment must be greater than or equal to the payment calculated using the Variable Interest Rate indicated on Line 18 of the table above. This fixed payment will be increased if the Variable Interest Rate reaches or exceeds the Critical Rate, in order to ensure that the Loan is repaid in full during the amortization period.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>7</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>If you have chosen the budget option, your periodic payments will consist only of accrued interest despite the information shown on Lines 15.1 or 16.  No principal will be repaid for the duration of the term. By exercising this option, you will be extending the amortization period indicated on Line 4 by the number of months this option is exercised.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block break-after="page"/>
						<fo:table width="{$tablewidth97}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth97_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline font-weight="bold">
												<xsl:text>Financing Conditions</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<xsl:for-each select="//CommitmentLetter/docTrackText">
											<fo:block padding-top="1pt" padding-bottom="1pt"  text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:value-of select="."/>
											</fo:inline>
											</fo:block>
										</xsl:for-each>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block break-after="page">
							<fo:leader leader-pattern="space"/>
						</fo:block>
					<xsl:variable name="tablewidth100" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths100" select="0.000"/>
						<xsl:variable name="defaultcolumns100" select="1"/>
						<xsl:variable name="defaultcolumnwidth100">
							<xsl:choose>
								<xsl:when test="$defaultcolumns100 &gt; 0">
									<xsl:value-of select="($tablewidth100 - $sumcolumnwidths100) div $defaultcolumns100"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth100_0" select="$defaultcolumnwidth100"/>
						<fo:table width="{$tablewidth100}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth100_0}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell font-size="10pt" height="0.22917in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">

											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>Fees</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.31250in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="10pt">
												<xsl:text>You agree to pay periodic administration fees to the Bank as set out on Line 10 of the above table. The periodic administration fees shall be paid on the dates and at the frequency of the periodic payments.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth101" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths101" select="7.65625"/>
						<xsl:variable name="factor101">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths101 &gt; 0.00000 and $sumcolumnwidths101 &gt; $tablewidth101">
									<xsl:value-of select="$tablewidth101 div $sumcolumnwidths101"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth101_0" select="7.65625 * $factor101"/>
						<fo:table width="{$tablewidth101}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth101_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt" height="0.18750in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="10pt">
												<xsl:text>Other fees may be payable, at the rate in effect, for certain other services.&#160; These are specified in the Statement of Disclosure and Your Guide to Personal Banking Solutions, which will be provided to you</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth102" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths102" select="0.000"/>
						<xsl:variable name="defaultcolumns102" select="1"/>
						<xsl:variable name="defaultcolumnwidth102">
							<xsl:choose>
								<xsl:when test="$defaultcolumns102 &gt; 0">
									<xsl:value-of select="($tablewidth102 - $sumcolumnwidths102) div $defaultcolumns102"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth102_0" select="$defaultcolumnwidth102"/>
						<fo:table width="{$tablewidth102}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth102_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.21875in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>Cost of Borrowing</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.18750in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>The Bank will provide you with a written cost of borrowing disclosure statement at, or prior to, the signing of the loan.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth103" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths103" select="0.30208"/>
						<xsl:variable name="factor103">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths103 &gt; 0.00000 and $sumcolumnwidths103 &gt; $tablewidth103">
									<xsl:value-of select="$tablewidth103 div $sumcolumnwidths103"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns103" select="1"/>
						<xsl:variable name="defaultcolumnwidth103">
							<xsl:choose>
								<xsl:when test="$factor103 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns103 &gt; 0">
									<xsl:value-of select="($tablewidth103 - $sumcolumnwidths103) div $defaultcolumns103"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth103_0" select="0.30208 * $factor103"/>
						<xsl:variable name="columnwidth103_1" select="$defaultcolumnwidth103"/>
						<fo:table width="{$tablewidth103}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black">
							<fo:table-column column-width="{$columnwidth103_0}in"/>
							<fo:table-column column-width="{$columnwidth103_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.10417in" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>Additional Terms and Conditions</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.27083in" number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>This Commitment is also subject to the following additional conditions:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.26042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>1.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.26042in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Loan Documentation</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>. This Commitment is conditional upon receipt by the Bank of all requested documentation in a form satisfactory to the Bank.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.28125in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>2.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.28125in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline text-decoration="underline">
												<xsl:text>Previous Commitments</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>. This Commitment replaces all previous mortgage commitments concerning your mortgage loan application.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.77083in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>3.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.77083in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>	</xsl:text>
											</fo:inline>
											<fo:inline text-decoration="underline">
												<xsl:text>Cancellation and force majeure</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; This Commitment may be cancelled at any time at the option of the Bank if there has been any material change to your financial situation or that of the guarantor as disclosed to the Bank. This Commitment may also be cancelled by the Bank if there has been any material change to the Property that adversely affects its value. The Bank will not be bound by this Commitment nor liable for the damages or losses arising from the non-performance of its obligations under this Commitment in the event of force majeure.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.22917in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>4.	</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.22917in">
										<fo:block  padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Title</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; Your title to the property must be satisfactory to the Bank.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.77083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>5.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.77083in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>	</xsl:text>
											</fo:inline>
											<fo:inline text-decoration="underline">
												<xsl:text>Fire and Extended Perils Insurance</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; You must provide a fire and extended perils insurance policy.&#160; The policy must be for the full replacement value of the buildings on the property and with loss payable to the Bank, as mortgagee in accordance with its priority ranking, and contain standard mortgage clauses approved by the Insurance Bureau of Canada.&#160; The policy, insurer and standard mortgage clauses must be satisfactory to the Bank.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							<fo:table-row>
									<fo:table-cell height="0.65625in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>6.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.65625in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Title Insurance or Title Survey/Certificate of Location</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; The Bank requires either title insurance from a title insurance company approved by the Bank or an acceptable i) plan of survey for Ontario and British Columbia or ii) surveyor's certificate/certificate of location for Manitoba, New Brunswick, Newfoundland, Nova Scotia, Prince Edward Island, and Quebec or iii) a search report on the property for Alberta and Saskatchewan, prepared and certified by a qualified surveyor/land surveyor.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.28125in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>7.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.28125in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Insured Loan</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; If the Loan is insured, then this Commitment is conditional on the approval of the Loan by the insurer.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.45833in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>8.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>

										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.45833in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Estoppel Certificate</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; If the Property is a strata or condominium unit then the Strata/Condominium Corporation or syndicate of co-owners must be advised of the Bank's interest. The Bank must be provided with the Strata Plan or an Estoppel Certificate (for all provinces except Quebec).</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.45833in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>9.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>

										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.45833in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Expenses and Fees</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; All fees and expenses in connection with the loan are payable by you, including, without limitation, legal, appraisal, survey/title insurance and inspection fees, whether or not any money is advanced (unless stated otherwise above).</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block></fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.65625in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>10.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>

										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.65625in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Solicitor or Notary (in Quebec)</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; The Bank reserves the right under all circumstances to request you to retain a solicitor, or notary in Quebec, approved by the Bank at your expense. For the province of British Columbia only, the Bank may allow you to use a notary public unless the loan has more than one Portion. In such case, all legal documentation must be completed by a solicitor.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.25000in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>11.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.25000in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Assignment</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; You may not assign this Commitment without the prior written consent of the Bank.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.29167in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>12.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.29167in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Language</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>.&#160; Les parties confirment leur volonté que la présente offre soit rédigée en anglais. The parties have requested that this Commitment Letter be drawn up in English.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block break-after="page">
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth104" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths104" select="0.000"/>
						<xsl:variable name="defaultcolumns104" select="1"/>
						<xsl:variable name="defaultcolumnwidth104">
							<xsl:choose>
								<xsl:when test="$defaultcolumns104 &gt; 0">
									<xsl:value-of select="($tablewidth104 - $sumcolumnwidths104) div $defaultcolumns104"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth104_0" select="$defaultcolumnwidth104"/>
						<fo:table width="{$tablewidth104}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth104_0}in"/>
							<fo:table-body>

							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth105" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths105" select="0.525 + 0.382 + 0.093"/>
						<xsl:variable name="defaultcolumns105" select="1"/>
						<xsl:variable name="factor105">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths105 &gt; 0.00000 and $sumcolumnwidths105 &gt; $tablewidth105">
									<xsl:value-of select="$tablewidth105 div $sumcolumnwidths105"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumnwidth105">
							<xsl:choose>
								<xsl:when test="$defaultcolumns105 &gt; 0">
									<xsl:value-of select="($tablewidth105 - $sumcolumnwidths105) div $defaultcolumns105"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth105_0" select="0.525 * $factor105"/>
						<xsl:variable name="columnwidth105_1" select="0.382 * $factor105"/>
						<xsl:variable name="columnwidth105_2" select="0.093 * $factor105"/>
						<fo:table width="{$tablewidth105}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth105_0}in"/>
							<fo:table-column column-width="{$columnwidth105_1}in"/>
							<fo:table-column column-width="{$columnwidth105_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.55208in" number-columns-spanned="3">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>Thank you for choosing National Bank of Canada for your home financing solution.</xsl:text>
											</fo:inline>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>We acknowledge having read and accepted this document on</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>____________________________________________.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.11458in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.11458in" number-columns-spanned="2" text-align="center">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="6pt">
												<xsl:text>Date</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth107" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths107" select="3.85417"/>
						<xsl:variable name="factor107">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths107 &gt; 0.00000 and $sumcolumnwidths107 &gt; $tablewidth107">
									<xsl:value-of select="$tablewidth107 div $sumcolumnwidths107"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns107" select="1"/>
						<xsl:variable name="defaultcolumnwidth107">
							<xsl:choose>
								<xsl:when test="$factor107 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns107 &gt; 0">
									<xsl:value-of select="($tablewidth107 - $sumcolumnwidths107) div $defaultcolumns107"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth107_0" select="3.85417 * $factor107"/>
						<xsl:variable name="columnwidth107_1" select="$defaultcolumnwidth107"/>
						<fo:table width="{$tablewidth107}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth107_0}in"/>
							<fo:table-column column-width="{$columnwidth107_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.11458in" number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo2.jpeg" width="2.4in"/>
											</fo:inline>
											</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.1in" number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline font-size="8pt">
												<xsl:text>_______________________________________________________________&#160;&#160;&#160;&#160;&#160;&#160;&#160; __________________________________________________</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="6pt">
												<xsl:text>(Signature of the Borrower)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; Date&#160;&#160;&#160; </xsl:text>
											</fo:inline>
											<!--fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="8pt">
												<xsl:text>&#160;</xsl:text>
											</fo:inline-->
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth108" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths108" select="3.85417"/>
						<xsl:variable name="factor108">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths108 &gt; 0.00000 and $sumcolumnwidths108 &gt; $tablewidth108">
									<xsl:value-of select="$tablewidth108 div $sumcolumnwidths108"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns108" select="1 + 1"/>
						<xsl:variable name="defaultcolumnwidth108">
							<xsl:choose>
								<xsl:when test="$factor108 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns108 &gt; 0">
									<xsl:value-of select="($tablewidth108 - $sumcolumnwidths108) div $defaultcolumns108"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth108_0" select="3.85417 * $factor108"/>
						<xsl:variable name="columnwidth108_1" select="$defaultcolumnwidth108"/>
						<xsl:variable name="columnwidth108_2" select="$defaultcolumnwidth108"/>
						<fo:table width="{$tablewidth108}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth108_0}in"/>
							<fo:table-column column-width="{$columnwidth108_1}in"/>
							<fo:table-column column-width="{$columnwidth108_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.1in" number-columns-spanned="3">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<!--fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block-->
											<fo:inline font-size="8pt">
												<xsl:text>_______________________________________________________________&#160;&#160;&#160;&#160;&#160;&#160;&#160; __________________________________________________</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="6pt">
												<xsl:text>(Signature of the Borrower)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; Date&#160;&#160;&#160; </xsl:text>
											</fo:inline>
											<!--fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="8pt">
												<xsl:text>&#160;</xsl:text>
											</fo:inline-->
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth109" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths109" select="3.85417"/>
						<xsl:variable name="factor109">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths109 &gt; 0.00000 and $sumcolumnwidths109 &gt; $tablewidth109">
									<xsl:value-of select="$tablewidth109 div $sumcolumnwidths109"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns109" select="1 + 1"/>
						<xsl:variable name="defaultcolumnwidth109">
							<xsl:choose>
								<xsl:when test="$factor109 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns109 &gt; 0">
									<xsl:value-of select="($tablewidth109 - $sumcolumnwidths109) div $defaultcolumns109"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth109_0" select="3.85417 * $factor109"/>
						<xsl:variable name="columnwidth109_1" select="$defaultcolumnwidth109"/>
						<xsl:variable name="columnwidth109_2" select="$defaultcolumnwidth109"/>
						<fo:table width="{$tablewidth109}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth109_0}in"/>
							<fo:table-column column-width="{$columnwidth109_1}in"/>
							<fo:table-column column-width="{$columnwidth109_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.1in" number-columns-spanned="3">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<!--fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block-->
											<fo:inline font-size="8pt">
												<xsl:text>_______________________________________________________________&#160;&#160;&#160;&#160;&#160;&#160;&#160; __________________________________________________</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="6pt">
												<xsl:text>(Signature of the Borrower)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; Date&#160;&#160;&#160; </xsl:text>
											</fo:inline>
											<!--fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="8pt">
												<xsl:text>&#160;</xsl:text>
											</fo:inline-->
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth110" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths110" select="3.85417"/>
						<xsl:variable name="factor110">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths110 &gt; 0.00000 and $sumcolumnwidths110 &gt; $tablewidth110">
									<xsl:value-of select="$tablewidth110 div $sumcolumnwidths110"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns110" select="1 + 1"/>
						<xsl:variable name="defaultcolumnwidth110">
							<xsl:choose>
								<xsl:when test="$factor110 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns110 &gt; 0">
									<xsl:value-of select="($tablewidth110 - $sumcolumnwidths110) div $defaultcolumns110"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth110_0" select="3.85417 * $factor110"/>
						<xsl:variable name="columnwidth110_1" select="$defaultcolumnwidth110"/>
						<xsl:variable name="columnwidth110_2" select="$defaultcolumnwidth110"/>
						<fo:table width="{$tablewidth110}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth110_0}in"/>
							<fo:table-column column-width="{$columnwidth110_1}in"/>
							<fo:table-column column-width="{$columnwidth110_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.1in" number-columns-spanned="3">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<!--fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block-->
											<fo:inline font-size="8pt">
												<xsl:text>_______________________________________________________________&#160;&#160;&#160;&#160;&#160;&#160;&#160; __________________________________________________</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="6pt">
												<xsl:text>(Signature of the Borrower)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; Date&#160;&#160;&#160; </xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="8pt">
												<xsl:text>&#160;</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
					<fo:block id="{$page-sequenceId}" />
				</fo:flow>
			</fo:page-sequence>
	</xsl:template>
	
	<!-- ************************************************************* -->
	<!-- ****************** ***Checked Check Box ***************** -->
	<!-- ************************************************************* -->

	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="8px" height="8px">
			<svg xmlns="http://www.w3.org/2000/svg" width="8px" height="8px">
				<rect x="0" y="0" width="8" height="8" style="fill: none; stroke: black"/>
				<line x1="0" y1="0" x2="8" y2="8" style="stroke:black"/>
				<line x1="0" y1="8" x2="8" y2="0" style="stroke:black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<!-- ************************************************************* -->
	<!-- ****************** Unchecked Check Box****************** -->
	<!-- ************************************************************* -->
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="8px" height="8px">
			<svg xmlns="http://www.w3.org/2000/svg" width="8px" height="8px">
				<rect x="0" y="0" width="8" height="8" style="fill: none; stroke: black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<xsl:variable name="maxwidth" select="7.52000"/>
		<xsl:variable name="tablewidth201" select="$maxwidth * 1.00000"/>
				<xsl:variable name="sumcolumnwidths201" select="0.04167 + 1.56250 + 0.04167"/>
				<xsl:variable name="factor201">
					<xsl:choose>
						<xsl:when test="$sumcolumnwidths201 &gt; 0.00000 and $sumcolumnwidths201 &gt; $tablewidth201">
							<xsl:value-of select="$tablewidth201 div $sumcolumnwidths201"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="1.000"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="defaultcolumns201" select="1"/>
				<xsl:variable name="defaultcolumnwidth201">
					<xsl:choose>
						<xsl:when test="$factor201 &lt; 1.000">
							<xsl:value-of select="0.000"/>
						</xsl:when>
						<xsl:when test="$defaultcolumns201 &gt; 0">
							<xsl:value-of select="($tablewidth201 - $sumcolumnwidths201) div $defaultcolumns201"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="0.000"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="columnwidth201_0" select="$defaultcolumnwidth201"/>
				<xsl:variable name="columnwidth201_1" select="1.56250 * $factor201"/>



	<!-- ************************************************************* -->
	<!-- *************************Footerall ****************************-->
	<!-- ************************************************************* -->
	<xsl:template name="footerall">
	  <xsl:param name="footerText" />
	  <xsl:param name="page-sequence" />
	  <xsl:variable name="tablewidth201" select="$maxwidth * 1.00000"/>
		<fo:static-content flow-name="xsl-region-after">
			<fo:block>
				<fo:table width="{$tablewidth201}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black" display-align="center">
					<fo:table-column column-width="{$columnwidth201_0}in"/>
					<fo:table-column column-width="{$columnwidth201_1}in"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell height="0.01042in" number-columns-spanned="2" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt"/>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="2" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:block space-before.optimum="-8pt">
										<fo:leader leader-length="97%" leader-pattern="rule" rule-thickness="1pt" color="black"/>
									</fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell font-size="6pt" text-align="left" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:inline font-size="6pt">
										<xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="$footerText"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell font-size="6pt" text-align="center" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:inline font-size="6pt" font-weight="normal">
										<xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>Page&#160;</xsl:text>
									</fo:inline>
									<fo:page-number font-size="6pt"/>
									<fo:inline font-size="6pt" font-weight="normal">
										<xsl:text>&#160;of&#160;</xsl:text>
									</fo:inline>
									<fo:inline font-size="6pt" font-weight="normal">
										<fo:page-number-citation ref-id="{$page-sequence}" />
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>

	<xsl:template name="firstpageheader">
	 	<xsl:param name="headingText" />
		<xsl:variable name="maxwidth" select="7.30000"/>
		<xsl:variable name="image-dir" select="//CommitmentLetter/Logo_Image"></xsl:variable>
		<fo:static-content flow-name="RegionBeforeFirstPage">
			<fo:block>
				<fo:table width="{$maxwidth}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black">
					<fo:table-column column-width="proportional-column-width(33)"/>
					<fo:table-column column-width="proportional-column-width(20)"/>
					<fo:table-column column-width="proportional-column-width(47)"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell height="0.5in" number-columns-spanned="3">
								<fo:block>
									<xsl:text>&#160;</xsl:text>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell height="0.08333in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:external-graphic  src="file:/{$image-dir}logo.jpeg" width="1.6in"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell height="0.08333in">
								<fo:block padding-top="1pt" padding-bottom="1pt"/>
							</fo:table-cell>
							<fo:table-cell  height="0.08333in" font-size="12pt" font-weight="bold" text-align="right" display-align="center">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<xsl:value-of select="$headingText"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell height="0.01042in" number-columns-spanned="3" display-align="before">
								<fo:block padding-top="1pt" padding-bottom="1pt"  space-before.optimum="-8pt">
									<fo:leader leader-length="100%" leader-pattern="rule" rule-thickness="2pt"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>
	<xsl:template name="nonfirstpageheader">
	 	<xsl:param name="headingText" />
		<xsl:variable name="maxwidth" select="7.30000"/>
		<xsl:variable name="image-dir" select="//CommitmentLetter/Logo_Image"></xsl:variable>
		<fo:static-content flow-name="RegionBeforeOtherPage">
			<fo:block>
				<fo:table width="{$maxwidth}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black">
					<fo:table-column column-width="proportional-column-width(33)"/>
					<fo:table-column column-width="proportional-column-width(20)"/>
					<fo:table-column column-width="proportional-column-width(47)"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell height="0.5in" number-columns-spanned="3">
								<fo:block>
									<xsl:text>&#160;</xsl:text>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell height="0.08333in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:external-graphic  src="file:/{$image-dir}logo.jpeg" width="1.6in"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell height="0.08333in">
								<fo:block padding-top="1pt" padding-bottom="1pt"/>
							</fo:table-cell>
							<fo:table-cell  height="0.08333in" font-size="12pt" font-weight="bold" text-align="right" display-align="center">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<xsl:value-of select="$headingText"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>
</xsl:stylesheet>
