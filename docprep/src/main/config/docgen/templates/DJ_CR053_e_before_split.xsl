<?xml version="1.0" encoding="UTF-8"?>
<!--09/Mar/2005 DVG #DG166 #1063  DJ - miscellaneous DocPrep fixes  -->
<!--22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik-->
<!-- Author Zivko Radulovic -->
<!-- edited by Catherine Rutgaizer, March 2004 -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- ================  Attribute sets ============== -->
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBA1.5Optimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttrPg4">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainTextAttr_Page_2">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ListLabelIndent">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="padding-left">-3mm</xsl:attribute>
	</xsl:attribute-set>
	<!-- ================  Attribute sets ============== -->

	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<!-- ========================== top level ================================  -->
	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateBorrowerLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreatePageTwo">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					Page <fo:page-number/> of <fo:page-number-citation ref-id="page_2"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLine_Page_2"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateHeader_Page_2"/>
				<xsl:call-template name="Page_2_Item_1"/>
				<xsl:call-template name="Page_2_Item_1a"/>
				<xsl:call-template name="Page_2_Item_1b"/>
				<xsl:call-template name="Page_2_Item_1c"/>
				<xsl:call-template name="Page_2_Item_2"/>
				<xsl:call-template name="Page_2_Item_2a"/>
				<xsl:call-template name="Page_2_Item_2b"/>
				<xsl:call-template name="Page_2_Item_2b_after"/>
				<xsl:call-template name="Page_2_Item_2c"/>
				<xsl:call-template name="Page_2_Item_2c_after"/>
				<xsl:call-template name="Page_2_Item_3"/>
				<!-- primary borrower -->
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
						<xsl:if test="position()=1">
							<xsl:call-template name="Page_2_Item_4"/>
							<xsl:call-template name="Page_2_Item_4a"/>
							<xsl:call-template name="Page_2_Item_4b"/>
						</xsl:if>
					</xsl:for-each>
				</xsl:element>
				<xsl:call-template name="Page_2_Item_5"/>
				<!-- <xsl:call-template name="Page_2_Item_6"/> -->
				<xsl:call-template name="Page_2_Item_6"/>
				<xsl:call-template name="Page_2_Item_6a"/>
				<xsl:call-template name="Page_2_Item_7"/>
				<xsl:call-template name="Page_2_Item_7a"/>
				<xsl:call-template name="Page_2_Item_8"/>
				<xsl:call-template name="Page_2_Item_Ending"/>
				<xsl:call-template name="Page_2_Item_Ending_a"/>
				<fo:block id="page_2" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreatePageThree">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					<!-- Page <fo:page-number/> of <fo:page-number-citation ref-id="page_3"/></xsl:element> -->
					Page 1 of 1</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage3"/>
				<xsl:call-template name="Page_3_Item_1"/>
				<fo:block id="page_3" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreatePageFour">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					Page <fo:page-number/> of <fo:page-number-citation ref-id="page_4"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage4"/>
				<xsl:call-template name="Page_4_Item_1"/>
				<xsl:call-template name="DealConditionsNotWaived"/>
				<fo:block id="page_4" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ======================= PAGE 1 ======================= -->

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">11cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
							Mortgage Loan Financing Approval
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateBorrowerLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2pt</xsl:attribute>
							<xsl:attribute name="space-before">2pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="BorrowersListedBlocks"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2pt</xsl:attribute>
							<xsl:attribute name="space-before">2pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="PrimBorrAddress"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
						Subject:
				</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
						Application for a mortgage loan
					</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
						Identification code: <xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">2em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>Madam,</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">0mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>Sir,</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>I am pleased to inform you that your application for a mortgage loan has been approved. You will find attached a financing offer containing the terms and conditions related to this loan.</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			As agreed, your file will be transferred to  <xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyCompanyName"/>. 
			If you require more information, please contact your mortgage representative,  
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/salutation"/>
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactFirstName"/>
			<xsl:text>&#160;</xsl:text>
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactLastName"> </xsl:value-of> at <xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"> </xsl:value-of>. 
			She/He will be pleased to answer all your questions.
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>We thank you for placing your trust in us and hope to have the pleasure of serving you again in the future. In the meantime, please accept our best regards.</xsl:text>
		</xsl:element>
	</xsl:template>
	<!-- Underwriter info -->

	<xsl:template name="CreateUnderwriterLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">30mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">100mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:copy use-attribute-sets="PaddingAll0mm"/>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Telephone:	
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumber"/>
											<xsl:choose>
												<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> or 1-888-281-4420 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<!-- <xsl:when test="//Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when> -->
												<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> or 1-800-728-2728 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text> </xsl:text>ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Fax:
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="//Deal/underwriter/Contact/contactFaxNumber"/>
											<xsl:choose>
												<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> or 1-888-888-7777</xsl:when>
												<xsl:when test="//Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999</xsl:when>
												<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> or 1-888-888-8888</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ======================= END PAGE 1 ======================= -->

	<xsl:template name="Page_4_Item_1">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> Name of borrower(s):</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">2pt</xsl:attribute>
									<xsl:attribute name="space-before">2pt</xsl:attribute>
									<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
									<xsl:attribute name="font-size">8pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="./salutation"/>&#160;
									<xsl:value-of select="./borrowerFirstName"/>&#160;
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">8pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<fo:inline font-weight="bold">CONDITIONS :</fo:inline>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- START OF TABLE   MI Policy numbers-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">6cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="mortgageInsuranceCheckBox">
								<xsl:with-param name="parInsurer">1</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
							<!-- <xsl:text> - </xsl:text> -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Insured loan CMHC no. 
								<xsl:call-template name="mortgageInsurancePolicyNum">
								<xsl:with-param name="parInsurer">1</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="mortgageInsuranceCheckBox">
								<xsl:with-param name="parInsurer">2</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
							<!-- <xsl:text> - </xsl:text> --> 
							Insured loan G.E. Capital no.
								<xsl:call-template name="mortgageInsurancePolicyNum">
								<xsl:with-param name="parInsurer">2</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Commission $-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">6.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Commission 
								<xsl:call-template name="mortgageInsurancePremiumAmount">
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/MIUpfront= 'Y'">
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							 included with loan 
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/MIUpfront= 'N'">
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 non included with loan  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Tax $-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Tax <xsl:value-of select="//Deal/MIPremiumPST"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							 Payable
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 check included  
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 to account 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by the contractor  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Subscription right -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Subscription right $ 
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 check included 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 to account 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by the contractor 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by Caisse  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE    Evaluation fees  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Evaluation fees $  
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 check included
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 to account 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by Caisse  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   The down payment must be deposited at the Caise -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							The down payment must be deposited at the Caisse before sending instructions to the notary. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Multiproject -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/multiProject">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Multiprojets option  
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Certificat of Location -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Certificat of Location of no more than 10 years, indicating the actual state of the property. The certificat must be complet and precis in all aspect. The property must not have been altered, modified or extended since the date of issuance. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   New certificate  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId='1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/newConstructionId='2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							New certificate of location demanded (new construction). 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Disbursement date expected -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Disbursement date expected <xsl:value-of select="//Deal/estimatedClosingDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Clause for 1/12th of taxes  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:if test="//Deal/paymentFrequencyId='0'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:if>
									<xsl:if test="not(//Deal/paymentFrequencyId= '0')">
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:if test="//Deal/paymentFrequencyId='0'">
										 Clause for 1/12th of taxes  $<xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
									</xsl:if>
									<xsl:if test="not(//Deal/paymentFrequencyId= '0')">
										 Clause for 1/12th of taxes 
									</xsl:if>
								</xsl:when>
								<xsl:otherwise> Clause for 1/12th of taxes  </xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:choose>
										<xsl:when test="//Deal/paymentFrequencyId='4'">
											<xsl:call-template name="CheckedCheckbox"/> 1/52th $ <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:when test="//Deal/paymentFrequencyId='5'">
											<xsl:call-template name="CheckedCheckbox"/> 1/52th $<xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="UnCheckedCheckbox"/> 1/52th $	
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/> 1/52th $	
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:choose>
										<xsl:when test="//Deal/paymentFrequencyId='2'">
											<xsl:call-template name="CheckedCheckbox"/> 1/26th  <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:when test="//Deal/paymentFrequencyId='3'">
											<xsl:call-template name="CheckedCheckbox"/> 1/26th <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="UnCheckedCheckbox"/> 1/26th $	
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/> 1/26th $	
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Standard clauses for condominium.  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/propertyTypeId = '14'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/propertyTypeId = '15'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/propertyTypeId = '16'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Standard clauses for condominium.  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  constructions  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/progressAdvance= 'Y'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Construction: progressive disbursement, the first disbursement will have to be made at the latest 90 days after the date of the mortgage act and retainer of 15 %.  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  For new construction   -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							For new construction: the builder must be accredited by a guaranty program for new homes, and the property must be requbtered in the program. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Renovation -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Renovation: disbursement must be controlled if above $ 10 000. The builder must be accredited through the APCHQ program for renovation
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Obtain renonctation -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/progressAdvance= 'Y'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Obtain renonctation of legal hypotheque and cession of priority of rank, certificate of completion of the construction and 15 % holdbacks until 35 days following the date of work completion. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Intervention   -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Mortgage loan act intervention as surety 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Obtain copy of the water analysis  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Obtain copy of the water analysis and the conformity certificate from the municipality for the well and the septic tank if the property b not connected to the aqueduc system. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  To the request of the borrower(s) -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							To the request of the borrower(s), the payments will be made
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  weekly for the amount of -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">7cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 weekly for the amount of    
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 including life   
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 including life &#38; invalidity 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  aux 2  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">7cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by weekly accelerated payments of  
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 including life 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 including life &#38; invalidity 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  First paymen to be made the -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">7cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							 First paymen to be made the 
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId= '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId= '3'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 7th day following the disbursement of the loan;
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId= '4'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId= '5'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 14th day following the dbbursement of the loan.
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  
		 START OF TABLE  Le or les emprunteurs 
		 ===================================================  
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row"><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
												
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">Le or les emprunteurs désirent un prêt RAP, S.V.P. communiquez avec ces derniers sur la réception du dossier. </fo:inline>
						</xsl:element>
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		 =================================================== 
		 END OF TABLE 
		 ===================================================  -->
		<!-- ===================================================  
		 START OF TABLE  Commentaires  
		- ===================================================  
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
											
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">Commentaires sur promotion </fo:inline>
						</xsl:element>
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		 ===================================================  
		 END OF TABLE 
		 ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Autres -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Other 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>

	<xsl:template name="DealConditionsNotWaived">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttrPg4"/>
			<xsl:element name="fo:list-block">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
				<xsl:apply-templates select="//Deal/DocumentTracking"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="DocumentTracking">
		<xsl:choose>
			<xsl:when test="Condition/conditionTypeId='0'">
				<xsl:element name="fo:list-item">
					<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
					<xsl:element name="fo:list-item-label">
						<xsl:element name="fo:block" use-attribute-sets="ListLabelIndent">
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:list-item-body">
						<xsl:attribute name="start-indent">body-start()</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">justify</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="./text"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="((documentStatusId='0') or (documentStatusId='0')) and (Condition/conditionTypeId='2') and (Condition/conditionTypeId!='5')">
					<xsl:element name="fo:list-item">
						<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
						<xsl:element name="fo:list-item-label">
							<xsl:element name="fo:block">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:list-item-body">
							<xsl:attribute name="start-indent">body-start()</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="text-align">justify</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select="./text"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Page_3_Item_1">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>The loan will be granted under the following terms and conditions, in addition to those in use at the Caisse.</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute> -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 1. DOCUMENTS TO BE GIVEN TO THE NOTARY</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Upon receipt hereof, you are required to send the notary the following documents if they are available, and any other document the notary may ask for:</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<fo:list-block>
							<xsl:attribute name="start-indent">1mm</xsl:attribute>
							<xsl:attribute name="provisional-distance-between-starts">12mm</xsl:attribute>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										your marriage contract, where applicable;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ======================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										your deed of acquisition of the property, where applicable;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										-	a copy of the preliminary contract (purchase offer) and memorandum, where applicable;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										a certificate of location accepted by the Caisse or meeting the specifications on the first side of this form, where applicable;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										an insurance policy against fire and other perils for an amount sufficient to cover the loan, and the line of credit if applicable, or a coverage note, or in the case of a condominium, the certificate establishing that an insurance covering the co-owned property has been subscribed by the administrators;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										municipal and school tax receipts; and
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										in the case of a legal person, proof of its juridical existence and its power to take out the planned loan.
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
						</fo:list-block>
						<!-- =============================================================================-->
						<!-- GROUP 2  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 2. SIGNING OF THE DEED OF HYPOTHEC AND DISBURSEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>You shall be required to sign a deed of hypothecary loan based on the forms in use at the Caisse. The loan will be disbursed once the Caisse, tjas received satisfactory evidence that:</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<fo:list-block>
							<xsl:attribute name="start-indent">1mm</xsl:attribute>
							<xsl:attribute name="provisional-distance-between-starts">12mm</xsl:attribute>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										it holds a good and valid hypothec of the rank required;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ======================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>
										the property is insured against fire and other perils for an amount sufficient to cover the claim of the Caisse, and that the policy contains the'standard mortgage clause in favor of the Caisse;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										all taxes and charges that may affect the property have been paid;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="text-align">justify</xsl:attribute>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										any irregularity noted in the certificate of location has been corrected;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										the certificate of authorization required under the <fo:inline font-style="italic"> Environment Quality Act </fo:inline>(R.S.Q., c. Q-2) has been issued in the Borrower's name
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										all the terms and conditions applicable to the loan have been fulfilled.
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
						</fo:list-block>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>The line of credit, if any, shall be disbursed only if you avail yourself of the privilege conferred by the "Owner Protection" clause in accordance with the conditions set in the deed of hypothec.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 3  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 3. CONSTRUCTION, REPAIRS OR IMPROVEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>If the purpose of the loan is to build, repair or improve a property, the loan shall be advanced in successive disbursements as the work progresses. You will be required, prior to each disbursement, to provide the Caisse or the notary with a solemn declaration on legal construction hypothecs. If the work has been entrusted with a general contractor, disbursements may be effected through joint cheques made to your order jointly with the general contractor and, if applicable, with those persons who gave you notice of their contract with the general contractor, or to your order jointly with the persons with whom you have directly entered into a contract. If the work has not been entrusted with a general contractor, disbursements may be effected through cheques made jointly to your order and to that of any person with whom you have directly entered into a contract! Instead of ascertaining that the persons who may hold a legal construction hypothec have been paid as stipulated above, the Caisse may require that you provide it, prior to every disbursement, with a "Consent to Priority of Hypothec" signed in its favor by each and every such person. In all cases, the Caisse may make withholdings so that the value of the work exceed by 15%, in each case, the total of the disburse-. ments requested and the disbursements already advanced, with the last disbursement of 15% to be made 35 days after the completion of the work, all of which at the discretion of the Caisse.
							</xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							You should also note that, according to the Act <fo:inline font-style="italic">respecting labour relations, vocational training and manpower management in the construction industry </fo:inline>
							(R.S.Q., c. R-20), you will be held solidarity liable for the payment of any wages due to construction employees   working on the site, unless you make sure that the contractor or the general contractor with whom you contracted, directly or through an intermediary, holds the prescribed license issued by the Régie du bâtiment du Quebec.
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 4  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 4. PAYMENT OF INTEREST</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>If the loan is advanced in successive disbursements, the Caisse may at any time require that the accrued interest be paid regularty and in full. The interest accrued on the date of each disbursement may be deducted from the disbursement by the Caisse.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 5  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 5. DEADLINE FOR DISBURSEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>If the loan cannot be fully disbursed within three months of the dale of signing of the deed of hypothec, the Caisse may, upon giving written notice to that effect, refuse to make any further disbursement, but shall retain all its recourses with respect to the sums already advanced.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 6  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 6. COSTS</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>You shall pay the fees of the notary, surveyor and assessor, as well as publication charges, inspection costs, environmental verification costs and other costs of the same type. The Caisse may retain sufficient sums from the proceeds of the loan to pay them. You shall also pay, when the loan is repaid, with or without subrogation, the costs and fees of the notarial cancellation.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 7  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 7. DEBIT AUTHORIZATION</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Any sum due may be debited directly from one of your savings accounts at the Caisse.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 8  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 8. SALE PRICE OF THE PROPERTY</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>If the loan is used for the acquisition of a property, the Caisse reserves the right to reduce the amount of the loan if the sale price is lower than the one stipulated in the offer to buy.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- GROUP 9  -->
						<!-- ============================================================================  -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 9. CHANGES IN YOUR FINANCIAL SITUATION</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							The Caisse reserves the right to withdraw this offer to finance should it find that your financial situation has significantly deteriorated since you filed your loan application.
						</xsl:element>
						<!-- =============================================================================-->
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<!-- ====================================================  Page 2 ====================================================   -->

	<!-- ====================================================  Page 2 ====================================================   -->
	<xsl:template name="CreateTitleLine_Page_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<!-- <xsl:attribute name="space-before">0.7in</xsl:attribute> -->
			<!-- <xsl:attribute name="break-before">page</xsl:attribute> -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">14cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								OFFER TO FINANCE - IMMOVABLE HYPOTHEC <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- Subject property, French formatting of address-->
	<!--
	<xsl:template match="Property">
		<xsl:element name="fo:block"><xsl:attribute name="keep-together">always</xsl:attribute>			
			<xsl:value-of select="propertyStreetNumber"/><xsl:text>, </xsl:text><xsl:if test="streetType"><xsl:value-of select="streetType"/><xsl:text>  </xsl:text></xsl:if>
			<xsl:value-of select="propertyStreetName"/><xsl:if test="streetDirection"><xsl:text>  </xsl:text><xsl:value-of select="streetDirection"/></xsl:if>
			<xsl:if test="unitNumber">, unité <xsl:value-of select="unitNumber"/></xsl:if>
		</xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="concat(propertyCity,', ',province)"/></xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/></xsl:element>
	</xsl:template>
	-->

	<!-- Subject property, English formatting of address -->
	<xsl:template match="Property">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:value-of select="propertyStreetNumber"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="propertyStreetName"/>
			<xsl:text> </xsl:text>
			<xsl:if test="streetType">
				<xsl:value-of select="streetType"/>
				<xsl:text>  </xsl:text>
			</xsl:if>
			<xsl:if test="streetDirection">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="streetDirection"/>
			</xsl:if>
			<xsl:if test="unitNumber">, unit <xsl:value-of select="unitNumber"/>
			</xsl:if>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyCity,' ',province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateHeader_Page_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">75mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">80mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						To:
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:call-template name="BorrowersListedBlocks"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:call-template name="PrimBorrAddress"/>
						</xsl:element>
					</xsl:element>
					<!-- =============================  -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						From:
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyCompanyName"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine1"/>
						</xsl:element>
						<xsl:if test="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine2">
							<xsl:element name="fo:block">
								<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:attribute name="font-weight">bold</xsl:attribute>
								<fo:inline font-weight="bold">
									<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine2"/>
								</fo:inline>
							</xsl:element>
						</xsl:if>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/city"/>, <xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/province"/>
							</fo:inline>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/postalFSA"/>&#160;<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/postalLDU"/>
							</fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:text>Dear Member(s):</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			We are pleased to inform you that your application for a hypothecary loan in the amount of <fo:inline font-weight="bold">
				<xsl:value-of select="//Deal/totalLoanAmount"/>
			</fo:inline> has been accepted, at the following conditions:
		</xsl:element>
	</xsl:template>

	<!-- ==========================  Page 2 Item 1  ==========================   -->
	<xsl:template name="Page_2_Item_1">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>1. </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>The loan shall bear interest: </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_1a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:call-template name="ConstructPage2Item1aBox"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>a)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1aText"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1aBox">
		<xsl:param name="pRate"/>
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='0'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1aText">
		<xsl:param name="pRate"/>
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='0'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					at a fixed rate: i.e., a rate of <fo:inline font-weight="bold">
							<xsl:value-of select="//Deal/netInterestRate"/>
						</fo:inline> per annum, reckoned six-monthly and not in advance 
				</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					at a fixed rate: MARKER
				</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Page_2_Item_1b">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:call-template name="ConstructPage2Item1bBox"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>b)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1bText"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1bBox">
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='2'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="//Deal/interestTypeId='3'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1bText">
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='2'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text>at a variable rate: i.e., at the Caisse centrale Desjardins' prime rate increased by </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/discount"/>
								</fo:inline>
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/premium"/>
								</fo:inline>
							</xsl:when>
						</xsl:choose>
						<xsl:text> % per annum. The rate applicable to the loan shall vary with each change in such prime rate and shall be calculated monthly and not In advance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="//Deal/interestTypeId='3'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text>at a variable rate: i.e., at the Caisse centrale Desjardins' prime rate increased by  </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/discount"/>
								</fo:inline>
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/premium"/>
								</fo:inline>
							</xsl:when>
						</xsl:choose>
						<xsl:text> % per annum. The rate applicable to the loan shall vary with each change in such prime rate and shall be calculated monthly and not In advance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text>  at a variable rate: at the Caisse centrale Desjardins' prime rate. The rate applicable to the loan shall vary with each change in such prime rate and shall be calculated monthly and not in advance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Page_2_Item_1c">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="text-align">justify</xsl:attribute>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '2'">
									Maximum rate: However, the rate applicable to the loan shall not at any time exceed <fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/netInterestRate"/>
									</fo:inline> % per annum reckoned monthly and not in advance. i.e., <fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/TO_BE_DETERMINED"/>
									</fo:inline>  % per annum reckoned six-monthly and not in advance. 										
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Maximum rate: However, the rate applicable to the loan shall not at any time exceed MARKER % per annum reckoned six-monthly and not in advance. </xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:element name="fo:block">
								i.e., MARKER % per annum reckoned six-monthly and not in advance.
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ==========================  Page 2 Item 2  ==========================   -->

	<xsl:template name="Page_2_Item_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>2. </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Repayment shall be made by means of regular, equal, consecutive payments of principal and interest in the amount of <fo:inline font-weight="bold">
								<xsl:value-of select="//Deal/PandiPaymentAmount"/>
							</fo:inline> each, commencing on 
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId='0'">
									<fo:inline font-weight="bold"> 30ème jour suivant le déboursé du prêt </fo:inline> and thereafter on the <fo:inline font-weight="bold"> même  jour </fo:inline> of each <fo:inline font-weight="bold"> mois.</fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId='2'">
									<fo:inline font-weight="bold"> 14 jours suivant le déboursé du prêt </fo:inline> et les autres successivement le <fo:inline font-weight="bold"> même  jour </fo:inline> de chaque <fo:inline font-weight="bold"> deux semaines.</fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId='4'"> <!--#DG166 replaced 'la' for 'le' -->
									<fo:inline font-weight="bold"> 7 ème jour suivant le déboursé du prêt </fo:inline> et les autres successivement le <fo:inline font-weight="bold"> même  jour </fo:inline> de chaque <fo:inline font-weight="bold"> semaine.</fo:inline>
								</xsl:when>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> a)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							up to and including MARKER , at which date any remaining balance shall become due; 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2b">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> b)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							until the expiry of a term of MARKER commencing on the date of the signing of the deed of hypothec, any remaining balance becoming due at the expiry of that term;
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2b_after">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">If the loan is secured by &#160;&#160;&#160;&#160;&#160; </fo:inline>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId='1'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
									<fo:inline font-weight="bold">&#160; the Canada Mortgage and Housing Corporation &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/mortgageInsurerId='2'">
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; the Canada Mortgage and Housing Corporation &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; the Canada Mortgage and Housing Corporation &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2c">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId= '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/mortgageInsurerId= '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> c)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						until the expiry of a term of <fo:inline font-weight="bold">
								<xsl:value-of select="//Deal/actualPaymentTerm"/> month </fo:inline>
						commencing on the date of adjustment of interest, namely <fo:inline font-weight="bold"> MARKER </fo:inline>, any remaining balance becoming due at the expiry of that term.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2c_after">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.3cm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						The amount of the payments is based on an amortization period of
						<fo:inline font-weight="bold">
								<xsl:value-of select="//specialRequirementTags/amortizationTermProcessed/termNumber"> </xsl:value-of>
								<xsl:choose>
									<xsl:when test="//specialRequirementTags/amortizationTermProcessed/termMeasure='MONTHS'"> months. </xsl:when>
									<xsl:when test="//specialRequirementTags/amortizationTermProcessed/termMeasure='YEARS'"> year(s). </xsl:when>
								</xsl:choose>
							</fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ==========================  Page 2 Item 3  ==========================   -->

	<xsl:template name="Page_2_Item_3">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>3.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="UnCheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						A line of credit for <fo:inline font-weight="bold">$ 0 </fo:inline> will also be granted to you for the purpose of the "Owner Protection" clause.	
				</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_4">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>4.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>Notwithstanding the foregoing terms and conditions which shall appear in the deed of hypothec, if you opt for </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_4a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="(./LDInsuranceTypeId = '1' and ./lifeStatusId = '1') or (./LDInsuranceTypeId = '2' and ./disabilityStatusId = '2')">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> life insurance only, </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="./LDInsuranceTypeId = '2' and ./disabilityStatusId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> life and disability insurance, </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_4b">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
						your regular payments will be 
						<fo:inline font-weight="bold">
								<xsl:value-of select="//Deal/pmntPlusLifeDisability"/>
							</fo:inline>
						in order to include the premium for such insurance, which corresponds to an additional interest rate of
						<fo:inline font-weight="bold">&#160;<xsl:value-of select="//Deal/interestRateIncrement"/>&#160;</fo:inline>
						per annum, subject to the provisions of the insurance policy in force at the Caisse.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_5">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>5.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> Prepayments on the loan may be made   &#160;&#160;&#160;  </xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/prePaymentOptionsId= '4'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> without paying a penalty &#160;&#160;</xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/interestTypeId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/interestTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/interestTypeId = '5'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> upon payment of a penalty  </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						 if the prepaid amounts exceed <fo:inline font-weight="bold">  15.0%  </fo:inline> of the initial amount of the loan in accordance with the deed of hypothec.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_6">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>6.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
						The loan and the line of credit, if applicable, shall be secured by a hypothec of 
						<xsl:choose>
								<xsl:when test="//Deal/lienPositionId='0'">
									<fo:inline font-weight="bold"> 1</fo:inline>
									<fo:inline font-weight="bold" font-size="8" baseline-shift="super">st </fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline font-weight="bold"> 2</fo:inline>
									<fo:inline font-weight="bold" font-size="8" baseline-shift="super">nd </fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						 rank on the Property located at the following address:
				</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_6a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">170mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:attribute name="padding-left">5px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:apply-templates select="//DealAdditional/Property"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_7">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>7.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>In addition to the terms and conditions specified overleaf, which shall continue to apply notwithstanding the signing of the deed of hypothec, the following shall apply: </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_7a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">5px</xsl:attribute>
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold"> See Appendix </fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_8">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>8.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> The deed of hypothec must be signed within 150 days of the date hereof. After that period, the Caisse reserves the right to change theinterest rate or other terms and conditions, in which case it will send you a new Offer to Finance, or it may decide not to grant the loan.</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_Ending">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>A copy hereof has been sent to the notary, Mtre </xsl:text>
							<xsl:choose>
								<xsl:when test="//DealAdditional/solicitorData/partyProfile/Contact/contactLastName">
									<xsl:value-of select="//DealAdditional/solicitorData/partyProfile/Contact/contactFirstName"/>&#160;<xsl:value-of select="//DealAdditional/solicitorData/partyProfile/Contact/contactLastName"/>.
								</xsl:when>
								<xsl:otherwise> to be determined</xsl:otherwise>
							</xsl:choose>
							<xsl:text>.   If you require further information or if you no longer wish to obtain this loan, please contact us without delay. </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">14pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/Contact/contactFirstName"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/underwriter/Contact/contactLastName"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>For a hypothecary loan of other than first rank subject to the Consumer Protection Act, a copy hereof in notarial form must be appended to the contract</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_Ending_a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4.0cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">13.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>Attachments: </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
									<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
									<xsl:attribute name="content-height">100%</xsl:attribute>
									<xsl:attribute name="content-width">100%</xsl:attribute>
								</xsl:element> -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
									<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
									<xsl:attribute name="content-height">100%</xsl:attribute>
									<xsl:attribute name="content-width">100%</xsl:attribute>
								</xsl:element> -->
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Borrower's Statement: Legal Construction Hypothecs </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Consent to Priority of Hypothec: Legal Construction Hypothecs </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!--<xsl:template name="CreatePageTwo">
	<xsl:element name="fo:page-sequence">
		<xsl:attribute name="master-reference">main</xsl:attribute>
		<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
					<xsl:call-template name="CreatePageTwoTopLine"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoTable"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoCCLine"></xsl:call-template>
			</xsl:element>
	</xsl:element>
</xsl:template> -->

	<xsl:template name="CreatePageTwoCCLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:value-of select="concat('cc:    ', //Deal/sourceOfBusinessProfile/Contact/contactFirstName, '   ', //Deal/sourceOfBusinessProfile/Contact/contactLastName , '  représentant(e) hypothécaire ' )"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ============================================================================================================================ -->

	<xsl:template name="CreateTitleLinePage4">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								ANNEXE TO OFFER TO FINANCE - MORTGAGE LOAN <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLinePage3">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">11cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								TERMS AND CONDITIONS
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2em</xsl:attribute>
							<xsl:attribute name="space-before">2em</xsl:attribute>
							<xsl:value-of select="//General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:element name="fo:block">
				<xsl:call-template name="BorrowerFullName"/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>
	<xsl:template name="BorrAddress">
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/city,' ',./Address/province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/postalFSA,'  ', ./Address/postalLDU)"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PrimBorrAddress">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:if test="position()=1">
				<xsl:for-each select="BorrowerAddress">
					<xsl:if test="borrowerAddressTypeId='0'">
						<xsl:call-template name="BorrAddress"/>
						<text>some text</text>
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="mortgageInsuranceCheckBox">
		<xsl:param name="parInsurer"/>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<!-- Start  -->
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="UnCheckedCheckbox"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- END  -->
	</xsl:template>

	<xsl:template name="mortgageInsurancePolicyNum">
		<xsl:param name="parInsurer"/>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<!-- Start  -->
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- END  -->
	</xsl:template>

	<xsl:template name="mortgageInsurancePremiumAmount">
		<xsl:param name="parInsurer">99</xsl:param>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$parInsurer='99'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--   ================================================================= -->
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
			<xsl:call-template name="CreatePageTwo"/>
			<xsl:call-template name="CreatePageThree"/>
			<xsl:call-template name="CreatePageFour"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
