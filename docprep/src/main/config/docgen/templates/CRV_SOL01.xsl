<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
  09/Jan/2007 DVG #DG564 #5476  Macquarie PROD - Sol Pac Interest Calculation Period Field  
	13/Oct/2006 DVG #DG522 #4922  Cervus CR # 106 Rebranding  
	09/Jan/2006 DVG #DG374 #2583  CR#86 - Conventional Changes for Solicitors Package  
	25/Nov/2005 DVG #DG364 #2362  CR#79 Solicitors Package - Big Changes  
	31/Oct/2005 DVG #DG352 #2361  CR#65 Solicitors Package template text addition  
	03/Oct/2005 DVG #DG326 #2208  Solicitors changes post CR73  
		- logo is configurable, reverse paper height change
	26/Sep/2005 DVG #DG322 #2162  CR#73 - Solicitors Package Date Removal  
	23/Feb/2005 DVG #DG142 #793  Cervus - faxing multiple attachments.
	    changed paper size; removed outline level;
	04/Jan/2005 DVG #DG122 scr#820 Cervus - Commitment Template change
	04/Jan/2005 DVG #DG120 scr#814  Open  Cervus - Commitment Template change
	29/Dec/2004 DVG #DG116 	scr#812  Open  Cervus - Changes to solicitor's package-->
	<xsl:output method="text"/>

	<xsl:variable name="lenderName" select="/*/Deal/LenderProfile/lenderName"/><!--#DG522 -->	
	<!--#DG326 -->

  <xsl:variable name="borrowerLangIda" select="/*/Deal/Borrower[primaryBorrowerFlag = 'Y']/languagePreferenceId"/>  
  <!-- if I don't do below, 'borrowerLangId' will always be equal to whatever, 
  no time to investigate now, later... -->
  <xsl:variable name="borrowerLangId" select="translate($borrowerLangIda, ' ', '')"/>
  
  <xsl:variable name="isQebecProperty" select="/*/Deal/Property[primaryPropertyFlag = 'Y']/provinceId = 11"/>

	<xsl:variable name="lendName" select="translate($lenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" /><!--#DG522 -->	
	<xsl:variable name="lendNameLang">	
		<xsl:choose>
			<xsl:when test="$isQebecProperty and $borrowerLangId = 1">
		    <xsl:value-of select="concat($lendName,'_Fr')"/>
      </xsl:when>
			<xsl:otherwise>
		    <xsl:value-of select="concat($lendName,'_En')"/>
      </xsl:otherwise>
		</xsl:choose>
	</xsl:variable>	
	
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendNameLang,'.xsl'))" />
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />

	<!-- ================================================  -->
	<xsl:template match="/">
		<!--#DG326 -->
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
    <xsl:message>isQebecProperty: <xsl:value-of select="$isQebecProperty"/></xsl:message>

    <!--#DG364 reformed; Since the differences between the QC English 
    and other English is only in some selected terms, let's just use 
    the same template for now -->
			<xsl:choose>
				<xsl:when test="$isQebecProperty">		
					<!-- ================================================  -->
					<!-- Quebec Forms-->
					<!-- ================================================  -->
  				<xsl:choose>
  					<xsl:when test="$borrowerLangId = 1">
    					<!-- ================================================  -->
    					<!-- Quebec French Forms-->
    					<!-- ================================================  -->
  						<xsl:call-template name="SolPkgFr"/>
  					</xsl:when>
  					<xsl:otherwise>
    					<!-- ================================================  -->
    					<!-- Quebec English Forms-->
    					<!-- ================================================  -->
  						<!--xsl:call-template name="SolPkgEnQC"/-->
					    <xsl:call-template name="SolPkgEn"/>
  					</xsl:otherwise>
  				</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<!-- ================================================  -->
					<!-- Other Provinces English Forms -->
					<!-- ================================================  -->
					<xsl:call-template name="SolPkgEn"/>
				</xsl:otherwise>
			</xsl:choose>
	</xsl:template>

	<!-- ================================================  -->

	<xsl:template name="SolPkgEn">

		<xsl:call-template name="RTFFileStart"/>
		<xsl:call-template name="SolInstructionsEn"/>
		<xsl:call-template name="MortgageDetailsEn"/>
		<xsl:text>\pard\sect </xsl:text><!--#DG522 -->
		<xsl:call-template name="SolGuideEn"/>
		<xsl:text>\pard\plain {\sect }\paperh15840 </xsl:text>		<!--#DG326 paper height-->
		<xsl:call-template name="MtgCmtEn"/>
		<xsl:text>\pard\plain {\sect }\paperh20160 </xsl:text>		<!--#DG326 paper height-->
		<xsl:call-template name="ConfirmCloseEn"/>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ================================================  -->

	<xsl:template name="SolPkgEnQC">

		<!--#DG364 xsl:call-template name="RTFFileStart"/>
		<xsl:call-template name="SolInstructionsEnQC"/>
		<xsl:call-template name="MortgageDetailsEnQC"/>
		<xsl:text>\pard\par\page</xsl:text>
		<xsl:call-template name="SolGuideEnQC"/>
		<xsl:text>\pard\plain {\sect }\paperh15840 </xsl:text>		
		<xsl:call-template name="MtgCmtEnQC"/>
		<xsl:text>\pard\plain {\sect }\paperh20160 </xsl:text>		
		<xsl:call-template name="ConfirmCloseEnQC"/>
		<xsl:call-template name="RTFFileEnd"/-->
	</xsl:template>

	<!-- ================================================  -->

	<xsl:template name="SolPkgFr">

		<xsl:call-template name="RTFFileStart"/>
		<xsl:call-template name="SolInstructionsFr"/>
		<xsl:call-template name="MortgageDetailsFr"/>
		<xsl:text>\pard\page</xsl:text>
		<xsl:call-template name="SolGuideFr"/>
		<xsl:text>\pard\plain\sect\paperh15840 </xsl:text>		
		<xsl:call-template name="MtgCmtFr"/>
		<xsl:text>\pard\plain\sect\paperh20160 </xsl:text>		
		<xsl:call-template name="ConfirmCloseFr"/>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="SolInstructionsEn">
		<xsl:text>\paperh20160 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
		<xsl:call-template name="LenderLogo"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>\par </xsl:text>

    <!--#DG364 -->		
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
  		<xsl:call-template name="ContactAddress"/>
		</xsl:for-each>		
		
		<!--#DG364 xsl:if test="//Deal/BranchProfile/Contact/Addr/addressLine1">
		  <xsl:value-of select="//Deal/BranchProfile/Contact/Addr/addressLine1"/>
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:if test="//Deal/BranchProfile/Contact/Addr/addressLine2 != ''">
		  <xsl:value-of select="//Deal/BranchProfile/Contact/Addr/addressLine2"/>
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//Deal/BranchProfile/Contact/Addr/city"/>
		<xsl:text>{, }</xsl:text>
		<xsl:value-of select="//Deal/BranchProfile/Contact/Addr/province"/>
		<xsl:text>{  }</xsl:text>
		<xsl:value-of select="//Deal/BranchProfile/Contact/Addr/postalFSA"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//Deal/BranchProfile/Contact/Addr/postalLDU"/-->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\par 
</xsl:text>
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 AMENDED SOLICITOR INSTRUCTIONS AS OF </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->
		<xsl:value-of select="//General/CurrentDate"/>
		<xsl:text>\par\par\par}

\pard \ql 
{\f1 \fs20 
</xsl:text>
		<xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50]/Contact">
			  <!--#DG364 -->
  		  <xsl:call-template name="ContactName"/>
        <xsl:text>\par\par </xsl:text> 
				<xsl:value-of select="../partyCompanyName"/>
				<xsl:text>\par </xsl:text>
				
        <!--#DG364 -->		
    		<xsl:for-each select="Addr">
      		<xsl:call-template name="ContactAddress"/>
    		</xsl:for-each>		
				
				<!--#DG364 xsl:value-of select="./Contact/Addr/addressLine1"/>
				<xsl:text>\par </xsl:text>
				<xsl:if test="./Contact/Addr/addressLine2">
  				<xsl:value-of select="./Contact/Addr/addressLine2"/>
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:value-of select="./Contact/Addr/city"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="./Contact/Addr/province"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="./Contact/Addr/postalFSA"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="./Contact/Addr/postalLDU"/-->
				<xsl:text>\par </xsl:text>
				
				<xsl:if test="contactPhoneNumber">
					<xsl:text>Phone: </xsl:text>
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="contactPhoneNumber"/>
					</xsl:call-template>
					<xsl:if test="contactPhoneNumberExtension!=''">
						<xsl:text> Ext. </xsl:text>
						<xsl:value-of select="contactPhoneNumberExtension"/>
					</xsl:if>
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:if test="contactFaxNumber">
					<xsl:text>Fax: </xsl:text>
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="contactFaxNumber"/>
					</xsl:call-template>
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:text>{\pard \f1\fs20\sa180\sb180 Dear  </xsl:text>

        <!--#DG364 -->
  		  <xsl:call-template name="ContactName"/>
				
				<!--#DG364 xsl:value-of select="./Contact/contactFirstName"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="./Contact/contactLastName"/-->
			<xsl:text>, \par}</xsl:text>
		</xsl:for-each>
    <xsl:text>}</xsl:text>
  	  <xsl:call-template name="BorowTbRow"/>
  		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 RE:\cell 
BORROWER(S):\cell \caps </xsl:text>
    <!--#DG364 xsl:value-of select="//specialRequirementTags/borrowerAndGuarantorNames/borrowerNames/names"/-->
		<xsl:call-template name="BorrowersListedBlocks"/>
		<xsl:text>\cell }
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRow"/>
  		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Surety(ies)</xsl:text>
			</xsl:when>
			<xsl:otherwise>
		    <xsl:value-of select="//specialRequirementTags/GuarantorClause"/>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text>, if any:\cell 
</xsl:text>

		<!--#DG364 xsl:for-each select="//specialRequirementTags/borrowerAndGuarantorNames/guarantorNames/names">
			<xsl:value-of select="./Name"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<xsl:call-template name="GuarantorNames"/>

		<xsl:text>\cell }
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRow"/>
  		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\b\f1\fs20 \cell SECURITY ADDRESS:\cell 
</xsl:text>
     <xsl:call-template name="propAdr"/>	
		<xsl:text>\cell \fs20
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRow"/>
  		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell CLOSING DATE:\cell </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRow"/>
  		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell APPROVED LOAN AMOUNT:\cell </xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRow"/>
  		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell LENDER REFERENCE NUMBER:\cell </xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRow"/>
  		<xsl:text>
\pard\b0\ql \brdrb\brdrs\brdrw30\brsp20 
{\f1\fs10 \par }

\pard\fs20
\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par A </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothecary</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Loan has been approved for the above-noted Borrower(s), in accordance with 
the terms and conditions outlined in the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothecary</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Commitment attached. We have been advised that 
you will be acting for the Borrower(s) as well as 
<!--#DG522 Cervus Financial Corp.--></xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>; hereinafter referred to as 
"<!--#DG522 Cervus-->Macquarie Financial", in attending to the preparation, execution and 
registration of the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> against 
the property and ensuring that the interests of 
<!--#DG522 Cervus-->Macquarie Financial as </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothecary Creditor</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgagee</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> are valid and appropriately 
secured.\par \par }

{\f1\fs20 Any amendments required due to errors, omissions or non-compliance on your part will be 
for your account and your responsibility to correct. To complete the registration of this 
Transaction please refer to our </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Legal counsel's</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Solicitor</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Registration Guidelines located on our website at 
{\field{\*\fldinst{HYPERLINK "http://www.<!--#DG522 cervus-->macquariefinancial.com" }}
{\fldrslt{\cf2\ul www.<!--#DG522 cervus-->macquariefinancial.com}}}
 in the lawyer centre, as well as the enclosed </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Legal Counsel</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Solicitor</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Instructions.\par \par}

\f1\fs20 It is your responsibility to ensure that all solicitor conditions outlined in the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothecary</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Commitment, Solicitor Instructions and Solicitor Registration Guidelines are duly 
noted and fully complied with prior to closing and included in your Final Report.\par\par

</xsl:text>
	</xsl:template>

  <!--*****************************************************-->
  <!--#DG364 not used; this used to be the remainder of the sol. instructions (just above)-->		
	<xsl:template name="SolInstructionsOld">
		
    <xsl:text>{\f1\fs20\b\ul BORROWER IDENTIFICATION \par \par}

{\f1\fs20\ql \b You are required to confirm the identity of the Borrower(s) and Guarantor(s), if 
any. The Lender also requires you to forward a photocopy of the evidence used to confirm their 
identity together with a completed ID Verification Form prior to your release of funds. The ID 
Verification Form is available on our web site at 
{\ul \b http://www.<!--#DG522 cervus-->macquariefinancial.com/lawyercentre.asp} \par \par }
{\f1\fs20\b\ul FUNDING \par \par}
\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 The mortgage funds will be sent to you by Electronic Fund Transfer on {\b </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>} and should be disbursed without delay provided you are satisfied that all 
requirements have been met and adequate precaution has been taken to ensure that the mortgage 
security will retain priority. Interest will accrue from this date unless the Bank receives 
written notification from your office at least 24 hours prior to the scheduled closing date. If 
the Bank does not receive adequate notice from your office, you assume responsibility for the 
accrued interest. \par\par }
\pard\plain \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 If the closing of this transaction is delayed more than 5 business days, all funds must 
be returned to our office immediately. You will also be required to provide written notification 
of the new closing date }
{\f1\fs20\ul no later than 12pm four (4) business days prior to the revised closing date}.
{\f1\fs20  If you are within this four day window, the earliest the transaction would then close 
would be on the fourth business day.\par\par }
{\f1\fs20 The trust account that you have instructed us to deposit 
funds into is: \par \par}</xsl:text>
		<xsl:for-each select="//Deal/PartyProfile">
			<xsl:if test="./partyTypeId = 50">
				<xsl:text>{\f1\fs20 Bank: \tab \tab \tab </xsl:text>
				<xsl:value-of select="./bankName"/>
				<xsl:text> \par }</xsl:text>
				<xsl:text>{\f1\fs20 Transit: \tab \tab </xsl:text>
				<xsl:call-template name="BankTransitWoInstitution"/>
				<xsl:text> \par }</xsl:text>
				<xsl:text>{\f1\fs20 Account No: \tab \tab </xsl:text>
				<xsl:call-template name="AccountNumWStars"/>
				<xsl:text> \par\par\par }</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>
\pard\plain \s22\qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 If this information is incorrect or has changed please contact us immediately. Please 
note that the first and last digits of your account and transit number have been blocked for 
security purposes. \par\par }
\pard\plain \ql <!--#DG142 added keep's-->\keep\keepn\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 

{\f1\fs20\b\ul CONFIRMATION OF CLOSING\par\par }
{\f1\fs20 \qj We require that you complete and return the enclosed Confirmation of Closing within 24 hours of the scheduled closing date to confirm that this transaction has closed, that funds were disbursed. <!--#DG142 deleted \par -->\par }
</xsl:text>
		<xsl:text>\page</xsl:text>
		<xsl:text>
{\f1\fs20\b\ul TITLE INSURANCE REQUIREMENT\par\par }
{\f1\fs20 \qj If this is a non-purchase mortgage transaction in any province or a purchase 
transaction in Ontario, the Lender requires the Borrower to purchase a lender title insurance 
policy. The Lender will accept Title Insurance issued by:\par \par}

{\f1\fs20 \tab - }{\f1\fs20 \tab First Canadian Title Company \par}
{\f1\fs20 \tab - }{\f1\fs20 \tab Stewart Title Gurantee Company \par}
{\f1\fs20 \tab - }{\f1\fs20 \tab St. Paul Gurantee Company \par}
{\f1\fs20 \tab - }{\f1\fs20 \tab TitlePlus (Ontario only) \par}
{\f1\fs20 \tab - }{\f1\fs20 \tab Fidelity National \par \par}

{\f1\fs20\b\ul TERMS AND CONDITIONS \par\par }

{\f1\fs20 \qj In connection with this transaction we enclose the Mortgage Commitment Letter and 
Solicitor\rquote s Guide. Please ensure that all outstanding conditions outlined in the Mortgage 
Commitment Letter and Solicitor\rquote s Guide that are the responsibility of the Solicitor, are duly 
noted and fully complied with prior to closing and included in your Final Report. \par\par }

{\f1\fs20 \qj All mortgages are to be registered in the name of 
{\b <!--#DG522 Cervus Financial Corp.--></xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>} Our address for service is 
{\b 20 Toronto Street, Suite 830, Toronto, Ontario M5C 2B8}.\par\par }

{\f1\fs20 \ql All applicable mortgage schedules and standard charge terms are available on our 
web site:  \ul \b http://www.<!--#DG522 cervus-->macquariefinancial.com/lawyercentre.asp \par \par }

{\f1\fs20 \qj We consent to your acting for the Lender as well as the Borrower(s) and/or 
Guarantor(s) provided that you disclose this fact to the Borrower(s) and/or Guarantor(s) and 
obtain their consent in writing and that you disclose to each party all information you possess 
or obtain which is or may be relevant to this transaction.\par\par }

{\f1\fs20 \qj Legal fees and all other costs, charges and expenses associated with this 
transaction are payable by the Borrower(s) whether or not the Mortgage proceeds are 
advanced.\par\par }
{\f1\fs20\b\ul SOLICITOR\rquote S FINAL REPORT\par\par }

{\f1\fs20 \qj <!-- #DG122 In connection with this transaction we enclose the Solicitor's Final Report on Title-->
The Solicitor\rquote s Final Report on Title can be found on our website at 
{\b\ul http://www.<!--#DG522 cervus-->macquariefinancial.com/lawyercentre.asp}. 
You must complete and return the report and 
require enclosures by 30 days after closing. Failure to comply with any of our instructions may 
result in discontinued future dealings with you or your firm. \par\par }

\pard\plain \s2\ql \keepn\widctlpar\aspalpha\aspnum\faauto\<!--#DG142 outlinelevel1\-->adjustright\itap0 
{\b\f1\fs20 Final Reports and supporting documentation should be mailed to: \par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\par }
\pard \qj \widctlpar\aspalpha\aspnum\faauto\<!--#DG142 outlinelevel0\-->adjustright\itap0 
{\f1\fs20 <!--#DG142 deleted \par --> 
\par 
\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 <!--#DG522 Cervus Financial Corp.--></xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>\par }
{\f1\fs20 20 Toronto Street, Suite 830\par }
{\f1\fs20 Toronto, Ontario\par }
{\f1\fs20 M5C 2B8 \par }
{\f1\fs20 Attention: Servicing Department \par }
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\<!--#DG142 outlinelevel0\-->adjustright\itap0 
</xsl:text>
		<!--
<xsl:text>
{\f1\fs20 
\par Contact: </xsl:text>
<xsl:value-of select="//specialRequirementTags/currentUser/Contact/contactFirstName"></xsl:value-of><xsl:text>  </xsl:text>
<xsl:value-of select="//specialRequirementTags/currentUser/Contact/contactLastName"></xsl:value-of>
<xsl:text>
\par Mortgage Service Representative \par 
<xsl:text>
\par Phone: \tab </xsl:text>
			<xsl:call-template name="FormatPhone">
				<xsl:with-param name="pnum" select="//specialRequirementTags/currentUser/Contact/contactPhoneNumber"/>
			</xsl:call-template>	
<xsl:text>\tab </xsl:text><xsl:text> Ext. </xsl:text>
<xsl:value-of select="//specialRequirementTags/currentUser/Contact/contactPhoneNumberExtension"/>

<xsl:text>\par Fax: \tab </xsl:text>
			<xsl:call-template name="FormatPhone">
				<xsl:with-param name="pnum" select="//specialRequirementTags/currentUser/Contact/contactFaxNumber"/>
			</xsl:call-template>	
<xsl:text> \tab </xsl:text>
<xsl:text>\par Email: \tab </xsl:text>
<xsl:value-of select="//specialRequirementTags/currentUser/Contact/contactEmailAddress"/>
-->
		<xsl:text>
{\f1\fs20 
\par Phone: \tab (416) 861-1315 
\par \tab 1 877 GO-CERVUS \par
\par Fax: \tab (416) 861-8484
\par \par
If you have any question or concerns, please feel free to call. \par }
\pard \ql \fi-360\li360\widctlpar\aspalpha\aspnum\faauto\ilvl12\adjustright\lin360\itap0 
{\f1\fs20\par }
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par 
\par Sincerely,\tab \tab 
\par </xsl:text>
		<xsl:for-each select="//Deal/UserProfile[userProfileId=//Deal/administratorId]">
			<xsl:value-of select="concat(Contact/contactFirstName, ' ', Contact/contactLastName)"/>
			<xsl:text> Ext. </xsl:text>
			<xsl:value-of select="Contact/contactPhoneNumberExtension"/>
		</xsl:for-each>
		<xsl:text>
\par Mortgage Fulfillment Specialist
\par </xsl:text>
		<xsl:text>
\par <!--#DG522 Cervus Financial Corp.--></xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text> \par Encl. \par}
</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="FormatPhone">
		<xsl:param name="pnum"/>
		<xsl:if test="string-length($pnum)=10">
			<xsl:text>(</xsl:text>
			<xsl:value-of select="substring($pnum, 1, 3)"/>
			<xsl:text>) </xsl:text>
			<xsl:value-of select="substring($pnum, 4, 3)"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="substring($pnum, 7, 4)"/>
		</xsl:if>
	</xsl:template>
	<!-- 111-222333444 -->

	<!-- new requirement: pvcs #554 -->
	<xsl:template name="BankTransitWoInstitution">
		<xsl:if test="contains(./bankTransitNum, '-')">
			<xsl:variable name="transNum" select="substring-after(./bankTransitNum, '-')"/>
			<xsl:text>*</xsl:text>
			<xsl:value-of select="substring($transNum, 2, (string-length($transNum) -2))"/>
			<xsl:text>*</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template name="AccountNumWStars">
		<xsl:text>*</xsl:text>
		<xsl:value-of select="substring(./bankAccNum, 2, (string-length(./bankAccNum) -2))"/>
		<xsl:text>*</xsl:text>
	</xsl:template>

	<!-- ========================================================================== -->
	<!-- RTF file start -->
	<!-- ========================================================================== -->
	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff63\deflang1033\deflangfe1033
{\fonttbl
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f14\fnil\fcharset2\fprq2{\*\panose 05000000000000000000}Wingdings;}
}
{\colortbl;
\red0\green0\blue0;
\red0\green0\blue255;
\red0\green255\blue255;
\red0\green255\blue0;
\red255\green0\blue255;
\red255\green0\blue0;
\red255\green255\blue0;
\red255\green255\blue255;
\red0\green0\blue128;
\red0\green128\blue128;
\red0\green128\blue0;
\red128\green0\blue128;
\red128\green0\blue0;
\red128\green128\blue0;
\red128\green128\blue128;
\red192\green192\blue192;
\red255\green255\blue255;
}
{\stylesheet
{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \snext0 Normal;}
{\s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 \sbasedon0 \snext0 heading 1;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext15 header;}
{\s16\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext16 footer;}
}

<!--#DG364 \margl720\margr720\margt720\margb720 -->
\margl432\margr432\margt720\margb720 

\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0
\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp
\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0
\sectd \linex0\headery540\footery480\endnhere\sectlinegrid360\sectdefaultcl 

</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileEnd">
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!--#DG142 new compressed format used-->
	<xsl:template name="LenderLogo">	
		<!--#DG326 xsl:text>{\pict\picscalex100\picscaley100\piccropl0\piccropr0\piccropt0\piccropb0 ...-->		
		<xsl:value-of select="$logoRtf"/>
	</xsl:template>

	<!--xsl:include href="CRV_SOL02.xsl"/-->
	<!-- ************************************************************************ 	-->
	<!-- #DG116 all docs merged 	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="SolGuideEn">
		<!--#DG142 xsl:call-template name="RTFFileStart2"/-->
		<xsl:call-template name="SolGuidHeadEn"/>
		<!--#DG364 xsl:call-template name="Start"/>
		<xsl:call-template name="MortgageDetailsEn"/-->
		<xsl:call-template name="AdvancesEn"/>
		<xsl:call-template name="FundDetails"/>
		<!--#DG364 xsl:call-template name="Other"/>
		<xsl:text>\page </xsl:text>
		<xsl:call-template name="Searches"/-->
		<!--xsl:call-template name="RTFFileEnd2"/-->
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- templates section                                                                                -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="SolGuidHeadEn">
		<xsl:text>\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto
\adjustright\rin360\lin0\itap0 <!--#DG522 {\par }--></xsl:text><!--#DG142 -->
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 AMENDED SOLICITOR INSTRUCTIONS AS OF </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->

    <!--#DG364 -->
		<xsl:text>{\pard\sa360\ql\f1\fs28\b </xsl:text>
		<xsl:call-template name="LenderLogo"/>
		<xsl:text><!--#DG364 SOLICITOR\rquote S GUIDE -->
            \ul REQUISITION FOR FUNDS
\par}</xsl:text>

		<xsl:call-template name="FaxToEn"/>
		<xsl:text>\pard\par\f1\fs20\b You are required to submit this Requisition for Funds at least 
three (3) business days prior to closing.\line 
If the Requisition for Funds is not received three (3) 
business days prior to closing funding may be delayed.\par\par

\b0 </xsl:text>
		
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="Start">
		<xsl:text>{\f1\fs20 {\qj If you are satisfied that the Mortgagor(s) (also known 
as the \'93Borrower(s)\'94) has or will acquire a good and marketable title to the property 
described in our Commitment Letter, complete our requirements as set out below, and prepare a 
Mortgage as outlined herein.\par }}
\pard\plain \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
{\pard \f1\fs20\sa180 \b\ul DOCUMENTATION: \par}
{\pard\f1\fs20 A mortgage form must be registered. Copies of schedules, if applicable, and 
standard charge terms can be obtained from our web site: 
{\b \ul http://www.cervus.com/lawyercentre.asp.}}
{\f1\fs20 \par \par }
</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="MortgageDetailsEn">
		<xsl:text>
{\pard\f1\fs20\sa180\ql \b\ul </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>HYPOTHEC</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>MORTGAGE</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> DETAILS:}
{\f1\fs20 \par \par }

{\f1\fs20\ql All applicable Provincial mortgage forms and standard charge terms are available on 
our web site: 
{\field{\*\fldinst{HYPERLINK "http://www.<!--#DG522 cervus-->macquariefinancial.com" }}
{\fldrslt{\cf2\ul http://www.<!--#DG522 cervus-->macquariefinancial.com}}}
 in the Lawyer Centre.\par \par }

{\pard \f1\fs20\sa180
The </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Document is to be registered MONTHLY as follows:
\par}</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>\pard \f1\fs20 \ql \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Total </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Amount:\cell </xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Type:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/dealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Priority:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/lienPosition"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Term:\cell }
{\f1
</xsl:text>
		<!-- <xsl:value-of select="//specialRequirementTags/PaymentTerm"/> -->
		<xsl:value-of select="//Deal/MtgProd/paymentTerm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
	  <xsl:call-template name="MtgDetailTbRow"/>
		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright</xsl:text>
		<!-- insert logic here 
<xsl:call-template name="VariableRateMortgageDetails"/>
 end insert logic here -->
		<!-- change the logic again and again - #554 -->

    <!--#DG364 -->
  	<xsl:text>{\f1 Interest Rate for Registration: \cell </xsl:text>
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3' or //Deal/MtgProd/interestTypeId='2'">
				<!-- Variable rate mortgage (VRM) or Capped Variable -->
				<!-- row1 -->
				<xsl:text>Prime \emdash  <!--#DG322 0.75-->0.90% </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<!-- row2 -->
				<xsl:value-of select="//Deal/netInterestRate"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- end change the logic again - #554 -->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Amortization: \cell }
{\f1 </xsl:text>
		<!--#DG364 xsl:value-of select="number(//Deal/amortizationTerm) div 12"/>
		<xsl:text> years</xsl:text-->
		<xsl:value-of select="/*/specialRequirementTags/AmortizationInYrsMnth"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Advance Date: \cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Interest Adjustment Date:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/interimInterestAdjustmentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Maturity Date:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/maturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
<!--  #554
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 First Regular Payment Date:\cell }
{\f1 </xsl:text>
<xsl:value-of select="//Deal/firstPaymentDate"/>
<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}--><!-- #554 -->

\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Monthly P &amp; I Payment: \cell }<!--#DG364 -->
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/PandIPaymentAmountMonthly"/>
		<xsl:text> \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>

<!--#DG364 added Calculation Period -->
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Calculation Period:\cell}
{\f1 </xsl:text>
		<xsl:variable name="mtgProdIdt" select="/*/Deal/mtgProdId"/>

		<!--#DG364 xsl:value-of select="/*/Deal/paymentFrequency"/-->		
		<!--xsl:text>Monthly</xsl:text -->
		<!-- #DG364 Sasa -->
		<xsl:choose>
			<!-- #DG564 <xsl:when test="$mtgProdIdt=5 or $mtgProdIdt=6"> -->
			<xsl:when test="$mtgProdIdt=5 or $mtgProdIdt=12">
				<xsl:text>Monthly</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Semi-annually not in advance</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- End of #DG364 Sasa -->
		<xsl:text> \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>

<!--#DG364 
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Interest Calculated: \cell }
{\f1 </xsl:text>
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3' or //Deal/MtgProd/interestTypeId='2'">
				<xsl:text>Monthly</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Semi-annually not in advance</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}<!- - #554 end --></xsl:text>

<!--#DG364 ???? i guess it is the one below...
Please insert the BC registration number if property type is British Columbia
-->

		<!-- #DG116 -->
		<xsl:if test="/*/Deal/Property/provinceId=2">
			<xsl:text>\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 <!--#DG522 Cervus -->Macquarie Financial extraprovincial registration number:\cell }
{\f1 A0062234\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
		</xsl:if>
		<!-- #DG116 end -->
		
		<!--#DG142 xsl:text>}</xsl:text-->
		<xsl:text>\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 
<!--#DG364 
Note: If the closing date gets moved from one month to the next, ALL dates shown 
above must be changed accordingly. -->
For the purposes of registration, the First Payment Date is one month after the 
Interest Adjustment Date.  If the closing date changes from one month to the next, ALL dates 
shown above must be changed accordingly.
\par}

</xsl:text>

    <!--#DG352 -->
		<xsl:text>{\b\f1\fs20 </xsl:text>
		<xsl:choose>
			<xsl:when test="$mtgProdIdt=5 or $mtgProdIdt=12"><!--#DG374 added -->
				<xsl:text>{\par\ul ARM FIXED PAYMENT:}\par
Rate adjusts every month on the first of each month as 
<!--#DG522 Cervus-->Macquarie Financial Prime rate changes. Payments are 
based on 3 year rate as at 3 days prior to closing and will remain fixed for the duration of the 
term.\par
</xsl:text>
			</xsl:when>
			<xsl:when test="$mtgProdIdt=6">
				<xsl:text>{\par\ul ARM ADJUSTABLE PAYMENT:}\par
Payments are based on the ARM rate and are adjusted on the first of each month as the 
<!--#DG522 Cervus-->Macquarie Financial Prime rate changes. The Borrower has 
selected to have their mortgage payment based on the actual 
Adjustable interest rate and is required to execute our Acknowledgement and Consent form 
available on our web site at 
{\field{\*\fldinst{HYPERLINK "http://www.<!--#DG522 cervus-->macquariefinancial.com" }}
{\fldrslt{\cf2\ul www.<!--#DG522 cervus-->macquariefinancial.com}}}.\par
</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>}</xsl:text>
    <!--#DG352 end -->
    
		<!--#DG364 xsl:text>{\pard \qj \f1\fs20\sa240\b
Payments have been set up as outlined on the Mortgage Commitment.  If the Borrower(s) would like to change payment frequency or amount, please have them complete the Payment Change Request Form and forward via fax to (416) 861-8484 as soon as possible.  The Payment Change Request Form is available on our web site at {\b \ul http://www.cervus.com/lawyercentre.asp}.\par}
{\pard \qj \f1\fs20\sa240
In the event of an interest rate change resulting in a reduced rate, the Lender will advise the Mortgagor(s) of the new payment terms directly, by letter, after closing.  You are not required to change the interest rate or payment amount in the mortgage itself.
\par}
</xsl:text-->
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="VariableRateMortgageDetails">
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3'">
				<!-- Variable rate mortgage (VRM) -->
				<!-- row1 -->
				<xsl:text>{\f1Base Rate:\cell }{\f1 </xsl:text>
				<xsl:value-of select="//Deal/postedRate"/>
				<xsl:text>\cell }
\pard \ql  \sa200 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
				<!-- row2 -->
				<xsl:text>{\f1Rate Variance: \cell }{\f1 </xsl:text>
				<xsl:choose>
					<xsl:when test="//Deal/discount='0.000%' and //Deal/premium='0.000%'">
						<xsl:text>0.000%</xsl:text>
					</xsl:when>
					<xsl:when test="not(//Deal/discount) and not(//Deal/premium)">
						<xsl:text>0.000%</xsl:text>
					</xsl:when>
					<xsl:when test="number(substring-before(//Deal/discount, '%')) &gt; 0">
						<xsl:text>-</xsl:text>
						<xsl:value-of select="//Deal/discount"/>
					</xsl:when>
					<xsl:when test="number(substring-before(//Deal/premium, '%')) &gt; 0">
						<xsl:value-of select="//Deal/premium"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>error!</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>\cell }
\pard \ql \sa200 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
				<!-- row3 -->
				<xsl:text>{\f1Equivalent Rate:\cell }{\f1 </xsl:text>
				<xsl:value-of select="//Deal/netInterestRate"/>
				<xsl:text>\cell }
\pard \ql  \sa200 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\f1Interest Rate:\cell }{\f1 </xsl:text>
				<xsl:value-of select="//specialRequirementTags/InterestRate"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="AdvancesEn">
		<!-- ===================================  #642 =================================== -->
		<!-- 
<xsl:variable name="loan-amnt" select="substring-after(translate(//Deal/totalLoanAmount,',',''),'$')"/>
<xsl:variable name="total-dedts" select="substring-after(translate(//specialRequirementTags/deductibleFees/totalDeductionsAmount,',',''),'$')"/>
<xsl:variable name="net-advance" select="number($loan-amnt) - number($total-dedts)"/>

<xsl:text>loan-amnt = </xsl:text><xsl:value-of select="$loan-amnt"/><xsl:text>\par </xsl:text>
<xsl:text>total-dedts = </xsl:text><xsl:value-of select="$total-dedts"/><xsl:text>\par </xsl:text>
<xsl:text>number(loan-amnt) = </xsl:text><xsl:value-of select="format-number(number($loan-amnt),'0.00')"/><xsl:text>\par </xsl:text>
<xsl:text>number(total-dedts) = </xsl:text><xsl:value-of select="number($total-dedts)"/><xsl:text>\par </xsl:text>
<xsl:text>net-advance = </xsl:text><xsl:value-of select="$net-advance"/><xsl:text>\par </xsl:text><xsl:text>\par </xsl:text>
-->
		<xsl:text>
{\pard\f1\fs20\sa240\ul\b 
</xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>HYPOTHEC</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>MORTGAGE</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> ADVANCE\par}
{\pard\f1\fs20
When all conditions precedent to this transaction have been met, funds in the amount of {\b </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/netAdvance"/>
		<xsl:text>} will be advanced. \par }
\pard \qj \fi720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 
{\f1\fs20 \par }
</xsl:text>

  	<xsl:call-template name="MtgAdvanceTbRowA"/>

		<xsl:text>\ql\fs20\b PRINCIPAL AMOUNT:\b0\fs24\cell 
\pard\intbl\qr\fs20 </xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>\fs24\cell 
\row 
</xsl:text>

  	<xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\ql\sb120\fs20\sb120 \tab Total Interest Adjustment Amount:\cell 
\qr </xsl:text>
		<xsl:value-of select="/*/Deal/interimInterestAmount"/>
		<xsl:text>\cell 
\row 
</xsl:text>

		<xsl:for-each select="//specialRequirementTags/deductibleFees/DealFee">
      <xsl:call-template name="MtgAdvanceTbRowA"/>
      <xsl:text>\sb120\ql \tab </xsl:text>
      <!--#DG364 xsl:value-of select="./Fee/feeDescription"/--> 
      <xsl:value-of select="./Fee/feeType"/> 
      <xsl:text>:\cell \qr </xsl:text>
      <xsl:value-of select="feeAmount"/>
      <xsl:text>\cell 
\row 
</xsl:text>
		</xsl:for-each>
		
  	<xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\pard\intbl\sb120\sa180\ql \tab Total Deductions:\cell 
\pard\intbl\sb120\sa180\qr </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/totalDeductionsAmount"/>
		<xsl:text>\cell 
\row 
</xsl:text>
  	<xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\ql\b NET ADVANCE:</xsl:text>
		<xsl:text>\fs24\cell 
\pard\intbl\fs20\qr </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/netAdvance"/>
		<xsl:text>\b0\fs24\cell 
\row 

\fs20 </xsl:text>
		<!-- 
<xsl:call-template name="createRefundableFeeBlankRow"></xsl:call-template>
<xsl:for-each select="//SolicitorsPackage/Fees/refundableFee">//SolicitorsPackage/Fees/refundableFee
<xsl:call-template name="createRefundableFeeRows"></xsl:call-template>
</xsl:for-each>
-->
  	<xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\qr\cf0\fs20\cell\cell
\row 
</xsl:text>
  	<xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\pard\intbl\ql Interest Adjustment (daily per diem):\cell
\pard\intbl\qr </xsl:text>
		<xsl:value-of select="//Deal/perdiemInterestAmount"/>
		<xsl:text>\cell 
\row 
</xsl:text>
		<!-- 

\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par The net Mortgage proceeds, payable to you, in trust, will be deposited into your Trust account on the scheduled closing date, which is anticipated to be </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>. 
\par \par }</xsl:text>
		<xsl:if test="//SolicitorsPackage/Refi">[if //SolicitorsPackage/Refi]
			<xsl:text>\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Refinances:}
{\f1\fs20  If the purpose of this Mortgage is to refinance an existing debt, the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \sa120\keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Existing </xsl:text>
			<xsl:choose>
				<xsl:when test="//Deal/lienPositionId='0'">First</xsl:when>
				<xsl:otherwise>Second</xsl:otherwise>
			</xsl:choose>			
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Debts to be Paid and Accounts Closed:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 

}</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
			<xsl:for-each select="//Deal/Borrower/Liability">
				<xsl:text>\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
				<xsl:value-of select="./liabilityDescription"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./liabilityAmount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
		</xsl:if>
 _________________________________________________________________________________________  -->
	</xsl:template>
	<!-- ================================================  -->

	<xsl:template name="FundDetails">	
		<xsl:text>\pard\sa240\ul\b\fs20\par FUNDING DETAILS\b0\ulnone\par 
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Borrower Name:\par\cell
\pard\intbl\caps </xsl:text>
    <!--#DG364 xsl:value-of select="//specialRequirementTags/borrowerAndGuarantorNames/borrowerNames/names"/-->
		<xsl:call-template name="BorrowersListedBlocks"/>
		<xsl:text>\line\caps0\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Property Address:\par\cell
\pard\intbl\caps </xsl:text>
     <xsl:call-template name="propAdr"/>	
		<xsl:text>\fs20\par\caps0\cell
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Lender Reference Number:\par\cell
\pard\intbl </xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>\cell
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Net Advance Requested:\par\cell
\pard\intbl </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/netAdvance"/>
		<xsl:text>\cell
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Date Funds Are Required:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Title Insurance Company:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Title Insurance Policy Number:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Fire Insurance/Certificate of Insurance Company:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Fire Insurance/Certificate of Insurance Policy Number:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
    <xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50]">

  	  <xsl:call-template name="MtgAdvanceTbRow"/>
 	<!-- #DG364 Sasa -->
	<!-- xsl:text>Solicitor -->
	<xsl:choose>
		<xsl:when test="$isQebecProperty">
	  		<xsl:text>Legal Counsel</xsl:text>
		</xsl:when>
		<xsl:otherwise>
	  		<xsl:text>Solicitor</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<!-- End of #DG364 Sasa -->

	<xsl:text> Name:\par\cell
\pard\intbl </xsl:text>
      <xsl:for-each select="Contact">
  		  <xsl:call-template name="ContactName"/>
  		</xsl:for-each>
  		<xsl:text>\par\cell
\row
</xsl:text>
  	  <xsl:call-template name="MtgAdvanceTbRow"/>
  		<xsl:text>Bank:\par\cell
\pard\intbl </xsl:text>
			<xsl:value-of select="./bankName"/>
			<xsl:text>\par\cell
\row
</xsl:text>
  	  <xsl:call-template name="MtgAdvanceTbRow"/>
  		<xsl:text>Transit:\tab\cell
\pard\intbl </xsl:text>
			<xsl:call-template name="BankTransitWoInstitution"/>
			<xsl:text>\line\cell
\row
</xsl:text>
  	  <xsl:call-template name="MtgAdvanceTbRow"/>
  		<xsl:text>\qj Account No:\tab\cell
\pard\intbl </xsl:text>
			<xsl:call-template name="AccountNumWStars"/>
			<xsl:text>\line\cell
\row
</xsl:text>
    </xsl:for-each>
    
		<xsl:text>\pard\line NOTE: The first and last digits of your account and transit number have 
been blocked for security purposes. If this information is incorrect or has changed please 
contact us immediately.\line\line 
</xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> proceeds will be electronically deposited into your trust account on the date requested, 
provided all Source of Business conditions on the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothecary</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Commitment have been satisfied.  
Please refer to the Solicitor Registration Guidelines 
for additional funding requirements.\line\par

\pard\qj\cf0\par
\par
________________________________________ \tab\tab\tab ______________________________________\line 
Signature of </xsl:text>
	<!-- #DG364 Sasa -->
	<xsl:choose>
		<xsl:when test="$isQebecProperty">
	  		<xsl:text>Legal Counsel</xsl:text>
		</xsl:when>
		<xsl:otherwise>
	  		<xsl:text>Solicitor</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<!-- End of #DG364 Sasa -->
  <xsl:text>\tab\tab\tab\tab\tab\tab Date\par
\pard <!--#DG522 \sa240\ulnone\b0\par-->
</xsl:text>
	</xsl:template>


	<!-- ================================================  -->
	<xsl:template name="Other">
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
FIRE INSURANCE
\par}
{\pard\f1\fs20\qj\sa180 
You must ensure, prior to advancing funds, that fire insurance coverage for building(s) on the 
property is in place for not less than their full replacement value with</xsl:text>
		<xsl:choose>
			<xsl:when test="//Deal/LenderProfile/lenderProfileId = '0'"> first </xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//Deal/lienPositionId = '0'"> first </xsl:when>
					<xsl:otherwise> second </xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> payable to </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<!--
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">			
			<xsl:text> and </xsl:text>
				<xsl:call-template name="DealFunderName"/>				
			</xsl:when>
		</xsl:choose>		
		-->
		<!-- <xsl:text> and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> as Mortgagee at the address shown on the Charge/Mortgage and including a duly completed standard mortgage clause approved by the Insurance Bureau of Canada. Policies must not contain a co-insurance or similar clause that may limit the amount payable.
\par }
</xsl:text>
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
MATRIMONIAL PROPERTY ACT
\par}
{\pard\f1\fs20\qj\sa180
You are to certify that all requirements under the Matrimonial Property Act in your province, where applicable, have been met and that the status does not in any way affect our mortgage/charge.
\par }
</xsl:text>
	</xsl:template>
	<!-- ================================================  -->

	<!-- ================================================  -->
	<xsl:template name="Searches">
		<xsl:text>
{\pard\f1\fs20\sa180\ul\b 
SEARCHES
\par}
{\pard\f1\fs20\qj\sa180
You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. </xsl:text>
		<xsl:value-of select="//specialRequirementTags/Execution"/>
		<xsl:text>Prior to releasing any mortgage proceeds, the solicitor will carry out the necessary searches with respect to any liens, encumbrances, executions that may be registered against the property. </xsl:text>
		<xsl:text>It is your responsibility to ensure that there are no work orders or deficiency notices outstanding against the property, that all realty taxes and levies which have or will become due and payable up to the closing date are paid to the Municipality. If your search reveals title has been transferred since the date of the Offer to Purchase and before the closing date you must notify us immediately and discontinue all work on this transaction unless notified otherwise. Immediately prior to registration of the mortgage, you are to obtain a Sheriff\rquote s Certificate/General Registry Search indicating that there are no writs of execution on file against the mortgagor(s), guarantor(s), if any, or any previous owner, which would adversely affect our security.
\par }
</xsl:text>
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
SURVEY REQUIREMENTS (Non Title Insured Transactions Outside of Ontario)
\par}
{\pard\f1\fs20\qj\sa180
You must obtain and review a survey or a surveyor's certificate / Real Property Report (AB, NL, and SK) completed by a recognized land surveyor and dated within the last twenty (20) years. Satisfy yourself from the survey that the position of the buildings on the land complies with all municipal, provincial and other government requirements. Where an addition has been made since the date of the survey, an updated survey is required unless there is no doubt that the addition is also clearly within the lot lines and meets all setback requirements. Where a survey is not available for conventional uninsured mortgages and you are satisfied that the position of the building on the land complies with all municipal, provincial and other government requirements, a declaration of possession (or statutory declaration) for not less than ten (10) continuous years and indicating no changes to or disputes regarding the property is acceptable.
\par }
{\pard\f1\fs20\qj\sa180 \b
Additional Provincial Requirements: 
\par }
</xsl:text>
		<xsl:call-template name="SurveyRequirements_row"/>
		<xsl:text>{\f1\fs20 Alberta:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 
A Stamp of Compliance is required. If the Real Property report is older than six (6) months, an Affidavit of Examination by the Vendor is required confirming no changes have transpired since the original survey was completed.
}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="SurveyRequirements_row"/>
		<xsl:text>{\f1\fs20 Manitoba:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 
A Zoning Memorandum is required. If the survey is older than 10 years, an Affidavit of Examination by the Vendor is required confirming no changes have transpired since the original survey was completed. 
}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="SurveyRequirements_row"/>
		<xsl:text>{\f1\fs20 Saskatchewan:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 
A Zoning Memorandum is required. If the Real Property report is older than twenty (20) years, an Affidavit of Examination by the Vendor is required confirming no changes have transpired since the original survey was completed.
}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:text>
{\pard\f1\fs20\qj\sb180\sa180 \b
Note: If you are unable to comply with our survey requirements we will accept a title insurance policy from our approved list of suppliers. 
\par }
</xsl:text>
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
CONDOMINIUM (if applicable)
\par}
{\pard\f1\fs20\qj\sa180
You are to confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. You are to review the Condominium Corporation\rquote s Declaration and Bylaws and confirm they contain nothing derogatory to our security. You are to assign the voting rights to </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> and </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text> and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> (if applicable).
\par}
</xsl:text>
		<!-- 
<xsl:text>
{\pard\f1\fs20\qj\sa180\b
The following clause must be inserted into the Mortgage:
\par }

{\pard\f1\fs20\qj\sa180 
In the event that the Mortgagor sells, transfers, assigns or conveys any parking unit(s) encumbered by the Mortgage while retaining title to (or ownership of) the dwelling unit so encumbered by the Mortgage, or in the event that the Mortgagor sells, transfers, assigns or conveys the aforementioned parking until(s) as well as the said dwelling unit but to different purchasers, transferees or assignees, then in either case the total outstanding principal and interest indebtedness secured by the Mortgage shall become due and payable.
\par }
</xsl:text>
-->
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
PLANNING ACT (Ontario only)
\par}
{\pard\f1\fs20\qj\sa180
You must ensure that the Mortgage does not contravene the provisions of the Planning Act as amended from time to time, and that the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.
\par }
</xsl:text>
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
NEW CONSTRUCTION
\par}
{\pard\f1\fs20\qj\sa180
If this transaction is a new construction, you must ensure that an acceptable Certificate of Completion and New Home Warranty Certificate are obtained prior to advancing the mortgage proceeds.
\par }
</xsl:text>
		<!-- #554, edition #2
<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
RENEWAL OF GUARANTOR(S)
\par}
{\pard\f1\fs20\qj\sa180
The following wording must be included in a Schedule to the Charge/Mortgage if there is a Guarantor:  
\par}
{\pard\f1\fs20\qj\sa180
 In addition to the Covenantor's promises and agreements contained in this Mortgage, the Covenantor(s) also agree(s) that at the sole discretion of Cervus Financial Corp., the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s).
 par }
</xsl:text>
-->
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
SOLICITOR\rquote S FINAL REPORT
\par}
{\pard\f1\fs20\qj\sa180
You must submit the Solicitor\rquote s Final Report on Title, on the enclosed form and with the stated enclosures, within 30 days after the final advance is made. If your report cannot be submitted in this time frame, you must provide us with a letter explaining the reason(s) for the delay. 
{\b Failure to comply with any of the above instructions may result in discontinued future dealings with you or your firm.}
\par }
</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="createRefundableFeeBlankRow">
		<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:text>\cell </xsl:text>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620\row 
}
</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- row definitions                                                                                      -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="SurveyRequirements_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10800 
\pard \intbl</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="DealFunderName">
		<xsl:for-each select="//Deal/UserProfile">
			<xsl:if test="./userTypeId='2'">
				<xsl:value-of select="concat(./Contact/contactFirstName,' ', ./Contact/contactLastName)"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- rtf file start and rtf file end                                                                    -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="RTFFileEnd2">
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- legal size  (216 �- 356 mm  or  612pt  x 1008pt); margings: top=36pt, bottom =36pt, left=36pt, right=36pt-->
	<xsl:template name="RTFFileStart2">
		<xsl:text>		
\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\info {\author Catherine Rutgaizer}{\operator filogix automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company filogix inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \paperw12240 <!--#DG142 \paperh20160 -->\margl720\margr720\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\footer \pard\plain \ql \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\field {\*\fldinst }
{\fldrslt {\lang1024\langfe1024\noproof 1}}}
\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }}
</xsl:text>
	</xsl:template>

	<!--xsl:include href="CRV_SOL04.xsl"/-->
	<xsl:template name="MtgCmtEn">
		<xsl:call-template name="MtgHeaderEn"/>
		<!-- <xsl:call-template name="MtgFooterEn"/>-->
		<xsl:call-template name="MtgPageAEn"/>
		<xsl:call-template name="MtgPageBStartEn"/>
		<xsl:call-template name="MtgPageBEn"/>
		<!--#DG142 xsl:call-template name="MtgPageCStartEn"/-->
		<xsl:call-template name="MtgPageCEn"/>
		<!--xsl:call-template name="RTFFileEnd4"/-->
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- templates section                                                                                -->
	<!-- ************************************************************************ -->
	<!-- ================================================  -->

	<!-- ================================================ EnglishHeader start  ================================================  -->
	<xsl:template name="MtgHeaderEn">
		<xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text><!--#DG142 -->
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 AMENDED SOLICITOR INSTRUCTIONS AS OF </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->
		<!-- #DG116 changed header to title else it would apply to the whole doc -->
		<xsl:text>{\pard\plain \s20\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\i\f1\fs28 </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>HYPOTHECARY</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>MORTGAGE</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> COMMITMENT}\par}</xsl:text>
	</xsl:template>

	<!-- ================================================ EmptyHeaderstart  ================================================  -->
	<xsl:template name="EmptyHeader">
		<xsl:text>{\header \pard\plain \s20\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 { }}</xsl:text>
	</xsl:template>

	<!-- ================================================ EnglishFooter start  ================================================  -->
	<xsl:template name="MtgFooterEn">
		<xsl:text>{\footer 
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone 
\clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx3720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone 
\clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx7530
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone 
\clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx11340
\pard\plain \s21\qr \widctlpar\intbl
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright {\f1\fs20\cell }
\pard \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum
\faauto\adjustright {\f1\fs20 Page 
{\field{\*\fldinst {\cs22  PAGE }}
{\fldrslt {\cs22\lang1024\langfe1024\noproof 1}}}{\f1  of }
{\field{\*\fldinst {\cs22  NUMPAGES }}
{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}\cell }
\pard \s21\qr \widctlpar\intbl
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Initials _____ Date _______________}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone 
\clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx3720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone 
\clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx7530
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone 
\clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth3810 \cellx11340
\row }

\pard\plain \s21\ql \widctlpar
\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\fs20\par }}</xsl:text>
	</xsl:template>

	<!-- ================================================ EnglishPage1 start  ================================================  -->
	<xsl:template name="MtgPageAEn">
		<xsl:text>
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \s4\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel3\adjustright\itap0 
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6030 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx11340
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:call-template name="LenderLogo"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\qr </xsl:text><!--#DG522 right justify-->
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>
\par </xsl:text>


    <!--#DG364 -->
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
  		<xsl:call-template name="ContactAddress"/>
		</xsl:for-each>		

		<!--#DG364 xsl:value-of select="//Deal/BranchProfile/Contact/Addr/addressLine1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//Deal/BranchProfile/Contact/Addr/addressLine2"/>
		<xsl:if test="//Deal/BranchProfile/Contact/Addr/addressLine2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/BranchProfile/Contact/Addr/city"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//Deal/BranchProfile/Contact/Addr/province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="concat(//Deal/BranchProfile/Contact/Addr/postalFSA,' ', //Deal/BranchProfile/Contact/Addr/postalLDU)"/-->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6030 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr
\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:if test="//Deal/BranchProfile/Contact/contactPhoneNumber">
			<xsl:text>Telephone:</xsl:text>
		</xsl:if>
		<xsl:text>\cell </xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select=".//Deal/BranchProfile/Contact/contactPhoneNumber"/>
		</xsl:call-template>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 
\cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:if test="//Deal/BranchProfile/Contact/contactFaxNumber">
			<xsl:text>Fax:</xsl:text>
		</xsl:if>
		<xsl:text>\cell </xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="//Deal/BranchProfile/Contact/contactFaxNumber"/>
		</xsl:call-template>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx5940
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1260 \cellx7200
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8730
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 TO:\cell </xsl:text>
		<xsl:call-template name="PrimaryBorrowerName"/>
		<xsl:text>}\line {\f1\fs20 </xsl:text>
		<xsl:call-template name="PrimaryBorrowerAddress"/>
		<xsl:text>\cell</xsl:text>
		<xsl:text>}
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\b\fs20\ul\f1 Underwriter:\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<!--FXP16901 xsl:call-template name="DealUnderwriter"/-->
		<xsl:value-of select="$lenderName"/>
		<xsl:text> \cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\b\fs20\ul Date:\par }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//General/CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8730
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11340
\row 
}
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\line </xsl:text>
		<xsl:value-of select="//specialRequirementTags/CareOf"/>
		<xsl:text> \cell </xsl:text>
		<xsl:value-of select="//Deal/SourceFirmProfile/sourceFirmName"/>
		<xsl:text>\par </xsl:text>
		<!--FXP16901 xsl:value-of select="concat(//specialRequirementTags/currentUser/Contact/contactFirstName,' ', //specialRequirementTags/currentUser/Contact/contactLastName)"/-->
		<xsl:value-of select="$lenderName"/>
		<xsl:text>\line </xsl:text>
		<xsl:call-template name="CurrentUserAddress"/>
		<!--FXP16901 xsl:if test="//specialRequirementTags/currentUser/Contact/contactEmailAddress">
			<xsl:text> \par </xsl:text>
			<xsl:value-of select="//specialRequirementTags/currentUser/Contact/contactEmailAddress"/>
		</xsl:if-->
		<xsl:text>\cell }
{\b\f1\ul\fs20 Lender Reference Number:\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmgf\clbrdrt
\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trrh230\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 \cell \cell }
{\b\f1\ul\fs20 <!-- Lender Reference Number:-->\cell }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<!-- <xsl:value-of select="//Deal/dealId"/>-->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh230\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5400 \cellx5939
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmgf\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
<!--FXP16901 \trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell Telephone:\cell </xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>
		</xsl:call-template>
		<xsl:if test="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension and string-length(//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension)!=0">
			<xsl:text> Ext. </xsl:text>
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension"/>
		</xsl:if>
		<xsl:text>\cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\fs20 \cell }
\pard\plain \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell Fax:\cell </xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="//Deal/SourceOfBusinessProfile/Contact/contactFaxNumber"/>
		</xsl:call-template>
		<xsl:text>\cell \cell }
\pard\plain \s3\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel2\adjustright 
{\f1\fs20 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-91\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth901 \cellx810
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1261 \cellx1800
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4139 \cellx5939
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2790 \cellx8729
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2610 \cellx11339
\row 
}-->
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s15\ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\i\f1\fs28 We are pleased to confirm that your application for a </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothecary</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Loan has been approved under the following terms and conditions:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5386 \cellx11340
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\b\fs20\ul </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Borrower</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgagor</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text>(s):\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:call-template name="BorrowersListedBlocks"/>
		<!-- 
<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
<xsl:value-of select="."/>
<xsl:if test="not(position()=last())">
<xsl:text>\par </xsl:text>
</xsl:if>
</xsl:for-each>
-->
		<xsl:text>\cell }{\b\f1\fs20\ul Security Address:\par }</xsl:text>
		<xsl:text>{\f1\fs20 </xsl:text>
		<!--#DG364 xsl:call-template name="SubjectPropertyLines"/-->
     <xsl:call-template name="propAdr"/>	
		
		<xsl:text>\par </xsl:text>
		<!--
<xsl:for-each select="//CommitmentLetter/Properties/Property">
<xsl:text>{\f1\fs20 </xsl:text>
<xsl:value-of select="./AddressLine1"/><xsl:text>\par </xsl:text>
<xsl:value-of select="./AddressLine2"/>
<xsl:if test="./AddressLine2">
<xsl:text>\par </xsl:text>
</xsl:if>
<xsl:value-of select="./City"/><xsl:text>, </xsl:text><xsl:value-of select="./Province"/><xsl:text>  </xsl:text><xsl:value-of select="./PostalCode"/><xsl:text>}</xsl:text>
<xsl:if test="not(position()=last())">
<xsl:text>\par </xsl:text>
</xsl:if>
</xsl:for-each>
-->
		<xsl:text>\cell}<!--#DG142 -->
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5386 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20\ul </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>SURETY</xsl:text>
			</xsl:when>
			<xsl:otherwise>
		    <xsl:value-of select="//specialRequirementTags/GuarantorClause"/>
			</xsl:otherwise>
		</xsl:choose>		
		
		<xsl:text>:\cell }
{\f1\fs20 </xsl:text>
		<xsl:call-template name="GuarantorNames"/>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2250 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3794 \cellx5954
\clvmrg\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth1 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 LOAN TYPE:   }
{\f1\fs20 </xsl:text>
		<!-- #DG564 <xsl:value-of select="/*/Deal/MtgProd/mtgProdName"/> -->
		<xsl:value-of select="/*/Deal/MtgProd/mtgProd"/>
		<xsl:text> \tab\tab }
{\b\f1\fs22 LTV:  }{\f1\fs20 </xsl:text>
		<xsl:call-template name="FormatLTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 COMMITMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/commitmentIssueDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 FIRST PAYMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/firstPaymentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 INTEREST ADJUSTMENT DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/interimInterestAdjustmentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 CLOSING DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 MATURITY DATE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/maturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22 INTEREST ADJUSTMENT AMOUNT:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/interimInterestAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs22\ul LOAN:\cell }
\pard\plain \s5\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel4\adjustright 
{\b\f1\fs22\ul TERMS:\cell }
\pard\plain \s6\ql \sa120\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel5\adjustright 
{\b\f1\fs22\ul INSTALLMENT - </xsl:text>
		<xsl:value-of select="/*/Deal/paymentFrequency"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3240 \cellx3150
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3780 \cellx6930
\clvertalb\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4410 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 

\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl
\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt
\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Amount:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//specialRequirementTags/LoanAmountMinusFees"/>
		<xsl:text> \cell }
{\b\f1\fs20 Interest Rate: \cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//specialRequirementTags/InterestRate"/>
		<xsl:text> \cell }
{\b\f1\fs20 Principal and Interest:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Deal/PandiPaymentAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Insurance:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//specialRequirementTags/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs20 Term:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//specialRequirementTags/PaymentTerm"/>
		<xsl:text> \cell }
{\b\f1\fs20 <!--#DG120 Taxes (estimated):-->\cell }
{\f1\fs20 </xsl:text>
		<!--#DG120 xsl:value-of select="//specialRequirementTags/TaxPortion"/-->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell }
{\f1\fs20 \cell }
{\b\f1\fs20 Amortization:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//specialRequirementTags/AmortizationInYrsMnth"/>
		<xsl:text>\cell }
{\b\f1\fs20 <!--#DG120 Total Installment:-->\cell }
{\f1\fs20 </xsl:text>
		<!--#DG120 xsl:value-of select="//Deal/totalPaymentAmount"/-->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Total Loan:\cell }
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//specialRequirementTags/TotalAmount"/>
		<xsl:text> \cell \cell \cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx3150
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx5040
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1890 \cellx6930
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2340 \cellx9270
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2070 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s18\ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\b Please be advised the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Legal Counsel</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>solicitor</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> acting on behalf of the transaction, will be:\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Solicitor Name:\cell </xsl:text>
		<xsl:call-template name="SolicitorName"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Address:\cell </xsl:text>
		<xsl:call-template name="SolicitorAddress"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\pard\plain \s18\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Phone / Fax:\cell </xsl:text>
		<xsl:call-template name="SolicitorPhoneFaxEn"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\pard\plain \s18\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1620 \cellx1530
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth9810 \cellx11340
\row 
}
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl
\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb
\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard \sa120\sb180 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<!-- To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than 
<xsl:value-of select="//Deal/returnDate"/>
after which time if not accepted, shall be considered null and void. \line \line -->
		<xsl:text> Thank you for choosing 
<!--#DG522 Cervus Financial Corp.--></xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text> for your financing.
\par }
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\ql </xsl:text>
		<!--  removed as per Derek's suggestion 29/06/04
<xsl:for-each select="//specialRequirementTags/ClosingText/Line">
<xsl:value-of select="."/><xsl:text>\line </xsl:text>
</xsl:for-each>
-->
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s20\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
</xsl:text>
	</xsl:template>
	<!-- ================================================ EnglishPage1 end  ================================================  -->

	<!-- ================================================ EnglishPage2Start start ================================================  -->
	<xsl:template name="MtgPageBStartEn">
		<xsl:text>{\sect } \linex0\headery706\footery60\endnhere\sectdefaultcl</xsl:text>
	</xsl:template>
	<!-- ================================================ EnglishPage2Start end  ================================================  -->

	<!-- ================================================ EnglishPage2 start ================================================  -->
	<xsl:template name="MtgPageBEn">
		<xsl:text>\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \ql \sb120\sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul TERMS AND CONDITIONS:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 THIS </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text> HYPOTHECARY </xsl:text>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text>COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING:\par }
{\b\f1\fs18 The following conditions must be met, and the requested documents 
must be received in form and content satisfactory to </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text> 
no later than ten (10) business days prior to the advance of the mortgage. Failure to do so will delay current closing and/or void this commitment.\par }{\f1\fs18 </xsl:text>
		<xsl:for-each select="//specialRequirementTags/Conditions/Condition">
			<xsl:value-of select="position()"/>
			<xsl:text>.  </xsl:text>
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text> \cell } \row<!--#DG142 -->
</xsl:text>
	</xsl:template>
	<!-- ================================================ EnglishPage2 end  ================================================  -->

	<!-- ================================================ EnglishPage3Start start ================================================  -->
	<xsl:template name="MtgPageCStartEn">
		<xsl:text>{\sect }\sectd \linex0\headery706\footery60\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
	</xsl:template>
	<!-- ================================================ EnglishPage3Start end ================================================  -->

	<!-- ================================================ EnglishPage3 start ================================================  -->
	<xsl:template name="MtgPageCEn">
		<xsl:text>\trowd\trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth11440\cellx11350
\pard\plain\s17\qj\sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
<!--#DG142 {\b\caps\f1\fs18\ul \par}--></xsl:text>
		<!-- #554 
<xsl:text>The interest rate indicated on page 1 of this Commitment Letter will be guaranteed to apply to the mortgage, provided that the mortgage funds on or before the scheduled closing date which in any event must be no more than 90 days from the date of the commitment. If the scheduled closing date is more than 90 days from the date of the commitment, the interest rate will be fixed on the 90th day prior to the scheduled closing date at the rate that Cervus Financial Corp. then posting for this product. \par
If your mortgage does not close by the scheduled closing date, your rate guarantee will expire, and at Cervus Financial Corp.'s option the interest rate applicable to the mortgage may be reset in accordance with currently prevailing rates and with reference to a revised scheduled closing date which in any event cannot be more than 90 days from the date on which the rate is reset.\par 
Once the interest rate is established and provided the mortgage funds on or before the scheduled closing date, the interest rate applicable to the mortgage will not be changed.\par
The interest rate is guaranteed only for the product and term shown. \par 
Cervus Financial Corp. will obtain a credit report at the time of instructing the solicitor. Cervus Financial Corp. reserves the right to cancel or change terms, if credit rating or financial status does not meet Cervus Financial Corp.'s lending standards.  
</xsl:text>
-->
		<!--
<xsl:for-each select="//specialRequirementTags/RateGuarantee/Line">
	<xsl:value-of select="."/>
		<xsl:if test="not(position()=last())"><xsl:text>//specialRequirementTags/RateGuarantee/Line \par </xsl:text>
	</xsl:if>
</xsl:for-each>
-->
		<!-- #554 <xsl:text>

{\f1\fs18\b\ul\caps Payment Policy:\par }
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>

<xsl:text>Provided the mortgage is not in default, the mortgagor(s) may prepay up to 20% of the original principal amount of the mortgage without notice, bonus or penalty in each calendar year. Each prepayment shall be made on a regular payment date and shall be not less than $100.00. In addition, provided the mortgage is not in default, the mortgagor(s) have the privilege of increasing their regular payment amount up to a maximum of 20% of the current principal and interest payment once per calendar year. \par 
These privileges are non-cumulative from year to year. \par 
Interest is calculated semi-annually not in advance.  
</xsl:text>
-->
		<!--
<xsl:for-each select="//specialRequirementTags/PrivilegePayment/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())"><xsl:text> \par </xsl:text>
	</xsl:if>
</xsl:for-each>

<xsl:for-each select="//specialRequirementTags/InterestCalc/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())"><xsl:text>\par </xsl:text>
	</xsl:if>
</xsl:for-each>
-->
		<xsl:text>\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18\par Closing:\par }<!--#DG142 -->
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:text>This approval is subject to cancellation at the option of 
<!--#DG522 Cervus Financial Corp.--></xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text> should 
any of the following occur: \par 
If there has been any material change to the applicant's financial status as disclosed in the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>loan</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> application or if there have been any unapproved material changes to the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>mortgagee</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> premises that adversely affect the value thereof. \par 
Any misrepresentation of facts contained in this </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>loan</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> application or other documentation entitles 
<!--#DG522 Cervus Corporation--></xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text> to decline to 
advance a portion or all of the loan proceeds, or to demand immediate repayment of all monies 
secured by the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text>. \par 
<!--#DG522 Cervus Corporation--></xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text> determines in good faith (which 
determination shall be final, conclusive and binding upon the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Borrower(s)</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgagor</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text>) and notifies the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Borrower</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgagor</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> at least seven days prior to the Closing 
Date that, by reason of circumstance affecting the Canadian money markets, there is no market for 
extendible commercial paper. \par 
I/We accept the Offer to provide </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> financing and agree to be bound by the terms, conditions and 
provisions contained herein. I/We hereby authorize 
<!--#DG522 Cervus Corporation--></xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text>, it's successor and/or assigns, to make 
whatever credit/payoff inquiries it deems necessary in 
connection with my/our credit application or in the 
course of review or collection of any credit extended in reliance on the application. I/We hereby 
authorize any person or consumer reporting agency to compile and furnish any information it may 
have or obtain in response to such credit inquiries. I/We authorize this document to be 
reproduced by a copy machine to facilitate multiple credit and payoff inquiries. The Mortgagor(s) 
and Guarantor (if applicable) acknowledge receipt of a signed copy of the </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothecary</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage Loan</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> 
Commitment. \par 
<!--#DG522 Cervus Corporation--></xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text> is a wholly owned subsidiary of 
<!--#DG522 Cervus Financial Group Inc.-->the Macquarie group of companies. 
</xsl:text>
		<!--
<xsl:for-each select="//specialRequirementTags/ClosingText/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())"><xsl:text> \par </xsl:text></xsl:if>
</xsl:for-each>
-->
		<xsl:text>\par }
\pard \s17\qj \sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18 <!-- ACCEPTANCE:-->\par }
\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:text>
			<!-- This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text><xsl:value-of select="//Deal/commitmentExpirationDate"/>  <xsl:text> after which time, if not accepted, shall be considered null and void.-->
		</xsl:text>
		<!--
<xsl:for-each select="//specialRequirementTags/Acceptance/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())">
		<xsl:text>\par </xsl:text>
	</xsl:if>
</xsl:for-each>
-->
		<xsl:text>\par \par }
\pard \s17\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>\par \par }
\pard\plain \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 Authorized by: __________</xsl:text>
		<xsl:call-template name="DealUnderwriter"/>
		<xsl:text> _____________\par }
{\f1 \cell }
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text>
		<!--
<xsl:for-each select="//specialRequirementTags/Signature/Line">
	<xsl:value-of select="."/>
	<xsl:if test="not(position()=last())">
		<xsl:text> \par </xsl:text>
	</xsl:if>
</xsl:for-each>
-->
		<xsl:text>\par }
{\f1 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 APPLICANT\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 CO-APPLICANT\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 WITNESS\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\caps </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>SURETY</xsl:text>
			</xsl:when>
			<xsl:otherwise>
		    <xsl:value-of select="//specialRequirementTags/GuarantorClause"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}<!-- \pard \ql \widctlpar\tx9644\aspalpha\aspnum\faauto\adjustright\itap0 -->
<!--\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 --></xsl:text>
	</xsl:template>
	<!-- ================================================ EnglishPage3 end  ================================================  -->


	<!-- ************************************************************************ 	-->
	<!-- FRENCH template section                                                 	   	-->
	<!-- ************************************************************************ 	-->

	<xsl:template name="SolInstructionsFr">
		<xsl:text>\paperh20160 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 </xsl:text>
		<xsl:call-template name="LenderLogo"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>\par </xsl:text>
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
  		<xsl:call-template name="ContactAddress"/>
		</xsl:for-each>		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20\par 
</xsl:text>
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 INSTRUCTIONS AU NOTAIRE MODIFI�ES EN DATE DU </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->
		<xsl:value-of select="//General/CurrentDate"/>
		<xsl:text>\par\par\par}

\pard \ql 
{\f1 \fs20 
</xsl:text>
		<xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50]/Contact">
  		  <xsl:call-template name="ContactName"/>
        <xsl:text>\par\par </xsl:text> 
				<xsl:value-of select="../partyCompanyName"/>
				<xsl:text>\par </xsl:text>
				
    		<xsl:for-each select="Addr">
      		<xsl:call-template name="ContactAddress"/>
    		</xsl:for-each>		
				<xsl:text>\par </xsl:text>
				
				<xsl:if test="contactPhoneNumber">
					<xsl:text>T�l�phone: </xsl:text>
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="contactPhoneNumber"/>
					</xsl:call-template>
					<xsl:if test="contactPhoneNumberExtension!=''">
						<xsl:text> Poste: </xsl:text>
						<xsl:value-of select="contactPhoneNumberExtension"/>
					</xsl:if>
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:if test="contactFaxNumber">
					<xsl:text>Fax: </xsl:text>
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="contactFaxNumber"/>
					</xsl:call-template>
					<xsl:text>\par </xsl:text>
				</xsl:if>
				<xsl:text>{\pard \f1\fs20\sa180\sb180 Cher </xsl:text>

  		  <xsl:call-template name="ContactName"/>
				
			<xsl:text>, \par}</xsl:text>
		</xsl:for-each>
    <xsl:text>}</xsl:text>
  	  <xsl:call-template name="BorowTbRowA"/>
  		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 OBJET:\cell 
EMPRUNTEUR(S):\cell \caps </xsl:text>
		
    <!--#DG364 xsl:value-of select="//specialRequirementTags/borrowerAndGuarantorNames/borrowerNames/names"/-->
		<xsl:call-template name="BorrowersListedBlocks"/>
		
		<xsl:text>\cell }
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRowA"/>
  		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell </xsl:text>
		<xsl:value-of select="//specialRequirementTags/GuarantorClause"/>
		<xsl:text>, si applicable:\cell 
</xsl:text>
		<!--#DG364 xsl:for-each select="//specialRequirementTags/borrowerAndGuarantorNames/guarantorNames/names">
			<xsl:value-of select="./Name"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->
		<xsl:call-template name="GuarantorNames"/>
		<xsl:text>\cell}
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRowA"/>
  		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\b\f1\fs20 \cell ADRESSE DE LA PROPRI�T�:\cell 
</xsl:text>
     <xsl:call-template name="propAdr">	
		    <xsl:with-param name="separaPostal" select="' '"/>
	   </xsl:call-template>
     
     
		<xsl:text>\cell \fs20
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRowA"/>
  		<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell DATE DE CONCLUSION:\cell </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRowA"/>
  		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell MONTANT DE PR�T AUTORIS�:\cell </xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="BorowTbRowA"/>
  		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 \cell NUM�RO DE R�F�RENCE DU PR�TEUR:\cell </xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  		<xsl:text>\pard\b0\ql \brdrb\brdrs\brdrw30\brsp20 
{\f1\fs10 \par }

\pard\f1\fs20 Ma�tre,\par

\pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par Un pr�t hypoth�caire a �t� autoris� pour l'emprunteur ou les emprunteurs 
susmentionn�s conform�ment aux modalit�s d�crites dans l'approbation de pr�t hypoth�caire 
ci-jointe. Vos services � titre de notaire instrumentant sont requis pour repr�senter la 
<!--#DG522 Corporation Financi�re Cervus (ci-apr�s appel�e " Cervus ")--> 
Financi�re Macquarie Lt�e (ci-apr�s appel�e " Financi�re Macquarie ") 
pour pr�parer, passer et inscrire 
l'hypoth�que grevant la propri�t� et que vous vous assuriez que les int�r�ts de 
<!--#DG522 Cervus-->Financi�re Macquarie en tant 
que cr�ancier hypoth�caire sont valides et garantis de fa�on appropri�e.\par \par }

Vous serez responsable de toute modification requise en raison d'erreurs, d'omissions ou 
d'inobservation de votre part et devrez apporter les corrections n�cessaires.\par\par

{\f1\fs20 Veuillez vous referez � nos Directives G�n�rales d'Enregistrement pour notaire que 
vous trouverez sur notre site Web � 
{\field{\*\fldinst{HYPERLINK "http://www.<!--#DG522 cervus-->macquariefinancial.com" }}
{\fldrslt{\cf2\ul www.<!--#DG522 cervus-->macquariefinancial.com}}}
 dans la section notaire et aux instructions ci-dessous en vue de compl�ter l'enregistrement de 
cette transaction.\par\par}

\f1\fs20 Veuillez vous assurer que toutes les conditions d�crites dans l'Approbation 
Hypoth�caire, les instructions au notaire et dans Directives G�n�rales d'Enregistrement pour 
notaire qui sont la responsabilit� du notaire ont �t� enti�rement observ�es et d�ment remplies 
avant la cl�ture et incluses dans votre rapport final.\par\par

</xsl:text>
	</xsl:template>

	<xsl:template name="SolGuideFr">
		<xsl:call-template name="SolGuidHeadFr"/>
		<xsl:call-template name="AdvancesFr"/>
		<xsl:call-template name="FundDetailsFr"/>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="SolGuidHeadFr">
		<xsl:text>\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto
\adjustright\rin360\lin0\itap0 {\par }</xsl:text><!--#DG142 -->
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 INSTRUCTIONS AU NOTAIRE MODIFI�ES EN DATE DU </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->

		<xsl:text>{\pard\sa360\ql\f1\fs28\b </xsl:text>
		<xsl:call-template name="LenderLogo"/>
		<xsl:text>
            \ul DEMANDE DE FONDS\par}
</xsl:text>
		<xsl:call-template name="FaxToFr"/>
		<xsl:text>\pard\par\f1\fs20\b Veuillez soumettre cette Demande de Fonds au moins trois (3) 
jours ouvrables avant la date de cl�ture. Si nous ne recevons pas la Demande de Fonds trois (3) 
jours ouvrables avant la cl�ture, le financement pourrait �tre retard�.\par\par

\b0 </xsl:text>
		
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="MortgageDetailsFr">
		<xsl:text>
{\pard\f1\fs20\sa180\ql \b\ul D�TAIL DE L'HYPOTH�QUE:}
\f1\fs20 \par \par 

{\f1\fs20\ql Toutes les annexes � l'hypoth�que applicables ainsi que l'acte de pr�t et 
d'hypoth�que peuvent �tre obtenus sur notre site Web � 
{\field{\*\fldinst{HYPERLINK "http://www.<!--#DG522 cervus-->macquariefinancial.com" }}
{\fldrslt{\cf2\ul http://www.<!--#DG522 cervus-->macquariefinancial.com}}}
 section notaire.\par \par }

{\pard \f1\fs20\sa180
L'hypoth�que doit �tre inscrite MENSUELLEMENT de la fa�on suivante :\par}
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>\pard \f1\fs20 \ql \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
Montant total de l'hypoth�que:\cell </xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
Type d'hypoth�que:\cell 
</xsl:text>
		<xsl:value-of select="//Deal/dealPurpose"/>
		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
Rang:\cell 
</xsl:text>
		<xsl:text></xsl:text>
		<xsl:value-of select="//Deal/lienPosition"/>
		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
Terme du pr�t hypoth�caire:\cell 
</xsl:text>
		<!-- <xsl:value-of select="//specialRequirementTags/PaymentTerm"/> -->
		<xsl:value-of select="//Deal/MtgProd/paymentTerm"/>
		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
	  <xsl:call-template name="MtgDetailTbRow"/>
		<xsl:text>\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright </xsl:text>
		<xsl:text>Taux d'int�r�t � inscrire:\cell </xsl:text>
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3' or //Deal/MtgProd/interestTypeId='2'">
				<!-- Variable rate mortgage (VRM) or Capped Variable -->
				<!-- row1 -->
				<xsl:text>Taux pr�f�rentiel \emdash  <!--#DG322 0.75-->0.90% </xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<!-- row2 -->
				<xsl:value-of select="//Deal/netInterestRate"/>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
Amortissement:\cell 
</xsl:text>
		<xsl:value-of select="/*/specialRequirementTags/AmortizationInYrsMnth"/>
		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
Date d'avance du pr�t hypoth�caire:\cell 
</xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
Date d'ajustement de l'int�r�t:\cell 
</xsl:text>
		<xsl:value-of select="//Deal/interimInterestAdjustmentDate"/>
		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
Date d'�ch�ance:\cell 
</xsl:text>
		<xsl:value-of select="//Deal/maturityDate"/>
		<xsl:text>\cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
Versement mensuel capital &amp; int�r�t:\cell 
</xsl:text>
		<xsl:value-of select="//Deal/PandIPaymentAmountMonthly"/>
		<xsl:text> \cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
  	  <xsl:call-template name="MtgDetailTbRow"/>
  		<xsl:text>\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
Calcul des int�r�ts:\cell
</xsl:text>
		<!--xsl:value-of select="/*/Deal/paymentFrequency"/-->		
		<xsl:variable name="mtgProdIdt" select="/*/Deal/mtgProdId"/>
		<xsl:choose>
			<!-- #DG564 <xsl:when test="$mtgProdIdt=5 or $mtgProdIdt=6"> -->
			<xsl:when test="$mtgProdIdt=5 or $mtgProdIdt=12">
				<xsl:text>Mensuel</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Semi-annuellement pas en avance</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> \cell 
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
		
		<xsl:text>\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 
Nota : Pour l'enregistrement du pr�t, la date du premier paiement sera un mois apr�s la date 
d'ajustement d'int�r�t. Si la date de conclusion change d'un mois � un autre, toutes les dates 
indiqu�es ci-dessus doivent �tres modifi�es en cons�quence.\par}
</xsl:text>

    <!--#DG352 -->
		<xsl:text>{\b\f1\fs20 </xsl:text>
		<xsl:choose>
			<xsl:when test="$mtgProdIdt=5 or $mtgProdIdt=12"><!--#DG374 added -->
				<xsl:text>{\par\ul PR�T � TAUX AJUSTABLE AVEC PAIEMENTS FIXES:}\par
Le taux est ajust� le premier de chaque mois si le taux pr�f�rentiel 
<!--#DG522 Cervus-->Financi�re Macquarie change. Les paiements <!-- #DG564 -->
sont fixes, bas�s sur le taux fixe de 3 ans  en vigueur 3 jours avant la date de cl�ture et 
resteront fixes pour la dur�e du terme.\par
</xsl:text>
			</xsl:when>
			<xsl:when test="$mtgProdIdt=6">
				<xsl:text>{\par\ul PR�T HYPOTH�CAIRE � TAUX ADJUSTABLE AVEC PAIEMENTS VARIABLES:}\par
Les paiements sont bas�s sur le taux ajustable et sont revis�s 
le premier de chaque de mois si le taux pr�f�rential 
<!--#DG522 Cervus-->Financi�re Macquarie change. L'emprunteurs a choisi d'avoir les paiements 
hypoth�caires bas�s sur le taux ajustable et devra signer 
notre formulaire de Reconnaissance et Consentement disponible sur notre site web au 
{\field{\*\fldinst{HYPERLINK "http://www.<!--#DG522 cervus-->macquariefinancial.com" }}
{\fldrslt{\cf2\ul www.<!--#DG522 cervus-->macquariefinancial.com}}}.\par
</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>}</xsl:text>
    <!--#DG352 end -->

	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="AdvancesFr">
		<!-- ===================================  #642 =================================== -->
		<xsl:text>
{\pard\f1\fs20\sa240\ul\b 
AVANCE DU PR�T HYPOTH�CAIRE\par}
{\pard\f1\fs20
Lorsque toutes les conditions pr�alables � la pr�sente transaction seront 
satisfaites, la somme de \b </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/netAdvance"/>
		<xsl:text>\b0  sera avanc�e.\par }
\pard \qj \fi720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 
{\f1\fs20 \par }
</xsl:text>

  	<xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\ql\fs20\b MONTANT EN CAPITAL\b0\fs24\cell 
\pard\intbl\qr\fs20 </xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>\fs24\cell 
\row 
</xsl:text>

  	<xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\ql\sb120\fs20\sb120 \tab Ajustement de l'int�r�t (montant total)\cell 
\qr </xsl:text>
		<xsl:value-of select="/*/Deal/interimInterestAmount"/>
		<xsl:text>\cell 
\row 
</xsl:text>

		<xsl:for-each select="//specialRequirementTags/deductibleFees/DealFee">
  	  <xsl:call-template name="MtgAdvanceTbRowA"/>
			<xsl:text>\sb120\ql \tab </xsl:text>
      <!--#DG364 xsl:value-of select="./Fee/feeDescription"/--> 
      <xsl:value-of select="./Fee/feeType"/> 
			<xsl:text>\cell \qr </xsl:text>
			<xsl:value-of select="feeAmount"/>
			<xsl:text>\cell 
\row 
</xsl:text>
		</xsl:for-each>
		
	  <xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\pard\intbl\sb120\sa180\ql \tab D�ductions totales\cell 
\pard\intbl\sb120\sa180\qr </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/totalDeductionsAmount"/>
		<xsl:text>\cell 
\row 
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\ql\b AVANCE NETTE\fs24\cell 
\pard\intbl\fs20\qr </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/netAdvance"/>
		<xsl:text>\b0\fs24\cell 
\row 

\fs20 </xsl:text>
  	  <xsl:call-template name="MtgAdvanceTbRowA"/>
			<xsl:text>\qr\cf0\fs20\cell\cell
\row 
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRowA"/>
		<xsl:text>\pard\intbl\ql Ajustement de l'int�r�t (quotidien)\cell
\pard\intbl\qr </xsl:text>
		<xsl:value-of select="//Deal/perdiemInterestAmount"/>
		<xsl:text>\cell 
\row 
</xsl:text>
	</xsl:template>
	<!-- ================================================  -->

	<!-- ================================================  -->
	<xsl:template name="FundDetailsFr">	
		<xsl:text>\pard\sa240\ul\b\fs20\par D�TAIL DU FINANCEMENT\b0\ulnone\par 
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Emprunteur (s):\par\cell
\pard\intbl\caps </xsl:text>
    <!--#DG364 xsl:value-of select="//specialRequirementTags/borrowerAndGuarantorNames/borrowerNames/names"/-->
		<xsl:call-template name="BorrowersListedBlocks"/>
		<xsl:text>\line\caps0\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Adresse de la propri�t�:\par\cell
\pard\intbl\caps </xsl:text>
     <xsl:call-template name="propAdr"/>	
		<xsl:text>\fs20\par\caps0\cell
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Num�ro de r�f�rence pr�teur:\par\cell
\pard\intbl </xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>\cell
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Avance Nette:\par\cell
\pard\intbl </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/netAdvance"/>
		<xsl:text>\cell
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Date � laquelle les fonds sont requis:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Nom du service d_assurance titre:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Num�ro de l_assurance titre:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Assurance incendie / Certificat de la Soci�t� d_assurance:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
	  <xsl:call-template name="MtgAdvanceTbRow"/>
		<xsl:text>Num�ro de l_assurance incendie:\par\cell 
</xsl:text>
		<!--#DG522 xsl:value-of select="//Deal/estimatedClosingDate"/-->
    <xsl:text>____________________________</xsl:text>
		<xsl:text>\cell 
\row
</xsl:text>
    <xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50]">

  	  <xsl:call-template name="MtgAdvanceTbRow"/>
  		<xsl:text>Nom du Notaire:\par\cell
\pard\intbl </xsl:text>
      <xsl:for-each select="Contact">
  		  <xsl:call-template name="ContactName"/>
  		</xsl:for-each>
  		<xsl:text>\par\cell
\row
</xsl:text>
  	  <xsl:call-template name="MtgAdvanceTbRow"/>
  		<xsl:text>Banque:\par\cell
\pard\intbl </xsl:text>
			<xsl:value-of select="./bankName"/>
			<xsl:text>\par\cell
\row
</xsl:text>
  	  <xsl:call-template name="MtgAdvanceTbRow"/>
  		<xsl:text>Succursale:\tab\cell
\pard\intbl </xsl:text>
			<xsl:call-template name="BankTransitWoInstitution"/>
			<xsl:text>\line\cell
\row
</xsl:text>
  	  <xsl:call-template name="MtgAdvanceTbRow"/>
  		<xsl:text>\qj N� de compte: \tab\cell
\pard\intbl </xsl:text>
			<xsl:call-template name="AccountNumWStars"/>
			<xsl:text>\line\cell
\row
</xsl:text>
    </xsl:for-each>
    
		<xsl:text>\pard\par\b Veuillez prendre note que les premier et dernier chiffres de votre num�ro 
de compte et de succursale ont �t� supprim�s aux fins de s�curit�. Si ces donn�es sont inexactes 
ou ont chang�, veuillez communiquer avec nous imm�diatement.\b0\par\par 

Les fonds hypoth�caires seront �lectroniquement d�pos�s dans votre compte en fid�icommis � la 
date demand�e, � condition que toutes les conditions \lquote source d'affaire\rquote  apparaissant dans 
l'approbation hypoth�caire aient �t� re�ues et approuv�es par 
<!--#DG522 Cervus-->Financi�re Macquarie. R�f�rez-vous au document 
Directives G�n�rales d'Enregistrement pour Notaire pour des exigences de financement 
compl�mentaires.\line\par

\pard\qj\cf0\par
\par
________________________________________ \tab\tab\tab ______________________________________\line 
Signature du notaire\tab\tab\tab\tab\tab\tab\tab Date\b0\par
</xsl:text>
	</xsl:template>

	<xsl:template name="MtgCmtFr">
		<xsl:call-template name="MtgHeaderFr"/>
		<!-- <xsl:call-template name="MtgFooterEn"/>-->
		<xsl:call-template name="MtgPageAFr"/>
		<xsl:call-template name="MtgPageBStartEn"/>
		<xsl:call-template name="MtgPageBFr"/>
		<!--#DG142 xsl:call-template name="MtgPageCStartEn"/-->
		<xsl:call-template name="MtgPageCFr"/>
		<!--xsl:call-template name="RTFFileEnd4"/-->
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="MtgHeaderFr">
		<xsl:text>\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text><!--#DG142 -->
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 INSTRUCTIONS AU NOTAIRE MODIFI�ES EN DATE DU </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->
		<!-- #DG116 changed header to title else it would apply to the whole doc -->
		<xsl:text>{\pard\plain \s20\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0} 
\pard\b\i\f1\fs28 APPROBATION HYPOTH\'c9CAIRE\b0\i0\f1\fs24\par
</xsl:text>
	</xsl:template>

	<!-- ================================================ EnglishPage1 start  ================================================  -->
	<xsl:template name="MtgPageAFr">
		<xsl:text>

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmgf\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6300
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\lang1033\f1\fs20 </xsl:text>
		<xsl:call-template name="LenderLogo"/>
		<xsl:text>\cell
\pard\intbl\qr </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>
\par </xsl:text>

		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
  		<xsl:call-template name="ContactAddress"/>
		</xsl:for-each>		

		<xsl:text>\cell
\row

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmrg\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6300
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs \cellx8640
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx11520
\pard\intbl\cell 
</xsl:text>
		<xsl:if test="//Deal/BranchProfile/Contact/contactPhoneNumber">
			<xsl:text>T�l�phone:</xsl:text>
		</xsl:if>
		<xsl:text>\cell </xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="/*/Deal/BranchProfile/Contact/contactPhoneNumber"/>
		</xsl:call-template>
		<xsl:text>\cell
\row

\trowd\trgaph108\trleft-91\trrh70\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmrg\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs
\clbrdrb\brdrw10\brdrs \cellx6300
\clbrdrl\brdrw10\brdrs \cellx8640
\clbrdrr\brdrw10\brdrs \cellx11520
\pard\intbl\cell 
</xsl:text>
		<xsl:if test="//Deal/BranchProfile/Contact/contactFaxNumber">
			<xsl:text>T�l�copieur:</xsl:text>
		</xsl:if>
		<xsl:text>\cell </xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="/*/Deal/BranchProfile/Contact/contactFaxNumber"/>
		</xsl:call-template>
		<xsl:text>\cell
\row

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx792
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6300
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8640
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl A :\cell
\lang1036 </xsl:text>
		<xsl:call-template name="PrimaryBorrowerName"/>
		<xsl:text>\line </xsl:text>
		<xsl:call-template name="PrimaryBorrowerAddress"/>
		<xsl:text>\cell
\pard\intbl\keepn\lang1033\ul\b Sourscripteur\par
\pard\intbl\ulnone\b0 </xsl:text>
		<!--FXP16901 xsl:call-template name="DealUnderwriter"/-->
		<xsl:value-of select="$lenderName"/>
		<xsl:text>\cell
\pard\intbl\keepn\ul\b Date\par
\pard\intbl\ulnone\b0 </xsl:text>
		<xsl:value-of select="//General/CurrentDate"/>
		<xsl:text>\lang1033\cell
\row

</xsl:text>
		<xsl:text>\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmgf\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx792
\clvmgf\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6300
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs \cellx8640
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx11520
\pard\intbl\line </xsl:text>
		<xsl:value-of select="//specialRequirementTags/CareOf"/>
		<xsl:text>\cell
\pard\intbl\ri-1008 </xsl:text>
		<xsl:value-of select="//Deal/SourceFirmProfile/sourceFirmName"/>
		<xsl:text>\par
\pard\intbl </xsl:text>
		<!--xsl:value-of select="concat(//specialRequirementTags/currentUser/Contact/contactFirstName,' ', //specialRequirementTags/currentUser/Contact/contactLastName)"/-->
		<xsl:value-of select="$lenderName"/>
		<xsl:text>\line </xsl:text>
		<xsl:call-template name="CurrentUserAddress"/>
		<!--FXP16901 xsl:if test="//specialRequirementTags/currentUser/Contact/contactEmailAddress">
			<xsl:text> \par </xsl:text>
			<xsl:value-of select="//specialRequirementTags/currentUser/Contact/contactEmailAddress"/>
		</xsl:if-->
		<xsl:text>\cell
\ul\b Num\'e9ro de r\'e9f\'e9rence Pr\'eateur :\cell
\pard\intbl\sa120\ulnone\b0 </xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>\cell
\row

\trowd\trgaph108\trleft-91\trrh276\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmrg\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx792
\clvmrg\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx6300
\clvmgf\clbrdrl\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8640
\clvmgf\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\cell\cell\cell\cell
\row

<!--FXP16901 \trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmrg\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx792
\cellx2700
\clbrdrr\brdrw10\brdrs \cellx6300
\clvmrg\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8640
\clvmrg\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\cell 
Telephone:\cell 
</xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>
		</xsl:call-template>
		<xsl:if test="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension!=0">
			<xsl:text> poste </xsl:text>
			<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension"/>
		</xsl:if>
		<xsl:text>\cell\pard\intbl\keepn\cell\pard\intbl\sa120\cell
\row

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmrg\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx792
\clbrdrb\brdrw10\brdrs \cellx2700
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6300
\clvmrg\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8640
\clvmrg\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\cell 
Fax:\cell 
</xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="/*/Deal/SourceOfBusinessProfile/Contact/contactFaxNumber"/>
		</xsl:call-template>
		<xsl:text>\cell\cell\cell
\row-->

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\sa120\lang1036\b\i\fs28 Nous avons le plaisir de vous informer que votre demande de 
pr\'eat hypoth\'e9caire a \'e9t\'e9 approuv\'e9e selon les modalit\'e9s 
et conditions suivantes:\cell
\row

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx2700
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx6300
\clvmgf\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\lang1033\ul\i0\fs20 DEMANDEUR(S):\cell
\lang1036\ulnone\b0 </xsl:text>
		<xsl:call-template name="BorrowersListedBlocks"/>
		<xsl:text>\cell \b\ul PROPRI�T�:\ulnone\b0\par </xsl:text>
     <xsl:call-template name="propAdr"/>			
		<xsl:text>\par\cell
\row

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx2700
\clbrdrl\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx6300
\clvmrg\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\ul\b </xsl:text>
		<xsl:value-of select="//specialRequirementTags/GuarantorClause"/>
		<xsl:text>:\cell\ulnone\b0 </xsl:text>
		<xsl:call-template name="GuarantorNames"/>
		<xsl:text>\cell\cell
\row


\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\b\fs22\caps 
PRODUIT:   \b0\fs20\tab </xsl:text>
		<!-- #DG564 <xsl:value-of select="/*/Deal/MtgProd/mtgProdName"/> -->
		<xsl:value-of select="/*/Deal/MtgProd/mtgProd"/>
		<xsl:text>\tab\b\fs22 RPV:    \b0\fs20 </xsl:text>
		<xsl:call-template name="FormatLTV"/>
		<xsl:text>\cell
\row

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowA"/>
  		<xsl:text>\b DATE DE L'APPROBATION:\par
\pard\intbl\sa120\b0 </xsl:text>
		<xsl:value-of select="//Deal/commitmentIssueDate"/>
		<xsl:text>\cell
\pard\intbl\b DATE DU PREMIER PAIEMENT:\par
\pard\intbl\sa120\b0 </xsl:text>
		<xsl:value-of select="//Deal/firstPaymentDate"/>
		<xsl:text>\cell
\pard\intbl\b DATE DE L'AJUSTEMENT D'INT�R�T:\par
\pard\intbl\ri279\sa120\b0 </xsl:text>
		<xsl:value-of select="//Deal/interimInterestAdjustmentDate"/>
		<xsl:text>\cell
\row

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowA"/>
  		<xsl:text>\b DATE de CL\'d4TURE:\par
\pard\intbl\sa120\b0 </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>\cell
\pard\intbl\lang1033\b DATE D'�CH�ANCE:\par
\pard\intbl\sa120\b0 </xsl:text>
		<xsl:value-of select="//Deal/maturityDate"/>
		<xsl:text>\cell
\pard\intbl\b MONTANT DE L'AJUSTEMENT D'INT�R�T:\par
\pard\intbl\sa120\b0 </xsl:text>
		<xsl:value-of select="//Deal/interimInterestAmount"/>
		<xsl:text>\cell
\row

\trowd\trgaph108\trleft-91\trrh458\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalb\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx3420
\clvertalb\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx7020
\clvertalb\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx11520
\pard\intbl\sa120\ul\b\fs22 PR�T:\cell
\pard\intbl\keepn\sa120\caps0 MODALIT�S:\cell
\lang1036 PAIEMENT \endash  </xsl:text>
		<xsl:value-of select="//Deal/paymentFrequency"/>
		<xsl:text>\ulnone\cell
\row

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowC"/>
  		<xsl:text>\fs20 Montant:\cell 
\b0 </xsl:text>
		<xsl:value-of select="/*/specialRequirementTags/LoanAmountMinusFees"/>
		<xsl:text>\cell 
\b Taux d'int�r�t:\cell 
\b0 </xsl:text>
		<xsl:value-of select="/*/specialRequirementTags/InterestRate"/>
		<xsl:text>\cell 
\b Capital  et Int�r�t:\cell 
\b0 </xsl:text>
		<xsl:value-of select="/*/Deal/PandiPaymentAmount"/>
		<xsl:text>\cell 
\row 

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowC"/>
  		<xsl:text>\b Prime d'assurance:\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/specialRequirementTags/Premium"/>
		<xsl:text>\cell 
\b Terme:\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/specialRequirementTags/PaymentTerm"/>
		<xsl:text> \cell 
<!--#DG120 \b Taxes (estimated):\b0 -->\cell 
</xsl:text>
		<!--#DG120 xsl:value-of select="//specialRequirementTags/TaxPortion"/-->
		<xsl:text>\cell 
\row 

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowC"/>
  		<xsl:text>\b\cell\b0\cell
\b Amortissement:\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/specialRequirementTags/AmortizationInYrsMnth"/>
		<xsl:text>\cell 
<!--#DG120 \b Total Installment:\b0-->\cell 
</xsl:text>
		<!--#DG120 xsl:value-of select="//Deal/totalPaymentAmount"/-->
		<xsl:text>\cell 
\row 

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowC"/>
  		<xsl:text>\clbrdrb\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\b Total du pr�t:\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/specialRequirementTags/TotalAmount"/>
		<xsl:text>\cell\cell\cell\cell \cell 
\row 

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx11520
\pard\intbl\sa120\lang1036\b Le notaire instrumentant pour cette transaction sera:\cell
\row

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowD"/>
  		<xsl:text>\b0 Nom du notaire:\cell 
</xsl:text>
		<xsl:call-template name="SolicitorName"/>
		<xsl:text>\cell
\row

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowD"/>
  		<xsl:text>Adresse:\cell 
</xsl:text>
		<xsl:call-template name="SolicitorAddress"/>
		<xsl:text>\cell 
\row 

</xsl:text>
  	  <xsl:call-template name="MtgCmtTbRowD"/>
  		<xsl:text>T�l�phone:\line T�l�copieur:\cell 
</xsl:text>
		<xsl:call-template name="SolicitorPhoneFaxFr"/>
		<xsl:text>\par\cell 
\row 

\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl\sb180\sa120  Nous vous remercions d\rquote avoir choisi la 
<!--#DG522 Corporation Financi�re Cervus-->Financi�re Macquarie Lt�e 
pour le financement de votre pr\'eat hypoth\'e9caire\par
\pard\intbl\cell
\row

\pard\plain \s20\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0
</xsl:text>
	</xsl:template>
	<!-- ================================================ Fr Page1 end  ================================================  -->

	<!-- ================================================ Fr Page2 start ================================================  -->
	<xsl:template name="MtgPageBFr">
		<xsl:text>\trowd \trgaph108\trleft-90
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11430\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \ql \sb120\sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul TERMES ET CONDITIONS DE L'HYPOTH�QUE:\par }
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 CET ENGAGEMENT HYPOTH�CAIRE EST CONDITIONNEL � LA R�CEPTION DES �L�MENTS SUIVANTS:\par }
{\b\f1\fs18 Les conditions suivantes doivent �tre r�unies et les documents demand�s doivent �tre 
re�us et satisfaisants � </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text> 
 pas plus tard que dix (10) jours ouvrables avant le d�boursement de l'hypoth�que. 
Tout manquement pourrait retarder la cl�ture et/ou annuler cet engagement.\par }
{\f1\fs18 </xsl:text>
		<xsl:for-each select="//specialRequirementTags/Conditions/Condition">
			<xsl:value-of select="position()"/>
			<xsl:text>.  </xsl:text>
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text> \cell }
\row<!--#DG142 -->
</xsl:text>
	</xsl:template>
	<!-- ================================================ FrPage2 end  ================================================  -->

	<!-- ================================================ FrPage3Start start ================================================  -->
	<xsl:template name="MtgPageCStartFr">
		<xsl:text>{\sect }\sectd \linex0\headery706\footery60\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
\cell 
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
	</xsl:template>
	<!-- ================================================ FrPage3Start end ================================================  -->

	<!-- ================================================ FrPage3 start ================================================  -->
	<xsl:template name="MtgPageCFr">
		<xsl:text>\trowd\trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth11440\cellx11350
\pard\plain\s17\qj\sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\b\fs24\ul\caps\f1\fs18\par CLOTURE:\par }
\pard \s17\qj \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone La <!--#DG522 Corporation Financi�re Cervus-->Financi�re Macquarie Lt�e 
se r�serve le droit d'annuler 
cette approbation advenant une des situations suivantes :\par 
S'il y a eu un changement significatif au statut financier du demandeur tel que r�v�l� dans la 
demande de pr�t ou s'il y a eu des changements significatifs non approuv�s aux locaux hypoth�qu�s 
qui en affectent la valeur de fa�on d�favorable.\par 
Toute information ou d�claration fausse ou trompeuse contenue sur cette demande de pr�t ou tout 
autre documents autorise la <!--#DG522 Corporation Financi�re Cervus-->Financi�re Macquarie Lt�e 
� d�cliner le d�boursement d'une partie 
ou de la totalit� du montant du pr�t, ou de demander le remboursement imm�diat des sommes 
garanties par l'hypoth�que.\par 
Toute information ou d�claration fausse ou trompeuse contenue sur cette demande de pr�t ou tout 
autre documents autorise la <!--#DG522 Corporation Financi�re Cervus-->Financi�re Macquarie Lt�e 
� d�cliner le d�boursement d'une partie 
ou de la totalit� du montant du pr�t, ou de demander le remboursement imm�diat des sommes 
garanties par l'hypoth�que.\par
La <!--#DG522 Corporation Financi�re Cervus-->Financi�re Macquarie Lt�e 
d�termine en toute bonne foi (la d�termination sera finale, 
conclusive et obligatoire sur le D�biteur) et notifie le d�biteur au moins sept (7) jours avant 
la date de cl�ture que, en raison des circonstances affectant les march�s de capitaux canadiens, 
il n'y a aucun march� pour l'effet de commerce renouvelable.\par 
\b D�claration\b0\par

J'accepte cette offre de financement hypoth�caire et j'accepte d'�tre li� par les modalit�s, 
conditions et les dispositions contenues ci-dessus. Par la pr�sente, j'autorise la 
<!--#DG522 Corporation Financi�re Cervus-->Financi�re Macquarie Lt�e, ses affili�s et/ou 
mandataires, � faire toutes v�rifications de solvabilit� et 
� obtenir l'information qu'ils jugent n�cessaires en rapport avec ma demande de cr�dit. \par
J'autorise toute personne autoris�e ou toute agence d'�valuation de cr�dit � rassembler et 
fournir toute information qu'elle peut avoir ou obtenir en r�ponse � des telles demandes 
d'informations. \par
J'accepte que ce document soit reproduit (photocopi�) pour faciliter de multiples demandes de 
renseignement.\par\par

Le Demandeur et le Garant (si applicable) accusent r�ception d'une copie sign�e de l'Engagement 
Hypoth�caire.\par\par

\b Disposition interpr�tative\b0\par 
Partout o� le contexte l'exige, le singulier pourra �tre interpr�t� comme le pluriel, le masculin 
comme le f�minin et vice-versa.\par
La <!--#DG522 Corporation Financi�re Cervus-->Financi�re Macquarie Lt�e 
est une filiale en propri�t� exclusive du 
<!--#DG522 Groupe Financier Cervus Inc.-->groupe de companies de Macquarie.\par }
\pard \s17\qj \sb120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\fs24\ul\caps\f1\fs18 \par }
\pard \s17\qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone \par \par }
\pard \s17\qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b0\f1\fs18\ulnone </xsl:text>
		<xsl:value-of select="$lenderName"/><!--#DG522 -->
		<xsl:text>\par \par }
\pard\plain \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 Autoris� par: __________</xsl:text>
		<xsl:call-template name="DealUnderwriter"/>
		<xsl:text> _____________\par }
{\f1 \cell }
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 \trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 </xsl:text>
		<xsl:text>\par }
{\f1 \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11440 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 T�MOIN\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DEMANDEUR\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 T�MOIN\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 CO-DEMANDEUR\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\pard\plain \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 T�MOIN\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\caps </xsl:text>
		<xsl:value-of select="//specialRequirementTags/GuarantorClause"/>
		<xsl:text>\cell }
\pard \s20\ql \widctlpar\intbl\brdrb\brdrs\brdrw30\brsp20 \aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \s20\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 DATE\par \cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth3\trwWidth11440\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3600 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth5670 \cellx9180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2170 \cellx11350
\row 
}<!-- \pard \ql \widctlpar\tx9644\aspalpha\aspnum\faauto\adjustright\itap0 -->
<!--\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 --></xsl:text>
	</xsl:template>
	<!-- ================================================ FrPage3 end  ================================================  -->
		
	<xsl:template name="ConfirmCloseFr">
		<xsl:call-template name="ConfirmCloseHeadFr"/>
		<xsl:call-template name="FaxToFr"/>
		<xsl:call-template name="LenderRefFr"/>
		<xsl:call-template name="lastCentFr"/>
	</xsl:template>

	<xsl:template name="ConfirmCloseHeadFr">
		<xsl:text>\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }</xsl:text>
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 INSTRUCTIONS AU NOTAIRE MODIFI�ES EN DATE DU </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->
		<!--#DG364 -->
		<xsl:text>\pard\f1 
\brdrl\brdrs\brdrw10 \brdrt\brdrs\brdrw10 
\brdrr\brdrs\brdrw10 \brdrb\brdrs\brdrw10 
\sa240\qc\fs20\line
\ul\b\fs28 CONFIRMATION DE LA CONCLUSION\fs20\par
\par
\i *** Veuillez retourner le pr�sent formulaire � notre bureau dans les <!-- #DG564 48-->24 heures suivant la date 
de conclusion pr�vue.***\par\par\par\ulnone\i0
\pard\intbl </xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- Primary borrower's address on separate lines                                    -->
	<!-- ================================================  -->
	<xsl:template name="PrimaryBorrowerAddress">
		<xsl:for-each select="//Deal/Borrower[primaryBorrowerFlag='Y']
/BorrowerAddress[borrowerAddressTypeId=0]">
			<!-- current -->
			
      <!--#DG364 -->
  		<xsl:for-each select="Addr">
    		<xsl:call-template name="ContactAddress"/>
  		</xsl:for-each>		
			
			<!--#DG364 xsl:value-of select="./Addr/addressLine1"/>
			<xsl:text>\line </xsl:text>
			<xsl:if test="./Addr/addressLine2 and string-length(./Addr/addressLine2)>0">
				<xsl:value-of select="./Addr/addressLine2"/>
				<xsl:text>\line </xsl:text>
			</xsl:if>
			<xsl:value-of select="./Addr/city"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Addr/province"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="concat(./Addr/postalFSA,' ', ./Addr/postalLDU)"/-->
			<xsl:if test="position() !=last()">\line </xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="//Deal/Borrower[borrowerTypeId=0]">
			<xsl:call-template name="BorrowerFullName"/>
			<xsl:if test="position() !=last()">\line </xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="GuarantorNames">
		<xsl:for-each select="//Deal/Borrower">
			<xsl:if test="./borrowerTypeId=1">
				<xsl:call-template name="BorrowerFullName"/>
				<xsl:if test="position() !=last()">\line </xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation!=''">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial!=''">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="PrimaryBorrowerName">
		<xsl:for-each select="//Deal/Borrower[primaryBorrowerFlag='Y']">
				<xsl:call-template name="BorrowerFullName"/>
  			<xsl:if test="position() !=last()">\line </xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="DealUnderwriter">
		<xsl:for-each select="//Deal/UserProfile">
			<xsl:if test="./userProfileId=//Deal/underwriterUserId">
				<xsl:value-of select="concat(./Contact/contactFirstName,' ', ./Contact/contactLastName)"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<!-- Source Of businessProfile 											         -->
	<!-- ================================================  -->
	<xsl:template name="BrokerAddress">
		<xsl:for-each select="//Deal/SourceOfBusinessProfile/Contact">
		
      <!--#DG364 -->
  		<xsl:for-each select="Addr">
    		<xsl:call-template name="ContactAddress"/>
  		</xsl:for-each>		
		
			<!--#DG364 xsl:value-of select="./Addr/addressLine1"/>
			<xsl:text>\line </xsl:text>
			<xsl:if test="./Addr/addressLine2">
				<xsl:value-of select="./Addr/addressLine2"/>
				<xsl:text>\line </xsl:text>
			</xsl:if>
			<xsl:value-of select="./Addr/city"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Addr/province"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="concat(./Addr/postalFSA, ' ',./Addr/postalLDU) "/-->
			<xsl:text> </xsl:text>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<!-- Current user                        											         -->
	<!-- ================================================  -->
	<xsl:template name="CurrentUserAddress">
		<xsl:for-each select="//specialRequirementTags/currentUser/Contact">
		
      <!--#DG364 -->
  		<xsl:for-each select="Addr">
    		<xsl:call-template name="ContactAddress"/>
  		</xsl:for-each>		
		
			<!--#DG364 xsl:value-of select="./Addr/addressLine1"/>
			<xsl:text>\line </xsl:text>
			<xsl:if test="./Addr/addressLine2">
				<xsl:value-of select="./Addr/addressLine2"/>
				<xsl:text>\line </xsl:text>
			</xsl:if>
			<xsl:value-of select="./Addr/city"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Addr/province"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="concat(./Addr/postalFSA, ' ',./Addr/postalLDU) "/-->
			<xsl:text> </xsl:text>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<!-- Subject property, English formatting of address, one line -->
	<!--#DG364 xsl:template name="SubjectPropertyLines">
		<xsl:for-each select="//Deal/Property">
			<xsl:value-of select="propertyStreetNumber"/>
			<xsl:text>\caps  </xsl:text>
			<xsl:value-of select="propertyStreetName"/>
			<xsl:text> </xsl:text>
			<xsl:if test="streetType">
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="streetType"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:if test="streetDirection">
				<xsl:text>\caps </xsl:text>
				<xsl:value-of select="streetDirection"/>
			</xsl:if>
			<xsl:if test="unitNumber">
				<xsl:if test="string-length(unitNumber) > 0">
					<xsl:text>\caps , unit </xsl:text>
					<xsl:value-of select="unitNumber"/>
				</xsl:if>
			</xsl:if>
			<xsl:text>\line </xsl:text>
			<xsl:text>\caps </xsl:text>
			<xsl:value-of select="concat(propertyCity,', ',province)"/>
			<xsl:text>\line </xsl:text>
			<xsl:text>\caps </xsl:text>
			<xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
		</xsl:for-each>
	</xsl:template-->

	<!-- ================================================  -->
	<!-- duplicate, deleted xsl:template name="FormatPhone"-->
	<!-- ================================================  -->
	<xsl:template name="FormatLTV">
		<xsl:variable name="temp">
			<xsl:value-of select="substring-before(//Deal/combinedLTV,'%')"/>
		</xsl:variable>
		<!-- // debug
	<xsl:text>temp = </xsl:text><xsl:value-of select="$temp"/>
	<xsl:text>\line </xsl:text>
	-->
		<xsl:if test="contains(//Deal/combinedLTV, '%')">
			<xsl:value-of select="concat(substring($temp, 1, string-length($temp)-1),'%')"/>
		</xsl:if>
	</xsl:template>

	<!--  ============================= SolicitorName ====================================== -->
	<xsl:template name="SolicitorName">
		<xsl:for-each select="/*/Deal/PartyProfile">
			<xsl:if test="./partyTypeId='50'">
				<xsl:text>{</xsl:text>
				<xsl:value-of select="./Contact/contactFirstName"/>
				<xsl:text> </xsl:text>
				<xsl:if test="./Contact/contactMiddleInitial">
					<xsl:value-of select="./Contact/contactMiddleInitial"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:value-of select="./Contact/contactLastName"/>
				<xsl:text>}</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!--  ============================= SolicitorAddress ====================================== -->
	<xsl:template name="SolicitorAddress">
		<xsl:for-each select="//Deal/PartyProfile">
			<xsl:if test="./partyTypeId='50'">
				<xsl:text>{</xsl:text>
				
        <!--#DG364 -->
    		<xsl:for-each select="Contact/Addr">
      		<xsl:call-template name="ContactAddress"/>
    		</xsl:for-each>						
				
				<!--#DG364 xsl:value-of select="./Contact/Addr/addressLine1"/>
				<xsl:text>\line </xsl:text>
				<xsl:if test="./Contact/Addr/addressLine2">
					<xsl:value-of select="./Contact/Addr/addressLine2"/>
					<xsl:text>\line </xsl:text>
				</xsl:if>
				<xsl:value-of select="./Contact/Addr/city"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="./Contact/Addr/province"/>
				<xsl:text>\line </xsl:text>
				<xsl:value-of select="concat(./Contact/Addr/postalFSA,' ', ./Contact/Addr/postalLDU)"/-->
				<xsl:text>}</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="SolicitorPhoneFaxEn">
		<xsl:for-each select="//Deal/PartyProfile">
			<xsl:if test="./partyTypeId='50'">
				<xsl:text>Phone: </xsl:text>
				<xsl:call-template name="FormatPhone">
					<xsl:with-param name="pnum" select="./Contact/contactPhoneNumber"/>
				</xsl:call-template>
				<xsl:if test="./Contact/contactPhoneNumberExtension and string-length(./Contact/contactPhoneNumberExtension)!=0">
					<xsl:text> Ext. </xsl:text>
					<xsl:value-of select="./Contact/contactPhoneNumberExtension"/>
				</xsl:if>
				<xsl:text>\line </xsl:text>
				<xsl:if test="./Contact/contactFaxNumber">
					<xsl:text>Fax: \tab </xsl:text>
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="./Contact/contactFaxNumber"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="SolicitorPhoneFaxFr">
		<xsl:for-each select="//Deal/PartyProfile[partyTypeId=50]/Contact">
				<xsl:call-template name="FormatPhone">
					<xsl:with-param name="pnum" select="contactPhoneNumber"/>
				</xsl:call-template>
				<xsl:if test="contactPhoneNumberExtension!=''">
					<xsl:text> poste </xsl:text>
					<xsl:value-of select="contactPhoneNumberExtension"/>
				</xsl:if>
				<xsl:text>\line </xsl:text>
				<xsl:if test="contactFaxNumber!=''">
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="contactFaxNumber"/>
					</xsl:call-template>
				</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- rtf file start and rtf file end                                                                    -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="RTFFileEnd4">
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- letter size  (216 �- 279 mm or  612pt  x 792pt); margings: top=36pt, bottom =36pt, left=21.6pt, right=21.6pt-->

	<!--xsl:include href="CRV_SOL05.xsl"/-->
	<xsl:template name="ConfirmCloseEn">
		<xsl:call-template name="ConfirmCloseHeadEn"/>
		<xsl:call-template name="FaxToEn"/>
		<xsl:call-template name="LenderRef"/>
		<xsl:call-template name="lastCent"/>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- templates section                                                                                -->
	<!-- ************************************************************************ -->

	<xsl:template name="ConfirmCloseHeadEn">
		<xsl:text>\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }</xsl:text><!--#DG142 -->
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 AMENDED SOLICITOR INSTRUCTIONS AS OF </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->
		<!--#DG364 -->
		<xsl:text>\pard\f1 
\brdrl\brdrs\brdrw10 \brdrt\brdrs\brdrw10 
\brdrr\brdrs\brdrw10 \brdrb\brdrs\brdrw10 
\sa240\qc\fs20\line
\ul\b\fs28 CONFIRMATION OF CLOSING\fs20  \par
\par
\i **Please insert </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Hypothec</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Mortgage</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text> Registration Number and forward to our office within <!-- #DG564 48-->24hrs after the 
scheduled closing date**\par\par\par\ulnone\i0
\pard\intbl </xsl:text>
	</xsl:template>

	<!--  ============================= contact Address ====================================== -->
	<xsl:template name="ContactAddress">
    <xsl:value-of select="addressLine1"/>
    <xsl:text>\line </xsl:text>
    <xsl:if test="addressLine2 != ''">
    	<xsl:value-of select="addressLine2"/>
    	<xsl:text>\line </xsl:text>
    </xsl:if>
    <xsl:value-of select="city"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="province"/>
    <xsl:text>\line </xsl:text>
    <xsl:value-of select="postalFSA"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="postalLDU"/>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="propAdr">	
		<xsl:param name="separaPostal" select="'\line '"/>
		<xsl:for-each select="/*/Deal/Property[primaryPropertyFlag='Y']">	
    	<xsl:text>\caps </xsl:text>
      <xsl:if test="unitNumber != ''">
      	<xsl:value-of select="unitNumber"/>
      	<xsl:text> - </xsl:text>
      </xsl:if>
      <xsl:value-of select="propertyStreetNumber"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="propertyStreetName"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="streetType"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="streetDirection"/>
      <xsl:text>\par </xsl:text>
      <xsl:if test="propertyAddressLine2 != ''">
      	<xsl:value-of select="propertyAddressLine2"/>
      	<xsl:text>\par </xsl:text>
      </xsl:if>
      <xsl:value-of select="propertyCity"/>
      <xsl:text>, </xsl:text>
      <xsl:value-of select="province"/>
      <xsl:value-of select="$separaPostal"/>
      <xsl:value-of select="propertyPostalFSA"/>
      <xsl:text> </xsl:text>
      <xsl:value-of select="propertyPostalLDU"/>
      <xsl:text>\caps0 </xsl:text>
		</xsl:for-each>      
	</xsl:template>
	
	<!-- ================================================  -->
	<xsl:template name="FaxToEn">
		<!-- row 1 -->
		<xsl:call-template name="FaxTo_row_type1"/>
		<xsl:text>\f1\fs20\b FAX TO:\cell </xsl:text>
		<!-- #554
	<xsl:call-template name="FormatPhone">
		<xsl:with-param name="pnum" select="//specialRequirementTags/currentUser/Contact/contactFaxNumber"/>
	</xsl:call-template>	
#554 end -->
		<xsl:text>(416) 861-8484 or \line 1-888-753-5842</xsl:text>
		<!-- <xsl:value-of select="//Deal/BranchProfile/Contact/contactFaxNumber"/>-->
		<xsl:text>\cell 
FROM:\cell </xsl:text>
		<xsl:call-template name="SolicitorName"/>
		<xsl:text>\cell 
\row 

</xsl:text>
		<!-- row 2 -->
		<xsl:call-template name="FaxTo_row_type1"/>
		<xsl:text>ATTENTION:\cell 
Originations </xsl:text>
		<!-- #554 <xsl:call-template name="CurrentUserName"/> -->
		<xsl:text>\cell \cell 
</xsl:text>
		<xsl:call-template name="SolicitorAddress"/>
		<xsl:text>\cell 
\row 
</xsl:text>
		<!-- row 3 -->
		<xsl:call-template name="FaxTo_row_type1"/>
		<xsl:text>DATE:\cell 
</xsl:text>
		<xsl:value-of select="//General/CurrentDate"/>
		<xsl:text>\cell \cell 
</xsl:text>
		<xsl:call-template name="SolicitorPhoneFaxEn"/>
		<xsl:text>\cell 
\row 

\b0 </xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="FaxToFr">
		<!-- row 1 -->
		<xsl:call-template name="FaxTo_rowFr"/>
		<xsl:text>\f1\fs20\b DESTINATAIRE:\cell </xsl:text>
		<!-- #554
	<xsl:call-template name="FormatPhone">
		<xsl:with-param name="pnum" select="//specialRequirementTags/currentUser/Contact/contactFaxNumber"/>
	</xsl:call-template>	
#554 end -->
		<xsl:text>(416) 861-8484 ou \line 1-888-753-5842</xsl:text>
		<!-- <xsl:value-of select="//Deal/BranchProfile/Contact/contactFaxNumber"/>-->
		<xsl:text>\cell 
EXP�DITEUR:\cell </xsl:text>
		<xsl:call-template name="SolicitorName"/>
		<xsl:text>\cell 
\row 

</xsl:text>
		<!-- row 2 -->
		<xsl:call-template name="FaxTo_rowFr"/>
		<xsl:text>ATTENTION:\cell 
Service du montage </xsl:text>
		<!-- #554 <xsl:call-template name="CurrentUserName"/> -->
		<xsl:text>\cell \cell 
</xsl:text>
		<xsl:call-template name="SolicitorAddress"/>
		<xsl:text>\cell 
\row 
</xsl:text>
		<!-- row 3 -->
		<xsl:call-template name="FaxTo_rowFr"/>
		<xsl:text>DATE:\cell 
</xsl:text>
		<xsl:value-of select="//General/CurrentDate"/>
		<xsl:text>\cell \cell 
</xsl:text>
		<xsl:call-template name="SolicitorPhoneFaxEn"/>
		<xsl:text>\cell 
\row 

\b0 </xsl:text>
	</xsl:template>

	<!--  ============================= SolicitorNameAndAddress ====================================== -->
	<!-- duplicate, removed xsl:template name="SolicitorName"-->
	<!-- duplicate, removed xsl:template name="SolicitorAddress"-->
	<!-- ================================================  -->
	<!-- duplicate, removed xsl:template name="SolicitorPhoneFaxEn"-->
	<!-- ================================================  -->
	<!--xsl:template name="BorrowersListedBlocks"-->
	<!-- ================================================  -->
	<!--xsl:template name="BorrowerFullName"-->
	<!-- ================================================  -->

	<!-- Subject property, English formatting of address, upper case -->
	<!--#DG364 xsl:template match="Property">
		<xsl:value-of select="propertyStreetNumber"/>
		<xsl:text>\caps  </xsl:text>
		<xsl:value-of select="propertyStreetName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="streetType">
			<xsl:text>\caps </xsl:text>
			<xsl:value-of select="streetType"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:if test="streetDirection">
			<xsl:text>\caps </xsl:text>
			<xsl:value-of select="streetDirection"/>
		</xsl:if>
		<xsl:if test="unitNumber">
			<xsl:if test="string-length(unitNumber) > 0">
				<xsl:text>\caps , unit </xsl:text>
				<xsl:value-of select="unitNumber"/>
			</xsl:if>
		</xsl:if>
		<xsl:text>\line </xsl:text>
		<xsl:text>\caps </xsl:text>
		<xsl:value-of select="concat(propertyCity,', ',province)"/>
		<xsl:text>\line </xsl:text>
		<xsl:text>\caps </xsl:text>
		<xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
	</xsl:template-->

	<!-- ================================================  -->
	<xsl:template name="LenderRef">
	   <!--#DG364 added some blank lines -->
		<xsl:call-template name="HorizLine"/>
		<!-- row 1 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b {LENDER REFERENCE NUMBER:}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 2 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<!-- #DG364 Sasa -->
		<!--xsl:text>{MORTGAGOR(S):}}</xsl:text -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
	  			<xsl:text>{BORROWER(S):}}</xsl:text>
			</xsl:when>
			<xsl:otherwise>
	  			<xsl:text>{MORTGAGOR(S):}}</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- End of #DG364 Sasa -->
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b \caps {</xsl:text>
		<xsl:call-template name="BorrowersListedBlocks"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 3 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b {MUNICIPAL ADDRESS:}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<!--#DG364 xsl:apply-templates select="//Deal/Property"/-->
     <xsl:call-template name="propAdr"/>			
		
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 4 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<!-- #DG364 Sasa -->
		<!-- xsl:text>{MORTGAGE AMOUNT:}}</xsl:text -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
	  			<xsl:text>{HYPOTEC AMOUNT:}}</xsl:text>
			</xsl:when>
			<xsl:otherwise>
	  			<xsl:text>{MORTGAGE AMOUNT:}}</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- End of #DG364 Sasa -->
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 5 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b {CLOSING DATE:}}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 6 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b </xsl:text>
		<!-- #DG364 Sasa -->
		<!-- xsl:text>{MORTGAGE REGISTRATION NO:}}</xsl:text -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
	  			<xsl:text>{HYPOTEC REGISTRATION NO:}}</xsl:text>
			</xsl:when>
			<xsl:otherwise>
	  			<xsl:text>{MORTGAGE REGISTRATION NO:}}</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- End of #DG364 Sasa -->
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b\line _________________________________________________________________}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="HorizLine"/>
	</xsl:template>
	<!-- ================================================  -->

	<!-- ================================================  -->
	<xsl:template name="LenderRefFr">
	   <!--#DG364 added some blank lines -->
		<xsl:call-template name="HorizLine"/>
		<!-- row 1 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b NUM�RO DE R�F�RENCE DU PR�TEUR:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 2 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b EMPRUNTEUR(S) / GARANT(S):}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b \caps {</xsl:text>
		<xsl:call-template name="BorrowersListedBlocks"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 3 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b ADRESSE MUNICIPALE:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<!--#DG364 xsl:apply-templates select="//Deal/Property"/-->
     <xsl:call-template name="propAdr"/>			
		
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 4 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b MONTANT DE L'HYPOTH�QUE:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 5 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b DATE DE CONCLUSION:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b {</xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\line\cell \row }</xsl:text>
		<!-- row 6 -->
		<xsl:call-template name="LenderRef_row"/>
		<xsl:text>{\f1\fs20\b N� D'INSCRIPTION DE L'HYPOTH�QUE:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20\b\line _________________________________________________________________}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="HorizLine"/>
	</xsl:template>
	<!-- ================================================  -->

	<!-- ================================================  -->
	<xsl:template name="lastCent">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa360 </xsl:text>
		<xsl:text>We confirm that the above referenced transaction has closed. Our </xsl:text>
    <!--#DG364 -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
  		  <xsl:text>Legal Counsel</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  		  <xsl:text>Solicitor</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:text>'s Final report will follow.</xsl:text>
		<xsl:text>\par}</xsl:text>
		<!-- Signature -->
		<xsl:call-template name="Signature_row"/>
		<xsl:text>{\f1\fs20    }</xsl:text>
		<xsl:text>\cell \pard \intbl \qr </xsl:text>
		<xsl:text>{\f1\fs20\sb720 
 {______________________________________}}
</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="Signature_row"/>
		<xsl:text>{\f1\fs20    }</xsl:text>
		<xsl:text>\cell \pard \intbl \qr</xsl:text>
		<xsl:text>{\f1\fs20 Signature of</xsl:text>
		<!-- #DG364 Sasa -->
		<!-- xsl:text> Solicitor}</xsl:text -->
		<xsl:choose>
			<xsl:when test="$isQebecProperty">
	  			<xsl:text> Legal Counsel}
</xsl:text>
			</xsl:when>
			<xsl:otherwise>
	  			<xsl:text> Solicitor}
</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- End of #DG364 Sasa -->
		<xsl:text>\cell \row }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="lastCentFr">
		<xsl:call-template name="Prgr_Arial_10"/>
		<xsl:text>\sa360 </xsl:text>
		<xsl:text>Nous confirmons que la transaction susmentionn�e a �t� conclue. Notre rapport final 
du conseiller juridique suivra.</xsl:text>
		<xsl:text>\par}</xsl:text>
		<!-- Signature -->
		<xsl:call-template name="Signature_row"/>
		<xsl:text>{\f1\fs20    }</xsl:text>
		<xsl:text>\cell \pard \intbl \qr </xsl:text>
		<xsl:text>{\f1\fs20\sb720 
 {______________________________________}}
</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="Signature_row"/>
		<xsl:text>{\f1\fs20    }</xsl:text>
		<xsl:text>\cell \pard \intbl \qr</xsl:text>
		<xsl:text>{\f1\fs20 Signature du conseiller juridique}
</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- Row definitions 														                -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	
	<!--#DG364 not used -->
	<xsl:template name="FaxTo_row_type1_Old">
		<xsl:text>{\trowd \trgaph55\trleft-55\ql
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10840 
\pard \intbl</xsl:text>
	</xsl:template>

  <!--#DG364 new -->	
	<xsl:template name="FaxTo_row_type1">
		<xsl:text>\trowd\trgaph55\trleft845\trpaddl55\trpaddr55\trpaddfl3\trpaddfr3
\cellx2340
\cellx5400
\cellx6660
\cellx10860
\pard \intbl </xsl:text>
	</xsl:template>

	<xsl:template name="FaxTo_rowFr">
		<xsl:text>\trowd\trgaph55\trleft845\trpaddl55\trpaddr55\trpaddfl3\trpaddfr3
\cellx2550
\cellx5150
\cellx6660
\cellx10860
\pard \intbl </xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="FaxTo_row_type2">
		<xsl:text>{\trowd \trgaph55\trleft-55
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5000 
\pard \intbl</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="LenderRef_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx4000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \intbl</xsl:text>
	</xsl:template>

  <!--#DG364 new -->
	<xsl:template name="MtgDetailTbRow">
		<xsl:text>\trowd\trgaph108\trleft900\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx5580
\cellx10620
\pard\intbl </xsl:text>
	</xsl:template>
  
	<xsl:template name="MtgAdvanceTbRow">
		<xsl:text>\trowd\trgaph108\trleft900\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx5040
\cellx8460
\pard\intbl </xsl:text>
	</xsl:template>

	<xsl:template name="MtgAdvanceTbRowA">
		<xsl:text>\trowd\trgaph108\trleft900\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx5740
\cellx8460
\pard\intbl </xsl:text>
	</xsl:template>

	<xsl:template name="BorowTbRow">
		<xsl:text>\trowd\trgaph108\trleft-108
\trbrdrl\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx540
\cellx3960
\cellx10646
\pard\intbl </xsl:text>
	</xsl:template>

	<xsl:template name="BorowTbRowA">
		<xsl:text>\trowd\trgaph108\trleft-108
\trbrdrl\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx900
\cellx5220
\cellx10620
\pard\intbl </xsl:text>
	</xsl:template>

	<xsl:template name="MtgCmtTbRowA">
		<xsl:text>\trowd\trgaph108\trleft-91\trrh755\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx3420
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx7020
\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx11520
\pard\intbl </xsl:text>
	</xsl:template>

	<xsl:template name="MtgCmtTbRowC">
		<xsl:text>\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs \cellx1980
\clbrdrr\brdrw10\brdrs \cellx3420
\clbrdrl\brdrw10\brdrs \cellx5400
\clbrdrr\brdrw10\brdrs \cellx7020
\clbrdrl\brdrw10\brdrs \cellx9982
\clbrdrr\brdrw10\brdrs \cellx11520
\pard\intbl </xsl:text>
	</xsl:template>

	<xsl:template name="MtgCmtTbRowD">
		<xsl:text>\trowd\trgaph108\trleft-91\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs \cellx1980
\clbrdrr\brdrw10\brdrs \cellx11520
\pard\intbl 
</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="ContactName">
		<xsl:value-of select="contactFirstName"/>
		<xsl:text> </xsl:text>							
		<xsl:if test="contactMiddleInitial!= ''">
			<xsl:value-of select="contactMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>							
		<xsl:value-of select="contactLastName"/>
	</xsl:template>

	<!-- ================================================  -->
	<!-- row with 3 columns -->
	<xsl:template name="Chk_list_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \sa180 \intbl</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- row with 3 columns, default between rows -->
	<xsl:template name="Chk_list_row2">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \intbl</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="produce-empty-rows">
		<xsl:param name="count"/>
		<xsl:if test="$count != 0">
			<xsl:call-template name="Chk_list_empty_row"/>
			<xsl:call-template name="produce-empty-rows">
				<xsl:with-param name="count" select="$count - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="Chk_list_empty_row">
		<xsl:call-template name="Chk_list_row"/>
		<xsl:text>{\f1\fs20 [    ]}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>\cell \pard \intbl {\f1\fs20 _________________________________________________________________________________________ }
</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- duplicate, deleted xsl:template name="FormatPhone"-->
	<!-- ================================================  -->
	<xsl:template name="WeConfirm_line">
		<xsl:call-template name="Chk_list_row"/>
		<xsl:text>{\f1\fs20    }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 _________________________________________________________________________________________}
</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
	</xsl:template>
	<!-- ================================================  -->

	<!-- row with 3 columns -->
	<xsl:template name="BranchName_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx640
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx3300
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \sa180 \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->

	<!-- row with 2 columns -->
	<xsl:template name="Signature_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6400
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->

	<!-- ================================================  -->
	<xsl:template name="CurrentUserName">
	  <!--#DG364 -->
		<xsl:for-each select="//specialRequirementTags/currentUser/Contact">
		  <xsl:call-template name="ContactName"/>
		</xsl:for-each>
	</xsl:template>


	<!-- ************************************************************************ -->
	<!-- Utility templates 														                -->
	<!-- ************************************************************************ -->

	<!-- ================================================  -->
	<xsl:template name="HorizLine">
		<xsl:text>{\pard\sa180\brdrb \brdrs \brdrw30 \brsp10 \par}</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="Prgr_Arial_10_bold">
		<xsl:text>{\pard\sa180\f1\fs20\b
</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="Prgr_Arial_10">
		<xsl:text>{\pard\sa180\f1\fs20
</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- legal size  (216 �- 356 mm  or  612pt  x 1008pt); margings: top=36pt, bottom =36pt, left=21.6pt, right=21.6pt-->
</xsl:stylesheet>
