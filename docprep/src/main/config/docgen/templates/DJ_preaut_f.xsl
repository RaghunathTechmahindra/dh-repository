<?xml version="1.0" encoding="UTF-8"?>
<!-- created by Catherine Rutgaizer -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:xmlns="http://www.w3.org/1999/XSL/Format">
	
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	
	<!-- define attribute sets -->
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr">
		<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ObjectAttr" use-attribute-sets="MainFontAttr SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>								 
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr" use-attribute-sets="SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
 		<xsl:attribute name="keep-together">always</xsl:attribute>	
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>	
	<!-- end attribute sets -->	
	
	<xsl:template name="CreatePageOne">
		<xsl:element name="page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-before</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateSalesRepLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="space-before">7mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">170mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">25mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="SpaceBAOptimum"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">c:\dev\Desjardins\img\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="margin-left">2mm</xsl:attribute>
			<xsl:attribute name="space-before.optimum">17mm</xsl:attribute>
		 	<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block">
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>

						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TitleFontAttr"/>
							Avis de Pré-Autorisation d'un prêt hypothécaire
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
		 	<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:attribute name="margin-left">2mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Le  <xsl:value-of select="//General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateSalesRepLines">
		<xsl:element name="fo:table">
		 	<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:attribute name="margin-left">2mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">100mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">							
							<xsl:if test="//Deal/SourceOfBusinessProfile/Contact/salutation"><xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/salutation"/></xsl:if>
							<xsl:value-of select="concat(//Deal/SourceOfBusinessProfile/Contact/contactFirstName , '   ', //Deal/SourceOfBusinessProfile/Contact/contactLastName ) "/>
						</xsl:element>
						<xsl:element name="fo:block">
							Téléphone:  <xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>							
							<xsl:if test="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension"><xsl:value-of select="concat('  	poste: ', //Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension) "/></xsl:if>							
						</xsl:element>
						<xsl:element name="fo:block">
							Télécopieur:  <xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactFaxNumber"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
		 	<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
			<xsl:attribute name="margin-left">2mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">30mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">160mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Objet:</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							Nom de l'emprunteur: 							
							<xsl:if test="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerTypeId='0'">
								<xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerFirstName"/>
								<xsl:if test="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerMiddleInitial"><xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerMiddleInitial"/><xsl:text> </xsl:text></xsl:if>							
								<xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerLastName"/>
							</xsl:if>								
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:if test="position()=2">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell"><xsl:text> </xsl:text></xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									Nom du 1er co-emprunteur: 
									<xsl:value-of select="./borrowerFirstName"></xsl:value-of><xsl:text> </xsl:text><xsl:if test="./borrowerMiddleInitial"><xsl:value-of select="./borrowerMiddleInitial"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="./borrowerLastName"></xsl:value-of>
								</xsl:element>	
							</xsl:element>	
						</xsl:element>	
					</xsl:if>
					<xsl:if test="position()=3">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell"><xsl:text> </xsl:text></xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									Nom du 2ème co-emprunteur: 
									<xsl:value-of select="./borrowerFirstName"></xsl:value-of><xsl:text> </xsl:text><xsl:if test="./borrowerMiddleInitial"><xsl:value-of select="./borrowerMiddleInitial"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="./borrowerLastName"></xsl:value-of>
								</xsl:element>	
							</xsl:element>	
						</xsl:element>	
					</xsl:if>
					<xsl:if test="position()=4">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell"><xsl:text> </xsl:text></xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									Nom du 3ème co-emprunteur: 
									<xsl:value-of select="./borrowerFirstName"></xsl:value-of><xsl:text> </xsl:text><xsl:if test="./borrowerMiddleInitial"><xsl:value-of select="./borrowerMiddleInitial"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="./borrowerLastName"></xsl:value-of>
								</xsl:element>	
							</xsl:element>	
						</xsl:element>	
					</xsl:if>
				</xsl:for-each >
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:text> </xsl:text></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
								Numéro de dossier: <xsl:text> </xsl:text><xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:text> </xsl:text></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
								Montant pré-autorisé: <xsl:text> </xsl:text><xsl:value-of select="//Deal/totalLoanAmount"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>			
				Monsieur (Madame),
	    </xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
				La présente a pour but de vous confirmer la pré-autorisation du prêt des personnes mentionnées en titre, aux conditions suivantes:
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:list-block">
					<xsl:attribute name="margin-left">5mm</xsl:attribute>				
					<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
					<xsl:copy use-attribute-sets="MainFontAttr"/>
						<xsl:apply-templates select="//specialRequirementTags/conditions/condition"/>
				</xsl:element>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
				Veuillez prendre note que vous devez faire suivre les documents originaux au nom de    NAME???.
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
				Nous vous remercions de votre collaboration. 
		</xsl:element>
     </xsl:template>  
     
     <xsl:template match="condition">
		<xsl:element name="fo:list-item">
			<xsl:element name="fo:list-item-label">
				<xsl:element name="fo:block">&#x2022;</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item-body">
			<xsl:attribute name="start-indent">body-start()</xsl:attribute>
				<xsl:element name="fo:block"><xsl:value-of select="."/></xsl:element>
			</xsl:element>
		</xsl:element>
     </xsl:template>
     
     <xsl:template name="CreateUnderwriterLines">         
		<xsl:element name="fo:table">
		 	<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">4mm</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="font-style">italic</xsl:attribute>
								<xsl:attribute name="font-size">14pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">40pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
								<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select="//Deal/underwriter/userType"/>
							</xsl:element>									
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
	</xsl:template>
	
	<xsl:template name="FOStart">
		<xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:root">
			<xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute>
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">1in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">1in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">40mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">30mm</xsl:attribute>
						<xsl:attribute name="margin-left">38mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
