<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- 06/Jun/2005 DVG #DG224 #1464  Merix - e2e - MCAP Data Sheet  - New based on xcd-->
	<!-- 13/Apr/2005 DVG #DG182 #1207 Xceed - VRM (data sheet) -->
	<!-- 21/Mar/2005 DVG #DG172 #1103  Xceed - Data Sheet  - new field-->
	<!-- 29/Dec/2004 DVG #DG118 	scr#813 Xceed - Data Sheet Change -->
	<xsl:output method="text"/>

	<xsl:template match="/">	
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//LanguageEnglish">
				<xsl:call-template name="EnglishHeader"/>
				<xsl:call-template name="EnglishMain"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//LanguageFrench">
				<!--				<xsl:call-template name="FrenchTemplate"/>-->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishMain">
		<xsl:text>{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4068 \cellx3960
\cltxlrtb\clftsWidth3\clwWidth6948 \cellx10908
\row 
}
\pard \ql \widctlpar\brdrb\brdrs\brdrw10\brsp20 \aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs20 \par }
\pard \ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{ \par }}
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta  .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta  )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta  )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb  (}{\pntxta )}}
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Loan Number:\cell }
\pard\plain \s1\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs24 </xsl:text><xsl:value-of select="//ServicingMortgageNum"/><xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Alt Doc or Bk:\cell}
{\f1 AltDoc: }{\b\f1 </xsl:text><xsl:value-of select="//FinancingProgram"/><xsl:text>}
{\f1 \tab Bankruptcy: }{\b\f1 </xsl:text><xsl:value-of select="//PreviousBankruptStatus"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><!-- #DG224-->
		<xsl:value-of select="//LenderName"/>
		<xsl:text> Product Type:\cell }
\pard\plain \s1\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs24 </xsl:text><xsl:value-of select="//XceedProductTypeAlpha"/><xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Mortgagor Name:\cell }
{\b\f1 </xsl:text>
		<xsl:for-each select="//BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
<xsl:text>\cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//GuarantorClause"/><xsl:text>:\cell }
{\b\f1 </xsl:text>
		<xsl:for-each select="//GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Mailing Address:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PropertyAddress/Line1"/><xsl:text>{ }</xsl:text><xsl:value-of select="//PropertyAddress/Line2"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Mailing City:\cell }
{\b\f1 </xsl:text>
		<xsl:value-of select="//PropertyAddress/City"/>
		<xsl:if test="//PropertyAddress/City and //PropertyAddress/Province">
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:value-of select="//PropertyAddress/Province"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Mailing Postal Code:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PropertyAddress/PostalCode"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Client Search Key:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//BorrowerLastName"/>	<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Telephone Number:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//BorrowerPhoneNumber"/>	<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Work Telephone Number:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//BorrowerWorkPhoneNumber"/>	
	<xsl:if test="//BorrowerWorkPhoneNumberExt">
		<xsl:text> xt </xsl:text><xsl:value-of select="//BorrowerWorkPhoneNumberExt"></xsl:value-of>
	</xsl:if>
<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}


\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payor Name:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PrimaryBorrowerName"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Language:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//Language"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth2\trwWidth5000\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}

\pard \ql \sb120\keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 
\par 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 BANK INFO\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Bank Branch Number:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//BankBranchNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Bank Number:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//BankNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Bank Account Number:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//BankAccountNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Bank Name for Verification:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//BankName"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 CREATE </xsl:text><!-- #DG224-->
		<xsl:value-of select="//LenderName"/>
		<xsl:text> MORTGAGE \cell \cell}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Charge:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//ChargePriority"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Property Address:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PropertyAddress/Line1"/><xsl:text>{ }</xsl:text><xsl:value-of select="//PropertyAddress/Line2"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Property City:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PropertyAddress/City"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Civic Search Key:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PropertyStreetName"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Legal Address:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PropertyAddress/Line1"/><xsl:text>{ }</xsl:text><xsl:value-of select="//PropertyAddress/Line2"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Legal Plan:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//LegalLine1"/>	<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
</xsl:text>
<xsl:if test="//LegalLine2">
<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\cell 
{\b\f1 </xsl:text><xsl:value-of select="//LegalLine2"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
</xsl:text>
</xsl:if>
<xsl:if test="//LegalLine3">
<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\cell 
{\b\f1 </xsl:text><xsl:value-of select="//LegalLine3"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
</xsl:text>
</xsl:if>
<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Province:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PropertyAddress/Province"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Occupancy code:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//OccupancyType"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Property Usage:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PropertyType"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Improvement type:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//ImprovementType"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Condominium:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//Condo"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Construction:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//NewConstruction"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Free Hold:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//CondoFreeHold"/>	<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date of Appraisal:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//AppraisalDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Value of Land:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//LandValue"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Value of Improvement:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//ImprovementValue"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Value:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//ActualAppraisal"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Rating Code:\cell }
{\b\f1 10\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Assumable:\cell }
{\b\f1 N\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Number of Units:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//NumUnits"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Interim Financing:\cell }
{\b\f1 N\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 High Ratio:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//HighRatio"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Mortgage Insurance:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//MortgageInsurance"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Solicitor Name and Firm:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//Solicitor/Name"/>

		<!-- ******************************************************************************************************************************** -->
		<!--  Here we need to see if both of the items exist before we go and concatenate them together with a dash -->
		<!-- ******************************************************************************************************************************** -->
		<xsl:if test="//Solicitor/Name and //Solicitor/Firm">
			<xsl:text> - </xsl:text>
		</xsl:if>

		<xsl:value-of select="//Solicitor/Firm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Solicitor Address:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//Solicitor/Address1"/><xsl:text>{ }</xsl:text><xsl:value-of select="//Solicitor/Address2"/><xsl:text>\par </xsl:text>
<xsl:value-of select="//Solicitor/AddressCity"/>
		<xsl:if test="//Solicitor/AddressCity and //Solicitor/AddressProvince">
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:value-of select="//Solicitor/AddressProvince"/><xsl:text>{  }</xsl:text><xsl:value-of select="//Solicitor/AddressPostal"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date of Loan Application:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//ApplicationDate"/>	<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date of Com Issued:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//ApprovalDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date Com Accepted:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//AcceptedDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Special Process Option:\cell }
{\b\f1 N\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Lender Number:\cell }
{\b\f1 9060\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product Brand:\cell }
{\b\f1 BA\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Renewal Code:\cell }
{\b\f1 </xsl:text><!-- #DG182 {\b\f1 B1\cell }-->
     	<xsl:choose>
                  <xsl:when test="//InterestTypeId = 1">
				<xsl:text>B3</xsl:text>
		  </xsl:when>
	          <xsl:when test="//InterestTypeId = 3">
		 		<xsl:text>B2</xsl:text>
	          </xsl:when>
	          <xsl:otherwise>
				<xsl:text>B1</xsl:text>
	          </xsl:otherwise>
        </xsl:choose>
	<!-- #DG182 end -->	
	<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Line of Business:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//LOB"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product Line:\cell }
{\b\f1 </xsl:text><!-- #DG182 {\b\f1 01\cell }-->
     	<xsl:choose>
		  <xsl:when test="//InterestTypeId = 1">
				<xsl:text>01</xsl:text>
		  </xsl:when>
	          <xsl:when test="//InterestTypeId = 3">
		 		<xsl:text>01</xsl:text>
	          </xsl:when>
	          <xsl:otherwise>
				<xsl:text>1</xsl:text>
	          </xsl:otherwise>
        </xsl:choose>
	<!-- #DG182 end -->	
	<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product Type:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//XceedProductTypeNumeric"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Source of Business:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//SOB"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Business Incentives:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//SOB"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Commited Amount:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//LoanAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date of 1}{\f1\fs20\super st}{\f1\fs20  Advance:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//AdvanceDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Interest Rate:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//NetInterestRate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date to Set Rate:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//IADDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Compounding Freq:\cell }
{\b\f1 2\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Frequency:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PaymentFrequency"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Accelerated:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PaymentFrequencyAccelerated"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 First Payment Date:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//FirstPaymentDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Term:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PaymentTerm"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Maturity Date:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//MaturityDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amortization:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//Amortization"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Primary Payment Type:\cell }
{\b\f1 1\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Invoice Required:\cell }
{\b\f1 N\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Amount:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//TotalPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Lockin Expiry Date:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//MaturityDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Privilege Payment Option:\cell }
{\b\f1 20\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Early Payout Option:\cell }
{\b\f1 </xsl:text>
	<!-- #DG182 
	<!- - #DG118 - ->	
     	<xsl:choose>
	          <xsl:when test="//PrePaymentOptionsID = 0">
		 		<xsl:text>90</xsl:text>
	          </xsl:when>
	          <xsl:otherwise>
				<xsl:text>03</xsl:text>
	          </xsl:otherwise>
        </xsl:choose>
	<!- - #DG118 end -->	
     	<xsl:choose>
		  <xsl:when test="//InterestTypeId = 1">
				<xsl:text>03</xsl:text>
		  </xsl:when>
	          <xsl:when test="//InterestTypeId = 3">
		 		<xsl:text>03</xsl:text>
	          </xsl:when>
	          <xsl:otherwise>
				<xsl:text>90</xsl:text>
	          </xsl:otherwise>
        </xsl:choose>
	<!-- #DG182 end -->		
	<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Review Renewal:\cell }
{\b\f1 Y\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Rate Subsidy:\cell }
{\b\f1 N\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Municipality:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//TaxDepartmentName"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Annual Taxes:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//AnnualTaxes"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prevent Update:\cell }
{\b\f1 N\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Property Tax Amount:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//TaxEscrowAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Method:\cell }
{\b\f1 1\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 PAC Start Date:\tab \cell }
{\b\f1 </xsl:text><xsl:value-of select="//FirstPaymentDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 PAC Expiry Date:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//MaturityDate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs20  \trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 PAC Amount:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//TotalPaymentAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payor Name:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//PrimaryBorrowerName"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Bank Code:\cell }
{\b\f1 100\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Investor Number:\cell }
{\b\f1 1001\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}</xsl:text>
	<!-- #DG172  new field-->
	<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Cross Sell:\cell }
{\b\f1 </xsl:text><xsl:value-of select="//CrossSellProfile"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3708 \cellx3600
\cltxlrtb\clftsWidth3\clwWidth7308 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0</xsl:text>
	</xsl:template>
	
	<!-- ************************************************************************ 	-->
	<!-- French template section                                             			-->
	<!-- ************************************************************************ 	-->
	<!--	<xsl:template name="FrenchTemplate">
		</xsl:template>-->

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text> {
\par }}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
	 <!-- #DG670 -->
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl 
{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} 
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;
\red0\green0\blue0;
\red0\green0\blue255;
\red0\green255\blue255;
\red0\green255\blue0;
\red255\green0\blue255;
\red255\green0\blue0;
\red255\green255\blue0;
\red255\green255\blue255;
\red0\green0\blue128;
\red0\green128\blue128;
\red0\green128\blue0;
\red128\green0\blue128;
\red128\green0\blue0;
\red128\green128\blue0;
\red128\green128\blue128;
\red192\green192\blue192;
}
{\stylesheet {\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}}
\margl720\margr720 \widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin
\dghspace180\dgvspace180\dghorigin720\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale114\viewzk2\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd 
\linex0\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>
	<xsl:template name="EnglishHeader">
		<xsl:text>{\header 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4068 \cellx3960
\cltxlrtb\clftsWidth3\clwWidth6948 \cellx10908
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//LenderName"/>
		<xsl:text>\cell }\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs20 </xsl:text>
		<xsl:value-of select="//BranchAddress/Line1"/>
		<xsl:value-of select="/BranchAddress/Line2"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//BranchAddress/Province"/>
		<xsl:text>{  }</xsl:text>
		<xsl:value-of select="//BranchAddress/Postal"/>
		<xsl:text>\par Phone: </xsl:text>
		<xsl:value-of select="//BranchAddress/PhoneLocal"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//BranchAddress/PhoneTollFree"/>
		<xsl:text>\par Fax: </xsl:text>
		<xsl:value-of select="//BranchAddress/FaxLocal"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//BranchAddress/FaxTollFree"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4068 \cellx3960
\cltxlrtb\clftsWidth3\clwWidth6948 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4068 \cellx3960
\cltxlrtb\clftsWidth3\clwWidth6948 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\ul \cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4068 \cellx3960
\cltxlrtb\clftsWidth3\clwWidth6948 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4068 \cellx3960
\cltxlrtb\clftsWidth3\clwWidth6948 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\ul New Customer Data Input Sheet\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text><!-- #DG224-->
		<xsl:value-of select="//LenderName"/>
		<xsl:text> Deal Number: </xsl:text><xsl:value-of select="//DealNum"/><xsl:text>\tab Page }{\field{\*\fldinst {\f1\fs18  PAGE }}{\fldrslt {\f1\fs18\lang1024\langfe1024\noproof 1}}}{\f1\fs18  of }{\field{\*\fldinst {\f1\fs18 NUMPAGES }}{\fldrslt {\f1\fs18\lang1024\langfe1024\noproof 3}}}{\f1\fs18 \tab Date: </xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>}\cell 
\pard \ql  \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text>
	</xsl:template>
</xsl:stylesheet>
