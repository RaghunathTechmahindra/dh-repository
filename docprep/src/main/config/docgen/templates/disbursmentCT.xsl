<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
29/Aug/2008 DVG #DG748 FXP21885: Concentra CR209 Change logo 
06/Dec/2007 DVG #DG670 remove improper extra lines from rtf templates
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="text"/>

  <!--#DG748 -->
	<xsl:variable name="lendName" select="translate(/*/lenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />
	<xsl:variable name="logoFile">
		<xsl:choose>
			<xsl:when test="/*/French">
				<xsl:value-of select="concat('Logo',concat(concat($lendName,'_fr'),'.xsl'))" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat('Logo',concat(concat($lendName,'_en'),'.xsl'))" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />

	<xsl:template match="/">
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
		
		<xsl:call-template name="RTFFileStart"/>
		<xsl:call-template name="DLLogoAndBranchData"/>
		<xsl:call-template name="DLCurrentDateP1"/>
		<xsl:call-template name="DLSolicitorDataP1"/>
		<xsl:call-template name="DLAttentionP1"/>
		<xsl:call-template name="DLRELineP1"/>
		<xsl:call-template name="DLGuarantorsLineP1"/>
		<xsl:call-template name="DLSecurityLineP1"/>
		<xsl:call-template name="DLMortgageNumberLineP1"/>
		<xsl:call-template name="DLLineP1"/>
		<xsl:call-template name="DLTextPart1P1"/>
		<xsl:call-template name="DLTextPart2P1"/>
		<xsl:call-template name="DLTextPart3P1"/>
		<xsl:call-template name="DLMortgageInsurancePremiumP1"/>
		<xsl:call-template name="DLWrittenAuthorizationP1"/>
		<xsl:for-each select="//LenderSection">
			<xsl:call-template name="LenderTemplate"/>
		</xsl:for-each>
		<xsl:for-each select="/*/LOBSection">
			<xsl:call-template name="LOBTemplate"/>
		</xsl:for-each>
		<xsl:call-template name="FeaturesPage"/>
		<xsl:call-template name="FirstEnvelope"/>
		<xsl:call-template name="SecondEnvelope"/>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ -->
	<!-- templates section                                                        -->
	<!-- ************************************************************************ -->
	<xsl:template name="FirstEnvelope">
		<xsl:text>\par

\sect \sectd \pgwsxn13680\pghsxn5947\marglsxn1440\margrsxn720\margtsxn720\margbsxn720
\linex0\endnhere  \pard\plain
\par\par\par\par\par\par 

  </xsl:text>
		<xsl:text>\pard \ql \li0\ri0\widctlpar\tx6800\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 </xsl:text>
		<xsl:for-each select="/*/borrowers/borrower">
			<xsl:text>\tab {\f1\fs18 </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text> \par}

</xsl:text>
		</xsl:for-each>
		<xsl:text>\tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/PrimaryProperty/propertyAddress1"/>
		<xsl:text> \par}

</xsl:text>
		<xsl:choose>
			<xsl:when test="/*/PrimaryProperty/propertyAddress2">
				<xsl:text>\tab {\f1\fs18 </xsl:text>
				<xsl:value-of select="/*/PrimaryProperty/propertyAddress2"/>
				<xsl:text>\par}

</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>\tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/PrimaryProperty/propertyAddressCity"/>
		<xsl:text>\~ </xsl:text>
		<xsl:value-of select="/*/PrimaryProperty/propertyAddressProvince"/>
		<xsl:text>\~ </xsl:text>
		<xsl:value-of select="/*/PrimaryProperty/propertyAddressPostal"/>
		<xsl:text>\~ </xsl:text>
		<xsl:text> \par}

</xsl:text>
		<xsl:text>\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 {}</xsl:text>
	</xsl:template>

	<xsl:template name="SecondEnvelope">
		<xsl:text>\page

\par\par\par\par\par\par

</xsl:text>
		<xsl:text>\pard \ql \li0\ri0\widctlpar\tx6800\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 </xsl:text>
		<xsl:text>\tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorName"/>
		<xsl:text> \par}

</xsl:text>
		<xsl:text>\tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorAddress1"/>
		<xsl:text> \par}

</xsl:text>
		<xsl:choose>
			<xsl:when test="/*/SolicitorData/propertyAddress2">
				<xsl:text>\tab {\f1\fs18 </xsl:text>
				<xsl:value-of select="/*/SolicitorData/SolicitorAddress2"/>
				<xsl:text>\par}

</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>\tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorAddressCity"/>
		<xsl:text>\~ </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorAddressProvince"/>
		<xsl:text>\~ </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorAddressPostal"/>
		<xsl:text>\~ </xsl:text>
		<xsl:text> \par}

</xsl:text>
		<xsl:text>\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 {}</xsl:text>
		<xsl:text>\sect </xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPage">
		<xsl:call-template name="FeaturesPageTitle"/>
		<xsl:call-template name="FeaturesPageRow1"/>
		<xsl:call-template name="FeaturesPageRow2"/>
		<xsl:call-template name="FeaturesPageRow3"/>
		<xsl:call-template name="FeaturesPageRow4"/>
		<xsl:call-template name="FeaturesPageRow5"/>
		<xsl:call-template name="FeaturesPageRow6"/>
		<xsl:call-template name="FeaturesPageRow7"/>
		<xsl:call-template name="FeaturesPageRow8"/>
		<xsl:call-template name="FeaturesPageFinalStatement"/>
	</xsl:template>

	<xsl:template name="FeaturesPageFinalStatement">
		<xsl:text>{\par}

{\f1\fs18 Should you require additional information or wish to discuss one of the above features, 
please do not hesitate to contact our office. Our toll-free number is 1-800-788-6311.}</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageTitle">
		<xsl:text>{\page

\pard \tab\tab\tab}{\f1\fs18\b Features of Your </xsl:text>
		<xsl:value-of select="/*/lenderName"/>
		<xsl:text> Mortgage</xsl:text>
		<xsl:text> \par\par\par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageRow8">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18\b Assumptions}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 Prospective purchasers of your home may assume your mortgage upon * approval by </xsl:text>
		<xsl:value-of select="/*/lenderName"/>
		<xsl:text> (*not applicable in all provinces.)\par\par}

</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
		<xsl:text>\pard \par

</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageRow7">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18\b Portability}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 Your mortgage is portable which means you may transfer your current term and interest rate to your new home.  Should additional mortgage proceeds be required </xsl:text>
		<xsl:value-of select="/*/lenderName"/>
		<xsl:text> can blend the rates to avoid penalties.   (Usual application and appraisal requirements will apply.) \par\par}

</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageRow6">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18\b Statements}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 On the anniversary date of your mortgage </xsl:text>
		<xsl:value-of select="/*/lenderName"/>
		<xsl:text> will provide you with a detailed annual statement.\par\par}

</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageRow5">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18\b PrePayment Options}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 If you have chosen a closed term, your mortgage still allows for very 
liberal repayment privileges.  You can choose weekly/bi-weekly, semi-monthly payment frequency, 
25% increase on your principal and interest payments each year, up to double regular principal 
and interest payment and 20% Lump Sum payment of your original mortgage amount each calendar 
year.  Partial payment amounts must be equal to or greater than $1,000.00 with payments accepted 
up to a maximum of 6 times a year.  This is non-cumulative.\par\par}

</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageRow4">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18\b Early Renewals}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 Early renewal is available upon request at a nominal fee.  Open term 
mortgages must renew to closed terms.  Closed term mortgages may be subject to a penalty.  Rate 
blends are available. \par\par}

</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageRow3">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18\b Renewals}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 A renewal offer will be forwarded to you approximately sixty (60) days 
prior to the expiry date of your existing term.  The lowest rate in effect within the sixty-day 
period prior to the renewal is guaranteed for the chosen term.\par\par}

</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageRow2">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18\b Property Taxes}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 If </xsl:text>
		<xsl:value-of select="/*/lenderName"/>
		<xsl:text> is responsible for paying your property taxes, please review your Commitment Letter 
for particulars.  Your tax account will be reviewed annually and you will be notified of any 
payment changes due to an increase or decrease in property taxes.</xsl:text>
		<xsl:text>\par\par}
</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
	</xsl:template>

	<xsl:template name="FeaturesPageRow1">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18\b Interest Calculation}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 Interest is calculated semi-annually not in advance.  The total principal 
and interest payment is applied to the principal balance while interest accrues on the reducing 
balance for a six-month period.  Every six months the accrued interest is added (compounded) to 
the principal balance.\par\par}

</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ -->
	<xsl:template name="LOBTemplate">
		<xsl:text>\page

\par

</xsl:text>
		<xsl:call-template name="DLLogoAndBranchData"/>
		<xsl:call-template name="DLParagraph"/>
		<xsl:call-template name="DLCurrentDateP1"/>
		<xsl:call-template name="DLParagraph"/>
		<xsl:call-template name="DLBorrowersLOB"/>
		<xsl:call-template name="DLGuarantorsLOB"/>
		<xsl:call-template name="DLPrimaryPropertyLOB"/>
		<xsl:call-template name="DLRELOB"/>
		<xsl:call-template name="DLLineP1"/>
		<xsl:call-template name="DLThankYouLineLOB"/>
		<xsl:call-template name="DLCondOneLOB"/>
		<xsl:call-template name="DLConfirmSectionLOB"/>
		<xsl:call-template name="DLReturnedFeeLOB"/>
		<xsl:call-template name="DLBottomOfThePageLOB"/>
		<xsl:call-template name="DLReturnCopyLOB"/>
		<xsl:call-template name="DLAnyQuestionsLOB"/>
	</xsl:template>

	<xsl:template name="DLAnyQuestionsLOB">
		<xsl:text>{\f1\fs18 If you have any questions regarding your mortgage do not hesitate to contact the undersigned. \par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 Yours truly,\par\par\par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/UserData/UserName"/>
		<xsl:text> \par}

</xsl:text>
		<xsl:text>{\f1\fs18 Financial Services Administrator \par }

</xsl:text>
		<xsl:text>{\f1\fs18 Phone No. 1-800-788-6311 Extension No. </xsl:text>
		<xsl:value-of select="/*/UserData/UserExtension"/>
		<xsl:text>\par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 Enclosure}</xsl:text>
	</xsl:template>

	<xsl:template name="DLReturnCopyLOB">
		<xsl:choose>
			<xsl:when test="//isLOB">
				<xsl:text>{\f1\fs18 Please return the copy of this letter with the requested information in the envelope provided.\par\par}

</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="DLBottomOfThePageLOB">
		<xsl:choose>
			<xsl:when test="//InsuranceTag">
				<xsl:text>{\f1\fs18 If you had Life Insurance on your mortgage, then the existing life 
insurance will be transferred to your new mortgage number. However, the insurance will only cover 
the previous mortgage amount. For details on your existing coverage, please contact CUMIS 
Insurance at 1-800-263-9120. \par\par}

</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\f1\fs18 This mortgage is not life insured through </xsl:text>
				<xsl:value-of select="/*/lenderName"/>
				<xsl:text>.\par\par}

</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- <xsl:text>{\f1\fs18 This mortgage is not life insured through </xsl:text>
<xsl:value-of select="/*/lenderName"/>
<xsl:text>.\par\par}</xsl:text> -->
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/lenderName"/>
		<xsl:text> offers a wide range of financial services that may be of interest to you.</xsl:text>
		<xsl:text> For information on our products, please contact our Service Centre at 1-800-788-6311.\par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLReturnedFeeLOB">
		<xsl:choose>
			<xsl:when test="//isLOB">
				<xsl:text>{\f1\fs18 Please note the "Returned Item Fee" has been increased to $60.00.\par\par}

</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="DLConfirmSectionLOB">
		<xsl:text>{\f1\fs18 This will confirm that all proceeds of your mortgage have been forwarded to your solicitor. The </xsl:text>
		<xsl:value-of select="/*/paymentFrequency"/>
		<xsl:text> payment in the amount of </xsl:text>
		<xsl:value-of select="/*/pAndIPayment"/>
		<xsl:text> will be debited from your account on </xsl:text>
		<xsl:value-of select="/*/firstPaymentDate"/>
		<xsl:text>. \par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 This will also confirm your interest rate is </xsl:text>
		<xsl:value-of select="/*/interestRate"/>
		<xsl:text> for a </xsl:text>
		<xsl:value-of select="/*/term"/>
		<xsl:text> term. </xsl:text>
		<xsl:text> The mortgage renewal date is </xsl:text>
		<xsl:value-of select="/*/maturityDate"/>
		<xsl:text>.\par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLCondOneLOB">
		<xsl:choose>
			<xsl:when test="//isLOB">
				<xsl:text>{\f1\fs18 At this time we request that you confirm and provide the following:\par\par}

</xsl:text>
				<xsl:text>{\f1\fs18 Above address correct?   Yes____ No___\par\par}

</xsl:text>
				<xsl:for-each select="/*/borrowers/borrower">
					<xsl:text>\pard \ql \li0\ri0\widctlpar\tx10\tx30\tx6000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 </xsl:text>
					<xsl:text>{\f1\fs18 </xsl:text>
					<xsl:text>\tab </xsl:text>
					<xsl:value-of select="."/>
					<xsl:text> work phone number \tab (_______________________________)\par 

</xsl:text>
					<xsl:text>\tab Home phone number:\tab (_______________________________)\par 

</xsl:text>
					<xsl:text>\tab Email address: \tab (_______________________________)\par 

</xsl:text>
					<xsl:text>\tab Fax number: \tab (_______________________________)\par\par 

</xsl:text>
					<xsl:text>}</xsl:text>
					<xsl:text>\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 {\par}

</xsl:text>
				</xsl:for-each>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="DLThankYouLineLOB">
		<xsl:text>{\f1\fs18 Thank you for selecting }</xsl:text>
		<xsl:value-of select="/*/lenderName"/>
		<xsl:text>{\f1\fs18  for your mortgage. We look forward to servicing your needs.\par\par }

</xsl:text>
	</xsl:template>

	<xsl:template name="DLRELOB">
		<xsl:text>\pard \ql \li0\ri0\widctlpar\tx10\tx300\tx1000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 {</xsl:text>
		<xsl:text>\tab {\f1\fs18 Re:\tab Mortgage Number: \tab </xsl:text>
		<xsl:value-of select="/*/servicingMortgageNumber"/>
		<xsl:text>}\par

</xsl:text>
		<xsl:text>\tab\tab{\f1\fs18 Mortgage Amount: \tab </xsl:text>
		<xsl:value-of select="/*/LoanAmount"/>
		<xsl:text>}\par

</xsl:text>
		<xsl:text>}</xsl:text>
		<xsl:text>\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 </xsl:text>
		<xsl:text>{\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLPrimaryPropertyLOB">
		<xsl:for-each select="/*/PrimaryProperty">
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="./propertyAddress1Upper"/>
			<xsl:text>\par }

</xsl:text>
			<xsl:choose>
				<xsl:when test="./propertyAddress2">
					<xsl:text>{\f1\fs18 </xsl:text>
					<xsl:value-of select="./propertyAddress2Upper"/>
					<xsl:text>\par}

</xsl:text>
				</xsl:when>
			</xsl:choose>
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="./propertyAddressCityUpper"/>
			<xsl:text>\~</xsl:text>
			<xsl:value-of select="./propertyAddressProvinceUpper"/>
			<xsl:text>\~\~</xsl:text>
			<xsl:value-of select="./propertyAddressPostal"/>
			<xsl:text>\par\par\par\par }

</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="DLGuarantorsLOB">
		<xsl:for-each select="/*/guarantorsUpper/guarantorUpper">
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text>\par }

</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="DLBorrowersLOB">
		<xsl:text>\par

</xsl:text>
		<xsl:for-each select="/*/borrowersUpper/borrowerUpper">
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text>\par }

</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<!-- ************************************************************************** -->
	<xsl:template name="LenderTemplate">
		<xsl:text>\page

\par

</xsl:text>
		<xsl:call-template name="DLLogoAndBranchData"/>
		<xsl:call-template name="DLCurrentDateP1"/>
		<xsl:call-template name="DLTaxDepartmentData"/>
		<xsl:call-template name="DLTaxAttention"/>
		<xsl:call-template name="DLTaxRe"/>
		<xsl:call-template name="DLLineP1"/>
		<xsl:call-template name="DLTaxAdvice"/>
		<xsl:call-template name="DLTaxBorrowersAndGuarantors"/>
		<xsl:call-template name="DLProperties"/>
		<xsl:call-template name="DLBottomOfThePage"/>
	</xsl:template>

	<xsl:template name="DLBottomOfThePage">
		<xsl:text>{\f1\fs18 Please change your records accordingly. If there are any questions concerning this matter, please contact our office.\par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 Yours truly,\par\par\par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/UserData/UserName"/>
		<xsl:text> \par}

</xsl:text>
		<xsl:text>{\f1\fs18 Financial Services Administrator \par }

</xsl:text>
		<xsl:text>{\f1\fs18 Phone No. 306-956-</xsl:text>
		<xsl:value-of select="/*/UserData/UserExtension"/>
		<xsl:text>\par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 Enclosure}</xsl:text>
	</xsl:template>

	<xsl:template name="DLProperties">
		<xsl:for-each select="/*/properties/property">
			<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
			<xsl:text>{\f1\fs18 Legal Description: }</xsl:text>
			<xsl:text>\cell \pard \intbl </xsl:text>
			<xsl:choose>
				<xsl:when test="./LegalLine1">
					<xsl:text>{\f1\fs18 </xsl:text>
					<xsl:value-of select="./LegalLine1"/>
					<xsl:text>\par}

</xsl:text>
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="./LegalLine2">
					<xsl:text>{\f1\fs18 </xsl:text>
					<xsl:value-of select="./LegalLine2"/>
					<xsl:text>\par}

</xsl:text>
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="./LegalLine3">
					<xsl:text>{\f1\fs18 </xsl:text>
					<xsl:value-of select="./LegalLine3"/>
					<xsl:text>\par}

</xsl:text>
				</xsl:when>
			</xsl:choose>
			<xsl:text>\cell \pard \intbl \row 
</xsl:text>
			<xsl:text>\pard \par

</xsl:text>
			<!-- ***************** -->
			<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
			<xsl:text>{\f1\fs18 Civic Address: }</xsl:text>
			<xsl:text>\cell \pard \intbl </xsl:text>
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="./propertyAddress1"/>
			<xsl:text>\par}

</xsl:text>
			<xsl:choose>
				<xsl:when test="./propertyAddress2">
					<xsl:text>{\f1\fs18 </xsl:text>
					<xsl:value-of select="./propertyAddress2"/>
					<xsl:text>\par}

</xsl:text>
				</xsl:when>
			</xsl:choose>
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="./propertyAddressCity"/>
			<xsl:text>\~\~</xsl:text>
			<xsl:value-of select="./propertyAddressProvince"/>
			<xsl:text>\par}

</xsl:text>
			<xsl:text>\cell \pard \intbl \row 
</xsl:text>
			<xsl:text>\pard \par

</xsl:text>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="DLTaxBorrowersAndGuarantors">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx2000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 Name: }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:for-each select="/*/borrowers/borrower">
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text>}\par

</xsl:text>
		</xsl:for-each>
		<xsl:for-each select="/*/guarantors/guarantor">
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text>}\par

</xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
		<xsl:text>\pard \par

</xsl:text>
	</xsl:template>

	<xsl:template name="DLTaxAdvice">
		<xsl:text>{\f1\fs18 Please be advised that we will be paying the property taxes on the following property beginning with the </xsl:text>
		<xsl:value-of select="/*/interimFinal"/>
		<xsl:text>\par\par }

</xsl:text>
	</xsl:template>

	<xsl:template name="DLTaxRe">
		<xsl:text>\pard \ql \li0\ri0\widctlpar\tx10\tx1000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 {</xsl:text>
		<xsl:text>\tab {\f1\fs18 Re: \tab Property Tax Payments \par}

</xsl:text>
		<xsl:text>\tab\tab{\f1\fs18 Our File No. </xsl:text>
		<xsl:value-of select="/*/servicingMortgageNumber"/>
		<xsl:text>}\par

</xsl:text>
		<xsl:text>}</xsl:text>
		<xsl:text>\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 </xsl:text>
		<xsl:text>{\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLTaxAttention">
		<xsl:text>{\f1\fs18 Attention: Tax Department \par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLTaxDepartmentData">
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/TaxDepartmentData/TaxDepartmentNameUpper"/>
		<xsl:text>}\par

</xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/TaxDepartmentData/TaxDepartmentAddress1Upper"/>
		<xsl:text>}\par

</xsl:text>
		<xsl:choose>
			<xsl:when test="/*/TaxDepartmentData/TaxDepartmentAddress2">
				<xsl:text>{\f1\fs18 </xsl:text>
				<xsl:value-of select="/*/TaxDepartmentData/TaxDepartmentAddress2Upper"/>
				<xsl:text>}\par 

</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/TaxDepartmentData/TaxDepartmentCityUpper"/>
		<xsl:text>\~</xsl:text>
		<xsl:value-of select="/*/TaxDepartmentData/TaxDepartmentProvinceUpper"/>
		<xsl:text>\~\~</xsl:text>
		<xsl:value-of select="/*/TaxDepartmentData/TaxDepartmentPostal"/>
		<xsl:text>}\par\par

</xsl:text>
	</xsl:template>

	<xsl:template name="DLWrittenAuthorizationP1">
		<xsl:text>{\f1\fs18 WRITTEN AUTHORIZATION WILL BE REQUIRED FROM OUR OFFICE PRIOR TO RELEASING 
FUNDS PENDING RECEIPT OF THE PRELIMINARY REPORT AND RELEVANT DOCUMENTATION.\par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 We trust you will find the enclosures in order and look forward to your 
final report and other relevant documentation within 30 days. If you have any questions, please 
contact the undersigned. \par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 Yours truly, \par\par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/UserData/UserName"/>
		<xsl:text>\par}

</xsl:text>
		<xsl:text>{\f1\fs18 Financial Services Administrator \par}

</xsl:text>
		<xsl:text>{\f1\fs18 1-800-788-6311 Extension No.</xsl:text>
		<xsl:value-of select="/*/UserData/UserExtension"/>
		<xsl:text>\par\par}

</xsl:text>
		<xsl:text>{\f1\fs18 Enclosure </xsl:text>
	</xsl:template>

	<xsl:template name="DLMortgageInsurancePremiumP1">
		<xsl:text>\pard \ql \li0\ri0\widctlpar\tx10\tqr\tx8000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 {</xsl:text>
		<xsl:text>\tab {\f1\fs18 Mortgage Insurance Premium \tab </xsl:text>
		<xsl:value-of select="/*/premium"/>
		<xsl:text>\par}

</xsl:text>
		<xsl:text>\tab {\f1\fs18 Mortgage Premium PST\tab </xsl:text>
		<xsl:value-of select="/*/PST"/>
		<xsl:text>\par}

</xsl:text>
		<xsl:text>\tab {\f1\fs18 IAD Amount \tab </xsl:text>
		<xsl:value-of select="/*/IADAmount"/>
		<xsl:text>\par}

</xsl:text>
		<xsl:text>\tab {\f1\fs18 Existing Mortgage \tab </xsl:text>
		<xsl:value-of select="/*/existingMortgage"/>
		<xsl:text>\par}

</xsl:text>
		<xsl:text>}</xsl:text>
		<xsl:for-each select="/*/Fees/Fee">
			<xsl:text>\tab {\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text> }\tab {\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text> }\par

</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 </xsl:text>
		<xsl:text>{\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLTextPart3P1">
		<xsl:text>{\f1\fs18 We have made the following deductions from the gross advance of </xsl:text>
		<xsl:value-of select="/*/LoanAmount"/>
		<xsl:text> \par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLTextPart2P1">
		<xsl:text>{\f1\fs18 This cheque is being forwarded to you "In Trust". You may disburse the 
mortgage proceeds once you ascertain the title will be free and clear of all encumbrances and all 
other conditions in our Instructions to Solicitor will be fulfilled. Should you be unable to meet 
the trust conditions within 10 days, please contact our office for further instructions. \par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLTextPart1P1">
		<xsl:text>{\f1\fs18 We are enclosing our cheque in the amount of </xsl:text>
		<xsl:value-of select="/*/netAdvance"/>
		<xsl:text> that represents the proceeds for the above mortgage.</xsl:text>
		<xsl:text> \par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLLineP1">
		<xsl:text>{\f1\fs18 ________________________________________________________________________________________________________ \par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLMortgageNumberLineP1">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx3000 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18  }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 Mortgage Number: }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/servicingMortgageNumber"/>
		<xsl:text>}</xsl:text>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
		<xsl:text>\pard \par

</xsl:text>
	</xsl:template>

	<xsl:template name="DLSecurityLineP1">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx3000 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18  }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 Security: }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:for-each select="/*/properties/property">
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="./propertyAddress1"/>
			<xsl:text> \par}

</xsl:text>
			<xsl:choose>
				<xsl:when test="./propertyAddress2">
					<xsl:text>{\f1\fs18 </xsl:text>
					<xsl:value-of select="./propertyAddress2"/>
					<xsl:text>}\par 

</xsl:text>
				</xsl:when>
			</xsl:choose>
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="./propertyAddressCity"/>
			<xsl:text>{\f1\fs18 ,\~}</xsl:text>
			<xsl:value-of select="./propertyAddressProvince"/>
			<xsl:text> \par }

</xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
		<!-- <xsl:text>\pard \par </xsl:text> -->
	</xsl:template>

	<xsl:template name="DLGuarantorsLineP1">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx3000 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18  }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/guarantorClause"/>
		<xsl:text>(s): }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:for-each select="/*/guarantors/guarantor">
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text> \par}

</xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
		<!-- <xsl:text>\pard \par </xsl:text> -->
	</xsl:template>

	<xsl:template name="DLRELineP1">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx500

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx3000 

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 RE: }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs18 Borrower(s): }</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:for-each select="/*/borrowers/borrower">
			<xsl:text>{\f1\fs18 </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text> \par}

</xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \pard \intbl \row 
</xsl:text>
		<!-- <xsl:text>\pard \par </xsl:text> -->
	</xsl:template>

	<xsl:template name="DLAttentionP1">
		<xsl:text>{\f1\fs18 ATTENTION: </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorName"/>
		<xsl:text>\par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLSolicitorDataP1">
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorNameUpper"/>
		<xsl:text>\par}

</xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorAddress1Upper"/>
		<xsl:text>\par}

</xsl:text>
		<xsl:choose>
			<xsl:when test="/*/SolicitorData/SolicitorAddress2">
				<xsl:text>{\f1\fs18 </xsl:text>
				<xsl:value-of select="/*/SolicitorData/SolicitorAddress2Upper"/>
				<xsl:text>}\par 

</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorAddressCityUpper"/>
		<xsl:text>\~}</xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorAddressProvinceUpper"/>
		<xsl:text>\~\~}</xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorData/SolicitorAddressPostal"/>
		<xsl:text>\par\par}

</xsl:text>
	</xsl:template>

	<xsl:template name="DLCurrentDateP1">
		<xsl:text>\par{\f1\fs18  </xsl:text>
		<xsl:value-of select="/*/CurrentDate"/>
		<xsl:text>}\par\par

</xsl:text>
	</xsl:template>

	<xsl:template name="DLParagraph">
		<xsl:text>{\par }

</xsl:text>
	</xsl:template>

	<xsl:template name="DLLogoAndBranchData">
		<xsl:text>\trowd \trgaph108\trleft-108 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx5000

\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone\cellx10908 
\pard \intbl </xsl:text>
		<!-- #DG748 xsl:call-template name="LogoImage"/-->
		<xsl:value-of select="$logoRtf"/>

\cell \pard \intbl 
<xsl:text>\qr{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/lenderName"/>
		<xsl:text>} \par

</xsl:text>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/BranchData/BranchAddress1"/>
		<xsl:text>}\par

</xsl:text>
		<xsl:choose>
			<xsl:when test="/*/BranchData/BranchAddress2">
				<xsl:text>{\f1\fs18 </xsl:text>
				<xsl:value-of select="/*/BranchData/BranchAddress2"/>
				<xsl:text>}\par\par 

</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/BranchData/BranchAddressCity"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/BranchData/BranchAddressProvince"/>
		<xsl:text>\~\~</xsl:text>
		<xsl:value-of select="/*/BranchData/BranchAddressPostal"/>
		<xsl:text>}\par

</xsl:text>
		<xsl:text>\cell \pard \intbl \row
</xsl:text>
		<xsl:text>\pard \par

</xsl:text>
	</xsl:template>
	<!-- ************************************************************************ -->

	<!-- ************************************************************************ -->
	<!-- Lender logo                                                              -->
	<!-- ************************************************************************ -->
	<!-- #DG748 now comes from external file
	<xsl:template name="LogoImage">
		<xsl:text>
\f1{\pict\wmetafile8\picwgoal3330\pichgoal645 
0100090000035538000000003b38000000000400000003010800050000000c02d5ffde00050000
...
fefefefefefefefefefefefefe0000030000000000
}</xsl:text>
	</xsl:template-->

	<!-- ************************************************************************ -->
	<!-- rtf file start and rtf file end                                          -->
	<!-- ************************************************************************ -->

	<xsl:template name="RTFFileEnd">
		<xsl:text>}}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
    <!-- #DG670 -->
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl 
  {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} 
  {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
}
{\colortbl;
 \red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;
 \red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;
 \red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;
 \red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;
}
{\stylesheet {\ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24
\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}
{\*\cs10 \additive Default Paragraph Font;}}
{\info {\author Zivko Radulovic}{\operator BASIS100 automatic report producer}
{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company Basis100 Inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\formshade\horzdoc
\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand
\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl
\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0
\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \margl720\margr720\margt720\margb720\deftab720 
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\footer \pard\plain \s15\ql \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr
\posy0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid
\langnp1033\langfenp1033
{\cs16 }
{\field {\*\fldinst {\cs16 }}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}}}
{\f1\fs16 }
{\field {\*\fldinst {\cs16 }}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}}}
{\cs16 \par }
\pard \s15\ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }}

</xsl:text>
	</xsl:template>
	
</xsl:stylesheet>
