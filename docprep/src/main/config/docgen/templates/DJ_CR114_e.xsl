<?xml version="1.0" encoding="UTF-8"?>
<!--
12/Sep/2005 DVG #DG316 #  2106  DJ CR114 - Ensure underwriter initial appears in signature line  
09/Mar/2005 DVG #DG166 #1063  DJ - miscellaneous DocPrep fixes  
22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik
-->
<!-- Author Zivko Radulovic -->
<!-- edited by Catherine Rutgaizer, March 2004 -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- ================  Attribute sets ============== -->
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBA1.5Optimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainTextAttr_Page_2">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ListLabelIndent">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="padding-left">-3mm</xsl:attribute>
	</xsl:attribute-set>
	<!-- ================  Attribute sets ============== -->
	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<!-- ========================== top level ================================  -->
	<xsl:template name="CreatePageTwo">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					Page <fo:page-number/> of <fo:page-number-citation ref-id="page_2"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLine_Page_2"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateHeader_Page_2"/>
				<xsl:call-template name="Page_2_Item_1"/>
				<xsl:call-template name="Page_2_Item_1a"/>
				<xsl:call-template name="Page_2_Item_1b"/>
				<xsl:call-template name="Page_2_Item_1c"/>
				<xsl:call-template name="Page_2_Item_2"/>
				<xsl:call-template name="Page_2_Item_2a"/>
				<xsl:call-template name="Page_2_Item_2b"/>
				<xsl:call-template name="Page_2_Item_2b_after"/>
				<xsl:call-template name="Page_2_Item_2c"/>
				<xsl:call-template name="Page_2_Item_2c_after"/>
				<xsl:call-template name="Page_2_Item_3"/>
				<!-- primary borrower -->
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
						<xsl:if test="position()=1">
							<xsl:call-template name="Page_2_Item_4"/>
							<xsl:call-template name="Page_2_Item_4a"/>
							<xsl:call-template name="Page_2_Item_4b"/>
						</xsl:if>
					</xsl:for-each>
				</xsl:element>
				<xsl:call-template name="Page_2_Item_5"/>
				<!-- <xsl:call-template name="Page_2_Item_6"/> -->
				<xsl:call-template name="Page_2_Item_6"/>
				<xsl:call-template name="Page_2_Item_6a"/>
				<xsl:call-template name="Page_2_Item_7"/>
				<xsl:call-template name="Page_2_Item_7a"/>
				<xsl:call-template name="Page_2_Item_8"/>
				<xsl:call-template name="Page_2_Item_Ending"/>
				<xsl:call-template name="Page_2_Item_Ending_a"/>
				<fo:block id="page_2" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ====================================================  Page 2 ====================================================   -->
	<!-- ====================================================  Page 2 ====================================================   -->
	<xsl:template name="CreateTitleLine_Page_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<!-- <xsl:attribute name="space-before">0.7in</xsl:attribute> -->
			<!-- <xsl:attribute name="break-before">page</xsl:attribute> -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">14cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								OFFER TO FINANCE - IMMOVABLE HYPOTHEC <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- Subject property, English formatting of address -->
	<xsl:template match="Property">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:value-of select="propertyStreetNumber"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="propertyStreetName"/>
			<xsl:text> </xsl:text>
			<xsl:if test="streetType">
				<xsl:value-of select="streetType"/>
				<xsl:text>  </xsl:text>
			</xsl:if>
			<xsl:if test="streetDirection">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="streetDirection"/>
			</xsl:if>
			<xsl:if test="unitNumber">, unit <xsl:value-of select="unitNumber"/>
			</xsl:if>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyCity,' ',province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateHeader_Page_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">75mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">80mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						To:
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:call-template name="BorrowersListedBlocks"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:call-template name="PrimBorrAddress"/>
						</xsl:element>
					</xsl:element>
					<!-- =============================  -->
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						From:
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyName"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine1"/>
						</xsl:element>
						<xsl:if test="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine2">
							<xsl:element name="fo:block">
								<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:attribute name="font-weight">bold</xsl:attribute>
								<fo:inline font-weight="bold">
									<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine2"/>
								</fo:inline>
							</xsl:element>
						</xsl:if>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/city"/>, <xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/province"/>
							</fo:inline>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/postalFSA"/>&#160;<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/postalLDU"/>
							</fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:text>Dear Member(s):</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			We are pleased to inform you that your application for a hypothecary loan in the amount of <fo:inline font-weight="bold">
				<xsl:value-of select="//Deal/totalLoanAmount"/>
			</fo:inline> has been accepted, at the following conditions:
		</xsl:element>
	</xsl:template>
	<!-- ==========================  Page 2 Item 1  ==========================   -->
	<xsl:template name="Page_2_Item_1">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>1. </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>The loan shall bear interest: </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_1a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:call-template name="ConstructPage2Item1aBox"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>a)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1aText"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="ConstructPage2Item1aBox">
		<xsl:param name="pRate"/>
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='0'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ConstructPage2Item1aText">
		<xsl:param name="pRate"/>
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='0'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					at a fixed rate: i.e., a rate of <fo:inline font-weight="bold">
							<xsl:value-of select="//Deal/netInterestRate"/>
						</fo:inline> per annum, reckoned six-monthly and not in advance 
				</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					at a fixed rate: MARKER
				</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Page_2_Item_1b">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:call-template name="ConstructPage2Item1bBox"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>b)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1bText"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="ConstructPage2Item1bBox">
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='2'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="//Deal/interestTypeId='3'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ConstructPage2Item1bText">
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='2'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text>at a variable rate: i.e., at the Caisse centrale Desjardins' prime rate increased by </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/discount"/>
								</fo:inline>
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/premium"/>
								</fo:inline>
							</xsl:when>
						</xsl:choose>
						<xsl:text> % per annum. The rate applicable to the loan shall vary with each change in such prime rate and shall be calculated monthly and not In advance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="//Deal/interestTypeId='3'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text>at a variable rate: i.e., at the Caisse centrale Desjardins' prime rate increased by  </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/discount"/>
								</fo:inline>
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/premium"/>
								</fo:inline>
							</xsl:when>
						</xsl:choose>
						<xsl:text> % per annum. The rate applicable to the loan shall vary with each change in such prime rate and shall be calculated monthly and not In advance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text>  at a variable rate: at the Caisse centrale Desjardins' prime rate. The rate applicable to the loan shall vary with each change in such prime rate and shall be calculated monthly and not in advance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Page_2_Item_1c">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="text-align">justify</xsl:attribute>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '2'">
									Maximum rate: However, the rate applicable to the loan shall not at any time exceed <fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/netInterestRate"/>
									</fo:inline> % per annum reckoned monthly and not in advance. i.e., <fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/TO_BE_DETERMINED"/>
									</fo:inline>  % per annum reckoned six-monthly and not in advance. 										
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Maximum rate: However, the rate applicable to the loan shall not at any time exceed MARKER % per annum reckoned six-monthly and not in advance. </xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:element name="fo:block">
								i.e., MARKER % per annum reckoned six-monthly and not in advance.
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ==========================  Page 2 Item 2  ==========================   -->
	<xsl:template name="Page_2_Item_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>2. </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Repayment shall be made by means of regular, equal, consecutive payments of principal and interest in the amount of <fo:inline font-weight="bold">
								<xsl:value-of select="//Deal/PandiPaymentAmount"/>
							</fo:inline> each, commencing on 
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId='0'">
									<fo:inline font-weight="bold"> 30ème jour suivant le déboursé du prêt </fo:inline> and thereafter on the <fo:inline font-weight="bold"> même  jour </fo:inline> of each <fo:inline font-weight="bold"> mois.</fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId='2'">
									<fo:inline font-weight="bold"> 14 jours suivant le déboursé du prêt </fo:inline> et les autres successivement le <fo:inline font-weight="bold"> même  jour </fo:inline> de chaque <fo:inline font-weight="bold"> deux semaines.</fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId='4'"> 
									<fo:inline font-weight="bold"> 7 ème jour suivant le déboursé du prêt </fo:inline> et les autres successivement le <fo:inline font-weight="bold"> même  jour </fo:inline> de chaque <fo:inline font-weight="bold"> semaine.</fo:inline><!-- #DG166  -->
								</xsl:when>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_2a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> a)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							up to and including MARKER , at which date any remaining balance shall become due; 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_2b">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> b)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							until the expiry of a term of MARKER commencing on the date of the signing of the deed of hypothec, any remaining balance becoming due at the expiry of that term;
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_2b_after">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">If the loan is secured by &#160;&#160;&#160;&#160;&#160; </fo:inline>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId='1'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
									<fo:inline font-weight="bold">&#160; the Canada Mortgage and Housing Corporation &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/mortgageInsurerId='2'">
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; the Canada Mortgage and Housing Corporation &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; the Canada Mortgage and Housing Corporation &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_2c">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId= '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/mortgageInsurerId= '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> c)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						until the expiry of a term of <fo:inline font-weight="bold">
								<xsl:value-of select="//Deal/actualPaymentTerm"/> month </fo:inline>
						commencing on the date of adjustment of interest, namely <fo:inline font-weight="bold"> MARKER </fo:inline>, any remaining balance becoming due at the expiry of that term.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_2c_after">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.3cm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						The amount of the payments is based on an amortization period of
						<fo:inline font-weight="bold">
								<xsl:value-of select="//specialRequirementTags/amortizationTermProcessed/termNumber"> </xsl:value-of>
								<xsl:choose>
									<xsl:when test="//specialRequirementTags/amortizationTermProcessed/termMeasure='MONTHS'"> months. </xsl:when>
									<xsl:when test="//specialRequirementTags/amortizationTermProcessed/termMeasure='YEARS'"> year(s). </xsl:when>
								</xsl:choose>
							</fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ==========================  Page 2 Item 3  ==========================   -->
	<xsl:template name="Page_2_Item_3">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>3.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="UnCheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						A line of credit for <fo:inline font-weight="bold">$ 0 </fo:inline> will also be granted to you for the purpose of the "Owner Protection" clause.	
				</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_4">
 		<fo:table  xsl:use-attribute-sets="TableLeftFixed">

		<!--#DG140 xsl:element name="fo:table">
			<would not work like this !! xsl:copy use-attribute-sets="TableLeftFixed"/-->
			
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
						<!--#DG140 xsl:element name="fo:block">
							<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
							<xsl:text>4.</xsl:text>
						</fo:block><!--#DG140 -->
					</xsl:element>
					<xsl:element name="fo:table-cell">
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
						<!--#DG140 xsl:element name="fo:block">
							<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
							<xsl:text>Notwithstanding the foregoing terms and conditions which shall appear in the deed of hypothec, if you opt for </xsl:text>
						</fo:block><!--#DG140 -->
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</fo:table><!--#DG140 -->
	</xsl:template>

	<xsl:template name="Page_2_Item_4a">
 		<fo:table  xsl:use-attribute-sets="TableLeftFixed">

		<!--#DG140 xsl:element name="fo:table">
			<would not work like this !! xsl:copy use-attribute-sets="TableLeftFixed"/-->
			
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="(./LDInsuranceTypeId = '1' and ./lifeStatusId = '1') or (./LDInsuranceTypeId = '2' and ./disabilityStatusId = '2')">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> life insurance only, </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="./LDInsuranceTypeId = '2' and ./disabilityStatusId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> life and disability insurance, </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</fo:table><!--#DG140 -->
	</xsl:template>

	<xsl:template name="Page_2_Item_4b">
 		<fo:table  xsl:use-attribute-sets="TableLeftFixed">

		<!--#DG140 xsl:element name="fo:table">
			<would not work like this !! xsl:copy use-attribute-sets="TableLeftFixed"/-->
			
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
						your regular payments will be 
						<fo:inline font-weight="bold">
								<xsl:value-of select="//Deal/pmntPlusLifeDisability"/>
							</fo:inline>
						in order to include the premium for such insurance, which corresponds to an additional interest rate of
						<fo:inline font-weight="bold">&#160;<xsl:value-of select="//Deal/interestRateIncrement"/>&#160;</fo:inline>
						per annum, subject to the provisions of the insurance policy in force at the Caisse.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</fo:table><!--#DG140 -->
	</xsl:template>
	<xsl:template name="Page_2_Item_5">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>5.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> Prepayments on the loan may be made   &#160;&#160;&#160;  </xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/prePaymentOptionsId= '4'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> without paying a penalty &#160;&#160;</xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/interestTypeId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/interestTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/interestTypeId = '5'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> upon payment of a penalty  </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						 if the prepaid amounts exceed <fo:inline font-weight="bold">  15.0%  </fo:inline> of the initial amount of the loan in accordance with the deed of hypothec.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_6">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>6.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
						The loan and the line of credit, if applicable, shall be secured by a hypothec of 
						<xsl:choose>
								<xsl:when test="//Deal/lienPositionId='0'">
									<fo:inline font-weight="bold"> 1</fo:inline>
									<fo:inline font-weight="bold" font-size="8" baseline-shift="super">st </fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline font-weight="bold"> 2</fo:inline>
									<fo:inline font-weight="bold" font-size="8" baseline-shift="super">nd </fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						 rank on the Property located at the following address:
				</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_6a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">170mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:attribute name="padding-left">5px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:apply-templates select="//DealAdditional/Property"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_7">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>7.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>In addition to the terms and conditions specified overleaf, which shall continue to apply notwithstanding the signing of the deed of hypothec, the following shall apply: </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_7a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">5px</xsl:attribute>
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold"> See Appendix </fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_8">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>8.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> The deed of hypothec must be signed within 150 days of the date hereof. After that period, the Caisse reserves the right to change theinterest rate or other terms and conditions, in which case it will send you a new Offer to Finance, or it may decide not to grant the loan.</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_Ending">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>A copy hereof has been sent to the notary, Mtre </xsl:text>
							<xsl:choose>
								<xsl:when test="//DealAdditional/solicitorData/partyProfile/Contact/contactLastName">
									<xsl:value-of select="//DealAdditional/solicitorData/partyProfile/Contact/contactFirstName"/>&#160;<xsl:value-of select="//DealAdditional/solicitorData/partyProfile/Contact/contactLastName"/>.
								</xsl:when>
								<xsl:otherwise> to be determined</xsl:otherwise>
							</xsl:choose>
							<xsl:text>.   If you require further information or if you no longer wish to obtain this loan, please contact us without delay. </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">14pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<!--#DG316 add middle name 
							<xsl:value-of select="//Deal/underwriter/Contact/contactFirstName"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/underwriter/Contact/contactLastName"/-->
							<xsl:for-each select="/*/Deal/underwriter/Contact">
								<xsl:value-of select="contactFirstName"/>
								<xsl:text> </xsl:text>							
								<xsl:if test="contactMiddleInitial">
									<xsl:value-of select="contactMiddleInitial"/>
									<xsl:text> </xsl:text>
								</xsl:if>							
								<xsl:value-of select="contactLastName"/>
							</xsl:for-each>
							
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>For a hypothecary loan of other than first rank subject to the Consumer Protection Act, a copy hereof in notarial form must be appended to the contract</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_Ending_a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4.0cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">13.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>Attachments: </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
									<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
									<xsl:attribute name="content-height">100%</xsl:attribute>
									<xsl:attribute name="content-width">100%</xsl:attribute>
								</xsl:element> -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
									<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
									<xsl:attribute name="content-height">100%</xsl:attribute>
									<xsl:attribute name="content-width">100%</xsl:attribute>
								</xsl:element> -->
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Borrower's Statement: Legal Construction Hypothecs </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Consent to Priority of Hypothec: Legal Construction Hypothecs </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!--<xsl:template name="CreatePageTwo">
	<xsl:element name="fo:page-sequence">
		<xsl:attribute name="master-reference">main</xsl:attribute>
		<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
					<xsl:call-template name="CreatePageTwoTopLine"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoTable"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoCCLine"></xsl:call-template>
			</xsl:element>
	</xsl:element>
</xsl:template> -->
	<xsl:template name="CreatePageTwoCCLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:value-of select="concat('cc:    ', //Deal/sourceOfBusinessProfile/Contact/contactFirstName, '   ', //Deal/sourceOfBusinessProfile/Contact/contactLastName , '  représentant(e) hypothécaire ' )"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2em</xsl:attribute>
							<xsl:attribute name="space-before">2em</xsl:attribute>
							<xsl:value-of select="//General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='0']">
			<xsl:element name="fo:block">
				<xsl:call-template name="BorrowerFullName"/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<xsl:template name="BorrAddress">
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/city,' ',./Address/province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/postalFSA,'  ', ./Address/postalLDU)"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PrimBorrAddress">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:if test="position()=1">
				<xsl:for-each select="BorrowerAddress">
					<xsl:if test="borrowerAddressTypeId='0'">
						<xsl:call-template name="BorrAddress"/>
						<!-- #DG140 text>some text</text-->
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- ============================================================================================================================ -->
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageTwo"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
