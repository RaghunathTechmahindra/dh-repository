<!-- prepared by: Catherine Rugaizer -->
<!-- Legal size paper -->
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' >  
<xsl:output method="text"/>

<xsl:template match="/">
	<xsl:call-template name="RTFFileStart5"/>
	<xsl:call-template name="DocumentHeader5"/>
	<xsl:call-template name="FaxTo"/>
	<xsl:call-template name="LenderRef"/>
	<xsl:call-template name="lastCent"/>
	<xsl:call-template name="RTFFileEnd5"/> 	
</xsl:template>  
	
<!-- ************************************************************************ -->
<!-- templates section                                                                                -->
<!-- ************************************************************************ -->

<xsl:template name="DocumentHeader5">

<!-- ====== #727 by Catherine ====== -->
  <xsl:if test="//specialRequirementTags/isAmended = 'Y'">
          <xsl:text>{\f1\fs20 AMENDED SOLICITOR INSTRUCTIONS AS OF </xsl:text>  <xsl:value-of select="//General/CurrentDate"/>
          <xsl:text>\par \par }</xsl:text>
  </xsl:if>
<!-- ====== #727 by Catherine end ====== -->

<xsl:text>{\pard\sa240
\brdrt \brdrs \brdrw10 \brsp10
\brdrl \brdrs \brdrw10 \brsp10
\brdrb \brdrs \brdrw10 \brsp10
\brdrr \brdrs \brdrw10 \brsp10
\qc\f1\fs20 \line\b\ul {\fs28CONFIRMATION OF CLOSING} \line
\par}</xsl:text>

</xsl:template>

<!-- ================================================  -->
<xsl:template name="FaxTo">

<!-- row 1 -->
<xsl:call-template name="FaxTo_row_type1"/>
<xsl:text>{\f1\fs20\b {FAX TO:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {</xsl:text>
<!-- #554
	<xsl:call-template name="FormatPhone">
		<xsl:with-param name="pnum" select="//specialRequirementTags/currentUser/Contact/contactFaxNumber"/>
	</xsl:call-template>	
#554 end -->	
<xsl:text>(416) 861-8484</xsl:text>
<xsl:text>}}</xsl:text> <!-- <xsl:value-of select="//Deal/BranchProfile/Contact/contactFaxNumber"/>-->
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {FROM:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text><xsl:call-template name="SolicitorName"/><xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>
	
<!-- row 2 -->
<xsl:call-template name="FaxTo_row_type1"/>
<xsl:text>{\f1\fs20\b {ATTENTION:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b </xsl:text>
<xsl:text>Originations </xsl:text>
<!-- #554 <xsl:call-template name="CurrentUserName"/> -->
<xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text><xsl:call-template name="SolicitorAddress"/><xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 3 -->
<xsl:call-template name="FaxTo_row_type1"/>
<xsl:text>{\f1\fs20\b {DATE:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b </xsl:text><xsl:value-of select="//General/CurrentDate"/><xsl:text>}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:call-template name="SolicitorPhoneAndFax"/>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>
	
</xsl:template>

<!--  ============================= SolicitorNameAndAddress ====================================== -->
<xsl:template name="SolicitorName">
	<xsl:for-each select="//Deal/PartyProfile">
		<xsl:if test="./partyTypeId='50'">		
			<xsl:text>{</xsl:text>
				<xsl:value-of select="./Contact/contactFirstName"/><xsl:text> </xsl:text>
				<xsl:if test="./Contact/contactMiddleInitial"><xsl:value-of select="./Contact/contactMiddleInitial"/><xsl:text> </xsl:text></xsl:if>
				<xsl:value-of select="./Contact/contactLastName"/>
			<xsl:text>}</xsl:text>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<xsl:template name="SolicitorAddress">
	<xsl:for-each select="//Deal/PartyProfile">
		<xsl:if test="./partyTypeId='50'">		
			<xsl:text>{</xsl:text>
				<xsl:value-of select="./Contact/Addr/addressLine1"/><xsl:text>\line </xsl:text>
				<xsl:if test="./Contact/Addr/addressLine2"><xsl:value-of select="./Contact/Addr/addressLine2"/><xsl:text>\line </xsl:text></xsl:if>
				<xsl:value-of select="./Contact/Addr/city"/><xsl:text>, \line </xsl:text>
				<xsl:value-of select="./Contact/Addr/province"/><xsl:text>\line </xsl:text>
				<xsl:value-of select="concat(./Contact/Addr/postalFSA,' ', ./Contact/Addr/postalLDU)"/>
			<xsl:text>}</xsl:text>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="SolicitorPhoneAndFax">
	<xsl:for-each select="//Deal/PartyProfile">
		<xsl:if test="./partyTypeId='50'">		
			<xsl:text>{Phone: \tab </xsl:text>
				<xsl:call-template name="FormatPhone">
					<xsl:with-param name="pnum" select="./Contact/contactPhoneNumber"/>
				</xsl:call-template>	
				<xsl:if test="./Contact/contactPhoneNumberExtension and string-length(./Contact/contactPhoneNumberExtension)!=0">
					<xsl:text> Ext. </xsl:text><xsl:value-of select="./Contact/contactPhoneNumberExtension"/>
				</xsl:if>
				<xsl:text>\line </xsl:text>
				<xsl:if test="./Contact/contactFaxNumber">
					<xsl:text>Fax: \tab </xsl:text>
					<xsl:call-template name="FormatPhone">
						<xsl:with-param name="pnum" select="./Contact/contactFaxNumber"/>
					</xsl:call-template>	
				</xsl:if>
			<xsl:text>}</xsl:text>			
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
   <xsl:template name="BorrowersListedBlocks">
	<xsl:for-each select="//Deal/Borrower">
		<xsl:if test="./borrowerTypeId=0">
			<xsl:text>{</xsl:text>
			    <xsl:call-template name="BorrowerFullName"/>
			<xsl:text>}</xsl:text>
			<xsl:if test="position() !=last()">\line </xsl:if>
		</xsl:if>			
	</xsl:for-each>
   </xsl:template>   

<!-- ================================================  -->
 <xsl:template name="BorrowerFullName">
    	<xsl:if test="salutation"><xsl:value-of select="salutation"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="borrowerFirstName"/><xsl:text> </xsl:text>
	<xsl:if test="borrowerMiddleInitial"><xsl:value-of select="borrowerMiddleInitial"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="borrowerLastName"/>     
 </xsl:template>

<!-- ================================================  -->
 <!-- Subject property, English formatting of address, upper case -->
<xsl:template match="Property">
	<xsl:value-of select="propertyStreetNumber"/><xsl:text>\caps  </xsl:text><xsl:value-of select="propertyStreetName"/><xsl:text> </xsl:text>
	<xsl:if test="streetType"><xsl:text>\caps </xsl:text><xsl:value-of select="streetType"/><xsl:text> </xsl:text></xsl:if>
	<xsl:if test="streetDirection"><xsl:text>\caps </xsl:text><xsl:value-of select="streetDirection"/></xsl:if>
	<xsl:if test="unitNumber">
		<xsl:if test="string-length(unitNumber) > 0">
			<xsl:text>\caps , unit </xsl:text><xsl:value-of select="unitNumber"/>
		</xsl:if>	
	</xsl:if>
	<xsl:text>\line </xsl:text>
	<xsl:text>\caps </xsl:text><xsl:value-of select="concat(propertyCity,', ',province)"/><xsl:text>\line </xsl:text>
	<xsl:text>\caps </xsl:text><xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="LenderRef">
<xsl:call-template name="HorizLine"/>

<!-- row 1 -->
<xsl:call-template name="LenderRef_row"/>
<xsl:text>{\f1\fs20\b {LENDER REFERENCE NUMBER:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {</xsl:text><xsl:value-of select="//Deal/sourceApplicationId"/>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 2 -->
<xsl:call-template name="LenderRef_row"/>
<xsl:text>{\f1\fs20\b {MORTGAGOR(S):}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b \caps {</xsl:text><xsl:call-template name="BorrowersListedBlocks"/><xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 3 -->
<xsl:call-template name="LenderRef_row"/>
<xsl:text>{\f1\fs20\b {MUNICIPAL ADDRESS:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {</xsl:text> <xsl:apply-templates select="//Deal/Property"/><xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 4 -->
<xsl:call-template name="LenderRef_row"/>
<xsl:text>{\f1\fs20\b {MORTGAGE AMOUNT:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {</xsl:text><xsl:value-of select="//Deal/totalLoanAmount"/><xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 5 -->
<xsl:call-template name="LenderRef_row"/>
<xsl:text>{\f1\fs20\b {CLOSING DATE:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {</xsl:text><xsl:value-of select="//Deal/estimatedClosingDate"/><xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 6 -->
<xsl:call-template name="LenderRef_row"/>
<xsl:text>{\f1\fs20\b {MORTGAGE REGISTRATION NO:}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs20\b {</xsl:text>_________________________________________________________________ <xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>


<xsl:call-template name="HorizLine"/>
</xsl:template>

<!-- ================================================  -->


<!-- ================================================  -->
<xsl:template name="lastCent">

<xsl:call-template name="Prgr_Arial_10"/>
<xsl:text>\sa360 </xsl:text>
<xsl:text>We confirm that the above referenced transaction has closed.  Our Solicitor’s Final report will follow.</xsl:text>
<xsl:text>\par}</xsl:text>
	
<!-- Signature -->

<xsl:call-template name="Signature_row"/>
<xsl:text>{\f1\fs20    }</xsl:text>
<xsl:text>\cell \pard \intbl \qr </xsl:text> 
<xsl:text>{\f1\fs20\sb720 
 {______________________________________}}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="Signature_row"/>
<xsl:text>{\f1\fs20    }</xsl:text>
<xsl:text>\cell \pard \intbl \qr</xsl:text> 
<xsl:text>{\f1\fs20 Signature of Solicitor}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

</xsl:template>


<!-- ************************************************************************ -->
<!-- Row definitions 														                -->
<!-- ************************************************************************ -->
<!-- ================================================  -->
<xsl:template name="FaxTo_row_type1">
<xsl:text>{\trowd \trgaph55\trleft-55\ql
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10840 
\pard \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="FaxTo_row_type2">
<xsl:text>{\trowd \trgaph55\trleft-55
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5000 
\pard \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="LenderRef_row">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx4000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 3 columns -->
<xsl:template name="Chk_list_row">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \sa180 \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 3 columns, default between rows -->
<xsl:template name="Chk_list_row2">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="produce-empty-rows">
<xsl:param name="count"/>
<xsl:if test="$count != 0">
	<xsl:call-template name="Chk_list_empty_row"/>
	<xsl:call-template name="produce-empty-rows">
		<xsl:with-param name="count" select="$count - 1"/>
	</xsl:call-template>
</xsl:if>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="Chk_list_empty_row">
<xsl:call-template name="Chk_list_row"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \intbl {\f1\fs20 _________________________________________________________________________________________ }
</xsl:text>
<xsl:text>\cell \row }</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="FormatPhone">
	<xsl:param name="pnum"/>
	<xsl:if test="string-length($pnum)=10">
		<xsl:text>(</xsl:text>
		<xsl:value-of select="substring($pnum, 1, 3)"/>
		<xsl:text>) </xsl:text>
		<xsl:value-of select="substring($pnum, 4, 3)"/>
		<xsl:text>-</xsl:text>	
		<xsl:value-of select="substring($pnum, 7, 4)"/>
	</xsl:if>		
</xsl:template>

<!-- ================================================  -->
<xsl:template name="WeConfirm_line">
<xsl:call-template name="Chk_list_row"/>
<xsl:text>{\f1\fs20    }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 _________________________________________________________________________________________}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 3 columns -->
<xsl:template name="BranchName_row">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx640
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx3300
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \sa180 \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 2 columns -->
<xsl:template name="Signature_row">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6400
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11400 
\pard \intbl</xsl:text>
</xsl:template>
<!-- ================================================  -->

<!-- ================================================  -->
<xsl:template name="CurrentUserName">
	<xsl:for-each select="//specialRequirementTags/currentUser">
		<xsl:value-of select="concat(./Contact/contactFirstName,' ', ./Contact/contactLastName)"/>
	</xsl:for-each>
</xsl:template>

<!-- ************************************************************************ -->
<!-- Utility templates 														                -->
<!-- ************************************************************************ -->
<!-- ================================================  -->
<xsl:template name="HorizLine">
<xsl:text>{\pard\sa180\brdrb \brdrs \brdrw30 \brsp10 \par}</xsl:text>
</xsl:template>	

<!-- ================================================  -->
<xsl:template name="Prgr_Arial_10_bold">
<xsl:text>{\pard\sa180\f1\fs20\b
</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="Prgr_Arial_10">
<xsl:text>{\pard\sa180\f1\fs20
</xsl:text>
</xsl:template>

<!-- ************************************************************************ -->
<!-- rtf file start and rtf file end                                                                    -->
<!-- ************************************************************************ -->

<!-- ================================================  -->
<xsl:template name="RTFFileEnd5">  
	<xsl:text>}}</xsl:text>
</xsl:template>  

<!-- ================================================  -->
<!-- legal size  (216 × 356 mm  or  612pt  x 1008pt); margings: top=36pt, bottom =36pt, left=21.6pt, right=21.6pt-->
 <xsl:template name="RTFFileStart5">  
<xsl:text>
{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\info {\author Zivko Radulovic, Catherine Rutgaizer}{\operator filogix automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company filogix inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \paperw12240 \paperh20160 \margl432\margr432\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\footer \pard\plain \ql \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\field {\*\fldinst }
{\fldrslt {\lang1024\langfe1024\noproof 1}}}
\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }}
</xsl:text>		
</xsl:template>  
<!--
<xsl:template name="RTFFileStart5">  
<xsl:text>
\widowctrl\ftnbj\aenddoc\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \paperw12240 \paperh20160 \margl432\margr432\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\footer \pard\plain \ql \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\field {\*\fldinst }
{\fldrslt {\lang1024\langfe1024\noproof 1}}}
\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }
</xsl:text>		
</xsl:template>  
-->
	
</xsl:stylesheet>  
