<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
	25/Jul/2006 DVG #DG470 #3930  Concentra - Build Fixes 
	05/Jun/2006 DVG #DG432 #3348  Concentra - Incorrect Mortgage Fee & Amortization Period  
	31/May/2006 DVG #DG430 #3356  Concentra - Solicitor's Package Cosmetic Changes - minor  
  17/May/2006 DVG #DG422 #3231  Concentra - Commitment Letter Updates as a result of Template Changes
	24/Oct/2005 DVG #DG344 #2286  CR#132 Solicitors Package changes  
		#1670  Concentra - CR #158 (Solicitors package)  
		- total conversion from the xml to the xsl format
	-->
	<xsl:output method="text"/>	

	<xsl:variable name="lendName" select="translate(/*/CommitmentLetter/LenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendName,'.xsl'))" />
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />
	
	<xsl:template match="/">
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
		
		<xsl:call-template name="RTFFileStart"/>
		
		<!-- #DG732 rewritten -->
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageFrench">
				<!-- #DG732 same as Eng. for now - xsl:call-template name="ComitLeterMainFr"/-->
				<xsl:call-template name="EnglishCommitmentLetter"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="EnglishCommitmentLetter"/><!--#DG286 will be called from solicit.pkg too -->
      </xsl:otherwise>
		</xsl:choose>
		<!--#DG334 no need xsl:call-template name="RTFFileEnd"/-->
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<xsl:template name="EnglishCommitmentLetter">
		<xsl:call-template name="EnglishFooter"/>
		<xsl:call-template name="EnglishPage1"/>
		<xsl:call-template name="EnglishPage4"/>
		<xsl:call-template name="EnglishPage5"/>
	</xsl:template>
	
	<xsl:template name="EnglishPage1">
		<xsl:text>{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs28 MORTGAGE COMMITMENT\par }</xsl:text>
		<xsl:value-of select="$logoRtf"/>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\f1\fs18 \cell}{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>}\par{\f1\fs18 </xsl:text>
		
		<!--#DG422 xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line2">
		  <xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line2"/>
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Postal"/-->
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
      <xsl:call-template name="ContactAddress"/>
		</xsl:for-each>
		
		<xsl:text>\par\cell }
		
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\f1\fs18\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908\row }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard\ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\f1\fs18 \cell}{\f1\fs18 Tel: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\f1\fs18\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmrg\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth4934\cellx4826
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row }


\trowd \trgaph108\trleft-108
\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrnone\cellx500
\clbrdrt\brdrs\brdrw10\clbrdrl\brdrnone\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\cellx4826
\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\cellx7740
\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\cellx10908

\pard \intbl{\f1\fs18\b\ul TO:}\cell 

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>

		<!--#DG422 xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Postal"/-->
		<xsl:for-each select="/*/Deal/Borrower[primaryBorrowerFlag='Y']/BorrowerAddress[borrowerAddressTypeId=0]/Addr">
    		<xsl:call-template name="ContactAddress"/>
 		</xsl:for-each>
		
		<xsl:text>\cell }

\pard \intbl{\f1\fs18\b\ul Underwriter}\par {\f1\fs18  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Underwriter"/>
		<xsl:text> }
\par {\f1\fs18\b\ul Administrator }\par 
{\f1\fs18 </xsl:text>
		<xsl:for-each select="/*/Deal/UserProfile[userProfileId=/*/Deal/administratorId]/Contact">
		  <xsl:call-template name="ContactName"/>
		</xsl:for-each>
		<xsl:text>}\cell 
\pard \intbl{\f1\fs18\b\ul Date}\par 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CurrentDate"/>
		<xsl:text>}\par 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchCurrentTime"/>
		<xsl:text>}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108
\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrnone\cellx500
\clbrdrt\brdrs\brdrw10\clbrdrl\brdrnone\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\cellx4826
\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\cellx10908
\pard \intbl{\f1\fs18\b\ul C/O:}\cell 
\pard \intbl{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerFirmName"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerAgentName"/>
		<xsl:text>\par </xsl:text>
    		
		<!--#DG422 xsl:value-of select="/*/CommitmentLetter/BrokerAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:if test="/*/CommitmentLetter/BrokerAddress/Line2">
		  <xsl:value-of select="/*/CommitmentLetter/BrokerAddress/Line2"/>
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BrokerAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerAddress/Province"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerAddress/Postal"/-->
		<xsl:for-each select="/*/Deal/SourceOfBusinessProfile/Contact/Addr">
      <xsl:call-template name="ContactAddress"/>
		</xsl:for-each>
		
		<xsl:text>\par Tel.:</xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>
		</xsl:call-template>
		<xsl:if test="string-length(/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension)!=0">
			<xsl:text> Ext. </xsl:text>
			<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension"/>
		</xsl:if>

		<xsl:text>\par Fax: </xsl:text>
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="/*/Deal/SourceOfBusinessProfile/Contact/contactFaxNumber"/>
		</xsl:call-template>
		<xsl:text>\par}\cell 
\pard \intbl
{\f1\fs18\b\ul Broker Reference Number:}{\f1\fs18  </xsl:text>
		<xsl:value-of select="/*/Deal/sourceApplicationId"/>
		<xsl:text>}\par
{\f1\fs18\b\ul Lender Reference Number:}{\f1\fs18  </xsl:text>
		<xsl:value-of select="/*/Deal/dealId"/>
		<xsl:text>}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalt\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0{\par 
{\f1\fs24\b\i  We are pleased to confirm that your application for a Mortgage Loan has been approved under 
the following terms and conditions:\par}\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row }

\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGOR(S):\cell\b0 </xsl:text>
		<xsl:for-each select="/*/Deal/Borrower[borrowerTypeId=0]"><!--#DG422 no guarantors -->
			<xsl:call-template name="BorrowerFullName"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
{\b\f1\fs18 PURPOSE:\b0  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text>(s):}{\f1\fs18 \cell </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18\ul SECURITY ADDRESS: \cell LEGAL DESCRIPTION: \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<!--#DG422 xsl:for-each select="/*/CommitmentLetter/Properties/Property"-->
		<xsl:for-each select="/*/Deal/Property[primaryPropertyFlag='Y']">	
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tx4900\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 
</xsl:text>
		  <!--#DG422 xsl:value-of select="./AddressLine1"/>
			<xsl:text> \par 
</xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>\par 
</xsl:text>
			<xsl:value-of select="./PostalCode"/-->
      <xsl:call-template name="propAdr"/>			
			<xsl:text> \par 
\cell </xsl:text>
			<xsl:if test="legalLine1 != ''">
			  <xsl:value-of select="legalLine1"/>
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:if test="legalLine2 != ''">
			  <xsl:value-of select="legalLine2"/>
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:if test="legalLine3 != ''">
			  <xsl:value-of select="legalLine3"/>
				<xsl:text>\par </xsl:text>
			</xsl:if>
			
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row }

</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 COMMITMENT DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="/*/Deal/commitmentIssueDate"/>
		<xsl:text>
\par 
\par }

{\b\f1\fs18 INTEREST ADJUSTMENT DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/IADDate"/>
		<xsl:text>\cell }
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 COMMITMENT EXPIRY DATE:}
{\f1\fs18 
\par </xsl:text>
		<!--#DG422 xsl:value-of select="/*/CommitmentLetter/CommitmentExpiryDate"/-->
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text>
\par 
\par }
{\b\f1\fs18 FIRST PAYMENT DATE:}
{\f1\fs18 \par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
{\b\f1\fs18 ADVANCE DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text>
\par 
\par }
{\b\f1\fs18 MATURITY DATE:}
{\f1\fs18 \par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 LOAN TYPE:  \b0 </xsl:text>
    <!--#DG430 xsl:value-of select="/*/CommitmentLetter/Product"/-->
		<xsl:value-of select="/*/Deal/dealType"/>
  	<xsl:choose>
  		<xsl:when test="/*/Deal/mtgProdId = 16">
  		  <xsl:text> ARM</xsl:text>
  		</xsl:when>
  		<xsl:otherwise>
  		  <xsl:text> Fixed</xsl:text>
  		</xsl:otherwise>
  	</xsl:choose>
		
		<xsl:text>\cell }
{\b\f1\fs18 LTV:\b0 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 INTEREST RATE:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 MTG.INSUR.PREMIUM:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs18 TERM:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PaymentTerm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 TOTAL LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 AMORTIZATION PERIOD:}{\f1\fs18 \cell </xsl:text>
		<!--#DG432 xsl:value-of select="/*/CommitmentLetter/Amortization"/-->
		<xsl:call-template name="YearsAndMonths">
			<xsl:with-param name="amort" select="/*/Deal/effectiveAmortizationMonths"/>
		</xsl:call-template>
		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC PAYMENT AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TotalPayment"/>
		<xsl:text>\cell }
{\b\f1\fs18 PAYMENT FREQUENCY:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PaymentFrequency"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 EST.ANNUAL PROP.TAXES:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AnnualTaxes"/>
		<xsl:text>\cell }
{\b\f1\fs18 TAXES TO BE PAID BY:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TaxPayor"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\clbrdrt\brdrnone\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\cellx10908
\pard \intbl\par 
{\f1\fs18\b Per Annum, </xsl:text>
		<!--#DG432 xsl:value-of select="/*/CommitmentLetter/CompoundingFrequency"/-->
  	<xsl:choose>
  		<xsl:when test="/*/Deal/interestCompound=12">
  		  <xsl:text>monthly</xsl:text>
  		</xsl:when>
  		<xsl:otherwise>
  		  <xsl:text>semi-annually</xsl:text>
  		</xsl:otherwise>
  	</xsl:choose>		
 		
		<xsl:text>, not in advance \par}\cell 
\pard \intbl \row

\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 PST PAYABLE ON INSUR.PREMIUM:\cell }{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGE FEES:\cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell }{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>		
		
		<xsl:text>{\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalt\clbrdrt\brdrnone \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\cell\row }

\trowd\trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalt\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrnone
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908

\pard \qj \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\f1\fs18 OTHER TERMS: \par\par 

</xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition[conditionId=2001]">
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:text>\par\par </xsl:text>
			</xsl:for-each>
		</xsl:for-each>
		<!--<xsl:text>In this approval and any schedule(s) to this approval, 
you and your mean the Borrower, Co-Borrower and Guarantor, if any, and we, our and us mean 
Concentra Financial Services Association. \par\par 
All of our normal requirements and, if applicable, those of the mortgage insurer must be met. 
All costs, including legal, survey, mortgage insurance etc. are for the account of the applicant(s). 
The mortgage insurance premium (if applicable) will be added to the mortgage. Any fees specified 
herein may be deducted from the Mortgage advance. If for any reason the loan is not advanced, 
you agree to pay all application, legal, appraisal and survey costs incurred in this transaction. \par\par 
This Mortgage Commitment is subject to the details and terms outlined herein as well as the conditions 
described on the attached schedules. \par\par Thank you for choosing Concentra Financial Services Association for your financing. 
We look forward to hearing from you soon. \par
 (1) You will be required to obtain fire insurance coverage in an amount at least equal 
to the replacement cost. Concentra Financial Services Association, 333 3rd Ave N Saskatoon SK S7K 2M2, 
is to be shown as 1st Loss Payable, as Mortgagee. \par
 (2) You will be required to sign a financing disclosure statement in accordance with the consumer 
protection legislation of your province. 
This statement will be signed at your solicitor's office at the time the mortgage document is signed. \par
 (3) Payment of your Concentra Financial Mortgage is to be by way of Pre Authorized Payment Plan. 
You will sign a Pre Authorized Payment Plan enrollment agreement at the time the mortgage is signed 
and you will provide Concentra Financial with a "Void" cheque for the account you wish the mortgage 
payment withdrawn from. \par
 (4) Concentra Financial will use the law firm of your choice for preparation and registration of the 
mortgage. We reserve the right to disqualify certain law firms based on past performance. Please 
provide your Mortgage Representative with the name, address, phone and fax numbers of your solicitor. \par
 (5) By accepting this Concentra Financial Mortgage Commitment, you agree to and authorize the following: \par
 (a) You authorize and direct Concentra Financial to pay the mortgage proceeds to your solicitor. \par
 (b) You certify that you have reviewed the information presented to Concentra Financial in the Mortgage 
Application used to obtain this Mortgage Commitment. You further certify that the information contained in the 
Application is true and complete. Further, you consent to any credit investigation necessary for the approval 
of the mortgage by Concentra Financial. Concentra Financial reserves the right to withdraw this commitment 
if the information provided is found to be incomplete or false or if subsequent credit investigations reveal 
reportings that fail to meet our standards. \par
 (c) I/We authorize Concentra Financial to give to, obtain, verify, share and exchange credit and other 
 information about us with others, including credit bureaus, mortgage insurers and other persons with 
 whom we may have financial dealings as well as any other persons as may be permitted by law. \par 
 (6) All documentation, including offers to purchase (must include the full name(s) and address(s) 
of the vendor(s)), sale agreements for current properties, employment verification, etc., are to be 
fully complete, legible, and in a form acceptable to Concentra Financial.</xsl:text>-->
		<xsl:text>\par }\par\par

{\f1\fs18\b\ul TERMS AND CONDITIONS:\par \par }
{\f1\fs18\b\ul \par Rate Guarantee and Rate Adjustment Policies: \par }
{\f1\fs18 Your interest rate for the selected term will be automatically adjusted to the lowest rate 
applicable for this deal, in effect between the date of this commitment letter and the mortgage 
funding date to a maximum of 120 days. (A minimum mortgage amount may be required to obtain 
these rates.) \par\par Interest rates can be held up to 150 days with our CAPPED RATE Program 
(0.50% over the current rate for the term chosen). Ask your Mortgage Representative about our 
Capped Rate Program.\par \par }{\f1\fs18\b\ul Renewal:\par }
{\f1\fs18 At the sole discretion of </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>, the Mortgage may be renewed at maturity for any term, with or without a change 
in the interest rate payable under the Mortgage, by entering into one or more written agreements 
with the Mortgagor(s).\par }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0{\cell }
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
{\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalt\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\row }
\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
{\pard \par \page \par }</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishPage4">
		<xsl:text>\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvertalt\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0{\f1\fs18 \~ }\par
{\f1\fs18\b\ul Standard Conditions:}\cell\row</xsl:text>

    <!--#DG422 -->
    <!--#DG430 8,311,  # 21, 16, 12, 249, 224, 225, 226, 228, 292, 9, 312 NOT to appear -->
    <!-- taken from CommitmentBrokerConditions class 
    and :BASIS:scheduletextcoop:ENDBASIS:374 (refinance) - removed, only sol. pkg
    include only variable and electronic verbiage, except 212, 375, 376, 179, 373 
    -->

		<xsl:for-each select="/*/CommitmentLetter/BrokerConditions/Condition[
      (conditionId &gt;= 60 and conditionId &lt;= 62) 
      or conditionId = 65 or conditionId = 66 or conditionId = 71 or conditionId = 77
      or conditionId = 86 or conditionId = 87 or conditionId = 97 or conditionId = 99
      or (conditionId &gt;= 100 and conditionId &lt;= 153) 
      or (conditionId &gt;= 155 and conditionId &lt;= 157) 
      or conditionId = 159 or conditionId = 160
      or (conditionId &gt;= 163 and conditionId &lt;= 178) 
      or (conditionId &gt;= 181 and conditionId &lt;= 192) 
      or conditionId = 199
      or conditionId = 202 or conditionId = 203
      or conditionId = 205 or conditionId = 206 or conditionId = 207
      or conditionId = 210 or conditionId = 211      
      or (conditionId &gt;= 213 and conditionId &lt;= 217)
      or conditionId = 221 or conditionId = 222
      or conditionId = 323 or conditionId = 324
      or (conditionId &gt;= 326 and conditionId &lt;= 328)
      or conditionId = 331
      or conditionId = 333 or conditionId = 335 or conditionId = 358 
      or conditionId = 412
      or (conditionId &gt;= 417 and conditionId &lt;= 426) 
      or conditionId = 705
			]">		
		<!-- or the reverse test
    xsl:for-each select="/*/CommitmentLetter/BrokerConditions/Condition[not(
      (conditionId &gt;= 0 and conditionId &lt;= 6)
      or (conditionId &gt;= 8 and conditionId &lt;= 40)
      or (conditionId &gt;= 42 and conditionId &lt;= 48)
      or (conditionId &gt;= 50 and conditionId &lt;= 59)
      or conditionId = 64
      or (conditionId &gt;= 67 and conditionId &lt;= 70)
      or (conditionId &gt;= 72 and conditionId &lt;= 76)
      or (conditionId &gt;= 78 and conditionId &lt;= 85)
      or (conditionId &gt;= 88 and conditionId &lt;= 96)
      or conditionId = 98 
      or conditionId = 154 or conditionId = 158 or conditionId = 161 or conditionId = 162
      or conditionId = 180
      or (conditionId &gt;= 193 and conditionId &lt;= 198)
      or conditionId = 200 or conditionId = 201 or conditionId = 204 or conditionId = 208
      or conditionId = 209
      or (conditionId &gt;= 218 and conditionId &lt;= 220)
      or (conditionId &gt;= 223 and conditionId &lt;= 283)
      or conditionId = 292
      or (conditionId &gt;= 295 and conditionId &lt;= 322)
      or conditionId = 332 or conditionId = 334
      or (conditionId &gt;= 336 and conditionId &lt;= 357)
      or (conditionId &gt;= 359 and conditionId &lt;= 372)
      or (conditionId &gt;= 377 and conditionId &lt;= 403)
      or conditionId = 406
      or (conditionId &gt;= 408 and conditionId &lt;= 411)
      or (conditionId &gt;= 413 and conditionId &lt;= 416)
      or (conditionId &gt;= 427 and conditionId &lt;= 435)
      or (conditionId &gt;= 706 and conditionId &lt;= 714)
      or conditionId = 2000 or conditionId = 2001 or conditionId = 2005      
			)]"-->		

			<xsl:text>\trowd \trgaph108\trleft-108
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908

\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>. </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row </xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\cell\row
\pard\page\par </xsl:text>
    
    <xsl:text>\pard \ql \li0\ri0\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0
\trowd\trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 \trbrdrl\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrh\brdrs\brdrw10 \trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalt
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908

\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0

\pard \intbl{\f1\fs18\b\ul \par THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING\par\par }
{\f1\fs18 The following conditions must be met, and the requested documents 
must be received in form and content satisfactory to </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> no later than ten (10) days prior to the advance of the mortgage. 
Failure to do so may delay or void this commitment.\par }\cell\row </xsl:text>

    <!--#DG422 -->
    <!-- taken from "com.filogix.docprep.doctags.hide.conditions" list (CommitmentConditions class) 
      320, 713, 2000, 2001    -->
    <!--#DG470 conds. 16, 336 only appear conditionally, 325,329,330 not to appear here  -->
    <!--#DG732 REVERSED-cond. 23 is to appear at most once, remove here and add it alone at the end -->
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition[not(
      conditionId = 16 or conditionId = 336
      or conditionId = 325 or conditionId = 329 or conditionId = 330 
      or conditionId = 320 or conditionId = 713 or conditionId = 2000 or conditionId = 2001)]
      | /*/CommitmentLetter/BrokerConditions/Condition[conditionId = 16 or conditionId = 336]">
		
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908

\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>. </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
			  <xsl:text>\par
</xsl:text>
			</xsl:for-each>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
		</xsl:for-each>

    <xsl:text>\trowd \trgaph108\trleft-108
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10\cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b\ul Requests for any changes to the terms and conditions of the Commitment will only be considered 
if your request is received by us in writing at least 5 business days prior to the Advance Date.\par }
{\f1\fs18\b\ul \par ACCEPTANCE:\par \par }
{\f1\fs18 This Commitment shall be open for acceptance by you 
until 11:59 pm on }{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/Deal/returnDate"/>
		<xsl:text>}{\f1\fs18  \~\~ after which time, 
if not accepted, shall be considered null and void. Furthermore, the mortgage must be advanced by no 
later than }{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/Deal/estimatedClosingDate"/>
		<xsl:text>}{\f1\fs18  \~\~ at which time it 
expires.\par\par }
{\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright\rin0\lin0\tab\tab 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>}\par\par\tab 
{\f1\fs18 Authorized by:___________________________________________}\par\tab\tab\tab\tab\tab\tab\tab\tab 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Underwriter"/>
		<xsl:text>}\line\line\line
\ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0}\cell \pard \intbl \row
\pard \ql \widctlpar\faauto\itap0 
\pard \par 

\page 

</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishPage5">
		<xsl:text>\trowd\trkeep \trgaph108\trleft-108
\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b \par\par I / We </xsl:text>
		<xsl:for-each select="/*/Deal/Borrower">
			<xsl:call-template name="BorrowerFullName"/>
			<xsl:if test="position() !=last()">, </xsl:if>
		</xsl:for-each>
		<xsl:text> warrant that all representations made 
by me/us and all the information submitted to you in connection with my/our mortgage application 
are true and accurate. I / We fully understand that any misrepresentations of fact contained in 
my/our mortgage application or other documentation entitles you to decline to advance a portion 
or all of the loan proceeds, or to demand immediate repayment of all monies secured by the 
Mortgage.\par\par\par }
{\f1\fs18\b I / We, the undersigned applicants accept the terms of this Mortgage Commitment 
as stated above and agree to fulfill the conditions of approval outlined herein.}\par\par
{\f1\fs18\b </xsl:text>

    <!-- :BASIS:PrivacyClause:ENDBASIS::BASIS:PrivacyClauseEnding:ENDBASIS: \par-->
		<xsl:for-each select="/*/CommitmentLetter/BrokerConditions/Condition[conditionId = 375]/Line">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="/*/CommitmentLetter/BrokerConditions/Condition[conditionId = 376]/Line">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>		
		
		<xsl:text>}
{\tx100\tx4000\tx7800\aspalpha\aspnum\faauto\adjustright\rin0\lin0\tab
{\f1\fs18\b ___________________________}\tab {\f1\fs18\b ___________________________ } \tab 
{\f1\fs18\b ___________________________}\par
\tab{\f1\fs18\b WITNESS }\tab {\f1\fs18\b APPLICANT } \tab {\f1\fs18\b DATE}\par\par\par
\tab{\f1\fs18\b ___________________________}\tab {\f1\fs18\b ___________________________ } \tab 
{\f1\fs18\b ___________________________}\par\tab{\f1\fs18\b WITNESS }\tab 
{\f1\fs18\b CO-APPLICANT } \tab {\f1\fs18\b DATE}\par\par\par
\tab{\f1\fs18\b ___________________________}\tab {\f1\fs18\b ___________________________ } \tab 
{\f1\fs18\b ___________________________}\par\tab{\f1\fs18\b WITNESS }\tab 
{\f1\fs18\b </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text> } \tab {\f1\fs18\b DATE}\par\par\par
\ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0}\cell \row

</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishFooter">
		<xsl:text>{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }{\f1\fs16 of }
{\field{\*\fldinst {NUMPAGES }}}}}</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->

	<xsl:template name="ComitLeterMainFr">
	 <!-- #DG732 if needed it can be copied/adapted from genx version -->
	 <xsl:text>\pard\f1\fs32\adjustmiddle\b FRENCH NOT IMPLEMENTED\par please contact Filogix for further information\b0 </xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="FormatPhone">
		<xsl:param name="pnum"/>
		<xsl:if test="string-length($pnum)=10">
			<xsl:text>(</xsl:text>
			<xsl:value-of select="substring($pnum, 1, 3)"/>
			<xsl:text>) </xsl:text>
			<xsl:value-of select="substring($pnum, 4, 3)"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="substring($pnum, 7, 4)"/>
		</xsl:if>
	</xsl:template>
	<!-- 111-222333444 -->

	<!-- ================================================  -->
	<xsl:template name="ContactName">
		<xsl:value-of select="contactFirstName"/>
		<xsl:text> </xsl:text>							
		<xsl:if test="contactMiddleInitial != ''">
			<xsl:value-of select="contactMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>							
		<xsl:value-of select="contactLastName"/>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<!--xsl:if test="borrowerMiddleInitial"-->

		<xsl:if test="string-length(borrowerMiddleInitial)!=0">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

  <!--#DG422 -->
	<!-- ================================================  -->
	<xsl:template name="propAdrs">	
		<xsl:param name="separaPostal" select="' '"/>
		<xsl:for-each select="/*/Deal/Property[primaryPropertyFlag='Y']">	
      <xsl:call-template name="propAdr">
	      <xsl:with-param name="separaPostal" select="$separaPostal"/>
      </xsl:call-template>
		</xsl:for-each>      
	</xsl:template>

	<xsl:template name="propAdr">	
		<xsl:param name="separaPostal" select="' '"/>
  	<xsl:text>\caps </xsl:text>
    <xsl:if test="unitNumber != ''">
    	<xsl:value-of select="unitNumber"/>
    	<xsl:text> - </xsl:text>
    </xsl:if>
    <xsl:value-of select="propertyStreetNumber"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="propertyStreetName"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="streetType"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="streetDirection"/>
    <xsl:text>\par </xsl:text>
    <xsl:if test="propertyAddressLine2 != ''">
    	<xsl:value-of select="propertyAddressLine2"/>
    	<xsl:text>\par </xsl:text>
    </xsl:if>
    <xsl:value-of select="propertyCity"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="province"/>
    <xsl:value-of select="$separaPostal"/>
    <xsl:value-of select="propertyPostalFSA"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="propertyPostalLDU"/>
    <xsl:text>\caps0 </xsl:text>
	</xsl:template>

	<!--  ============================= contact Address ====================================== -->
	<xsl:template name="ContactAddress">
		<xsl:param name="separa" select="'\line '"/>
		<xsl:param name="separaPostal" select="' '"/>
    <xsl:value-of select="addressLine1"/>
    <xsl:value-of select="$separa"/>
    <xsl:if test="addressLine2 != ''">
    	<xsl:value-of select="addressLine2"/>
      <xsl:value-of select="$separa"/>
    </xsl:if>
    <xsl:value-of select="city"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="province"/>
    <xsl:value-of select="$separaPostal"/>
    <xsl:value-of select="postalFSA"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="postalLDU"/>
	</xsl:template>

  <!--#DG432 -->
	<!--  ============================= contact Address ====================================== -->
	<xsl:template name="YearsAndMonths">
		<xsl:param name="amort" select="300"/>

    <xsl:value-of select="floor($amort div 12)"/>
    <xsl:text> years </xsl:text>
		<xsl:variable name="mth" select="$amort mod 12"/>    
    <xsl:value-of select="$mth"/>
    <xsl:text> months</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>}</xsl:text>
	</xsl:template>
			<!--{ \par }\pard \ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\par }} -->

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 
\deff0\deflang1033\deflangfe1033
{\fonttbl
  {\f0\froman\fcharset0\fprq2
    {\*\panose 02020603050405020304}Times New Roman;}
  {\f1\fswiss\fcharset0\fprq2
    {\*\panose 020b0604020202020204}Arial;}
}
{\colortbl;
  \red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;
  \red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;
  \red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;
  \red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;
}
{\stylesheet
{\ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}
{\*\cs10 \additive Default Paragraph Font;}}
{\info{\author Zivko Radulovic}
{\operator FILOGIX automatic report producer}
{\creatim\yr2000\mo3\dy10\hr00\min00}{\revtim\yr2000\mo3\dy00\hr00\min00}
{\version1}{\edmins1}{\nofpages1}{\nofwords0}
{\nofchars0}{\*\company FILOGIX Inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc
\dntblnsbdb\nospaceforul\formshade\horzdoc\dgmargin\dghspace180\dgvspace180
\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100
\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln
\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0
\sectd \linex0\endnhere
\sectlinegrid360
\sectdefaultcl\margl720\margr720\margt720\margb720\deftab720

{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
		
\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033{\footer 
\pard\plain\s15\ql \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0
\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid
\langnp1033\langfenp1033{\cs16 Page  }{\field {\*\fldinst {\cs16 PAGE  } 	}
{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}	}	}{\f1\fs16 of }{\field 
{\*\fldinst {\cs16 NUMPAGES  } }{\fldrslt {\cs16\lang1024\langfe1024\noproof 1}	}	}
{\cs16 \par }
\pard \s15\ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto
\adjustright\rin360\lin0\itap0{\par }}</xsl:text>
	</xsl:template>
	
</xsl:stylesheet>
