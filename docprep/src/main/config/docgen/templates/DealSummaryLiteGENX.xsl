<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//LanguageEnglish">
				<xsl:call-template name="EnglishHeader"/>
				<xsl:call-template name="EnglishFooter"/>
				<xsl:call-template name="EnglishLoanDetailsTable"/>
				<xsl:call-template name="EnglishDecisioningInfoTable"/>
              <!-- ***********************MCM Impl Team XS_16.24 starts --> 
                <xsl:call-template name="EnglishQualifyTitleTable"/>
                <xsl:call-template name="EnglishQualifyDetailsTable"/>  
                <xsl:if test="//ComponentExist = 'Y'">
                   <xsl:call-template name="EnglishComponentSummaryTable"/>
                   <xsl:call-template name="EnglishComponentDetailsTable" />
                </xsl:if>

              <!-- ***********************MCM Impl Team XS_16.24 ends -->
				<xsl:call-template name="EnglishBureauSummaryTable"/>
				<xsl:call-template name="EnglishBrokerNotesTable"/>
				<xsl:call-template name="EnglishPropertyTable"/>
				<xsl:call-template name="EnglishApplicantsTable"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//LanguageFrench">
				<xsl:call-template name="FrenchHeader"/>
				<xsl:call-template name="FrenchFooter"/>
				<xsl:call-template name="FrenchLoanDetailsTable"/>
				<xsl:call-template name="FrenchDecisioningInfoTable"/>
              <!-- ***********************MCM Impl Team XS_16.23 starts --> 
                <xsl:call-template name="FrenchQualifyTitleTable"/>
                <xsl:call-template name="FrenchQualifyDetailsTable"/>    
                <xsl:if test="//ComponentExist = 'Y'">
                   <xsl:call-template name="FrenchComponentSummaryTable"/>
                   <xsl:call-template name="FrenchComponentDetailsTable" />
                </xsl:if>
              <!-- ***********************MCM Impl Team XS_16.23 ends -->
				<xsl:call-template name="FrenchBureauSummaryTable"/>
				<xsl:call-template name="FrenchBrokerNotesTable"/>
				<xsl:call-template name="FrenchPropertyTable"/>
				<xsl:call-template name="FrenchApplicantsTable"/>
			</xsl:when>
		</xsl:choose>
		<xsl:if test="//CreditBureauReport">
			<xsl:call-template name="CreditBureauTable"/>
		</xsl:if>
		<xsl:if test="//BusinessRules">
			<xsl:call-template name="BusinessRulesTable"/>
		</xsl:if>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishLoanDetailsTable">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Details of Loan\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Loan Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//TotalLoanAmount"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Deal Purpose:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//DealPurpose"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 MI Premium:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//MIPremium"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Special Feature:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//SpecialFeature"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Purchase Price:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PurchasePrice"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Product"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 P and I:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PandIPayment"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Term:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PaymentTerm"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb \brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Escrow:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//TotalEscrowPayment"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Frequency:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PaymentFrequency"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Payment:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//TotalPayment"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amortization:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Amortization"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
{\f1\fs20 \cell \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Posted Rate:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PostedInterestRate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 \clftsWidth3\clwWidth2754 \cellx10908
\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Deal Status:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//DealStatus"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Buydown:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//BuydownRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Status Date:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//DealStatusDate"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Discount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Discount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 \clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Insurance Reference #:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//MIPolicyNumber"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Premium:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Premium"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 \clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Insurance Status:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//MIStatus"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Rate:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//NetRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 \clftsWidth3\clwWidth2754 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product Type:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//productType"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Charge:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Charge"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}</xsl:text>
<xsl:if test="//DownPayments">
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5513 \cellx5405
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5503 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Down Payment Source\cell }
{\b\f1\fs20 Down Payment Amount\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5513 \cellx5405
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5503\cellx10908
\row 
}
</xsl:text>
<xsl:for-each select="//DownPayments/DownPayment">
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5513 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5503 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./Source"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./Amount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5513 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5503 \cellx10908
\row 
}</xsl:text>
</xsl:for-each>
</xsl:if>
<xsl:text>\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishDecisioningInfoTable">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright
{\b\f1\fs20 Decisioning Information\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\f1\fs20 Credit Score:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CreditScore"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 LTV:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 GDS:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedGDS"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Income:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedTotalIncome"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 TDS:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedTDS"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Liabilities:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedTotalLiabilities"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 GDS (Qualifying Rate):\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Combined3YrGDS"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Assets:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedTotalAssets"/>
		<xsl:text>\cell 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 TDS (Qualifying Rate):\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Combined3YrTDS"/>
		<xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Qualifying Rate:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//QualifyingRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Qualifying Product:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//QualifyProduct"/>
		<xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
	</xsl:template>
 
 
<!-- ***********************MCM Impl Team XS_16.24 starts -->  
<xsl:template name="EnglishQualifyTitleTable">
<xsl:choose>
<xsl:when test="//QualifyTitle/QualifyBanner = 'NA'"> 
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908 
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Qualifying Details from Source of Business - Not Available \cell }

\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908 
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Qualifying Details from Source of Business \cell }



\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
 
 
<xsl:template name="EnglishQualifyDetailsTable">
  <xsl:for-each select="//QualifyDetail"> 
<xsl:text>
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyRate"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyPayment"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Compounding Period:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyPeriod"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 GDS:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyGDS"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amortization:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyAmortization"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 TDS:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyTDS"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Repayment Type:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyRepayment"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  </xsl:for-each>
  <xsl:text>
  \pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
</xsl:text>
</xsl:template>

 
<xsl:template name="EnglishComponentSummaryTable">
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Component Summary\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="//TotalAmountComp"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Cashback Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="//TotalCashBackAmount"/>
  <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Unallocated Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="//UnallocateAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
</xsl:text>
</xsl:template>


<xsl:template name="EnglishComponentDetailsTable">
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Component Details\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
  </xsl:text>

  <xsl:call-template name="EnglishMortgageTable"/> 
  <xsl:call-template name="EnglishLocTable"/> 
  <xsl:call-template name="EnglishLoanTable"/>
  <xsl:call-template name="EnglishCCTable"/>
  <xsl:call-template name="EnglishOverDraftTable"/>
     
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
</xsl:template>


<xsl:template name="EnglishMortgageTable">
  <xsl:for-each select="//Component/Mortgage"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Component Type:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ProductMortgage"/>
  <xsl:text>\cell }</xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Mortgage Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./MortgageAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Posted Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PostedRateMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 MI Premium:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./MiPremiumMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Discount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./DiscountMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Allocate MI Premium:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AllocateMiPremiumMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Premium:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PremiumMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Mortgage Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalLocAmountMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Buy Down Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./BuyDownRateMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amortization Period:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AmoritizationPeriodMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Net Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./NetRateMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Effective Amortization:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./EffectiveAmoritizationMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 P and I Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PIPaymentMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Term Description:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentTermMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Additional Principal Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalPaymentMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Actual Payment Term:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ActualPaymentTermMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Allocate Tax Escrow:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AllocateTaxEscrowMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Frequency:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentFreqMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Tax Escrow:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TaxEscrowMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prepayment Option:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PrePaymentOptionMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalPaymentMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Privilege Payment Option:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PriPaymentOptionMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Holdback Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./HoldBackAmountMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Commission Code:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CommissionCodeMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Rate Guarantee Period:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./RatePeriodMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Cashback Percentage:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CashBackMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Existing Account:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ExistingAccountMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Cashback Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CashBackAmountMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Existing Account Reference:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ExistingAccountRefMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Repayment Type:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./RepaymentTypeMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 First Payment Date:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./FirstPaymentDateMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Rate Locked In?:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./RateLockInMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Maturity Date:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./MaturityDateMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Additional Details:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsMort"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="EnglishLocTable">
  <xsl:for-each select="//Component/Loc"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Component Type:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ProductLoc"/>
  <xsl:text>\cell }</xsl:text> 
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 LOC Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./LocAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Posted Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PostedRateLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 MI Premium:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./MiPremiumLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Discount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./DiscountLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Allocate MI Premium:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AllocateMiPremiumLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Premium:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PremiumLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total LOC Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalLocAmountLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Net Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./NetRateLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Frequency:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentFrequencyLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Interest Only Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./InterestOnlyPaymentLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Commission Code:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CommissionCodeLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Allocate Tax Escrow:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AllocateTaxEscrowLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Repayment Type:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./RepaymentTypeLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Tax Escrow:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TaxEscrowLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Existing Account:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ExistingAccountLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalPaymentLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Existing Account Reference:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AccountRefLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Holdback Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./HoldbackAmountLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 First Payment Date:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./FirstPaymentDateLoc"/>
  <xsl:text>\cell }
{\cell }
{\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Additional Details:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsLoc"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="EnglishLoanTable">
  <xsl:for-each select="//Component/Loan"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Component Type:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeLoan"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ProductLoan"/>
  <xsl:text>\cell }</xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Loan Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./LoanAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Posted Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PostedRateLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Term Description:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentTermLoan"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Discount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./DiscountLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Actual Payment Term:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ActualPaymentTermLoan"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Premium:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PremiumLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Payment Frequency:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentFrequencyLoan"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Net Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./NetRateLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total Payment:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalPaymentLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Additional Details:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsLoan"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="EnglishCCTable">
<xsl:for-each select="//Component/CreditCard"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Component Type:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeCC"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeCC"/>
  <xsl:text>\cell }</xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Credit Card Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CreditCardAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Interest Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./InterestRateCC"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Additional Details:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsCC"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="EnglishOverDraftTable">
  <xsl:for-each select="//Component/OverDraft"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Component Type:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeOverDraft"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Product:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeOverDraft"/>
  <xsl:text>\cell }</xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Overdraft Amount:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./OverDraftAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Interest Rate:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./InterestRateOverDraft"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Additional Details:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsOverDraft"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="MiddleLine">
<xsl:text>
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone
\clbrdrr\brdrnone
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone
\cltxlrtb\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
</xsl:text>
</xsl:template>

<xsl:template name="EmptyLine">
<xsl:text>
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone  
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone  
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone  
\clbrdrl\brdrnone  
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone  
\cltxlrtb\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone  
\clbrdrl\brdrnone  
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone  
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone  
\clbrdrl\brdrnone  
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\cell
\cell
\cell
\cell
\row 
}
</xsl:text>
</xsl:template>
<!-- ***********************MCM Impl Team XS_16.24 ends -->  
 
<!-- ***********************MCM Impl Team XS_16.23a starts -->  
<xsl:template name="FrenchQualifyTitleTable">
<xsl:choose>
<xsl:when test="//QualifyTitle/QualifyBanner = 'NA'"> 
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908 
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Détails d'admissibilité obtenus de la source d'affaire - Non disponible \cell }

\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908 
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Détails d'admissibilité obtenus de la source d'affaire \cell }



\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:template>
 
 
<xsl:template name="FrenchQualifyDetailsTable">
  <xsl:for-each select="//QualifyDetail"> 
  <xsl:text>
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyRate"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyPayment"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Période de calcul de l’intérêt:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyPeriod"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 ABD:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyGDS"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amortissement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyAmortization"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 ATD:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyTDS"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de remboursement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./QualifyRepayment"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  </xsl:for-each>
  <xsl:text>
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
</xsl:text>
</xsl:template>

 
<xsl:template name="FrenchComponentSummaryTable">
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Sommaire de la composante\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant total:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="//TotalAmountComp"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant total de la remise en argent:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="//TotalCashBackAmount"/>
  <xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant non attribué:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="//UnallocateAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
</xsl:text>
</xsl:template>


<xsl:template name="FrenchComponentDetailsTable">
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Détails de la composante\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
  </xsl:text>

  <xsl:call-template name="FrenchMortgageTable"/> 
  <xsl:call-template name="FrenchLocTable"/> 
  <xsl:call-template name="FrenchLoanTable"/>
  <xsl:call-template name="FrenchCCTable"/>
  <xsl:call-template name="FrenchOverDraftTable"/>
     
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
</xsl:template>


<xsl:template name="FrenchMortgageTable">
  <xsl:for-each select="//Component/Mortgage"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de composante:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Produit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ProductMortgage"/>
  <xsl:text>\cell }</xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant de l’hypothèque:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./MortgageAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux affiché:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PostedRateMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prime d’assurance:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./MiPremiumMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Escompte:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./DiscountMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Attribuer la prime d’assurance:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AllocateMiPremiumMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prime:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PremiumMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant total de l’hypothèque:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalLocAmountMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux d’intérêt réduit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./BuyDownRateMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Période d’amortissement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AmoritizationPeriodMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux net:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./NetRateMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amortissement effectif:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./EffectiveAmoritizationMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiement de C &amp; I:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PIPaymentMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Description du terme du paiement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentTermMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiement supplémentaire sur le capital:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalPaymentMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Terme réel du remboursement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ActualPaymentTermMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Attribuer l'impôt foncier:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AllocateTaxEscrowMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Fréquence des paiements:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentFreqMortgage"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiements en main tierce pour l’impôt foncier:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TaxEscrowMortgage"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Option de remboursement anticipé:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PrePaymentOptionMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiement total:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalPaymentMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Option de paiement spécial:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PriPaymentOptionMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant de la retenue:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./HoldBackAmountMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Code de commissionnement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CommissionCodeMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Période de garantie du taux:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./RatePeriodMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Remise en argent(%):\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CashBackMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Compte existant:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ExistingAccountMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Remise en argent($):\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CashBackAmountMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Référence au compte existant:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ExistingAccountRefMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de remboursement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./RepaymentTypeMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date du premier paiement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./FirstPaymentDateMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux immobilisé?:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./RateLockInMort"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date d’échéance:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./MaturityDateMort"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Détails supplémentaires:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsMort"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="FrenchLocTable">
  <xsl:for-each select="//Component/Loc"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de composante:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Produit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ProductLoc"/>
  <xsl:text>\cell }</xsl:text> 
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant de la marge de crédit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./LocAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux affiché:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PostedRateLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prime d’assurance:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./MiPremiumLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Escompte:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./DiscountLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Attribuer la prime d’assurance:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AllocateMiPremiumLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prime:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PremiumLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant total de la marge de crédit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalLocAmountLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux net:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./NetRateLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Fréquence des paiements:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentFrequencyLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiement d’intérêts seulement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./InterestOnlyPaymentLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Code de commissionnement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CommissionCodeLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Attribuer l'impôt foncier:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AllocateTaxEscrowLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de remboursement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./RepaymentTypeLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiements en main tierce pour l’impôt foncier:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TaxEscrowLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Compte antérieur:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ExistingAccountLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiement total:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalPaymentLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Référence au compte existant:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AccountRefLoc"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant de la retenue:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./HoldbackAmountLoc"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date du premier paiement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./FirstPaymentDateLoc"/>
  <xsl:text>\cell }
{\cell }
{\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Détails supplémentaires:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsLoc"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="FrenchLoanTable">
  <xsl:for-each select="//Component/Loan"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de composante:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeLoan"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Produit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ProductLoan"/>
  <xsl:text>\cell }</xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant du prêt:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./LoanAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux affiché:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PostedRateLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Description du terme du paiement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentTermLoan"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Escompte:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./DiscountLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Terme réel du remboursement:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ActualPaymentTermLoan"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prime:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PremiumLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Fréquence des paiements:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./PaymentFrequencyLoan"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux net:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./NetRateLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiement total:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./TotalPaymentLoan"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Détails supplémentaires:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsLoan"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="FrenchCCTable">
<xsl:for-each select="//Component/CreditCard"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de composante:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeCC"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Produit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeCC"/>
  <xsl:text>\cell }</xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant de la carte de crédit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./CreditCardAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux d’intérêt:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./InterestRateCC"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Détails supplémentaires:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsCC"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<xsl:template name="FrenchOverDraftTable">
  <xsl:for-each select="//Component/OverDraft"> 
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de composante:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeOverDraft"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Produit:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./ComponentTypeOverDraft"/>
  <xsl:text>\cell }</xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant du découvert:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./OverDraftAmount"/>
  <xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux d’intérêt:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./InterestRateOverDraft"/>
  <xsl:text>\cell }
  </xsl:text>
  <xsl:call-template name="MiddleLine"/>
  <xsl:text>
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Détails supplémentaires:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
  <xsl:value-of select="./AdditionalDetailsOverDraft"/>
  <xsl:text>\cell }
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone  
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone  
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2754 \cellx10908
\row 
}
  </xsl:text>
  <xsl:call-template name="EmptyLine"/>
  </xsl:for-each>
</xsl:template>

<!-- ***********************MCM Impl Team XS_16.23a ends -->  
 
	<xsl:template name="EnglishBureauSummaryTable">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Bureau Summary\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>

<xsl:for-each select="//BureauSummary/Line">
<xsl:value-of select="."/>
<xsl:text>\par </xsl:text>
</xsl:for-each>

		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishBrokerNotesTable">
		<xsl:text>\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright
{\b\f1\fs20 Broker Notes\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908\row }
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth11016 \cellx10908\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs20 </xsl:text>
		<xsl:for-each select="//BrokerNotes/Note">
			<xsl:text>- </xsl:text>
			
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishPropertyTable">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Property\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1188 \cellx1080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2988 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Address:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//PropertyAddress/Line1"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//PropertyAddress/City"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//PropertyAddress/Province"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//PropertyPostalCode"/>
		<xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 New Construction?:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//NewConstruction"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1188 \cellx1080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2988 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1188 \cellx1080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2988 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Occupancy:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//OccupancyType"/><xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Structure Age:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PropertyStructureAge"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1188 \cellx1080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2988 \cellx10908
\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
</xsl:text>
	</xsl:template>
	
	<xsl:template name="EnglishApplicantsTable">
	<xsl:for-each select="//ApplicantDetails/Applicant">
		<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11030 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 </xsl:text><xsl:if test="./PrimaryBorrower='Y'"><xsl:text>Primary </xsl:text></xsl:if><xsl:text>Applicant\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11030 \cellx10915
\row 
}
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Name:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./Name"/><xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Income:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./TotalIncome"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Age:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./Age"/><xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Net Worth:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./NetWorth"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Marital Status:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./MaritalStatus"/><xsl:text>}
{\f1\fs20 \cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8755 \cellx10915
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Address:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./Address/Line1"/>
<xsl:text>{ }</xsl:text>
<xsl:value-of select="./Address/Line2"/>
<xsl:text>{ }</xsl:text>
<xsl:value-of select="./Address/City"/>
<xsl:text>{ }</xsl:text>
<xsl:value-of select="./Address/Province"/>
<xsl:text>{ }</xsl:text>
<xsl:value-of select="./Address/PostalCode"/>
<xsl:text> Phone: </xsl:text><xsl:value-of select="./Address/PhoneNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8755 \cellx10915
\row 
}</xsl:text>

<!--  ASSETS -->
<xsl:if test="./Assets">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6300 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Asset Type\cell Asset Description\cell Asset Value\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6300 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>

<xsl:for-each select="./Assets/Asset">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6300 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./AssetType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./AssetDescription"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./AssetValue"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6300 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:if test="./Liabilities">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth675 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Liability Type\cell CB\cell Liability Description\cell Liability Amount\cell Monthly Payment\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth675 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>

<xsl:for-each select="./Liabilities/Liability">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth675 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./LiabilityType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./CB"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityDescription"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityMonthlyPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth675 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:if test="./EmploymentHistory">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1958 \cellx4118
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2362 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Employer Name\cell Status\cell Job Title\cell Time at Job\cell Annual Income\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1958 \cellx4118
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2362 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>

<xsl:for-each select="./EmploymentHistory/Employment">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1958 \cellx4118
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2362 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./EmployerName"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./EmploymentStatus"/><xsl:text>\cell
</xsl:text><xsl:value-of select="./JobTitle"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./TimeAtJob"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./IncomeAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1958 \cellx4118
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2362 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>
</xsl:for-each>
</xsl:if>
<xsl:text>\pard \ql \widctlpar\faauto\itap0 
{\par }
</xsl:text>
</xsl:for-each>
	</xsl:template>

	<xsl:template name="CreditBureauTable">
		<xsl:text>\pard\par\sect\plain
{\f1\b </xsl:text>

<xsl:choose>
	<xsl:when test="//LanguageFrench"><xsl:text>Rapport de Bureau de crédit</xsl:text></xsl:when>
	<xsl:otherwise><xsl:text>Credit Bureau Report</xsl:text></xsl:otherwise>
</xsl:choose>

<xsl:text>:\par\par }
{\f1\fs20</xsl:text>
<xsl:for-each select="//CreditBureauReport/Line">
	<xsl:value-of select="."/>
	<xsl:text>\par </xsl:text>
</xsl:for-each>
<xsl:text>}</xsl:text>
	</xsl:template>

	<xsl:template name="BusinessRulesTable">
		<xsl:text>\sect\pard
{\f1\b </xsl:text>

<xsl:choose>
	<xsl:when test="//LanguageFrench"><xsl:text>Règles d’affaire</xsl:text></xsl:when>
	<xsl:otherwise><xsl:text>Business Rules</xsl:text></xsl:otherwise>
</xsl:choose>

<xsl:text>:\par}
{\f1\fs20 </xsl:text>
<xsl:for-each select="//BusinessRules/Rule">
<!--  Here we create a table to hold the business rule and critical indicator.  If the item
	doesn't have a critical flag, then we assume it to be a description and insert 
	it into a single column row. -->
		<xsl:choose>
			<xsl:when test = "@Critical = 'Y' or @Critical = 'N'" >
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\cltxlrtb\clftsWidth3\clwWidth10728 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\fs20 </xsl:text>
<xsl:choose>
<xsl:when test="@Critical = 'Y'">C</xsl:when>
<xsl:otherwise>-</xsl:otherwise>
</xsl:choose>
<xsl:text>\cell </xsl:text>
<xsl:value-of select="."/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\cltxlrtb\clftsWidth3\clwWidth10728 \cellx10908
\row 
}</xsl:text>
</xsl:when>
<xsl:otherwise>	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\fs20 </xsl:text><xsl:value-of select="."/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>
<xsl:text>}\par </xsl:text>
	</xsl:template>
	
	<xsl:template name="EnglishHeader">
		<xsl:text>{\header \pard\plain 
\qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 MTG#:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//ReferenceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20\ul Deal Summary \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Deal #:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//DealNum"/>
		<xsl:text>\cell }
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Page }{\field{\*\fldinst {\f1\fs20  PAGE }}{\fldrslt {\fs20\lang1024\langfe1024\noproof 1}}}{\f1\fs20  of }{\field{\*\fldinst {\f1\fs20  NUMPAGES }}{\fldrslt {\fs20\lang1024\langfe1024\noproof 1}}}{\f1\fs20 \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd 
\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clftsWidth3\clwWidth1908 \cellx1800\clftsWidth3\clwWidth2700 \cellx4500\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908\row }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Borrower:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//BorrowerName"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Ref. Source Application #:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
</xsl:text>
		<xsl:value-of select="//ReferenceSourceAppNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Closing Date:\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//EstClosingDate"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Source Information </xsl:text>
<xsl:if test="//Source2Name"><xsl:text>1 Name:\line
Source Information 2 </xsl:text>
</xsl:if>
<xsl:text>Name:
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//SourceName"/>
<xsl:if test="//Source2Name">
<xsl:text> \line </xsl:text>
<xsl:value-of select="//Source2Name"/>
</xsl:if>
<xsl:text>
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0
{\fs20\lang1024\langfe1024\noproof 
{\shp
{
\*\shpinst\shpleft0\shptop114\shpright10800\shpbottom114\shpfhdr1\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz1\shplid2054
{\sp{\sn shapeType}{\sv 20}}
{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}
{\sp{\sn fFillOK}{\sv 0}}
{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}
}
{
\shprslt
{\*\do\dobxcolumn\dobypara\dodhgt8193\dpline\dpptx0\dppty0\dpptx10800\dppty0\dpx0\dpy114\dpxsize10800\dpysize0\dplinew15\dplinecor0\dplinecog0\dplinecob0}
}}}
{\f1\fs20 \par }
}</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishFooter">
		<xsl:text>{\footer \pard\plain \qc \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\fs20\lang1024\langfe1024\noproof 
{\shp{\*\shpinst\shpleft0\shptop120\shpright10800\shpbottom120\shpfhdr1\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz0\shplid2049
{\sp{\sn shapeType}{\sv 20}}
{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}
{\sp{\sn fFillOK}{\sv 0}}
{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}}
{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8192\dpline\dpptx0\dppty0\dpptx10800\dppty0\dpx0\dpy120\dpxsize10800\dpysize0\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}}
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//LenderName"/>
		<xsl:text>
\par }
{\f1\fs16 </xsl:text>
		<xsl:value-of select="//BranchAddressComplete"/>
		<xsl:text>
\par Phone: </xsl:text>
		<xsl:value-of select="//BranchPhone"/>
		<xsl:text>   Fax: </xsl:text>
		<xsl:value-of select="//BranchFax"/>
		<xsl:text>
\par Toll-free Phone: </xsl:text>
		<xsl:value-of select="//BranchTollPhone"/>
		<xsl:text>   Toll-free Fax: </xsl:text>
		<xsl:value-of select="//BranchTollFax"/>
		<xsl:text>
}
{\f1\fs20 \par }
}
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- French template section                                             			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="FrenchHeader">
		<xsl:text>{\header \pard\plain 
\qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 N{\super o} d’hyp:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//ReferenceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20\ul Récapitulatif de la demande \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 N{\super o} de demande:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//DealNum"/>
		<xsl:text>\cell }
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Page }{\field{\*\fldinst {\f1\fs20  PAGE }}{\fldrslt {\fs20\lang1024\langfe1024\noproof 1}}}{\f1\fs20  de }{\field{\*\fldinst {\f1\fs20  NUMPAGES }}{\fldrslt {\fs20\lang1024\langfe1024\noproof 1}}}{\f1\fs20 \cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\trowd 
\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clftsWidth3\clwWidth1908 \cellx1800\clftsWidth3\clwWidth2700 \cellx4500\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908\row }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Emprunteur:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//BorrowerName"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 N{\super o} de réf. de la demande source:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
</xsl:text>
		<xsl:value-of select="//ReferenceSourceAppNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date de clôture:\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//EstClosingDate"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Source </xsl:text>
<xsl:if test="//Source2Name"><xsl:text>1:\line
Source 2</xsl:text></xsl:if><xsl:text>:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 
</xsl:text>
		<xsl:value-of select="//SourceName"/>
<xsl:if test="//Source2Name">
<xsl:text> \line </xsl:text>
<xsl:value-of select="//Source2Name"/>
</xsl:if>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clftsWidth3\clwWidth1908 \cellx1800
\clftsWidth3\clwWidth2700 \cellx4500
\clftsWidth3\clwWidth3654 \cellx8154
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0
{\fs20\lang1024\langfe1024\noproof 
{\shp
{
\*\shpinst\shpleft0\shptop114\shpright10800\shpbottom114\shpfhdr1\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz1\shplid2054
{\sp{\sn shapeType}{\sv 20}}
{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}
{\sp{\sn fFillOK}{\sv 0}}
{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}
}
{
\shprslt
{\*\do\dobxcolumn\dobypara\dodhgt8193\dpline\dpptx0\dppty0\dpptx10800\dppty0\dpx0\dpy114\dpxsize10800\dpysize0\dplinew15\dplinecor0\dplinecog0\dplinecob0}
}}}
{\f1\fs20 \par }
}</xsl:text>
	</xsl:template>

	<xsl:template name="FrenchFooter">
		<xsl:text>{\footer \pard\plain \qc \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 {\fs20\lang1024\langfe1024\noproof 
{\shp{\*\shpinst\shpleft0\shptop120\shpright10800\shpbottom120\shpfhdr1\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz0\shplid2049
{\sp{\sn shapeType}{\sv 20}}
{\sp{\sn fFlipH}{\sv 0}}
{\sp{\sn fFlipV}{\sv 0}}
{\sp{\sn shapePath}{\sv 4}}
{\sp{\sn fFillOK}{\sv 0}}
{\sp{\sn fFilled}{\sv 0}}
{\sp{\sn fArrowheadsOK}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}
{\sp{\sn fLayoutInCell}{\sv 1}}}
{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8192\dpline\dpptx0\dppty0\dpptx10800\dppty0\dpx0\dpy120\dpxsize10800\dpysize0\dplinew15\dplinecor0\dplinecog0\dplinecob0}}}}
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//LenderName"/>
		<xsl:text>
\par }
{\f1\fs16 </xsl:text>
		<xsl:value-of select="//BranchAddressComplete"/>
		<xsl:text>
\par Téléphone: </xsl:text>
		<xsl:value-of select="//BranchPhone"/>
		<xsl:text>   Télécopieur: </xsl:text>
		<xsl:value-of select="//BranchFax"/>
		<xsl:text>
\par Téléphone sans frais: </xsl:text>
		<xsl:value-of select="//BranchTollPhone"/>
		<xsl:text>   Télécopieur sans frais: </xsl:text>
		<xsl:value-of select="//BranchTollFax"/>
		<xsl:text>
}
{\f1\fs20 \par }
}
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 {</xsl:text>
	</xsl:template>

	<xsl:template name="FrenchLoanDetailsTable">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Détails du prêt\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Montant total du prêt:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//TotalLoanAmount"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 But de la demande:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//DealPurpose"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prime d’assurance:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//MIPremium"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Traitement spécial:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//SpecialFeature"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prix d’achat:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PurchasePrice"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Produit:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//Product"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 C et I:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PandIPayment"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Terme:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PaymentTerm"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb \brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total des fonds placés en main tierce:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//TotalEscrowPayment"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Fréquence des paiements:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PaymentFrequency"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Paiement total:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//TotalPayment"/><xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Amortissement:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//EffectiveAmortization"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
{\f1\fs20 \cell \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux affiché:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PostedInterestRate"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 \clftsWidth3\clwWidth2754 \cellx10908
\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Statut de la demande:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//DealStatus"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux d’intérêt réduit:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//BuydownRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Date du statut:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//DealStatusDate"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Escompte:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Discount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 \clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 N{\super o} de référence de l’assurance:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//MIPolicyNumber"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Prime:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Premium"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 \clftsWidth3\clwWidth2754 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Statut de l’assurance:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//MIStatus"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//NetRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 \clftsWidth3\clwWidth2754 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Type de produit:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//productType"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Charge:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Charge"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx2641
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2764 \cellx5405
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2749 \cellx8154
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2754 \cellx10908\row 
}</xsl:text>
<xsl:if test="//DownPayments">
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5513 \cellx5405
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5503 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Source de la mise de fonds\cell }
{\b\f1\fs20 Montant de la mise de fonds\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5513 \cellx5405
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth5503\cellx10908
\row 
}
</xsl:text>
<xsl:for-each select="//DownPayments/DownPayment">
<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5513 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5503 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./Source"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./Amount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5513 \cellx5405
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5503 \cellx10908
\row 
}</xsl:text>
</xsl:for-each>
</xsl:if>
<xsl:text>\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
	</xsl:template>
	
	<xsl:template name="FrenchDecisioningInfoTable">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright
{\b\f1\fs20 Information sur la prise de décision\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\f1\fs20 Cote de crédit:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CreditScore"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 RPV:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 ABD:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedGDS"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Revenu total:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedTotalIncome"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 ATD:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedTDS"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total du passif:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedTotalLiabilities"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 ABD (Taux Admissible):\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Combined3YrGDS"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Total de l’actif:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//CombinedTotalAssets"/>
		<xsl:text>\cell 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 ATD (Taux Admissible):\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//Combined3YrTDS"/>
		<xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Taux Admissible:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//QualifyingRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell 
}
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Produit Admissible:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//QualifyProduct"/>
		<xsl:text>\cell 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2615 \cellx2507
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth3111 \cellx5618
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\clftsWidth3\clwWidth2566 \cellx8184
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth2724 \cellx10908\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
	</xsl:template>
	
	<xsl:template name="FrenchBureauSummaryTable">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Récapitulatif du rapport du Bureau de crédit\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>

<xsl:for-each select="//BureauSummary/Line">
<xsl:value-of select="."/>
<xsl:text>\par </xsl:text>
</xsl:for-each>

		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
	</xsl:template>
	
	<xsl:template name="FrenchBrokerNotesTable">
		<xsl:text>\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright
{\b\f1\fs20 Notes du courtier\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright
{\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10
\trbrdrl\brdrs\brdrw10
\trbrdrb\brdrs\brdrw10
\trbrdrr\brdrs\brdrw10
\trbrdrh\brdrs\brdrw10
\trbrdrv\brdrs\brdrw10
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10
\clbrdrl\brdrs\brdrw10
\clbrdrb\brdrs\brdrw10
\clbrdrr\brdrs\brdrw10
\clcbpat16\clftsWidth3\clwWidth11016 \cellx10908\row }
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth11016 \cellx10908\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs20 </xsl:text>
		<xsl:for-each select="//BrokerNotes/Note">
			<xsl:text>- </xsl:text>
			
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clftsWidth3\clwWidth11016 \cellx10908\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
	</xsl:template>
	
	<xsl:template name="FrenchPropertyTable">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s1\ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\outlinelevel0\adjustright 
{\b\f1\fs20 Propriété\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1188 \cellx1080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2988 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Adresse:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:value-of select="//PropertyAddress/Line1"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//PropertyAddress/City"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//PropertyAddress/Province"/>
		<xsl:text>{ }</xsl:text>
		<xsl:value-of select="//PropertyPostalCode"/>
		<xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Nouvelle construction?:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//NewConstruction"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1188 \cellx1080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2988 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1188 \cellx1080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2988 \cellx10908
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Occupation:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//OccupancyType"/><xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Âge de la structure:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="//PropertyStructureAge"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1188 \cellx1080
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx5940
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx7920
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2988 \cellx10908
\row 
}
\pard \qc \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
</xsl:text>
	</xsl:template>
	
	<xsl:template name="FrenchApplicantsTable">
	<xsl:for-each select="//ApplicantDetails/Applicant">
		<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11030 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Demandeur </xsl:text><xsl:if test="./PrimaryBorrower='Y'"><xsl:text>principal </xsl:text></xsl:if><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth11030 \cellx10915
\row 
}
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Nom:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./Name"/><xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Revenu:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./TotalIncome"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Age:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./Age"/><xsl:text>\cell }
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Valeur nette:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./NetWorth"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 État civil:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./MaritalStatus"/><xsl:text>}
{\f1\fs20 \cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8755 \cellx10915
\pard \qr \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Adresse:\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./Address/Line1"/>
<xsl:text>{ }</xsl:text>
<xsl:value-of select="./Address/Line2"/>
<xsl:text>{ }</xsl:text>
<xsl:value-of select="./Address/City"/>
<xsl:text>{ }</xsl:text>
<xsl:value-of select="./Address/Province"/>
<xsl:text>{ }</xsl:text>
<xsl:value-of select="./Address/PostalCode"/>
<xsl:text> Téléphone: </xsl:text><xsl:value-of select="./Address/PhoneNumber"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth8755 \cellx10915
\row 
}</xsl:text>

<!--  ASSETS -->
<xsl:if test="./Assets">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6300 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type d’actif\cell Description de l’actif\cell Valeur de l’actif\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth6300 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>

<xsl:for-each select="./Assets/Asset">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6300 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./AssetType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./AssetDescription"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./AssetValue"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6300 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:if test="./Liabilities">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth675 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Type de passif\cell BC\cell Description du passif\cell Montant du passif\cell Paiement du passif\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth675 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>

<xsl:for-each select="./Liabilities/Liability">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth675 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./LiabilityType"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./CB"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityDescription"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityAmount"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./LiabilityMonthlyPayment"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth675 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>
</xsl:for-each>
</xsl:if>

<xsl:if test="./EmploymentHistory">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1958 \cellx4118
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2362 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Nom de l’employeur\cell Statut\cell Titre/Poste\cell Depuis combien de temps\cell Revenu annuel\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1958 \cellx4118
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2362 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\clcbpat16\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>

<xsl:for-each select="./EmploymentHistory/Employment">
<xsl:text>\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1958 \cellx4118
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2362 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text><xsl:value-of select="./EmployerName"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./EmploymentStatus"/><xsl:text>\cell
</xsl:text><xsl:value-of select="./JobTitle"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./TimeAtJob"/><xsl:text>\cell 
</xsl:text><xsl:value-of select="./IncomeAmount"/><xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph115\trleft-115\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl115\trpaddr115\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2275 \cellx2160
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1958 \cellx4118
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2362 \cellx6480
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth1980 \cellx8460
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2455 \cellx10915
\row 
}</xsl:text>
</xsl:for-each>
</xsl:if>
<xsl:text>\pard \ql \widctlpar\faauto\itap0 
{\par }
</xsl:text>
</xsl:for-each>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>\par }}</xsl:text>
	</xsl:template>
	<xsl:template name="RTFFileStart">
	 <!-- #DG670 -->
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff63\deflang1033\deflangfe1033
{\fonttbl
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
}
{\colortbl;
\red0\green0\blue0;
\red0\green0\blue255;
\red0\green255\blue255;
\red0\green255\blue0;
\red255\green0\blue255;
\red255\green0\blue0;
\red255\green255\blue0;
\red255\green255\blue255;
\red0\green0\blue128;
\red0\green128\blue128;
\red0\green128\blue0;
\red128\green0\blue128;
\red128\green0\blue0;
\red128\green128\blue0;
\red128\green128\blue128;
\red192\green192\blue192;
\red255\green255\blue255;
}
{\stylesheet
{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \snext0 Normal;}
{\s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 \sbasedon0 \snext0 heading 1;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext15 header;}
{\s16\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext16 footer;}
}
\margl720\margr720\margt720\margb720 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\headery540\footery480\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>
</xsl:stylesheet>
