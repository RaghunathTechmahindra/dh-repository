<?xml version="1.0" encoding="UTF-8"?>
<!--
19/Oct/2005 DVG #DG334 #2264  DJ docs - Change phone numbers in CR047, CR050, CR059, CR060, CR061  
06/Apr/2005 DVG #DG176 #1182  DJ documents - comma between city and province  -->
<!--09/Mar/2005 DVG #DG166 #1063  DJ - miscellaneous DocPrep fixes  -->
<!--22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik-->
<!-- created by Catherine Rutgaizer -->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- define attribute sets -->
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr">
		<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ObjectAttr" use-attribute-sets="MainFontAttr SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr" use-attribute-sets="SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr2">
		<xsl:attribute name="line-height">14pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="CouponTitle" use-attribute-sets="TitleFontAttr">
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="rowHeight">
		<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="BottomLine">
		<xsl:attribute name="border-bottom-style">solid</xsl:attribute>
		<xsl:attribute name="border-bottom-color">black</xsl:attribute>
		<xsl:attribute name="border-bottom-width">0.5px</xsl:attribute>
	</xsl:attribute-set>
	<!-- end attribute sets -->
	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
					<xsl:attribute name="text-align">center</xsl:attribute>
					Page <fo:page-number/> de <fo:page-number-citation ref-id="{generate-id(/)}"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateOriginBranchLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">170mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="SpaceBAOptimum"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">140mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TitleFontAttr"/>
							Avis de transfert d'un dossier de financement hypothécaire
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
							Le  <xsl:value-of select="//General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateOriginBranchLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">100mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:for-each select="//DealAdditional/originationBranch/partyProfile">
							<xsl:element name="fo:block">
								<xsl:if test="./Contact/salutation">
									<xsl:value-of select="./Contact/salutation"/>
									<xsl:text>  </xsl:text>
								</xsl:if>
								<xsl:value-of select="concat(./Contact/contactFirstName, '   ',./Contact/contactLastName)"/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:value-of select="partyName"/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:value-of select="./Contact/Address/addressLine1"/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:value-of select="./Contact/Address/addressLine2"/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:value-of select="./Contact/Address/city"/>
								<xsl:text>, </xsl:text><!--#DG176-->
								<xsl:value-of select="./Contact/Address/province"/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:value-of select="./Contact/Address/postalFSA"/>
								<xsl:text>  </xsl:text>
								<xsl:value-of select="./Contact/Address/postalLDU"/>
							</xsl:element>
						</xsl:for-each>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">22mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">130mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Objet :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
						Nom de l'emprunteur : 							
						<xsl:if test="//specialRequirementTags/OrderedBorrowers/Borrower/primaryBorrowerFlag='Y'">
								<xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerFirstName"/>
								<xsl:text> </xsl:text>
								<xsl:if test="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerMiddleInitial">
									<xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerMiddleInitial"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerLastName"/>
							</xsl:if>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='0']">
					<xsl:if test="position()=2">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:text> </xsl:text>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									Nom du 1er co-emprunteur : 
									<xsl:value-of select="./borrowerFirstName"/>
									<xsl:text> </xsl:text>
									<xsl:if test="./borrowerMiddleInitial">
										<xsl:value-of select="./borrowerMiddleInitial"/>
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:if>
					<xsl:if test="position()=3">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:text> </xsl:text>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									Nom du 2ème co-emprunteur : 
									<xsl:value-of select="./borrowerFirstName"/>
									<xsl:text> </xsl:text>
									<xsl:if test="./borrowerMiddleInitial">
										<xsl:value-of select="./borrowerMiddleInitial"/>
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:if>
					<xsl:if test="position()=4">
						<xsl:element name="fo:table-row">
							<xsl:element name="fo:table-cell">
								<xsl:text> </xsl:text>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									Nom du 3ème co-emprunteur : 
									<xsl:value-of select="./borrowerFirstName"/>
									<xsl:text> </xsl:text>
									<xsl:if test="./borrowerMiddleInitial">
										<xsl:value-of select="./borrowerMiddleInitial"/>
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:if>
				</xsl:for-each>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:text> </xsl:text>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
								Numéro de dossier : <xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			Monsieur (Madame),
	    </xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			Nous désirons vous informer que les services de financement au point de vente ont autorisé un prêt hypothécaire aux clients en objet. Comme ces clients souhaitent transiger à votre caisse populaire, nous vous expédions le dossier se rapportant à ce prêt. Ce dossier comprend notamment l'offre de financement que nous leur avons fait parvenir.		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			Nous vous invitons, s'il y a lieu, à entrer en contact avec ces derniers afin de poursuivre la transaction et de développer cette nouvelle relation d'affaires par l'offre de services complémentaires.
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			Veuillez transmettre la documentation au notaire dans les plus brefs délais s.v.p.
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			Dès que le prêt sera déboursé, modifié ou annulé, nous vous prions de nous en informer en complétant le coupon-réponse ci-dessous et en nous le retournant par télécopieur.
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			Nous vous remercions à l'avance de votre collaboration.
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateUnderwriterLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">30mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">100mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:copy use-attribute-sets="PaddingAll0mm"/>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Téléphone :	
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<!--#DG334 xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumber"/>
											ou 
											<!- - #DG166 xsl:choose> 
												<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> ou 1-888-281-4420 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<!- - <xsl:when test="//Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when> - ->
												<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> ou 1-800-728-2728 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text> </xsl:text>ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:otherwise>
											</xsl:choose-->514-376-7181 ou sans frais 1-877-376-7181
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Télécopieur :
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<!--#DG334 xsl:value-of select="//Deal/underwriter/Contact/contactFaxNumber"/>
											<xsl:choose>
												<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> ou 1-888-888-7777</xsl:when>
												<xsl:when test="//Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999</xsl:when>
												<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> ou 1-888-888-8888</xsl:when>
											</xsl:choose-->514-322-2364 ou sans frais 1-866-243-2364
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- PAGE 2 - Coupon  -->
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<xsl:template name="CreatePageTwo">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
					<xsl:attribute name="text-align">center</xsl:attribute>
					Page <fo:page-number/> de <fo:page-number-citation ref-id="{generate-id(/)}"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="BorrowerName"/>
				<xsl:call-template name="CreateCouponLines"/>
				<fo:block id="{generate-id(/)}" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="BorrowerName">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			Nom de l'emprunteur: 							
			<xsl:if test="//specialRequirementTags/OrderedBorrowers/Borrower/primaryBorrowerFlag='Y'">
				<xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerFirstName"/>
				<xsl:text> </xsl:text>
				<xsl:if test="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerMiddleInitial">
					<xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerMiddleInitial"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:value-of select="//specialRequirementTags/OrderedBorrowers/Borrower/borrowerLastName"/>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateCouponLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:attribute name="margin-left">2mm</xsl:attribute>
			<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">170mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="PaddingAll2mm"/>
							<xsl:copy use-attribute-sets="CouponTitle"/>
							COUPON RÉPONSE
						</xsl:element>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">170mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:call-template name="CouponDetailsTable"/>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CouponDetailsTable">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">75mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">60mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">10mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>Code d'identification :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>
							<xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">10mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>Numéro de folio :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">3mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>
							<xsl:copy use-attribute-sets="BottomLine"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">10mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>Montant déboursé :	</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">3mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>
							<xsl:copy use-attribute-sets="BottomLine"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">-13mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:element name="fo:inline">
								<xsl:attribute name="letter-spacing">10mm</xsl:attribute>,$</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">10mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>Date déboursé :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">3mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>
							<xsl:copy use-attribute-sets="BottomLine"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">10mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>Date d'annulation du dossier :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">3mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="rowHeight"/>
							<xsl:copy use-attribute-sets="BottomLine"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- FO Start -->
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">10mm</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">1in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">38mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">15mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
			<xsl:call-template name="CreatePageTwo"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
