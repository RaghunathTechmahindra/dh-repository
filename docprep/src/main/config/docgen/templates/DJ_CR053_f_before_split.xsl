<?xml version="1.0" encoding="UTF-8"?>
<!-- Author Zivko Radulovic -->
<!-- edited by Catherine Rutgaizer, March 2004 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xmlns="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>	
	
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBA1.5Optimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="MainFontAttrPg4">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainTextAttr_Page_2">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ListLabelIndent">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="padding-left">-3mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:template name="CreatePageOne">
		<xsl:element name="page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>	 
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateBorrowerLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreatePageTwo">
		<xsl:element name="page-sequence"><xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content"><xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainFontAttr"/><xsl:attribute name="text-align">center</xsl:attribute>
				<xsl:attribute name="space-before.optimum">4mm</xsl:attribute><xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					Page <fo:page-number/> de <fo:page-number-citation ref-id="page_2"/></xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLine_Page_2"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateHeader_Page_2"/>
				<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					<xsl:call-template name="Page_2_Item_1"/>
					<xsl:call-template name="Page_2_Item_1a"/>
					<xsl:call-template name="Page_2_Item_1b"/>
					<xsl:call-template name="Page_2_Item_1c"/>
					<xsl:call-template name="Page_2_Item_2"/>
					<xsl:call-template name="Page_2_Item_2a"/>
					<xsl:call-template name="Page_2_Item_2b"/>
					<xsl:call-template name="Page_2_Item_2b_after"/>
					<xsl:call-template name="Page_2_Item_2c"/>
					<xsl:call-template name="Page_2_Item_2c_after"/>
					<xsl:call-template name="Page_2_Item_3"/>
				</xsl:element>
				<!-- primary borrower -->
				<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
						<xsl:if test="position()=1"> 
							<xsl:call-template name="Page_2_Item_4"/>
							<xsl:call-template name="Page_2_Item_4a"/>
							<xsl:call-template name="Page_2_Item_4b"/>
						</xsl:if>	
					</xsl:for-each>
				</xsl:element>
				<xsl:call-template name="Page_2_Item_5"/>
				<!-- <xsl:call-template name="Page_2_Item_6"/> -->
				<xsl:call-template name="Page_2_Item_6"/>
				<xsl:call-template name="Page_2_Item_6a"/>
				<xsl:call-template name="Page_2_Item_7"/>
				<xsl:call-template name="Page_2_Item_7a"/>
				<xsl:call-template name="Page_2_Item_8"/>
				<xsl:call-template name="Page_2_Item_Ending"/>
				<xsl:call-template name="Page_2_Item_Ending_a"/>
				<fo:block id="page_2" line-height="0pt"/> 
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreatePageThree">
		<xsl:element name="page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content"><xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainFontAttr"/><xsl:attribute name="text-align">center</xsl:attribute>
				<xsl:attribute name="space-before.optimum">4mm</xsl:attribute><xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					<!-- Page <fo:page-number/> of <fo:page-number-citation ref-id="page_3"/></xsl:element> -->
					Page 1 de 1</xsl:element> 
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage3"/>
				<xsl:call-template name="Page_3_Item_1"/>
   				<fo:block id="page_3" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreatePageFour">
		<xsl:element name="page-sequence"><xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content"><xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainFontAttr"/><xsl:attribute name="text-align">center</xsl:attribute>
				<xsl:attribute name="space-before.optimum">4mm</xsl:attribute><xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					Page <fo:page-number/> de <fo:page-number-citation ref-id="page_4"/></xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage4"/>
				<xsl:call-template name="Page_4_Item_1"/>
				<xsl:call-template name="DealConditionsNotWaived"/>
   				<fo:block id="page_4" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="Page_4_Item_1">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> Nom du ou des emprunteurs :</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">2pt</xsl:attribute><xsl:attribute name="space-before">2pt</xsl:attribute>
									<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="./salutation"/>&#160;
									<xsl:value-of select="./borrowerFirstName"/>&#160;
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<fo:inline font-weight="bold">CONDITIONS :</fo:inline>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- START OF TABLE   MI Policy numbers-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">7cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">7cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="mortgageInsuranceCheckBox">
								<xsl:with-param name="parInsurer">1</xsl:with-param>
								<xsl:with-param name="parId"><xsl:value-of select="//Deal/mortgageInsurerId"></xsl:value-of></xsl:with-param>
								<xsl:with-param name="parStatus"><xsl:value-of select="//Deal/MIStatusId"></xsl:value-of></xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								Prêt assuré SCHL No. 
									<xsl:call-template name="mortgageInsurancePolicyNum">
										<xsl:with-param name="parInsurer">1</xsl:with-param>
										<xsl:with-param name="parId"><xsl:value-of select="//Deal/mortgageInsurerId"></xsl:value-of></xsl:with-param>
										<xsl:with-param name="parStatus"><xsl:value-of select="//Deal/MIStatusId"></xsl:value-of></xsl:with-param>
									</xsl:call-template>								
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="mortgageInsuranceCheckBox">
									<xsl:with-param name="parInsurer">2</xsl:with-param>
									<xsl:with-param name="parId"><xsl:value-of select="//Deal/mortgageInsurerId"></xsl:value-of></xsl:with-param>
									<xsl:with-param name="parStatus"><xsl:value-of select="//Deal/MIStatusId"></xsl:value-of></xsl:with-param>
								</xsl:call-template>
								 prêt assuré GE CAPITAL No.
									<xsl:call-template name="mortgageInsurancePolicyNum">
										<xsl:with-param name="parInsurer">2</xsl:with-param>
										<xsl:with-param name="parId"><xsl:value-of select="//Deal/mortgageInsurerId"></xsl:value-of></xsl:with-param>
										<xsl:with-param name="parStatus"><xsl:value-of select="//Deal/MIStatusId"></xsl:value-of></xsl:with-param>
									</xsl:call-template>								
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Prime $-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">6.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								Prime 
							<xsl:call-template name="mortgageInsurancePremiumAmount">
								<xsl:with-param name="parId"><xsl:value-of select="//Deal/mortgageInsurerId"></xsl:value-of></xsl:with-param>
								<xsl:with-param name="parStatus"><xsl:value-of select="//Deal/MIStatusId"></xsl:value-of></xsl:with-param>
							</xsl:call-template>								
						</xsl:element>
					</xsl:element>
					
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:choose>
								<xsl:when test="//Deal/MIUpfront= 'Y'">
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
							  incluse au prêt
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/MIUpfront= 'N'">
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 non incluse au prêt  
						</xsl:element>
				</xsl:element>	</xsl:element>	</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Taxe $-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								Taxe <xsl:value-of select="//Deal/MIPremiumPST"></xsl:value-of>
						</xsl:element>
					</xsl:element>					
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
							 Payable
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 chèque ci-joint  
						</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 au compte 
						</xsl:element>		
						<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 par le contracteur  
						</xsl:element>										
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE   Droit de souscription-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								Droit de souscription $ 
							</xsl:element>
					</xsl:element>
					
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 chèque ci-joint 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 au compte 
						</xsl:element>		
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 par le contracteur 
						</xsl:element>										
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 par la caisse  
						</xsl:element>						
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE    Frais d evaluation  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								Frais d'évaluation $  
							</xsl:element>
					</xsl:element>
					
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 chèque ci-joint
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 au compte 
						</xsl:element>		
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
								 par la caisse  
						</xsl:element>						
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE   La mise de fonds  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								La mise de fonds devra être déposée à la C.P. avant l'envoi des instructions au notaire.
							</xsl:element>
					</xsl:element>										
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE   Option multipro -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:choose>
							<xsl:when test="//Deal/multiProject"><xsl:call-template name="CheckedCheckbox"/></xsl:when>
							<xsl:otherwise><xsl:call-template name="UnCheckedCheckbox"/></xsl:otherwise>
						</xsl:choose>						
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Option Multiprojets 
					</xsl:element></xsl:element>
				</xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE   certificat de -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Certificat de localisation de moins de 10 ans indiquant l'état actuel de la propriété. Le certificat devra être complet et exact en tous points, l'immeuble n'ayant subi aucune modification, addition et/ou altération depuis son émission.
					</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>		
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE   nouveau certificat de -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId='1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/newConstructionId='2'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>									
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Nouveau certificat de localisation demandé (nouvelle construction). 
						</xsl:element>
				</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  date pr -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Date prévue du déboursé <xsl:value-of select="//Deal/estimatedClosingDate"></xsl:value-of>
						</xsl:element>
				</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  Clause de 1/12e des taxes  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">6cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">6cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">6cm</xsl:attribute></xsl:element>						
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">   									                                   
									<xsl:if test="//Deal/paymentFrequencyId='0'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:if>
									<xsl:if test="not(//Deal/paymentFrequencyId= '0')">
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:if>										
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:if test="//Deal/paymentFrequencyId='0'">
										Clause de 1/12e des taxes <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
									</xsl:if>
									<xsl:if test="not(//Deal/paymentFrequencyId= '0')">
										Clause de 1/12e des taxes $ 
									</xsl:if>									
								</xsl:when>
								<xsl:otherwise>Clause de 1/12e des taxes $ </xsl:otherwise>
							</xsl:choose>								
					</xsl:element></xsl:element>										
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">   
									<xsl:choose>
										<xsl:when test="//Deal/paymentFrequencyId='4'">
											<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
											 Clause de 1/52e des taxes <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:when test="//Deal/paymentFrequencyId='5'">
											<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
											 Clause de 1/52e <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>											
										<xsl:otherwise>
											<xsl:call-template name="UnCheckedCheckbox"/> Clause de 1/52e
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/> Clause de 1/52e $
								</xsl:otherwise>
							</xsl:choose>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">   
									<xsl:choose>
										<xsl:when test="//Deal/paymentFrequencyId='2'">
											<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
											 1/26e <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/> 
										</xsl:when>
										<xsl:when test="//Deal/paymentFrequencyId='3'">
											<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
											 1/26e <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/> 
										</xsl:when>											
										<xsl:otherwise>
											<xsl:call-template name="UnCheckedCheckbox"/> 1/26e $
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/> 1/26e $
								</xsl:otherwise>
							</xsl:choose>							
					</xsl:element></xsl:element></xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->					
		<!-- ===================================================  -->
		<!-- START OF TABLE  clauses usuelles  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:choose>
							<xsl:when test="//DealAdditional/propertyTypeId = '2'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:when test="//DealAdditional/propertyTypeId = '14'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:when test="//DealAdditional/propertyTypeId = '15'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:when test="//DealAdditional/propertyTypeId = '16'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>																											
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>		
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Clauses usuelles pour condominium. 
					</xsl:element>
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  constructions  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/progressAdvance= 'Y'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Construction: déboursé progressif, le 1er déboursé devra se faire au plus tard 90 jours après la date de l'acte de prêt hypothécaire et retenue de 15%. 
						</xsl:element>
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  pour constructions  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:choose>
							<xsl:when test="//DealAdditional/Property/newConstructionId= '1'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:when test="//DealAdditional/Property/newConstructionId= '2'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>									
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>		
				</xsl:element></xsl:element>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Pour construction neuve: Le constructeur doit être accrédité auprès d'un programme de garantie de maisons neuves et la propriété doit être enregistrée à ce programme. 
					</xsl:element>
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  Rénovations  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Rénovations: Contrôle du déboursé et pour rénovations supérieures à 10 000$, le constructeur doit être accrédité auprès d'un programme de rénovations.
						</xsl:element>
				</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  Obtenir -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:choose>
							<xsl:when test="//Deal/progressAdvance= 'Y'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>		
				</xsl:element></xsl:element>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Obtenir renonciation des hypothèques légales ou consentement à priorité d'hypothèque, certificat de fin des travaux et retenue de 15 % jusqu'à 35 jours de la date de fin des travaux.
					</xsl:element></xsl:element>										
		</xsl:element></xsl:element></xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE   Intervention   -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Intervention à l'acte de prêt hypothécaire à titre de caution. 
						</xsl:element></xsl:element>										
		</xsl:element></xsl:element></xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  Obtenir une analys-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Obtenir une analyse d'eau acceptable pour la caisse et le certificat de conformité de la municipalité pour le puits et la fosse septique si la propriété n'est pas reliée au réseau d'aqueduc. 
						</xsl:element></xsl:element>										
		</xsl:element></xsl:element></xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  À la demande -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							À la demande de l'emprunteur ou des emprunteurs, les remboursements se feront:
						</xsl:element></xsl:element>										
		</xsl:element></xsl:element></xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->				
		<!-- ===================================================  -->
		<!-- START OF TABLE  hebdomadairement  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:text> </xsl:text>
						</xsl:element></xsl:element>										
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 hebdomadairement à raison de $  
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 incluant vie 
						</xsl:element>					
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 incluant vie-invalidité 
					</xsl:element>	</xsl:element>										
		</xsl:element></xsl:element></xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  aux 2  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text> </xsl:text>
					</xsl:element></xsl:element>										
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 aux 2 semaines à raison de $   
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 incluant vie 
						</xsl:element>					
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 ncluant vie-invalidité 
						</xsl:element>							
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  le premier se faisant -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">6.5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:text> </xsl:text>
						</xsl:element></xsl:element>										
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							 le premier se faisant  
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId= '2'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId= '3'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>									
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 7e jour suivant le déboursé du prêt 
						</xsl:element>					
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId= '4'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId= '5'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>										
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 14e jour suivant le déboursé du prêt 
						</xsl:element>							
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>	
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		<!-- ===================================================  -->
		<!-- START OF TABLE  Le ou les emprunteurs  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row"><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Le ou les emprunteurs désirent un prêt RAP, S.V.P. communiquez avec ces derniers sur la réception du dossier. 
						</xsl:element>
					</xsl:element>										
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		

		<!-- ===================================================  -->
		<!-- START OF TABLE  Commentaires  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								Commentaires sur promotion 
							</xsl:element>
					</xsl:element>										
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		

		<!-- ===================================================  -->
		<!-- START OF TABLE  Autres -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								Autres  
							</xsl:element>
					</xsl:element>										
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->		
		
	</xsl:template>
	
	<xsl:template name="DealConditionsNotWaived">
		<xsl:element name="fo:block"><xsl:attribute name="keep-together">always</xsl:attribute><xsl:copy use-attribute-sets="MainFontAttrPg4"/>
			<xsl:element name="fo:list-block"><xsl:attribute name="space-before.optimum">3mm</xsl:attribute><xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
				<xsl:apply-templates select="//Deal/DocumentTracking"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

     <xsl:template match="DocumentTracking">
     	<xsl:choose>
     	    <xsl:when test="Condition/conditionTypeId='0'">
				<xsl:element name="fo:list-item"><xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
					<xsl:element name="fo:list-item-label">
						<xsl:element name="fo:block" use-attribute-sets="ListLabelIndent"><xsl:call-template name="CheckedCheckbox"/></xsl:element>
					</xsl:element>
					<xsl:element name="fo:list-item-body"><xsl:attribute name="start-indent">body-start()</xsl:attribute>
						<xsl:element name="fo:block"><xsl:attribute name="text-align">justify</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="./text"/></xsl:element></xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
		          <xsl:if test="((documentStatusId='0') or (documentStatusId='0')) and (Condition/conditionTypeId='2') and (Condition/conditionTypeId!='5')">
					<xsl:element name="fo:list-item"><xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
						<xsl:element name="fo:list-item-label"><xsl:element name="fo:block"><xsl:call-template name="CheckedCheckbox"/> - </xsl:element></xsl:element>
						<xsl:element name="fo:list-item-body"><xsl:attribute name="start-indent">body-start()</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="text-align">justify</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select="./text"/></xsl:element></xsl:element>
					</xsl:element>
				</xsl:if>			
			</xsl:otherwise>	
		</xsl:choose>
     </xsl:template> 
	
	<xsl:template name="Page_3_Item_1">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Le prêt sera consenti aux conditions suivantes, en plus de celles en usage à la caisse.</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute> -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 1. DOCUMENTS À REMETTRE AU NOTAIRE</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Sur réception de la présente, vous devez transmettre au notaire les documents suivants s'ils sont disponibles, et tout autre document que le notaire pourra exiger :</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<fo:list-block>
							<xsl:attribute name="start-indent">1mm</xsl:attribute>
							<xsl:attribute name="provisional-distance-between-starts">12mm</xsl:attribute>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										votre contrat de mariage, le cas échéant;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ======================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										votre titre d'acquisition de l'immeuble, le cas échéant;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										copie du contrat préliminaire (offre d'achat) et de la note d'information, s'il y a lieu;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										un certificat de localisation à la satisfaction de la caisse, ou conforme aux spécifications prévues au recto, le cas échéant;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										une police d'assurance incendie et autres risques pour un montant suffisant pour couvrir le prêt, et l'ouverture de crédit le cas échéant, ou une note de couverture ou s'il s'agit d'un condominium, le certificat attestant qu'une assurance couvrant l'immeuble de la copropriété a été souscrite par les administrateurs;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										les reçus de taxes municipales et scolaires; et,
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										s'il s'agit d'une personne morale, la preuve de son existence juridique et de son pouvoir d'effectuer l'emprunt projeté.
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
						</fo:list-block>
						<!-- =============================================================================-->
						<!-- GROUP 2  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 2. SIGNATURE DE L'ACTE HYPOTHÉCAIRE ET DÉBOURSEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Vous devrez signer un acte de prêt hypothécaire selon les formulaires en usage à la caisse. Le prêt sera déboursé lorsqu'il aura été démontré à la caisse, à sa satisfaction :</xsl:text>
						</xsl:element>
						<!-- ===================== -->
						<fo:list-block>
							<xsl:attribute name="start-indent">1mm</xsl:attribute>
							<xsl:attribute name="provisional-distance-between-starts">12mm</xsl:attribute>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										qu'elle détient une bonne et valable hypothèque du rang exigé;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ======================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>
										que l'immeuble est assuré contre l'incendie et autres risques pour un montant suffisant pour couvrir la créance de la caisse, et que la police contient la clause relative à la garantie hypothécaire en faveur de la caisse;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										que toutes les taxes ou charges pouvant affecter l'immeuble sont payées;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block><xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute><xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block><xsl:attribute name="text-align">justify</xsl:attribute>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										que toute irrégularité pouvant avoir été constatée dans le certificat de localisation est corrigée;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										que le certificat d'autorisation exigé par la Loi sur la qualité de l'environnement (L.R.Q., c. Q.-2) est délivré au nom de l'emprunteur;
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
							<fo:list-item>
								<fo:list-item-label>
									<xsl:attribute name="space-after">8mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:text>-</xsl:text>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<xsl:attribute name="start-indent">5mm</xsl:attribute>
									<xsl:attribute name="end-indent">6mm</xsl:attribute>
									<fo:block>
										<xsl:attribute name="line-height">10pt</xsl:attribute>
										<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
										<xsl:attribute name="font-size">8pt</xsl:attribute>									
										que toutes les conditions applicables au prêt sont respectées.
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
							<!-- ===================== -->
						</fo:list-block>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>L'ouverture de crédit, le cas échéant, sera déboursée uniquement si vous vous prévalez du privilège conféré par la clause "Protection propriétaire" conformément aux conditions de l'acte hypothécaire.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 3  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 3. CONSTRUCTION, RÉPARATION OU AMÉLIORATION D'UN IMMEUBLE</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>S'il s'agit d'un prêt à des fins de construction, de réparation ou d'amélioration d'un immeuble, le prêt sera versé au moyen de
déboursements successifs, au fur et à mesure de la progression de travaux. Vous devrez, avant chaque déboursé, remettre à la
caisse ou au notaire, une déclaration solennelle relative aux hypothèques légales de construction. Si les travaux on été confiés à
un entrepreneur général, les déboursés pourront être faits par chèques conjoints à votre ordre conjointement avec l'entrepreneur
général et, le cas échéant, les personnes vous ayant dénoncé leur contrat avec l'entrepreneur général, ou à votre ordre
conjointement avec les personnes avec qui vous avez contracté directement. Si les travaux n'ont pas été confiés à un entrepreneur
général, les déboursements pourront être faits par chèques conjoints à votre ordre conjointement avec toute personne avec qui
vous avez contracté directement. Au lieu de s'assurer du paiement des personnes pouvant détenir une hypothèque légale de la
construction tel qu'indiqué ci-dessus, la caisse pourra exiger que vous lui remettiez, avant tout déboursement, un "Consentement à
priorité d'hypothèque" signé en sa faveur par chacune de ces personnes. Dans tous les cas, la caisse pourra faire des retenues de
façon à ce que la valeur des travaux excède, dans chaque cas, de 15 % le total des déboursements demandés et des
déboursements déjà effectués, le dernier déboursement de 15 % pouvant être effectué 35 jours après la fin des travaux, le tout à la
discrétion de la caisse.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 4  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 4. PAIEMENT DE L'INTÉRÊT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Si le prêt est versé au moyen de déboursements successifs, la caisse pourra, en tout temps, exiger que l'intérêt accumulé soit
payé périodiquement en entier. L'intérêt accumulé au jour de chaque déboursement, poura être déduit de ce dernier par la caisse.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 5  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 5. DÉLAI DE DÉBOURSEMENT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Si le déboursement entier du prêt ne peut être effectué dans les trois mois de la date de signature de l'acte hypothécaire, la caisse
pourra, moyennant un avis écrit à cet effet, refuser de faire tout autre déboursement, conservant alors ses recours à l'égard des
sommes déjà versées.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 6  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 6. FRAIS</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Vous devez payer les honoraires de notaire, d'arpenteur-géomètre et d'évaluateur, ainsi que les frais de publication, d'inspection,
de vérification environnementale et autres frais de même nature. La caisse pourra retenir les sommes suffisantes à même le
produit du prêt pour les acquitter. Vous devrez payer également, lorsque le prêt sera remboursé avec ou sans subrogation, les
frais et honoraires de radiation notariée.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 7  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 7. AUTORISATION DE DÉBIT</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Toute somme exigible pourra être débitée directement de l'un ou l'autre de vos comptes d'épargne à la caisse.</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 8  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 8. PRIX DE VENTE DE L'IMMEUBLE</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Si le prêt sert à l'acquisiton d'un immeuble, la caisse se réserve le droit de réduire le montant du prêt si le prix de vente est inférieur
à celui prévu à l'offre d'achat.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
						<!-- =============================================================================-->
						<!-- GROUP 9  -->
						<!-- ============================================================================  -->
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<fo:inline font-weight="bold"> 9. MODIFICATION DE VOTRE SITUATION FINANCIÈRE</fo:inline>
						</xsl:element>
						<!-- ===================== -->
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">8pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<!-- <xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>  -->
							<!-- <xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute> -->
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>La caisse se réserve le droit de retirer la présente offre de financement si elle constate que votre situation financière s'est
détériorée sensiblement depuis votre demande de crédit.
</xsl:text>
						</xsl:element>
						<!-- =============================================================================-->
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_Ending_a">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4.0cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">13.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Documents annexés :</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Déclaration de l'emprunteur : hypothèques légales de la construction</xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Consentement à priorité : hypothèques légales de la construction</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_Ending">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Copie de la présente est transmise au notaire, Me </xsl:text>
							<xsl:choose>
								<xsl:when test="//DealAdditional/solicitorData/partyProfile/Contact/contactLastName">								
									<xsl:value-of select="//DealAdditional/solicitorData/partyProfile/Contact/contactFirstName"/>&#160;<xsl:value-of select="//DealAdditional/solicitorData/partyProfile/Contact/contactLastName"/>.
								</xsl:when>
								<xsl:otherwise> à déterminer.</xsl:otherwise>
							</xsl:choose> 						
							<xsl:text> Communiquer avec nous sans tarder si vous avez besoin de renseignements additionnels ou si vous ne désirez plus obtenir cet emprunt. </xsl:text>							
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">14pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/Contact/contactFirstName"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/underwriter/Contact/contactLastName"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Pour un prêt hypothécaire autre que de 1er rang soumis à la Loi sur la protection du consommateur, une copie des présentes doit être annexée à l'acte notarié.</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_8">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>8.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> L'acte hypothécaire devra être signé dans les 150 jours. Après ce délai, la caisse aura le droit de modifier le taux d'intérêt et les autres modalités, auquel cas elle vous transmettra une nouvelle offre de financement, ou elle pourra ne pas consentir le prêt. </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_7a">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/><xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">5px</xsl:attribute>
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold"> Voir Annexe </fo:inline>
						</xsl:element>
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_7">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row"><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>7.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> En plus des conditions prévues au verso, les conditions suivantes, qui continueront de s'appliquer malgré la signature de l'acte hypothécaire, sont exigées : </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_6a">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/><xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">170mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:attribute name="border">solid black 0.5px</xsl:attribute><xsl:attribute name="padding-left">5px</xsl:attribute>
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:apply-templates select="//DealAdditional/Property"/>
					</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	
	<!-- Subject property, French formatting of address-->
	<xsl:template match="Property">
		<xsl:element name="fo:block"><xsl:attribute name="keep-together">always</xsl:attribute>			
			<xsl:value-of select="propertyStreetNumber"/><xsl:text>, </xsl:text><xsl:if test="streetType"><xsl:value-of select="streetType"/><xsl:text>  </xsl:text></xsl:if>
			<xsl:value-of select="propertyStreetName"/><xsl:if test="streetDirection"><xsl:text>  </xsl:text><xsl:value-of select="streetDirection"/></xsl:if>
			<xsl:if test="unitNumber">, unité <xsl:value-of select="unitNumber"/></xsl:if>
		</xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="concat(propertyCity,', ',province)"/></xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/></xsl:element>
	</xsl:template>
	
	<xsl:template name="Page_2_Item_6">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>							
							<xsl:text>6.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							 Le prêt et l'ouverture de crédit, le cas échéant, doivent être garantis par une hypothèque de 
							<xsl:choose>
								<xsl:when test="//Deal/lienPositionId='0'">
									<fo:inline font-weight="bold"> 1</fo:inline><fo:inline font-weight="bold" font-size="8" baseline-shift="super">er </fo:inline>
								</xsl:when>
							    <xsl:otherwise>
							    		<fo:inline font-weight="bold"> 2</fo:inline><fo:inline font-weight="bold" font-size="8" baseline-shift="super">ième </fo:inline>
							    </xsl:otherwise>
							</xsl:choose>
							 rang sur l'immeuble situé à l'adresse suivante :
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>

	<xsl:template name="Page_2_Item_5">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>5.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> Le prêt sera remboursable par anticipation &#160;&#160;&#160;  </xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/prePaymentOptionsId= '4'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Sans indemnité &#160;&#160;</xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:when test="//Deal/interestTypeId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>								
								<xsl:when test="//Deal/interestTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>								
								<xsl:when test="//Deal/interestTypeId = '5'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>																								
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Avec indemnité </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							 si les montants payés par anticipation excèdent <fo:inline font-weight="bold">  15.0%  </fo:inline> du montant initial du prêt conformément à l'acte hypothécaire.
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_4">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">
							<xsl:text>4.</xsl:text>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">
							<xsl:text>Malgré les modalités précitées qui paraîtront à l'acte hypothécaire, si vous adhérez à</xsl:text>
					</xsl:element>	</xsl:element>
		</xsl:element></xsl:element></xsl:element>
	</xsl:template>
	
	<xsl:template name="Page_2_Item_4b">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">
							<xsl:text> </xsl:text>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">
						les paiements périodiques que vous devrez effectuer s'élèveront à 
						<fo:inline font-weight="bold"><xsl:value-of select="//Deal/pmntPlusLifeDisability"/></fo:inline>, 
						afin d'inclure la prime de cette ou de ces assurances, qui correspond à un taux d'intérêt annuel additionnel de
						<fo:inline font-weight="bold">&#160;<xsl:value-of select="//Deal/interestRateIncrement"/>, &#160;</fo:inline>
						le tout sous réserve des dispositions de la police d'assurance en vigueur à la caisse.
					</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>
	</xsl:template>
	
	<xsl:template name="Page_2_Item_4a">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">8.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">8.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:choose>
							<xsl:when test="(./LDInsuranceTypeId = '1' and ./lifeStatusId = '1') or (./LDInsuranceTypeId = '2' and ./disabilityStatusId = '2')">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text> Assurance vie seulement</xsl:text>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:choose>
							<xsl:when test="./LDInsuranceTypeId = '2' and ./disabilityStatusId = '1'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text> Assurance vie et Assurance invalidité </xsl:text>
					</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>
	</xsl:template>
	
	<xsl:template name="Page_2_Item_3">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>3.</xsl:text>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Une ouverture de crédit de <fo:inline font-weight="bold">0 $ </fo:inline> vous sera également accordée aux fins de la clause "Protection propriétaire".
					</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2b_after">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<fo:inline font-weight="bold">Dans le cas d'un prêt assuré par &#160;&#160;&#160;&#160;&#160; </fo:inline>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId='1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
									<fo:inline font-weight="bold">&#160; la Société canadienne d'hypothèques et de logement &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element> -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/mortgageInsurerId='2'">
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; la Société canadienne d'hypothèques et de logement  &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; la Société canadienne d'hypothèques et de logement  &#160;&#160;&#160;</fo:inline>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2c_after">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>Le montant des paiements est basé sur une période d'amortissement de </xsl:text>
							<fo:inline font-weight="bold"> <xsl:value-of select="//specialRequirementTags/amortizationTermProcessed/termNumber"> </xsl:value-of>
								<xsl:choose>
									<xsl:when test="//specialRequirementTags/amortizationTermProcessed/termMeasure='MONTHS'"> mois. </xsl:when>
									<xsl:when test="//specialRequirementTags/amortizationTermProcessed/termMeasure='YEARS'"> an(s). </xsl:when>
								</xsl:choose>
							</fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="Page_2_Item_2">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">
							<xsl:text>2. </xsl:text>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							Le remboursement s'effectuera au moyen de paiements périodiques, constitués de capital et d'intérêts, égaux et consécutifs de <fo:inline font-weight="bold"><xsl:value-of select="//Deal/PandiPaymentAmount"></xsl:value-of> </fo:inline> chacun, le premier paiement devant être fait le 
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId='0'">
									<fo:inline font-weight="bold" > 30ème jour suivant le déboursé du prêt </fo:inline> et les autres successivement le <fo:inline font-weight="bold"> même  jour </fo:inline> de chaque <fo:inline font-weight="bold"> mois.</fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId='2'">
									<fo:inline font-weight="bold" > 14 jours suivant le déboursé du prêt </fo:inline> et les autres successivement le <fo:inline font-weight="bold"> même  jour </fo:inline> de chaque <fo:inline font-weight="bold"> deux semaines.</fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId='4'">
									<fo:inline font-weight="bold" > 7 ème jour suivant la déboursé du prêt </fo:inline> et les autres successivement le <fo:inline font-weight="bold"> même  jour </fo:inline> de chaque <fo:inline font-weight="bold"> semaine.</fo:inline>
								</xsl:when>																
							</xsl:choose>							
					</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>
	</xsl:template>

	<xsl:template name="CreateHeader_Page_2">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">75mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">80mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">
						À :
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:call-template name="BorrowersListedBlocks"/>
						</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:call-template name="PrimBorrAddress"/>
						</xsl:element>
					</xsl:element>
					<!-- =============================  -->
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">
						De :
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyCompanyName"/>
						</xsl:element>
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine1"></xsl:value-of>
						</xsl:element>		
						<xsl:if test="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine2">
							<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/><xsl:attribute name="font-weight">bold</xsl:attribute>
								<fo:inline font-weight="bold"><xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine2"></xsl:value-of></fo:inline>
							</xsl:element>						
						</xsl:if>
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold"><xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/city"></xsl:value-of>, <xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/province"></xsl:value-of></fo:inline>
						</xsl:element>
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold"><xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/postalFSA"></xsl:value-of>&#160;<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/postalLDU"></xsl:value-of></fo:inline>
						</xsl:element>												
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		
		<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/><xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:text>Cher(s) membre(s),</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/><xsl:attribute name="text-align">justify</xsl:attribute>
			C'est avec plaisir que nous vous informons que votre demande de prêt hypothécaire de <fo:inline font-weight="bold"><xsl:value-of select="//Deal/totalLoanAmount"/> </fo:inline>  a été acceptée avec les modalités suivantes :
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_2_Item_1c">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"></xsl:element>
					<xsl:element name="fo:table-cell"></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">justify</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/interestTypeId = '2'">
										Taux maximum : Toutefois, le taux applicable au prêt ne peut à aucun moment excéder <fo:inline font-weight="bold"> <xsl:value-of select="//Deal/netInterestRate"></xsl:value-of> </fo:inline> l'an, calculé mensuellement et non à l'avance soit  <fo:inline font-weight="bold"><xsl:value-of select="//Deal/TO_BE_DETERMINED"></xsl:value-of> </fo:inline>  l'an calculé semestriellement et non à l'avance. 
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>Taux maximum: Toutefois, le taux applicable au prêt ne peut à aucun moment excéder l'an, calculé mensuellement et non à l'avance soit  l'an calculé semestriellement et non à l'avance. </xsl:text>
									</xsl:otherwise>
								</xsl:choose>														
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<!-- zzz -->
	<xsl:template name="Page_2_Item_1b">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1bBox"></xsl:call-template>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>b)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1bText"></xsl:call-template>					
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_2a">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> a)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>jusqu'au inclusivement, date à laquelle tout solde alors dû deviendra exigible;</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_2b">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="//Deal/interestTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									<!-- <xsl:element name="fo:external-graphic">
										<xsl:attribute name="src">d:\mos\admin\docgen\templates\not_checked.gif</xsl:attribute>
										<xsl:attribute name="content-height">100%</xsl:attribute>
										<xsl:attribute name="content-width">100%</xsl:attribute>
									</xsl:element>  -->
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> b)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>jusqu'à l'échéance d'un terme de commençant à la date de signature de l'acte hypothécaire, tout solde dû devenant exigible à l'échéance de ce terme;</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_2c">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId= '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:when test="//Deal/mortgageInsurerId= '2'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>								
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> c)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								jusqu'à l'échéance d'un terme de <fo:inline font-weight="bold"> <xsl:value-of select="//Deal/actualPaymentTerm"></xsl:value-of> mois </fo:inline>
								commençant à la date d'ajustement des intérêts, soit le <fo:inline font-weight="bold"> jour du déboursé du prêt </fo:inline> tout solde dû devenant exigible à l'échéance de ce terme.
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_1a">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1aBox"></xsl:call-template>					
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>a)</xsl:text>
						</xsl:element>
					</xsl:element>
					<!-- zzz -->					
					<xsl:call-template name="ConstructPage2Item1aText"></xsl:call-template>					
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<xsl:template name="Page_2_Item_1">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>1. </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Le prêt portera intérêt : </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	<!--<xsl:template name="CreatePageTwo">
	<xsl:element name="page-sequence">
		<xsl:attribute name="master-reference">main</xsl:attribute>
		<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
					<xsl:call-template name="CreatePageTwoTopLine"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoTable"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoCCLine"></xsl:call-template>
			</xsl:element>
	</xsl:element>
</xsl:template> -->
	<xsl:template name="CreatePageTwoCCLine">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="concat('cc:    ', //Deal/sourceOfBusinessProfile/Contact/contactFirstName, '   ', //Deal/sourceOfBusinessProfile/Contact/contactLastName , '  représentant(e) hypothécaire ' )"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">2em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>Monsieur (Madame),</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block"><xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>C'est avec grand plaisir que nous vous informons que votre demande de financement hypothécaire a été acceptée. Vous trouverez ci-joint une offre de financement comportant les modalités et conditions relatives à votre emprunt.</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block"><xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			Tel que convenu, votre dossier sera acheminé à la <xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyCompanyName"></xsl:value-of>. 
			Pour toute information additionnelle, nous vous invitons à contacter votre représentant(e) hypothécaire 
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/salutation"/>
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactFirstName"/><xsl:text>&#160;</xsl:text><xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactLastName"> </xsl:value-of> au numéro de téléphone suivant : <xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"> </xsl:value-of>. 
			Il / Elle se fera un plaisir de répondre à toutes vos questions.
		</xsl:element>
		<xsl:element name="fo:block"><xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>Nous vous remercions de la confiance que vous nous témoignez et nous espérons avoir le plaisir de vous servir à nouveau. Nous vous prions de recevoir, Monsieur (Madame), l'expression de nos sentiments distingués.</xsl:text>
		</xsl:element>
	</xsl:template>
		
		<!-- Underwriter info -->
     <xsl:template name="CreateUnderwriterLines">         
		<xsl:element name="fo:table">	<xsl:copy use-attribute-sets="TableLeftFixed"/><xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="MainFontAttr"/><xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="font-style">italic</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="space-before.optimum">5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select="//Deal/underwriter/userType"/>
						</xsl:element>		
						<xsl:element name="fo:table">	<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column"><xsl:attribute name="column-width">30mm</xsl:attribute></xsl:element>
							<xsl:element name="fo:table-column"><xsl:attribute name="column-width">100mm</xsl:attribute></xsl:element>
							<xsl:element name="fo:table-body">	<xsl:copy use-attribute-sets="PaddingAll0mm"/>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block"><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
											Téléphone :	
									</xsl:element></xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block"><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumber"/>
										     	<xsl:choose>
														<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> or 1-888-281-4420 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when>
														<!-- <xsl:when test="//Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when> -->
														<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> or 1-800-728-2728 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when>
													<xsl:otherwise>
														<xsl:text> </xsl:text>ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
													</xsl:otherwise>		
												</xsl:choose>
									</xsl:element></xsl:element>
								</xsl:element>	
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block"><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
											Télécopieur :
									</xsl:element></xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block"><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="//Deal/underwriter/Contact/contactFaxNumber"/>
										     	<xsl:choose>
													<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> ou 1-888-888-7777</xsl:when>
													<xsl:when test="//Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999</xsl:when>
													<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> ou 1-888-888-8888</xsl:when>
												</xsl:choose>												
									</xsl:element></xsl:element>
						</xsl:element></xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element></xsl:element>		
	</xsl:template>
	
	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>  
					</xsl:element></xsl:element></xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">	<xsl:copy use-attribute-sets="TableLeftFixed"/><xsl:attribute name="space-before">10mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">3cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">11cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll2mm"/><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Avis de financement - Hypothèque immobilière
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateTitleLine_Page_2">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<!-- <xsl:attribute name="space-before">0.7in</xsl:attribute> -->
			<!-- <xsl:attribute name="break-before">page</xsl:attribute> -->
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">14cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll2mm"/><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Offre de financement - Hypothèque immobilière <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateTitleLinePage4">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll2mm"/><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Annexe à l'Offre de financement - Hypothèque immobilière <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateTitleLinePage3">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">3cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">11cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>				
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll2mm"/><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Conditions à l'Offre de financement
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">19cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll0mm"/>
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainFontAttr"/>
						<xsl:attribute name="space-after">2em</xsl:attribute><xsl:attribute name="space-before">2em</xsl:attribute>
							Le  <xsl:value-of select="//General/CurrentDate"/>
			</xsl:element></xsl:element></xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>
	
     <xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:element name="fo:block"><xsl:call-template name="BorrowerFullName"/></xsl:element>	
		</xsl:for-each>
     </xsl:template>   
     
     <xsl:template name="BorrowerFullName">
     	<xsl:if test="salutation"><xsl:value-of select="salutation"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="borrowerFirstName"/><xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial"><xsl:value-of select="borrowerMiddleInitial"/><xsl:text> </xsl:text></xsl:if><xsl:value-of select="borrowerLastName"/>     
	</xsl:template>
	
	<xsl:template name="BorrAddress">	
		<xsl:element name="fo:block"><xsl:value-of select="./Address/addressLine1"/></xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="./Address/addressLine2"/></xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="concat(./Address/city,' ',./Address/province)"/></xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="concat(./Address/postalFSA,'  ', ./Address/postalLDU)"/></xsl:element>
	</xsl:template>	
	
	<xsl:template name="PrimBorrAddress">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:if test="position()=1">
				<xsl:for-each select="BorrowerAddress"><xsl:if test="borrowerAddressTypeId='0'">
					<xsl:call-template name="BorrAddress"/><text>some text</text>
				</xsl:if></xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
		
	<xsl:template name="CreateBorrowerLines">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">19cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll2mm"/><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2pt</xsl:attribute><xsl:attribute name="space-before">2pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="BorrowersListedBlocks"/>
						</xsl:element>						
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2pt</xsl:attribute><xsl:attribute name="space-before">2pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="PrimBorrAddress"/>
						</xsl:element>						
				</xsl:element></xsl:element>
			</xsl:element></xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">10mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">2cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll2mm"/><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
								Objet :
							</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Demande de financement hypothécaire 
							</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Numéro de dossier : <xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="FOStart">
		<xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:root">
			<xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute>
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
			<xsl:call-template name="CreatePageTwo"/>
			<xsl:call-template name="CreatePageThree"/>
			<xsl:call-template name="CreatePageFour"/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"></rect>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"></line>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"></line>
			</svg>
		</fo:instream-foreign-object>	
	</xsl:template>

	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"></rect>
			</svg>
		</fo:instream-foreign-object>	
	</xsl:template>	
	
	<xsl:template name="mortgageInsuranceCheckBox">
		<xsl:param name="parInsurer"></xsl:param>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<!-- Start  -->
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'"><xsl:call-template name="CheckedCheckbox"></xsl:call-template></xsl:when>
							<xsl:when test="$parStatus='16'"><xsl:call-template name="CheckedCheckbox"></xsl:call-template></xsl:when>
							<xsl:when test="$parStatus='23'"><xsl:call-template name="CheckedCheckbox"></xsl:call-template></xsl:when>						
							<xsl:otherwise><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template></xsl:otherwise>
						</xsl:choose>								
					</xsl:when>
					<xsl:otherwise><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template></xsl:otherwise>
				</xsl:choose>			
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'"><xsl:call-template name="CheckedCheckbox"></xsl:call-template></xsl:when>
							<xsl:when test="$parStatus='16'"><xsl:call-template name="CheckedCheckbox"></xsl:call-template></xsl:when>
							<xsl:when test="$parStatus='23'"><xsl:call-template name="CheckedCheckbox"></xsl:call-template></xsl:when>						
							<xsl:otherwise><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template></xsl:otherwise>
						</xsl:choose>								
					</xsl:when>
					<xsl:otherwise><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template></xsl:otherwise>
				</xsl:choose>			
			</xsl:when>			
			<xsl:otherwise><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template></xsl:otherwise>
		</xsl:choose>
		<!-- END  -->
	</xsl:template>
	
	<xsl:template name="mortgageInsurancePolicyNum">
		<xsl:param name="parInsurer"></xsl:param>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<!-- Start  -->
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'"><xsl:value-of select="//Deal/MIPolicyNumber"></xsl:value-of></xsl:when>
							<xsl:when test="$parStatus='16'"><xsl:value-of select="//Deal/MIPolicyNumber"></xsl:value-of></xsl:when>
							<xsl:when test="$parStatus='23'"><xsl:value-of select="//Deal/MIPolicyNumber"></xsl:value-of></xsl:when>						
							<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
						</xsl:choose>								
					</xsl:when>
					<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
				</xsl:choose>			
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'"><xsl:value-of select="//Deal/MIPolicyNumber"></xsl:value-of></xsl:when>
							<xsl:when test="$parStatus='16'"><xsl:value-of select="//Deal/MIPolicyNumber"></xsl:value-of></xsl:when>
							<xsl:when test="$parStatus='23'"><xsl:value-of select="//Deal/MIPolicyNumber"></xsl:value-of></xsl:when>						
							<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
						</xsl:choose>								
					</xsl:when>
					<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
				</xsl:choose>			
			</xsl:when>			
			<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
		</xsl:choose>
		<!-- END  -->
	</xsl:template>	

	<xsl:template name="ConstructPage2Item1aBox">
		<xsl:param name="pRate"></xsl:param>
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='0'"> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<!-- ======================================= -->
			<xsl:otherwise> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1aText">
		<xsl:param name="pRate"></xsl:param>
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='0'"> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text>à taux fixe : au taux de </xsl:text>
						<fo:inline font-weight="bold"><xsl:value-of select="//Deal/netInterestRate"/>	</fo:inline>
						<xsl:text>  l'an, calculé semestriellement et non à l'avance;</xsl:text>
					</xsl:element>
				</xsl:element>					
			</xsl:when>
			<xsl:otherwise> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text>à taux fixe: au taux de l'an, calculé semestriellement et non à l'avance;</xsl:text>
					</xsl:element>
				</xsl:element>			
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="mortgageInsurancePremiumAmount">
		<xsl:param name="parInsurer">99</xsl:param>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<!-- Start  -->
		<xsl:choose>
			<xsl:when test="$parInsurer='99'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'"><xsl:value-of select="//Deal/MIPremiumAmount"></xsl:value-of></xsl:when>
							<xsl:when test="$parStatus='16'"><xsl:value-of select="//Deal/MIPremiumAmount"></xsl:value-of></xsl:when>
							<xsl:when test="$parStatus='23'"><xsl:value-of select="//Deal/MIPremiumAmount"></xsl:value-of></xsl:when>						
							<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
						</xsl:choose>								
					</xsl:when>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'"><xsl:value-of select="//Deal/MIPremiumAmount"></xsl:value-of></xsl:when>
							<xsl:when test="$parStatus='16'"><xsl:value-of select="//Deal/MIPremiumAmount"></xsl:value-of></xsl:when>
							<xsl:when test="$parStatus='23'"><xsl:value-of select="//Deal/MIPremiumAmount"></xsl:value-of></xsl:when>						
							<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
						</xsl:choose>								
					</xsl:when>					
					<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
				</xsl:choose>			
			</xsl:when>
			<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
		</xsl:choose>
		<!-- END  -->
	</xsl:template>		

	<xsl:template name="ConstructPage2Item1bBox">
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='2'"> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<!-- ======================================= -->		
			<xsl:when test="//Deal/interestTypeId='3'"> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<!-- ======================================= -->
			<xsl:otherwise> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1bText">
		<xsl:choose>
			<xsl:when test="//Deal/interestTypeId='2'"> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text>à taux variable : au taux préférentiel de la Caisse centrale Desjardins majoré de </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold"><xsl:value-of select="//Deal/discount"/></fo:inline>		
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold"><xsl:value-of select="//Deal/premium"/></fo:inline>							
							</xsl:when>
						</xsl:choose>
						<xsl:text> l'an. Le taux applicable au prêt variera à chaque modification du dit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>
					</xsl:element>
				</xsl:element>					
			</xsl:when>
			<xsl:when test="//Deal/interestTypeId='3'"> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">justify</xsl:attribute>
							<xsl:text>à taux variable : au taux préférentiel de la Caisse centrale Desjardins majoré de  </xsl:text>
							<xsl:choose>
								<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
									<fo:inline font-weight="bold"><xsl:value-of select="//Deal/discount"/></fo:inline>		
								</xsl:when>
								<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
									<fo:inline font-weight="bold"><xsl:value-of select="//Deal/premium"/></fo:inline>							
								</xsl:when>
							</xsl:choose>							
							<xsl:text> l'an. Le taux applicable au prêt variera à chaque modification dudit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>
					</xsl:element>
				</xsl:element>					
			</xsl:when>			
			<xsl:otherwise> 
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text>  à taux variable: au taux préférentiel de la Caisse centrale Desjardins majoré de   l'an. Le taux applicable au prêt variera à chaque modification du dit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>					
					</xsl:element>
				</xsl:element>			
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


</xsl:stylesheet>
