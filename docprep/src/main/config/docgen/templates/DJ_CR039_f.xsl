<?xml version="1.0" encoding="UTF-8"?>
<!--
31/Jan/2007 DVG #DG574 #5634  CR 039 - changes for PPI  
12/Jan/2007 DVG #DG566 Upgrading to FOP0.9x 
06/Apr/2005 DVG #DG176 #1182  DJ documents - comma between city and province  
22/Feb/2005 DVG #DG140 # conform templates to jdk1.4 xalan, new xerces, fop, batik

created by Catherine Rutgaizer 
-->
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:fox="http://xml.apache.org/fop/extensions"
	xmlns:File="xalan://java.io.File">
	<!--	xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>

  <!-- #DG574 relative logo file name-->
  <xsl:variable name="logoPatha" select="document-location()" />
  <xsl:variable name="logoPathf" select="File:new($logoPatha)"/>
  <xsl:variable name="logoPath" select="File:getParent($logoPathf)"/>  
  <xsl:variable name="logoFilea" select="'\DJLogo_fr.gif'" />
  <xsl:variable name="logoFile" select="concat($logoPath,$logoFilea)" />

	<!-- define attribute sets -->
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr">
		<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ObjectAttr" use-attribute-sets="MainFontAttr SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr" use-attribute-sets="SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	<!-- end attribute sets -->

	<xsl:template match="/">
	  <xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>

		<xsl:call-template name="FOStart"/>
	</xsl:template>
	
	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
					<xsl:attribute name="text-align">center</xsl:attribute>
					Page <fo:page-number/> de <fo:page-number-citation ref-id="{generate-id(/)}"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateSalesRepLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
				<fo:block id="{generate-id(/)}" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="SpaceBAOptimum"/>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<!-- #DG574 xsl:element name="fo:external-graphic">
				<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
				<xsl:attribute name="height">1.1in</xsl:attribute>
				<xsl:attribute name="width">2.86in</xsl:attribute>
			</xsl:element-->
      <fo:external-graphic src="{$logoFile}" height="28mm" width="73mm"/>							
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">120mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TitleFontAttr"/>
						Avis – Acceptation conditionnelle d'un prêt hypothécaire 
				</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
					Le  <xsl:value-of select="/*/General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateSalesRepLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">70mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">75mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Représentant hypothécaire Desjardins :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:if test="/*/Deal/SourceOfBusinessProfile/Contact/salutation">
								<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/salutation"/>
							</xsl:if>
							<xsl:value-of select="concat(//Deal/SourceOfBusinessProfile/Contact/contactFirstName , '   ', //Deal/SourceOfBusinessProfile/Contact/contactLastName ) "/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
						Télécopieur : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactFaxNumber"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
						Courriel :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
							<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactEmailAddress"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">20mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">150mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="ObjectAttr"/>
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Objet :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Demande de financement hypothécaire </xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="padding-top">1mm</xsl:attribute>
						Numéro de dossier : <xsl:value-of select="/*/Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			La présente a pour but de confirmer l'acceptation du prêt hypothécaire conformément au financement demandé par <xsl:call-template name="BorrowersListed"/>  pour l'achat de la propriété ci-dessous mentionnée, sous les conditions suivantes :</xsl:element>

		<xsl:element name="fo:block">
			<!--#DG566 xsl:attribute name="keep-together">always</xsl:attribute-->
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:element name="fo:list-block">
				<xsl:attribute name="margin-left">5mm</xsl:attribute>
				<xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
				<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
				
				<!--#DG574 guarantee 1 item minimum -->  
  			<fo:list-item>
  				<fo:list-item-label>
  					<fo:block/>
  				</fo:list-item-label>
  				<fo:list-item-body>
  					<fo:block/>
  				</fo:list-item-body>
  			</fo:list-item>				
				
				<xsl:apply-templates select="/*/Deal/DocumentTracking[documentStatusId = 0 or documentStatusId = 1]"/> 
			</xsl:element>
		</xsl:element>
		
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:call-template name="PropertyDescription"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			Veuillez prendre note que vous devez faire suivre les documents originaux au Centre de financement au point de vente le plus rapidement possible.
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			En espérant le tout conforme, veuillez agréer l'expression de nos sentiments les meilleurs.
		</xsl:element>
	</xsl:template>

	<xsl:template name="BorrowersListed">
		<xsl:for-each select="/*/specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='0']">
			<xsl:choose>
				<xsl:when test="position()=1">
					<xsl:call-template name="BorrowerFullName"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="position()=last()">
							<xsl:text> et </xsl:text>
							<xsl:call-template name="BorrowerFullName"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>, </xsl:text>
							<xsl:call-template name="BorrowerFullName"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<!-- PVCS # 611, new condition for conditions -->
	<xsl:template match="DocumentTracking">
		<xsl:if test="Condition/conditionTypeId='0' or (Condition/conditionTypeId='2' and documentResponsibilityRoleId != '60')">
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">&#x2022;</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<!--#DG566 xsl:attribute name="keep-together">always</xsl:attribute-->
						<xsl:value-of select="./text"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template name="BorrowerFullName">
		<xsl:value-of select="salutation"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<xsl:template name="PropertyDescription">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="padding-top">1mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">18mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">38mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">27mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">61mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"> Propriété :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
 						<fo:block/><!--#DG574 guarantee 1 item minimum -->  
						<xsl:apply-templates select="/*/DealAdditional/Property"/>
					</xsl:element>					
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Type de prêt :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<!-- DG#140 xsl:element name="fo:table-cell"-->
							<xsl:element name="fo:block">
								<xsl:call-template name="MortgageInsurance"/>
							</xsl:element>
						<!-- DG#140 /xsl:element-->
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Coût d'achat :	</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/totalPurchasePrice"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>        
				<!--#DG574 add REFIIMPROVEMENTAMOUNT -->  
        <xsl:if test="/*/Deal/dealPurposeId = 17">
   				<fo:table-row>
  					<fo:table-cell>
  						<fo:block/>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block>Améliorations :	</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block text-align="right">
  							<xsl:value-of select="/*/Deal/refiImprovementAmount"/>
  						</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block/>
  					</fo:table-cell>
   				</fo:table-row>
   				<fo:table-row>
  					<fo:table-cell>
  						<fo:block/>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block>Achat + Améliorations :	</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block text-align="right">
  							<xsl:value-of select="/*/specialRequirementTags/asImprovedAmount"/>
  						</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block/>
  					</fo:table-cell>
   				</fo:table-row>
        </xsl:if>

				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Mise de fonds :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/downPaymentAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Montant du prêt :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/netLoanAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Montant de la prime :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/MIPremiumAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Montant total du prêt :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/totalLoanAmount"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Taux annuel :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">right</xsl:attribute>
							<xsl:value-of select="/*/Deal/netInterestRate"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Garantie :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">Référez aux conditions</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Dossier de l’assureur :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="/*/Deal/MIPolicyNumber"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Financement :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
  					<fo:block/><!--#DG574 guarantee 1 item minimum -->
						<xsl:for-each select="/*/DealAdditional/originationBranch/partyProfile">
							<xsl:element name="fo:block">
								<xsl:call-template name="PartyNameAndCompany"/>
							</xsl:element>
							<xsl:element name="fo:block">
								<xsl:apply-templates select="/*/DealAdditional/originationBranch/partyProfile/Contact/Address"/>
							</xsl:element>
						</xsl:for-each>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PartyNameAndCompany">
		<xsl:element name="fo:block">
			<xsl:value-of select="./partyName"/>
		</xsl:element>
		<!-- <xsl:element name="fo:block"><xsl:value-of select="./partyCompanyName"/></xsl:element> -->
	</xsl:template>

	<xsl:template match="Address">
		<xsl:element name="fo:block">
			<xsl:value-of select="./addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./city,', ',./province)"/><!--#DG176-->
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./postalFSA,'  ', ./postalLDU)"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="MortgageInsurance">
		<xsl:choose>
			<xsl:when test="/*/Deal/mortgageInsurerId='0'">Conventionnel</xsl:when>
			<xsl:when test="/*/Deal/mortgageInsurerId='1'">SCHL</xsl:when>
			<xsl:when test="/*/Deal/mortgageInsurerId='2'">
				<xsl:choose>
					<xsl:when test="/*/Deal/MITypeId='6'">GE50/50</xsl:when>
					<xsl:otherwise>GE100</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- Subject property, French formatting of address-->
	<xsl:template match="Property">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:value-of select="propertyStreetNumber"/>
			<xsl:text>, </xsl:text>
			<xsl:if test="streetType">
				<xsl:value-of select="streetType"/>
				<xsl:text>  </xsl:text>
			</xsl:if>
			<xsl:value-of select="propertyStreetName"/>
			<xsl:if test="streetDirection">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="streetDirection"/>
			</xsl:if>
			<xsl:if test="unitNumber">, unité <xsl:value-of select="unitNumber"/>
			</xsl:if>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyCity,', ',province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateUnderwriterLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="/*/Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">30mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">100mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:copy use-attribute-sets="PaddingAll0mm"/>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Téléphone :	
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumber"/>
											<xsl:choose>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='173'"> ou 1-888-281-4420 poste : <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<!-- xsl:when test="/*/Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999 poste : <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when> -->
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='175'"> ou 1-800-728-2728 poste : <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text> </xsl:text>poste : <xsl:value-of select="/*/Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Télécopieur :
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="/*/Deal/underwriter/Contact/contactFaxNumber"/>
											<xsl:choose>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='173'"> ou 1-888-888-7777</xsl:when>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999</xsl:when>
												<xsl:when test="/*/Deal/branchProfile/branchProfileId='175'"> ou 1-888-888-8888</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="FOStart">
		<!-- DG#140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- DG#140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>					
					<!--#DG574  
					xsl:attribute name="page-height">11in</xsl:attribute>
          xsl:attribute name="page-width">8.5in</xsl:attribute-->
					<xsl:attribute name="page-height">279mm</xsl:attribute>					
				  <xsl:attribute name="page-width">216mm</xsl:attribute>					
					<xsl:attribute name="margin-top">0mm</xsl:attribute>
					<xsl:attribute name="margin-bottom">10mm</xsl:attribute>
					<xsl:attribute name="margin-left">0mm</xsl:attribute>
					<xsl:attribute name="margin-right">25mm</xsl:attribute>
 					<!-- #DG566 moved down <xsl:element name="fo:region-before"> ....-->
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">38mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">15mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
