<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--
	07/Nov/2005 DVG #DG354 Decline letter project - new
  -->
	<xsl:output method="text"/>

	<xsl:variable name="lendName" select="translate(/*/Deal/LenderProfile/lenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendName,'.xsl'))" />
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />

	<xsl:variable name="theLenderName" select="/*/Deal/LenderProfile/lenderName"/>
	
	<xsl:template match="/">
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>

		<xsl:call-template name="RTFFileStart"/>		
    <xsl:call-template name="mainEn"/>
		<!--xsl:choose>
  		<xsl:when test="/*/Deal/Borrower[primaryBorrowerFlag = 'Y']/languagePreferenceId = 1"/>
  	    <xsl:call-template name="mainFr"/>
			</xsl:when>
			<xsl:otherwise>
  	    <xsl:call-template name="mainEn"/>
			</xsl:otherwise>
		</xsl:choose-->    		
		
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<xsl:template name="mainEn">

		<xsl:text>
\trowd\trgaph108\trleft-90
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\clvmgf
\clvertalc
\cellx4680
\cellx9460

\pard\intbl\f1\fs20 </xsl:text>
		<xsl:value-of select="$logoRtf"/>
		<xsl:text>\cell \qr</xsl:text>
		<xsl:call-template name="BranchAdr"/>		
		<xsl:text>\cell\row
\pard\par

\f1\fs20 </xsl:text>

		<xsl:call-template name="BorrowersListAll"/>
		<xsl:text>\line </xsl:text>
		<xsl:call-template name="PrimaryBorrowerAddress"/>
		<xsl:text>\par\par

</xsl:text>
		<xsl:value-of select="/*/General/CurrentDate"/>
		<xsl:text>\par\par 

\f1\fs20 Dear </xsl:text>
		<xsl:for-each select="/*/Deal/Borrower">
			
      <xsl:if test="position() != 1">		
      	<xsl:choose>
      		<xsl:when test="position() = last()">
      		  <xsl:text> and </xsl:text>
      		</xsl:when>
      		<xsl:otherwise>
      		  <xsl:text>, </xsl:text>    		  
      		</xsl:otherwise>
      	</xsl:choose>			
			</xsl:if>
			
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
		<xsl:text>:\par\par

\ul\b RE: Deal reference number </xsl:text>
		<xsl:value-of select="/*/Deal/dealId"/>
		<xsl:text>\ulnone\b0\par\par

</xsl:text>
		<xsl:text>\f1\fs20 Thank you for your credit application to </xsl:text>
		<xsl:value-of select="$theLenderName"/>
		<xsl:choose>
	  		<xsl:when test="/*/Deal/statusId = 24">  		
	    			<xsl:text>. After careful review of your application, we 
regret to inform you that we have declined your credit request.</xsl:text>  	    
			</xsl:when>
			<xsl:otherwise>
	    			<xsl:text>. Please note that your interest rate has 
incurred a premium, above the posted rate.</xsl:text>
			</xsl:otherwise>
		</xsl:choose>    		

		<xsl:text> This may have been due in part to information contained on your credit 
bureau. If you wish to view a copy of your credit report, please contact one or both of the 
following:\par\par

Equifax Canada Inc.\par
Consumer Relations Department\par
P.O. Box 190, Jean Talon Station\par
Montreal QC  H1S 2Z2\par
Phone: 1-800-465-7166\par\par

TransUnion Canada\par
Consumer Relations Centre\par
P.O. Box 338, LCD 1\par
Hamilton ON  L8L 7W2\par
Phone: (905) 525-0262 or 1-866-525-0262\par\par

If anything changes in your financial situation, please feel free to contact your Broker.\par\par

Sincerely,\par\par

</xsl:text>

		<xsl:for-each select="/*/Deal/UserProfile[userProfileId=/*/Deal/underwriterUserId]/Contact">
			<xsl:value-of select="contactFirstName"/><xsl:text> </xsl:text>				
    	<xsl:if test="string-length(borrowerMiddleInitial)>0">
    		<xsl:value-of select="contactMiddleInitial"/>
    		<xsl:text>. </xsl:text>
    	</xsl:if>
			<xsl:value-of select="contactLastName"/>				
		</xsl:for-each>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="$theLenderName"/>
		<xsl:text>\par\par 

{\footer\pard\qc\fs18 </xsl:text>		
		<xsl:value-of select="$theLenderName"/>		
		<xsl:text>\par </xsl:text>
		<xsl:call-template name="BranchAdrFoot"/>
		<xsl:text>\par }</xsl:text>
	</xsl:template>
	

	<!-- ================================================  -->
	<!-- Primary borrower's address on separate lines                                    -->
	<!-- ================================================  -->
	<xsl:template name="PrimaryBorrowerAddress">
		<xsl:for-each select="/*/Deal/Borrower[primaryBorrowerFlag='Y']">
			<xsl:for-each select="./BorrowerAddress[borrowerAddressTypeId='0']/Addr">
				<!-- current -->
				<xsl:value-of select="addressLine1"/>
				<xsl:text>\line </xsl:text>
				<xsl:if test="addressLine2 and string-length(addressLine2)>0">
					<xsl:value-of select="addressLine2"/>
					<xsl:text>\line </xsl:text>
				</xsl:if>
				<xsl:value-of select="city"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="province"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="concat(postalFSA,' ', postalLDU)"/>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowersListAll">
		<xsl:param name="separa" select="'\line '"/>
	
		<xsl:for-each select="/*/Deal/Borrower">
			<xsl:if test="position() != 1">
				<xsl:value-of select="$separa"/>
			</xsl:if>
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="/*/Deal/Borrower[borrowerTypeId=0]">
			<xsl:if test="position() != 1">, </xsl:if>
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="GuarantorNames">
		<xsl:for-each select="/*/Deal/Borrower[borrowerTypeId=1]">
			<xsl:text>, </xsl:text>
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="string-length(borrowerMiddleInitial)>0">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="PrimaryBorrowerName">
		<xsl:for-each select="/*/Deal/Borrower">
			<xsl:if test="./primaryBorrowerFlag='Y'">
				<xsl:call-template name="BorrowerFullName"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<!-- branch address -->
	<!-- ================================================  -->
	<xsl:template name="BranchAdr">
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">		
  		<xsl:value-of select="addressLine1"/>
  		<xsl:if test="addressLine2">
    		<xsl:text>\par </xsl:text>
    		<xsl:value-of select="addressLine2"/>
  		</xsl:if>
 			<xsl:text>,\par </xsl:text>
  		<xsl:value-of select="concat(city, ', ', province,' ',postalFSA,' ', postalLDU)"/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BranchAdrFoot">
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
  		<xsl:value-of select="addressLine1"/>
 			<xsl:text>, </xsl:text>
  		<xsl:if test="addressLine2">
    		<xsl:value-of select="addressLine2"/>
    		<xsl:text>, </xsl:text>
  		</xsl:if>
  		<xsl:value-of select="concat(city, ', ', province,' ',postalFSA,' ', postalLDU, ' ')"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\deff0\deflang1033\deflangfe1033
{\fonttbl{\f0\fswiss\fcharset0 Arial;}{\f1\fswiss\fprq2\fcharset0 Arial;}}
{\*\generator Msftedit 5.41.15.1507;}\viewkind4\uc1 
\margl1440\margr1440\margt720\margb720 
</xsl:text>
	</xsl:template>

</xsl:stylesheet>
