<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
25/Jul/2006 DVG #DG470 #3930  Concentra - Build Fixes 
31/May/2006 DVG #DG430 #3356  Concentra - Solicitor's Package Cosmetic Changes   
19/May/2006 DVG #DG424 #3243  Concentra - CR 160 - Solicitor Package, Fire Insurance wording change  
08/May/2006 DVG #DG418 #3111  CR#177 VRM  solicitor's package to accommodate new VRM product. 
13/Feb/2006 DVG #DG392 #2286  CR#132 Solicitors Package changes  
	- additional changes to #2286
24/Oct/2005 DVG #DG344 #2286  CR#132 Solicitors Package changes  
	#1670  Concentra - CR #158 (Solicitors package)  
	- total conversion from the xml to the xsl format
//MCM Impl Team 15-Jul-2008 XS_16.6 Modified changes under 'EnglishGuide' and 'EnglishRequestForFunds'
	-->
	 <!--MCM Impl 25-Jul-2008 Bug fix for artf750550,artf750527 -->

	<xsl:import href="CommitmentLetterCT.xsl"/>

	<xsl:output method="text"/>

	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LanguageEnglish">
				<xsl:call-template name="EnglishFooter"/>
				<xsl:call-template name="EnglishSolicitorInstructions"/>
				<xsl:call-template name="EnglishGuide"/>
				<!--xsl:call-template name="EnglishCommitmentLetter"/>
        <xsl:text>\pard \par \page </xsl:text-->				
				<xsl:call-template name="EnglishRequestForFunds"/>
				<xsl:call-template name="EnglishFinalReport"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LanguageFrench">
				<!--				<xsl:call-template name="FrenchTemplate"/>-->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishSolicitorInstructions">
		<xsl:call-template name="LogoAddress"/>
		<xsl:text>\pard\highlight {\f1\fs24\par  \f2\fs18 
_______________________________________________________________________________________________________\f1\fs24\par}


\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \tab\tab\tab\tab\tab\tab\tab\tab\tab\tab\tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/CurrentDate"/>
		<xsl:text>
\par }
<!--#DG424 \pard \qj \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 -->
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Name">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		
		<!--#DG424 xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SolicitorAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/SolicitorAddress/Postal"/-->
		<xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50]/Contact/Addr">
  		<xsl:call-template name="ContactAddress"/>
 		</xsl:for-each>		
		
		<xsl:text>
\par 
\par Dear  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:text>,
\par 
\par }

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 RE:\cell BORROWER(S):\cell </xsl:text>
		<xsl:for-each select="//SolicitorsPackage/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trbrdrh
\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>, if any:\cell 
</xsl:text>
		<xsl:for-each select="//SolicitorsPackage/GuarantorNames">
			<xsl:value-of select="./Name"/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}

\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell SECURITY ADDRESS:\cell 
</xsl:text>
		<!--#DG424 xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}
{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/PostalCode"/-->
    <xsl:call-template name="propAdrs"/><!--#DG430 -->	
		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell CLOSING DATE:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell APPROVED LOAN AMOUNT:\cell </xsl:text>
		<!--#DG418 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth648 \cellx540
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2970 \cellx3510
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth7136 \cellx10646
\row 
}
\pard \qj \widctlpar\tx540\aspalpha\aspnum\faauto\adjustright\itap0
{\f1\fs18 _______________________________________________________________________________________________________}\par

{\f1\fs18 \par }
\pard \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 A Mortgage Loan has been approved for the above noted Borrower(s). We have been advised 
that you will be acting for </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text> (hereinafter called the \ldblquote Lender\rdblquote ) in attending to the preparation, execution and 
registration of the Mortgage against the property and ensuring that the interests of the Lender 
as Mortgagee are valid and appropriately secured. Please note that the Lender will not require or 
approve an interim report on title or draft documentation, including the Mortgage. The Lender 
will rely solely on you to ensure that the Mortgage is prepared in accordance with the 
instructions outlined herein. Any amendments required due to errors, omissions or non-compliance 
on your part will be for your account and your responsibility to correct. The Mortgage may be 
registered electronically, where available.\par\par

The Lender will also rely solely on you to confirm the identity of the Borrower(s) and Guarantor/
Covenantor, if any, and to retain the evidence used to confirm their identity in your file. To 
comply with anti-money laundering regulations, </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text> must be supplied with 
verification of mortgagor(s) identity, and guarantor(s) as applicable, on 
all mortgages.  The enclosed \ldblquote Mortgage \emdash  ID Verification 
Form\rdblquote  must be completed and returned with the 
Preliminary Report/Request for Funds.  Failure to supply this information will result in mortgage 
disbursements being delayed or rejected for which the solicitor assumes full responsibility. 
Solicitor must certify that all mortgage documentation has been executed by the proper parties 
thereto. The Solicitor must obtain photo identification by way of valid Driver's License, Health 
Card, Passport and must retain copies of same in his file and produce copies of same forthwith 
upon demand by </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text>. The solicitor's certification must state 
that the Applicants/Mortgagors/Guarantors/Covenantors set out in the mortgage and in the 
Agreement of Purchase and Sale are one and the same persons who have executed the documentation.\par\par

Any material facts which may adversely affect the Lender's position as a First Charge 
on the property are to be disclosed to the Lender prior to the advance of any funds.  Similarly, 
any issues which may affect the Lender's security or any subsequent amendments to the Agreement 
of Purchase and Sale are to be referred to this office for direction prior to the release of 
funds. Our prior consent will be required relative to any requests for additional or secondary 
financing. If for any reason you are unable to register our Mortgage on the scheduled closing 
date, we must be notified at least 24 hours prior to that date and advised of the reason for the 
delay. No advance can be made after the scheduled closing date without the Lender's written 
authorization.\par\par

You must submit the Solicitor\rquote s Request for Funds together with the required 
documentation to<!--#DG392 our--> Mortgage Services {\f1\fs18\ul no later than three (3) days prior 
to closing}. The Solicitor\rquote s Request for Funds should contain confirmation that all our 
requirements have been or will be met and should specifically outline any qualifications that 
will appear in your Final Report and certification of title.\par\par

<!--#DG392 deleted duplicate text -->
The mortgage funds will be sent to you by PREPAID courier, or other methods as determined by </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text>, and should be disbursed without delay provided you are satisfied that all 
requirements have been met and adequate precaution has been taken to ensure that the mortgage 
security will retain priority.\par\par

Legal fees and all other costs, charges and expenses associated with this transaction 
are payable by the Borrower(s) whether or not the Mortgage proceeds are advanced.\par\par

In connection with this transaction we enclose the following documents:}\par\par
<!--
\trowd \trgaph108\trleft-108 
\cellx630
\cellx1200
\cellx10908
\pard\plain \intbl \f1\fs18 \cell \pard \intbl
\pard \intbl{\f14\fs18 l}\cell  \pard \intbl{\f1\fs18\b\ul Mortgage Commitment Letter}
{\f1\fs18  - Please ensure that any outstanding conditions outlined in the Commitment Letter that 
are the responsibility of the Solicitor, are duly noted and that full compliance is confirmed 
prior to advance and included in your final report.}\cell 
\pard \intbl \row
-->
\trowd \trgaph108\trleft-108 
\cellx630
\cellx1200
\cellx10908
\pard\plain \intbl \f1\fs18 \cell 
\pard \intbl{\f14\fs18 l}\cell 
\pard \intbl{\f1\fs18\b\ul Solicitor Instructions and Guidelines}
{\f1\fs18  - Self explanatory}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108 
\cellx630
\cellx1200
\cellx10908
\pard\plain \intbl \f1\fs18 \cell 
\pard \intbl{\f14\fs18 l}\cell 
\pard \intbl{\f1\fs18\b\ul Schedules}
{\f1\fs18  - </xsl:text>
    <!-- :BASIS:scheduletextcoop:ENDBASIS:-->
		<xsl:for-each select="/*/CommitmentLetter/BrokerConditions/Condition[conditionId = 374]">
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:text>}\cell 
\pard \intbl 
\row

\trowd \trgaph108\trleft-108 
\cellx630
\cellx1200
\cellx10908
\pard\plain \intbl \f1\fs18 \cell 
\pard \intbl{\f14\fs18 l}\cell 
\pard \intbl{\f1\fs18\b\ul Provincial/Other Requirements}
{\f1\fs18 - </xsl:text>
    <!-- :BASIS:provincialclause:ENDBASIS:-->
    <!--#DG430 added disclosure line -->
		<xsl:for-each select="/*/SolicitorsPackage/ProvincialClause/Line">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
    <!--#DG430 let's try some qk dirty fix for ON and BC ... -->
		<xsl:for-each select="/*/CommitmentLetter/BrokerConditions/Condition[conditionId = 325
      or conditionId = 329 or conditionId = 330]/Line">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:for-each select="/*/SolicitorsPackage/Disclosure/Line">
			<xsl:value-of select="."/>
		</xsl:for-each>

		<!-- cuz of the position test below, this join won't work ...!!! how come??
    xsl:for-each select="/*/SolicitorsPackage/ProvincialClause/Line 
      | /*/SolicitorsPackage/Disclosure/Line"> 
			<xsl:value-of select="."/>
			<xsl:if test="not(position() = last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each-->

		<xsl:text>}\cell 
\pard \intbl \row
\pard \par

{\f1\fs18 Other required security/Special Conditions: \par}</xsl:text>
        <!--Reverse #DG392, as of ticket#4973-->
		<xsl:value-of select="/*/Deal/solicitorSpecialInstructions"/>

		<!--#DG392 xsl:for-each select="/*/CommitmentLetter/BrokerConditions/Condition[conditionId = 336]"-->
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition[conditionId = 0
      or conditionId = 8 or conditionId = 9 or conditionId = 12 or conditionId = 23
      or conditionId = 56 or conditionId = 292 or conditionId = 306 or conditionId = 362
      or conditionId = 414]"> 
			<xsl:text>
{\f1\fs18 - </xsl:text>
			<xsl:for-each select="Line">
				<xsl:value-of select="."/>
			</xsl:for-each>
			<xsl:text>\par }</xsl:text>
		</xsl:for-each>
		
		<xsl:text>\par

{\f1\fs18 We consent to your acting for the Lender as well as the Borrower(s) and/or </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/GuarantorClause"/>
		<xsl:text> provided that you disclose this fact to the Borrower(s) and/or </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/GuarantorClause"/>
		<xsl:text> and obtain their consent in writing and that you disclose to each party all information you 
possess or obtain which is or may be relevant to this transaction. }\par\par

{\f1\fs20\b\ul <!--#DG392 MORTGAGE SERVICES-->FINANCIAL SERVICES }\par\par

{\f1\fs18 Documents and funding requests should be directed to:}\par\par

{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
		<xsl:text>\par </xsl:text>

		<!--#DG424 xsl:value-of select="/*/SolicitorsPackage/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:if test="/*/SolicitorsPackage/BranchAddress/Line2">
		  <xsl:value-of select="/*/SolicitorsPackage/BranchAddress/Line2"/>
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/SolicitorsPackage/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/BranchAddress/Province"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/BranchAddress/Postal"/-->
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
      <xsl:call-template name="ContactAddress"/>
		</xsl:for-each>
		
		<xsl:text>\par\par}

{\f1\fs18 Contact: </xsl:text>
		<xsl:for-each select="//Deal/UserProfile[userProfileId=//Deal/administratorId]/Contact">
		  <xsl:call-template name="ContactName"/>
			
			<xsl:text>}\par\par 
{\f1\fs18 Phone:}\tab {\f1\fs18 1-800-788-6311 Ext. </xsl:text>
			<xsl:value-of select="contactPhoneNumberExtension"/>
  		<xsl:text>}\par
{\f1\fs18 Fax:}\tab {\f1\fs18 1-800-575-4013}\par\par</xsl:text>
		</xsl:for-each>
		<xsl:text>{\f1\fs18 If you have any question or concerns, please feel free to call.}\par\par

{\f1\fs18 Sincerely,}\par\par\par

{\f1\fs18 </xsl:text>
		<xsl:for-each select="//Deal/UserProfile[userProfileId=//Deal/administratorId]/Contact">
		  <xsl:call-template name="ContactName"/>
		</xsl:for-each>
		<xsl:text>}\par
{\f1\fs18 Financial Services Administrator}\par
\pard \par 

\page 
</xsl:text>
	</xsl:template>
	
	<!-- ************************************************************************ 	-->	
	<xsl:template name="EnglishGuide">
		<xsl:text>\par{\f1\fs28\b\ul SOLICITOR'S GUIDE \par }
{\f1\fs18 If you are satisfied that the Mortgagor(s) (also known as the 
\ldblquote Borrower(s)\rdblquote ) has or will acquire a good and marketable title to the 
property described, complete our requirements as set out below, and prepare a Mortgage as 
outlined herein.}\par\par

{\f1\fs18\b\ul Documentation:  }{\f1\fs18 The Mortgage Form Number identified in our cover letter 
together with the applicable Schedule(s) must be registered. }\par\par

{\f1\fs18\ul\b Mortgage Requirements: }{\f1\fs18 The mortgage Document is to be registered as 
follows:}\par\par

\pard \ql \li0\ri0\widctlpar\tx100\tx6000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
{\tab {\f1\fs18 Approved Loan Amount: } \tab {\f1\fs18 </xsl:text>
		<!--#DG418 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>		
		<xsl:text>}\par\par

\tab {\f1\fs18 Interest Rate (Compounded </xsl:text>
    <!-- #DG392 xsl:value-of select="/*/SolicitorsPackage/CompoundingFrequency"/-->
  	<xsl:choose>
  		<xsl:when test="/*/Deal/interestCompound=12">
  		  <xsl:text>Monthly</xsl:text>
  		</xsl:when>
  		<xsl:otherwise>
  		  <xsl:text>Semi-Annually</xsl:text>
  		</xsl:otherwise>
  	</xsl:choose>		
		
		<xsl:text>): \tab </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/InterestRate"/>
		<xsl:text>}\par\par

\tab {\f1\fs18 Principal and Interest Payment: } \tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/PandIPaymentMonthly"/>
		<xsl:text>}\par\par

\tab {\f1\fs18 Interest Adjustment Date: } \tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/IADDate"/>
		<xsl:text>}\par\par

\tab {\f1\fs18 First Monthly Payment Date: } \tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/FirstPaymentDateMonthly"/>
		<xsl:text>}\par\par

\tab {\f1\fs18 Maturity/Renewal Date: } \tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/MaturityDate"/>
		<xsl:text>}\par\par
}\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{\par}

{\f1\fs18 {\f1\fs18\b\ul Searches:}  </xsl:text>
		<!--#DG392 xsl:value-of select="/*/SolicitorsPackage/Execution"/-->
		<xsl:text>You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. Prior to releasing any mortgage proceeds, the solicitor will carry out the necessary searches with respect to any liens, encumbrances, executions; that may be registered against the property. It is your responsibility to ensure that there are no work orders or deficiency notices outstanding against the property, that all realty taxes and levies which have or will become due and payable up to the interest adjustment date are paid to the Municipality. If your search reveals title has been transferred since the date of the Offer to Purchase and before the closing date you must notify us immediately and discontinue all work on this transaction unless notified otherwise. Immediately prior to registration of the mortgage, you are to obtain a Sheriff’s Certificate/General Registry Search indicating that there are no writs of execution on file against the mortgagor(s), guarantor(s), if any, or any previous owner, which would adversely affect our security.
}\par\par

{\f1\fs18 We require that Title to the subject property be in the name(s) of the mortgagor(s) only. If Title will be held in name(s) other than shown on the mortgage document, this mortgage may not proceed and we ask that you advise our office immediately.
}\par\par

{\f1\fs18 {\f1\fs18\b\ul Condo/Strata Title:} You are to confirm that the Condominium 
Corporation is registered and has maintained adequate Fire Insurance. You are to review the 
Condominium Corporation's Declaration and Bylaws and confirm they contain nothing derogatory to 
our security. You are to assign the voting rights to </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text> (if applicable).}\par\par

{\f1\fs18\b\ul Survey:}{\f1\fs18 \~ For mortgages $250,000.00 or more, a Surveyor\rquote s 
Certificate or Title Insurance (acceptable to Concentra Financial Services Association) is 
required. The cost of this certificate or insurance, if any, will be the responsibility of the 
borrower. }\par\par

{\f1\fs18\b\ul Fire Insurance:}
{\f1\fs18 \~ You must ensure, prior to advancing funds, that fire 
insurance coverage for building(s) on the property is in place for not less than their full 
replacement value with any loss payable to </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text> as Mortgagee at the address <!--#DG424 shown on the Charge/Mortgage-->
located at </xsl:text>      
  		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
    		<xsl:call-template name="ContactAddress">
		      <xsl:with-param name="separa" select="', '"/>
		      <xsl:with-param name="separaPostal" select="', '"/>
	      </xsl:call-template>
  		</xsl:for-each>		
			
			<xsl:text> and including a duly completed 
standard mortgage clause approved by the Insurance Bureau of Canada. Policies must not contain a 
co-insurance or similar clause that may limit the amount payable. Unless otherwise noted, we 
will not require a copy of this policy.} \par\par

{\f1\fs18\b\ul Matrimonial Property Act:}{\f1\fs18 \~ You are to certify that all requirements 
under the Matrimonial Property Act in your province, where applicable, have been met and that 
the status does not in any way affect our mortgage/charge.} \par\par

{\f1\fs18\b\ul Advances:}{\f1\fs18 \~ When all conditions precedent to this transaction have 
been met, funds in the amount of  </xsl:text>
		<!--#DG418 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<!--  MCM Impl Team 15-Jul-2008 XS_16.6 Added a check for displaying '_TBD_' Starts-->
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId!=0">
		<xsl:text>_TBD_</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/Deal/totalLoanAmount"/></xsl:otherwise>
		</xsl:choose>				
		<!-- MCM Impl Team 15-Jul-2008 XS_16.6 Added a check for displaying '_TBD_' Ends-->
		<xsl:text> will be advanced with the following costs being deducted 
from the advance by the Lender;} \par\par

\pard \ql \li0\ri0\widctlpar\tx1000\tqr\tx9000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
{\tab {\f1\fs18 Mortgage Insurance Premium } \tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/Premium"/>
		<xsl:text>}\par
\tab {\f1\fs18 (included in Total Loan Amount )}\par\tab {\f1\fs18 Provincial Sales Tax 
on Premium } \tab {\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PST"/>
		<xsl:text>}\par}</xsl:text>
		
		<xsl:for-each select="//SolicitorsPackage/Fees/Fee">
			<xsl:text>\pard \ql \li0\ri0\widctlpar\tx1000\tqr\tx9000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
{\f1\fs18\tab </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\tab </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>}\par</xsl:text>
		</xsl:for-each>
		
		<xsl:text>{\f1\fs18 \tab TOTAL \tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/TotalDeductionsAmount"/>
		<xsl:text>}\par\par

{\f1\fs18 \tab Interest Adjustment (daily per diem) \tab </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PerDiemInterestAmount"/>
		<xsl:text>\par}
\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{ \par }
<!-- MCM Impl Team 15-Jul-2008 XS_16.6 Added a check for displaying '_TBD_' Starts -->
{\f1\fs18 The net Mortgage proceeds in the amount of </xsl:text>
<xsl:choose>
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId!=0">
		<xsl:text>_TBD_</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/Deal/totalLoanAmount"/></xsl:otherwise>
		</xsl:choose>
<xsl:text>
<!--MCM Impl Team 15-Jul-2008 XS_16.6 Added a check for displaying '_TBD_' Ends -->
, payable to you, in trust, will be sent to your office prior 
to the scheduled closing date, which is anticipated to be </xsl:text>
		<!--#DG418 xsl:value-of select="//SolicitorsPackage/NetAdvanceAmount"/-->
		<xsl:value-of select="/*/Deal/estimatedClosingDate"/>
		<xsl:text>. }{ \par\par }</xsl:text>
		
		<!-- NOTE: Refinances internal and external never go together -->
		<!-- One of them must be commented out -->
		<!-- In this case both of them must be commented out -->
		<!-- Refinances internal<RptText idcode="REFInternalPart1"></RptText><DocSection idcode="liabilities"><RptText idcode="REFInternalPart2"></RptText></DocSection><RptText idcode="REFInternalPart3"></RptText> -->
		<!-- Refinances external<RptText idcode="REFExternalPart1"></RptText><DocSection idcode="liabilities"><RptText idcode="REFExternalPart2"></RptText></DocSection><RptText idcode="REFExternalPart3"></RptText>  -->

		<!--TemplatePortion idcode="REFExternalPart3">}\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{ \par }{\f1\fs18 The balance, if any, is to be paid to the Borrower(s).}\par\par</TemplatePortion>
		<TemplatePortion idcode="REFExternalPart2">\tab {\f1\fs18 :BASIS:liabilityverb:ENDBASIS: } \tab {\f1\fs18 :BASIS:liabilityamount:ENDBASIS:}\par</TemplatePortion>
		<TemplatePortion idcode="REFExternalPart1">{\f1\fs18\b\ul Refinances: }{\f1\fs18 If the purpose of this Mortgage is to refinance existing debt, the balance of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.}\par\par\pard \ql \li0\ri0\widctlpar\tx2000\tqr\tx7000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{\tab {\f1\fs18 Existing :BASIS:lpdescription:ENDBASIS: Mortgage } \tab {\f1\fs18 :BASIS:existingloanamount:ENDBASIS:}\par\tab {\f1\fs18\b Other debts to be paid } \tab {\f1\fs18\b Amount:}\par</TemplatePortion>
		<TemplatePortion idcode="REFInternalPart3">}\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{ \par }{\f1\fs18 The balance, if any, is to be paid to the Borrower(s).}\par\par</TemplatePortion>
		<TemplatePortion idcode="REFInternalPart2">\tab {\f1\fs18 :BASIS:liabilityverb:ENDBASIS: } \tab {\f1\fs18 :BASIS:liabilityamount:ENDBASIS:}\par</TemplatePortion>
		<TemplatePortion idcode="REFInternalPart1">{\f1\fs18\b\ul Refinances: }{\f1\fs18 If the purpose of this Mortgage is to refinance existing debt with :BASIS:lendername:ENDBASIS:, we will retain the following additional funds to discharge existing mortgage(s) owing to us;}\par\par\pard \ql \li0\ri0\widctlpar\tx2000\tqr\tx7000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{\tab {\f1\fs18 Existing :BASIS:lendername:ENDBASIS: Mortgage } \tab {\f1\fs18 :BASIS:existingloanamount:ENDBASIS:}\par}\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{ \par }{\f1\fs18 The balance of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below.}\par\par\pard \ql \li0\ri0\widctlpar\tx2000\tqr\tx7000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{\tab {\f1\fs18\b Other debts to be paid: } \tab {\f1\fs18\b Amount:}\par</TemplatePortion>
		-->
		

		<!--  Internal refi -->
		<xsl:if test="//SolicitorsPackage/InternalRefi">
			<xsl:text>
\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0
{\b\f1\fs18\ul Refinances:}{\f1\fs18 If the purpose of this Mortgage is to refinance existing debt with </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text>, we will retain the following additional funds to discharge existing mortgage(s) owing to us;\par }
\pard \qj \keep\keepn\fi720\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Existing </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LenderName"/>
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b Other Debts to be Paid:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \keep\keepn\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
</xsl:text>
			</xsl:for-each>
			<xsl:text>\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance, if any, is to be paid to the Borrower(s).
\par }
\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\par \par}
</xsl:text>
		</xsl:if>

		<!--  External refi -->
		<xsl:if test="//SolicitorsPackage/ExternalRefi">
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs18 \ul Refinances:}{\f1\fs18  If the purpose of this Mortgage is to refinance existing debt, the balance
 of the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Existing </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/LPDescription"/>
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b Other Debts to be Paid:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs18 \par The balance, if any, is to be paid to the Borrower(s).
\par \par \par}</xsl:text>
		</xsl:if>


		<xsl:text>{\f1\fs18\b\ul Construction/Progress Advances:}{\f1\fs18 \~ If the security for this Mortgage is 
a new property and we are advancing by way of progress advances, the advances will be done on a 
cost-to-complete basis. You are responsible for the disbursement of all loan advances made 
payable to you in trust and to ensure the priority of each advance by conducting all necessary 
searches and ensure that the requirements of the appropriate Provincial Construction Lien Act 
have been met before each advance is made. You are to retain appropriate amounts from the 
Mortgage proceeds to comply with Provincial lien holdback requirements. Unless otherwise noted, </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text> will require a copy of the executed Certificate of Completion and Possession form 
and/or the Occupancy Certificate.}\par\par

{\f1\fs18\b\ul Disclosure:}{\f1\fs18 \~ The Mortgagor(s) and the </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text> (if any) must sign a Statement of Disclosure. One copy is to be given to the 
Borrower(s) and a second copy is to be forwarded to </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text>. Please ensure that the applicable provincial legislation and timeframes regarding 
Disclosure Statements are adhered to. } \par\par

{\f1\fs18\b\ul Planning Act:}{\f1\fs18 \~ You must ensure that the Mortgage does not contravene 
the provisions of the Planning Act as amended from time to time, and that the Mortgagor(s) do not 
retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a 
power of appointment with respect to any land abutting the land secured by the Mortgage.} \par\par

</xsl:text>
<!-- 		<TemplatePortion idcode="SGRenewalForGuaranators">{\f1\fs18\b\ul Renewal for Guarantor(s):}{\f1\fs18 \~ If applicable, the following wording must be included in a Schedule to the Charge/Mortgage: \par}{\f1\fs18 "In addition to the :BASIS:GuarantorClause:ENDBASIS:'s promises and agreements contained in this Mortgage, the :BASIS:GuarantorClause:ENDBASIS:(s) also agree(s) that at the sole discretion of :BASIS:LenderName:ENDBASIS:, the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s)."}\par\par</TemplatePortion>
    -->
		<xsl:text>{\f1\fs18\b\ul Corporation as a Borrower:}{\f1\fs18 \~ If the Mortgagor(s) or </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/GuarantorClause"/>
		<xsl:text>(s) is a corporation, you must obtain a certified copy of the directors' resolution 
and the borrowing by-law of the corporation authorizing the Charge/Mortgage and ensure that the 
corporation is duly incorporated under applicable Provincial or Federal laws with all necessary 
power to charge its interest in the property and include these documents(s) with the Solicitor's 
Final Report on Title.} \par\par

{\f1\fs18\b\ul Solicitor\rquote s Report:}{\f1\fs18 \~ You must submit the Solicitor's Final 
Report on Title, on the enclosed form and with the stated enclosures, within 60 days after the 
final advance is made. If your report cannot be submitted in this time frame, you must provide us 
with a letter explaining the reason(s) for the delay. Failure to comply with any of the above 
instructions may result in discontinued future dealings with you or your firm.}\par\par

\pard \par 

\page

</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishRequestForFunds">
		<xsl:text>\par\trowd \trgaph108\trleft1500
\trbrdrt\brdrs\brdrw15\brdrcf1 \trbrdrl\brdrs\brdrw15\brdrcf1 \trbrdrb\brdrs\brdrw15\brdrcf1 
\trbrdrr\brdrs\brdrw15\brdrcf1 \trbrdrh\brdrs\brdrw15\brdrcf1 \trbrdrv\brdrs\brdrw15\brdrcf1 
\clbrdrt\brdrs\brdrw15\brdrcf1 \clbrdrl\brdrs\brdrw15\brdrcf1 \clbrdrb\brdrs\brdrw15\brdrcf1 
\clbrdrr\brdrs\brdrw15\brdrcf1 \cellx8000 
\pard \qc\intbl\par {\f1\fs28\b\ul SOLICITOR\rquote S REQUEST FOR FUNDS}\par \cell 
\pard \intbl \row

\pard \par
{\f1\fs18 _______________________________________________________________________________________________________}\par
\pard \ql \fi720\li5040\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin5040\itap0 
{\f1\fs18\par}

\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 FAX TO:\cell \b0</xsl:text>
		<xsl:value-of select="//SolicitorsPackage/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 FROM:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Name"/>
		<xsl:value-of select="//SolicitorsPackage/SolicitorAddress1"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 
\cellx4745\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ATTENTION:\cell \b0
</xsl:text>
		<!--#DG470 xsl:value-of select="//SolicitorsPackage/FunderName"/-->
		<xsl:for-each select="/*/Deal/UserProfile[userProfileId=/*/Deal/administratorId]/Contact">
		  <xsl:call-template name="ContactName"/>
		</xsl:for-each>
		<xsl:text>\cell \cell }
{\f1\fs18 </xsl:text>

		<!--#DG424 xsl:value-of select="//SolicitorsPackage/Solicitor/Address1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Address2"/>
		<xsl:if test="//SolicitorsPackage/Solicitor/Address2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:text>}\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressCity"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressProvince"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/AddressPostal"/-->
		<xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50]/Contact/Addr">
  		<xsl:call-template name="ContactAddress"/>
 		</xsl:for-each>		
		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\pard \ql \sa120\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 DATE:\cell }{\f1\fs18 ________________\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Phone: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Phone"/>
		<xsl:text>\par Fax: </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/Solicitor/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph57\trleft-115\trftsWidth1\trautofit1\trpaddl115\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth1440 \cellx1325
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx4745
\cltxlrtb\clftsWidth3\clwWidth900 \cellx5645
\cltxlrtb\clftsWidth3\clwWidth5148 \cellx10793
\row 
}
\pard
{\f1\fs18 _______________________________________________________________________________________________________}\par
\pard\plain \s2\ql \keepn\widctlpar\tx1260\aspalpha\aspnum\faauto\outlinelevel1\adjustright\itap0 
{\f1\fs18 \par }

\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGE REFERENCE NUMBER:\cell \b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/ReferenceNum"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGOR(S):\cell \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MUNICIPAL ADDRESS:\cell \b0 
</xsl:text>

		<!--#DG424 xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line1"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line1">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Line2"/>
		<xsl:if test="//SolicitorsPackage/PropertyAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/PropertyAddress/Province"/-->
    <xsl:call-template name="propAdrs"/><!--#DG430 -->	
		
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGE AMOUNT:\cell \b0 </xsl:text>
		<!--#DG418 xsl:value-of select="//SolicitorsPackage/LoanAmount"/-->
		<xsl:value-of select="/*/Deal/totalLoanAmount"/>		

		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\pard\plain \s15\ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 CLOSING DATE:\cell \b0 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/AdvanceDate"/>
		<xsl:text>\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx4140
\cltxlrtb\clftsWidth3\clwWidth6660 \cellx10800
\row 
}
\pard

{\f1\fs18 _______________________________________________________________________________________________________}\par
\par
<!--MCM Impl Team 15-Jul-2008 XS_16.6 Added '$__' Starts -->
<!--MCM Impl Bug fix for artf750550,artf750527 starts -->
{\f1\fs18\b I/We request funds in the amount of 
</xsl:text>
<xsl:choose>
<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId!=0">
<xsl:text> &lt;$__________________ &gt; </xsl:text>
</xsl:when>
<xsl:otherwise><xsl:value-of select="/*/Deal/totalLoanAmount"/></xsl:otherwise>
</xsl:choose>
<xsl:text>
 for closing on ______________________.}\par\par
 <!--MCM Impl Bug fix for artf750550,artf750527 Ends -->
<!--MCM Impl Team 15-Jul-2008 XS_16.6 Added '$__' Ends -->
{\f1\fs18\b I/We acknowledge that there are no amendments and/or schedules to the Agreement of 
Purchase and Sale which affect the purchase price or deposit amount.}\par\par

{\f1\fs18\b I/WE ADVISE AND CERTIFY THAT:}\par\par

\trowd \trgaph108\trleft-108 
\cellx360
\cellx900
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell 
\pard \intbl{\f1\fs18 All terms and conditions set out in your instructions have been satisfied.}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108 
\cellx360
\cellx900
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell 
\pard \intbl{\f1\fs18 All documents required prior to release of the mortgage proceeds have been 
forwarded with the Solicitor\rquote s Request for Funds for </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text> to review. Written authorization from </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text> will be required in order to release the mortgage proceeds.}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108 
\cellx360
\cellx900
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell 
\pard \intbl{\f1\fs18 The Mortgagor's new mailing address is as follows:\line
____________________________________________________________________________\line
____________________________________________________________________________\line}\cell 
\pard \intbl \row

\pard\par{\f1\fs18\b DELIVERY OF FUNDS IS REQUESTED AS FOLLOWS}\par
\trowd \trgaph108\trleft-108 
\cellx360
\cellx900
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell 
\pard \intbl{\f1\fs18 Deliver funds courier collect via 
__________________________________________________ courier account #
______________________________________________________________}\cell 
\pard \intbl \row 

\trowd \trgaph108\trleft-108 
\cellx360
\cellx900
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell 
\pard \intbl{\f1\fs18 Deposit/Wire funds to my/our Trust Account (see attached VOID cheque)}\cell 
\pard \intbl \row 

\trowd \trgaph108\trleft-108 
\cellx360
\cellx900
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell 
\pard \intbl{\f1\fs18 Bank Name: _________________________________________________________________}\cell 
\pard \intbl \row 

\trowd \trgaph108\trleft-108 
\cellx360
\cellx900
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell 
\pard \intbl{\f1\fs18 Account #: ______________________ Route and Transit: ____________________________}\cell 
\pard \intbl \row 

\pard\par
<!--{\f1\fs18\b * If this last option is chosen, I/We understand that you will retain sufficient 
funds to cover all wire costs.}\par-->\par

\pard \ql \li0\ri0\widctlpar\tx5000\tx7000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
{\tab {\f1\fs18 ___________________________________________________ }\par
\tab\tab {\f1\fs18 Signature of Solicitor}\par}
\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{ \par }

\pard \par 

\page 

\par
{\f1\fs18 DOCUMENTS REQUIRED PRIOR TO RELEASE OF THE MORTGAGE PROCEEDS AND ATTACHED HERETO:}\par\par

\pard \ql \li0\ri0\widctlpar\tx50\tx200\tx400\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
{\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Pre-Authorized Chequing (PAC) form together with "VOID" cheque   } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Statement of Disclosure   } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Statutory Declaration    } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Mortgage ID Verification Form } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Western Protocol Solicitor’s Opinion } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Signed Authorization to Send Assessment and Tax Bill (#45-06-20) PEI } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Personal Guarantee and Letter of Independent Legal Advice } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Assignment of Site / Lot Lease  } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Assignment of Condominium Voting Rights (NB, NF, PEI) } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Estoppel Certificate (in BC-Section 59 or Status Certificate; in MB both-Form 1: Disclosure Certificate and Form 2: Status Certificate are required) } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Verification of Payout of Debts Prior to Release of Mortgage Proceeds } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Interim Financing Documents: Commitment Letter, Assignment of Proceeds, Letter of Comfort  } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Well Water Test / Certificate, indicating that the water is potable and fit for human consumption    } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Septic Tank Certificate  } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Final Inspection } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Occupancy Certificate / Permit } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 New Home Warranty Certificate of Possession  } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 New Home Warranty Builder's Registration Number and Unit Enrollment Number  } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 Other (specify) } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 
_______________________________________________________________________________________ } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 
_______________________________________________________________________________________ } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 
_______________________________________________________________________________________ } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 
_______________________________________________________________________________________ } \par
\tab {\f1\fs18 [} \tab {\f1\fs18 ]} \tab {\f1\fs18 
_______________________________________________________________________________________ } \par}

\page 

</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishFinalReport">
		<xsl:text>\pard\f1\fs18\par
\trowd \trgaph108\trleft1500
\trbrdrt\brdrs\brdrw15\brdrcf1 \trbrdrl\brdrs\brdrw15\brdrcf1 \trbrdrb\brdrs\brdrw15\brdrcf1 
\trbrdrr\brdrs\brdrw15\brdrcf1 \trbrdrh\brdrs\brdrw15\brdrcf1 \trbrdrv\brdrs\brdrw15\brdrcf1 
\clbrdrt\brdrs\brdrw15\brdrcf1 \clbrdrl\brdrs\brdrw15\brdrcf1 \clbrdrb\brdrs\brdrw15\brdrcf1 
\clbrdrr\brdrs\brdrw15\brdrcf1 \cellx8000 
\pard \qc\intbl\par {\f1\fs28\b\ul SOLICITOR\rquote S FINAL REPORT ON TITLE}\par \cell 
\pard \intbl \row

\pard \ql \widctlpar\faauto\itap0 
\f1\fs18 \par \par 
\pard \ql \widctlpar\tx100\tx7000\aspalpha\aspnum\faauto\adjustright\itap0 
{\tab }{\b\f1\fs18 Solicitor/Notary\rquote s Reference No. ____________________________ }{\tab }
{\b\f1\fs18 _________________________}{\par 
\tab \tab }{\b\f1\fs18 Date\par\par}

\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth918 \cellx810
\cltxlrtb\clftsWidth3\clwWidth10098 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 TO:\cell </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>
\par </xsl:text>

		<!--#DG424 xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/-->
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
      <xsl:call-template name="ContactAddress"/>
		</xsl:for-each>
		
		<xsl:text>
\par \par Attention: </xsl:text>
		<xsl:for-each select="//Deal/UserProfile[userProfileId=//Deal/administratorId]/Contact">
		  <xsl:call-template name="ContactName"/>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth918 \cellx810
\cltxlrtb\clftsWidth3\clwWidth10098 \cellx10908
\row 
}

\pard \par
{\f1\fs18 In accordance with your instructions, we have acted as solicitors in the following 
transaction. We have registered the "Mortgage" (Mortgage, Charge or Deed of Loan) on the 
appropriate form in the appropriate </xsl:text>
		<!--#DG392 xsl:value-of select="/*/SolicitorsPackage/LandOfficeClause"/-->
		<xsl:text>Land Titles/Registry Office</xsl:text>

		<xsl:text> and make our final report as follows:}\par\par

\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Mortgagor(s)\cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/SolicitorsPackage/GuarantorClause"/>
		<xsl:text>\cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Municipal Address of property \cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Legal Address of Property \cell ______________________________________________________________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth3258 \cellx3150
\cltxlrtb\clftsWidth3\clwWidth7758 \cellx10908
\row 
}
\pard \par\par

{\f1\fs18\b It is our opinion that:}\par
\trowd \trgaph108\trleft-108 
\cellx720
\cellx10908 
\pard\plain \intbl \f1\fs18{\f1\fs18 (1)}\cell
\pard \intbl {\f1\fs18 A valid and legally binding </xsl:text>
		<xsl:value-of select="/*/Deal/lienPosition"/>
		<xsl:text> Mortgage in favour of </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text> for the full amount of the monies advanced was registered on  
_____________________________________________ in the </xsl:text>
		<!--#DG392 xsl:value-of select="/*/SolicitorsPackage/LandOfficeClause"/-->
		<xsl:text>Land Titles/Registry Office</xsl:text>
		
		<xsl:text> Division of _____________________________________________________ as Instrument 
No. ___________________________.}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108 
\cellx720
\cellx10908 
\pard \intbl {\f1\fs18 (2)}\cell \pard \intbl{\f1\fs18 Solicitor certifies that all mortgage 
documentation has been executed by the proper parties thereto. The solicitor has obtained photo 
identification by way of valid Driver’s License, Health Card, Passport and has retained copies 
of same in his file and will produce copies of same forthwith upon demand by </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text>. The solicitor certifies that the Applicants/Mortgagors/Guarantors/Covenantors 
set out in the mortgage and in the Agreement of Purchase and Sale are one and the same persons 
who have executed the documentation.}\par\cell \pard \intbl \row 

\pard \trowd \trgaph108\trleft-108 
\cellx720
\cellx10908 
\pard \intbl {\f1\fs18 (3)}\cell \pard \intbl{\f1\fs18 The Mortgagor(s) have good and marketable 
title in fee simple to the property free and clear of any prior encumbrances, other than the 
minor defects listed below which {\f1\fs18\b do not} affect the priority of the Mortgage or the 
marketability of the property. All lien holdback/retention period requirements have been met. 
Easements, Encroachments and Restrictions etc. are listed below:}\par
{\f1\fs18 ________________________________________________________________________________________________ }\par
{\f1\fs18 ________________________________________________________________________________________________ }\par
{\f1\fs18 ________________________________________________________________________________________________ }\par
{\f1\fs18 ________________________________________________________________________________________________ }\par
{\f1\fs18 ________________________________________________________________________________________________ }\par\cell 
\pard \intbl \row 

\pard{\f1\fs18\b Condominium/strata unit(s) if applicable:} \par
\trowd \trgaph108\trleft-108
\cellx630
\cellx1200
\cellx10908
\pard\plain \intbl\cell \pard \intbl{\f1\fs18 3 a)}\cell \pard \intbl{\f1\fs18 We confirm that 
the Condominium Corporation is registered and has maintained adequate Fire Insurance. We have 
reviewed the Condominium Corporation's Declaration and Bylaws and confirm they contain nothing 
derogatory to your security. We have assigned the voting rights to </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text>, if applicable.}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108
\cellx630
\cellx1200
\cellx10908
\pard\plain \intbl\cell \pard \intbl{\f1\fs18 b)}\cell \pard \intbl{\f1\fs18 All necessary steps 
have been taken to confirm your right to vote should you wish to do so.}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108
\cellx630
\cellx1200
\cellx10908
\pard\plain \intbl\cell \pard \intbl{\f1\fs18 c)}\cell \pard\intbl {\f1\fs18 Applicable notice 
provision, if any: __________________________________________________} \cell 
\pard \intbl \row

\pard \par
\trowd \trgaph108\trleft-108 
\cellx630
\cellx10908 
\pard \intbl{\f1\fs18 (4)}\cell \pard \intbl{\f1\fs18 All restrictions have been complied with in 
full and there are no work orders or deficiency notices outstanding against the property.}\cell 
\pard \intbl \row 

\trowd \trgaph108\trleft-108 
\cellx630
\cellx10908 
\pard \intbl{\f1\fs18 (5)}\cell \pard \intbl{\f1\fs18 Solicitor certifies all taxes and levies 
due and payable on the property to the municipality have been paid up to and including the date 
of funding.}\cell 
\pard\intbl \row 

\pard \intbl{\f1\fs18 (6)}\cell \pard \intbl{\f1\fs18 The Mortgage does not contravene the 
provisions of the Planning Act as amended from time to time, because the Mortgagor(s) do not 
retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a 
power of appointment with respect to any land abutting the land secured by the Mortgage.}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 (7)}\cell \pard \intbl{\f1\fs18 A true copy of the Mortgage (including 
Standard Charge Terms, all Schedules to it, and the Mortgagor(s) Acknowledgement and Direction, 
if applicable) has been given to each Mortgagor.} \cell 
\pard \intbl \row 

\trowd \trgaph108\trleft-108 
\cellx630
\cellx10908 
\pard \intbl{\f1\fs18 (8)}\cell \pard \intbl{\f1\fs18 Fire Insurance coverage has been arranged 
in accordance with your instructions for full replacement cost(s) with loss payable to </xsl:text>
			<xsl:value-of select="/*/SolicitorsPackage/LenderName"/>
			<xsl:text> in accordance with the I.B.C. standard mortgage clause.}\cell 
\pard \intbl \row 

\pard\par

\page 

\par
\trowd \trgaph108\trleft-108 
\cellx630
\cellx5769
\cellx10908 
\pard \intbl \cell \pard \intbl{\f1\fs18 Insurance Company: ______________________________ \par}\cell 
\pard \intbl{\f1\fs18 Broker: _______________________________________ \par}\cell 
\pard \intbl \row 

\trowd\trgaph108\trleft-108 
\cellx630
\cellx5769
\cellx10908 
\pard \intbl \cell \pard \intbl{\f1\fs18 Amount of Insurance: _____________________________ \par}\cell
{\f1\fs18 Policy No: _____________________________________ \par}\cell 
\pard \intbl \row 

\trowd \trgaph108\trleft-108 
\cellx630
\cellx5769
\cellx10908 
\pard \intbl \cell \pard \intbl{\f1\fs18 Effective Date: ___________________________________ \par}\cell
{\f1\fs18 Expiry Date: ____________________________________ \par}\cell 
\pard \intbl \row 

\pard\par\par

{\f1\fs18\b The following documents are enclosed for your file: }\par\par

\trowd \trgaph108\trleft-108 
\cellx270
\cellx720
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Duplicate registered copy of the Charge / Mortgage including Standard Charge Terms 
and all Schedules
}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108 
\cellx270
\cellx720
\cellx10908 
\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Electronic Charge and Acknowledgement and Direction  {\f1\fs18\b (Ontario electronic 
registration counties only)}}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Instrument 
Number {\f1\fs18 (Teranet Registration) }}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Certificate of Title / State of Title Certificate / Status of Title Certificate / 
Certificate of Registered Ownership }\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Zoning Certificate (Ontario)
 }\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Survey or Surveyor's Certificate or Title Insurance in lieu of a Survey, if applicable 
 }\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Registered 
Amending Agreement }\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Municipal Tax 
Certificate and/or Roll Number ______________________ (required whether or not title insurance 
has been obtained)}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Fire Insurance Policy}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Condominium Corporation Insurance Binder}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Declaration as to Possession (Manitoba properties – new purchases only)
 }\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 General Security Agreement (GSA) Registered under PPSA (Mobiles homes – except BC)
}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Verification that the General Security Agreement has been registered under PPSA
}\cell 
\pard \intbl \row

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Statement of Funds Received and Disbursed / Trust Ledger Statement }\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 Other (
specify)}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 
_______________________________________________________________________________________________}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 
_______________________________________________________________________________________________}\cell 
\pard \intbl \row 

\pard \intbl{\f1\fs18 [}\cell \pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 
_______________________________________________________________________________________________}\cell 
\pard \intbl \row

\trowd \trgaph108\trleft-108 \cellx270\cellx720\cellx10908\pard \intbl{\f1\fs18 [}\cell 
\pard \intbl{\f1\fs18 ]}\cell \pard \intbl{\f1\fs18 
_______________________________________________________________________________________________}\cell 
\pard \intbl \row 

\pard\par\par

\pard \ql \li0\ri0\widctlpar\tx5000\tx7000\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
{\tab {\f1\fs18 ___________________________________________________ }\par\tab\tab 
{\f1\fs18 Signature of Solicitor}\par}\pard \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0{ \par }
</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- French template section                                             			-->
	<!-- ************************************************************************ 	-->
	<!--	<xsl:template name="FrenchTemplate">
	</xsl:template>-->


	<!--table with logo and branch address -->
	<xsl:template name="LogoAddress">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx5269
\cltxlrtb\clftsWidth3\clwWidth5377 \cellx10646
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{\f1 </xsl:text>  
		<xsl:value-of select="$logoRtf"/>
		<xsl:text>\cell }
\pard \ql \clvertalc\widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{\f1\fs18 </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderName"/>
		<xsl:text>\par </xsl:text>
		
		<!--#DG424 xsl:if test="//CommitmentLetter/BranchAddress/Line1">
  		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/-->
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
      <xsl:call-template name="ContactAddress"/>
		</xsl:for-each>
		
		<xsl:text>\par Tel: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par Fax: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustleft 
{
\trowd \trrh1266\trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvertalc\cltxlrtb\clftsWidth3\clwWidth3377 \cellx5269
\clvertalc\cltxlrtb\clftsWidth3\clwWidth7522 \cellx15646
\row 
}</xsl:text>	
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>{\f1\par }}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff63\deflang1033\deflangfe1033
{\fonttbl
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f14\fnil\fcharset2\fprq2{\*\panose 05000000000000000000}Wingdings;}
}
{\colortbl;
\red0\green0\blue0;
\red0\green0\blue255;
\red0\green255\blue255;
\red0\green255\blue0;
\red255\green0\blue255;
\red255\green0\blue0;
\red255\green255\blue0;
\red255\green255\blue255;
\red0\green0\blue128;
\red0\green128\blue128;
\red0\green128\blue0;
\red128\green0\blue128;
\red128\green0\blue0;
\red128\green128\blue0;
\red128\green128\blue128;
\red192\green192\blue192;
\red255\green255\blue255;
}
{\stylesheet
{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 \snext0 Normal;}
{\s1\ql \keepn\widctlpar\aspalpha\aspnum\faauto\outlinelevel0\adjustright\itap0 \sbasedon0 \snext0 heading 1;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext15 header;}
{\s16\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 \sbasedon0 \snext16 footer;}
}
\margl720\margr720\margt720\margb720 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0<!--#DG286 -->\headery0\footery400\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>
</xsl:stylesheet>
