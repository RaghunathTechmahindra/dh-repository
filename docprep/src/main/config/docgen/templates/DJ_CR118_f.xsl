<?xml version="1.0" encoding="UTF-8"?>
<!-- created by Catherine Rutgaizer -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:xmlns="http://www.w3.org/1999/XSL/Format">
	
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	
	<!-- define attribute sets -->
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">9pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="ObjectAttr" use-attribute-sets="MainFontAttr SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>								 
	</xsl:attribute-set>
	<xsl:attribute-set name="DateFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr">
		<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr" use-attribute-sets="SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
 		<xsl:attribute name="keep-together">always</xsl:attribute>	
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>	

	<!-- end attribute sets -->	
	
	<xsl:template name="CreatePageOne">
		<xsl:element name="page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
		</xsl:element></xsl:element>
	</xsl:template>

	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">	<xsl:copy use-attribute-sets="TableLeftFixed"/><xsl:attribute name="space-before">5mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">170mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row"><xsl:element name="fo:table-cell">
				<xsl:element name="fo:block"><xsl:copy use-attribute-sets="SpaceBAOptimum"/><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:external-graphic">
						<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
						<xsl:attribute name="height">1.1in</xsl:attribute><xsl:attribute name="width">2.86in</xsl:attribute>
				</xsl:element></xsl:element>
			</xsl:element></xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">	<xsl:copy use-attribute-sets="TableLeftFixed"/><xsl:attribute name="margin-left">2mm</xsl:attribute><xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">40mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">80mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="PaddingAll0mm"/>
					<xsl:element name="fo:block"></xsl:element></xsl:element>
				<xsl:element name="fo:table-cell"><xsl:attribute name="padding-top">1mm</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 1px</xsl:attribute>
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="TitleFontAttr"/>MÉMO</xsl:element>
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="TitleFontAttr"/>Demande de prêt annulée</xsl:element>
				</xsl:element></xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">	<xsl:copy use-attribute-sets="TableLeftFixed"/><xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">19cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row"><xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="DateFontAttr"/><xsl:attribute name="keep-together">always</xsl:attribute>	
						Le  <xsl:value-of select="//General/CurrentDate"/>
					</xsl:element>
				</xsl:element></xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
		 	<xsl:copy use-attribute-sets="TableLeftFixed"/><xsl:attribute name="space-before.optimum">8mm</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">22mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">130mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="ObjectAttr"/><xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Objet :</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Demande de financement hypothécaire annulée</xsl:element>
						<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column"><xsl:attribute name="column-width">45mm</xsl:attribute></xsl:element>
							<xsl:element name="fo:table-column"><xsl:attribute name="column-width">60mm</xsl:attribute></xsl:element>
							<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
								<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
									Emprunteurs : </xsl:element></xsl:element>
								<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
									<xsl:apply-templates select="//specialRequirementTags/OrderedBorrowers"/>
								</xsl:element></xsl:element></xsl:element>	
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Numéro de dossier : </xsl:element></xsl:element>
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:value-of select="//Deal/dealId"/></xsl:element></xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Propriété :</xsl:element></xsl:element>
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:apply-templates select="//DealAdditional/Property"/></xsl:element></xsl:element>
								</xsl:element></xsl:element></xsl:element>
				</xsl:element></xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>

	<!-- Subject property, French formatting of address-->
	<xsl:template match="Property">
		<xsl:element name="fo:block"><xsl:attribute name="keep-together">always</xsl:attribute>			
			<xsl:value-of select="propertyStreetNumber"/><xsl:text>, </xsl:text><xsl:if test="streetType"><xsl:value-of select="streetType"/><xsl:text>  </xsl:text></xsl:if>
			<xsl:value-of select="propertyStreetName"/><xsl:if test="streetDirection"><xsl:text>  </xsl:text><xsl:value-of select="streetDirection"/></xsl:if>
			<xsl:if test="unitNumber">, unité <xsl:value-of select="unitNumber"/></xsl:if>
		</xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="concat(propertyCity,', ',province)"/></xsl:element>
		<xsl:element name="fo:block"><xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/></xsl:element>
	</xsl:template>
	
	<!-- list of borrowers -->
	<xsl:template match="OrderedBorrowers">
		<xsl:for-each select="Borrower">
			<xsl:element name="fo:block">		
				<xsl:value-of select="./borrowerFirstName"></xsl:value-of><xsl:text> </xsl:text>
				<xsl:if test="./borrowerMiddleInitial"><xsl:value-of select="./borrowerMiddleInitial"/><xsl:text> </xsl:text></xsl:if>
				<xsl:value-of select="./borrowerLastName"></xsl:value-of>
			</xsl:element>	
		</xsl:for-each>		
	</xsl:template>	

	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainBlockAttr"/><xsl:attribute name="space-before.optimum">16mm</xsl:attribute>
			Veuillez prendre note que la demande en objet a été annulée.
     	</xsl:element>
     </xsl:template> 
     
<!-- FO Start -->	
	<xsl:template name="FOStart">
		<xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:root">
			<xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute>
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0mm</xsl:attribute>
					<xsl:attribute name="margin-bottom">1in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">1in</xsl:attribute>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">1mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
