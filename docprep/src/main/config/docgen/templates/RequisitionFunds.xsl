<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
27.Nov.2008 DVG #DG782 FXP23557: CTFS CR 264 - UAT C - Requisition of Funds
17/Oct/2008 DVG #DG768 FXP22148: CTFS CR 264 - Document clean-up
21/Aug/2008 DVG #DG742 LEN311996: Ron Mills CTFS ticket - MI being deducted twice 
09/May/2008 DVG #DG720 CTFS - CR203 - new form for requisiton of Funds
-->
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:fox="http://xml.apache.org/fop/extensions"
  xmlns:localTranslations="http://local.Translations.ca"
  exclude-result-prefixes="localTranslations">

	<xsl:import href="common.xsl"/>

	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>

  <xsl:variable name="translations" select="document('')/*/localTranslations:var/t[@lang=$lang]"/>
  <xsl:variable name="language" select="0"/>
  <xsl:variable name="lang">
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageFrench">
				<xsl:value-of select="'fr'" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'en'" />
			</xsl:otherwise>
		</xsl:choose>
  </xsl:variable>  
  <xsl:variable name="IsOneAndOnly" select="/*/CommitmentLetter[contains(Product, 'One-And-Only')]"/><!-- #DG782 -->

	<!-- define attribute sets -->
	<xsl:attribute-set name="BorderTop0.3mm">
		<xsl:attribute name="border-top-style">solid</xsl:attribute> 
		<xsl:attribute name="border-top-color">black</xsl:attribute> 
		<xsl:attribute name="border-top-width">0.3mm</xsl:attribute> 
	</xsl:attribute-set>	

	<xsl:attribute-set name="BorderBotom0.3mm">
		<xsl:attribute name="border-bottom-style">solid</xsl:attribute> 
		<xsl:attribute name="border-bottom-color">black</xsl:attribute> 
		<xsl:attribute name="border-bottom-width">0.3mm</xsl:attribute> 
	</xsl:attribute-set>	

	<xsl:attribute-set name="BorderLeft0.3mm">
		<xsl:attribute name="border-left-style">solid</xsl:attribute> 
		<xsl:attribute name="border-left-color">black</xsl:attribute> 
		<xsl:attribute name="border-left-width">0.3mm</xsl:attribute> 
	</xsl:attribute-set>	

	<xsl:attribute-set name="BorderRight0.3mm">
		<xsl:attribute name="border-right-style">solid</xsl:attribute> 
		<xsl:attribute name="border-right-color">black</xsl:attribute> 
		<xsl:attribute name="border-right-width">0.3mm</xsl:attribute>
	</xsl:attribute-set>	

	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
			  <!-- #DG782 -->
				<fo:simple-page-master master-name="main"
					page-height="279mm" page-width="216mm"
					margin-top="05mm" margin-bottom="05mm" 
					margin-left="15mm" margin-right="15mm">
					<fo:region-body 
            margin-top="0mm" margin-bottom="0mm"
						margin-left="0mm" margin-right="0mm"/>
					<fo:region-before extent="0mm" margin-left="5mm"/>
					<fo:region-after extent="0mm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<xsl:call-template name="CreatePageOne"/>
		</fo:root>
	</xsl:template>

	<xsl:template name="CreatePageOne">
		<fo:page-sequence master-reference="main">
      <fo:flow flow-name="xsl-region-body">
				<xsl:call-template name="CreateMainText"/>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>

	<xsl:template name="CreateMainText">
    <fo:block font-size="28pt" text-align="center" font-weight="bold">
  		<fo:block>
        <!-- <xsl:text>Requisition of Funds</xsl:text> -->
       <xsl:value-of select="$translations[@name='titleA']"/>
  		</fo:block>
  	 	<fo:block>
        <!-- <xsl:text>Mortgage Funding</xsl:text> -->
        <xsl:value-of select="$translations[@name='titleB']"/>
  		</fo:block>
    </fo:block>
    
    <xsl:call-template name="Section1"/>
    <xsl:call-template name="Section2"/>
    <xsl:call-template name="Section3"/>
    <xsl:call-template name="Section4"/>
    <fo:block keep-together="always"><!-- #DG782 -->
      <xsl:call-template name="Section5"/>
    </fo:block>
    <fo:block keep-together="always"><!-- #DG782 -->
      <xsl:call-template name="Section6"/>
    </fo:block>
	</xsl:template>

	<xsl:template name="Section1">    
    <fo:block font-size="12pt" font-weight="bold" space-before="6mm">
  		<fo:block>
        <!-- <xsl:text>Section 1 - Request Type</xsl:text> -->
        <xsl:value-of select="$translations[@name='section1']"/>
  		</fo:block>
  	 	<fo:block space-before="-0.8em"> 
        <fo:leader leader-pattern="rule" leader-length="100%" />
  		</fo:block>
    </fo:block>
    <fo:block font-size="12pt" space-before="4mm">
  		<fo:block text-indent="8mm" space-before="2mm">      
  			<xsl:call-template name="UnCheckedCheckbox"/>
        <fo:leader leader-pattern="space" leader-length="4mm" />
  			<!-- <xsl:text>Automatic Funds Transfer from CTB Account 101014</xsl:text> -->
        <xsl:value-of select="$translations[@name='section1AutoFunds']"/>
  		</fo:block>
  		<fo:block text-indent="8mm" space-before="2mm">      
  			<xsl:call-template name="UnCheckedCheckbox"/>
        <fo:leader leader-pattern="space" leader-length="4mm" />
  			<!-- <xsl:text>Cheque</xsl:text> -->
        <xsl:value-of select="$translations[@name='section1Xq']"/>
  		</fo:block>
    </fo:block>    
	</xsl:template>

	<xsl:template name="Section2">
    <fo:block font-size="12pt" font-weight="bold" space-before="6mm">
  		<fo:block>
        <!-- <xsl:text>Section 2 - Solicitor Information</xsl:text> -->
        <xsl:value-of select="$translations[@name='section2']"/>
  		</fo:block>
  	 	<fo:block space-before="-0.8em"> 
        <fo:leader leader-pattern="rule" leader-length="100%" />
  		</fo:block>
    </fo:block>
    
    <!-- #DG782 show headers anyways 
    <xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50 and bankName != '']"-->		
    <xsl:for-each select="/*/Deal/PartyProfile[partyTypeId=50]">		
      <fo:block font-size="12pt" space-before="4mm">
  			<!-- <xsl:text>Solicitor DTA:</xsl:text> -->
        <xsl:value-of select="$translations[@name='section2SolDta']"/>
        <xsl:text>&#160;&#160;&#160;</xsl:text>
        <xsl:value-of select="ptPBusinessId"/>
  		</fo:block>  		
		
  		<fo:table table-layout="fixed" width="100%" space-before="4mm">
  			<fo:table-column column-width="proportional-column-width(30)"/>
  			<fo:table-column column-width="proportional-column-width(180)"/>
  			<fo:table-body>
  				<fo:table-row line-height="1.3em">
  					<fo:table-cell>
  						<fo:block>
          			<!-- <xsl:text>solicitor</xsl:text> -->
                <xsl:value-of select="$translations[@name='section2Solic']"/>
  						</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>  					
          		<xsl:for-each select="./Contact">
    						<fo:block>
            		  <xsl:call-template name="ContactName"/>
  	 					  </fo:block>
          		</xsl:for-each>
  					</fo:table-cell>
  				</fo:table-row>
  
  				<fo:table-row line-height="1.3em">
  					<fo:table-cell>
  						<fo:block>
          			<!-- <xsl:text>Bank #</xsl:text> -->
                <xsl:value-of select="$translations[@name='section2Bank']"/>
  						</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block>
          			<xsl:value-of select="bankName"/>
  						</fo:block>
  					</fo:table-cell>
  				</fo:table-row>
  
  				<fo:table-row line-height="1.3em">
  					<fo:table-cell>
  						<fo:block>
          			<!-- <xsl:text>Branch #</xsl:text> -->
                <xsl:value-of select="$translations[@name='section2Branch']"/>
  						</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block>
  							<xsl:value-of select="bankTransitNum"/>
  						</fo:block>
  					</fo:table-cell>
  				</fo:table-row>
  
  				<fo:table-row line-height="1.3em">
  					<fo:table-cell>
  						<fo:block>
          			<!-- <xsl:text>Account #</xsl:text> -->
                <xsl:value-of select="$translations[@name='section2Acct']"/>
  						</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
  						<fo:block>
  							<xsl:value-of select="bankAccNum"/>
  						</fo:block>
  					</fo:table-cell>
  				</fo:table-row>
  			</fo:table-body>
  		</fo:table>  		
  		
      <fo:block font-size="12pt" space-before="4mm">
  			<!-- <xsl:text>Solicitor Address:</xsl:text> -->
        <xsl:value-of select="$translations[@name='section2SolAdr']"/>
  		</fo:block>  		  
			<xsl:for-each select="./Contact/Addr">
        <fo:block font-size="12pt" space-before="2mm">
    			<xsl:call-template name="doAddres"/>
    		</fo:block>  		
			</xsl:for-each>
  
    </xsl:for-each>
	</xsl:template>

	<xsl:template name="Section3">
    <fo:block font-size="12pt" font-weight="bold" space-before="6mm">
  		<fo:block>
        <!-- <xsl:text>Section 3 - Advance Information</xsl:text> -->
        <xsl:value-of select="$translations[@name='section3']"/>
  		</fo:block>
  	 	<fo:block space-before="-0.8em"> 
        <fo:leader leader-pattern="rule" leader-length="100%" />
  		</fo:block>
    </fo:block>

		<fo:table table-layout="fixed" width="100%" space-before="5mm">
			<fo:table-column column-width="proportional-column-width(45)"/>
			<fo:table-column column-width="proportional-column-width(55)"/>
			<fo:table-body>
				<fo:table-row line-height="1.3em">
					<fo:table-cell>
						<fo:block>
        			<!-- <xsl:text>Approved Mortgage Amount:</xsl:text> -->
              <xsl:value-of select="$translations[@name='section3MtgAmt']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
	            <xsl:value-of select="/*/Deal/totalLoanAmount"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row line-height="1.3em">
					<fo:table-cell>
						<fo:block>
        			<!-- <xsl:text>Genworth Premium</xsl:text> -->
              <xsl:value-of select="$translations[@name='section3MIAmt']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
        			<xsl:value-of select="/*/Deal/MIPremiumAmount"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row line-height="1.3em">
					<fo:table-cell>
						<fo:block>
        			<!-- <xsl:text>Genworth PST</xsl:text> -->
              <xsl:value-of select="$translations[@name='section3MIPst']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
        			<!-- #DG742 xsl:value-of select="/*/Deal/PST"/-->
        			<xsl:value-of select="/*/Deal/MIPremiumPST"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row line-height="1.3em">
					<fo:table-cell>
						<fo:block font-weight="bold">
        			<!-- <xsl:text>Net Mortgage Advance</xsl:text> -->
              <xsl:value-of select="$translations[@name='section3MtgAdv']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block font-weight="bold">
						  <xsl:if test="not($IsOneAndOnly)"><!-- #DG782 -->         
          			<!-- #DG742 <xsl:value-of select="/*/SolicitorsPackage/NetAdvanceAmount"/> -->
          			<!-- <xsl:value-of select="/*/Deal/netLoanAmount"/> -->        			
            		<xsl:value-of select="/*/SolicitorsPackage/AdvanceAmount"/>
              </xsl:if>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

        <!-- #DG768 -->
				<fo:table-row line-height="1.3em">
					<fo:table-cell>
						<fo:block>
        			<!-- IF CTOAO � Initial Advance Amount:   -->
              <xsl:value-of select="$translations[@name='section3IniAdvAmt']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
						  <!-- #DG782 -->
						  <xsl:if test="$IsOneAndOnly">
            		<!-- <xsl:value-of select="/*/SolicitorsPackage/AdvanceAmount"/> -->
            		<xsl:variable name="iniBorAdv" select="/*/Deal/DealFee[feeId = 56]"/>
            		<xsl:choose>
                  <xsl:when test="$iniBorAdv">
                    <xsl:for-each select="$iniBorAdv">
                      <xsl:value-of select="feeAmount"/>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$translations[@name='zeroCur']"/>                   
                  </xsl:otherwise>
                </xsl:choose>                       
              </xsl:if>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell>
						<fo:block space-before="0.8em"/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row line-height="1.3em">
					<fo:table-cell>
						<fo:block>
        			<!-- <xsl:text>Date Funds are required:</xsl:text> -->
              <xsl:value-of select="$translations[@name='section3FundReq']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
        			<xsl:value-of select="/*/Deal/estimatedClosingDate"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>  		
	</xsl:template>

	<xsl:template name="Section4">
    <fo:block font-size="12pt" font-weight="bold" space-before="6mm">
  		<fo:block>
        <!-- <xsl:text>Section 4 - Customer Information</xsl:text> -->
        <xsl:value-of select="$translations[@name='section4']"/>
  		</fo:block>
  	 	<fo:block space-before="-0.8em"> 
        <fo:leader leader-pattern="rule" leader-length="100%" />
  		</fo:block>
    </fo:block>

		<fo:table table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(45)"/>
			<fo:table-column column-width="proportional-column-width(55)"/>
			<fo:table-body>

    		<xsl:for-each select="/*/Deal/Borrower">
					<xsl:call-template name="Section4Cust"/>
    		</xsl:for-each>

			</fo:table-body>
		</fo:table>  		
	</xsl:template>

  <!-- repeat whole section for each customer -->
	<xsl:template name="Section4Cust">
		<fo:table-row line-height="4mm">
			<fo:table-cell>
				<fo:block>
    			<fo:leader leader-pattern="space"/>
    		</fo:block>
			</fo:table-cell>
		</fo:table-row>
	
		<fo:table-row line-height="1.3em">
			<fo:table-cell>
				<fo:block>
    			<!-- <xsl:text>Customer Name:</xsl:text> -->
          <xsl:value-of select="$translations[@name='section4BorName']"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="BorrowerFullName"/>
			  </fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row line-height="1.3em">
			<fo:table-cell>
				<fo:block>
    			<!-- <xsl:text>Expert #</xsl:text> -->
          <xsl:value-of select="$translations[@name='section4XprtNr']"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block>
    			<xsl:value-of select="/*/Deal/sourceApplicationId"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row line-height="1.3em">
			<fo:table-cell>
				<fo:block>
    			<!-- <xsl:text>Express #</xsl:text> -->
          <xsl:value-of select="$translations[@name='section4XprsNr']"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block>
    			<xsl:value-of select="/*/Deal/dealId"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row line-height="1.3em">
			<fo:table-cell>
				<fo:block>
    			<!-- <xsl:text>WB Customer #</xsl:text> -->
          <xsl:value-of select="$translations[@name='section4WBCustNr']"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block>
    			<xsl:value-of select="/*/Deal/servicingMortgageNumber"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>


	<xsl:template name="Section5">
    <fo:block font-size="12pt" font-weight="bold" space-before="6mm">
  		<fo:block>
        <!-- <xsl:text>Section 5 - Approval Information</xsl:text> -->
        <xsl:value-of select="$translations[@name='section5']"/>
  		</fo:block>
  	 	<fo:block space-before="-0.8em"> 
        <fo:leader leader-pattern="rule" leader-length="100%" />
  		</fo:block>
    </fo:block>

		<fo:table table-layout="fixed" width="100%" space-before="3mm">
			<fo:table-column column-width="proportional-column-width(45)"/>
			<fo:table-column column-width="proportional-column-width(55)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block space-before="0.6em"/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
        			<!-- <xsl:text>Express Approver:</xsl:text> -->
              <xsl:value-of select="$translations[@name='section5XprsAprov']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
        		<xsl:for-each select="/*/Deal/UserProfile
              [userProfileId =/*/Deal/underwriterUserId]/Contact">
  						<fo:block>
          		  <xsl:call-template name="ContactName"/>
	 					  </fo:block>
        		</xsl:for-each>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell>
						<fo:block space-before="0.6em"/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
        			<!-- <xsl:text>Requested By:</xsl:text> -->
              <xsl:value-of select="$translations[@name='section5ReqBy']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="BorderBotom0.3mm">
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell>
						<fo:block space-before="0.6em"/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
        			<!-- <xsl:text>Approved By:</xsl:text> -->
              <xsl:value-of select="$translations[@name='section5AprovBy']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="BorderBotom0.3mm">
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row>
					<fo:table-cell>
						<fo:block space-before="0.6em"/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
        			<!-- <xsl:text>Approver Signature:</xsl:text> -->
              <xsl:value-of select="$translations[@name='section5AprovSig']"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="BorderBotom0.3mm">
						<fo:block>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>  		
	</xsl:template>

	<xsl:template name="Section6">
    <fo:block font-size="12pt" font-weight="bold" space-before="6mm">
  		<fo:block>
        <!-- Section 6 � Clearing Approval Information -->
        <xsl:value-of select="$translations[@name='section6']"/>
  		</fo:block>
  	 	<fo:block space-before="-0.8em"> 
        <fo:leader leader-pattern="rule" leader-length="100%" />
  		</fo:block>
    </fo:block>

		<fo:table table-layout="fixed" width="100%" space-before="3mm">
			<fo:table-column column-width="proportional-column-width(70)"/>
			<fo:table-column column-width="proportional-column-width(30)"/>
			<fo:table-body>
    		<xsl:call-template name="SignLine">       
          <xsl:with-param name="counta" select="'1'"/>
        </xsl:call-template>
    		<xsl:call-template name="SignLine">       
          <xsl:with-param name="counta" select="'2'"/>
        </xsl:call-template>
			</fo:table-body>
		</fo:table>  		

	</xsl:template>

  <xsl:template name="SignLine">
    <xsl:param name="counta"/>
		<fo:table-row>
			<fo:table-cell>
				<fo:block space-before="0.6em"/>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
          <xsl:value-of select="$counta" />          				
				  <xsl:text>.&#160;</xsl:text>
    			<!-- Approver Signature -->
          <xsl:value-of select="$translations[@name='section6AprovSig']"/>
          <fo:leader leader-pattern="rule" leader-length="77mm" rule-thickness="1pt"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block>
          <xsl:value-of select="$translations[@name='datea']"/>
				  <xsl:text>:&#160;&#160;</xsl:text>
          <fo:leader leader-pattern="rule" leader-length="40mm" rule-thickness="1pt"/>
			  </fo:block>
			</fo:table-cell>
		</fo:table-row>
  </xsl:template>

  <!-- translations for literals go here -->
  <localTranslations:var>
     <t lang="en" name="titleA">Requisition of Funds</t>
     <t lang="fr" name="titleA">fr-Requisition of Funds</t>
     <t lang="en" name="titleB">Mortgage Funding</t>
     <t lang="fr" name="titleB">fr-Mortgage Funding</t>
     <t lang="en" name="section1">Section 1 - Request Type</t>
     <t lang="fr" name="section1">fr-Section 1 - Request Type</t>
     <t lang="en" name="section1AutoFunds">Automatic Funds Transfer from CTB Account 101014</t>
     <t lang="fr" name="section1AutoFunds">fr-Automatic Funds Transfer from CTB Account 101014</t>
     <t lang="en" name="section1Xq">Cheque</t>
     <t lang="fr" name="section1Xq">fr-Cheque</t>
     <t lang="en" name="section2">Section 2 - Solicitor Information</t>
     <t lang="fr" name="section2">fr-Section 2 - Solicitor Information</t>
     <t lang="en" name="section2SolDta">Solicitor Client #:</t><!-- #DG768 -->
     <t lang="fr" name="section2SolDta">fr-Solicitor Client #:</t>
     <t lang="en" name="section2Solic">Solicitor:</t>
     <t lang="fr" name="section2Solic">fr-Solicitor:</t>     
     <t lang="en" name="section2Bank">Bank #</t>
     <t lang="fr" name="section2Bank">fr-Bank #</t>
     <t lang="en" name="section2Branch">Branch #</t>
     <t lang="fr" name="section2Branch">fr-Branch #</t>
     <t lang="en" name="section2Acct">Account #</t>
     <t lang="fr" name="section2Acct">fr-Account #</t>
     <t lang="en" name="section2SolAdr">Solicitor Address:</t>
     <t lang="fr" name="section2SolAdr">fr-Solicitor Address:</t>
     <t lang="en" name="section3">Section 3 - Advance Information</t>
     <t lang="fr" name="section3">fr-Section 3 - Advance Information</t>
     <t lang="en" name="section3MtgAmt">Approved Mortgage Amount:</t>
     <t lang="fr" name="section3MtgAmt">fr-Approved Mortgage Amount:</t>
     <t lang="en" name="section3MIAmt">Genworth Premium</t>
     <t lang="fr" name="section3MIAmt">fr-Genworth Premium</t>
     <t lang="en" name="section3MIPst">Genworth PST</t>
     <t lang="fr" name="section3MIPst">fr-Genworth PST</t>
     <t lang="en" name="section3MtgAdv">Net Mortgage Advance</t>
     <t lang="fr" name="section3MtgAdv">fr-Net Mortgage Advance</t>
     <t lang="en" name="section3IniAdvAmt">IF CTOAO � Initial Advance Amount</t><!-- #DG768 -->
     <t lang="fr" name="section3IniAdvAmt">fr-IF CTOAO � Initial Advance Amount</t>    
     <t lang="en" name="section3FundReq">Date Funds are required:</t>
     <t lang="fr" name="section3FundReq">fr-Date Funds are required:</t>
     <t lang="en" name="section4">Section 4 - Customer Information</t>
     <t lang="fr" name="section4">fr-Section 4 - Customer Information</t>
     <t lang="en" name="section4BorName">Customer Name:</t>
     <t lang="fr" name="section4BorName">fr-Customer Name:</t>
     <t lang="en" name="section4XprtNr">Expert #</t>
     <t lang="fr" name="section4XprtNr">fr-Expert #</t>
     <t lang="en" name="section4XprsNr">Express #</t>
     <t lang="fr" name="section4XprsNr">fr-Express #</t>
     <t lang="en" name="section4WBCustNr">WB Customer #</t>
     <t lang="fr" name="section4WBCustNr">fr-WB Customer #</t>
     <t lang="en" name="section5">Section 5 - Mortgage Department Approval Information</t><!-- #DG768 -->
     <t lang="fr" name="section5">fr-Section 5 - Mortgage Department Approval Information</t>
     <t lang="en" name="section5XprsAprov">Express Approver:</t>
     <t lang="fr" name="section5XprsAprov">fr-Express Approver:</t>
     <t lang="en" name="section5ReqBy">Approver Signature:</t><!-- #DG768 -->
     <t lang="fr" name="section5ReqBy">fr-Approver Signature:</t>
     <t lang="en" name="section5AprovBy">Authorized Signature:</t>
     <t lang="fr" name="section5AprovBy">fr-Authorized Signature:</t>
     <t lang="en" name="section5AprovSig">Submitted to Clearing on:</t>
     <t lang="fr" name="section5AprovSig">fr-Submitted to Clearing on:</t>     
     <t lang="en" name="section6">Section 6 - Clearing Approval Information</t><!-- #DG768 -->
     <t lang="fr" name="section6">fr-Section 6</t>
     <t lang="en" name="section6AprovSig">Approver Signature:&#160;&#160;</t>
     <t lang="fr" name="section6AprovSig">fr-Approver Signature:&#160;&#160;</t>
     <t lang="en" name="datea">Date</t>
     <t lang="fr" name="datea">fr-Date</t>
     <t lang="en" name="zeroCur">$0.00</t>
     <t lang="fr" name="zeroCur">fr-0,00$</t>     
  </localTranslations:var>

</xsl:stylesheet>
