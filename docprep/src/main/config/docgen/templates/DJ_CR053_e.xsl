<?xml version="1.0" encoding="UTF-8"?>
<!-- Author Zivko Radulovic -->
<!-- edited by Catherine Rutgaizer, March 2004 -->
<!--22/Feb/2005 DVG DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- ================  Attribute sets ============== -->
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBA1.5Optimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ListLabelIndent">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="padding-left">-3mm</xsl:attribute>
	</xsl:attribute-set>
	<!-- ================  Attribute sets ============== -->

	<!-- ========================== top level ================================  -->
	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateBorrowerLines"/>
				<xsl:call-template name="CreateObjetLine"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- ======================= PAGE 1 ======================= -->
	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">11cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
							Mortgage Loan Financing Approval
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2em</xsl:attribute>
							<xsl:attribute name="space-before">2em</xsl:attribute>
							<xsl:value-of select="//General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateBorrowerLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2pt</xsl:attribute>
							<xsl:attribute name="space-before">2pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="BorrowersListedBlocks4only"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2pt</xsl:attribute>
							<xsl:attribute name="space-before">2pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="PrimBorrAddress"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="BorrowersListedBlocks4only">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='0']">
			<xsl:if test="position()  &lt; 5">
				<xsl:element name="fo:block">
					<xsl:call-template name="BorrowerFullName"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="position() = 5">et al.</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<xsl:template name="PrimBorrAddress">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:if test="position()=1">
				<xsl:for-each select="BorrowerAddress">
					<xsl:if test="borrowerAddressTypeId='0'">
						<xsl:call-template name="BorrAddress"/>
						<!-- DG#140 text>some text</text-->
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BorrAddress">
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/city,' ',./Address/province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/postalFSA,'  ', ./Address/postalLDU)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateObjetLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
						Subject:
				</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
						Application for a mortgage loan
					</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
						Identification code: <xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">2em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>Madam,</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">0mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>Sir,</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>I am pleased to inform you that your application for a mortgage loan has been approved. You will find attached a financing offer containing the terms and conditions related to this loan.</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			As agreed, your file will be transferred to  <xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyName"/>. 
			If you require more information, please contact your mortgage representative,  
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/salutation"/>
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactFirstName"/>
			<xsl:text>&#160;</xsl:text>
			<xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactLastName"> </xsl:value-of> at <xsl:value-of select="//Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"> </xsl:value-of>. 
			She/He will be pleased to answer all your questions.
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:text>We thank you for placing your trust in us and hope to have the pleasure of serving you again in the future. In the meantime, please accept our best regards.</xsl:text>
		</xsl:element>
	</xsl:template>
	<!-- Underwriter info -->
	<xsl:template name="CreateUnderwriterLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">10mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:table">
							<xsl:copy use-attribute-sets="TableLeftFixed"/>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">30mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-column">
								<xsl:attribute name="column-width">100mm</xsl:attribute>
							</xsl:element>
							<xsl:element name="fo:table-body">
								<xsl:copy use-attribute-sets="PaddingAll0mm"/>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Telephone:	
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumber"/>
											<xsl:choose>
												<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> or 1-888-281-4420 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<!-- <xsl:when test="//Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when> -->
												<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> or 1-800-728-2728 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:text> </xsl:text>ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
								<xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											Fax:
									</xsl:element>
									</xsl:element>
									<xsl:element name="fo:table-cell">
										<xsl:element name="fo:block">
											<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
											<xsl:attribute name="keep-together">always</xsl:attribute>
											<xsl:value-of select="//Deal/underwriter/Contact/contactFaxNumber"/>
											<xsl:choose>
												<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> or 1-888-888-7777</xsl:when>
												<xsl:when test="//Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999</xsl:when>
												<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> or 1-888-888-8888</xsl:when>
											</xsl:choose>
										</xsl:element>
									</xsl:element>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ======================= END PAGE 1 ======================= -->
	<!--   ================================================================= -->
	<xsl:template name="FOStart">
		<!-- DG#140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- DG#140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
