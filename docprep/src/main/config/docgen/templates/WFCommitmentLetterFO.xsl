﻿<?xml version="1.0" encoding="UTF-8"?>
<!-- 
28/Jun/2006 DVG #DG450 #3615  Wells Fargo - Commitment incorrect contact number  
07/Nov/2005 DVG #DG356 #2272  Wells Fargo E2E - Express Commitment letter  
14/Apr/2005 DVG #DG188 #1184 Xceed - CR 121 (Toll free number on french commitment letters) -->
<!-- 21/Mar/2005 DVG #DG170 #1106  Xceed - Commitment letter/solicitors package commitment letter -->
<!--22/Feb/2005 DVG #DG140 # conform templates to jdk1.4 xalan, new xerces, fop, batik-->
<!-- 07/Dec/2004 DVG #DG112 	scr#787 Xceed - Template change  -->
<!-- edited with XML Spy v4.3 U (http://www.xmlspy.com) by Brad Hadfield (Basis100 Inc) -->
<!-- edited with XML Spy v4.3 U by Catherine Rutgaizer (filogix Inc) -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" />	

	<!--#DG356 -->
  <xsl:variable name="condSeq" select="0" />

	<!-- get the lender code from the template path and use it to build the logo path -->
	<xsl:variable name="lendName" select="translate(/*/CommitmentLetter/LenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />	
	<xsl:variable name="lendNameLang">	
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageFrench">
		    <xsl:value-of select="concat($lendName,'_Fr')"/>
      </xsl:when>
			<xsl:otherwise>
		    <xsl:value-of select="concat($lendName,'_En')"/>
      </xsl:otherwise>
		</xsl:choose>
	</xsl:variable>	
  <!-- eg. file:///C:/mosdocprep/admin_CTF/docgen/templates/WFCommitmentLetterFO.xsl -->
  <!-- hard coded for now - will only work in 3.1 
  xsl:variable name="logoPatha" select="document-location()" /-->
  <xsl:variable name="logoPatha" select="'\mos_docprep\admin_WF\docgen\templates\WFCommitmentLetterFO.xsl'"/>
	<!--  
  ATTENTION - this is hard coded with the name of current template !! (until xsl 2.0)
  -->
  <xsl:variable name="logoPath" select="substring($logoPatha, 1, string-length($logoPatha) -24)" />
	<xsl:variable name="logoFilea" select="concat('Logo',concat($lendNameLang,'.gif'))" />
	<xsl:variable name="logoFile" select="concat($logoPath,$logoFilea)" />

	<xsl:template match="/">
	  <xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
	
		<xsl:call-template name="FOStart"/>
	</xsl:template>	

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
<xsl:template name="EnglishHeader">
	<!--  header -->
	<fo:static-content flow-name="xsl-region-before">
		<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
			<fo:inline font-style="italic" font-weight="bold" font-size="14pt">MORTGAGE COMMITMENT</fo:inline>
		</fo:block>
	</fo:static-content>
</xsl:template>

<xsl:template name="EnglishFooter">
	<!-- footer -->
	<fo:static-content flow-name="xsl-region-after">
		<fo:table text-align="left" table-layout="fixed">
			<fo:table-column column-width="8cm"/>
			<fo:table-column column-width="3.55cm"/>
			<fo:table-column column-width="8cm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-top="2mm" padding-left="2mm" padding-right="2mm" padding-bottom="2mm" >
					</fo:table-cell>
					<fo:table-cell padding-top="2mm" padding-left="2mm" padding-right="2mm" padding-bottom="2mm" >
						<fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="center">Page <fo:page-number/> of <fo:page-number-citation ref-id="endofdoc"/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="2mm" padding-left="2mm" padding-right="2mm" padding-bottom="2mm" >
						<fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="right">Initials <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="50pt"/> Date <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="100pt"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</fo:static-content>	
	<!-- footer end -->		

</xsl:template>
	
	<xsl:template name="EnglishPage1">
		<fo:page-sequence master-reference="main" language="en">
			<xsl:call-template name="EnglishHeader"></xsl:call-template>
			<xsl:call-template name="EnglishFooter"></xsl:call-template>
				<fo:flow flow-name="xsl-region-body">
					<fo:table text-align="left" table-layout="fixed">
						<fo:table-column column-width="1.25cm"/>
						<fo:table-column column-width="2.80cm"/>
						<fo:table-column column-width="5.5cm"/>
						<fo:table-column column-width="2.5cm"/>
						<fo:table-column column-width="2.5cm"/>
						<fo:table-column column-width="5cm"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding-top="2mm" padding-left="2mm" padding-right="2mm" padding-bottom="2mm" border="solid Black" number-rows-spanned="3" number-columns-spanned="3">
									<!--  Change this to be the relative path (from the root).  The drive letter isn't needed 
                  #DG356 changed to use variable path 
									<fo:external-graphic src="\mos_docprep\admin_WF\docgen\templates\XCD2.GIF" height=".84in" width="2.7in"/-->
									<fo:external-graphic src="{$logoFile}" height=".84in" width="2.7in"/>
								</fo:table-cell>
								
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border="solid Black" number-columns-spanned="3">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/LenderName"/></fo:block>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/></fo:block>
									<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
										<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
											<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
										</fo:block>
									</xsl:if>
										<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" margin-bottom="6.0pt">
											<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
											<xsl:text> </xsl:text>
											<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/><xsl:text>  </xsl:text>
											<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
										</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Telephone:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Fax:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">TO:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="2">								
									<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
										<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
											<xsl:value-of select="."/>
										</fo:block>
									</xsl:for-each>
								<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
								</fo:block>
								<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
									</fo:block>
								</xsl:if>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/><xsl:text> </xsl:text>
										<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/><xsl:text>  </xsl:text>
										<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="2">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<fo:inline text-decoration="underline" font-weight="bold">Underwriter</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/Underwriter"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<fo:inline text-decoration="underline" font-weight="bold">Date</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/CurrentDate"/></fo:block>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/BranchCurrentTime"/></fo:block>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black" number-rows-spanned="4">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/CareOf"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-rows-spanned="2" number-columns-spanned="2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/BrokerFirmName"/>
									</fo:block>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/BrokerAgentName"/>
									</fo:block>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line1"/>
									</fo:block>
									<xsl:if test="//CommitmentLetter/BrokerAddress/Line2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/BrokerAddress/Line2"/>
									</fo:block>
									</xsl:if>									
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/BrokerAddress/City"/><xsl:text> </xsl:text>
										<xsl:value-of select="//CommitmentLetter/BrokerAddress/Province"/><xsl:text>  </xsl:text>
										<xsl:value-of select="//CommitmentLetter/BrokerAddress/PostalCode"/>
									</fo:block>

									<xsl:if test="//CommitmentLetter/BrokerEmail">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerEmail"/></fo:block>
									</xsl:if>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt">Broker Reference Number:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black" number-rows-spanned="3" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt">Lender Reference Number:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black" number-rows-spanned="3">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/DealNum"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Telephone:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerAgentContactData/BrokerAgentPhoneNumber"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Fax:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-right="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerAgentContactData/BrokerAgentFaxNumber"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" border-right="solid Black" number-columns-spanned="6">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-style="italic" font-weight="bold" font-size="14pt">We are pleased to confirm that your application for a Mortgage Loan has been approved under the following terms and conditions:</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt">Mortgagor(s):</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black">
								<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="."/></fo:block>
								</xsl:for-each>									
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="3" number-rows-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt">Security Address:</fo:inline><xsl:text> </xsl:text>
										<xsl:for-each select="//CommitmentLetter/Properties/Property">
										<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="./AddressLine1"/></fo:block>

										<xsl:if test="./AddressLine2">
										<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="./AddressLine2"/></fo:block>
										</xsl:if>

										<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="./City"/><xsl:text> </xsl:text> <xsl:value-of select="./Province"/><xsl:text> </xsl:text> <xsl:value-of select="./PostalCode"/></fo:block>
										</xsl:for-each>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt"><xsl:value-of select="//CommitmentLetter/GuarantorClause"/><xsl:text>: </xsl:text></fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" border-bottom="solid Black"><xsl:text> </xsl:text>
								<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="."/></fo:block>
								</xsl:for-each>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="3">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="11pt">LOAN TYPE: </fo:inline><xsl:value-of select="//CommitmentLetter/Product"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-right="solid Black" number-columns-spanned="3">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="11pt">LTV: </fo:inline><xsl:value-of select="//CommitmentLetter/LTV"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<fo:table text-align="left" table-layout="fixed">
						<fo:table-column column-width="2.5cm"/>
						<fo:table-column column-width="3.25cm"/>
						<fo:table-column column-width="2.75cm"/>
						<fo:table-column column-width="3.8cm"/>
						<fo:table-column column-width="4.25cm"/>
						<fo:table-column column-width="3cm"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">COMMITMENT DATE:</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">FIRST PAYMENT DATE:</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">INTEREST ADJUSTMENT DATE:</fo:inline>
										<!--#DG356 fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/IADDate"/></fo:block-->
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">CLOSING DATE:</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/AdvanceDate"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">MATURITY DATE:</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/MaturityDate"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">INTEREST ADJUSTMENT AMOUNT:</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/IADAmount"/></fo:block>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="11pt">LOAN</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="11pt">TERMS</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="11pt">INSTALLMENT - <xsl:value-of select="//CommitmentLetter/PaymentFrequency"/></fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Amount:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/LoanAmount"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Interest Rate:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/InterestRate"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Principal and Interest:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/PandIPayment"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Insurance:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/Premium"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Term:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/PaymentTerm"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Taxes (estimated):</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/TaxPortion"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Admin Fee:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/AdminFee"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Amortization:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/Amortization"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Total Installment:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/TotalPayment"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Total Loan:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/TotalAmount"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" number-columns-spanned="6">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Please be advised the solicitor acting on behalf of the transaction, will be:</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">Name:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="5">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">																	<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">Address:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="5">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">																	<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="5">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">																	<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">Phone / Fax:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="5">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">																	<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-left="solid Black" border-bottom="solid Black" number-columns-spanned="6">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-left="solid Black" border-bottom="solid Black" number-columns-spanned="6">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than <xsl:text> </xsl:text>
										<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
										<xsl:text> </xsl:text> after which time if not accepted, shall be considered null and void.
									</fo:block>
									<xsl:for-each select="//CommitmentLetter/ClosingText/Line">
										<xsl:choose>
											<!--  When there is a node with text in it (non-empty), then we display it normally.  
											However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
											<xsl:when test="* | text()">
												<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="."/></fo:block>
											</xsl:when>
											<xsl:otherwise>
												<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
													<fo:leader line-height="9pt"/>
												</fo:block>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>
								</fo:table-cell>
							</fo:table-row>							
						</fo:table-body>
					</fo:table>
					<fo:block space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" id="LastPage" line-height="1pt" font-size="1pt"/>
				</fo:flow>
			</fo:page-sequence>
	</xsl:template>
	
	<xsl:template name="EnglishPage2">
	<fo:page-sequence master-reference="main" language="en" >
		<xsl:call-template name="EnglishHeader"></xsl:call-template>
		<xsl:call-template name="EnglishFooter"></xsl:call-template>
		<fo:flow flow-name="xsl-region-body">
			<fo:table text-align="left" table-layout="fixed">
				<fo:table-column column-width="1cm"/>
				<fo:table-column column-width="18.55cm"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-top="solid Black" border-right="solid Black" border-left="solid Black" number-columns-spanned="2">
							<fo:block font-size="9pt">
								<fo:block space-after="6.0pt">
									<fo:inline text-decoration="underline" font-weight="bold" font-size="9pt">TERMS AND CONDITIONS:</fo:inline>
								</fo:block>
								<fo:block space-after="6.0pt">
									<fo:inline font-weight="bold" font-size="9pt">THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING:</fo:inline>
								</fo:block>
								<fo:block space-after="6.0pt">The following conditions must be met, and the requested documents must be received in form and content satisfactory to 
									<xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text> </xsl:text>
									 no later than ten (10) days prior to the advance of the mortgage. Failure to do so may delay or void this commitment.
								</fo:block>

                <!--#DG356 Order the conditions as follows:
                  1. Condition ID 506
                  2. All the broker conditions
                  3. All the Solicitors conditions
                  4. All conditions where the responsibility is that of the applicant should be placed after the label Standard Conditions
                  
                  in order to number each condition in sequence, count each group and start from there
                  -->
                <xsl:variable name="cond506" select="/*/CommitmentLetter/Conditions/Condition[conditionId=506]"/>
								<xsl:for-each select="$cond506">
              		<xsl:call-template name="listCondition"/>
								</xsl:for-each>
                <xsl:variable name="cntCond506" select="count($cond506)"/>

                <xsl:variable name="condBrk" select="/*/CommitmentLetter/Conditions/Condition[
                                  (conditionId &gt;= 508 and conditionId &lt;= 548)
                                  or (conditionId = 551)]"/>
								<xsl:for-each select="$condBrk">
                  <xsl:call-template name="listCondition">
                    <xsl:with-param name="cntFrom" select="$cntCond506"/>
                  </xsl:call-template>
								</xsl:for-each>
                <xsl:variable name="cntCondBrk" select="$cntCond506+count($condBrk)"/>

                <xsl:variable name="condSol" select="/*/CommitmentLetter/Conditions/Condition[
                                  (conditionId &gt;= 552 and conditionId &lt;= 574)
                                  or (conditionId = 589)]"/>
								<xsl:for-each select="$condSol">
                  <xsl:call-template name="listCondition">
                    <xsl:with-param name="cntFrom" select="$cntCondBrk"/>
                  </xsl:call-template>
								</xsl:for-each>
                <xsl:variable name="cntCondSol" select="$cntCondBrk+count($condSol)"/>

								<fo:block space-after="6.0pt">
									<fo:inline text-decoration="underline" font-weight="bold" font-size="9pt">
                  STANDARD CONDITIONS:</fo:inline>
								</fo:block>
                <xsl:variable name="condApl" select="/*/CommitmentLetter/Conditions/Condition[
                                  conditionId = 55
                                  or (conditionId &gt;= 500 and conditionId &lt;= 505)
                                  or (conditionId = 507)
                                  or (conditionId &gt;= 575 and conditionId &lt;= 588)]"/>
								<xsl:for-each select="$condApl">
                  <xsl:call-template name="listCondition"/>
								</xsl:for-each>
                <!-- noneed xsl:variable name="cntCondApl" select="$cntCondBrk+count($condApl)"/-->
								
								<!--#DG356 end -->
								
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
<!-- Catherine, 25-Oct-2005, the whole section below was commented out as per Nadine's request 
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-left="solid Black" number-columns-spanned="2">
							<fo:block space-before="9pt" >
								<fo:inline text-decoration="underline" font-weight="bold" font-size="9pt">STANDARD CONDITIONS:</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>					
					
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block font-size="9pt">1.</fo:block> 
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black">
							<fo:block font-size="9pt">
-->							
								<!-- DG112 removed xsl:value-of select="//CommitmentLetter/VoidChq"/-->
<!--								
								<xsl:text>Receipt of a void cheque for mortgage payment account.</xsl:text>  --><!-- #DG112 added -->
<!--								
								
							</fo:block>
						</fo:table-cell>
					</fo:table-row>					
					
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block font-size="9pt">2.</fo:block> 
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black">
							
							<xsl:for-each select="//CommitmentLetter/TitleInsurance/Line">
								<xsl:choose>
-->								
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
<!--									
									<xsl:when test="* | text()"><fo:block font-size="9pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block font-size="9pt">3.</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black">
							<xsl:for-each select="//CommitmentLetter/FireInsurance/Line">
								<xsl:choose>
-->								
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
<!--									
									<xsl:when test="* | text()"><fo:block font-size="9pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" >
							<fo:block font-size="9pt">4.</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black" >
							<xsl:for-each select="//CommitmentLetter/TaxesPaid/Line">
								<xsl:choose>
-->								
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
<!--									
									<xsl:when test="* | text()"><fo:block font-size="9pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
-->					
					<!--
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="2">
							<fo:block space-before="12pt" space-after="6pt" font-size="9pt">
								<fo:inline text-decoration="underline" font-weight="bold">RATE GUARANTEE AND RATE ADJUSTMENT POLICIES:</fo:inline>
							</fo:block>
							
							<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
								<xsl:choose>
									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
							
						</fo:table-cell>
					</fo:table-row>
					-->

					<!-- #DG170 commented out
					<!- - #DG112 - ->
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black" >
							<fo:block font-size="9pt">5.</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black" >
							<fo:block font-size="9pt"><xsl:text>All WFFCC Mortgages are closed to prepayment in full except in the case of a proven bona fide sale and payment of the applicable penalty.</xsl:text> </fo:block>
						</fo:table-cell>
					</fo:table-row>
					<!- - #DG112 end -->
					
				</fo:table-body>
			</fo:table>
		</fo:flow>
	</fo:page-sequence>
	
	</xsl:template>
	
	<!--#DG356 -->
	<xsl:template name="listCondition">
      <xsl:param name="cntFrom" select="0"/>

			<fo:list-item>
				<fo:list-item-label end-indent="label-end()">
					<fo:block><xsl:value-of select="$cntFrom+position()"/><xsl:text>.</xsl:text></fo:block>
				</fo:list-item-label>
				<fo:list-item-body start-indent="body-start()">
					<fo:block keep-together="always">		
						<xsl:for-each select="./Line">
							<fo:block>
								<xsl:choose>
								<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="normalize-space() != ''">
                    <xsl:value-of select="."/>
										</xsl:when>
									<xsl:otherwise>
                    <fo:leader line-height="9pt"/>
                  </xsl:otherwise>
								</xsl:choose>
							</fo:block>												
						</xsl:for-each>
					</fo:block>
					<fo:block>
						<fo:leader line-height="9pt"/>
					</fo:block>
				</fo:list-item-body>
			</fo:list-item>
	</xsl:template>
	
	<xsl:template name="EnglishPage3">
	<fo:page-sequence master-reference="main" language="en" >
		<xsl:call-template name="EnglishHeader"/>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="center">Page <fo:page-number/> of <fo:page-number-citation ref-id="endofdoc"/></fo:block>
			</fo:static-content>
		    <fo:flow flow-name="xsl-region-body">
			<fo:table text-align="left" table-layout="fixed">
				<fo:table-column column-width="6cm"/>
				<fo:table-column column-width="10cm"/>
				<fo:table-column column-width="3.55cm"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-top="solid Black" border-left="solid Black" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="3">

              <!--#DG356 -->
              <xsl:variable name="rateGuar" select="/*/CommitmentLetter/RateGuarantee"/>
							<xsl:if test="$rateGuar">
  							<fo:block space-before="6pt" space-after="6pt" font-size="9pt">
  								<fo:inline text-decoration="underline" font-weight="bold">RATE GUARANTEE AND RATE ADJUSTMENT POLICIES:</fo:inline>
  							</fo:block>
  							
  							<xsl:for-each select="$rateGuar/Line">
  								<xsl:choose>
  									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt"><xsl:value-of select="."/></fo:block>
  									</xsl:when>
  									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt"><fo:leader line-height="9pt"/></fo:block>			
  									</xsl:otherwise>
  								</xsl:choose>
  							</xsl:for-each>
							</xsl:if>


              <!--#DG356 -->
              <xsl:variable name="privPaym" select="/*/CommitmentLetter/PrivilegePayment"/>
							<xsl:if test="$privPaym">              							
  							<fo:block font-size="9pt">
  								<fo:inline text-decoration="underline" font-weight="bold">PAYMENT POLICY:</fo:inline>
  							</fo:block>  							
  							<xsl:for-each select="$privPaym/Line">
  								<xsl:choose>
  									<!--  When there is a node with text in it (non-empty), then we display it normally.  
  									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
  									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt" text-align="justify"><xsl:value-of select="."/></fo:block>
  									</xsl:when>
  									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt" text-align="justify"><fo:leader line-height="9pt"/></fo:block>			
  									</xsl:otherwise>
  								</xsl:choose>
  							</xsl:for-each>
							</xsl:if>
  														
							<xsl:for-each select="/*/CommitmentLetter/InterestCalc/Line">
								<xsl:choose>
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt" text-align="justify"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt" text-align="justify"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>

              <!--#DG356 -->
              <xsl:variable name="closin" select="/*/CommitmentLetter/Closing"/>
							<xsl:if test="$closin">
  							<fo:block font-size="9pt">
  								<fo:inline text-decoration="underline" font-weight="bold">CLOSING:</fo:inline>
  							</fo:block>
  							<xsl:for-each select="$closin/Line">
  							 <fo:block font-size="9pt" space-after="6pt" text-align="justify"><xsl:value-of select="."/></fo:block>
  							</xsl:for-each>							
							</xsl:if>

              <!--#DG356 -->
              <xsl:variable name="acept" select="/*/CommitmentLetter/Acceptance"/>
							<xsl:if test="$acept">
  							<fo:block font-size="9pt">
  								<fo:inline text-decoration="underline" font-weight="bold">ACCEPTANCE:</fo:inline>
  							</fo:block>
  							<xsl:for-each select="$acept/Line">
  								<xsl:choose>
  									<!--  When there is a node with text in it (non-empty), then we display it normally.  
  									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
  									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt" text-align="justify"><xsl:value-of select="."/></fo:block>
  									</xsl:when>
  									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt" text-align="justify"><fo:leader line-height="9pt"/></fo:block>			
  									</xsl:otherwise>
  								</xsl:choose>
  							</xsl:for-each>
							</xsl:if>

							<fo:block text-align="right" font-size="9pt" space-before="6pt" space-after="6pt"><xsl:value-of select="//CommitmentLetter/LenderName"/></fo:block>
							<fo:block text-align="right" font-size="9pt" space-before="6pt" space-after="6pt">Authorized by: <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="75pt"/>
								<xsl:value-of select="//CommitmentLetter/Underwriter"/>
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="100pt"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" number-columns-spanned="3">
							<xsl:for-each select="//CommitmentLetter/Signature/Line">
								<xsl:choose>
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="* | text()"><fo:block text-align="right" font-size="9pt" space-before="6pt" space-after="6pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block text-align="right" font-size="9pt" space-before="6pt" space-after="6pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>							
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block space-before="30pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">WITNESS</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
							<fo:block space-before="30pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">APPLICANT</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
							<fo:block space-before="30pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">DATE</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">WITNESS</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">CO-APPLICANT</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">DATE</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block space-after="12pt" font-size="9pt">WITNESS</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block space-after="12pt" font-size="9pt"><xsl:value-of select="//CommitmentLetter/GuarantorClause"/></fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block space-after="12pt" font-size="9pt">DATE</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
			<fo:block id="endofdoc"/>
		</fo:flow>
	</fo:page-sequence>
	</xsl:template>
	
	<!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<xsl:template name="FrenchHeader">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
					<fo:inline font-style="italic" font-weight="bold" font-size="14pt">LETTRE D'ENGAGEMENT</fo:inline>
				</fo:block>
			</fo:static-content>
	</xsl:template>
	<xsl:template name="FrenchFooter">
			<fo:static-content flow-name="xsl-region-after">
				<fo:table text-align="left" table-layout="fixed">
					<fo:table-column column-width="8cm"/>
					<fo:table-column column-width="3.55cm"/>
					<fo:table-column column-width="8cm"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding-top="2mm" padding-left="2mm" padding-right="2mm" padding-bottom="2mm" >
							</fo:table-cell>
							<fo:table-cell padding-top="2mm" padding-left="2mm" padding-right="2mm" padding-bottom="2mm" >
								<fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="center">Page <fo:page-number/> de <fo:page-number-citation ref-id="endofdoc"/></fo:block>
							</fo:table-cell>
							<fo:table-cell padding-top="2mm" padding-left="2mm" padding-right="2mm" padding-bottom="2mm" >
								<fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="right">Initiales <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="50pt"/> Date <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="100pt"/></fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:static-content>	
	</xsl:template>
	
	<xsl:template name="FrenchPage1">
	<fo:page-sequence master-reference="main" language="en">
				<xsl:call-template name="FrenchHeader"/>
				<xsl:call-template name="FrenchFooter"/>
				<fo:flow flow-name="xsl-region-body">
					<fo:table text-align="left" table-layout="fixed">
						<fo:table-column column-width="2.75cm"/>
						<fo:table-column column-width="2.05cm"/>
						<fo:table-column column-width="4.75cm"/>
						<fo:table-column column-width="3.0cm"/>
						<fo:table-column column-width="3.0cm"/>
						<fo:table-column column-width="4cm"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding-top="2mm" padding-left="2mm" padding-right="2mm" padding-bottom="2mm" border="solid Black" number-rows-spanned="3" number-columns-spanned="3">
									<!--  Change this to be the relative path (from the root).  The drive letter isn't needed 
                  #DG356 changed to use admin_WF path 
									<fo:external-graphic src="\mos_docprep\admin_WF\docgen\templates\XCD2F.GIF" height=".84in" width="2.7in"/-->
									<fo:external-graphic src="{$logoFile}" height=".84in" width="2.7in"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border="solid Black" number-columns-spanned="3">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Société financière Wells Fargo Canada<!--<xsl:value-of select="//CommitmentLetter/LenderName"/><xsl:text>--></fo:block>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/></fo:block>
									<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/></fo:block>
									</xsl:if>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" margin-bottom="6.0pt"><xsl:value-of select="//CommitmentLetter/BranchAddress/City"/><xsl:text> </xsl:text>
										<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/><xsl:text>  </xsl:text>
										<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Téléphone:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:text> </xsl:text>
                    <!--#DG188 1-888-257-8889 -->
                    <!--#DG450 1-888-811-6660 -->
										<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Télécopieur:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
                    <xsl:text> </xsl:text>
                    <!--#DG450 1-888-482-6544 -->
  									<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
                  </fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
									<fo:block line-height="12pt" font-size="8pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">EMPRUNTEUR(S):</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="2">
								<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
								<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="."/></fo:block>
								</xsl:for-each>
								<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
								</fo:block>
								<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
								<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
									<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
								</fo:block>
								</xsl:if>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/><xsl:text> </xsl:text> <xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/><xsl:text>  </xsl:text>
										<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="2">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<fo:inline text-decoration="underline" font-weight="bold">Souscripteur</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/Underwriter"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<fo:inline text-decoration="underline" font-weight="bold">Date</fo:inline>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/CurrentDate"/></fo:block>
										<fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/BranchCurrentTime"/></fo:block>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black" number-rows-spanned="4">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><!--<xsl:value-of select="//CommitmentLetter/CareOf"/><xsl:text>-->A/S</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-rows-spanned="2" number-columns-spanned="2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerFirmName"/></fo:block>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerAgentName"/></fo:block>
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerAddress/Line1"/></fo:block>
									<xsl:if test="//CommitmentLetter/BrokerAddress/Line2">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerAddress/Line2"/></fo:block>
									</xsl:if>									
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerAddress/City"/><xsl:text> </xsl:text> <xsl:value-of select="//CommitmentLetter/BrokerAddress/Province"/><xsl:text>  </xsl:text>
											<xsl:value-of select="//CommitmentLetter/BrokerAddress/PostalCode"/>
									</fo:block>

									<xsl:if test="//CommitmentLetter/BrokerEmail">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerEmail"/></fo:block>
									</xsl:if>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt">Numéro de référence du courtier:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black" number-rows-spanned="3" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt">Numéro de référence du prêteur:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black" number-rows-spanned="3">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/DealNum"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Téléphone:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerAgentContactData/BrokerAgentPhoneNumber"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always">Télécopieur:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-right="solid Black">
									<fo:block line-height="12pt" font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"><xsl:value-of select="//CommitmentLetter/BrokerAgentContactData/BrokerAgentFaxNumber"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" border-right="solid Black" number-columns-spanned="6">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-style="italic" font-weight="bold" font-size="14pt">Nous sommes heureux de confirmer que votre demande de prêt hypothécaire a été approuvée selon les conditions mentionnées ci-dessous:</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt">EMPRUNTEUR(s):</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black">
								<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="."/></fo:block>
								</xsl:for-each>									
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="3" number-rows-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt">ADRESSE DE LA PROPRIÉTÉ:</fo:inline><xsl:text> </xsl:text>
										<xsl:for-each select="//CommitmentLetter/Properties/Property">
										<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="./AddressLine1"/></fo:block>

										<xsl:if test="./AddressLine2">
										<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="./AddressLine2"/></fo:block>
										</xsl:if>

										<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="./City"/><xsl:text> </xsl:text><xsl:value-of select="./Province"/><xsl:text>  </xsl:text><xsl:value-of select="./PostalCode"/></fo:block>
										</xsl:for-each>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="10pt"><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/GuarantorClause"/><xsl:text>: </xsl:text></fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" border-bottom="solid Black"><xsl:text> </xsl:text>
								<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:text> </xsl:text><xsl:value-of select="."/></fo:block>
								</xsl:for-each>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="3">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="11pt">PRODUIT: </fo:inline><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/Product"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-right="solid Black" number-columns-spanned="3">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="11pt">RATIO PRÊT VALEUR: </fo:inline><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/LTV"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<fo:table text-align="left" table-layout="fixed">
						<fo:table-column column-width="3.00cm"/>
						<fo:table-column column-width="2.75cm"/>
						<fo:table-column column-width="2.85cm"/>
						<fo:table-column column-width="3.7cm"/>
						<fo:table-column column-width="4.25cm"/>
						<fo:table-column column-width="3cm"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">DATE D'ENGAGEMENT:</fo:inline>
										<fo:block keep-together="always"><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">DATE DU PREMIER VERSEMENT:</fo:inline>
										<fo:block keep-together="always"><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">DATE D'AJUSTEMENT DES INTÉRÊTS:</fo:inline>
										<!--#DG356 fo:block keep-together="always"><xsl:value-of select="//CommitmentLetter/IADDate"/></fo:block-->
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">DATE DE CLÔTURE:</fo:inline>
										<fo:block keep-together="always"><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/AdvanceDate"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">DATE D'ÉCHÉANCE:</fo:inline>
										<fo:block keep-together="always"><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/MaturityDate"/></fo:block>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black" border-left="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">
										<fo:inline font-weight="bold" font-size="11pt">MONTANT DE L'AJUSTEMENT DES INTÉRÊTS:</fo:inline>
										<fo:block keep-together="always"><xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/IADAmount"/></fo:block>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="11pt">PRÊT</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="11pt">TERMES</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" number-columns-spanned="2">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline text-decoration="underline" font-weight="bold" font-size="11pt">PAIEMENT - <xsl:text> </xsl:text><xsl:value-of select="//CommitmentLetter/PaymentFrequency"/></fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Montant:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/LoanAmount"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Taux d'intérêt:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/InterestRate"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Capital et intérêts:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/PandIPayment"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Assurance:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/Premium"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Terme:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/PaymentTerm"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Taxes (approximatives):</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/TaxPortion"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Frais d'acceptation:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/AdminFee"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Amortissement:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/Amortization"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Versement total:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/TotalPayment"/></fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Total du prêt:</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="//CommitmentLetter/TotalAmount"/></fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" number-columns-spanned="6">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">
										<fo:inline font-weight="bold" font-size="10pt">Veuillez indiquer le notaire mandaté(e) pour cette transaction:</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">Nom du notaire:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="5">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">																	<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">Adresse:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="5">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">																	<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="5">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">																	<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" font-size="10pt">Téléphone / Télécopieur:</fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" number-columns-spanned="5">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">																	<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-left="solid Black" border-bottom="solid Black" number-columns-spanned="6">
									<fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"/>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-left="solid Black" border-bottom="solid Black" number-columns-spanned="6">
									<fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt">La signature de la présente Lettre d'engagement par le(s) emprunteur(s) atteste l'acceptation des conditions mentionnées dans celle-ci. La présente lettre d'engagement doit nous être retournée dûment signée au plus tard le <xsl:text> </xsl:text>
										<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
										<xsl:text> à défaut de quoi  ladite lettre d'engagement devient nulle at non avenue.</xsl:text></fo:block>
																			
										<xsl:for-each select="//CommitmentLetter/ClosingText/Line">
											<xsl:choose>
											<!--  When there is a node with text in it (non-empty), then we display it normally.  
												However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
												<xsl:when test="* | text()"><fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><xsl:value-of select="."/></fo:block>
												</xsl:when>
												<xsl:otherwise><fo:block font-size="10pt" line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt"><fo:leader line-height="9pt"/></fo:block>					
												</xsl:otherwise>
											</xsl:choose>
									</xsl:for-each>
								</fo:table-cell>
							</fo:table-row>							
						</fo:table-body>
					</fo:table>
					<fo:block space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" id="LastPage" line-height="1pt" font-size="1pt"/>
				</fo:flow>
			</fo:page-sequence>
	</xsl:template>
	
	<xsl:template name="FrenchPage2">
	<fo:page-sequence master-reference="main" language="en" >
		<xsl:call-template name="FrenchHeader"/>
		<xsl:call-template name="FrenchFooter"/>
		<fo:flow flow-name="xsl-region-body">
			<fo:table text-align="left" table-layout="fixed">
				<fo:table-column column-width="1cm"/>
				<fo:table-column column-width="18.55cm"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-top="solid Black" border-right="solid Black" border-left="solid Black" number-columns-spanned="2">
							<fo:block font-size="9pt">
								<fo:block space-after="6.0pt">
									<fo:inline text-decoration="underline" font-weight="bold" font-size="9pt">TERMES ET CONDITIONS:</fo:inline>
								</fo:block>
								<fo:block space-after="6.0pt">
									<fo:inline font-weight="bold" font-size="9pt">CETTE LETTRE D'ENGAGEMENT EST ASSUJETTIE AUX CONDITIONS SUIVANTES:</fo:inline>
								</fo:block>
								<fo:block space-after="6.0pt">Les conditions suivantes doivent être complétées et jugées satisfaisantes par SFWFC au plus tard dix (10) jours avant le date de clôture du déboursé hypothécaire à défaut de quoi la présente lettre d'engagement pourrait être annulée.
								</fo:block>

                <!--#DG356 xsl:for-each select="//CommitmentLetter/Conditions/Condition">
									<fo:list-item>
										<fo:list-item-label end-indent="label-end()">
											<fo:block><xsl:value-of select="position()"/><xsl:text>.</xsl:text></fo:block>
										</fo:list-item-label>
										<fo:list-item-body start-indent="body-start()"><fo:block keep-together="always">		
											<xsl:for-each select="./Line">
											<fo:block>
												<xsl:choose>
												<!--  When there is a node with text in it (non-empty), then we display it normally.  
													However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
													<xsl:when test="* | text()"><xsl:value-of select="."/>
														</xsl:when>
													<xsl:otherwise><fo:leader line-height="9pt"/></xsl:otherwise>
												</xsl:choose>
												</fo:block>												
											</xsl:for-each>
											</fo:block>
											<fo:block>
												<fo:leader line-height="9pt"/>
											</fo:block>
										</fo:list-item-body>
									</fo:list-item>
								</xsl:for-each-->
								
                <!--#DG356 Order the conditions as follows:
                  1. Condition ID 506
                  2. All the broker conditions
                  3. All the Solicitors conditions
                  4. All conditions where the responsibility is that of the applicant should be placed after the label Standard Conditions
                  
                  in order to number each condition in sequence, count each group and start from there
                  -->
                <xsl:variable name="cond506" select="/*/CommitmentLetter/Conditions/Condition[conditionId=506]"/>
								<xsl:for-each select="$cond506">
              		<xsl:call-template name="listCondition"/>
								</xsl:for-each>
                <xsl:variable name="cntCond506" select="count($cond506)"/>

                <xsl:variable name="condBrk" select="/*/CommitmentLetter/Conditions/Condition[
                                  conditionId = 55
                                  or (conditionId &gt;= 508 and conditionId &lt;= 515)
                                  or (conditionId &gt;= 516 and conditionId &lt;= 548)
                                  or (conditionId &gt;= 527 and conditionId &lt;= 530)
                                  or (conditionId = 551)]"/>
								<xsl:for-each select="$condBrk">
                  <xsl:call-template name="listCondition">
                    <xsl:with-param name="cntFrom" select="$cntCond506"/>
                  </xsl:call-template>
								</xsl:for-each>
                <xsl:variable name="cntCondBrk" select="$cntCond506+count($condBrk)"/>

                <xsl:variable name="condSol" select="/*/CommitmentLetter/Conditions/Condition[
                                  (conditionId &gt;= 552 and conditionId &lt;= 574)
                                  or (conditionId = 589)]"/>
								<xsl:for-each select="$condSol">
                  <xsl:call-template name="listCondition">
                    <xsl:with-param name="cntFrom" select="$cntCondBrk"/>
                  </xsl:call-template>
								</xsl:for-each>
                <xsl:variable name="cntCondSol" select="$cntCondBrk+count($condSol)"/>

								<fo:block space-after="6.0pt">
									<fo:inline text-decoration="underline" font-weight="bold" font-size="9pt">CONDITIONS STANDARD:</fo:inline>
								</fo:block>
                <xsl:variable name="condApl" select="/*/CommitmentLetter/Conditions/Condition[
                                  (conditionId &gt;= 549 and conditionId &lt;= 505)
                                  or (conditionId = 507)
                                  or (conditionId &gt;= 575 and conditionId &lt;= 588)]"/>
								<xsl:for-each select="$condApl">
                  <xsl:call-template name="listCondition"/>
								</xsl:for-each>
                <!-- noneed xsl:variable name="cntCondApl" select="$cntCondBrk+count($condApl)"/-->
								
								<!--#DG356 end -->
								
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
<!-- Catherine, 25-Oct-2005, the whole section below was commented out as per Nadine's request 
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-left="solid Black" number-columns-spanned="2">
							<fo:block space-before="9pt" >
								<fo:inline text-decoration="underline" font-weight="bold" font-size="9pt">CONDITIONS STANDARD:</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block font-size="9pt">1.</fo:block> 
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black">
							<fo:block font-size="9pt">
-->							
								<!-- DG112 removed xsl:value-of select="//CommitmentLetter/VoidChq"/-->							
<!--								
								<xsl:text>Réception  d'un chèque portant la mention annulé ainsi qu'une autorisation dûment remplie et signée pour permettre à la WFFCC de prélever automatiquement les versements hypothécaires du compte bancaire.</xsl:text> --><!-- #DG112 added -->							
<!--								
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block font-size="9pt">2.</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black">
							
							
							<xsl:for-each select="//CommitmentLetter/TitleInsurance/Line">
								<xsl:choose>
-->								
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
<!--																		
									<xsl:when test="* | text()"><fo:block font-size="9pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block font-size="9pt">3.</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black">
							<xsl:for-each select="//CommitmentLetter/FireInsurance/Line">
								<xsl:choose>
-->								
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
<!--									
									<xsl:when test="* | text()"><fo:block font-size="9pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" >
							<fo:block font-size="9pt">4.</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black" >
							<xsl:for-each select="//CommitmentLetter/TaxesPaid/Line">
								<xsl:choose>
-->								
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
<!--									
									<xsl:when test="* | text()"><fo:block font-size="9pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
-->					
<!--					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="2">
							<fo:block space-before="12pt" space-after="6pt" font-size="9pt">
								<fo:inline text-decoration="underline" font-weight="bold">RATE GUARANTEE AND RATE ADJUSTMENT POLICIES:</fo:inline>
							</fo:block>
							
							<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
								<xsl:choose>
									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt"></xsl:text><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
							
						</fo:table-cell>
					</fo:table-row>-->
					
					<!-- #DG170 commented out
					<!- - #DG112 - ->
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black" >
							<fo:block font-size="9pt">5.</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black" >
							<fo:block font-size="9pt"><xsl:text>Toutes les prêts hypothécaires d'WFFCC sont fermés. Le paiement du solde en totalité n'est pas permis sauf dans le cas d'une vente véritable prouvée et paiement de la pénalité applicable.</xsl:text> </fo:block>
						</fo:table-cell>
					</fo:table-row>
					<!- - #DG112 end -->
					
				</fo:table-body>
			</fo:table>
		</fo:flow>
	</fo:page-sequence>
	</xsl:template>
	
	<xsl:template name="FrenchPage3">
	<fo:page-sequence master-reference="main" language="en" >
		<xsl:call-template name="FrenchHeader"/>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block font-size="10pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always" text-align="center">Page <fo:page-number/> de <fo:page-number-citation ref-id="endofdoc"/></fo:block>
			</fo:static-content>
		<fo:flow flow-name="xsl-region-body">
			<fo:table text-align="left" table-layout="fixed">
				<fo:table-column column-width="6cm"/>
				<fo:table-column column-width="10cm"/>
				<fo:table-column column-width="3.55cm"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-top="solid Black" border-left="solid Black" border-right="solid Black" border-bottom="solid Black" number-columns-spanned="3">

							<fo:block space-before="6pt" space-after="6pt" font-size="9pt">
								<fo:inline text-decoration="underline" font-weight="bold">TAUX GARANTI ET POLITIQUE DE RAJUSTEMENT DE TAUX:</fo:inline>
							</fo:block>
							
							<xsl:for-each select="//CommitmentLetter/RateGuarantee/Line">
								<xsl:choose>
									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
							<fo:block font-size="9pt">
								<fo:inline text-decoration="underline" font-weight="bold">POLITIQUE DE PAIEMENT ANTICIPÊ:</fo:inline>
							</fo:block>
							
							<xsl:for-each select="//CommitmentLetter/PrivilegePayment/Line">
								<xsl:choose>
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt" text-align="justify"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt" text-align="justify"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
														
							<xsl:for-each select="//CommitmentLetter/InterestCalc/Line">
								<xsl:choose>
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt" text-align="justify"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt" text-align="justify"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
							<fo:block font-size="9pt">
								<fo:inline text-decoration="underline" font-weight="bold">CLÔTURE:</fo:inline>
							</fo:block>
							
							<xsl:for-each select="//CommitmentLetter/Closing/Line">
							<fo:block font-size="9pt" space-after="6pt" text-align="justify"><xsl:value-of select="."/></fo:block>
							</xsl:for-each>
							
							<fo:block font-size="9pt">
								<fo:inline text-decoration="underline" font-weight="bold">ACCEPTATION:</fo:inline>
							</fo:block>
							<xsl:for-each select="//CommitmentLetter/Acceptance/Line">
								<xsl:choose>
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="* | text()"><fo:block font-size="9pt" space-after="6pt" text-align="justify"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block font-size="9pt" space-after="6pt" text-align="justify"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
							<fo:block text-align="right" font-size="9pt" space-before="6pt" space-after="6pt"><xsl:text>Société financière Wells Fargo Canada</xsl:text><!-- <xsl:value-of select="//CommitmentLetter/LenderName"/> --></fo:block>
							<fo:block text-align="right" font-size="9pt" space-before="6pt" space-after="6pt">Autorisé par: <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="75pt"/>
								<xsl:value-of select="//CommitmentLetter/Underwriter"/><fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="100pt"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-right="solid Black" number-columns-spanned="3">
							<xsl:for-each select="//CommitmentLetter/Signature/Line">
								<xsl:choose>
									<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="* | text()"><fo:block text-align="right" font-size="9pt" space-before="6pt" space-after="6pt"><xsl:value-of select="."/></fo:block>
									</xsl:when>
									<xsl:otherwise><fo:block text-align="right" font-size="9pt" space-before="6pt" space-after="6pt"><fo:leader line-height="9pt"/></fo:block>			
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>							
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block space-before="30pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">TÉMOIN</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
							<fo:block space-before="30pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">EMPRUNTEUR(S)</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
							<fo:block space-before="30pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">DATE</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">TÉMOIN</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">CO-EMPRUNTEUR(S)</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block font-size="9pt">DATE</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-left="solid Black" border-bottom="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block space-after="12pt" font-size="9pt">TÉMOIN</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-bottom="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block space-after="12pt" font-size="9pt"><xsl:value-of select="//CommitmentLetter/GuarantorClause"/></fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="1mm" padding-left="2mm" padding-right="2mm" border-right="solid Black" border-bottom="solid Black">
							<fo:block space-before="24pt" font-size="9pt">
								<fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="90%"/>
							</fo:block>
							<fo:block space-after="12pt" font-size="9pt">DATE</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
			<fo:block id="endofdoc"/>
		</fo:flow>
	</fo:page-sequence>
	</xsl:template>
	
<!-- =========================================================== FOStart ===================================================== -->
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->

<!--	
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://xml.apache.org/fop/extensions">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" page-height="11in" page-width="8.5in" margin-top="1cm" margin-bottom="1cm" margin-left="1cm" margin-right="1cm">
					<fo:region-before extent="10mm"/>
					<fo:region-body margin-bottom="10mm" margin-top="5mm"/>
					<fo:region-after extent="10mm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
-->
	
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">10mm</xsl:attribute>
					<xsl:attribute name="margin-bottom">10mm</xsl:attribute>
					<xsl:attribute name="margin-left">10mm</xsl:attribute>
					<xsl:attribute name="margin-right">10mm</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">5mm</xsl:attribute>
						<xsl:attribute name="margin-top">5mm</xsl:attribute>
						<xsl:attribute name="margin-left">2mm</xsl:attribute>
						<xsl:attribute name="margin-right">2mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">5mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:choose>
				<xsl:when test="//CommitmentLetter/LanguageEnglish">
					<xsl:call-template name="EnglishPage1"/>
					<xsl:call-template name="EnglishPage2"/>
					<xsl:call-template name="EnglishPage3"/>
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="//CommitmentLetter/LanguageFrench">
					<xsl:call-template name="FrenchPage1"/>
					<xsl:call-template name="FrenchPage2"/>
					<xsl:call-template name="FrenchPage3"/>
				</xsl:when>
			</xsl:choose>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
