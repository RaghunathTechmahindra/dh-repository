﻿<!--?xml version="1.0" encoding="ISO-8859-1"?-->
<!--
14/Nov/2005	SA		#1328	DJ docs - Added apprisal phone extension
19/Oct/2005	DVG #DG334	#2264	DJ docs - Change phone numbers in CR047, CR050, CR059, CR060, CR061  
06/Apr/2005	DVG #DG176	#1182	DJ docs - comma between city and province
25/Jan/2005	DVG #DG128	#882 	DJ docs - Ensure Appraiser's partyprofile.partycompanyname is shown 
-->
<!-- created by Catherine Rutgaizer -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!--	xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" />
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	
	<!-- define attribute sets -->
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">9pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="UnderwriterFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="DateFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBAOptimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainBlockAttr" use-attribute-sets="MainFontAttr">
		<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">justify</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="TitleFontAttr" use-attribute-sets="SpaceBAOptimum">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="BottomLine">
		<xsl:attribute name="border-bottom-style">solid</xsl:attribute>
		<xsl:attribute name="border-bottom-color">black</xsl:attribute>
		<xsl:attribute name="border-bottom-width">0.5px</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="rowHeight">
		<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
		<xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
	</xsl:attribute-set>
	<!-- end attribute sets -->
	
	<xsl:template name="CreatePageOne">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">fr</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
					<xsl:attribute name="text-align">center</xsl:attribute>Page <fo:page-number/> de <fo:page-number-citation ref-id="page_1"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateLogoLine"/>
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="FromToSection"/>
				<xsl:call-template name="PropertyDetails"/>
				<xsl:call-template name="CreateMainText"/>
				<xsl:call-template name="CreateUnderwriterLines"/>
				<fo:block id="page_1" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateLogoLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">170mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="SpaceBAOptimum"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="margin-left">2mm</xsl:attribute>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">35mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">90mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TitleFontAttr"/>Mandat destiné à l'évaluateur agréé </xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="TitleFontAttr"/>Dossier <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="DateFontAttr"/>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
							Le  <xsl:value-of select="//General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="FiveCols">
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">20mm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">50mm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">7mm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">35mm</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:table-column">
			<xsl:attribute name="column-width">53mm</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template name="FromToSection">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">5mm</xsl:attribute>
			<xsl:call-template name="FiveCols"/>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Expéditeur :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyName"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Destinataire (évaluateur) :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/appraiserData/partyProfile/partyName"/>
							<!--#DG128 Ensure Appraiser's partyprofile.partycompanyname is shown-->
							<xsl:element name="fo:block">
</xsl:element>
							<xsl:value-of select="//DealAdditional/appraiserData/partyProfile/partyCompanyName"/>
							<!--#DG128 end-->
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Adresse :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:apply-templates select="//DealAdditional/originationBranch/partyProfile/Contact/Address"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Adresse :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:for-each select="//DealAdditional/appraiserData/partyProfile/Contact">
								<xsl:apply-templates select="./Address"/>
								<xsl:element name="fo:block">Tél. : <xsl:value-of select="contactPhoneNumber"/>
								<!-- #1328 -->
									<xsl:if test="contactPhoneNumberExtension">
                                                                                <xsl:text> poste : </xsl:text><xsl:value-of select="contactPhoneNumberExtension"/>
									</xsl:if>
									<!-- end of #1328 -->
                                                                </xsl:element>
								<xsl:element name="fo:block">Téléc. : <xsl:value-of select="contactFaxNumber"/></xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Address">
		<xsl:element name="fo:block">
			<xsl:value-of select="./addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./city,', ',./province)"/><!--#DG176-->
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./postalFSA,'  ', ./postalLDU)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="PartyNameAndCompany">
		<xsl:element name="fo:block">
			<xsl:value-of select="./partyName"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./partyCompanyName"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="PropertyDetails">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			PAR LA PRÉSENTE NOUS VOUS CONFIONS LE MANDAT D'ÉVALUER LA PROPRIÉTÉ SUIVANTE :
		</xsl:element>
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:call-template name="FiveCols"/>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Nom :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/AppraisalOrder/useBorrowerInfo='true'">
									<xsl:for-each select="//DealAdditional/Borrower">
										<xsl:value-of select="./borrowerFirstName"/>
										<xsl:text> </xsl:text>
										<xsl:if test="./borrowerMiddleInitial">
											<xsl:value-of select="./borrowerMiddleInitial"/>
											<xsl:text> </xsl:text>
										</xsl:if>
										<xsl:value-of select="./borrowerLastName"/>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="//DealAdditional/Property/AppraisalOrder/propertyOwnerName"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Contact :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/Property/AppraisalOrder/contactName"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Adresse :</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:apply-templates select="//DealAdditional/Property"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">Tél. : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">1mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:for-each select="//DealAdditional/Property/AppraisalOrder">
								<xsl:if test="contactWorkPhoneNum">
									<xsl:value-of select="contactWorkPhoneNum"/>
									<xsl:if test="contactWorkPhoneNumExt"> poste: <xsl:value-of select="contactWorkPhoneNumExt"/>
									</xsl:if>
									<xsl:text> </xsl:text>(Travail)</xsl:if>
								<xsl:if test="contactHomePhoneNum">
									<xsl:element name="fo:block">
										<xsl:value-of select="contactHomePhoneNum"/>
										<xsl:text> </xsl:text> (Maison)</xsl:element>
								</xsl:if>
								<xsl:if test="contactCellPhoneNum">
									<xsl:element name="fo:block">
										<xsl:value-of select="contactCellPhoneNum"/>
										<xsl:text> </xsl:text> (Cellulaire)</xsl:element>
								</xsl:if>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- Subject property with legal lines, French formatting of address-->
	<xsl:template match="Property">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:value-of select="propertyStreetNumber"/>
			<xsl:text>, </xsl:text>
			<xsl:if test="streetType">
				<xsl:value-of select="streetType"/>
				<xsl:text>  </xsl:text>
			</xsl:if>
			<xsl:value-of select="propertyStreetName"/>
			<xsl:if test="streetDirection">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="streetDirection"/>
			</xsl:if>
			<xsl:if test="unitNumber">, unité <xsl:value-of select="unitNumber"/>
			</xsl:if>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyCity,', ',province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="legalLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="legalLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="legalLine3"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateMainText">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			<xsl:attribute name="font-weight">bold</xsl:attribute>
			AUX CONDITIONS SUIVANTES : 
	    </xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainBlockAttr"/>
			Le rapport d'évaluation devra être complété en conformité avec le code d'éthique et les règles uniformes de pratique professionnelle en matière d'évaluation de l'Ordre des évaluateurs agréés du Québec ou selon les normes USPAP de l'Institut canadien des évaluateurs. L'évaluation ainsi requise par la caisse devra être conforme, de façon générale, aux dispositions suivantes : 
		</xsl:element>
		<!-- Catherine: splitting the ConditionList into 2 parts forces the line break between paragraphs -->
		<xsl:element name="fo:block">
			<xsl:call-template name="ConditionList1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:call-template name="ConditionList2"/>
		</xsl:element>
	</xsl:template>
	<!-- ================================================== Condition List ================================================== -->
	<xsl:template name="ConditionList1">
		<xsl:element name="fo:list-block">
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:element name="fo:list-item">
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>1.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Pour toute évaluation résidentielle de type unifamilial, semi-détaché, maison en rangée ou multilogement, vous devrez utiliser les formulaires pour évaluation préparés par la Fédération des caisses Desjardins du Québec. Si la complexité de l'évaluation est plus grande, comme au niveau commercial, industriel ou autre, vous pourrez produire un rapport de type narratif en y incluant toutes les recherches, détails et analyses qui devront inclure au minimum les renseignements contenus dans le formulaire Desjardins.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>2.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						L'évaluateur doit, dans la réalisation du présent mandat, engager pleinement sa responsabilité civile personnelle et ce, conformément à l'article 3.04.01 du code de déontologie des évaluateurs agréés du Québec. De plus il certifie détenir une couverture d'assurance responsabilité professionnelle.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>3.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						L'évaluateur devra appliquer les trois techniques soit celle du coût, du revenu et de la parité. Si l'une des techniques ne peut être utilisée, l'évaluateur devra l'expliquer.
						<xsl:call-template name="Cond3Details"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="ConditionList2">
		<xsl:element name="fo:list-block">
			<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
			<xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>4.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Le rapport d'évaluation devra contenir des données comparatives sur les loyers, les ventes et le taux de capitalisation en vigueur sur le marché pour des biens semblables. L'évaluateur n'utilisera pas l'expression "aucune donnée disponible" à moins qu'il précise le pourquoi. Les éléments de comparaison sont l'emplacement, le genre d'immeuble, son utilisation, ses dimensions et son état général. La référence à des ventes ou à des baux comparables ne devra pas excéder une période de deux ans; contrairement aux ventes, les inscriptions des propriétés à vendre ne peuvent servir de base au calcul mais il peut en être fait mention dans le texte qui accompagne l'évaluation. Ce texte devra également contenir des faits à l'appui des hypothèses et des rajustements. Les rajustements devront tenir compte des choix qu'un acheteur ou un locataire éventuel de l'immeuble en question pourra trouver sur le marché. Le cas échéant, il faudra tenir compte des facteurs de motivation comme les avantages fiscaux, l'achat par un étranger, la vente forcée, un financement favorable, etc. Lors d'évaluation d'immeuble commercial ou industriel, l'évaluateur devra au minimum visualiser de l'extérieur les immeubles ayant servi de base de comparaison.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>5.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						S'il s'agit d'évaluer un immeuble à usage restreint, l'évaluation comprendra un examen des autres utilisations possibles et une estimation des coûts de transformation, de rénovation ou de démolition, si nécessaire. L'évaluation d'un immeuble à usage restreint selon la méthode du revenu devra être basée sur les loyers en vigueur sur le marché même si l'entreprise est propriétaire occupant de l'immeuble. De plus, une liste des loyers comparatifs devra accompagner le rapport d'évaluation.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>6.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						L'évaluateur s'engage à inspecter ou faire inspecter, par l'un de ses représentants qualifiés, l'immeuble en question et ce, à l'intérieur comme à l'extérieur à moins que le mandat ne spécifie le contraire (dans ce cas, il devra l'indiquer dans le rapport).
					</xsl:element>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Il s'engage également à vérifier les baux, les états financiers et, si nécessaire, les frais d'exploitation tels que les comptes de taxes, d'assurance, de chauffage et d'électricité et toute autre dépense pouvant influer sur la valeur marchande.
					</xsl:element>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						L'évaluateur devra corroborer ces données avec le marché. S'il ne peut analyser ces éléments, il devra l'indiquer spécifiquement.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>7.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						L'évaluateur s'engage à faire les démarches auprès des autorités municipales afin de s'assurer de la conformité de l'immeuble aux règlements de zonage et d'urbanisme. De plus, l'évaluateur devra mentionner dans son rapport toute information pertinente en matière d'environnement.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>8.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Lorsque l'évaluateur utilisera la technique du revenu, ce dernier devrait utiliser un minimum de 5% en regard des frais de gestion, de 4% pour les frais d'entretien et de réparation, sauf dans le cas ou les revenus de l'immeuble sont en fonction d'un bail triple net, alors, les frais d’entretien et de réparation seront d'un minimum de 2%. Qu'il utilise ou non les pourcentages suggérés l'évaluateur devra en tout temps les commenter et les justifier.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>9.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Dans le cas d'immeuble de type multilogements, le taux de vacance devra être tiré de la SCHL et corroboré avec l'immeuble à évaluer. Pour le locatif commercial le taux de vacance sera établi en fonction du secteur et corroboré par l'immeuble à évaluer.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>10.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Terrain :<xsl:element name="fo:block">
							<xsl:attribute name="keep-together">always</xsl:attribute>
						Lorsqu'il s'agit d'évaluer un terrain vacant, la technique de parité devra toujours être utilisée. De plus, la méthode de lotissement devra être incluse au rapport et tenir compte d'un écoulement sur le marché ne dépassant pas 24 mois. Quant à la superficie résiduelle, elle devra être évaluée en tant que terrain en vrac ou non subdivisé.
			</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>11.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Lors de l'évaluation d'un immeuble locatif, un minimum de 35 des locaux devront être visités. Le choix des locaux à visiter devra être déterminé par l'évaluateur.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>12.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						L'évaluateur, s'il est en conflit d'intérêt, doit confier le mandat à l'un de ses confrères et en informer la caisse.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>13.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Aucune condition limitative ne viendra contredire les engagements pris en regard des articles précédents.
			</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">
						<xsl:attribute name="font-weight">bold</xsl:attribute>14.</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="keep-together">always</xsl:attribute>
						Commentaires additionnels de la caisse :
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
							<xsl:call-template name="CommentsForApprariser"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Cond3Details">
		<xsl:element name="fo:block">
			<xsl:element name="fo:list-block">
				<xsl:element name="fo:list-item">
					<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
					<xsl:element name="fo:list-item-label">
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>3.1</xsl:element>
					</xsl:element>
					<xsl:element name="fo:list-item-body">
						<xsl:attribute name="start-indent">body-start()</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="keep-together">always</xsl:attribute>
				La technique du coût : L'évaluateur devra tenir compte du prix le plus probable qu'il en coûterait pour reconstruire la propriété. De ce montant, sera déduit une dépréciation qui permet de tenir compte de l'état actuel de la bâtisse. Quant au terrain, il devra être évalué comme étant vacant. La technique du coût devra être subdivisée en 5 catégories soit a) terrain, b) coût de remplacement, c) dépréciation, d) dépendances, e) aménagement de terrain. Lorsqu'une évaluation est produite à partir d'un devis (nouvelle construction ou rénovation), l'évaluateur devra apporter ses commentaires sur les coûts du projet.
			</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item">
					<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
					<xsl:element name="fo:list-item-label">
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>3.2</xsl:element>
					</xsl:element>
					<xsl:element name="fo:list-item-body">
						<xsl:attribute name="start-indent">body-start()</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="keep-together">always</xsl:attribute>
					La technique de revenu : L'évaluateur capitalisera les revenus de l'année en cours, déduction faite des dépenses, des frais de gestion et des provisions pour vacance et pour réparations majeures, en se basant sur le taux de capitalisation déterminé en fonction de la méthode hypothèque/mise de fonds et corroboré par les indices de marché, en tenant compte d'un retour acceptable sur l'investissement. L'évaluateur devra se baser sur une évaluation du bien tel quel. Il ne peut s'appuyer sur des prévisions de plus de 12 mois, sur une valeur spéculative ou sur des hypothèses sans fondement.
				</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="keep-together">always</xsl:attribute>
					Advenant que l'évaluation vise une construction neuve ou une rénovation, le rapport devra inclure le revenu potentiel que produira l'immeuble concerné une fois les travaux complétés.
			</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item">
					<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
					<xsl:element name="fo:list-item-label">
						<xsl:element name="fo:block">
							<xsl:attribute name="font-weight">bold</xsl:attribute>3.3</xsl:element>
					</xsl:element>
					<xsl:element name="fo:list-item-body">
						<xsl:attribute name="start-indent">body-start()</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="keep-together">always</xsl:attribute>
				La technique de parité : L'évaluateur devra estimer le prix de vente le plus probable de la bâtisse en la comparant à d'autres du même genre. Certains critères sont nécessaires tels que décrits plus bas. Les comparables utilisés devront être clairement identifiés.
			</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ================================================== end Condition List ================================================== -->
	<!-- 
     <xsl:template name="DoNotes">
		<xsl:element name="fo:table">	<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">9mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">150mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell"><xsl:text></xsl:text></xsl:element>
				<xsl:element name="fo:table-cell"><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
					<xsl:element name="fo:list-block"><xsl:attribute name="margin-left">5mm</xsl:attribute>
					<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
						<xsl:apply-templates select="//Deal/DealNotes/DealNote"/>	
				</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>
    </xsl:template>
  -->
	<xsl:template name="CommentsForApprariser">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">150mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:text/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="margin-left">5mm</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
							<xsl:value-of select="//DealAdditional/Property/AppraisalOrder/commentsForAppraiser"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="DealNote">
		<xsl:if test="dealNotesCategoryId='10'">
			<xsl:element name="fo:list-item">
				<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
				<xsl:element name="fo:list-item-label">
					<xsl:element name="fo:block">•</xsl:element>
				</xsl:element>
				<xsl:element name="fo:list-item-body">
					<xsl:attribute name="start-indent">body-start()</xsl:attribute>
					<xsl:element name="fo:block">
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="dealNotesText"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template name="CreateUnderwriterLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:copy use-attribute-sets="PaddingAll2mm"/>
				<xsl:element name="fo:table-row">
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">7mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">0pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="UnderwriterFontAttr"/>
							<xsl:attribute name="space-before.optimum">5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/userType"/>
							<xsl:call-template name="UnderwriterContactInfo"/>
							<xsl:call-template name="SignatureLines"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="UnderwriterContactInfo">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:copy use-attribute-sets="UnderwriterFontAttr"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">30mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">95mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="PaddingAll0mm"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							Téléphone :
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumber"/>
							<xsl:choose>
								<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> or 1-888-281-4420 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
								</xsl:when>
								<!-- <xsl:when test="//Deal/branchProfile/branchProfileId='174'"> or 1-888-888-9999 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/></xsl:when> -->
								<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> or 1-800-728-2728 ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text> </xsl:text>ext:  <xsl:value-of select="//Deal/underwriter/Contact/contactPhoneNumberExtension"/>
								</xsl:otherwise>
							</xsl:choose-->
							514-376-7181 ou sans frais 1-877-376-7181
							<!-- end of #2264 -->
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							Télécopieur :
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/Contact/contactFaxNumber"/>
							<xsl:choose>
								<xsl:when test="//Deal/branchProfile/branchProfileId='173'"> ou 1-888-888-7777</xsl:when>
								<xsl:when test="//Deal/branchProfile/branchProfileId='174'"> ou 1-888-888-9999</xsl:when>
								<xsl:when test="//Deal/branchProfile/branchProfileId='175'"> ou 1-888-888-8888</xsl:when>
							</xsl:choose-->
							514-322-2364 ou sans frais 1-866-243-2364
							<!-- end of #2264 -->
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="SignatureLines">
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainFontAttr"/>
			<xsl:attribute name="space-before.optimum">5mm</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
		Nous acceptons le mandat mentionné précédemment et nous joignons une copie signée au rapport d'évaluation de la caisse :
		</xsl:element>
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">7mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">25mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">70mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">20mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">25mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:copy use-attribute-sets="MainFontAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">Signature : </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="rowHeight"/>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="rowHeight"/>
						<xsl:text> </xsl:text>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="rowHeight"/>
						<xsl:copy use-attribute-sets="BottomLine"/>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1mm</xsl:attribute>Évaluateur agréé </xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:text> </xsl:text>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="space-before.optimum">1mm</xsl:attribute>Date</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- FO Start -->
	<xsl:template name="FOStart">
		<!--xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
			<xsl:attribute name="xmlns:xsl>http://www.w3.org/1999/XSL/Transform</xsl:attribute>			
		</xsl:element-->
		<xsl:element name="fo:root">
			<!--xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0mm</xsl:attribute>
					<xsl:attribute name="margin-bottom">10mm</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">15mm</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">30mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">15mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
