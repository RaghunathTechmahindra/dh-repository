<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema">

<!-- Venkata - Hardcoding Lender Name - CR - Begin -->
<xsl:variable name="lenderName" select="'GE Money'" />	
<!-- Venkata - Hardcoding Lender Name - CR - End -->

	<xsl:output version="1.0" method="html" encoding="UTF-8" indent="no"/>
	<xsl:variable name="fo:layout-master-set">
		<fo:layout-master-set>
			<fo:simple-page-master master-name="default-page" page-height="14in" page-width="8.5in" margin-left="0.6in" margin-right="0.6in">
				<fo:region-body margin-top="0.79in" margin-bottom="0.79in"/>
			</fo:simple-page-master>
		</fo:layout-master-set>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:variable name="maxwidth" select="7.30000"/>
		<fo:root>
			<xsl:copy-of select="$fo:layout-master-set"/>
			<fo:page-sequence master-reference="default-page" initial-page-number="1" format="1">
				<fo:flow flow-name="xsl-region-body">
					<fo:block>
						<xsl:variable name="tablewidth0" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths0" select="0.000"/>
						<xsl:variable name="defaultcolumns0" select="1"/>
						<xsl:variable name="defaultcolumnwidth0">
							<xsl:choose>
								<xsl:when test="$defaultcolumns0 &gt; 0">
									<xsl:value-of select="($tablewidth0 - $sumcolumnwidths0) div $defaultcolumns0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth0_0" select="$defaultcolumnwidth0"/>
						<fo:table width="{$tablewidth0}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth0_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; </xsl:text>
											</fo:inline>
											<fo:inline font-size="10pt" font-weight="bold">
												<xsl:text> DISCLOSURE STATEMENT</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; (Made in Accordance with the Provisions of the Mortgage Brokers Act, 1976)</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>

						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth2" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths2" select="0.34375 + 4.90625 + 2.02083 + 2.12500 + 1.25000 + 1.03125 + 0.22917 + 2.00000"/>
						<xsl:variable name="factor2">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths2 &gt; 0.00000 and $sumcolumnwidths2 &gt; $tablewidth2">
									<xsl:value-of select="$tablewidth2 div $sumcolumnwidths2"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth2_0" select="0.20375 * $factor2"/>
						<xsl:variable name="columnwidth2_1" select="0.34375 * $factor2"/>
						<xsl:variable name="columnwidth2_2" select="6.34625 * $factor2"/>
						<xsl:variable name="columnwidth2_3" select="0.22917 * $factor2"/>
						<xsl:variable name="columnwidth2_4" select="1.00000 * $factor2"/>
						<fo:table width="{$tablewidth2}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth2_0}in"/>
							<fo:table-column column-width="{$columnwidth2_1}in"/>
							<fo:table-column column-width="{$columnwidth2_2}in"/>
							<fo:table-column column-width="{$columnwidth2_3}in"/>
							<fo:table-column column-width="{$columnwidth2_4}in"/>

							<fo:table-body>

								<fo:table-row>
									<fo:table-cell  number-columns-spanned="5" text-align="left" >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>DATED&#160;&#160;</xsl:text>
											</fo:inline>

											<xsl:choose>
												<xsl:when test="//DSCL_CURRENT_DATE">
													<fo:inline font-size="10pt" text-decoration="underline" >
														<xsl:value-of select="//DSCL_CURRENT_DATE"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>&#160;&#160;DAY OF&#160;&#160;</xsl:text>
											</fo:inline>

											<xsl:choose>
												<xsl:when test="//DSCL_CURRENT_MONTH">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_CURRENT_MONTH"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>______________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>,&#160;&#160;</xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_CURRENT_YEAR">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_CURRENT_YEAR"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>______________ .</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>, AT&#160;&#160;</xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_CURRENT_TIME">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_CURRENT_TIME"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>______________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="center">
										<fo:block>
											<fo:leader leader-pattern="space"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>1.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>The amount of the mortgage will be</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										<fo:inline font-size="10pt">
											<xsl:value-of select="//DSCL_MTGAMT_1"/>
										</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>2.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>The charges, fees, etc., associated with this mortgage are as follows:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Bonus to be charged to Borrower</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_BONUS_1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Application fee</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										<fo:inline font-size="10pt">
											<xsl:value-of select="//DSCL_APPLCTNFEE_1"/>
										</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Appraisal cost</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
											<xsl:value-of select="//DSCL_INSPCTNFEE_1"/>
										</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Estimated survey cost</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Estimated legal fees &amp; disbursements</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Fee for registering mortgage</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Disability &amp;/or Life Insurance</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Fire Insurance for 1st. year</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Mortgage Insurance CMHC or GE Capital</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										<fo:inline font-size="10pt">
											<xsl:value-of select="//DSCL_MTGINSRNCE_1"/>
										</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Interest adjustment: $ </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_INTADJSTMNT_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_INTADJSTMNT_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text> per diem for </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_INTSTDAYS_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_INTSTDAYS_1"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>______</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text> days</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Hold back for property taxes</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										<fo:inline font-size="10pt">
											<xsl:value-of select="//DSCL_PTPYTXHDBK_1"/>
										</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Brokerage fee</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										<fo:inline font-size="10pt">
											<xsl:value-of select="//DSCL_BRKCOMSN_1"/>
										</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Other charges (Specify)</xsl:text>
											</fo:inline>
										</fo:block>

									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										<fo:inline font-size="10pt">
											<xsl:value-of select="//DSCL_OTHRCHRGS_1"/>
										</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Total Deductions:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										<fo:inline font-size="10pt">
											<xsl:value-of select="//DSCL_TOTLDED_1"/>
										</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>3.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Amount of money to be paid to the Borrower or to be disbursed on his direction</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>$</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_MTGAMT_1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>4.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>The annual percentage rate will be&#160;&#160;</xsl:text>
											</fo:inline>
											<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
												<xsl:value-of select="//DSCL_RATE_3"/>&#160;
											</fo:inline>
											<fo:inline font-size="10pt">
												<xsl:text>%.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>5.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Where the annual percentage rate is subject to variations, it shall vary in the following manner, based on the following conditions:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" />
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" >
										<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_ANNLPERCRTEVRTNS_2"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>6.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>The interest shall be calculated semi-annually, not in advance.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>7.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell  number-columns-spanned="4" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>The payments are $ </xsl:text>
											</fo:inline>

											<xsl:choose>
												<xsl:when test="//DSCL_MTGPAYMTAMT_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_MTGPAYMTAMT_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text> payable </xsl:text>
											</fo:inline>

											<xsl:choose>
												<xsl:when test="//NL_DISC_PYMTFREQNCY_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//NL_DISC_PYMTFREQNCY_1"/>
													</fo:inline>
													<fo:inline font-size="10pt" >
														<xsl:text>.</xsl:text>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>______________ .</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>8.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Where the payments are subject to variations they shall vary in the following manner:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline />
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="before" number-columns-spanned="4">
										<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_PYMTVRTNS_2"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>9.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Do the payments include property taxes? </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_PYMTWTHPRPTYTAXS_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_PYMTWTHPRPTYTAXS_1"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>___________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>. If yes, how much? $ </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_PRPTYTAXS_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_PRPTYTAXS_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>______________.</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>10.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>The mortgage will become due and payable on </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_MTGDUE_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_MTGDUE_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_______________.</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text> at which time the borrower will owe $</xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_BLNCEENDTRM_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_BLNCEENDTRM_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_______________.</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text> (based on annual rate in Section 4) assuming all payments are made when due.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>11.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>The term of the mortgage is </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_MTGTERM_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_MTGTERM_1"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text> months. Amortization period of the mortgage is </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_MTGAMRTIZTON_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_MTGAMRTIZTON_1"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text> years.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>12.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>Are there any rights to prepay a portion or the full amount of the principal amount? </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_PREPYMTAMT_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_PREPYMTAMT_1"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>. If yes, specify.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt" />
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" >
										<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_PREPYMTAMTCNDTS_1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>13.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text> If the borrower is in default under the mortgage, interest will be due and payable on the amount of the default at a rate equal to the rate outlined in Section 4.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" text-align="left">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>14.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="4" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
												<xsl:text>The first payment is due and payable on </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_PYMTDT_1">
													<fo:inline font-size="10pt" text-decoration="underline" text-align="center">
														<xsl:value-of select="//DSCL_PYMTDT_1"/></fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_______________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline font-size="10pt" >
												<xsl:text>.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

							</fo:table-body>
						</fo:table>

						<fo:block break-after="page"/>

						<fo:inline font-size="inherited-property-value(&apos;font-size&apos;) - 2pt">
							<xsl:text>I acknowledge having read and received this Disclosure Statement.</xsl:text>
						</fo:inline>
						<fo:block>
							<xsl:text>&#xA;</xsl:text>
						</fo:block>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth12" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths12" select="2.08333 + 0.14583 + 2.07292 + 0.16667 + 2.00000"/>
						<xsl:variable name="factor12">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths12 &gt; 0.00000 and $sumcolumnwidths12 &gt; $tablewidth12">
									<xsl:value-of select="$tablewidth12 div $sumcolumnwidths12"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth12_0" select="2.08333 * $factor12"/>
						<xsl:variable name="columnwidth12_1" select="0.14583 * $factor12"/>
						<xsl:variable name="columnwidth12_2" select="2.07292 * $factor12"/>
						<xsl:variable name="columnwidth12_3" select="0.16667 * $factor12"/>
						<xsl:variable name="columnwidth12_4" select="2.00000 * $factor12"/>
						<fo:table width="{$tablewidth12}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth12_0}in"/>
							<fo:table-column column-width="{$columnwidth12_1}in"/>
							<fo:table-column column-width="{$columnwidth12_2}in"/>
							<fo:table-column column-width="{$columnwidth12_3}in"/>
							<fo:table-column column-width="{$columnwidth12_4}in"/>
							<fo:table-body>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="white" border-right-width="0.01042in" display-align="after">
										<fo:block>
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_BORROW_1"/>
											</fo:inline>
											<fo:leader leader-pattern="space"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right-style="solid" border-right-color="white" border-right-width="0.01042in">
									<fo:block/>
									</fo:table-cell>
									<fo:table-cell padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="white" border-right-width="0.01042in" display-align="after">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DISC_ADRSBRWER_1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Name of Borrower</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Address</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Signature</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block />
									</fo:table-cell>
								</fo:table-row>
								<xsl:for-each select="//borrowerloopNL/Borrower">								
								<fo:table-row keep-with-previous="always">
									<fo:table-cell padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="white" border-right-width="0.01042in" display-align="after">
										<fo:block>
											<fo:inline font-size="10pt">
												<xsl:value-of select="name"/>
											</fo:inline>
											<fo:leader leader-pattern="space"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right-style="solid" border-right-color="white" border-right-width="0.01042in">
										<fo:block />
									</fo:table-cell>
									<fo:table-cell padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="white" border-right-width="0.01042in" display-align="after">
										<fo:block >
											<fo:inline font-size="10pt">
												<xsl:value-of select="address"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Name of Borrower</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Address</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Signature</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								</xsl:for-each>
								<fo:table-row>
									<fo:table-cell>
										<fo:block />
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell padding-top="6pt" number-columns-spanned="5" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" display-align="after">
										<fo:block>
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_ADRSPRPTY_1"/>
											</fo:inline>
											<fo:leader leader-pattern="space"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Property to be Mortgaged</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block />
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell  padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" display-align="after">
										<fo:block>
											<fo:inline font-size="10pt">
												<!-- Venkata - Hardcoding Lender Name - CR - Begin -->
												<!-- <xsl:value-of select="//DSCL_LEND_1"/> -->
												 <xsl:value-of select="$lenderName"/>
												 <!-- Venkata - Hardcoding Lender Name - CR - End -->
											</fo:inline>
											<fo:leader leader-pattern="space"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block/>
									</fo:table-cell>
									<fo:table-cell  padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" display-align="after">
										<fo:block>
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_LENDADDR_1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Name of Lender</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Address</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Signature</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block />
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" display-align="after">
										<fo:block>
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_NMEMTGBRKR_1"/>
											</fo:inline>
											<fo:leader leader-pattern="space"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<fo:leader leader-pattern="space"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="after" padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block>
											<fo:inline font-size="10pt">
												<xsl:value-of select="//DSCL_ADRSMTGBRKR_1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row keep-with-previous="always">
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Name of Mortgage Broker</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Address</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Signature</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:inline font-size="8pt">
							<xsl:text>NOTE: This Disclosure Statement must be given to a prospective Borrower at least 48 hours before the execution of a mortgage.</xsl:text>
						</fo:inline>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

</xsl:stylesheet>
