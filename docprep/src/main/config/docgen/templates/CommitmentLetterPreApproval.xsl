<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
05/Feb/2008 DVG #DG694 FXP17570: MyNext CR18 - Correct spelling on French Commitment letter 
03/May/2007 SL #FXP16678  Express Standard French Commitment Template
12/Jul/2006  #3771 - BW - 2nd Page of Commitment Letter is Printing Off Blank 
30/May/2006 DVG #DG428 #2819  Dundee - CMHC fee missing from commitment  
25/Apr/2006 DVG #DG408 #3057  Commitment Letter -doesn't display the unit number  
28/Oct/2005 DVG #DG350 #2304  Solicitor package/Commitment Letter text changes  
19/Oct/2005 DVG #DG336 #2301  Commitment letter <LenderName> tag  
30/Aug/2005 DVG #DG304 #1941  Commitment Letter changes  
	conditions now are split into 2 mutually exclusive sections: terms&cond. and cond. upon
05/Aug/2005 DVG #DG286  #1861  - improvements for Merix included here too
12/Jul/2005 DVG #DG256 #1788  Commitment Letter - minor
04/Jul/2005 DVG #DG244 #1735  NBrook Logo 
	- configure the template for variable logos
24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package 
- Many changes to become the Xprs standard	-->
<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="text"/>	

  <!--#FXP16678-->
	<xsl:variable name="lendName" select="translate(/*/Deal/LenderProfile/lenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />
	<xsl:variable name="logoFile">
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageEnglish">
				<xsl:value-of select="concat('Logo',concat($lendName,'.xsl'))" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat('Logo',concat(concat($lendName,'_fr'),'.xsl'))" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />
	
	<xsl:template match="/">
		<!--#DG244 -->
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
		
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageEnglish">
				<xsl:call-template name="EnglishCommitmentLetter"/><!--#DG286 -->
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="FrenchCommitmentLetter"/><!--#FXP16678-->
			</xsl:otherwise>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<!--#DG286 -->
	<xsl:template name="EnglishCommitmentLetter">
		<xsl:call-template name="EnglishFooter"/>
		<xsl:call-template name="EnglishPage1"/>
		<!--xsl:call-template name="EnglishPage2Start"/-->
		<!--xsl:call-template name="EnglishPage2"/-->
		<!--	xsl:call-template name="EnglishPage3Start"/-->
		<!--xsl:call-template name="EnglishPage3"/-->
		<xsl:call-template name="EnglishComponentDetails"/>		
		<xsl:call-template name="EnglishPage4"/>
		<!-- <xsl:call-template name="EnglishPage5"/> -->
	</xsl:template>
	
	<xsl:template name="EnglishPage1">
		<xsl:text><!--{ \par }\pard \ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\par }} -->
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \<!--#DG336 --> \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs28 MORTGAGE PRE-APPROVAL\par }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text> <!--#DG244 -->
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell {\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalc
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell 
Tel: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\ul\b </xsl:text> TO: <xsl:text>\b0\ul0\tab </xsl:text>
		<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par\tab </xsl:text>
		</xsl:for-each>
		<xsl:text>

</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par\tab 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par\tab </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par\tab 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
{\b\f1\fs18\ul Underwriter: \par\ul0\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text> \cell }
{\b\f1\fs18\ul Date: \par\ul0\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
		<xsl:text>
		\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchCurrentTime"/>
		<xsl:text>
		\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\ul\b </xsl:text> C/O: <xsl:text>\cell }
{\b\f1\fs18 Broker Reference Number: \b0 </xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
<xsl:text>\par
\b\f1\fs18 Lender Reference Number: \b0 </xsl:text>
<xsl:value-of select="//Deal/dealId"/>
		<xsl:text> \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BORROWER(S):  \b0 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
{\i\f1 We are pleased to confirm that you have been pre-approved for a Mortgage Loan with the following terms and conditions:\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
<!-- Express 4.1 add commitment expiry date start  -->
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx3636
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx7272
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\qc\b\f1\fs18\ul PURPOSE: \par\b0\ul0  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
{\qc\b\f1\fs18\ul PURCHASE PRICE: \par\b0\ul0  </xsl:text>
		<xsl:value-of select="//Deal/totalPurchasePrice"/>
		<xsl:text>\cell }
{\qc\b\f1\fs18\ul COMMITMENT EXPIRY DATE: \par\b0\ul0  </xsl:text>
		<xsl:value-of select="//Deal/commitmentExpirationDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx3636
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx7272
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx10908
\row 
}
<!-- Express 4.1 add commitment expiry date end -->
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 

</xsl:text>
		<!--#DG408 xsl:for-each select="//CommitmentLetter/Properties/Property"-->
		<xsl:for-each select="/*/Deal/Property">
			<xsl:text></xsl:text>
		</xsl:for-each>
		<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added Product Type Starts-->
{\b\f1\fs18 PRODUCT TYPE:  \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ProductType"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added Product Type Ends-->
{\b\f1\fs18 LOAN TYPE:  \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Product"/>
		<xsl:text>\cell\par }
{\b\f1\fs18}{\f1\fs18 \cell </xsl:text>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 INTEREST RATE:}{\f1\fs18 \cell </xsl:text>
	<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added a check for displaying 'N/A*' for Net Interest Date Starts-->
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId=1">
		<xsl:text>N/A*</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/InterestRate"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added a check for displaying 'N/A*' for Net Interest Date Starts-->
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 MTG.INSUR.PREMIUM:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs18 TERM:}{\f1\fs18 \cell </xsl:text>
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added a check for displaying 'N/A*' for Term Starts-->		
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' or /*/Deal/MtgProd/underwriteAsTypeId=2">
		<xsl:text>N/A*</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/PaymentTerm"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added a check for displaying 'N/A*' for Term Ends-->
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 TOTAL LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 AMORTIZATION PERIOD:}{\f1\fs18 \cell </xsl:text>
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added a check for displaying 'N/A*' for Amortization Period Starts-->		
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' or /*/Deal/MtgProd/underwriteAsTypeId=2">
		<xsl:text>N/A*</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/Amortization"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added a check for displaying 'N/A*' for Amortization Period Ends-->
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}

<!--#DG428 moved up and shift the others down -->
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC PAYMENT AMOUNT\b0:\cell 
</xsl:text>
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId=1">
		<xsl:text>N/A*</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/TotalPayment"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
{\b\f1\fs18 PAYMENT FREQUENCY\b0:\cell 
</xsl:text>
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added a check for displaying 'N/A*' for Payment Frequency Starts-->
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId=1">
		<xsl:text>N/A*</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/PaymentFrequency"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added a check for displaying 'N/A*' for Payment Frequency Ends-->
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908 
\row 
}

\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 REPAYMENT TYPE\b0:\cell 
</xsl:text>
		<xsl:value-of select="//Deal/repaymentType"/>
		<xsl:text>\cell\par }
{\b\f1\fs18 }{\f1\fs18 \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row}

<!-- Express 4.1 PST PAYABLE ON INSUR PREMIUM: start  -->
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\tab PST PAYABLE ON INSUR PREMIUM:\b0\cell }{\f1\fs18 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
<!-- Express 4.1 PST PAYABLE ON INSUR PREMIUM: end  -->

\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\tab MORTGAGE FEES:\cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Fees/Fee">
			<xsl:text>
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell }{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
<xsl:text>\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\tab \cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>	
		<!-- MCM Impl Team. | XS_16.3 | 25-Aug-2008 | Component Note added - Starts -->
<xsl:choose>		
<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' ">
<xsl:text>
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr208\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }</xsl:text>

		<xsl:text>{\b\f1\fs18 * Please see the Terms and Conditions or Component Details Section for more details : \par \cell }</xsl:text>	


<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
</xsl:text>
</xsl:when>
</xsl:choose>	
<!-- MCM Impl Team. | XS_16.3 | 25-Aug-2008 | Component Note added - Ends -->
		
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 In this approval and any schedule(s) to this approval, }{\b\f1\fs18 you and your}
{\f1\fs18  mean the Borrower, Co-Borrower and </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>, if any, and }{\b\f1\fs18 
we, our and us }{\f1\fs18  mean </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
\par \par 

All of our normal requirements and, if applicable, those of the mortgage insurer must be 
met. All costs, including legal, survey, mortgage insurance etc. are for the account of the 
<!--#DG350 applicant-->borrower(s). The mortgage insurance premium (if applicable) will be added to the mortgage. Any 
fees specified herein may be deducted from the Mortgage advance. If for any reason the loan is 
not advanced, you agree to pay all application, legal, appraisal and survey costs incurred in 
this transaction.\par \par 

This Pre-Approval  is subject to the details and terms outlined herein.
\par
\par Thank you for choosing </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> for your financing.
\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 
\par \page 
\par }</xsl:text>
	</xsl:template>
	<!--
<xsl:template name="EnglishPage2Start">
<xsl:text>{\sect } \linex0\headery706\footery260\endnhere\sectdefaultcl</xsl:text>
</xsl:template>
-->
	<xsl:template name="EnglishPage2">
		<xsl:text>\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul TERMS AND CONDITIONS:
\par 
\par Mortgage:
\par }
{\f1\fs18 
This Mortgage will be subject to all extended terms set forth in our standard form of mortgage contract or in the mortgage contract prepared by our solicitors, whichever the case may be, and insured mortgage loans will be subject to the provisions of the National Housing Act (N.H.A.) and the regulations thereunder.\par }
{\b\f1\fs18\ul \par Rate Guarantee and Rate Adjustment Policies:\par }
{\f1\fs18 Provided that the Mortgage Loan is advanced by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text>, the interest rate will be </xsl:text>
		<xsl:value-of select="//CommitmentLetter/InterestRate"/>
		<xsl:text> per annum, calculated half yearly, not in advance. In the event that the effective rate of interest is lower at closing, the interest rate may be adjusted.
\par 
\par }{\b\f1\fs18\ul Title Requirements / Title Insurance:
\par }{\f1\fs18 Title to the Property must be acceptable to our Solicitor who will ensure that the Mortgagor(s) have good and marketable title in fee simple to the property and that it is clear of any encumbrances which might affect the priority of the </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> Mortgage. Alternatively, we will accept title insurance from a title insurance provider acceptable to </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>. The Mortgage must be a first charge on the property.
\par 
\par }{\b\f1\fs18\ul Survey Requirements / Title Insurance:
\par }{\f1\fs18 A survey or surveyors certificate completed by a recognized land surveyor and dated within the last ten (10) years is to be furnished to our Solicitor pri
or to any advance. The survey must confirm that the location of the building(s) on the property complies with all municipal, provincial and other government requirements. A survey older than ten (10) years, but not older than twenty (20) years, may be accepted provided that it is accompanied by a Declaration of Possession from the then present owners of the property. Alternatively, we will accept title insurance from a title insurance provider acceptable to </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>.
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<!--
<xsl:template name="EnglishPage3Start">
<xsl:text>{\sect }\sectd \linex0\headery706\footery260\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Page {\field{\*\fldinst {\cs22  PAGE }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}{\f1\fs20  of }{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
</xsl:template>
-->
	<xsl:template name="EnglishPage3">
		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul Privilege Payment Policies: }{\f1\fs18 \par This mortgage is CLOSED. The Mortgagor (borrower), when not in default of any of the terms, covenants, conditions or provisions contained in the mortgage, shall have the following privileges for payment of extra principal amounts: 1) During each 12 month period following the interest adjustment date the borrower may, without penalty: i) make prepayments totaling up to twenty percent (20%) of the original principal amount of the mortgage; ii) increase regular monthly payment by a total up to twenty percent (20%) of the original monthly payment amount so as to reduce the amortization of the mortgage without changing term. 2) If the mortgaged property is sold pursuant to a bona fide arms length agreement, the borrower may repay the mortgage in full upon payment of three (3) months interest penalty calculated at the current interest rate of the mortgage. 3) In addition to the prepayment privileges described in paragraph 1, at any time after the third anniversary of the interest adjustment date, the borrower may prepay the mortgage in whole or in part upon payment of three (3) months interest penalty calculated at the current interest rate of the mortgage.\par \par }
{\b\f1\fs18\ul Interest Adjustment Payment }
{\f1\fs18 \par Prior to commencement of the regular monthly loan payments interest will accrue at the rate payable on the Mortgage loan, on all funds advanced to the Mortgagor(s). Interest will be computed from the advance date and will become due and payable on the first day of the month next following the advance date.
\par 
\par }{\b\f1\fs18\ul Prepayment Policies:
\par }{\f1\fs18 You can prepay this mortgage on payment of 3 months simple interest on the principal amount owing at the prepayment date, or the interest rate differential,whichever is greater.TBD
\par 
\par }{\b\f1\fs18\ul Renewal:
\par }{\f1\fs18 At the sole discretion of BasisXPressLender, the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s).

\par 
\par }{\b\f1\fs18\ul Early Renewal: 
\par }{\f1\fs18 Provided that the Mortgage is not in 
default and that the building on the lands described in the Mortgage is used only as residence and comprises no more than four (4) dwelling units, you may renegotiate the term and payment provisions of the Mortgage prior to the end of the original term of
 the Mortgage, provided that: 
\par }\pard \ql \widctlpar\intbl\tx180\tx1000\faauto {\f1\fs18 \tab a) \tab the Mortgage may only be renegotiated for a closed term 
\par \tab b) \tab the Mortgagor(s) select a renewal option that results in the new Maturity Date being equal to or greater than 
\par \tab \tab the original Maturity Date of the current term; and 
\par \tab c) \tab payment of the interest rate differential and an early renewal processing fee is made upon execution of such 
\par \tab \tab renewal or amending agreement. 
\par }\pard \ql \widctlpar\intbl\faauto {\f1\fs18 
\par 
\par }{\b\f1\fs18\ul NSF Fee: 
\par }{\f1\fs18 There may be a fee charged for any payment returned due to insufficient funds or stopped payments.
\par 
\par }{\b\f1\fs18\ul Assumption Policies:
\par }{\f1\fs18 In the event that you subsequently enter into an agreement to sell, convey or transfer the property, any subsequent purchaser(s) who wish to assume this Mortgage, must be approved by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>, otherwise this Mortgage Loan will become due and payable upon your vacating or selling the property.
\par 
\par }{\b\f1\fs18\ul Portability Policies:
\par }{\f1\fs18 Should you decide to sell the mortgaged property and purchase another property, you may transfer the remaining mortgage balance together with the interest rate set out therein to the new property provided that the Mortgage Loan is not in default and a new mortgage loan application has been accepted by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> and all terms and conditions set out in the subsequent Mortgage Loan approval have been met.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishPage4">
		<!-- 12/Jul/2006  #3771-->
		<xsl:variable name="conditionIds" select="//CommitmentLetter/Conditions/Condition[(conditionId = 117 
			or conditionId = 109 or conditionId = 131 or conditionId = 226 or conditionId = 404 
			or conditionId = 408 or conditionId = 2005 or conditionId = 21 or conditionId = 185 or conditionId = 99 
			or conditionId = 159 or conditionId = 206 or conditionId = 211 or conditionId = 212 or conditionId = 213 
			or conditionId = 214 or conditionId = 335 or conditionId = 705 or conditionId = 203 or conditionId = 205 
			or conditionId = 216 or conditionId = 217 or conditionId = 713 or conditionId = 764 or conditionId = 65 
			or conditionId = 71 or conditionId = 471)]"/>
		<xsl:if test="count($conditionIds)&gt;0">
			<!-- 12/Jul/2006  #3771-->
			<!--#DG304 -->
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\b\f1\fs18\ul TERMS AND CONDITIONS\par 
\cell\row}</xsl:text>
			<!--#DG304 all changes to the list below should be syncronized to the 'conditional upon' section -->
			<xsl:for-each select="$conditionIds">
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
				<xsl:value-of select="position()"/>
				<xsl:text>.\cell }
{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
			</xsl:for-each>
			<xsl:text>\pard\page\par </xsl:text>
			<!-- 12/Jul/2006  #3771-->
		</xsl:if>
		<!-- 12/Jul/2006  #3771-->
		<!--#DG304 end -->
		<xsl:if test="count($conditionSections)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\pard 
\ql \widctlpar\intbl\itap2\faauto {\b\f1\fs18\ul THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING
\par 
\par }{\f1\fs18 The following conditions must be met, and the requested documents must be received in form and content satisfactory to </xsl:text>
			<!--#DG336 xsl:value-of select="//SolicitorsPackage/LenderName"/-->
			<xsl:value-of select="//CommitmentLetter/LenderName"/>
			<!--#DG238 -->
			<xsl:text> no later than ten (10) days prior to the advance of the mortgage. Failure to do so may delay or void this commitment.
\par \par \cell }
\pard \ql \widctlpar\intbl\itap2\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
 
 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908 
\row 
}</xsl:text>
		</xsl:if>
		<!--  	<xsl:if test="//CommitmentLetter/Conditions">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
		</xsl:if> -->
		<!--#DG304 all changes to the list below should be syncronized to the 'terms and conditions' section -->
		<!--  	<xsl:for-each select="//CommitmentLetter/Conditions/Condition[not(conditionId = 117 
			or conditionId = 109 or conditionId = 131 or conditionId = 226 or conditionId = 404 
			or conditionId = 408 or conditionId = 2005 or conditionId = 21 or conditionId = 185 or conditionId = 99 
			or conditionId = 159 or conditionId = 206 or conditionId = 211 or conditionId = 212 or conditionId = 213 
			or conditionId = 214 or conditionId = 335 or conditionId = 705 or conditionId = 203 or conditionId = 205 
			or conditionId = 216 or conditionId = 217 or conditionId = 713 or conditionId = 764 or conditionId = 65 
			or conditionId = 71 or conditionId = 471)]">
			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }

{\f1\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>-->
		<xsl:call-template name="EnglishPage4Asum"/>
		<xsl:call-template name="EnglishPage4Cond"/>
		<xsl:call-template name="EnglishPage4Flex"/>
		<xsl:call-template name="EnglishPage4Inst"/>
		<xsl:call-template name="EnglishPage4Other"/>
		<xsl:call-template name="EnglishPage4Port"/>
		<xsl:call-template name="EnglishPage4Prep"/>
		<xsl:call-template name="EnglishPage4Radj"/>
		<xsl:call-template name="EnglishPage4Svch"/>
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10		
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \par \par }
{\b\f1\fs18\ul Requests for any changes to the terms and conditions of the Commitment will only be considered if your request is received by us in writing at least 5 business days prior to the Advance Date.
\par\keepn 
\par\keepn ACCEPTANCE:
\par\keepn 
\par\keepn }
{\f1\fs18 This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> after which time, if not accepted, shall be considered null and void. Furthermore, the mortgage must be advanced by no later than </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text> at which time it expires.
\par\keepn 
\par\keepn }\pard \ql \widctlpar\intbl\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright {\f1\fs18 \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
\par\keepn 
\par\keepn \tab Authorized by:___________________________________________
\par\keepn \tab \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>
\par\keepn 
\par\keepn 
\par\keepn }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell\row }

\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page }</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishPage5">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s15\ql\widctlpar\intbl\keepn\faauto 
{\b\f1\fs18 I / We, hereby authorize </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> to debit the account indicated below with payments on the Mortgage Loan (including taxes and life insurance premiums, if applicable)
\par }
\pard\plain \ql \widctlpar\intbl\keepn\faauto 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\keepn\faauto 
{\b\f1\fs18 [ ] Chequing/Savings Account \cell  [ ] Chequing Account  \cell  [ ] Current Account No._________________ \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Bank _______________________________________________ Branch_________________________________________________
\par 
\par Address____________________________________________________________________________________________________
\par 
\par Transit No.________________________________ Telephone No._____________________________________________________
\par 
\par }{\b\f1\fs18 A sample "VOID" cheque is enclosed.}{\f1\fs18 
\par 
\par }{\b\f1\fs18 I / We </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/>
		<!--#DG256 -->
		<xsl:text> warrant that all representations made by me/us and all the information submitted to you in connection with my/our mortgage application are true and accurate. I / We fully understand that any misrepresentations of fact contained in my/our mortgage application or other documentation entitles you to decline to advance a portion or all of the loan proceeds, or to demand immediate repayment of all monies secured by the Mortgage.
\par \par \par 

I / We, the undersigned <!--#DG350 applicant-->borrowers accept the terms of this Mortgage 
Commitment as stated above and agree to fulfill the conditions of approval outlined herein.}
{\f1\fs18 \par \par }

\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7800\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell ___________________________
\par <!--#DG350 APPLICANT-->BORROWER
\par 
\par \cell ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par CO-<!--#DG350 APPLICANT-->BORROWER
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text>
	</xsl:template>
	<xsl:variable name="conditionSections" select="/*/CommitmentLetter/Conditions/Condition[not(conditionId = 117 
			or conditionId = 109 or conditionId = 131 or conditionId = 226 or conditionId = 404 
			or conditionId = 408 or conditionId = 2005 or conditionId = 21 or conditionId = 185 or conditionId = 99 
			or conditionId = 159 or conditionId = 206 or conditionId = 211 or conditionId = 212 or conditionId = 213 
			or conditionId = 214 or conditionId = 335 or conditionId = 705 or conditionId = 203 or conditionId = 205 
			or conditionId = 216 or conditionId = 217 or conditionId = 713 or conditionId = 764 or conditionId = 65 
			or conditionId = 71 or conditionId = 471)]"/>
	<xsl:template name="EnglishPage4Asum">
		<xsl:variable name="conditionAsum" select="$conditionSections[sectionCode = 'ASUM']"/>
		<xsl:if test="count($conditionAsum)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Assumption Policies\par 
\cell \row}</xsl:text>
			<xsl:for-each select="$conditionAsum">
				<xsl:sort data-type="number" select="sequenceId"/>
				<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }\row</xsl:text>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishPage4Cond">
		<xsl:variable name="conditionCond" select="$conditionSections[sectionCode = 'COND' and (conditionResponsibilityRoleId=0 
			or conditionResponsibilityRoleId=50 or conditionResponsibilityRoleId=5)]"/>
		<xsl:if test="count($conditionCond)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Conditions 
\cell \row}</xsl:text>
			<xsl:variable name="conditionCondBorrower" select="$conditionCond[conditionResponsibilityRoleId=0]"/>
			<xsl:if test="count($conditionCondBorrower)&gt;0">
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs18 Borrower\par 
You must satisfy the following conditions at least 10 days before closing: \par
\cell \row}</xsl:text>
				<xsl:for-each select="$conditionCondBorrower">
					<xsl:sort data-type="number" select="sequenceId"/>
					<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
					<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto
{\f1\fs18 </xsl:text>
					<xsl:for-each select="./Line">
						<xsl:value-of select="."/>
						<xsl:if test="not(position()=last())">
							<xsl:text>\par </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>\par \cell }\row</xsl:text>
				</xsl:for-each>
			</xsl:if>
			<xsl:variable name="conditionSolicitor" select="$conditionCond[conditionResponsibilityRoleId=50]"/>
			<xsl:if test="count($conditionSolicitor)&gt;0">
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs18 Solicitor\par 
Your Solicitor is to provide the following before closing: \par
\cell \row}</xsl:text>
				<xsl:for-each select="$conditionSolicitor">
					<xsl:sort data-type="number" select="sequenceId"/>
					<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
					<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto
{\f1\fs18 </xsl:text>
					<xsl:for-each select="./Line">
						<xsl:value-of select="."/>
						<xsl:if test="not(position()=last())">
							<xsl:text>\par </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>\par \cell }\row</xsl:text>
				</xsl:for-each>
			</xsl:if>
			<xsl:variable name="conditionLender" select="$conditionCond[conditionResponsibilityRoleId=5]"/>
			<xsl:if test="count($conditionLender)&gt;0">
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs18 Lender\par 
We will obtain the following: \par
\cell \row}</xsl:text>
				<xsl:for-each select="$conditionLender">
					<xsl:sort data-type="number" select="sequenceId"/>
					<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
					<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto
{\f1\fs18 </xsl:text>
					<xsl:for-each select="./Line">
						<xsl:value-of select="."/>
						<xsl:if test="not(position()=last())">
							<xsl:text>\par </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>\par \cell }\row</xsl:text>
				</xsl:for-each>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishPage4Flex">
		<xsl:variable name="conditionFlex" select="$conditionSections[sectionCode = 'FLEX']"/>
		<xsl:if test="count($conditionFlex)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Payment Flexibility Options\par 
\cell \row}</xsl:text>
			<xsl:for-each select="$conditionFlex">
				<xsl:sort data-type="number" select="sequenceId"/>
				<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }\row</xsl:text>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishPage4Inst">
		<xsl:variable name="conditionInst" select="$conditionSections[sectionCode = 'INST']"/>
		<xsl:if test="count($conditionInst)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Instructions\par 
\cell \row}</xsl:text>
			<xsl:for-each select="$conditionInst">
				<xsl:sort data-type="number" select="sequenceId"/>
				<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }\row</xsl:text>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishPage4Other">
		<xsl:variable name="conditionOther" select="$conditionSections[sectionCode = 'OTHR']"/>
		<xsl:if test="count($conditionOther)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Other\par 
\cell \row}</xsl:text>
			<xsl:for-each select="$conditionOther">
				<xsl:sort data-type="number" select="sequenceId"/>
				<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }\row</xsl:text>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishPage4Port">
		<xsl:variable name="conditionPort" select="$conditionSections[sectionCode = 'PORT']"/>
		<xsl:if test="count($conditionPort)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Portability Options\par 
\cell \row}</xsl:text>
			<xsl:for-each select="$conditionPort">
				<xsl:sort data-type="number" select="sequenceId"/>
				<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }\row</xsl:text>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishPage4Prep">
		<xsl:variable name="conditionPrep" select="$conditionSections[sectionCode = 'PREP']"/>
		<xsl:if test="count($conditionPrep)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Prepayment Policies\par 
\cell \row}</xsl:text>
			<xsl:for-each select="$conditionPrep">
				<xsl:sort data-type="number" select="sequenceId"/>
				<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }\row</xsl:text>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishPage4Radj">
		<xsl:variable name="conditionRadj" select="$conditionSections[sectionCode = 'RADJ']"/>
		<xsl:if test="count($conditionRadj)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Rate Adjustment Policies\par 
\cell \row}</xsl:text>
			<xsl:for-each select="$conditionRadj">
				<xsl:sort data-type="number" select="sequenceId"/>
				<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }\row</xsl:text>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishPage4Svch">
		<xsl:variable name="conditionSvch" select="$conditionSections[sectionCode = 'SVCH']"/>
		<xsl:if test="count($conditionSvch)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Administration &amp; Service Fees\par 
\cell \row}</xsl:text>
			<xsl:for-each select="$conditionSvch">
				<xsl:sort data-type="number" select="sequenceId"/>
				<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
				<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }\row</xsl:text>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="EnglishFooter">
		<xsl:text>{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }{\f1\fs16 of }
{\field{\*\fldinst {NUMPAGES }}}}}</xsl:text>
	</xsl:template>


	<!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<!--#FXP16678 -->
	<xsl:template name="FrenchCommitmentLetter">
		<xsl:call-template name="FrenchFooter"/>
		<xsl:call-template name="FrenchPage1"/>
		<xsl:call-template name="FrenchComponentDetails"/>
		<xsl:call-template name="FrenchPage4"/>
		<!-- <xsl:call-template name="FrenchPage5"/> -->
	</xsl:template>
	
	<xsl:template name="FrenchPage1">
		<xsl:text><!--{ \par }\pard \ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\par }} -->
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \<!--#DG336 --> \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs28 PR�-APPROBATION HYPOTH�CAIRE\par }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text><!--#DG244 -->
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell {\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalc
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell 
Tel: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\ul\b </xsl:text> TO: <xsl:text>\b0\ul0\tab </xsl:text>
		<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par\tab </xsl:text>
		</xsl:for-each>
		<xsl:text>

</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par\tab 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par\tab </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par\tab 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
{\b\f1\fs18\ul Sousscripteur: \par\ul0\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text> \cell }
{\b\f1\fs18\ul Date: \par\ul0\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
		<xsl:text>
		\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchCurrentTime"/>
		<xsl:text>
		\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\ul\b </xsl:text> C/O: <xsl:text>\cell }
{\b\f1\fs18 Num�ro de r�f�rence du courtier: \b0 </xsl:text>
		<xsl:value-of select="//Deal/sourceApplicationId"/>
<xsl:text>\par
\b\f1\fs18 Num�ro de r�f�rence du pr�teur: \b0 </xsl:text>
<xsl:value-of select="//Deal/dealId"/>
		<xsl:text> \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 EMPRUNTEUR(S):  \b0 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
{\i\f1 Nous sommes heureux de confirmer que votre demande de pr�t hypoth�caire a �t� pr�-approuv�e selon les conditions stipul�es ci-dessous:\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
<!-- Express 4.1 add commitment expiry date start  -->
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx3636
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx7272
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\qc\b\f1\fs18\ul OBJET DU PR\'caT: \par\b0\ul0  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
{\qc\b\f1\fs18\ul PRIX D\rquote ACHAT: \par\b0\ul0  </xsl:text>
		<xsl:value-of select="//Deal/totalPurchasePrice"/>
		<xsl:text>\cell }
{\qc\b\f1\fs18\ul DATE D\rquote ECHEANCE DE L\rquote ENGAGEMENT: \par\b0\ul0  </xsl:text>
		<xsl:value-of select="//Deal/commitmentExpirationDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx3636
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx7272
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3636 \cellx10908
\row 
}
<!-- Express 4.1 add commitment expiry date end -->
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 

</xsl:text>
		<!--#DG408 xsl:for-each select="//CommitmentLetter/Properties/Property"-->
		<xsl:for-each select="/*/Deal/Property">
			<xsl:text></xsl:text>
		</xsl:for-each>
		<xsl:text>
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added Product Type Starts-->
{\b\f1\fs18 Type de produit: \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ProductType"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
<!--  MCM Impl Team 30-Jul-2008 XS_16.2 Added Product Type Ends-->
{\b\f1\fs18 TYPE DE PRET:  \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Product"/>
		<xsl:text>\cell\par }
{\b\f1\fs18 }{\f1\fs18 \cell </xsl:text>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 MONTANT DU PR\'caT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 TAUX D'INT\'c9R\'caT:}{\f1\fs18 \cell </xsl:text>
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId=1">
		<xsl:text>S/O</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/InterestRate"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 PRIME D'ASSUR.PR\'caT HYPO.:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs18 TERME:}{\f1\fs18 \cell </xsl:text>
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' or /*/Deal/MtgProd/underwriteAsTypeId=2">
		<xsl:text>S/O</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/PaymentTerm"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 MONTANT TOTAL DU PR\'caT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 P\'c9RIODE D'AMORTISSEMENT:}{\f1\fs18 \cell </xsl:text>
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' or /*/Deal/MtgProd/underwriteAsTypeId=2">
		<xsl:text>S/O</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/Amortization"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 MONTANT DE BASE DE PAIEMENT\b0:\cell </xsl:text>
<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId=1">
		<xsl:text>S/O</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/InterestRate"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
{\b\f1\fs18 FR�QUENCE DES PAIEMENTS\b0:\cell 
</xsl:text>
		<xsl:choose>		
		<xsl:when test="/*/Deal/MtgProd/componentEligibleFlag ='Y' and /*/Deal/MtgProd/underwriteAsTypeId=1">
		<xsl:text>S/O</xsl:text>
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="/*/CommitmentLetter/PaymentFrequency"/></xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}

\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 TYPE DE REMBOURSEMENT\b0:\cell 
</xsl:text>
		<xsl:value-of select="//Deal/repaymentType"/>
		<xsl:text>\cell\par }
{\b\f1\fs18 }{\f1\fs18 \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row}

<!-- Express 4.1 french PST PAYABLE ON INSUR PREMIUM: start  -->
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\tab TAXE PROV.SUR PRIME D\rquote ASSUR.:\b0\cell }{\f1\fs18 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
<!-- Express 4.1 french PST PAYABLE ON INSUR PREMIUM: end  -->

\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\tab FRAIS HYPOTHECAIRES:\cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell }{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
<xsl:text>\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\tab \cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>		
		    <!-- MCM Impl Team. | XS_16.28 | 22-Aug-2008 | Component Note added - Starts -->
    <xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr208\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par 
</xsl:text>
	  <xsl:if test="/*/Deal/MtgProd/componentEligibleFlag='Y'">
	      <!-- #DG792 -->
		    <xsl:text>\b Veuillez voir les termes et conditions ou la 
section Details de la composante pour plus d'informations\par 
\b0 </xsl:text>	
	  </xsl:if>
    <xsl:text>\cell }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr208\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
</xsl:text>
<!-- MCM Impl Team. | XS_16.28 | 22-Aug-2008 | Component Note added - Ends -->	
		
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 Dans la pr�sente et toute (s) annexe (s) s'y rattachant, les terms }{\b\f1\fs18 vous et votre}
{\f1\fs18  signifient l'emprunteur, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>, s'il y a lieu, et les termes }{\b\f1\fs18 
nous et notre }{\f1\fs18 d�signent </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
\par \par 

Toutes les conditions standard d'octroi de pr�t hypoth�caire, le cas �ch�ant, doivent �tre 
remplies. Tous les frais, y compris honoraries de notaire, d'expertise, d'assurance pr�t 
hypoth�caire, etc. sont pour le compte du (des) demandeur (s). La prime d'assurance pr�t 
hypoth�caire, le cas �ch�ant, sera ajout�e au pr�t hypoth�caire. Tous frais indiqu�s dans la 
pr�sente sont deductibles de l'rquote avance hypoth�caire. Si, pour une raison quelconque, le 
pr�t n'est pas d�bours�, il vous incombe de payer tous les frais engag�s, tels que frais 
d'ouverture de dossier, honoraires, frais d'�valuation et d'expertise. \par \par

La pr�sente pr�-approbation est assujettie aux conditions stipul�es dans la pr�sente. \par\par
Merci d'avoir choisi </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> pur votre demande de pr�t hypoth�caire.
\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 
\par \page 
\par } </xsl:text>
	</xsl:template>

	<xsl:template name="FrenchPage4">
	<!-- 12/Jul/2006  #3771-->
		<xsl:variable name="conditionIds"  select="//CommitmentLetter/Conditions/Condition[(conditionId = 117 
			or conditionId = 109 or conditionId = 131 or conditionId = 226 or conditionId = 404 
			or conditionId = 408 or conditionId = 2005 or conditionId = 21 or conditionId = 185 or conditionId = 99 
			or conditionId = 159 or conditionId = 206 or conditionId = 211 or conditionId = 212 or conditionId = 213 
			or conditionId = 214 or conditionId = 335 or conditionId = 705 or conditionId = 203 or conditionId = 205 
			or conditionId = 216 or conditionId = 217 or conditionId = 713 or conditionId = 764 or conditionId = 65 
			or conditionId = 71 or conditionId = 471)]"/>
		<xsl:if test="count($conditionIds)&gt;0">
	<!-- 12/Jul/2006  #3771-->
	
		<!--#DG304 -->
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18\ul TERMES ET CONDITIONS\par 
\cell \row}</xsl:text>               

		<!--#DG304 all changes to the list below should be syncronized to the 'conditional upon' section -->
		
			<xsl:for-each select="$conditionIds">

			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>

			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }
{\f1\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 
</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard\page\par </xsl:text>
		<!-- 12/Jul/2006  #3771-->
		</xsl:if>
		<!-- 12/Jul/2006  #3771-->
		<!--#DG304 end -->
<xsl:if test="count($conditionSections)&gt;0">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\pard 
\ql \widctlpar\intbl\itap2\faauto 
{\b\f1\fs18\ul LA PRESENTE LETTRE D'ENGAGEMENT EST ASSUJETTIE AUX CONDITIONS SUIVANTES:
\par 
\par }
{\f1\fs18 Les conditions suivantes doivent �tre remplies et les documents indiqu�s doivent �tre 
re�us et jug�s acceptables quant au fond et � la forme par </xsl:text>
		<!--#DG336 xsl:value-of select="//SolicitorsPackage/LenderName"/-->
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<!--#DG238 -->
		<xsl:text> , au plus tard dix (10) jours avant l'octroi du pr�t hypoth�caire, � d�faut de quoi 
la pr�sente Lettre d'engagement pourrait �tre annul�e.
\par \par \cell }
\pard \ql \widctlpar\intbl\itap2\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 

\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}</xsl:text></xsl:if>
	<!--  	<xsl:if test="//CommitmentLetter/Conditions">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
		</xsl:if>-->
		<!--#DG304 all changes to the list below should be syncronized to the 'terms and conditions' section -->
<!--  		<xsl:for-each select="//CommitmentLetter/Conditions/Condition[not(conditionId = 117 
			or conditionId = 109 or conditionId = 131 or conditionId = 226 or conditionId = 404 
			or conditionId = 408 or conditionId = 2005 or conditionId = 21 or conditionId = 185 or conditionId = 99 
			or conditionId = 159 or conditionId = 206 or conditionId = 211 or conditionId = 212 or conditionId = 213 
			or conditionId = 214 or conditionId = 335 or conditionId = 705 or conditionId = 203 or conditionId = 205 
			or conditionId = 216 or conditionId = 217 or conditionId = 713 or conditionId = 764 or conditionId = 65 
			or conditionId = 71 or conditionId = 471)]">
			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }

{\f1\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>-->
		<xsl:call-template name="FrenchPage4Asum"/>
		<xsl:call-template name="FrenchPage4Cond"/>
		<xsl:call-template name="FrenchPage4Flex"/>
		<xsl:call-template name="FrenchPage4Inst"/>
		<xsl:call-template name="FrenchPage4Other"/>
		<xsl:call-template name="FrenchPage4Port"/>
		<xsl:call-template name="FrenchPage4Prep"/>
		<xsl:call-template name="FrenchPage4Radj"/>
		<xsl:call-template name="FrenchPage4Svch"/>
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrs\brdrw10		 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \par \par }
{\b\f1\fs18\ul Toute demande de modification aux conditions stipul�es dans la pr�sente lettre 
doit nous parvenir par �crit au moins 5 jours ouvrables avant la date de l'avance.
\par\keepn 
\par\keepn ACCEPTATION:
\par\keepn 
\par\keepn }
{\f1\fs18 La pr�sente Lettre d'engagement peut �tre accept�e jusqu'� 23 h 59 le </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> apr�s quoi, elle sera consid�r�e comme nulle et non avenue. </xsl:text>
		<!--xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text> at which time it expires.</-->
		<xsl:text>
\par\keepn 
\par\keepn }\pard \ql \widctlpar\intbl\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
\par\keepn 
\par\keepn \tab Autoris� par:___________________________________________
\par\keepn \tab \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>
\par\keepn 
\par\keepn 
\par\keepn }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell\row }

\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page }</xsl:text>
	</xsl:template>

<xsl:template name="FrenchPage4Asum">  
  <xsl:variable name="conditionAsum" select="$conditionSections[sectionCode = 'ASUM']"/>  
  <xsl:if test="count($conditionAsum)&gt;0">
  <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Suppositions\par 
\cell \row}</xsl:text>		
	<xsl:for-each select="$conditionAsum">
	<xsl:sort data-type="number" select="sequenceId"/>
	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
	<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
		 <xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		</xsl:for-each>		
  </xsl:if>
</xsl:template>

<xsl:template name="FrenchPage4Cond"> 
			<xsl:variable name="conditionCond" select="$conditionSections[sectionCode = 'COND' and (conditionResponsibilityRoleId=0 
			or conditionResponsibilityRoleId=50 or conditionResponsibilityRoleId=5)]"/>			
			<xsl:if test="count($conditionCond)&gt;0">
            <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Conditions 
\cell \row}</xsl:text>		
  <xsl:variable name = "conditionCondBorrower" select="$conditionCond[conditionResponsibilityRoleId=0]"/>
			<xsl:if test="count($conditionCondBorrower)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs18 EMPRUNTEUR\par 
Vous devez satisfaire les conditions suivantes au moins 10 jours avant la date de cl�ture : \par
\cell \row}</xsl:text>			
			<xsl:for-each select="$conditionCondBorrower">
			<xsl:sort data-type="number" select="sequenceId"/>
			<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
			<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto
{\f1\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		   </xsl:for-each>		
		</xsl:if>
   <xsl:variable name = "conditionSolicitor" select="$conditionCond[conditionResponsibilityRoleId=50]"/>
			<xsl:if test="count($conditionSolicitor)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs18 AVOCAT OU NOTAIRE :\par 
Votre avocat ou notaire fournira ce qui suit avant la cl�ture: \par
\cell \row}</xsl:text>			
			<xsl:for-each select="$conditionSolicitor">
			<xsl:sort data-type="number" select="sequenceId"/>
			<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
			<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto
{\f1\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		   </xsl:for-each>		
		</xsl:if>
  <xsl:variable name = "conditionLender" select="$conditionCond[conditionResponsibilityRoleId=5]"/>
			<xsl:if test="count($conditionLender)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs18 PR�TEUR:\par 
Nous obtiendrons les renseignements suivants: \par
\cell \row}</xsl:text>			
			<xsl:for-each select="$conditionLender">
			<xsl:sort data-type="number" select="sequenceId"/>
			<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
			<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto
{\f1\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		   </xsl:for-each>		
		</xsl:if>						
  </xsl:if>
</xsl:template>

<xsl:template name="FrenchPage4Flex">   
	  <xsl:variable name="conditionFlex" select="$conditionSections[sectionCode = 'FLEX']"/>	
     <xsl:if test="count($conditionFlex)&gt;0">
     <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Flexibilit� de paiement\par 
\cell \row}</xsl:text>		
	<xsl:for-each select="$conditionFlex">
	<xsl:sort data-type="number" select="sequenceId"/>
	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
	<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
		 <xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		</xsl:for-each>		
  </xsl:if>
</xsl:template>

<xsl:template name="FrenchPage4Inst">   
	  <xsl:variable name="conditionInst" select="$conditionSections[sectionCode = 'INST']"/>
	  <xsl:if test="count($conditionInst)&gt;0">
      <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Instructions\par 
\cell \row}</xsl:text>		
	<xsl:for-each select="$conditionInst">
	<xsl:sort data-type="number" select="sequenceId"/>
	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
	<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
		 <xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		</xsl:for-each>		
  </xsl:if>
   
</xsl:template>

<xsl:template name="FrenchPage4Other">   
	<xsl:variable name="conditionOther" select="$conditionSections[sectionCode = 'OTHR']"/>
    <xsl:if test="count($conditionOther)&gt;0">
    <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Autre\par 
\cell \row}</xsl:text>		
	<xsl:for-each select="$conditionOther">
	<xsl:sort data-type="number"  select="sequenceId" />
	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
	<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
		 <xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		</xsl:for-each>		
  </xsl:if>
</xsl:template>

<xsl:template name="FrenchPage4Port">    
	 <xsl:variable name="conditionPort" select="$conditionSections[sectionCode = 'PORT']"/>	
	 <xsl:if test="count($conditionPort)&gt;0">
     <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Portabilit�\par 
\cell \row}</xsl:text>		
	<xsl:for-each select="$conditionPort">
	<xsl:sort data-type="number" select="sequenceId"/>
	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
	<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
		 <xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		</xsl:for-each>		
  </xsl:if>   
</xsl:template>

<xsl:template name="FrenchPage4Prep">    
	 <xsl:variable name="conditionPrep" select="$conditionSections[sectionCode = 'PREP']"/>
     <xsl:if test="count($conditionPrep)&gt;0">
     <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Paiement d'avance\par 
\cell \row}</xsl:text>		
	<xsl:for-each select="$conditionPrep">
	<xsl:sort data-type="number" select="sequenceId"/>
	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
	<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
		 <xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		</xsl:for-each>		
  </xsl:if>
</xsl:template>

<xsl:template name="FrenchPage4Radj">  
	 <xsl:variable name="conditionRadj" select="$conditionSections[sectionCode = 'RADJ']"/>
	 <xsl:if test="count($conditionRadj)&gt;0">
     <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Rajustement de taux\par 
\cell \row}</xsl:text>		
	<xsl:for-each select="$conditionRadj">
	<xsl:sort data-type="number"  select="sequenceId"/>
	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
	<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
		 <xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		</xsl:for-each>		
  </xsl:if>   
</xsl:template>

<xsl:template name="FrenchPage4Svch">  
	 <xsl:variable name="conditionSvch" select="$conditionSections[sectionCode = 'SVCH']"/>
     <xsl:if test="count($conditionSvch)&gt;0">
     <xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl-108\trpaddr108\trpaddfl3\trpaddfr3\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\itap2\faauto {\b\f1\fs24 Frais de Service\par 
\cell \row}</xsl:text>		
	<xsl:for-each select="$conditionSvch">
	<xsl:sort data-type="number"  select="sequenceId"/>
	<xsl:text>\trowd \trgaph108\trleft-108\trkeep\trftsWidth1\trpaddl208\trpaddr108\trpaddfl3\trpaddfr3
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
	<xsl:text>\pard \ql \widctlpar\intbl\itap2\faauto{\f1\fs18 </xsl:text>
		 <xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }\row</xsl:text>
		</xsl:for-each>		
  </xsl:if>
</xsl:template>

	<xsl:template name="FrenchPage5">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s15\ql \widctlpar\intbl\keepn\faauto 
{\b\f1\fs18 J'autorise/Nous autorisons par la pr�sente </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> � d�biter le compte indiqu� ci-dessous du montant des versements hypoth�caires, y 
compris le montant des imp�ts fonciers et la prime d'assurance pr�t hypoth�caire, le cas �ch�ant.
\par }
\pard\plain \ql \widctlpar\intbl\keepn\faauto 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\keepn\faauto 
{\b\f1\fs18 [ ] Compte de ch�ques/d'�pargne \cell  [ ] Compte de ch�ques  \cell  [ ] Compte courant Num�ro.______________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Banque_______________________________________________ Succursale____________________________________________
\par 
\par Adresse____________________________________________________________________________________________________
\par 
\par No de succursale________________________________ No. de t�l�phone_______________________________________________
\par 
\par }{\b\f1\fs18 Ci-joint un ch�que marqu� 'ANNULE'.}{\f1\fs18 
\par 
\par }{\b\f1\fs18 J'affirme/Nous affirmons, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/><!--#DG256 -->
		<xsl:text> , que toutes les d�clarations formelles et toute l'information fournie � l'�gard de 
mon/notre demande de pr�t hypoth�caire sont exactes et v�ridiques. Je comprends/Nous comprenons 
parfaitement que toute fausse d�claration relative � ma/notre demande de pr�t hypoth�caire ou � 
toute autre documentation vous donne droit de refuser l'octroi d'une partie de l'ensemble du pr�t 
ou d'exiger le remboursement imm�diat de toutes les sommes garanties par l'hypoth�que.
\par \par \par 

Je/Nous, le (les) soussign�(s), accepte (acceptons) les conditions de la pr�sente Lettre 
d'engagement telles que stipul�es ci-dessus et je m'engage/nous engageons � en respecter les modalit�s.}
{\f1\fs18 \par \par }

\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7800\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par TEMOIN
\par 
\par \cell ___________________________
\par DEMANDEUR
\par 
\par \cell ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par TEMOIN
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par CO-DEMANDEUR
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\keepn\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par TEMOIN
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\keepn\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text>
	</xsl:template>

	<xsl:template name="FrenchFooter">
		<xsl:text>{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }
{\f1\fs16 of }
{\field{\*\fldinst {NUMPAGES }}}}}</xsl:text>
	</xsl:template>


	<!--#DG408 ================================================  -->
	<xsl:template name="propAdr">	
		<xsl:param name="separaPostal" select="'\line '"/>
    <xsl:if test="unitNumber != ''">
    	<xsl:value-of select="unitNumber"/>
    	<xsl:text> - </xsl:text>
    </xsl:if>
    <xsl:value-of select="propertyStreetNumber"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="propertyStreetName"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="streetType"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="streetDirection"/>
    <xsl:text>\par </xsl:text>
    <xsl:if test="propertyAddressLine2 != ''">
    	<xsl:value-of select="propertyAddressLine2"/>
    	<xsl:text>\par </xsl:text>
    </xsl:if>
    <xsl:value-of select="propertyCity"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="province"/>
    <xsl:value-of select="$separaPostal"/>
    <xsl:value-of select="propertyPostalFSA"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="propertyPostalLDU"/>
	</xsl:template>

  <!-- MCM Impl Team. | XS_16.3 | 25-Aug-2008 | EnglishComponentDetails template added to display Component Details as conditions - Starts -->
	
	<xsl:template name="EnglishComponentDetails">
		<xsl:variable name="conditionIds"  select="//CommitmentLetter/Conditions/Condition[(conditionId = 3001 
				or conditionId = 3002 or conditionId = 3003 or conditionId = 3004 or conditionId = 3005
				)]"/>		
		<xsl:if test="/*/CommitmentLetter/Conditions/Condition/ConditionSection='MCM' and /*/Deal/MtgProd/componentEligibleFlag='Y' and count($conditionIds)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\pard
\ql \widctlpar\intbl\faauto {\b\f1\fs18\ul COMPONENT DETAILS 
\par\cell}</xsl:text>
				<xsl:text>				
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10  
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908 
\row 
}</xsl:text>
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
			<xsl:for-each select="$conditionIds">					
				<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
				<xsl:value-of select="position()"/>
				<xsl:text>.\cell }
{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>\pard\par </xsl:text>
		</xsl:if>
	</xsl:template>
	
<!-- MCM Impl Team. | XS_16.3 | 25-Aug-2008 | EnglishComponentDetails template added to display Component Details as conditions - Ends -->
<!-- MCM Impl Team. | XS_16.28 | 25-Aug-2008 | FrenchComponentDetails template added to display Component Details as conditions - Starts -->	
  <xsl:template name="FrenchComponentDetails">
		<xsl:variable name="conditionIds"  select="//CommitmentLetter/Conditions/Condition[(conditionId = 3001 
				or conditionId = 3002 or conditionId = 3003 or conditionId = 3004 or conditionId = 3005
				)]"/>		
		<xsl:if test="/*/CommitmentLetter/Conditions/Condition/ConditionSection='MCM' and /*/Deal/MtgProd/componentEligibleFlag='Y' and count($conditionIds)&gt;0">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\pard
\ql \widctlpar\intbl\faauto {\b\f1\fs18\ul D�tails de la composante 
\par\cell}</xsl:text>
				<xsl:text>				
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10  
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908 
\row 
}</xsl:text>
				<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
			<xsl:for-each select="$conditionIds">					
				<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
				<xsl:value-of select="position()"/>
				<xsl:text>.\cell }
{\f1\fs18 </xsl:text>
				<xsl:for-each select="./Line">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:text>\par </xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>\pard\par </xsl:text>
		</xsl:if>
	</xsl:template>
<!-- MCM Impl Team. | XS_16.28 | 25-Aug-2008 | FrenchComponentDetails template added to display Component Details as conditions - Ends -->	

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>{\par }}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f32\froman\fcharset238\fprq2 Times New Roman CE;}
{\f33\froman\fcharset204\fprq2 Times New Roman Cyr;}
{\f35\froman\fcharset161\fprq2 Times New Roman Greek;}
{\f36\froman\fcharset162\fprq2 Times New Roman Tur;}
{\f37\froman\fcharset177\fprq2 Times New Roman (Hebrew);}
{\f38\froman\fcharset178\fprq2 Times New Roman (Arabic);}
{\f39\froman\fcharset186\fprq2 Times New Roman Baltic;}
{\f40\fswiss\fcharset238\fprq2 Arial CE;}{\f41\fswiss\fcharset204\fprq2 Arial Cyr;}
{\f43\fswiss\fcharset161\fprq2 Arial Greek;}{\f44\fswiss\fcharset162\fprq2 Arial Tur;}
{\f45\fswiss\fcharset177\fprq2 Arial (Hebrew);}{\f46\fswiss\fcharset178\fprq2 Arial (Arabic);}
{\f47\fswiss\fcharset186\fprq2 Arial Baltic;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;
\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;
\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;
\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;
\red192\green192\blue192;\red255\green255\blue255;}
{\stylesheet{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}
{\*\cs10 \additive Default Paragraph Font;}
{\s15\ql \widctlpar\faauto\itap0 \b\f1\fs18\lang1033\langfe1033\cgrid\langnp1033
\langfenp1033 \sbasedon0 \snext15 
Body Text;}}{\info{\title </xsl:text>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageFrench">
				<xsl:text>LETTRE D'ENGAGEMENT</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>MORTGAGE COMMITMENT</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> }{\author Zivko Radulovic}{\operator John Schisler}
{\creatim\yr2002\mo8\dy27\hr11\min17}{\revtim\yr2002\mo8\dy27\hr11\min17}{\version2}
{\edmins3}{\nofpages5}{\nofwords0}{\nofchars0}{\*\company filogix Inc}
{\nofcharsws0}{\vern8283}}\margl720\margr720\margt720\margb720 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0
\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale90\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp
\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0
<!--#DG286 -->\headery0\footery400\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>

	<!--#DG244 not used here, keep it just because it may be used by other templates-->
	<!-- ************************************************************************ 	-->
	<!-- LOGO					                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="Logo">
		<xsl:value-of select="$logoRtf"/>
	</xsl:template>
	
</xsl:stylesheet>
