<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema">

<!-- Venkata - Hardcoding Lender Name - CR - Begin -->
<xsl:variable name="lenderName" select="'GE Money'" />	
<!-- Venkata - Hardcoding Lender Name - CR - End -->

	<xsl:output version="1.0" method="html" encoding="UTF-8" indent="no"/>
	<xsl:variable name="fo:layout-master-set">
		<fo:layout-master-set>
			<fo:simple-page-master master-name="default-page" page-height="11in" page-width="8.5in" margin-left="0.6in" margin-right="0.6in">
				<fo:region-body margin-top="0.79in" margin-bottom="0.79in"/>
			</fo:simple-page-master>
		</fo:layout-master-set>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:variable name="maxwidth" select="7.30000"/>
		<fo:root>
			<xsl:copy-of select="$fo:layout-master-set"/>
			<fo:page-sequence master-reference="default-page" initial-page-number="1" format="1">
				<fo:flow flow-name="xsl-region-body">
					<fo:block>
						<fo:table width="{$maxwidth}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="proportional-column-width(100)"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell  text-align="center">
										<fo:block padding-top="1pt" padding-bottom="13pt">
											<fo:inline color="red" font-size="10pt" font-weight="bold">
												<xsl:text>Schedule</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell  text-align="center">
										<fo:block padding-top="1pt">
											<fo:inline color="maroon" font-size="10pt" font-weight="bold">
												<xsl:text>Form MBR 1- Statement of Disclosure</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell  text-align="center">
										<fo:block padding-bottom="10pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>(Mortgage Brokers&apos; and Lenders&apos; Registration Act, R.S.N.S 1989, c. 291)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell  text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="10pt">
											<fo:inline font-size="10pt">
												<xsl:text>This form must be completed in duplicate with every mortgage application or renewal in accordance with the Mortgage Brokers&apos; and Lenders&apos; Registration Act, and one copy given to the mortgage borrower at least 48 hours before he is asked to sign the mortgage documents. All signatures must be originals, not duplicates.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="3pt">
											<fo:table width="100%" border-collapse="separate" color="black" display-align="before">
												<fo:table-column column-width="proportional-column-width(5)"/>
												<fo:table-column column-width="proportional-column-width(95)"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>1.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(5)"/>
																	<fo:table-column column-width="proportional-column-width(87)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>Date</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_DATA_2"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-left="170pt" padding-top="1pt" number-columns-spanned="2">
																				<fo:block font-size="8pt">
																					<xsl:text>(date on which statement of disclosure is made)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>2.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(35)"/>
																	<fo:table-column column-width="proportional-column-width(57)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>Name of mortgage lender (mortgagee)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<!-- Venkata - Hardcoding Lender Name - CR - Begin -->
																					<!-- <xsl:value-of select="//DSCL_LNDR_OFFCE_1"/> -->
																					<xsl:value-of select="$lenderName"/>
																					<!-- Venkata - Hardcoding Lender Name - CR - End -->
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" number-columns-spanned="2">
																				<fo:block>
																					<xsl:value-of select="//DSCL_LENDADDR_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-left="170pt" padding-top="1pt" number-columns-spanned="2">
																				<fo:block font-size="8pt">
																					<xsl:text>(address of mortgage lender)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>3.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(44)"/>
																	<fo:table-column column-width="proportional-column-width(48)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>Name(s) of mortgage borrower(s) (mortgagor(s))</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" number-columns-spanned="2">
																				<fo:block>
																					<xsl:value-of select="//DSCL_BOR_COBOR_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt">
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" number-columns-spanned="2">
																				<fo:block>
																					<xsl:value-of select="//DISC_ADRSBRWER_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-left="170pt" padding-top="1pt" number-columns-spanned="2">
																				<fo:block font-size="8pt">
																					<xsl:text>(address of mortgage borrower(s) (mortgagor(s)))</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>4.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(18)"/>
																	<fo:table-column column-width="proportional-column-width(74)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>Property mortgaged</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_ADRSPRPTY_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-left="170pt" padding-top="1pt" number-columns-spanned="2">
																				<fo:block font-size="8pt">
																					<xsl:text>(address and description of buildings)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt">
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" number-columns-spanned="2">
																				<fo:block>
																					<xsl:value-of select="//DISC_PRTYDESC_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt">
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" number-columns-spanned="2">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt">
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>5.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(19)"/>
																	<fo:table-column column-width="proportional-column-width(6.0)"/>
																	<fo:table-column column-width="proportional-column-width(0.6)"/>
																	<fo:table-column column-width="proportional-column-width(6.0)"/>
																	<fo:table-column column-width="proportional-column-width(0.6)"/>
																	<fo:table-column column-width="proportional-column-width(6.0)"/>
																	<fo:table-column column-width="proportional-column-width(0.6)"/>
																	<fo:table-column column-width="proportional-column-width(6.0)"/>
																	<fo:table-column column-width="proportional-column-width(0.6)"/>
																	<fo:table-column column-width="proportional-column-width(19)"/>
																	<fo:table-column column-width="proportional-column-width(12)"/>
																	<fo:table-column column-width="proportional-column-width(5)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>Principal amount of the</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-width="0.09042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_TYPMTG_1 = '1'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell/>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-width="0.09042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_TYPMTG_2 = '1'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell/>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-width="0.09042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_TYPMTG_3 = '1'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell/>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-width="0.09042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_TYPMTG_OTHER = '1'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell/>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>mortgage borrowed is $</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-width="0.09042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_MTGAMT_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>.</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt" text-align="center">
																				<fo:block>
																					<xsl:text>1st</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell/>
																			<fo:table-cell padding-top="1pt" text-align="center">
																				<fo:block>
																					<xsl:text>2nd</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell/>
																			<fo:table-cell padding-top="1pt" text-align="center">
																				<fo:block>
																					<xsl:text>3rd</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell/>
																			<fo:table-cell padding-top="1pt" number-columns-spanned="3">
																				<fo:block>
																					<xsl:text>Other (check one)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt">
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>6.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(61)"/>
																	<fo:table-column column-width="proportional-column-width(2)"/>
																	<fo:table-column column-width="proportional-column-width(10)"/>
																	<fo:table-column column-width="proportional-column-width(0.6)"/>
																	<fo:table-column column-width="proportional-column-width(10)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell padding-bottom="7pt">
																				<fo:block>
																					<xsl:text>The costs and fees associated with this mortgage are as follows:</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="7pt">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="7pt">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="7pt">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="7pt">
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Bonus on mortgage</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<!--<xsl:value-of select="//DISC_BONUS_1"/>-->
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Brokerage fees or commissions</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_BRKCOMSN_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Insurance fees</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<!--<xsl:value-of select="//DISC_INSURANCEFEE_1"/>-->
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Inspection and appraisal fees</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_INSPCTNFEE_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Mortgage application or renewal fees</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_APPLCTNFEE_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Legal fees and disbursements of not more than</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Interest adjustment (if applicable)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_INTRSTADJ_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Other charges (specify)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_OTHERCHARGES_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="3pt">
																				<fo:block>
																					<xsl:text>Total deductions</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-right="1pt" padding-top="3pt" text-align="right">
																				<fo:block>
																					<xsl:text>$</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="3pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_TOTALDED_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>7.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(75)"/>
																	<fo:table-column column-width="proportional-column-width(17)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="2">
																				<fo:block>
																					<xsl:text>Net amount of money to be paid to the mortgage borrower to be disbursed on his direction</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>or to be credited to the mortgage borrower&apos;s existing mortgage (in case of renewal) $</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell display-align="after" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_LOANAMTPAIDTOMGTEE_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>8.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(42)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-column column-width="proportional-column-width(42)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>The annual percentage rate of mortgage will be</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_APR_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>% or where the annual percentage rate is</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
															<fo:block padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(54)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-column column-width="proportional-column-width(30)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>subject to variations, the initial annual percentage rate will be</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_INITALAPR_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>%.</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>9.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(45)"/>
																	<fo:table-column column-width="proportional-column-width(47)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>The method of calculation of interest is as follows:</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_MTHODCALC_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" number-columns-spanned="2">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>10.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(25)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(46)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(9)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>The term of this mortgage is</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_MTGTERM_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<fo:inline>
																						<xsl:text >months/</xsl:text>
																					</fo:inline>	
																					<fo:inline text-decoration="line-through">
																						<xsl:text >years</xsl:text>
																					</fo:inline>	
																					<fo:inline>
																						<xsl:text >(Strike one); the amortization period is</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_MTGAMRTIZTON_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;years.</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt">
																<xsl:text>11.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(27)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(10)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(29)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>Subject to the agreement, the</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_PYMTFREQNCY_1 = '1'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>(weekly)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_PYMTFREQNCY_2 = '1'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>(bi-weekly)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_PYMTFREQNCY_3 = '1'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>(monthly) (check one) payment</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-bottom="6pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(54)"/>
																	<fo:table-column column-width="proportional-column-width(12)"/>
																	<fo:table-column column-width="proportional-column-width(26)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>(principal and interest) for the term of this mortgage will be $</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_MTGPAYMTAMT_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>.&#160;</xsl:text>
																					<xsl:value-of select="//DISC_PYMTFREQNCY_4"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>12.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(25)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(5)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(7)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(37)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>The first payment is due on</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_PYMTDT_1_DAY"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>(day)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_PYMTDT_1_MONTH"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>(month)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_PYMTDT_1_YEAR"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>(year).</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell  number-columns-spanned="2"> 
															<fo:block break-after="page"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>13.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(53)"/>
																	<fo:table-column column-width="proportional-column-width(6.35)"/>
																	<fo:table-column column-width="proportional-column-width(9.15)"/>
																	<fo:table-column column-width="proportional-column-width(10.7)"/>
																	<fo:table-column column-width="proportional-column-width(12.8)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="5">
																				<fo:block>
																					<xsl:text>Based on the annual percentage rate disclosed in Section 8 and the amount and number of payments</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>noted above, the mortgage will become due and payable in</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_MTGTERM_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell number-columns-spanned="3">
																				<fo:block>
																					<fo:inline>
																					<xsl:text>&#160;months/</xsl:text>
																					</fo:inline>	
																					<fo:inline text-decoration="line-through">
																						<xsl:text >years</xsl:text>
																					</fo:inline>	
																					<fo:inline>
																						<xsl:text >(Strike one), at which</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="4">
																				<fo:block>
																					<xsl:text>time the mortgage borrower, if all payments have been made on the due date, will owe $</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_BLNCEENDTRM_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>.</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>14.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(45.5)"/>
																	<fo:table-column column-width="proportional-column-width(5)"/>
																	<fo:table-column column-width="proportional-column-width(9.5)"/>
																	<fo:table-column column-width="proportional-column-width(5)"/>
																	<fo:table-column column-width="proportional-column-width(27)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>In the case of a construction mortgage, interest will</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_CNSTRCTINMTGINTST_1_Y = 'Y'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center">
																				<fo:block>
																					<xsl:text>or will not</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_CNSTRCTINMTGINTST_1_N = 'Y'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;be deducted on advances by</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="5">
																				<fo:block>
																					<xsl:text>the mortgage lender prior to disbursement to the mortgage borrower.</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>15.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(92)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell text-align="justify">
																				<fo:block>
																					<xsl:text>Where the annual percentage rate or the term of mortgage is subject to variations, it shall vary in the following manner:</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_ANNLPERCRTEVRTNS_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>16.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(92)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell text-align="justify">
																				<fo:block>
																					<xsl:text>The terms and conditions of repayment before maturity of the mortgage loan contract are as follows (i.e. prepayment of early payment penalty):</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DSCL_PYMTVRTNS_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>17.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(92)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell text-align="justify">
																				<fo:block>
																					<xsl:text>Where the mortgage loan is not repaid at maturity or payments are not made when due, the following charges may be imposed (i.e. late payment penalty):</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:value-of select="//DISC_CHANGESIFMTGNOTREPAID_1"/>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>18.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(4)"/>
																	<fo:table-column column-width="proportional-column-width(6)"/>
																	<fo:table-column column-width="proportional-column-width(31)"/>
																	<fo:table-column column-width="proportional-column-width(45)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell text-align="justify" number-columns-spanned="5" padding-bottom="2pt">
																				<fo:block>
																					<xsl:text>Subject to agreement, municipal taxes will be collected from the mortgage borrower (motgagor) by the mortgage lender (mortgagee) in advance.</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell text-align="center" padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_MUNICIPALTXSPYDBYLNDR_Y = 'Y'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt" padding-bottom="1pt">
																				<fo:block>
																					<xsl:text>Yes</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																					<xsl:choose>
																					<xsl:when test="//DISC_MUNICIPALTXSPYDBYLNDR_N = 'Y'">
																					<xsl:call-template name="CheckedCheckbox"/>
																					</xsl:when>
																					<xsl:otherwise>
																					<xsl:call-template name="UnCheckedCheckbox"/>
																					</xsl:otherwise>
																					</xsl:choose>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="1pt" padding-bottom="1pt" number-columns-spanned="2">
																				<fo:block>
																					<xsl:text>No</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-bottom="1pt">
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell padding-top="2pt" text-align="justify" number-columns-spanned="4">
																				<fo:block>
																					<xsl:text>If &quot;Yes&quot; interest will be paid in the following manner:&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block>
																				<xsl:variable name="conditioncheck" select="//DISC_MUNICIPALTXSPYDBYLNDR_Y"/>
																					 <xsl:if test= "$conditioncheck='Y' ">
																					<xsl:value-of select="//DISC_MTHDOFPAYMETIFTXSPAIDBYLNDR_1"/>
																					</xsl:if>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell number-columns-spanned="5" text-align="center" padding-top="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block>
																					<xsl:text>&#160;</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell text-align="center">
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<xsl:text>19.</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="2pt" padding-bottom="6pt">
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(92)"/>
																	<fo:table-column column-width="proportional-column-width(8)"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell text-align="justify">
																				<fo:block>
																					<xsl:text>Where the mortgage borrower (mortgagor) directs the mortgage lender (mortgagee) to assign the mortgage to a third party, the assignment fee will not exceed twenty-five dollars ($25.00).</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block break-after="page"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="center" padding-bottom="13pt">
										<fo:block font-weight="bold">
											<xsl:text>Statutory Notice</xsl:text>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-bottom="13pt">
										<fo:block>
											<xsl:text>Section 24 of the Mortgage Brokers&apos; and Lenders&apos; Registration Act states:</xsl:text>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="justify" padding-bottom="13pt">
										<fo:block>
											<fo:inline font-weight="bold">
											<xsl:text>24&#160;</xsl:text>
											</fo:inline>
											<fo:inline>
											<xsl:text>Notwithstanding any stipulation to the contrary, where a mortgagor is entitled to redeem the mortgage or where the mortgagee demands payment of the mortgage by the mortgagor, the mortgagor, upon payment of any balance outstanding in respect of the morgage, may require the mortgagee, instead of giving a release of mortgage, to assign the mortgage to such third person as he directs, and the mortgagee is bound to assign accordingly.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-bottom="13pt">
										<fo:block>
											<fo:table width="100%" border-collapse="separate" color="black">
												<fo:table-column column-width="proportional-column-width(4)"/>
												<fo:table-column column-width="proportional-column-width(96)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell display-align="before">
															<fo:block>
																<xsl:text>I/We</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<fo:table width="100%" border-collapse="separate" color="black">
																	<fo:table-column column-width="proportional-column-width(28)"/>
																	<fo:table-column column-width="proportional-column-width(3)"/>
																	<fo:table-column column-width="proportional-column-width(51)"/>
																	<fo:table-column column-width="proportional-column-width(9)"/>
																	<fo:table-column column-width="proportional-column-width(9)"/>
																	<fo:table-body>
																		<xsl:for-each select="//borrowerloopNS/Borrower">
																		<fo:table-row>
																			<fo:table-cell padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell padding-top="6pt" text-align="center">
																				<fo:block>
																					<xsl:text>of</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell padding-top="6pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell text-align="center" font-size="8pt" padding-top="1pt">
																				<fo:block>
																					<xsl:text>(name)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell text-align="center" font-size="8pt" padding-top="1pt">
																				<fo:block>
																					<xsl:text>(address)</xsl:text>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																			<fo:table-cell>
																				<fo:block/>
																			</fo:table-cell>
																		</fo:table-row>
																	</xsl:for-each>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-bottom="13pt">
										<fo:block>
											<fo:table width="100%" border-collapse="separate" color="black">
												<fo:table-column column-width="proportional-column-width(4)"/>
												<fo:table-column column-width="proportional-column-width(27)"/>
												<fo:table-column column-width="proportional-column-width(3)"/>
												<fo:table-column column-width="proportional-column-width(50)"/>
												<fo:table-column column-width="proportional-column-width(8)"/>
												<fo:table-column column-width="proportional-column-width(8)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="6" text-align="justify">
															<fo:block>
																<xsl:text>the mortgage borrower (mortgagor) under this proposed mortgage, have read and fully understand the above statement furnished to me by</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="4" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block>
																<!-- Venkata - Hardcoding Lender Name - CR - Begin -->
																<!-- <xsl:value-of select="//DSCL_LNDR_OFFCE_1"/> -->
																<xsl:value-of select="$lenderName"/>
																<!-- Venkata - Hardcoding Lender Name - CR - End -->
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="4" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block>
																<xsl:value-of select="//DSCL_LENDADDR_1"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="4" padding-top="1pt" font-size="8pt" text-align="center">
															<fo:block>
																<xsl:text>(name and address of mortgage lender(mortgagee))</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-bottom="13pt">
										<fo:block>
											<fo:table width="100%" border-collapse="separate" color="black">
												<fo:table-column column-width="proportional-column-width(36)"/>
												<fo:table-column column-width="proportional-column-width(4)"/>
												<fo:table-column column-width="proportional-column-width(6)"/>
												<fo:table-column column-width="proportional-column-width(5)"/>
												<fo:table-column column-width="proportional-column-width(3)"/>
												<fo:table-column column-width="proportional-column-width(2)"/>
												<fo:table-column column-width="proportional-column-width(36)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="7" text-align="justify">
															<fo:block>
																<xsl:text>I/We have not yet signed any mortgage paper or blank documents on this mortgage and now sign this statement in</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>
																<xsl:text>duplicate, which has been fully completed this</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block>
																<xsl:value-of select="//DSCL_CURRENT_DATE"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block>
																<xsl:text>day of</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block>
																<xsl:value-of select="//DSCL_CURRENT_MONTH"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<xsl:text>, 20</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block>
																<xsl:value-of select="//DSCL_CURRENT_YR"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<xsl:text>&#160;and I/We hereby acknowledge receipt of a</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="7" text-align="justify">
															<fo:block>
																<xsl:text>fully completed signed copy.</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block>
											<xsl:text>Sign both copies separately</xsl:text>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="justify" padding-bottom="13pt">
										<fo:block>
											<xsl:text>Do not use carbon for signatures.</xsl:text>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-bottom="13pt">
										<fo:block>
											<fo:table width="100%" border-collapse="separate" color="black">
												<fo:table-column column-width="proportional-column-width(50)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="4pt" number-columns-spanned="3" text-align="justify">
															<fo:block>
																<xsl:text>Witness</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="4pt">
															<fo:block>
																<xsl:text>Address</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="4pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="4pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<xsl:for-each select="//borrowerloopNS/Borrower">
													<fo:table-row>
														<fo:table-cell padding-top="12pt">
															<fo:block>
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="12pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="12pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													</xsl:for-each>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" font-size="8pt">
															<fo:block>
																<xsl:text>(Signature(s) of mortgage borrower(s) (mortgagors(s))</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-bottom="13pt">
										<fo:block>
											<fo:table width="100%" border-collapse="separate" color="black">
												<fo:table-column column-width="proportional-column-width(2)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(73)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>
																<xsl:text>I,</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block>
																<fo:inline font-size="8pt">
																	<xsl:text>(name of mortgage lender(mortgagee))</xsl:text>
																</fo:inline>
																<fo:inline>
																	<xsl:text>&#160;have fully completed the above statement in duplicate</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="3">
															<fo:block>
																<xsl:text>and have furnished one signed copy to the mortgage borrower(s) (mortgagor(s)) on the above date.</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-bottom="13pt">
										<fo:block>
											<fo:table width="100%" border-collapse="separate" color="black">
												<fo:table-column column-width="proportional-column-width(50)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="4pt" number-columns-spanned="3" text-align="justify">
															<fo:block>
																<xsl:text>Witness</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="4pt">
															<fo:block>
																<xsl:text>Address</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="4pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="4pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="12pt">
															<fo:block>
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="12pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="12pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block>
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell number-columns-spanned="2" font-size="8pt">
															<fo:block>
																<xsl:text>(Signature(s) of mortgage lender(mortgagee))</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="justify" font-weight="bold" padding-bottom="13pt">
										<fo:block color="#004bc0">
											<xsl:text>Form MBR1 replaced and Forms MBR2 and MBR3 repealed: O.I.C 85-693, N.S.Reg. 105/85.</xsl:text>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	<!-- ************************************************************* -->
	<!-- Checked Check Box                                                  -->
	<!--                                                                                   -->
	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="8px" height="8px">
			<svg xmlns="http://www.w3.org/2000/svg" width="8px" height="8px">
				<rect x="0" y="0" width="8" height="8" style="fill: none; stroke: black"/>
				<line x1="0" y1="0" x2="8" y2="8" style="stroke:black"/>
				<line x1="0" y1="8" x2="8" y2="0" style="stroke:black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<!-- ************************************************************* -->
	<!-- Unchecked Check Box                                              -->
	<!--                                                                                   -->
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="8px" height="8px">
			<svg xmlns="http://www.w3.org/2000/svg" width="8px" height="8px">
				<rect x="0" y="0" width="8" height="8" style="fill: none; stroke: black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
</xsl:stylesheet>
