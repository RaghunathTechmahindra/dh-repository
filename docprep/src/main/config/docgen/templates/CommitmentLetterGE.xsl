<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
04/Jan/2008 DVG #DG680 FXP19784: GE Money - CR 27c Replace generic GE Money fax number on Commitment Letter
16/Oct/2006 DVG #DG524 #4929  GEM CR#16 Quebec - express Commitment  
15/Mar/2005 SL #1902 P&I Amount fix in Commitment letter   
07/Dec/2005 DVG #DG368 #1902  GE Solicitor Package change - revisit
19/Oct/2005 DVG #DG336 #2301  Commitment letter <LenderName> tag  
11/Aug/2005 DVG #DG292 #1902  GE Solicitor Package change  - create new templates
- created based on MX
05/Aug/2005 DVG #DG286  #1861  Merix - commitment letter Page 2 blank 
12/Jul/2005 DVG #DG256 #1788  Commitment Letter - minor
04/Jul/2005 DVG #DG244 #1735  NBrook Logo 
	- configure the template for variable logos
24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package 
- Many changes to become the Xprs standard	
-->
<xsl:stylesheet version="1.0"  
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>	

<!-- Vandana - CR - 24 July,2007 -->
	<xsl:variable name="MtgProdId" select = "//Deal/mtgProdId" />
	<xsl:variable name="isProductHeloc" select="$MtgProdId = 19 or $MtgProdId = 20 or $MtgProdId = 21"/>
	<xsl:variable name = "isProductTypeLOCSecured" select="//Deal/productTypeId = '2'" />	
	<xsl:variable name="lendName" select="translate(/*/CommitmentLetter/LenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />	
	<xsl:variable name="lenderName">
	  <xsl:choose>
	    <xsl:when test="$lendName != 'TrustCo'">
	      <xsl:value-of select="'GE Money'"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="'GE Money Trust Company'"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:variable>
	<xsl:variable name="ratePremium" select="substring-before(//Deal/premium,'%')"/>
	<!--#DG524 -->
		<!-- commented as part of CR 26 #FXP17823 xsl:variable name="lenderName" select="'GE Money'" / -->	
	<!--#DG530 -->
<!-- Vandana - CR - 24 July,2007 -->

  
	<xsl:variable name="guarantor" select="/*/CommitmentLetter/GuarantorClause"/>
  
	<!--#DG244 -->
	<!-- Moved the variable declaration up -->
	<!-- xsl:variable name="lendName" select="translate(/*/CommitmentLetter/LenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" / -->	
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendName,'.xsl'))" />
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />
	
	<xsl:template match="/">
		<!--#DG244 -->
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
		
		<xsl:call-template name="RTFFileStart"/>
		<!--#DG524 rewritten -->
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageEnglish">				
				<xsl:call-template name="CommitmentLetterEn"/><!--#DG286 will be called from solicit.pkg too -->
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageFrench">
				<xsl:call-template name="CommitmentLetterFr"/><!--#DG286 will be called from solicit.pkg too -->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<xsl:template name="CommitmentLetterEn">
		<xsl:call-template name="FooterEn"/>
		<xsl:call-template name="PageAEn"/>
		<xsl:call-template name="PageDEn"/>
		<xsl:choose>
			<xsl:when test="$isProductHeloc and $isProductTypeLOCSecured">
			</xsl:when>
			<xsl:otherwise><xsl:text> \page </xsl:text><xsl:call-template name="PageEEn"/></xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<xsl:template name="PageAEn">
		<xsl:text>
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \qc<!--#DG336 --> \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs28 MORTGAGE COMMITMENT \par }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text> <!--#DG244 -->
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell {\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalc
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell 
Tel: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<!-- #DG680 xsl:value-of select="/*/CommitmentLetter/BranchAddress/Fax"/-->
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="/*/Deal/UserProfile
        [userProfileId =/*/Deal/underwriterUserId]/Contact/contactFaxNumber"/>
		</xsl:call-template>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell }
{\b\f1\fs18 Underwriter \par\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text> \cell }
{\b\f1\fs18 Date \par\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\row 
}
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>

</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
{\b\f1\fs18 Mortgage Reference Number:\cell \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b \cell Deal Number:\cell\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
{\i\f1 We are pleased to confirm that your application for a Mortgage Loan has been approved under the following terms and conditions:\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGOR(S):\cell\b0 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
{\b\f1\fs18 PURPOSE:\b0  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>(S):}{\f1\fs18 \cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18\ul SECURITY ADDRESS: \cell LEGAL DESCRIPTION: \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Properties/Property">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tx4900\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 
</xsl:text>
			<xsl:value-of select="./AddressLine1"/>
			<xsl:text> \par 
</xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>\par 
</xsl:text>
			<xsl:value-of select="./PostalCode"/>
			<xsl:text> \par 
\cell </xsl:text>
			<xsl:value-of select="./LegalLine1"/>
			<xsl:if test="./LegalLine1">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine2"/>
			<xsl:if test="./LegalLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine3"/>
			<xsl:if test="./LegalLine3">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 COMMITMENT DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/>
		<xsl:text>
\par 
\par }

{\b\f1\fs18 INTEREST ADJUSTMENT DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/IADDate"/>
		<xsl:text>\cell }
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 COMMITMENT EXPIRY DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentExpiryDate"/>
		<xsl:text>
\par 
\par }
{\b\f1\fs18 FIRST PAYMENT DATE:}
{\f1\fs18 \par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
{\b\f1\fs18 ADVANCE DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text>
\par 
\par }
{\b\f1\fs18 MATURITY DATE:}
{\f1\fs18 \par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 LOAN TYPE:  \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Product"/>
		<xsl:text>\cell }
{\b\f1\fs18 LTV:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 INTEREST RATE:}{\f1\fs18 \cell </xsl:text>
<xsl:choose>
    <xsl:when test="$isProductHeloc and $isProductTypeLOCSecured">
	<xsl:text>Prime</xsl:text>
	<xsl:choose>
	    <xsl:when test="$ratePremium &gt; 0">
		<xsl:text> + Spread</xsl:text>
	</xsl:when>
	</xsl:choose>
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="//CommitmentLetter/InterestRate"/>
 </xsl:otherwise>
</xsl:choose>
		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 MTG.INSUR.PREMIUM:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs18 TERM:}{\f1\fs18 \cell </xsl:text>
		<xsl:choose>
		    <xsl:when test="$isProductHeloc and $isProductTypeLOCSecured">
			<xsl:text>N/A</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="//CommitmentLetter/PaymentTerm"/>
		 </xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 TOTAL LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 AMORTIZATION PERIOD:}{\f1\fs18 \cell </xsl:text>
		<xsl:choose>
		    <xsl:when test="$isProductHeloc and $isProductTypeLOCSecured">
			<xsl:text>N/A</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="//CommitmentLetter/Amortization"/>
		 </xsl:otherwise>
		</xsl:choose>		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 <!--#DG368 BASIC PAYMENT AMOUNT-->PRINCIPAL AND INTEREST PAYMENT:}{\f1\fs18 \cell 
</xsl:text>
<xsl:choose>
    <xsl:when test="$isProductHeloc and $isProductTypeLOCSecured">
	<xsl:text>Interest Only</xsl:text>
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="/*/Deal/PandiPaymentAmount"/>
 </xsl:otherwise>
</xsl:choose>
		<xsl:text>\cell }
{\b\f1\fs18 PAYMENT FREQUENCY:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentFrequency"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 <!--#DG368 moved down EST.ANNUAL PROP.TAXES-->
TAX ESCROW PAYMENT:\b0 \cell </xsl:text>
		<!--#DG368 xsl:value-of select="//CommitmentLetter/AnnualTaxes"/-->
		<xsl:choose>
		    <xsl:when test="$isProductHeloc and $isProductTypeLOCSecured">
			<xsl:text>N/A</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="/*/Deal/escrowPaymentAmount"/>
		 </xsl:otherwise>
		</xsl:choose>
		
		<xsl:text>\cell }
{\b\f1\fs18 TAXES TO BE PAID BY:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TaxPayor"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}

<!--#DG368 added new entries -->
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 TOTAL PAYMENT:\b0 \cell </xsl:text>
<xsl:choose>
    <xsl:when test="$isProductHeloc and $isProductTypeLOCSecured">
	<xsl:text>Interest Only</xsl:text>
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="/*/Deal/totalPaymentAmount"/>
 </xsl:otherwise>
</xsl:choose>
<xsl:text>\cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}

<!--#DG368 moved from above -->
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 EST.ANNUAL PROP.TAXES:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AnnualTaxes"/>
		<xsl:text>\cell \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}


\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 PST PAYABLE ON INSUR.PREMIUM:\cell }{\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGE FEES:\cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell }{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 In this </xsl:text>
<xsl:choose>
    <xsl:when test="$isProductHeloc">
	<xsl:text>Mortgage Commitment </xsl:text>
</xsl:when>
<xsl:otherwise>
		<xsl:text>approval </xsl:text>
    </xsl:otherwise>
</xsl:choose>

<xsl:text>and any schedule(s) to this </xsl:text>
<xsl:choose>
	<xsl:when test="$isProductHeloc">
		<xsl:text>commitment</xsl:text>
	</xsl:when>
	<xsl:otherwise>
		<xsl:text> approval</xsl:text>
	</xsl:otherwise>
</xsl:choose>
<xsl:text>, }{\b\f1\fs18 you and your}{\f1\fs18  mean the Borrower, Co-Borrower and </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>, if any, and }</xsl:text>
		
<xsl:choose>
	<xsl:when test="$isProductHeloc"><xsl:text>{\b\f1\fs18 
we, our, us }{\f1\fs18 and }{\b\f1\fs18 GE Money}{\f1\fs18  mean the entity identified in condition 1 on the following page</xsl:text>
	</xsl:when>
	<xsl:otherwise><xsl:text>{\b\f1\fs18 
we, our and us }{\f1\fs18  mean </xsl:text>
		<xsl:value-of select="$lenderName"/>
	</xsl:otherwise>
</xsl:choose>

<xsl:text>.
\par 
\par All of our normal requirements and, if applicable, those of the mortgage insurer must be 
met. All costs, including legal, survey, mortgage insurance etc. are for the account of the applicant(s). The mortgage insurance premium (if applicable) will be added to the mortgage. Any fees specified herein may be deducted from the Mortgage advance. If
 for any reason the loan is not advanced, you agree to pay all application, legal, appraisal and survey costs incurred in this transaction.
\par 
\par This Mortgage Commitment is subject to the details and terms outlined herein as well as the conditions described on the attached schedules.
\par 
\par To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> after which time if not accepted, shall be considered null and void.
\par 
\par Thank you for choosing </xsl:text>
		<!--xsl:value-of select="//CommitmentLetter/LenderName"/-->
		<xsl:value-of select="$lenderName"/>  <!--#DG524 -->
		<xsl:text> for your financing.
\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 
\par \page 
\par } </xsl:text>
	</xsl:template>

	<xsl:template name="PageDEn">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\b\f1\fs18\ul THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING
\par 
\par }{\f1\fs18 The following conditions must be met, and the requested documents must be received in form and content satisfactory to </xsl:text>
		<!--#DG336 xsl:value-of select="//SolicitorsPackage/LenderName"/-->		
		<!--xsl:value-of select="//CommitmentLetter/LenderName"/-->
		<xsl:value-of select="$lenderName"/>  <!--#DG524 -->
		<!--#DG238 -->
		<xsl:text> no later than ten (10) days prior to the advance of the mortgage. Failure to do so may delay or void this commitment.
\par \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}</xsl:text>
		<xsl:if test="//CommitmentLetter/Conditions">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
		</xsl:if>
		<xsl:for-each select="//CommitmentLetter/Conditions/Condition">
			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }

{\f1\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \par \par }
</xsl:text>
<xsl:choose>
	<xsl:when test="not($isProductHeloc)">
<xsl:text>{\b\f1\fs18\ul Requests for any changes to the terms and conditions of the Commitment will only be considered if your request is received by us in writing at least 5 business days prior to the Advance Date.
\par 
\par ACCEPTANCE:
\par 
\par }
{\f1\fs18 This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> after which time, if not accepted, shall be considered null and void. Furthermore, the mortgage must be advanced by no later than </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text> at which time it expires.
\par 
\par }</xsl:text>
	</xsl:when>
	<xsl:otherwise>
		<xsl:text>{\b\f1\fs18\ul Requests for any changes to the terms and conditions of the Commitment will only be considered if your request is received by us in writing at least 5 business days prior to the Advance Date. \par }
		</xsl:text>
	</xsl:otherwise>
</xsl:choose><xsl:text>
\pard \ql \widctlpar\intbl\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright {\f1\fs18 \tab \tab </xsl:text>
		<!--xsl:value-of select="//CommitmentLetter/LenderName"/-->
		<xsl:value-of select="$lenderName"/>  <!--#DG524 -->
		<xsl:text> 
\par 
\par \tab Authorized by:___________________________________________
\par \tab \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>
\par 
\par 
\par }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}

\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par }</xsl:text>
	</xsl:template>

	<xsl:template name="PageEEn">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s15\ql \widctlpar\intbl\faauto 
{\b\f1\fs18 I / We, hereby authorize </xsl:text>
		<!--xsl:value-of select="//CommitmentLetter/LenderName"/-->
		<xsl:value-of select="$lenderName"/>  <!--#DG524 -->
		<xsl:text> to debit the account indicated below with payments on the Mortgage Loan (including taxes and life insurance premiums, if applicable)
\par }
\pard\plain \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 [ ] Chequing/Savings Account \cell  [ ] Chequing Account  \cell  [ ] Current Account No._________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Bank _______________________________________________ Branch_________________________________________________
\par 
\par Address____________________________________________________________________________________________________
\par 
\par Transit No.________________________________ Telephone No._____________________________________________________
\par 
\par }{\b\f1\fs18 A sample "VOID" cheque is enclosed.}{\f1\fs18 
\par 
\par }{\b\f1\fs18 I / We </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/><!--#DG256 -->
		<xsl:text> warrant that all representations made by me/us and all the information submitted to you in connection with my/our mortgage application are true and accurate. I / We fully understand that any misrepresentations of fact contained in my/our mortgage application or other documentation entitles you to decline to advance a portion or all of the loan proceeds, or to demand immediate repayment of all monies secured by the Mortgage.
\par 
\par 
\par I / We, the undersigned applicants accept the terms of this Mortgage Commitment as stated above and agree to fulfill the conditions of approval outlined herein.}
{\f1\fs18 \par \par }
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7800\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell ___________________________
\par APPLICANT
\par 
\par \cell ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par CO-APPLICANT
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text>
	</xsl:template>

	<xsl:template name="FooterEn">
		<xsl:text>
{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }{\f1\fs16 of }
{\field{\*\fldinst {NUMPAGES }}}}}

</xsl:text>
	</xsl:template>


	<!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<!--#DG524 brand new copy of English templates translated to French -->
	<xsl:template name="CommitmentLetterFr">
		<xsl:call-template name="FooterFr"/>
		<xsl:call-template name="PageAFr"/>
		<xsl:call-template name="PageDFr"/>
		<xsl:call-template name="PageEFr"/>
	</xsl:template>
	
	<xsl:template name="PageAFr">
		<xsl:text>\uc1\pard\qc\b\f1\fs28 ENGAGEMENT HYPOTH�CAIRE\b0\par
\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmgf\clvertalc\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx4826
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl 
</xsl:text> 
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell 
\f1\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell \row 

\trowd\trgaph108\trleft-108
\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmrg\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx4826
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\cell 
T�l. :</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
T�l�c. :</xsl:text>
		<!-- #DG680 xsl:value-of select="/*/CommitmentLetter/BranchAddress/Fax"/-->
		<xsl:call-template name="FormatPhone">
			<xsl:with-param name="pnum" select="/*/Deal/UserProfile
        [userProfileId =/*/Deal/underwriterUserId]/Contact/contactFaxNumber"/>
		</xsl:call-template>		
		<xsl:text>\cell\row 
		
\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmrg\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx4826
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx8114
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\cell\b Souscripteur\par\b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Underwriter"/>
		<xsl:text> \cell 
\b Date\par\b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CurrentDate"/>
		<xsl:text>\cell\row 

\trowd\trgaph108\trleft-108\trrh518\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmgf\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10
\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx4826
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrb\brdrw10\brdrs \cellx7740
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs
\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>

</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell 
\b Num�ro de r�f�rence hypoth�caire :\cell \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell\row 

\trowd\trgaph108\trleft-108\trrh517\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clvmrg\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx4826
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx7740
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\b \cell 
Num�ro de dossier :\cell\b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/DealNum"/>
		<xsl:text>\cell \row 

\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\par
\i\fs24 Nous avons le plaisir de confirmer que votre demande de pr�t hypoth�caire a �t� approuv�e 
conform�ment aux conditions et modalit�s suivantes :\par \cell\row 

\trowd\trgaph108\trleft-108\trrh210\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs \cellx1670
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx4826
\clvmgf\clbrdrl\brdrw10\brdrs
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\i0\b\fs18 D�BITEUR(S) HYPOTH�CAIRE(S) :\cell\b0 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell 
\b OBJET :\b0  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell \row 

\trowd\trgaph108\trleft-108\trrh210\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx1670
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx4826
\clvmrg\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\b </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>(S) :\b0\cell </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \cell \row 

\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs \cellx4826
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\ul\b ADRESSE DE LA PROPRI�T� PRISE EN GARANTIE :\cell 
DESCRIPTION L�GALE :\ulnone\b0\cell\row 

</xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/Properties/Property">
			<xsl:text>\pard\intbl\tx180\tx4900 </xsl:text>
			<xsl:value-of select="./AddressLine1"/>
			<xsl:text> \par 
</xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>\par 
</xsl:text>
			<xsl:value-of select="./PostalCode"/>
			<xsl:text> \par 
\cell </xsl:text>
			<xsl:value-of select="./LegalLine1"/>
			<xsl:if test="./LegalLine1">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine2"/>
			<xsl:if test="./LegalLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine3"/>
			<xsl:if test="./LegalLine3">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell\row 

</xsl:text>
		<xsl:text>\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx3563
\clbrdrt\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx7235
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\b DATE DE L'ENGAGEMENT :\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CommitmentIssueDate"/>
		<xsl:text>\par\par 

\b DATE DES REDRESSEMENTS DES INT�R�TS :\b0\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/IADDate"/>
		<xsl:text>\cell 
\pard\intbl\qc\b DATE D'�XPIRATION DE L'ENGAGEMENT :\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CommitmentExpiryDate"/>
		<xsl:text>\par\par

\b DATE DU PREMIER VERSEMENT :\b0\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell 
\b DATE DE L'AVANCE :\b0\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text>\par\par 
\b DATE D'�CH�ANCE :\b0\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell\row 

\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs \cellx4826
\clbrdrt\brdrw10\brdrs \cellx7475
\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\b TYPE DE PR�T : \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Product"/>
		<xsl:text>\cell 
\b RPV : \b0\cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LTV"/>
		<xsl:text>\cell \row 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs \cellx2851
\cellx4826
\cellx7475
\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\b MONTANT DU PR�T DE BASE :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell 
\b TAUX D'INT�R�T :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/InterestRate"/>
		<xsl:text>\cell\row 

\pard\intbl\b PRIME D'ASS. HYP :\b0\cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Premium"/>
		<xsl:text>\cell 
\b TERME :\b0\cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PaymentTerm"/>
		<xsl:text>\cell \row 

\pard\intbl\b MONTANT TOTAL DU PR�T :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell 
\b P�RIODE D'AMORTISSEMENT :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Amortization"/>
		<xsl:text>\cell \row 

\pard\intbl\b VERSEMENT DE CAPITAL ET INT�R�T :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/Deal/PandiPaymentAmount"/>
		<xsl:text>\cell 
\b FR�QUENCE DES VERSEMENTS :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PaymentFrequency"/>
			<xsl:text>\cell\row 

\pard\intbl\b VERSEMENT DES TAXES FONCI�RES EN FID�ICOMMIS :\b0 \cell 
</xsl:text>
		<xsl:value-of select="/*/Deal/escrowPaymentAmount"/>
		<xsl:text>\cell 
\b TAXES FONCI�RES � PAYER PAR :\b0\cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TaxPayor"/>
		<xsl:text>\cell\row 

\pard\intbl\b PAIEMENT TOTAL :\b0 \cell 
</xsl:text>
		<xsl:value-of select="/*/Deal/totalPaymentAmount"/>
		<xsl:text>\cell \cell \cell\row 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs \cellx2851
\cellx4826
\cellx7475
\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\b TAXES FONCI�RES ANNUELLES ESTIMATIVES :\b0\cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AnnualTaxes"/>
		<xsl:text>\cell \cell \cell \row 

\trowd\trgaph108\trleft-108\trrh101\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs \cellx4826
\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\tx180\tqr\tx7500\b TPS � PAYER SUR LA PRIME D'ASS :\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PST"/>
		<xsl:text>\cell \row 

\b FRAIS HYPOTH�CAIRES :\b0\cell\cell\row 

</xsl:text>

		<xsl:for-each select="/*/CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd\trgaph108\trleft-108\trrh100\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx180
\clbrdrb\brdrw10\brdrs \cellx4826
\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\tx180\tqr\tx7500\b\cell 
</xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\b0\cell 
</xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell\row 
</xsl:text>
		</xsl:for-each>
		
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\intbl\qj Dans la pr�sente approbation et toute annexe � cette approbation, 
\b vous et votre\b0  font r�f�rence � l'emprunteur, au co-emprunteur et au </xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>, le cas �ch�ant, et \b nous\b0  signifie </xsl:text>
		<xsl:value-of select="$lenderName"/>		
		<xsl:text>.\par\par 

Toutes nos exigences normales et, le cas �ch�ant, celles de l'assureur hypoth�caire doivent �tre 
respect�es. Tous les co�ts, notamment les frais juridiques, d'arpentage et d'assurance 
hypoth�caire incombent aux demandeurs. La prime d'assurance hypoth�caire (le cas �ch�ant) sera 
ajout�e au pr�t hypoth�caire. Tous les frais pr�cis�s aux pr�sentes peuvent �tre d�duits de 
l'avance hypoth�caire. Si pour une raison quelconque, le pr�t n'est pas d�caiss�, vous convenez 
de payer tous les frais relatifs � la demande, les frais juridiques, d'�valuation et d'arpentage 
engag�s dans la pr�sente op�ration.\par\par 

Cet engagement hypoth�caire est assujetti aux pr�cisions et aux conditions d�crites aux pr�sentes 
ainsi qu'aux conditions d�crites dans les annexes ci-jointes.\par\par 

Pour accepter ces conditions, cet engagement hypoth�caire doit �tre sign� par toutes les parties 
et nous �tre retourn� au plus tard le </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> apr�s quoi, si l'engagement n'est pas accept�, il sera consid�r� comme nul et non 
avenu.\par\par 

Nous vous remercions d'avoir choisi </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> pour votre financement.\par 
\pard\intbl\cell\row 

\pard\par
\page\par 

</xsl:text>
	</xsl:template>

	<xsl:template name="PageDFr">
		<xsl:text>\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\ul\b\f1\fs18 CET ENGAGEMENT EST CONDITIONNEL � LA R�CEPTION DE CE QUI SUIT\b0\ulnone\par\par 

Les conditions suivantes doivent �tre respect�es, et les documents demand�s doivent �tre re�us 
sous la forme et le contenu satisfaisants pour </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> au plus tard dix (10) jours avant l'avance du pr�t hypoth�caire. Le d�faut d'agir 
ainsi peut retarder ou annuler le pr�sent engagement.\par\par\cell\row
 
</xsl:text>

		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition">
			<xsl:text>\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs \cellx360
\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell 
</xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell \row 

</xsl:text>
		</xsl:for-each>
		
		<xsl:text>\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10908
\pard\intbl\par\par

\ul\b Nous tiendrons compte des demandes relatives � toute modification aux conditions et aux 
modalit�s de l'engagement � condition de recevoir vos demandes par �crit dans les cinq jours 
ouvrables pr�c�dant la date de l'avance.\par\par 

ACCEPTATION :\ulnone\b0 \par\par 

Cet engagement doit �tre ouvert � votre acceptation jusqu'� 23 h 59 le </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> . Apr�s ce d�lai, s'il n'est pas accept�, il sera consid�r� comme nul et non avenu. 
Par ailleurs, le pr�t hypoth�caire doit �tre avanc� au plus tard � la </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text> ,soit la date d'expiration.\par\par 

\pard\intbl\tx4000\tx4850\tx5500\tab\tab </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text>\par\par 

\tab Autoris� par :___________________________________________\par 
\tab \tab \tab </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Underwriter"/>
		<xsl:text>\par \par \par 

\pard\intbl\cell\row

\pard\par\page


</xsl:text>
	</xsl:template>

	<xsl:template name="PageEFr">
		<xsl:text>\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\b J'autorise / Nous autorisons, par la pr�sente, </xsl:text>
		<xsl:value-of select="$lenderName"/>  
		<xsl:text> � d�biter le compte indiqu� ci-dessous des versements sur le pr�t hypoth�caire 
(y compris les taxes et les primes d'assurance vie, le cas �ch�ant)\par
\b0\cell\row 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs \cellx3240
\cellx6660
\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\b [ ] Compte de ch�que/d'�pargne\cell 
 [ ] Compte de ch�que\cell 
 [ ] No de compte courant_________________ \cell \row 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\tx100\tx4000\tx7000\b0\par
\pard\intbl Banque _____________________________________________ Succursale_____________________________________________\par\par 

Adresse____________________________________________________________________________________________________\par\par 

No de domiciliation________________________________ No de t�l�phone ______________________________________________\par\par 

\b Un sp�cimen de ch�que � ANNUL� � est joint.\b0\par\par 

\b Je / Nous </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BorrowerNamesLine"/>
		<xsl:text> garantis/garantissons que toutes les d�clarations faites par moi / nous et tous les 
renseignements qui vous ont �t� pr�sent�s relativement � mon / notre demande de pr�t hypoth�caire 
sont v�ridiques et exacts. Je comprends / Nous comprenons enti�rement que toute fausse 
d�claration des faits contenus dans ma / notre demande ou autre documentation vous donne le droit 
de refuser d'avancer une partie ou la totalit� des produits du pr�t, ou de demander le 
remboursement imm�diat de toutes les sommes garanties par le pr�t hypoth�caire.\par\par\par 

Je / Nous, le(s) demandeur(s) soussign�(s), accepte / acceptons les conditions du pr�sent 
engagement hypoth�caire tel qu'il est �nonc� ci-dessus et conviens / convenons de respecter les 
conditions de l'approbation stipul�es aux pr�sentes.\b0\par\par

\pard\intbl\tx100\tx4000\tx7800\par
\cell\row

\trowd\trgaph108\trleft-108\trrh205\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs \cellx3240
\cellx6660
\clbrdrr\brdrw10\brdrs \cellx10908
\pard\intbl\tx100\tx4000\tx7000\b ___________________________\par 
T�MOIN\par\par \cell 
___________________________\par 
DEMANDEUR\par\par \cell 
___________________________\par 
DATE\par\par \cell \row 

\pard\intbl\tx100\tx4000\tx7000\b ___________________________\par 
T�MOIN\par\par \cell 
___________________________\par 
CO-DEMANDEUR\par\par \cell 
___________________________\par 
DATE\par\par \cell \row 

\pard\intbl\tx100\tx4000\tx7000\b ___________________________\par 
T�MOIN\par\par \cell 
___________________________\par 
</xsl:text>
		<xsl:value-of select="$guarantor"/><!--#DG530 -->
		<xsl:text>\par\par \cell 
___________________________\par 
DATE\par\par \cell \row 

\pard 
</xsl:text>
	</xsl:template>

	<xsl:template name="FooterFr">
		<xsl:text>{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }
{\f1\fs16 de }{\field{\*\fldinst {NUMPAGES }}}}}

</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>{\par }}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033{\fonttbl{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f32\froman\fcharset238\fprq2 Times New Roman CE;}{\f33\froman\fcharset204\fprq2 Times New Roman Cyr;}{\f35\froman\fcharset161\fprq2 Times New Roman Greek;}{\f36\froman\fcharset162\fprq2 Times New Roman Tur;}
{\f37\froman\fcharset177\fprq2 Times New Roman (Hebrew);}{\f38\froman\fcharset178\fprq2 Times New Roman (Arabic);}{\f39\froman\fcharset186\fprq2 Times New Roman Baltic;}{\f40\fswiss\fcharset238\fprq2 Arial CE;}{\f41\fswiss\fcharset204\fprq2 Arial Cyr;}
{\f43\fswiss\fcharset161\fprq2 Arial Greek;}{\f44\fswiss\fcharset162\fprq2 Arial Tur;}{\f45\fswiss\fcharset177\fprq2 Arial (Hebrew);}{\f46\fswiss\fcharset178\fprq2 Arial (Arabic);}{\f47\fswiss\fcharset186\fprq2 Arial Baltic;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;
\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;\red255\green255\blue255;}{\stylesheet{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}{\s15\ql \widctlpar\faauto\itap0 \b\f1\fs18\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext15 
Body Text;}}{\info{\title </xsl:text>
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageFrench">
				<xsl:text/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>MORTGAGE COMMITMENT</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> }{\author Zivko Radulovic}{\operator John Schisler}{\creatim\yr2002\mo8\dy27\hr11\min17}{\revtim\yr2002\mo8\dy27\hr11\min17}{\version2}{\edmins3}{\nofpages5}{\nofwords0}{\nofchars0}{\*\company filogix Inc}
{\nofcharsws0}{\vern8283}}\margl720\margr720\margt720\margb720 \widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale90\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0<!--#DG286 -->\headery0\footery400\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>

	<!--#DG244 not used here, keep it just because it may be used by other templates-->
	<!-- ************************************************************************ 	-->
	<!-- LOGO					                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="Logo">
		<xsl:value-of select="$logoRtf"/>
	</xsl:template>

	<!-- #DG680 ================================================  -->
	<xsl:template name="FormatPhone">
		<xsl:param name="pnum"/>
		<xsl:if test="string-length($pnum)&gt;=7">
			<xsl:text>(</xsl:text>
			<xsl:value-of select="substring($pnum, 1, 3)"/>
			<xsl:text>) </xsl:text>
			<xsl:value-of select="substring($pnum, 4, 3)"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="substring($pnum, 7)"/>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>
