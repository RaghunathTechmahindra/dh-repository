<?xml version="1.0" encoding="UTF-8"?>
<!--
06/Feb/2006 DVG #DG390 #2697  DJ-Incorrect MI PST in  Offre de financement hypotheque  
09/Mar/2005 DVG #DG166 #1063  DJ - miscellaneous DocPrep fixes  
22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik

 Author Zivko Radulovic -->
<!-- edited by Catherine Rutgaizer, March 2004 -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- ================  Attribute sets ============== -->
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttrPg4">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainTextAttr_Page_2">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ListLabelIndent">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="padding-left">-3mm</xsl:attribute>
	</xsl:attribute-set>
	<!-- ================  Attribute sets ============== -->
	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<!-- ========================== top level ================================  -->
	<xsl:template name="CreatePageFour">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					Page <fo:page-number/> of <fo:page-number-citation ref-id="page_4"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage4"/>
				<xsl:call-template name="Page_4_Item_1"/>
				<xsl:call-template name="DealConditionsNotWaived"/>
				<fo:block id="page_4" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="Page_4_Item_1">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> Name of borrower(s):</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
								<xsl:element name="fo:block">
									<xsl:attribute name="space-after">2pt</xsl:attribute>
									<xsl:attribute name="space-before">2pt</xsl:attribute>
									<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
									<xsl:attribute name="font-size">8pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="./salutation"/>&#160;
									<xsl:value-of select="./borrowerFirstName"/>&#160;
									<xsl:value-of select="./borrowerLastName"/>
								</xsl:element>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">8pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<fo:inline font-weight="bold">CONDITIONS :</fo:inline>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- START OF TABLE   MI Policy numbers-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">6cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:call-template name="mortgageInsuranceCheckBox">
								<xsl:with-param name="parInsurer">1</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
							<!-- <xsl:text> - </xsl:text> -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Insured loan CMHC no. 
								<xsl:call-template name="mortgageInsurancePolicyNum">
								<xsl:with-param name="parInsurer">1</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="mortgageInsuranceCheckBox">
								<xsl:with-param name="parInsurer">2</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
							<!-- <xsl:text> - </xsl:text> --> 
							Insured loan G.E. Capital no.
								<xsl:call-template name="mortgageInsurancePolicyNum">
								<xsl:with-param name="parInsurer">2</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Commission $-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">6.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Commission 
								<xsl:call-template name="mortgageInsurancePremiumAmount">
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/MIUpfront= 'Y'">
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							 included with loan 
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/MIUpfront= 'N'">
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 non included with loan  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Tax $-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Tax 
							<xsl:if test="(//Deal/mortgageInsurerId='1' or //Deal/mortgageInsurerId='2') and (//Deal/MIStatusId='15' or //Deal/MIStatusId='16' or //Deal/MIStatusId='23')">
								<!--#DG390 xsl:value-of select="//Deal/MIPremiumPST"/-->
								<!--   	public static int FEE_TYPE_CMHC_PST_ON_PREMIUM = 13;  
										public static int FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM = 19;
									-->
								<xsl:for-each select="/*/specialRequirementTags/DealFee[feeId = 13 or feeId = 19]">
									<xsl:value-of select="feeAmount"/>
								</xsl:for-each>
									
							</xsl:if>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							 Payable
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 check included  
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 to account 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by the contractor  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Subscription right -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Subscription right $ 
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 check included 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 to account 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by the contractor 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by Caisse  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE    Evaluation fees  -->
		<!-- ===================================================  -->
		
		<xsl:for-each select="//specialRequirementTags/DealFee[feeId = 0 or feeId = 22]"><!--#DG166  -->		

 		<fo:table  xsl:use-attribute-sets="TableLeftFixed">
		<!--#DG140 xsl:element name="fo:table">
			<would not work like this !! xsl:copy use-attribute-sets="TableLeftFixed"/-->
			
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">					
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
						<!--#DG140 xsl:element name="fo:block">
							<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
							<xsl:call-template name="CheckBoxes"/><!--#DG140 -->
						</fo:block><!--#DG140 -->					
					</xsl:element>
					<xsl:element name="fo:table-cell">
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
						<!--#DG140 xsl:element name="fo:block">
							<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
							Evaluation fees $  
							<xsl:value-of select="feeAmount"/> <!--#DG166  -->
						</fo:block><!--#DG140 -->
					</xsl:element>
					<xsl:element name="fo:table-cell">
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
						<!--#DG140 xsl:element name="fo:block">
							<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
							<xsl:call-template name="CheckBoxes"/><!--#DG140 -->
							 check included
						</fo:block><!--#DG140 -->					
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
						<!--#DG140 xsl:element name="fo:block">
							<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
							<xsl:call-template name="CheckBoxes"/><!--#DG140 -->
							 to account 
						</fo:block><!--#DG140 -->					
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
						<!--#DG140 xsl:element name="fo:block">
							<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
							<xsl:call-template name="CheckBoxes"/><!--#DG140 -->
							 by Caisse  
						</fo:block><!--#DG140 -->					
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</fo:table><!--#DG140 -->
		</xsl:for-each><!--#DG166  -->		
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   The down payment must be deposited at the Caise -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							The down payment must be deposited at the Caisse before sending instructions to the notary. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Multiproject -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/multiProject">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Multiprojets option  
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Certificat of Location -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Certificat of Location of no more than 10 years, indicating the actual state of the property. The certificat must be complet and precis in all aspect. The property must not have been altered, modified or extended since the date of issuance. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   New certificate  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId='1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/newConstructionId='2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							New certificate of location demanded (new construction). 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Disbursement date expected -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Disbursement date expected <xsl:value-of select="//Deal/estimatedClosingDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Clause for 1/12th of taxes  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:if test="//Deal/paymentFrequencyId='0'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:if>
									<xsl:if test="not(//Deal/paymentFrequencyId= '0')">
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:if test="//Deal/paymentFrequencyId='0'">
										 Clause for 1/12th of taxes  $<xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
									</xsl:if>
									<xsl:if test="not(//Deal/paymentFrequencyId= '0')">
										 Clause for 1/12th of taxes 
									</xsl:if>
								</xsl:when>
								<xsl:otherwise> Clause for 1/12th of taxes  </xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:choose>
										<xsl:when test="//Deal/paymentFrequencyId='4'">
											<xsl:call-template name="CheckedCheckbox"/> 1/52th $ <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:when test="//Deal/paymentFrequencyId='5'">
											<xsl:call-template name="CheckedCheckbox"/> 1/52th $<xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="UnCheckedCheckbox"/> 1/52th $	
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/> 1/52th $	
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTaxEscrow">
									<xsl:choose>
										<xsl:when test="//Deal/paymentFrequencyId='2'">
											<xsl:call-template name="CheckedCheckbox"/> 1/26th  <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:when test="//Deal/paymentFrequencyId='3'">
											<xsl:call-template name="CheckedCheckbox"/> 1/26th <xsl:value-of select="//DealAdditional/propertyTaxEscrow"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="UnCheckedCheckbox"/> 1/26th $	
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/> 1/26th $	
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Standard clauses for condominium.  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/propertyTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/propertyTypeId = '14'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/propertyTypeId = '15'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/propertyTypeId = '16'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Standard clauses for condominium.  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  constructions  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/progressAdvance= 'Y'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Construction: progressive disbursement, the first disbursement will have to be made at the latest 90 days after the date of the mortgage act and retainer of 15 %.  
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  For new construction   -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							For new construction: the builder must be accredited by a guaranty program for new homes, and the property must be requbtered in the program. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Renovation -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Renovation: disbursement must be controlled if above $ 10 000. The builder must be accredited through the APCHQ program for renovation
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Obtain renonctation -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/progressAdvance= 'Y'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Obtain renonctation of legal hypotheque and cession of priority of rank, certificate of completion of the construction and 15 % holdbacks until 35 days following the date of work completion. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Intervention   -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Mortgage loan act intervention as surety 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Obtain copy of the water analysis  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Obtain copy of the water analysis and the conformity certificate from the municipality for the well and the septic tank if the property b not connected to the aqueduc system. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  To the request of the borrower(s) -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							To the request of the borrower(s), the payments will be made
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  weekly for the amount of -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">7cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 weekly for the amount of    
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 including life   
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 including life &#38; invalidity 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  aux 2  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">7cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 by weekly accelerated payments of  
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 including life 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 including life &#38; invalidity 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  First paymen to be made the -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">9cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">7cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							 First paymen to be made the 
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId= '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId= '3'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 7th day following the disbursement of the loan;
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId= '4'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId= '5'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>		
							 14th day following the dbbursement of the loan.
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  
		 START OF TABLE  Le or les emprunteurs 
		 ===================================================  
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row"><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
												
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">Le or les emprunteurs désirent un prêt RAP, S.V.P. communiquez avec ces derniers sur la réception du dossier. </fo:inline>
						</xsl:element>
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		 =================================================== 
		 END OF TABLE 
		 ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Autres -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Other 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>
	
	<!--#DG140 optimize -->
	<xsl:template name="CheckBoxes">
		<xsl:choose>
			<xsl:when test="//Deal/mortgageInsurerId = '1'">
				<xsl:call-template name="CheckedCheckbox"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="UnCheckedCheckbox"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="DealConditionsNotWaived">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttrPg4"/>
			<xsl:element name="fo:list-block">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
				<xsl:attribute name="space-after.optimum">3mm</xsl:attribute>
				<xsl:apply-templates select="//Deal/DocumentTracking"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="DocumentTracking">
		<xsl:choose>
			<xsl:when test="Condition/conditionTypeId='0'">
				<xsl:element name="fo:list-item">
					<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
					<xsl:element name="fo:list-item-label">
						<xsl:element name="fo:block" use-attribute-sets="ListLabelIndent">
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:list-item-body">
						<xsl:attribute name="start-indent">body-start()</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="text-align">justify</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="./text"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="((documentStatusId='0') or (documentStatusId='0')) and (Condition/conditionTypeId='2') and (Condition/conditionTypeId!='5')">
					<xsl:element name="fo:list-item">
						<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
						<xsl:element name="fo:list-item-label">
							<xsl:element name="fo:block">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="fo:list-item-body">
							<xsl:attribute name="start-indent">body-start()</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="text-align">justify</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select="./text"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ============================================================================================================================ -->
	<xsl:template name="CreateTitleLinePage4">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								ANNEXE TO OFFER TO FINANCE - MORTGAGE LOAN <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CreateTitleLinePage3">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">11cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">12pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								TERMS AND CONDITIONS
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="mortgageInsuranceCheckBox">
		<xsl:param name="parInsurer"/>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<!-- Start  -->
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="UnCheckedCheckbox"/>
			</xsl:otherwise>
		</xsl:choose>
		<!-- END  -->
	</xsl:template>
	<xsl:template name="mortgageInsurancePolicyNum">
		<xsl:param name="parInsurer"/>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<!-- Start  -->
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- END  -->
	</xsl:template>
	<xsl:template name="mortgageInsurancePremiumAmount">
		<xsl:param name="parInsurer">99</xsl:param>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$parInsurer='99'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--   ================================================================= -->
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageFour"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
