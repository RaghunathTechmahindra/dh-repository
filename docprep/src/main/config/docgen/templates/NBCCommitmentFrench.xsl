<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output version="1.0" method="html" encoding="UTF-8" indent="no"/>
	<xsl:variable name="fo:layout-master-set">
		<fo:layout-master-set>
			<fo:simple-page-master master-name="default-page" page-height="14in" page-width="8.5in" margin-left="0.5in" margin-right="0.48in">
				<fo:region-body margin-top="0.5in" margin-bottom="0.59in"/>
				<fo:region-after extent="0.59in"/>
			</fo:simple-page-master>
			<fo:simple-page-master master-name="CL_First_Page" page-height="14in" page-width="8.5in" margin-left="0.5in" margin-right="0.48in">
				<fo:region-body margin-top="1.4in" margin-bottom="0.59in"/>
				<fo:region-before region-name="RegionBeforeFirstPage" extent="1.4in"/>
				<fo:region-after extent="0.59in"/>
			</fo:simple-page-master>
			<fo:simple-page-master master-name="CL_Other_Page" page-height="14in" page-width="8.5in" margin-left="0.5in" margin-right="0.48in">
				<fo:region-body margin-top="1.2in" margin-bottom="0.59in"/>
				<fo:region-before region-name="RegionBeforeOtherPage" extent="1.2in"/>
				<fo:region-after extent="0.59in"/>
			</fo:simple-page-master>
			<fo:page-sequence-master master-name="CL_Page_Master">
				<fo:repeatable-page-master-alternatives>
					<fo:conditional-page-master-reference master-reference="CL_First_Page" page-position="first"/>
					<fo:conditional-page-master-reference master-reference="CL_Other_Page" page-position="rest"/>
				</fo:repeatable-page-master-alternatives>
			</fo:page-sequence-master>
		</fo:layout-master-set>
	</xsl:variable>

<!-- Venkata, FXP16891 - NBC CR007 Commitment Letter Changes  -->

	<!-- ************************************************************* -->
	<!--  /CommitmentLetter :  NBC Solicitor package - French_Version -->
	<!-- ************************************************************* -->
	<xsl:template match="/">
		<fo:root>
			<xsl:variable name="maxwidth" select="7.52000"/>
			<xsl:variable name="docName" select="//CommitmentLetter/docName"/>
			<xsl:call-template name="CommitmentLetterFr"/><!-- will be called from NBC solicit.pkg too -->
		</fo:root>
	</xsl:template>
	
	<!-- *************************************************************** -->
	<!--  Document :  MORTGAGE COMMITMENT LETTER French Begin - 17975-->
	<!-- *************************************************************** -->
	<xsl:template name = "CommitmentLetterFr">
		<xsl:variable name="maxwidth" select="7.30000"/>
		<xsl:variable name="image-dir" select="//CommitmentLetter/Logo_Image"></xsl:variable>
			<xsl:copy-of select="$fo:layout-master-set"/>
				<xsl:variable name="page-sequenceId" select="concat('EOR', 'block1')" />
				<fo:page-sequence master-reference="default-page" initial-page-number="1" format="1" force-page-count="no-force">
					<xsl:call-template name="footerall">
						<xsl:with-param name="footerText" select="'FX17975-001 (2006-10-01) (Canada)'"/>
						<xsl:with-param name="page-sequence" select="$page-sequenceId" />
						<xsl:with-param name="page-of-str" select="'sur'" />
					</xsl:call-template>
					<fo:flow flow-name="xsl-region-body">
					<fo:block>
						<xsl:variable name="tablewidth0" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths0" select="1.78125 + 0.80208 + 4.07292"/>
						<xsl:variable name="factor0">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths0 &gt; 0.00000 and $sumcolumnwidths0 &gt; $tablewidth0">
									<xsl:value-of select="$tablewidth0 div $sumcolumnwidths0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth0_0" select="1.78125 * $factor0"/>
						<xsl:variable name="columnwidth0_1" select="0.80208 * $factor0"/>
						<xsl:variable name="columnwidth0_2" select="4.07292 * $factor0"/>
						<fo:table width="{$tablewidth0}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black">
							<fo:table-column column-width="{$columnwidth0_0}in"/>
							<fo:table-column column-width="{$columnwidth0_1}in"/>
							<fo:table-column column-width="{$columnwidth0_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.08333in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo_fr.jpeg" width="1.6in"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.08333in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell font-size="10pt" text-align="right" display-align="after" height="0.08333in">
										<fo:block padding-top="1pt" padding-bottom="4pt">
											<fo:inline font-size="11pt" font-weight="bold">
												<xsl:text>OFFRE DE FINANCEMENT HYPOTH�CAIRE</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.01042in" number-columns-spanned="3">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block space-before.optimum="-8pt">
												<fo:leader leader-length="100%" leader-pattern="rule" rule-thickness="2pt"/>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth1" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths1" select="0.40625 + 0.04167 + 2.53125 + 0.04167 + 0.04167"/>
						<xsl:variable name="factor1">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths1 &gt; 0.00000 and $sumcolumnwidths1 &gt; $tablewidth1">
									<xsl:value-of select="$tablewidth1 div $sumcolumnwidths1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns1" select="1"/>
						<xsl:variable name="defaultcolumnwidth1">
							<xsl:choose>
								<xsl:when test="$factor1 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns1 &gt; 0">
									<xsl:value-of select="($tablewidth1 - $sumcolumnwidths1) div $defaultcolumns1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth1_0" select="0.40625 * $factor1"/>
						<xsl:variable name="columnwidth1_1" select="2.53125 * $factor1"/>
						<xsl:variable name="columnwidth1_2" select="$defaultcolumnwidth1"/>
						<fo:table width="{$tablewidth1}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth1_0}in"/>
							<fo:table-column column-width="{$columnwidth1_1}in"/>
							<fo:table-column column-width="{$columnwidth1_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:text>Date</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10pt">
												<xsl:value-of select="//CommitmentLetter/date1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth2" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths2" select="0.04167"/>
						<xsl:variable name="defaultcolumns2" select="1"/>
						<xsl:variable name="defaultcolumnwidth2">
							<xsl:choose>
								<xsl:when test="$defaultcolumns2 &gt; 0">
									<xsl:value-of select="($tablewidth2 - $sumcolumnwidths2) div $defaultcolumns2"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth2_0" select="$defaultcolumnwidth2"/>
						<fo:table width="{$tablewidth2}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth2_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt" text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-weight="bold" text-align="center">
												<xsl:text>Acceptation conditionnelle de votre demande de financement hypoth�caire</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth3" select="$maxwidth * 1.00000 - 0.00521 - 0.00521"/>
						<xsl:variable name="sumcolumnwidths3" select="3.7"/>
						<xsl:variable name="factor3">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths3 &gt; 0.00000 and $sumcolumnwidths3 &gt; $tablewidth3">
									<xsl:value-of select="$tablewidth3 div $sumcolumnwidths3"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns3" select="1"/>
						<xsl:variable name="defaultcolumnwidth3">
							<xsl:choose>
								<xsl:when test="$factor3 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns3 &gt; 0">
									<xsl:value-of select="($tablewidth3 - $sumcolumnwidths3) div $defaultcolumns3"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth3_0" select="3.7 * $factor3"/>
						<xsl:variable name="columnwidth3_1" select="$defaultcolumnwidth3"/>
						<fo:table width="{$maxwidth}in">
							<fo:table-column column-width="{0.00521}in"/>
							<fo:table-column column-width="{$tablewidth3}in"/>
							<fo:table-column column-width="{0.00521}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<fo:table width="{$tablewidth3}in" space-before.optimum="1pt" space-after.optimum="2pt" border-top-style="solid" border-top-color="black" border-top-width="0.00521in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.00521in" border-left-style="solid" border-left-color="black" border-left-width="0.00521in" border-right-style="solid" border-right-color="black" border-right-width="0.00521in" border-collapse="separate" color="black">
												<fo:table-column column-width="{$columnwidth3_0}in"/>
												<fo:table-column column-width="{$columnwidth3_1}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell font-size="10pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth4" select="$columnwidth3_0 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths4" select="1.26042"/>
																<xsl:variable name="factor4">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths4 &gt; 0.00000 and $sumcolumnwidths4 &gt; $tablewidth4">
																			<xsl:value-of select="$tablewidth4 div $sumcolumnwidths4"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns4" select="1"/>
																<xsl:variable name="defaultcolumnwidth4">
																	<xsl:choose>
																		<xsl:when test="$factor4 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns4 &gt; 0">
																			<xsl:value-of select="($tablewidth4 - $sumcolumnwidths4) div $defaultcolumns4"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth4_0" select="1.26042 * $factor4"/>
																<xsl:variable name="columnwidth4_1" select="$defaultcolumnwidth4"/>
																<fo:table width="{$tablewidth4}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black">
																	<fo:table-column column-width="{$columnwidth4_0}in"/>
																	<fo:table-column column-width="{$columnwidth4_1}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Banque Nationale</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Adresse CTPH :</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt" number-rows-spanned="2">
																				<fo:block padding-top="1pt" padding-bottom="1pt"  font-size="10pt">
																					<fo:inline>
																						<xsl:value-of select="//CommitmentLetter/branchAddress"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" height="0.26042in" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell font-size="10pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth5" select="$columnwidth3_1 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths5" select="1.34375"/>
																<xsl:variable name="factor5">
																	<xsl:choose>
																		<xsl:when test="$sumcolumnwidths5 &gt; 0.00000 and $sumcolumnwidths5 &gt; $tablewidth5">
																			<xsl:value-of select="$tablewidth5 div $sumcolumnwidths5"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="1.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="defaultcolumns5" select="1"/>
																<xsl:variable name="defaultcolumnwidth5">
																	<xsl:choose>
																		<xsl:when test="$factor5 &lt; 1.000">
																			<xsl:value-of select="0.000"/>
																		</xsl:when>
																		<xsl:when test="$defaultcolumns5 &gt; 0">
																			<xsl:value-of select="($tablewidth5 - $sumcolumnwidths5) div $defaultcolumns5"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth5_0" select="1.34375 * $factor5"/>
																<xsl:variable name="columnwidth5_1" select="$defaultcolumnwidth5"/>
																<fo:table width="{$tablewidth5}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black">
																	<fo:table-column column-width="{$columnwidth5_0}in"/>
																	<fo:table-column column-width="{$columnwidth5_1}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Succursale</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Adresse succursale : </xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																			<fo:table-cell font-size="10pt" number-rows-spanned="2">
																				<fo:block padding-top="1pt" padding-bottom="1pt"/>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" height="0.26042in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell font-size="10pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth6" select="$columnwidth3_0 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths6" select="0.000"/>
																<xsl:variable name="defaultcolumns6" select="1"/>
																<xsl:variable name="defaultcolumnwidth6">
																	<xsl:choose>
																		<xsl:when test="$defaultcolumns6 &gt; 0">
																			<xsl:value-of select="($tablewidth6 - $sumcolumnwidths6) div $defaultcolumns6"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth6_0" select="$defaultcolumnwidth6"/>
																<fo:table width="{$tablewidth6}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth6_0}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Nom du demandeur : </xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline  font-size="10pt">
																						<xsl:value-of select="//CommitmentLetter/borName5"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell font-size="10pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:variable name="tablewidth7" select="$columnwidth3_1 * 1.00000"/>
																<xsl:variable name="sumcolumnwidths7" select="0.000"/>
																<xsl:variable name="defaultcolumns7" select="1"/>
																<xsl:variable name="defaultcolumnwidth7">
																	<xsl:choose>
																		<xsl:when test="$defaultcolumns7 &gt; 0">
																			<xsl:value-of select="($tablewidth7 - $sumcolumnwidths7) div $defaultcolumns7"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="0.000"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:variable>
																<xsl:variable name="columnwidth7_0" select="$defaultcolumnwidth7"/>
																<fo:table width="{$tablewidth7}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
																	<fo:table-column column-width="{$columnwidth7_0}in"/>
																	<fo:table-body>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline>
																						<xsl:text>Adresse de la propri�t� :</xsl:text>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																		<fo:table-row>
																			<fo:table-cell font-size="10pt" padding-left="0.05000in">
																				<fo:block padding-top="1pt" padding-bottom="1pt">
																					<fo:inline font-size="10pt">
																						<xsl:value-of select="//CommitmentLetter/propAddress"/>
																					</fo:inline>
																				</fo:block>
																			</fo:table-cell>
																		</fo:table-row>
																	</fo:table-body>
																</fo:table>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell font-size="10pt" text-align="center" display-align="center" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="3pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>D�tail du pr�t</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="2">
														<fo:table>
															<fo:table-column column-width="proportional-column-width(59)"/>
															<fo:table-column column-width="proportional-column-width(41)"/>
															<fo:table-body>
																<fo:table-row>
																	<fo:table-cell border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
																		<fo:block>
																		<fo:table>
																			<fo:table-column column-width="proportional-column-width(30)"/>
																			<fo:table-column column-width="proportional-column-width(51)"/>
																			<fo:table-column column-width="proportional-column-width(19)"/>
																			<fo:table-body>
																				<fo:table-row>
																					<fo:table-cell padding-left="0.05000in" font-size="10pt">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline>
																								<xsl:text>Montant du pr�t :</xsl:text>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell number-columns-spanned="2" font-size="10pt">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline>
																								<xsl:value-of select="//CommitmentLetter/princAmtBor"/>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell padding-left="0.05000in"  number-columns-spanned="2" font-size="10pt">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline>
																								<xsl:text>Montant publi� / enregistr� de la charge / hypoth�que :</xsl:text>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell font-size="10pt">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline>
																								<xsl:value-of select="//CommitmentLetter/regAmtCharge"/>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell padding-left="0.05000in" font-size="10pt">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline>
																								<xsl:text>Prix d'achat :</xsl:text>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell number-columns-spanned="2" font-size="10pt">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline>
																								<xsl:value-of select="//CommitmentLetter/purPrice"/>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																				<fo:table-row>
																					<fo:table-cell padding-left="0.05000in" font-size="10pt">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline>
																								<xsl:text>Mise de fonds :</xsl:text>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																					<fo:table-cell number-columns-spanned="2" font-size="10pt">
																						<fo:block padding-top="1pt" padding-bottom="1pt">
																							<fo:inline>
																								<xsl:value-of select="//CommitmentLetter/downPayment"/>
																							</fo:inline>
																						</fo:block>
																					</fo:table-cell>
																				</fo:table-row>
																			</fo:table-body>
																		</fo:table>
																		</fo:block>
																	</fo:table-cell>
																	<fo:table-cell padding-left="0.05000in" display-align="before" font-size="10pt">
																		<fo:block padding-top="1pt" padding-bottom="1pt">
																			<fo:inline>
																				<xsl:text>La pr�sente offre est valide jusqu'au :</xsl:text>
																			</fo:inline>
																		</fo:block>
																		<fo:block padding-top="1pt" padding-bottom="1pt">
																			<fo:inline>
																				<xsl:value-of select="//CommitmentLetter/commitValUntil"/>
																			</fo:inline>
																		</fo:block>
																	</fo:table-cell>
																</fo:table-row>
															</fo:table-body>
														</fo:table>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth12" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths12" select="0.000"/>
						<xsl:variable name="defaultcolumns12" select="1"/>
						<xsl:variable name="defaultcolumnwidth12">
							<xsl:choose>
								<xsl:when test="$defaultcolumns12 &gt; 0">
									<xsl:value-of select="($tablewidth12 - $sumcolumnwidths12) div $defaultcolumns12"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth12_0" select="$defaultcolumnwidth12"/>
						<fo:table width="{$tablewidth12}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth12_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt" height="0.18750in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-weight="bold" text-align="justify">
												<xsl:text>Information pour le juriste : Voir la section Condition de financement page 3.</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-top="4pt" height="0.25000in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" display-align="center">
										<fo:block padding-top="1pt" font-size="10pt">
											<fo:inline font-size="10pt">
												<xsl:value-of select="//CommitmentLetter/infoSol"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.26042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.25000in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.26042in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth13" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths13" select="0.000"/>
						<xsl:variable name="defaultcolumns13" select="1"/>
						<xsl:variable name="defaultcolumnwidth13">
							<xsl:choose>
								<xsl:when test="$defaultcolumns13 &gt; 0">
									<xsl:value-of select="($tablewidth13 - $sumcolumnwidths13) div $defaultcolumns13"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth13_0" select="$defaultcolumnwidth13"/>
						<fo:table width="{$tablewidth13}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth13_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt">
										<fo:block padding-top="1pt" padding-bottom="4pt" text-align="justify">
											<fo:inline font-weight="bold">
												<xsl:text>Si vous avez choisi de r�partir votre pr�t en diff�rentes tranches (les �Tranches�), votre pr�t b�n�ficie d'une option Multi-Choix. Si le pr�t est assur� et que votre prime d'assurance pr�t est financ�e, cette prime est incluse dans le montant du pr�t.</xsl:text>
											</fo:inline>
										</fo:block>
										<fo:block padding-top="1pt" padding-bottom="4pt" text-align="justify">
											<fo:inline font-weight="bold">
												<xsl:text>Le montant du pr�t, tel qu'indiqu� plus en d�tail au tableau figurant � la page 2, correspond au montant maximal que la Banque met � votre disposition et ce, malgr� tout autre montant garanti par la charge ou l'hypoth�que enregistr�e/publi�e � l'encontre de la Propri�t�.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth14" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths14" select="0.000"/>
						<xsl:variable name="defaultcolumns14" select="1"/>
						<xsl:variable name="defaultcolumnwidth14">
							<xsl:choose>
								<xsl:when test="$defaultcolumns14 &gt; 0">
									<xsl:value-of select="($tablewidth14 - $sumcolumnwidths14) div $defaultcolumns14"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth14_0" select="$defaultcolumnwidth14"/>
						<fo:table width="{$tablewidth14}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" text-align="center" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth14_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-weight="bold">
												<xsl:text>R�PARTITION DU MONTANT DU PR�T</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth15" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths15" select="1.03125 + 2.25000 + 0.10417 + 0.93750 + 0.15625 + 0.59375"/>
						<xsl:variable name="factor15">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths15 &gt; 0.00000 and $sumcolumnwidths15 &gt; $tablewidth15">
									<xsl:value-of select="$tablewidth15 div $sumcolumnwidths15"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth15_0" select="1.06395 * $factor15"/>
						<xsl:variable name="columnwidth15_1" select="1.85126 * $factor15"/>
						<xsl:variable name="columnwidth15_3" select="0.93750 * $factor15"/>
						<xsl:variable name="columnwidth15_4" select="0.15625 * $factor15"/>
						<xsl:variable name="columnwidth15_5" select="1.06395 * $factor15"/>
						<fo:table width="{$tablewidth15}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="8pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth15_0}in"/>
							<fo:table-column column-width="{$columnwidth15_1}in"/>
							<fo:table-column column-width="{$columnwidth15_3}in"/>
							<fo:table-column column-width="{$columnwidth15_4}in"/>
							<fo:table-column column-width="{$columnwidth15_5}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding-top="4pt">
										<fo:block/>
									</fo:table-cell>
									<fo:table-cell padding-top="4pt" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.05000in">
										<fo:block/>
									</fo:table-cell>
									<fo:table-cell padding-top="4pt" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block/>
									</fo:table-cell>
									<fo:table-cell padding-top="4pt" border-top-style="solid" border-top-color="black" border-top-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block/>
									</fo:table-cell>
									<fo:table-cell padding-top="4pt">
										<fo:block/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.10000in">
										<fo:block padding-top="5pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Montant du financement de base :</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="5pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:value-of select="//CommitmentLetter/availAmt"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="3" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in" padding-left="0.10000in">
										<fo:block padding-top="5pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Montants additionnels financ�s :</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.10000in">
										<fo:block padding-top="5pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>�	&#160;&#160;&#160;Prime d'assurance-pr�t (SCHL ou Genworth) :</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="5pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:value-of select="//CommitmentLetter/amtDedPrinAmtBor"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
										<fo:block padding-top="5pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth16" select="$columnwidth15_1 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths16" select="1.72917 + 1.87083 + 0.51458"/>
											<xsl:variable name="factor16">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths16 &gt; 0.00000 and $sumcolumnwidths16 &gt; $tablewidth16">
														<xsl:value-of select="$tablewidth16 div $sumcolumnwidths16"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth16_0" select="1.72917 * $factor16"/>
											<xsl:variable name="columnwidth16_1" select="1.87083 * $factor16"/>
											<xsl:variable name="columnwidth16_2" select="0.51458 * $factor16"/>
											<fo:table width="{$tablewidth16}in" border-collapse="separate" color="black">
												<fo:table-column column-width="{$columnwidth16_0}in"/>
												<fo:table-column column-width="{$columnwidth16_1}in"/>
												<fo:table-column column-width="{$columnwidth16_2}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-left="0.10000in">
															<fo:block >
																<fo:inline>
																	<xsl:text>�	&#160;&#160;&#160;Autres : </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block />
														</fo:table-cell>
														<fo:table-cell>
															<fo:block  />
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.10000in">
										<fo:block padding-top="5pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Montant du Pr�t :</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="5pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:value-of select="//CommitmentLetter/loanAmount"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="5pt" />
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="5pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-bottom="4pt"/>
									</fo:table-cell>
									<fo:table-cell padding-bottom="4pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.05000in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell padding-bottom="4pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell padding-bottom="4pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-right-style="solid" border-right-color="black" border-right-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-bottom="4pt"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth17" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths17" select="7.47917"/>
						<xsl:variable name="factor17">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths17 &gt; 0.00000 and $sumcolumnwidths17 &gt; $tablewidth17">
									<xsl:value-of select="$tablewidth17 div $sumcolumnwidths17"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth17_0" select="7.47917 * $factor17"/>
						<fo:table width="{$tablewidth17}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth17_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>Le tableau suivant d�crit les conditions qui r�giront votre pr�t. Si Ie pr�t a �t� divis� en tranches, chaque tranche est r�gie et remboursable suivant les modalit�s �nonc�es dans le tableau. (Veuillez examiner les renseignements suivants afin de vous assurer qu'ils correspondent aux choix et d�cisions que vous avez effectu�s lors de votre demande de financement hypoth�caire aupr�s de notre repr�sentant. Pour toutes questions, n'h�sitez pas � communiquer avec nous au 1 888 835-6281 et il nous fera plaisir d'y r�pondre).</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth18" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths18" select="0.03763 + 0.35752 + 0.20968 + 0.20968 + 0.20968"/>
						<xsl:variable name="factor18">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths18 &gt; 0.00000 and $sumcolumnwidths18 &gt; $tablewidth18">
									<xsl:value-of select="$tablewidth18 div $sumcolumnwidths18"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth18_0" select="0.03763 * $factor18"/>
						<xsl:variable name="columnwidth18_1" select="0.35752 * $factor18"/>
						<xsl:variable name="columnwidth18_2" select="0.20968 * $factor18"/>
						<xsl:variable name="columnwidth18_3" select="0.20968 * $factor18"/>
						<xsl:variable name="columnwidth18_4" select="0.20968 * $factor18"/>
						<fo:table width="{$tablewidth18}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center" border-right-style="solid" border-right-color="black" border-right-width="0.00521in">
							<fo:table-column column-width="{$columnwidth18_0}in"/>
							<fo:table-column column-width="{$columnwidth18_1}in"/>
							<fo:table-column column-width="{$columnwidth18_2}in"/>
							<fo:table-column column-width="{$columnwidth18_3}in"/>
							<fo:table-column column-width="{$columnwidth18_4}in"/>
							<fo:table-body font-size="8pt">
								<fo:table-row>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-weight="bold">
												<xsl:text>Modalit�s de votre financement</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline  font-weight="bold">
												<xsl:text>Tranche 1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline  font-weight="bold">
												<xsl:text>Tranche 2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>Tranche 3</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>1.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Terme&#160; -&#160; Ouvert / Ferm�</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/term2"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/openRateLoan = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>ouvert</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/closedRateLoan = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" padding-left="0.05000in" display-align="center">
															<fo:block>
																<fo:inline>
																	<xsl:text>ferm�</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>ouvert</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" padding-left="0.05000in" display-align="center">
															<fo:block>
																<fo:inline>
																	<xsl:text>ferm�</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>ouvert</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" padding-left="0.05000in" display-align="center">
															<fo:block>
																<fo:inline>
																	<xsl:text>ferm�</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>2.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Montant en capital</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/princAmtBor"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>3.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Date d'ajustement des int�r�ts</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/intAdjDate"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>4.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>P�riode d'amortissement</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(75)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block>
																<fo:inline font-size="8pt">
																	<xsl:value-of select="//CommitmentLetter/amortPeriod"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" text-align="left">
															<fo:block>
																<fo:inline font-size="8pt">
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(75)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" text-align="left">
															<fo:block>
																<fo:inline font-size="8pt">
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(75)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" text-align="left">
															<fo:block>
																<fo:inline font-size="8pt">
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block break-after="page"/>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth32" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths32" select="0.000"/>
						<xsl:variable name="defaultcolumns32" select="1"/>
						<xsl:variable name="defaultcolumnwidth32">
							<xsl:choose>
								<xsl:when test="$defaultcolumns32 &gt; 0">
									<xsl:value-of select="($tablewidth32 - $sumcolumnwidths32) div $defaultcolumns32"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth32_0" select="$defaultcolumnwidth32"/>
						<fo:table width="{$tablewidth32}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth32_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo_fr.jpeg" width="1.6in"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth33" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths33" select="0.03763 + 0.35752 + 0.20968 + 0.20968 + 0.20968"/>
						<xsl:variable name="factor33">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths33 &gt; 0.00000 and $sumcolumnwidths33 &gt; $tablewidth33">
									<xsl:value-of select="$tablewidth33 div $sumcolumnwidths33"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth33_0" select="0.03763 * $factor33"/>
						<xsl:variable name="columnwidth33_1" select="0.35752 * $factor33"/>
						<xsl:variable name="columnwidth33_2" select="0.20968 * $factor33"/>
						<xsl:variable name="columnwidth33_3" select="0.20968 * $factor33"/>
						<xsl:variable name="columnwidth33_4" select="0.20968 * $factor33"/>
						<fo:table width="{$tablewidth33}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center" border-right-style="solid" border-right-color="black" border-right-width="0.00521in">
							<fo:table-column column-width="{$columnwidth33_0}in"/>
							<fo:table-column column-width="{$columnwidth33_1}in"/>
							<fo:table-column column-width="{$columnwidth33_2}in"/>
							<fo:table-column column-width="{$columnwidth33_3}in"/>
							<fo:table-column column-width="{$columnwidth33_4}in"/>
							<fo:table-body font-size="8pt">
								<fo:table-row>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-weight="bold">
												<xsl:text>Modalit�s de votre financement</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline  font-weight="bold">
												<xsl:text>Tranche 1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline  font-weight="bold">
												<xsl:text>Tranche 2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>Tranche 3</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>5.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Date du premier Versement r�gulier</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/dateFirstPayment"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>6.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Fr�quence des versements</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>&#160;</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(65)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/paymentFreq1 = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>mensuelle</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/paymentFreq2 = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>aux 2 semaines</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/paymentFreq3 = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>hebdomadaire</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(65)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/alwUnchkd = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>mensuelle</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/alwUnchkd = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>aux 2 semaines</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/alwUnchkd = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>hebdomadaire</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(65)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/alwUnchkd = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>mensuelle</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/alwUnchkd = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>aux 2 semaines</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline >
																	<xsl:choose>
																		<xsl:when test="//CommitmentLetter/alwUnchkd = '1' ">
																			<xsl:call-template name="CheckedCheckbox"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:call-template name="UnCheckedCheckbox"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:inline >
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="center" padding-left="0.05000in">
															<fo:block>
																<fo:inline>
																	<xsl:text>hebdomadaire</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>7.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Date de fin du Terme (inscrire date �ch�ance r�el)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/matDate"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>8.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Versement p�riodique pour les charges fonci�res (selon la fr�quence indiqu�e � la ligne 6)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/propTaxInstall"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>9.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Date de premi�re r�vision du taux d'int�r�t</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/intRevFreq"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>10.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Frais d'administration p�riodiques (selon la fr�quence indiqu�e � la ligne 6)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/other1"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>11.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Prime d'assurance &#xA; (selon la fr�quence indiqu�e � la ligne 6)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/totInsPrem"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>12.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux d'int�r�t promotionnel, dur�e de la promotion et montant du versement y aff�rent (le cas �ch�ant)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<xsl:value-of select="//CommitmentLetter/teaserRate"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/teaserTerm"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="3pt" padding-bottom="1pt">
															<fo:block text-align="left">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/teaserPayAmt"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="3pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="3pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>13.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Renouvellement automatique</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="1.6in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="0.4in"/>
												<fo:table-column column-width="0.3in"/>
												<fo:table-column column-width="0.2in"/>
												<fo:table-column column-width="0.7in"/>
												<fo:table-body font-size="8pt">
													<fo:table-row>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
											  					<xsl:choose>
																	<xsl:when test="//CommitmentLetter/intRateRenewAuto1 = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt" >
																	<xsl:text>&#160;Oui</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/intRateRenNotAuto1 = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>Non</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:table width="1.6in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="0.4in"/>
												<fo:table-column column-width="0.3in"/>
												<fo:table-column column-width="0.2in"/>
												<fo:table-column column-width="0.7in"/>
												<fo:table-body font-size="8pt">
													<fo:table-row>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
											  					<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt" >
																	<xsl:text>&#160;Oui</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell  text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>Non</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:table width="1.6in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="0.4in"/>
												<fo:table-column column-width="0.3in"/>
												<fo:table-column column-width="0.2in"/>
												<fo:table-column column-width="0.7in"/>
												<fo:table-body font-size="8pt">
													<fo:table-row>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
											  					<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt" >
																	<xsl:text>&#160;Oui</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//CommitmentLetter/alwUnchkd = '1'">
																		<xsl:call-template name="CheckedCheckbox"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:call-template name="UnCheckedCheckbox"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="left">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>Non</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>14.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Remise en argent (le cas �ch�ant)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:value-of select="//CommitmentLetter/cashbackAmt1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.22917in" display-align="center" number-columns-spanned="5" text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="4pt" padding-bottom="1pt">
											<fo:inline font-weight="bold">
												<xsl:text>Les modalit�s suivantes s'appliquent uniquement � un Pr�t � taux fixe</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>15.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux d'int�r�t fixe (calcul� semestriellement et non � l'avance)</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//CommitmentLetter/intRateFixed"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//CommitmentLetter/discount"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:value-of select="//CommitmentLetter/fxRate"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>Taux affich�</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>Rabais de taux</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>Taux affich�</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>Rabais de taux</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>Taux affich�</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<fo:inline font-size="6pt">
																	<xsl:text>Rabais de taux</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="6pt">
												<xsl:text>15.1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux pond�r� (calcul� semestriellement et non � l'avance), montant de l'indemnit� financ�e par application du taux pond�r� et le montant du versement y aff�rent (le cas �ch�ant)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/refPaymentIC"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>16.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Montant du versement r�gulier (capital + int�r�ts)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/monPayment"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.21875in" display-align="center" number-columns-spanned="5" text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="4pt" padding-bottom="1pt">
											<fo:inline font-weight="bold">
												<xsl:text>Les modalit�s suivantes s'appliquent uniquement � un Pr�t � taux variable</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>17.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux hypoth�caire (TH) </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>3</xsl:text>
											</fo:inline>
											<fo:inline vertical-align="super">
												<xsl:text>&#160;</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>: taux applicable � un pr�t hypoth�caire d'un terme ferm� de :</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(65)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(65)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(25)"/>
												<fo:table-column column-width="proportional-column-width(65)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>18.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux d'int�r�t variable (TIV) en date du</xsl:text>
											</fo:inline>
										</fo:block>
										<fo:block padding-bottom="2pt">
											<fo:table>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-column column-width="proportional-column-width(60)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<xsl:value-of select="//CommitmentLetter/intRateActDate"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block font-size="6pt">
																<xsl:text>(calcul� mensuellement et non � </xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="2">
															<fo:block font-size="6pt">
																<xsl:text>l'avance)</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:value-of select="//CommitmentLetter/varIntRate"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:value-of select="//CommitmentLetter/intRateSpread"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:value-of select="//CommitmentLetter/intRateSpreadVal"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block font-size="6pt">
																<xsl:value-of select="//CommitmentLetter/diffMorRateNSpread"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TH</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>+/-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>�cart</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TH</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>+/-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>�cart</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TH</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>+/-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>�cart</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="6pt">
												<xsl:text>18.1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux pond�r� (calcul� mensuellement et non � l'avance), montant de l'indemnit� financ�e par application du taux pond�r� et le montant du versement y aff�rent (le cas �ch�ant)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right" font-size="6pt">
																<xsl:text>+</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>Taux pond�r�</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right" font-size="6pt">
																<xsl:text>+</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>Taux pond�r�</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right" font-size="6pt">
																<xsl:text>+</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left" font-size="6pt">
																<xsl:text>%</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>Taux pond�r�</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>&#160;</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="left"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>19.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Fr�quence de r�vision du Taux d'int�r�t variable</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right">
																<fo:inline>
																	<xsl:text>Tous les</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/intRateRevFreq1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois ou</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right">
																<fo:inline>
																	<xsl:text>Tous les</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/intRateRevFreq2"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>semaines</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right">
																<fo:inline>
																	<xsl:text>Tous les</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois ou</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right">
																<fo:inline>
																	<xsl:text>Tous les</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>semaines</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right">
																<fo:inline>
																	<xsl:text>Tous les</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>mois ou</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="right">
																<fo:inline>
																	<xsl:text>Tous les</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>semaines</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>20.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux plafond </xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>(le cas �ch�ant)</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>4</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/cappedRate"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>21.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux critique (le cas �ch�ant) </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>5 </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center">
																<fo:inline>
																	<xsl:value-of select="//CommitmentLetter/cricRate"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(30)"/>
												<fo:table-column column-width="proportional-column-width(40)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<fo:inline>
																	<xsl:text>%</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>22.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Montant du Versement r�gulier fixe (capital + int�r�ts)</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>6</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/fixedPayAmt"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>23.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Montant du premier versement r�gulier variable (capital + int�r�ts), calcul� selon le taux d'int�r�t indiqu� � la ligne 18 selon le cas (donn� � titre indicatif seulement si une promotion s'applique)</xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>6</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<xsl:value-of select="//CommitmentLetter/empProgPay"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:inline>
							<xsl:text>_____________________</xsl:text>
						</fo:inline>
						<fo:block>
							<xsl:text>&#xA;</xsl:text>
						</fo:block>
						<xsl:variable name="tablewidth75" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths75" select="0.50000"/>
						<xsl:variable name="factor75">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths75 &gt; 0.00000 and $sumcolumnwidths75 &gt; $tablewidth75">
									<xsl:value-of select="$tablewidth75 div $sumcolumnwidths75"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns75" select="1"/>
						<xsl:variable name="defaultcolumnwidth75">
							<xsl:choose>
								<xsl:when test="$factor75 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns75 &gt; 0">
									<xsl:value-of select="($tablewidth75 - $sumcolumnwidths75) div $defaultcolumns75"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth75_0" select="0.50000 * $factor75"/>
						<xsl:variable name="columnwidth75_1" select="$defaultcolumnwidth75"/>
						<fo:table width="{$tablewidth75}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="6pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth75_0}in"/>
							<fo:table-column column-width="{$columnwidth75_1}in"/>
							<fo:table-body font-size="10pt">

								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>1</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>La fr�quence des versements sert � �tablir quand les versements p�riodiques doivent �tre effectu�s � compter de la date de rajustement des int�r�ts. Par exemple, si la fr�quence de versement est mensuelle, les versements p�riodiques doivent �tre effectu�s mensuellement, le premier versement devant �tre effectu� un mois apr�s la date de rajustement des int�r�ts.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>




								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>2</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>Prenez note que vous pourrez profiter des baisses de taux d'int�r�t pouvant survenir jusqu'� 3 jours avant la signature des documents l�gaux aupr�s de votre conseiller juridique. Le taux indiqu� dans la pr�sente Offre est donc sujet � changement. </xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" height="0.05208in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>3</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="before" height="0.05208in" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>�Taux hypoth�caire� ou �TH� signifie le taux d'int�r�t annuel annonc� publiquement par la Banque � l'occasion et � partir duquel elle d�termine les taux d'int�r�t applicables aux pr�ts hypoth�caires r�sidentiels � taux fixe et du terme ferm� mentionn� � la ligne 17 ci-dessus.
</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>4</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>�Taux plafond� signifie, s'il y a lieu, le taux d'int�r�t maximum applicable � votre Pr�t.</xsl:text>
											</fo:inline>
												</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>5</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>�Taux critique� signifie, le taux au-del� duquel le versement r�gulier fixe indiqu� � la ligne 22 n'est plus suffisant pour couvrir l'int�r�t exigible sur le capital de votre Pr�t.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="8pt">
												<xsl:text>Note </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>6</xsl:text>
											</fo:inline>
											<fo:inline font-size="8pt">
												<xsl:text>:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.13542in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="8pt">
												<xsl:text>Ce versement fixe doit �tre �gal ou sup�rieur au versement calcul� avec le taux d'int�r�t variable indiqu� � la ligne 18 du Sommaire. Ce versement fixe sera augment� si le taux d'int�r�t variable atteint ou d�passe le taux critique, de fa�on � s'assurer que le Pr�t soit rembours� en totalit� au cours de la p�riode d'amortissement.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

							</fo:table-body>
						</fo:table>
						<fo:block break-after="page"/>
						<xsl:variable name="tablewidth76" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths76" select="0.000"/>
						<xsl:variable name="defaultcolumns76" select="1"/>
						<xsl:variable name="defaultcolumnwidth76">
							<xsl:choose>
								<xsl:when test="$defaultcolumns76 &gt; 0">
									<xsl:value-of select="($tablewidth76 - $sumcolumnwidths76) div $defaultcolumns76"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth76_0" select="$defaultcolumnwidth76"/>
						<fo:table width="{$tablewidth76}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth76_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo_fr.jpeg" width="1.6in"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth77" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths77" select="0.38542 + 3.21875 + 2.44792 + 2.44792 + 2.43750"/>
						<xsl:variable name="factor77">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths77 &gt; 0.00000 and $sumcolumnwidths77 &gt; $tablewidth77">
									<xsl:value-of select="$tablewidth77 div $sumcolumnwidths77"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth77_0" select="0.38542 * $factor77"/>
						<xsl:variable name="columnwidth77_1" select="3.21875 * $factor77"/>
						<xsl:variable name="columnwidth77_2" select="2.44792 * $factor77"/>
						<xsl:variable name="columnwidth77_3" select="2.44792 * $factor77"/>
						<xsl:variable name="columnwidth77_4" select="2.43750 * $factor77"/>
						<fo:table width="{$tablewidth77}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center" border-right-style="solid" border-right-color="black" border-right-width="0.00521in">
							<fo:table-column column-width="{$columnwidth77_0}in"/>
							<fo:table-column column-width="{$columnwidth77_1}in"/>
							<fo:table-column column-width="{$columnwidth77_2}in"/>
							<fo:table-column column-width="{$columnwidth77_3}in"/>
							<fo:table-column column-width="{$columnwidth77_4}in"/>
							<fo:table-body font-size="8pt">
								<fo:table-row>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-weight="bold">
												<xsl:text>Modalit�s de votre financement</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline  font-weight="bold">
												<xsl:text>Tranche 1</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline  font-weight="bold">
												<xsl:text>Tranche 2</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" border-top-style="solid" border-top-color="black" border-top-width="0.01042in">
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt" font-weight="bold">
												<xsl:text>Tranche 3</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.21875in" display-align="center" number-columns-spanned="5" text-align="center"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.02500in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="4pt" padding-bottom="1pt">
											<fo:inline font-weight="bold">
												<xsl:text>Les modalit�s suivantes s'appliquent uniquement � une Marge de cr�dit</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>24.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Taux d'int�r�t annuel variable (TIAV) en </xsl:text>
											</fo:inline>
										</fo:block>
										<fo:block padding-bottom="2pt">
											<fo:table>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(80)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block>
																<xsl:text>date du</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-right="2pt" padding-top="1pt" padding-bottom="1pt"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="left">
																<xsl:value-of select="//CommitmentLetter/intRateActDate"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="2" padding-top="1pt" padding-bottom="1pt">
															<fo:block font-size="7.5pt" font-weight="bold">
																<xsl:text>(donn� � titre indicatif seulement)</xsl:text>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:value-of select="//CommitmentLetter/intRatePrime"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:value-of select="//CommitmentLetter/spreadGT0"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center" font-size="6pt">
																<xsl:value-of select="//CommitmentLetter/varRateToPrime"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block font-size="6pt">
																<xsl:value-of select="//CommitmentLetter/calcField"/>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>Taux de base</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>+/-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>�cart</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIAV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>Taux de base</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>+/-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>�cart</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIAV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:table width="95%" border-collapse="separate" color="black" >
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>
												<fo:table-column column-width="proportional-column-width(20)"/>
												<fo:table-column column-width="proportional-column-width(10)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>=</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>Taux de base</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>+/-</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>�cart</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center"/>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block text-align="center" font-size="6pt">
																<xsl:text>TIAV</xsl:text>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding-top="1pt" padding-bottom="1pt">
															<fo:block/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt">
											<fo:inline font-size="8pt">
												<xsl:text>25.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-right="0.05000in" text-align="left" padding-left="0.10000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt" text-align="justify">
											<fo:inline>
												<xsl:text>Versement optionnel</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
									<fo:table-cell display-align="center" padding-left="0.05000in"  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" >
										<fo:block padding-top="2pt" padding-bottom="2pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="left" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" padding-left="0.03000in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Toutes les Tranches de votre Pr�t sont:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.12500in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth94" select="$columnwidth77_2 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths94" select="0.15625"/>
											<xsl:variable name="factor94">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths94 &gt; 0.00000 and $sumcolumnwidths94 &gt; $tablewidth94">
														<xsl:value-of select="$tablewidth94 div $sumcolumnwidths94"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns94" select="1"/>
											<xsl:variable name="defaultcolumnwidth94">
												<xsl:choose>
													<xsl:when test="$factor94 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns94 &gt; 0">
														<xsl:value-of select="($tablewidth94 - $sumcolumnwidths94) div $defaultcolumns94"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth94_0" select="0.15625 * $factor94"/>
											<xsl:variable name="columnwidth94_1" select="$defaultcolumnwidth94"/>
											<fo:table width="{$tablewidth94}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth94_0}in"/>
												<fo:table-column column-width="{$columnwidth94_1}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/loanInsType1 = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>

														</fo:table-cell>
														<fo:table-cell font-size="8pt" height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt" padding-left="0.05in">
																<fo:inline><xsl:text>&#160;</xsl:text>
																	<xsl:text>non assur�es</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.12500in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth95" select="$columnwidth77_3 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths95" select="0.15625"/>
											<xsl:variable name="factor95">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths95 &gt; 0.00000 and $sumcolumnwidths95 &gt; $tablewidth95">
														<xsl:value-of select="$tablewidth95 div $sumcolumnwidths95"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns95" select="1"/>
											<xsl:variable name="defaultcolumnwidth95">
												<xsl:choose>
													<xsl:when test="$factor95 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns95 &gt; 0">
														<xsl:value-of select="($tablewidth95 - $sumcolumnwidths95) div $defaultcolumns95"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth95_0" select="0.15625 * $factor95"/>
											<xsl:variable name="columnwidth95_1" select="$defaultcolumnwidth95"/>
											<fo:table width="{$tablewidth95}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth95_0}in"/>
												<fo:table-column column-width="{$columnwidth95_1}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/loanInsType2 = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.15625in"  padding-left="0.05in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>assur�es par la SCHL</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.12500in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" >
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth96" select="$columnwidth77_4 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths96" select="0.15625"/>
											<xsl:variable name="factor96">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths96 &gt; 0.00000 and $sumcolumnwidths96 &gt; $tablewidth96">
														<xsl:value-of select="$tablewidth96 div $sumcolumnwidths96"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns96" select="1"/>
											<xsl:variable name="defaultcolumnwidth96">
												<xsl:choose>
													<xsl:when test="$factor96 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns96 &gt; 0">
														<xsl:value-of select="($tablewidth96 - $sumcolumnwidths96) div $defaultcolumns96"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth96_0" select="0.15625 * $factor96"/>
											<xsl:variable name="columnwidth96_1" select="$defaultcolumnwidth96"/>
											<fo:table width="{$tablewidth96}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth96_0}in"/>
												<fo:table-column column-width="{$columnwidth96_1}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell height="0.15625in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/loanInsType3 = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.15625in" padding-left="0.05in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>assur�es par GENWORTH</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.27083in" display-align="center" text-align="center" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in" border-left-style="solid" border-left-color="black" border-left-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
	<fo:inline >

<xsl:choose>
																<xsl:when test="//CommitmentLetter/budgetOpt = '1'">
																   <xsl:call-template name="CheckedCheckbox"/>
																</xsl:when>
																<xsl:otherwise>
																    <xsl:call-template name="UnCheckedCheckbox"/>
																</xsl:otherwise>
															</xsl:choose>
	</fo:inline ></fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.27083in" display-align="center" number-columns-spanned="4" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in"  padding-top="0.03000in" padding-left="0.03000in">
										<fo:block padding-top="3pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Option budget </xsl:text>
											</fo:inline>
											<fo:inline font-size="6pt" vertical-align="super">
												<xsl:text>7&#160; </xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text>(cocher si applicable)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>

<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>

<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>

<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>

<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>
						<fo:block>
						<fo:leader leader-pattern="space"/>
						</fo:block>


						<fo:block>
												<fo:inline>
													<xsl:text>_____________________</xsl:text>
												</fo:inline>
												</fo:block>
						<xsl:variable name="tablewidth98" select="$maxwidth * 1.00000"/>
											<xsl:variable name="sumcolumnwidths98" select="0.50000"/>
											<xsl:variable name="factor98">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths98 &gt; 0.00000 and $sumcolumnwidths98 &gt; $tablewidth98">
														<xsl:value-of select="$tablewidth98 div $sumcolumnwidths98"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns98" select="1"/>
											<xsl:variable name="defaultcolumnwidth98">
												<xsl:choose>
													<xsl:when test="$factor98 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns98 &gt; 0">
														<xsl:value-of select="($tablewidth98 - $sumcolumnwidths98) div $defaultcolumns98"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth98_0" select="0.50000 * $factor98"/>
											<xsl:variable name="columnwidth98_1" select="$defaultcolumnwidth98"/>
						<fo:table width="{$tablewidth98}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth98_0}in"/>
												<fo:table-column column-width="{$columnwidth98_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell display-align="before" height="0.13542in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>Note </xsl:text>
																</fo:inline>
																<fo:inline font-size="6pt" vertical-align="super">
																	<xsl:text>7</xsl:text>
																</fo:inline>
																<fo:inline font-size="8pt">
																	<xsl:text>:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.13542in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
																<fo:inline font-size="8pt">
																	<xsl:text>Si vous avez choisi l'Option budget, les versements r�guliers ne seront compos�s que de l'int�r�t couru, malgr� ce qui est indiqu� � la ligne 15.1 ou 16, du sommaire. Aucun remboursement en capital ne sera fait pendant la dur�e du terme. En exer�ant cette option, vous allongez la p�riode d'amortissement indiqu�e � la ligne 4 selon le nombre de mois o� vous l'exercez.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>


						<fo:table>
							<fo:table-column/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell display-align="before" height="9.5in">
											<xsl:variable name="tablewidth97" select="$maxwidth * 1.00000"/>
											<xsl:variable name="sumcolumnwidths97" select="0.000"/>
											<xsl:variable name="defaultcolumns97" select="1"/>
											<xsl:variable name="defaultcolumnwidth97">
												<xsl:choose>
													<xsl:when test="$defaultcolumns97 &gt; 0">
														<xsl:value-of select="($tablewidth97 - $sumcolumnwidths97) div $defaultcolumns97"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth97_0" select="$defaultcolumnwidth97"/>
											<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo_fr.jpeg" width="1.6in"/>
											</fo:inline>
										</fo:block>
											<fo:table width="{$tablewidth97}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth97_0}in"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-weight="bold">
																	<xsl:text>Conditions de financement</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<xsl:for-each select="//CommitmentLetter/docTrackText">
																<fo:block padding-top="1pt" padding-bottom="1pt"  text-align="justify">
																<fo:inline font-size="8pt">
																	<xsl:value-of select="."/>
																</fo:inline>
																</fo:block>
															</xsl:for-each>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<!--<fo:table-cell display-align="before">
											<fo:block>
												<fo:inline>
													<xsl:text>_____________________</xsl:text>
												</fo:inline>
										<xsl:variable name="tablewidth98" select="$maxwidth * 1.00000"/>
											<xsl:variable name="sumcolumnwidths98" select="0.50000"/>
											<xsl:variable name="factor98">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths98 &gt; 0.00000 and $sumcolumnwidths98 &gt; $tablewidth98">
														<xsl:value-of select="$tablewidth98 div $sumcolumnwidths98"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns98" select="1"/>
											<xsl:variable name="defaultcolumnwidth98">
												<xsl:choose>
													<xsl:when test="$factor98 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns98 &gt; 0">
														<xsl:value-of select="($tablewidth98 - $sumcolumnwidths98) div $defaultcolumns98"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth98_0" select="0.50000 * $factor98"/>
											<xsl:variable name="columnwidth98_1" select="$defaultcolumnwidth98"/>
											<fo:table width="{$tablewidth98}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth98_0}in"/>
												<fo:table-column column-width="{$columnwidth98_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell display-align="before" height="0.13542in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="8pt">
																	<xsl:text>Note </xsl:text>
																</fo:inline>
																<fo:inline font-size="6pt" vertical-align="super">
																	<xsl:text>7</xsl:text>
																</fo:inline>
																<fo:inline font-size="8pt">
																	<xsl:text>:</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.13542in">
															<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
																<fo:inline font-size="8pt">
																	<xsl:text>Si vous avez choisi l'Option budget, les versements r�guliers ne seront compos�s que de l'int�r�t couru, malgr� ce qui est indiqu� � la ligne 15.1 ou 16, du sommaire. Aucun remboursement en capital ne sera fait pendant la dur�e du terme. En exer�ant cette option, vous allongez la p�riode d'amortissement indiqu�e � la ligne 4 selon le nombre de mois o� vous l'exercez.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
											</fo:block>
										</fo:table-cell>-->
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						<fo:block break-after="page">
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth99" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths99" select="0.000"/>
						<xsl:variable name="defaultcolumns99" select="1"/>
						<xsl:variable name="defaultcolumnwidth99">
							<xsl:choose>
								<xsl:when test="$defaultcolumns99 &gt; 0">
									<xsl:value-of select="($tablewidth99 - $sumcolumnwidths99) div $defaultcolumns99"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth99_0" select="$defaultcolumnwidth99"/>
						<fo:table width="{$tablewidth99}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth99_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo_fr.jpeg" width="1.6in"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth100" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths100" select="0.000"/>
						<xsl:variable name="defaultcolumns100" select="1"/>
						<xsl:variable name="defaultcolumnwidth100">
							<xsl:choose>
								<xsl:when test="$defaultcolumns100 &gt; 0">
									<xsl:value-of select="($tablewidth100 - $sumcolumnwidths100) div $defaultcolumns100"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth100_0" select="$defaultcolumnwidth100"/>
						<fo:table width="{$tablewidth100}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth100_0}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell font-size="10pt" height="0.22917in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>Frais</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.31250in">
										<fo:block>
											<fo:leader leader-pattern="space"/>
										</fo:block>
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="10pt">
												<xsl:text>Vous vous engagez � payer � la Banque les frais d'administration p�riodiques indiqu�s � la ligne 10 du tableau ci-dessus. Les frais d'administration p�riodiques sont pay�s � la m�me fr�quence et aux m�mes dates que celles pr�vues pour les versements p�riodiques.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth101" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths101" select="7.65625"/>
						<xsl:variable name="factor101">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths101 &gt; 0.00000 and $sumcolumnwidths101 &gt; $tablewidth101">
									<xsl:value-of select="$tablewidth101 div $sumcolumnwidths101"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth101_0" select="7.65625 * $factor101"/>
						<fo:table width="{$tablewidth101}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth101_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell font-size="10pt" height="0.18750in">
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline font-size="10pt">
												<xsl:text>D'autres frais peuvent �tre payables par vous pour certains services selon le tarif alors en vigueur. Ils sont indiqu�s dans la D�claration du co�t d'emprunt et le Guide des Solutions Bancaires Personnelles dont vous recevrez copie.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth102" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths102" select="0.000"/>
						<xsl:variable name="defaultcolumns102" select="1"/>
						<xsl:variable name="defaultcolumnwidth102">
							<xsl:choose>
								<xsl:when test="$defaultcolumns102 &gt; 0">
									<xsl:value-of select="($tablewidth102 - $sumcolumnwidths102) div $defaultcolumns102"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth102_0" select="$defaultcolumnwidth102"/>
						<fo:table width="{$tablewidth102}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth102_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.21875in" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>Co�t de l'emprunt</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.18750in">
										<fo:block>
											<fo:leader leader-pattern="space"/>
										</fo:block>
										<fo:block padding-top="1pt" padding-bottom="1pt" text-align="justify">
											<fo:inline>
												<xsl:text>Avant ou lors de la signature du Pr�t, la Banque vous fournira une d�claration �crite vous informant du co�t de l'emprunt relativement au Pr�t.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth103" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths103" select="0.30208"/>
						<xsl:variable name="factor103">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths103 &gt; 0.00000 and $sumcolumnwidths103 &gt; $tablewidth103">
									<xsl:value-of select="$tablewidth103 div $sumcolumnwidths103"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns103" select="1"/>
						<xsl:variable name="defaultcolumnwidth103">
							<xsl:choose>
								<xsl:when test="$factor103 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns103 &gt; 0">
									<xsl:value-of select="($tablewidth103 - $sumcolumnwidths103) div $defaultcolumns103"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth103_0" select="0.30208 * $factor103"/>
						<xsl:variable name="columnwidth103_1" select="$defaultcolumnwidth103"/>
						<fo:table width="{$tablewidth103}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" font-size="10pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth103_0}in"/>
							<fo:table-column column-width="{$columnwidth103_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.10417in" number-columns-spanned="2" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>Conditions suppl�mentaires</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.27083in" number-columns-spanned="2">
										<fo:block>
											<fo:leader leader-pattern="space"/>
										</fo:block>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>La pr�sente Offre est assujettie aux conditions suppl�mentaires suivantes :</xsl:text>
											</fo:inline>
										</fo:block>
										<fo:block>
											<fo:leader leader-pattern="space"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>1.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Documentation.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> La pr�sente Offre est conditionnelle � la r�ception par la Banque de tous les documents requis, d�ment compl�t�s � la satisfaction de la Banque.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>2.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Offres ant�rieures.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> La pr�sente Offre remplace toute offre de financement hypoth�caire ant�rieure relative � votre demande financement hypoth�caire</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>3.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Annulation et force majeure.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> La pr�sente Offre peut �tre annul�e en tout temps par la Banque s'il y a un changement important � la situation financi�re que vous ou la Caution avez d�clar� � la Banque. La pr�sente Offre peut �galement �tre annul�e par la Banque s'il survient une modification importante � la Propri�t� affectant d�favorablement sa valeur financi�re. En cas de force majeure, la pr�sente Offre ne liera pas la Banque et celle-ci ne pourra �tre tenue responsable des dommages ou pertes encourus suite au d�faut de la Banque d'ex�cuter ses obligations pr�vues � la pr�sente Offre.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>4.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Titre de propri�t�.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> L'�tat du titre de propri�t� relativement � votre Propri�t� doit �tre � la satisfaction de la Banque.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>5.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Assurance incendies et autres risques.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> Vous devez fournir une police d'assurance contre les incendies et autres risques. Cette police doit couvrir la pleine valeur de remplacement des b�timents situ�s sur la Propri�t� et l'indemnit� doit �tre payable � la Banque � titre de cr�ancier hypoth�caire conform�ment � son rang, et renfermer les clauses usuelles de protection en faveur des cr�anciers hypoth�caires approuv�es par le Bureau d'assurance du Canada. La police, l'assureur et les clauses hypoth�caires usuelles doivent �tre � la satisfaction de la Banque.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before" >
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>6.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Assurance-titres ou plan d'arpentage / certificat de localisation.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> La Banque requiert une assurance-titres souscrite aupr�s d'une compagnie d'assurance-titres approuv�e par la Banque ou encore, i) plan d'arpentage pour l'Ontario et la Colombie-Britannique; ii) un certificat d'arpenteur / de localisation pour le Manitoba, le Nouveau-Brunswick, la Nouvelle-�cosse, l'�le du Prince-�douard, Terre-Neuve et le Qu�bec; ou iii) rapport de recherche sur immeuble pour l'Alberta et la Saskatchewan, pr�par� et certifi� par un arpenteur / arpenteur-g�om�tre qualifi�.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>7.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Pr�t assur�.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> Si le Pr�t est assur�, la pr�sente Offre est conditionnelle � l'approbation du Pr�t par l'assureur-pr�t.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>8.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Certificat d'irr�gularit�.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> Si la Propri�t� est une unit� de copropri�t�, l'association condominiale / le syndicat des copropri�taires doit �tre inform�(e) des droits de la Banque. Un plan du condominium ou un certificat d'irr�gularit� doit �tre fourni � la Banque (toutes les provinces sauf le Qu�bec).</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>9.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Frais et d�penses.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> Sauf stipulation contraire pr�vue dans la pr�sente Offre, vous �tes redevable de tous les frais et de toutes les d�penses relatifs au Pr�t, incluant notamment les frais d'�valuation, d'arpentage, d'assurance-titres, d'inspection ainsi que les frais juridiques, que des fonds aient �t� avanc�s ou non.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>10.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell >
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Avocat / notaire (Qu�bec).</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> En toutes circonstances, la Banque se r�serve le droit de vous demander de retenir les services d'un avocat / notaire (Qu�bec seulement) approuv� par la Banque, � vos frais. Pour la province de la Colombie-Britannique seulement, la Banque peut vous autoriser � avoir recours aux services d'un notaire public, � moins que le Pr�t ne comporte plus d'une tranche. Dans ce cas, tous les documents juridiques doivent �tre remplis par un avocat.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="before">
										<fo:block padding-top="1pt" padding-bottom="7pt">
											<fo:inline>
												<xsl:text>11.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="7pt" text-align="justify">
											<fo:inline text-decoration="underline">
												<xsl:text>Cession.</xsl:text>
											</fo:inline>
											<fo:inline>
												<xsl:text> Vous ne pouvez c�der la pr�sente Offre sans le consentement �crit pr�alable de la Banque.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block break-after="page"/>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth104" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths104" select="0.000"/>
						<xsl:variable name="defaultcolumns104" select="1"/>
						<xsl:variable name="defaultcolumnwidth104">
							<xsl:choose>
								<xsl:when test="$defaultcolumns104 &gt; 0">
									<xsl:value-of select="($tablewidth104 - $sumcolumnwidths104) div $defaultcolumns104"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth104_0" select="$defaultcolumnwidth104"/>
						<fo:table width="{$tablewidth104}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth104_0}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo_fr.jpeg" width="1.6in"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth105" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths105" select="0.705 + 0.322 + 0.003"/>
						<xsl:variable name="defaultcolumns105" select="1"/>
						<xsl:variable name="factor105">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths105 &gt; 0.00000 and $sumcolumnwidths105 &gt; $tablewidth105">
									<xsl:value-of select="$tablewidth105 div $sumcolumnwidths105"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumnwidth105">
							<xsl:choose>
								<xsl:when test="$defaultcolumns105 &gt; 0">
									<xsl:value-of select="($tablewidth105 - $sumcolumnwidths105) div $defaultcolumns105"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth105_0" select="0.705 * $factor105"/>
						<xsl:variable name="columnwidth105_1" select="0.322 * $factor105"/>
						<xsl:variable name="columnwidth105_2" select="0.003 * $factor105"/>
						<fo:table width="{$tablewidth105}in" border-collapse="separate" font-size="10pt" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth105_0}in"/>
							<fo:table-column column-width="{$columnwidth105_1}in"/>
							<fo:table-column column-width="{$columnwidth105_2}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.55208in" number-columns-spanned="3">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>Nous vous remercions d'avoir choisi la Banque Nationale pour votre solution de financement hypoth�caire.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell display-align="after" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Nous reconnaissons avoir pris connaissance de la pr�sente et l'avoir accept�e le</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding-top="2pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
										<fo:block padding-top="1pt" padding-bottom="1pt" />
									</fo:table-cell>
									<fo:table-cell text-align="right">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.11458in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.11458in" text-align="center">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="6pt">
												<xsl:text>Date</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell height="0.11458in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:variable name="tablewidth107" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths107" select="3.85417"/>
						<xsl:variable name="factor107">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths107 &gt; 0.00000 and $sumcolumnwidths107 &gt; $tablewidth107">
									<xsl:value-of select="$tablewidth107 div $sumcolumnwidths107"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="defaultcolumns107" select="1"/>
						<xsl:variable name="defaultcolumnwidth107">
							<xsl:choose>
								<xsl:when test="$factor107 &lt; 1.000">
									<xsl:value-of select="0.000"/>
								</xsl:when>
								<xsl:when test="$defaultcolumns107 &gt; 0">
									<xsl:value-of select="($tablewidth107 - $sumcolumnwidths107) div $defaultcolumns107"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth107_0" select="3.85417 * $factor107"/>
						<xsl:variable name="columnwidth107_1" select="$defaultcolumnwidth107"/>
						<fo:table width="{$tablewidth107}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth107_0}in"/>
							<fo:table-column column-width="{$columnwidth107_1}in"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell height="0.11458in" number-columns-spanned="2">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<fo:external-graphic  src="file:/{$image-dir}logo_fr2.jpeg" width="2.6in"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:table width="{$maxwidth}in">
						<fo:table-column column-width="proportional-column-width(60)"/>
						<fo:table-column column-width="proportional-column-width(5)"/>
						<fo:table-column column-width="proportional-column-width(35)"/>
						<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding-top="15pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
								<fo:block/>
							</fo:table-cell>
							<fo:table-cell/>
							<fo:table-cell padding-top="15pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="before">
								<fo:block>
									<fo:inline text-align="left" font-size="7pt">
										<xsl:text>(Signature de l'emprunteur)</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell/>
							<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="before">
								<fo:block>
									<fo:inline text-align="left" font-size="7pt">
										<xsl:text>Date</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding-top="15pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
								<fo:block/>
							</fo:table-cell>
							<fo:table-cell/>
							<fo:table-cell padding-top="15pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="before">
								<fo:block>
									<fo:inline text-align="left" font-size="7pt">
										<xsl:text>(Signature de l'emprunteur)</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell/>
							<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="before">
								<fo:block>
									<fo:inline text-align="left" font-size="7pt">
										<xsl:text>Date</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding-top="15pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
								<fo:block/>
							</fo:table-cell>
							<fo:table-cell/>
							<fo:table-cell padding-top="15pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="before">
								<fo:block>
									<fo:inline text-align="left" font-size="7pt">
										<xsl:text>(Signature de l'emprunteur)</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell/>
							<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="before">
								<fo:block>
									<fo:inline text-align="left" font-size="7pt">
										<xsl:text>Date</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding-top="15pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
								<fo:block/>
							</fo:table-cell>
							<fo:table-cell/>
							<fo:table-cell padding-top="15pt" padding-bottom="2pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.01042in">
								<fo:block/>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="before">
								<fo:block>
									<fo:inline text-align="left" font-size="7pt">
										<xsl:text>(Signature de l'emprunteur)</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell/>
							<fo:table-cell padding-top="1pt" padding-bottom="1pt" display-align="before">
								<fo:block>
									<fo:inline text-align="left" font-size="7pt">
										<xsl:text>Date</xsl:text>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						</fo:table-body>
						</fo:table>
					</fo:block>
					<fo:block id="{$page-sequenceId}" />
				</fo:flow>
			</fo:page-sequence>
	</xsl:template>

	<!-- ************************************************************* -->
	<!-- ****************** ***Checked Check Box ***************** -->
	<!-- ************************************************************* -->

	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="8px" height="8px">
			<svg xmlns="http://www.w3.org/2000/svg" width="8px" height="8px">
				<rect x="0" y="0" width="8" height="8" style="fill: none; stroke: black"/>
				<line x1="0" y1="0" x2="8" y2="8" style="stroke:black"/>
				<line x1="0" y1="8" x2="8" y2="0" style="stroke:black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<!-- ************************************************************* -->
	<!-- ****************** Unchecked Check Box****************** -->
	<!-- ************************************************************* -->
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="8px" height="8px">
			<svg xmlns="http://www.w3.org/2000/svg" width="8px" height="8px">
				<rect x="0" y="0" width="8" height="8" style="fill: none; stroke: black"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<xsl:variable name="maxwidth" select="7.52000"/>
		<xsl:variable name="tablewidth201" select="$maxwidth * 1.00000"/>
				<xsl:variable name="sumcolumnwidths201" select="0.04167 + 1.56250 + 0.04167"/>
				<xsl:variable name="factor201">
					<xsl:choose>
						<xsl:when test="$sumcolumnwidths201 &gt; 0.00000 and $sumcolumnwidths201 &gt; $tablewidth201">
							<xsl:value-of select="$tablewidth201 div $sumcolumnwidths201"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="1.000"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="defaultcolumns201" select="1"/>
				<xsl:variable name="defaultcolumnwidth201">
					<xsl:choose>
						<xsl:when test="$factor201 &lt; 1.000">
							<xsl:value-of select="0.000"/>
						</xsl:when>
						<xsl:when test="$defaultcolumns201 &gt; 0">
							<xsl:value-of select="($tablewidth201 - $sumcolumnwidths201) div $defaultcolumns201"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="0.000"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="columnwidth201_0" select="$defaultcolumnwidth201"/>
				<xsl:variable name="columnwidth201_1" select="1.56250 * $factor201"/>



	<!-- ************************************************************* -->
	<!-- *************************Footerall ****************************-->
	<!-- ************************************************************* -->
	<xsl:template name="footerall">
	  <xsl:param name="footerText" />
	  <xsl:param name="page-sequence" />
	  <xsl:variable name="tablewidth201" select="$maxwidth * 1.00000"/>
		<fo:static-content flow-name="xsl-region-after">
			<fo:block>
				<fo:table width="{$tablewidth201}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" border-separation="0.04167in" color="black" display-align="center">
					<fo:table-column column-width="{$columnwidth201_0}in"/>
					<fo:table-column column-width="{$columnwidth201_1}in"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell height="0.01042in" number-columns-spanned="2" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt"/>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="2" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:block space-before.optimum="-8pt">
										<fo:leader leader-length="97%" leader-pattern="rule" rule-thickness="1pt" color="black"/>
									</fo:block>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell font-size="6pt" text-align="left" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:inline font-size="6pt">
										<xsl:text>&#160;&#160;</xsl:text><xsl:value-of select="$footerText"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell font-size="6pt" text-align="center" padding-top="0.00000in" padding-bottom="0.00000in" padding-left="0.00000in" padding-right="0.00000in">
								<fo:block padding-top="1pt" padding-bottom="1pt">
									<fo:inline font-size="6pt" font-weight="normal">
										<xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>&#160;&#160;</xsl:text><xsl:text>Page&#160;</xsl:text>
									</fo:inline>
									<fo:page-number font-size="6pt"/>
									<fo:inline font-size="6pt" font-weight="normal">
										<xsl:text>&#160;of&#160;</xsl:text>
									</fo:inline>
									<fo:inline font-size="6pt" font-weight="normal">
										<fo:page-number-citation ref-id="{$page-sequence}" />
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>
</xsl:stylesheet>
