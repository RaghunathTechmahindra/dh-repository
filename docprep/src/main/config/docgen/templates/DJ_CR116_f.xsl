<?xml version="1.0" encoding="UTF-8"?>
<!--
06/Sep/2006 DVG #DG496 #4599  CR116 - incorrect logic for tick box  
21/Jul/2006 DVG #DG468 #3578  Words missing on geneated document  
03/May/2006 DVG #DG412 #3022  CR 311  funcional spec for the CR 311 (changes to document CR 053) 
06/Feb/2006 DVG #DG390 #2697  DJ-Incorrect MI PST in  Offre de financement hypotheque  
09/Mar/2005 DVG #DG166 #1063  DJ - miscellaneous DocPrep fixes  
22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik

 Author Zivko Radulovic -->
<!-- edited by Catherine Rutgaizer, March 2004 -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>

	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBA1.5Optimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainTextAttr_Page_2">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttrPg4">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="ListLabelIndent">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="padding-left">10mm</xsl:attribute>
		<xsl:attribute name="margin-left">1mm</xsl:attribute>
	</xsl:attribute-set>

	<xsl:template name="CreatePageFour">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					Page <fo:page-number/> de <fo:page-number-citation ref-id="page_4"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage4"/>
				<xsl:call-template name="Page_4_Item_1"/>
				<fo:block id="page_4" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLinePage4">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before">1px</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Annexe à l'Offre de financement - Hypothèque immobilière <xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_4_Item_1">
		<!-- ===================================================  -->
		<!-- START OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text> Nom du ou des emprunteurs :</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="space-after">2pt</xsl:attribute>
								<xsl:attribute name="space-before">2pt</xsl:attribute>
								<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
								<xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="BorrowersListedBlocks"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">12pt</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
			<xsl:attribute name="font-size">8pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<fo:inline font-weight="bold">CONDITIONS :</fo:inline>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- START OF TABLE   MI Policy numbers-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">7cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">7cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="mortgageInsuranceCheckBox">
								<xsl:with-param name="parInsurer">1</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Prêt assuré SCHL No. 
						<xsl:call-template name="mortgageInsurancePolicyNum">
								<xsl:with-param name="parInsurer">1</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="mortgageInsuranceCheckBox">
								<xsl:with-param name="parInsurer">2</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						 prêt assuré GE CAPITAL No.
							<xsl:call-template name="mortgageInsurancePolicyNum">
								<xsl:with-param name="parInsurer">2</xsl:with-param>
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE  MI Policy numbers -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Prime $-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">4cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">6.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Prime 
						<xsl:call-template name="mortgageInsurancePremiumAmount">
								<xsl:with-param name="parId">
									<xsl:value-of select="//Deal/mortgageInsurerId"/>
								</xsl:with-param>
								<xsl:with-param name="parStatus">
									<xsl:value-of select="//Deal/MIStatusId"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:element>
					<xsl:choose>
						<xsl:when test="(//Deal/mortgageInsurerId='1' or //Deal/mortgageInsurerId='2') and (//Deal/MIStatusId='15' or //Deal/MIStatusId='16' or //Deal/MIStatusId='23')">
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
									<xsl:choose>
										<xsl:when test="//Deal/MIUpfront= 'N'">
											<xsl:call-template name="CheckedCheckbox"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="UnCheckedCheckbox"/>
										</xsl:otherwise>
									</xsl:choose>
								  incluse au prêt
							</xsl:element>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
									<xsl:choose>
										<xsl:when test="//Deal/MIUpfront= 'Y'">
											<xsl:call-template name="CheckedCheckbox"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="UnCheckedCheckbox"/>
										</xsl:otherwise>
									</xsl:choose>		
									 non incluse au prêt  
							</xsl:element>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
									<xsl:call-template name="UnCheckedCheckbox"/>	  incluse au prêt
							</xsl:element>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
									<xsl:call-template name="UnCheckedCheckbox"/> 	 non incluse au prêt  
							</xsl:element>
							</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE Prime $-->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Taxe $-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Taxe 
						<xsl:if test="(//Deal/mortgageInsurerId='1' or //Deal/mortgageInsurerId='2') and (//Deal/MIStatusId='15' or //Deal/MIStatusId='16' or //Deal/MIStatusId='23')">
								<!--#DG390 xsl:value-of select="//Deal/MIPremiumPST"/-->
								<!--   	public static int FEE_TYPE_CMHC_PST_ON_PREMIUM = 13;  
										public static int FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM = 19;
									-->
								<xsl:for-each select="/*/specialRequirementTags/DealFee[feeId = 13 or feeId = 19]">
									<xsl:value-of select="feeAmount"/>
								</xsl:for-each>
									
							</xsl:if>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						 Payable
					</xsl:element>
					</xsl:element>
					<xsl:call-template name="Fees-13-or-19"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE  Taxe $-->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Droit de souscription-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Droit de souscription 
						<xsl:for-each select="//specialRequirementTags/DealFee">
								<xsl:if test="self::node()[feeId='3' or feeId='15']">
									<xsl:value-of select="./feeAmount"/>
								</xsl:if>
							</xsl:for-each>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="Fees-3-or-15"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE Droit de souscription -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE    Frais d evaluation  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<!-- #DG166 moved to template below
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/dealTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Frais d'évaluation 
						<xsl:value-of select="//specialRequirementTags/FraisDeval"/>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="Fees-0-or-22"/>
				</xsl:element-->
				<xsl:call-template name="Fees-0-or-22"/>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   La mise de fonds  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//specialRequirementTags/isDownpayment='Y'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox">
							</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
						  <!-- #DG412 -->
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						La mise de fonds devra être déposée à la 
            <xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyName"/>
             avant l'envoi des instructions au notaire.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Option multipro -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/multiProject = 'Y'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Option Multiprojets 
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   certificat de -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Certificat de localisation de moins de 10 ans indiquant l'état actuel de la propriété. Le certificat devra être complet et exact en tous points, l'immeuble n'ayant subi aucune modification, addition et/ou altération depuis son émission.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   nouveau certificat de -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId='1' or //DealAdditional/Property/newConstructionId='2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Nouveau certificat de localisation demandé (nouvelle construction). 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  date pr -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Date prévue du déboursé <xsl:value-of select="//Deal/estimatedClosingDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Clause de 1/12e des taxes  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
      <!--#DG468 removed warning-->
			<fo:table-column column-width="10mm"/>
			<fo:table-column column-width="60mm"/>
			<fo:table-column column-width="60mm"/>
			<fo:table-column column-width="40mm"/>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/escrowPayment/escrowTypeId='0' and //Deal/paymentFrequencyId='0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/escrowPayment/escrowTypeId='0' and //Deal/paymentFrequencyId='0'"> 
									Clause de 1/12e des taxes <xsl:value-of select="//Deal/escrowPayment/escrowPaymentAmount"/>
								</xsl:when>
								<xsl:otherwise>Clause de 1/12e des taxes $ </xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/escrowPayment/escrowTypeId='0' and (//Deal/paymentFrequencyId='4' or //Deal/paymentFrequencyId='5')">
									<xsl:call-template name="CheckedCheckbox"/>
									 Clause de 1/52e <xsl:value-of select="//Deal/escrowPayment/escrowPaymentAmount"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/> Clause de 1/52e $
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/escrowPayment/escrowTypeId='0' and (//Deal/paymentFrequencyId='2' or //Deal/paymentFrequencyId='3')">
									<xsl:call-template name="CheckedCheckbox"/>
									 1/26e <xsl:value-of select="//Deal/escrowPayment/escrowPaymentAmount"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/> 1/26e $
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  clauses usuelles  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/propertyTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/propertyTypeId = '14'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/propertyTypeId = '15'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/propertyTypeId = '16'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Clauses usuelles pour condominium. 
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  constructions  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
							  <xsl:when test="//Deal/progressAdvance= 'Y' or //DealAdditional/Property/newConstructionId='1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Construction: déboursé progressif, le 1er déboursé devra se faire au plus tard 90 jours après la date de l'acte de prêt hypothécaire et retenue de 15%. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  pour constructions  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '1'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:when test="//DealAdditional/Property/newConstructionId= '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Pour construction neuve: Le constructeur doit être accrédité auprès d'un programme de garantie de maisons neuves et la propriété doit être enregistrée à ce programme. 
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Rénovations  -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//specialRequirementTags/isRefiImprAmnt='Y'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Rénovations: Contrôle du déboursé et pour rénovations supérieures à 10 000$, le constructeur doit être accrédité auprès d'un programme de rénovations.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Obtenir -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
							  <!-- #DG496 -->
								<xsl:when test="/*/Deal/progressAdvance= 'Y' or /*/DealAdditional/Property/newConstructionId = 1 
                  or /*/DealAdditional/Property/newConstructionId = 2">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Obtenir renonciation des hypothèques légales ou consentement à priorité d'hypothèque, certificat de fin des travaux et retenue de 15 % jusqu'à 35 jours de la date de fin des travaux.
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE   Intervention   -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='1']">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Intervention à l'acte de prêt hypothécaire à titre de caution de 
							<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId='1']">
								<xsl:call-template name="BorrowerFullName"/>
								<xsl:if test="position() != last()">, </xsl:if>
							</xsl:for-each>
							<xsl:text>.</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Obtenir une analys-->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//DealAdditional/Property/waterTypeId='0'">
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Obtenir une analyse d'eau acceptable pour la caisse et le certificat de conformité de la municipalité pour le puits et la fosse septique si la propriété n'est pas reliée au réseau d'aqueduc. 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  À la demande -->
		<!-- ===================================================  
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							À la demande de l'emprunteur ou des emprunteurs, les remboursements se feront:
						</xsl:element></xsl:element>										
		</xsl:element></xsl:element></xsl:element>
		 ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  hebdomadairement  -->
		<!-- ===================================================  
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:text> </xsl:text>
						</xsl:element></xsl:element>										
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 hebdomadairement à raison de $  
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 incluant vie 
						</xsl:element>					
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 incluant vie-invalidité 
					</xsl:element>	</xsl:element>										
		</xsl:element></xsl:element></xsl:element>
		 ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  aux 2  -->
		<!-- ===================================================  
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text> </xsl:text>
					</xsl:element></xsl:element>										
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 aux 2 semaines à raison de $   
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 incluant vie 
						</xsl:element>					
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId = '1'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 ncluant vie-invalidité 
						</xsl:element>							
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>
		 ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  le premier se faisant -->
		<!-- ===================================================  
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">6.5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:text> </xsl:text>
						</xsl:element></xsl:element>										
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							 le premier se faisant  
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId= '2'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId= '3'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>									
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 7e jour suivant le déboursé du prêt 
						</xsl:element>					
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId= '4'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId= '5'">
									<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
								</xsl:when>										
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>		
							 14e jour suivant le déboursé du prêt 
						</xsl:element>							
			</xsl:element></xsl:element></xsl:element>
		</xsl:element>	
		 ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Le ou les emprunteurs  -->
		<!-- ===================================================  
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">15cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row"><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							Le ou les emprunteurs désirent un prêt RAP, S.V.P. communiquez avec ces derniers sur la réception du dossier. 
						</xsl:element>
					</xsl:element>										
				</xsl:element>
			</xsl:element>
		</xsl:element>
		 ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Commentaires  -->
		<!-- =================================================== 
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId = '1'">
										<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>		
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								Commentaires sur promotion 
							</xsl:element>
					</xsl:element>										
				</xsl:element>
			</xsl:element>
		</xsl:element>
		 ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
		<!-- ===================================================  -->
		<!-- START OF TABLE  Autres -->
		<!-- ===================================================  -->
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">15cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/DocumentTracking[documentResponsibilityRoleId='60']">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						Autres conditions
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:call-template name="ListConditions"/>
		<!-- ===================================================  -->
		<!-- END OF TABLE -->
		<!-- ===================================================  -->
	</xsl:template>

	<!-- #DG468 replaced with a new one 
  =================================== CONDITIONS ============================================== -->
	<!-- PVCS # 611, new condition for conditions - ->
	<xsl:template name="ListConditions">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:copy use-attribute-sets="MainFontAttrPg4"/>
			<xsl:element name="fo:list-block">
				<xsl:attribute name="space-before.optimum">3mm</xsl:attribute>
        <!- -#DG412 xsl:for-each select="//Deal/DocumentTracking[Condition/conditionTypeId='2']"- ->
				<xsl:for-each select="/*/Deal/DocumentTracking
        [documentStatusId != 5 and Condition/conditionTypeId=2]">
					<xsl:element name="fo:list-item">
						<xsl:attribute name="space-before.optimum">2mm</xsl:attribute>
						<xsl:element name="fo:list-item-label">
							<xsl:element name="fo:block" use-attribute-sets="ListLabelIndent"> &#x2022;</xsl:element>
						</xsl:element>
						<xsl:element name="fo:list-item-body">
							<xsl:attribute name="start-indent">2cm</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="text-align">justify</xsl:attribute>
			          <xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:value-of select="./text"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template-->

	<!-- =================================== CONDITIONS ============================================== -->
	<!-- #DG468 rewritten with table -->
	<xsl:template name="ListConditions">
		<fo:table xsl:use-attribute-sets="TableLeftFixed">
			<fo:table-column column-width="12mm"/>
			<fo:table-column column-width="163mm"/>
			<fo:table-body xsl:use-attribute-sets="MainFontAttrPg4" space-before.optimum="3mm">
				<xsl:for-each select="/*/Deal/DocumentTracking
          [documentStatusId != 5 and Condition/conditionTypeId=2]">
 					<fo:table-row keep-together="always">
  					<fo:table-cell>
  							<fo:block text-align="right"> &#x2022;</fo:block>
  					</fo:table-cell>
  					<fo:table-cell>
							<fo:block text-align="justify" start-indent="8mm">
								<xsl:value-of select="./text"/>
							</fo:block>
  					</fo:table-cell>
 					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<!-- =================================== FEES ============================================== -->
	<xsl:template name="Fees-0-or-22">
		<xsl:choose>
			<xsl:when test="//specialRequirementTags/DealFee[feeId='0' or feeId='22']">
			
				<!--#DG166  -->
				<xsl:for-each select="//specialRequirementTags/DealFee[feeId='0' or feeId='22']"> 			
				<xsl:element name="fo:table-row">				
					<xsl:element name="fo:table-cell">
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<xsl:choose>
								<xsl:when test="//Deal/dealTypeId = '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
 						</fo:block >
					</xsl:element>
				
					<xsl:element name="fo:table-cell">
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							Frais d'évaluation <xsl:value-of select="feeAmount"/> 
 						</fo:block >
					</xsl:element>
					<!--#DG166  end -->
					
					<xsl:element name="fo:table-cell">
						<!-- #DG166 	moved up to allow header
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:for-each select="//specialRequirementTags/DealFee[feeId='0' or feeId='22']"-->						
						
	 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->								
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '1'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 chèque ci-joint  
							</fo:block><!--#DG140 -->					
	 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '4'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 au compte 
							</fo:block><!--#DG140 -->					
 							<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '6'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 par la caisse  
							</fo:block><!--#DG140 -->					
						<!--/xsl:element-->
					</xsl:element>
				</xsl:element>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-row">				
					<!--#DG166  -->
					<xsl:element name="fo:table-cell">
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<xsl:call-template name="UnCheckedCheckbox"/>
 						</fo:block >
					</xsl:element>									
					<xsl:element name="fo:table-cell">
 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							Frais d'évaluation <xsl:value-of select="feeAmount"/> 
 						</fo:block >
					</xsl:element>
					<!--#DG166  end -->
				
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="UnCheckedCheckbox"/> chèque ci-joint  
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="UnCheckedCheckbox"/> au compte 
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:call-template name="UnCheckedCheckbox"/> par la caisse 
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Fees-3-or-15">
		<xsl:choose>
			<xsl:when test="//specialRequirementTags/DealFee[feeId='3' or feeId='15']">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:for-each select="//specialRequirementTags/DealFee[feeId='3' or feeId='15']">
 							<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '1'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 chèque ci-joint  
							</fo:block><!--#DG140 -->					
 							<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '4'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 au compte 
							</fo:block><!--#DG140 -->					
	 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '5'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 par le contracteur  
							</fo:block><!--#DG140 -->					
	 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '6'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 par la caisse  
							</fo:block><!--#DG140 -->					
						</xsl:for-each>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/> chèque ci-joint  
					</xsl:element>
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/> au compte 
					</xsl:element>
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/> par le contracteur  
					</xsl:element>
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/> par la caisse 
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Fees-13-or-19">
		<xsl:choose>
			<xsl:when test="//specialRequirementTags/DealFee[feeId='13' or feeId='19']">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:for-each select="//specialRequirementTags/DealFee[feeId='13' or feeId='19']">
	 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '1'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 chèque ci-joint  
							</fo:block><!--#DG140 -->					
	 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '4'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 au compte 
							</fo:block><!--#DG140 -->					
	 						<fo:block xsl:use-attribute-sets="MainTextAttr_Page_2">
							<!--#DG140 xsl:element name="fo:block">
								<would not work like this !! xsl:copy use-attribute-sets="MainTextAttr_Page_2"/-->
								<xsl:choose>
									<xsl:when test="./feePaymentMethodId = '5'">
										<xsl:call-template name="CheckedCheckbox"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UnCheckedCheckbox"/>
									</xsl:otherwise>
								</xsl:choose>		
								 par le contracteur  
							</fo:block><!--#DG140 -->					
						</xsl:for-each>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/> chèque ci-joint  
					</xsl:element>
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/> au compte 
					</xsl:element>
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/> par le contracteur  
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageFour"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="mortgageInsuranceCheckBox">
		<xsl:param name="parInsurer"/>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="UnCheckedCheckbox"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="mortgageInsurancePolicyNum">
		<xsl:param name="parInsurer"/>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ConstructPage2Item1aBox">
		<xsl:param name="pRate"/>
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='0'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<!-- ======================================= -->
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ConstructPage2Item1aText">
		<xsl:param name="pRate"/>
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='0'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text>à taux fixe : au taux de </xsl:text>
						<fo:inline font-weight="bold">
							<xsl:value-of select="//Deal/netInterestRate"/>
						</fo:inline>
						<xsl:text>  l'an, calculé semestriellement et non à l'avance;</xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text>à taux fixe: au taux de l'an, calculé semestriellement et non à l'avance;</xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="mortgageInsurancePremiumAmount">
		<xsl:param name="parInsurer">99</xsl:param>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$parInsurer='99'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ConstructPage2Item1bBox">
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='2'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<!-- ======================================= -->
			<xsl:when test="//Deal/MtgProd/interestTypeId='3'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<!-- ======================================= -->
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="ConstructPage2Item1bText">
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='2'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text>à taux variable : au taux préférentiel de la Caisse centrale Desjardins majoré de </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/discount"/>
								</fo:inline>
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/premium"/>
								</fo:inline>
							</xsl:when>
						</xsl:choose>
						<xsl:text> l'an. Le taux applicable au prêt variera à chaque modification du dit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text>à taux variable : au taux préférentiel de la Caisse centrale Desjardins majoré de  </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/discount"/>
								</fo:inline>
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/premium"/>
								</fo:inline>
							</xsl:when>
						</xsl:choose>
						<xsl:text> l'an. Le taux applicable au prêt variera à chaque modification dudit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">12pt</xsl:attribute>
						<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text>  à taux variable: au taux préférentiel de la Caisse centrale Desjardins majoré de   l'an. Le taux applicable au prêt variera à chaque modification du dit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:if test="./borrowerTypeId = '0'">
				<xsl:element name="fo:block">
					<xsl:call-template name="BorrowerFullName"/>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>
	<xsl:template name="BorrAddress">
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/city,' ',./Address/province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/postalFSA,'  ', ./Address/postalLDU)"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="PrimBorrAddress">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:if test="position()=1">
				<xsl:for-each select="BorrowerAddress">
					<xsl:if test="borrowerAddressTypeId='0'">
						<xsl:call-template name="BorrAddress"/>
						<!-- DG#140 text>some text</text-->
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
