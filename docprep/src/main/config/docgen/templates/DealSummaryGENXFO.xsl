<!-- edited with XML Spy v4.3 U (http://www.xmlspy.com) by Brad Hadfield (Basis100 Inc) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
		<xsl:choose>
			<xsl:when test="//LanguageEnglish">
				<xsl:call-template name="EnglishPage1"/>
				<xsl:if test="//CreditBureauReport">
					<xsl:call-template name="EnglishCreditBureauSection"/>
				</xsl:if>
				<xsl:if test="//BusinessRules">
					<xsl:call-template name="EnglishBusinessRulesSection"/>
				</xsl:if>				
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//LanguageFrench">
				<!--				<xsl:call-template name="FrenchTemplate"/>-->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="FOEnd"/>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<!--  This header isn't actually used right now, but it is here just in case clients want the title to extend to all pages -->
	<xsl:template name="EnglishHeader">
		<xsl:text>&lt;fo:static-content flow-name="xsl-region-before"&gt;
		<!-- <xsl:element name="fo:static-content"><xsl:attribute name="flow-name">xsl-region-before</xsl:attribute></xsl:element> -->
			&lt;fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"&gt;
				&lt;fo:table text-align="left" table-layout="fixed"&gt;
					&lt;fo:table-column column-width="12.75cm"/&gt;
					&lt;fo:table-column column-width="12.55cm"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="14.0pt" font-weight="bold"&gt;Deal Summary&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell display-align="after"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="end"&gt;
									</xsl:text><xsl:if test="//ReferenceNum"><xsl:text>Servicing Mortgage Number: </xsl:text><xsl:value-of select="//ReferenceNum"/></xsl:if><xsl:text>
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row height="1pt"&gt;
							&lt;fo:table-cell number-columns-spanned="2"&gt;
								&lt;fo:block&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
					&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
			&lt;/fo:block&gt;
			&lt;fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"&gt;
				</xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>
			&lt;/fo:block&gt;
		&lt;/fo:static-content&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishFooter">
		<xsl:text>
			&lt;fo:static-content flow-name="footer"&gt;
				&lt;fo:block background-color="white" color="black" font-family="Arial, Helvetica" font-size="12.0pt" font-style="normal" font-weight="normal" margin-right="18.0pt" text-align="end" text-decoration="none" vertical-align="baseline"&gt;Page &lt;fo:page-number/&gt; of &lt;fo:page-number-citation ref-id="endofdoc"/&gt;&lt;/fo:block&gt; 
			&lt;/fo:static-content&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishPage1">
		<xsl:text>&lt;fo:page-sequence master-reference="main" language="en"&gt;</xsl:text>
<!--		<xsl:call-template name="EnglishHeader"/>-->
		<xsl:call-template name="EnglishFooter"/>
		<xsl:text>		&lt;fo:flow flow-name="region-body"&gt;
		<!--  Uncomment out the static-content stuff to have this appear as each page's header -->
			<!--		&lt;fo:static-content flow-name="xsl-region-before"&gt;-->
			&lt;fo:block line-height="12pt" space-before.optimum="1.5pt" space-after.optimum="1.5pt" keep-together="always"&gt;
				&lt;fo:table text-align="left" table-layout="fixed"&gt;
					&lt;fo:table-column column-width="12.75cm"/&gt;
					&lt;fo:table-column column-width="12.55cm"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="14.0pt" font-weight="bold"&gt;Deal Summary&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell display-align="after"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold" text-align="end"&gt;</xsl:text>
<xsl:if test="//ReferenceNum">
<xsl:text>Servicing Mortgage Number: </xsl:text><xsl:value-of select="//ReferenceNum"/>
</xsl:if>								
								<xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row height="1pt"&gt;
							&lt;fo:table-cell number-columns-spanned="2"&gt;
								&lt;fo:block&gt;
									&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black"/&gt;
								&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
					&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
			&lt;/fo:block&gt;
			<!--		&lt;/fo:static-content&gt;-->
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				</xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>
			&lt;/fo:block&gt;
			&lt;fo:block&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="144.0pt"/&gt;
				&lt;fo:table-column column-width="103.35pt"/&gt;
				&lt;fo:table-column column-width="75.85pt"/&gt;
				&lt;fo:table-column column-width="94.25pt"/&gt;
				&lt;fo:table-column column-width="86.85pt"/&gt;
				&lt;fo:table-column column-width="97.55pt"/&gt;
				&lt;fo:table-column column-width="91.15pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Summary Snapshot&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Deal #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Borrower Name&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Deal Status&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Deal Status Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Source Firm&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Source&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;LOB&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DealNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//BorrowerName"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DealStatus"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DealStatusDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SourceFirm"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Source"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//LOB"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Deal Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Deal Purpose&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Tot Ln Amt&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Purchase Price&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Payment Term&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Est. Closing Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Special Feature&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DealType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DealPurpose"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//TotalLoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//PurchasePrice"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//ActualPaymentTerm"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SpecialFeature"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;</xsl:text>

	<xsl:for-each select="//ApplicantDetails/Applicant">
			<xsl:text>&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="144.0pt"/&gt;
				&lt;fo:table-column column-width="133.7pt"/&gt;
				&lt;fo:table-column column-width="136.6pt"/&gt;
				&lt;fo:table-column column-width="136.7pt"/&gt;
				&lt;fo:table-column column-width="142.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;</xsl:text><xsl:if test="./PrimaryBorrower='Y'"><xsl:text>Primary </xsl:text></xsl:if><xsl:text>Applicant Details&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Name&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Net Worth&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Name"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Type"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./NetWorth"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Date of Birth&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Marital Status&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;SIN&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Citizenship Status&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;# of Dependents&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./DateOfBirth"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./MaritalStatus"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./SIN"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./CitizenshipStatus"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./NumDependants"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Existing Client?&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Reference Client #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;# of Times Bankrupt&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Bankruptcy Status&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Staff of Lender?&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./ExistingClient"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./ClientReferenceNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./TimesBankrupt"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./BankruptcyStatus"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./StaffOfLender"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Home Phone #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Work Phone #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Fax #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;E-mail Address&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Language Preference&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./HomePhone"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./WorkPhone"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./FaxPhone"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EmailAddress"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LanguagePreference"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
			
<xsl:if test="./Addresses">
			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="144.0pt"/&gt;
				&lt;fo:table-column column-width="135.0pt"/&gt;
				&lt;fo:table-column column-width="207.0pt"/&gt;
				&lt;fo:table-column column-width="207.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Address Details&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
					
	<xsl:for-each select="./Addresses/Address">
			<xsl:text>&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="./Status"/><xsl:text> Address&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Time at Residence&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Residential Status&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row</xsl:text><xsl:if test="not(position()=last())"><xsl:text> keep-with-next="always"</xsl:text></xsl:if><xsl:text>&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Line1"/><xsl:text>, 
</xsl:text><xsl:value-of select="./Line2"/>
<xsl:if test="./Line2">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./City"/>
<xsl:if test="./City and ./Province">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./Province"/><xsl:text>, 
</xsl:text><xsl:value-of select="./PostalCode"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./TimeAtResidence"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./ResidentialStatusID"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
	</xsl:for-each>
	<xsl:text>&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
</xsl:if>			

<xsl:for-each select="./EmploymentHistory/Employment">
			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="144.9pt"/&gt;
				&lt;fo:table-column column-width="22.5pt"/&gt;
				&lt;fo:table-column column-width="111.15pt"/&gt;
				&lt;fo:table-column column-width="109.35pt"/&gt;
				&lt;fo:table-column column-width="99.0pt"/&gt;
				&lt;fo:table-column column-width="112.5pt"/&gt;
				&lt;fo:table-column column-width="93.6pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;</xsl:text><xsl:value-of select="./EmploymentStatus"/><xsl:text> Employment&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Employer Name&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Employment Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EmployerName"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EmploymentType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Employer Phone #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Employer Fax #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Employer E-mail Address&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EmployerWorkPhone"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EmployerFax"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EmployerEmailAddress"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Employer Mailing Address&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Industry Sector&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Occupation&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Job Title&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Time at Job&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EmployerMailingAddressLine1"/><xsl:text>, </xsl:text>
<xsl:value-of select="./EmployerMailingAddressLine2"/>
<xsl:if test="./EmployerMailingAddressLine2">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./EmployerCity"/>
<xsl:if test="./EmployerCity and ./EmployerProvince">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./EmployerProvince"/>
<xsl:text>, </xsl:text><xsl:value-of select="./EmployerPostalCode"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./IndustrySector"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Occupation"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./JobTitle"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./TimeAtJob"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Description&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Period&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Amount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% Inc in GDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% Inc in TDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./IncomeType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./IncomeDescription"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./IncomePeriod"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./IncomeAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInTDS "/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
</xsl:for-each>

<xsl:if test="./EmploymentHistory/OtherIncome">
			<xsl:text>&lt;fo:table table-layout="fixed" keep-together="always"&gt;
				&lt;fo:table-column column-width="116.1pt"/&gt;
				&lt;fo:table-column column-width="114.6pt"/&gt;
				&lt;fo:table-column column-width="115.35pt"/&gt;
				&lt;fo:table-column column-width="115.35pt"/&gt;
				&lt;fo:table-column column-width="115.35pt"/&gt;
				&lt;fo:table-column column-width="115.35pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Other Income&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Description&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Period&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Amount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% Included in GDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% Included in TDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
					
<xsl:for-each select="./EmploymentHistory/OtherIncome/Line">
			<xsl:text>&lt;fo:table-row</xsl:text><xsl:if test="not(position()=last())"><xsl:text> keep-with-next="always"</xsl:text></xsl:if><xsl:text>&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./IncomeType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./IncomeDescription"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./IncomePeriod"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./IncomeAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInTDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
			</xsl:for-each>
			
			<xsl:text>&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
</xsl:if>			
		<xsl:if test="./Assets">
			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="143.1pt"/&gt;
				&lt;fo:table-column column-width="220.05pt"/&gt;
				&lt;fo:table-column column-width="166.95pt"/&gt;
				&lt;fo:table-column column-width="162.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Asset Details&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Description&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Value&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% Included in Net Worth&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
<xsl:for-each select="./Assets/Asset">					
					<xsl:text>&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./AssetType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;92 </xsl:text><xsl:value-of select="./AssetDescription"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./AssetValue"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInNetWorth"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
</xsl:for-each>
					<xsl:text>&lt;fo:table-row&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold" text-align="end"&gt;Total:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="white" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;</xsl:text><xsl:value-of select="./Assets/Total"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;
								&lt;fo:leader line-height="12pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
</xsl:if>
	<xsl:if test="./Liabilities">
			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="107.1pt"/&gt;
				&lt;fo:table-column column-width="216.35pt"/&gt;
				&lt;fo:table-column column-width="90.0pt"/&gt;
				&lt;fo:table-column column-width="72.0pt"/&gt;
				&lt;fo:table-column column-width="63.0pt"/&gt;
				&lt;fo:table-column column-width="63.0pt"/&gt;
				&lt;fo:table-column column-width="81.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Liability Details&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Description&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Amount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Monthly&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% in GDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% in TDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Payoff&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
<xsl:for-each select="./Liabilities/Liability">					
					<xsl:text>&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LiabilityType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LiabilityDescription"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LiabilityAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LiabilityMonthlyPayment"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInTDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LiabilityPayoffType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
</xsl:for-each>					
					<xsl:text>&lt;fo:table-row&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold" text-align="end"&gt;Totals:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="white" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;</xsl:text><xsl:value-of select="./Liabilities/TotalAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="white" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;</xsl:text><xsl:value-of select="./Liabilities/TotalMonthly"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;
								&lt;fo:leader line-height="12pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;
			</xsl:text>
			</xsl:if>
			</xsl:for-each>

	<xsl:for-each select="//Properties/Property">
			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="121.5pt"/&gt;
				&lt;fo:table-column column-width="17.0pt"/&gt;
				&lt;fo:table-column column-width="93.4pt"/&gt;
				&lt;fo:table-column column-width="10.2pt"/&gt;
				&lt;fo:table-column column-width="34.9pt"/&gt;
				&lt;fo:table-column column-width="73.1pt"/&gt;
				&lt;fo:table-column column-width="65.4pt"/&gt;
				&lt;fo:table-column column-width="49.35pt"/&gt;
				&lt;fo:table-column column-width="59.0pt"/&gt;
				&lt;fo:table-column column-width="30.15pt"/&gt;
				&lt;fo:table-column column-width="25.6pt"/&gt;
				&lt;fo:table-column column-width="0.45pt"/&gt;
				&lt;fo:table-column column-width="112.5pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Property Details&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Primary Property?&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="12" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Property Address&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PrimaryProperty"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="12" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text>
<xsl:value-of select="./AddressLine1"/><xsl:text>, </xsl:text>
<xsl:value-of select="./AddressLine2"/>
<xsl:if test="./AddressLine2">
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="./City"/><xsl:text>, </xsl:text>
<xsl:value-of select="./Province"/><xsl:text>, </xsl:text>
<xsl:value-of select="./PostalCode"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Legal Description&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Dwelling Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Dwelling Style&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;

							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt"&gt;</xsl:text><xsl:value-of select="./LegalLine1"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
						<xsl:if test="./LegalLine2">							
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./LegalLine2"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
						</xsl:if>
						<xsl:if test="./LegalLine3">							
							<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./LegalLine3"/><xsl:text>&lt;/fo:block&gt;</xsl:text>
						</xsl:if>
						
						<xsl:text>&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./DwellingType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./DwellingStyle"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="8" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Builder Name&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Garage Size&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Garage Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="8" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./BuilderName"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./GarageSize"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./GarageType"/><xsl:text>	&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;New Construction?&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Structure Age&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;# of Units&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;# of Bedrooms&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Living Space&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Lot Size&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./NewConstruction"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./StructureAge"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./NumUnits"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./NumBedrooms"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LivingSpace"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LotSize"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Heat Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;UFFI Insulation&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Water&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Sewage&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;MLS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./HeatingType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./UFFIInsulation"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Water"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Sewage"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./MLS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Property Usage&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Occupancy&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Property Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="6" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Property Location&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Zoning&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PropertyUsage"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./OccupancyType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PropertyType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="6" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PropertyLocation"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Zoning"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Purchase Price&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Land Value&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Equity Available&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Estimated Value&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Actual Appraised Value&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PurchasePrice"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./LandValue"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EquityAvailable"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./EstimatedValue"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./ActualAppraisalValue"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Appraisal Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="12" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Appraisal Source&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./AppraisalDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="12" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./AppraisalSource"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Appraiser&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Appraiser Address&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Appraiser Phone #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Appraiser Fax #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row</xsl:text><xsl:if test="./PropertyExpenses"><xsl:text> keep-with-next="always"</xsl:text></xsl:if><xsl:text>&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./AppraiserName"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./AppraiserAddress"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./AppraiserPhone"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./AppraiserFax"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
<xsl:if test="./PropertyExpenses">
					<xsl:text>&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="13" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Expenses&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Period&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Amount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% Inc in GDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;% Inc in TDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
		<xsl:for-each select="./PropertyExpenses/Expense">
			<xsl:text>&lt;fo:table-row</xsl:text><xsl:if test="not(position()=last())"><xsl:text> keep-with-next="always"</xsl:text></xsl:if><xsl:text>&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./ExpenseType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./ExpensePeriod"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./ExpenseAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInGDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./PercentIncludedInTDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
		</xsl:for-each>
	</xsl:if>
	<xsl:text>&lt;/fo:table-body&gt;
	&lt;/fo:table&gt;
	&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;
		&lt;fo:leader line-height="12pt"/&gt;
	&lt;/fo:block&gt;</xsl:text>
</xsl:for-each>

			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="342.35pt"/&gt;
				&lt;fo:table-column column-width="351.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Terms&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Total Purchase Price&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Charge&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//TotalPurchasePrice"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Charge"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Down Payment&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Lender&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//RequiredDownPaymentAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Lender"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;LTV&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Product&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//LTV"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Product"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Loan&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Posted Interest Rate&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//NetLoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//PostedInterestRate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;MI Premium&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Discount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Premium"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Discount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Total Loan&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Premium&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//TotalLoanAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Premium"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Amortization Period&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Buydown&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Amortization"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//BuydownRate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Effective Amortization Period&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Net Rate&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//EffectiveAmortization"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//NetRate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Payment Term Description&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;P and I&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//PaymentTerm"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//PandIPayment"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Actual Payment Term&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Additional P and I&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//ActualPaymentTerm"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//AdditionalPrincipalPayment"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Payment Frequency&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Total Escrow&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//PaymentFrequency"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//TotalEscrowPayment"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Prepayment Penalty&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Total Payment&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;</xsl:text>
						
							<xsl:for-each select="//Prepayment/Line">
								<xsl:choose>
								<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="* | text()"><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="6.0pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text>	
										</xsl:when>
									<xsl:otherwise><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="6.0pt"&gt;&lt;/fo:block&gt;</xsl:text>										</xsl:otherwise>
								</xsl:choose>				
							</xsl:for-each>
						
						<xsl:text>
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//TotalPayment"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Privilege Payment Option&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Holdback Amount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//PrivilegePaymentOption"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//AdvanceHold"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="108.0pt"/&gt;
				&lt;fo:table-column column-width="30.6pt"/&gt;
				&lt;fo:table-column column-width="77.4pt"/&gt;
				&lt;fo:table-column column-width="61.2pt"/&gt;
				&lt;fo:table-column column-width="138.6pt"/&gt;
				&lt;fo:table-column column-width="25.2pt"/&gt;
				&lt;fo:table-column column-width="113.4pt"/&gt;
				&lt;fo:table-column column-width="12.6pt"/&gt;
				&lt;fo:table-column column-width="126.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Other Details&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Credit Score&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Combined GDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Combined 3 Year GDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Combined TDS&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Combined 3 Year TD-&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//CreditScore"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//CombinedGDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Combined3YrGDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//CombinedTDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Combined3YrTDS"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Deal Purpose&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Branch&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DealPurpose"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//BranchName"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Deal Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Cross Sell&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DealType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//CrossSell"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Special Feature&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Reference Existing Mortgage Number&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					</xsl:text>
					<xsl:choose>
						<xsl:when test="//SpecialFeature and //RefExistingMtgNum">
							<xsl:text>
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SpecialFeature"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//RefExistingMtgNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
							</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica"&gt;
								&lt;fo:leader line-height="13pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica"&gt;
								&lt;fo:leader line-height="13pt"/&gt;
							&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
							</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Pre-App Est. Purchase Price&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Reference Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//PreAppEstPurchasePrice"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//ReferenceType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Repayment Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Reference Deal #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//RepaymentType"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//ReferenceNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Second Mortgage Exists?&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Estimated Closing Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SecondMortgage"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Financing Program&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;First Payment Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//FinancingProgram"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//FirstPaymentDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Tax Payor&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Maturity Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//TaxPayor"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MaturityDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Rate Locked In?&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Commitment Expected Return Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="4" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//RateLockedIn"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//CommitmentExpiryDate"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Source Name&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Source Firm&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Reference Source App #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="3"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Underwriter&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SourceName"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SourceFirm"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="3"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Underwriter"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="5" &gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Source Address&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Source Phone&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Source Fax&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="5" &gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SourceAddress"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
			
<!--  Equity Takeout  -->
			<xsl:if test="//Refi">
			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
					&lt;fo:table-column column-width="140.75pt"/&gt;
					&lt;fo:table-column column-width="126.0pt"/&gt;
					&lt;fo:table-column column-width="10.05pt"/&gt;
					&lt;fo:table-column column-width="138.4pt"/&gt;
					&lt;fo:table-column column-width="64.15pt"/&gt;
					&lt;fo:table-column column-width="74.25pt"/&gt;
					&lt;fo:table-column column-width="138.4pt"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row keep-with-next="always"&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Equity Take-Out&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row keep-with-next="always"&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Date Purchased&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Mortgage Holder&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Original Purchase Price&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Value of Improvements&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Blended Amortization&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row keep-with-next="always"&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/OriginalPurchaseDate"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/MortgageHolder"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/PurchasePrice"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/ImprovementValue"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/BlendedAmortization"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row keep-with-next="always"&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Purpose&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="3" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Original Mtg. Amount&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Outstanding Mtg. Amount&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row keep-with-next="always"&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/Purpose"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="3" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/OriginalMtgAmount"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/ExistingLoanAmount"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row keep-with-next="always"&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="7" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Improvements&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="7" padding-left="5.75pt" padding-right="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//Refi/ImproveDesc"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
					&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
				&lt;fo:block font-family="Arial, Helvetica"&gt;
					&lt;fo:leader line-height="12pt"/&gt;
				&lt;/fo:block&gt;</xsl:text>
</xsl:if>

			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="173.2pt"/&gt;
				&lt;fo:table-column column-width="173.25pt"/&gt;
				&lt;fo:table-column column-width="173.2pt"/&gt;
				&lt;fo:table-column column-width="173.25pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Progress Advance&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Progress Advance&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Advance to Date Amount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Next Advance Amount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Advance Number&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//ProgressAdvance"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//AdvanceToDateAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//NextAdvanceAmount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.75pt" padding-right="5.75pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//AdvanceNumber"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
	<xsl:if test="//MI">			
			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="130.5pt"/&gt;
				&lt;fo:table-column column-width="130.5pt"/&gt;
				&lt;fo:table-column column-width="130.5pt"/&gt;
				&lt;fo:table-column column-width="130.5pt"/&gt;
				&lt;fo:table-column column-width="171.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Mortgage Insurance Information&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Indicator&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Status&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Existing Policy #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Certificate #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Prequalification Certificate #&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Indicator"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Status"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/ExistingPolicyNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/CertificateNum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/PrequalificationMINum"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Type&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Insurer&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Payor&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Upfront&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Premium&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Type"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Insurer"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Payor"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Upfront"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-bottom-style="none" border-left-color="black" border-left-style="solid" border-left-width="0.5pt" border-right-color="black" border-right-style="solid" border-right-width="0.5pt" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/Premium"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;RU Intervention&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="5" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//MI/RUIntervention"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
				&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
	</xsl:if>

	<xsl:if test="//DownPayments">
			<xsl:text>
			&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="207.0pt"/&gt;
				&lt;fo:table-column column-width="342.0pt"/&gt;
				&lt;fo:table-column column-width="144.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Down Payments&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Source&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Description&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Amount&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
		<xsl:for-each select="//DownPayments/DownPayment">
			<xsl:text>&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Source"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Description"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Amount"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
		</xsl:for-each>
			<xsl:text>
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold" text-align="right"&gt;Down Payment Available:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DownPayments/Total"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt" number-columns-spanned="2"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold" text-align="right"&gt;Actual Down Payment:&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="//DownPayments/RequiredDownPayment"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
			&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
	</xsl:if>

<!--  FEES  -->
	<xsl:if test="//Fees">
		<xsl:text>&lt;fo:table table-layout="fixed"&gt;
					&lt;fo:table-column column-width="162.0pt"/&gt;
					&lt;fo:table-column column-width="153.0pt"/&gt;
					&lt;fo:table-column column-width="378.0pt"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row keep-with-next="always"&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Fees&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
						&lt;fo:table-row keep-with-next="always"&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Type&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Amount&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Status&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;</xsl:text>
						
			<xsl:for-each select="//Fees/Fee">
				<xsl:text>&lt;fo:table-row</xsl:text><xsl:if test="not(position()=last())"><xsl:text> keep-with-next="always"</xsl:text></xsl:if><xsl:text>&gt;
							&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Type"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Amount"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
							&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" space-before="2.0pt" space-after="2.0pt"&gt;</xsl:text><xsl:value-of select="./Status"/><xsl:text>&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;</xsl:text>
			</xsl:for-each>
												
					<xsl:text>&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
				&lt;fo:block font-family="Arial, Helvetica"&gt;
					&lt;fo:leader line-height="12pt"/&gt;
				&lt;/fo:block&gt;</xsl:text>
	</xsl:if>				
				
	<xsl:if test="//Conditions">
			<xsl:text>
					&lt;fo:table&gt;
					&lt;fo:table-column column-width="27.0pt"/&gt;
					&lt;fo:table-column column-width="135.0pt"/&gt;
					&lt;fo:table-column column-width="531.0pt"/&gt;
					&lt;fo:table-body&gt;
						&lt;fo:table-row&gt;
							&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" number-columns-spanned="2" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;
								&lt;fo:block font-family="Arial, Helvetica" font-weight="bold">Conditions&lt;/fo:block&gt;
							&lt;/fo:table-cell&gt;
						&lt;/fo:table-row&gt;
			</xsl:text>
			
			<xsl:for-each select="//Conditions/Condition">
				<xsl:text>
						&lt;fo:table-row keep-together="always"&gt;
				</xsl:text>

				<xsl:choose>
					<!--  Only 1 condition (both first and last row) -->
					<xsl:when test="position()=1 and position()=last()">
					<xsl:text>&lt;fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.5pt" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;</xsl:text>
					</xsl:when>

					<!--  First row -->
					<xsl:when test="position()=1">
					<xsl:text>&lt;fo:table-cell border-bottom-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.5pt" border-right-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;</xsl:text>
					</xsl:when>
					
					<!-- Last row -->
					<xsl:when test="position()=last()">
					<xsl:text>&lt;fo:table-cell border-left-style="solid" border-left-color="black" border-left-width="0.5pt" border-right-style="none" border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;</xsl:text>
					</xsl:when>
				
					<!-- Rows in between -->
					<xsl:otherwise>
					<xsl:text>&lt;fo:table-cell border-bottom-style="none" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.5pt" border-right-style="none" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;</xsl:text>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="position()"/><xsl:text>.&lt;/fo:block&gt;
				&lt;/fo:table-cell&gt;
				</xsl:text>

				<xsl:choose>
					<!--  Only 1 condition (both first and last row) -->
					<xsl:when test="position()=1 and position()=last()">
					<xsl:text>&lt;fo:table-cell border-right-style="solid" border-right-color="black" border-right-width="0.5pt" border-left-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" number-columns-spanned="2" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;</xsl:text>
					</xsl:when>

					<!--  First row -->
					<xsl:when test="position()=1">
					<xsl:text>&lt;fo:table-cell border-bottom-style="none" border-right-style="solid" border-right-color="black" border-right-width="0.5pt" border-left-style="none" border-top-color="black" border-top-style="solid" border-top-width="0.5pt" number-columns-spanned="2" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;</xsl:text>
					</xsl:when>
					
					<!-- Last row -->
					<xsl:when test="position()=last()">
					<xsl:text>&lt;fo:table-cell border-right-style="solid" border-right-color="black" border-right-width="0.5pt" border-left-style="none" border-bottom-color="black" border-bottom-style="solid" border-bottom-width="0.5pt" number-columns-spanned="2" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;</xsl:text>
					</xsl:when>
				
					<!-- Rows in between -->
					<xsl:otherwise>
					<xsl:text>&lt;fo:table-cell border-bottom-style="none" border-top-style="none" border-right-style="solid" border-right-color="black" border-right-width="0.5pt" border-left-style="none" number-columns-spanned="2" padding-bottom="5.75pt" padding-left="5.75pt" padding-right="5.75pt" padding-top="5.75pt"&gt;</xsl:text>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:for-each select="./Line">
					<xsl:choose>
					<!--  When there is a node with text in it (non-empty), then we display it normally.  
						However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
						<xsl:when test="* | text()"><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text>	
							</xsl:when>
						<xsl:otherwise><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9pt"&gt;&lt;fo:leader line-height="9pt"/&gt;&lt;/fo:block&gt;</xsl:text>							</xsl:otherwise>
					</xsl:choose>				
				</xsl:for-each>

				<xsl:text>&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
			</xsl:for-each>
			
			<xsl:text>
					&lt;/fo:table-body&gt;
					&lt;/fo:table&gt;
			&lt;fo:block font-family="Arial, Helvetica"&gt;
				&lt;fo:leader line-height="12pt"/&gt;
			&lt;/fo:block&gt;</xsl:text>
	</xsl:if>

	<xsl:if test="//Notes">
			<xsl:text>&lt;fo:table table-layout="fixed"&gt;
				&lt;fo:table-column column-width="162.0pt"/&gt;
				&lt;fo:table-column column-width="153.0pt"/&gt;
				&lt;fo:table-column column-width="378.0pt"/&gt;
				&lt;fo:table-body&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-weight="bold"&gt;Notes&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;
					&lt;fo:table-row keep-with-next="always"&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Date&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;User&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell background-color="#C0C0C0" border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="10.0pt" font-weight="bold"&gt;Text&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
		<xsl:for-each select="//Notes/Note">
		<xsl:text>&lt;fo:table-row</xsl:text><xsl:if test="not(position()=last())"><xsl:text> keep-with-next="always"</xsl:text></xsl:if><xsl:text>&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./Date"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="./User"/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-color="black" border-style="solid" border-width="0.5pt" padding-left="5.4pt" padding-right="5.4pt"&gt;</xsl:text>
							
							<xsl:for-each select="./Text/Line">
								<xsl:choose>
								<!--  When there is a node with text in it (non-empty), then we display it normally.  
									However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
									<xsl:when test="* | text()"><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9.0pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text></xsl:when>
									<xsl:otherwise><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="9pt"&gt;&lt;fo:leader line-height="9pt"/&gt;&lt;/fo:block&gt;</xsl:text></xsl:otherwise>
								</xsl:choose>							
							</xsl:for-each>

						<xsl:text>&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>
		</xsl:for-each>
				<xsl:text>&lt;/fo:table-body&gt;
			&lt;/fo:table&gt;</xsl:text>
	</xsl:if>
	<xsl:text>&lt;fo:block id="endofdoc"/&gt;
				&lt;/fo:flow&gt;
			&lt;/fo:page-sequence&gt;
		</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishCreditBureauSection">
	<xsl:text>&lt;fo:page-sequence master-reference="appendix" language="en"&gt;
				&lt;fo:flow flow-name="region-body"&gt;
			&lt;fo:block font-family="Arial, Helvetica" font-size="14pt" font-weight="bold"&gt;Credit Bureau Report:&lt;/fo:block&gt;</xsl:text>
			
			<xsl:for-each select="//CreditBureauReport/Line">
				<xsl:choose>
				<!--  When there is a node with text in it (non-empty), then we display it normally.  
					However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
					<xsl:when test="* | text()"><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="10pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text></xsl:when>
					<xsl:otherwise><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="10pt"&gt;&lt;fo:leader line-height="10pt"/&gt;&lt;/fo:block&gt;</xsl:text></xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			
			<xsl:text>&lt;/fo:flow&gt;
			&lt;/fo:page-sequence&gt;</xsl:text>
	</xsl:template>
		
	<xsl:template name="EnglishBusinessRulesSection">
	<xsl:text>&lt;fo:page-sequence master-reference="appendix" language="en"&gt;
				&lt;fo:flow flow-name="region-body"&gt;
				&lt;fo:block font-family="Arial, Helvetica" font-size="12.0pt" font-style="normal" font-weight="normal" margin-right="18.0pt" text-align="start" text-decoration="none" text-indent="0pt" vertical-align="baseline" white-space-collapse="false"&gt;
				&lt;fo:block font-weight="bold">Business Rules:&lt;/fo:block&gt;
				&lt;fo:table table-layout="fixed"&gt;
					&lt;fo:table-column column-width="14.4pt"/&gt;
					&lt;fo:table-column column-width="525.6pt"/&gt;
					&lt;fo:table-body&gt;</xsl:text>
		<xsl:for-each select="//BusinessRules/Rule">	
		<xsl:text>&lt;fo:table-row&gt;</xsl:text>

		<xsl:choose>
			<!--  we know this row requires an indicator, so that will require 2 cells -->
			<xsl:when test = "@Critical = 'Y' or @Critical = 'N'" >
			<xsl:text>&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
							&lt;fo:block font-size="10.0pt"&gt;</xsl:text>
					<xsl:choose>
						<xsl:when test="@Critical = 'Y'"><xsl:text>C</xsl:text></xsl:when>
						<xsl:otherwise><xsl:text>-</xsl:text></xsl:otherwise>
					</xsl:choose>
						<xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
						&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
					<xsl:text>&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
							<xsl:text>&lt;fo:block font-size="10.0pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;
						&lt;/fo:table-cell&gt;
					&lt;/fo:table-row&gt;</xsl:text>

		</xsl:for-each>
					<xsl:text>&lt;/fo:table-body&gt;
				&lt;/fo:table&gt;
				&lt;/fo:block&gt;
				&lt;/fo:flow&gt;
			&lt;/fo:page-sequence&gt;</xsl:text>
	</xsl:template>
								
	<xsl:template name="FOStart">
		<xsl:text>
		&lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;
		&lt;fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://xml.apache.org/fop/extensions"&gt;
			&lt;fo:layout-master-set&gt;
				&lt;fo:simple-page-master master-name="main" page-height="612.0pt" page-width="792.0pt"&gt;
					&lt;fo:region-body column-count="1" margin-bottom="36.0pt" margin-left="36.0pt" margin-right="36.0pt" margin-top="36.0pt" region-name="region-body"/&gt;
					&lt;fo:region-after extent="36.0pt" overflow="visible" padding-after="36pt" padding-left="36.0pt" padding-right="36.0pt" precedence="true" region-name="footer"/&gt;
				&lt;/fo:simple-page-master&gt;
				&lt;fo:simple-page-master master-name="appendix" page-height="792.0pt" page-width="612.0pt"&gt;
					&lt;fo:region-body column-count="1" margin-bottom="36.0pt" margin-left="36.0pt" margin-right="36.0pt" margin-top="36.0pt" region-name="region-body"/&gt;
					&lt;fo:region-after extent="36.0pt" overflow="visible" padding-after="36pt" padding-left="90.0pt" padding-right="90.0pt" precedence="true" region-name="footer"/&gt;
				&lt;/fo:simple-page-master&gt;
			&lt;/fo:layout-master-set&gt;
	</xsl:text>
	</xsl:template>
	<xsl:template name="FOEnd">
		<xsl:text>&lt;/fo:root&gt;</xsl:text>
	</xsl:template>
</xsl:stylesheet>
