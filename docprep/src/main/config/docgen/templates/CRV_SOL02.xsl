<!-- prepared by: Catherine Rugaizer -->
<!-- Legal size paper -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:call-template name="RTFFileStart2"/>
		<xsl:call-template name="DocumentHeader"/>
		<xsl:call-template name="Start"/>
		<xsl:call-template name="MortgageDetails"/>
		<xsl:call-template name="Advances"/>
		<xsl:call-template name="Other"/>
		<xsl:text>\page </xsl:text>
		<xsl:call-template name="Searches"/>
		<xsl:call-template name="RTFFileEnd2"/>
	</xsl:template>
	
	<!-- ************************************************************************ -->
	<!-- templates section                                                                                -->
	<!-- ************************************************************************ -->
	<!-- ================================================  -->
	<xsl:template name="DocumentHeader">
		<!-- ====== #727 by Catherine ====== -->
		<xsl:if test="//specialRequirementTags/isAmended = 'Y'">
			<xsl:text>{\f1\fs20 AMENDED SOLICITOR INSTRUCTIONS AS OF </xsl:text>
			<xsl:value-of select="//General/CurrentDate"/>
			<xsl:text>\par \par }</xsl:text>
		</xsl:if>
		<!-- ====== #727 by Catherine end ====== -->
		<xsl:text>{\pard\sa360\ql\f1\fs28\b\ul SOLICITOR\rquote S GUIDE 
\par}</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="Start">
		<xsl:text>{\f1\fs20 {\qj If you are satisfied that the Mortgagor(s) (also known as the \'93Borrower(s)\'94) has or will acquire a good and marketable title to the property described in our Commitment Letter, complete our requirements as set out below, and prepare a Mortgage as outlined herein.\par }}
\pard\plain \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }
{\pard \f1\fs20\sa180 \b\ul DOCUMENTATION: \par}
{\pard\f1\fs20 A mortgage form must be registered. Copies of schedules, if applicable, and standard charge terms can be obtained from our web site: {\b \ul http://www.cervus.com/lawyercentre.asp.}}
{\f1\fs20 \par \par }
</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="MortgageDetails">
		<xsl:text>
{\pard\f1\fs20\sa180\ql \b\ul MORTGAGE DETAILS:}
{\f1\fs20 \par \par }
</xsl:text>
		<xsl:text>{\pard \f1\fs20\sa180
The Mortgage Document is to be registered MONTHLY as follows:
\par}</xsl:text>
		<xsl:text>{\trowd \pard \f1\fs20 \ql \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Total Mortgage Amount:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Mortgage Type:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/dealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Priority:\cell }
{\f1 </xsl:text>
		<xsl:text>First \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Mortgage Term:\cell }
{\f1
</xsl:text>
		<!-- <xsl:value-of select="//specialRequirementTags/PaymentTerm"/> -->
		<xsl:value-of select="//Deal/MtgProd/paymentTerm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright</xsl:text>
		<!-- insert logic here 
<xsl:call-template name="VariableRateMortgageDetails"/>
 end insert logic here -->
		<!-- change the logic again and again - #554 -->
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3' or //Deal/MtgProd/interestTypeId='2'">
				<!-- Variable rate mortgage (VRM) or Capped Variable -->
				<!-- row1 -->
				<xsl:text>{\f1Interest Rate for Registration: \cell }{\f1 Prime – 0.75% </xsl:text>
				<xsl:text>\cell }
\pard \ql  \sa200 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<!-- row2 -->
				<xsl:text>{\f1Interest Rate:\cell }{\f1 </xsl:text>
				<xsl:value-of select="//Deal/netInterestRate"/>
				<xsl:text>\cell }
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!-- end change the logic again - #554 -->
		<xsl:text>
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1Amortization: \cell }
{\f1 </xsl:text>
		<xsl:value-of select="number(//Deal/amortizationTerm) div 12"/>
		<xsl:text> years</xsl:text>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1Mortgage Advance Date: \cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
<!-- end insert here -->
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Interest Adjustment Date:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/interimInterestAdjustmentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Maturity Date:\cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/maturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
<!--  #554
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 First Regular Payment Date:\cell }
{\f1 </xsl:text>
<xsl:value-of select="//Deal/firstPaymentDate"/>
<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
-->
			<!-- #554 -->
\pard \ql \sa200\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1 Monthly Payment: \cell }
{\f1 </xsl:text>
		<xsl:value-of select="//Deal/PandIPaymentAmountMonthly"/>
		<xsl:text> \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
\pard \qj \sa200\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1 Interest Calculated: \cell }
{\f1 </xsl:text>
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3' or //Deal/MtgProd/interestTypeId='2'">
				<xsl:text>Monthly</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Semi-annually not in advance</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}
<!-- #554 end -->
}</xsl:text>
		<xsl:text>\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20 Note: If the closing date gets moved from one month to the next, ALL dates shown above must be changed accordingly. \par\par 
}
{\pard \qj \f1\fs20\sa240\b
Payments have been set up as outlined on the Mortgage Commitment.  If the Borrower(s) would like to change payment frequency or amount, please have them complete the Payment Change Request Form and forward via fax to (416) 861-8484 as soon as possible.  The Payment Change Request Form is available on our web site at {\b \ul http://www.cervus.com/lawyercentre.asp}.\par}
{\pard \qj \f1\fs20\sa240
In the event of an interest rate change resulting in a reduced rate, the Lender will advise the Mortgagor(s) of the new payment terms directly, by letter, after closing.  You are not required to change the interest rate or payment amount in the mortgage itself.
\par}
</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="VariableRateMortgageDetails">
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3'">
				<!-- Variable rate mortgage (VRM) -->
				<!-- row1 -->
				<xsl:text>{\f1Base Rate:\cell }{\f1 </xsl:text>
				<xsl:value-of select="//Deal/postedRate"/>
				<xsl:text>\cell }
\pard \ql  \sa200 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
				<!-- row2 -->
				<xsl:text>{\f1Rate Variance: \cell }{\f1 </xsl:text>
				<xsl:choose>
					<xsl:when test="//Deal/discount='0.000%' and //Deal/premium='0.000%'">
						<xsl:text>0.000%</xsl:text>
					</xsl:when>
					<xsl:when test="not(//Deal/discount) and not(//Deal/premium)">
						<xsl:text>0.000%</xsl:text>
					</xsl:when>
					<xsl:when test="number(substring-before(//Deal/discount, '%')) &gt; 0">
						<xsl:text>-</xsl:text>
						<xsl:value-of select="//Deal/discount"/>
					</xsl:when>
					<xsl:when test="number(substring-before(//Deal/premium, '%')) &gt; 0">
						<xsl:value-of select="//Deal/premium"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>error!</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>\cell }
\pard \ql \sa200 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
				<!-- row3 -->
				<xsl:text>{\f1Equivalent Rate:\cell }{\f1 </xsl:text>
				<xsl:value-of select="//Deal/netInterestRate"/>
				<xsl:text>\cell }
\pard \ql  \sa200 \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\f1Interest Rate:\cell }{\f1 </xsl:text>
				<xsl:value-of select="//specialRequirementTags/InterestRate"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5688 \cellx5580
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx10620
\row 
}</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="Advances">
		<!-- ===================================  #642 =================================== -->
		<!-- 
<xsl:variable name="loan-amnt" select="substring-after(translate(//Deal/totalLoanAmount,',',''),'$')"/>
<xsl:variable name="total-dedts" select="substring-after(translate(//specialRequirementTags/deductibleFees/totalDeductionsAmount,',',''),'$')"/>
<xsl:variable name="net-advance" select="number($loan-amnt) - number($total-dedts)"/>

<xsl:text>loan-amnt = </xsl:text><xsl:value-of select="$loan-amnt"/><xsl:text>\par </xsl:text>
<xsl:text>total-dedts = </xsl:text><xsl:value-of select="$total-dedts"/><xsl:text>\par </xsl:text>
<xsl:text>number(loan-amnt) = </xsl:text><xsl:value-of select="format-number(number($loan-amnt),'0.00')"/><xsl:text>\par </xsl:text>
<xsl:text>number(total-dedts) = </xsl:text><xsl:value-of select="number($total-dedts)"/><xsl:text>\par </xsl:text>
<xsl:text>net-advance = </xsl:text><xsl:value-of select="$net-advance"/><xsl:text>\par </xsl:text><xsl:text>\par </xsl:text>
-->
		<xsl:text>
{\pard\f1\fs20\sa240\ul\b 
MORTGAGE ADVANCE
\par}
{\pard\f1\fs20
When all conditions precedent to this transaction have been met, funds in the amount of {\b </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/netAdvance"/>
		<xsl:text>} will be advanced. \par }
\pard \qj \fi720\li720\widctlpar\aspalpha\aspnum\faauto\adjustright\lin720\itap0 
{\f1\fs20 \par }
</xsl:text>
		<xsl:text>{\trowd \trgaph108\trleft1980
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\pard \intbl</xsl:text>
		<xsl:text>{\f1\fs20\sb180 \b PRINCIPAL AMOUNT }</xsl:text>
		<xsl:text>\cell \pard \intbl \qr </xsl:text>
		<xsl:text>{\f1\fs20\sb720 {</xsl:text>
		<xsl:value-of select="//Deal/totalLoanAmount"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text>
		<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\sb120 \tab Total Interest Adjustment Amount\cell \qr</xsl:text>
		<xsl:value-of select="//Deal/interimInterestAmount"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\row 
}</xsl:text>
		<xsl:for-each select="//specialRequirementTags/deductibleFees/DealFee">
			<xsl:text>\pard \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright {\f1\fs20\sb120 \tab </xsl:text>
			<xsl:value-of select="./Fee/feeDescription"/>
			<xsl:text>\cell \qr </xsl:text>
			<xsl:value-of select="feeAmount"/>
			<xsl:text>\cell }</xsl:text>
			<xsl:text>\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20\sb120 \sa180 \tab Total Deductions\cell \qr </xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/totalDeductionsAmount"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\row 
}
</xsl:text>
		<xsl:text>{\trowd \trgaph108\trleft1980
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\pard \intbl</xsl:text>
		<xsl:text>{\f1\fs20\sb240 \b NET ADVANCE }</xsl:text>
		<xsl:text>\cell \pard \intbl \qr </xsl:text>
		<xsl:text>{\f1\fs20\sb240 \b {</xsl:text>
		<xsl:value-of select="//specialRequirementTags/deductibleFees/netAdvance"/>
		<xsl:text>}}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<!-- 
<xsl:call-template name="createRefundableFeeBlankRow"></xsl:call-template>
<xsl:for-each select="//SolicitorsPackage/Fees/refundableFee">//SolicitorsPackage/Fees/refundableFee
<xsl:call-template name="createRefundableFeeRows"></xsl:call-template>
</xsl:for-each>
-->
		<xsl:text>
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 \cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\row 
}
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Interest Adjustment (daily per diem)\cell \qr</xsl:text>
		<xsl:value-of select="//Deal/perdiemInterestAmount"/>
		<xsl:text>\cell }
\pard \qr \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8000
\row 
}
</xsl:text>
		<!-- 

\pard \qj \widctlpar\tx4140\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par The net Mortgage proceeds, payable to you, in trust, will be deposited into your Trust account on the scheduled closing date, which is anticipated to be </xsl:text>
		<xsl:value-of select="//Deal/estimatedClosingDate"/>
		<xsl:text>. 
\par \par }</xsl:text>
		<xsl:if test="//SolicitorsPackage/Refi">[if //SolicitorsPackage/Refi]
			<xsl:text>\pard \qj \keep\keepn\widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs20\ul Refinances:}
{\f1\fs20  If the purpose of this Mortgage is to refinance an existing debt, the Mortgage proceeds will be forwarded to you, in trust, to be disbursed as outlined below. You are to confirm payment in the Statement of Funds Received and Disbursed and/or a covering letter.\par \par }
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \sa120\keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 Existing </xsl:text>
			<xsl:choose>
				<xsl:when test="//Deal/lienPositionId='0'">First</xsl:when>
				<xsl:otherwise>Second</xsl:otherwise>
			</xsl:choose>			
			<xsl:text> Mortgage\cell </xsl:text>
			<xsl:value-of select="//SolicitorsPackage/ExistingLoanAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs20 Debts to be Paid and Accounts Closed:\cell Amount:\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 

}</xsl:text>
			<xsl:for-each select="//SolicitorsPackage/Liabilities/Liability">
				<xsl:text>\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
				<xsl:value-of select="./Description"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./Amount"/>
				<xsl:text>\cell }
			<xsl:for-each select="//Deal/Borrower/Liability">
				<xsl:text>\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\pard \qj \keep\keepn\widctlpar\intbl\tx720\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
				<xsl:value-of select="./liabilityDescription"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="./liabilityAmount"/>
				<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1260\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth5040 \cellx6300
\cltxlrtb\clftsWidth3\clwWidth4320 \cellx10620
\row 
}</xsl:text>
			</xsl:for-each>
			<xsl:text>
\pard \qj \widctlpar\tx720\aspalpha\aspnum\faauto\adjustright\itap0 
{\f1\fs20 \par }</xsl:text>
		</xsl:if>
 _________________________________________________________________________________________  -->
	</xsl:template>
	<!-- ================================================  -->
	<!-- ================================================  -->
	<xsl:template name="Other">
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
FIRE INSURANCE
\par}
{\pard\f1\fs20\qj\sa180 
You must ensure, prior to advancing funds, that fire insurance coverage for building(s) on the property is in place for not less than their full replacement value with</xsl:text>
		<xsl:choose>
			<xsl:when test="//Deal/LenderProfile/lenderProfileId = '0'"> first </xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//Deal/lienPositionId = '0'"> first </xsl:when>
					<xsl:otherwise> second </xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> payable to </xsl:text>
		<xsl:value-of select="//Deal/LenderProfile/lenderName"/>
		<!--
		<xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">			
			<xsl:text> and </xsl:text>
				<xsl:call-template name="DealFunderName"/>				
			</xsl:when>
		</xsl:choose>		
		-->
		<!-- <xsl:text> and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> as Mortgagee at the address shown on the Charge/Mortgage and including a duly completed standard mortgage clause approved by the Insurance Bureau of Canada. Policies must not contain a co-insurance or similar clause that may limit the amount payable.
\par }
</xsl:text>
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
MATRIMONIAL PROPERTY ACT
\par}
{\pard\f1\fs20\qj\sa180
You are to certify that all requirements under the Matrimonial Property Act in your province, where applicable, have been met and that the status does not in any way affect our mortgage/charge.
\par }
</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<!-- ================================================  -->
	<xsl:template name="Searches">
		<xsl:text>
{\pard\f1\fs20\sa180\ul\b 
SEARCHES
\par}
{\pard\f1\fs20\qj\sa180
You must conduct all relevant searches with respect to the property normally undertaken to protect our interests. </xsl:text>
		<xsl:value-of select="//specialRequirementTags/Execution"/>
		<xsl:text>Prior to releasing any mortgage proceeds, the solicitor will carry out the necessary searches with respect to any liens, encumbrances, executions that may be registered against the property. </xsl:text>
		<xsl:text>It is your responsibility to ensure that there are no work orders or deficiency notices outstanding against the property, that all realty taxes and levies which have or will become due and payable up to the closing date are paid to the Municipality. If your search reveals title has been transferred since the date of the Offer to Purchase and before the closing date you must notify us immediately and discontinue all work on this transaction unless notified otherwise. Immediately prior to registration of the mortgage, you are to obtain a Sheriff\rquote s Certificate/General Registry Search indicating that there are no writs of execution on file against the mortgagor(s), guarantor(s), if any, or any previous owner, which would adversely affect our security.
\par }
</xsl:text>
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
SURVEY REQUIREMENTS (Non Title Insured Transactions Outside of Ontario)
\par}
{\pard\f1\fs20\qj\sa180
You must obtain and review a survey or a surveyor's certificate / Real Property Report (AB, NL, and SK) completed by a recognized land surveyor and dated within the last twenty (20) years. Satisfy yourself from the survey that the position of the buildings on the land complies with all municipal, provincial and other government requirements. Where an addition has been made since the date of the survey, an updated survey is required unless there is no doubt that the addition is also clearly within the lot lines and meets all setback requirements. Where a survey is not available for conventional uninsured mortgages and you are satisfied that the position of the building on the land complies with all municipal, provincial and other government requirements, a declaration of possession (or statutory declaration) for not less than ten (10) continuous years and indicating no changes to or disputes regarding the property is acceptable.
\par }
{\pard\f1\fs20\qj\sa180 \b
Additional Provincial Requirements: 
\par }
</xsl:text>
		<xsl:call-template name="SurveyRequirements_row"/>
		<xsl:text>{\f1\fs20 Alberta:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 
A Stamp of Compliance is required. If the Real Property report is older than six (6) months, an Affidavit of Examination by the Vendor is required confirming no changes have transpired since the original survey was completed.
}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="SurveyRequirements_row"/>
		<xsl:text>{\f1\fs20 Manitoba:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 
A Zoning Memorandum is required. If the survey is older than 10 years, an Affidavit of Examination by the Vendor is required confirming no changes have transpired since the original survey was completed. 
}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:call-template name="SurveyRequirements_row"/>
		<xsl:text>{\f1\fs20 Saskatchewan:}</xsl:text>
		<xsl:text>\cell \pard \intbl </xsl:text>
		<xsl:text>{\f1\fs20 
A Zoning Memorandum is required. If the Real Property report is older than twenty (20) years, an Affidavit of Examination by the Vendor is required confirming no changes have transpired since the original survey was completed.
}</xsl:text>
		<xsl:text>\cell \row }</xsl:text>
		<xsl:text>
{\pard\f1\fs20\qj\sb180\sa180 \b
Note: If you are unable to comply with our survey requirements we will accept a title insurance policy from our approved list of suppliers. 
\par }
</xsl:text>
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
CONDOMINIUM (if applicable)
\par}
{\pard\f1\fs20\qj\sa180
You are to confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. You are to review the Condominium Corporation\rquote s Declaration and Bylaws and confirm they contain nothing derogatory to our security. You are to assign the voting rights to </xsl:text>
		<xsl:value-of select="//Deal/LenderProfile/lenderName"/>
		<!-- <xsl:choose>
			<xsl:when test="//SolicitorsPackage/LenderNameFund">
				<xsl:text> and </xsl:text>
				<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/>			
			</xsl:when>
		</xsl:choose> -->
		<!-- <xsl:text> and </xsl:text>
		<xsl:value-of select="//SolicitorsPackage/LenderNameFund"/> -->
		<xsl:text> (if applicable).
\par}
</xsl:text>
		<!-- 
<xsl:text>
{\pard\f1\fs20\qj\sa180\b
The following clause must be inserted into the Mortgage:
\par }

{\pard\f1\fs20\qj\sa180
“In the event that the Mortgagor sells, transfers, assigns or conveys any parking unit(s) encumbered by the Mortgage while retaining title to (or ownership of) the dwelling unit so encumbered by the Mortgage, or in the event that the Mortgagor sells, transfers, assigns or conveys the aforementioned parking until(s) as well as the said dwelling unit but to different purchasers, transferees or assignees, then in either case the total outstanding principal and interest indebtedness secured by the Mortgage shall become due and payable.”
\par }
</xsl:text>
-->
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
PLANNING ACT (Ontario only)
\par}
{\pard\f1\fs20\qj\sa180
You must ensure that the Mortgage does not contravene the provisions of the Planning Act as amended from time to time, and that the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.
\par }
</xsl:text>
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
NEW CONSTRUCTION
\par}
{\pard\f1\fs20\qj\sa180
If this transaction is a new construction, you must ensure that an acceptable Certificate of Completion and New Home Warranty Certificate are obtained prior to advancing the mortgage proceeds.
\par }
</xsl:text>
		<!-- #554, edition #2
<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
RENEWAL OF GUARANTOR(S)
\par}
{\pard\f1\fs20\qj\sa180
The following wording must be included in a Schedule to the Charge/Mortgage if there is a Guarantor:  
\par}
{\pard\f1\fs20\qj\sa180
“In addition to the Covenantor’s promises and agreements contained in this Mortgage, the Covenantor(s) also agree(s) that at the sole discretion of Cervus Financial Corp., the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s).“
\par }
</xsl:text>
-->
		<xsl:text>
{\pard\f1\fs20\sa180\sb240 \ul\b 
SOLICITOR’S FINAL REPORT
\par}
{\pard\f1\fs20\qj\sa180
You must submit the Solicitor’s Final Report on Title, on the enclosed form and with the stated enclosures, within 30 days after the final advance is made. If your report cannot be submitted in this time frame, you must provide us with a letter explaining the reason(s) for the delay. 
{\b Failure to comply with any of the above instructions may result in discontinued future dealings with you or your firm.}
\par }
</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="createRefundableFeeBlankRow">
		<xsl:text>
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs20 </xsl:text>
		<xsl:text>\cell </xsl:text>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft1980\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\cltxlrtb\clftsWidth3\clwWidth4860 \cellx6480
\cltxlrtb\clftsWidth3\clwWidth4140 \cellx10620\row 
}
</xsl:text>
	</xsl:template>
	<!-- ************************************************************************ -->
	<!-- row definitions                                                                                      -->
	<!-- ************************************************************************ -->
	<!-- ================================================  -->
	<xsl:template name="SurveyRequirements_row">
		<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1440
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10800 
\pard \intbl</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<xsl:template name="DealFunderName">
		<xsl:for-each select="//Deal/UserProfile">
			<xsl:if test="./userTypeId='2'">
				<xsl:value-of select="concat(./Contact/contactFirstName,' ', ./Contact/contactLastName)"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!-- ************************************************************************ -->
	<!-- rtf file start and rtf file end                                                                    -->
	<!-- ************************************************************************ -->
	<!-- ================================================  -->
	<xsl:template name="RTFFileEnd2">
		<xsl:text>}}</xsl:text>
	</xsl:template>
	<!-- ================================================  -->
	<!-- legal size  (216 × 356 mm  or  612pt  x 1008pt); margings: top=36pt, bottom =36pt, left=36pt, right=36pt-->
	<xsl:template name="RTFFileStart2">
		<xsl:text>
{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\info {\author Catherine Rutgaizer}{\operator filogix automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company filogix inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \paperw12240 \paperh20160 \margl720\margr720\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\footer \pard\plain \ql \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\field {\*\fldinst }
{\fldrslt {\lang1024\langfe1024\noproof 1}}}
\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }}
</xsl:text>
	</xsl:template>
</xsl:stylesheet>
