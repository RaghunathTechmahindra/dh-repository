<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- 
        20/Nov/2006 SL         #5164  Suppress  the section "Reate Guarantee and Rate Adjustment Policies" if the interstTypeID <> 0
	19/Oct/2005 DVG #DG336 #2301  Commitment letter <LenderName> tag  
	29/Sep/2005 DVG #DG324 #2059  Merix - E2E - Changes to Solicitor Package  
	05/Aug/2005 DVG #DG286  #1861  Merix - commitment letter Page 2 blank 
	12/Jul/2005 DVG #DG256 #1788  Commitment Letter - minor
	04/Jul/2005 DVG #DG244 #1735  NBrook Logo 
		- configure the template for variable logos
	24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package 
	- Many changes to become the Xprs standard	-->
	<xsl:output method="text"/>	

	<!--#DG244 -->
	<xsl:variable name="lendName" select="translate(//CommitmentLetter/LenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendName,'.xsl'))" />
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />
	
	<!-- #FXP17288 -->
	<xsl:variable name="lenderName" select="//SolicitorsPackage/LenderName" />
	
	<xsl:template match="/">
		<!--#DG244 -->
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
		
		<xsl:call-template name="RTFFileStart"/>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageEnglish">				
				<xsl:call-template name="EnglishCommitmentLetter"/><!--#DG286 will be called from solicit.pkg too -->
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageFrench">
				<xsl:call-template name="FrenchFooter"/>
				<xsl:call-template name="FrenchPage1"/>
				<!--				<xsl:call-template name="FrenchPage2Start"/> -->
				<xsl:call-template name="FrenchPage2"/>
				<!--				<xsl:call-template name="FrenchPage3Start"/> -->
				<xsl:call-template name="FrenchPage3"/>
				<xsl:call-template name="FrenchPage4"/>
				<xsl:call-template name="FrenchPage5"/>
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<!--#DG286 -->
	<xsl:template name="EnglishCommitmentLetter">
		<xsl:call-template name="EnglishFooter"/>
		<xsl:call-template name="EnglishPage1"/>
		<!--xsl:call-template name="EnglishPage2Start"/> -->
		<xsl:call-template name="EnglishPage2"/>
		<!--xsl:call-template name="EnglishPage3Start"/> -->
		<xsl:call-template name="EnglishPage3"/>
		<xsl:call-template name="EnglishPage4"/>
		<xsl:call-template name="EnglishPage5"/>
	</xsl:template>
	
	<xsl:template name="EnglishPage1">
		<xsl:text><!--{ \par }\pard \ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\par }} -->
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \qc<!--#DG336 --> \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs28 MORTGAGE COMMITMENT \par }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text> <!--#DG244 -->
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell {\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalc
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell 
Tel: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell }
{\b\f1\fs18 Underwriter \par\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text> \cell }
{\b\f1\fs18 Date \par\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\row 
}
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>

</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
{\b\f1\fs18 Mortgage Reference Number:\cell \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b \cell Deal Number:\cell\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
{\i\f1 We are pleased to confirm that your application for a Mortgage Loan has been approved under the following terms and conditions:\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGOR(S):\cell\b0 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
{\b\f1\fs18 PURPOSE:\b0  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>(S):}{\f1\fs18 \cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18\ul SECURITY ADDRESS: \cell LEGAL DESCRIPTION: \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Properties/Property">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tx4900\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 
</xsl:text>
			<xsl:value-of select="./AddressLine1"/>
			<xsl:text> \par 
</xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>\par 
</xsl:text>
			<xsl:value-of select="./PostalCode"/>
			<xsl:text> \par 
\cell </xsl:text>
			<xsl:value-of select="./LegalLine1"/>
			<xsl:if test="./LegalLine1">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine2"/>
			<xsl:if test="./LegalLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine3"/>
			<xsl:if test="./LegalLine3">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 COMMITMENT DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/>
		<xsl:text>
\par 
\par }

{\b\f1\fs18 INTEREST ADJUSTMENT DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/IADDate"/>
		<xsl:text>\cell }
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 COMMITMENT EXPIRY DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentExpiryDate"/>
		<xsl:text>
\par 
\par }
{\b\f1\fs18 FIRST PAYMENT DATE:}
{\f1\fs18 \par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
{\b\f1\fs18 ADVANCE DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text>
\par 
\par }
{\b\f1\fs18 MATURITY DATE:}
{\f1\fs18 \par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 LOAN TYPE:  \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Product"/>
		<xsl:text>\cell }
{\b\f1\fs18 LTV:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 INTEREST RATE:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 MTG.INSUR.PREMIUM:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs18 TERM:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentTerm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 TOTAL LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 AMORTIZATION PERIOD:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Amortization"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC PAYMENT AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalPayment"/>
		<xsl:text>\cell }
{\b\f1\fs18 PAYMENT FREQUENCY:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentFrequency"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 EST.ANNUAL PROP.TAXES:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AnnualTaxes"/>
		<xsl:text>\cell }
{\b\f1\fs18 TAXES TO BE PAID BY:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TaxPayor"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 PST PAYABLE ON INSUR.PREMIUM:\cell }{\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGE FEES:\cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell }{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 In this approval and any schedule(s) to this approval, }{\b\f1\fs18 you and your}{\f1\fs18  mean the Borrower, Co-Borrower and </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>, if any, and }{\b\f1\fs18 
we, our and us }{\f1\fs18  mean </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
\par 
\par All of our normal requirements and, if applicable, those of the mortgage insurer must be 
met. All costs, including legal, survey, mortgage insurance etc. are for the account of the applicant(s). The mortgage insurance premium (if applicable) will be added to the mortgage. Any fees specified herein may be deducted from the Mortgage advance. If
 for any reason the loan is not advanced, you agree to pay all application, legal, appraisal and survey costs incurred in this transaction.
\par 
\par This Mortgage Commitment is subject to the details and terms outlined herein as well as the conditions described on the attached schedules.
\par 
\par To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> after which time if not accepted, shall be considered null and void.
\par 
\par Thank you for choosing </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> for your financing.
\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 
\par \page 
\par } </xsl:text>
	</xsl:template>

	<!--
<xsl:template name="EnglishPage2Start">
<xsl:text>{\sect } \linex0\headery706\footery260\endnhere\sectdefaultcl</xsl:text>
</xsl:template>
-->

	<xsl:template name="EnglishPage2">
		<xsl:text>\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul TERMS AND CONDITIONS:
\par 
\par Mortgage:
\par }
{\f1\fs18 
This Mortgage will be subject to all extended terms set forth in our standard form of mortgage contract or in the mortgage contract prepared by our solicitors, whichever the case may be, and insured mortgage loans will be subject to the provisions of the National Housing Act (N.H.A.) and the regulations thereunder.</xsl:text>\par }
<xsl:choose>
    <xsl:when test=" //Deal/interestTypeId = 0">
         {\b\f1\fs18\ul \par <xsl:text>Rate Guarantee and Rate Adjustment Policies:
         \par }{\f1\fs18 Provided that the Mortgage Loan is advanced by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text>, the interest rate will be </xsl:text>
		<xsl:value-of select="//CommitmentLetter/InterestRate"/>
		         <xsl:text> per annum, calculated half yearly, not in advance. In the event that the effective rate of interest is lower at closing, the interest rate may be adjusted.</xsl:text>
         \par }
     </xsl:when>
</xsl:choose>
{\b\f1\fs18\ul \par <xsl:text>Title Requirements / Title Insurance:
\par }{\f1\fs18 Title to the Property must be acceptable to our Solicitor who will ensure that the Mortgagor(s) have good and marketable title in fee simple to the property and that it is clear of any encumbrances which might affect the priority of the </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> Mortgage. Alternatively, we will accept title insurance from a title insurance provider acceptable to </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>. The Mortgage must be a first charge on the property.
\par 
\par }{\b\f1\fs18\ul Survey Requirements / Title Insurance:
\par }{\f1\fs18 A title insurance policy, from a title insurance provider acceptable to </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
				<xsl:text>, must be obtained covering both the lenders and the Borrower(s) interests without exceptions to coverage. Computershare Trust Company of Canada should be named as insured.</xsl:text>
		<xsl:text>
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<!--
<xsl:template name="EnglishPage3Start">
<xsl:text>{\sect }\sectd \linex0\headery706\footery260\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Page {\field{\*\fldinst {\cs22  PAGE }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}{\f1\fs20  of }{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
</xsl:template>
-->
	<xsl:template name="EnglishPage3">
		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text>
		<!--#DG286 xsl:text>{\b\f1\fs18\ul Privilege Payment Policies: }{\f1\fs18 \par This mortgage is CLOSED. The Mortgagor (borrower), when not in default of any of the terms, covenants, conditions or provisions contained in the mortgage, shall have the following privileges for payment of extra principal amounts: 1) During each 12 month period following the interest adjustment date the borrower may, without penalty: i) make prepayments totaling up to twenty percent (20%) of the original principal amount of the mortgage; ii) increase regular monthly payment by a total up to twenty percent (20%) of the original monthly payment amount so as to reduce the amortization of the mortgage without changing term. 2) If the mortgaged property is sold pursuant to a bona fide arms length agreement, the borrower may repay the mortgage in full upon payment of three (3) months interest penalty calculated at the current interest rate of the mortgage. 3) In addition to the prepayment privileges described in paragraph 1, at any time after the third anniversary of the interest adjustment date, the borrower may prepay the mortgage in whole or in part upon payment of three (3) months interest penalty calculated at the current interest rate of the mortgage.\par \par }</xsl:text-->
		<xsl:text>{\b\f1\fs18\ul Privilege Payment Policies: }{\f1\fs18 \par </xsl:text>

		<!--#DG324 xsl:value-of select="//CommitmentLetter/Privilege/Line"/-->
    <!-- moved from the regular conditions section --> 
		<xsl:for-each select="/*/CommitmentLetter/Conditions
      /Condition[conditionId = 62 or conditionId = 63]/Line">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>

		<xsl:text>\par }

<!--#DG324 
{\b\f1\fs18\ul Interest Adjustment Payment: }
{\f1\fs18 \par Prior to commencement of the regular monthly loan payments interest will accrue at 
the rate payable on the Mortgage loan, on all funds advanced to the Mortgagor(s). Interest will 
be computed from the advance date and will become due and payable on the first day of the month 
next following the advance date.\par\par }
-->
{\b\f1\fs18\ul Prepayment Policies:\par }
{\f1\fs18 
<!--#DG324 
You can prepay this mortgage on payment of 3 months simple interest on the principal 
amount owing at the prepayment date, or the interest rate differential,whichever is greater.
-->
</xsl:text>

		<xsl:variable name="prePay" select="/*/Deal/prePaymentOptionsId"/>		
		<xsl:variable name="term" select="/*/Deal/actualPaymentTerm"/>
		<xsl:variable name="prePayCondId">
    	<xsl:choose>
    		<xsl:when test="$prePay = 0">501</xsl:when>
    		<xsl:when test="$prePay = 2">  
        	<xsl:choose>
        		<xsl:when test="$term &lt;= 60">548</xsl:when>
        		<xsl:otherwise>66</xsl:otherwise>
        	</xsl:choose>			
    		</xsl:when>
    		<xsl:when test="$prePay = 4">71</xsl:when>
    		<xsl:when test="$prePay = 5">96</xsl:when>
    		<xsl:otherwise>-1</xsl:otherwise>
    	</xsl:choose>			
		</xsl:variable>

    <!-- moved from the regular conditions section --> 
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition[conditionId = $prePayCondId]/Line">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>

		<xsl:text>\par }

<!--#DG286 
{\b\f1\fs18\ul Renewal:\par }
{\f1\fs18 At the sole discretion of BasisXPressLender, the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s).

\par 
\par }
{\b\f1\fs18\ul Early Renewal: 
\par }{\f1\fs18 Provided that the Mortgage is not in 
default and that the building on the lands described in the Mortgage is used only as residence and comprises no more than four (4) dwelling units, you may renegotiate the term and payment provisions of the Mortgage prior to the end of the original term of
 the Mortgage, provided that: 
\par }
\pard \ql \widctlpar\intbl\tx180\tx1000\faauto {\f1\fs18 \tab a) \tab the Mortgage may only be renegotiated for a closed term 
\par \tab b) \tab the Mortgagor(s) select a renewal option that results in the new Maturity Date being equal to or greater than 
\par \tab \tab the original Maturity Date of the current term; and 
\par \tab c) \tab payment of the interest rate differential and an early renewal processing fee is made upon execution of such 
\par \tab \tab renewal or amending agreement. 
\par }\pard \ql \widctlpar\intbl\faauto {\f1\fs18 
\par 
\par }-->
{\b\f1\fs18\ul NSF Fee: \par }
{\f1\fs18 There may be a fee charged for any payment returned due to insufficient funds or stopped payments.
\par 
\par }<!--#DG286 {\b\f1\fs18\ul Assumption Policies:
\par }{\f1\fs18 In the event that you subsequently enter into an agreement to sell, convey or transfer the property, any subsequent purchaser(s) who wish to assume this Mortgage, must be approved by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>, otherwise this Mortgage Loan will become due and payable upon your vacating or selling the property.
\par 
\par } -->
{\b\f1\fs18\ul Portability Policies:
\par }{\f1\fs18 Should you decide to sell the mortgaged property and purchase another property, you may transfer the remaining mortgage balance together with the interest rate set out therein to the new property provided that the Mortgage Loan is not in default and a new mortgage loan application has been accepted by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> and all terms and conditions set out in the subsequent Mortgage Loan approval have been met.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page \par }</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishPage4">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\ql \widctlpar\intbl\faauto 
{\b\f1\fs18\ul THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING\par\par }

{\f1\fs18 The following conditions must be met, and the requested documents must be received in 
form and content satisfactory to </xsl:text>
		<!--#DG336 xsl:value-of select="//SolicitorsPackage/LenderName"/-->
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<!--#DG238 -->
		<xsl:text> no later than ten (10) days prior to the advance of the mortgage. Failure to do so may delay or void this commitment.
\par \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}</xsl:text>
  
		<!--#DG324 those conds will only appear in pag. 3 instead
    xsl:if test="//CommitmentLetter/Conditions"-->
		<xsl:variable name="conds" select="/*/CommitmentLetter/Conditions/Condition[not(
      conditionId = 62 or conditionId = 63 or conditionId = 66
      or conditionId = 71 or conditionId = 96 or conditionId = 501 or conditionId = 548
      )]"/>
		<xsl:if test="$conds">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
		</xsl:if>
		<!--#DG324 xsl:for-each select="//CommitmentLetter/Conditions/Condition"-->
		<xsl:for-each select="$conds">

			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell 
</xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
					<xsl:text>\par </xsl:text>
			</xsl:for-each>
			<xsl:text>\cell }
\row 

</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \par \par }
{\b\f1\fs18\ul Requests for any changes to the terms and conditions of the Commitment will only be considered if your request is received by us in writing at least 5 business days prior to the Advance Date.
\par 
\par ACCEPTANCE:
\par 
\par }
{\f1\fs18 This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> after which time, if not accepted, shall be considered null and void. Furthermore, the mortgage must be advanced by no later than </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text> at which time it expires.
\par 
\par }\pard \ql \widctlpar\intbl\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright {\f1\fs18 \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
\par 
\par \tab Authorized by:___________________________________________
\par \tab \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>
\par 
\par 
\par }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}

\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page }</xsl:text>
	</xsl:template>

	<xsl:template name="EnglishPage5">
		<xsl:text><!--#DG324 \trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s15\ql \widctlpar\intbl\faauto 

{\b\f1\fs18 I / We, hereby authorize </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> to debit the account indicated below with payments on the Mortgage Loan (including taxes and life insurance premiums, if applicable)
\par }
\pard\plain \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 [ ] Chequing/Savings Account \cell  [ ] Chequing Account  \cell  [ ] Current Account No._________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Bank _______________________________________________ Branch_________________________________________________
\par 
\par Address____________________________________________________________________________________________________
\par 
\par Transit No.________________________________ Telephone No._____________________________________________________
\par 
\par }{\b\f1\fs18 A sample "VOID" cheque is enclosed.}{\f1\fs18 
\par 
\par }-->\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 I / We </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BorrowerNamesLine"/><!--#DG256 -->
		<xsl:text> warrant that all representations made by me/us and all the information submitted to you in connection with my/our mortgage application are true and accurate. I / We fully understand that any misrepresentations of fact contained in my/our mortgage application or other documentation entitles you to decline to advance a portion or all of the loan proceeds, or to demand immediate repayment of all monies secured by the Mortgage.
\par 
\par 
\par I / We, the undersigned applicants accept the terms of this Mortgage Commitment as stated above and agree to fulfill the conditions of approval outlined herein.}
{\f1\fs18 \par \par }
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7800\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell ___________________________
\par APPLICANT
\par 
\par \cell ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par CO-APPLICANT
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text>
	</xsl:template>

	<xsl:template name="EnglishFooter">
		<xsl:text>{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }{\f1\fs16 of }
{\field{\*\fldinst {NUMPAGES }}}}}</xsl:text>
	</xsl:template>


	<!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="FrenchPage1">
		<xsl:text>
			<!--{ \par }\pard \ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\par }} -->
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f1\fs28 MORTGAGE COMMITMENT \par }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text><!--#DG244 -->
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell {\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalc
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell 
Tel: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell }
{\b\f1\fs18 Underwriter \par\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text> \cell }
{\b\f1\fs18 Date \par\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\row 
}
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>

</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="//CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="//CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
{\b\f1\fs18 Mortgage Reference Number:\cell \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18\b \cell Deal Number:\cell\b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
{\i\f1 We are pleased to confirm that your application for a Mortgage Loan has been approved under the following terms and conditions:\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGOR(S):\cell\b0 </xsl:text>
		<xsl:for-each select="//CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
{\b\f1\fs18 PURPOSE:\b0  </xsl:text>
		<xsl:value-of select="//CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>(S):}{\f1\fs18 \cell </xsl:text>
		<xsl:for-each select="//CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18\ul SECURITY ADDRESS: \cell LEGAL DESCRIPTION: \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Properties/Property">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tx4900\aspalpha\aspnum\faauto\adjustright
{\f1\fs18 
</xsl:text>
			<xsl:value-of select="./AddressLine1"/>
			<xsl:text> \par 
</xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>\par 
</xsl:text>
			<xsl:value-of select="./PostalCode"/>
			<xsl:text> \par 
\cell </xsl:text>
			<xsl:value-of select="./LegalLine1"/>
			<xsl:if test="./LegalLine1">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine2"/>
			<xsl:if test="./LegalLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine3"/>
			<xsl:if test="./LegalLine3">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 COMMITMENT DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentIssueDate"/>
		<xsl:text>
\par 
\par }

{\b\f1\fs18 INTEREST ADJUSTMENT DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/IADDate"/>
		<xsl:text>\cell }
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 COMMITMENT EXPIRY DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentExpiryDate"/>
		<xsl:text>
\par 
\par }
{\b\f1\fs18 FIRST PAYMENT DATE:}
{\f1\fs18 \par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
{\b\f1\fs18 ADVANCE DATE:}
{\f1\fs18 
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text>
\par 
\par }
{\b\f1\fs18 MATURITY DATE:}
{\f1\fs18 \par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 LOAN TYPE:  \b0 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Product"/>
		<xsl:text>\cell }
{\b\f1\fs18 LTV:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LTV"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 INTEREST RATE:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 MTG.INSUR.PREMIUM:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f1\fs18 TERM:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentTerm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 TOTAL LOAN AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell }
{\b\f1\fs18 AMORTIZATION PERIOD:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Amortization"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 BASIC PAYMENT AMOUNT:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TotalPayment"/>
		<xsl:text>\cell }
{\b\f1\fs18 PAYMENT FREQUENCY:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PaymentFrequency"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 EST.ANNUAL PROP.TAXES:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AnnualTaxes"/>
		<xsl:text>\cell }
{\b\f1\fs18 TAXES TO BE PAID BY:}{\f1\fs18 \cell </xsl:text>
		<xsl:value-of select="//CommitmentLetter/TaxPayor"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 PST PAYABLE ON INSUR.PREMIUM:\cell }{\f1\fs18 </xsl:text>
		<xsl:value-of select="//CommitmentLetter/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 MORTGAGE FEES:\cell }{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		<xsl:for-each select="//CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 \cell </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell }{\f1\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 In this approval and any schedule(s) to this approval, }{\b\f1\fs18 you and your}{\f1\fs18  mean the Borrower, Co-Borrower and </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>, if any, and }{\b\f1\fs18 
we, our and us }{\f1\fs18  mean </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
\par 
\par All of our normal requirements and, if applicable, those of the mortgage insurer must be 
met. All costs, including legal, survey, mortgage insurance etc. are for the account of the applicant(s). The mortgage insurance premium (if applicable) will be added to the mortgage. Any fees specified herein may be deducted from the Mortgage advance. If
 for any reason the loan is not advanced, you agree to pay all application, legal, appraisal and survey costs incurred in this transaction.
\par 
\par This Mortgage Commitment is subject to the details and terms outlined herein as well as the conditions described on the attached schedules.
\par 
\par To accept these terms, this Mortgage Commitment must be signed by all parties and returned to us by no later than </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> after which time if not accepted, shall be considered null and void.
\par 
\par Thank you for choosing </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> for your financing.
\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 
\par \page 
\par } </xsl:text>
	</xsl:template>
	<!--
<xsl:template name="FrenchPage2Start">
<xsl:text>{\sect } \linex0\headery706\footery260\endnhere\sectdefaultcl</xsl:text>
</xsl:template>
-->
	<xsl:template name="FrenchPage2">
		<xsl:text>\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul TERMS AND CONDITIONS:
\par 
\par Mortgage:
\par }
{\f1\fs18 
This Mortgage will be subject to all extended terms set forth in our standard form of mortgage contract or in the mortgage contract prepared by our solicitors, whichever the case may be, and insured mortgage loans will be subject to the provisions of the National Housing Act (N.H.A.) and the regulations thereunder.\par }
{\b\f1\fs18\ul \par Rate Guarantee and Rate Adjustment Policies:\par }
{\f1\fs18 Provided that the Mortgage Loan is advanced by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text>, the interest rate will be </xsl:text>
		<xsl:value-of select="//CommitmentLetter/InterestRate"/>
		<xsl:text> per annum, calculated half yearly, not in advance. In the event that the effective rate of interest is lower at closing, the interest rate may be adjusted.
\par 
\par }{\b\f1\fs18\ul Title Requirements / Title Insurance:
\par }{\f1\fs18 Title to the Property must be acceptable to our Solicitor who will ensure that the Mortgagor(s) have good and marketable title in fee simple to the property and that it is clear of any encumbrances which might affect the priority of the </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> Mortgage. Alternatively, we will accept title insurance from a title insurance provider acceptable to </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>. The Mortgage must be a first charge on the property.
\par 
\par }{\b\f1\fs18\ul Survey Requirements / Title Insurance:
\par }{\f1\fs18 A title insurance policy, from a title insurance provider acceptable to </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
				<xsl:text>, must be obtained covering both the lenders and the Borrower(s) interests without exceptions to coverage. Computershare Trust Company of Canada should be named as insured.</xsl:text>
		<xsl:text>
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<!--
<xsl:template name="FrenchPage3Start">
<xsl:text>{\sect }\sectd \linex0\headery706\footery260\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\f1\fs20 Page {\field{\*\fldinst {\cs22  PAGE }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}{\f1\fs20  of }{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
</xsl:template>
-->
	<xsl:template name="FrenchPage3">
		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18\ul Privilege Payment Policies: }{\f1\fs18 \par This mortgage is CLOSED. The Mortgagor (borrower), when not in default of any of the terms, covenants, conditions or provisions contained in the mortgage, shall have the following privileges for payment of extra principal amounts: 1) During each 12 month period following the interest adjustment date the borrower may, without penalty: i) make prepayments totaling up to twenty percent (20%) of the original principal amount of the mortgage; ii) increase regular monthly payment by a total up to twenty percent (20%) of the original monthly payment amount so as to reduce the amortization of the mortgage without changing term. 2) If the mortgaged property is sold pursuant to a bona fide arms length agreement, the borrower may repay the mortgage in full upon payment of three (3) months interest penalty calculated at the current interest rate of the mortgage. 3) In addition to the prepayment privileges described in paragraph 1, at any time after the third anniversary of the interest adjustment date, the borrower may prepay the mortgage in whole or in part upon payment of three (3) months interest penalty calculated at the current interest rate of the mortgage.\par \par }
{\b\f1\fs18\ul Interest Adjustment Payment }
{\f1\fs18 \par Prior to commencement of the regular monthly loan payments interest will accrue at the rate payable on the Mortgage loan, on all funds advanced to the Mortgagor(s). Interest will be computed from the advance date and will become due and payable on the first day of the month next following the advance date.
\par 
\par }{\b\f1\fs18\ul Prepayment Policies:
\par }{\f1\fs18 You can prepay this mortgage on payment of 3 months simple interest on the principal amount owing at the prepayment date, or the interest rate differential,whichever is greater.TBD
\par 
\par }{\b\f1\fs18\ul Renewal:
\par }{\f1\fs18 At the sole discretion of BasisXPressLender, the Mortgage may be renewed at maturity for any term, with or without a change in the interest rate payable under the Mortgage, by entering into one or more written agreements with the Mortgagor(s).

\par 
\par }{\b\f1\fs18\ul Early Renewal: 
\par }{\f1\fs18 Provided that the Mortgage is not in 
default and that the building on the lands described in the Mortgage is used only as residence and comprises no more than four (4) dwelling units, you may renegotiate the term and payment provisions of the Mortgage prior to the end of the original term of
 the Mortgage, provided that: 
\par }\pard \ql \widctlpar\intbl\tx180\tx1000\faauto {\f1\fs18 \tab a) \tab the Mortgage may only be renegotiated for a closed term 
\par \tab b) \tab the Mortgagor(s) select a renewal option that results in the new Maturity Date being equal to or greater than 
\par \tab \tab the original Maturity Date of the current term; and 
\par \tab c) \tab payment of the interest rate differential and an early renewal processing fee is made upon execution of such 
\par \tab \tab renewal or amending agreement. 
\par }\pard \ql \widctlpar\intbl\faauto {\f1\fs18 
\par 
\par }{\b\f1\fs18\ul NSF Fee: 
\par }{\f1\fs18 There may be a fee charged for any payment returned due to insufficient funds or stopped payments.
\par 
\par }{\b\f1\fs18\ul Assumption Policies:
\par }{\f1\fs18 In the event that you subsequently enter into an agreement to sell, convey or transfer the property, any subsequent purchaser(s) who wish to assume this Mortgage, must be approved by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text>, otherwise this Mortgage Loan will become due and payable upon your vacating or selling the property.
\par 
\par }{\b\f1\fs18\ul Portability Policies:
\par }{\f1\fs18 Should you decide to sell the mortgaged property and purchase another property, you may transfer the remaining mortgage balance together with the interest rate set out therein to the new property provided that the Mortgage Loan is not in default and a new mortgage loan application has been accepted by </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> and all terms and conditions set out in the subsequent Mortgage Loan approval have been met.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<xsl:template name="FrenchPage4">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\b\f1\fs18\ul THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING
\par 
\par }{\f1\fs18 The following conditions must be met, and the requested documents must be received in form and content satisfactory to BasisXPressLender no later than ten (10) days prior to the advance of the mortgage. Failure to do so may delay or void this commitment.
\par \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}</xsl:text>
		<xsl:if test="//CommitmentLetter/Conditions">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
		</xsl:if>
		<xsl:for-each select="//CommitmentLetter/Conditions/Condition">
			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f1\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }

{\f1\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \par \par }
{\b\f1\fs18\ul Requests for any changes to the terms and conditions of the Commitment will only be considered if your request is received by us in writing at least 5 business days prior to the Advance Date.
\par 
\par ACCEPTANCE:
\par 
\par }
{\f1\fs18 This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text>
		<xsl:value-of select="//CommitmentLetter/CommitmentReturnDate"/>
		<xsl:text> after which time, if not accepted, shall be considered null and void. Furthermore, the mortgage must be advanced by no later than </xsl:text>
		<xsl:value-of select="//CommitmentLetter/AdvanceDate"/>
		<xsl:text> at which time it expires.
\par 
\par }\pard \ql \widctlpar\intbl\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright {\f1\fs18 \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> 
\par 
\par \tab Authorized by:___________________________________________
\par \tab \tab \tab </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text>
\par 
\par 
\par }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f1\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<xsl:template name="FrenchPage5">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s15\ql \widctlpar\intbl\faauto 
{\b\f1\fs18 I / We, hereby authorize </xsl:text>
		<xsl:value-of select="//CommitmentLetter/LenderName"/>
		<xsl:text> to debit the account indicated below with payments on the Mortgage Loan (including taxes and life insurance premiums, if applicable)
\par }
\pard\plain \ql \widctlpar\intbl\faauto 
{\f1\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f1\fs18 [ ] Chequing/Savings Account \cell  [ ] Chequing Account  \cell  [ ] Current Account No._________________ \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par }
\pard \ql \widctlpar\intbl\faauto 
{\f1\fs18 Bank _______________________________________________ Branch_________________________________________________
\par 
\par Address____________________________________________________________________________________________________
\par 
\par Transit No.________________________________ Telephone No._____________________________________________________
\par 
\par }{\b\f1\fs18 A sample "VOID" cheque is enclosed.}{\f1\fs18 
\par 
\par }{\b\f1\fs18 I / We </xsl:text>
		<xsl:value-of select="//CommitmentLetter/Underwriter"/>
		<xsl:text> warrant that all representations made by me/us and all the information submitted to you in connection with my/our mortgage application are true and accurate. I / We fully understand that any misrepresentations of fact contained in my/our mortgage application or other documentation entitles you to decline to advance a portion or all of the loan proceeds, or to demand immediate repayment of all monies secured by the Mortgage.
\par 
\par 
\par I / We, the undersigned applicants accept the terms of this Mortgage Commitment as stated above and agree to fulfill the conditions of approval outlined herein.}
{\f1\fs18 \par \par }
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7800\aspalpha\aspnum\faauto\adjustright 
{\f1\fs18 \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell ___________________________
\par APPLICANT
\par 
\par \cell ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par CO-APPLICANT
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f1\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f1\fs18 ___________________________
\par </xsl:text>
		<xsl:value-of select="//CommitmentLetter/GuarantorClause"/>
		<xsl:text>
\par \par \cell }
{\b\f1\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text>
	</xsl:template>
	<xsl:template name="FrenchFooter">
		<xsl:text>{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 {\f1\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }{\f1\fs16 of }
{\field{\*\fldinst {NUMPAGES }}}}}</xsl:text>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>{\par }}</xsl:text>
	</xsl:template>

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033{\fonttbl{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f32\froman\fcharset238\fprq2 Times New Roman CE;}{\f33\froman\fcharset204\fprq2 Times New Roman Cyr;}{\f35\froman\fcharset161\fprq2 Times New Roman Greek;}{\f36\froman\fcharset162\fprq2 Times New Roman Tur;}
{\f37\froman\fcharset177\fprq2 Times New Roman (Hebrew);}{\f38\froman\fcharset178\fprq2 Times New Roman (Arabic);}{\f39\froman\fcharset186\fprq2 Times New Roman Baltic;}{\f40\fswiss\fcharset238\fprq2 Arial CE;}{\f41\fswiss\fcharset204\fprq2 Arial Cyr;}
{\f43\fswiss\fcharset161\fprq2 Arial Greek;}{\f44\fswiss\fcharset162\fprq2 Arial Tur;}{\f45\fswiss\fcharset177\fprq2 Arial (Hebrew);}{\f46\fswiss\fcharset178\fprq2 Arial (Arabic);}{\f47\fswiss\fcharset186\fprq2 Arial Baltic;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;
\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;\red255\green255\blue255;}{\stylesheet{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}{\s15\ql \widctlpar\faauto\itap0 \b\f1\fs18\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext15 
Body Text;}}{\info{\title </xsl:text>
		<xsl:choose>
			<xsl:when test="//CommitmentLetter/LanguageFrench">
				<xsl:text/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>MORTGAGE COMMITMENT</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> }{\author Zivko Radulovic}{\operator John Schisler}{\creatim\yr2002\mo8\dy27\hr11\min17}{\revtim\yr2002\mo8\dy27\hr11\min17}{\version2}{\edmins3}{\nofpages5}{\nofwords0}{\nofchars0}{\*\company filogix Inc}
{\nofcharsws0}{\vern8283}}\margl720\margr720\margt720\margb720 \widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand\viewkind1\viewscale90\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0<!--#DG286 -->\headery0\footery400\endnhere\sectlinegrid360\sectdefaultcl </xsl:text>
	</xsl:template>

	<!--#DG244 not used here, keep it just because it may be used by other templates-->
	<!-- ************************************************************************ 	-->
	<!-- LOGO					                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="Logo">
		<xsl:value-of select="$logoRtf"/>
	</xsl:template>
	
</xsl:stylesheet>
