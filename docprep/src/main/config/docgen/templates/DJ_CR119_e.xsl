<?xml version="1.0" encoding="UTF-8"?>
<!-- Author Zivko Radulovic, Catherine Rutgaizer -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xmlns="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	<!-- ================================================================== -->
	<xsl:template name="CreateTitleLine">
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">\mos_docprep\admin_DJ\docgen\templates\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.1in</xsl:attribute>
								<xsl:attribute name="width">2.86in</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="display-align">center</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">18pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">3pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:attribute name="border-before-style">solid</xsl:attribute><xsl:attribute name="border-before-width">1mm</xsl:attribute>
							<xsl:attribute name="border-after-style">solid</xsl:attribute><xsl:attribute name="border-after-width">1mm</xsl:attribute>
								MORTGAGE APPLICATION
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								PURPOSE:
							</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
								Savings, credit and complementary financial services
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ================================================================== -->
	<!-- ### -->
	<!-- ================================================================== -->	
	<xsl:template name="CreateActifPassifSection">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
			<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
			<xsl:attribute name="space-before.optimum">2mm</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>					
		</xsl:element>	
		<xsl:element name="fo:table"><xsl:attribute name="text-align">left</xsl:attribute><xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">8cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10.5cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:call-template name="CreateActifSection"></xsl:call-template> 
						</xsl:element>
					</xsl:element>							
					<!-- end of cell -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>							
					<!-- end of cell -->					
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute>
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:element name="fo:block">
							 <xsl:call-template name="CreatePasifSection"></xsl:call-template> 
						</xsl:element>
					</xsl:element>		
					<!-- end of cell -->					
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ================================================================== -->	
	
	<!-- Catherine -->
	<xsl:template name="ActifColumns">
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">25mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">30mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">25mm</xsl:attribute></xsl:element>
	</xsl:template>
	
	<xsl:template name="passifColumns">
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">20mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">28mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">16mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">19mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">22mm</xsl:attribute></xsl:element>
	</xsl:template>	
	<!-- end Catherine -->
	
	<xsl:template name="CreateActifSection">
		<xsl:element name="fo:table">	<xsl:attribute name="text-align">left</xsl:attribute><xsl:attribute name="table-layout">fixed</xsl:attribute>
				<xsl:call-template name="ActifColumns"/> <!-- Catherine -->
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									ASSETS
							</xsl:element>
					</xsl:element>		
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Institution / details
							</xsl:element>
					</xsl:element>		
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Amount
							</xsl:element>
				</xsl:element></xsl:element>
			</xsl:element></xsl:element>
		<xsl:call-template name="ActifDetail"/>
	</xsl:template>
	<!-- ================================================================== -->	
	<xsl:template name="CreatePasifSection">
		<xsl:element name="fo:table">	<xsl:attribute name="text-align">left</xsl:attribute><xsl:attribute name="table-layout">fixed</xsl:attribute>
				<xsl:call-template name="passifColumns"/>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									LIABILITIES
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Institution / details
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->					
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Limit
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->					
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Monthly payment
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->					
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">0mm</xsl:attribute>	<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
						<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
						<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
									Balance
							</xsl:element>
					</xsl:element>		
					<!-- end of cell -->
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:call-template name="PassifDetail"/>
	</xsl:template>
	
	<!-- ===================    Catherine ============================ -->
	<xsl:attribute-set name="tableFont">
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Times Roman</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>		
	</xsl:attribute-set>
	<xsl:attribute-set name="BottFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>		
	</xsl:attribute-set>		
	<xsl:attribute-set name="rowHeight">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="rowAttr" use-attribute-sets="rowHeight">
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="rowAttrNar">
		<xsl:attribute name="padding-top">1mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
		<xsl:attribute name="padding-right">1mm</xsl:attribute>
		<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
	</xsl:attribute-set>	
	<xsl:attribute-set name="rowEmpty" use-attribute-sets="rowAttrNar">
		<xsl:attribute name="padding-top">6mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="DollarAmntFormat">
		<xsl:attribute name="line-height">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>    
			
	<xsl:template name="ActifDetail">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:call-template name="ActifColumns"/>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="tableFont"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">Deposits, savings (with account #'s)</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotalAssetCash"/></xsl:element>
					</xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row"><!-- ACT 4, 5-->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
					<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
						<xsl:element name="fo:block">Financial institution (name and address)</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="padding-top">3mm</xsl:attribute>						
							<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute>
								<xsl:element name="fo:table-column"><xsl:attribute name="column-width">20mm</xsl:attribute></xsl:element>
								<xsl:element name="fo:table-column"><xsl:attribute name="column-width">10mm</xsl:attribute></xsl:element>
								<xsl:element name="fo:table-column"><xsl:attribute name="column-width">50mm</xsl:attribute></xsl:element>
								<xsl:element name="fo:table-body"><xsl:element name="fo:table-row">
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Caisse</xsl:text></xsl:element></xsl:element>
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:text>	</xsl:text></xsl:element></xsl:element>
									<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Bank</xsl:text></xsl:element></xsl:element>
								</xsl:element>
							</xsl:element></xsl:element>							
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- ACT 7, 8, 9-->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">Real estate</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="padding-top">2mm</xsl:attribute>Furniture</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
							<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotAssetProperty"/></xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotAssetVehicle"/></xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="border-bottom">solid white 0px</xsl:attribute>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute>					
						<xsl:element name="fo:block">Other assets</xsl:element>
						<xsl:element name="fo:block">Investments</xsl:element>
						<xsl:element name="fo:block">Bonds</xsl:element>
						<xsl:element name="fo:block"><xsl:attribute name="padding-top">5mm</xsl:attribute>
							RRSP</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="padding-bottom">11mm</xsl:attribute>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotAssetStocks"/></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
							<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotAssetRrsp"/></xsl:element></xsl:element>
				</xsl:element>

				<xsl:call-template name="ListedAssets"/>
				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>					
						<xsl:element name="fo:block">TOTAL</xsl:element><xsl:element name="fo:block">ASSETS</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:attribute name="number-columns-spanned">2</xsl:attribute>
					<xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotalAssetSide"/></xsl:element></xsl:element>
				</xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>	
	
	<xsl:template name="ListedAssets">
		<xsl:for-each select="//specialRequirementTags/ListedAsset">
			<xsl:element name="fo:table-row">
				<xsl:element name="fo:table-cell">
					<xsl:attribute name="padding-top">1mm</xsl:attribute>
					<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
					<xsl:attribute name="padding-left">1mm</xsl:attribute>
					<xsl:attribute name="border-left">solid black 0.5px</xsl:attribute>				
						<xsl:element name="fo:block"></xsl:element></xsl:element>						
				<xsl:element name="fo:table-cell">
					<xsl:attribute name="padding-top">1mm</xsl:attribute>
					<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
					<xsl:attribute name="padding-left">1mm</xsl:attribute>
					<xsl:attribute name="border">solid black 0.5px</xsl:attribute>				
						<xsl:element name="fo:block"><xsl:value-of select="ListedAssetTitle"/></xsl:element></xsl:element>
				<xsl:element name="fo:table-cell">
					<xsl:attribute name="padding-top">1mm</xsl:attribute>
					<xsl:attribute name="padding-bottom">1mm</xsl:attribute>
					<xsl:attribute name="padding-left">1mm</xsl:attribute>
					<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
		 			<xsl:attribute name="text-align">right</xsl:attribute>
					<xsl:attribute name="padding-right">2mm</xsl:attribute>
					<xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:value-of select="ListedAssetValue"/></xsl:element></xsl:element>				
			</xsl:element>								
		</xsl:for-each>	
	</xsl:template>
	
	<xsl:template name="PassifDetail">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:call-template name="passifColumns"/>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="tableFont"/>
				<xsl:element name="fo:table-row"><!-- PAS 4, 5 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">Credit cards </xsl:element><xsl:element name="fo:block">and lined of </xsl:element><xsl:element name="fo:block">credit</xsl:element>
						</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotLiabCreditCardMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totLiabCreditCard"/></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 6-9 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 11-14 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">5</xsl:attribute>
						<xsl:element name="fo:block">Mortgages</xsl:element><xsl:element name="fo:block">  and personal </xsl:element><xsl:element name="fo:block"> loans</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotLiabMtgMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totLiabMtg"/></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 15 -18 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 19 -22 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 23 -26 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 27 -30 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowEmpty"/><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:attribute name="number-columns-spanned">5</xsl:attribute>
						<xsl:element name="fo:block">Other </xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row"><!-- PAS 32 -35 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>					
						<xsl:element name="fo:block">Rentals</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotLiabLeaseMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totLiabLease"/></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 36 -39 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>					
						<xsl:element name="fo:block">Pensions</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotLiabChildMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totLiabChild"/></xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row"><!-- PAS 40 -43 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>					
						<xsl:element name="fo:block">Surety</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:element name="fo:table-row"><!-- PAS 51 -55 -->
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>					
						<xsl:element name="fo:block"></xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/>
						<xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/TotOtherLiabMonthly"/></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttrNar"/><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totOtherLiab"/></xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">TOTAL MONTHLY OBLIGATIONS 
							<xsl:element name="inline"><xsl:copy use-attribute-sets="DollarAmntFormat"/><xsl:text>                          </xsl:text>
								<xsl:value-of select="//specialRequirementTags/TotalMonthlyLiabilities"/></xsl:element>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute><xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">TOTAL LIABILITIES</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-rows-spanned">2</xsl:attribute><xsl:copy use-attribute-sets="DollarAmntFormat"/>
						<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totalLiabilitySide"/></xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/>
					<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">GDS SUMMARY
						<xsl:element name="inline"><xsl:copy use-attribute-sets="DollarAmntFormat"/><xsl:text>                          </xsl:text>
							<xsl:value-of select="//Deal/combinedGDS"/></xsl:element>
					</xsl:element></xsl:element>
				</xsl:element>				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="border-after-width">0px</xsl:attribute>
					<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
						<xsl:element name="fo:block">TDS SUMMARY 
						<xsl:element name="inline"><xsl:copy use-attribute-sets="DollarAmntFormat"/><xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/combinedTDS"/></xsl:element>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="border-after-width">0px</xsl:attribute>
					<xsl:attribute name="number-columns-spanned">2</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:element name="fo:block">NET WORTH</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="border-after-width">0px</xsl:attribute>
						<xsl:copy use-attribute-sets="DollarAmntFormat"/>
							<xsl:element name="fo:block"><xsl:value-of select="//specialRequirementTags/totalNetWorth"/></xsl:element></xsl:element>
				</xsl:element>								
		</xsl:element></xsl:element>		
	</xsl:template>
	
<!-- #########################################################################################################  -->		

	<xsl:template name="CreateBottomTable">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute><xsl:attribute name="padding-top">4mm</xsl:attribute>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">93mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-column"><xsl:attribute name="column-width">93mm</xsl:attribute></xsl:element>
		<xsl:element name="fo:table-body">
			<xsl:element name="fo:table-row"><xsl:attribute name="keep-together">always</xsl:attribute>
				<xsl:element name="fo:table-cell">
				    <xsl:call-template name="QuestionsGuarantorOther"/>		
				 </xsl:element>   
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
				<xsl:element name="fo:table-cell">				    
				    <xsl:call-template name="QuestionsBankruptcyStatus"/>							 
			</xsl:element></xsl:element>
		</xsl:element></xsl:element>
	</xsl:template>
	
	<xsl:template name="QuestionsGuarantorOther">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute><xsl:copy use-attribute-sets="tableFont"/>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">65mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">12mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">12mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="rowAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="padding-bottom">1mm</xsl:attribute>
						Are you currently a guarantor?
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
							<xsl:value-of select="concat(borrowerFirstName,' ', borrowerLastName)"/>
						</xsl:element></xsl:element>
						<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
						<xsl:call-template name="IsGuarantorChkBox"><!-- <xsl:with-param name="YN_var" select="guarantorOtherLoans"/> --> </xsl:call-template>
					</xsl:element>
				</xsl:for-each>	
		</xsl:element></xsl:element>
	</xsl:template>			
	
	<xsl:template name="QuestionsBankruptcyStatus">
		<xsl:element name="fo:table"><xsl:attribute name="table-layout">fixed</xsl:attribute><xsl:copy use-attribute-sets="tableFont"/>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">65mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4mm</xsl:attribute></xsl:element>
				<xsl:element name="fo:table-column"><xsl:attribute name="column-width">24mm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:copy use-attribute-sets="rowAttr"/>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="padding-bottom">1mm</xsl:attribute>
						Bankruptcy status
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"></xsl:element></xsl:element>
				</xsl:element>
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:element name="fo:table-row">
						<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
							<xsl:value-of select="concat(borrowerFirstName,' ', borrowerLastName)"/>
						</xsl:element></xsl:element>
						<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:attribute name="space-before.optimum">1mm</xsl:attribute></xsl:element></xsl:element>
						<xsl:call-template name="BankruptcyStatusEn"/>
					</xsl:element>
				</xsl:for-each>	
		</xsl:element></xsl:element>
	</xsl:template>			

	<!-- this template returns two cells (columns) -->
	<xsl:template name="IsGuarantorChkBox">
		<xsl:choose>
			<xsl:when test="guarantorOtherLoans='Y'">
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="CheckedCheckbox"/>  Yes</xsl:element></xsl:element>	
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="UnCheckedCheckbox"/> No</xsl:element></xsl:element>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="UnCheckedCheckbox"/>  Yes</xsl:element></xsl:element>	
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:call-template name="CheckedCheckbox"/> No</xsl:element></xsl:element>	
			</xsl:otherwise>
		</xsl:choose>				
	</xsl:template>
	
	<!-- this template returns 1 cells (column) -->
	<xsl:template name="BankruptcyStatusEn">
		<xsl:choose>
			<xsl:when test="bankruptcyStatusID='0'">
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Never bankrupt</xsl:element></xsl:element>	
			</xsl:when>
			<xsl:when test="bankruptcyStatusID='1'">
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Fully discharged</xsl:element></xsl:element>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell"><xsl:element name="fo:block">Partially discharged</xsl:element></xsl:element>	
			</xsl:otherwise>
		</xsl:choose>				
	</xsl:template>

<!-- #########################################################################################################  -->		

	
	<xsl:template name="OriginRow">
		<xsl:element name="fo:table-row"><xsl:copy use-attribute-sets="tableFont"/>
			<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="number-columns-spanned">14</xsl:attribute>
				<xsl:element name="fo:block">Origin:</xsl:element></xsl:element>
			<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">Folio : </xsl:element></xsl:element>
			<xsl:element name="fo:table-cell"><xsl:copy use-attribute-sets="rowAttr"/><xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">Transit :	</xsl:element></xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateEmploymentHistorySection">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
			<xsl:attribute name="font-size">8pt</xsl:attribute><xsl:attribute name="space-before.optimum">2mm</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>	
				EMPLOYMENT INFORMATION
		</xsl:element>
		<!-- Line 1 -->
		<xsl:element name="fo:table"><xsl:attribute name="text-align">left</xsl:attribute><xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body">
				<!-- &&& Repeat for all sorted borrowers. There should be 4 of them. -->
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:if test="./borrowerTypeId=0">
						<xsl:for-each select="./EmploymentHistory">							
								<xsl:if test="./employmentHistoryStatusId = 0">
									<xsl:call-template name="EmploymentHistoryTableRowOne"></xsl:call-template>
								</xsl:if>
						</xsl:for-each>
					</xsl:if>					
					<xsl:if test="./borrowerTypeId=0">
						<xsl:for-each select="./EmploymentHistory">							
								<xsl:if test="./employmentHistoryStatusId = 1">
									<xsl:if test="./monthsAtCurrentEmployment &lt; 36">
										<xsl:call-template name="EmploymentHistoryTableRowTwo"></xsl:call-template>
									</xsl:if>
								</xsl:if>
								<xsl:call-template name="EmploymentHistoryTableRowThree"></xsl:call-template>	
						</xsl:for-each>
					</xsl:if>																						
				</xsl:for-each>
				<!-- <xsl:call-template name="BorrowerAddressTableRow"></xsl:call-template>	
				<xsl:call-template name="BorrowerTableRowMaritalStatus"></xsl:call-template>		
				<xsl:call-template name="BorrowerTableRowResidentialStatus"></xsl:call-template>	
				<xsl:call-template name="BorrowerTableRowResidentialStatusTwo"></xsl:call-template>
				<xsl:call-template name="PreviousAddressesList"></xsl:call-template> -->
			</xsl:element>
		</xsl:element>
		<!-- ====================== -->
	</xsl:template>	
	<!-- ================================================================== -->
	<xsl:template name="EmploymentHistoryTableRowOne">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Applicant's current employer
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./employerName"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Telephone #
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./Contact/contactPhoneNumber"></xsl:value-of><xsl:if test="./Contact/contactPhoneNumberExtension"><xsl:text> x </xsl:text><xsl:value-of select="./Contact/contactPhoneNumberExtension"></xsl:value-of></xsl:if>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Postion
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./jobTitle"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						For
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>					
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="../monthsAtCurrentEmployment"></xsl:value-of> 
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:choose>
					<xsl:when test="./employmentHistoryStatusId=0">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Perm.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Temp.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Other</xsl:text> 
						</xsl:element>																	
					</xsl:when>
					<xsl:when test="./employmentHistoryStatusId=1">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Perm.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Temp.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Other</xsl:text> 
						</xsl:element>					
					</xsl:when>					
					<xsl:otherwise>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Perm.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Temp.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Other</xsl:text> 
						</xsl:element>					
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<!-- end of cell -->	
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Gross monthly income
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./Income/monthlyIncomeAmount"></xsl:value-of> 
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->								
		</xsl:element>	
	</xsl:template>
	<!-- ================================================================== -->
	<xsl:template name="EmploymentHistoryTableRowTwo">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Employeur precedent
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./employerName"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Telephone #
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./Contact/contactPhoneNumber"></xsl:value-of><xsl:text> x </xsl:text><xsl:value-of select="./Contact/contactPhoneNumberExtension"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Postion
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="./jobTitle"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						For
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>					
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
						<xsl:value-of select="../monthsAtCurrentEmployment"></xsl:value-of> 
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:choose>
					<xsl:when test="./employmentHistoryStatusId=0">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Perm.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Temp.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Other</xsl:text> 
						</xsl:element>																	
					</xsl:when>
					<xsl:when test="./employmentHistoryStatusId=1">
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Perm.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Temp.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Other</xsl:text> 
						</xsl:element>					
					</xsl:when>					
					<xsl:otherwise>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Perm.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Temp.</xsl:text> 
						</xsl:element>					
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Other</xsl:text> 
						</xsl:element>					
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<!-- end of cell -->	
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Gross monthly income
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./Income/monthlyIncomeAmount"></xsl:value-of> 
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->								
		</xsl:element>	
	</xsl:template>
	<!-- ================================================================== -->
	<!-- ================================================================== -->
	<xsl:template name="EmploymentHistoryTableRowThree">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">19</xsl:attribute>
				
					<xsl:element name="fo:block">
						<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
						<xsl:attribute name="font-size">8pt</xsl:attribute>
						<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
						<xsl:attribute name="keep-together">always</xsl:attribute>
							Other income (pension, etc.)
					</xsl:element>
									
					<xsl:for-each select="./Income">
						<xsl:choose>
							<xsl:when test="./incomeTypeId=0"></xsl:when>
							<xsl:when test="./incomeTypeId=1"></xsl:when>
							<xsl:when test="./incomeTypeId=2"></xsl:when>
							<xsl:otherwise>
								<xsl:element name="fo:block">
									<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
									<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
									<xsl:attribute name="keep-together">always</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
										<xsl:value-of select="./incomeDescription"></xsl:value-of><xsl:text>   </xsl:text><xsl:value-of select="./incomeAmount"></xsl:value-of>
								</xsl:element>							
							</xsl:otherwise>
						</xsl:choose>					
					</xsl:for-each>
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	<!-- ================================================================== -->		
	<xsl:template name="EmploymentHistoryTableRowTwoZZCommented">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">19</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
							Previous address (if less than 3 years at current address:
				</xsl:element>
				<xsl:if test="//DealAdditional/primBorMonthsAtCurrentAddress &lt; 36">
					<xsl:for-each select="//DealAdditional/Borrower/BorrowerAddress">
						<xsl:if test="./borrowerAddressTypeId = 1">
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
								<xsl:attribute name="font-size">9pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="./Address/addressLine1"></xsl:value-of>
									<xsl:if test="./Address/addressLine2">
										<xsl:text>, </xsl:text><xsl:value-of select="./Address/addressLine2"></xsl:value-of>
									</xsl:if>
									<xsl:text>, </xsl:text><xsl:value-of select="./Address/city"></xsl:value-of>
									<xsl:text>, </xsl:text><xsl:value-of select="./Address/province"></xsl:value-of>
									<xsl:text>, </xsl:text><xsl:value-of select="./Address/postalFSA"></xsl:value-of>
									<xsl:text>  </xsl:text><xsl:value-of select="./Address/postalLDU"></xsl:value-of>
							</xsl:element>														
						</xsl:if>
					</xsl:for-each>
				</xsl:if>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	
	<!-- ================================================================== -->
	<xsl:template name="CreateBorrowerSection">
		<xsl:element name="fo:block">
			<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute>
			<xsl:attribute name="font-weight">bold</xsl:attribute>			
			<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
			<xsl:attribute name="keep-together">always</xsl:attribute>	
				APPLICANT INFORMATION
		</xsl:element>
		<!-- Line 1 -->
		<xsl:element name="fo:table">
			<xsl:attribute name="text-align">left</xsl:attribute>
			<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">1cm</xsl:attribute></xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:call-template name="OriginRow"></xsl:call-template>	
				<!-- &&& Repeat for all sorted borrowers. There should be 4 of them. -->
				<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
					<xsl:call-template name="BorrowerTableRowOne"></xsl:call-template>					
				</xsl:for-each>
				<xsl:call-template name="BorrowerAddressTableRow"></xsl:call-template>	
				<xsl:call-template name="BorrowerTableRowMaritalStatus"></xsl:call-template>		
				<xsl:call-template name="BorrowerTableRowResidentialStatus"></xsl:call-template>	
				<xsl:call-template name="BorrowerTableRowResidentialStatusTwo"></xsl:call-template>
				<xsl:call-template name="PreviousAddressesList"></xsl:call-template>
			</xsl:element>
		</xsl:element>
		<!-- ====================== -->
	</xsl:template>	

	<!-- ===========================  -->
	<xsl:template name="PreviousAddressesList">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">19</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Previous address (if less than 3 years) :
				</xsl:element>
				<xsl:if test="//DealAdditional/primBorMonthsAtCurrentAddress &lt; 36">
					<xsl:for-each select="//DealAdditional/Borrower/BorrowerAddress">
						<xsl:if test="./borrowerAddressTypeId = 1">
							<xsl:element name="fo:block">
								<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
								<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
								<xsl:attribute name="space-before.optimum">1pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
								<xsl:attribute name="keep-together">always</xsl:attribute>
									<xsl:value-of select="./Address/addressLine1"></xsl:value-of>
									<xsl:if test="./Address/addressLine2">
										<xsl:text>, </xsl:text><xsl:value-of select="./Address/addressLine2"></xsl:value-of>
									</xsl:if>
									<xsl:if test="./Address/city"><xsl:text>, </xsl:text><xsl:value-of select="./Address/city"/></xsl:if>
									<xsl:if test="./Address/province"><xsl:text>, </xsl:text><xsl:value-of select="./Address/province"/></xsl:if>
									<xsl:if test="./Address/postalFSA"><xsl:text>, </xsl:text><xsl:value-of select="./Address/postalFSA"/></xsl:if>
									<xsl:if test="./Address/postalLDU"><xsl:text>  </xsl:text><xsl:value-of select="./Address/postalLDU"/></xsl:if>
							</xsl:element>														
						</xsl:if>
					</xsl:for-each>
				</xsl:if>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->
	<xsl:template name="BorrowerAddressTableRow">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">10</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Address	
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:value-of select="//DealAdditional/BorrowerAddress/Address/addressLine1"/>
					<xsl:if test="//DealAdditional/BorrowerAddress/Address/addressLine2">
						<xsl:text>, </xsl:text>
						<xsl:value-of select="//DealAdditional/BorrowerAddress/Address/addressLine2"></xsl:value-of>					
					</xsl:if>
					<xsl:text>, </xsl:text><xsl:value-of select="//DealAdditional/BorrowerAddress/Address/city"></xsl:value-of>
					<xsl:text>, </xsl:text><xsl:value-of select="//DealAdditional/BorrowerAddress/Address/province"></xsl:value-of>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Postal code</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="//DealAdditional/BorrowerAddress/Address/postalFSA"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="//DealAdditional/BorrowerAddress/Address/postalLDU"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Telephone #</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="//DealAdditional/Borrower/borrowerHomePhoneNumber"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->			
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->
	<xsl:template name="BorrowerTableRowMaritalStatus">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">10</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Civil Status	
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="//DealAdditional/Borrower/maritalStatusId=0">
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>&#160;  Single &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160;  Married &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160;  Divorced &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160;  Other &#160;&#160;</xsl:text>
						</xsl:when>
						<xsl:when test="//DealAdditional/Borrower/maritalStatusId=2">
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text> &#160; Single &#160;&#160;</xsl:text>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>&#160;  Married &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160;  Divorced &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160; Other &#160;&#160;</xsl:text>
						</xsl:when>						
						<xsl:when test="//DealAdditional/Borrower/maritalStatusId=5">
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160;  Single &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160; Married &#160;&#160;</xsl:text>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>&#160; Divorced &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160; Other &#160;&#160;</xsl:text>
						</xsl:when>						
						<xsl:otherwise>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160; Single &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160; Married &#160;&#160;</xsl:text>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>&#160; Divorced &#160;&#160;</xsl:text>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>&#160; Other &#160;&#160;</xsl:text>						
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">9</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Dependents</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="//DealAdditional/Borrower/numberOfDependents"></xsl:value-of>						
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->
	<xsl:template name="BorrowerTableRowResidentialStatusTwo">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="//DealAdditional/BorrowerAddress/residentialStatusId=1">
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Tenant </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Tenant </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>For</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:if test="//DealAdditional/BorrowerAddress/residentialStatusId != 1">
						<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
						     <xsl:choose><xsl:when test="position()=1">
									<xsl:value-of select="./monthsAtCurrentAddress"/>
							</xsl:when>	</xsl:choose>						
						</xsl:for-each>	
					</xsl:if>	
				</xsl:element>				
			</xsl:element>						
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Rent</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:for-each select="//DealAdditional/Borrower/Liability">
							<xsl:if test="./liabilityTypeId=16">
								<xsl:value-of select="./liabilityMonthlyPayment"></xsl:value-of>
							</xsl:if>
						</xsl:for-each>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Home owner</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:for-each select="//DealAdditional/Borrower/Liability">
							<xsl:if test="./liabilityTypeId=16">
								<xsl:value-of select="./liabilityDescription"></xsl:value-of>
							</xsl:if>
						</xsl:for-each>
				</xsl:element>				
			</xsl:element>			
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Telephone #</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text> </xsl:text>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->					
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->
	<xsl:template name="BorrowerTableRowResidentialStatus">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="//DealAdditional/BorrowerAddress/residentialStatusId=1">
							<xsl:call-template name="CheckedCheckbox"></xsl:call-template><xsl:text>  Landlord </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template><xsl:text>  Landlord </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>For</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:if test="//DealAdditional/BorrowerAddress/residentialStatusId = 1">					
						<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
						     <xsl:choose><xsl:when test="position()=1">
									<xsl:value-of select="./monthsAtCurrentAddress"/>
							</xsl:when>	</xsl:choose>						
						</xsl:for-each>	
					</xsl:if>	
				</xsl:element>				
			</xsl:element>						
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">4</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Mortgage payment</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:for-each select="//DealAdditional/Borrower/Liability">
							<xsl:if test="./liabilityTypeId=0">
								<xsl:value-of select="./liabilityMonthlyPayment"></xsl:value-of>
							</xsl:if>
						</xsl:for-each>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Creditor</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:for-each select="//DealAdditional/Borrower/Liability">
							<xsl:if test="./liabilityTypeId=0">
								<xsl:value-of select="./liabilityDescription"></xsl:value-of>
							</xsl:if>
						</xsl:for-each>
				</xsl:element>				
			</xsl:element>			
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Telephone #</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:text> </xsl:text>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->					
		</xsl:element>	
	</xsl:template>
	
	<!-- ===========================  -->	
	
	<xsl:template name="BorrowerTableRowTwo">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">8</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Name (last, first)
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
					<xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:value-of select="//DealAdditional/Borrower/borrowerFirstName"/>
					<xsl:text/>
					<xsl:value-of select="//DealAdditional/Borrower/borrowerLastName"/>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="display-align">center</xsl:attribute>
				<xsl:attribute name="border">solid black 0.5px</xsl:attribute><xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="./borrowerGenderId='1'">
							<xsl:call-template name="CheckedCheckbox"/><xsl:text>  M &#160;&#160;</xsl:text><xsl:call-template name="UnCheckedCheckbox"/><xsl:text> F</xsl:text>
						</xsl:when>
						<xsl:when test="./borrowerGenderId='2'">
							<xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  M &#160;&#160;</xsl:text><xsl:call-template name="CheckedCheckbox"/><xsl:text> F </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  M    </xsl:text><xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  F </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Date of birth</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./borrowerBirthDate"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">6</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>					
					<xsl:text>Social insurance number</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./socialInsuranceNumber"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	<!-- ===========================  -->
	<xsl:template name="BorrowerTableRowOne">
		<xsl:element name="fo:table-row">
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">start</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">9</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute>
					<xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						Name (last, first)	
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">9pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1pt</xsl:attribute><xsl:attribute name="space-after.optimum">1pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:value-of select="./borrowerFirstName"/><xsl:text> </xsl:text><xsl:value-of select="./borrowerLastName"/>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="display-align">center</xsl:attribute>
				<xsl:attribute name="border">solid black 0.5px</xsl:attribute><xsl:attribute name="number-columns-spanned">2</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:choose>
						<xsl:when test="./borrowerGenderId='1'">
							<xsl:call-template name="CheckedCheckbox"/><xsl:text>  M   </xsl:text><xsl:call-template name="UnCheckedCheckbox"/><xsl:text> F</xsl:text>
						</xsl:when>
						<xsl:when test="./borrowerGenderId='2'">
							<xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  M   </xsl:text><xsl:call-template name="CheckedCheckbox"/><xsl:text> F</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  M    </xsl:text><xsl:call-template name="UnCheckedCheckbox"/><xsl:text>  F</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">3</xsl:attribute> 
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Date of birth</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./borrowerBirthDate"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
			<xsl:element name="fo:table-cell">
				<xsl:attribute name="padding-top">0mm</xsl:attribute><xsl:attribute name="padding-bottom">0mm</xsl:attribute>
				<xsl:attribute name="padding-right">0mm</xsl:attribute><xsl:attribute name="padding-left">1mm</xsl:attribute>
				<xsl:attribute name="text-align">center</xsl:attribute><xsl:attribute name="border">solid black 0.5px</xsl:attribute>
				<xsl:attribute name="number-columns-spanned">5</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute>
					<xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:text>Social insurance number</xsl:text>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute>
					<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
						<xsl:value-of select="./socialInsuranceNumber"></xsl:value-of>
				</xsl:element>				
			</xsl:element>
			<!-- end of cell -->
		</xsl:element>	
	</xsl:template>
	
	<!-- ================================================================== -->
	<!-- ### -->
	<!-- ================================================================== -->	
	<xsl:template name="CreatePageOne">
		<xsl:element name="page-sequence">
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-before</xsl:attribute>
				<!-- <xsl:call-template name="CreateLogoLine"/> -->
			</xsl:element>
			<xsl:element name="fo:static-content"><xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block"><xsl:attribute name="space-before.optimum">3mm</xsl:attribute><xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">8pt</xsl:attribute>
					<xsl:attribute name="keep-together">always</xsl:attribute>
					Page <fo:page-number/> of <fo:page-number-citation ref-id="{generate-id(/)}"/> </xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<!-- <xsl:call-template name="CreateLogoLine"></xsl:call-template>	 -->
				<xsl:call-template name="CreateTitleLine"/>
				<xsl:call-template name="CreateBorrowerSection"/>
				<xsl:call-template name="CreateEmploymentHistorySection"/>
				<xsl:call-template name="CreateActifPassifSection"/>
				<xsl:call-template name="CreateBottomTable"/>
				<fo:block id="{generate-id(/)}" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- ================================================================== -->	

	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>
	<xsl:template name="FOStart">
		<xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element>
		<xsl:element name="fo:root">
			<xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute>
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">40mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">15mm</xsl:attribute>
						<xsl:attribute name="margin-top">10mm</xsl:attribute>
						<xsl:attribute name="margin-left">10mm</xsl:attribute>
						<xsl:attribute name="margin-right">10mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageOne"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
