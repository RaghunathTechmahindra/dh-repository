<?xml version="1.0" encoding="UTF-8"?>
<!--
21/Nov/2007 DVG #DG664 FXP19308: Solicitor's package was missing guarantor information 
04/May/2006 DVG #DG416 #1965  Xeed Solicitor Package Middle Name initial is not shown  
	- a final transformation to align upload to the schema, before sending to ldd.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<!-- this could be used, but just for optimization sake, will make 
  a special call for the root (below)	
  xsl:template match="/">
	  <xsl:call-template name="filterNode"/>
	</xsl:template-->

	<xsl:template match="/">
		<xsl:element name="Document">
			<xsl:for-each select="/*/@*">
		    <!--xsl:message>used is: <xsl:value-of select="."/></xsl:message-->
			  <xsl:call-template name="createAtr"/>
			</xsl:for-each>		

			<xsl:for-each select="/*/*">
    		<xsl:if test="name() != 'Deal'">
				  <xsl:call-template name="filterNode"/>
    		</xsl:if>
			</xsl:for-each>		
		</xsl:element>
	</xsl:template>

	<xsl:template name="filterNode">
		<xsl:if test="name() != 'Privilege' and name() != 'conditionId' 
      and name() != 'BrokerConditions' and name() != 'Premium' 
      and name() != 'PST' and name() !='priorAdvanceDate'
      and name() != 'IADAmountMinus5' and name() != 'TotalDeductionsAmountMinus5' 
      and name() != 'NetAdvanceAmount' and name() != 'AdminFee'      
      and name() != 'AdvanceAmount' and name() != 'userDescription'       
      and name() != 'OpenStatem' and name() != 'BorIdTxt'
      and name() != 'FundingTxt' and name() != 'ConfirmClosing'
      and name() != 'TitleInsReq' and name() != 'Searches'
      and name() != 'SurveyReq' and name() != 'CondoStrataTitle'
      and name() != 'FireInsur' and name() != 'NewConstructionTxt'
      and name() != 'ConstructionProgrAdv' and name() != 'AssignmentRents'
      and name() != 'Disclosure' and name() != 'PlanningAct'
      and name() != 'MatrimonialPropAct' and name() != 'Renew4Guarantors'
      and name() != 'CorporationAsBor' and name() != 'SolicitorRpt'
      and name() != 'RegisteredName' and name() != 'RegisteredAddress'
      and name() != 'RegisteredPhoneFax' 
      ">
      <!-- #DG664 send guarantor names 
        and name()!='GuarantorNames' -->
		  <xsl:call-template name="createNode"/>
		</xsl:if>
	</xsl:template>

	<xsl:template name="createNode">
		<xsl:element name="{name()}">		
  		<xsl:for-each select="./@*">
  		  <xsl:call-template name="createAtr"/>
  		</xsl:for-each>		
  		
    	<xsl:choose>
    		<xsl:when test="count(./*)=0">
			     <xsl:value-of select="."/>
    		</xsl:when>
    		<xsl:otherwise>
    			<xsl:for-each select="./*">
    				  <xsl:call-template name="filterNode"/>
    			</xsl:for-each>
    		</xsl:otherwise>
    	</xsl:choose>
		</xsl:element>
	</xsl:template>

	<xsl:template name="createAtr">
		<xsl:attribute name="{name()}">
	     <xsl:value-of select="."/>
		</xsl:attribute>
	</xsl:template>

</xsl:stylesheet>
