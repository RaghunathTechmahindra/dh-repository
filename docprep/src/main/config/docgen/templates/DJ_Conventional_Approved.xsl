<?xml version="1.0" encoding="UTF-8"?>
<!-- Author Zivko Radulovic -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xmlns="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
<!--		<xsl:call-template name="RTFFileStart"/>
		<xsl:call-template name="Test01"/>
		<xsl:call-template name="RTFFileEnd"/> -->
	</xsl:template>
	
<xsl:template name="CreatePageOne">
	<xsl:element name="page-sequence">
		<xsl:attribute name="master-reference">main</xsl:attribute>
		<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-before</xsl:attribute>
					<xsl:call-template name="CreateLogoLine"></xsl:call-template>	
			</xsl:element>
			
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
					<!-- <xsl:call-template name="CreateLogoLine"></xsl:call-template>	 -->
					<xsl:call-template name="CreateTitleLine"></xsl:call-template>
					<xsl:call-template name="CreateDateLines"></xsl:call-template>
					<xsl:if test="//Deal/systemTypeId='45'">
						<!-- real estate type of deal -->
						<xsl:if test="//Deal/borrowerSection/primaryBorrower/borrowerTypeId='0'">
							<xsl:if test="count(//specialRequirementTags/realEstateData)=1">
								<xsl:call-template name="CreateRealEstateLine"></xsl:call-template>
								<xsl:call-template name="CreateFaxLineForRealEstate"></xsl:call-template> 
							</xsl:if>
						</xsl:if>
						<!-- Construcotr deal -->
						<xsl:if test="//Deal/borrowerSection/primaryBorrower/borrowerTypeId='3'">
							<xsl:call-template name="CreateConstructorLine"></xsl:call-template>
							<xsl:call-template name="CreateFaxLineForBuilder"></xsl:call-template>
						</xsl:if>
					</xsl:if>
					<xsl:call-template name="CreateObjetLine"></xsl:call-template>
					<xsl:call-template name="CreateMainText"></xsl:call-template> 
					<!-- <xsl:call-template name="CreatePageTwo"></xsl:call-template> -->
			</xsl:element>
	</xsl:element>
</xsl:template>

<!-- <xsl:template name="CreatePageThree">
	<xsl:element name="page-sequence">
		<xsl:attribute name="master-reference">main</xsl:attribute>
		<xsl:attribute name="language">en</xsl:attribute>		
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
					<xsl:element name="block-container">
						<xsl:attribute name="break-before">page</xsl:attribute>
						<xsl:attribute name="position">absolute</xsl:attribute>
						<xsl:attribute name="top">1mm</xsl:attribute>
						<xsl:attribute name="left">50mm</xsl:attribute>
						<xsl:attribute name="height">1.22in</xsl:attribute>
						<xsl:attribute name="width">3.13in</xsl:attribute>

							<xsl:element name="fo:external-graphic">
								<xsl:attribute name="src">f:\temporary\DJLogo.gif</xsl:attribute>
								<xsl:attribute name="height">1.22in</xsl:attribute>
								<xsl:attribute name="width">3.13in</xsl:attribute>
							</xsl:element>						
					</xsl:element>
			</xsl:element> 			
	</xsl:element>
</xsl:template> -->

<xsl:template name="CreatePageTwo">
	<xsl:element name="page-sequence">
		<xsl:attribute name="master-reference">main</xsl:attribute>
		<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
					<xsl:call-template name="CreatePageTwoTopLine"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoTable"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoCCLine"></xsl:call-template>
			</xsl:element>
	</xsl:element>
</xsl:template>

<xsl:template name="CreatePageTwoCCLine">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>		
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="concat('cc:    ', //Deal/sourceOfBusinessProfile/Contact/contactFirstName, '   ', //Deal/sourceOfBusinessProfile/Contact/contactLastName , '  représentant(e) hypothécaire ' )"></xsl:value-of>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreatePageTwoTable">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<!-- <xsl:attribute name="space-before">0.1in</xsl:attribute> -->
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">12cm</xsl:attribute>
			</xsl:element>			
			<xsl:element name="fo:table-body">
				<!-- TABLE ROW 1-->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-top">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-left">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Propriété
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-top">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-right">solid black 1px</xsl:attribute>
							<xsl:choose>
								<xsl:when test="//Deal/propertySection/primaryProperty/unitNumber">
									<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
										<xsl:value-of select="concat(//Deal/propertySection/primaryProperty/unitNumber, ' - ', //Deal/propertySection/primaryProperty/propertyStreetNumber, ' ', //Deal/propertySection/primaryProperty/propertyStreetName, ' ' , //Deal/propertySection/primaryProperty/propertyStreetType, ' ' ,  //Deal/propertySection/primaryProperty/propertyStreetType)"></xsl:value-of>
									</xsl:element>																	
								</xsl:when>
								<xsl:otherwise>
									<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
										<xsl:value-of select="concat(//Deal/propertySection/primaryProperty/propertyStreetNumber, ' ', //Deal/propertySection/primaryProperty/propertyStreetName, ' ' , //Deal/propertySection/primaryProperty/propertyStreetType, ' ' ,  //Deal/propertySection/primaryProperty/propertyStreetType)"></xsl:value-of>
									</xsl:element>																									
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="//Deal/primaryProperty/propertyAddressLine2">
								<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
									<xsl:value-of select="//Deal/propertySection/primaryProperty/propertyAddressLine2"></xsl:value-of>
								</xsl:element>										
							</xsl:if>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="concat( //Deal/propertySection/primaryProperty/propertyCity, ' ', //Deal/propertySection/primaryProperty/propertyProvince )"></xsl:value-of>
							</xsl:element>								
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="concat( //Deal/propertySection/primaryProperty/propertyPostalFSA, ' ', //Deal/propertySection/primaryProperty/propertyPostalLDU)"></xsl:value-of>
							</xsl:element>								
					</xsl:element>					
				</xsl:element>
				<!-- TABLE ROW 2-->				
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-left">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Coût d'achat :
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-right">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="//Deal/totalPurchasePrice"></xsl:value-of>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>		
				<!-- TABLE ROW 3 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-left">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Mise de fonds :
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-right">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="//Deal//downPaymentAmount"></xsl:value-of>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>		
				<!-- TABLE ROW 4 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-left">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Montant total du prêt :
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-right">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="//Deal//totalLoanAmount"></xsl:value-of>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>		
				<!-- TABLE ROW 5 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-left">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Taux annuel :
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-right">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="//Deal//netInterestRate"></xsl:value-of>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>					
				<!-- TABLE ROW -->						
				<!-- TABLE ROW 6 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-bottom">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-left">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-top">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Garantie
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-right">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-bottom">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-top">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Referez au conditions
							</xsl:element>										
					</xsl:element>					
				</xsl:element>					
				<!-- TABLE ROW -->										
				<!-- TABLE ROW 6 -->
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-bottom">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-left">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-top">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Financement :
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
						<xsl:attribute name="border-right">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-bottom">solid black 1px</xsl:attribute>
						<xsl:attribute name="border-top">solid black 1px</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="//Deal/branchProfile/branchName"></xsl:value-of>
							</xsl:element>										
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="//Deal/branchProfile/Contact/Address/addressLine1"></xsl:value-of>
							</xsl:element>							
							<xsl:if test="//Deal/branchProfile/Contact/Address/addressLine2">
								<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
									<xsl:value-of select="//Deal/branchProfile/Contact/Address/addressLine2"></xsl:value-of>
								</xsl:element>								
							</xsl:if>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="//Deal/branchProfile/Contact/Address/province"></xsl:value-of>
							</xsl:element>						
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								<xsl:value-of select="//Deal/branchProfile/Contact/Address/postalFSA"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="//Deal/branchProfile/Contact/Address/postalLDU"></xsl:value-of>
							</xsl:element>								
					</xsl:element>					
				</xsl:element>					
				<!-- TABLE ROW -->														
			</xsl:element>			
	</xsl:element></xsl:template>

<xsl:template name="CreatePageTwoTopLine">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">6cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">11cm</xsl:attribute>
			</xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">12pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="concat('Dossier:    ', //Deal/dealId )"></xsl:value-of>
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">12pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="concat( 'Demandeur:',//Deal/borrowerSection/primaryBorrower/borrowerFirstName, ' ',//Deal/borrowerSection/primaryBorrower/borrowerLastName)"></xsl:value-of>
							</xsl:element>										
					</xsl:element>															
				</xsl:element>
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreateMainText">
	<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">2em</xsl:attribute><xsl:attribute name="space-after.optimum">0.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
		La présente a pour but de confirmer l'acceptation du prêt hypothécaire conformément à l'offre de financement émise à 
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:choose>
				<xsl:when test="position()=1">
					<xsl:value-of select="./borrowerSalutation"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="./FirstName"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="./LastName"></xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="position()=last()">
							<xsl:text> et </xsl:text><xsl:value-of select="./borrowerSalutation"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="./FirstName"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="./LastName"></xsl:value-of>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>, </xsl:text><xsl:value-of select="./borrowerSalutation"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="./FirstName"></xsl:value-of><xsl:text> </xsl:text><xsl:value-of select="./LastName"></xsl:value-of>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>			
		</xsl:for-each>
		pour l'achat de la propriété ci-dessous mentionnée.
	</xsl:element>	
	<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">2em</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
		En espérant le tout conforme, veuillez agréer, monsieur(madame), l'expression de nos sentiments les meilleurs.	
	</xsl:element>
	<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-style">italic</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">2em</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
		<xsl:value-of select=" concat(//Deal/underwriter/Contact/contactFirstName , '   ', //Deal/underwriter/Contact/contactLastName )   "></xsl:value-of>
	</xsl:element>			
	<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
		<xsl:value-of select="//Deal/underwriter/userType"></xsl:value-of>
	</xsl:element>			
</xsl:template>

<xsl:template name="CreateLogoLine">																	
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">25mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">12pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:element name="fo:external-graphic">
									<xsl:attribute name="src">f:\temporary\DJLogo.gif</xsl:attribute>
									<xsl:attribute name="height">1.1in</xsl:attribute>
									<xsl:attribute name="width">2.86in</xsl:attribute>
								</xsl:element>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreateLogoLineOLD">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">14cm</xsl:attribute>
			</xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Centre de Financement au point de vente
							</xsl:element>										
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Fédération des caisses Desjardins du Quéebec
							</xsl:element>							
					</xsl:element>					
				</xsl:element>
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreateTitleLine">	
	<xsl:element name="fo:table">			
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="space-before">0.7in</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute> 
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-family">Times Roman</xsl:attribute><xsl:attribute name="font-size">14pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Avis - Acceptation d'un Prêt Hypothécaire Conventionnel
							</xsl:element>										
					</xsl:element>
				</xsl:element>
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreateDateLines">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="space-after">2em</xsl:attribute><xsl:attribute name="space-before">2em</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>	<xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>	
								Le  <xsl:value-of select="//General/CurrentDate"></xsl:value-of>
							</xsl:element>								
					</xsl:element>					
				</xsl:element>				
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreateRealEstateLine">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="concat(//specialRequirementTags/realEstateData/partyProfile/Contact/salutation,' ',//specialRequirementTags/realEstateData/partyProfile/Contact/contactFirstName, ' ' ,//specialRequirementTags/realEstateData/partyProfile/Contact/contactLastName)"></xsl:value-of> 
							</xsl:element>						
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="//specialRequirementTags/realEstateData/partyProfile/partyCompanyName"></xsl:value-of> 
							</xsl:element>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="//specialRequirementTags/realEstateData/partyProfile/Contact/Address/addressLine1"></xsl:value-of>
							</xsl:element>
							<xsl:if test="//specialRequirementTags/partyProfile/Contact/Address/addressLine2">
								<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
									<xsl:value-of select="//specialRequirementTags/realEstateData/partyProfile/Contact/Address/addressLine2"></xsl:value-of>
								</xsl:element>							
							</xsl:if>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="//specialRequirementTags/realEstateData/partyProfile/Contact/Address/city"></xsl:value-of>
							</xsl:element>							
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="concat( //specialRequirementTags/realEstateData/partyProfile/Contact/Address/postalFSA,' ', //specialRequirementTags/realEstateData/partyProfile/Contact/Address/postalLDU )"></xsl:value-of>
							</xsl:element>							
					</xsl:element>					
				</xsl:element>				
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreateFaxLineForRealEstate">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								Télécopieur :
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="//Deal/sourceOfBusinessProfile/Contact/contactFaxNumber"></xsl:value-of>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>				
			</xsl:element>			
	</xsl:element>
</xsl:template>


<xsl:template name="CreateFaxLineForBuilder">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								Télécopieur:
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="//Deal/borrowerSection/primaryBorrower/borrowerFaxNumber"></xsl:value-of>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>				
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreateConstructorLine">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">3cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								Constructeur:
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								<xsl:value-of select="//Deal/borrowerSection/primaryBorrower/borrowerFirstName"></xsl:value-of><xsl:text> </xsl:text>          <xsl:value-of select="//Deal/borrowerSection/primaryBorrower/borrowerLastName"></xsl:value-of>
							</xsl:element>										
					</xsl:element>					
				</xsl:element>				
			</xsl:element>			
	</xsl:element>
</xsl:template>

<xsl:template name="CreateObjetLine">	
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="space-before">0.7in</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">2cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>			
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-size">12pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute><xsl:attribute name="keep-together">always</xsl:attribute>								 
								Objet:
							</xsl:element>										
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Demande de financement hypothécaire 
							</xsl:element>										
							<xsl:element name="fo:block"><xsl:attribute name="line-height">12pt</xsl:attribute><xsl:attribute name="font-weight">bold</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>								 
								Numéro de dossier: <xsl:text> </xsl:text><xsl:value-of select="//Deal/dealId"></xsl:value-of>
							</xsl:element>							
					</xsl:element>					
				</xsl:element>				
			</xsl:element>			
	</xsl:element>
</xsl:template>


<xsl:template name="CreateDateLinesBCK">
	<xsl:element name="fo:table">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:attribute name="text-align">left</xsl:attribute>
							<xsl:element name="fo:block"><xsl:attribute name="space-before">10pt</xsl:attribute><xsl:attribute name="line-height">10pt</xsl:attribute><xsl:attribute name="font-size">10pt</xsl:attribute><xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute><xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute><xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>	<xsl:attribute name="keep-together">always</xsl:attribute>	
								Le:  <xsl:value-of select="//General/CurrentDate"></xsl:value-of>
							</xsl:element>										
					</xsl:element>
				</xsl:element>
			</xsl:element>			
	</xsl:element>
</xsl:template>


<xsl:template name="FOStart">
	<xsl:element name="?xml">
		<xsl:attribute name="version">1.0</xsl:attribute>
		<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
	</xsl:element>
	<xsl:element name="fo:root">
		<xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
		<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute>
		<xsl:element name="fo:layout-master-set">
			<xsl:element name="fo:simple-page-master">
				<xsl:attribute name="master-name">main</xsl:attribute>
				<xsl:attribute name="page-height">11in</xsl:attribute>
				<xsl:attribute name="page-width">8.5in</xsl:attribute>
				<xsl:attribute name="margin-top">0in</xsl:attribute>
				<xsl:attribute name="margin-bottom">1in</xsl:attribute>
				<xsl:attribute name="margin-left">0in</xsl:attribute>
				<xsl:attribute name="margin-right">1in</xsl:attribute>
				<xsl:element name="fo:region-before">
					<xsl:attribute name="extent">40mm</xsl:attribute>
					<xsl:attribute name="margin-left">25mm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:region-body">
					<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
					<xsl:attribute name="margin-top">25mm</xsl:attribute>
					<xsl:attribute name="margin-left">25mm</xsl:attribute>
					<xsl:attribute name="margin-right">25mm</xsl:attribute>
				</xsl:element>
				<xsl:element name="fo:region-after">
					<xsl:attribute name="extent">10mm</xsl:attribute>
				</xsl:element>								
			</xsl:element>
		</xsl:element>	
		<xsl:call-template name="CreatePageOne"></xsl:call-template>
		<xsl:call-template name="CreatePageTwo"></xsl:call-template>
		<!-- <xsl:call-template name="CreatePageThree"></xsl:call-template> -->
	</xsl:element>
</xsl:template>



<!-- ================================================================================= -->
<!--     START of RTF part     -->
<!-- ================================================================================= -->

<xsl:template name="Test01">

<xsl:text>\~\par</xsl:text>
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx2000
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx9468
\intbl \cell 
\intbl {\ql\f1\fs18 Centre de Financement au point de vente\par \ql Fédération des caisses Desjardins du Quéebec }\cell 
\pard \intbl\row
</xsl:text>
<xsl:text>\pard \par\par </xsl:text>
<xsl:text>
\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrs10
\clbrdrl\brdrs10
\clbrdrb\brdrs20
\clbrdrr\brdrs20\cellx9468
\intbl\qc {\f1\fs24\b  Avis - Acceptation d\rquote un Prêt Hypothécaire Conventionnel}\cell 
\pard \intbl\row
</xsl:text>
<!-- ===========================================================================  -->
<!-- header section  -->
<!-- ===========================================================================  -->
<xsl:text>\pard \par\par </xsl:text>
<xsl:text>Le </xsl:text>
<xsl:text>\pard \par\par </xsl:text>
<xsl:text>{\f1\fs20 Constructeur:}</xsl:text>
<xsl:text>\pard \par </xsl:text>
<xsl:text>{\f1\fs20 Télécopieur:}</xsl:text>
<xsl:text>\pard \par\par\par\par </xsl:text>
<!-- ===========================================================================  -->
<!-- RE: line  -->
<!-- ===========================================================================  -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx1000
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx9468
\intbl {\ql\f1\fs20\b Objet}\cell 
\intbl {\ql\f1\fs20\b Demande de financement hypothécaire\par Numéro de dossier: }\cell 
\pard \intbl\row
</xsl:text>
<xsl:text>\pard \par\par\par\par </xsl:text>
<!-- ===========================================================================  -->
<!-- Text on page 1 -->
<!-- ===========================================================================  -->
<xsl:text>{\f1\fs20 La présente a pour but de confirmer l'acceptation du prêt hypothécaire conformément à l'offre de financement émise à monsieur </xsl:text>
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:value-of select="./borrowerSalutation"/><xsl:text> </xsl:text><xsl:value-of select="./FirstName"></xsl:value-of><xsl:text> </xsl:text>
			<xsl:if test="position()=last()">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
<xsl:text>Eric Beauchamp, madame Caroline Lalonde et monsieur Bernard Beauchamp pour l'achat de la propriété ci-dessous mentionnée.} </xsl:text>
<xsl:text>\pard \par\par </xsl:text>
<xsl:text>{\f1\fs20 En espérant le tout conforme, veuillez agréer, monsieur(madame), l'expression de nos sentiments les meilleurs.}</xsl:text>
<xsl:text>\pard \par\par\par\par </xsl:text>
<xsl:text>{\f1\fs20 Analyste}</xsl:text>
<!-- ===========================================================================  -->
<!-- PAGE 2  -->
<!-- ===========================================================================  -->
<xsl:text>\pard \page \pard\par </xsl:text>
<xsl:text>{\f1\fs20\b Dossier: }</xsl:text>
<xsl:text>\par </xsl:text>
<!-- ===========================================================================  -->
<!-- First row in the table  -->
<!-- ===========================================================================  -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrs10
\clbrdrl\brdrs10
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx2500
\clbrdrt\brdrs10
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrs10\cellx9468
\intbl {\ql\f1\fs20 Propriété}\cell 
\intbl {\ql\f1\fs20 Property comes here\par line 2\par line 3 }\cell 
\pard \intbl\row
</xsl:text>
<!-- ===========================================================================  -->
<!-- Second row in the table  -->
<!-- ===========================================================================  -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs10
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx2500
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrs10\cellx9468
\intbl {\ql\f1\fs20 Coût d\rquote achat :}\cell 
\intbl {\ql\f1\fs20 </xsl:text><xsl:value-of select="//Deal/totalPurchasePrice"></xsl:value-of><xsl:text>}\cell 
\pard \intbl\row
</xsl:text>
<!-- ===========================================================================  -->
<!-- Third row in the table  -->
<!-- ===========================================================================  -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs10
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx2500
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrs10\cellx9468
\intbl {\ql\f1\fs20 Mise de fonds :}\cell 
\intbl {\ql\f1\fs20 </xsl:text><xsl:value-of select="//Deal//downPaymentAmount"></xsl:value-of><xsl:text> }\cell 
\pard \intbl\row
</xsl:text>
<!-- ===========================================================================  -->
<!-- Fourth row in the table  -->
<!-- ===========================================================================  -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs10
\clbrdrb\brdrnone
\clbrdrr\brdrnone\cellx2500
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrnone
\clbrdrr\brdrs10\cellx9468
\intbl {\ql\f1\fs20 Montant total du prêt :}\cell 
\intbl {\ql\f1\fs20 </xsl:text><xsl:value-of select="//Deal//totalLoanAmount"></xsl:value-of><xsl:text>}\cell 
\pard \intbl\row
</xsl:text>
<!-- ===========================================================================  -->
<!-- Fifth row in the table  -->
<!-- ===========================================================================  -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrnone
\clbrdrl\brdrs10
\clbrdrb\brdrs10
\clbrdrr\brdrnone\cellx2500
\clbrdrt\brdrnone
\clbrdrl\brdrnone
\clbrdrb\brdrs10
\clbrdrr\brdrs10\cellx9468
\intbl {\ql\f1\fs20 Taux annuel :}\cell 
\intbl {\ql\f1\fs20 </xsl:text><xsl:value-of select="//Deal//netInterestRate"></xsl:value-of><xsl:text>}\cell 
\pard \intbl\row
</xsl:text>
<!-- ===========================================================================  -->
<!-- Sixth row in the table  -->
<!-- ===========================================================================  -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrs10
\clbrdrl\brdrs10
\clbrdrb\brdrs10
\clbrdrr\brdrnone\cellx2500
\clbrdrt\brdrs10
\clbrdrl\brdrnone
\clbrdrb\brdrs10
\clbrdrr\brdrs10\cellx9468
\intbl {\ql\f1\fs20 Garantie :}\cell 
\intbl {\ql\f1\fs20 ZZZ }\cell 
\pard \intbl\row
</xsl:text>
<!-- ===========================================================================  -->
<!-- Seventh row in the table  -->
<!-- ===========================================================================  -->
<xsl:text>\trowd\trkeep\trgaph108\trleft-108 
\clbrdrt\brdrs10
\clbrdrl\brdrs10
\clbrdrb\brdrs10
\clbrdrr\brdrnone\cellx2500
\clbrdrt\brdrs10
\clbrdrl\brdrnone
\clbrdrb\brdrs10
\clbrdrr\brdrs10\cellx9468
\intbl {\ql\f1\fs20 Financement :}\cell 
\intbl {\ql\f1\fs20 ZZZ }\cell 
\pard \intbl\row
</xsl:text>
</xsl:template>

	<!-- ==================================================================== -->
	<!-- ==================================================================== -->
	<xsl:template name="RTFFileStart">
		<xsl:text>
{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl 
{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} 
{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial, Helvetica;}
{\f2\fnil\fcharset2\fprq2{\*\panose 05000000000000000000}Wingdings;}
}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\stylesheet {\ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}{\*\cs10 \additive Default Paragraph Font;}}
{\info {\author Zivko Radulovic}{\operator BASIS100 automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company Basis100 Inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \margl1440\margr1440\margt1440\margb1440\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
</xsl:text>
	</xsl:template>
	<!-- ==================================================================== -->
	<!-- ==================================================================== -->
	<xsl:template name="RTFFileEnd">
		<xsl:text>}</xsl:text>
	</xsl:template>
</xsl:stylesheet>
