<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
24.Dec.2008 DVG #DG798 LEN339938: 	Ron Mills Commitment letters being generated with wrong font  
10April2007 Xue Bin Zhao changed this template based on CTFS One-And-Only Account CR BRD.
22/Sep/2006 DVG #DG516 #4753  Commitment Letter Revisions  
30/Jun/2006 DVG #DG454 #3559  CTFS - Customization of Express Documents
  - created from Genx 
30/May/2006 DVG #DG428 #2819  Dundee - CMHC fee missing from commitment  
25/Apr/2006 DVG #DG408 #3057  Commitment Letter -doesn't display the unit number  
28/Oct/2005 DVG #DG350 #2304  Solicitor package/Commitment Letter text changes  
19/Oct/2005 DVG #DG336 #2301  Commitment letter <LenderName> tag  
30/Aug/2005 DVG #DG304 #1941  Commitment Letter changes  
	conditions now are split into 2 mutually exclusive sections: terms&cond. and cond. upon
05/Aug/2005 DVG #DG286  #1861  - improvements for Merix included here too
12/Jul/2005 DVG #DG256 #1788  Commitment Letter - minor
04/Jul/2005 DVG #DG244 #1735  NBrook Logo 
	- configure the template for variable logos
24/Jun/2005 DVG #DG238 #1679  Changes to the Solicitor package 
- Many changes to become the Xprs standard	-->

<xsl:stylesheet version="2.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:java="http://xml.apache.org/xalan/java" >
	
	<xsl:output method="text"/>
	
	<!--#DG244 -->
	<xsl:variable name="lendName" select="translate(/*/CommitmentLetter/LenderName, '/ \\:*?&quot;&lt;&gt;\|', '')"/>
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendName,'.xsl'))"/>
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*"/>

  <!--Define a global BOOLEAN variable isPreApproval used for whole template-->
  <xsl:variable name="isPreApproval" select="/*/Deal/specialFeatureId = 1"/>
  <xsl:variable name="InitialBorrowerAdvanceFeeAmount">
        <xsl:for-each select="//CommitmentLetter/Fees/Fee[starts-with(FeeVerb, 'Initial')]">
            <xsl:value-of select="FeeAmount"/>
        </xsl:for-each>
  </xsl:variable>
  <xsl:variable name="IsOneAndOnly" select="/*/CommitmentLetter[contains(Product, 'One-And-Only')]"/>

  <!--#DG516 change title if pre approval-->
  <xsl:variable name="docTitle">
  	<xsl:choose>
  		<xsl:when test="/*/CommitmentLetter/LanguageEnglish">
    		<xsl:choose>
    			<xsl:when test="$isPreApproval">
	                <xsl:choose>
	                    <xsl:when test="$IsOneAndOnly">
    				              <xsl:text>CANADIAN TIRE ONE-AND-ONLY PRE-APPROVAL CERTIFICATE (the Commitment)</xsl:text>
                      </xsl:when>
                      <xsl:otherwise>
    				              <xsl:text>PRE-APPROVAL CERTIFICATE (the Commitment)</xsl:text>
	                    </xsl:otherwise>
	                </xsl:choose>
    			</xsl:when>
    			<xsl:otherwise>
	                <xsl:choose>
	                    <xsl:when test="$IsOneAndOnly">
    				 <xsl:text>CANADIAN TIRE ONE-AND-ONLY COMMITMENT (the Commitment)</xsl:text>
                       </xsl:when>
                       <xsl:otherwise>
    				       <xsl:text>MORTGAGE COMMITMENT (the Commitment)</xsl:text>
                        </xsl:otherwise>
     		        </xsl:choose>
                </xsl:otherwise>
   	        </xsl:choose>
  		</xsl:when>
  		<xsl:otherwise>
  			<xsl:text>MORTGAGE COMMITMENT</xsl:text>
  		</xsl:otherwise>
  	</xsl:choose>
  </xsl:variable>
  
  <!-- CR224b -->
  <xsl:variable name="dealPurposeId" select="/*/Deal/dealPurposeId"/>
  <xsl:variable name="mtgProdId" select="/*/Deal/mtgProdId"/>
  <xsl:variable name="mtgAdvDate" select="/*/CommitmentLetter/AdvanceDate"/>
  <xsl:variable name="iadDate" select="/*/CommitmentLetter/IADDate"/>
  <xsl:variable name="firstPaymentDate" select="//CommitmentLetter/FirstPaymentDate"/>
  <xsl:variable name="maturityDate" select="/*/CommitmentLetter/MaturityDate"/>
  <xsl:variable name="principalAmount" select="/*/Deal/totalLoanAmount"/>
  <xsl:variable name="insurancePremium" select="/*/Deal/MIPremiumAmount"/>
<!--   <xsl:variable name="netAdvance" select="/*/Deal/netLoanAmount"/> -->
  <xsl:variable name="netAdvance" select="/*/CommitmentLetter/netAdvance"/>
  <xsl:variable name="fixedInterestRate" select="/*/CommitmentLetter/InterestRate"/>
  <xsl:variable name="paymentTerm" select="//CommitmentLetter/PaymentTerm"/>
  <xsl:variable name="amortizationTerm" select="/*/CommitmentLetter/Amortization"/>
  <xsl:variable name="costOfBorrow" select="/*/Deal/totalCostOfBorrowing"/>
  <xsl:variable name="interestOverterm" select="/*/Deal/interestOverTerm"/>
  <xsl:variable name="iadPayment" select="/*/Deal/interimInterestAmount"/>
  <xsl:variable name="paymentFrequence" select="/*/Deal/paymentFrequency"/>
  <xsl:variable name="balanceOnMaturity" select="/*/Deal/balanceRemainingAtEndOfTerm"/>
  <xsl:variable name="repName" select="/*/CommitmentLetter/BrokerAgentName"/>
  <xsl:variable name="repNumber" select="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumber"/>
  <xsl:variable name="repNumberExtension" select="/*/Deal/SourceOfBusinessProfile/Contact/contactPhoneNumberExtension"/>
  <xsl:variable name="totalPayments" select="/*/CommitmentLetter/TotalPayments"/>
  <xsl:variable name="isCoBorrowers" select="/*/Deal/Borrower[primaryBorrowerFlag!='Y']"/>
  <xsl:variable name="sobRegionId" select="/*/CommitmentLetter/SOBRegionId"/>
  <xsl:variable name="sobRegionDescription" select="/*/CommitmentLetter/SOBRegionDescription"/>
  <xsl:variable name="refExistingMtdNumber" select="/*/Deal/refExistingMtgNumber"/>
  <xsl:variable name="pandiPaymentAmount" select="/*/Deal/PandiPaymentAmount"/>
  <xsl:variable name="paymentDueOn" select="/*/CommitmentLetter/PaymentDueOn"/>
  <xsl:variable name="primeBaseAdj" select="/*/Deal/primeBaseAdj"/>
  <xsl:variable name="netInterestRate" select="/*/Deal/netInterestRate"/>
  <xsl:variable name="miPremiumPST" select="/*/Deal/MIPremiumPST"/>
  <xsl:variable name="effectiveAmortization" select="/*/CommitmentLetter/EffectiveAmortization"/>
  <xsl:variable name="internalRatePercentage" select="/*/CommitmentLetter/InternalRatePercentage"/>
  <xsl:variable name="adjustmentFactor" select="/*/CommitmentLetter/adjustmentFactor"/>
  <!-- #DG798 -->
  <xsl:variable name="tollFreePhone" select="'1-866-685-CTFS (2837)'"/>
  <xsl:variable name="tollFreePhoneA" select="'1-866-679-3491'"/>
  <xsl:variable name="tollFreePhoneB" select="'1-866-679-3489'"/>
  
  
  <!-- CR224b First Canadian Title addresses -->
  <xsl:variable name="ontAddress">
      <xsl:text>
          FAX NUMBER:  (905) 525-4710 OR 1-800-801-8827 \par
          TEL. NUMBER: (905) 525-1264 OR 1-800-757-2249 \par
          ADDRESS:     4 Hughson Street South, Suite 901 \par
                       Hamilton, ON    L8N 3Z1 \par
      </xsl:text>
  </xsl:variable>
  <xsl:variable name="bcAddress">
      <xsl:text>
          FAX NUMBER:  (604) 692-0229 or  1-866-692-0229 \par
          TEL. NUMBER: (604) 662-8141 or 1-888-328-2778 \par
          ADDRESS:     777 Hornby Street, Suite 1820 \par
                       Vancouver, BC  V6Z 1S4 \par
      </xsl:text>
  </xsl:variable>
  <xsl:variable name="abAddress">
      <xsl:text>
          FAX NUMBER:  (403) 232-1411 or 1-888-232-1411 \par
          TEL. NUMBER: (403) 265-4088 or 1-800-771-4313 \par
          ADDRESS:     639 Fifth Ave. S.W., Suite 1120 \par
                       Calgary, AB  T2P 0M9 \par
      </xsl:text>
  </xsl:variable>

	<xsl:template match="/">
		<!--#DG244 -->
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/>
		</xsl:message>		

    <xsl:call-template name="RTFFileStart"/><!-- #DG798 -->
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageFrench">
				<xsl:call-template name="FrenchFooter"/>
				<xsl:call-template name="FrenchPage1"/>
				<!--				<xsl:call-template name="FrenchPage2Start"/> -->
				<xsl:call-template name="FrenchPage2"/>
				<!--				<xsl:call-template name="FrenchPage3Start"/> -->
				<xsl:call-template name="FrenchPage3"/>
				<xsl:call-template name="FrenchPage4"/>
				<xsl:call-template name="FrenchPage5"/>
			</xsl:when>
			<xsl:otherwise>
			   <xsl:call-template name="englSelection"/>
      </xsl:otherwise>
		</xsl:choose>
		
		<xsl:call-template name="RTFFileEnd"/>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<!-- #DG798 -->
  <xsl:template name="englSelection">
		<xsl:choose><!-- CR224b -->
		    <!-- CR224b Purchase Pre-Approval Mortgage Commitment -->
		    <xsl:when test="$dealPurposeId=0 and $isPreApproval and not($IsOneAndOnly)">
		        <xsl:message>doc type is:Purchase Pre-Approval Mortgage Commitment</xsl:message>
		        <xsl:call-template name="MortgagePreApprovalLetterEN"/>
		    	<xsl:call-template name="ExtraCommitmentLetterStart"/>
		   		<xsl:call-template name="EnglishCommitmentLetter">
		   		    <xsl:with-param name="isFooter" select="false()"/>
		   		</xsl:call-template>
		    </xsl:when>
		    
		    <!-- CR224b Purchase Fixed Mortgage Commitment -->
		    <xsl:when test="$dealPurposeId=0 and not($isPreApproval) and ($mtgProdId=5 or $mtgProdId=6 or $mtgProdId=7 or $mtgProdId=8 or $mtgProdId=9 or $mtgProdId=10 or $mtgProdId=12 or $mtgProdId=13)"> <!-- CHANGE IT -->
		        <xsl:message>doc type is:Purchase Fixed Mortgage Commitment</xsl:message>
		        <xsl:call-template name="MortgageApprovalPurchaseEN"/>
		        <xsl:call-template name="MortgageDisclosireFixedEN">
		            <xsl:with-param name="lastBrace" select="' '"/>
		        </xsl:call-template>
		    	<xsl:call-template name="ExtraCommitmentLetterStart"/>
		   		<xsl:call-template name="EnglishCommitmentLetter">
		   		    <xsl:with-param name="isFooter" select="false()"/>
		   		</xsl:call-template>
		    </xsl:when>
		    
		    <!-- CR224b Purchase Variable Commitment -->
		    <xsl:when test="$dealPurposeId=0 and not($isPreApproval) and $mtgProdId=11">
			    <xsl:message>doc type is:Purchase Variable Commitment</xsl:message>
			    <xsl:call-template name="MortgageApprovalPurchaseEN"/>
			    <xsl:call-template name="MortgageDisclosireVariableEN">
			        <xsl:with-param name="lastBrace" select="' '"/>
			    </xsl:call-template>
		    	<xsl:call-template name="ExtraCommitmentLetterStart"/>
		   		<xsl:call-template name="EnglishCommitmentLetter">
		   		    <xsl:with-param name="isFooter" select="false()"/>
		   		</xsl:call-template>
		    </xsl:when>
		    
		    <!-- CR224b Purchase Open Commitment -->
		    <xsl:when test="$dealPurposeId=0 and not($isPreApproval) and $mtgProdId=0">
		        <xsl:message>doc type is:Purchase Open Commitment</xsl:message>
		        <xsl:call-template name="MortgageApprovalPurchaseEN"/>
		        <xsl:call-template name="MortgageDisclosireOpenEN">
		            <xsl:with-param name="lastBrace" select="' '"/>
		        </xsl:call-template>
		    	<xsl:call-template name="ExtraCommitmentLetterStart"/>
		   		<xsl:call-template name="EnglishCommitmentLetter">
		   		    <xsl:with-param name="isFooter" select="false()"/>
		   		</xsl:call-template>
		    </xsl:when>
		    
		    <!-- CR224b Refinance Fixed Mortgage Commitment -->
		    <xsl:when test="($dealPurposeId=4 or $dealPurposeId=5) and not($isPreApproval) and ($mtgProdId=5 or $mtgProdId=6 or $mtgProdId=7 or $mtgProdId=8 or $mtgProdId=9 or $mtgProdId=10 or $mtgProdId=12 or $mtgProdId=13)">
		        <xsl:message>doc type is:Refinance Fixed Mortgage Commitment</xsl:message>
		        <xsl:call-template name="MortgageApprovalRefinanceEN"/>
		        <xsl:call-template name="MortgageDisclosireFixedEN">
		            <xsl:with-param name="lastBrace" select="' '"/>
		        </xsl:call-template>
		        <xsl:call-template name="RequestForStatementAndAuthorization"/>
		    	<xsl:call-template name="ExtraCommitmentLetterStart"/>
		   		<xsl:call-template name="EnglishCommitmentLetter">
		   		    <xsl:with-param name="isFooter" select="false()"/>
		   		</xsl:call-template>
		    </xsl:when>
		    
		    <!-- CR224b Refinance Variable Commitment -->
		    <xsl:when test="($dealPurposeId=4 or $dealPurposeId=5) and not($isPreApproval) and $mtgProdId=11">
		        <xsl:message>doc type is:Refinance Variable Commitment</xsl:message>
		        <xsl:call-template name="MortgageApprovalRefinanceEN"/>
		        <xsl:call-template name="MortgageDisclosireVariableEN">
		            <xsl:with-param name="lastBrace" select="' '"/>
		        </xsl:call-template>
		        <xsl:call-template name="RequestForStatementAndAuthorization"/>
		    	<xsl:call-template name="ExtraCommitmentLetterStart"/>
		   		<xsl:call-template name="EnglishCommitmentLetter">
		   		    <xsl:with-param name="isFooter" select="false()"/>
		   		</xsl:call-template>
		    </xsl:when>
		    
		    <!-- CR224b Refinance Open Commitment -->
		    <xsl:when test="($dealPurposeId=4 or $dealPurposeId=5) and not($isPreApproval) and $mtgProdId=0">
		        <xsl:message>doc type is:Refinance Open Commitment</xsl:message>
		        <xsl:call-template name="MortgageApprovalRefinanceEN"/>
		        <xsl:call-template name="MortgageDisclosireOpenEN">
		            <xsl:with-param name="lastBrace" select="' '"/>
		        </xsl:call-template>
		        <xsl:call-template name="RequestForStatementAndAuthorization"/>
		    	<xsl:call-template name="ExtraCommitmentLetterStart"/>
		   		<xsl:call-template name="EnglishCommitmentLetter">
		   		    <xsl:with-param name="isFooter" select="false()"/>
		   		</xsl:call-template>
		    </xsl:when>
		    
		    <xsl:otherwise>
		        <!-- #DG798 <xsl:call-template name="RTFFileStart"/-->
		        <!--#DG286 -->
		        <xsl:call-template name="EnglishCommitmentLetter">
		          <xsl:with-param name="isFooter" select="true()"/>
		        </xsl:call-template>
		    </xsl:otherwise>
		</xsl:choose>
  </xsl:template>	
	
	<!--#DG286 -->
	<xsl:template name="EnglishCommitmentLetter">
	    <xsl:param name="isFooter"/>
	    <xsl:choose>
	      <xsl:when test="$IsOneAndOnly">
	        <xsl:if test="$isFooter = true()">
			  <xsl:call-template name="EnglishFooter"/>
            </xsl:if>
            <xsl:if test="$isFooter != true()">
              <xsl:text>{\footer}
</xsl:text>
            </xsl:if>
		    <xsl:call-template name="EnglishOneAndOnlyPage1"/>
			<xsl:call-template name="EnglishOneAndOnlyPage4"/>
			<xsl:call-template name="EnglishOneAndOnlyPage5"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="$isFooter = true()">
			  <xsl:call-template name="EnglishFooter"/>
            </xsl:if>
            <xsl:if test="$isFooter != true()">
                <xsl:text>{\footer}
</xsl:text>
            </xsl:if>
		    <xsl:call-template name="EnglishPage1"/>
		    <!--xsl:call-template name="EnglishPage2Start"/-->
		    <!--xsl:call-template name="EnglishPage2"/-->
		    <!--	xsl:call-template name="EnglishPage3Start"/-->
		    <!--xsl:call-template name="EnglishPage3"/-->
		    <xsl:call-template name="EnglishPage4"/>
		    <xsl:call-template name="EnglishPage5"/>
	      </xsl:otherwise>
	    </xsl:choose>
	</xsl:template>

	<xsl:template name="EnglishOneAndOnlyPage1">
		<xsl:text>{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \qc\widctlpar\aspalpha\aspnum\faauto\itap0 
{\b\f0\fs27 </xsl:text>
    <xsl:value-of select="$docTitle"/>		
		<xsl:text>\par }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text>
		<!--#DG244 -->
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell {\f0\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par  
</xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalc
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \cell 
Tel: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \cell }
{\b\f0\fs18 Client Service Representative \par 
\b0 </xsl:text>
		<!--xsl:value-of select="/*/CommitmentLetter/Underwriter"/-->
		<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactFirstName"/>
    <xsl:text> \intbl </xsl:text>
    <xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactLastName"/>
		<xsl:text>\cell }
{\b\f0\fs18 Date \par 
\b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\row 
}
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par 
</xsl:text>
		</xsl:for-each>
		<xsl:text>
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par 
</xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
{\b\f0\fs18 Product Reference Number:\cell \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
</xsl:text>
	<xsl:choose>
	 <xsl:when test="$isPreApproval">
		<xsl:text>{\i\fs20 We are pleased to confirm that your application for a 
Canadian Tire One-and-Only{\super TM} account (the {\b Account}), to be 
secured by a collateral mortgage on a property (the {\b\i\fs20 Property}) 
to be determined, has been pre-approved subject to the terms and conditions 
that follow, which Canadian Tire reserves the right to modify once a Property 
has been selected, and to the Canadian Tire One-and-Only Terms and Conditions 
and the Disclosure Statement that will be provided to you once the Property has 
been selected, a sample of which is attached to this Commitment.
\par \par  
 
This Commitment assumes that the Account will be funded entirely under the 
variable portion of the account. The regular payment amount, interest rate and 
payment frequency listed below are based on the entire balance in the Account 
remaining in the variable portion of the Account. Once an advance has been 
made, you may convert one or more portions of the outstanding balance for a 
fixed term at a fixed rate of interest, and a disclosure statement for each 
fixed rate portion will be provided to you at that time.
\cell }</xsl:text> 	
		
	 </xsl:when>
	 <xsl:otherwise>
    <xsl:text>{\i\fs18 We are pleased to confirm that your application for a 
Canadian Tire One-and-Only{\super TM} account (the Account), to be secured by 
a collateral mortgage on the property described below, has been approved 
subject to the terms and conditions that follow and to the Canadian Tire 
One-and-Only Terms and Conditions and the Disclosure Statement that are also 
provided to you with this Commitment.\par\par 

This Commitment assumes that the Account will be funded entirely under the 
variable portion of the account. The regular payment amount, interest rate and 
payment frequency listed below are based on the entire balance in the Account 
remaining in the variable portion of the Account. Once an advance has been 
made, you may convert one or more portions of the outstanding balance for a 
fixed term at a fixed rate of interest, and a disclosure statement for each 
fixed rate portion will be provided to you at that time.\par \cell } 
</xsl:text> 
	 </xsl:otherwise>
	</xsl:choose>

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
<xsl:text>{\b\f0\fs18 <!--#DG350 MORTGAGOR(S)-->BORROWER:\cell\b0 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par  
</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
{\b\f0\fs18 PURPOSE:\b0  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text>:}{\f0\fs18 \cell </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par  
</xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18\ul PROPERTY ADDRESS: \cell LEGAL DESCRIPTION OF PROPERTY: \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		<!--#DG408 xsl:for-each select="/*/CommitmentLetter/Properties/Property"-->
		<xsl:for-each select="/*/Deal/Property">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tx4900\aspalpha\aspnum\faauto\adjustright
{\f0\fs18 </xsl:text>

    <xsl:choose>
      <xsl:when test="$isPreApproval">
	       <xsl:text>TBD\cell TBD</xsl:text>
        </xsl:when>
      <xsl:otherwise>

			<!--#DG408 xsl:value-of select="./AddressLine1"/>
			<xsl:text> \par 
</xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>\par 
</xsl:text>
			<xsl:value-of select="./PostalCode"/-->
			<xsl:call-template name="propAdr"/>
			<xsl:text> \par 
\cell </xsl:text>
			<!--#DG408 xsl:value-of select="./LegalLine1"/>
			<xsl:if test="./LegalLine1">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine2"/>
			<xsl:if test="./LegalLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine3"/>
			<xsl:if test="./LegalLine3">
				<xsl:text>\par </xsl:text>
			</xsl:if-->
			<xsl:if test="legalLine1 != ''">
				<xsl:value-of select="legalLine1"/>
				<xsl:text>\par  
</xsl:text>
			</xsl:if>
			<xsl:if test="legalLine2 != ''">
				<xsl:value-of select="legalLine2"/>
				<xsl:text>\par  
</xsl:text>
			</xsl:if>
			<xsl:if test="legalLine3 != ''">
				<xsl:value-of select="legalLine3"/>
				<xsl:text>\par  
</xsl:text>
			</xsl:if>

      </xsl:otherwise>
    </xsl:choose>

			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
      <xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2447\cellx2339
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3241 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5328 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 COMMITMENT DATE:}
{\f0\fs18 
\par  
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CommitmentIssueDate"/>
	 <xsl:text>} \cell
{\b\f0\fs18 COMMITMENT EXPIRY DATE:}
{\f0\fs18 
\par  
</xsl:text>		
		<xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
	 <xsl:text>
} \cell
{\b\f0\fs18 ACCOUNT ACTIVATION/ADVANCE DATE:}
{\f0\fs18 
\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
	 <xsl:text>
} \cell

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trftsWidthB3\trftsWidthA3\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2447\cellx2339
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3241 \cellx5580
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth5328 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 PRODUCT TYPE:  \b0 </xsl:text>
		<xsl:text>\tab Canadian Tire One-and-Only</xsl:text>
		<xsl:text>\cell }</xsl:text>

		<xsl:text>{\b\f0\fs18 LTV:\b0\cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LTV"/>
		<!--xsl:text>{\b\f0\fs18 MORTGAGE PRIORITY:\b0\cell </xsl:text>
		<xsl:value-of select="/*/Deal/lienPosition"/-->

		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

<!--#DG516 rewritten to organize columns -->
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3528 \cellx3420
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1406 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 CREDIT LIMIT:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f0\fs18 CURRENT ANNUAL RATE:}{\f0\fs18 \cell </xsl:text>
		 <xsl:value-of select="/*/CommitmentLetter/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 MTG.INSUR.PREMIUM:\b0 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Premium"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 PAYMENT FREQUENCY:\b0\cell </xsl:text>
		<xsl:text>End of Each Month</xsl:text>
		<xsl:text>\cell }	 		
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 MAXIMUM AMOUNT OF COLLATERAL MORTGAGE:\b0 \cell </xsl:text>
\line 
<xsl:choose>
  	<!--
	MIPayorId: 1 = Borrower, 2 = Lender	
	Base Mortgae Amount: TotalAmount
	-->
  	<xsl:when test="/*/Deal/MIPayorId=1 and translate(/*/Deal/MIPremiumAmount, '$,', '') &gt; 0 and /*/Deal/MIUpfront='N'">
    	<!--xsl:value-of select="concat('$',translate(//CommitmentLetter/TotalAmount, '$,', '') + translate(/*/Deal/MIPremiumAmount, '$,', '') )"/-->
    	<xsl:value-of select="/*/Deal/totalLoanAmount"/>  	
  	</xsl:when>
  	<xsl:otherwise>
		  <xsl:value-of select="/*/Deal/netLoanAmount"/>
  	</xsl:otherwise>
  	</xsl:choose>  			
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 MINIMUM PAYMENT AMOUNT:\b0\cell 
\line Interest Only 
\cell }
\row 

\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 PST PAYABLE ON INSUR.PREMIUM:\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PST"/>
		<xsl:text>\cell }
{\b\f0\fs18 EST.ANNUAL PROP.TAXES:\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AnnualTaxes"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row

\pard \ql \widctlpar\intbl\faauto 
\cell \cell
{\b\f0\fs18 PROP. TAXES PAID BY:\b0 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TaxPayor"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 FEES/ADVANCES: \cell }{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 \cell </xsl:text>
			<!--xsl:value-of select="./FeeVerb"/-->
			<xsl:variable name="SubFeeVerb" select="./FeeVerb"/>
			<xsl:value-of select="substring-before($SubFeeVerb,'(' )"/>
			<xsl:text>\cell }{\f0\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
			
<xsl:choose> 
<xsl:when test="$isPreApproval">
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 In this Commitment, which includes any schedule(s), the Canadian 
Tire One-and-Only Terms and Conditions and the Disclosure Statement, 
}{\b\f0\fs18 you and your}
{\f0\fs18  mean each Borrower named above and }{\b\f0\fs18 we, our, lender and us }{\f0\fs18  mean </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
    <xsl:text>. Each Borrower is liable, both jointly and severally, for payment 
and performance of all obligations under this Commitment and the Mortgage.
\par \par 

Please note that the Credit Limit is the amount that you may borrow. Where the 
down payment or equity in the Property represents less than 20% of the value 
of the property, this Commitment will be subject to mortgage default insurance 
being issued by an approved insurer. You may choose either to borrow the 
amount of the premium under your existing Credit Limit or to increase your 
Credit Limit to pay that premium but for no other purpose. If you choose the 
second option, the Maximum Amount of Collateral Mortgage will be the sum of 
your existing Credit Limit and the amount of the premium, and your Credit 
Limit will revert to your existing Credit Limit as you make principal payments 
that total the amount of the premium.\par \par

Please note that the "Current Annual Rate" is the Canadian Tire One-and-Only 
Variable Interest Rate, which is a variable rate of interest.\par \par

All of the conditions on the attached schedule(s) and, if applicable, those of 
the mortgage insurer, must be satisfied, unless expressly waived in writing by 
us or the mortgage insurer. All costs, including legal fees and disbursements, 
title insurance premium, mortgage insurance premium, etc. are for your 
account. The mortgage insurance premium (if applicable) will be added to the 
principal amount of the initial advance under the Account and may be deducted 
from the initial advance. Any other fees and charges specified herein may be 
deducted by us from the initial advance. Even if for any reason no advance is 
made to you, you agree to pay all applicable application, legal fees and 
disbursements and appraisal costs incurred by us in this transaction.\par \par

Once you have made an offer on a property, please contact us at </xsl:text>
        <xsl:value-of select="$tollFreePhoneA" /><!-- #DG798 -->
        <xsl:text> to discuss next steps.	
</xsl:text>
</xsl:when>		
<xsl:otherwise>				
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs16 In this Commitment, which includes any schedule(s), the 
Canadian Tire One-and-Only Terms and Conditions and the Disclosure Statement, }{\b\f0\fs16 you and your}
{\f0\fs16  mean each Borrower named above and }{\b\f0\fs16 we, our, lender and us }
{\f0\fs16  mean </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>. Each Borrower is liable, both jointly and severally, for payment 
and performance of all obligations under this Commitment and the Collateral Mortgage.\par \par
 
Please note that the Credit Limit is the amount that you may borrow. However, 
if you are required to pay a mortgage insurance premium, you may choose either 
to borrow the amount of the premium under your existing Credit Limit or to 
increase your Credit Limit to pay that premium but for no other purpose. 
If you choose the second option, the Maximum Amount of Collateral Mortgage 
will be the sum of your existing Credit Limit and the amount of the premium, 
and your Credit Limit will revert to your existing Credit Limit as you make 
principal payments that total the amount of the premium.\par \par

Please note that the "Current Annual Rate" is the 
Canadian Tire One-and-Only Variable Interest Rate, which is a 
variable rate of interest.\par \par

All of the conditions on the attached schedule(s) and, if applicable, those 
of the mortgage insurer, must be satisfied, unless expressly waived in writing 
by us or the mortgage insurer. All costs, including legal fees and 
disbursements, title insurance premium and mortgage insurance premium, etc. 
are for your account. The mortgage insurance premium (if applicable) will 
form part of the principal amount of the initial advance under the Account 
and will be paid from the initial advance. Any other fees and charges 
specified herein may be deducted by us from the initial advance. Even if 
for any reason no advance is made to you, you agree to pay all applicable 
application, legal fees and disbursements and appraisal costs incurred by 
us in this transaction.\par \par
 
To accept this Commitment, each Borrower and Guarantor, if applicable, must sign 
below and this Commitment must be returned to us by no later than </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
		<xsl:text> after which time, if not accepted, we may choose to cancel this 
Commitment.  As well, the Collateral Mortgage must be registered by </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text> or we may cancel this Commitment.\par \par
 
In the case of any inconsistency between any term in the Collateral Mortgage 
granted by you to us and registered against the Property and this 
Commitment, the terms contained in the Collateral Mortgage will govern and 
will override the terms in this Commitment.  In the case of any 
inconsistency between any term in the Collateral Mortgage and the 
Disclosure Statement, the terms contained in the Disclosure Statement will 
govern and will override the terms in the Collateral Mortgage. 
</xsl:text>
      </xsl:otherwise>
    </xsl:choose>

		<xsl:text>\par \par 

Thank you for choosing </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> for your financing.\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs16 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 
\par \page
 

\par }
</xsl:text>
	</xsl:template>
	
  <xsl:template name="EnglishOneAndOnlyPage4">
		<!--#DG304 -->
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\b\f0\fs18\ul TERMS AND CONDITIONS\par 
\cell \row}

</xsl:text>
		<!--#DG304 all changes to the list below should be syncronized to the 'conditional upon' section -->
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition[conditionTypeId='2']">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 
\pard \ql \widctlpar\intbl\faauto
{\f0\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }
{\f0\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par  
</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par  
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright \row

</xsl:text>

		</xsl:for-each>
		<xsl:text>\pard \ql \widctlpar\intbl\faauto 
{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard\page\par 

\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\pard 
\ql \widctlpar\intbl\faauto {\b\f0\fs18\ul THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING
\par\par }

</xsl:text>
    <xsl:choose>
		  <xsl:when test="$isPreApproval">
			 <xsl:text>{\f0\fs18 The following conditions must be met unless 
waived by us in writing at our sole discretion, and the requested documents  
must be received in form and content satisfactory to us. \par \par  

\cell }</xsl:text>
		  </xsl:when>
		  <xsl:otherwise>
			 <xsl:text>{\f0\fs18 The following conditions must be met unless waived  
by us in writing at our sole discretion, and the requested documents must be  
received in form and content satisfactory to us no later than ten (10) days  
prior to the scheduled Account Activation/Advance Date. Failure to do so may  
result in us cancelling this Commitment. \par \par  

\cell }</xsl:text>
		  </xsl:otherwise>
    </xsl:choose>
    <xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10  
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908 
\row 
}
</xsl:text>
    <xsl:if test="/*/CommitmentLetter/Conditions">
    <!-- commented as it is printing 2 empty lines for the first row number - Venkata -->
    <!-- Venkata commented - Jun 22, 2007 xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text -->
		</xsl:if>
		<!--#DG304 all changes to the list below should be syncronized to the 'terms and conditions' section -->
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition[conditionTypeId='0']">
			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f0\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }

{\f0\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par  
</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par  
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f0\fs18 \par \par } 
 
</xsl:text>
		<xsl:choose>
  		<xsl:when test="$isPreApproval"></xsl:when>
  		<xsl:otherwise>{\b\f0\fs18\ul Requests for any changes to the terms and 
conditions of this Commitment will only be considered if your request is 
received by us in writing at least 5 business days prior to the scheduled 
Account Activation/Advance Date.}
  		</xsl:otherwise>
  		</xsl:choose>
  		<xsl:text>				
\par 
\par {\b\f0\fs18\ul ACCEPTANCE:\par\par }

{\f0\fs18 </xsl:text>
    <xsl:choose>
      <xsl:when test="$isPreApproval">
        <xsl:text>This Commitment is open for acceptance by you until </xsl:text>
        <!-- xsl:value-of select="/*/CommitmentLetter/CommitmentExpiryDate" -->
        <xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
        <xsl:text>, after which time, if an offer has not been made on a  
Property, we may choose to cancel this Commitment.</xsl:text>
      </xsl:when>
		  <xsl:otherwise>
        <xsl:text>This Commitment is open for acceptance by you until 11:59 pm on</xsl:text>  
        <xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
        <xsl:text>, after which time, if not accepted, we may choose to cancel this Commitment. As well, the 
Collateral Mortgage must be registered by </xsl:text> 
        <xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
        <xsl:text> or we may cancel this Commitment. </xsl:text>
      </xsl:otherwise>
		</xsl:choose>
		<xsl:text>
<!--This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text>
<xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
<xsl:text> after which time, if not accepted; we may choose to cancel this Commitment. Furthermore, the loan must be advanced by no later than </xsl:text>
<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
<xsl:text> at which time; we will have no further obligation to proceed with the advance.		
-->
\par \par }

\pard \ql \widctlpar\intbl\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright {\f0\fs18 \tab \tab </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>\par\par  

\tab Authorized by:___________________________________________
\par \tab \tab \tab </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Underwriter"/>
		<xsl:text>\par\par\par } 

\pard \ql \widctlpar\intbl\faauto 
{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}

\pard \ql \widctlpar\faauto\itap0 {\f0\fs18 \par  
\page }


</xsl:text>
	</xsl:template>
	
	<xsl:template name="EnglishOneAndOnlyPage5">
		<xsl:text>\pard \ql \widctlpar\faauto\itap0 {\f0\fs18 \par  
\page }


\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\b\f0\fs18 </xsl:text>
    <xsl:choose>
      <xsl:when test="not($isPreApproval)">ACCOUNT AGREEMENT
\par\par }

{\f0\fs18 We, Canadian Tire Bank, offer to open a Canadian Tire One-and-Only 
account for the Borrower(s). If you accept this Commitment, there will be an 
agreement between you and us. The terms of the agreement are set out in this 
Commitment, the Canadian Tire One-and-Only Terms and Conditions and the 
Disclosure Statement. By accepting this Commitment, you confirm that you have 
read, understood and agreed to the terms of this Commitment, the Canadian Tire 
One-and-Only Terms and Conditions and the Disclosure Statement. If you do your 
banking with us on the Internet, our agreement with you also includes the 
Terms and Conditions governing our websites.\par\par }

</xsl:when>
		<xsl:otherwise> }
		</xsl:otherwise>
		</xsl:choose>
		<xsl:text>
{\b\f0\fs18 \par 
PERSONAL INFORMATION\par\par }

{\f0\fs18 The term "Personal Information" means any information about an 
identifiable individual. Personal Information about you, the Borrower(s), or 
the Guarantor(s), as applicable, is collected, used, and disclosed in 
accordance with the Canadian Tire Financial Services Privacy Communication 
(the {\b\f0\fs18 Communication}), including, in particular, to process and 
maintain the Account of the Borrower(s).
As described in the Communication, Personal Information may also be shared 
with other parties who administer the Account and to market and sell other 
Canadian Tire branded products and services. The Communication is updated from 
time to time. You are referred to the most current version of the 
Communication, which is enclosed and which is also available online at 
myCTFS.com.\par\par } 
 
{\b\f0\fs18 </xsl:text>
		<xsl:choose>
      <xsl:when test="$isPreApproval">I / We, the undersigned accept the terms of 
this Commitment as stated above and agree to fulfill the conditions of 
approval outlined herein. }
		</xsl:when>
    <xsl:otherwise>I / we, the undersigned accept the terms of this Commitment as 
stated above and agree to fulfill the conditions of approval herein and in the 
Canadian Tire One-and-Only Terms and Conditions.\par \par
 
By signing below, I / we (choose one of the following):\par \par

_____ acknowledge receiving the Disclosure Statement at least 2 business 
days before I / we signed this Commitment\par  
OR\par \par

_____ acknowledge receiving the Disclosure Statement with this Commitment, and  
waive the requirement that it be provided 
to me / us 2 business days before I / We sign this Commitment. } 
</xsl:otherwise>
		</xsl:choose>
		<xsl:text>
{\f0\fs18 \par \par  
 
Each Borrower has the right to receive an individual copy of the Disclosure 
Statement and all subsequent prescribed disclosure relating to the Account 
(the {\b\f0\fs18 Required Disclosure}). By signing below, each Borrower 
consents to Canadian Tire Bank providing the Required Disclosure to the 
Borrower to whom this Commitment is addressed at the address for the Property 
subject to the Collateral Mortgage. Any Borrower named below may revoke this 
consent at any time by contacting a Client Service Representative at the phone 
number listed above so that each of you will receive individually addressed 
copies of the Required Disclosure.\par\par  
 
</xsl:text>
    <xsl:choose>
		  <xsl:when test="not($isPreApproval)"><xsl:text>}{\b\f0\fs18 </xsl:text>SIGNATURES
\par } 
{\f0\fs18 Your signature(s) will be stored along with other information 
about you to form your signature card.\par\par 

By signing below, each Borrower requests a debit card and debit card services.
\par\par \par

\cell \row}

</xsl:when>
      <xsl:otherwise>\cell \row } 

</xsl:otherwise>
    </xsl:choose><xsl:text>
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell ___________________________
\par <!--#DG350 APPLICANT-->BORROWER (sign above)
\par 
\par \cell ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\par \cell ___________________________
\par BORROWER (print name above)
\par 
\par \cell 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f0\fs18 ___________________________
\par BORROWER (sign above)
\par \par 

\cell }
{\b\f0\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\par \cell ___________________________
\par BORROWER (print name above)
\par 
\par \cell 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f0\fs18 ___________________________
\par  
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text> (sign above)
\par \par  

\cell }
{\b\f0\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\cell ___________________________
\par  
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text> (print name above)
\par 
\cell 
\par 
\par
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text>
  </xsl:template>

	<xsl:template name="EnglishPage1">
		<xsl:text>
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \qc\widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f0\fs27 <!--#DG516 MORTGAGE COMMITMENT--></xsl:text>
<xsl:value-of select="$docTitle"/>		
		<xsl:text>\par }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text>
		<!--#DG244 -->
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell {\f0\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par  
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par  
</xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalc
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \cell 
Tel: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \cell }
{\b\f0\fs18 Client Service Representative \par 
\b0 </xsl:text>
		<!-- <xsl:value-of select="/*/CommitmentLetter/Underwriter"/> -->
		<xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactFirstName"/>
    <xsl:text> \intbl </xsl:text>
    <xsl:value-of select="/*/Deal/SourceOfBusinessProfile/Contact/contactLastName"/>
		<xsl:text> \cell }
{\b\f0\fs18 Date \par 
\b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\row 
}
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par  
</xsl:text>
		</xsl:for-each>
		<xsl:text>

</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par  
</xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
{\b\f0\fs18 Mortgage Reference Number:\cell \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \par} 
</xsl:text>
    <xsl:choose>
      <xsl:when test="$isPreApproval">
        <xsl:text>{\i\fs22 We are pleased to confirm that your application for a 
Mortgage on a property (the {\b\i\fs22 Property}) to be determined has been 
pre-approved subject to </xsl:text>
		    <xsl:value-of select="/*/CommitmentLetter/LenderName"/>
        <xsl:text> being satisfied with: (i) the Property once it has been selected, 
(ii) the terms and conditions that follow, which Canadian Tire Bank reserves 
the right to modify once a Property has been selected, and (iii) the terms of 
the Disclosure Statement that will be provided to you once the Property has 
been selected, a sample of which is attached to this Commitment.\par \cell} 
</xsl:text> 
      </xsl:when>
      <xsl:otherwise>
		    <xsl:text>{\i\f0 We are pleased to confirm that your application for a 
Mortgage on the property described below has been approved subject to the 
terms and conditions that follow and the terms of the Disclosure Statement 
that is provided to you with this Commitment.\par \cell}  
</xsl:text>
    </xsl:otherwise>
    </xsl:choose>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 <xsl:text><!--#DG350 MORTGAGOR(S)-->BORROWER:\cell\b0 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par  
</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
{\b\f0\fs18 PURPOSE:\b0  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text>:}{\f0\fs18 \cell </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par  
</xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18\ul PROPERTY ADDRESS: \cell LEGAL DESCRIPTION OF PROPERTY: \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		<!--#DG408 xsl:for-each select="/*/CommitmentLetter/Properties/Property"-->
		<xsl:for-each select="/*/Deal/Property">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tx4900\aspalpha\aspnum\faauto\adjustright
{\f0\fs18 
</xsl:text>
			<!--#DG408 xsl:value-of select="./AddressLine1"/>
			<xsl:text> \par 
</xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>\par 
</xsl:text>
			<xsl:value-of select="./PostalCode"/-->
			<xsl:call-template name="propAdr"/>
			<xsl:text> \par 
\cell </xsl:text>
			<!--#DG408 xsl:value-of select="./LegalLine1"/>
			<xsl:if test="./LegalLine1">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine2"/>
			<xsl:if test="./LegalLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine3"/>
			<xsl:if test="./LegalLine3">
				<xsl:text>\par </xsl:text>
			</xsl:if-->
			<xsl:if test="legalLine1 != ''">
				<xsl:value-of select="legalLine1"/>
				<xsl:text>\par  
</xsl:text>
			</xsl:if>
			<xsl:if test="legalLine2 != ''">
				<xsl:value-of select="legalLine2"/>
				<xsl:text>\par  
</xsl:text>
			</xsl:if>
			<xsl:if test="legalLine3 != ''">
				<xsl:value-of select="legalLine3"/>
				<xsl:text>\par  
</xsl:text>
			</xsl:if>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 COMMITMENT DATE:}
{\f0\fs18 
\par  
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CommitmentIssueDate"/>
		<xsl:text>\par\par }

{\b\f0\fs18 INTEREST ADJUSTMENT DATE:}
{\f0\fs18 
\par  
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/IADDate"/>
		<xsl:text>\cell }
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 COMMITMENT EXPIRY DATE:}
{\f0\fs18 
\par  
</xsl:text>
		<!--#DG516 xsl:value-of select="/*/CommitmentLetter/CommitmentExpiryDate"/-->
		<xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
		<xsl:text>\par \par }

{\b\f0\fs18 FIRST PAYMENT DATE:}
{\f0\fs18 \par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
{\b\f0\fs18 MORTGAGE ADVANCE DATE:}
{\f0\fs18 
\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text>\par \par }

{\b\f0\fs18 MATURITY DATE:}
{\f0\fs18 \par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 MORTGAGE TYPE:  \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Product"/>
		<xsl:text>\cell }</xsl:text>

		<!--#DG516 xsl:text>{\b\f0\fs18 LTV:\b0\cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LTV"/-->
		<xsl:text>{\b\f0\fs18 MORTGAGE PRIORITY:\b0\cell </xsl:text>
		<xsl:value-of select="/*/Deal/lienPosition"/>

		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

<!--#DG516 rewritten to organize columns -->
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3528 \cellx3420
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1406 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 BASIC AMOUNT:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f0\fs18 INTEREST RATE:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 MTG.INSUR.PREMIUM:\b0 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f0\fs18 TERM:\b0 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PaymentTerm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 TOTAL PRINCIPAL AMOUNT:\b0 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell }
{\b\f0\fs18 AMORTIZATION PERIOD:\b0 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Amortization"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

<!--#DG428 moved up and shift the others down -->
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 REGULAR PAYMENT AMOUNT\b0:\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TotalPayment"/>
		<xsl:text>\cell }
{\b\f0\fs18 INTEREST COMPOUNDING:\b0\cell 
</xsl:text>
    <!--#DG516 interestCompounding picklist table does NOT exist, so...hard code it -->
    <xsl:variable name="intCompound" select="/*/Deal/MtgProd/interestCompoundingId"/>
  	<xsl:choose>
  		<xsl:when test="$intCompound = 1">
  		  <xsl:text>Monthly</xsl:text>
  		</xsl:when>
  		<xsl:otherwise>
  		  <xsl:text>Semi-Annual</xsl:text>
  		</xsl:otherwise>
  	</xsl:choose>    		
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 EST.ANNUAL PROP.TAXES\b0:\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AnnualTaxes"/>
		<xsl:text>\cell }
{\b\f0\fs18 PAYMENT FREQUENCY\b0:\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PaymentFrequency"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row
<!--#DG516 end -->

\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 PST PAYABLE ON INSUR.PREMIUM:\b0\cell 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PST"/>
		<xsl:text>\cell }
{\b\f0\fs18 TAXES TO BE PAID BY:\b0 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TaxPayor"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 MORTGAGE FEES:\cell }{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 \cell </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell }{\f0\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 In this Commitment, which includes any schedule(s) and the Disclosure Statement, }{\b\f0\fs18 you and your}
{\f0\fs18  mean each Borrower named above and }{\b\f0\fs18 we, our, lender and us}{\f0\fs18  mean </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
    <xsl:text>. Each Borrower is liable, both jointly and severally, for payment 
and performance of all obligations under this Commitment and the Mortgage. \par \par

All of the conditions on the attached schedule(s) and, if applicable, those of 
the mortgage insurer must be satisfied, unless expressly waived by us or the 
mortgage insurer. All costs, including legal fees and disbursements, title 
insurance premium and mortgage insurance premium, etc. are for your account. 
The mortgage insurance premium (if applicable) will be added to the principal 
amount of the Mortgage. Any fees and charges specified herein may be deducted 
from the loan advance. If for any reason the loan is not advanced, you agree 
to pay all applicable application, legal fees and disbursements and appraisal 
costs incurred in this transaction.\par \par  
 
</xsl:text>
      <xsl:if test="$isPreApproval!=1"> 
        <xsl:text>To accept these terms, this 
Commitment must be signed by the Borrower and the Guarantor, if applicable, 
and returned to us by no later than </xsl:text>
		  <xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
		  <xsl:text> after which time if not accepted, we may choose to cancel this  
Commitment.  Furthermore, the loan must be advanced by no later than </xsl:text>
		  <xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		  <xsl:text> at which time we will have no further obligation to proceed with the advance.
\par \par </xsl:text>
    </xsl:if>
    <!-- #5137 -->
    <xsl:text>In the case of any inconsistency between any term in the 
Mortgage granted by you to us and registered against the Property and this 
Commitment, the terms contained in the Mortgage will govern and will override 
the terms in this Commitment.  In the case of any 
inconsistency between any term in the Mortgage and the Disclosure Statement, 
the terms contained in the Disclosure Statement will 
govern and will override the terms in the Mortgage.
\par \par </xsl:text>
    <xsl:if test="$isPreApproval"> 
      <xsl:text>Once you have made an offer on a Property, please contact us at </xsl:text>
      <xsl:value-of select="$tollFreePhoneA" /><!-- #DG798 -->
      <xsl:text> to discuss next steps.\par\par 
</xsl:text>
    </xsl:if>
    <xsl:text>Thank you for choosing </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> for your financing.\par }

\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 
\par \page 
\par }</xsl:text>
	</xsl:template>
	<!--
<xsl:template name="EnglishPage2Start">
<xsl:text>{\sect } \linex0\headery706\footery260\endnhere\sectdefaultcl </xsl:text>
</xsl:template>
-->
	
  <xsl:template name="EnglishPage2">
		<xsl:text>\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18\ul TERMS AND CONDITIONS:
\par 
\par Mortgage:
\par }
{\f0\fs18 
This Mortgage will be subject to all extended terms set forth in our standard 
form of mortgage contract or in the mortgage contract prepared by our 
solicitors, whichever the case may be, and insured mortgage loans will be 
subject to the provisions of the National Housing Act (N.H.A.) and the 
regulations thereunder.\par }
{\b\f0\fs18\ul \par Rate Guarantee and Rate Adjustment Policies:\par }
{\f0\fs18 Provided that the Mortgage Loan is advanced by </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text>, the interest rate will be </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/InterestRate"/>
    <xsl:text> per annum, calculated half yearly, not in advance. In the event 
that the effective rate of interest is lower at closing, the interest rate may 
be adjusted.
\par 
\par }{\b\f0\fs18\ul Title Requirements / Title Insurance:
\par }
{\f0\fs18 Title to the Property must be acceptable to our Solicitor who 
will ensure that the Mortgagor(s) have good and marketable title in fee simple 
to the property and that it is clear of any encumbrances which might affect 
the priority of the </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> Mortgage. Alternatively, we will accept title insurance from a  
title insurance provider acceptable to </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>. The Mortgage must be a first charge on the property.
\par 
\par }{\b\f0\fs18\ul Survey Requirements / Title Insurance:
\par } 
{\f0\fs18 A survey or surveyors certificate completed by a recognized land 
surveyor and dated within the last ten (10) years is to be furnished to our 
Solicitor prior to any advance. The survey must confirm that the location of 
the building(s) on the property complies with all municipal, provincial and 
other government requirements. A survey older than ten (10) years, but not 
older than twenty (20) years, may be accepted provided that it is accompanied 
by a Declaration of Possession from the then present owners of the property. 
Alternatively, we will accept title insurance from a title insurance provider 
acceptable to </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>.\par  
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 \par 
\page 


\par }</xsl:text>
	</xsl:template>

	<!--
<xsl:template name="EnglishPage3Start">
<xsl:text>{\sect }\sectd \linex0\headery706\footery260\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\f0\fs20 Page {\field{\*\fldinst {\cs22  PAGE }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}{\f0\fs20  of }{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
</xsl:template>
-->
	<xsl:template name="EnglishPage3">
		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18\ul Privilege Payment Policies: }{\f0\fs18 \par This mortgage is 
CLOSED. The Mortgagor (borrower), when not in default of any of the terms, 
covenants, conditions or provisions contained in the mortgage, shall have the 
following privileges for payment of extra principal amounts: 1) During each 12 
month period following the interest adjustment date the borrower may, without 
penalty: i) make prepayments totaling up to twenty percent (20%) of the 
original principal amount of the mortgage; ii) increase regular monthly 
payment by a total up to twenty percent (20%) of the original monthly payment 
amount so as to reduce the amortization of the mortgage without changing term. 
2) If the mortgaged property is sold pursuant to a bona fide arms length 
agreement, the borrower may repay the mortgage in full upon payment of three 
(3) months interest penalty calculated at the current interest rate of the 
mortgage. 3) In addition to the prepayment privileges described in paragraph 1,
 at any time after the third anniversary of the interest adjustment date, the 
borrower may prepay the mortgage in whole or in part upon payment of three (3) 
months interest penalty calculated at the current interest rate of the 
mortgage.\par \par }

{\b\f0\fs18\ul Interest Adjustment Payment }
{\f0\fs18 \par Prior to commencement of the regular monthly loan payments 
interest will accrue at the rate payable on the Mortgage loan, on all funds 
advanced to the Mortgagor(s). Interest will be computed from the advance date 
and will become due and payable on the first day of the month next following 
the advance date.
\par 
\par }{\b\f0\fs18\ul Prepayment Policies:
\par }{\f0\fs18 You can prepay this mortgage on payment of 3 months simple 
interest on the principal amount owing at the prepayment date, or the interest 
rate differential,whichever is greater.TBD
\par 
\par }{\b\f0\fs18\ul Renewal:
\par }{\f0\fs18 At the sole discretion of BasisXPressLender, the Mortgage may 
be renewed at maturity for any term, with or without a change in the interest 
rate payable under the Mortgage, by entering into one or more written 
agreements with the Mortgagor(s).

\par 
\par }{\b\f0\fs18\ul Early Renewal: 
\par }{\f0\fs18 Provided that the Mortgage is not in 
default and that the building on the lands described in the Mortgage is used 
only as residence and comprises no more than four (4) dwelling units, you may 
renegotiate the term and payment provisions of the Mortgage prior to the end 
of the original term of
 the Mortgage, provided that: 
\par }\pard \ql \widctlpar\intbl\tx180\tx1000\faauto {\f0\fs18 \tab a) \tab 
the Mortgage may only be renegotiated for a closed term
\par \tab b) \tab the Mortgagor(s) select a renewal option that results in the 
new Maturity Date being equal to or greater than
\par \tab \tab the original Maturity Date of the current term; and 
\par \tab c) \tab payment of the interest rate differential and an early 
renewal processing fee is made upon execution of such
\par \tab \tab renewal or amending agreement. 
\par }\pard \ql \widctlpar\intbl\faauto {\f0\fs18 
\par 
\par }{\b\f0\fs18\ul NSF Fee: 
\par }{\f0\fs18 There may be a fee charged for any payment returned due to 
insufficient funds or stopped payments.
\par 
\par }{\b\f0\fs18\ul Assumption Policies:
\par }{\f0\fs18 In the event that you subsequently enter into an agreement to 
sell, convey or transfer the property, any subsequent purchaser(s) who wish to 
assume this Mortgage, must be approved by </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>, otherwise this Mortgage Loan will become due and payable upon  
your vacating or selling the property.
\par 
\par }{\b\f0\fs18\ul Portability Policies:
\par }{\f0\fs18 Should you decide to sell the mortgaged property and purchase 
another property, you may transfer the remaining mortgage balance together 
with the interest rate set out therein to the new property provided that the 
Mortgage Loan is not in default and a new mortgage loan application has been 
accepted by </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> and all terms and conditions set out in the subsequent  
Mortgage Loan approval have been met.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishPage4">
		<!--#DG304 -->
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3  
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10  
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto {\b\f0\fs18\ul TERMS AND CONDITIONS\par 
\cell \row}

</xsl:text>
		<!--#DG304 all changes to the list below should be syncronized to the 'conditional upon' section -->
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition[conditionTypeId='2']">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text>
			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f0\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }
{\f0\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
\row 

</xsl:text>
		</xsl:for-each>
		<xsl:text>\pard\page\par </xsl:text>
		<!--#DG304 end -->
    <xsl:text>\trowd 
\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard
\ql \widctlpar\intbl\faauto {\b\f0\fs18\ul THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING
\par 
\par }</xsl:text>
<xsl:choose>
	<xsl:when test="$isPreApproval">
    <xsl:text>{\f0\fs18 The following conditions must be met unless waived by us 
in writing at our sole discretion, and the requested documents must be 
received in form and content satisfactory to us. </xsl:text>
    </xsl:when>
	<xsl:otherwise>
    <xsl:text>{\f0\fs18 The following conditions must be met, and the requested 
documents must be received in form and content satisfactory to </xsl:text>
		<!--#DG336 xsl:value-of select="//SolicitorsPackage/LenderName"/-->
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<!--#DG238 -->
    <xsl:text> no later than ten (10) days prior to the advance of the mortgage. 
Failure to do so may delay or void this commitment.</xsl:text>
	</xsl:otherwise>
</xsl:choose>
\par \par \cell }<xsl:text>
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10  
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908 
\row 
}
</xsl:text>
    <xsl:if test="/*/CommitmentLetter/Conditions">
      <!-- commented as it is printing 2 empty lines for the first row number - Venkata -->
      <!-- Venkata commented - Jun 22, 2007 xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text -->
		</xsl:if>
		<!--#DG304 all changes to the list below should be syncronized to the 'terms and conditions' section -->
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition[conditionTypeId='0']">
			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f0\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }

{\f0\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f0\fs18 \par \par }</xsl:text>
    <xsl:if test="$isPreApproval!=1">
      <xsl:text>{\b\f0\fs18\ul Requests for any changes to the terms and conditions 
of this Commitment will only be considered if your request is received by us 
in writing at least 5 business days prior to the Mortgage Advance Date.\par \par }

</xsl:text>
    </xsl:if>
    <xsl:text>{\b\f0\fs18\ul ACCEPTANCE:
\par 
\par }</xsl:text> 
    <xsl:choose>
      <xsl:when test="$isPreApproval">
		    <xsl:text>{\f0\fs18 This Commitment is open for acceptance by you until </xsl:text>
			   <xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
		    <xsl:text> after which time, if an offer has not been made on a  
Property, we may choose to cancel this Commitment. \par \par }</xsl:text>	
      </xsl:when>
	    <xsl:otherwise>
		    <xsl:text>{\f0\fs18 This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text>
			   <xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
         <xsl:text> after which time, if not accepted, we may choose to cancel this 
Commitment. Furthermore, the loan must be advanced by no later than </xsl:text>
		    <xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		    <xsl:text> at which time, we will have no further obligation to proceed with the advance.\par \par }</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
\pard \ql \widctlpar\intbl\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright 
<xsl:text>{\f0\fs18 \tab \tab </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> 
\par 
\par \tab Authorized by:___________________________________________
\par \tab \tab \tab </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Underwriter"/>
		<xsl:text>
\par 
\par 
\par }
\pard \ql \widctlpar\intbl\faauto 
{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 \par \page }</xsl:text>
	</xsl:template>
	
  <xsl:template name="EnglishPage5">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s15\ql \widctlpar\intbl\faauto </xsl:text>

<xsl:if test="$isPreApproval!=1"> 
<xsl:text>{\b\f0\fs18 PRE-AUTHORIZED DEBIT AUTHORIZATION
\par
\par }{\b\f0\fs18 You, } {\f0\fs18 being each Borrower named above, one or 
more of whom is authorized to sign on the account indicated on the voided 
specimen cheque that you have provided to }{\b\f0\fs18 us, } 
{\f0\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
<xsl:text>, or such replacement account as indicated on any new voided 
specimen cheque that you may provide to us, (each, }{\b\f0\fs18 your External 
Account}{\f0\fs18 ), agree to be bound by the following terms and conditions 
and authorize us to: (i) debit the External Account with the amount of each 
regular payment and any other amount that you owe to us from time to time 
under the Mortgage that you have granted to us on the day (or if such day is 
not a business day, the next business day) that such amount is due, (ii) debit 
your External Account with the amount of any overdue payment, unpaid interest 
and principal, default charges and any other amounts owing under the Mortgage 
at any time without prior notice to you, and (iii) transfer funds from the 
External Account to any account identified by you that you maintain at 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
    <xsl:text>, as appropriate, at the times (whether scheduled or unscheduled 
transfers) and in the amounts specified by you.
\par 
\par 
You acknowledge that this Authorization is provided for our benefit and the
benefit of the financial institution where the External Account is held
(the }{\b\f0\fs18 Processing Institution}{\f0\fs18 ) and is being provided in
consideration of such Processing Institution agreeing to process pre-authorized
debit requests and funds transfer requests (each, a }{\b\f0\fs18 PAD}{\f0\fs18 )
against your External Account in accordance with the rules of the Canadian
Payments Association. You represent and warrant that you are authorized to 
withdraw funds from the External Account without the authorization of any 
other person. Provided that you have specified the amount of a funds transfer, 
you agree that we are not required to give you notice before we process any 
funds transfer in that amount. You consent to the disclosure to the Processing 
Institution of any personal information that you have provided to us that is 
required by the Processing Institution to process each PAD. You agree to 
notify us of any changes in the information that you provided to us with 
respect to your External Account. You may cancel this Authorization at 
any time by giving 30 days' prior notice to us. That notice may be in 
writing or may be given orally (if we are able to verify your identity). This 
Authorization only applies to PADs and cancellation of this Authorization will 
not affect your obligations under the Mortgage, including your obligation to 
make all payments on your Mortgage by pre-authorized debit.
\par 
\par
You acknowledge that: (i) delivery of this Authorization to us also constitutes 
delivery thereof by you to the Processing Institution, and (ii) the Processing 
Institution is not required to verify that each PAD submitted by us has been 
issued in accordance with this Authorization (including the amount) or that the 
purpose of the payment for which a PAD was submitted has been fulfilled as a 
condition of honouring a PAD.
\par 
\par
You may dispute a PAD if (i) it was not drawn in accordance with this 
Authorization, or (ii) if you have cancelled this Authorization. In order to 
be reimbursed for a disputed PAD, you must deliver a written declaration that 
either (i) or (ii) above took place to the Processing Institution within 90 
days after the date that the disputed PAD was posted to your Account, and if 
you do not, the disputed PAD must be resolved between you and us.
\par 
\par
By signing below, each Borrower warrants to us on a continuing basis that no 
other signature is required to deal with the External Account.
\par 
\par }
\pard\plain \ql \widctlpar\intbl\faauto 
{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \par }
\pard \ql \widctlpar\intbl\faauto 
{\f0\fs18 Bank _______________________________________________ 
Branch_________________________________________________
\par 
\par Address____________________________________________________________________________________________________
\par 
\par Transit No.________________________________ Telephone No._____________________________________________________
\par 
\par }</xsl:text>
</xsl:if>

<xsl:text>{\b\f0\fs18\ul \par\par
PERSONAL INFORMATION 
\par 
\par }{\f0\fs18 The term "Personal Information" means any information about an 
identifiable individual. Personal Information about you, the Borrower(s), or 
the Guarantor(s), as applicable, is collected, used, and disclosed in 
accordance with the Canadian Tire Financial Services Privacy Communication 
(the } {\b\f0\fs18 Communication}{\f0\fs18 ), including, in particular, to 
process and maintain the Mortgage account of the Borrower(s). As described in 
the Communication, Personal Information may also be shared with other parties 
who administer the Mortgage and to market and sell other Canadian Tire branded 
products and services. The Communication is updated from time to time. You are 
referred to the most current version of the Communication, which is enclosed 
and which is also available online at myCTFS.com.
\par 
\par }</xsl:text>

    <xsl:if test="$isPreApproval!=1">  
	     <xsl:text>{\b\f0\fs18 ACCEPTANCE AND DISCLOSURE
\par \par}</xsl:text>
    </xsl:if>

    <xsl:text>{\b\f0\fs18I / We, the undersigned accept the terms of this Commitment 
as stated above and agree to fulfill the conditions of approval outlined herein.
\par \par }</xsl:text>

<xsl:if test="$isPreApproval!=1">  
	<xsl:text>{\b\f0\fs18By signing below, I / we (choose one of the following):
\par \par
_____ acknowledge receiving the Disclosure Statement at least 2 business days 
before I / we signed this Commitment 
\par \par
OR
\par \par
_____ acknowledge receiving the Disclosure Statement with this Commitment, and 
waive the requirement that it be provided
            to me / us 2 business days before I / We sign this Commitment.  
\par \par }</xsl:text>
</xsl:if>

<xsl:text>{\f0\fs18 Each Borrower has the right to receive an individual copy 
of the Disclosure Statement and all subsequent prescribed disclosure relating 
to the Mortgage loan (the }{\b\f0\fs18 Required Disclosure}{\f0\fs18 ).  
By signing below, each Borrower consents to </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<!--#DG256 -->
    <xsl:text> providing the Required Disclosure to the Borrower to whom this 
Commitment is addressed at the address for the Property subject to the 
Mortgage. Any Borrower named below may revoke this consent at any time by 
contacting a Customer Service Representative at the phone number listed above 
so that each of you will receive individually addressed copies of the Required 
Disclosure.
\par \par}</xsl:text>

<xsl:if test="$isPreApproval!=1">
	<xsl:text>{\b\f0\fs18 SIGNATURES
}{\f0\fs18 \par \par

Your signature(s) may be stored along with other information about you to form 
your signature card. If you apply for additional products in the future, your 
signature may also be used to form your signature card for those additional 
products.
\par \par }</xsl:text>
</xsl:if>

\pard \ql \widctlpar\intbl\tx100\tx4000\tx7800\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
<xsl:text>{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell ___________________________
\par <!--#DG350 APPLICANT-->BORROWER (sign above)
\par 
\par \cell ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\par \cell ___________________________
\par BORROWER (print name above)
\par 
\par \cell 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f0\fs18 ___________________________
\par BORROWER (sign above)
\par \par \cell }
{\b\f0\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\par \cell ___________________________
\par BORROWER (print name above)
\par 
\par \cell 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f0\fs18 ___________________________
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text> (sign above)
\par \par \cell }
{\b\f0\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\par \cell ___________________________
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text> (print name above)
\par 
\par \cell 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text>
	</xsl:template>

	<xsl:template name="EnglishFooter">
		<xsl:text>{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 
{\f0\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }{\f0\fs16 of }
{\field{\*\fldinst {NUMPAGES }}}}}</xsl:text>
	</xsl:template>
	
  <!-- ////////// CR224B Mortgage Pre-Approval Letter  \\\\\\\\\\ -->
  <xsl:template name="MortgagePreApprovalLetterEN">
    <xsl:call-template name="CommitmentLetterHeader">
      <xsl:with-param name="letterTitle" select="'MORTGAGE PREAPPROVAL'"/>
    </xsl:call-template>
    <xsl:text>
\f0\par
\pard\qj\f0 Thank you for applying for a Canadian Tire Mortgage\'99\super\fs18\'86\nosupersub\fs22 . 
It\rquote s a pleasure to inform you that your mortgage application has been pre-approved! \par
\par
\pard\qj Carefully review the enclosed Pre-Approval Certificate. \b0 If you have any questions feel free to call 
</xsl:text>
    <xsl:call-template name="SOBRegion"/>
    <xsl:text>
\par
\par
\pard\qj Once you have entered into a Purchase Agreement for your new home 
please call us to finalize your mortgage application.\par
\par
\pard\qj The Canadian Tire Financial Services Privacy Communication, which 
governs us and our collection, use and disclosure of personal information, is 
enclosed for your careful review and future reference. \par
\par 
\pard\qj If you have any questions or require further assistance, please do 
not hesitate to contact us. Thank you for choosing Canadian Tire Bank.\par
\par
\pard\par
Yours truly,\par
\par

\lang1033\
</xsl:text>
    <xsl:value-of select="$repName"/>
    <xsl:text>
\lang4105\par
\lang1033\
    </xsl:text>
    <xsl:choose>
      <xsl:when test="$sobRegionId = 1">
        <xsl:value-of select="$sobRegionDescription"/>
        <xsl:text>
\lang4105\par
Canadian Tire Bank \par
\lang1033\ 
</xsl:text>
<!-- <xsl:value-of select="$repNumber"/> -->
        <xsl:value-of select="java:com.basis100.deal.util.StringUtil.getAsFormattedPhoneNumber($repNumber, $repNumberExtension)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Your Customer Service Team</xsl:text>
        <xsl:text>
\lang4105\par
Canadian Tire Bank \par
</xsl:text>
        <xsl:value-of select="$tollFreePhone" /><!-- #DG798 -->
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>
\fs24\par\par

\pard \ltrpar\qc \li0\ri0\widctlpar\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0\pararsid12153335 
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
\f0\fs18\super\insrsid12153335\charrsid6691217 \'86}{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
\f0\fs18
\insrsid12153335\charrsid6691217 Canadian Tire Mortgage}
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
\f0\fs18\super\insrsid12153335\charrsid6691217 TM}
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
\f0\fs18\insrsid12153335\charrsid6691217 
 products are provided by Canadian Tire Bank. }{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
\f0\fs18\insrsid12153335\charrsid11536016 
\par }{\rtlch\fcs1 \af1\afs16 \ltrch\fcs0 \cs19
\f0\fs16\insrsid12153335\charrsid6691217 \'ae/\'99}{\rtlch\fcs1 \af1\afs16 \ltrch\fcs0 
\f0\fs16\insrsid12153335\charrsid6691217 The trademarks of Canadian Tire Corporation, Limited are used under licence.}{
\rtlch\fcs1 \af1 \ltrch\fcs0 \f0\insrsid12153335\charrsid6691217 
\par }\pard\plain \ltrpar\s16\qr 
\li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640\wrapdefault\aspalpha\aspnum\faauto
\adjustright\rin0\lin0\itap0\pararsid12153335 
\rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 
\fs24\lang4105\langfe4105\cgrid\langnp4105\langfenp4105 {
\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid12153335 \tab \tab }{\rtlch\fcs1 \af0\afs12 
\ltrch\fcs0 \fs12\insrsid12412007\charrsid9204318               }
{\rtlch\fcs1 \af0\afs12 \ltrch\fcs0 \f0\fs12\insrsid12412007\charrsid9204318 RB08/MTGPREAPP1}{\rtlch\fcs1 
\af0\afs12 \ltrch\fcs0 \f0\fs12\insrsid12153335\charrsid12153335 
\par }
</xsl:text>
</xsl:template>
	
  <!-- ////////// CR224b Mortgage Approval Purchase Letter \\\\\\\\\\ -->	
  <xsl:template name="MortgageApprovalPurchaseEN">
    <xsl:call-template name="CommitmentLetterHeader">
	     <xsl:with-param name="letterTitle" select="'MORTGAGE APPROVAL-PURCHASE'"/>
    </xsl:call-template>
    <xsl:text>
\par 
\pard\qj Thank you for applying for a Canadian Tire Mortgage\'99\super\'86\nosupersub . 
It\rquote s our pleasure to inform you that your mortgage application has been approved! \par
\par
\pard\qj Carefully review the enclosed Mortgage Commitment letter. If you 
have any questions, please feel free to call 
</xsl:text>
    <xsl:call-template name="SOBRegion"/>
    <xsl:text>
\par 
\par 
\pard\qj To acknowledge that you are accepting the terms and conditions set out 
in your Mortgage Commitment letter, initial each page, sign and date it.  
Please fax the Mortgage Commitment letter, toll-free to \b </xsl:text>
      <xsl:value-of select="$tollFreePhoneB" /><!-- #DG798 -->
      <xsl:text> \b0 or mail it in the enclosed self-addressed envelope. \par\par
 
\pard\qj It is important that you fulfill the conditions portion of the 
Mortgage Commitment letter by providing all of the requested documentation, 
\ul as soon as possible\ul0. The requested documents should be faxed or 
mailed with your Mortgage Commitment letter.\par\par 

\pard\qj The Canadian Tire Financial Services Privacy Communication, which 
governs us and our collection, use and disclosure of personal information, 
is enclosed for your careful review and future reference. \par\par 

\pard\qj If you have any questions or require further assistance, please do 
not hesitate to contact us.  Thank you for choosing Canadian Tire Bank.\par
\pard\par
Yours truly,\par\par

</xsl:text>
    <xsl:value-of select="$repName"/>
    <xsl:text>\par

</xsl:text>
    <xsl:choose>
      <xsl:when test="$sobRegionId = 1">
        <xsl:value-of select="$sobRegionDescription"/>
        <xsl:text>\par
Canadian Tire Bank \par
</xsl:text>
<!--  <xsl:value-of select="$repNumber"/> -->
        <xsl:value-of select="java:com.basis100.deal.util.StringUtil.getAsFormattedPhoneNumber($repNumber, $repNumberExtension)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Your Customer Service Team</xsl:text>
        <xsl:text>\par
Canadian Tire Bank \par
</xsl:text>
        <xsl:value-of select="$tollFreePhone" /><!-- #DG798 -->
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>\fs24\par\par

\pard \ltrpar\qc \li0\ri0\widctlpar\wrapdefault\aspalpha\aspnum\faauto
\adjustright\rin0\lin0\itap0\pararsid12153335 {\rtlch\fcs1 \af1\afs18 
\ltrch\fcs0 \f0\fs18\super\insrsid12153335\charrsid6691217 \'86}
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
\f0\fs18\insrsid12153335\charrsid6691217 Canadian Tire Mortgage}
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 \f0\fs18\super\insrsid12153335\charrsid6691217 TM}
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 \f0\fs18\insrsid12153335\charrsid6691217 
 products are provided by Canadian Tire Bank. }{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
\f0\fs18\insrsid12153335\charrsid11536016 
\par }{\rtlch\fcs1 \af1\afs16 \ltrch\fcs0 \cs19\f0\fs16\insrsid12153335\charrsid6691217 \'ae/\'99}
{\rtlch\fcs1 \af1\afs16 \ltrch\fcs0 \f0\fs16\insrsid12153335\charrsid6691217 
The trademarks of Canadian Tire Corporation, Limited are used under licence.}{
\rtlch\fcs1 \af1 \ltrch\fcs0 \f0\insrsid12153335\charrsid6691217 
\par }
\pard\plain \ltrpar\s16\qr \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640
\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
\pararsid12153335 \rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 
\fs24\lang4105\langfe4105\cgrid\langnp4105\langfenp4105 {
\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid12153335 \tab \tab }
{\rtlch\fcs1 \af0\afs12 \ltrch\fcs0 \fs12\insrsid12412007\charrsid9204318               }
{\rtlch\fcs1 \af0\afs12 \ltrch\fcs0 \f0\fs12\insrsid12412007\charrsid9204318 RB08/MTGSOLICAPP1}
{\rtlch\fcs1 
\af0\afs12 \ltrch\fcs0 \f0\fs12\insrsid12153335\charrsid12153335 
\par }
</xsl:text>
  </xsl:template>

  <!-- ////////// CR224b Mortgage Approval-Refinance Letter \\\\\\\\\\\ -->
  <xsl:template name="MortgageApprovalRefinanceEN">
    <xsl:call-template name="CommitmentLetterHeader">
	   <xsl:with-param name="letterTitle" select="'MORTGAGE APPROVAL-REFINANCE'"/>
    </xsl:call-template>
    <xsl:text>
\par
\pard\qj Thank you for applying for a Canadian Tire Mortgage\'99\super\'86\nosupersub\ .
It\rquote s our pleasure to inform you that your mortgage application has been approved! \par
\par
\pard\sl240\slmult0\qj\cf1 We are happy that you have decided to take advantage 
of our simple mortgage processing service at no cost to you. Carefully review 
the enclosed Mortgage Commitment letter.  If you have any questions, please 
feel free to call 
</xsl:text>
    <xsl:call-template name="SOBRegion"/>
    <xsl:text>
\par
\par
\pard\qj\cf0 To acknowledge that you are accepting the terms and conditions 
set out in your Mortgage Commitment letter, initial each page, sign, date and 
please fax the Mortgage Commitment letter, toll-free to \b </xsl:text>
      <xsl:value-of select="$tollFreePhoneB" /><!-- #DG798 -->
      <xsl:text> \b0 or mail it in the enclosed, self-addressed envelope. \par\par

It is important that you fulfill the conditions portion of the Mortgage 
Commitment letter by providing all of the requested documentation, 
\ul as soon as possible\ul0. The requested documents should be faxed or 
mailed with your Mortgage Commitment letter.\par\par

\pard\qj\b Please note, if we are paying out an existing mortgage, it can take 
a week or more for your current financial institution to provide us with a 
payout statement.  To ensure that we are able to fund your new mortgage in 
a timely manner, please be sure to complete and return the enclosed Mortgage 
Payout Authorization form with your Mortgage Commitment letter and requested 
documents.\b0\par\par

\pard\sl240\slmult0\qj\cf1 Once the documents have been received and processed 
by our office, a representative from First Canadian Title will contact you. 
They will make arrangements to come to your home at a convenient time to review 
and sign the mortgage documents with you. It's as simple as that! \par
\pard\qj\cf0\par

The Canadian Tire Financial Services Communication, which governs us and our 
collection, use and disclosure of personal information, is enclosed for your 
careful review and future reference. \par\par

If you have any questions or require further assistance, please do not 
hesitate to contact us.  Thank you for choosing Canadian Tire Bank.\par
\pard\par
Yours truly,\par
</xsl:text>
    <xsl:value-of select="$repName"/>
    <xsl:text>\par
</xsl:text>
    <xsl:choose>
      <xsl:when test="$sobRegionId = 1">
        <xsl:value-of select="$sobRegionDescription"/>
        <xsl:text>\par
Canadian Tire Bank \par
</xsl:text>
        <!-- <xsl:value-of select="$repNumber"/> -->
        <xsl:value-of select="java:com.basis100.deal.util.StringUtil.getAsFormattedPhoneNumber($repNumber, $repNumberExtension)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Your Customer Service Team</xsl:text>
        <xsl:text>
\lang4105\par
Canadian Tire Bank\par
</xsl:text>
        <xsl:value-of select="$tollFreePhone" /><!-- #DG798 -->
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>
\fs24\par\par

\pard \ltrpar\qc \li0\ri0\widctlpar\wrapdefault\aspalpha\aspnum\faauto
\adjustright\rin0\lin0\itap0\pararsid12153335 {\rtlch\fcs1 \af1\afs18 
\ltrch\fcs0 \f0\fs18\super\insrsid12153335\charrsid6691217 \'86}
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
\f0\fs18\insrsid12153335\charrsid6691217 Canadian Tire Mortgage}
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 \f0\fs18\super\insrsid12153335\charrsid6691217 TM}
{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 \f0\fs18\insrsid12153335\charrsid6691217 
 products are provided by Canadian Tire Bank. }{\rtlch\fcs1 \af1\afs18 \ltrch\fcs0 
 \f0\fs18\insrsid12153335\charrsid11536016 
\par }{\rtlch\fcs1 \af1\afs16 \ltrch\fcs0 \cs19\f0\fs16\insrsid12153335\charrsid6691217 \'ae/\'99}
{\rtlch\fcs1 \af1\afs16 \ltrch\fcs0 \f0\fs16\insrsid12153335\charrsid6691217 
The trademarks of Canadian Tire Corporation, Limited are used under licence.}{
\rtlch\fcs1 \af1 \ltrch\fcs0 \f0\insrsid12153335\charrsid6691217 
\par }\pard\plain \ltrpar\s16\qr \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640
\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0
\pararsid12153335 \rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 
\fs24\lang4105\langfe4105\cgrid\langnp4105\langfenp4105 {
\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid12153335 \tab \tab }
{\rtlch\fcs1 \af0\afs12 \ltrch\fcs0 \fs12\insrsid12412007\charrsid9204318               }
{\rtlch\fcs1 \af0\afs12 \ltrch\fcs0 \f0\fs12\insrsid12412007\charrsid9204318 RB08/MTGFCTAPP1}{\rtlch\fcs1 
\af0\afs12 \ltrch\fcs0 \f0\fs12\insrsid12153335\charrsid12153335 
\par }
</xsl:text>
  </xsl:template>

  <!-- CR224b Common Footer -->
  <xsl:template name="CommonFooter">
    <xsl:text>
{\footerr \ltrpar \pard\plain \ltrpar\qc \li0\ri0\widctlpar\wrapdefault
\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0\pararsid10058463 \rtlch\fcs1 
\af0\afs24\alang1025 \ltrch\fcs0 \fs24\lang1033\langfe1033\cgrid\langnp1033
\langfenp1033 {\rtlch\fcs1 \af0\afs20 \ltrch\fcs0 \fs20\lang4105\langfe4105
\langnp4105\langfenp4105\insrsid10058463\charrsid12667730 {\*\xmlopen\xmlns1
{\factoidname PostalCode}}
{\*\xmlopen\xmlns1{\factoidname Street}}P.O. Box 3000{\*\xmlclose}, 
{\*\xmlopen\xmlns1{\factoidname PostalCode}}Welland{\*\xmlclose}, 
{\*\xmlopen\xmlns1{\factoidname PostalCode}}Ontario{\*\xmlclose}  
{\*\xmlopen\xmlns1{\factoidname PostalCode}}L3B 5S5
{\*\xmlclose}{\*\xmlclose}
\par }\pard \ltrpar\ql \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640
\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 
{\rtlch\fcs1 \af0 \ltrch\fcs0 \lang4105\langfe4105\langnp4105\langfenp4105\insrsid12667730 
\par }}
</xsl:text>
  </xsl:template>
	
  <!-- CR224B Purchase Common Header for all Commitment Letters  -->	
  <xsl:template name="CommitmentLetterHeader">
     <xsl:param name="letterTitle"/>
<!--      
    <xsl:text>{\rtf1\adeflang1025\ansi\ansicpg1252\uc1\adeff0\deff0\stshfdbch0
\stshfloch0\stshfhich0\stshfbi0\deflang1033\deflangfe1033
{\fonttbl
{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}
{\f0\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
}
{\stylesheet{
\ql \li0\ri0\widctlpar\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0
\lin0\itap0 \rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 \fs22\lang1033
\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}
{\*\cs10 \additive \ssemihidden 
Default Paragraph Font;}{\*\ts11\tsrowd\trftsWidthB3\trpaddl108\trpaddr108
\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\tblind0\tblindtype3\tscellwidthfts0
\tsvertalt\tsbrdrt\tsbrdrl\tsbrdrb\tsbrdrr\tsbrdrdgl\tsbrdrdgr\tsbrdrh\tsbrdrv 
\ql \li0\ri0\widctlpar\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0
\lin0\itap0 \rtlch\fcs1 \af0\afs20 \ltrch\fcs0 \fs20\lang1024\langfe1024\cgrid
\langnp1024\langfenp1024 \snext11 \ssemihidden Normal Table;}}

{\*\latentstyles\lsdstimax156\lsdlockeddef0}{\*\rsidtbl \rsid7739569\rsid9837457\rsid14484980}
{\*\generator Microsoft Word 11.0.8026;}{\info{\title }{\author  }
{\operator  }{\creatim\yr2008\mo8\dy12\hr13\min35}
{\revtim\yr2008\mo8\dy12\hr13\min36}{\version1}{\edmins1}{\nofpages1}{\nofwords3}{\nofchars18}{\*\company Filogix Inc}{\nofcharsws20}{\vern24609}{\*\password 00000000}}{\*\xmlnstbl }
\paperw12240\paperh15840
\margl1080\margr900\margt719\margb1440
\gutter0\ltrsect \widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc
\dntblnsbdb\nospaceforul\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1080\dgvorigin719\dghshow1\dgvshow1
\jexpand
\viewkind1\viewscale90\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule\nobrkwrptbl\snaptogridincell\allowfieldendsel\wrppunct
\asianbrkrule\rsidroot9837457\newtblstyruls\nogrowautofit \fet0
{\*\wgrffmtfilter 013f}\ilfomacatclnup0\ltrpar 
\sectd \ltrsect\linex0\endnhere\sectlinegrid360\sectdefaultcl\sectrsid9837457\sftnbj 
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang 
{\pntxta .}}{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang 
{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang 
{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang 
{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang {\pntxtb (}
{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang 
{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang 
{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang 
{\pntxtb (}{\pntxta )}}{\*\pnseclvl9
\pnlcrm\pnstart1\pnindent720\pnhang {\pntxtb (}{\pntxta )}}</xsl:text>
-->    
    <xsl:text>
\pard\plain \ltrpar\qc \li0\ri0\widctlpar\wrapdefault\aspalpha\aspnum\faauto
\adjustright\rin0\lin0\itap0 \rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 
\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{\rtlch\fcs1 \af1 \ltrch\fcs0 \b\f0\insrsid9837457\charrsid9837457 </xsl:text>
    <xsl:value-of select="$letterTitle"/>
    <xsl:text>
}{\rtlch\fcs1 \af1 \ltrch\fcs0 \b\f0\insrsid14484980\charrsid9837457 
\par }
\cellx10401\pard\intbl
</xsl:text>
    <xsl:call-template name="Logo"/>
    <xsl:text>
\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx4354\cellx10401\pard\intbl\lang1033\b\fs20 Primary Borrower:\b0\par
</xsl:text>
    <xsl:for-each select="/*/Deal/Borrower[primaryBorrowerFlag='Y']">
      <xsl:value-of select="salutation"/>
      <xsl:text> </xsl:text>
	     <xsl:value-of select="borrowerFirstName"/>
		  <xsl:text> </xsl:text>
	   <xsl:value-of select="borrowerLastName"/>
    </xsl:for-each>
    <xsl:text>
\par 
\lang4105\par
\lang1033\b 
</xsl:text>
    <xsl:if test="string-length($isCoBorrowers) != 0">
      <xsl:text>CoBorrowers: \b0\par </xsl:text>
      <xsl:for-each select="/*/Deal/Borrower[primaryBorrowerFlag!='Y']">
          <xsl:value-of select="salutation"/>
          <xsl:text> </xsl:text>
      	<xsl:value-of select="borrowerFirstName"/>
      	<xsl:text> </xsl:text>
      	<xsl:value-of select="borrowerLastName"/>
      	<xsl:text>\par </xsl:text>
      </xsl:for-each>
    </xsl:if>
    <xsl:text>
\par 
\lang1033\b Mailing Address:\b0\par 
</xsl:text>
    <xsl:for-each select="/*/Deal/Borrower[primaryBorrowerFlag='Y']">
    	<xsl:value-of select="BorrowerAddress/Addr/addressLine1"/>
    	<xsl:text>\par </xsl:text>
    	<xsl:if test="string-length(BorrowerAddress/Addr/addressLine2)!=0">
    		<xsl:value-of select="BorrowerAddress/Addr/addressLine2"/>
    		<xsl:text>\par </xsl:text>
    	</xsl:if>
    	<xsl:value-of select="BorrowerAddress/Addr/city"/>
    	<xsl:text>,  </xsl:text>
    	<xsl:value-of select="BorrowerAddress/Addr/province"/>
    	<xsl:text>\par </xsl:text>
    	<xsl:value-of select="BorrowerAddress/Addr/postalFSA"/>
    	<xsl:text> </xsl:text>
    	<xsl:value-of select="BorrowerAddress/Addr/postalLDU"/>
    </xsl:for-each>
    <xsl:text>
\par
\cell                                                          \qr\lang1033 </xsl:text>
    <xsl:value-of  select="/*/CommitmentLetter/CurrentDate"/>  
    <xsl:text>\lang4105\par
\par
\par

\qr\b Mortgage Reference Number:\b0\lang1033\ 
</xsl:text>
    <xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
    <xsl:text>
\lang4105\cell\row

\pard\nowidctlpar\par
\fs18 Dear \lang1033\ 
</xsl:text>
    <xsl:for-each select="/*/Deal/Borrower">
        <xsl:sort select="primaryBorrowerFlag" order="descending"/>
        <xsl:value-of select="salutation"/>
        <xsl:text> </xsl:text>
    	<xsl:value-of select="borrowerFirstName"/>
    	<xsl:text> </xsl:text>
    	<xsl:value-of select="borrowerLastName"/>
    	<xsl:choose>
    		<xsl:when test="position() = last()">
    		<xsl:text></xsl:text>
    		</xsl:when>
    		<xsl:otherwise>
    		<xsl:text>, </xsl:text>
    		</xsl:otherwise>
    	</xsl:choose>
    </xsl:for-each>
    <xsl:text>\par 
</xsl:text>
  </xsl:template>

  <!-- CR224b SOBRegion template -->
  <xsl:template name="SOBRegion">
    <xsl:choose>
        <xsl:when test="$sobRegionId = 1">
            <xsl:text>me, </xsl:text>
            <xsl:value-of select="$repName"/>
      		<xsl:text>, your </xsl:text>
      		<xsl:value-of select="$sobRegionDescription"/>
      		<xsl:text>, at </xsl:text>
      		<!-- <xsl:value-of select="$repNumber"/> -->
      		<xsl:value-of select="java:com.basis100.deal.util.StringUtil.getAsFormattedPhoneNumber($repNumber, $repNumberExtension)"/>
      		<xsl:text>.</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>\lang4105 one of our Customer Service Representatives toll-free at \b </xsl:text>
            <xsl:value-of select="$tollFreePhone" /><!-- #DG798 --> 
            <xsl:text>\b0 .</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="DocumentEnd">
    <xsl:text>&#134;Canadian Tire MortgageTM products are provided by Canadian Tire Bank. 
&#174;/&#153;The trademarks of Canadian Tire Corporation, Limited are used under licence.
		              RB08/MTGPREAPP1
</xsl:text>
  </xsl:template>

  <!-- CR224b DIsclosure Statement. Important Dates Chapter -->
  <xsl:template name="ImportantDatesChapter">
    <xsl:text>
\trowd\trgaph108\trleft-108
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\intbl\tldot\tx7056\b 1. Important Dates\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\qj\intbl\fi-360\li720\tx720\tldot\tx7056
\f0\fs18 -\tab\b\i\f0\fs18 Mortgage Advance Date:  
</xsl:text>
    <xsl:value-of select="$mtgAdvDate"/>
    <xsl:text>\par

\pard\intbl\fi-360\li720\cf0\b0\i0
\f0\fs18 -\tab\b\i\f0\fs18 Interest Adjustment Date:  
</xsl:text>
    <xsl:value-of select="$iadDate"/>
    <xsl:text>\par

\cf0\b0\i0\f0\fs18 -\tab\b\i\f0\fs18 First Payment Date:  
</xsl:text>
    <xsl:value-of select="$firstPaymentDate"/>
    <xsl:text>\par

\cf0\b0\i0\f0\fs18 -\tab\b\i\f0\fs18 Maturity Date:  
</xsl:text>
    <xsl:value-of select="$maturityDate"/>
    <xsl:text>\par

</xsl:text>
  </xsl:template>

  <!-- CR224b DIsclosure Statement. Principal Amount Chapter -->
  <xsl:template name="PrincipalAmountChapter">
    <xsl:text>
\pard\intbl\tldot\tx7056\cf0\i0\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 2. Principal Amount\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 
The amount which you are borrowing from us (the\b\i  Principal Amount\b0\i0 ) is 
\b\cell\b0\cell 
</xsl:text>
    <xsl:value-of select="$principalAmount"/>
    <xsl:text>\par

</xsl:text>
  </xsl:template>


  <!-- CR224b DIsclosure Statement. Insurance Premium Chapter -->
  <xsl:template name="InsurancePremiumChapter">
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\clvertalc\cellx8346\clvertalc\cellx10188\pard\intbl\tldot\tx7056\cf0\b 3. Insurance Premium\par
\cell\pard\intbl\qc\tldot\tx7056\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 The Principal 
Amount includes a mortgage insurance premium of that we pay to the Mortgage Insurer. 
\b\cell\b0\cell \par
</xsl:text>
    <xsl:value-of select="$insurancePremium"/>
    <xsl:text>\par

</xsl:text>
  </xsl:template>


  <!-- CR224b DIsclosure Statement. Advance Of Funds Chapter -->
  <xsl:template name="AdvanceOfFundsChapter">
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\clvertalc\cellx8346\clvertalc\cellx10188\pard\intbl\tldot\tx7056\cf0\b 
4. Advance of Funds\par
\cell\pard\intbl\qc\tldot\tx7056\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\clvertalc\cellx8346\clvertalc\cellx10188\pard\qj\intbl\tldot\tx7056 
After deducting the amount, if any, in section 3 above (plus applicable taxes), 
the net amount of money that we will advance to your solicitor on the Mortgage Advance Date is \par
\cell\par
\par

\cell\cf1\ 
</xsl:text>
    <xsl:value-of select="$netAdvance"/>
  </xsl:template>

  <!-- CR224b Mortgage Disclosire Fixed -->
  <xsl:template name="MortgageDisclosireFixedEN">
    <xsl:param name="lastBrace"/>
    <xsl:call-template name="DisclosureStatementHeader">
      <xsl:with-param name="rateType" select="'Fixed Rate (Closed)'"/>
    </xsl:call-template>
    <xsl:call-template name="CommonFooter"/>
    <xsl:call-template name="ImportantDatesChapter"/>
    <xsl:call-template name="PrincipalAmountChapter"/>
    <xsl:call-template name="InsurancePremiumChapter"/>
    <xsl:call-template name="AdvanceOfFundsChapter"/>
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\clvertalc\cellx8346\clvertalc\cellx10188\pard\intbl\tldot\tx7056\cf0\b 5. Interest\par
\cell\pard\intbl\qc\tldot\tx7056\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\clvertalc\cellx8346\clvertalc\cellx10188\pard\qj\intbl\tldot\tx7056 
Interest is charged from the Mortgage Advance Date on the balance of the 
Principal Amount that remains unpaid at an annual rate of \cf1\b\
</xsl:text>
    <xsl:value-of select="$fixedInterestRate"/>
    <xsl:text>
\cf0\b0  (the \b\i Interest Rate\b0\i0 ).  Interest is calculated using a 
daily interest rate factor which is equivalent to the Interest Rate 
calculated semi-annually not in advance.\par\par

If you do not pay us any amount required to be paid under the Mortgage that 
is in respect of interest when it is due, you will be charged interest on such 
arrears of interest (this is called compound interest) at the Interest Rate.  
You must pay compound interest immediately when we ask you to pay it.  If any 
arrears of interest and compound interest on such arrears are not paid in full 
by the end of any successive six-month period following the Interest Adjustment 
Date, such arrears and compound interest will be added to the Principal Amount 
on such day and interest at the Interest Rate will be charged on such increased 
Principal Amount.\par

\cell\pard\intbl\qc\tldot\tx7056\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\intbl\tldot\tx7056\b 6. Term and Amortization Period**\par

\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\qj\intbl\tldot\tx7056 The term of the Mortgagee is \cf1\b\ 
</xsl:text>
    <xsl:value-of select="$paymentTerm"/> 
    <xsl:text>\cf0\b0 .\par\par

The amortization period of the Mortgage is \cf1\b\ 
</xsl:text>
    <xsl:value-of select="$effectiveAmortization"/> 
    <xsl:text>\cf0\b0 . \par\par

Both the Term and the Amortization Period begin on the Interest Adjustment Date.\par

\cell\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\intbl\tldot\tx7056\b 7. Cost of Borrowing\par

\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\qj\intbl\tldot\tx7056 The cost to you of borrowing the Principal Amount 
(\i the \b Cost of Borrowing\b0\i0 ) is \par

which is the amount of interest that will accrue from the Mortgage Advance Date 
until the Maturity Date, assuming that you make all payments when due and make 
no prepayments.\par
\cell\cell\cf1\ 
</xsl:text>
    <xsl:value-of select="$interestOverterm"/> 
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\intbl\tldot\tx7056\cf0\b 8. Payments\par

\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\qj\intbl\tldot\tx7056\b Interest Adjustment Payment\par\par

\b0 On the Interest Adjustment Date you must pay us interest in the amount of \par
which covers interest on the Principal Amount from the Mortgage Advance Date to 
but excluding the Interest Adjustment Date.\par\par

\b Principal and Interest Payments\par\par

\b0 Beginning with the First Payment Date, you must make \cf1\b\ 
</xsl:text>
    <xsl:value-of select="$paymentFrequence"/> 
    <xsl:text>, \cf0\b0 regular payments of principal and interest, each in the amount of \par
to and including the Maturity Date.  Each regular payment is due \cf1\b\
</xsl:text>
    <xsl:value-of select="$paymentDueOn"/>
    <xsl:text>\b0  \cf0 (your \b regular payment date\b0 ).\par\par

On the Maturity Date (unless the Mortgage has been renewed) you must pay the 
outstanding balance of the Principal Amount and accrued interest in the total 
amount of \par
\pard\qj\intbl\keep\keepn\tldot\tx7056\b\par
Total Payments\par
\b0\par
The total amount of all payments that you must make to us from the Mortgage 
Advance Date until the end of the Term is \par
\par
\pard\qj\intbl\tldot\tx7056\b Application of Payments\par
\b0\par
Each regular payment that you make is applied first to the accrued interest 
and the balance, if any, is then applied to the outstanding Principal 
Amount.\par
\cell\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\cell\par
\par
\cf1\ 
</xsl:text>
    <xsl:value-of select="$iadPayment"/> 
    <xsl:text>\par
\par
\par
\par
\par
\par
\par
</xsl:text>
    <xsl:value-of select="$pandiPaymentAmount"/>
    <xsl:text>\par
\par
\par
\par
\par
</xsl:text>
    <xsl:value-of select="$balanceOnMaturity"/>
    <xsl:text>\par
\par
\par
\par
\par
</xsl:text>
    <xsl:value-of select="$totalPayments"/>
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\cf0\b
9. Prepayment Privileges\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989
\cellx8346
\cellx10188

\pard\qj\intbl\tldot\tx7056 You have the following prepayment privileges, 
provided you are not in default under the Mortgage:\par\par

(a)   In the 12 month period beginning on the Interest Adjustment Date and in 
each 12 month period thereafter (each, a \b mortgage year\b0 ) you may prepay 
in total up to 20% of the Principal Amount (the\b  free prepayment amount\b0 ) 
without any charge.  The free prepayment amount is not cumulative and therefore 
if not used in a mortgage year, the unused portion of the free prepayment 
amount cannot be carried forward and used in a later mortgage year.  Any 
prepayment must be made on a regular payment date and requires 10 days written 
notice to us.  \par\par

(b)   On 10 days written notice to us, you may increase the amount of your 
regular payments of principal and interest, provided that the total of all 
such increases during any mortgage year does not exceed 20% of the amount of 
the first regular payment that was required under the current term of this 
mortgage. \b  \b0 After you have increased the amount of your regular payments, 
you may, on 10 days\rquote  written notice to us, reduce the amount of your 
regular payments to an amount that is not less than the amount of the first 
regular payment that was required under the mortgage.\b\par\par

\pard\intbl\keep\keepn
\fi-360\li720\tx720\tldot\tx7056\b0\fs18 (c)\tab On any regular payment date 
you may prepay all or any part of the Principal Amount, but to the extent that 
the amount that you prepay exceeds the unused free prepayment amount in that 
mortgage year (the \b excess prepayment amount\b0 ) you must pay us a 
prepayment charge in an amount equal to the greater of (i) 3 months\rquote 
interest at the Interest Rate on the excess prepayment amount,\b \b0 or (ii) 
the prepayment interest rate differential amount.\b\line\par
\pard\intbl\keep\keepn\tldot\tx7056\par
\par
\pard\intbl\tldot\tx7056 Prepayment interest rate differential amount \b0 
means the amount by which (a) below exceeds (b) below:\par
\par
(a) the total amount of interest which would have been payable on the excess 
prepayment amount, calculated from the date of prepayment to the Maturity Date,
 at the Interest Rate; and\par
\par
(b) the total amount of interest which would have been payable on the excess 
prepayment amount, calculated from the date of prepayment to the Maturity Date,
 at the fixed annual interest rate set by us, in our sole discretion, and 
which would be applicable to a closed first mortgage for a term commencing on 
the date of prepayment and expiring on the Maturity Date, or for a mortgage 
term nearest in length to the remainder of the Term on the prepayment date;\par
\par
(If (a) above is the same or less than (b) above, then the prepayment interest 
rate differential amount is zero.)\par
\cell\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 10. Other Charges\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188
\pard\intbl\qj\tldot\tx7056 If the Mortgage is not repaid in full on the 
Maturity Date (unless it has been renewed) or you do not make any payment when 
due, in addition to compound interest, you may be charged legal fees and costs 
(including the fees and costs of our internal legal counsel), on a substantial 
indemnity basis, resulting from actions to collect a payment or to enforce the 
Mortgage and any other security, which you must then pay to us.\par
\pard\intbl\li360\tldot\tx7056\par
\pard\intbl\tldot\tx7056 In addition, you must pay us our fee (currently $30) 
for any pre-authorized debit, cheque or other payment instrument that is 
dishonoured when presented to your financial institution. \par
\par
After the Mortgage has been paid in full, you will be responsible for paying 
applicable government registration fees and if applicable, our fee for 
preparing the discharge. Our discharge fee is \par
currently \cell\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\cell\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\pard\intbl
\ql\tldot\tx7056 $200.00\cell\row

\pard\nowidctlpar\lang4105\par
</xsl:text>
    <xsl:value-of select="$lastBrace"/>
  </xsl:template>

  <!-- CR224b Mortgage Disclosire Variable -->
  <xsl:template name="MortgageDisclosireOpenEN">
    <xsl:param name="lastBrace"/>
    <xsl:call-template name="DisclosureStatementHeader">
      <xsl:with-param name="rateType" select="'Fixed Rate (Open)'"/>
    </xsl:call-template>
    <xsl:call-template name="CommonFooter"/>
    <xsl:call-template name="ImportantDatesChapter"/>
    <xsl:call-template name="PrincipalAmountChapter"/>
    <xsl:call-template name="InsurancePremiumChapter"/>
    <xsl:call-template name="AdvanceOfFundsChapter"/>
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\clvertalc\cellx8346\clvertalc\cellx10188\pard\intbl\tldot\tx7056\cf0\b 5. Interest\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188
\pard\qj\intbl\tldot\tx7056 Interest is charged from the Mortgage Advance Date 
on the balance of the Principal Amount that remains unpaid at an annual rate 
of \cf1\lang1033\b\
</xsl:text>
<xsl:value-of select="$fixedInterestRate"/>
<xsl:text>
\lang4105 \cf0\b0 (the \b\i Interest Rate\b0\i0 ). Interest is calculated 
using a daily interest rate factor which is equivalent to the Interest Rate 
calculated semi-annually not in advance.\par
\par
If you do not pay us any amount required to be paid under the Mortgage that is 
in respect of interest when it is due, you will be charged interest on such 
arrears of interest (this is called compound interest) at the Interest Rate. 
You must pay compound interest immediately when we ask you to pay it. If any 
arrears of interest and compound interest on such arrears are not paid in full 
by the end of any successive six-month period following the Interest 
Adjustment Date, such arrears and compound interest will be added to the 
Principal Amount on such day and interest at the Interest Rate will be charged 
on such increased Principal Amount.\par
\cell\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\clvertalc\cellx8346\clvertalc\cellx10188\pard\intbl\tldot
\tx7056\b 6. Term and Amortization Period**\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 The term of the Mortgagee is \cf1\lang1033\b\ 
</xsl:text>
<xsl:value-of select="$paymentTerm"/>
<xsl:text>
\b0\lang4105  \cf0.\par
\par
The amortization period of the Mortgage is \cf1\lang1033\b\ 
</xsl:text>
<xsl:value-of select="$effectiveAmortization"/>
<xsl:text>
\cf0\lang4105\b0 . \par
\par
Both the Term and the Amortization Period begin on the Interest Adjustment Date.\par
\cell\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 7. Cost of Borrowing\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 The cost to you of 
borrowing the Principal Amount (\i the \b Cost of Borrowing\b0\i0 ) is \par 
which is the amount of interest that will accrue from the Mortgage Advance 
Date until the Maturity Date, assuming that you make all payments when due and 
make no prepayments.\par
\cell \par
\par

\cell\cf1\lang1033\ 
</xsl:text>
    <xsl:value-of select="$interestOverterm"/>
    <xsl:text>\cf0\lang4105\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 8. Payments\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056\b Interest Adjustment Payment\par
\par
\b0 On the Interest Adjustment Date you must pay us interest in the amount of \par
which covers interest on the Principal Amount from the Mortgage Advance Date 
to but excluding the Interest Adjustment Date.\par
\par
\b Principal and Interest Payments\par
\par
\b0 Beginning with the First Payment Date, you must make \cf1\lang1033\b\
</xsl:text>
<xsl:value-of select="$paymentFrequence"/> 
<xsl:text>
\lang4105 , \cf0\b0 regular payments of principal and interest, each in the amount of \par
to and including the Maturity Date.  Each regular payment is due \cf1\lang1033\b\
</xsl:text>
<xsl:value-of select="$paymentDueOn"/>
<xsl:text>
\lang4105\b0  \cf0 (your \b regular payment date\b0 ).\par
\par
On the Maturity Date (unless the Mortgage has been renewed) you must pay the 
outstanding balance of the Principal Amount and accrued interest in the total 
amount of \par
\pard\intbl\keep\keepn\tldot\tx7056\b\par
Total Payments\par
\b0\par
The total amount of all payments that you must make to us from the Mortgage 
Advance Date until the end of the Term is \par
\par
\pard\intbl\tldot\tx7056\b Application of Payments\par
\b0\par
Each regular payment that you make is applied first to the accrued interest 
and the balance, if any, is then applied to the outstanding Principal 
Amount.\par
\cell\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\cell\cf1\par
\par
\lang1033\ 
</xsl:text>
<xsl:value-of select="$iadPayment"/>
<xsl:text>
\lang4105\par
\par
\par
\par
\par
\par
\par
\lang1033\ 
</xsl:text>
    <xsl:value-of select="$pandiPaymentAmount"/>
<xsl:text>
\lang4105\par
\par
\par
\par
\par
\lang1033\ 
</xsl:text>
<xsl:value-of select="$balanceOnMaturity"/>
<xsl:text>
\lang4105\par
\par
\par
\par
\lang1033\
</xsl:text>
<xsl:value-of select="$totalPayments"/>
<xsl:text>
\cf0\lang4105\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 9. Prepayment Privileges\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188
\pard\qj\intbl\tldot\tx7056 If you are not in 
default under the Mortgage, you may, without charge, on 10\b \b0 days\rquote 
written notice to us, prepay all or any part of the Principal Amount or 
increase by any amount the amount of your regular payments of principal and 
interest. After you have increased the amount of your regular payment, you may,
 on 10 days\rquote written notice to us, reduce the amount of your regular 
payments to an amount that is not less than the amount of the first regular 
payment required under the current term of the Mortgage.\par
\cell\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 10. Other Charges\par
\cell\b0\cell\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 If the Mortgage 
is not repaid in full on the Maturity Date (unless it has been renewed) or 
you do not make any payment when due, in addition to compound interest, you 
may be charged legal fees and costs (including the fees and costs of our 
internal legal counsel), on a substantial indemnity basis, resulting from 
actions to collect a payment or to enforce the Mortgage and any other security,
 which you must then pay to us.\par
\pard\intbl\li360\tldot\tx7056\par
\pard\qj\intbl\tldot\tx7056 In addition, you must pay us our fee (currently 
$30) for any pre-authorized debit, cheque or other payment instrument that is 
dishonoured when presented to your financial institution.\par
\par
After the Mortgage has been paid in full, you will be responsible for paying 
applicable government registration fees and, if applicable, our fee for 
preparing the discharge. Our discharge fee is currently\'85\'85\cell\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
       \cell\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
$200.00\cell\row

\pard\f0\par
</xsl:text>
    <xsl:value-of select="$lastBrace"/>
  </xsl:template>

  <!-- CR224b Mortgage Disclosire Variable -->
  <xsl:template name="MortgageDisclosireVariableEN">
    <xsl:param name="lastBrace"/>
    <xsl:call-template name="DisclosureStatementHeader">
      <xsl:with-param name="rateType" select="'Variable Rate (Closed)'"/>
    </xsl:call-template>
    <xsl:call-template name="CommonFooter"/>
    <xsl:call-template name="ImportantDatesChapter"/>
    <xsl:call-template name="PrincipalAmountChapter"/>
    <xsl:call-template name="InsurancePremiumChapter"/>
    <xsl:call-template name="AdvanceOfFundsChapter"/>
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 5. Interest\par
\cell\b0\cell\pard\intbl\qr\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 Interest is charged 
from the Mortgage Advance Date on the balance of the Principal Amount that 
remains unpaid at a variable annual rate equal to the Canadian Tire prime rate 
from time to time in effect (currently \lang1033\b\
</xsl:text>
<xsl:value-of select="$internalRatePercentage"/>
<xsl:text>
\lang4105)\b0  
</xsl:text>
<xsl:choose>
    <xsl:when test="translate($netInterestRate, '%', '') &gt;= translate($internalRatePercentage, '%', '')">
<xsl:text>
plus
</xsl:text>
    </xsl:when>
    <xsl:otherwise>
<xslText>
less
</xslText>    
    </xsl:otherwise>
</xsl:choose> 
<xsl:text>
 an adjustment factor of \lang1033\b\ 
</xsl:text>
    <xsl:value-of select="$adjustmentFactor"/>
<xsl:text>
\lang4105 %\b0  (the \b Variable Interest Rate\b0 ).  Your \b 
Current Variable Interest Rate\b0  is therefore \lang1033\b\ 
</xsl:text>
	<xsl:value-of select="$netInterestRate"/>
<xsl:text>
\lang4105 \b0 per annum calculated monthly not in advance. Please see the 
attached Variable Equivalent Rate Table for the equivalent per annum rate 
calculated half-yearly not in advance. The \b Canadian Tire prime rate\b0 is 
the variable annual rate of interest that Canadian Tire Bank charges for 
variable interest loans and declares as its prime rate, which rate is subject 
to change from time to time at Canadian Tire Bank\rquote s sole discretion. 
The Variable Interest Rate is adjusted automatically on the first business day 
following any change in the Canadian Tire prime rate and will apply until the 
day before it is next adjusted. \par
\par
If you do not pay us any amount required to be paid under the Mortgage that is 
in respect of interest when it is due, you will be charged interest on such 
arrears of interest (this is called compound interest) at the Variable 
Interest Rate. You must pay compound interest immediately when we ask you to 
pay it. If any arrears of interest and compound interest on such arrears are 
not paid in full by the end of any successive six-month period following the 
Interest Adjustment Date, such arrears and compound interest will be added to 
the Principal Amount on such day and interest at the Variable Interest Rate 
will be charged on such increased Principal Amount.\par
\cell\cell\pard\intbl\qr\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 6. Term and Amortization Period**\par
\cell\b0\cell\pard\intbl\qr\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 The term of the Mortgagee is \lang1033\b\ 
</xsl:text>
    <xsl:value-of select="floor(/*/Deal/actualPaymentTerm div 12)"/>
<xsl:text>
\lang4105  \b0 years and \lang1033\b\ 
</xsl:text>
    <xsl:value-of select="/*/Deal/actualPaymentTerm mod 12"/>
<xsl:text>
\lang4105\b0  months (the \b\i Term\b0\i0 ).\par
\par
The amortization period of the Mortgage is \lang1033\b\ 
</xsl:text>
    <xsl:value-of select="floor(/*/Deal/effectiveAmortizationMonths div 12)"/>
<xsl:text>
\lang4105\b0  years and \lang1033\b\ 
</xsl:text>
    <xsl:value-of select="/*/Deal/effectiveAmortizationMonths mod 12"/>
<xsl:text>
\lang4105 \b0 months (the \b\i Amortization Period\b0\i0 ). Since the amount 
of your regular principal and interest payment is not automatically adjusted 
with any change in the Variable Interest Rate, it is possible that the 
remaining Amortization Period will increase if the Variable Interest Rate 
increases.\par
\par
Both the Term and the Amortization Period begin on the Interest Adjustment Date.\par
\cell\cell\pard\intbl\qr\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 7. Cost of Borrowing\par
\cell\b0\cell\pard\intbl\qr\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 The cost to you of 
borrowing the Principal Amount (\i the \b Cost of Borrowing\b0\i0 ) is \par 
which is the amount of interest that will accrue from the Mortgage Advance 
Date until the Maturity Date, assuming that the Variable Interest Rate does 
not change from the Current Variable Interest Rate and that you make all 
payments when due and make no prepayments.\par
\cell \par
\par
\cell\pard\intbl\tldot\tx7056\lang1033\ 
</xsl:text>
    <xsl:value-of select="$interestOverterm"/>
    <xsl:text>\lang4105\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 8. Payments\par
\cell\b0\cell\pard\intbl\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056\b Interest Adjustment Payment\par
\par
\b0 On the Interest Adjustment Date you must pay us interest in the amount of 
\par which covers interest on the Principal Amount from the Mortgage Advance 
Date to but excluding the Interest Adjustment Date.\par
\par
\b Principal and Interest Payments\par
\par
\b0 Beginning with the First Payment Date, you must make \lang1033\b\ 
</xsl:text>
<xsl:value-of select="$paymentFrequence"/> 
<xsl:text>
\lang4105 , \b0 regular payments of principal and interest, each in the amount of \par
to and including the Maturity Date.  Each regular payment is due \lang1033\b\ 
</xsl:text>
<xsl:value-of select="$paymentDueOn"/> 
<xsl:text>
\lang4105\b0  (your \b regular payment date\b0 ).\par
\par
If the Variable Interest Rate increases because of increases in the Canadian 
Tire prime rate and the amount of your regular payment is not enough to pay 
outstanding accrued interest, your regular payments will be increased so that 
outstanding accrued interest is paid.\par
\par
On the Maturity Date (unless the Mortgage has been renewed) you must pay the 
outstanding balance of the Principal Amount and accrued interest in the total 
amount of \par
\pard\intbl\keep\keepn\tldot\tx7056 (This amount is based on the assumption 
that the Variable Interest Rate does not change from the Current Variable 
Interest Rate and that you make all payments when due and make no prepayments.)
\par
\par
\b Total Payments\par
\b0\par
The total amount of all payments that you must make to us from the Mortgage 
Advance Date until the end of the Term assuming that the Variable Interest 
Rate does not change from the Current Variable Interest Rate and that you make 
all payments when due and make no prepayments is \par
\par
\pard\intbl\tldot\tx7056\b Application of Payments\par
\b0\par
Each regular payment that you make is applied first to the accrued interest 
and the balance, if any, is then applied to the outstanding Principal 
Amount.\par
\cell\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\cell\pard\intbl\tldot\tx7056\par
\par
\lang1033\ 
</xsl:text>
    <xsl:value-of select="$iadPayment"/>
<xsl:text>
\lang4105\par
\par
\par
\par
\par
\par
\par
</xsl:text>
    <xsl:value-of select="$pandiPaymentAmount"/>
<xsl:text>
\par
\par
\par
\par
\par
\par
\par
\par
\par
\lang1033\ </xsl:text><xsl:value-of select="$balanceOnMaturity"/><xsl:text>\lang4105\par
\par
\par
\par
\par
\par
\par
\par
\par
\lang1033\ 
</xsl:text>
    <xsl:value-of select="$totalPayments"/>
    <xsl:text>\lang4105\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 9. Prepayment Privileges\par
\cell\b0\cell\pard\intbl\qr\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 You have the 
following prepayment privileges, provided you are not in default under the 
Mortgage:\par
\par
(a) In the 12 month period beginning on the Interest Adjustment Date and in 
each 12 month period thereafter (each, a \b mortgage year\b0 ) you may prepay 
in total up to 20% of the Principal Amount (the\b free prepayment amount\b0 ) 
without any charge. The free prepayment amount is not cumulative and therefore 
if not used in a mortgage year, the unused portion of the free prepayment 
amount cannot be carried forward and used in a later mortgage year. Any 
prepayment must be made on a regular payment date and requires 10 days written 
notice to us. \par
\par
(b) On 10 days\rquote written notice to us, you may increase the amount of 
your regular payments of principal and interest, provided that the total of 
all such increases during any mortgage year does not exceed 20% of the amount 
of the first regular payment that was required under the current term of this 
mortgage.\b \b0 After you have increased the amount of your regular payments, 
you may, on 10\b \b0 days\rquote written notice to us, reduce the amount of 
your regular payments to an amount that is not less than the amount of the 
first regular payment that was required under the mortgage. \par
\b\par
\b0 (c) On any regular payment date you may prepay all or any part of the 
Principal Amount, but to the extent that the amount that you prepay exceeds 
the unused free prepayment amount in that mortgage year (the \b excess 
prepayment amount\b0 ) you must pay us a prepayment charge in an amount equal 
to the greater of (i) 3 months\rquote interest at the Variable Interest Rate 
on the excess prepayment amount, or (ii) the prepayment interest rate 
differential amount.\par
\b\par
Prepayment interest rate differential amount \b0 means the amount by which (a) 
below exceeds (b) below:\par
\par
(a) the total amount of interest which would have been payable on the excess 
prepayment amount, calculated from the date of prepayment to the Maturity Date,
 at the Variable Interest Rate; and\par
\par
(b) the total amount of interest which would have been payable on the excess 
prepayment amount, calculated from the date of prepayment to the Maturity Date,
 at the fixed annual interest rate set by us, in our sole discretion, and 
which would be applicable to a closed first mortgage for a term commencing on 
the date of prepayment and expiring on the Maturity Date, or for a mortgage 
term nearest in length to the remainder of the Term on the prepayment date;\par
\par
(If (a) above is the same or less than (b) above, then the prepayment interest 
rate differential amount is zero.)\par
\cell\cell\pard\intbl\qr\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\intbl\tldot\tx7056\b 10. Other Charges\par
\cell\b0\cell\pard\intbl\tldot\tx7056\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx7989\cellx8346\cellx10188\pard\qj\intbl\tldot\tx7056 If the Mortgage is 
not repaid in full on the Maturity Date (unless it has been renewed) or you do 
not make any payment when due, in addition to compound interest, you may be 
charged legal fees and costs (including the fees and costs of our internal 
legal counsel), on a substantial indemnity basis, resulting from actions to 
collect a payment or to enforce the Mortgage and any other security, which you 
must then pay to us.\par
\par
In addition, you must pay us our fee (currently $30) for any pre-authorized 
debit, cheque or other payment instrument that is dishonoured when presented 
to your financial institution. \par
\par
After the Mortgage has been paid in full, you will be responsible for paying 
applicable government registration fees and, if applicable, our fee for 
preparing the discharge. Our current discharge fee is\'85\'85\cell\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\cell\pard\intbl\tldot\tx7056\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
\par
$200.00\cell\row

\pard\f0\par
\page\b\f0\fs28 Variable Rate Mortgage Equivalent Rate Table\par
\b0\fs16\par
\fs18 The interest rate payable on the mortgage loan amount under a variable 
rate mortgage is calculated monthly not in advance. The table below sets out 
what would be the equivalent interest rate if the interest were calculated 
half-yearly not in advance.\par
\par
\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx2466\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx5040\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx7614\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx10188\pard\intbl\tldot\tx7056\b\fs16 Interest rate per annum\par
calculated monthly not in\par
advance (%)\b0\fs18\cell\b\fs16 Equivalent Interest rate per\par
annum calculated half yearly\par
not advance (%)\b0\fs18\cell\b\fs16 Interest rate per annum\par
calculated monthly not in\par
advance (%)\b0\fs18\cell\b\fs16 Equivalent Interest rate per\par
annum calculated half yearly\par
not advance (%)\b0\fs18\cell\row

\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 \trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx2466\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx5040\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx7614\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs \cellx10188\pard\intbl\itap2\qc\fs16 4.48%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.49%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.50%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.51%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.52%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.53%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.54%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.55%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.56%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.57%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.58%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.59%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.60%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.61%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.62%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.63%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.64%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.65%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.66%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.67%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.68%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.69%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.70%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.71%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.72%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.73%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.74%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.75%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.76%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.77%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.78%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.79%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.80%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.81%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.82%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.83%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.84%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.85%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.86%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.87%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.88%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.89%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.90%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\qc\tldot\tx7056\b\f0\cell\pard\intbl\itap2\qc\b0\f0 4.52202%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.53221%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.54240%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.55259%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.56278%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.57297%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.58316%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.59335%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.60354%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.61373%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.62392%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.63412%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.64431%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.65450%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.66470%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.67489%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.68509%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.69528%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.70548%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.71567%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.72587%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.73606%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.74626%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.75646%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.76666%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.77686%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.78705%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.79725%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.80745%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.81765%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.82785%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.83806%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.84826%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.85846%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.86866%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.87886%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.88907%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.89927%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.90947%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.91968%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.92988%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.94009%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.95029%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\qc\tldot\tx7056\b\cell\pard\intbl\itap2\qc\b0 4.91%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.92%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.93%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.94%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.95%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.96%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.97%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.98%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.99%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.00%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.01%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.02%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.03%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.04%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.05%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.06%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.07%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.08%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.09%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.10%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.11%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.12%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.13%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.14%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.15%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.16%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.17%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.18%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.19%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.20%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.21%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.22%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.23%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.24%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.25%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.26%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.27%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.28%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.29%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.30%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.31%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.32%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.33%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\qc\tldot\tx7056\b\cell\pard\intbl\itap2\qc\b0 4.96050%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.97071%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.98091%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 4.99112%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.00133%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.01154%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.02175%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.03195%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.04216%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.05237%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.06258%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.07279%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.08301%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.09322%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.10343%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.11364%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.12385%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.13407%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.14428%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.15450%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.16471%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.17493%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.18514%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.19536%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.20557%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.21579%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.22601%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.23622%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.24644%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.25666%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.26688%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.27710%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.28732%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.29754%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.30776%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.31798%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.32820%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.33842%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.34864%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.35887%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.36909%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.37931%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.38954%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\tldot\tx7056\b\f0\cell\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx2466
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx5040
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx7614
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx10188\row

\pard\b0\fs18\par
\page


\par
\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx2466\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx5040\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx7614\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx10188\pard\intbl\tldot\tx7056\b\f0\fs16 Interest rate per annum\par
calculated monthly not in\par
advance (%)\b0\fs18\cell\b\fs16 Equivalent Interest rate per\par
annum calculated halfyearly\par
not advance (%)\b0\fs18\cell\b\fs16 Interest rate per annum\par
calculated monthly not in\par
advance (%)\b0\fs18\cell\b\fs16 Equivalent Interest rate per\par
annum calculated halfyearly\par
not advance (%)\b0\fs18\cell\row

\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 \trbrdrt\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx2466\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx5040\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx7614\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx10188\pard\intbl\itap2\qc\fs16 
5.34%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.35%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.36%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.37%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.38%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.39%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.40%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.41%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.42%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.43%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.44%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.45%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.46%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.47%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.48%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.49%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.50%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.51%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.52%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.53%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.54%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.55%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.56%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.57%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.58%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.59%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.60%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.61%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.62%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.63%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.64%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.65%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.66%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.67%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.68%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.69%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.70%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.71%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.72%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.73%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.74%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.75%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.76%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\qc\tldot\tx7056\b\cell\pard\intbl\itap2\qc\b0 5.39976%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.40999%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.42021%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.43044%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.44066%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.45089%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.46112%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.47134%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.48157%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.49180%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.50203%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.51226%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.52249%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.53272%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.54295%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.55318%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.56341%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.57364%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.58387%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.59410%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.60434%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.61457%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.62480%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.63504%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.64527%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.65551%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.66574%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.67598%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.68621%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.69645%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.70669%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.71692%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.72716%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.73740%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.74764%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.75788%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.76812%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.77836%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.78860%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.79884%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.80908%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.81932%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.82956%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\qc\tldot\tx7056\b\cell\pard\intbl\itap2\qc\b0 5.77%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.78%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.79%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.80%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.81%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.82%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.83%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.84%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.85%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.86%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.87%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.88%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.89%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.90%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.91%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.92%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.93%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.94%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.95%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.96%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.97%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.98%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.99%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.00%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.01%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.02%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.03%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.04%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.05%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.06%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.07%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.08%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.09%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.10%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.11%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.12%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.13%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.14%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.15%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.16%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.17%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.18%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.19%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\qc\tldot\tx7056\b\cell\pard\intbl\itap2\qc\b0 5.83981%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.85005%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.86029%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.87054%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.88078%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.89103%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.90127%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.91152%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.92176%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.93201%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.94226%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.95250%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.96275%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.97300%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.98325%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 5.99350%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.00374%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.01399%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.02424%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.03450%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.04475%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.05500%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.06525%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.07550%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.08575%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.09601%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.10626%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.11652%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.12677%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.13702%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.14728%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.15754%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.16779%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.17805%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.18831%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.19856%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.20882%\nestcell{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.21908%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.22934%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.23960%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.24986%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.26012%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\itap2\qc 6.27038%\nestcell
{\*\nesttableprops\trowd\trgaph108\trrh247\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx1010\nestrow}{\nonesttables\par}
\pard\intbl\qc\tldot\tx7056\b\cell\trowd\trgaph108\trleft-108\trbrdrl\brdrs\brdrw10 
\trbrdrt\brdrs\brdrw10 \trbrdrr\brdrs\brdrw10 \trbrdrb\brdrs\brdrw10 
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx2466
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx5040
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx7614
\clbrdrl\brdrw10\brdrs\clbrdrt\brdrw10\brdrs\clbrdrr\brdrw10\brdrs\clbrdrb\brdrw10\brdrs 
\cellx10188
\row

\pard\b0\f0\fs18\par
</xsl:text>
    <xsl:value-of select="$lastBrace"/>
  </xsl:template>

  <!-- CR224b Common header for Dislosure Statement -->
  <xsl:template name="DisclosureStatementHeader">
    <xsl:param name="rateType"/>
    <xsl:text>{\sect}\ql </xsl:text>
    <xsl:call-template name="Logo"/>
    <xsl:text>\fs16\par
\pard\qr\lang1033\b\fs24
</xsl:text>
    <xsl:value-of select="$rateType"/>
    <xsl:text>\par
\pard\qc\b0\par
\b Disclosure Statement\par
\pard\b0\par
\fs20\b Borrower Name:\b0\tab 
</xsl:text>
    <xsl:for-each select="/*/Deal/Borrower">
        <xsl:sort select="primaryBorrowerFlag" order="descending"/>
        <xsl:value-of select="salutation"/>
        <xsl:text> </xsl:text>
    	<xsl:value-of select="borrowerFirstName"/>
    	<xsl:text> </xsl:text>
    	<xsl:value-of select="borrowerLastName"/>
    	<xsl:choose>
    		<xsl:when test="position() = last()">
    		<!-- <xsl:text>;</xsl:text> -->
    		</xsl:when>
    		<xsl:otherwise>
    		<xsl:text>, </xsl:text>
    		</xsl:otherwise>
    	</xsl:choose>
    </xsl:for-each>
    <xsl:text>
\par
\b Mortgage Reference Number:\b0\tab\ 
</xsl:text>
    <xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
    <xsl:text>
\par
\b Property Address:\b0\tab
</xsl:text>
    <xsl:value-of select="/*/Deal/Property/propertyStreetNumber"/><xsl:text> </xsl:text>
    <xsl:value-of select="/*/Deal/Property/propertyStreetName"/><xsl:text> </xsl:text>
    <xsl:value-of select="/*/Deal/Property/streetType"/><xsl:text> </xsl:text>
    <xsl:value-of select="/*/Deal/Property/streetDirection"/><xsl:text> </xsl:text>
    <xsl:value-of select="/*/Deal/Property/propertyCity"/><xsl:text> </xsl:text>
    <xsl:value-of select="/*/Deal/Property/province"/><xsl:text> </xsl:text>
    <xsl:value-of select="/*/Deal/Property/propertyPostalFSA"/><xsl:text> </xsl:text>
    <xsl:value-of select="/*/Deal/Property/propertyPostalLDU"/>
    <xsl:text>
\par
\ul\b\tab\tab\tab\tab\tab\tab\tab\tab\tab\tab\tab\tab\par
\ulnone\b0\fs18\par
\b\qj\ We\b0 ,\b our \b0 and \b us\b0 refer to Canadian Tire Bank. \b You \b0 
and\b your \b0 refer to the Borrower named above.\par \b Mortgage\b0 means the 
mortgage/charge of the Property by you to Canadian Tire Bank.\par
\par
</xsl:text>
  </xsl:template>

  <!-- CR224B Request for statement and Authorization -->
  <xsl:template name="RequestForStatementAndAuthorization">
    <xsl:text>
{\sect }
\viewkind4\uc1\pard\keepn\s1\fi720\li3600\lang4105\b\f0\fs24 REQUEST FOR STATEMENT &amp; AUTHORIZATION\par
\pard\fs20\par
\par
\par
Purpose:\tab Final Discharge   \par
\par
\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\clbrdrb\brdrw15\brdrs \cellx5850\pard\intbl Date:\cell\cell\b0 
</xsl:text>
    <xsl:value-of  select="/*/CommitmentLetter/CurrentDate"/>  <xsl:text>
\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\cellx5850\pard\intbl\b\cell\cell\b0\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\clbrdrb\brdrw15\brdrs \cellx5850\pard\intbl\b To:\cell\cell\b0 
</xsl:text>
    <xsl:value-of select="/*/Deal/refiCurMortgageHolder"/>
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\clbrdrt\brdrw15\brdrs 
\cellx5850\pard\intbl\b\cell\cell\pard\intbl\qc\b0 (borrower\rquote s current 
lender name)\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\cellx2249\clbrdrb\brdrw15\brdrs \cellx5850\cellx9990\pard\intbl\b\cell\cell Tel.:\b0  \b\cell 
</xsl:text>
    <xsl:value-of select="/*/CommitmentLetter/LenderPhoneNumber"/>
    <xsl:text>\cell\b0\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\cellx2249\clbrdrb\brdrw15\brdrs \cellx5850\cellx9990\pard\intbl\b\cell\cell Fax:\b0  \b\cell 
</xsl:text>
    <xsl:value-of select="/*/CommitmentLetter/LenderFaxNumber"/>
    <xsl:text>\cell\b0\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\cellx5850\cellx9990\pard\intbl\b\cell\cell\cell\b0\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\cellx4009\clbrdrb\brdrw15\brdrs \cellx9990
\pard\intbl\b Re:\cell\cell Borrower\rquote s Name(s)\cell\b0 
<!-- </xsl:text><xsl:value-of select="/*/CommitmentLetter/BorrowerNamesLine"/><xsl:text> -->
</xsl:text>
<xsl:for-each select="/*/Deal/Borrower">
    <xsl:sort select="primaryBorrowerFlag" order="descending"/>
    <xsl:value-of select="salutation"/>
    <xsl:text> </xsl:text>
  	<xsl:value-of select="borrowerFirstName"/>
  	<xsl:text> </xsl:text>
  	<xsl:value-of select="borrowerLastName"/>
  	<xsl:choose>
  		<xsl:when test="position() = last()">
  		<xsl:text></xsl:text>
  		</xsl:when>
  		<xsl:otherwise>
  		<xsl:text>, </xsl:text>
  		</xsl:otherwise>
  	</xsl:choose>
    </xsl:for-each>
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\cellx4009\clbrdrt\brdrw15\brdrs\clbrdrb\brdrw15\brdrs 
\cellx9990\pard\intbl\b\cell\cell\cell\b0\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\cellx4009\clbrdrt\brdrw15\brdrs\clbrdrb\brdrw15\brdrs \cellx9990
\pard\intbl\b\cell\cell Property Address:\b0  \b\cell\b0 
</xsl:text>
    <xsl:for-each select="/*/Deal/Property[primaryPropertyFlag='Y']">
    	<xsl:value-of select="propertyStreetNumber"/>
    		<xsl:text> </xsl:text>
    	<xsl:value-of select="propertyStreetName"/>
    	    <xsl:text> </xsl:text>
    	<xsl:value-of select="streetType"/>
    	    <xsl:text> </xsl:text>
    	<xsl:value-of select="streetDirection"/>
    </xsl:for-each>
    <xsl:text>\cell\row

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770
\cellx1430
\cellx4009
\clbrdrt\brdrw15\brdrs\clbrdrb\brdrw15\brdrs 
\cellx9990\pard\intbl\b\cell\cell\cell\b0
</xsl:text>
    <xsl:for-each select="/*/Deal/Property[primaryPropertyFlag='Y']">
    	<xsl:value-of select="propertyCity"/>
    	    <xsl:text>, </xsl:text>
    	<xsl:value-of select="province"/>
    	    <xsl:text>, </xsl:text>
    	<xsl:value-of select="propertyPostalFSA"/>
    	    <xsl:text> </xsl:text>
    	<xsl:value-of select="propertyPostalLDU"/>
    </xsl:for-each>
    <xsl:text>\cell\row 

\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx770\cellx1430\cellx4009\clbrdrt\brdrw15\brdrs\clbrdrb\brdrw15\brdrs 
\cellx9990\pard\intbl\b\cell\cell Existing Mortgage No.\b0  \b\cell     
\b0\cell\row

\pard\fi720\li3600 (existing mortgage number is mandatory)\lang1033\par
\pard\brdrb\brdrs\brdrw30 \hyphpar0\fi-1440\li1440\sl360\slmult1\tx0\tx720\b\par
\pard\hyphpar0\tx0\lang4105\b0 Sir/Madam,\par
\par
\pard\hyphpar0\qj\tx0 I/We hereby authorize you to provide a statement, for 
the above-noted mortgage/account to \b First Canadian Title \b0 on behalf of 
Canadian Tire Bank\b \b0 by fax at:\par
\pard\hyphpar0\fi-180\li900\b 
</xsl:text>
    <xsl:choose>
	    <xsl:when test="/*/Deal/Borrower[primaryBorrowerFlag='Y']/BorrowerAddress/Addr/provinceId = 9">
	        <xsl:value-of select="$ontAddress"/>
	    </xsl:when>
	    <xsl:when test="/*/Deal/Borrower[primaryBorrowerFlag='Y']/BorrowerAddress/Addr/provinceId = 1">
	        <xsl:value-of select="$abAddress"/>
	    </xsl:when>
	    <xsl:when test="/*/Deal/Borrower[primaryBorrowerFlag='Y']/BorrowerAddress/Addr/provinceId = 2">
	        <xsl:value-of select="$bcAddress"/>
	    </xsl:when>
    </xsl:choose>
    <xsl:text>\par

\trowd\trgaph108\trleft-120\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\cellx4680\clbrdrb\brdrw15\brdrs \cellx10320\pard\intbl\qj\tx1350\tx1800\b0 
The effective date of this Statement should be:\b\cell 
</xsl:text>
    <xsl:value-of select="/*/Deal/estimatedClosingDate"/>
    <xsl:text>\cell\row

\pard\hyphpar0\qj\tx0\b0\tab\tab\tab\tab\tab\tab\tab (new mortgage closing date)\par
\b\par
\b0 The above mentioned statement should reflect the outstanding principal 
balance; accrued interest as of the above date; any debit or credit tax account 
balance; the per diem rate of interest on such principal balance accruing from 
the above date; whether the loan is in good standing; and if the mortgage 
contains a readvanceable provision and/or if additional principal advances 
could be made after the date of the statement. If there are multiple products 
associated with the mortgage security, provide a statement for each product.  
For discharge or assignment/transfers, prepare the statement on the basis that 
any allowable prepayment privilege has been applied prior to the calculation of 
the penalty amount.   \ul\b Please note:  If this request is for payout 
purposes and the mortgage secures a Line of Credit product, we will require 
that the Line of Credit be closed upon receipt of payment.\par\ulnone\b0\par

For assignment/transfer statements, please provide the CMHC or Genworth 
reference number associated with this mortgage, if applicable.\par\par\par\par


\trowd\trgaph108\trleft-108\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrb\brdrw15\brdrs \cellx4712\cellx5270\clbrdrb\brdrw15\brdrs 
\cellx10332
\pard\intbl\hyphpar0\qj\tx0\cell\cell\cell\row

\trowd\trgaph108\trleft-108\trrh305\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrt\brdrw15\brdrs \cellx4712\cellx5270\clbrdrt\brdrw15\brdrs 
\cellx10332\pard\intbl\hyphpar0\qj\tx0 Borrower\rquote s Signature\cell\cell 
Borrower\rquote s Signature\cell\row

\pard\par
\fs22\par

\ltrpar \sectd \ltrsect\linex0\headery706\footery706\colsx708\endnhere\sectlinegrid360
\sectdefaultcl\sftnbj {\footerr \ltrpar \pard\plain \ltrpar\ql 
\li0\ri0\widctlpar\wrapdefault\faauto\rin0\lin0\itap0\pararsid13979044 
\rtlch\fcs1 \af0\afs24\alang1025 \ltrch\fcs0 
\f0\fs22\lang4105\langfe1033\cgrid\langnp4105\langfenp1033 {\rtlch\fcs1 
\af217\afs12 \ltrch\fcs0 \f0\fs12\cf17\insrsid8280129\charrsid13979044 NOTICE 
OF CONFIDENTIALITY
\par The information contained on this form is intended for the exclusive use 
of the addressee named above and may contain confidential information. It is 
strictly prohibited for any person(s) other than the addressee to disclose, 
distribute or reproduce this information. If you are not the intended recipient 
or have received this form in error, please notify Canadian Tire Bank 
immediately by calling us at </xsl:text>
      <xsl:value-of select="$tollFreePhone" /><!-- #DG798 -->
      <xsl:text> and permanently destroy the misdirected copy. Thank you. \par }
\pard\plain \ltrpar\s18\qr \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640
\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \rtlch\fcs1 
\af0\afs24\alang1025 \ltrch\fcs0 \f0\fs22\lang4105\langfe1033\cgrid\langnp4105\langfenp1033 
{\rtlch\fcs1 \ab\af0\afs14 \ltrch\fcs0 \fs14\insrsid8280129 
\par }{\rtlch\fcs1 \ab\af0\afs14 \ltrch\fcs0 \f0\fs14\insrsid8280129\charrsid16450588 RB06}
{\rtlch\fcs1 \ab\af0\afs14 \ltrch\fcs0 
\f0\fs14\insrsid14235845\charrsid16450588 /MOPS3}{\rtlch\fcs1 \ab\af0\afs14 \ltrch\fcs0 
\f0\fs14\insrsid8280129\charrsid16450588 /ONFCTPAYOUTREQ1
\par }}

</xsl:text>
  </xsl:template>
	
  <!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="FrenchPage1">
		<xsl:text><!--{ \par }\pard \ql \ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\itap0 {\par }} -->
{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
\pard\plain \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
{\b\f0\fs27 </xsl:text>
<xsl:value-of select="$docTitle"/>		
		<xsl:text>\par }
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clvmgf\clvertalc\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright </xsl:text>
		<!--#DG244 -->
		<xsl:call-template name="Logo"/>
		<xsl:text>\cell {\f0\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line1"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/BranchAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Province"/>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Postal"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf\clvertalc
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \cell 
Tel: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Phone"/>
		<xsl:text>\par 
Fax: </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BranchAddress/Fax"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \cell }
{\b\f0\fs18 Underwriter \par\b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Underwriter"/>
		<xsl:text> \cell }
{\b\f0\fs18 Date \par\b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CurrentDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3288 \cellx8114
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth2794 \cellx10908
\row 
}
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>

</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line1"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Line2"/>
		<xsl:if test="/*/CommitmentLetter/ClientAddress/Line2">
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/City"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Province"/>
		<xsl:text>\par 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/ClientAddress/Postal"/>
		<xsl:text>\cell }
{\b\f0\fs18 Mortgage Reference Number:\cell \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/BrokerSourceNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh518\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
<!--
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18\b \cell Deal Number:\cell\b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/DealNum"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh517\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 \clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2914 \cellx7740
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3168 \cellx10908
\row 
}-->
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \par }
{\i\f0 We are pleased to confirm that your application for a Mortgage on the 
property described below has been approved subject to the terms and conditions 
that follow and the terms of the Disclosure Statement that is provided to you 
with this Commitment. \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826
\clvmgf\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 MORTGAGOR(S):\cell\b0 </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/BorrowerNames/Name">
			<xsl:value-of select="."/>
			<xsl:if test="not(position()=last())">
				<xsl:text>\par </xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\cell }
{\b\f0\fs18 PURPOSE:\b0  </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/DealPurpose"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmgf
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text>(S):}{\f0\fs18 \cell </xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/GuarantorNames/Name">
			<xsl:value-of select="."/>
			<xsl:text>\par </xsl:text>
		</xsl:for-each>
		<xsl:text>\cell \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh210\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1728 \cellx1620
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3206 \cellx4826\clvmrg
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18\ul SECURITY ADDRESS: \cell LEGAL DESCRIPTION: \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		<!--#DG408 xsl:for-each select="/*/CommitmentLetter/Properties/Property"-->
		<xsl:for-each select="/*/Deal/Property">
			<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tx4900\aspalpha\aspnum\faauto\adjustright
{\f0\fs18 
</xsl:text>
			<!--#DG408 xsl:value-of select="./AddressLine1"/>
			<xsl:text> \par 
</xsl:text>
			<xsl:value-of select="./AddressLine2"/>
			<xsl:if test="./AddressLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./City"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="./Province"/>
			<xsl:text>\par 
</xsl:text>
			<xsl:value-of select="./PostalCode"/-->
			<xsl:call-template name="propAdr"/>
			<xsl:text> \par 
\cell </xsl:text>
			<!--#DG408 xsl:value-of select="./LegalLine1"/>
			<xsl:if test="./LegalLine1">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine2"/>
			<xsl:if test="./LegalLine2">
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:value-of select="./LegalLine3"/>
			<xsl:if test="./LegalLine3">
				<xsl:text>\par </xsl:text>
			</xsl:if-->
			<xsl:if test="legalLine1 != ''">
				<xsl:value-of select="legalLine1"/>
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:if test="legalLine2 != ''">
				<xsl:value-of select="legalLine2"/>
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:if test="legalLine3 != ''">
				<xsl:value-of select="legalLine3"/>
				<xsl:text>\par </xsl:text>
			</xsl:if>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx5520
\clbrdrt\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 COMMITMENT DATE:}
{\f0\fs18 
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CommitmentIssueDate"/>
		<xsl:text>
\par 
\par }

{\b\f0\fs18 INTEREST ADJUSTMENT DATE:}
{\f0\fs18 
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/IADDate"/>
		<xsl:text>\cell }
\pard \qc \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 COMMITMENT EXPIRY DATE:}
{\f0\fs18 
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/CommitmentExpiryDate"/>
		<xsl:text>
\par 
\par }
{\b\f0\fs18 FIRST PAYMENT DATE:}
{\f0\fs18 \par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/FirstPaymentDate"/>
		<xsl:text>\cell }
{\b\f0\fs18 ADVANCE DATE:}
{\f0\fs18 
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text>
\par 
\par }
{\b\f0\fs18 MATURITY DATE:}
{\f0\fs18 \par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/MaturityDate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3671 \cellx3563
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth3672 \cellx7235
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3673 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 LOAN TYPE:  \b0 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Product"/>
		<xsl:text>\cell }
{\b\f0\fs18 MORTGAGE PRIORITY:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/Deal/lienPosition"/>
		<xsl:text>\cell }		
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 BASIC LOAN AMOUNT:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LoanAmount"/>
		<xsl:text>\cell }
{\b\f0\fs18 INTEREST RATE:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/InterestRate"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 MTG.INSUR.PREMIUM:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Premium"/>
		<xsl:text>\cell }
{\b\f0\fs18 TERM:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PaymentTerm"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 TOTAL LOAN AMOUNT:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TotalAmount"/>
		<xsl:text>\cell }
{\b\f0\fs18 AMORTIZATION PERIOD:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Amortization"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 BASIC PAYMENT AMOUNT:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TotalPayment"/>
		<xsl:text>\cell }
{\b\f0\fs18 PAYMENT FREQUENCY:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PaymentFrequency"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\faauto 
{\b\f0\fs18 EST.ANNUAL PROP.TAXES:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AnnualTaxes"/>
		<xsl:text>\cell }
{\b\f0\fs18 TAXES TO BE PAID BY:}{\f0\fs18 \cell </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/TaxPayor"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2959 \cellx2851
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth1975 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth2649 \cellx7475
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3433 \cellx10908
\row 
}
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 PST PAYABLE ON INSUR.PREMIUM:\cell }{\f0\fs18 </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/PST"/>
		<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 MORTGAGE FEES:\cell }{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh101\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4934 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		<xsl:for-each select="/*/CommitmentLetter/Fees/Fee">
			<xsl:text>\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\pard \ql \widctlpar\intbl\tx180\tqr\tx7500\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 \cell </xsl:text>
			<xsl:value-of select="./FeeVerb"/>
			<xsl:text>\cell }{\f0\fs18 </xsl:text>
			<xsl:value-of select="./FeeAmount"/>
			<xsl:text>\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh100\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth288 \cellx180
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth4646 \cellx4826
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth6082 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \qj \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 In this Commitment, which includes any schedule(s) and the Disclosure Statement, }{\b\f0\fs18 you and your}
{\f0\fs18  mean each Borrower named above and }{\b\f0\fs18 we, our, lender and us }{\f0\fs18  mean </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>. Each Borrower is liable, both jointly and severally, for payment and performance of all obligations under this Commitment and the Mortgage. 
\par \par 

All of the conditions on the attached schedule(s) and, if applicable, those of 
the mortgage insurer must be satisfied, unless expressly waived by us or the 
mortgage insurer. All costs, including legal fees and disbursements, survey, 
mortgage insurance etc. are for your account. The mortgage insurance premium 
(if applicable) will be added added to the principal amount of the Mortgage. 
Any fees and charges specified herein may be deducted from the loan advance. 
If for any reason the loan is not advanced, you agree to pay all application, 
legal fees and disbursements, and appraisal and survey costs incurred in this 
transaction.
\par \par 
To accept these terms, this Mortgage Commitment must be signed by the Borrower 
and the Guarantor, if applicable, and returned to us by no later than 
</xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
    <xsl:text> after which time if not accepted, we may choose to cancel this 
Commitment. Furthermore, the loan must be advanced by no later than </xsl:text>
    <xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text> at which time; we will have no further obligation to proceed with the advance.
\par \par
In the case of any inconsistency between any term in the Mortgage granted by 
you to us and registered against the Property and this Commitment, the terms 
contained in this Commitment will govern and will override the terms in the 
Mortgage.
\par \par 
Thank you for choosing </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> for your financing.
\par }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \trowd \trgaph108\trleft-108\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 
\par \page 
\par } </xsl:text>
	</xsl:template>

	<!--
<xsl:template name="FrenchPage2Start">
<xsl:text>{\sect } \linex0\headery706\footery260\endnhere\sectdefaultcl </xsl:text>
</xsl:template>
-->
	<xsl:template name="FrenchPage2">
		<xsl:text>\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18\ul TERMS AND CONDITIONS:
\par 
\par Mortgage:
\par }
{\f0\fs18 
This Mortgage will be subject to all extended terms set forth in our standard 
form of mortgage contract or in the mortgage contract prepared by our 
solicitors, whichever the case may be, and insured mortgage loans will be 
subject to the provisions of the National Housing Act (N.H.A.) and the 
regulations thereunder.\par }
{\b\f0\fs18\ul \par Rate Guarantee and Rate Adjustment Policies:\par }
{\f0\fs18 Provided that the Mortgage Loan is advanced by </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text>, the interest rate will be </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/InterestRate"/>
    <xsl:text> per annum, calculated half yearly, not in advance. In the event 
that the effective rate of interest is lower at closing, the interest rate may 
be adjusted.
\par 
\par }{\b\f0\fs18\ul Title Requirements / Title Insurance:
\par }{\f0\fs18 Title to the Property must be acceptable to our Solicitor who 
will ensure that the Mortgagor(s) have good and marketable title in fee simple 
to the property and that it is clear of any encumbrances which might affect 
the priority of the </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> Mortgage. Alternatively, we will accept title insurance from a 
    title insurance provider acceptable to </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>. The Mortgage must be a first charge on the property.
\par 
\par }{\b\f0\fs18\ul Survey Requirements / Title Insurance:
\par }{\f0\fs18 A survey or surveyors certificate completed by a recognized 
land surveyor and dated within the last ten (10) years is to be furnished to 
our Solicitor pri or to any advance. The survey must confirm that the location 
of the building(s) on the property complies with all municipal, provincial and 
other government requirements. A survey older than ten (10) years, but not 
older than twenty (20) years, may be accepted provided that it is accompanied 
by a Declaration of Possession from the then present owners of the property. 
Alternatively, we will accept title insurance from a title insurance provider 
acceptable to </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>.
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<!--
<xsl:template name="FrenchPage3Start">
<xsl:text>{\sect }\sectd \linex0\headery706\footery260\endnhere\sectdefaultcl 
{\footer \trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\pard\plain \s21\qc \widctlpar\intbl\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright
{\f0\fs20 Page {\field{\*\fldinst {\cs22  PAGE }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}{\f0\fs20  of }{\field{\*\fldinst {\cs22  NUMPAGES }}{\fldrslt {\cs22\lang1024\langfe1024\noproof 3}}}}{\cell }
\pard\plain \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-90\trkeep
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrnone 
\clbrdrl\brdrnone 
\clbrdrb\brdrnone 
\clbrdrr\brdrnone 
\cltxlrtb\clftsWidth3\clwWidth11430 \cellx11340
\row 
}
\pard\plain \s21\ql \widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\itap0 
{\par }
}</xsl:text>
</xsl:template>
-->
	<xsl:template name="FrenchPage3">
		<xsl:text>\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18\ul Privilege Payment Policies: }{\f0\fs18 \par This mortgage is 
CLOSED. The Mortgagor (borrower), when not in default of any of the terms, 
covenants, conditions or provisions contained in the mortgage, shall have the 
following privileges for payment of extra principal amounts: 1) During each 12 
month period following the interest adjustment date the borrower may, without 
penalty: i) make prepayments totaling up to twenty percent (20%) of the 
original principal amount of the mortgage; ii) increase regular monthly 
payment by a total up to twenty percent (20%) of the original monthly payment 
amount so as to reduce the amortization of the mortgage without changing term. 
2) If the mortgaged property is sold pursuant to a bona fide arms length 
agreement, the borrower may repay the mortgage in full upon payment of three 
(3) months interest penalty calculated at the current interest rate of the 
mortgage. 3) In addition to the prepayment privileges described in paragraph 1,
 at any time after the third anniversary of the interest adjustment date, the 
borrower may prepay the mortgage in whole or in part upon payment of three (3) 
months interest penalty calculated at the current interest rate of the 
mortgage.\par \par }
{\b\f0\fs18\ul Interest Adjustment Payment }
{\f0\fs18 \par Prior to commencement of the regular monthly loan payments 
interest will accrue at the rate payable on the Mortgage loan, on all funds 
advanced to the Mortgagor(s). Interest will be computed from the advance date 
and will become due and payable on the first day of the month next following 
the advance date.
\par 
\par }{\b\f0\fs18\ul Prepayment Policies:
\par }{\f0\fs18 You can prepay this mortgage on payment of 3 months simple 
interest on the principal amount owing at the prepayment date, or the interest 
rate differential,whichever is greater.TBD
\par 
\par }{\b\f0\fs18\ul Renewal:
\par }{\f0\fs18 At the sole discretion of BasisXPressLender, the Mortgage may 
be renewed at maturity for any term, with or without a change in the interest 
rate payable under the Mortgage, by entering into one or more written 
agreements with the Mortgagor(s).
\par 
\par }{\b\f0\fs18\ul Early Renewal: 
\par }{\f0\fs18 Provided that the Mortgage is not in 
default and that the building on the lands described in the Mortgage is used 
only as residence and comprises no more than four (4) dwelling units, you may 
renegotiate the term and payment provisions of the Mortgage prior to the end 
of the original term of
 the Mortgage, provided that: 
\par }\pard \ql \widctlpar\intbl\tx180\tx1000\faauto {\f0\fs18 \tab a) \tab 
the Mortgage may only be renegotiated for a closed term
\par \tab b) \tab the Mortgagor(s) select a renewal option that results in the 
new Maturity Date being equal to or greater than
\par \tab \tab the original Maturity Date of the current term; and 
\par \tab c) \tab payment of the interest rate differential and an early 
renewal processing fee is made upon execution of such
\par \tab \tab renewal or amending agreement. 
\par }\pard \ql \widctlpar\intbl\faauto {\f0\fs18 
\par 
\par }{\b\f0\fs18\ul NSF Fee: 
\par }{\f0\fs18 There may be a fee charged for any payment 
returned due to insufficient funds or stopped payments.
\par 
\par }{\b\f0\fs18\ul Assumption Policies:
\par }{\f0\fs18 In the event that you subsequently enter into an agreement to 
sell, convey or transfer the property, any subsequent purchaser(s) who wish to 
assume this Mortgage, must be approved by </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text>, otherwise this Mortgage Loan will become due and payable 
upon your vacating or selling the property.
\par 
\par }{\b\f0\fs18\ul Portability Policies:
\par }{\f0\fs18 Should you decide to sell the mortgaged property and purchase 
another property, you may transfer the remaining mortgage balance together 
with the interest rate set out therein to the new property provided that the 
Mortgage Loan is not in default and a new mortgage loan application has been 
accepted by </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> and all terms and conditions set out in the subsequent Mortgage Loan approval have been met.\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108
\trbrdrt\brdrs\brdrw10 
\trbrdrl\brdrs\brdrw10 
\trbrdrb\brdrs\brdrw10 
\trbrdrr\brdrs\brdrw10 
\trbrdrh\brdrs\brdrw10 
\trbrdrv\brdrs\brdrw10 
\trftsWidth1\trautofit1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	
  <xsl:template name="FrenchPage4">
<xsl:text>\trowd 
\trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908\pard
\ql \widctlpar\intbl\faauto {\b\f0\fs18\ul THIS COMMITMENT IS CONDITIONAL UPON RECEIPT OF THE FOLLOWING
\par 
\par }{\f0\fs18 The following conditions must be met, unless waived by us in 
writing at our sole discretion and the requested documents must be received in 
form and content satisfactory to us no later than ten (10) days prior to the 
scheduled Account Activation/Advance Date. Failure to do so may result in us 
cancelling this commitment.
\par \par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
</xsl:text>
    <xsl:if test="/*/CommitmentLetter/Conditions">
<!-- commented as it is printing 2 empty lines for the first row number - Venkata -->
<!-- Venkata commented - Jun 22, 2007 xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3
\clbrdrl\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908 </xsl:text -->
		</xsl:if>
		<xsl:for-each select="/*/CommitmentLetter/Conditions/Condition">
			<xsl:text>\pard \ql \widctlpar\intbl\faauto
{\f0\fs18 </xsl:text>
			<xsl:value-of select="position()"/>
			<xsl:text>.\cell }

{\f0\fs18 </xsl:text>
			<xsl:for-each select="./Line">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:text>\par </xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth468 \cellx360
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth10548 \cellx10908
\row 
}
</xsl:text>
		</xsl:for-each>
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard \ql \widctlpar\intbl\faauto 
{\f0\fs18 \par \par }
{\b\f0\fs18\ul Requests for any changes to the terms and conditions of this 
Commitment will only be considered if your request is received by us in 
writing at least 5 business days prior to the scheduled Account 
Activation/Advance Date.
\par 
\par ACCEPTANCE:
\par 
\par }
{\f0\fs18 This Commitment shall be open for acceptance by you until 11:59 pm on </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/priorAdvanceDate"/>
    <xsl:text> after which time, if not accepted; we may choose to cancel this 
Commitment. Furthermore, the loan must be advanced by no later than </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/AdvanceDate"/>
		<xsl:text> at which time; we will have no further obligation to proceed with the advance.
\par 
\par }\pard \ql \widctlpar\intbl\tx4000\tx4850\tx5500\aspalpha\aspnum\faauto\adjustright {\f0\fs18 \tab \tab </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/LenderName"/>
		<xsl:text> 
\par 
\par \tab Authorized by:___________________________________________
\par \tab \tab \tab </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/Underwriter"/>
		<xsl:text>
\par 
\par 
\par }
\pard \ql \widctlpar\intbl\faauto 
{\f0\fs18 \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\pard \ql \widctlpar\faauto\itap0 
{\f0\fs18 \par \page \par }</xsl:text>
	</xsl:template>
	<xsl:template name="FrenchPage5">
		<xsl:text>\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrt\brdrs\brdrw10 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\pard\plain \s15\ql \widctlpar\intbl\faauto 
{\b\f0\fs18 ACCOUNT AGREEMENT
\par
\par }{\f0\fs18 We, Canadian Tire Bank, offer to open a Canadian Tire 
One-and-Only account for the Borrower(s). If you accept this Commitment, there 
will be an agreement between you and us. The terms of the agreement are set 
out in this Commitment, the Canadian Tire One-and-Only Account Terms and 
Conditions and the Disclosure Statement. By accepting this Commitment, you 
confirm that you have read, understood and agreed to the terms of this 
Commitment, the Canadian Tire One-and-Only Account Terms and Conditions and 
the Disclosure Statement. If you do your banking with us on the Internet, our 
agreement with you also includes the Terms and Conditions governing our 
websites.
\par
\par } 
{\b\f0\fs18 PERSONAL INFORMATION 
\par 
\par }{\f0\fs18 The term "Personal Information" means any information about an 
identifiable individual. Personal Information about you, the Borrower(s), or 
the Guarantor(s), as applicable, is collected, used, and disclosed in 
accordance with the Canadian Tire Financial Services Privacy Communication 
(the Communication), including, in particular, to process and maintain the 
One-and-Only account of the Borrower(s). As described in the Communication, 
Personal Information may also be shared with other parties who administer the 
One-and-Only account and to market and sell other Canadian Tire branded 
products and services. The Communication is updated from time to time. You are 
referred to the most current version of the Communication, which is enclosed 
and which is also available online at myCTFS.com.
\par 
\par }{\b\f0\fs18 I / we, the undersigned accept the terms of this Commitment 
as stated above and agree to fulfill the conditions of approval herein and in 
the Canadian Tire One-and-Only Account Terms and Conditions.
\par \par 
By signing below, I / we (choose on of the following):
\par \par
_____ acknowledge receiving the Disclosure Statement at least 2 business 
days before I / we signed this Commitment
\par OR
\par \par
_____ acknowledge receiving the Disclosure Statement with this Commitment, 
and waive the requirement that it be provided to me / us 2 business days 
before I / We sign this Commitment. }

{\f0\fs18 \par \par 
Each Borrower has the right to receive an individual copy of the Disclosure 
Statement and all subsequent prescribed disclosure relating to the 
One-and-Only account (the Required Disclosure). By signing below, each 
Borrower consents to Canadian Tire Bank providing the Required Disclosure to 
the Borrower to whom this Commitment is addressed at the address for the 
Property subject to the Collateral Mortgage. Any Borrower named below may 
revoke this consent at any time by contacting a Client Service Representative 
at the phone number listed above so that each of you will receive individually 
addressed copies of the Required Disclosure.
}

{\b\f0\fs18 SIGNATURES
\par 
\par }{\f0\fs18 Your signature(s) will be stored along with other information 
about you to form your signature card.}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7800\aspalpha\aspnum\faauto\adjustright 
{\f0\fs18 \par 
\cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth11016 \cellx10908
\row 
}
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell ___________________________
\par <!--#DG350 APPLICANT-->BORROWER (sign above)
\par 
\par \cell ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\par \cell ___________________________
\par BORROWER (print name above)
\par 
\par \cell 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f0\fs18 ___________________________
\par BORROWER (sign above)
\par \par \cell }
{\b\f0\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\par \cell ___________________________
\par BORROWER (print name above)
\par 
\par \cell 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18 ___________________________
\par WITNESS
\par 
\par \cell }
{\b\f0\fs18 ___________________________
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text> (sign above)
\par \par \cell }
{\b\f0\fs18 ___________________________
\par DATE
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh205\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\intbl\tx100\tx4000\tx7000\aspalpha\aspnum\faauto\adjustright 
{\b\f0\fs18
\par 
\par 
\par \cell ___________________________
\par </xsl:text>
		<xsl:value-of select="/*/CommitmentLetter/GuarantorClause"/>
		<xsl:text> (print name above)
\par 
\par \cell 
\par 
\par 
\par \cell }
\pard \ql \widctlpar\intbl\aspalpha\aspnum\faauto\adjustright 
{
\trowd \trgaph108\trrh206\trleft-108\trftsWidth1\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3 
\clbrdrl\brdrs\brdrw10 
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3348 \cellx3240
\clbrdrb\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth3420 \cellx6660
\clbrdrb\brdrs\brdrw10 
\clbrdrr\brdrs\brdrw10 
\cltxlrtb\clftsWidth3\clwWidth4248 \cellx10908
\row 
}
\pard \ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 </xsl:text>
	</xsl:template>
	<xsl:template name="FrenchFooter">
		<xsl:text>{\footer \pard\plain \ql \widctlpar
\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\itap0 
{\f0\fs24{Page }{\field{\*\fldinst {PAGE  }}{\fldrslt {\lang1024\langfe1024\noproof 1}}}{ }{\f0\fs16 of }
{\field{\*\fldinst {NUMPAGES }}}}}</xsl:text>
	</xsl:template>
	
  <!--#DG408 ================================================  -->
	<xsl:template name="propAdr">
		<xsl:param name="separaPostal" select="'\line '"/>
		<xsl:if test="unitNumber != ''">
			<xsl:value-of select="unitNumber"/>
			<xsl:text> - </xsl:text>
		</xsl:if>
		<xsl:value-of select="propertyStreetNumber"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="propertyStreetName"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="streetType"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="streetDirection"/>
		<xsl:text>\par </xsl:text>
		<xsl:if test="propertyAddressLine2 != ''">
			<xsl:value-of select="propertyAddressLine2"/>
			<xsl:text>\par </xsl:text>
		</xsl:if>
		<xsl:value-of select="propertyCity"/>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="province"/>
		<xsl:value-of select="$separaPostal"/>
		<xsl:value-of select="propertyPostalFSA"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="propertyPostalLDU"/>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="RTFFileEnd">
		<xsl:text>{\par }}</xsl:text>
	</xsl:template>
	
  <xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl <!-- #DG798 {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}-->
{\f0\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
}
{\stylesheet{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}
{\*\cs10 \additive Default Paragraph Font;}{\s15\ql \widctlpar\faauto\itap0 
\b\f0\fs18\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext15 
Body Text;}}
{\info{\title </xsl:text>
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageFrench">
				<xsl:text/>
			</xsl:when>
			<xsl:otherwise>
        <xsl:value-of select="$docTitle"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> }{\author Zivko Radulovic}{\operator John Schisler}
{\creatim\yr2002\mo8\dy27\hr11\min17}{\revtim\yr2002\mo8\dy27\hr11\min17}
{\version2}{\edmins3}{\nofpages5}{\nofwords0}{\nofchars0}{\*\company filogix Inc}
{\nofcharsws0}{\vern8283}}
\margl720\margr720\margt720\margb720 \widowctrl
\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul
\hyphcaps0\formshade\horzdoc
\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand
\viewkind1\viewscale90\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp
\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0
\sectd \linex0\headery0\footery400\endnhere\sectlinegrid360
\sectdefaultcl </xsl:text>
	</xsl:template>
	
	<!-- CR224b -->
	<xsl:template name="ExtraCommitmentLetterStart">
	<xsl:text>
{\sect}\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl <!-- #DG798 {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}-->
{\f0\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
}
{\stylesheet{\ql \widctlpar\aspalpha\aspnum\faauto\adjustright\itap0 
\fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \snext0 Normal;}
{\*\cs10 \additive Default Paragraph Font;}{\s15\ql \widctlpar\faauto\itap0 
\b\f0\fs18\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 \sbasedon0 \snext15 
Body Text;}}
{\info{\title </xsl:text>
		<xsl:choose>
			<xsl:when test="/*/CommitmentLetter/LanguageFrench">
				<xsl:text/>
			</xsl:when>
			<xsl:otherwise>
        <!--#DG516 xsl:text>MORTGAGE COMMITMENT</xsl:text-->
        <xsl:value-of select="$docTitle"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> }{\author Zivko Radulovic}{\operator John Schisler}
{\creatim\yr2002\mo8\dy27\hr11\min17}{\revtim\yr2002\mo8\dy27\hr11\min17}
{\version2}{\edmins3}{\nofpages5}{\nofwords0}{\nofchars0}{\*\company filogix Inc}
{\nofcharsws0}{\vern8283}}
\margl720\margr720\margt720\margb720 
\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb
\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin720\dgvorigin720\dghshow1\dgvshow1
\jexpand
\viewkind1\viewscale90\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp
\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0
\headery0\footery400\endnhere\sectlinegrid360\sectdefaultcl 
</xsl:text>
  </xsl:template>
	
	<!--#DG244 not used here, keep it just because it may be used by other templates-->
	<!-- ************************************************************************ 	-->
	<!-- LOGO					                                          			-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="Logo">
		<xsl:value-of select="$logoRtf"/>
	</xsl:template>	

	<!-- #DG798 
	<xsl:template name="absolute">
	    <xsl:param name="x"/>
	    <xsl:choose>
	        <xsl:when test="$x &lt; 0">
	            <xsl:value-of select="$x * -1"/>
	        </xsl:when>
	        <xsl:otherwise>
	           <xsl:value-of select="$x"/>
	        </xsl:otherwise>
	    </xsl:choose>
	</xsl:template-->
	
</xsl:stylesheet>
