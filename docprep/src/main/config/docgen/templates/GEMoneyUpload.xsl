<?xml version="1.0" encoding="UTF-8"?>
<!--09/Mar/2005 DVG #DG166 #1063  DJ - miscellaneous DocPrep fixes  -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="/">
		<xsl:element name="Document">
			<xsl:for-each select="//Deal">
				<xsl:call-template name="doDeal"/>
			</xsl:for-each>		
			<xsl:for-each select="//documentType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>	
		</xsl:element>
	</xsl:template>

	<xsl:template name="createNode">
		<xsl:element name="{name()}">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doDeal">
		<xsl:element name="Deal">
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="applicationId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="applicationDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalLoanAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="amortizationTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="estimatedClosingDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="sourceApplicationId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceApplication">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalPurchasePrice">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="postedInterestRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="underwriterUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="underwriterUser">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="estimatedPaymentAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="PandiPaymentAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="escrowPaymentAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIPremiumAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="netLoanAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="statusDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="discount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="secondaryFinancing">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="originalMortgageNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="denialReasonId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="denialReason">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="renewalOptionsId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="renewalOptions">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="netInterestRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealPurposeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealPurpose">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="buydownRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="specialFeatureId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="specialFeature">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interimInterestAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="downPaymentAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lineOfBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lineOfBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mortgageInsurerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mortgageInsurer">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mtgProdId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mtgProd">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIPolicyNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="firstPaymentDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="netPaymentAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MARSNameSearch">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MARSPropertySearch">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MILoanNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="commisionAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maturityDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="commitmentIssueDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="commitmentAcceptDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refExistingMtgNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTotalLiabilities">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bankABANumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bankAccountNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bankName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="actualClosingDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTotalAssets">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTotalIncome">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="renewalDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="numberOfBorrowers">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="numberOfGuarantors">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="servicingMortgageNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentTermId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentFrequencyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentFrequency">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="prePaymentOptionsId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="prePaymentOptions">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateFormulaId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateFormula">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="statusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="status">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="riskRatingId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="riskRating">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="electronicSystemSource">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="commitmentExpirationDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="requestedSolicitorName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="requestedAppraisorName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestCompoundId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestCompound">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="secondApproverId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="secondApprover">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="jointApproverId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="jointApprover">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="administratorId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="administrator">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="refinanceNewMoney">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interimInterestAdjustmentDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="privilegePaymentId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="privilegePayment">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="advanceHold">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="clientNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedLTV">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTotalAnnualHeatingExp">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTotalAnnualTaxExp">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTotalCondoFeeExp">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTotalPropertyExp">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="commitmentPeriod">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="commitmentProduced">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="crossSellProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="crossSellProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="datePreAppFirm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="fifthApproverId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="fifthApprover">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="finalApproverId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="finalApprover">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="financeProgramId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="financeProgram">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="fourthApproverId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="fourthApprover">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="investorApproved">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="investorConfirmationNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lienHolderSecondaryFinancing">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lienPositionId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lienPosition">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIPayorId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIPayor">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MITypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="newMoney">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="nextRateChangeDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="numberOfProperties">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="oldMoney">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="onHoldDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="preApproval">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="premium">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateLock">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateLockExpirationDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateLockPeriod">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="referenceDealNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="referenceDealTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="referenceDealType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiCurrentBal">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="remainingBalanceSecondaryFin">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="returnDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="secondaryFinancingLienHolder">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceRefAppNBR">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceSystemMailBoxNBR">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="taxPayorId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="taxPayor">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="thirdApproverId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="thirdApprover">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalActAppraisedValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalEstAppraisedValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalPropertyExpenses">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="additionalPrincipal">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalPaymentAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="actualPaymentTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedGDSBorrower">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTDSBorrower">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedGDS3Year">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTDS3Year">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedTotalEquityAvailable">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="effectiveAmortizationMonths">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="postedRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="blendedRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="channelMedia">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="closingTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="closingType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIUpfront">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="preApprovalConclusionDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="preApprovalOriginalOfferDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="previouslyDeclined">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="requiredDownpayment">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIIndicatorId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIIndicator">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="repaymentTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="repaymentType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="decisionModificationTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="decisionModificationType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="scenarioApproved">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="scenarioRecommended">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="scenarioNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="scenarioDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIComments">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="systemTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="systemType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalFeeAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maximumLoanAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="scenarioLocked">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalLoanBridgeAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="solicitorSpecialInstructions">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mortgageInsuranceResponse">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedLendingValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="existingLoanAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="GDSTDS3YearRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maximumPrincipalAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maximumTDSExpensesAllowed">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MICommunicationFlag">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIExistingPolicyNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="minimumIncomeRequired">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="PAPurchasePrice">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="instructionProduced">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="teaserDiscount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="teaserMaturityDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="teaserNetInterestRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="teaserPIAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="teaserTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="openMIResponseFlag">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="resolutionConditionId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="resolutionCondition">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="checkNotesFlag">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="closingDatePlus90Days">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIPremiumPST">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="perdiemInterestAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="IADNumberOfDays">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="PandIPaymentAmountMonthly">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="firstPaymentDateMonthly">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="funderProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="funderProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="progressAdvance">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="advanceToDateAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="advanceNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIRUIntervention">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="preQualificationMICertNum">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiPurpose">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiOrigPurchasePrice">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiBlendedAmortization">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiOrigMtgAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiImprovementsDesc">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiImprovementAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="nextAdvanceAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiOrigPurchaseDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refiCurMortgageHolder">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="resubmissionFlag">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIPolicyNumCMHC">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MIPolicyNumGE">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mccMarketType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="brokerCommPaid">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="brokerCommPa">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="holdReasonId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="holdReason">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pmntPlusLifeDisability">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifePremium">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="disabilityPremium">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="combinedPremium">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestRateIncrement">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestRateIncLifeDisability">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="equivalentInterestRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="commisionCode">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="multiProject">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="proprietairePlus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="proprietairePlusLOC">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceApplicationVersion">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="addCreditCostInsIncurred">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalCreditCost">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="intWithoutInsCreditCost">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="uploadSerialNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="fundingUploadDone">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomeVerificationTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomeVerificationType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Property">
				<xsl:call-template name="doProperty"/>
			</xsl:for-each>
			<xsl:for-each select="Borrower">
				<xsl:call-template name="doBorrower"/>
			</xsl:for-each>
			<xsl:for-each select="Bridge">
				<xsl:call-template name="doBridge"/>
			</xsl:for-each>
			<xsl:for-each select="DocumentTracking">
				<xsl:call-template name="doDocumentTracking"/>
			</xsl:for-each>
			<xsl:for-each select="EscrowPayment">
				<xsl:call-template name="doEscrowPayment"/>
			</xsl:for-each>
			<xsl:for-each select="DownPaymentSource">
				<xsl:call-template name="doDownPaymentSource"/>
			</xsl:for-each>
			<xsl:for-each select="DealFee">
				<xsl:call-template name="doDealFee"/>
			</xsl:for-each>
			<xsl:for-each select="InsureOnlyApplicant">
				<xsl:call-template name="doInsureOnlyApplicant"/>
			</xsl:for-each>

			<xsl:for-each select="SourceOfBusinessProfile">
				<xsl:call-template name="doSourceOfBusinessProfile"/>
			</xsl:for-each>
			<xsl:for-each select="InvestorProfile">
				<xsl:call-template name="doInvestorProfile"/>
			</xsl:for-each>
			<xsl:for-each select="BranchProfile">
				<xsl:call-template name="doBranchProfile"/>
			</xsl:for-each>
			<xsl:for-each select="SourceFirmProfile">
				<xsl:call-template name="doSourceFirmProfile"/>
			</xsl:for-each>
			<xsl:for-each select="GroupProfile">
				<xsl:call-template name="doGroupProfile"/>
			</xsl:for-each>
			<xsl:for-each select="PricingProfile">
				<xsl:call-template name="doPricingProfile"/>
			</xsl:for-each>
			<xsl:for-each select="LenderProfile">
				<xsl:call-template name="doLenderProfile"/>
			</xsl:for-each>
			<xsl:for-each select="Underwriter">
				<xsl:call-template name="doUserProfile"/>
			</xsl:for-each>
			<xsl:for-each select="MtgProd">
				<xsl:call-template name="doMtgProd"/>
			</xsl:for-each>
			<xsl:for-each select="DealNotes">
				<xsl:call-template name="doDealNotes"/>
			</xsl:for-each>

			<xsl:for-each select="mortyLenderId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mortyLender">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="ecniLenderId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="ecniLender">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mortgageInsurancePayorId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mortgageInsurancePayor">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mortgageInsuranceTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mortgageInsuranceType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

  <!--Extension copyid="1" name="SourceOfBusinessProfile" action="subtree" searchKey="sourceOfBusinessProfileId" />
  <Extension copyid="1" name="InvestorProfile" action="subtree" searchKey="investorProfileId" />
  <Extension copyid="1" name="BranchProfile" action="subtree" searchKey="branchProfileId" />
  <Extension copyid="1" name="SourceFirmProfile" action="subtree" searchKey="sourceFirmProfileId" />
  <Extension copyid="1" name="GroupProfile" action="subtree" searchKey="groupProfileId" />
- <Extension copyid="1" name="PricingProfile" action="subtree"> <SearchKey type="pemethod" name="getActualPricingProfileId" searchKey="dealId" /> </Extension>
  <Extension copyid="1" name="LenderProfile" action="subtree" searchKey="lenderProfileId" />
  <Extension copyid="1" name="Underwriter" action="subtree" entity="UserProfile" searchKey="underwriterUserId" />
  <Extension copyid="1" name="MtgProd" action="subtree" entity="MtgProd" searchKey="mtgProdId" />
  <Extension copyid="false" name="DealNotes" action="element" dbchild="true" />

  <Field action="sqlstmt" elementName="mortyLenderId" query="Select mortyLenderId from InstitutionProfile where InstitutionProfileId = 0" />
  <Field action="sqlstmt" elementName="ecniLenderId" query="Select ecniLenderId from InstitutionProfile where InstitutionProfileId = 0" />

  <Field action="map" type="renameentityfield" name="MIPayorId" with="mortgageInsurancePayorId" />
  <Field action="map" type="renameentityfield" name="MITypeId" with="mortgageInsuranceTypeId" />
  <Field action="map" type="picklist" name="MIPayorId" with="mortgageInsurancePayorId" />
  <Field action="map" type="picklist" name="MITypeId" with="mortgageInsuranceTypeId" /-->

		</xsl:element>
	</xsl:template>


	<xsl:template name="doProperty">
		<xsl:element name="Property">

			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="streetTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="streetType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyUsageId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyUsage">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dwellingStyleId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dwellingStyle">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="occupancyTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="occupancyType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyStreetNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyStreetName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="heatTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="heatType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyAddressLine2">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyCity">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyPostalFSA">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyPostalLDU">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="legalLine1">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="legalLine2">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="legalLine3">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="numberOfBedrooms">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="yearBuilt">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insulatedWithUFFI">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="provinceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="province">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="monthlyCondoFees">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="purchasePrice">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="municipalityId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="municipality">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="landValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyValueSource">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="appraisalSourceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="appraisalSource">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="equityAvailable">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="newConstructionId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="newConstruction">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="estimatedAppraisalValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="actualAppraisalValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="loanToValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="unitNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lotSize">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="primaryPropertyFlag">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="appraisalDateAct ">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lotSizeDepth">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lotSizeFrontage">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lotSizeUnitOfMeasureId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lotSizeUnitOfMeasure">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="numberOfUnits">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyLocationId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyLocation">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyOccurenceNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyReferenceNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sewageTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sewageType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="streetDirectionId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="streetDirection">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="waterTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="waterType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="livingSpace">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="livingSpaceUnitOfMeasureId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="livingSpaceUnitOfMeasure">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="structureAge">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dwellingTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dwellingType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lendingValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalPropertyExpenses">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalAnnualTaxAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="zoning">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="MLSListingFlag">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="garageSizeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="garageSize">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="garageTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="garageType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="builderName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="provinceName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="provinceAbbreviation">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="PropertyExpense">
				<xsl:call-template name="doPropertyExpense"/>
			</xsl:for-each>
			<xsl:for-each select="AppraisalOrder">
				<xsl:call-template name="doAppraisalOrder"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doBorrower">
		<xsl:element name="Borrower">
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerFirstName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerMiddleInitial">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerLastName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="firstTimeBuyer">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerBirthDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="nsfOccurenceTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="nsfOccurenceType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerGenderId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerGender">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="salutationId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="salutation">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maritalStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maritalStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="socialInsuranceNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerHomePhoneNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerWorkPhoneNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerFaxNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerEmailAddress">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="clientReferenceNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="numberOfDependents">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="numberOfTimesBankrupt">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="raceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="race">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditScore">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="GDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="TDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="educationLevelId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="educationLevel">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="netWorth">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="existingClient">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditSummary">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="languagePreferenceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="languagePreference">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerGeneralStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerGeneralStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="citizenshipTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="citizenshipType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="primaryBorrowerFlag">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalAssetAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalLiabilityAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentHistoryTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentHistoryType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalIncomeAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dateOfCreditBureauProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditBureauOnFileDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bureauAttachment">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="existingClientComments">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bankruptcyStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bankruptcyStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="age">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="staffOfLender">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="staffType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="totalLiabilityPayments">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="GDS3Year">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="TDS3Year">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="SINCheckDigit">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditBureauNameId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditBureauName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerWorkPhoneExtension">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditBureauSummary">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insuranceProportionsId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insuranceProportions">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifePercentCoverageReq">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="disabilityStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="disabilityStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifeStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifeStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="smokeStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="smokeStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="disPercentCoverageReq">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="guarantorOtherLoans">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="isGuarantor">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="BorrowerAddress">
				<xsl:call-template name="doBorrowerAddress"/>
			</xsl:for-each>
			<xsl:for-each select="Income">
				<xsl:call-template name="doIncome"/>
			</xsl:for-each>
			<xsl:for-each select="Asset">
				<xsl:call-template name="doAsset"/>
			</xsl:for-each>
			<xsl:for-each select="Liability">
				<xsl:call-template name="doLiability"/>
			</xsl:for-each>
			<xsl:for-each select="CreditReference">
				<xsl:call-template name="doCreditReference"/>
			</xsl:for-each>
			<xsl:for-each select="EmploymentHistory">
				<xsl:call-template name="doEmploymentHistory"/>
			</xsl:for-each>
			<xsl:for-each select="LifeDisabilityPremiums">
				<xsl:call-template name="doLifeDisabilityPremiums"/>
			</xsl:for-each>
			<xsl:for-each select="equifaxCreditReportXml">
				<xsl:copy-of select="."/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="doBridge">
		<xsl:element name="Bridge">
			<xsl:for-each select="bridgeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="investorProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lenderProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bridgePurposeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bridgePurpose">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentTermId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pricingProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pricingProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="discount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="actualPaymentTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="postedInterestRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="netInterestRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="netLoanAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="postedRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maturityDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="premium">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doDocumentTracking">
		<xsl:element name="DocumentTracking">
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="applicationId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentTrackingId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentTrackingSourceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentRequestDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dStatusChangeDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentDueDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentResponsibilityId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentResponsibility">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentResponsibilityRoleId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="documentResponsibilityRole">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="conditionId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="condition">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="tagContextId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="tagContext">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="tagContextClassId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="tagContextClass">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="verb">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="conditionSection">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doEscrowPayment">
		<xsl:element name="EscrowPayment">
			<xsl:for-each select="conditionSection">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="escrowPaymentId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="escrowTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="escrowType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="escrowPaymentDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="escrowPaymentAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doDownPaymentSource">
		<xsl:element name="DownPaymentSource">
			<xsl:for-each select="downPaymentSourceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="downPaymentSourceTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="downPaymentSourceType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="amount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="DPSDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doDealFee">
		<xsl:element name="DealFee">
			<xsl:for-each select="dealFeeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="fee">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeComments">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="payableIndicator">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feePaymentMethodId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feePaymentMethod">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feePaymentDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="payorProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="payorProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Fee">
				<xsl:call-template name="doFee"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doInsureOnlyApplicant">
		<xsl:element name="InsureOnlyApplicant">
			<xsl:for-each select="insureOnlyApplicantId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insureOnlyApplicantFirstName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insureOnlyApplicantLastName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insureOnlyApplicantBirthDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="primaryInsureOnlyApplicantFlag">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insureOnlyApplicantGenderId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insureOnlyApplicantGender">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insuranceProportionsId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insuranceProportions">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifePercentCoverageReq">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="smokeStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="smokeStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="disabilityStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="disabilityStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifeStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifeStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="disPercentCoverageReq">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="insureOnlyApplicantAge">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doSourceOfBusinessProfile">
		<xsl:element name="SourceOfBusinessProfile">
			<xsl:for-each select="sourceOfBusinessProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lengthOfService">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="tierLevel">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="compensationFactor">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="SOBPShortName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="SOBPBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="SOBPBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceOfBusinessCategoryId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceOfBusinessCategory">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="alternativeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="alternative">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="partyProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="notes">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="SOBRegionId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="SOBRegion">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceFirmProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="systemTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="systemType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceOfBusinessCode">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceOfBusinessPriorityId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceOfBusinessPriority">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Contact">
				<xsl:call-template name="doContact"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doInvestorProfile">
		<xsl:element name="InvestorProfile">
			<xsl:for-each select="investorProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="investorProfileName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultInvestor">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contact">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="IPShortName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="IPBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="IPBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Contact">
				<xsl:call-template name="doContact"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doBranchProfile">
		<xsl:element name="BranchProfile">
			<xsl:for-each select="branchProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="regionProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="regionProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchManagerUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchManagerUser">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="costCentre">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="servicingCentre">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contact">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="BPShortName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="BPBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="BPBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="timeZoneEntryId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="timeZoneEntry">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="partyProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="partyProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="GEBranchTransitNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="CMHCBranchTransitNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Contact">
				<xsl:call-template name="doContact"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doSourceFirmProfile">
		<xsl:element name="SourceFirmProfile">
			<xsl:for-each select="sourceFirmName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="systemTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="systemType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceFirmProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceFirmProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contact">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sfShortName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sfBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sfBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="alternativeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="alternative">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sfMOProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sfMOProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="sourceFirmCode">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Contact">
				<xsl:call-template name="doContact"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doGroupProfile">
		<xsl:element name="GroupProfile">
			<xsl:for-each select="groupProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="groupName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="supervisorUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="supervisorUser">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contact">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Contact">
				<xsl:call-template name="doContact"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doPricingProfile">
		<xsl:element name="PricingProfile">
			<xsl:for-each select="pricingProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="indexName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="ppBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="ppBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestRateChangeFrequency">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pricingRegistrationTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="primeIndexIndicator">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pricingStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pricingStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateDisabledDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="ppDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateCode">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="rateCodeDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doLenderProfile">
		<xsl:element name="LenderProfile">
			<xsl:for-each select="lenderProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultLender">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lenderName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contact">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lpShortName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lpBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lpBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="upfrontMIAllowed">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultInvestorId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultInvestor">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="privateLabelIndicator">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="registrationName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestCompoundId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestCompound">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Contact">
				<xsl:call-template name="doContact"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doUserProfile">
		<!--xsl:element name="UserProfile"-->
		<xsl:element name="Underwriter">
			<xsl:for-each select="JOINT_APPROVERS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bilingual">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="branchProfileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="groupId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="groupProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="groupProfileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="jointApproverUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="managerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="partnerUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="regionId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="regionProfileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="secondApproverUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="standInUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="standardAccess">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="upBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="userProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="userTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="userType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<!-- this one is different; below is the original list from userprofile.java
			xsl:for-each select="userProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="userTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="userType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="groupProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="groupProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="standardAccess">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="bilingual">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="secondApproverUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="secondApproverUser">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="jointApproverUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="jointApproverUser">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contact">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="profileStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="partnerUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="partnerUser">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="standInUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="standInUser">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="managerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="manager">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="upBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="upBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each-->

			<xsl:for-each select="Contact">
				<xsl:call-template name="doContact"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doMtgProd">
		<xsl:element name="MtgProd">
			<xsl:for-each select="mtgProdId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mtgProdName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="minimumAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maximumAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="minimumLTV">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="maximumLTV">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentTermId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="paymentTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pricingProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pricingProfile">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lineOfBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lineOfBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="commitmentTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mpBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mpBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="prePaymentOptionsId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="prePaymentOptions">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="privilegePaymentId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="privilegePayment">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="teaserDiscount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="teaserTerm">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="mpShortName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestCompoundingId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="interestCompounding">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="miscellaneousRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="PricingProfile">
				<xsl:call-template name="doPricingProfile"/>
			</xsl:for-each>

			<xsl:for-each select="ptTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="ptDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doDealNotes">
		<xsl:element name="DealNotes">
			<xsl:for-each select="dealId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealNotesId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealNotesCategoryId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealNotesCategory">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealNotesDate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="applicationId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealNotesUserId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealNotesUser">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="dealNotesText">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doPropertyExpense">
		<xsl:element name="PropertyExpense">
			<xsl:for-each select="propertyExpenseId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyExpensePeriodId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyExpensePeriod">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyExpenseTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyExpenseType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="property">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyExpenseAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="propertyExpenseDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="monthlyExpenseAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="peIncludeInGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="peIncludeInTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pePercentInGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pePercentInTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pePercentOutGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="pePercentOutTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doBorrowerAddress">
		<xsl:element name="BorrowerAddress">
			<xsl:for-each select="borrowerAddressId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerAddressTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerAddressType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="addrId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="monthsAtAddress">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="residentialStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="residentialStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			
			<xsl:for-each select="Addr">
				<xsl:call-template name="doAddr"/>
			</xsl:for-each>
			
		</xsl:element>
	</xsl:template>

	<xsl:template name="doAddr">
		<xsl:element name="Addr">
			<xsl:for-each select="addrId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="provinceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="province">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="addressLine1">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="addressLine2">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="city">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="postalFSA">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="postalLDU">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doIncome">
		<xsl:element name="Income">
			<xsl:for-each select="incomePeriodId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomePeriod">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomeTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomeType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="income">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomeAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incIncludeInGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incIncludeInTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incPercentInGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incPercentInTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incPercentOutGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incPercentOutTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomeDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="annualIncomeAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="monthlyIncomeAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

		</xsl:element>
	</xsl:template>

	<xsl:template name="doAsset">
		<xsl:element name="Asset">

			<xsl:for-each select="assetId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="assetDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="assetValue">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="assetTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="assetType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="includeInNetWorth">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="percentInNetWorth">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

		</xsl:element>
	</xsl:template>

	<xsl:template name="doLiability">
		<xsl:element name="Liability">

			<xsl:for-each select="liabilityId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="liabilityTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="liabilityType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="liabilityAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="liabilityMonthlyPayment">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="includeInGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="includeInTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="liabilityDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="liabilityPayOffTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="liabilityPayOffType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="percentInGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="percentInTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="liabilityPaymentQualifier">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="percentOutGDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="percentOutTDS">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

		</xsl:element>
	</xsl:template>

	<xsl:template name="doCreditReference">
		<xsl:element name="CreditReference">
			<xsl:for-each select="creditReferenceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="institutionName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="accountNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="currentBalance">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditRefTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditRefType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="creditReferenceDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="timeWithReference">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

		</xsl:element>
	</xsl:template>

	<xsl:template name="doEmploymentHistory">
		<xsl:element name="EmploymentHistory">
			<xsl:for-each select="employmentHistoryId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="occupationId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="occupation">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="industrySectorId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="industrySector">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="employerName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="monthsOfService">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="employmentHistoryNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contact">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="employmentHistoryTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="employmentHistoryType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="incomeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="income">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="employmentHistoryStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="employmentHistoryStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<!--xsl:for-each select="jobTitle">
				<xsl:call-template name="createNode"/>
			</xsl:for-each-->
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="jobTitleId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="jobTitle">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Contact">
				<xsl:call-template name="doContact"/>
			</xsl:for-each>

		</xsl:element>
	</xsl:template>

	<xsl:template name="doLifeDisabilityPremiums">
		<xsl:element name="LifeDisabilityPremiums">

			<xsl:for-each select="lifeDisabilityPremiumsId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="borrowerId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lenderProfileId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="LDInsuranceRateId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="LDInsuranceRate">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="LDInsuranceTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="LDInsuranceType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="disabilityStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifeStatusId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="lifeStatus">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

		</xsl:element>
	</xsl:template>

	<xsl:template name="doContact">
		<xsl:element name="Contact">
			<xsl:for-each select="contactId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="copyId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactFirstName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactMiddleInitial">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactLastName">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactPhoneNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactFaxNumber">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactEmailAddress">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="addrId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="salutationId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="salutation">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactJobTitle">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="contactPhoneNumberExtension">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="languagePreferenceId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="languagePreference">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="preferredDeliveryMethodId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="preferredDeliveryMethod">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>

			<xsl:for-each select="Addr">
				<xsl:call-template name="doAddr"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template name="doFee">
		<xsl:element name="Fee">
			<xsl:for-each select="feeDescription">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="standardAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="GLAccount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="offSetingFeeTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="offSetingFeeType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultPaymentMethodId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultPaymentMethod">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feePayorTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feePayorType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeBusinessId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeBusiness">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="payableIndicator ">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultFeeAmount">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultFeePaymentDateTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="defaultFeePaymentDateType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeGenerationTypeId">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="feeGenerationType">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
			<xsl:for-each select="refundable">
				<xsl:call-template name="createNode"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
