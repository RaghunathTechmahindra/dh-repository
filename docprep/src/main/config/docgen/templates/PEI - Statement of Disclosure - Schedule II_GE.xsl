<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	
<!-- Venkata - Hardcoding Lender Name - CR - Begin -->
<xsl:variable name="lenderName" select="'GE Money'" />	
<!-- Venkata - Hardcoding Lender Name - CR - End -->

	<xsl:output version="1.0" method="html" encoding="UTF-8" indent="no"/>
	<xsl:variable name="fo:layout-master-set">
		<fo:layout-master-set>
			<fo:simple-page-master master-name="default-page" page-height="11in" page-width="8.5in" margin-left="0.6in" margin-right="0.6in">
				<fo:region-body margin-top="0.79in" margin-bottom="0.79in"/>
			</fo:simple-page-master>
		</fo:layout-master-set>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:variable name="maxwidth" select="7.30000"/>
		<fo:root>
			<xsl:copy-of select="$fo:layout-master-set"/>
			<fo:page-sequence master-reference="default-page" initial-page-number="1" format="1">
				<fo:flow flow-name="xsl-region-body">
					<fo:block>
						<xsl:variable name="tablewidth0" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths0" select="0.000"/>
						<xsl:variable name="defaultcolumns0" select="1"/>
						<xsl:variable name="defaultcolumnwidth0">
							<xsl:choose>
								<xsl:when test="$defaultcolumns0 &gt; 0">
									<xsl:value-of select="($tablewidth0 - $sumcolumnwidths0) div $defaultcolumns0"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth0_0" select="$defaultcolumnwidth0"/>
						<fo:table width="{$tablewidth0}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth0_0}in"/>
							<fo:table-body font-size="12pt">
								<fo:table-row>
									<fo:table-cell height="0.50000in" text-align="center">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>SCHEDULE II</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>STATEMENT OF DISCLOSURE</xsl:text>
											</fo:inline>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<fo:inline font-size="12pt" font-weight="bold">
												<xsl:text>MORTGAGE LOANS</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth1" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths1" select="0.59375 + 0.04167 + 7.34375 + 0.04167"/>
						<xsl:variable name="factor1">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths1 &gt; 0.00000 and $sumcolumnwidths1 &gt; $tablewidth1">
									<xsl:value-of select="$tablewidth1 div $sumcolumnwidths1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth1_0" select="0.64375 * $factor1"/>
						<xsl:variable name="columnwidth1_1" select="7.28375 * $factor1"/>
						<fo:table width="{$tablewidth1}in"  border-collapse="separate"  color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth1_0}in"/>
							<fo:table-column column-width="{$columnwidth1_1}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell padding-top="0.02083in" padding-bottom="1pt" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Date:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="1pt" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10">
												<xsl:value-of select="//DSCL_DATE_1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2" text-align="center" padding-top="1pt" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>(date on which the statement of disclosure is made)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2" border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.04083in" padding-bottom="1pt" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10">	<!-- Venkata - Hardcoding Lender Name - CR - Begin --><!-- <xsl:value-of select="//DSCL_LEND_1"/>-->  <xsl:value-of select="$lenderName"/><!-- Venkata - Hardcoding Lender Name - CR - End -->&#160;
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2" text-align="center" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>(name of lender)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding-top="0.04083in" padding-bottom="1pt" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Address:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.04083in" padding-bottom="1pt" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10">	  <xsl:text>&#160;&#160; </xsl:text>
												<xsl:value-of select="//DSCL_LENDADD_1"/>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2" text-align="center" padding-top="1pt" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>(address of lender)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2" border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in" padding-top="0.02083in" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline font-size="10">	<xsl:value-of select="//DSCL_BOR_COBOR_1"/>
											</fo:inline>
											<fo:inline>
												<xsl:text>&#160;&#160;&#160;</xsl:text>
											</fo:inline>
										<!--<fo:inline font-size="10">		<xsl:value-of select="//DSCL_BORROW_2"/>
											</fo:inline>-->
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="2" text-align="center" padding-top="1pt" padding-bottom="0.02083in" padding-left="0.02083in" padding-right="0.02083in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>(name of borrower(s))</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<fo:block>
							<fo:leader leader-pattern="space"/>
						</fo:block>
						<xsl:variable name="tablewidth2" select="$maxwidth * 1.00000"/>
						<xsl:variable name="sumcolumnwidths2" select="9.73958"/>
						<xsl:variable name="factor2">
							<xsl:choose>
								<xsl:when test="$sumcolumnwidths2 &gt; 0.00000 and $sumcolumnwidths2 &gt; $tablewidth2">
									<xsl:value-of select="$tablewidth2 div $sumcolumnwidths2"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="1.000"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="columnwidth2_0" select="9.73958 * $factor2"/>
						<fo:table width="{$tablewidth2}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
							<fo:table-column column-width="{$columnwidth2_0}in"/>
							<fo:table-body font-size="10pt">
								<fo:table-row>
									<fo:table-cell height="0.30208in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Property on which there will be a mortgage (address and description of buildings)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth3" select="$columnwidth2_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths3" select="1.98958 + 1.73958 + 0.11458 + 1.52083 + 0.18750 + 1.32292 + 2.26042"/>
											<xsl:variable name="factor3">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths3 &gt; 0.00000 and $sumcolumnwidths3 &gt; $tablewidth3">
														<xsl:value-of select="$tablewidth3 div $sumcolumnwidths3"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns3" select="1 + 1"/>
											<xsl:variable name="defaultcolumnwidth3">
												<xsl:choose>
													<xsl:when test="$factor3 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns3 &gt; 0">
														<xsl:value-of select="($tablewidth3 - $sumcolumnwidths3) div $defaultcolumns3"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>

											<fo:table width="{$tablewidth3}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="proportional-column-width(17)"/>
												<fo:table-column column-width="proportional-column-width(12)"/>
												<fo:table-column column-width="proportional-column-width(2)"/>
												<fo:table-column column-width="proportional-column-width(12)"/>
												<fo:table-column column-width="proportional-column-width(2)"/>
												<fo:table-column column-width="proportional-column-width(12)"/>
												<fo:table-column column-width="proportional-column-width(15)"/>

												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>1. Principal amount of the $</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10">
																	<xsl:value-of select="//DSCL_AMOUNT_1"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>, $  </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10">
																	<xsl:value-of select="//PE_DSCL_AMOUNT_2"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>, $ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="10">
																	<xsl:value-of select="//DSCL_AMOUNT_3"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>.</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>(1st,</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>2nd,</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>3rd)</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell text-align="justify" text-depth="10pt">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline >
												<xsl:text>2. Deduct charges, fees, etc. where applicable. (This amount must equal the total of the amounts under section 9.)</xsl:text>
											</fo:inline>
											<fo:block text-depth="10pt">
												<fo:inline >
													<xsl:text>$ </xsl:text>
												</fo:inline>
												<xsl:choose>
													<xsl:when test="//DSCL_TOTFEES_2">
														<fo:inline font-size="10pt" text-decoration="underline" text-depth="10pt">
															<xsl:value-of select="//DSCL_TOTFEES_2"/>&#160;
														</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<fo:inline font-size="9pt" keep-together.within-line="always">
															<xsl:text>____________</xsl:text>
														</fo:inline>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>3. Amount of money to be paid to borrower or to be disbursed on the borrower&apos;s direction is $ </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_NETLOAN_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_NETLOAN_1"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text>, $ </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_NETLOAN_2">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_NETLOAN_2"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text>,</xsl:text>
											</fo:inline>
										</fo:block>
										<fo:block>
											<fo:inline>
												<xsl:text> $ </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_NETLOAN_3">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_NETLOAN_3"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>4. The annual percentage rate of the mortgage of $ </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_MTGAMT_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_MTGAMT_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text> will be </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_RATE_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_RATE_1"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text>%* or, if the annual percentage rate is subject to variations, the initial annual percentage rate will be </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_RATE_2">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_RATE_2"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text>%*</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell height="0.04167in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>*Accurate to within 1/8th of 1%</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.18750in" text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>5. The principal amount and the cost of borrowing based on the annual percentage rate disclosed in section 4 will be payable in </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_PAYFREQ_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_PAYFREQ_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text> payments of $ </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_PAYAMT_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_PAYAMT_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>6. Based on the anual percentage rate disclosed in section 4, the mortgage will become due and payable in </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_MTGTERM_2">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_MTGTERM_2"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text> years at which time the borrower, if all payments have been made on the due date, will owe $ </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_BLNCEENDTRM_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_BLNCEENDTRM_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>7. The term of mortgage: </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_MTGTERM_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_MTGTERM_1"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text> months. Amortization period of mortgage </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_MTGAMRTIZTON_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_MTGAMRTIZTON_1"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>_________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text> years.</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block>
											<fo:leader leader-pattern="space"/>
										</fo:block>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>8. Where the term of the loan is subject to variations, it shall vary in the following manner:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<xsl:choose>
									<xsl:when test="//DSCL_TERMVAR_1">
										<fo:table-row>
												<fo:table-cell height="0.26042in" >
													<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">
														<fo:inline font-size="10pt" text-decoration="underline">
															<xsl:value-of select="//DSCL_TERMVAR_1"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
										</fo:table-row>
									</xsl:when>
									<xsl:otherwise>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
									</xsl:otherwise>
								</xsl:choose>
								<fo:table-row>

								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>9. The charge, fee, etc, under section 2 are made up as follows:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth11" select="$columnwidth2_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths11" select="2.34375 + 1.72917 + 0.11458 + 0.82292"/>
											<xsl:variable name="factor11">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths11 &gt; 0.00000 and $sumcolumnwidths11 &gt; $tablewidth11">
														<xsl:value-of select="$tablewidth11 div $sumcolumnwidths11"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns11" select="1"/>
											<xsl:variable name="defaultcolumnwidth11">
												<xsl:choose>
													<xsl:when test="$factor11 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns11 &gt; 0">
														<xsl:value-of select="($tablewidth11 - $sumcolumnwidths11) div $defaultcolumns11"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth11_0" select="2.34375 * $factor11"/>
											<xsl:variable name="columnwidth11_1" select="1.72917 * $factor11"/>
											<xsl:variable name="columnwidth11_2" select="0.11458 * $factor11"/>
											<xsl:variable name="columnwidth11_3" select="0.82292 * $factor11"/>
											<xsl:variable name="columnwidth11_4" select="$defaultcolumnwidth11"/>
											<fo:table width="{$tablewidth11}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth11_0}in"/>
												<fo:table-column column-width="{$columnwidth11_1}in"/>
												<fo:table-column column-width="{$columnwidth11_2}in"/>
												<fo:table-column column-width="{$columnwidth11_3}in"/>
												<fo:table-column column-width="{$columnwidth11_4}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.13042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.13042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.13042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.13042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.13042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Mortgage insurance fees</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.16667in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//DSCL_MIFEE_1">
																		<fo:inline font-size="10pt" text-decoration="underline">
																			<xsl:value-of select="//DSCL_MIFEE_1"/>
																		</fo:inline>
																	</xsl:when>
																	<xsl:otherwise>
																		<fo:inline font-size="9pt" >
																			<xsl:text>____________</xsl:text>
																		</fo:inline>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell >
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>


													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Inspection and appraisal fees</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell display-align="after" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//DISC_INSPCTNFEE_1">
																		<fo:inline font-size="10pt" text-decoration="underline">
																			<xsl:value-of select="//DISC_INSPCTNFEE_1"/>
																		</fo:inline>
																	</xsl:when>
																	<xsl:otherwise>
																		<fo:inline font-size="9pt" >
																			<xsl:text>____________</xsl:text>
																		</fo:inline>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="5" >
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Estimated legal fees and disbursements (as provided by the borrower) of not more than</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell height="0.17708in" number-columns-spanned="2">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.17708in" text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline font-size="9pt" >
																	<xsl:text>_____</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell height="0.17708in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Other charges</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$ </xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell text-align="right">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<xsl:choose>
																	<xsl:when test="//DSCL_OTHERFEE_1">
																		<fo:inline font-size="10pt" text-decoration="underline">
																			<xsl:value-of select="//DSCL_OTHERFEE_1"/>
																		</fo:inline>
																	</xsl:when>
																	<xsl:otherwise>
																		<fo:inline font-size="9pt" >
																			<xsl:text>____________</xsl:text>
																		</fo:inline>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell>
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row text-align="left">
														<fo:table-cell height="0.19792in">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>Total as shown in section 2</xsl:text>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
														<fo:table-cell display-align="after" height="0.19792in" number-columns-spanned="3">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell text-align="center">
															<fo:block padding-top="1pt" padding-bottom="1pt">
																<fo:inline>
																	<xsl:text>$ </xsl:text>
																</fo:inline>
																<xsl:choose>
																	<xsl:when test="//DSCL_TOTFEES_2">
																		<fo:inline font-size="10pt" text-decoration="underline">
																			<xsl:value-of select="//DSCL_TOTFEES_2"/>
																		</fo:inline>
																	</xsl:when>
																	<xsl:otherwise>
																		<fo:inline font-size="9pt" >
																			<xsl:text>____________</xsl:text>
																		</fo:inline>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
														
													</fo:table-row>

												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>10. Where the annual percentage rate is subject to variations, it shall vary in the following manner, based on the following conditions:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<xsl:choose>
									<xsl:when test="//DSCL_RATEVAR_1">
										<fo:table-row>
												<fo:table-cell height="0.26042in">
													<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">
														<fo:inline font-size="10pt" text-decoration="underline">
															<xsl:value-of select="//DSCL_RATEVAR_1"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
										</fo:table-row>
									</xsl:when>
									<xsl:otherwise>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
									</xsl:otherwise>
								</xsl:choose>

								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>11. The terms and conditions of repayment before maturity of the loan contract are as follows:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<xsl:choose>
									<xsl:when test="//DSCL_REPAYTERMS_1">
										<fo:table-row>
												<fo:table-cell height="0.26042in">
													<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">
														<fo:inline font-size="10pt" text-decoration="underline">
															<xsl:value-of select="//DSCL_REPAYTERMS_1"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
										</fo:table-row>
									</xsl:when>
									<xsl:otherwise>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
									</xsl:otherwise>
								</xsl:choose>



								<fo:table-row>
									<fo:table-cell text-align="justify">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>12. Where the mortgage is not repaid at maturity or a payment is not made when due, the following charges may be imposed:</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>


								<xsl:choose>
									<xsl:when test="//DSCL_OVERDUE_1">
										<fo:table-row>
												<fo:table-cell height="0.26042in" >
													<fo:block padding-top="1pt" padding-bottom="1pt" white-space-collapse="false">
														<fo:inline font-size="10pt" text-decoration="underline">
															<xsl:value-of select="//DSCL_OVERDUE_1"/>
														</fo:inline>
													</fo:block>
												</fo:table-cell>
										</fo:table-row>
									</xsl:when>
									<xsl:otherwise>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" order-bottom-width="0.01042in">
												<fo:block padding-top="1pt" padding-bottom="1pt"/>
											</fo:table-cell>
										</fo:table-row>
									</xsl:otherwise>
								</xsl:choose>


								<fo:table-row>
									<fo:table-cell height="0.12500in">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:block>
												<fo:leader leader-pattern="space"/>
											</fo:block>
											<fo:inline>
												<xsl:text>13. The first payment is due on </xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_PYMTDAY_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_PYMTDAY_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<xsl:choose>
												<xsl:when test="//DSCL_PYMTDT_1_MONTH">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_PYMTDT_1_MONTH"/>
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
											<fo:inline>
												<xsl:text>,&#160;&#160;20</xsl:text>
											</fo:inline>
											<xsl:choose>
												<xsl:when test="//DSCL_PYMTYR_1">
													<fo:inline font-size="10pt" text-decoration="underline">
														<xsl:value-of select="//DSCL_PYMTYR_1"/>&#160;
													</fo:inline>
												</xsl:when>
												<xsl:otherwise>
													<fo:inline font-size="9pt" >
														<xsl:text>____________</xsl:text>
													</fo:inline>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.22917in">
										<fo:block padding-top="1pt" padding-bottom="1pt"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth15" select="$columnwidth2_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths15" select="4.81250"/>
											<xsl:variable name="factor15">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths15 &gt; 0.00000 and $sumcolumnwidths15 &gt; $tablewidth15">
														<xsl:value-of select="$tablewidth15 div $sumcolumnwidths15"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns15" select="1"/>
											<xsl:variable name="defaultcolumnwidth15">
												<xsl:choose>
													<xsl:when test="$factor15 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns15 &gt; 0">
														<xsl:value-of select="($tablewidth15 - $sumcolumnwidths15) div $defaultcolumns15"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth15_0" select="4.81250 * $factor15"/>
											<xsl:variable name="columnwidth15_1" select="$defaultcolumnwidth15"/>
											<fo:table width="{$tablewidth15}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth15_0}in"/>
												<fo:table-column column-width="{$columnwidth15_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.12500in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.12500in" border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell height="0.04167in" text-align="right">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Signature on behalf of the lender</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<fo:leader leader-pattern="space"/>
										</fo:block>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<xsl:variable name="tablewidth16" select="$columnwidth2_0 * 1.00000"/>
											<xsl:variable name="sumcolumnwidths16" select="4.82292"/>
											<xsl:variable name="factor16">
												<xsl:choose>
													<xsl:when test="$sumcolumnwidths16 &gt; 0.00000 and $sumcolumnwidths16 &gt; $tablewidth16">
														<xsl:value-of select="$tablewidth16 div $sumcolumnwidths16"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="1.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="defaultcolumns16" select="1"/>
											<xsl:variable name="defaultcolumnwidth16">
												<xsl:choose>
													<xsl:when test="$factor16 &lt; 1.000">
														<xsl:value-of select="0.000"/>
													</xsl:when>
													<xsl:when test="$defaultcolumns16 &gt; 0">
														<xsl:value-of select="($tablewidth16 - $sumcolumnwidths16) div $defaultcolumns16"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="0.000"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:variable name="columnwidth16_0" select="4.82292 * $factor16"/>
											<xsl:variable name="columnwidth16_1" select="$defaultcolumnwidth16"/>
											<fo:table width="{$tablewidth16}in" space-before.optimum="1pt" space-after.optimum="2pt" border-collapse="separate" color="black" display-align="center">
												<fo:table-column column-width="{$columnwidth16_0}in"/>
												<fo:table-column column-width="{$columnwidth16_1}in"/>
												<fo:table-body font-size="10pt">
													<fo:table-row>
														<fo:table-cell height="0.26042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
														<fo:table-cell height="0.26042in" border-bottom-style="dotted" border-bottom-color="black" border-bottom-width="0.01042in">
															<fo:block padding-top="1pt" padding-bottom="1pt"/>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="right">
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>Signature of borrower(s)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block padding-top="1pt" padding-bottom="1pt">
											<fo:inline>
												<xsl:text>(EC16/87;116/87)</xsl:text>
											</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
