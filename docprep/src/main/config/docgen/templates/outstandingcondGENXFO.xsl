<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
		<xsl:choose>
			<xsl:when test="//LanguageEnglish">
				<xsl:call-template name="EnglishPage1"/>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="//LanguageFrench">
				<!--				<xsl:call-template name="FrenchTemplate"/>-->
			</xsl:when>
		</xsl:choose>
		<xsl:call-template name="FOEnd"/>
	</xsl:template>
	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	<xsl:template name="EnglishHeader">
		<xsl:text>&lt;fo:static-content flow-name="header"&gt;
			&lt;fo:block&gt;
				&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" text-align="end" vertical-align="baseline"&gt;Page: &lt;fo:page-number/&gt; of &lt;fo:page-number-citation ref-id="endofdoc"/&gt;
				&lt;/fo:block&gt;
				&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" text-align="start" text-decoration="none" vertical-align="baseline"&gt;
					&lt;fo:leader line-height="11pt"/&gt;
				&lt;/fo:block&gt;
			&lt;/fo:block&gt;
		&lt;/fo:static-content&gt;</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishFooter">
		<xsl:text>&lt;fo:static-content flow-name="footer"&gt;
			&lt;fo:block&gt;
				&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" text-align="center" vertical-align="baseline"&gt;
					&lt;fo:leader leader-pattern="rule" rule-thickness="1pt" color="black" leader-length="80%"/&gt;
				&lt;/fo:block&gt;
				&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" text-align="center" vertical-align="baseline"&gt;</xsl:text><xsl:value-of select="//LenderName"/><xsl:text>&lt;/fo:block&gt;
				&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" text-align="center" vertical-align="baseline"&gt;</xsl:text><xsl:value-of select="//BranchAddressComplete"/><xsl:text>&lt;/fo:block&gt;
				&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" text-align="center" vertical-align="baseline"&gt;Phone: </xsl:text><xsl:value-of select="//BranchPhone"/><xsl:text>   Fax: </xsl:text><xsl:value-of select="//BranchFax"/><xsl:text>&lt;/fo:block&gt;
				&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" text-align="center" vertical-align="baseline"&gt;Toll-free Phone: </xsl:text><xsl:value-of select="//BranchTollPhone"/><xsl:text>   Toll-free Fax: </xsl:text><xsl:value-of select="//BranchTollFax"/><xsl:text>&lt;/fo:block&gt;
			&lt;/fo:block&gt;
		&lt;/fo:static-content&gt;</xsl:text>
	</xsl:template>
	<xsl:template name="EnglishPage1">
	<xsl:text>&lt;fo:page-sequence master-reference="main" language="en"&gt;</xsl:text>
				<xsl:call-template name="EnglishHeader"/>
				<xsl:call-template name="EnglishFooter"/>
				<xsl:text>&lt;fo:flow flow-name="region-body" font-family="Arial, Helvetica" font-size="11pt"&gt;
							&lt;fo:table table-layout="fixed"&gt;
								&lt;fo:table-column column-width="540pt"/&gt;
								&lt;fo:table-body&gt;
									&lt;fo:table-row&gt;
										&lt;fo:table-cell background-color="#C0C0C0" border-style="none" padding-left="5.4pt" padding-right="5.4pt" text-align="right"&gt;
											&lt;fo:block font-size="12.0pt" font-weight="bold"&gt;Conditions Outstanding&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;
								&lt;/fo:table-body&gt;
							&lt;/fo:table&gt;
							&lt;fo:block font-size="12.0pt"&gt;
								&lt;fo:leader line-height="12pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:table table-layout="fixed"&gt;
								&lt;fo:table-column column-width="55.4pt"/&gt;
								&lt;fo:table-column column-width="75.0pt"/&gt;
								&lt;fo:table-column column-width="125.0pt"/&gt;
								&lt;fo:table-column column-width="125.0pt"/&gt;
								&lt;fo:table-column column-width="159.6pt"/&gt;
								&lt;fo:table-body&gt;
									&lt;fo:table-row&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;To:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//SourceFirmName"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;From:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//LenderNameShort"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;
									&lt;fo:table-row&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;Attn:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//SourceName"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;Loan #:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//ReferenceSourceAppNum"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;
									&lt;fo:table-row&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;Phone:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//SourcePhone"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;Lender Reference #:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//DealNum"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;
									&lt;fo:table-row&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;Fax:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//SourceFax"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;Date:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//CurrentDate"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;
									&lt;fo:table-row&gt;
										&lt;fo:table-cell border-style="none" number-columns-spanned="2" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;Insurance Reference #:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//MIPolicyNumber"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;Insurance Status:&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block text-align="start"&gt;</xsl:text><xsl:value-of select="//MIStatus"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;
								&lt;/fo:table-body&gt;
							&lt;/fo:table&gt;
							&lt;fo:block font-size="12.0pt"&gt;
								&lt;fo:leader line-height="12pt"/&gt;
							&lt;/fo:block&gt;
							&lt;fo:table table-layout="fixed"&gt;
								&lt;fo:table-column column-width="355.4pt"/&gt;
								&lt;fo:table-column column-width="184.6pt"/&gt;
								&lt;fo:table-body&gt;
									&lt;fo:table-row&gt;
										&lt;fo:table-cell background-color="#C0C0C0" border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block font-size="10.0pt" font-weight="bold" text-align="start"&gt;Conditions of Approval Outstanding For: &lt;fo:inline font-size="11pt" font-weight="normal"&gt;</xsl:text><xsl:value-of select="//ClientName"/><xsl:text>&lt;/fo:inline&gt;
											&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell background-color="#C0C0C0" border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block font-size="10.0pt" font-weight="bold" text-align="start"&gt;Closing Date: &lt;fo:inline font-size="11pt" font-weight="normal"&gt;</xsl:text><xsl:value-of select="//EstClosingDate"/><xsl:text>&lt;/fo:inline&gt;
											&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;
								&lt;/fo:table-body&gt;
							&lt;/fo:table&gt;
							&lt;fo:block font-size="12.0pt"&gt;
								&lt;fo:leader line-height="12pt"/&gt;
							&lt;/fo:block&gt;
		
					<!--  Conditions -->
							&lt;fo:table table-layout="fixed"&gt;
								&lt;fo:table-column column-width="30.4pt"/&gt;
								&lt;fo:table-column column-width="414.2pt"/&gt;
								&lt;fo:table-column column-width="95.4pt"/&gt;
								&lt;fo:table-body&gt;
									&lt;fo:table-row keep-together="always"&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt"&gt;
												&lt;fo:leader/&gt;
											&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" font-weight="bold"&gt;Documents reviewed by:&lt;/fo:block&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" font-weight="bold"&gt;</xsl:text><xsl:value-of select="//CurrentUser/Name"/><xsl:text>&lt;/fo:block&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" font-weight="bold"&gt;Phone: </xsl:text><xsl:value-of select="//CurrentUser/Phone"/><xsl:text>&lt;/fo:block&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" font-weight="bold" space-after="11pt"&gt;Fax: </xsl:text><xsl:value-of select="//CurrentUser/Fax"/><xsl:text>&lt;/fo:block&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" font-weight="bold" space-after="11pt"&gt;**ALL CONDITIONS MUST BE MET </xsl:text><xsl:value-of select="//ConditionsMetDays"/><xsl:text> BUSINESS DAYS PRIOR TO CLOSING OR COMMITMENT MAY BE CANCELLED**&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt"&gt;</xsl:text><xsl:value-of select="./Status"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;
									</xsl:text>

								<xsl:for-each select="//Conditions/Condition">
									<xsl:text>&lt;fo:table-row keep-together="always"&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt"&gt;
												&lt;fo:leader leader-pattern="rule" rule-thickness=".5pt" color="black"/&gt;
											&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
										</xsl:text>
										
											<xsl:for-each select="./Description/Line">
												<xsl:choose>
												<!--  When there is a node with text in it (non-empty), then we display it normally.  
													However, to ensure that empty nodes get a blank line rendered, we handle this in the 'otherwise' clause via fo:leader. -->
													<xsl:when test="* | text()"><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="11pt"&gt;</xsl:text><xsl:value-of select="."/><xsl:text>&lt;/fo:block&gt;</xsl:text>			
													</xsl:when>
													<xsl:otherwise><xsl:text>&lt;fo:block font-family="Arial, Helvetica" font-size="11pt"&gt;&lt;fo:leader line-height="11pt"/&gt;&lt;/fo:block&gt;</xsl:text>														</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
											
											<xsl:text>
											&lt;fo:block&gt;
												&lt;fo:leader line-height="12pt"/&gt;
											&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
										&lt;fo:table-cell border-style="none" padding-left="5.4pt" padding-right="5.4pt"&gt;
											&lt;fo:block font-family="Arial, Helvetica" font-size="11pt"&gt;</xsl:text><xsl:value-of select="./Status"/><xsl:text>&lt;/fo:block&gt;
										&lt;/fo:table-cell&gt;
									&lt;/fo:table-row&gt;</xsl:text>
								</xsl:for-each>
								
								<xsl:text>&lt;/fo:table-body&gt;
							&lt;/fo:table&gt;
							</xsl:text>
					<!--  End conditions -->
					
					<xsl:if test="//BrokerConditionFooter">
					<xsl:text>
					&lt;fo:block&gt;
						&lt;fo:leader line-height="12pt"/&gt;
					&lt;/fo:block&gt;
					&lt;fo:block&gt;
						&lt;fo:leader line-height="12pt"/&gt;
					&lt;/fo:block&gt;
					&lt;fo:block font-family="Arial, Helvetica" font-size="11pt" font-weight="bold" font-style="italic" text-decoration="underline"&gt;</xsl:text><xsl:value-of select="//BrokerConditionFooter"/><xsl:text>&lt;/fo:block&gt;
					</xsl:text>
					</xsl:if>
					
					<xsl:text>
					&lt;fo:block id="endofdoc"/&gt;
				&lt;/fo:flow&gt;
			&lt;/fo:page-sequence&gt;</xsl:text>
	</xsl:template>
	<xsl:template name="FOStart">
		<xsl:text>&lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;
		&lt;fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://xml.apache.org/fop/extensions"&gt;
			&lt;fo:layout-master-set&gt;
				&lt;fo:simple-page-master master-name="main" page-height="792.0pt" page-width="612.0pt"&gt;
					&lt;fo:region-body column-count="1" margin-bottom="72.0pt" margin-left="36.0pt" margin-right="36.0pt" margin-top="36.0pt" region-name="region-body"/&gt;
					&lt;fo:region-before extent="36.0pt" overflow="visible" padding-before="36pt" padding-left="90.0pt" padding-right="90.0pt" precedence="true" region-name="header"/&gt;
					&lt;fo:region-after extent="72.0pt" overflow="visible" padding-after="36pt" padding-left="90.0pt" padding-right="90.0pt" precedence="true" region-name="footer"/&gt;
				&lt;/fo:simple-page-master&gt;
			&lt;/fo:layout-master-set&gt;	</xsl:text>
	</xsl:template>
	<xsl:template name="FOEnd">
		<xsl:text>&lt;/fo:root&gt;</xsl:text>
	</xsl:template>
</xsl:stylesheet>
