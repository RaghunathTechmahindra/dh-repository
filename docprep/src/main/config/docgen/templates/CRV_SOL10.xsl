<!-- prepared by: Catherine Rugaizer -->
<!-- Legal size paper -->
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' >  
<xsl:output method="text"/>

<xsl:template match="/">
	<xsl:call-template name="RTFFileStart"/>

	<xsl:call-template name="DocumentHeader"/>
	<xsl:call-template name="RefNo"/>
	<xsl:call-template name="ToTable"/>
	<xsl:call-template name="InAccordance"/>
	<xsl:call-template name="NewTbl"/>
	<xsl:call-template name="WeCertify"/>
	
	<!-- #554
	<xsl:call-template name="OpinionTable"/>
	-->
	<xsl:call-template name="EnclosedDocuments"/>
	<xsl:call-template name="SolicitorSignature"/>
	
	<xsl:call-template name="RTFFileEnd"/> 	
</xsl:template>  
	
<!-- ************************************************************************ -->
<!-- templates section                                                                                -->
<!-- ************************************************************************ -->

<xsl:template name="DocumentHeader">

<!-- ====== #727 by Catherine ====== -->
  <xsl:if test="//specialRequirementTags/isAmended = 'Y'">
          <xsl:text>{\f1\fs20 AMENDED SOLICITOR INSTRUCTIONS AS OF </xsl:text>  <xsl:value-of select="//General/CurrentDate"/>
          <xsl:text>\par \par }</xsl:text>
  </xsl:if>
<!-- ====== #727 by Catherine end ====== -->

<xsl:text>{\pard\sa240
\brdrt \brdrs \brdrw10 \brsp10
\brdrl \brdrs \brdrw10 \brsp10
\brdrb \brdrs \brdrw10 \brsp10
\brdrr \brdrs \brdrw10 \brsp10
\qc\f1\fs20\line \fs28 \b\ul SOLICITOR\rquote S FINAL REPORT ON TITLE \fs20\line 
\par}</xsl:text>

</xsl:template>

<!-- ================================================  -->
<xsl:template name="RefNo">

<xsl:call-template name="RefNo_row"/>
<xsl:text>{\f1\fs24\b {Lender\rquote s Reference No. }}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs24\b {____________________}}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs24\b {}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs24\b {__________________}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>
	
<xsl:call-template name="RefNo_row"/>
<xsl:text>{\f1\fs24 }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs24\b }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs24\b }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>{\f1\fs24 {Date}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>
	
</xsl:template>

<!-- ================================================  -->
<xsl:template name="ToTable">

<xsl:call-template name="ToTable_row"/>
<xsl:text> TO:}</xsl:text>
<xsl:text>\cell \pard \intbl {\f1\fs24</xsl:text>
<xsl:value-of select="//Deal/LenderProfile/lenderName"/><xsl:text> \line</xsl:text>
<xsl:call-template name="BranchAddress"/>
<xsl:text> \line Attention: Servicing Department </xsl:text>
<!-- #554 <xsl:value-of select="concat(//specialRequirementTags/currentUser/Contact/contactFirstName,' ', //specialRequirementTags/currentUser/Contact/contactLastName)"/> -->
<xsl:text>}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

</xsl:template>

<!-- ================================================  -->
<xsl:template name="BranchAddress">
	<xsl:for-each select="//Deal/BranchProfile">
		<xsl:value-of select="./Contact/Addr/addressLine1"/><xsl:text>\line </xsl:text>
		<xsl:value-of select="./Contact/Addr/addressLine2"/><xsl:text>\line </xsl:text>
		<xsl:value-of select="./Contact/Addr/city"/><xsl:text>, </xsl:text><xsl:value-of select="./Contact/Addr/province"/><xsl:text>\line </xsl:text>
		<xsl:value-of select="concat(./Contact/Addr/postalFSA,' ', ./Contact/Addr/postalLDU)"/>
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="InAccordance">

<xsl:call-template name="Prgr_Arial_10"/>
<xsl:text>\sb240\sa180 \qj </xsl:text>
<xsl:text>In accordance with your instructions, we have acted as your solicitors in the following transaction. We have registered a Mortgage/Charge (or Deed of Loan, if applicable) the “Mortgage” on the appropriate form in the appropriate </xsl:text>
<xsl:value-of select="//specialRequirementTags/LandOfficeClause"/>
<xsl:text> and make our final report as follows:</xsl:text>
<xsl:text>\par}</xsl:text>

<!-- #554
<xsl:call-template name="Prgr_Arial_10"/>
<xsl:text>Mortgagor(s) _______________________________________________________________________________________</xsl:text>
<xsl:text>\par}</xsl:text>
		
<xsl:call-template name="Prgr_Arial_10"/>
<xsl:value-of select="//specialRequirementTags/GuarantorClause"/>
<xsl:text> ________________________________________________________________________________________</xsl:text>
<xsl:text>\par}</xsl:text>
<xsl:call-template name="Prgr_Arial_10"/>
<xsl:text>Municipal Address of Property _________________________________________________________________________</xsl:text>
<xsl:text>\par}</xsl:text>

<xsl:call-template name="Prgr_Arial_10"/>
<xsl:text>Legal Address of Property _____________________________________________________________________________</xsl:text>
<xsl:text>\par}</xsl:text>
#554 end -->

</xsl:template>

<!-- ================================================  -->
<xsl:template name="OpinionTable"> <!-- It is our opinion that:  -->

<xsl:call-template name="Prgr_Arial_10_bold"/>
<xsl:text>\sb240</xsl:text>
<xsl:text>It is our opinion that:</xsl:text>
<xsl:text>\par}</xsl:text>

<xsl:call-template name="Opinion_row"/>
<xsl:text>{\f1\fs20 (1)}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20 A valid and legally binding </xsl:text>
		<xsl:choose>
			<xsl:when test="//Deal/LenderProfile/lenderProfileId = '0'"> first </xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//Deal/lienPositionId = '0'"> first </xsl:when>	
					<xsl:otherwise> second </xsl:otherwise>
				</xsl:choose>	
			</xsl:otherwise>
		</xsl:choose>		
<xsl:text>Mortgage in favour of </xsl:text>
<xsl:value-of select="//Deal/LenderProfile/lenderName"/><xsl:text>, </xsl:text>
<xsl:text>for the full amount of the monies advanced was registered on _______________________________________ in the </xsl:text>
<xsl:value-of select="//specialRequirementTags/LandOfficeClause"/>
<xsl:text> Division of ___________________________________________________________________as Instrument No. ______________________________.}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="Opinion_row"/>
<xsl:text>{\f1\fs20 (2)}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text>
<xsl:text>\cell \pard \qj \intbl </xsl:text> 
<xsl:text>{\f1\fs20 The Mortgagor(s) have good and marketable first charge to the property free and clear of any prior encumbrances, other than the minor defects listed below which {\ul \b do not} 
affect the priority of the Mortgage or the marketability of the property. All lien holdback/ retention period requirements have been met. Easements, Encroachments and Restrictions etc. are listed below:}</xsl:text>
	<xsl:if test="//Deal/lienPositionId = '1'">A first mortgage balance shall not exceed <xsl:value-of select="//SolicitorsPackage/equityMtgLiability"/> //SolicitorsPackage/equityMtgLiability  with monthly payments of no more than </xsl:if>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="produce-empty-rows">
	<xsl:with-param name="count" select="6"/>
	<xsl:with-param name="type" select="1"/>
</xsl:call-template>

<xsl:call-template name="CondominiumTable"/>

<xsl:call-template name="Opinion_row"/>
<xsl:text>{\f1\fs20 (3)}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20 All restrictions have been complied with in full and there are no work orders or deficiency notices outstanding against the property.}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="Opinion_row"/>
<xsl:text>{\f1\fs20 (4)}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20 All taxes and levies due and payable on the property to the Municipality have been paid up to _______________.}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="Opinion_row"/>
<xsl:text>{\f1\fs20 (5)}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20 The Mortgage does not contravene the provisions of the Planning Act as amended from time to time, because the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="Opinion_row"/>
<xsl:text>{\f1\fs20 (6)}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20 A true copy of the Mortgage (including Standard Charge Terms, all Schedules to it, and the Mortgagor(s) Acknowledgement and Direction, if applicable) has been given to each Mortgagor.}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="Opinion_row"/>
<xsl:text>{\f1\fs20 (7)}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20 Fire Insurance coverage has been arranged in accordance with your instructions for full replacement cost(s) with loss payable to </xsl:text>
<xsl:value-of select="//Deal/LenderProfile/lenderName"/>
<xsl:text>, in accordance with the I.B.C. standard mortgage clause.}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="item7_row"/>
<xsl:text>{\f1\fs20 
Insurance Company:_____________________________
}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20
Broker: ___________________________________
}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="item7_row"/>
<xsl:text>{\f1\fs20 
Amount of Insurance: ____________________________
}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20
Policy No: ________________________________
}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="item7_row"/>
<xsl:text>{\f1\fs20
Effective Date: _________________________________
}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20
Expiry Date: _______________________________
}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>
<xsl:text>\page </xsl:text>
<xsl:call-template name="Prgr_Arial_10_bold"/>
<xsl:text>\sb240</xsl:text>
<xsl:text>Title Insurance, if applicable:</xsl:text>
<xsl:text>\par}</xsl:text>

<xsl:call-template name="Opinion_row"/>
<xsl:text>{\f1\fs20 (8)}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20 A lender Title Insurance policy has been purchased in accordance with your instructions.}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="item7_row"/>
<xsl:text>{\f1\fs20
Insurance Company:
}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \qj \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20
Policy No:
}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

</xsl:template>
<!-- ================================================  -->

<!-- ================================================  -->
<xsl:template name="CondominiumTable"> <!-- Condominium/strata unit(s) if applicable:  -->

<xsl:call-template name="Prgr_Arial_10_bold"/>
<xsl:text>\sb240</xsl:text>
<xsl:text>Condominium/strata unit(s) if applicable:</xsl:text>
<xsl:text>\par}</xsl:text>

<xsl:call-template name="item2a_row"/>
<xsl:text>{\f1\fs20 a)}</xsl:text>
<xsl:text>\cell \pard \qj \intbl </xsl:text>
<xsl:text>{\f1\fs20 We confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. We have reviewed the Condominium Corporation’s Declaration and Bylaws and confirm they contain nothing derogatory to your security. We have assigned the voting rights to </xsl:text>
 <xsl:value-of select="//Deal/LenderProfile/lenderName"/>
<xsl:text>, if applicable.}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="item2a_row"/>
<xsl:text>{\f1\fs20 b)}</xsl:text>
<xsl:text>\cell \pard \qj \intbl </xsl:text>
<xsl:text>{\f1\fs20 All necessary steps have been taken to confirm your right to vote should you wish to do so.}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="item2a_row"/>
<xsl:text>{\f1\fs20 c)}</xsl:text>
<xsl:text>\cell \pard \sa180 \intbl </xsl:text>
<xsl:text>{\f1\fs20 Applicable notice provisions, if any: ________________________________________________________}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

</xsl:template>

<!-- ================================================  -->
<xsl:template name="EnclosedDocuments">

<xsl:call-template name="Prgr_Arial_10_bold"/>
<xsl:text>\sb240</xsl:text>
<xsl:text>The following documents are enclosed for your file:</xsl:text>
<xsl:text>\par}</xsl:text>

<!-- row 1 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Solicitor’s Final Report on Title} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 2 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Duplicate registered copy of the Charge/Mortgage, or Deed of Loan including Standard Charge Terms, all Schedules to it and acknowledgement of receipt of Mortgagor(s) and </xsl:text>
<xsl:value-of select="//specialRequirementTags/GuarantorClause"/>
<xsl:text>, if any} </xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 3 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Electronic Charge and Acknowledgement and Direction (Ontario electronic registration counties only)} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 4 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Instrument Number (Teranet Registration)} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 4a  -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Photocopy of Borrower Identification together with the ID Verification form } 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 5 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Guarantee Agreement (Ontario electronic registration counties only)} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 6 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Certificate of Title (or Provincial Comparable)} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 7 -->
<xsl:choose>
	<xsl:when test="//Deal/Property/provinceId='11'"></xsl:when>
	<xsl:otherwise>
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Survey or Surveyor’s Certificate, if applicable} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>	
	</xsl:otherwise>
</xsl:choose>

<!-- row 7 
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Survey or Surveyor’s Certificate or Title Insurance in lieu of a Survey, if applicable} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>
-->

<!-- row 8 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Title Insurance Policy and Schedules A &amp; B to Policy} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 9 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Registered Amendment Agreement (if applicable)} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 10 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Execution Certificate} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 11-->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Municipal Tax Certificate (if applicable)} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 12 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Certificate of Completion and Possession} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 13 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 New Home Warranty Certificate of Possession} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 14 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 Occupancy Certificate/Permit} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 15 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Personal Guarantee and Letter of Independent Legal Advice} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 16 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Sheriff’s Certificate/GR Search} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 17 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Signed Statutory Declaration} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 18 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Fire Insurance Policy} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 19 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Condominium Corporation Insurance Binder and Status Certificate} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 20 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Zoning Certificate/Memorandum (Ontario properties only)} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 21 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Declaration as to Possession (Manitoba properties only)} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 22 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Registered Assignment of Rents} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 23 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ General Security Agreement (GSA) Registered under PPSA} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 24 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Statement of Funds Received and Disbursed} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 25 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Verification of Payout of Debts} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 26 -->
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text><xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>{\f1\fs20 \ Other (specify) {
______________________________________________________________________________}} 
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="produce-empty-rows">
	<xsl:with-param name="count" select="4"/>
	<xsl:with-param name="type" select="2"/>
</xsl:call-template>

</xsl:template>

<!-- ================================================  -->
<xsl:template name="NewTbl">

<!-- row 1 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Mortgagor(s)}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 2 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Covenantor}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 3 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Municipal Address of Property}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 4 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Legal Description of Property}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 5 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Mortgage Registration Number}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 6 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Mortgage Registration Date}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 7 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Insurance Company}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 8 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Insurance Policy Number}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 9 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Insurance Amount}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 10 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Insurance Effective Date}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 11 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Insurance Expiry Date}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 12 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Title Insurance Company, if applicable}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<!-- row 13 -->
<xsl:call-template name="NewTbl_row"/>
<xsl:text>{\f1\fs20 {Title Insurance Policy Number}}</xsl:text>
<xsl:text>\cell \pard\sa140 \intbl </xsl:text>
<xsl:text>{\f1\fs20 {</xsl:text>
<xsl:text>}}</xsl:text>
<xsl:text>\cell \row }</xsl:text>

</xsl:template>

<!-- ================================================  -->
<xsl:template name="BulletedParagraph">
	<xsl:text>\pard\fi-360\li720\tx720\'b7\tab\ </xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="WeCertify">

<xsl:call-template name="Prgr_Arial_10_indent"/>
<xsl:text>\par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>We certify that a true copy of the Mortgage (including Standard Charge Terms, all Schedules to it, and the Mortgagor(s) Acknowledgement and Direction, if applicable) has been executed by each Mortgagor.</xsl:text>
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>Fire Insurance coverage has been arranged in accordance with your instructions for full replacement cost(s) with loss payable to Cervus Financial Corp.</xsl:text>	
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>A lender title insurance policy has been purchase, in accordance with your instructions (if applicable).</xsl:text>	
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>If this is a new construction, an executed Certificate of Completion and New Home Warranty Certificate were obtained prior to advancing funds and are enclosed with this report.</xsl:text>	
<xsl:text>\par \par }</xsl:text>

<xsl:call-template name="Prgr_Arial_10_bold"/>
<xsl:text>\sb180</xsl:text>
<xsl:text>Condominium/strata unit(s) if applicable:</xsl:text>
<xsl:text>\par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>We confirm that the Condominium Corporation is registered and has maintained adequate Fire Insurance. We have reviewed the Condominium Corporation’s Declaration and Bylaws and confirm they contain nothing derogatory to your security. We have assigned the voting rights to Cervus Corporation, if applicable.</xsl:text>
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>All necessary steps have been taken to confirm your right to vote should you wish to do so.</xsl:text>
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>Applicable notice provisions, if any: ________________________________________________________</xsl:text>
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_bold"/>

<xsl:text>\sb180</xsl:text>
<xsl:text>It is our opinion that (non title insured transaction only):</xsl:text>
<xsl:text>\par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>The Mortgagor(s) have good and marketable first charge to the property free and clear of any prior encumbrances, other than the minor defects listed below which do not affect the priority of the Mortgage or the marketability of the property. All lien holdback/ retention period requirements have been met. Easements, Encroachments and Restrictions etc. are listed below:</xsl:text>
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="produce-empty-rows">
	<xsl:with-param name="count" select="6"/>
	<xsl:with-param name="type" select="1"/>
</xsl:call-template>

<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>All restrictions have been complied with in full and there are no work orders or deficiency notices outstanding against the property.</xsl:text>
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>All taxes and levies due and payable on the property to the Municipality have been paid up to _______________.</xsl:text>
<xsl:text>\par \par }</xsl:text>
<xsl:call-template name="Prgr_Arial_10_indent"/><xsl:call-template name="BulletedParagraph"/>
<xsl:text>The Mortgage does not contravene the provisions of the Planning Act as amended from time to time, because the Mortgagor(s) do not retain the fee or the equity of redemption in, or a power or right to grant, assign or exercise a power of appointment with respect to any land abutting the land secured by the Mortgage.</xsl:text>
<xsl:text>\par \par }</xsl:text>

</xsl:template>

<!-- ================================================  -->
<xsl:template name="SolicitorSignature">

<xsl:call-template name="Signature_row"/>
<xsl:text></xsl:text>
<xsl:text>{\f1\fs22 }</xsl:text>
<xsl:text>\cell \pard \sb360 \intbl \qr </xsl:text> 
<xsl:text>{\f1\fs22 
 {____________________________________}}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

<xsl:call-template name="Signature_row"/>
<xsl:text>{\f1\fs22    }</xsl:text>
<xsl:text>\cell \pard \intbl \qr</xsl:text> 
<xsl:text>{\f1\fs22 Signature of Solicitor}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>

</xsl:template>
<!-- ================================================  -->

<!-- ************************************************************************ -->
<!-- Row definitions 														                -->
<!-- ************************************************************************ -->
<!-- ================================================  -->
<xsl:template name="RefNo_row">
<xsl:text>{\trowd \trgaph55\ql
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx3000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx8400 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx11120
\pard \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 2 columns -->
<xsl:template name="ToTable_row">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx840
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10920
\pard \intbl {\f1\fs24</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 3 columns -->
<xsl:template name="Opinion_row">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10920
\pard \sa180 \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 2 columns -->
<xsl:template name="item2a_row">
<xsl:text>{\trowd \trgaph55\trleft720 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx1320
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10920
\pard \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 3 columns -->
<xsl:template name="item7_row">
<xsl:text>{\trowd \trgaph55\trleft720 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx5940 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6000
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10920
\pard \sa180 \intbl</xsl:text>
</xsl:template>
<!-- ================================================  -->

<xsl:template name="DealFunderName">
	<xsl:for-each select="//Deal/UserProfile">
		<xsl:if test="./userTypeId='2'">
			<xsl:value-of select="concat(./Contact/contactFirstName,' ', ./Contact/contactLastName)"/>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="produce-empty-rows">
<xsl:param name="count"/>
<xsl:param name="type"/>
<xsl:if test="$count != 0">
	<xsl:choose>
		<xsl:when test="$type = 1"><xsl:call-template name="Opinion_empty_row"/></xsl:when>
		<xsl:when test="$type = 2"><xsl:call-template name="Chk_list_empty_row"/></xsl:when>
	</xsl:choose>
	<xsl:call-template name="produce-empty-rows">
		<xsl:with-param name="count" select="$count - 1"/>
		<xsl:with-param name="type" select="$type"/>
	</xsl:call-template>
</xsl:if>
</xsl:template>

<!-- ================================================  -->
<!-- #554 -->
<!-- two-column row with border -->
<xsl:template name="NewTbl_row">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrs \brdrw10 \clbrdrl\brdrs \brdrw10 \clbrdrb\brdrs \brdrw10 \clbrdrr\brdrs \brdrw10 \cellx5600
\clbrdrt\brdrs \brdrw10 \clbrdrl\brdrs \brdrw10 \clbrdrb\brdrs \brdrw10 \clbrdrr\brdrs \brdrw10 \cellx10920
\pard \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="Opinion_empty_row">
<!-- opinion row withou space after -->
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10920
\pard \intbl</xsl:text>
<!-- end opinion row withou space after -->
<xsl:text>{\f1\fs20 }</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \intbl {\f1\fs20 __________________________________________________________________________________________}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="Chk_list_empty_row">
<xsl:call-template name="Chk_list_row2"/>
<xsl:text>{\f1\fs20 [    ]}</xsl:text>
<xsl:text>\cell \pard \intbl </xsl:text> 
<xsl:text>\cell \pard \intbl {\f1\fs20 __________________________________________________________________________________________}
</xsl:text>
<xsl:text>\cell \row }</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 3 columns, default between rows -->
<xsl:template name="Chk_list_row2">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx450
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx720
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10920 
\pard \intbl</xsl:text>
</xsl:template>

<!-- ================================================  -->
<!-- row with 2 columns -->
<xsl:template name="Signature_row">
<xsl:text>{\trowd \trgaph55\trleft-55 
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx6400
\clbrdrt\brdrnone \clbrdrl\brdrnone \clbrdrb\brdrnone \clbrdrr\brdrnone\cellx10920
\pard \intbl</xsl:text>
</xsl:template>
<!-- ================================================  -->

<!-- ************************************************************************ -->
<!-- Utility templates 														                -->
<!-- ************************************************************************ -->
<!-- ================================================  -->
<xsl:template name="HorizLine">
<xsl:text>{\pard\sa180\brdrb \brdrs \brdrw30 \brsp10 \par}</xsl:text>
</xsl:template>	

<!-- ================================================  -->
<xsl:template name="Prgr_Arial_10_bold">
<xsl:text>{\pard\sa180\f1\fs20\b
</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="Prgr_Arial_10">
<xsl:text>{\pard\sa120\f1\fs20
</xsl:text>
</xsl:template>

<!-- ================================================  -->
<xsl:template name="Prgr_Arial_10_indent">
<xsl:text>{\pard\sa240\sb180\f1\fs20
</xsl:text>
</xsl:template>

<!-- ************************************************************************ -->
<!-- rtf file start and rtf file end                                                                    -->
<!-- ************************************************************************ -->
<!-- ================================================  -->
<xsl:template name="RTFFileEnd">  
	<xsl:text>}}</xsl:text>
</xsl:template>  

<!-- ================================================  -->
<!-- legal size  (216 × 356 mm  or  612pt  x 1008pt); margings: top=36pt, bottom =36pt, left=21.6pt, right=21.6pt-->
<xsl:template name="RTFFileStart">  
<xsl:text>
{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033
{\fonttbl {\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;} {\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}}
{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}
{\info {\author Zivko Radulovic, Catherine Rutgaizer}{\operator filogix automatic report producer}{\creatim\yr2000\mo3\dy10\hr00\min00}
{\revtim\yr2000\mo3\dy00\hr00\min00}{\version1}{\edmins1}{\nofpages1}{\nofwords0}{\nofchars0}
{\*\company filogix inc}{\nofcharsws0}{\vern8247}}
\widowctrl\ftnbj\aenddoc\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1\jexpand\viewkind1\viewscale100\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl \paperw12240 \paperh20160 \margl432\margr876\margt720\margb720\deftab720 {\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}
{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}
{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \ql \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\footer \pard\plain \ql \li0\ri0\widctlpar\tqc\tx4320\tqr\tx8640\pvpara\phmrg\posxr\posy0\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\field {\*\fldinst }
{\fldrslt {\lang1024\langfe1024\noproof 1}}}
\pard \ql \li0\ri360\widctlpar\tqc\tx4320\tqr\tx8640\aspalpha\aspnum\faauto\adjustright\rin360\lin0\itap0 {\par }}
</xsl:text>
		
</xsl:template>  
	
</xsl:stylesheet>  
