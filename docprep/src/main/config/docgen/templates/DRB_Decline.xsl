<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
24/Apr/2008 DVG #DG716 FXP21233: ML 3.3 - Rate Bonus Letter not appearing for Macquarie or Citizens 
27/Nov/2006 DVG #DG548 3.2 French version of decline letter
18/Jan/2006 DVG #DG380 #2657  3.1 - The underwriter's name in the decline letter is not correct  
07/Nov/2005 DVG #DG354 Decline letter project - new
-->
<xsl:stylesheet version="2.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:java="http://xml.apache.org/xalan/java" 
  >
	<xsl:output method="text" encoding="ISO-8859-1"/>

	<xsl:variable name="lendName" select="translate(/*/Deal/LenderProfile/lenderName, '/ \\:*?&quot;&lt;&gt;\|', '')" />
	<xsl:variable name="logoFile" select="concat('Logo',concat($lendName,'.xsl'))" />
	<xsl:variable name="logoRtf" select="document($logoFile)/*/xsl:template[@name='Logo']/*" />

	<xsl:variable name="lenderName" select="/*/Deal/LenderProfile/lenderName"/>

  <!--#DG548 use DocumentTypeId parameter  
   until code is changed (if ever) to send DocumentTypeId as a parameter, let's set the default
   based on the template name (may it never change...)  
  -->	
  <!-- #DG716	
  <xsl:param name="DocumentTypeId">
   <xsl:choose>
    <xsl:when test="contains(document-location(), 'DRB_RateBonus.xsl')">110</xsl:when>
    <xsl:otherwise>67</xsl:otherwise>
   </xsl:choose>
  </xsl:param-->
  <xsl:param name="documentProfile"/>
	<xsl:variable name="DocumentTypeId" select="java:getDocumentTypeId($documentProfile)"/>
	
	<xsl:template match="/">
		<xsl:message>The logo file used is: <xsl:value-of select="$logoFile"/></xsl:message>
		<!--xsl:message>DocumentTypeId is: <xsl:value-of select="$DocumentTypeId"/></xsl:message-->

		<xsl:call-template name="RTFFileStart"/>
    <!--#DG548 add french-->		
		<xsl:choose>      
  		<xsl:when test="/*/Deal/Borrower[primaryBorrowerFlag = 'Y']/languagePreferenceId = 1">
  	    <xsl:call-template name="mainFr"/>
			</xsl:when>
			<xsl:otherwise>
  	    <xsl:call-template name="mainEn"/>
			</xsl:otherwise>
		</xsl:choose>    		
		
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- English template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<xsl:template name="mainEn">

		<xsl:text>
\trowd\trgaph108\trleft-90
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\clvmgf
\clvertalc
\cellx4680
\cellx9460

\pard\intbl\f1\fs20 </xsl:text>
		<xsl:value-of select="$logoRtf"/>
		<xsl:text>\cell \qr </xsl:text>
		<xsl:call-template name="BranchAdr"/>		
		<xsl:text>\cell\row
\pard\par

\f1\fs20 </xsl:text>

		<xsl:call-template name="BorrowersListAll"/>
		<xsl:text>\line <!--#DG548-->
\fs8\line
\fs20 </xsl:text>
		<xsl:call-template name="PrimaryBorrowerAddress"/>
		<xsl:text>\par\par

</xsl:text>
		<xsl:value-of select="/*/General/CurrentDate"/>
		<xsl:text>\par\par 

\f1\fs20 Dear </xsl:text>
		<xsl:for-each select="/*/Deal/Borrower">
			
      <xsl:if test="position() != 1">		
      	<xsl:choose>
      		<xsl:when test="position() = last()">
      		  <xsl:text> and </xsl:text>
      		</xsl:when>
      		<xsl:otherwise>
      		  <xsl:text>, </xsl:text>    		  
      		</xsl:otherwise>
      	</xsl:choose>			
			</xsl:if>
			
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
		<xsl:text>:\par\par

\ul\b RE: Deal reference number </xsl:text>
		<xsl:value-of select="/*/Deal/dealId"/>
		<xsl:text>\ulnone\b0\par\par

</xsl:text>
		<xsl:text>\f1\fs20 Thank you for your credit application to </xsl:text>
		<xsl:value-of select="$lenderName"/>
    <xsl:choose>
	    <!--#DG548 also used for rate bonus  
  		xsl:when test="/*/Deal/statusId = 24"-->
  		<xsl:when test="$DocumentTypeId = 110">  		
   			<xsl:text>. Please note that your interest rate has 
incurred a premium, above the posted rate.</xsl:text>
		  </xsl:when>
      <xsl:otherwise>
 			  <xsl:text>. After careful review of your application, we 
regret to inform you that we have declined your credit request.</xsl:text>  	    
      </xsl:otherwise>  		
		</xsl:choose>

		<xsl:text>\par\par 

This may have been due in part to information contained on your credit 
bureau. If you wish to view a copy of your credit report, please contact one or both of the 
following:\par\par

Equifax Canada Inc.\par
Consumer Relations Department\par
P.O. Box 190, Jean Talon Station\par
Montreal QC  H1S 2Z2\par
Phone: 1-800-465-7166\par\par

TransUnion Canada\par
Consumer Relations Centre\par
P.O. Box 338, LCD 1\par
Hamilton ON  L8L 7W2\par
Phone: (905) 525-0262 or 1-866-525-0262\par\par

If anything changes in your financial situation, please feel free to contact your Broker.\par\par

Sincerely,\par\par

</xsl:text>

		<xsl:for-each select="/*/Deal/UserProfile[userProfileId=/*/Deal/underwriterUserId]/Contact">
      <!--#DG548
 			<xsl:value-of select="contactFirstName"/><!- -#DG380 - ->				
   		<xsl:text> </xsl:text>
    	<xsl:if test="string-length(contactMiddleInitial)>0">
    		<xsl:value-of select="contactMiddleInitial"/>
    		<xsl:text>. </xsl:text>
    	</xsl:if>
			<xsl:value-of select="contactLastName"/-->
			<xsl:call-template name="ContactFullName"/>
   		<xsl:text> </xsl:text>
		</xsl:for-each>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text>\par\par 

{\footer\pard\qc\fs18 </xsl:text>		
		<xsl:value-of select="$lenderName"/>		
		<xsl:text>\par </xsl:text>
		<xsl:call-template name="BranchAdrFoot"/>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- French template section                                                 	   	-->
	<!-- ************************************************************************ 	-->
	
	<xsl:template name="mainFr">
		<xsl:text>
\trowd\trgaph108\trleft-90
\trpaddl108\trpaddr108\trpaddfl3\trpaddfr3\clvmgf
\clvertalc
\cellx4680
\cellx9460

\pard\intbl\f1\fs20 </xsl:text>
		<xsl:value-of select="$logoRtf"/>
		<xsl:text>\cell \qr </xsl:text>
		<xsl:call-template name="BranchAdr"/>		
		<xsl:text>\cell\row
\pard\par

\f1\fs20 </xsl:text>

		<xsl:call-template name="BorrowersListAll"/>
		<xsl:text>\line <!--#DG548--> 
\fs8\line
\fs20 </xsl:text>
		<xsl:call-template name="PrimaryBorrowerAddress"/>
		<xsl:text>\par\par

</xsl:text>
		<xsl:value-of select="/*/General/CurrentDate"/>
		<xsl:text>\par\par 

\f1\fs20 Madame,\par
Monsieur, </xsl:text>
		<!-- #DG548 xsl:for-each select="/*/Deal/Borrower">
			
      <xsl:if test="position() != 1">		
      	<xsl:choose>
      		<xsl:when test="position() = last()">
      		  <xsl:text> and </xsl:text>
      		</xsl:when>
      		<xsl:otherwise>
      		  <xsl:text>, </xsl:text>    		  
      		</xsl:otherwise>
      	</xsl:choose>			
			</xsl:if>
			
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each-->
		<xsl:text>\par\par

\ul\b OBJET: Num�ro de r�f�rence de la transaction - </xsl:text>
		<xsl:value-of select="/*/Deal/dealId"/>
		<xsl:text>\ulnone\b0\par\par

</xsl:text>
		<xsl:text>\f1\fs20 Nous vous remercions d'avoir soumis une demande de cr�dit � </xsl:text>
		<xsl:value-of select="$lenderName"/>
    <xsl:choose>
	    <!--#DG548 also used for rate bonus  
  		xsl:when test="/*/Deal/statusId = 24"-->  		
  		<xsl:when test="$DocumentTypeId = 110">  		
    			<xsl:text>. Veuillez noter que votre taux d'int�r�t a �t� major�, se situant au-dessus du 
taux affich�.</xsl:text>
			</xsl:when>
			<xsl:otherwise>
  			<xsl:text>. Nous avons r�vis� votre demande et regrettons de vous informer que votre 
demande de cr�dit a �t� refus�e.</xsl:text>  	    
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text>\par\par 

Cela pourrait d�pendre des renseignements se trouvant sur votre rapport de 
solvabilit�. Pour consulter une copie de votre rapport de solvabilit�, veuillez communiquer avec 
l'une ou les deux agences ci-dessous:\par\par

Equifax Canada Inc.\par
Relations avec les consommateurs\par
C. P.  190, succursale Jean-Talon\par
Montr�al (Qu�bec)  H1S 2Z2\par
T�l�phone : 1 800 465-7166\par\par

TransUnion Canada\par
Centre de relations aux consommateurs\par
C. P. 338, LCD 1\par
Hamilton (Ontario)  L8L 7W2\par
T�l�phone : (905) 525-0262 ou 1 866 525-0262\par\par

Si votre situation financi�re devait changer, veuillez communiquer avec votre courtier.\par\par

Veuillez agr�er nos salutations distingu�es.\par\par

</xsl:text>

		<xsl:for-each select="/*/Deal/UserProfile[userProfileId=/*/Deal/underwriterUserId]/Contact">
      <!--#DG548
			<xsl:value-of select="contactFirstName"/>				
   		<xsl:text> </xsl:text>
    	<xsl:if test="contactMiddleInitial != ''">
    		<xsl:value-of select="contactMiddleInitial"/>
    		<xsl:text>. </xsl:text>
    	</xsl:if>
			<xsl:value-of select="contactLastName"/-->				
			<xsl:call-template name="ContactFullName"/>
   		<xsl:text> </xsl:text>
		</xsl:for-each>
		<xsl:text>\par </xsl:text>
		<xsl:value-of select="$lenderName"/>
		<xsl:text>\par\par 

{\footer\pard\qc\fs18 </xsl:text>		
		<xsl:value-of select="$lenderName"/>		
		<xsl:text>\par </xsl:text>
		<xsl:call-template name="BranchAdrFoot"/>
		<xsl:text>\par }</xsl:text>
	</xsl:template>

	<!-- ================================================  -->
	<!-- Primary borrower's address on separate lines                                    -->
	<!-- ================================================  -->
	<xsl:template name="PrimaryBorrowerAddress">
		<xsl:for-each select="/*/Deal/Borrower[primaryBorrowerFlag='Y']">
			<xsl:for-each select="./BorrowerAddress[borrowerAddressTypeId='0']/Addr">
				<!-- current -->
				<!-- #DG548 use new parsed address -->
				<xsl:variable name="primAdrLine" select="/*/General/primaryBorrowerAddress"/>
      	<xsl:choose>
      		<xsl:when test="$primAdrLine">
            <xsl:value-of select="$primAdrLine" />
      		</xsl:when>
      		<xsl:otherwise>
     				<xsl:value-of select="addressLine1"/>
    				<xsl:if test="addressLine2 and string-length(addressLine2)>0">
    				  <xsl:text>\line </xsl:text>
    					<xsl:value-of select="addressLine2"/>
    				</xsl:if>
      		</xsl:otherwise>
      	</xsl:choose>
 				<xsl:text>\line </xsl:text>

				<xsl:value-of select="city"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="province"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="postalFSA"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="postalLDU"/>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowersListAll">
		<xsl:param name="separa" select="'\line '"/>
	
		<xsl:for-each select="/*/Deal/Borrower">
			<xsl:if test="position() != 1">
				<xsl:value-of select="$separa"/>
			</xsl:if>
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="/*/Deal/Borrower[borrowerTypeId=0]">
			<xsl:if test="position() != 1">, </xsl:if>
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="GuarantorNames">
		<xsl:for-each select="/*/Deal/Borrower[borrowerTypeId=1]">
			<xsl:text>, </xsl:text>
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial != ''">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>


  <!--#DG548 ================================================  -->
	<xsl:template name="ContactFullName">
		<xsl:param name="saluta" select="''"/>	
		<xsl:if test="$saluta != ''">
			<xsl:value-of select="$saluta"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="contactFirstName"/>				
		<xsl:text> </xsl:text>
		<xsl:if test="contactMiddleInitial != ''">
			<xsl:value-of select="contactMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>
  	<xsl:value-of select="contactLastName"/>
	</xsl:template>

	<!-- ================================================  -->
	<xsl:template name="PrimaryBorrowerName">
		<xsl:for-each select="/*/Deal/Borrower[primaryBorrowerFlag='Y']">
			<xsl:call-template name="BorrowerFullName"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ================================================  -->
	<!-- branch address -->
	<!-- ================================================  -->
	<xsl:template name="BranchAdr">
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">		
  		<xsl:value-of select="addressLine1"/>
  		<xsl:if test="addressLine2">
    		<xsl:text>\par </xsl:text>
    		<xsl:value-of select="addressLine2"/>
  		</xsl:if>
 			<xsl:text>,\par </xsl:text>
  		<xsl:value-of select="concat(city, ', ', province,' ',postalFSA,' ', postalLDU)"/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BranchAdrFoot">
		<xsl:for-each select="/*/Deal/BranchProfile/Contact/Addr">
  		<xsl:value-of select="addressLine1"/>
 			<xsl:text>, </xsl:text>
  		<xsl:if test="addressLine2">
    		<xsl:value-of select="addressLine2"/>
    		<xsl:text>, </xsl:text>
  		</xsl:if>
  		<xsl:value-of select="concat(city, ', ', province,' ',postalFSA,' ', postalLDU, ' ')"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ************************************************************************ 	-->
	<!-- rtf file start and rtf file end                                          			-->
	<!-- ************************************************************************ 	-->

	<xsl:template name="RTFFileStart">
		<xsl:text>{\rtf1\ansi\ansicpg1252\deff0\deflang1033\deflangfe1033
{\fonttbl{\f0\fswiss\fcharset0 Arial;}{\f1\fswiss\fprq2\fcharset0 Arial;}}
{\*\generator Msftedit 5.41.15.1507;}\viewkind4\uc1 
\margl1440\margr1440\margt720\margb720 
</xsl:text>
	</xsl:template>

</xsl:stylesheet>
