<?xml version="1.0" encoding="UTF-8"?>
<!--
  03/May/2006 DVG #DG412 #3022  CR 311  funcional spec for the CR 311 (changes to document CR 053) 
  12/Sep/2005 DVG #DG316 #2106  DJ CR114 - Ensure underwriter initial appears in signature line  
  09/Mar/2005 DVG #DG166 #1063  DJ - miscellaneous DocPrep fixes  
  03/Mar/2005 DVG #DG162 #1034  CR114 - Text in Section #2 when biweekly payment selected 
  22/Feb/2005 DVG #DG140  conform templates to jdk1.4 xalan, new xerces, fop, batik
   edited by Catherine Rutgaizer, March 2004, May 2004, ex CR053, page 2 
  28/Apr/2005 DJ - CR114, #1261m changed item #8 (En plus des conditions prévues...) 
Author Zivko Radulovic, Catherine Rutgaizer 
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:fox="http://xml.apache.org/fop/extensions">
	<!-- xmlns:xmlns="http://www.w3.org/1999/XSL/Format"-->
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1"/>
	
	<xsl:template match="/">
		<xsl:call-template name="FOStart"/>
	</xsl:template>
	
	<xsl:attribute-set name="TableLeftFixed">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="table-layout">fixed</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll2mm">
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="SpaceBA1.5Optimum">
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="PaddingAll0mm">
		<xsl:attribute name="padding-top">0mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">0mm</xsl:attribute>
		<xsl:attribute name="padding-right">0mm</xsl:attribute>
		<xsl:attribute name="padding-left">0mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainFontAttr">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="MainTextAttr_Page_2">
		<xsl:attribute name="line-height">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
		<xsl:attribute name="keep-together">always</xsl:attribute>
	</xsl:attribute-set>

	<!-- first page of this document -->
	<xsl:template name="CreatePageTwo">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
          <!--#DG412 no hard coded page break anymore -->
					Page <fo:page-number/> de <fo:page-number-citation ref-id="page_2"/> 
					<!--#DG412 Page 1 de 2 -->
        </xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLine_Page_2"/>
				<xsl:call-template name="CreateDateLines"/>
				<xsl:call-template name="CreateHeader_Page_2"/>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					<xsl:call-template name="Page_2_Item_1"/>
					<xsl:call-template name="Page_2_Item_1a"/>
					<xsl:call-template name="Page_2_Item_1b"/>
					<xsl:call-template name="Page_2_Item_1c"/>
					<xsl:call-template name="Page_2_Item_2"/>
					<xsl:call-template name="Page_2_Item_2a"/>
					<xsl:call-template name="Page_2_Item_2b"/>
					<xsl:call-template name="Page_2_Item_2b_after"/>
					<xsl:call-template name="Page_2_Item_2c"/>
					<xsl:call-template name="Page_2_Item_2c_after"/>
					<xsl:call-template name="Page_2_Item_3"/>
				</xsl:element>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
					<xsl:call-template name="Page_2_Item_4"/>
					<xsl:call-template name="Page_2_Item_4a"/>
					<xsl:call-template name="Page_2_Item_4b"/>
				</xsl:element>
				<xsl:call-template name="Page_2_Item_5"/>
				<xsl:call-template name="Page_2_Item_6"/>

        <!--#DG412 no hard coded page break anymore -->
				<xsl:call-template name="Page_2_Item_7"/>
				<xsl:call-template name="Page_2_Item_7a"/>
				<xsl:call-template name="Page_2_Item_8"/>
				<xsl:call-template name="Page_2_Item_8a"/>
				<xsl:call-template name="Page_2_Item_Ending"/>
				<fo:block id="page_2" line-height="0pt"/>
				
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreatePageTwo2">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
          <!--#DG412 no hard coded page break anymore -->
					Page <fo:page-number/> de <fo:page-number-citation ref-id="page_2"/>
					<!--#DG412 Page 2 de 2 -->
        </xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="Page_2_Item_7"/>
				<xsl:call-template name="Page_2_Item_7a"/>
				<xsl:call-template name="Page_2_Item_8"/>
				<xsl:call-template name="Page_2_Item_8a"/>
				<xsl:call-template name="Page_2_Item_Ending"/>
				<!-- <xsl:call-template name="Page_2_Item_Ending_a"/> -->
				<fo:block id="page_2" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreateDateLines">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">19cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainFontAttr"/>
							<xsl:attribute name="space-after">2em</xsl:attribute>
							<xsl:attribute name="space-before">2em</xsl:attribute>
							Le  <xsl:value-of select="//General/CurrentDate"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template name="CreatePageFour">
		<xsl:element name="fo:page-sequence">
			<xsl:attribute name="initial-page-number">1</xsl:attribute>
			<xsl:attribute name="master-reference">main</xsl:attribute>
			<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:static-content">
				<xsl:attribute name="flow-name">xsl-region-after</xsl:attribute>
				<xsl:element name="fo:block">
					<xsl:copy use-attribute-sets="MainFontAttr"/>
					<xsl:attribute name="text-align">center</xsl:attribute>
					<xsl:attribute name="space-before.optimum">4mm</xsl:attribute>
					<xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
					Page <fo:page-number/> de <fo:page-number-citation ref-id="page_4"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
				<xsl:call-template name="CreateTitleLinePage4"/>
				<xsl:call-template name="Page_4_Item_1"/>
				<xsl:call-template name="DealConditionsNotWaived"/>
				<fo:block id="page_4" line-height="0pt"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- 	
	<xsl:template name="Page_2_Item_Ending_a">
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">4.0cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">13.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text>Documents annexés :</xsl:text>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:choose>
							<xsl:when test="//Deal/MtgProd/interestTypeId = '0'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text> Déclaration de l'emprunteur : hypothèques légales de la construction</xsl:text>
					</xsl:element>
					<xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:choose>
							<xsl:when test="//Deal/MtgProd/interestTypeId = '0'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"></xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text> Consentement à priorité : hypothèques légales de la construction</xsl:text>
					</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>
	</xsl:template>
-->

	<xsl:template name="Page_2_Item_Ending">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>Copie de la présente est transmise au notaire, Me. </xsl:text>
							<xsl:choose>
								<xsl:when test="//DealAdditional/solicitorData/partyProfile/Contact/contactLastName">
									<!--#DG412 xsl:value-of select="//DealAdditional/solicitorData/partyProfile/Contact/contactFirstName"/>
                  &#160;
                  <xsl:value-of select="//DealAdditional/solicitorData/partyProfile/Contact/contactLastName"/-->
            		  <xsl:for-each select="/*/DealAdditional/solicitorData/partyProfile/Contact">
             		    <xsl:call-template name="ContactName"/>
            		  </xsl:for-each>
							    <xsl:text>.</xsl:text>
								</xsl:when>
								<xsl:otherwise> à déterminer.</xsl:otherwise>
							</xsl:choose>
							<xsl:text> <!--#DG412 -->Veuillez communiquer avec nous sans tarder si vous avez besoin de 
                renseignements additionnels ou si vous ne désirez plus obtenir cet emprunt. </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">14pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-style">italic</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<!--#DG316 add middle name 
							<xsl:value-of select="//Deal/underwriter/Contact/contactFirstName"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="//Deal/underwriter/Contact/contactLastName"/-->
							<xsl:for-each select="/*/Deal/underwriter/Contact">
								<!--#DG412 xsl:value-of select="contactFirstName"/>
								<xsl:text> </xsl:text>							
								<xsl:if test="contactMiddleInitial">
									<xsl:value-of select="contactMiddleInitial"/>
									<xsl:text> </xsl:text>
								</xsl:if>							
								<xsl:value-of select="contactLastName"/-->
         		    <xsl:call-template name="ContactName"/>
							</xsl:for-each>							
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:value-of select="//Deal/underwriter/userType"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">8pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1em</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<xsl:text>Pour un prêt hypothécaire autre que de 1er rang soumis à la Loi sur la protection du consommateur, une copie des présentes doit être annexée à l'acte notarié.</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- 
	<xsl:template name="Page_2_Item_8">	
		<xsl:element name="fo:table"><xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">0.5cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-column"><xsl:attribute name="column-width">17cm</xsl:attribute></xsl:element>
			<xsl:element name="fo:table-body"><xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text>8.</xsl:text>
					</xsl:element></xsl:element>
					<xsl:element name="fo:table-cell"><xsl:element name="fo:block"><xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text> L'acte hypothécaire devra être signé dans les 150 jours. Après ce délai, la caisse aura le droit de modifier le taux d'intérêt et les autres modalités, auquel cas elle vous transmettra une nouvelle offre de financement, ou elle pourra ne pas consentir le prêt. </xsl:text>
					</xsl:element></xsl:element>
		</xsl:element></xsl:element></xsl:element>
	</xsl:template>
-->

	<xsl:template name="Page_2_Item_8a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-left">5px</xsl:attribute>
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold"> Voir Annexe </fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_8">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:attribute name="keep-together">always</xsl:attribute>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>8.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> En plus des conditions prévues <!--#DG410 aux documents ci-joints-->au verso, 
                les conditions suivantes, qui continueront de s'appliquer malgré la signature de 
                l'acte hypothécaire, sont exigées : </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_7a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-after.optimum">2mm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">5mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">170mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="border">solid black 0.5px</xsl:attribute>
						<xsl:attribute name="padding-left">5px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:apply-templates select="//DealAdditional/Property"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/Property/legalLine1"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/Property/legalLine2"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/Property/legalLine3"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- Subject property, French formatting of address-->
	<xsl:template match="Property">
		<xsl:element name="fo:block">
			<xsl:attribute name="keep-together">always</xsl:attribute>
			<xsl:value-of select="propertyStreetNumber"/>
			<xsl:text>, </xsl:text>
			<xsl:if test="streetType">
				<xsl:value-of select="streetType"/>
				<xsl:text>  </xsl:text>
			</xsl:if>
			<xsl:value-of select="propertyStreetName"/>
			<xsl:if test="streetDirection">
				<xsl:text>  </xsl:text>
				<xsl:value-of select="streetDirection"/>
			</xsl:if>
			<xsl:if test="unitNumber">, unité <xsl:value-of select="unitNumber"/>
			</xsl:if>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyCity,', ',province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(propertyPostalFSA,' ',propertyPostalLDU)"/>
		</xsl:element>
	</xsl:template>

	<!-- ======================================================      start    ======================================================= -->
	<xsl:template name="CreateHeader_Page_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">75mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">10mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">80mm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						À :
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:call-template name="BorrowersListedBlocks"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:call-template name="PrimBorrAddress"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						De :
					</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/partyName"/>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine1"/>
						</xsl:element>
						<xsl:if test="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine2">
							<xsl:element name="fo:block">
								<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
								<xsl:attribute name="font-weight">bold</xsl:attribute>
								<fo:inline font-weight="bold">
									<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/addressLine2"/>
								</fo:inline>
							</xsl:element>
						</xsl:if>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/city"/>, <xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/province"/>
							</fo:inline>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/postalFSA"/>&#160;<xsl:value-of select="//DealAdditional/originationBranch/partyProfile/Contact/Address/postalLDU"/>
							</fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
			<xsl:attribute name="space-before.optimum">6mm</xsl:attribute>
			<xsl:text>Cher(s) membre(s),</xsl:text>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
			<xsl:attribute name="text-align">justify</xsl:attribute>
			<xsl:text>C'est avec plaisir que nous vous informons que votre demande de prêt hypothécaire de </xsl:text>
			<fo:inline font-weight="bold">
				<xsl:value-of select="//Deal/totalLoanAmount"/>
			</fo:inline>  a été acceptée avec les modalités suivantes :
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_1">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>1. </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>Le prêt portera intérêt : </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_1a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:call-template name="ConstructPage2Item1aBox"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>a)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1aText"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1aBox">
		<xsl:param name="pRate"/>
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='0'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1aText">
		<xsl:param name="pRate"/>
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='0'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text>à taux fixe : au taux de </xsl:text>
						<fo:inline font-weight="bold">
							<xsl:value-of select="//Deal/netInterestRate"/>
						</fo:inline>
						<xsl:text>  l'an, calculé semestriellement et non à l'avance;</xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text>à taux fixe: au taux de l'an, calculé semestriellement et non à l'avance;</xsl:text>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Page_2_Item_1b">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:call-template name="ConstructPage2Item1bBox"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>b)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:call-template name="ConstructPage2Item1bText"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1bBox">
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='2'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="CheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:element>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ConstructPage2Item1bText">
		<xsl:choose>
			<xsl:when test="//Deal/MtgProd/interestTypeId='2'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text> à taux variable : au taux préférentiel de la Caisse centrale Desjardins majoré de </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold">(<xsl:value-of select="//Deal/discount"/>)</fo:inline>
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/premium"/>
								</fo:inline>
							</xsl:when>
						</xsl:choose>
					  <!--#DG412 moved to next line -->
						<xsl:text> l'an.</xsl:text>
					</xsl:element>
					<fo:block>
						<xsl:text>Le taux applicable au prêt variera à chaque modification du dit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>
					</fo:block>
				</xsl:element>
			</xsl:when>
			<xsl:when test="//Deal/MtgProd/interestTypeId='3'">
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text> à taux variable : au taux préférentiel de la Caisse centrale Desjardins majoré de </xsl:text>
						<xsl:choose>
							<xsl:when test="//specialRequirementTags/discountOrPremium='DISCOUNT'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/discount"/>
								</fo:inline>
							</xsl:when>
							<xsl:when test="//specialRequirementTags/discountOrPremium='PREMIUM'">
								<fo:inline font-weight="bold">
									<xsl:value-of select="//Deal/premium"/>
								</fo:inline>
							</xsl:when>
						</xsl:choose>
					  <!--#DG412 moved to next line -->
						<xsl:text> l'an.</xsl:text>
					</xsl:element>
					<fo:block>
						<xsl:text>Le taux applicable au prêt variera à chaque modification du dit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>
					</fo:block>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="fo:table-cell">
					<xsl:element name="fo:block">
						<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:attribute name="text-align">justify</xsl:attribute>
						<xsl:text> à taux variable: au taux préférentiel de la Caisse centrale Desjardins majoré de</xsl:text>
						<xsl:call-template name="spaces"/>
					  <!--#DG412 moved to next line -->
						<xsl:text> l'an.</xsl:text>
					</xsl:element>
					<fo:block>
						<xsl:text>Le taux applicable au prêt variera à chaque modification du dit taux préférentiel et il sera calculé mensuellement et non à l'avance. </xsl:text>
					</fo:block>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="Page_2_Item_1c">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/MtgProd/interestTypeId = '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:attribute name="text-align">justify</xsl:attribute>
							<xsl:choose>
								<xsl:when test="//Deal/MtgProd/interestTypeId = '2'">
								<xsl:text>Taux maximum : Toutefois, le taux applicable au prêt ne peut à aucun moment excéder </xsl:text>
								<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/bestRate"/>
									</fo:inline> 
								<xsl:text> l'an, calculé mensuellement et non à l'avance soit </xsl:text>  
								<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/equivalentInterestRate"/>
									</fo:inline>
									<xsl:text> l'an calculé semestriellement et non à l'avance. </xsl:text>
							</xsl:when>
								<xsl:otherwise>
									<xsl:text>Taux maximum: Toutefois, le taux applicable au prêt ne peut à aucun moment excéder</xsl:text>
									<xsl:call-template name="spaces"/>
									<xsl:text>l'an, calculé mensuellement et non à l'avance soit</xsl:text>
									<xsl:call-template name="spaces"/>
									<xsl:text>l'an calculé semestriellement et non à l'avance. </xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>2. </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
						<xsl:text>Le remboursement s'effectuera au moyen de paiements périodiques, constitués de capital et d'intérêts, égaux et consécutifs de </xsl:text>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//Deal/PandiPaymentAmount"/>
							</fo:inline> chacun, le premier paiement devant être fait le 
							<xsl:choose>
								<xsl:when test="//Deal/paymentFrequencyId='0'">
									<fo:inline font-weight="bold">
										<xsl:text> 30ème jour suivant le déboursé du prêt </xsl:text>
									</fo:inline>
										<xsl:text> et les autres successivement le </xsl:text>
									<fo:inline font-weight="bold">
										<xsl:text> même  jour </xsl:text>
									</fo:inline>
										<xsl:text> de chaque </xsl:text>
									<fo:inline font-weight="bold"> mois.</fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId='2'">
									<!--#DG162 fo:inline font-weight="bold"> 14 jours suivant le déboursé du prêt </fo:inline> et les autres successivement le -->
									<fo:inline font-weight="bold"><xsl:text> 14ème jour suivant le déboursé du prêt </xsl:text></fo:inline>
									<xsl:text> et les autres successivement le </xsl:text><!--#DG162 -->
									<fo:inline font-weight="bold"> même  jour </fo:inline>
									<xsl:text> de chaque </xsl:text>
									<fo:inline font-weight="bold">
									<xsl:text> deux semaines.</xsl:text></fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/paymentFrequencyId='4'">
									<fo:inline font-weight="bold"><xsl:text> 7ème jour suivant le déboursé du prêt </xsl:text></fo:inline>
									<xsl:text> et les autres successivement le </xsl:text><!--#DG162 --><!-- #DG166 -->
									<fo:inline font-weight="bold"><xsl:text> même  jour </xsl:text></fo:inline><xsl:text> de chaque </xsl:text>
									<fo:inline font-weight="bold"><xsl:text> semaine.</xsl:text></fo:inline>
								</xsl:when>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<!-- 
						<xsl:choose>
							<xsl:when test="//Deal/interestTypeId = '0'">
								<xsl:call-template name="CheckedCheckbox"></xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
							</xsl:otherwise>
						</xsl:choose>
						-->
							<xsl:call-template name="UnCheckedCheckbox"/>
							<!-- never check -->
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> a)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>jusqu'au</xsl:text>
							<xsl:call-template name="spaces"/>
							<xsl:text>inclusivement, date à laquelle tout solde alors dû deviendra exigible;</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2b">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId= '0'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> b)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>jusqu'à l'échéance d'un terme de</xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId= '0'">
									<xsl:text> </xsl:text>
									<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/actualPaymentTerm"/> mois</fo:inline>
									<xsl:text> </xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="spaces"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text><!--#DG412 -->commençant à la date de signature de l'acte hypothécaire, tout solde dû devenant exigible à l'échéance de ce terme;</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2b_after">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<fo:inline font-weight="bold">Dans le cas d'un prêt assuré par</fo:inline>
							<xsl:call-template name="spaces"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId='1'">
									<xsl:call-template name="CheckedCheckbox"/>
									<fo:inline font-weight="bold">&#160; la Société canadienne d'hypothèques et de logement </fo:inline>
									<xsl:call-template name="spaces"/>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:when>
								<xsl:when test="//Deal/mortgageInsurerId='2'">
									<xsl:call-template name="UnCheckedCheckbox"/>
									<fo:inline font-weight="bold">&#160; la Société canadienne d'hypothèques et de logement </fo:inline>
									<xsl:call-template name="spaces"/>
									<xsl:call-template name="CheckedCheckbox"/>
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<fo:inline font-weight="bold">&#160; la Société canadienne d'hypothèques et de logement </fo:inline>
									<xsl:call-template name="spaces"/>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<fo:inline font-weight="bold">&#160; GE CAPITAL </fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2c">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/mortgageInsurerId= '1' or //Deal/mortgageInsurerId= '2'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> c)</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						jusqu'à l'échéance d'un terme de <fo:inline font-weight="bold">
								<xsl:choose>
									<xsl:when test="//Deal/mortgageInsurerId= '1' or //Deal/mortgageInsurerId= '2'">
										<xsl:value-of select="//Deal/actualPaymentTerm"/> mois 
								</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="spaces"/>
									</xsl:otherwise>
								</xsl:choose>
							</fo:inline>
						<xsl:text> commençant à la date d'ajustement des intérêts, soit le <!--#DG412 -->jour </xsl:text>
						<fo:inline font-weight="bold"><xsl:text>du déboursé du prêt </xsl:text></fo:inline>
						<xsl:text> tout solde dû devenant exigible à l'échéance de ce terme.</xsl:text>
				</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_2c_after">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:attribute name="space-before.optimum">0.5cm</xsl:attribute>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>Le montant des paiements est basé sur une période d'amortissement de </xsl:text>
							<fo:inline font-weight="bold">
								<xsl:value-of select="//specialRequirementTags/amortizationTermProcessed/termNumber"> </xsl:value-of>
								<xsl:choose>
									<xsl:when test="//specialRequirementTags/amortizationTermProcessed/termMeasure='MONTHS'"> mois. </xsl:when>
									<xsl:when test="//specialRequirementTags/amortizationTermProcessed/termMeasure='YEARS'"> an(s). </xsl:when>
								</xsl:choose>
							</fo:inline>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_3">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">16.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>3.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:choose>
								<xsl:when test="//Deal/proprietairePlus='Y'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>Une ouverture de crédit de </xsl:text>
						<xsl:choose>
								<xsl:when test="//Deal/proprietairePlus='Y'">
									<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/proprietairePlusLOC"/>
									</fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="spaces"/>
								</xsl:otherwise>
							</xsl:choose>						
							<xsl:text> vous sera également accordée aux fins de la clause "Protection propriétaire".</xsl:text>
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_4">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>4.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text>Malgré les modalités précitées qui paraîtront à l'acte hypothécaire, si vous adhérez à</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_4b">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:text> </xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
						<xsl:text> les paiements périodiques que vous devrez effectuer s'élèveront à </xsl:text>
						<xsl:choose>
								<xsl:when test="//specialRequirementTags/OrderedBorrowers/Borrower[lifeStatusId= '1']">
									<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/pmntPlusLifeDisability"/>
									</fo:inline>, 
							</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="spaces"/>
								</xsl:otherwise>
							</xsl:choose>							
						<xsl:text> afin d'inclure la prime de cette ou de ces assurances, qui correspond à un taux d'intérêt annuel additionnel de </xsl:text>
						<xsl:choose>
								<xsl:when test="//specialRequirementTags/OrderedBorrowers/Borrower[lifeStatusId= '1']">
									<fo:inline font-weight="bold">&#160;<xsl:value-of select="//Deal/interestRateIncrement"/>, &#160;</fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="spaces"/>
								</xsl:otherwise>
							</xsl:choose>
						<xsl:text> le tout sous réserve des dispositions de la police d'assurance en vigueur à la caisse. </xsl:text>
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_4a">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">8.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell"/>
					<xsl:choose>
						<xsl:when test="//specialRequirementTags/OrderedBorrowers/Borrower[lifeStatusId= '1' and disabilityStatusId= '1']">
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<xsl:text> Assurance vie seulement</xsl:text>
								</xsl:element>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
									<xsl:call-template name="CheckedCheckbox"/>
									<xsl:text> Assurance vie et Assurance invalidité </xsl:text>
								</xsl:element>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
									<xsl:call-template name="BorrowersLifeBox"/>
									<xsl:text> Assurance vie seulement</xsl:text>
								</xsl:element>
							</xsl:element>
							<xsl:element name="fo:table-cell">
								<xsl:element name="fo:block">
									<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
									<xsl:call-template name="UnCheckedCheckbox"/>
									<xsl:text> Assurance vie et Assurance invalidité </xsl:text>
								</xsl:element>
							</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="BorrowersLifeBox">
		<xsl:choose>
			<xsl:when test="//specialRequirementTags/OrderedBorrowers/Borrower[(disabilityStatusId= '2' or disabilityStatusId= '3') and lifeStatusId= '1']">
				<xsl:call-template name="CheckedCheckbox"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="UnCheckedCheckbox"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--<xsl:template name="CreatePageTwo">
	<xsl:element name="fo:page-sequence">
		<xsl:attribute name="master-reference">main</xsl:attribute>
		<xsl:attribute name="language">en</xsl:attribute>
			<xsl:element name="fo:flow">
				<xsl:attribute name="flow-name">xsl-region-body</xsl:attribute>
					<xsl:call-template name="CreatePageTwoTopLine"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoTable"></xsl:call-template>	
					<xsl:call-template name="CreatePageTwoCCLine"></xsl:call-template>
			</xsl:element>
	</xsl:element>
</xsl:template> -->

	<xsl:template name="CreatePageTwoCCLine">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:attribute name="padding-top">2mm</xsl:attribute>
						<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
						<xsl:attribute name="padding-right">2mm</xsl:attribute>
						<xsl:attribute name="padding-left">2mm</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-family">Arial, Helvetica</xsl:attribute>
							<xsl:attribute name="font-size">10pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>
							<!--#DG412 xsl:value-of select="concat('cc:    ', //Deal/sourceOfBusinessProfile/Contact/contactFirstName, '   ', //Deal/sourceOfBusinessProfile/Contact/contactLastName , '  représentant(e) hypothécaire ' )"/-->
							<xsl:text>cc:    </xsl:text>
        		  <xsl:for-each select="/*/Deal/SourceOfBusinessProfile/Contact">
         		    <xsl:call-template name="ContactName"/>
        		  </xsl:for-each>
              <xsl:text>  représentant(e) hypothécaire </xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="CreateTitleLine_Page_2">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<!-- <xsl:attribute name="space-before">0.7in</xsl:attribute> -->
			<!-- <xsl:attribute name="break-before">page</xsl:attribute> -->
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">1cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">14cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll0mm"/>
						<xsl:element name="fo:block"/>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:copy use-attribute-sets="PaddingAll2mm"/>
						<xsl:attribute name="padding-left">0mm</xsl:attribute>
						<xsl:attribute name="text-align">center</xsl:attribute>
						<xsl:attribute name="border">solid black 1px</xsl:attribute>
						<xsl:element name="fo:block">
							<xsl:attribute name="line-height">12pt</xsl:attribute>
							<xsl:attribute name="font-weight">bold</xsl:attribute>
							<xsl:attribute name="font-family">Times Roman</xsl:attribute>
							<xsl:attribute name="font-size">14pt</xsl:attribute>
							<xsl:attribute name="space-before.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="space-after.optimum">1.5pt</xsl:attribute>
							<xsl:attribute name="keep-together">always</xsl:attribute>	
							<xsl:text> Offre de financement - Hypothèque immobilière </xsl:text>
							<xsl:value-of select="//Deal/dealId"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_5">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>5.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text> Le prêt sera remboursable par anticipation </xsl:text>
							<xsl:call-template name="spaces"/>
							<xsl:choose>
								<xsl:when test="//Deal/prePaymentOptionsId= '6'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Sans indemnité &#160;&#160;</xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/prePaymentOptionsId= '3'">
									<xsl:call-template name="CheckedCheckbox"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="UnCheckedCheckbox"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> Avec indemnité </xsl:text>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							 si les montants payés par anticipation excèdent 
							 <fo:inline font-weight="bold">
								<xsl:text> </xsl:text>
								<xsl:value-of select="//Deal/privilegePaymentDescription"/>
								<xsl:text> </xsl:text>
							</fo:inline> 
							<xsl:text> du montant initial du prêt conformément à l'acte hypothécaire.</xsl:text>
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_6">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>6.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<fo:inline font-weight="bold">(À remplir uniquement pour un prêt hypothécaire autre que de premier rang à un consommateur). </fo:inline>
						</xsl:element>
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<!--#DG412 moved up on its own line fo:inline font-weight="bold">(À remplir uniquement pour un prêt hypothécaire autre que de premier rang à un consommateur). </fo:inline-->
							<xsl:text>En incluant les primes de l'assurance vie et de l'assurance invalidité si vous adhérez à ces Assurances, le taux de crédit s'élèvera à </xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/lienPositionId='0'">
									<xsl:call-template name="spaces"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/interestRateIncLifeDisability"/>
									</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> l'an et les frais de crédit pour toute la durée du contrat (terme) s'élèveront à </xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/lienPositionId='0'">
									<xsl:call-template name="spaces"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/totalCreditCost"/>
									</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> soit </xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/lienPositionId='0'">
									<xsl:call-template name="spaces"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/intWithoutInsCreditCost"/>
									</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> pour l'intérêt et </xsl:text>
							<xsl:choose>
								<xsl:when test="//Deal/lienPositionId='0'">
									<xsl:call-template name="spaces"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline font-weight="bold">
										<xsl:value-of select="//Deal/addCreditCostInsIncurred"/>
									</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> pour l'assurance. Si le taux d'intérêt est variable, le taux de crédit indiqué ci-dessus est le taux qui serait en vigueur en date des présentes, conformément au paragraphe 1b) plus haut, et les frais de crédit sont fournis à titre indicatif sur la base dudit taux de crédit et sont susceptibles de varier selon les fluctuations du taux d'intérêt.</xsl:text>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template name="Page_2_Item_7">
		<xsl:element name="fo:table">
			<xsl:copy use-attribute-sets="TableLeftFixed"/>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">0.5cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-column">
				<xsl:attribute name="column-width">17cm</xsl:attribute>
			</xsl:element>
			<xsl:element name="fo:table-body">
				<xsl:attribute name="text-align">justify</xsl:attribute>
				<xsl:element name="fo:table-row">
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
							<xsl:text>7.</xsl:text>
						</xsl:element>
					</xsl:element>
					<xsl:element name="fo:table-cell">
						<xsl:element name="fo:block">
							<xsl:copy use-attribute-sets="MainTextAttr_Page_2"/>
						<xsl:text> Le prêt et l'ouverture de crédit, le cas échéant, doivent être garantis par une hypothèque de </xsl:text>
						<xsl:choose>
								<xsl:when test="//Deal/lienPositionId='0'">
									<fo:inline font-weight="bold"> 1</fo:inline>
									<fo:inline font-weight="bold" font-size="8" baseline-shift="super">er </fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline font-weight="bold"> 2</fo:inline>
									<fo:inline font-weight="bold" font-size="8" baseline-shift="super">ème </fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						<xsl:text> rang sur l'immeuble situé à l'adresse suivante :</xsl:text>
					</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- =========================================================== Utility tempates ===================================================== -->
	<xsl:template name="BorrowersListedBlocks">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower[borrowerTypeId = '0']">
			<xsl:element name="fo:block">
		    <xsl:call-template name="BorrowerFullName"/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BorrowerFullName">
		<xsl:if test="salutation">
			<xsl:value-of select="salutation"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerFirstName"/>
		<xsl:text> </xsl:text>
		<xsl:if test="borrowerMiddleInitial">
			<xsl:value-of select="borrowerMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>
		<xsl:value-of select="borrowerLastName"/>
	</xsl:template>

	<!-- #DG412 ================================================  -->
	<xsl:template name="ContactName">
		<xsl:param name="saluta" select="''"/>	
		<xsl:if test="$saluta != ''">
			<xsl:value-of select="$saluta"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="contactFirstName"/>
		<xsl:text> </xsl:text>							
		<xsl:if test="contactMiddleInitial!= ''">
			<xsl:value-of select="contactMiddleInitial"/>
			<xsl:text>. </xsl:text>
		</xsl:if>							
		<xsl:value-of select="contactLastName"/>
	</xsl:template>

	<xsl:template name="BorrAddress">
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine1"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="./Address/addressLine2"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/city,', ',./Address/province)"/>
		</xsl:element>
		<xsl:element name="fo:block">
			<xsl:value-of select="concat(./Address/postalFSA,'  ', ./Address/postalLDU)"/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="PrimBorrAddress">
		<xsl:for-each select="//specialRequirementTags/OrderedBorrowers/Borrower">
			<xsl:if test="position()=1">
				<xsl:for-each select="BorrowerAddress">
					<xsl:if test="borrowerAddressTypeId='0'">
						<xsl:call-template name="BorrAddress"/>
						<!-- #DG140 text>some text</text-->
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="CheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
				<line x1="0" y1="0" x2="9" y2="9" style="stroke:black;"/>
				<line x1="0" y1="9" x2="9" y2="0" style="stroke:black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<xsl:template name="UnCheckedCheckbox">
		<fo:instream-foreign-object width="9px" height="9px">
			<svg xmlns="http://www.w3.org/2000/svg" width="9px" height="9px">
				<rect x="0" y="0" width="9" height="9" style="fill: none; stroke: black;"/>
			</svg>
		</fo:instream-foreign-object>
	</xsl:template>

	<xsl:template name="mortgageInsuranceCheckBox">
		<xsl:param name="parInsurer"/>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:call-template name="CheckedCheckbox"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="UnCheckedCheckbox"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="UnCheckedCheckbox"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="UnCheckedCheckbox"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="mortgageInsurancePolicyNum">
		<xsl:param name="parInsurer"/>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$parInsurer='1'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$parInsurer='2'">
				<xsl:choose>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPolicyNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="mortgageInsurancePremiumAmount">
		<xsl:param name="parInsurer">99</xsl:param>
		<xsl:param name="parId">0</xsl:param>
		<xsl:param name="parStatus">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$parInsurer='99'">
				<xsl:choose>
					<xsl:when test="$parId='1'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$parId='2'">
						<xsl:choose>
							<xsl:when test="$parStatus='15'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='16'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:when test="$parStatus='23'">
								<xsl:value-of select="//Deal/MIPremiumAmount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="spaces">
		<xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
	</xsl:template>

	<!-- =========================================================== FOStart ===================================================== -->
	<xsl:template name="FOStart">
		<!-- #DG140 xsl:element name="?xml">
			<xsl:attribute name="version">1.0</xsl:attribute>
			<xsl:attribute name="encoding">ISO-8859-1</xsl:attribute>
		</xsl:element-->
		<xsl:element name="fo:root">
			<!-- #DG140 xsl:attribute name="xmlns:fo">http://www.w3.org/1999/XSL/Format</xsl:attribute>
			<xsl:attribute name="xmlns:fox">http://xml.apache.org/fop/extensions</xsl:attribute-->
			<xsl:element name="fo:layout-master-set">
				<xsl:element name="fo:simple-page-master">
					<xsl:attribute name="master-name">main</xsl:attribute>
					<xsl:attribute name="page-height">11in</xsl:attribute>
					<xsl:attribute name="page-width">8.5in</xsl:attribute>
					<xsl:attribute name="margin-top">0in</xsl:attribute>
					<xsl:attribute name="margin-bottom">0in</xsl:attribute>
					<xsl:attribute name="margin-left">0in</xsl:attribute>
					<xsl:attribute name="margin-right">0in</xsl:attribute>
					<xsl:element name="fo:region-before">
						<xsl:attribute name="extent">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-body">
						<xsl:attribute name="margin-bottom">25mm</xsl:attribute>
						<xsl:attribute name="margin-top">15mm</xsl:attribute>
						<xsl:attribute name="margin-left">25mm</xsl:attribute>
						<xsl:attribute name="margin-right">15.9mm</xsl:attribute>
					</xsl:element>
					<xsl:element name="fo:region-after">
						<xsl:attribute name="extent">10mm</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:call-template name="CreatePageTwo"/>
			<!--#DG412 xsl:call-template name="CreatePageTwo2"/-->
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
