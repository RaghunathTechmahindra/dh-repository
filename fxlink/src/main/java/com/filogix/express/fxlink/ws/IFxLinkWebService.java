/*
 * @(#)IFxLinkWebService.java     June 18, 2009
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink.ws;

import com.filogix.express.fxlink.WSResponseBean;
import com.filogix.express.fxlink.WSRetryableException;

/**
 * this interface defines FxLink web services.
 * @author Hiro
 * @since 4.2
 * @version 1.0, June 18, 2009
 */
public interface IFxLinkWebService {
	
	/**
	 * this method executes getMessage service in FxLink Server.
	 * when fatal error happens, this method throws RuntimeException or its subclass.
	 * and WSRetryableException is thrown when FxLink throws any exception.
	 * WSRetryableException will wrap the cause exception.
	 * 
	 * @return respose bean object 
	 * @throws WSRetryableException
	 */
	public WSResponseBean getMessage() throws WSRetryableException;

	/**
	 * this method executes acknowlage service in FxLink Server.
	 * when fatal error happens, this method throws RuntimeException or its subclass.
	 * and WSRetryableException is thrown when FxLink throws any exception.
	 * WSRetryableException will wrap the cause exception.
	 *  
	 * @throws WSRetryableException 
	 */
	public void acknowledge(String messageId, String statusId) throws WSRetryableException;
	
	/**
	 * Initialise required configurations
	 * @throws WSRetryableException
	 */
	public void init() throws WSRetryableException;
}
