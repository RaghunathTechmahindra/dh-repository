/*
 * @(#)FxLinkClientMain.java    2009-6-23
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink;


import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.FCXLoanAppQueue;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.fxlink.ws.IFxLinkWebService;

/**
 * the main class of the fxLink Client
 * @version 1.0 initial June 23, 2009, Hiro
 */
public class FxLinkClientMain {

	private final static Logger logger = 
		LoggerFactory.getLogger(FxLinkClientMain.class);

	private int sleepTimeNoData;
	private int sleepTimeOnError;
	private int retryMax;
	private int retryAttempt;
	private boolean stopped;

	/**
	 * Initialise fields
	 */
	private void init(){

		ResourceManager.init();
		PicklistData.init();
		new BXResources(); //Initialising BXResources

		PropertiesCache prop = PropertiesCache.getInstance();

		sleepTimeNoData = Integer.parseInt(prop.getProperty(
				Const.DEFAULT_INSTITUTION,
				Const.PROPERTY_KEY_SLEEP_TIME_ON_NODATA, 
				Const.DEFAULT_SLEEP_TIME_ON_NODATA));
		logger.info("sleepTimeNoData =" + sleepTimeNoData);
		sleepTimeOnError = Integer.parseInt(prop.getProperty(
				Const.DEFAULT_INSTITUTION,
				Const.PROPERTY_KEY_SLEEP_TIME_ON_ERROR, 
				Const.DEFAULT_SLEEP_TIME_ON_ERROR));
		logger.info("sleepTimeOnError =" + sleepTimeOnError);
		retryMax =  Integer.parseInt(prop.getProperty(
				Const.DEFAULT_INSTITUTION,
				Const.PROPERTY_KEY_RETRY_MAX, 
				Const.DEFAULT_MAX_RETRY));
		logger.info("retryMax =" + retryMax);
		this.retryAttempt = 0;
		this.stopped = false;
	}

	/**
	 * this method saves response from FxLink to ArchivedFCXLoanApp table.
	 * @param bean
	 * @param srk
	 */
	void archiveData(WSResponseBean bean, SessionResourceKit srk) {

		StringBuilder sb = new StringBuilder();
		sb.append("===== Recieved FCX Loan Application Data :");
		sb.append(" sourceApplicationId = ").append(bean.getSourceApplicationId());
		sb.append(" ,messageId = ").append(bean.getMessageId());
		logger.info(sb.toString());
		try {

			String message = new String(bean.getDocumentContent(),
					Const.FXLINK_ENCORDING_CHARSET);
			
			if(logger.isInfoEnabled()){
				logger.info("FCX ======================================== ");
				logger.info("messageId = " + bean.getMessageId());
				logger.info("sourceApplicationId = " + bean.getSourceApplicationId());
				logger.info("\r\n" + message);
				logger.info("FCX ======================================== ");
			}

			// insert record to FCXLoanAppQueue table
			FCXLoanAppQueue  queue = new FCXLoanAppQueue(srk);
			queue.create(bean.getMessageId(), bean.getSender(), bean
					.getReceiver(), bean.getSourceApplicationId(), bean
					.getFormat(), bean.getVersion(), new Date(), message,
					Const.ARCHFCXLOANSTATUSID_RECEIVED,
					Const.RETRY_COUNTER_START);
			queue.ejbStore();

			sb = new StringBuilder();
			sb.append("Stored data into FCXLoanAppQueue :");
			sb.append(" FcxLoanAppQueueId = ").append(queue.getFcxLoanAppQueueId());
			logger.info(sb.toString());

		} catch (Exception e) {
			ExceptionHandler.handleFatal(e,
					"Faild to insert data into FCXLoanAppQueue. "
					+ "MessageId = " + bean.getMessageId()
					+ ", DealId(SoruceAppId) = " + bean.getSourceApplicationId());
		}
	}

	/**
	 * This is the start point of fxLink Client.
	 * this method executes services of FxLink Server, stores data, handles errors, and etc.
	 */
	void execute(){

		init();

		SessionResourceKit srk = null;
		try {
			srk = new SessionResourceKit();

			IFxLinkWebService fxlink = (IFxLinkWebService)
			ExpressFxLinkContext.getService("fxLinkWebService");
			fxlink.init();

			logger.info("========== FxLink Client started ==========");
			while(stopped == false){
				WSResponseBean response = null;
				try{
					logger.debug("seding getMessage request");
					response = fxlink.getMessage();
					if(checkRecieve(response) == true) {
						logger.debug("received message");
						srk.beginTransaction();
						logger.debug("archiving data");
						archiveData(response, srk);
						//return always OK ...
						logger.debug("sending acknowlege request");
						fxlink.acknowledge(response.getMessageId(), 
								Const.FXLINK_RESPONSE_CODE_OK);
						srk.commitTransaction();
					} else {
						// Not to exhaust DB connection, clear DB connection
						srk.freeResources(); 
						Thread.sleep(sleepTimeNoData);
					}

					//ran successfully
					retryAttempt = 0;

				} catch (WSRetryableException e) {
					srk.cleanTransaction();
					sleepAndRetry(e);
					fxlink.init();
				}
			}
		} catch (Throwable fatal) {
			srk.cleanTransaction();
			logger.error("critical error was detected!", fatal);
			FxLinkUtils.sendAlertEmail(fatal);

		} finally {
			srk.freeResources();
		}
		logger.info("=== fxLink Client is stopping... ===");
	}

	/**
	 * this method is called when a retryable error occurs.
	 * when <b>retryAttempt</b> is less than <b>retryMax</b>, 
	 * this method just sleeps <b>sleepTimeOnError</b> time, 
	 * and retries.
	 * if <b>retryAttempt</b> reaches the maximum, this method
	 * sends an alert email and stops whole process setting 
	 * true to stopped flag.
	 * @param e
	 */
	private void sleepAndRetry(WSRetryableException e){

		logger.error("a connection error was cought", e);
		if(retryMax < ++retryAttempt){
			logger.error("retry attempt reached max");
			stopped = true;
			FxLinkUtils.sendAlertEmail(e);
			return;
		}

		logger.error("start sleeping ... (" + sleepTimeOnError + " milliseconds)");
		try {
			Thread.sleep(sleepTimeOnError);
		} catch (InterruptedException e1) {
			ExceptionHandler.handleFatal(
					e1, "exception occured during thread.sleep");
		}

		logger.error("work up from sleep. retrying : "
				+ retryAttempt + "/" + retryMax);
	}

	/**
	 * checks if the response contains data
	 * @param bean
	 * @return true - recieved data
	 */
	boolean checkRecieve(WSResponseBean bean){
		if(bean == null) return false;
		if(bean.getDocumentContent() == null) return false;
		return (bean.getDocumentContent().length > 0);
	}


	/**
	 * the main method of fxLink.
	 * @param args
	 */
	public static void main(String[] args) {
		FxLinkClientMain fxlinkClient = new FxLinkClientMain();
		fxlinkClient.execute();
	}

}
