/*
 * @(#)WSRetryableException.java    2009-6-23
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink;

import com.filogix.express.core.ExpressException;

public class WSRetryableException extends ExpressException {

	private static final long serialVersionUID = 1L;

	public WSRetryableException() {
	}

	public WSRetryableException(String message) {
		super(message);
	}

	public WSRetryableException(Throwable cause) {
		super(cause);
	}

	public WSRetryableException(String message, Throwable cause) {
		super(message, cause);
	}

}
