/*
 * @(#)ExceptionHandler.java    2009-6-23
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink;

import org.apache.axis2.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.core.ExpressRuntimeException;

/**
 * helper class for error handling
 * 
 * @author Hiro
 * @version 1.0 initial June 23, 2009
 */
public class ExceptionHandler {

	private final static Logger logger = LoggerFactory
	.getLogger(ExceptionHandler.class);

	/**
	 * this method takes any exception, prepends message, wraps the exception in
	 * ExpressRuntimeException, and throws it. in Fxlink client, any runtime
	 * exception will be handled as fatal error.
	 * 
	 * @param e
	 *            exception
	 * @param msg
	 *            message that will be prepended to the exception
	 */
	public static void handleFatal(Exception e, String msg) {
		logger.error(msg, e);
		throw new ExpressRuntimeException(msg, e);
	}

	/**
	 * this method takes any exception from FxLink WS, prepends message, wraps
	 * the exception in WSRetryableException, and throws it. in Fxlink client,
	 * WSRetryableException will be handled as retry-able exception.
	 * 
	 * @param e
	 *            exception
	 * @param msg
	 *            message that will be prepended to the exception
	 */
	public static void handleWebServieException(Exception e, String msg)
	throws WSRetryableException {

		if (e instanceof AxisFault) {
			AxisFault af = (AxisFault) e;
			logger.error("AxisFault.getCause()=" + af.getCause());
			logger.error("AxisFault.getFaultAction()=" + af.getFaultAction());
			logger.error("AxisFault.getFaultNode()=" + af.getFaultNode());
			logger.error("AxisFault.getFaultRole()=" + af.getFaultRole());
			logger.error("AxisFault.getFaultType()=" + af.getFaultType());
			logger.error("AxisFault.getLocalizedMessage()=" + af.getLocalizedMessage());
			logger.error("AxisFault.getMessage()=" + af.getMessage());
			logger.error("AxisFault.getReason()=" + af.getReason());
			logger.error("AxisFault.getDetail()=" + af.getDetail());
			logger.error("AxisFault.getFaultCode()=" + af.getFaultCode());
			logger.error("AxisFault.getFaultCodeElement()=" + af.getFaultCodeElement());
		}
		logger.error(msg, e);
		throw new WSRetryableException(msg, e);
	}

}
