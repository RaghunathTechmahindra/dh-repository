package com.filogix.express.fxlink.ws;

import java.util.ArrayList;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.fxlink.ExceptionHandler;
import com.filogix.express.fxlink.WSResponseBean;
import com.filogix.express.fxlink.WSRetryableException;

public class FxLinkWebServiceAxis2AXIOMImpl extends AbstractFxLinkWebService {
	
	private final static Logger logger = 
		LoggerFactory.getLogger(FxLinkWebServiceAxis2AXIOMImpl.class);
	
	private ServiceClient sender;
	
	@Override
	protected void prepareService() throws WSRetryableException {
		
		EndpointReference targetEPR = new EndpointReference(getEndPoint());
		
		Options options = new Options();
		options.setTo(targetEPR);
		options.setTransportInProtocol(Constants.TRANSPORT_HTTP);

		options.setTimeOutInMilliSeconds(getTimeout());
		options.setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);

		ArrayList<String> authSchemes = new ArrayList<String>();
		authSchemes.add(HttpTransportProperties.Authenticator.BASIC);
	       
		HttpTransportProperties.Authenticator
		auth = new HttpTransportProperties.Authenticator();
		
		auth.setUsername(getChannel().getUserId());
		auth.setPassword(getChannel().getPassword());
		auth.setAuthSchemes(authSchemes);
		auth.setPreemptiveAuthentication(true);
		options.setProperty(HTTPConstants.AUTHENTICATE, auth);
		
		try {
			sender = new ServiceClient();
			sender.setOptions(options);
		} catch (AxisFault e) {
			ExceptionHandler.handleWebServieException(e, "faild to prepare Fxlink webservice");
		}
	}

	public void acknowledge(String messageId, String statusId)
			throws WSRetryableException {
		
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace(
				"http://services.axis.fxportal.hermes.filogix.com", "");

		OMElement method = fac.createOMElement("acknowledge", omNs);
		
		OMElement omResponse = fac.createOMElement("response", omNs);
		method.addChild(omResponse);
		
		OMElement omMessageid = fac.createOMElement("messageId", omNs);
		omMessageid.addChild(fac.createOMText(omMessageid, messageId));
		omResponse.addChild(omMessageid);

		OMElement omStatus = fac.createOMElement("status", omNs);
		omStatus.addChild(fac.createOMText(omStatus, statusId));
		omResponse.addChild(omStatus);
		
		try {
			sender.sendRobust(method);
		} catch (AxisFault e) {
			ExceptionHandler.handleWebServieException(
					e, "faild to execute acknowledge service");
		}

	}

	public WSResponseBean getMessage() throws WSRetryableException {
		
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace(
				"http://services.axis.fxportal.hermes.filogix.com", "");

		OMElement method = fac.createOMElement("getMessage", omNs);
		
		OMElement value = fac.createOMElement("channel", omNs);
		value.addChild(fac.createOMText(value, getChannelName()));
		method.addChild(value);

		value = fac.createOMElement("clientFormat", omNs);
		value.addChild(fac.createOMText(value, getClientFormat()));
		method.addChild(value);

		value = fac.createOMElement("clientVersion", omNs);
		value.addChild(fac.createOMText(value, getClientVersion()));
		method.addChild(value);
		
		WSResponseBean bean = new WSResponseBean();
		OMElement response = null;
		try {
			response = sender.sendReceive(method);
		} catch (AxisFault e) {
			ExceptionHandler.handleWebServieException(
					e, "faild to execute getMessage service");
		}
		
		logger.debug(response.toString());
		
		if(response == null) return bean;
		
		OMElement versionedMessage = response.getFirstElement();
		if(versionedMessage == null || versionedMessage.getText() == null) return bean;
		
		bean.setSourceApplicationId(getTextFromElement(versionedMessage, "dealId"));
		bean.setFormat(getTextFromElement(versionedMessage, "format"));
		bean.setMessageId(getTextFromElement(versionedMessage, "messageId"));
		bean.setReceiver(getTextFromElement(versionedMessage, "receiver"));
		bean.setSender(getTextFromElement(versionedMessage, "sender"));
		//bean.setTimestamp(getTextFromElement(response, "timestamp")); //don't need to use it
		bean.setTransaction(getTextFromElement(versionedMessage, "transaction"));
		bean.setVersion(getTextFromElement(versionedMessage, "version"));
				
		OMElement arrayOfVersionedDocument = getFirstChildElement(versionedMessage, "documents");
		if(arrayOfVersionedDocument == null) return bean;

		OMElement versionedDocument = getFirstChildElement(arrayOfVersionedDocument, "documents");
		if(versionedDocument == null) return bean;
		
		String contextXml = getTextFromElement(versionedDocument, "content");
		if(contextXml != null){
			byte[] contextArray = Base64.decodeBase64(contextXml.getBytes());
			bean.setDocumentContent(contextArray);
		}

		bean.setDocumentDocname(getTextFromElement(versionedDocument, "docname"));
		bean.setDocumentFormat(getTextFromElement(versionedDocument, "format"));
		bean.setDocumentMimetype(getTextFromElement(versionedDocument, "mimetype"));
		bean.setDocumentVersion(getTextFromElement(versionedDocument, "version"));
		return bean;

	}
	
	private String getTextFromElement(OMElement element, String localName){
		OMElement child = getFirstChildElement(element, localName);
		return (child != null) ? child.getText() : null; 
	}
	
	private OMElement getFirstChildElement(OMElement element, String localName){
		QName qn = new QName(element.getDefaultNamespace().getNamespaceURI(), localName);
		return element.getFirstChildWithName(qn);
	}

}
