/*
 * @(#)AbstractFxLinkWebService.java    2009-6-23
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink.ws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.basis100.deal.entity.Channel;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.fxlink.ExceptionHandler;
import com.filogix.express.fxlink.WSRetryableException;
import com.filogix.externallinks.services.util.ChannelUtils;


public abstract class AbstractFxLinkWebService implements IFxLinkWebService, InitializingBean {

	private final static Logger logger = 
		LoggerFactory.getLogger(AbstractFxLinkWebService.class);

	private String channelName;
	private String clientFormat;
	private String clientVersion;
	private Integer channelId;
	private Integer timeout;

	private String endPoint;
	private Channel channel;

	/**
	 * Implementing InitializingBean 
	 * Spring automatically calls this method
	 */
	public void afterPropertiesSet() throws Exception {
		//Assert.notNull(this.channelName, "\"channelName\" must not null");
		Assert.notNull(this.clientFormat, "\"clientFormat\" must not null");
		Assert.notNull(this.clientVersion, "\"clientVersion\" must not null");
		Assert.notNull(this.channelId, "\"channelId\" must not null");
		Assert.notNull(this.timeout, "\"timeout\" must not null");
	}
	
	/**
	 * this method initialises required configurations
	 */
	public void init() throws WSRetryableException{
		
		SessionResourceKit srk = new SessionResourceKit();
		try {
			this.channel = new Channel(srk, this.channelId);
			this.endPoint = ChannelUtils.concatEndpoint(this.channel);
			PropertiesCache prop = PropertiesCache.getInstance();
			this.channelName = prop.getInstanceProperty("fxLinkClient.channelName");
		} catch (RemoteException e) {
			ExceptionHandler.handleFatal(e,"unexpected error");
		} catch (FinderException e) {
			String msg = "channelId=" + channelId
			+ " was not found in Channel table. ";
			ExceptionHandler.handleFatal(e, msg);
		} finally {
			srk.freeResources();
		}

		logger.info("channelId =" + this.channelId);
		logger.info("endPoint =" + this.endPoint);
		logger.info("channelName =" + this.channelName);
		logger.info("clientFormat =" + this.clientFormat);
		logger.info("clientVersion =" + this.clientVersion);

		prepareService();
	}

	/**
	 * the class implements this class has to override this method.
	 * this method is to prepare the initial sets of service stabs.
	 * and this method is called by init method in this class. 
	 */
	protected abstract void prepareService() throws WSRetryableException;


	protected String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	protected String getClientFormat() {
		return clientFormat;
	}

	public void setClientFormat(String clientFormat) {
		this.clientFormat = clientFormat;
	}

	protected String getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(String clientVersion) {
		this.clientVersion = clientVersion;
	}

	protected int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	protected String getEndPoint() {
		return endPoint;
	}

	protected Channel getChannel() {
		return channel;
	}
}
