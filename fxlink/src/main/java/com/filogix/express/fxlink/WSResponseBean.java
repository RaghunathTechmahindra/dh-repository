/*
 * @(#)WSResponseBean.java    2009-6-23
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink;

import java.util.Calendar;

public class WSResponseBean {

	private String sourceApplicationId;
	private String format;
	private String messageId;
	private String receiver;
	private String sender;
	private Calendar timestamp;
	private String transaction;
	private String version;

	private byte[] documentContent;
	private String documentDocname;
	private String documentFormat;
	private String documentMimetype;
	private String documentVersion;

	
	public String getSourceApplicationId() {
		return sourceApplicationId;
	}

	public void setSourceApplicationId(String sourceApplicationId) {
		this.sourceApplicationId = sourceApplicationId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Calendar getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public byte[] getDocumentContent() {
		return documentContent;
	}

	public void setDocumentContent(byte[] documentContent) {
		this.documentContent = documentContent;
	}

	public String getDocumentDocname() {
		return documentDocname;
	}

	public void setDocumentDocname(String documentDocname) {
		this.documentDocname = documentDocname;
	}

	public String getDocumentFormat() {
		return documentFormat;
	}

	public void setDocumentFormat(String documentFormat) {
		this.documentFormat = documentFormat;
	}

	public String getDocumentMimetype() {
		return documentMimetype;
	}

	public void setDocumentMimetype(String documentMimetype) {
		this.documentMimetype = documentMimetype;
	}

	public String getDocumentVersion() {
		return documentVersion;
	}

	public void setDocumentVersion(String documentVersion) {
		this.documentVersion = documentVersion;
	}
	

}
