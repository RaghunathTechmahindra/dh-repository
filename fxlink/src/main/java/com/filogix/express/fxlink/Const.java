/*
 * @(#)Const.java    2009-6-23
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink;

public class Const {

	public static final String PROPERTY_KEY_SLEEP_TIME_ON_NODATA = "fxLinkClient.sleep.time";
	public static final String PROPERTY_KEY_SLEEP_TIME_ON_ERROR = "fxLinkClient.retry.time";
	public static final String PROPERTY_KEY_RETRY_MAX = "fxLinkClient.maxAttempCounts";
	
	public static final int RETRY_COUNTER_START = 0;
	public static final int ARCHFCXLOANSTATUSID_RECEIVED = 0;
	public static final String FXLINK_RESPONSE_CODE_OK = "0";
	
	public static final int DEFAULT_INSTITUTION = -1;
	public static final String DEFAULT_SLEEP_TIME_ON_NODATA = "5000"; //5 sec
	public static final String DEFAULT_SLEEP_TIME_ON_ERROR = "180000"; //3 min
	public static final String DEFAULT_MAX_RETRY = "10";
	public static final String FXLINK_ENCORDING_CHARSET = "UTF-8";

}
