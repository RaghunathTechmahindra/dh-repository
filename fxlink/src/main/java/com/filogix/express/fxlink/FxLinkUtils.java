/*
 * @(#)FxLinkUtils.java    2009-7-15
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.express.email.EmailSender;

public class FxLinkUtils {

	private final static Logger logger = LoggerFactory
			.getLogger(FxLinkUtils.class);

	public static void sendAlertEmail(Throwable error) {

		logger.debug("before sending an email ...");
		try {
			EmailSender.sendAlertEmail("Express Link Client", error);
		} catch (RuntimeException e) {
			logger.error("faild to send an alert email", e);
		}
		logger.debug("after sending an email ...");
	}

}
