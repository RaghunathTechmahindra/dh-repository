/*
 * @(#)ExpresFxLinkContext.java    2009-05-08
 *
 * Copyright (C) 2008 Filogix, Inc. All rights reserved.
 */
package com.filogix.express.fxlink;

import com.filogix.express.context.ExpressContext;
import com.filogix.express.context.ExpressContextDirector;
import com.filogix.express.context.ExpressServiceException;

/**
 * ExpressFxLinkContext provides the unified interfaces to access services in
 * Express FxLink context.
 *
 * @version   1.0 May 5, 2009, Hiro
 */
public class ExpressFxLinkContext implements ExpressContext {

    // the key for the fxLink service factory.
    private final static String EXPRESS_FXLINK_CONTEXT_KEY =
        "com.filogix.express.fxlink";

    /**
     * returns a service instance from Express FxLink context.
     *
     * @param name the service name.
     * @return an instance of the service.
     * @throw ExpressServiceException if failed to get an instance for the
     * service.
     */
    synchronized public static Object getService(String name)
        throws ExpressServiceException {

        return ExpressContextDirector.getDirector().
            getServiceFactory(EXPRESS_FXLINK_CONTEXT_KEY).
            getService(name);
    }
}