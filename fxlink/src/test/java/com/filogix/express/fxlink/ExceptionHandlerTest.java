package com.filogix.express.fxlink;

import static org.junit.Assert.*;

import org.apache.axis2.AxisFault;
import org.junit.Test;

import com.filogix.express.core.ExpressRuntimeException;

public class ExceptionHandlerTest {

	@Test(expected=ExpressRuntimeException.class)
	public void testHandleFatal1() {
		ExceptionHandler.handleFatal(new Exception(), "");
	}

	@Test
	public void testHandleFatal2() {
		Exception e = new Exception("original message");
		String msg = null;
		try{
		ExceptionHandler.handleFatal(e, "additional message");
		}catch(RuntimeException r){
			msg = r.getMessage();
		}
		
		assertEquals("additional message", msg);
		
	}

	
	@Test(expected=WSRetryableException.class)
	public void testHandleWSRetryable1() throws WSRetryableException {
		ExceptionHandler.handleWebServieException(new AxisFault("test"), "");
	}
	
	@Test
	public void testHandleWSRetryable2() {
		AxisFault e = new AxisFault("original message");
		String msg = null;
		try{
		ExceptionHandler.handleWebServieException(e, "additional message");
		}catch(WSRetryableException ex){
			msg = ex.getMessage();
		}
		
		assertEquals("additional message", msg);
		
	}
}
