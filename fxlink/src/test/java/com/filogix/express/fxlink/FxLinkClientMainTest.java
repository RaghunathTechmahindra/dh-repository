/*
 * @(#)FxLinkClientMainTest.java     22-Jun-09
 * 
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.fxlink;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.Test;

import com.basis100.resources.SessionResourceKit;

/**
 * @author hkobayashi
 *
 */
public class FxLinkClientMainTest {

	/**
	 * Test method for {@link com.filogix.express.fxlink.FxLinkClientMain#archiveData(com.filogix.express.fxlink.WSResponseBean, com.basis100.resources.SessionResourceKit)}.
	 * @throws Exception 
	 */
	@Test
	public void testArchiveData() throws Exception {
		SessionResourceKit srk = null;
		try {
			srk = new SessionResourceKit();
			srk.beginTransaction();
			
			Connection con = srk.getConnection();
			Statement stat = con.createStatement();
			

			FxLinkClientMain fxlink = new FxLinkClientMain();
			
			WSResponseBean bean = new WSResponseBean();
			bean.setMessageId("UT-MID-100");
			bean.setReceiver("UT-Reciever");
			bean.setSender("UT-Sender");
			bean.setSourceApplicationId("UT-SAPP-200");
			bean.setFormat("UTX");
			bean.setVersion("1.1");
			
			bean.setDocumentContent("Contents of XML for unit test".getBytes(Const.FXLINK_ENCORDING_CHARSET));
			
			
			String delSql = "delete from FCXLoanAppQueue where SENDERMESSAGEID = '" + bean.getMessageId() + "'";
			stat.executeUpdate(delSql);
			
			//target
			fxlink.archiveData(bean, srk);
			

			String sql = "select * from FCXLoanAppQueue where SENDERMESSAGEID = '" + bean.getMessageId() + "'";
			ResultSet rs = stat.executeQuery(sql);
			if(rs.next() == false) fail("data was not saved");
			assertEquals(bean.getMessageId(), rs.getString("SENDERMESSAGEID"));
			assertEquals(bean.getSourceApplicationId(), rs.getString("SOURCEAPPLICATIONID"));
			assertEquals("Contents of XML for unit test", rs.getString("MESSAGE"));
			System.out.println(rs.getString("MESSAGE"));
			
		} finally{
			srk.cleanTransaction();
			srk.freeResources();
		}
		
		
		
	}

	/**
	 * Test method for {@link com.filogix.express.fxlink.FxLinkClientMain#checkRecieve(com.filogix.express.fxlink.WSResponseBean)}.
	 */
	@Test
	public void testCheckRecieve() {
		FxLinkClientMain fxlink = new FxLinkClientMain();
		assertFalse(fxlink.checkRecieve(null));
		
		WSResponseBean bean = new WSResponseBean();
		
		assertFalse(fxlink.checkRecieve(bean));
		
		bean = new WSResponseBean();
		byte[] barr = new byte[1];
		assertFalse(fxlink.checkRecieve(bean));
		
		bean = new WSResponseBean();
		barr = "Hello".getBytes();
		bean.setDocumentContent(barr);
		assertTrue(fxlink.checkRecieve(bean));
		
	}

}
