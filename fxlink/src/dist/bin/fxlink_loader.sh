#!/bin/sh
#
#==================================================================
PIDFILE=./fxlink.pid
COMMAND="java -classpath . LauncherBootstrap -launchfile fxlink.xml fxlink"
#=================================================================

case "$1" in
  start)
    if [ -f $PIDFILE ]; then
      PID=`cat $PIDFILE`
      echo "FxLink process seems already be running under PID $PID"
      echo "(PID file $PIDFILE already exists). Check for process..."
      echo `ps -f -p $PID`
      exit 3
    else
      echo "Starting Fxlink... "
      $COMMAND &
      PID=$!
      echo $PID > $PIDFILE
      exit 0
    fi
  ;;
  stop)
    if [ ! -f $PIDFILE ]; then
      echo "PID file $PIDFILE not found, unable to determine process to kill..."
      exit 3
    else
      echo "Stopping FxLink... "
      PID=`cat $PIDFILE`
      kill -9 $PID
      rm -f $PIDFILE
      exit 0
    fi
  ;;
  restart)
    $0 stop
    $0 start
  ;;
  *)
    echo ""
    echo "Fxlink control"
    echo "-------------"
    echo "Syntax:"
    echo "  $0 {start|stop|restart}"
    echo ""
    exit 3
  ;;
esac
