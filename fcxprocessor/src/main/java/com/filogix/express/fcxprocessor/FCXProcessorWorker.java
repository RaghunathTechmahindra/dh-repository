/*
 * @(#)FCXProcessorWorker.java    2009-07-15
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */

package com.filogix.express.fcxprocessor;

import java.util.Collection;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.FCXLoanAppQueue;
import com.basis100.deal.entity.IngestionQueue;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.email.EmailSender;


public class FCXProcessorWorker extends Thread {
	
	private SessionResourceKit srk;
	private String deal_xml;
	private FCXTransformer transformer;
	private FCXLoanAppQueue record;
	private int maxRetry;
	private boolean fatalError;
	private boolean retryableError;
	
	//Logger
	private final static Logger _log = LoggerFactory
	.getLogger(ExpressFCXProcessorContext.class);
	
	public FCXProcessorWorker(FCXTransformer transformer, FCXLoanAppQueue record, int maxRetry) {
		srk = new SessionResourceKit("FCXProcessorWorker");
		this.transformer = transformer;
		this.record = record;
		this.maxRetry = maxRetry;
		this.fatalError = false;
		this.retryableError = false;
	}
	
	/**
	 * Does the following:
	 * 1. Invokes the transformation method and passes the loan application to it.
	 * 2. If success, update database through FCXLoanAppQueue and IngestionQueue
	 * 3. If retry-able exception happens, retry up to maxRetry times. 
	 * For each retry, increase the retry counter in the FCXLoanAppQueue table by 1.
	 * 4. If fatal exception happens, exit.
	 */
	public void run(){
		try {
			//Transform
			this.deal_xml = (String)transformer.transform(record);
			//Successfully transformed.	
			//Update queue records.
			record.getSessionResourceKit().beginTransaction();
			//Insert the record into Ingestion Queue.
			IngestionQueue iq = new IngestionQueue(record.getSessionResourceKit());
			//create the ingestion queue item.
			iq.create(record.getSourceApplicationId(), getLenderId(record.getReceiverChannel()), 
					record.getSenderChannel(), record.getReceiverChannel(), 
					record.getReceivedTimeStamp(), this.deal_xml);
			iq.ejbStore();
			//Remove the record from FCXLoanAppQueue
			record.ejbRemove();
			//Remove previously failed record(s) with the same source app id.
			removePreviouslyFailed();
			//Commit.
			record.getSessionResourceKit().commitTransaction();
			//Process successfully completed.
			_log.info("Successfully transformed deal with IngestionQueueID:" + 
					iq.getPk().getId() + " and source application ID:" + 
					iq.getSourceApplicationId());
		} catch (FCXTransformerException e) {
			handleTransformationError(e);
		} catch (Exception e) {
			//DB Error. Fatal error.
			_log.error("Fatal error caught in FCXProcessorWorker", e);
			e.printStackTrace();
			this.fatalError = true;
		}
	}
	
	/**
	 * Do ejbRemove on all records found by invoking the findFailedBySourceApplicationId method on record. 
	 * @throws FinderException
	 * @throws RemoteException
	 */
	private void removePreviouslyFailed() throws FinderException, RemoteException{
		//Check if there is any other records with the same source app id in the queue that is in error status 
		Collection<FCXLoanAppQueue> previouslyFailed = 
			record.findFailedBySourceApplicationId(record.getSourceApplicationId());
		if(null == previouslyFailed) return;
		Iterator<FCXLoanAppQueue> previouslyFailedIt = previouslyFailed.iterator();
		while(previouslyFailedIt.hasNext()){
			FCXLoanAppQueue nextItem = previouslyFailedIt.next();
			nextItem.ejbRemove();
			_log.info("Marked FCXLoanAppQueue item" + 
					nextItem.getPk().getId() + 
					" for deletion with source application ID:" + 
					nextItem.getSourceApplicationId());
		}
	}
	
	/**
	 * Gets the last token from the receiver channel (delimiter is .)
	 * @param receiverChannel
	 * @return The string value of the lender. Null if we cannot find such value.
	 */
	private String getLenderId(String receiverChannel){
		int last_delim = receiverChannel.lastIndexOf('.') + 1;
		if(last_delim >0 && last_delim < receiverChannel.length()){
			return receiverChannel.substring(last_delim);
		} else {
			return null;
		}
	}
	
	private void handleTransformationError(FCXTransformerException e) {
		//Retry-able error.
		if(e.getFCXErrorStatusCode() == FCXQueueStatus.NETWORK_ERROR.getStatusCode()) {
			if(record.getRetryCounter() < maxRetry) {
				//Increment the error count.
				record.setRetryCounter(record.getRetryCounter() + 1);
				record.setFcxLoanAppStatusId(FCXQueueStatus.NETWORK_ERROR.getStatusCode());
				writeToDatabase(record);
				_log.error("Retryable transformation error:", e);
				this.retryableError = true;
			} else { //exceeded maxRetry. Email TA for this error.
				String overMaxRetry = "Maximum number of retries reached for " + record.getPk().getId();
				EmailSender.sendAlertEmail("FCXProcessor", e, null, overMaxRetry);
			}
		} else {
			//Other error. Set error code and update the record.
			record.setFcxLoanAppStatusId(e.getFCXErrorStatusCode());
			writeToDatabase(record);
			String unRetryableError = "Un-Retryable transformation error for FCXLoanAppQueue item with ID " + record.getPk().getId(); 
			_log.error(unRetryableError, e);
			EmailSender.sendAlertEmail("FCXProcessor", e, null, unRetryableError);
		}
	}
	
	private void writeToDatabase(FCXLoanAppQueue record) {
		try {
			record.ejbStore();
		} catch (RemoteException e1) {
			//Now we are at fatal error because we cannot do db write.
			_log.error("Fatal error: failed to update FCXApplicationQueue item " + record.getFcxLoanAppQueueId());
			EmailSender.sendAlertEmail("FCXProcessor", e1, null, "Fatal error happened. Cannot update DB entity");
			//Set fatal error and finish.
			this.fatalError = true;
		}
	}

	public SessionResourceKit getSrk() {
		return srk;
	}

	public void setSrk(SessionResourceKit srk) {
		this.srk = srk;
	}

	public String getDeal_xml() {
		return deal_xml;
	}

	public void setDeal_xml(String deal_xml) {
		this.deal_xml = deal_xml;
	}

	public FCXTransformer getTransformer() {
		return transformer;
	}

	public void setTransformer(FCXTransformer transformer) {
		this.transformer = transformer;
	}
	
	public boolean isFatalError() {
		return fatalError;
	}
	
	public boolean isRetryableError() {
		return retryableError;
	}
}
