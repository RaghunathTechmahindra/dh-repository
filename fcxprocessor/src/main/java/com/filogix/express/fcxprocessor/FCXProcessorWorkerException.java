package com.filogix.express.fcxprocessor;

public class FCXProcessorWorkerException extends Exception {
	

    public FCXProcessorWorkerException()
    {
        super();
    }

    public FCXProcessorWorkerException(String message)
    {
        super(message);
    }

    public FCXProcessorWorkerException(String message, Throwable cause)
    {
    	super(message, cause);
    }



}
