package com.filogix.express.fcxprocessor;

public class FCXTransformerException extends Exception {
	
	int FCXErrorStatusCode;
	
    public FCXTransformerException()
    {
        super();
    }
    
    public FCXTransformerException(String message)
    {
        super(message);
    }
    
    public FCXTransformerException(String message, int statusCode)
    {
        super(message);
        this.FCXErrorStatusCode = statusCode;
    }


    public FCXTransformerException(String message, int statusCode,Throwable cause)
    {
    	super(message, cause);
    	this.FCXErrorStatusCode = statusCode;
    }

	public int getFCXErrorStatusCode() {
		return FCXErrorStatusCode;
	}

	public void setFCXErrorStatusCode(int errorStatusCode) {
		FCXErrorStatusCode = errorStatusCode;
	}

}
