/*
 * @(#)FCXTransformerDataPowerImpl.java    2009-07-15
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */


package com.filogix.express.fcxprocessor;

import java.io.IOException;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axiom.soap.SOAPHeaderBlock;
import org.apache.axiom.soap.SOAPProcessingException;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.FCXLoanAppQueue;
import com.basis100.entity.FinderException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.datapower.InboundFCXServerFinalAnyServiceStub;
import com.filogix.externallinks.services.util.ChannelUtils;
import com.filogix.genericlenderanyfinal.inboundfcxserveranyfinal.XMLInboundFCXTypeDocument;
import com.filogix.genericlenderanyfinal.inboundfcxserveranyfinal.XMLResponseDealFCXTypeDocument;


/**
 * This is the Data Power implementation of the FCXTransformer.
 * This class talks to the Data Power XSLT transformation service 
 * through web service.
 */
public class FCXTransformerDataPowerImpl implements FCXTransformer {
	
	//Timeout in milliseconds.
	private int timeout;
	
	//Endpoint for webservice.
	private String endpoint;
	
	//Username for the webservice.
	private String username;
	
	//Password for the webservice.
	private String password;
	
	//ChannelId for the endpoint info in database.
	private int channelId;
	
	//Logger
	private final static Logger _log = LoggerFactory
	.getLogger(ExpressFCXProcessorContext.class);
	
	private void init() throws FCXTransformerException{
			SessionResourceKit srk = new SessionResourceKit();
			Channel channel;
			try {
				channel = new Channel(srk, channelId);
				String endpoint = ChannelUtils.concatEndpoint(channel);
				setEndpoint(endpoint);
				setUsername(channel.getUserId());
				setPassword(channel.getPassword());
			} catch (com.basis100.entity.RemoteException e) {
				throw new FCXTransformerException(e.getMessage(), FCXQueueStatus.OTHER_ERROR.getStatusCode());
			} catch (FinderException e) {
				throw new FCXTransformerException(e.getMessage(), FCXQueueStatus.OTHER_ERROR.getStatusCode());
			} finally {
				srk.freeResources();
			}
	}
		
	/**
	 * Does the following:
	 * 1. Connect to DataPower XSL Transformation web service through stub class.
	 * 2. If success, return the resulting DealXML.
	 * 3. If error happens, throw the exception.
	 * @throws FCXTransformerException 
	 */
	public String transform(Object record) throws FCXTransformerException {
		if(!(record instanceof FCXLoanAppQueue)) {
			throw new FCXTransformerException("Input is not FCXLoanAppQueue object.", FCXQueueStatus.OTHER_ERROR.getStatusCode());
		}
		
		FCXLoanAppQueue appRecord = (FCXLoanAppQueue)record;
		
		try{
			//Initialize values.
			init();
			//Initialize the stub.
			InboundFCXServerFinalAnyServiceStub stub = new InboundFCXServerFinalAnyServiceStub(endpoint);
			ServiceClient sc = stub._getServiceClient();
			
			//Set the timeout value.
			sc.getOptions().setTimeOutInMilliSeconds(getTimeout());
			
			//Set username and password.
			SOAPHeaderBlock soapHeaderBlock = getSOAPSecurityHeaderBlock(); 
			sc.addHeader(soapHeaderBlock);

			//Call transformation function.
			XMLInboundFCXTypeDocument reqDoc = XMLInboundFCXTypeDocument.Factory.newInstance();
			XMLInboundFCXTypeDocument.XMLInboundFCXType req = reqDoc.addNewXMLInboundFCXType();

			try {
				req.set(XmlObject.Factory.parse(appRecord.getMessage()));
			} catch (XmlException e) {
				throw new FCXTransformerException(e.getMessage(), 4);
			}
			
			XMLResponseDealFCXTypeDocument responseDoc = stub.inboundFCX(reqDoc);
			XmlObject xObj2;
			try {
				xObj2 = XmlObject.Factory.parse(responseDoc.getXMLResponseDealFCXType().getDomNode().getFirstChild().getNextSibling());
			} catch (XmlException e) {
				throw new FCXTransformerException(e.getMessage(), 4);
			}
			return xObj2.toString();

		} catch(AxisFault af) {
			String message = af.getMessage();
			QName faultcode = af.getFaultCode();
			String str_faultcode = "";
			if(null != faultcode) str_faultcode = faultcode.toString();
			_log.error("AxisFault "+ str_faultcode +" happened for FCXProcessor.");
			if(af.getCause() instanceof IOException) {
				throw new FCXTransformerException(message, FCXQueueStatus.NETWORK_ERROR.getStatusCode());
			} else if (str_faultcode.contains("401")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_AUTHENTICATION_ERROR.getStatusCode());
			} else if (str_faultcode.contains("403")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_AUTHORIZATION_ERROR.getStatusCode());
			} else if (str_faultcode.contains("440")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_INVALID_WSA_ERROR.getStatusCode());
			} else if (str_faultcode.contains("441")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_INVALID_TRANSACTION_IDENTIFIER_ERROR.getStatusCode());
			} else if (str_faultcode.contains("451")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_INVALID_RECEIVER_ELEMENT_ERROR.getStatusCode());
			} else if (str_faultcode.contains("452")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_INVALID_PAYLOAD_ERROR.getStatusCode());
			} else if (str_faultcode.contains("453")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_SCHEMA_VALIDATION_ERROR.getStatusCode());
			} else if (str_faultcode.contains("454")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_XSLT_FAILURE_ERROR.getStatusCode());
			} else if (str_faultcode.contains("500")){
				throw new FCXTransformerException(message, FCXQueueStatus.DP_INTERNAL_SYSTEM_ERROR.getStatusCode());
			} else {
				//Unknown error.
				af.printStackTrace();
				throw new FCXTransformerException(message, 4);
			}
		} catch (java.rmi.RemoteException re) {
			throw new FCXTransformerException(re.getMessage(), 4);
		}
	}

	private SOAPHeaderBlock getSOAPSecurityHeaderBlock()
			throws SOAPProcessingException {
		SOAPFactory fac = OMAbstractFactory.getSOAP11Factory();
		OMNamespace omNsWsse =
fac.createOMNamespace("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
"wsse");
         
		// create the security block
		SOAPHeaderBlock soapHeaderBlock = fac.createSOAPHeaderBlock("Security", omNsWsse);

		soapHeaderBlock.setMustUnderstand(true);
		
       // create the username Token element
		OMElement usernameTokenElement = fac.createOMElement("UsernameToken", omNsWsse);

		// create the username element
		OMElement usernameElement = fac.createOMElement("Username", omNsWsse);
		OMText omTextUsername = fac.createOMText(usernameElement, username);
		usernameElement.addChild(omTextUsername);

		// create the password element
		OMElement passwordElement = fac.createOMElement("Password", omNsWsse);
		OMText omTextPassword = fac.createOMText(passwordElement, password);
		passwordElement.addChild(omTextPassword);
         
		usernameTokenElement.addChild(usernameElement);
		usernameTokenElement.addChild(passwordElement);
		soapHeaderBlock.addChild(usernameTokenElement);
		return soapHeaderBlock;
	}
	
	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

}
