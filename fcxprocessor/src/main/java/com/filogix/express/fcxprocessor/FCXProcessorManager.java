/*
 * @(#)FCXProcessorManager.java    2009-07-15
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */


package com.filogix.express.fcxprocessor;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;


import com.basis100.deal.entity.FCXLoanAppQueue;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.filogix.express.email.EmailSender;

public class FCXProcessorManager {

		//Logger
		private final static Logger _log = LoggerFactory
		.getLogger(ExpressFCXProcessorContext.class);
		
		//Default retry value.
		private final static String DEFAULT_RETRY = "5";
		
		//Defualt sleep time.
		private final static String DEFAULT_SLEEP_TIME = "5000";
		
		//Default retry time.
		private final static String DEFAULT_RETRY_TIME = "180000";

		/**
		 * @param args
		 */
		public static void main(String[] args) throws InterruptedException{

			ResourceManager.init();
			PicklistData.init();
			BXResources bxResources = new BXResources();
			
			int maxRetry = Integer.parseInt(PropertiesCache.getInstance().
					getProperty(-1, "fcxprocessor.max.retry", DEFAULT_RETRY));			
			int sleepTime = Integer.parseInt(PropertiesCache.getInstance().
					getProperty(-1, "fcxprocessor.sleep.time", DEFAULT_SLEEP_TIME));
			int retryTime = Integer.parseInt(PropertiesCache.getInstance().
					getProperty(-1, "fcxprocessor.retry.time", DEFAULT_RETRY_TIME));
			
			//Step 1: Sanity check.
			sanityCheck();

			//Step 2: Enter the main loop.
			while(true){
				SessionResourceKit srk = new SessionResourceKit("FCXProcessor");
				try{
					FCXLoanAppQueue loanappUpdater = new FCXLoanAppQueue(srk);
					int[] statusIds = {FCXQueueStatus.READY.getStatusCode(), FCXQueueStatus.NETWORK_ERROR.getStatusCode()};
					FCXLoanAppQueue theApp = loanappUpdater.findNextAvailable(statusIds, maxRetry);

					//First mark the record as in progress.
					theApp.setFcxLoanAppStatusId(FCXQueueStatus.IN_PROGRESS.getStatusCode());
					theApp.ejbStore();
					FCXProcessorWorker worker = new FCXProcessorWorker((FCXTransformer) ExpressFCXProcessorContext.
							getService("FCXTransformer"), theApp, maxRetry);
					_log.info("Picked up " + theApp.getPk().getId() +" from FCXLoanAppQueue. Marked as in process.");
					//Start the worker and wait for it to finish.
					try{
						worker.start();
						//Wait for worker thread to finish.
						worker.join();
					} catch (InterruptedException ie) {
						String interruptedMsg = "InterruptedException during worker thread.";
						EmailSender.sendAlertEmail("FCXProcessor", ie, null, interruptedMsg);
						_log.error(interruptedMsg, ie);
						srk.freeResources();
						System.exit(1);
					}
					
					//Free up resource.
					srk.freeResources();
					//Check for fatal error. This might have happened during worker process.
					if(worker.isFatalError()) {
						String workerFatalError = "FCXProcessorManager encountered fatal error. Terminating."; 
						_log.error(workerFatalError);
						EmailSender.sendAlertEmail("FCXProcessor", null, null, workerFatalError);
						System.exit(1);
					} else if(worker.isRetryableError()) {
						//Sleep for an amount of time and try again.
						Thread.sleep(retryTime);
						_log.debug("Woke up from " + retryTime + " milliseconds of sleep for retryable network error.");
					}
					_log.debug("Deal XML result:" + StringUtils.substring(worker.getDeal_xml(), 0, 1000));
				} catch (RemoteException re) {
					//Unable instantiate db. Maybe should retry here.
					_log.error("Cannot instantiate the database search.", re);
					re.printStackTrace();
				} catch (FinderException fe) {
					//Finder exception.
					//Case 1: no qualified record found.
					if(fe.getMessage().contains("no qualified entry found")) {
						_log.debug("no qualified entry found in queue.");
						//Release resource and sleep for a number of seconds.
						srk.freeResources();
						Thread.sleep(sleepTime);
						_log.debug("Woke up from sleep of " + sleepTime + " milliseconds due to no record found.");
						//Go to next iteration.
						continue;
					} else {
						//Case 2: other finder exceptions.
						_log.error("Exception when finding next qualified record to process.", fe);
					}
				} catch (Exception e) {
					_log.error("Other exception happened during manager thread.", e);
					e.printStackTrace();
				} finally {
					//Release resources.
					srk.freeResources();
				}
			}
		}

		/**
		 * Does the sanity check. If there is any record in the queue that has status 'IN_PGORESS', 
		 * notify through email.
		 */
		private static void sanityCheck() {
			SessionResourceKit sanity_srk = new SessionResourceKit("FCXProcessorSanityCheck");
			try {
				FCXLoanAppQueue loanappSChecker = new FCXLoanAppQueue(sanity_srk);
				Collection<FCXLoanAppQueue> result = loanappSChecker.findByStatus(FCXQueueStatus.IN_PROGRESS.getStatusCode());
				if(result.size() == 0) {
					//Nothing is in process. Sanity check passed.
				} else {
					//Result of sanity check is not clean.
					StringBuffer emailErrorMsg = 
						new StringBuffer("Sanity check failed. Found following records with \"in progress\" status in queue:");
					java.util.Iterator<FCXLoanAppQueue> it = result.iterator();
					while(it.hasNext())
						emailErrorMsg.append(it.next().getPk().getId() + "\n");
					_log.info(emailErrorMsg.toString());
					EmailSender.sendAlertEmail("FCXProcessor", null, null, emailErrorMsg.toString());
				}
			} catch (Exception e) {
				String fatalDBErrorMsg = "Fatal DB Error occured during sanity check. FCXProcessor Exits."; 
				_log.error( fatalDBErrorMsg, e);
				EmailSender.sendAlertEmail("FCXProcessor", e, null, fatalDBErrorMsg);
				sanity_srk.freeResources();
				System.exit(1);
			}
			sanity_srk.freeResources();
		}
}