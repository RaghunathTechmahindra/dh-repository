package com.filogix.express.fcxprocessor;

public enum FCXQueueStatus {
	
	READY (0),
	IN_PROGRESS (1),
	SUCCESS (2),
	NETWORK_ERROR (3),
	OTHER_ERROR (4),
	DP_AUTHENTICATION_ERROR (5),
	DP_AUTHORIZATION_ERROR(6),
	DP_INVALID_WSA_ERROR(7),
	DP_INVALID_TRANSACTION_IDENTIFIER_ERROR(8),
	DP_INVALID_RECEIVER_ELEMENT_ERROR(9),
	DP_INVALID_PAYLOAD_ERROR(10),
	DP_SCHEMA_VALIDATION_ERROR(11),
	DP_XSLT_FAILURE_ERROR(12),
	DP_INTERNAL_SYSTEM_ERROR(13);
	
	final int status_code;
	
	private FCXQueueStatus(int error_code) {
		this.status_code = error_code;
	}
	
	int getStatusCode() {
		return status_code;
	}

}
