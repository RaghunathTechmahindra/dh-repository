/*
 * @(#)ExpresFCXProcessorContext.java    2009-05-08
 *
 * Copyright (C) 2008 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.fcxprocessor;

import com.filogix.express.context.ExpressContext;
import com.filogix.express.context.ExpressContextDirector;
import com.filogix.express.context.ExpressServiceException;

/**
 * ExpressFCXProcessorContext provides the unified interfaces to access services in
 * Express FCXProcessor context.
 *
 * @version   1.0 June 17, 2009
 * @author    Phil
 */
public class ExpressFCXProcessorContext implements ExpressContext {

	// // The logger
	// private final static Logger _logger =
	// LoggerFactory.getLogger(ExpressFCXProcessorContext.class);

    // the key for the fxLink service factory.
    private final static String EXPRESS_FCXPROCESSOR_CONTEXT_KEY =
        "com.filogix.express.fcxprocessor";

    /**
     * Returns a service instance from Express FCXProcessor context.
     *
     * @param name the service name.
     * @return an instance of the service.
     * @throw ExpressServiceException if failed to get an instance for the
     * service.
     */
    synchronized public static Object getService(String name)
        throws ExpressServiceException {
	        return ExpressContextDirector.getDirector().
	            getServiceFactory(EXPRESS_FCXPROCESSOR_CONTEXT_KEY).
	            getService(name);
    }
}