/**
 * 
 */
package com.filogix.express.fcxprocessor;

/**
 * @author Phil.Yuan
 *
 */
public interface FCXTransformer {
	
	public Object transform(Object input) throws FCXTransformerException;

}
