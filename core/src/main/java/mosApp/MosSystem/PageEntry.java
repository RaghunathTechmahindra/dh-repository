/*
 * @(#)PageEntry.java    2007-7-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package mosApp.MosSystem;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.log.SysLogger;

/**
 * PageEntry - 
 *
 * @version   1.0 2007-7-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class PageEntry implements Serializable{

    // The logger
    private final static Log _log = 
        LogFactory.getLog(PageEntry.class);

    /**
     * Constructor function
     */
    public PageEntry() {
    }


  // When an edit deal page (e.g. Deal Mod) is used in a view only
  // mode of operation many controls (buttons) require special treatments.
  // The GUI Standards address the behavior of the Submit, Cancel and Ok buttons
  // but this is not enough - many other bottons may trigger server side activity
  // that must be intercepted in view only mode - for example "Add Borrower" button
  // in Deal Mod.
  //
  // The view only behavior for such buttons can be set to either hide (button
  // not visible during view mode) or disable (button visoible but triggers no
  // behavior (other than re-display of screen).
  //
  // A system startup the property com.basis100.hideduringviewmode is read
  // to determine default behavior - if the property is absent 'Y' is assumed
  // meaning hide behavior, not disable behavior.
  private static boolean VIEWMODE_BUTTON_HIDE_BEHAVIOR = false;

  // page control flags
  boolean dealPage = false;
  boolean editable = false;
  boolean modified = false;
  boolean subPage = false;

  // tx copy number/level - if a transaction copy of deal data is created associated with the
  // page entry holds the number or level of the copy in the sessions deal edit control object.
  // For example, the level for a topmost deal edit page would be one (1). Faor a sub-page edited
  // via a transactional copy reached from a tomost/main page the level would be two (2).
  // If there is no transaction copy associated with the page the value will be -1.
  int txCopyLevel = -1;

  // special flag for deal entry page - to support special handling of tx copy
  boolean dealEntry = false;
  PageEntry parentPage;

  // populated when subPage == true
  //******************************************
  //* Variables
  //******************************************
  private int pageMosUserId = 0;
  private int pageMosUserTypeId = 0;
  private String pageName = "";
  private String pageLabel = "";
  private int pageId = 0;
  private int pageAccessType = 0;
  private int pageTaskId = 0;
  private int pageTaskSequence = 0;
  private int pageDealId = 0;
  private int pageDealCID = -1;
  private int pageCriteriaId1 = 0;
  private int pageCriteriaId2 = 0;
  private int pageCriteriaId3 = 0;
  private int pageCriteriaId4 = 0;
  private int pageCriteriaId5 = 0;

  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/21/2004
  private int pageCriteriaId6 = 0;  // filter by Deal Status

  //-- ========== SCR#750   ends ========== --//
  private String pageDealApplicationId = "";
  private String pageDealPrimaryBorrowerName = "";
  private String pageState1 = "";
  private String pageState2 = "";
  private Hashtable pageStateTable = null;
  private HashSet pageExcludePageVars = null;
  private boolean pageCondition1 = false;
  private boolean pageCondition2 = false;
  private boolean pageCondition3 = false;
  private boolean pageCondition4 = false;
  private boolean pageCondition5 = false;
  private boolean pageCondition6 = false;
  private boolean pageCondition7 = false;
  private boolean pageCondition8 = false;
  private boolean pageCondition9 = false;
  private int loadTarget = 0;
  private double netWorth = 0.0;
  private double totalCreditBureauLiab = 0.0;
  private double monthlyCreditBureauLiab = 0.0;

  //--DJ_LDI_CR--start--//
  private double totalPremium = 0.0;

  //--DJ_LDI_CR--end--//
  // Used fro page access determination during page navigation. Values:
  //
  // 1 - navigation to page from GOTO Page Shell function
  // 0 - navigation to page via any other means (page to page, or via directed workflow page
  //     links definition
  //
  // see: Deal Status Management
  private int navigationMode = 0;

  // use to support the task navigater
  boolean taskNavigateMode = false;
  String taskName = null;
  String prevPageName = null;
  PageEntry prevPE = null;
  String nextPageName = null;
  PageEntry nextPE = null;

  // pageDisplayMethod:
  //
  //        0 - load     (framework method forwardTo(requestContext))
  //    1 - display  (framework method display())
  private int pageDisplayMethod = 0;

  // some pages featur multiple data objects on the (mian) page. This will cause
  // multiple calls to PHC.setupBeforePageGeneration().
  //
  // Following flag used to detect this to avoid the multiple calls. The flag is reset
  // in thre standard PHC.clearMessage() which is called at the beginning of an input
  // cycle and so is quite reliable.
  private boolean setupBeforeGenerationCalled = false;

  // FXP26606 - bug from ML 
  private String mwqTaskNameFiler = "";
  
  /**
   *
   *
   */
  public static void setHideInViewMode(boolean b)
  {
    VIEWMODE_BUTTON_HIDE_BEHAVIOR = b;
  }

  /**
   *
   *
   */
  public boolean isHideInViewMode()
  {
    return VIEWMODE_BUTTON_HIDE_BEHAVIOR;
  }

  /**
   *
   *
   */
  public boolean getSetupBeforeGenerationCalled()
  {
    return setupBeforeGenerationCalled;
  }

  /**
   *
   *
   */
  public void setSetupBeforeGenerationCalled(boolean b)
  {
    setupBeforeGenerationCalled = b;
  }

  /*
   * Result: true - current screen contains modified data
   *         false - no modified data (view screen or no changes)
   *
   */
  public boolean isPageDataModified()
  {
    if (isEditable() == false)
    {
      return false;
    }

    // view page
    if (isDealEntry() == true)
    {
      return true;
    }

    // deal entry prior to initial submit
    return isModified();
  }

  public boolean isDealPage()
  {
    return dealPage;
  }

  /**
   *
   *
   */
  public boolean isEditable()
  {
    return editable;
  }

  /**
   *
   *
   */
  public boolean isModified()
  {
    return modified;
  }

  /**
   *
   *
   */
  public boolean isSubPage()
  {
    return subPage;
  }

  /**
   *
   *
   */
  public boolean isDealEntry()
  {
    return dealEntry;
  }

  /**
   *
   *
   */
  public int getTxCopyLevel()
  {
    return txCopyLevel;
  }

  /**
   *
   *
   */
  public PageEntry getParentPage()
  {
    return parentPage;
  }

  /**
   *
   *
   */
  public int getPageMosUserId()
  {
    return pageMosUserId;
  }

  /**
   *
   *
   */
  public int getPageMosUserTypeId()
  {
    return pageMosUserTypeId;
  }

  /**
   *
   *
   */
  public String getPageName()
  {
    return pageName;
  }

  /**
   *
   *
   */
  public String getPageLabel()
  {
    return pageLabel;
  }

  /**
   *
   *
   */
  public int getPageId()
  {
    return pageId;
  }

  /**
   *
   *
   */
  public int getPageAccessType()
  {
    return pageAccessType;
  }

  /**
   *
   *
   */
  public int getPageTaskId()
  {
    return pageTaskId;
  }

  /**
   *
   *
   */
  public int getPageTaskSequence()
  {
    return pageTaskSequence;
  }

  /**
   *
   *
   */
  public int getPageDealId()
  {
    return pageDealId;
  }

  /**
   *
   *
   */
  public int getPageDealCID()
  {
    return pageDealCID;
  }

    /**
     * the deal institution profile id.
     * @since 3.3
     */
    private int _dealInstitutionId = -1;

    /**
     * returns the deal institution profile id.
     *
     * @since 3.3
     */
    public int getDealInstitutionId() {

        return _dealInstitutionId;
    }

    /**
     * set the deal institution profile id.
     * @since 3.3
     */
    public void setDealInstitutionId(int id) {

        _dealInstitutionId = id;
    }

  /**
   *
   *
   */
  public int getPageCriteriaId1()
  {
    return pageCriteriaId1;
  }

  /**
   *
   *
   */
  public int getPageCriteriaId2()
  {
    return pageCriteriaId2;
  }

  /**
   *
   *
   */
  public int getPageCriteriaId3()
  {
    return pageCriteriaId3;
  }

  /**
   *
   *
   */
  public int getPageCriteriaId4()
  {
    return pageCriteriaId4;
  }

  /**
   *
   *
   */
  public int getPageCriteriaId5()
  {
    return pageCriteriaId5;
  }

  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/21/2004

  /**
   *
   */
  public int getPageCriteriaId6()
  {
    return pageCriteriaId6;
  }

  public void setPageCriteriaId6(int aCriteriaId)
  {
    pageCriteriaId6 = aCriteriaId;
  }

  //-- ========== SCR#750   ends ========== --//

  /**
   *
   *
   */
  public String getPageDealApplicationId()
  {
    return pageDealApplicationId;
  }

  /**
   *
   *
   */
  public String getPageDealPrimaryBorrowerName()
  {
    return pageDealPrimaryBorrowerName;
  }

  /**
   *
   *
   */
  public String getPageState1()
  {
    return pageState1;
  }

  /**
   *
   *
   */
  public String getPageState2()
  {
    return pageState2;
  }

  /**
   *
   *
   */
  public Hashtable getPageStateTable()
  {
    return pageStateTable;
  }

  /**
   *
   *
   */
  public boolean getPageCondition1()
  {
    return pageCondition1;
  }

  /**
   *
   *
   */
  public boolean getPageCondition2()
  {
    return pageCondition2;
  }

  /**
   *
   *
   */
  public boolean getPageCondition3()
  {
    return pageCondition3;
  }

  /**
   *
   *
   */
  public boolean getPageCondition4()
  {
    return pageCondition4;
  }

  /**
   *
   *
   */
  public boolean getPageCondition5()
  {
    return pageCondition5;
  }

  /**
   *
   *
   */
  public boolean getPageCondition6()
  {
    return pageCondition6;
  }

  /**
   *
   *
   */
  public boolean getPageCondition7()
  {
    return pageCondition7;
  }

  /**
   *
   *
   */
  public boolean getPageCondition8()
  {
    return pageCondition8;
  }

  /**
   *
   *
   */
  public boolean getPageCondition9()
  {
    return pageCondition9;
  }

  /**
   *
   *
   */
  public int getPageDisplayMethod()
  {
    return pageDisplayMethod;
  }

  /**
   *
   *
   */
  public boolean isTaskNavigateMode()
  {
    return taskNavigateMode;
  }

  /**
   *
   *
   */
  public String getTaskName()
  {
    return taskName;
  }

  /**
   *
   *
   */
  public String getPrevPageName()
  {
    return prevPageName;
  }

  /**
   *
   *
   */
  public PageEntry getPrevPE()
  {
    return prevPE;
  }

  /**
   *
   *
   */
  public String getNextPageName()
  {
    return nextPageName;
  }

  /**
   *
   *
   */
  public PageEntry getNextPE()
  {
    return nextPE;
  }

  /**
   *
   *
   */
  public int getNavigationMode()
  {
    return navigationMode;
  }

  /**
   *
   *
   */
  public int getLoadTarget()
  {
    return loadTarget;
  }

  /**
   *
   *
   */
  public HashSet getPageExcludePageVars()
  {
    return pageExcludePageVars;
  }

  /**
   *
   *
   */
  public double getNetWorth()
  {
    return netWorth;
  }

  //--DJ_LDI_CR--start--//

  /**
   *
   *
   */
  public double getTotalPremium()
  {
    return totalPremium;
  }

  //--DJ_LDI_CR--end--//

  /**
   *
   *
   */
  public double getTotalCreditBureauLiab()
  {
    return totalCreditBureauLiab;
  }

  /**
   *
   *
   */
  public double getMonthlyCreditBureauLiab()
  {
    return monthlyCreditBureauLiab;
  }

  public String getMWQTaskNameFilter() {
	  return mwqTaskNameFiler;
  }
  //******************************************
  //* "setters"
  //******************************************
  public void setDealPage(boolean b)
  {
    dealPage = b;
  }

  /**
   *
   *
   */
  public void setEditable(boolean b)
  {
    editable = b;
  }

  /**
   *
   *
   */
  public void setModified(boolean b)
  {
    modified = b;
  }

  /**
   *
   *
   */
  public void setSubPage(boolean b)
  {
    subPage = b;
  }

  /**
   *
   *
   */
  public void setDealEntry(boolean b)
  {
    dealEntry = b;
  }

  /**
   *
   *
   */
  public void setTxCopyLevel(int level)
  {
    txCopyLevel = level;

    // ensure use value -1 to represent 'no tx copy'
    if (txCopyLevel <= 0)
    {
      txCopyLevel = -1;
    }
  }

  /**
   *
   *
   */
  public void setParentPage(PageEntry pe)
  {
    parentPage = pe;

    subPage = (pe != null) ? true : false;
  }

  /**
   *
   *
   */
  public void setPageMosUserId(int aPageMosUserId)
  {
    pageMosUserId = aPageMosUserId;
  }

  /**
   *
   *
   */
  public void setPageMosUserTypeId(int aPageMosUserTypeId)
  {
    pageMosUserTypeId = aPageMosUserTypeId;
  }

  /**
   *
   *
   */
  public void setPageName(String aPageName)
  {
    pageName = (aPageName == null) ? null : aPageName.trim();
  }

  /**
   *
   *
   */
  public void setPageLabel(String aPageLabel)
  {
    pageLabel = (aPageLabel == null) ? null : aPageLabel.trim();
  }

  /**
   *
   *
   */
  public void setPageId(int aPageId)
  {
    pageId = aPageId;
  }

  /**
   *
   *
   */
  public void setPageAccessType(int aPageAccessType)
  {
    pageAccessType = aPageAccessType;
  }

  /**
   *
   *
   */
  public void setPageTaskId(int aPageTaskId)
  {
    pageTaskId = aPageTaskId;
  }

  /**
   *
   *
   */
  public void setPageTaskSequence(int aPageTaskSequence)
  {
    pageTaskSequence = aPageTaskSequence;
  }

  /**
   *
   *
   */
  public void setPageDealId(int aPageDealId)
  {
    pageDealId = aPageDealId;
  }

  /**
   *
   *
   */
  public void setPageDealCID(int cid)
  {
    pageDealCID = cid;
  }

  /**
   *
   *
   */
  public void setPageCriteriaId1(int aCriteriaId)
  {
    pageCriteriaId1 = aCriteriaId;
  }

  /**
   *
   *
   */
  public void setPageCriteriaId2(int aCriteriaId)
  {
    pageCriteriaId2 = aCriteriaId;
  }

  /**
   *
   *
   */
  public void setPageCriteriaId3(int aCriteriaId)
  {
    pageCriteriaId3 = aCriteriaId;
  }

  /**
   *
   *
   */
  public void setPageCriteriaId4(int aCriteriaId)
  {
    pageCriteriaId4 = aCriteriaId;
  }

  /**
   *
   *
   */
  public void setPageCriteriaId5(int aCriteriaId)
  {
    pageCriteriaId5 = aCriteriaId;
  }

  /**
   *
   *
   */
  public void setPageDealApplicationId(String aPageDealApplicationId)
  {
    pageDealApplicationId = (aPageDealApplicationId == null) ? null : aPageDealApplicationId.trim();
  }

  /**
   *
   *
   */
  public void setPageDealPrimaryBorrowerName(String aPageDealPrimaryBorrowerName)
  {
    pageDealPrimaryBorrowerName =
      (aPageDealPrimaryBorrowerName == null) ? null : aPageDealPrimaryBorrowerName.trim();
  }

  /**
   *
   *
   */
  public void setPageState1(String aPageState1)
  {
    pageState1 = aPageState1;
  }

  /**
   *
   *
   */
  public void setPageState2(String aPageState2)
  {
    pageState2 = aPageState2;
  }

  /**
   *
   *
   */
  public void setPageStateTable(Hashtable aPageStateTable)
  {
    pageStateTable = aPageStateTable;
  }

  /**
   *
   *
   */
  public void setPageCondition1(boolean aPageCondition1)
  {
    pageCondition1 = aPageCondition1;
  }

  /**
   *
   *
   */
  public void setPageCondition2(boolean aPageCondition2)
  {
    pageCondition2 = aPageCondition2;
  }

  /**
   *
   *
   */
  public void setPageCondition3(boolean aPageCondition3)
  {
    pageCondition3 = aPageCondition3;
  }

  /**
   *
   *
   */
  public void setPageCondition4(boolean aPageCondition4)
  {
    pageCondition4 = aPageCondition4;
  }

  /**
   *
   *
   */
  public void setPageCondition5(boolean aPageCondition5)
  {
    pageCondition5 = aPageCondition5;
  }

  /**
   *
   *
   */
  public void setPageCondition6(boolean aPageCondition6)
  {
    pageCondition6 = aPageCondition6;
  }

  /**
   *
   *
   */
  public void setPageCondition7(boolean aPageCondition7)
  {
    pageCondition7 = aPageCondition7;
  }

  /**
   *
   *
   */
  public void setPageCondition8(boolean aPageCondition8)
  {
    pageCondition8 = aPageCondition8;
  }

  /**
   *
   *
   */
  public void setPageCondition9(boolean aPageCondition9)
  {
    pageCondition9 = aPageCondition9;
  }

  /**
   *
   *
   */
  public void setPageDisplayMethod(int method)
  {
    pageDisplayMethod = method;
  }

  /**
   *
   *
   */
  public void setTaskNavigateMode(boolean b)
  {
    taskNavigateMode = b;
  }

  /**
   *
   *
   */
  public void setTaskName(String tn)
  {
    taskName = (tn == null) ? null : tn.trim();
  }

  /**
   *
   *
   */
  public void setPrevPageName(String nm)
  {
    prevPageName = (nm != null) ? nm.trim() : null;
  }

  /**
   *
   *
   */
  public void setPrevPE(PageEntry pe)
  {
    prevPE = pe;
  }

  /**
   *
   *
   */
  public void setNextPageName(String nm)
  {
    nextPageName = (nm != null) ? nm.trim() : null;
  }

  /**
   *
   *
   */
  public void setNextPE(PageEntry pe)
  {
    nextPE = pe;
  }

  /**
   *
   *
   */
  public void setNavigationMode(int mode)
  {
    navigationMode = mode;
  }

  /**
   *
   *
   */
  public void setLoadTarget(int val)
  {
    loadTarget = val;
  }

  /**
   *
   *
   */
  public void setPageExcludePageVars(HashSet aPageExcludePageVars)
  {
    pageExcludePageVars = aPageExcludePageVars;
  }

  /**
   *
   *
   */
  public void setNetWorth(double val)
  {
    netWorth = val;
  }

  public void setTotalPremium(double val)
  {
    totalPremium = val;
  }

  public void setTotalCreditBureauLiab(double val)
  {
    totalCreditBureauLiab = val;
  }

  public void setMonthlyCreditBureauLiab(double val)
  {
    monthlyCreditBureauLiab = val;
  }

  public void setMWQTaskNameFilter(String val) {
	  mwqTaskNameFiler = val;
  }

  //*********************************************
  // STATIC Methods
  //*********************************************
  public static PageEntry getPageEntryAsCopy(PageEntry pg)
  {
    PageEntry pgCopy = new PageEntry();

    pgCopy.setPageMosUserId(pg.getPageMosUserId());

    pgCopy.setPageMosUserTypeId(pg.getPageMosUserTypeId());

    pgCopy.setPageName(pg.getPageName());

    pgCopy.setPageLabel(pg.getPageLabel());

    pgCopy.setPageId(pg.getPageId());

    pgCopy.setPageTaskId(pg.getPageTaskId());

    pgCopy.setPageAccessType(pg.getPageAccessType());

    pgCopy.setPageTaskSequence(pg.getPageTaskSequence());

    pgCopy.setPageDealId(pg.getPageDealId());

    // add for ML.
    pgCopy.setDealInstitutionId(pg.getDealInstitutionId());

    pgCopy.setPageDealCID(pg.getPageDealCID());

    pgCopy.setPageDealPrimaryBorrowerName(pg.getPageDealPrimaryBorrowerName());

    pgCopy.setPageCriteriaId1(pg.getPageCriteriaId1());

    pgCopy.setPageCriteriaId2(pg.getPageCriteriaId2());

    pgCopy.setPageCriteriaId3(pg.getPageCriteriaId3());

    pgCopy.setPageCriteriaId4(pg.getPageCriteriaId4());

    pgCopy.setPageCriteriaId5(pg.getPageCriteriaId5());

    pgCopy.setPageCriteriaId6(pg.getPageCriteriaId6());

    pgCopy.setPageDealApplicationId(pg.getPageDealApplicationId());

    pgCopy.setPageState1(pg.getPageState1());

    pgCopy.setPageState2(pg.getPageState2());

    pgCopy.setPageCondition1(pg.getPageCondition1());

    pgCopy.setPageCondition2(pg.getPageCondition2());

    pgCopy.setPageCondition3(pg.getPageCondition3());

    pgCopy.setPageCondition4(pg.getPageCondition4());

    pgCopy.setPageCondition5(pg.getPageCondition5());

    pgCopy.setPageCondition6(pg.getPageCondition6());

    pgCopy.setPageCondition7(pg.getPageCondition7());

    pgCopy.setPageCondition8(pg.getPageCondition8());

    pgCopy.setPageCondition9(pg.getPageCondition9());

    pgCopy.setDealPage(pg.isDealPage());

    pgCopy.setEditable(pg.isEditable());

    pgCopy.setModified(pg.isModified());

    pgCopy.setSubPage(pg.isSubPage());

    pgCopy.setDealEntry(pg.isDealEntry());

    pgCopy.setPageStateTable(pg.getPageStateTable());

    // SHALLOW!
    pgCopy.setParentPage(pg.getParentPage());

    // SHALLOW!
    pgCopy.setTaskNavigateMode(pg.isTaskNavigateMode());

    pgCopy.setTaskName(pg.getTaskName());

    pgCopy.setPrevPageName(pg.getPrevPageName());

    pgCopy.setPrevPE(pg.getPrevPE());

    pgCopy.setNextPageName(pg.getNextPageName());

    pgCopy.setNextPE(pg.getNextPE());

    pgCopy.setPageExcludePageVars(pg.getPageExcludePageVars());
    
    pgCopy.setMWQTaskNameFilter(pg.getMWQTaskNameFilter());

    return pgCopy;
  }

  public void show(String id, SysLogger logger)
  {
    if (id != null)
    {
      logger.debug("PE:: " + id);
    }

    logger.debug("PE::");

    logger.debug("PE:: pageName              = " + pageName);

    logger.debug("PE:: pageLabel             = " + pageLabel);

    logger.debug("PE:: pageId                = " + pageId);

    logger.debug("PE:: pageMosUserId         = " + pageMosUserId);

    logger.debug("PE:: pageDealId            = " + pageDealId);

    logger.debug("PE:: pageDealCID           = " + pageDealCID);

    logger.debug("PE:: pageTxLevel           = " + txCopyLevel);

    logger.debug("PE:: dealPage              = " + dealPage);

    logger.debug("PE:: editable              = " + editable);

    logger.debug("PE:: modified              = " + modified);

    logger.debug("PE:: subPage               = " + subPage);

    logger.debug("PE:: dealEntry             = " + dealEntry);

    logger.debug("PE:: pageDealApplicationId = " + pageDealApplicationId);

    logger.debug("PE:: pageCriteriaId1      = " + pageCriteriaId1);

    logger.debug("PE:: pageCriteriaId2       = " + pageCriteriaId2);

    logger.debug("PE:: pageCriteriaId3       = " + pageCriteriaId3);

    logger.debug("PE:: pageCriteriaId4       = " + pageCriteriaId4);

    logger.debug("PE:: pageCriteriaId5       = " + pageCriteriaId5);

    logger.debug("PE:: pageCriteriaId6       = " + pageCriteriaId6);

    logger.debug("PE:: pageCondition1        = " + pageCondition1);

    logger.debug("PE:: pageCondition2        = " + pageCondition2);

    logger.debug("PE:: pageStateTable        = " + pageStateTable);

    logger.debug("PE:: pageState1            = " + pageState1);

    logger.debug("PE:: pageState2            = " + pageState2);

    logger.debug("PE:: isTaskNavigateMode    = " + taskNavigateMode);

    logger.debug("PE:: MWQTaskNameFilter     = " + mwqTaskNameFiler );

    logger.debug("PE:: taskName              = " + taskName);

    logger.debug("PE:: prevPageName          = " + prevPageName);

    logger.debug("PE:: nextPageName          = " + nextPageName);

    PageEntry parent = parentPage;

    if (parentPage == null)
    {
      logger.debug("PE:: parent                = null");
    }
    else
    {
      logger.debug("PE:: parent (id name)      = (" + parent.getPageId() + " "
        + parent.getPageLabel() + ")");
    }
  }
}
