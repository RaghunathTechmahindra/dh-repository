// Mc: MOS constants

package MosSystem;

/**
 * 29/Feb/2008 DVG #DG702 MLM x BDN merge adjustments
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 * 14/Mar/2007 DVG #DG586 FXP16091  Hide UserType Options from Sys Admins 
 * 03/May/2006 DVG #DG414 #3023  CR 303  funcional spec for the CR 303 (changes to document CR 040 )
 * 03/May/2006 DVG #DG412 #3022  CR 311  funcional spec for the CR 311 (changes to document CR 053)
 * 13/Jul/2005 DVG #DG258 #1793  Add Fee Tag to next build  #1791
 * 12/Jan/2005 DVG #DG126 BMO-CCAPS workflow notification
 * 23/AUG/2004-DVG #DG60 SCR#538 new implementation for Mail destination
 * 
 * BMO-CCAPS workflow notification
 *  *
 * @version 1.4 <br>
 * 
 * Date: 06/06/006 <br>
 * 
 * Author: NBC/PP Implementation Team <br>
 * 
 * Change: <br>
 * 
 * Added a constants for Generate pec email
 * MAIL_TO_SERVICEBRANCH,WORKFLOWTRIGGER_TRIGGERKEY_AUTOPREAPPROVAL<br>
 * @version 1.5  <br>
 * Date: MM/DD/YYY <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *added one constant PGNM_RATE_FLOAT_DOWN for RFD screen <br>
 *
 * * @version 1.6  <br>
 * Date: 07/18/006 <br>
 * Author: NBC/PP Implementation Team -Iteration 4<br>
 * Change:  <br>
 *added one constant ESCROWTYPE_ADMIN_FEE for Deal Compare <br>
 *

 * @version 1.4 <br>
 * 
 * Date: 06/06/006 <br>
 * 
 * Author: NBC/PP Implementation Team <br>
 * 
 * Change: <br>
 * 
 * Added a constants for Generate pec email
 * MAIL_TO_SERVICEBRANCH,WORKFLOWTRIGGER_TRIGGERKEY_AUTOPREAPPROVAL<br>
 * @version 1.5  <br>
 * Date: MM/DD/YYY <br>
 * Author: NBC/PP Implementation Team <br>
 * Change:  <br>
 *added one constant PGNM_RATE_FLOAT_DOWN for RFD screen <br>
 *
 * * @version 1.6  <br>
 * Date: 07/18/006 <br>
 * Author: NBC/PP Implementation Team -Iteration 4<br>
 * Change:  <br>
 *added one constant ESCROWTYPE_ADMIN_FEE for Deal Compare <br>
 *
 * @version 81  <br>
 * Date: 11/28/2006 <br>
 * Author: NBC/PP Implementation Team -Iteration 4<br>
 * Change:  <br>
 *added 5 constants for abbreviations for provinces 
 * @version 1.8  <br>
 * Date: 09-Jun-2006 <br>
 * Story:XS_11.11 & XS_11.6 <br>
 * Author: MCM Impl Team<br>
 * Change:  added 8 constants for Component types , Interest Compound Type Description's and default value
 * @version 1.9  <br>
 * Date: 16-Jun-2006 <br>
 * Story:XS_11.4 <br>
 * Author: MCM Impl Team<br>
 * Change:  added 26 constants for Province Id's and Month's in a year
 * @version 1.10  <br>
 * Date: 23-Jun-2006 <br>
 * Story:XS_11.3 <br>
 * Author: MCM Impl Team<br>
 * Change:  added 1 constant for PricingProfile rate code
 *          added constants for repayment types  
 * @version 1.11  <br>
 * Date: 1o-July-2008 <br>
 * Story:XS_2.39 <br>
 * Author: MCM Impl Team<br>
 * Change:  added BR_DEC
 */

public interface Mc
{
  public static final String DATASOURCE_NAME = "orcl";

  public static final int NUM_OF_PAGES = 7;

  public static final String US_THE_SESSION = "usTheSession";
  public static final String US_SAVED_PAGES = "gsSavedPages";
  public static final String DO_DEAL_SUMMARY_SNAPSHOT = "doDealSummarySnapShot";

  public static final String DO_UNDERWRITER_DEAL_SUMMARY_SNAPSHOT =
    "doUWDealSummarySnapShot";

  public static final int DISPLAY_METHOD_LOAD = 0;
  public static final int DISPLAY_METHOD_DISP = 1;

  // Mos User Type constants (values correspond to records in table userType)
  public static final int USER_TYPE_PASSIVE = 1;
  public static final int USER_TYPE_INV_ADMIN = 2;
  public static final int USER_TYPE_FUNDER = 2;
  public static final int USER_TYPE_JR_ADMIN = 3;
  public static final int USER_TYPE_ADMIN = 4;
  public static final int USER_TYPE_SR_ADMIN = 5;
  public static final int USER_TYPE_JR_UNDERWRITER = 6;
  public static final int USER_TYPE_UNDERWRITER = 7;
  public static final int USER_TYPE_SR_UNDERWRITER = 8;
  public static final int USER_TYPE_SUPERVISOR = 9;
  public static final int USER_TYPE_BRANCH_MGR = 10;
  public static final int USER_TYPE_REGION_MGR = 11;
  public static final int USER_TYPE_SENIOR_MGR = 12;
  public static final int USER_TYPE_SYS_ADMINISTRATOR = 13;

  //#DG586 this is the limit between regular users and special ones 
  // all special ones must be >= USER_TYPE_SPECIAL 
  public static final int USER_TYPE_SPECIAL = 80;    
  //--> Cervus II : Added new AUTO user type (AME spec. section 3)
  //--> By Billy 28Sept2004
  public static final int USER_TYPE_AUTO = 99;
  public static final int USER_TYPE_ECM = 98;
  public static final int USER_TYPE_DATAX = 97;
  public static final int USER_TYPE_SYSTEM = 0;
  //===================================================

  // Quasi-User Types (used in TASKGENERATE business rules)
  public static final int USER_DEAL_ADMINISTRATOR = 101;
  public static final int USER_DEAL_UNDERWRITER = 102;
  public static final int USER_SECOND_APPROVER = 103;
  public static final int USER_JOINT_APPROVER = 104;
  public static final int USER_DEAL_CURERENT_APPROVER = 105;
  public static final int USER_DEAL_FUNDER = 106;

  // Approval authority types (approvalAuthorityTypeId in table approval authority)
  public static final int APPROV_AUTH_TYPE_LOB = 0;
  public static final int APPROV_AUTH_TYPE_WHATOTHER = 1;

  // Approval authority types (approvalAuthorityTypeValue in table approval authority)
  public static final int APPROV_AUTH_TYPE_VALUE_A = 0;
  public static final int APPROV_AUTH_TYPE_VALUE_B = 1;
  public static final int APPROV_AUTH_TYPE_VALUE_C = 2;

  // Line of business types (lineOfBusinessId in table lineOfBusiness)
  public static final int LOB_A = 0;
  public static final int LOB_B = 1;
  public static final int LOB_B_MINUS = 2;
  public static final int LOB_C = 3;
  public static final int LOB_C_MINUS = 4;
  public static final int LOB_D = 5;
  
//***** Change by NBC Impl. Team - Version 81 - Start *****//
  
//***** Change by NBC Impl. Team - Version 81 - Start *****//

    /**
     * Variable to hold the abbreviation for province name - Nova Scotia
     * 
     * @value "NS"
     */
    public static final String PROVINCE_ABBR_DISCLOSURE_NS = "NS";

    /**
     * Variable to hold the abbreviation for province name - Newfoundland and Labrador
     * 
     * @value "NL"
     */
    public static final String PROVINCE_ABBR_DISCLOSURE_NL = "NL";

    /**
     * Variable to hold the abbreviation for province name - Alberta
     * 
     * @value "AB"
     */
    public static final String PROVINCE_ABBR_DISCLOSURE_AB = "AB";

    /**
     * Variable to hold the abbreviation for province name - Prince Edward Island
     * 
     * @value "PEI"
     */
    public static final String PROVINCE_ABBR_DISCLOSURE_PEI = "PEI";

    /**
     * Variable to hold the abbreviation for province name - Ontario
     * 
     * @value "ON"
     */
    public static final String PROVINCE_ABBR_DISCLOSURE_ON = "ON";

      
//  ***** Change by NBC Impl. Team - Version 81 - end *****//
  
      
//  ***** Change by NBC Impl. Team - Version 81 - end *****//
  
  // Party Types (values correspond top records in table partyType)
  public static final int PARTY_TYPE_BUSINESS_DEVELOPMENT_OFFICER = 0;
  public static final int PARTY_TYPE_SOLICITOR = 50;
  public static final int PARTY_TYPE_APPRAISER = 51;
  public static final int PARTY_TYPE_LIEN_HOLDER = 52;
  public static final int PARTY_TYPE_TRANSFERRING_LENDER = 53;
  public static final int PGNM_QUICKLINK_ADMININISTRATION = 538;
  public static final int PARTY_TYPE_SOLICITOR_NETWORK = 55;
  public static final int PARTY_TYPE_MUNICIPALITY = 1;
  public static final int PARTY_TYPE_ORIGINATION_BRANCH = 60;
  public static final int PARTY_TYPE_REFERRAL_SOURCE = 61;

    // ***** NBC/PP Implementation Team - Version 1.1 - START ******//
    // New party type
    public static final int PARTY_TYPE_SERVICE_BRANCH = 62;

    // ***** NBC/PP Implementation Team - Version 1.1 - END ******//
  // Condition Responsibility Role Types (values correspond to records in table
  // conditionResponsibilityRole)
    // SEAN Ticket #2497 Dec 09, 2005: to keep the consistence with the value in
    // database, comment out those not in database.  Also changed the
    // CRR_USER_TYPE_UNDERWRITER to 5, because the value in database is 5
  public static final int CRR_USER_TYPE_APPLICANT = 0;
  //public static final int CRR_USER_TYPE_JR_ADMIN = 2;
  //public static final int CRR_USER_TYPE_ADMIN = 3;
  //public static final int CRR_USER_TYPE_SR_ADMIN = 4;
  //public static final int CRR_USER_TYPE_JR_UNDERWRITER = 5;
  public static final int CRR_USER_TYPE_UNDERWRITER = 5;
  //public static final int CRR_USER_TYPE_SR_UNDERWRITER = 7;
  //public static final int CRR_USER_TYPE_SUPERVISOR = 8;
  //public static final int CRR_USER_TYPE_BRANCH_MGR = 9;
  //public static final int CRR_USER_TYPE_SENIOR_MGR = 10;
  public static final int CRR_PARTY_TYPE_SOLICITOR = 50;
  public static final int CRR_PARTY_TYPE_APPRAISER = 51;
  public static final int CRR_SOURCE_OF_BUSINESS = 90;
  public static final int CRR_PARTY_TYPE_LENDER = 5; 
    // SEAN Ticket #2497 Dec 09, 2005 END

  // Page Id constants (correspond to records in table page)
  public static final int PGNM_DEAL_HISTORY_ID = 1;
  public static final int PGNM_DEAL_SEARCH_ID = 2;
  public static final int PGNM_DEAL_NOTES_ID = 3;
  public static final int PGNM_DOC_TRACKING_ID = 4;
  public static final int PGNM_DEAL_SUMMARY_ID = 5;
  public static final int PGNM_SIGN_ON_ID = 6;
  public static final int PGNM_SIGNOFF_ID = 7;
  public static final int PGNM_INDIV_WORK_QUEUE_ID = 8;
  public static final int PGNM_MASTER_WORK_QUEUE_ID = 9;
  public static final int PGNM_DEAL_ENTRY_ID = 10;
  public static final int PGNM_CHANGE_PASSWORD_ID = 11;
  public static final int PGNM_PROPERTY_REVIEW_ID = 14;
  public static final int PGNM_CREDIT_REVIEW_ID = 15;
  public static final int PGNM_INCOME_ASSET_LIAB_REVIEW_ID = 16;
  public static final int PGNM_UNDERWRITER_WORKSHEET = 17;
//  ***** Change by NBC Impl. Team - Version 1.1 - Start*****//
    public static final int PGNM_RATE_FLOAT_DOWN  = 100;
//  ***** Change by NBC Impl. Team - Version 1.1 - End*****//
  public static final int PGNM_COMMITMENT_OFFER = 18;
  public static final int PGNM_CONDITIONS_REVIEW = 19;
  public static final int PGNM_DEAL_ENTRY = 21; // *******************
  public static final int PGNM_DEAL_MODIFICATION = 21;
  public static final int PGNM_MORTGAGE_INSURANCE = 22;
  public static final int PGNM_CLOSING_ACTIVITY = 26;
  public static final int PGNM_FUNDING_ACTIVITY = 27;
  public static final int PGNM_PRODUCTIVITY_STATISTICS = 29;
  public static final int PGNM_USER_PROFILES = 30;
  public static final int PGNM_INTEREST_RATES_ADMIN = 31;
  public static final int PGNM_DEAL_RESOLUTION = 38;
  public static final int PGNM_COMMITMENT_REVERSAL = 39;
  public static final int PGNM_DEAL_PROGRESS = 40;
  public static final int PGNM_PORTFOLIO_STATISTICS = 42;
  public static final int PGNM_FEE_REVIEW = 43;
  public static final int PGNM_PARTY_ADD = 44;
  public static final int PGNM_PARTY_SEARCH = 45;
  public static final int PGNM_PARTY_SUMMARY = 46;
  public static final int PGNM_PARTY_REVIEW = 47;
  public static final int PGNM_BRIDGE_REVIEW = 48;
  public static final int PGNM_SOB_SEARCH = 49;
  public static final int PGNM_APPRAISER_REVIEW_FACTORY = 50; 
  public static final int PGNM_APPRAISER_REVIEW = 70; 
  public static final int PGNM_APPRAISER_REVIEW_DJ = 71;
  public static final int PGNM_DECISION_MODIFICATION = 51;
  public static final int PGNM_TRANSACTION_HISTORY = 52;
  public static final int PGNM_USER_ADMININISTRATION = 53;
  public static final int PGNM_RESOLUTION_REVIEW = 54;
  public static final int PGNM_SENTINEL = 55;

  // New screens for SOB handling
  public static final int PGNM_SOB_ADD = 56;
  public static final int PGNM_SFIRM_ADD = 57;
  public static final int PGNM_SOB_REVIEW = 58;
  public static final int PGNM_SFIRM_REVIEW = 59;

  //// SYNCADD
  public static final int PGMN_SOB_RESUBMISSION = 60;

  //--DJ_LDI_CR--start--//
  public static final int PGMN_LIFE_DISABILITY_INSURANCE = 61;
  //--DJ_LDI_CR--end--//

  //***** Change By NBC/PP Implementation Team - GCD - START *****//
  public static final int PGNM_CREDIT_DECISION = 68;
  //***** Change By NBC/PP Implementation Team - GCD - END *****//
  
  //--DJ CS 2--start--//
  public static final int PGMN_CREDIT_RESPONSE = 66;
  //--DJ CS 2--end--//

  // sub-pages - id >= 500
  public static final int PGNM_APPLICANT_ENTRY = 500;
  public static final int PGNM_PROPERTY_ENTRY = 501;
  public static final int PGNM_DOC_TRACKING_DETAIL = 504;
  public static final int PGNM_INTEREST_RATE_HISTORY = 531;
  public static final int INTEREST_RATES_HISTORY = 531;
  public static final int PGNM_INTEREST_RATE_UPDATE = 532;
  public static final int INTEREST_RATES_UPDATE = 532;
  public static final int PGNM_CONDITIONS_DETAIL = 519;

  public static final int PGNM_TASK_REASSIGN = 525;

  public static final int PGNM_COMPONENT_DETAILS = 535;
  
  /***************MCM Impl Team******************************************/
  public static final int PGNM_COMPONENT_DETAILS_DEAL_SUMMARY = 536;
  /***************MCM Impl Team******************************************/
  
  public static final int TASK_REASSIGN_OPTIONS_LENGTH = 2;
  //***** Change By NBC/PP Implementation Team - GCD- START *****//
  public static final int PGNM_CD_APPLICANT_DETAILS = 533;
  //***** Change By NBC/PP Implementation Team - GCD - END *****//
  public static final int PGNM_MASTER_WORK_QUEUE_SEARCH_ID = 537;
  
  // Deal status codes:
  //
  // Note resolution conditions denied and collasped backed by deal attribute resolution with value:
  //        0 - Deal UW resolution
  //        1 - Higher/joint approver resolution
  //        2 - Deal UW reviewed (of 1)

  public static final int DEAL_INCOMPLETE = 0;
  public static final int DEAL_RECEIVED = 1;
  public static final int DEAL_IN_UNDEREWRITING = 2;
  public static final int DEAL_HIGHER_APPROVAL_REQUESTED = 3;
  public static final int DEAL_JOINT_APPROVAL_REQUESTED = 4;
  public static final int DEAL_EXTERNAL_APPROVAL_REQUESTED = 5;
  public static final int DEAL_REVIEW_APPROVED_CHANGES = 6; // Request for Higher/Joint approval after (requested) changes made
  public static final int DEAL_APPROVE_WITH_CHANGES = 7; // Request to Deal UW to perform changes requested by higher/joint approver
  public static final int DEAL_APPROVAL_RECOMMENDED = 8; // primarilly AAG usage
  public static final int DEAL_FINAL_APPROVAL_RECOMMENDED = 9; // primarilly AAG usage
  public static final int DEAL_EXTERNAL_APPROVAL_REVIEW = 10; // after higher/joint approval when ext. approval required (back to Deal UW)
  public static final int DEAL_APPROVED = 11;
  public static final int DEAL_PRE_APPROVAL_OFFERED = 12;
  public static final int DEAL_COMMIT_OFFERED = 13;
  public static final int DEAL_OFFER_ACCEPTED = 14;
  public static final int DEAL_PRE_APPROVAL_FIRM = 15;
  public static final int DEAL_CONDITIONS_OUT = 16;
  public static final int DEAL_CONDITIONS_SATISFIED = 17;
  public static final int DEAL_CLOSING_INSTRUCTED_CONDITIONS_OUT = 18; // CHANGED
  public static final int DEAL_CLOSING_INSTRUCTED_CONDITIONS_SATISFIED = 19; // NEW
  public static final int DEAL_POST_DATED_FUNDING = 20;
  public static final int DEAL_FUNDED = 21;
  public static final int DEAL_TO_SERVICING = 22;
  public static final int DEAL_COLLAPSED = 23;
  public static final int DEAL_DENIED = 24;
  public static final int DEAL_FUNDING_REVERSED = 25; //  NEW

  // Deal status code categories:
  //

  public static final int DEAL_STATUS_CATEGORY_PRE_DECISION = 0;
  public static final int DEAL_STATUS_CATEGORY_DECISION = 1;
  public static final int DEAL_STATUS_CATEGORY_POST_DECISION = 2;
  public static final int DEAL_STATUS_CATEGORY_FINAL_DISPOSITION = 3;

  // Deal Resolution conditions
  //

  public static final int DEAL_RES_CONDITION_NO_CONDITION = 0;
  public static final int DEAL_RES_CONDITION_DEAL_UNDERWRITER_DENIED = 1;
  public static final int DEAL_RES_CONDITION_HIGHER_APPROVER_DENIED = 2;
  public static final int DEAL_RES_CONDITION_DEAL_COLLAPSED = 3;
  public static final int DEAL_RES_CONDITION_RESOLUTION_CONFIRMED = 4;

    // business rule prefixes 
  public static final String AAG_RULE_PREFIX = "AAG";
  public static final String AGP_RULE_PREFIX = "AGP";
  public static final String CMHC_RULE_PREFIX = "CMHC";
  public static final String BUZ_RULE_AM = "AM-%";      //#DG702 
  public static final String BUZ_RULE_DE = "DE-%";      //#DG702 
  public static final String BUZ_RULE_DEA = "DEA-%";        //#DG702 
  public static final String BUZ_RULE_DEP = "DEP-%";        //#DG702
  public static final String BUZ_RULE_DEC = "DEC-%";        //MCM team
  public static final String BUZ_RULE_DI = "DI-%";
  public static final String BUZ_RULE_IC = "IC-%";
  public static final String BUZ_RULE_MI = "MI-%";
  public static final String BUZ_RULE_UN = "UN-%";

  // Deal History transaction types (corresponding to records in table TransactionType)
  public static final int DEAL_TX_TYPE_NORMAL_ACTIVITY = 0;
  public static final int DEAL_TX_TYPE_DATA_CHANGE = 1;
  public static final int DEAL_TX_TYPE_INTERFACE = 2;
  public static final int DEAL_TX_TYPE_EVENT = 3;
  //--DocCentral_FolderCreate--21Sep--2004--start--//
  public static final int DEAL_TX_TYPE_DOCCENTRAL_FOLDER_CREATION = 4;
  //--DocCentral_FolderCreate--21Sep--2004--end--//

  // Loan purpose codes
  public static final int DEAL_PURPOSE_PURCHASE = 0;
  public static final int DEAL_PURPOSE_TRANSFER = 1;
  public static final int DEAL_PURPOSE_RENEW_REGULAR = 2;
  public static final int DEAL_PURPOSE_RENEW_EARLY = 3;
  public static final int DEAL_PURPOSE_REFI_EXTERNAL = 4;
  public static final int DEAL_PURPOSE_REFI_EXISTING_CLIENT = 5;
  public static final int DEAL_PURPOSE_ETO_EXTERNAL = 6;
  public static final int DEAL_PURPOSE_ETO_EXISTING_CLIENT = 7;
  public static final int DEAL_PURPOSE_CONSTRUCTION = 8;
  public static final int DEAL_PURPOSE_ASSUMPTION = 9;
  public static final int DEAL_PURPOSE_INCREASED_ASSUMPTION = 10;
  public static final int DEAL_PURPOSE_PORTABLE = 11;
  public static final int DEAL_PURPOSE_PORTABLE_INCREASE = 12;
  public static final int DEAL_PURPOSE_PORTABLE_DECREASE = 13;
  public static final int DEAL_PURPOSE_COVENANT_CHANGE = 14;
  public static final int DEAL_PURPOSE_PARTIAL_DISCHARGE = 15;
  public static final int DEAL_PURPOSE_REPLACEMENT = 16;
  public static final int DEAL_PURPOSE_PURCHASE_WITH_IMPROVEMENTS = 17;
  public static final int DEAL_PURPOSE_DEFICIENCY_SALE = 18;   //#DG412
  public static final int DEAL_PURPOSE_WORKOUT = 19;   //#DG412

  // Reference Deal Types
  public static final int DEAL_REF_TYPE_BLANK = 0;
  public static final int DEAL_REF_TYPE_RESURRECTED_FROM = 1;
  public static final int DEAL_REF_TYPE_RESURRECTED_TO = 2;

  //// SYNCADD.
  public static final int DEAL_REF_TYPE_ORIGINAL_DEAL_RESUBMIT = 3;
  public static final int DEAL_REF_TYPE_RESUBMITTED_AS_DEAL = 4;

  // Borrower Types  UPDATED: 15\5\01
  public static final int BT_BORROWER = 0;
  public static final int BT_GUARANTOR = 1;
  public static final int BT_RELEASED_CONVENANTOR = 2;
  public static final int BT_BUILDER = 3;

  // Borrower Address Types
  public static final int BT_ADDR_TYPE_CURRENT = 0;
  public static final int BT_ADDR_TYPE_PREVIOUS = 1;
  public static final int BT_ADDR_TYPE_SALE_PROPERTY = 2;

  // Document status tracking codes (correspond to records in table documentStatus
  public static final int DOC_STATUS_OPEN = 0;

  public static final int DOC_STATUS_REQUESTED = 1;
  public static final int DOC_STATUS_RECEIVED = 2;
  public static final int DOC_STATUS_APPROVED = 3;
  public static final int DOC_STATUS_INSUFFICIENT = 4;
  public static final int DOC_STATUS_WAIVED = 5;
  public static final int DOC_STATUS_REJECTED = 6;
  public static final int DOC_STATUS_CAUTION = 7;
  public static final int DOC_STATUS_WAIVE_REQUESTED = 8;

  //Contants Received From Leniod For Deal Fee Review Screen
  public static final int FEE_PAYMENT_DATE_CURRENT = 0;
  public static final int FEE_PAYMENT_DATE_CURRENT_AND_TEN = 1;
  public static final int FEE_PAYMENT_DATE_OFFER_RETURN = 2;
  public static final int FEE_PAYMENT_DATE_FUNDS_ADVANCED = 3;

  public static final int FEE_GENERATION_TYPE_SYSTEM = 0;
  public static final int FEE_GENERATION_TYPE_USER = 1;

  //MIIndicator table - brad jul 24 2000
  public static final int MII_NOT_REQUIRED = 0;
  public static final int MII_REQUIRED_STD_GUIDELINES = 1;
  public static final int MII_UW_REQUIRED = 2;
  public static final int MII_UW_WAIVED = 3;
  public static final int MII_APPLICATION_FOR_LOAN_ASSESSMENT = 4;
  public static final int MII_MI_PRE_QUALIFICATION = 5;

  // Mortgage Insurers (table MortgageInsurer)
  public static final int MI_INSURER_BLANK = 0;
  public static final int MI_INSURER_CMHC = 1;
  public static final int MI_INSURER_GE = 2;
  public static final int MI_INSURER_AIG_UG = 3; 
  public static final int MI_INSURER_PMI = 4;

  //aatcha start
  public static final int MI_INSURER_AIGUG = 3;
  public static final int MI_WEIGTH_CMHC = 55;
  public static final int MI_WEIGTH_GE = 35;
  public static final int MI_WEIGTH_AIGUG = 20;
  //atcha end

  // Mortgage Insurance Payor (table MortgageInsurancePayor)
  public static final int MI_PAYOR_BLANK = 0;
  public static final int MI_PAYOR_BORROWER = 1;
  public static final int MI_PAYOR_LENDER = 2;

  // Mortgage Insurance Types (table MortgageInsuranceType)
  public static final int MI_TYPE_BLANK = 0;
  public static final int MI_TYPE_FULL_SERVICE = 1;
  public static final int MI_TYPE_BASIC_SERVICE = 2;
  public static final int MI_TYPE_TRANSFER_POLICY = 3;
  public static final int MI_TYPE_GE_EQUITY_TAKE_OUT = 4;

  //--> Ticket#149 - DJ007 - Add GE Capital 50-50 mortgage insurance products
  //--> By Billy 05Dec2003
  public static final int MI_TYPE_GE_EQUITY_TAKE_OUT_5050 = 5;
  public static final int MI_TYPE_GE_5050_PARTIAL_COVERAGE = 6;
  //--> Ticket#361 -- Added new MI Products : "GE Cashback" and "CMHC Flex Down"
  //--> By Billy 27April2004
  public static final int MI_TYPE_GE_CASHBACK = 7;
  public static final int MI_TYPE_CMHC_FLEXDOWN = 8;
  public static final int MI_TYPE_ALT_A_UPFRONT = 9;
  public static final int MI_CREDIT_ASSIST = 11; 
  public static final int MI_TYPE_VACATION = 17;
  public static final int MI_CMHC_SELF_EMPLOYED_SIMPLIFIED = 23;
  
  //=========================================================================

  // Mortgage Insurance Status (table MIstatus)
  public static final int MI_STATUS_BLANK = 0;
  public static final int MI_STATUS_INITIAL_REQUEST_PENDING = 1;
  public static final int MI_STATUS_INITIAL_REQUEST_SUBMITTED = 2;
  public static final int MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER = 3;
  public static final int MI_STATUS_CRITICAL_ERRORS_FOUND_BY_INSURER = 4;
  public static final int MI_STATUS_ERRORS_RECEIVED_FROM_INSURER = 5;
  public static final int MI_STATUS_ERRORS_RECEIVED_BY_INSURER = 6;
  public static final int MI_STATUS_ERRORS_CORRECTED_AND_PENDING = 7;
  public static final int MI_STATUS_CORRECTIONS_SUBMITTED = 8;
  public static final int MI_STATUS_MANUAL_UNDERWRITING_REPLY = 9;
  public static final int MI_STATUS_MANUAL_UNDERWRITING_REQUIRED = 10;
  public static final int MI_STATUS_CANCELLATION_PENDING = 11;
  public static final int MI_STATUS_CANCELLATION_SUBMITTED = 12;
  public static final int MI_STATUS_CANCELLATION_CONFIRMATION_REC = 13;
  public static final int MI_STATUS_CANCELLED = 14;
  public static final int MI_STATUS_APPROVAL_RECEIVED = 15;
  public static final int MI_STATUS_APPROVED = 16;
  public static final int MI_STATUS_DENIED_RECEIVED = 17;
  public static final int MI_STATUS_DENIED = 18;
  public static final int MI_STATUS_RISKED_RECEIVED = 19;
  public static final int MI_STATUS_RISKED = 20;
  public static final int MI_STATUS_CHANGES_PENDING = 21;
  public static final int MI_STATUS_CHANGES_SUBMITTED = 22;
  public static final int MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED = 23;

  //--BMO_MI_CR--start--//
  //// Indicator table (used to customize MI process outside BXP.Pre-Qualification
  public static final int MI_INDICATOR_NOT_REQUIRED = 0;
  public static final int MI_INDICATOR_REQUIRED_STD_GUIDELINES = 1;
  public static final int MI_INDICATOR_UW_REQUIRED = 2;
  public static final int MI_INDICATOR_UW_WAIVED = 3;
  public static final int MI_INDICATOR_APPLICATION_FOR_LOAN_ASSESSMENT = 4;
  public static final int MI_INDICATOR_PRE_QUALIFICATION = 5;

  public static final int MI_HANDLED_OUTSIDE_XPRESS_NO = 0;
  public static final int MI_HANDLED_OUTSIDE_XPRESS_YES = 1;

  //--BMO_MI_CR--end--//

  
  //FeeStatus table - brad jul 24 2000
  public static final int FEE_STATUS_REQUIRED = 0;
  public static final int FEE_STATUS_RECIEVED = 1;
  public static final int FEE_STATUS_WAIVED = 2;
  public static final int FEE_STATUS_UPDATE = 3;

  //FeeType table brad jan 31 2001
  public static int FEE_TYPE_APPRAISAL_FEE = 0;
  public static int FEE_TYPE_BRIDGE_FEE = 1;
  public static int FEE_TYPE_BUYDOWN_FEE = 2;
  public static int FEE_TYPE_CMHC_FEE = 3;
  public static int FEE_TYPE_STANDBY_FEE = 4;
  public static int FEE_TYPE_APPRAISAL_FEE_PAYABLE = 5;
  public static int FEE_TYPE_CMHC_FEE_PAYABLE = 6;
  public static int FEE_TYPE_AGENT_REFERRAL_FEE = 7;
  public static int FEE_TYPE_FINDER_FEE = 8;
  public static int FEE_TYPE_APPLICATION_FEE = 9;
  public static int FEE_TYPE_INSPECTION_FEE = 10;
  public static int FEE_TYPE_CMHC_PREMIUM = 11;
  public static int FEE_TYPE_CMHC_PREMIUM_PAYABLE = 12;
  public static int FEE_TYPE_CMHC_PST_ON_PREMIUM = 13;
  public static int FEE_TYPE_CMHC_PST_ON_PREMIUM_PAYABLE = 14;
  public static int FEE_TYPE_GE_CAPITAL_FEE = 15;
  public static int FEE_TYPE_GE_CAPITAL_FEE_PAYABLE = 16;
  public static int FEE_TYPE_GE_CAPITAL_PREMIUM = 17;
  public static int FEE_TYPE_GE_CAPITAL_PREMIUM_PAYABLE = 18;
  public static int FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM = 19;
  public static int FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM_PAYABLE = 20;
  public static int FEE_TYPE_PROPERTY_TAX_HOLDBACK = 21;
  public static int FEE_TYPE_APPRAISAL_FEE_ADDITIONAL = 22;
  public static int FEE_TYPE_CANCELLATION_FEE = 23;
  public static int FEE_TYPE_CASH_BACK = 24;
  public static int FEE_TYPE_PROPERTY_TAX_HOLDBACK_INTERIM = 25;
  public static int FEE_TYPE_PROPERTY_TAX_HOLDBACK_FINAL = 26;
  public static int FEE_TYPE_COUREER_FEE = 27;
  public static int FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE = 28;
  public static int FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE = 29;
  public static int FEE_TYPE_WIRE_FEE = 30;
  public static int FEE_TYPE_APP_FEE_NOT_CAPITALIZED = 31;
  public static int FEE_TYPE_REBATE = 32;                                       //#DG258
  public static int FEE_TYPE_TITLE_INSURANCE_FEE = 33;                  //#DG258
  public static int FEE_TYPE_APPRAISAL_REBATE_FEE = 34; 
  public static int FEE_TYPE_TITLE_INSURANCE_REBATE_FEE = 35; 
  public static int FEE_TYPE_REFUNDABLE_COMMITMENT_REBATE_FEE = 36; 
  
  public static int FEE_TYPE_AIG_UG_FEE = 37;  
  public static int FEE_TYPE_AIG_UG_FEE_PAYABLE = 38;
  public static int FEE_TYPE_AIG_UG_PREMIUM = 39;
  public static int FEE_TYPE_AIG_UG_PREMIUM_PAYABLE = 40;
  public static int FEE_TYPE_AIG_UG_PST_ON_PREMIUM = 41;
  public static int FEE_TYPE_AIG_UG_PST_ON_PREMIUM_PAYABLE = 42;
  
  public static int FEE_TYPE_CERVUS_EXISTING_MORTGAGE_PAYABLE = 43;
  public static int FEE_TYPE_CERVUS_PENALTY_PAYABLE_FEE = 44;
  public static int FEE_TYPE_SERVUS_DISCHARGE_PAYABLE_FEE = 45;
  public static int FEE_TYPE_SERVUS_ADMINISTRATION_FEE = 46;
  public static int FEE_TYPE_SERVUS_MISCS_PAYABLE_FEE = 47;
  public static int FEE_TYPE_CTFS_SOLICITOR_FEE = 48;
  public static int FEE_TYPE_CTFS_CENTRIA_FEE = 49;
  
  public static int FEE_TYPE_BROKER_FEE = 50;                   
  public static int FEE_TYPE_BONUS_FEE = 52;                                
  public static int FEE_TYPE_ORIGINATION_FEE = 53;          
  public static int FEE_TYPE_TERANET_FEE = 54;                 

  //FeePayorType table brad jan 31 2001
  public static int FEE_PAYOR_TYPE_NONE = 0;
  public static int FEE_PAYOR_TYPE_SOURCE_OF_BUSINESS = 1;
  public static int FEE_PAYOR_TYPE_SOURCE_FIRM = 2;
  public static int FEE_PAYOR_TYPE_SOLICITOR = 3;
  public static int FEE_PAYOR_TYPE_APPRAISER = 4;
  public static int FEE_PAYOR_TYPE_MORTGAGE_INSURER = 5;

  //FeePaymentDateType table brad jan 31 2001
  public static int FEE_PAYMENT_DATE_TYPE_CURRENT_DATE = 0;
  public static int FEE_PAYMENT_DATE_TYPE_CURRENT_DATE_PLUS_10_DAYS = 1;
  public static int FEE_PAYMENT_DATE_TYPE_OFFER_RETURN_DATE = 2;
  public static int FEE_PAYMENT_DATE_TYPE_FUNDS_ADVANCED_DATE = 3;

  //#DG600 language unknown
  public static final int LANGUAGE_UNKNOW = -1;

  //language preference  table brad jul 25 2000
  public static final int LANGUAGE_PREFERENCE_ENGLISH = 0;
  public static final int LANGUAGE_PREFERENCE_FRENCH = 1;
  public static final int LANGUAGE_PREFERENCE_SPANISH = 2;

  // payment frequency constants
  public static final int PAY_FREQ_MONTHLY = 0;
  public static final int PAY_FREQ_SEMIMONTHLY = 1;
  public static final int PAY_FREQ_BIWEEKLY = 2;
  public static final int PAY_FREQ_ACCELERATED_BIWEEKLY = 3;
  public static final int PAY_FREQ_WEEKLY = 4;
  public static final int PAY_FREQ_ACCELERATED_WEEKLY = 5;

  // income types: updated 15/5/01
  public static final int INCOME_SALARY = 0;
  public static final int INCOME_HOURLY = 1;
  public static final int INCOME_HOURLY_PLUS_COMMISSION = 2;
  public static final int INCOME_COMMISSION = 3;
  public static final int INCOME_INTEREST_INCOME = 4;
  public static final int INCOME_CHILD_SUPPORT = 5;
  public static final int INCOME_SELF_EMP = 6;
  public static final int INCOME_PENSION = 7;
  public static final int INCOME_ALIMONY = 8;
  public static final int INCOME_RENTAL_SUBTRACT = 9;
  public static final int INCOME_RENTAL_ADD = 10;
  public static final int INCOME_OTHER = 11;
//****** Changed by NBC/PP Implementation Team - GCD - START ******//
  public static final int INCOME_OTHER_EMPLOYMENT_INCOME = 12;
//****** Changed by NBC/PP Implementation Team - GCD - END ******//
  // credit review

  public static final int CREDIT_BUREAU_NONE = 0;

  public static final int BANKRUPTCY_STATUS_NEVER_BANKRUPT = 0;
  public static final int BANKRUPTCY_STATUS_FULLY_DISCHARGED = 1;
  public static final int BANKRUPTCY_STATUS_PARTIALLY_DISCHARGED = 2;

  public static final int NSF_OCCURENCE_NO_NSF = 0;
  public static final int NSF_OCCURENCE_SOME_NSF = 1;
  public static final int NSF_OCCURENCE_SEVERE_NSF = 2;

  public static final int PAYMENT_HISTORY_EXCELLENT = 0;
  public static final int PAYMENT_HISTORY_GOOD = 1;
  public static final int PAYMENT_HISTORY_FAIR = 2;
  public static final int PAYMENT_HISTORY_POOR = 3;

  public static final int BORROWER_GENERAL_STATUS_NEITHER_DELQ_NSF = 0;
  public static final int BORROWER_GENERAL_STATUS_SOME_DELQ = 1;
  public static final int BORROWER_GENERAL_STATUS_SOME_NSF = 2;
  public static final int BORROWER_GENERAL_STATUS_SEVERE_DELQ_PAST = 3;
  public static final int BORROWER_GENERAL_STATUS_CURR_DELQ = 4;
  public static final int BORROWER_GENERAL_STATUS_SEVERE_DELQ_CURR = 5;
  public static final int BORROWER_GENERAL_STATUS_HIGH_RISK = 6;

  // screen load target constants
  public static final int TARGET_NONE = 0;
  public static final int TARGET_DE_DOWNPAYMENT = 1;
  public static final int TARGET_DE_ESCROW = 2;

  public static final int TARGET_APP_ADDRESS = 3;
  public static final int TARGET_APP_EMPLOYMENT = 4;
  public static final int TARGET_APP_INCOME = 5;
  public static final int TARGET_APP_CREDIT = 6;
  public static final int TARGET_APP_ASSET = 7;
  public static final int TARGET_APP_LIAB = 8;
  public static final int TARGET_APP_CREDIT_BUREAU_LIAB = 10;

  public static final int TARGET_PRO_PROPERTY_EXPENSE = 9;

  //// SYNCADD.
  public static final int TARGET_CONREVIEW_STD = 11;
  public static final int TARGET_CONREVIEW_CUST = 12;

  //--DJ_CR136--23Nov2004--start--//
  public static final int TARGET_LD_INSUREONLYAPPLICANT= 13;
  //--DJ_CR136--23Nov2004--end--//

    //  ***** Change by NBC Impl. Team - Version 1.7 - End *****//
    public static final int TARGET_APP_IDENTIFICATION = 14; 
   //  ***** Change by NBC Impl. Team - Version 1.7 - End *****//

  public static final String TARGET_TAG = "<a name = \"loadtarget\">";

  public static final String PROPERTIES_FILE_NAME = "MOSdBConnTestProperty";

  // Individual Work Queue :: sort order literals (corresponding to sort order ids)
  // Individual Work Queue Page State Related constants ...

  // sort order ids
  public static final int IWQ_SORT_ORDER_PRIORITY = 0;
  public static final int IWQ_SORT_DUE_DATE = 1;
  public static final int IWQ_SORT_DEAL_NUMBER_ASC = 2;
  public static final int IWQ_SORT_DEAL_NUMBER_DEC = 3;
  public static final int IWQ_SORT_TASK_STATUS = 4;
  public static final int IWQ_SORT_SOURCE_BORROWER = 5;
  public static final int IWQ_SORT_SOURCE = 6;
  public static final int IWQ_SORT_SOURCE_FIRM = 7;
  public static final int IWQ_SORT_TASK_DESC = 8;
  public static final int IWQ_SORT_TIME_WARNING = 9;
  public static final int IWQ_SORT_WARNING = 10;
  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/20/2004
  public static final int IWQ_SORT_DEAL_STATUS = 12;
  //-- ========== SCR#750   ends ========== --//

  //--> Ticket#139 : BMO - Ability to prioritize Broker (SOB)
  //--> By Billy 28Nov2003
  public static final int IWQ_SORT_SOURCE_PRIORITY = 11;

  // these attributes are dealhold, automMI and dealdenial process.
  public static final int DEAL_HOLD_REASON_OTHERS = 0;
  public static final double DEAL_COMBINEDLTV = 75;
  //========================================================

  //---Release2.1--//
  //Modified to get the SortOrderOption Labels from Resource Bundle
  //--> By Billy 07Nov2002
  /*
     public static final String[] IWQ_SORT_ORDER_OPTIONS  =  {
     "Priority (default)",
     "Due Date/Time",
     "Deal Number ASC",
     "Deal Number DEC",
     "Status",
     "Borrower Name",
     "Source",
     "Source Firm",
     "Task Description",
     "Time Warning",
     "Warning"
     };
   */
  //===============================================================

  // Deal Resolution :: resolution options labels
  public static final String[] DEAL_RESOLUTION_OPTIONS_LABELS =
    {

    "Approve",
    "Hold Pending Info",
    "Deny",
    "Collaspe",
  };

  // Deal Resolution :: resolution options values
  public static final int[] DEAL_RESOLUTION_OPTIONS_VALUES =
    {

    0,
    0,
    0,
    0,
  };

  // Deal Resolution - Denial Labels :: resolution options labels
  public static final String[] DEAL_DENIAL_OPTIONS_LABELS =
    {

    "",
    "Credit",
    "Income",
    "Conditions",
    "Other",
  };

  // Deal Resolution - Denial Deal Status Codes :: resolution options values
  public static final int[] DEAL_DENIAL_OPTIONS_VALUES =
    {

    -1,
    -1,
    -1,
    -1,
    -1,
  };

  //denialreason pl table:
  public static final int DENIAL_REASON_OTHER = 0;
  public static final int DENIAL_REASON_PREVIOUS_BANKRUPT = 1;
  public static final int DENIAL_REASON_CREDIT_HISTORY = 2;
  public static final int DENIAL_REASON_NO_CREDIT_BUREAU = 3;
  public static final int DENIAL_REASON_INCOME_TYPE = 4;
  public static final int DENIAL_REASON_INCOME_INSUFFICIENT = 5;
  public static final int DENIAL_REASON_INCOME_VERIFICATION = 6;
  public static final int DENIAL_REASON_INCOME_MULTI = 7;
  public static final int DENIAL_REASON_JOB_STABILITY = 8;
  public static final int DENIAL_REASON_NET_WORTH = 9;
  public static final int DENIAL_REASON_DOWN_PAYMENT = 10;
  public static final int DENIAL_REASON_MULTI_COVENANT = 11;
  public static final int DENIAL_REASON_GDS_TDS = 12;
  public static final int DENIAL_REASON_PROPERTY = 13;
  public static final int DENIAL_REASON_PRODUCT_UNAVAILABLE = 14;
  public static final int DENIAL_REASON_LAND_STATUS_UNCLEAR = 15;
  public static final int DENIAL_REASON_EXTERNAL_LENDER_REFUSED = 16;

  //// SYNCADD.
  public static final int DENIAL_REASON_RETURN_TO_BROKER = 17;
  public static final int DENIAL_REASON_SOURCE_RESUBMISSION = 18;
  public static final int DENIAL_REASON_IC_REJECT = 19;
  
  //MCM Impl Team Bugfix:FXP22615
  public static final int DENIAL_REASON_FAILURE_PRODUCT_DEAL = 100; 
  public static final int DENIAL_REASON_FAILURE_PRODUCT_COMPONENT = 102; 
  public static final int DENIAL_REASON_FAILURE_RATE_DEAL = 101;
  public static final int DENIAL_REASON_FAILURE_RATE_COMPONENT = 103; 
  
  // Added new Status 20 to indicate Dup deal adoption.
  // -- By Billy 12Aug2002
  public static final int DENIAL_REASON_NEW_DEAL_ADOPTED = 20;

  // Master Work Queue Page State Related constants ...

  // sort order ids
  public static final int MWQ_SORT_DUE_DATE = 0;
  public static final int MWQ_SORT_ORDER_PRIORITY = 1;
  public static final int MWQ_SORT_DEAL_NUMBER = 2;
  public static final int MWQ_SORT_DEAL_NUMBER_DSC = 3;
  public static final int MWQ_SORT_TASK_STATUS = 4;
  public static final int MWQ_SORT_BORROWER = 5;
  public static final int MWQ_SORT_WARNING = 6;
  public static final int MWQ_SORT_TASK_DESC = 7;
  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/20/2004
  public static final int MWQ_SORT_DEAL_STATUS = 8;
  // Master Work Queue :: sort order literals (corresponding to sort order ids)
  //    public static final int MWQ_SORT_ORDER_OPTIONS_LENGTH = 8;
  //-- ========== SCR#750   ends ========== --//

  //--Release2.1--//
  //// This static initialized is moved to the BXGeneric.properties
  //// in order to translate on fly via the BXResource bundle.
  /**
     public static final String[] MWQ_SORT_ORDER_OPTIONS  =  {

     "Due Date/Time(Default)",
     "Priority ",
     "Deal Number ASC",
     "Deal Number DEC",
     "Status",
     "Borrower Name",
     "Warning",
     "Task Description"
     };
   **/

  public static final int MWQ_STATUS_TASK_ALL = 9;

  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/21/2004
  //        public static final int[] MWQ_STATUS_TASK_MAPPER =
  //            { Sc.TASK_OPEN, Sc.TASK_COMPLETE, Sc.MWQ_STATUS_TASK_ALL };
  //----SCR#1148--start--28Mar2005--//
  // Vlad ==> it should not be 0 otherwis the value will mismatch with unused type of task.
  // Moreover, it was not logically right from Product to add 'quasi' ON-HOLD task to this
  // filter. In reality it is not a task, but deal related filtering. It correlates to different
  // clause (deal), not task clause in reality. On the other hand, it is not a deal status either.
  //public static final int MWQ_TASK_NOT_ONHOLD = 0;
  public static final int MWQ_TASK_NOT_ONHOLD = 10;
  //----SCR#1148--end--28Mar2005--//

  public static final int[] MWQ_STATUS_TASK_MAPPER =
  {
          Sc.TASK_OPEN,  //1
          Sc.TASK_COMPLETE, //3
          Mc.MWQ_TASK_NOT_ONHOLD, //10
          Mc.MWQ_STATUS_TASK_ALL //9
  };
  //-- ========== SCR#750   ends ========== --//

  //--Release2.1--//
  //// This static initialized is moved to the BXGeneric.properties
  //// in order to translate on fly via the BXResource bundle.
  /**
     public static final String[] MWQ_STATUS_TASK_OPTIONS  =
     {
     "Opened ",
     "Complete ",
     "All"          //Always goes last
     };
   **/

  public static final String[] MONTHS_WITH_BLANK_ENTRY =
    {
    "",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec" //first option is blank as usual.
  };

  //-- ========== SCR#750 begins ========== --//
  //-- by Neil on Dec/21/2004
  //    public static final int MWQ_STATUS_TASK_OPTIONS_LENGTH = 3;
  public static final int MWQ_STATUS_TASK_OPTIONS_LENGTH = 4;
  //-- ========== SCR#750   ends ========== --//

  //Database related Names are defined in to contant names here
  //bcose there was name and status code changes happens freq
  //avoid hard coding any db col name in to Handler classes
  public static final String DB_NAME = "MOS";

  //borrower table
  public static final String BORROWER_TBL = "BORROWER";
  public static final String BR_TYPEID_COL = "BORROWERTYPEID";

  //doc tracking Table
  public static final String DOCTRACK_TBL = "DOCUMENTTRACKING";
  public static final String DT_CONDITIONID_COL = "CONDITIONID";

  //borrower address Table
  public static final String BR_ADDRESS_TBL = "BORROWERADDRESS";
  public static final String BR_ADDR_TYPEID_COL = "BORROWERADDRESSTYPE";

  //page table
  public static final String PAGE = "PAGE";
  public static final String PAGEID_COL = "PAGEID";

  public static final int DM_NO_MOD_REQUESTED = 0;
  public static final int DM_COMPLETE_RE_UWR_REQUESTED = 1;
  public static final int DM_NON_CRITICAL_NO_DOC_PREP = 2;
  public static final int DM_NON_CRITICAL_DOC_PREP = 3;

  //--Release2.1--//
  //Replased all Button Image with special Tokens
  //  !OK! -- OK button (ok.gif)
  //  !CANCEL! -- Cancel button (cancel.gif)
  //  !CLOSE! -- Close button (close.gif)
  //--> By Billy 12Nov2002
  public static final String OK_CLIENT_EXECUTE_BTN =
    "<a onclick=\"return IsSubmited();\" href=\"javascript:tool_click(5);\"><img src=\"../images/!OK!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

  //--> In order to achieve the same functionality of the Active Message Button in JATO we have to use
  //--> HREF instead and invoke the JavaScript function (performActMsgBtSubmit) in order to pass button
  //--> parameters to the special Commadn Handler (ActMessageCommand).
  //--> by BILLY 15July2002
  public static final String OK_SERVER_EXECUTE_BTN =
    //  "<INPUT TYPE=IMAGE NAME=\"handleActMessageOK(@)\" VALUE=\" \" SRC=\"/images/ok.gif\" ALIGN=TOP  width=86 height=25 alt=\"\" border=\"0\">";
    "<a onclick=\"return IsSubmited();\" href=\"javascript:performActMsgBtSubmit(\\'@\\');\"><img src=\"../images/!OK!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

  public static final String OK_MI_REQUEST_EXECUTE_BTN =
      "<a onclick=\"return IsSubmited();\" id=\"AMLMIRequestBtn\" href=\"javascript:sendAJAXMIRequest(\\'@\\');\">" +
      "<img src=\"../images/!OK!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

  public static final String CANCEL_SERVER_EXECUTE_BTN =
    //  "<INPUT TYPE=IMAGE NAME=\"handleActMessageOK(^)\" VALUE=\" \" SRC=\"/images/cancel.gif\" ALIGN=TOP  width=86 height=25 alt=\"\" border=\"0\">";
    "<a onclick=\"return IsSubmited();\" href=\"javascript:performActMsgBtSubmit(\\'^\\');\"><img src=\"../images/!CANCEL!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

  //====================================================================

  public static final String CANCEL_CLIENT_EXECUTE_BTN =
    "<a onclick=\"return IsSubmited();\" href=\"javascript:tool_click(5);\"><img src=\"../images/!CANCEL!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

  public static final String CLOSE_CLIENT_EXECUTE_BTN =
    "<a onclick=\"return IsSubmited();\" href=\"javascript:tool_click(5);\"><img src=\"../images/!CLOSE!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

  //======================== End changes for Release2.1 =====================================================
  // ------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
  public static final String YES_SERVER_EXECUTE_BTN =
    "<a onclick=\"return IsSubmited();\" href=\"javascript:performActMsgBtSubmit(\\'@\\');\"><img src=\"../images/!YES!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

//  public static final String NO_CLIENT_EXECUTE_BTN =
//    //    "<INPUT TYPE=IMAGE NAME=\"handleActMessageOK(^)\" VALUE=\" \" SRC=\"/images/cancel.gif\" ALIGN=TOP  width=86 height=25 alt=\"\" border=\"0\">";
//    "<a onclick=\"return IsSubmited();\" href=\"javascript:tool_click(5);\"><img src=\"/images/!NO!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

  public static final String NO_SERVER_EXECUTE_BTN =
    "<a onclick=\"return IsSubmited();\" href=\"javascript:performActMsgBtSubmit(\\'^\\');\"><img src=\"../images/!NO!\" width=86 height=25 alt=\"\" border=\"0\"></a>";

  // ------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------

  public static final String SPACER_EXECUTE_BTN = "&nbsp;&nbsp;&nbsp;&nbsp;";

  //conditiontype table - updated aug 18
  public static final int CONDITION_TYPE_INACTIVE = -1;
  public static final int CONDITION_TYPE_COMMITMENT_AND_DOC_TRACKING = 0;
  public static final int CONDITION_TYPE_DOC_TRACKING_ONLY = 1;
  public static final int CONDITION_TYPE_COMMITMENT_ONLY = 2;
  public static final int CONDITION_TYPE_VARIABLE_VERBIAGE = 3;
  public static final int CONDITION_TYPE_ELECTRONIC_VERBIAGE = 4;
  public static final int CONDITION_TYPE_FUNDER = 5;

  //PROVINCE table - brad jul 30 2000
  public static final int PROVINCE_OTHER = 0;
  public static final int PROVINCE_ALBERTA = 1;
  public static final int PROVINCE_BRITISH_COLUMBIA = 2;
  public static final int PROVINCE_MANITOBA = 3;
  public static final int PROVINCE_NEW_BRUNSWICK = 4;
  public static final int PROVINCE_NEWFOUNDLAND = 5;
  public static final int PROVINCE_NORTHWEST_TERRITORIES = 6;
  public static final int PROVINCE_NOVA_SCOTIA = 7;
  public static final int PROVINCE_NUNAVUT = 8;
  public static final int PROVINCE_ONTARIO = 9;
  public static final int PROVINCE_PRINCE_EDWARD_ISLAND = 10;
  public static final int PROVINCE_QUEBEC = 11;
  public static final int PROVINCE_SASKATCHEWAN = 12;
  public static final int PROVINCE_YUKON = 13;

  //PROPERTYTYPE table - brad jul 30 2000
  public static final int PROPERTY_TYPE_SINGLE_FAMILY = 0;
  public static final int PROPERTY_TYPE_MULTI_FAMILY = 1;
  public static final int PROPERTY_TYPE_CONDOMINIUM = 2;
  public static final int PROPERTY_TYPE_WORKING_FARM = 3;
  public static final int PROPERTY_TYPE_COMMERCIAL = 4;
  public static final int PROPERTY_TYPE_INDIAN_RESERVATION = 5;
  public static final int PROPERTY_TYPE_ACREAGE = 6;
  public static final int PROPERTY_TYPE_APARTMENT_WITH_STORES = 7;
  public static final int PROPERTY_TYPE_RETAIL = 8;
  public static final int PROPERTY_TYPE_OFFICE_BUILDING = 9;
  public static final int PROPERTY_TYPE_INDUSTRIAL = 10;
  public static final int PROPERTY_TYPE_HOTEL_MOTEL = 11;
  public static final int PROPERTY_TYPE_RECREATION = 12;
  public static final int PROPERTY_TYPE_LAND_FARM = 13;
  public static final int PROPERTY_TYPE_FREEHOLD_CONDO = 14;
  public static final int PROPERTY_TYPE_LEASEHOLD_CONDO = 15;
  public static final int PROPERTY_TYPE_TOWNHOUSE_CONDO = 16;
  public static final int PROPERTY_TYPE_STRATA = 17;
  public static final int PROPERTY_TYPE_AGRICULTURAL = 18;
  public static final int PROPERTY_TYPE_CO_OP = 19;
  // ***** Change by NBC/PP Impl. Team - START *****//
  public static final int PROPERTY_TYPE_LEASED_LAND = 20; 
  // ***** Change by NBC/PP Impl. Team - END *****//

  //EMPLOYMENTHISTORYSTATUS table - brad AUG 8 2000
  public static final int EMPLOYMENT_HISTORY_STATUS_CURRENT = 0;
  public static final int EMPLOYMENT_HISTORY_STATUS_PREVIOUS = 1;

  //EMPLOYMENTHISTORYTYPE table - Billy 10 Apr 2001
  public static final int EMPLOYMENT_HISTORY_TYPE_FULLTIME = 0;
  public static final int EMPLOYMENT_HISTORY_TYPE_PARTTIME = 1;
  public static final int EMPLOYMENT_HISTORY_TYPE_SEASONAL = 2;

  public static final int DOCUMENT_TRACKING_SOURCE_SYSTEM_GENERATED = 0;
  public static final int DOCUMENT_TRACKING_SOURCE_USER_SELECTED = 1;
  public static final int DOCUMENT_TRACKING_SOURCE_CUSTOM_FREE_FORM = 2;

  public static final int SOB_CATEGORY_TYPE_BROKER = 0;
  public static final int SOB_CATEGORY_TYPE_AGENT = 1;
  public static final int SOB_CATEGORY_TYPE_INTERNET = 2;
  public static final int SOB_CATEGORY_TYPE_DIRECT = 3;
  public static final int SOB_CATEGORY_TYPE_STAFF_REFERRAL = 4;
  public static final int SOB_CATEGORY_TYPE_INSURANCE_BROKER = 5;
  public static final int SOB_CATEGORY_TYPE_STAFF_OF_LENDER = 6;

  // RATE PRICING STATUSES
  public static final int PRICING_STATUS_ACTIVE = 0;
  public static final int PRICING_STATUS_DISABLED = 1;

  //SYSTEM TYPE   - Updated 20Nov2003 by Zivko
  public static final int SYSTEM_TYPE_MANUAL_ENTRY = 0;
  public static final int SYSTEM_TYPE_MORTY = 1;
  public static final int SYSTEM_TYPE_FILOGIX = 2;
  public static final int SYSTEM_TYPE_CONSUMERBASE = 3;
  public static final int SYSTEM_TYPE_EXTERNAL_LENDER = 4;
  public static final int SYSTEM_TYPE_EXTRANET = 5;
  public static final int SYSTEM_TYPE_ECNI = 6;
  public static final int SYSTEM_TYPE_CCAPS = 7;
  public static final int SYSTEM_TYPE_EXPERT = 8;

  //WORKFLOW_NOTIFICATION TYPE
  public static final int WORKFLOW_NOTIFICATION_TYPE_NULL = -1;
  public static final int WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL = 0; // New Deal without CapLink
  public static final int WORKFLOW_NOTIFICATION_TYPE_MI_REPLY = 1;
  public static final int WORKFLOW_NOTIFICATION_TYPE_DOCUMENT_DELIVERY_SUCCESS =  2;
  public static final int WORKFLOW_NOTIFICATION_TYPE_DOCUMENT_DELIVERY_FAILED = 3;


  public static final int WORKFLOW_NOTIFICATION_TYPE_MI_REQUEST = 4;
  public static final int WORKFLOW_NOTIFICATION_TYPE_MI_CANCEL = 5;


  public static final int WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_0 = 6; // WFETrigger.workflowTrigger(), type 0
  public static final int WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2 = 7; // WFETrigger.workflowTrigger(), type 2, without CapLink
  public static final int WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WCP = 8; // workflow notification type 2, with cap-link required

  public static final int WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WCP = 9; // workflow notification type New Deal , with cap-link required
  public static final int WORKFLOW_NOTIFICATION_TYPE_TASK_TRIGGER_TYPE_2_WUP = 10; // workflow notification type 2, with Cervus upload required
  public static final int WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_WUP = 11; // workflow notification type New Deal , with Cervus upload required
  public static final int WORKFLOW_NOTIFICATION_TYPE_NEW_DEAL_B = 12; //#DG126 workflow notification type New Deal, upload to BMO-CCAPS

  public static final int BRANCH_TYPE_ORIGINATION = 0;
  public static final int BRANCH_TYPE_SERVICING = 1;

  public static final int DEAL_TYPE_CONVENTIONAL = 0;
  public static final int DEAL_TYPE_HIGH_RATIO = 1;
  public static final int DEAL_TYPE_CONV_INSURED = 2;

  public static final int TAX_PAYOR_BORROWER = 0;
  public static final int TAX_PAYOR_LENDER = 1;

  // Using by Deal Resolution Screen
  public static final int DEAL_DENY_EXTERNAL_LENDER_REFUSED = 16;

  //PROPERTYUSAGE(Property Usage table) --updated by Linda
  public static final int PROPERTY_USAGE_PRIMARY_RESIDENCE = 0;
  //PROPERTYUSAGE(Property Usage table) --Edd
  public static final int PROPERTY_USAGE_SECOND_HOME = 2;
  
  /***** GR3.2.2 Mortgage Insurance Changes start *****/
  public static final int PROPERTY_USAGE_RENTAL = 1;
  public static final int PROPERTY_USAGE_INVESTMENT = 4;
  /***** GR3.2.2 Mortgage Insurance Changes end *****/
  
  //LIENPOSITION(Lien Position table) -- updated by Linda
  public static final int LIEN_POSITION_FIRST = 0;
  public static final int LIEN_POSITION_SECOND = 1;
  public static final int LIEN_POSITION_THIRD = 2; // added by Rs
  public static final int LIEN_POSITION_FORTH = 3;


  //SPECIALFEATRUE(Special Feature table) -- updated by Linda
  public static final int SPECIAL_FEATURE_PRE_APPROVAL = 1;

  public static final int LIABILITY_TYPE_MORTGAGE = 0;
  public static final int LIABILITY_TYPE_EQUITY_MORTGAGE = 1;
  public static final int LIABILITY_TYPE_CREDIT_CARD = 2;
  public static final int LIABILITY_TYPE_PERSONAL_LOAN = 3;
  public static final int LIABILITY_TYPE_AUTO_LOAN = 4;
  public static final int LIABILITY_TYPE_ALIMONY = 5;
  public static final int LIABILITY_TYPE_CHILD_SUPPORT = 6;
  public static final int LIABILITY_TYPE_STUDENT_LOAN = 7;
  public static final int LIABILITY_TYPE_WAGE_GARNISHMENT = 8;
  public static final int LIABILITY_TYPE_OTHER = 9;
  public static final int LIABILITY_TYPE_UNSECURED_LINE_OF_CREDIT = 10;
  public static final int LIABILITY_TYPE_INCOME_TAX = 11;
  public static final int LIABILITY_TYPE_MORTGAGE_ON_A_RENTAL_PROPERTY = 12;
  public static final int LIABILITY_TYPE_SECURED_LINE_OF_CREDIT = 13;
  public static final int LIABILITY_TYPE_CLOSING_COSTS = 14;
  public static final int LIABILITY_TYPE_LEASE = 15;
  public static final int LIABILITY_TYPE_RENT = 16;
  public static final int LIABILITY_TYPE_ESTIMATED_MUNICIPAL_TAXES = 17;
  public static final int LIABILITY_TYPE_ESTIMATED_CONDO_FEES = 18;
  public static final int LIABILITY_TYPE_ESTIMATED_HEATING_EXPENSES = 19;
  public static final int LIABILITY_TYPE_DEBT_STORE_CREDIT_CARD = 20;
  public static final int LIABILITY_TYPE_OPEN_ACCT = 21;

  // liability_payoff_type constants
  public static final int LIABILITY_PAYOFFTYPE_NONE = 0;
  public static final int LIABILITY_PAYOFFTYPE_PRIOR_TO_ADVANCE = 1;
  public static final int LIABILITY_PAYOFFTYPE_FROM_PROCEEDS = 2;
  public static final int LIABILITY_PAYOFFTYPE_INTERNAL_FUNDS = 3;
  public static final int LIABILITY_PAYOFFTYPE_PRIOR_TO_ADVANCE_CLOSE = 4;
  public static final int LIABILITY_PAYOFFTYPE_FROM_PROCEEDS_CLOSE = 5;
  public static final int LIABILITY_PAYOFFTYPE_INTERNAL_FUNDS_CLOSE = 6;

  //corrected 16/03/01 Brad
  public static final int DEAL_NOTES_CATEGORY_SOURCE_OF_BUSINESS = 2;
  public static final int DEAL_NOTES_CATEGORY_ADMIN = 1;
  public static final int DEAL_NOTES_CATEGORY_GENERAL = 0;
  public static final int DEAL_NOTES_CATEGORY_DECISIONING = 3;
  public static final int DEAL_NOTES_CATEGORY_MANAGEMENT = 4;
  public static final int DEAL_NOTES_CATEGORY_MI_REQUEST = 5;
  public static final int DEAL_NOTES_CATEGORY_MI_RESPONSE = 6;
  public static final int DEAL_NOTES_CATEGORY_DEAL_INGESTION = 7;
  public static final int DEAL_NOTES_CATEGORY_DOCUMENT_PREPARATION = 8;
  //--DJ_CR134--start--27May2004--//
  // For DJ client execute this model to get HomeBASE product, rate, payment info (if exist).
  public static final int DEAL_NOTES_CATEGORY_DEAL_INGESTION_PRODUCT = 10;
  //--DJ_CR134--end--//
  public static final int DEAL_NOTES_CATEGORY_INGESTION_LENDER = 11;  //#DG126 new, BMO-CCAPS
  public static final int DEAL_NOTES_CATEGORY_CONDITION = 12;         // Catherine, 7-Apr-05, pvcs#817


  //corrected 16/03/01 Brad.
  public static final int PREPAYMENT_OPTIONS_NOT_ALLOWED = 3;
  public static final int PREPAYMENT_OPTIONS_OPEN_WITH_PENALTY = 1;
  public static final int PREPAYMENT_OPTIONS_NHA = 2;
  public static final int PREPAYMENT_OPTIONS_3_MONTH_PENALTY = 0;
  public static final int PREPAYMENT_OPTIONS_OPEN = 4;
  public static final int PREPAYMENT_OPTIONS_2_MONTH_PENALTY = 5;

  public static final int PRIVILEGE_PAYMENT_FIFTEEN_PLUS_FIFTEEN = 0;
  public static final int PRIVILEGE_PAYMENT_TEN_PLUS_TEN = 1;
  public static final int PRIVILEGE_PAYMENT_TWENTY_PLUS_TWENTY = 2;
  public static final int PRIVILEGE_PAYMENT_NOT_ALLOWED = 3;
  public static final int PRIVILEGE_PAYMENT_OPEN = 4;

  public static final double CREDIT_BUREAU_LIABILITY_ORIGINAL = 2.0;
  public static final double CREDIT_BUREAU_LIABILITY_DELETED_AFTER_MODIFICATION = 0.0;
  public static final double CREDIT_BUREAU_LIAB_CHANGED_IN_LIABILITY_SECTION = 1.0;
  public static final double CREDIT_BUREAU_LIAB_EXPORTED_TO_LIABILITY = 3.0;

  // Definition for the Servicing Upload type
  public static final String SERVICING_UPLOAD_TYPE_COMMITMENT = "Commitment";
  public static final String SERVICING_UPLOAD_TYPE_CLOSING = "Closing";
  public static final String SERVICING_UPLOAD_TYPE_FUNDING = "Funding";
  public static final String SERVICING_UPLOAD_TYPE_DENY = "Deny";
  public static final String SERVICING_UPLOAD_TYPE_COLLAPSE = "Collapse";
  public static final String SERVICING_UPLOAD_TYPE_INGEST = "Ingest";
  public static final String SERVICING_UPLOAD_TYPE_RESUBMIT = "Resubmit";

  // Definition of Mail Destination Type
  public static final int MAIL_TO_DISABLED = 0;
  public static final int MAIL_TO_UNDERWRITER = 1;
  public static final int MAIL_TO_ADMINISTRATOR = 2;
  public static final int MAIL_TO_FUNDER = 3;
  public static final int MAIL_TO_CURRUSER = 4;
  /*#DG60 SCR#538 new implementation for Mail destination
  public static final int MAIL_TO_UNDERWRITERANDCURRUSER = 5;
  public static final int MAIL_TO_ADMINISTRATORANDCURRUSER = 6;
  public static final int MAIL_TO_FUNDERANDCURRUSER = 7;

  //// SYNCADD.
  public static final int MAIL_TO_SOBANDCURRUSER = 8;
*/
  public static final int MAIL_TO_SOB = 9;
  public static final int MAIL_TO_ORIGINATIONBRANCH = 10;
  public static final int MAIL_TO_APPRAISER = 11;
  public static final int MAIL_TO_SOLICITOR = 12; //#DG60
  public static final int MAIL_DISPLAY_ON_SCREEN = 14; // Catherine: 23-Mar-06  for Disclosure
  public static final int MAIL_TO_REFSOURCE = 15; //#DG414

//  ***** Change by NBC Impl. Team - Version 1.4 - Start *****//

    public static final int MAIL_TO_SERVICEBRANCH = 16;
//  ***** Change by NBC Impl. Team - Version 1.4 - End *****//

  // Definition of CreditBureau Name ID
  public static final int CREDIT_BUREAU_NAME_NONE = 0;
  public static final int CREDIT_BUREAU_NAME_EQUIFAX = 1;
  public static final int CREDIT_BUREAU_NAME_TRANSUNION = 2;

  //--Release2.1--start//
  //// For multilingual BXP it comes from the BXPResources GenericMessage
  // Definition of CreditScore Label based on CreditBureau Name ID
  ////public static final String    CREDIT_SCORE_LABEL_NONE         = "Credit Score:";
  ////public static final String    CREDIT_SCORE_LABEL_EQUIFAX      = "Beacon Score:";
  ////public static final String    CREDIT_SCORE_LABEL_TRANSUNION   = "Empirica Score:";

  // Definition of CreditBureau(CB) / SourceOfBusiness(SOB) Liability labels
  ////public static final String    CB_LIABILITY_LABEL         = "Credit Bureau Liabilities";
  ////public static final String    SOB_LIABILITY_LABEL        = "Liabilities from Source of Business";
  ////public static final String    CB_TOTAL_LIABILITY_LABEL   = "Total Bureau Liabilities:";
  ////public static final String    SOB_TOTAL_LIABILITY_LABEL  = "Total Source of Business Liabilities:";
  ////public static final String    CB_MONTH_PAYMENT_LABEL     = "Monthly Bureau Payments:";
  ////public static final String    SOB_MONTH_PAYMENT_LABEL    = "Month Source of Business Payments:";
  //--Release2.1-end//

  // Definition for Workflow Routing types
  public static final int ROUTE_TYPE_SOURCE = 0;
  public static final int ROUTE_TYPE_SOURCEFIRM = 1;
  public static final int ROUTE_TYPE_POSTALCODE = 2;
  public static final int ROUTE_TYPE_AREACODE = 3;
  public static final int ROUTE_TYPE_BORROWERLASTNAME = 4;
  public static final int ROUTE_TYPE_WORKFLOW = 5;
  public static final int ROUTE_TYPE_PROVINCE = 6;
  public static final int ROUTE_TYPE_SOBREGION = 7;
  public static final int ROUTE_TYPE_SPECIALFEATURE = 8;
  public static final int ROUTE_TYPE_SOURCEFIRM_PROVINCE = 9;
  public static final int ROUTE_TYPE_SELFEMPLOYED = 10;
  // SEAN Routing Order Change (May 05, 2005): corresponding to adding new
  // entries in table ROUTETYPE.
  public static final int ROUTE_TYPE_OCCUPANCYTYPE = 11; // the Occupancy Type
  public static final int ROUTE_TYPE_SOBSTATUS = 12; // the SOB Status
  // SEAN Routing Order Change END
  // Serghei Routing Order Change: corresponding to adding 
  //a new entry in the ROUTETYPR table.
  public static final int ROUTE_TYPE_NEWCONSTRUCTIONID = 13;
  // Serghei Routing Order Change end.


  // Definition for Routing Match Types
  public static final int ROUTE_MATCH_TYPE_USER = 0;
  public static final int ROUTE_MATCH_TYPE_GROUP = 1;
  public static final int ROUTE_MATCH_TYPE_BRANCH = 2;
  public static final int ROUTE_MATCH_TYPE_REGION = 3;
  public static final int ROUTE_MATCH_TYPE_INSTITUTION = 4;
  public static final int ROUTE_MATCH_TYPE_LENDER = 5;

  // dwelling type
  public static final int DWELLING_TYPE_DETACHED = 0;
  public static final int DWELLING_TYPE_SEMI_DETACHED = 1;
  public static final int DWELLING_TYPE_DUPLEX = 2;
  public static final int DWELLING_TYPE_ROW = 3;
  public static final int DWELLING_TYPE_APARTMENT = 4;
  public static final int DWELLING_TYPE_MOBILE = 5;
  public static final int DWELLING_TYPE_TRI_PLEX = 6;
  public static final int DWELLING_TYPE_STACKED = 7;
  public static final int DWELLING_TYPE_MODULAR_HOME = 8;
  public static final int DWELLING_TYPE_FOUR_PLEX = 9;

  // downpayment source type
  public static final int DP_TYPE_SALE_OF_EXISTING_PROPERTY = 0;
  public static final int DP_TYPE_PERSONAL_CASH = 1;
  public static final int DP_TYPE_RRSP = 2;
  public static final int DP_TYPE_BORROWED_AGAINST_LIQUID_ASSETS = 3;
  public static final int DP_TYPE_GIFT = 4;
  public static final int DP_TYPE_UNKNOWN = 5;
  public static final int DP_TYPE_SWEAT_EQUITY = 6;
  public static final int DP_TYPE_OTHER = 7;
  public static final int DP_TYPE_EQUALIZATION_PAYMENT = 8;
  public static final int DP_TYPE_SECONDARY_FINANCING = 10;
  public static final int DP_TYPE_GRANTS = 11;
  public static final int DP_TYPE_FIRST_MORTGAGE = 12;

  // new construction table
  public static final int CONSTRUCTION_EXISTING = 0;
  public static final int CONSTRUCTION_CONSTRUCTION = 1;
  public static final int CONSTRUCTION_NEW = 2;

  //// Closing activity labels.
  public static final String ONE_MONTH_AFTER_IAD_LABEL = "One month after IAD, ";

  public static final int OCCUPATION_OTHER = 0;
  public static final int OCCUPATION_MANAGEMENT = 1;
  public static final int OCCUPATION_CLERICAL = 2;
  public static final int OCCUPATION_LABOUR_TRADESPERSON = 3;
  public static final int OCCUPATION_RETIRED = 4;
  public static final int OCCUPATION_PROFESSIONAL = 5;
  public static final int OCCUPATION_SELFEMPLOYED = 6;


  //--> Ticket#146 -- DJ004 : Deal Res Add Hold Reasons & Hrs_Min
  //--> By Billy 09Jan2004
  // Hold Reason definition
  //// Product renamed and reduced number of hold reasons for DJ.
  public static final int HOLD_REASON_NONE = 0;
  public static final int HOLD_REASON_REFERRED_EXTERNAL = 1;
  public static final int HOLD_REASON_WAITING_FOR_CREDIT_EXPERIENCE = 2;
  //============================================================

  public static final int ASSET_TYPE_SAVINGS = 0;
  public static final int ASSET_TYPE_RRSP = 1;
  public static final int ASSET_TYPE_GIFT = 2;
  public static final int ASSET_TYPE_RESIDENCE = 3;
  public static final int ASSET_TYPE_VEHICLE = 4;
  public static final int ASSET_TYPE_PROPERTY = 5;
  public static final int ASSET_TYPE_STOCKS_BONDS_MUTUAL = 6;
  public static final int ASSET_TYPE_RENTAL_PROPERRTY = 7;
  public static final int ASSET_TYPE_OTHER = 8;
  public static final int ASSET_TYPE_DOWN_PAYMENT = 9;
  public static final int ASSET_TYPE_HOLDING = 10;
  public static final int ASSET_TYPE_HOUSEHOLD_GOODS = 11;
  public static final int ASSET_TYPE_LIFE_INSURANCE = 12;
  public static final int ASSET_TYPE_DEPOSIT = 13;


  public static final int PREF_DELIVERY_METHOD_BY_E_MAIL = 0;
  public static final int PREF_DELIVERY_METHOD_BY_FAX = 1;
  public static final int PREF_DELIVERY_METHOD_BY_E_MAIL_BACK_TO_REQUESTER = 2;

  ////--DJ_LD_CR--start--//
  //// Various contstants providing communication with DJ DLL calc engine.
  //// Separators.
  public static final String DJLD_JS_ESCAPE_CHARACTER = "'";
  public static final String DJLD_JS_ESCAPE_CHARACTER_PLUS_COMMA = "',";
  public static final String DJLD_JS_ESCAPE_CHARACTER_PLUS_ENTITY_SEP = "'|";
  public static final String DJLD_JS_ESCAPE_CHARACTER_PLUS_REPEATPART_SEP = "'||";
  public static final String DJLD_JS_ESCAPE_CHARACTER_PLUS_APPLICANT_SEP = "'|||";

  //// Parameters passing to RTP DLL calc (implemented via HomeBASE).
  public static final String DJLD_TYPE_OF_LOAN = "ITypePret";
  public static final String DJLD_AMOUNT_OF_LOAN = "dMontantInitial";
  public static final String DJLD_NUMBER_OF_APPLICANTS_ACCEPTED_LIFE = "iNbrPersonnes";
  public static final String DJLD_APPLICATION_DATE_YEAR = "IanneeEffYear";
  public static final String DJLD_APPLICATION_DATE_MONTH = "IanneeEffMonth";
  public static final String DJLD_APPLICATION_DATE_DAY = "IanneeEffDay";
  public static final String DJLD_APPLICANT_AGE = "Iage";
  public static final String DJLD_APPLICANT_GENDER = "IMale1Fem2";
  public static final String DJLD_APPLICANT_SMOKER_STATUS = "IindFumeur";
  public static final String DJLD_REDUCED_LIFE_INDICATOR = "iIndTarifReduit";
  public static final String DJLD_APPLICANT_LIFE_PERCENT_INSURANCE = "DpropVie";
  public static final String DJLD_APPLICANT_DISABILITY_PERCENT_INSURANCE = "DpropInv";
  public static final String DJLD_APPLICANT_DISABILITY_STATUS = "DisStatus";

  //// Parameters passing to Infocal DLL calc (implemented via HomeBASE).
  public static final String DJLD_COMPOUND_PERIOD = "CAP";
  public static final String DJLD_NET_INTEREST_RATE = "TAU";
  public static final String DJLD_TOTAL_LOAN_AMOUNT = "MTP";
  public static final String DJLD_FCP = "FCP";
  public static final String DJLD_PAYMENT_FREQUENCY = "FRE";
  public static final String DJLD_AMORTIZATION_PERIOD = "DUR";
  public static final String DJLD_ACTUAL_PMNT_TERM = "TER";
  public static final String DJLD_REM = "REM";
  public static final String DJLD_FPV = "FPV";

  //// Parameters getting from RTP/Infocal DLL calc engine (implemented via HomeBASE).
  public static final String DJLD_DLL_CALC_ERROR_CODE = "Error";
  public static final String DJLD_PAYMENT_PLUS_LIFE = "RRG"; // in & out
  public static final String DJLD_RESIDUAL_AMOUNT = "RES";
  public static final String DJLD_EQUIVALENT_INTEREST_RATE = "TEC";
  public static final String DJLD_INTEREST_RATE_INCREMENT_FOR_LD = "ASS";
  public static final String DJLD_LIFE_PREMIUM = "PVS";
  public static final String DJLD_PARAMETR_TO_FIND_LIFE_PREMIUM = "RVS";
  public static final String DJLD_DISABILITY_PREMIUM = "PIS";
  public static final String DJLD_EQUIVALENT_INTEREST_RATE_FOR_SECOND_MORTGAGE = "TES"; // for future usage, phase II.
  public static final String DJLD_DISABILITY_INSURANCE_PREMIUM = "RSA";

  //--DJ_Ticket#499--start//
   // These fields should be dynamically calculated and included into the DJ documents.
   public static final String DJLD_CREDIT_COSTS = "FCI";
   public static final String DJLD_ADD_CREDIT_COSTS_INCURRED_BY_INS = "FCA";
   public static final String DJLD_TOTAL_CREDIT_COSTS = "FCT";
   //--DJ_Ticket#499--end//

   //// InsuranceProportions contstants.
   public static final int DJ_OPTION_NOT_DEFINED = 0;
   public static final int DJ_HOME_OWNER_PLUS_OPTION = 1;
   public static final int DJ_TOTAL_OPTION = 2;
   public static final int DJ_REGULAR_OPTION = 3;
   public static final int DJ_DECLINED_OPTION = 4;

   // Currently both LIFE and DISABILITY has the same set of statuses.
   public static final String LIFE_DISABILITY_STATUS_ACCEPTED = "A";
   public static final String LIFE_DISABILITY_STATUS_REFUSED = "R";
   public static final String LIFE_DISABILITY_STATUS_INELIGIBLE = "I";

   // Definition of various constants in Infocal output
   public static final int INFOCAL_ENTITIES_NUMBER_REGULAR = 12;
   public static final int INFOCAL_ENTITIES_NUMBER_ERROR_CODE = 1;

   // Various constants for Life&Disability functionality (Life&Disability, DealEntry/DealMod,
   // UWorksheet screens.
   public static final double FULL_PERCENT_COVERAGE = 100.00;
   public static final String FULL_PERCENT_COVERAGE_STRING = "100";
   public static final double ZERO_PERCENT_COVERAGE = 0.00;
   public static final String ZERO_PERCENT_COVERAGE_STRING = "0";
   public static final String DISABILITY_INS_ZERO_STRING = "0";
   public static final String RVS_DOUBLE_ZERO_STRING = "0.00";
   ////--DJ_LD_CR--end--//

   //--Ticket#1294--13May2005--start--//
   public static final double HOME_OWNER_PLUS_DISABILITY_COVERAGE = 150.00;
   public static final String HOME_OWNER_PLUS_DISABILITY_COVERAGE_STRING = "150";
   //--Ticket#1294--13May2005--end--//

   // Definition of Interest Compound IDs
   public static final int INTEREST_COMPOUNDID_SEMIANNUAL = 0;
   public static final int INTEREST_COMPOUNDID_MONTHLY = 1;

   //--DisableProductRateTicket#570--10Aug2004--start--//
   public static final String NULL_STRING = "NULL";
   //--DisableProductRateTicket#570--10Aug2004--end--//

   //--DocCentral_FolderCreate--21Sep--2004--start--//
   public static final int DO_NOT_CREATE_DOC_CENTRAL_DEAL_FOLDER = 0;
   public static final int CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL = 1;
   public static final int CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_SUBMISSION = 2;
   // create folder into Exchagne 2.0
   public static final int CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_APPROVAL_2 = 3;
   public static final int CREATE_DOC_CENTRAL_DEAL_FOLDER_ON_SUBMISSION_2 = 4;
   //--DocCentral_FolderCreate--21Sep--2004--end--//

  //-- ========== SCR#931 begins ========== --//
  //-- by Neil on Feb 08, 2005
  public static final int IWQ_TASK_STATUS_OPTIONS_LENGTH = 4;
  //-- ========== SCR#931   ends ========== --//

  // --------------------- DJ, pvcs # 1683, spec v2, 21-Sep-05 Catherine -- begin ---------------------
  public static final int INSURER_ONLY_APPLICANT_GENDER_MALE = 1;
  public static final int INSURER_ONLY_APPLICANT_GENDER_FEMALE = 2;
  // --------------------- DJ, pvcs # 1683, spec v2, 21-Sep-05 Catherine -- end ---------------------

  // Rel 3.1, Interest types
  public static final int INTEREST_TYPE_FIXED = 0;
  public static final int INTEREST_TYPE_ADJUSTABLE = 1;
  public static final int INTEREST_TYPE_CAPPED = 2;
  public static final int INTEREST_TYPE_VRM = 3;
  public static final int INTEREST_TYPE_BUYDOWN = 4;
  public static final int INTEREST_TYPE_VIP = 5;
  public static final int INTEREST_TYPE_FLOAD_SOM = 6;
  public static final int INTEREST_TYPE_FLOAT_INDEX = 7;
  public static final int INTEREST_TYPE_FLOAT_SOT = 8;
  
public static final int RATE_FLOAT_DOWN_TYPE_DEFAULT = 1;

    public static final int RATE_FLOAT_DOWN_TYPE_UCURVE = 2;

    public static final int RATE_FLOAT_DOWN_TYPE_VCURVE = 3;

    public static final int ATNSERVLET_ACTION_DEFAULT = 0;

    public static final int ATNSERVLET_ACTION_RFD = 1;
    
//  ***** Change by NBC Impl. Team - Version 1.4 - Start *****//
    public static final String WORKFLOWTRIGGER_TRIGGERKEY_AUTOPREAPPROVAL="Auto-PreApproval";
//  ***** Change by NBC Impl. Team - Version 1.4 - End *****//
    public static final String WORKFLOWTRIGGER_KEY_OBFUSCATED = "Obfuscated";       //#DG702

//  ***** Change by NBC Impl. Team -Iteration 4 - Version 1.6 - Start *****//
    public static final int ESCROWTYPE_ADMIN_FEE=7;
//  ***** Change by NBC Impl. Team -Iteration 4 - Version 1.6 - End *****//
  // ***** Change by NBC Impl. Team - Start *****//
    public static final int ESCROWTYPE_CREDIT_LIFE=3;
  // ***** Change by NBC Impl. Team - End *****//
    

  //------------------- 3.1 MI, Edd, 31Jan2006 - start -------------------------
  public static final int LOC_TYPE_5_20 = 1;
  public static final int LOC_TYPE_10_15 = 2;
  public static final int PRODUCT_TYPE_MORTGAGE = 1;
  public static final int PRODUCT_TYPE_SECURED_LOC = 2;
  public static final int PRODUCT_TYPE_MULTIPLE_COMPONENT = 3; //2009-Aug-11, 4.2DA
  
  //-------------------- 3.1 MI, Edd, 31Jan2006 - end --------------------------
  
 //-------------------- Spring context ----------------------------------
  public static final String UG_XML_CONTEXT = "com.filogix.app.springctx";
  
  public static final String MI_UG_PREMIUM_ERROR_EN = "ERROR - ";
  public static final String MI_UG_PREMIUM_ERROR_FR = "Erreur  - ";
  public static final String MI_UG_PREMIUM_ERROR_TIMEOUT_EN = "TIMEOUT";
  public static final String MI_UG_PREMIUM_ERROR_TIMEOUT_FR = "ARR?T";

  //***** Change by NBC/PP Impl. Team - GCD - START *****//
  
public static final String SUPPORT_RATE_FLOAT_DOWN_FLAG = "Y";

  public static final int REQUEST_STATUS_ID_NOT_SUBMITTED = 1;
  public static final int REQUEST_STATUS_ID_SUBMIT_PENDING = 20;
  public static final int REQUEST_STATUS_ID_SUBMITTED = 21;
  public static final int REQUEST_STATUS_ID_ERROR_SUBMIT = 22;
  public static final int REQUEST_STATUS_ID_RESPONSE_AVAILABLE = 23;
  public static final int REQUEST_STATUS_ID_RESPONSE_RECEIVED = 24;

  public static final int RESPONSE_STATUS_ID_RECEIPT_PENDING = 19;
  public static final int RESPONSE_STATUS_ID_RECEIVED = 20;
  public static final int RESPONSE_STATUS_ID_ERROR_RECEIVED = 21;
  public static final int RESPONSE_STATUS_ID_ERROR_RESPOND = 22;
  
  public static final int RESPONSE_STATUS_ID_MI_ERROR_ASSUME_WENT_THRU = 23;
  public static final int RESPONSE_STATUS_ID_MI_ERROR_FAILED = 24;
  
  
  // service product
  public static final int SERVICE_PRODUCT_ID_GCD = 10;  //GCD ID in the SERVICEPRODUCT table.
  
    public static final int DEFAULT_APPLICANTNUMBER = 0;
    public static final int GCD_CHANNEL_ID = 7; //GCD ID in the CHANNEL table.
    public static final int REQUEST_SERVICE_TRANSACTION_TYPE_ID = 1; //Request ID in the SERVICETRANSACTIONTYPE table.
    public static final String GCD_SPRING_REQUEST_TYPE = "gcdRequestService";
    public static final String SPRING_APPLICATION_CONTEXT_XML = "com.filogix.app.springctx";
    public static final String JAXB_PAYLOAD = "com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload";
    public static final String SOURCE_FIRM_CODE = "NBC";
    public static final int CURRENT_EMPLOYMENT = 0;
    public static final int PREVIOUS_EMPLOYMENT = 1;
    public static final String JAXB_AUXDATA = "com.filogix.externallinks.adjudication.requestprocessor.jaxb.auxdata";
    public static final int BNC_GCD_REQUEST_PAYLOAD_TYPE = 14;
    public static final String PENDING_CREDIT_DECISION_WORKFLOW_TRIGGER_KEY = "Pending Credit Decision";  //WORKFLOWTRIGGERKEY
  
  // Assigned Task Id - "Pending Credit Decision"
  public static final int ASSIGNED_TASK_PENDING_CREDIT_DECISION = 500;
  public static final int ASSIGNED_TASK_APPRAISAL_REPORT_RECEIVED = 108;
  
  public static final String GCD_DEFAULT_FXREQUEST_ENCODING="UTF-8";
  
//*******************Change by NBC Impl. Team - Version 1.6  Start *****//
  public static final int ESCROW_PAYMENT_TERM =0;
  
  //Ticket 616--start//
  public static final String DEAL_SUMMARY_BESTRATE_EN="Best Rate";
  public static final String DEAL_SUMMARY_BESTRATE_FR="Meilleur Taux";
  public static final String DEAL_SUMMARY_CAPRATE_EN="Cap Rate";
  public static final String DEAL_SUMMARY_CAPRATE_FR="Taux Maximum";
  //Ticket 616--end//
  
  public static final int ESCROWTYPE_PROPERTY_TAX=0;
  
  //This variable will be depricated 
  //public static final String UG_XML_CONTEXT = "com.filogix.app.springctx";
  public static final String SPRINT_XML_CONTEXT = "com.filogix.app.springctx";
  
  //--DJ CS 2 --start--//
  public static final String DJ_SPRING_CONTEXT_REQUEST_AUTO = "dj-auto";
  public static final String DJ_SPRING_CONTEXT_REQUEST_MANUAL = "dj-manual";
  
  public static final int REQUEST_SERVICEPRODUCTID_DFP_AUTO = 89;
  public static final int REQUEST_SERVICEPRODUCTID_DFP_MANUAL = 90;
  
  //task id for DJ CS
  public static final int TASK_ID_DECISION_DEAL = 3;
  public static final int TASK_ID_REVIEW_CREDIT_RESPONSE = 86;
  public static final int TASK_ID_CREDIT_REQUEST_FAILED = 87;
  public static final int TASK_ID_AUTO_CREDIT_REQUEST_FAILED = 98;

  //payload type id
  public static final String PAYLOAD_TYPE_APPROVE_DFP = "16";
  public static final String PAYLOAD_TYPE_CANCEL_DFP = "17";
  public static final String PAYLOAD_TYPE_CREATE_DFP = "18";
  public static final String PAYLOAD_TYPE_REFUSE_DFP = "15";
  
  //DatX Response Payload type id
  public static final String DATX_RESPONSE_PAYLOAD_TYPE_CREATE_DFP = "C";
  public static final String DATX_RESPONSE_PAYLOAD_TYPE_REFUSE_DFP = "R";
  public static final String DATX_RESPONSE_PAYLOAD_TYPE_APPROVE_DFP = "A";
  public static final String DATX_RESPONSE_PAYLOAD_TYPE_CANCEL_DFP = "X";
  
  //FXP Request status id for DJ CS 
  public static final int REQUEST_STATUS_REQUEST_SUBMITTED = 19;
  public static final int REQUEST_STATUS_APPROVAL_SUBMITTED = 20;
  public static final int REQUEST_STATUS_CANCELLATION_SUBMITTED = 21;
  public static final int REQUEST_STATUS_REFUSAL_SUBMITTED = 22;
  public static final int REQUEST_STATUS_TRANSMISSION_ERROR = 23;
  
  //FXP Response status id for DJ CS
  public static final int RESPONSE_STATUS_COMPLETED = 6;
  public static final int RESPONSE_STATUS_COMPLETED_CREATE_DFP = 19;
  public static final int RESPONSE_STATUS_COMPLETED_REFUSE_DFP = 22;
  public static final int RESPONSE_STATUS_COMPLETED_APPROVE_DFP = 20;
  public static final int RESPONSE_STATUS_COMPLETED_CANCEL_DFP = 21;
//--DJ CS 2 --end--//
  
 //***************MCM Impl team changes starts - XS_11.11 & XS_11.6 *******************/  
  /*
     * MCM Impl Team. Important -- XS_11.11&11.6 --09-Jun-2008 @Todo: Note to
     * Developer. All the component type, Interest Compound Type Description
     * values should be changed to reflect the 'COMPONENTTYPE' and
     * 'INTERESTCOMPOUND' table field values in the DB.
     */
  //Component Type 
  public static final int COMPONENT_TYPE_MORTGAGE = 1;
  public static final int COMPONENT_TYPE_LOC = 2;
  public static final int COMPONENT_TYPE_LOAN = 3;
  public static final int COMPONENT_TYPE_CREDITCARD = 4;  
  public static final int COMPONENT_TYPE_OVERDRAFT = 5; 
  //Interest Compound Type Description
  public static final String INTEREST_COMPOUND_MONTHLY = "12";
  public static final String INTEREST_COMPOUND_SEMI_ANNUALLY = "2";
  //Default value for variable declaration
  public static final double DEFAULT_VALUE = 0.0d;
//***************MCM Impl team changes ends - XS_11.11 & XS_11.6 *******************/
  
//***************MCM Impl team changes Starts - XS_11.4 - Version 1.9 *******************/
  /**
     * MCM Impl Team. Important -- XS_11.4 -16-Jun-2008
     * @Todo: Note to Developer. All the province constants values should be
     *        changed to reflect the 'PROVINCEID' in the 'PROVINCE' table field
     *        values in the DB
     */
//Constants for Provine
  public static int OTHER                     =  0 ;
  public static int ALBERTA                   =  1 ;
  public static int BRITISH_COLUMBIA          =  2 ;
  public static int MANITOBA                  =  3 ;
  public static int NEW_BRUNSWICK             =  4 ;
  public static int NEWFOUNDLAND_AND_LABRADOR =  5 ;
  public static int NORTHWEST_TERRITORIES     =  6 ;
  public static int NOVA_SCOTIA               =  7 ;
  public static int NUNAVUT                   =  8 ;
  public static int ONTARIO                   =  9 ;
  public static int PRINCE_EDWARD_ISLAND      = 10 ;
  public static int QUEBEC                    = 11 ;
  public static int SASKATCHEWAN              = 12 ;
  public static int YUKON                     = 13 ;
  
  
  //Contants for Months
  public static int JAN = 1;
  public static int FEB = 2;
  public static int MAR = 3;
  public static int APR = 4;
  public static int MAY = 5;
  public static int JUN = 6;
  public static int JUL = 7;
  public static int AUG = 8;
  public static int SEP = 9;
  public static int OCT = 10;
  public static int NOV = 11;
  public static int DEC = 12;
  
//***************MCM Impl team changes ends - XS_11.4 -Version 1.9*******************/
  
  
//***************MCM Impl team changes Starts - XS_11.3 - Version 1.10 *******************/  
//Pricing Profile rate code -  XS_11.3 for Calculation Effective Amortization Months
  public static String UNCNF = "UNCNF";  

  // Repayment types
  public static int REPAYMENT_TYPE_BLENDED_PI = 0;
  public static int REPAYMENT_INTEREST_PLUS_FIXED_PRINCIPAL = 1;
  public static int REPAYMENT_INTEREST_ONLY_VARIABLE = 2;
  public static int REPAYMENT_INTEREST_ONLY_FIXED = 3;
  public static int REPAYMENT_AT_MATURITY = 4;
  
  
  //***************MCM Impl team changes ends - XS_11.3 -Version 1.10 *******************/  
  public static final int CALCTYPE_MIPREMIUM_EXTENDED = 1;
  public static final String MORTGAGEINSURER_XTD_PAYLOAD_YES = "Y";
  public static final String MORTGAGEINSURER_XTD_PAYLOAD_NEVER = "N";
  public static final String MORTGAGEINSURER_XTD_PAYLOAD_ALWAYS = "A";
  
  public static final String MORTGAGEINSURANCETYPE_XTDPAYLOAD_Y = "Y";
  public static final String MORTGAGEINSURANCETYPE_XTDPAYLOAD_N = "N";
  
  
  // 3.2.2GR -- starts
  public static final String MI_RESPONSE_MSG_UNSOLICITED_RESPONSE = "MI_RESPONSE_MSG_UNSOLICITED_RESPONSE";
  // 3.2.2GR -- ends
  
  // 3.2.4GR -- STARTS
  public static final String UUID_GENERATOR = "uuidGenerator";
  public static final String REQUEST_HEADER_EXCHANGE = "exchange2_";
  public static final String REQUEST_HEADER = "headerElement";
  public static final String REQUEST_HEADER_FROM = "headerElementFrom";
  public static final String REQUEST_HEADER_FROMADDRESS = "headerElementFromAddress";
  public static final String REQUEST_HEADER_TO = "headerElementTo";
  public static final String REQUEST_HEADER_ACTION = "headerElementAction";
  public static final String REQUEST_HEADER_ACTION_CM = "ConditionUpdate";
  public static final String REQUEST_HEADER_ACTION_DSU = "DealStatusUpdate";
  public static final String REQUEST_HEADER_MESSAGEID = "headerElementMessageId";
  public static final String REQUEST_HEADER_REFERENCEPARAMATERS = "headerElementReferenceParameters";
  public static final String REQUEST_HEADER_BUSINSSSTRANSACTIONID = "headerElementBusinessTransactionID";
  public static final String REQUEST_HEADER_SECURITY = "headerElementSecurity";
  public static final String REQUEST_HEADER_REQUEST = "headerElementRequest";
  // 3.2.4GR -- ends

  // 4.1GR -- STARTS
  public static final String EXCHANGE_FOLDERCREATOR = "exchangeFolderCreator_";
  //4.1 GR -- ENDS

  // 4.2GR -- STARTS
  public static final String REQUEST_HEADER_ESBPOSTOFFICE = "ESBPostOffice_";
  public static final String TIMEOUT = "Timeout";
  //4.2 GR -- ENDS


  public static final String DATABASE_SYSTEM_DATE = "SYSDATE";
  
}
