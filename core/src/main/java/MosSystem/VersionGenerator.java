/*
 * Created on Feb 16, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package MosSystem;

import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author MMorris
 * 
 * This is a static class that reads the current 
 * build version from the property file build.porperties.  
 * 
 * This file is included in the warfile at build time.
 */
public class VersionGenerator
{

    public static String getVersion()
    {

        ResourceBundle bundle = null;
        try
        {

            InputStream in = VersionGenerator.class.getClassLoader()
                    .getResourceAsStream("build.properties");
            bundle = new PropertyResourceBundle(in);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return bundle.getString("release.name") + "." + bundle.getString("release.patch");
    }
    
    public static void main(String args[]){
        
        
        System.out.println("Release name - " + VersionGenerator.getVersion());
    }

}