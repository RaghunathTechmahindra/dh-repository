// Sc: System constants

/**
 * Date: June 25 2008<br>
 * @author: MCM Implementation Team<br>
 * Change:  <br>
 *	added 4 new constants for DB props
 *  COMPONENT_LOC_PROP_FILE_NAME,COMPONENT_LOC_CHILD_PROP_FILE_NAME,
 *  COMPONENTMTG_PROP_FILE_NAME,COMPONENTINMTG_PROP_FILE_NAME
 * @version 1.1 XS_2.32 change add CONFIRMDELETE, CONFIRM_DELETE_COMPONENT
 * @version 1.2 July 29, 2008 XS_2.60 change add COMPONENTINFO_MTG_PROP_FILE_NAME
 *                                 COMPONENTINFO_LOC_PROP_FILE_NAME
 */

package MosSystem;

public interface Sc extends Mc
{
  // Define the Version Number here
  //public static final String BX_VERSION = "Release3.0 Build 1.0";
    public static final String BX_VERSION = VersionGenerator.getVersion();

  public static final  String COMBO_BOX_NAME = "cbPageNames";
  public static final  String TB_DEAL_ID     = "tbDealId";

  public static final String STANDARD_DATE_DISPLAY_FORMAT	= "dd/mm/yyyy";
  public static final String STANDARD_DATE_INPUT_FORMAT 	= "dd/mm/yyyy";

  // Security Policy record id
  public static final int PASSWORD_POLICY_ALLOWED_AUTH_ATTEMPTS = 1;
  public static final int PASSWORD_POLICY_CHANGE_DAYS = 2;
  public static final int PASSWORD_POLICY_GRACE_LOGINS= 3;
  public static final int PASSWORD_POLICY_WARN_DAYS   = 4;
  public static final int PASSWORD_POLICY_PASSWORD_STRENGTH   = 5;
  public static final int PASSWORD_POLICY_PASSWORD_MIN_LENGTH = 6;
  public static final int PASSWORD_POLICY_PASSWORD_MAX_FAILED = 7;
  public static final int PASSWORD_POLICY_PASSWORD_RETENTION = 8;
  public static final int PASSWORD_POLICY_NEWPASSWORD_VALID_DAYS = 9;
  public static final int PASSWORD_POLICY_PASSWORD_CONSECUTIVE_CHAR_ALLOWED = 10;
  public static final int PASSWORD_POLICY_PASSWORD_CHAR_SEQUENCING_ALLOWED = 11;

  // Password Strength definition
  public static final int PASSWORDD_STRENGTH_NONE       = 0;
  public static final int PASSWORDD_STRENGTH_MIXED_CASE = 1;
  public static final int PASSWORDD_STRENGTH_ALPH_NUM   = 2;
  public static final int PASSWORDD_STRENGTH_SPEC_CHAR  = 3;
  public static final int PASSWORDD_STRENGTH_MIXED_AND_ALPHNUM = 4;
  public static final int PASSWORDD_STRENGTH_ALL        = 5;

  // Constant to reset Failed Password Attempts
  public static final int PASSWORD_FAILED_ATTEMPTS_TRUE = 0;

  // Profile Status codes (records in table profileStatus)
  public static final int PROFILE_STATUS_ACTIVE      =  0;
  public static final int PROFILE_STATUS_SUSPENDED   =  1;
  public static final int PROFILE_STATUS_DEACTIVE    =  2;
  public static final int PROFILE_STATUS_DISALLOWED  =  3;
  public static final int PROFILE_STATUS_CAUTION     =  4;
  public static final int PROFILE_STATUS_ONLEAVE     =  5;

  // UserSecurityPolicy Status codes
  public static final int USERSECUTITYPOLICY_STATUS_NEW     =  0;
  public static final int USERSECUTITYPOLICY_STATUS_CURRENT =  1;
  public static final int USERSECUTITYPOLICY_STATUS_PREVIOUS=  2;

  // SOB-Firm Association Status codes

  public static final char SOB_FIRM_STATUS_ACTIVE   =  'A';
  public static final char SOB_FIRM_STATUS_PREVIOUS =  'P';

  // Page access permission codes (records in table access type)
  public static final int PAGE_ACCESS_UNASSIGNED            =  0;
  public static final int PAGE_ACCESS_DISALLOWED            =  1;
  public static final int PAGE_ACCESS_VIEW_ONLY             =  2;
  public static final int PAGE_ACCESS_EDIT                  =  3;
  public static final int PAGE_ACCESS_SUSPENDED             =  4;
  public static final int PAGE_ACCESS_EDIT_ON_DECISION_MOD  =  5;
  //--Ticket#781--XD_ARM/VRM--19Apr2005--start--//
  public static final int PAGE_ACCESS_NOT_FOUND = -1;
  //--Ticket#781--XD_ARM/VRM--19Apr2005--end--//

  // Assigned Task status codes
  public static final int TASK_OPEN			= 1;
  public static final int TASK_REROUTE			= 2;
  public static final int TASK_COMPLETE			= 3;
  public static final int TASK_CANCELLED		= 4;

  /*
  blic static final int	ASSIGNED_TASK_STATUS_UNUSED				= 0;
  public static final int	ASSIGNED_TASK_STATUS_OPEN				= 1;	//!!!! ONLY THIS ONE IS USED
  public static final int	ASSIGNED_TASK_STATUS_RE_ROUTE			= 2;
  public static final int	ASSIGNED_TASK_STATUS_COMPLETED			= 3;
  public static final int	ASSIGNED_TASK_STATUS_CANCELLED			= 4;
*/
  // Assigned Task priority codes
  public static final int TASK_PRIORITY_VERY_HIGH	= 1;
  public static final int TASK_PRIORITY_HIGH	    = 2;
  public static final int TASK_PRIORITY_MEDIUM	= 3;
  public static final int TASK_PRIORITY_LOW	    = 4;
  public static final int TASK_PRIORITY_VERY_LOW	= 5;


  // Business Rule Names
  public static final String WORFLOW_SELECT_BR_NAME       =   "WORKFLOWSELECT";
  public static final String TASK_BR_NAME_TEST_COMPLETION =   "TASKCOMPLETE";
  public static final String TASK_BR_NAME_TASK_GENERATE   =   "TASKGENERATE";

  // Assigned Task completion result qualifier codes (from TASKCOMPLETE business rules)
  public static final int TASK_NOT_DONE               = 0;
  public static final int TASK_DONE                   = 1;
  public static final int TASK_LOOP_BACK_PREDECESSOR  = 2;
  public static final int TASK_LOOP_BACK_ORIGIN       = 3;
  public static final int TASK_WORKFLOW_REGRESSION    = 5;
  public static final int TASK_EXTERNAL_DECISION      = 6;
  public static final int TASK_CANCEL                 = 7;

  // Assigned Task time milestone status codes
  public static final int TASK_TIME_NORMAL		= 1;
  public static final int TASK_TIME_ESCALATED		= 2;
  public static final int TASK_TIME_EXPIRED		= 3;

  //***** Change by NBC/PP Impl. Team - GCD - START *****//
  public static final int TASK_VISIBLEFLAG_HIDDEN = 0;
  public static final int TASK_VISIBLEFLAG_VISIBLE = 1;
  //***** Change by NBC/PP Impl. Team - GCD - END *****//

  // GUI field-Entity mapping files - located in dbProperties location
  //   .property  com.basis100.picklist.displayfieldpicklist  (mossys.properties)
  public static final String DEAL_PROPS_FILE_NAME			  	= "deal.properties";
  public static final String DEAL_APPLICANT_PROPS_FILE_NAME		= "dealentryapplicant.properties";
  public static final String DEAL_PROPERTY_PROPS_FILE_NAME		= "dealentryproperty.properties";
  public static final String DOWNPAY_PROP_FILE_NAME		  		= "downpay.properties";
  public static final String ESCROWPAY_PROP_FILE_NAME	  		= "escrowpay.properties";
  public static final String PROPERTY_PROP_FILE_NAME	  		= "property.properties";
  public static final String PROPEXP_PROP_FILE_NAME		  		= "propexp.properties";
  public static final String APPLICANT_PROP_FILE_NAME	  		= "applicant.properties";
  public static final String APPADDRESS_PROP_FILE_NAME  		= "appaddress.properties";
  public static final String APPADDRESS_ADDR_PROP_FILE_NAME 	= "appaddressaddr.properties";
  public static final String EMPLOYMENT_PROP_FILE_NAME  		= "employment.properties";
  public static final String OTHRINCOME_PROP_FILE_NAME  		= "othrincome.properties";
  public static final String CREDITREF_PROP_FILE_NAME	  		= "creditref.properties";
  public static final String ASSET_PROP_FILE_NAME			  	= "asset.properties";
  public static final String LIABILITY_PROP_FILE_NAME	  		= "liability.properties";
  public static final String EMP_ADDR_PROP_FILE_NAME    		= "empaddr.properties";
  public static final String EMP_CONTACT_PROP_FILE_NAME 		= "empcontact.properties";
  public static final String EMP_INCOME_PROP_FILE_NAME  		= "empincome.properties";
  public static final String UW_MAIN_DEAL_PROP_FILE_NAME		= "underwriterdeal.properties";
  public static final String UW_ESCROW_PROP_FILE_NAME			  = "uwescrowpay.properties";
  public static final String UW_DOWNPAYMENT_PROP_FILE_NAME	= "uwdownpay.properties";
  public static final String UW_MI_COMBOBOX_PROP_FILE_NAME  = "uwmicombo.properties";
  
  //  ***** Change by MCM Impl. Team - Start *****//
  public static final String COMPONENT_LOC_PROP_FILE_NAME  = "component_IN_LOC.properties";
  public static final String COMPONENT_LOC_CHILD_PROP_FILE_NAME  = "componentLOC.properties";
  //  ***** Change by NBC Impl. Team - End *****//
  
  //--DJ_CR136--start--//
  public static final String LIFEDISABILITYINS_PROP_FILE_NAME = "lifedisabilityinsurance.properties";
  //--DJ_CR136--start--//

  //  ***** Change by NBC Impl. Team - Version 1.1 - Start *****//
  public static final String IDENTIFICATION_PROP_FILE_NAME	  		= "identification.properties";
  //  ***** Change by NBC Impl. Team - Version 1.1 - End *****//
  
  
  //  ***** Change by MCM Impl. Team - Start *****//
  public static final String COMPONENTMTG_PROP_FILE_NAME  = "componentMTG.properties";
  public static final String COMPONENTINMTG_PROP_FILE_NAME  = "component_IN_MTG.properties";
  //  ***** Change by NBC Impl. Team - End *****//

  //  ***** Change by MCM Impl. Team - Start XS_2.36 *****//
  public static final String COMPONENTCC_PROP_FILE_NAME  = "componentCC.properties";
  public static final String COMPONENTINCC_PROP_FILE_NAME  = "component_IN_CC.properties";
  //  ***** Change by NBC Impl. Team - End  XS_2.36 *****//

  //  ***** Change by MCM Impl. Team - Start XS_2.38 *****//
  public static final String COMPONENT_OVERDRAFT_PROP_FILE_NAME  = "componentOverdraft.properties";
  public static final String COMPONENT_IN_OVERDRAFT_PROP_FILE_NAME  = "component_IN_Overdraft.properties";
  //  ***** Change by NBC Impl. Team - End  XS_2.38 *****//
  
  //  ***** Change by MCM Impl. Team - XS 2.37 Start *****//
  public static final String COMPONENT_LOAN_PROP_FILE_NAME  = "component_IN_Loan.properties";
  public static final String COMPONENT_LOAN_CHILD_PROP_FILE_NAME  = "componentLoan.properties";
  //  ***** Change by NBC Impl. Team - XS 2.37 End *****//
  
  //  ***** Change by MCM Impl. Team - XS 2.60 Start *****//
  public static final String COMPONENTINFO_MTG_PROP_FILE_NAME  = "compInfoMTG.properties";
  public static final String COMPONENTINFO_LOC_PROP_FILE_NAME  = "compInfoLOC.properties";
  //  ***** Change by NBC Impl. Team - XS 2.60 End *****//
  
  
  // The beginning of the data validation properties set.

  //Property delimiter for JavaScript data field validations
  public static final char JS_PROPERTY_DELIMITER   = '|';

  //// Data validation properties files for various JavaScript client data validation.
  public static final String DVALID_DEAL_MAIN_PROPS_FILE_NAME			  	= "dvdeal.properties";
  public static final String DVALID_DEAL_TILE_APPLICANT_PROPS_FILE_NAME		= "dvdealapplicant.properties";
  public static final String DVALID_DEAL_TILE_PROPERTY_PROPS_FILE_NAME		= "dvdealproperty.properties";
  public static final String DVALID_DEAL_TILE_DOWNPAY_PROP_FILE_NAME		  		= "dvdealdownpmnt.properties";
  public static final String DVALID_DEAL_TILE_ESCROWPAY_PROP_FILE_NAME	  		= "dvdealescrow.properties";
  public static final String DVALID_DEAL_RATES_PROP_FILE_NAME		  		= "dvdealrates.properties";
  public static final String DVALID_PROPERTY_MAIN_PROP_FILE_NAME		  		= "dvproperty.properties";
  public static final String DVALID_PROPERTY_EXPENSE_PROP_FILE_NAME		  		= "dvpropertyexpense.properties";
  public static final String DVALID_UW_MAIN_PROP_FILE_NAME		= "dvuworksheetdeal.properties";
  public static final String DVALID_UW_RATES_PROP_FILE_NAME		= "dvuworksheetrates.properties";
  public static final String DVALID_UW_TILE_ESCROW_PROP_FILE_NAME			  = "dvuworksheetescrow.properties";
  public static final String DVALID_UW_TILE_DOWNPAYMENT_PROP_FILE_NAME	= "dvuworksheetdownpmnt.properties";
  public static final String DVALID_SOURCE_BUS_EDIT_PROPS_FILE_NAME = "dvsourcebusedit.properties";
  public static final String DVALID_SOURCE_FIRM_EDIT_PROPS_FILE_NAME = "dvsourcefirmedit.properties";
  public static final String DVALID_PARTY_SEARCH_PROPS_FILE_NAME = "dvpartysearch.properties";
  public static final String DVALID_PARTY_ADD_PROPS_FILE_NAME = "dvpartyadd.properties";
  public static final String DVALID_PORTFOLIO_STATS_PROPS_FILE_NAME = "dvportfoliostats.properties";
  public static final String DVALID_DEAL_RESOLUTION_PROPS_FILE_NAME = "dvdealresolution.properties";
  public static final String DVALID_SOB_SEARCH_PROPS_FILE_NAME = "dvsobsearch.properties";

  //--DJ_CR136--start--//
  public static final String DVALID_LIFE_DIS_INSURE_ONLY_APPL_PROPS_FILE_NAME = "dvlifedisinsureonly.properties";
  //--DJ_CR136--end--//

  /*************** MCM Impl team changes starts - xs_2.27 *******************/
  public static final String DVALID_COMPONENT_DETAILS_PROPS_FILE_NAME = "dvcompnentdetails.properties";  
  /*************** MCM Impl team changes ends - xs_2.27 *********************/
  
  
  //// Not created yet.
  ////public static final String DVALID_APPLICANT_PROP_FILE_NAME	  		= "dvapplicant.properties";

  //// This const duplicates its analog from the JATO world. It is done
  //// to separate the modulus.
  public static final char QUALIFIED_FIELD_NAME_DELIMITER = '_';

  // Automated E-Mail message types (for selection of standard subject lines, etc)
  public static final int EMAIL_TYPE_SYS_ADMIN_NOTICE  = 0;
  public static final int EMAIL_TYPE_SYS_ADMIN_ALARM   = 1;

  
  // System Messages (values match the contents of column messageId in the
  // systemClientMessages table
  //
  //  NOTICE: Current literal strings are used - these will change to

  //          table driven in a future release.


//--Release2.1--//
//--> Commented out all hardcoded system Messages.
//--> Modified to use ResourceBundle to get System Message in difference languages.
//--> By Billy 10Dec2002
/*********************************************************************************
///// Messages returned by product

  public static final String ERRORS_AND_WARNINGS_WINDOW_TITLE = "Warnings and Errors";


  public static final String LOGIN_INVALID_TITLE = "Login Error";

  public static final String LOGIN_IN_USE	=
  "This user ID has an active session.<BR>" +
  "To close the existing session and begin working with this user ID select 'OK.'<BR>" +
  "To return to the Sign On screen and sign on with a different user ID select 'Cancel.'";

  public static final String LOGIN_SUSPENDED	=
  "This user ID is <B>suspended</B>.<BR>" +
  "To begin working with this user ID select 'OK.'<BR>" +
  "To return to the Sign On screen and sign on with a different user ID select 'Cancel.'";

  public static final String INVALID_LOGIN =
  "Invalid logon ID and/or password entered. Please re-enter your logon ID and/or password.";

  public static final String LOGIN_ATTEMPTS_EXCEEDED =
  "Invalid logon ID and/or password entered. " +
  "The number of allowable login attempts has been exceeded. " +
  "Please contact your System Administrator to verify your account information.";

  public static final String USER_INACTIVE =
  "This logon ID has been denied access to the system. " +
  "Please contact your System Administrator to verify your account information.";

  public static final String USER_PASSWORD_LOCKED =
  "Your password has been locked. " +
  "Please contact your System Administrator to verify your account information.";

  public static final String USER_NEWPASSWORD_LOCKED =
  "Your new password was expired. " +
  "Please contact your System Administrator to verify your account information.";

  public static final String PASSWORD_CHANGED =
  "Your new password has been saved.";

  public static final String CHG_PSWD_ATTEMPTS_EXCEEDED =
  "Invalid current password entered. " +
  "The number of allowable attempts has been exceeded. " +
  "You must cancel your current session. " +
  "Please contact your System Administrator to verify you account information.";

  public static final String INCORRECT_PASSWORD =
  "The current password entered is invalid. Please re-enter your password.";

  public static final String INVALID_NEW_PASSWORD =
  "The new password entered is invalid with the following reason(s).  Please re-enter. <br>";

  public static final String PASSWORDS_NO_NOT_MATCH =
  "The new and confirmation passwords do not match. " +
  "Please re-enter the new and confirmation passwords.";

  public static final String PASSWORD_WILL_EXPIRE = "";
  public static final String PASSWORD_EXPIRED		= "";


  public static final String NO_MATCH_FOR_GO_DEAL_NUMBER =
  "There was no match for the Go To Deal Number. " +
  "Please re-enter the deal number, or use Deal Search " +
  "to find the correct deal number.";

  public static final String NO_WORK_QUEUE_ACCESS =
  "Your account does not provide access to a Work Queue. " +
  "Please contact your System Administrator for instructions.";

  public static final String PAGE_UNDER_CONSTRUCTION =
  "This page is currently under construction.";

  public static final String SINGULAR_CAUTION_NOT_ALLOWED =
  "All conditions have been marked as having a status of either 'Approved' or " +
  "'Waived', except for one which is set to 'Caution'." +
  " The deal can not progress. Please either change the status from 'Caution,'" +
  " or re-evaluate the status of the other conditions.";

  public static final String CAN_NOT_REQUEST_DOCUMENTS =
  "Documents can not be requested from this page. " +
  "Please either change the status of the requested document, or, " +
  "if you wish to request a document, please go to the 'Conditions Review' screen.";


  public static final String TASK_RELATED_UNEXPECTED_FAILURE =
  "A problem occurred while processing your request. Please re-enter your request. " +
  "If this problem recurs please report the incident to your System Administrator.";

  public static final String TASK_RELATED_STRANDED_DEAL =
  "Warning! This deal has not reached resolution, but has no remaining open tasks. " +
  "Please record the deal number and report this incident to your System Administrator.";


  public static final String TASK_PROCESSED_NO_ERROR =
  "Your request was successfully processed.";

// Toni

  public static final String DEALID_MISSING =
  "The deal number is missing.";

  public static final String DEAL_NOTE_NOTE_MISSING =
  "Please enter text for the deal note.";

  public static final String DEAL_NOTE_CATEGORY_INVALID =
  "Please select a valid deal note category.";

  public static final String PASSWORD_CHANGE_REQUIRED =
  "Your password has expired. Please select a new password.";

  public static final String PASSWORD_CHANGE_ONE_GRACE =
  "Your password has expired. You have one grace log on remaining.";

  public static final String PASSWORD_CHANGE_MANY_GRACE =
  "Your password has expired. You have % grace log ons remaining.";

  public static final String PASSWORD_CHANGE_ANNOY_WARNING =
  "Your password will expire on %. " +
  "You may change your password now to avoid further notices.";

  public static final String FUND_DATE_INVALID =
  "Please enter a valid Funded Date in the requested format.";

  public static final String REVERSE_COMMITMENT_CONFIRM =
  "To reverse the decision or unlock the deal, select an option " +
  "in the 'Modify Decision' drop-down list.";

  public static final String DEAL_RESOLUTION_MISSING_RESOLUTION =
  "A deal resolution option must be selected.";


//UGLY ***
  public static final String DEAL_RESOLUTION_APPROVAL_OPTION_NOT_ALLOWED_STATUS_CATEGORY =
  "This deal can not be approved at this time.";

  public static final String DEAL_RESOLUTION_APPROVAL_MISSING_APPROVAL_TYPE =
  "Please select an approval type.";

  public static final String DEAL_RESOLUTION_APPROVAL_EAR_OPTION_REQUIRED =
  "This deal requires external lender approval. " +
  "To approve this deal in the 'Approval Type' drop-down list please select External Approval Granted.";

  public static final String DEAL_RESOLUTION_APPROVE_WITH_CHANGE_NOT_ALLOWED =
  "The 'Approve with Changes' option is available only when a higher approval has been requested, or when " +
  "external lender approval was requested, and the lender declined but released the deal.";

  public static final String DEAL_RESOLUTION_APPROVE_EXTERNAL_APPROVAL_GRANTED_NOT_ALLOWED =
  "The 'External lender approval granted' option is available only when " +
  "the previous option was 'External lender approval requested'.";

  public static final String DEAL_RESOLUTION_APPROVE_REQUEST_EXTERNAL_APPROVAL_NOT_ALLOWED =
  "'External lender approval requested' is available only for deal that have an external lender.";

  public static final String DEAL_RESOLUTION_HOLD_OPTION_NOT_ALLOWED_STATUS_CATEGORY =
  "The 'Hold' option is only available before the deal has been decisioned.";

  public static final String DEAL_RESOLUTION_HOLD_MISSING_HOLD_OPTION =
  "Please define how long the deal is to be held. " +
  "Enter either the number of days to hold the deal, " +
  "or the date the deal is to be held to.";

  public static final String DEAL_RESOLUTION_HOLD_INVALID_NUMBER_OF_DAYS =
  "The number of days to hold the deal is invalid. " +
  "Please re-enter the number of days the deal is to be held.";

  public static final String DEAL_RESOLUTION_HOLD_INVALID_HOLD_DATE =
  "The date the deal to be held until is invalid. Please re-enter the date.";

  public static final String DEAL_RESOLUTION_DENY_CONFIRM_DIALOG =
  "You are about to deny this deal. " +
  "Select 'OK' to deny the deal. " +
  "Select 'Cancel' to return to the Deal Resolution screen.";

  public static final String DEAL_RESOLUTION_COLLASPE_OPTION_NOT_ALLOWED_STATUS_CATEGORY =
  "The deal cannot be collapsed, due to the current deal status.";

  public static final String DEAL_RESOLUTION_DENY_MISSING_DENY_REASON =
  "Please select a reason to deny the deal.";

  public static final String DEAL_RESOLUTION_COLLASPE_CONFIRM_DIALOG =
  "You are about to collapse this deal. "+
  "Select 'OK' to deny the deal. " +
  "Select 'Cancel' to return to the Deal Resolution screen.";

  public static final String UW_DEAL_RESOLUTION_NOT_ALLOWED_STATUS_CATEGORY =
  "This deal cannot be resolved, due to the current status. " +
  "It is likely the deal has already been resolved.";

  public static final String UW_CANNOT_RESOLVE_LOCKED_SCENARIO =
  "This scenario can not be resolved, as the scenario is currently locked. " +
  "Please select an unlocked scenario.";

  public static final String UW_CANNOT_RESOLVE_VALIDATION_CRITICAL =
  "This scenario can not be resolved, due to critical data conditions. " +
  "Please review the critical errors listed in the 'Warnings and errors' window.";

  public static final String UW_SCENARIO_SAVED_WITH_MESSAGES =
  "The current scenario has been saved. " +
  "Please review the information listed in the 'Warnings and errors' window.";

  public static final String UW_SCENARIO_SAVED_CLEAN =
  "The current scenario has been saved. ";

  public static final String UW_CREATE_SCENARIO_SAVE_CURRENT =
  "A new scenario will be created, but the current scenario has been changed." +
  "Select 'OK' to save the current scenario. " +
  "Select 'Cancel' to discard changes in the current scenario.";

  public static final String UW_SWITCH_SCENARIO_SAVE_CURRENT =
  "Switching to selected scenario, but the current scenario has been changed." +
  "Select 'OK' to save the current scenario. " +
  "Select 'Cancel' to discard changes in the current scenario.";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_APPLICANT =
  "You are about to delete this applicant. " +
  "Select 'OK' to delete this applicant. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_PROPERTY =
  "You are about to delete this property. " +
  "Select 'OK' to delete this property. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_DOWN_PAYMENT =
  "You are about to delete this down payment. " +
  "Select 'OK' to delete this down payment. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ESCROW_PAYMENT =
  "You are about to delete this escrow payment. " +
  "Select 'OK' to delete this escrow payment. " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_PROPERTY_EXPENSE =
  "You are about to delete this property expense. " +
  "Select 'OK' to delete this property expense. " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ADDRESS =
  "You are about to delete this address. " +
  "Select 'OK' to delete this address. " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_EMP_RECORD =
  "You are about to delete this employment record. " +
  "Select 'OK' to delete this employment record. " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_INCOME_RECORD =
  "You are about to delete this income record. " +
  "Select 'OK' to delete this income record. " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_CREDIT_REFERENCE =
  "You are about to delete this credit reference record. " +
  "Select 'OK' to delete this credit reference record. " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ASSET =
  "You are about to delete this asset. " +
  "Select 'OK' to delete this asset. " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_LIAB =
  "You are about to delete the selected liability record(s). " +
  "Select 'OK' to delete the selected liability record(s). " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL_INCOMPLETE_STATUS =
  "The deal has been saved, however the deal is incomplete, because " +
  "critical deal information is missing. " +
  "Please review the information listed in the 'Warnings and errors' window. " +
  "Select 'OK' to leave the @ screen. " +
  "Select 'Cancel' to return to the @ screen and add more information.";

  public static final String DEAL_ENTRY_MOD_SAVED_WITH_WARNINGS =
  "The deal has been saved, however one or more non-critical data conditions are missing. " +
  "Please review the  information listed in the 'Warnings and errors' window. " +
  "Select 'OK' to leave the @ screen. " +
  "Select 'Cancel' to return to the @ screen and add more information.";

  public static final String DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL =
  "The deal has been saved, however one or more critical data conditions are missing. " +
  "Please review the  information listed in the 'Warnings and errors' window. " +
  "Select 'OK' to leave the @ screen. " +
  "Select 'Cancel' to return to the @ screen and add more information.";

  public static final String DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS =
  "The deal has been saved. The deal number is #.";

  public static final String PAGE_ACCESS_DENIED =
  "The deal you have specified cannot be opened in the screen you selected, " +
  "because of the current deal status. " +
  "For more information please contact your System Administrator.";

  public static final String PAGE_ACCESS_ERROR =
  "There was a system error in determining if the specified deal is in an appropriate status " +
  "for the screen you selected. Access has been denied. " +
  "For more information please contact your System Administrator.";

  public static final String FATAL_SESSION_ENDED =
  "A system error has occured. You will be logged off immediately. " +
  "For further details please contact your System Administrator.";

  public static final String PAGE_DENIED_DEFAULT =
  "Access to this screen is denied. " +
  "Please contact your System Administrator";

  public static final String PAGE_DENIED_DEAL_LOCKED =
  "Another user is currently working on this deal and the deal is locked.";

  public static final String SUBPAGE_EXIT_VIA_SUBMIT_OR_CANCEL =
  "To leave this screen select either 'Submit' or 'Cancel'";

  public static final String SUBPAGE_EXIT_VIA_OK =
  "You are attempting to leave a secondary screen, without first returning to the main screen. " +
  "Please select 'OK' or 'Submit' on the secondary screen";

  public static final String CONFIRM_CANCEL_DEAL_ENTRY =
  "You are about to leave this screen without saving the new deal. " +
  "Select 'OK' to leave the @ screen without saving the details of the new deal. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String PAGE_ACCESS_DENIED_MSG =
  "You do not have access permission for the screen you selected. " +
  "Please contact your System Administrator.";

  public static final String PAGE_ACCESS_SUSPENDED_MSG =
  "Your access to the screen you selected has been suspended. " +
  "Please contact your System Administrator.";

  public static final String CLOSING_ACTIVITY_MISSING_SOLICITOR =
  "This deal can not be closed, because a Solicitor has not been assigned." +
  "Please assign a Solicitor to this deal.";

  public static final String TECH_FAILURE_LOGIN =
  "A problem occurred while processing your request. Please re-enter your request. " +
  "If this problem recurs please report the incident to your System Administrator.";

  public static final String FEE_TYPE_DUPLICATION_WARNING=
  "The fee types are not unique";

  public static final String MATURITY_DATE_INVALID =
  "Please enter a valid new 'Maturity Date' in the requested format.";

    public static final String MATURITY_DATE_CHANGED =
  "Please enter a 'New Maturity Date' that is in the future";

  public static final String RATE_EFFECTIVE_DATE_INVALID =
  "Please enter a valid 'Rate Date' in the requested format.";

  public static final String RATE_EFFECTIVE_DATE_ACTUAL =
  "Please enter an 'Effective Date/Time' that is in the future";

  public static final String RATE_STATUS_DISABLED =
   "You are about to permanently disable this rate code. " +
  "Select 'OK' to disable this rate code. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String RATE_POSTED_AND_DISCOUNT_INVALID =
   "Enter a valid 'Posted Rate' and 'Max Discount Allowed'.";

  public static final String RATE_SHARP_RAISE =
   "You are about to adjust the Posted Rate by more than half a percent. " +
  "Select 'OK' to adjust the Posted Rate. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String RATE_STATUS_NOT_SELECTED =
  "Please select a Rate Status.";

  public static final String DEAL_APPROVE_CONDITIONS_INFO =
  "The following approval conditions were detected.";

  public static final String DEAL_APPROVE_ESCALATE =

  "This deal exceeds your approval authority. " +
  "Select 'OK' to forward this deal to a higher or joint approver. " +
  "Select 'Cancel' to return to the Deal Resolution screen.";

  public static final String DEAL_APPROVE_ESCALATE_WITH_USER =

  "This deal exceeds your approval authority, " +
  "and can be forwarded to ^ for the required joint or higher approval. " +
  "Select 'OK' to forward this deal to a higher or joint approver. " +
  "Select 'Cancel' to return to the Deal Resolution screen.";

  public static final String DEAL_APPROVE_CANT_ESCALATE =
  "This deal exceeds your approval authority, but a higher or joint approver cannot be assigned. " +
  "Contact your System Administrator to increase your approval authority.";

  public static final String DEAL_APPROVE_INSUFFICIENT_LOB_LIMIT =
  "The deal amount exceeds your approval limit for this line of business. " +
  "This deal requires higher approval.";

  public static final String MI_INDICATOR_INVALID_INFO_MESSAGE =
  "The Mortgage Insurance Indicator is not valid.";

  public static final String EMAIL_ALERT_RUNNING_SHORT_ON_IP_MTG_SERVICING_NUMBERS =
  "The system is running low on assignable Mortgage Servicing Numbers. " +
  "The Institution Profile record requires maintenance.";

  public static final String EMAIL_ALERT_RUNNING_SHORT_ON_IP_CLIENT_NUMBERS =
  "The system is running low on assignable Client Numbers. " +
  "The Institution Profile record requires maintenance";

  public static final String PARTY_APPROVE_DELETE =
  "You are about to remove this party from the deal. " +
  "Select 'OK' to remove this party from the deal. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String PARTY_DISASSOCIATE_FROM_DEAL =
  "You are about to replace an existing party on the deal. " +
  "Select 'OK' to replace the party. " +
  "Select 'Cancel' to return to the @ screen.";

///// Changed since seen by product

//  public static final String NOT_SUPPORTED_FROM_DEAL_SEARCH =
//  "The selected function is not available from the Deal Search screen.";
// ... to

  public static final String FUNCTION_NOT_SUPPORTED =
  "The selected function is not available from the @ screen.";


///// Not seen by product

  public static final String CONDITION_REVIEW_DELETE =
  "You are about to delete this condition. Do you want to continue?";

  public static final String CONDITION_REVIEW_DOCUMENT_TEXT_ABSENT =
  "Please enter a text into Condition field of the free form condition.";

  public static final String CONDITION_REVIEW_DOCUMENT_LABEL_ABSENT =
  "Please enter a text into Document Label field of the free form condition.";

  public static final String DEAL_RESOULTION_ENTER_APPROVAL_NOTE =
  "Please enter a note regarding the deal approval.";

  public static final String DEAL_RESOULTION_ENTER_COLLASPE_NOTE =
  "Please enter a note regarding the deal collapse.";

  public static final String DEAL_RESOULTION_ENTER_DENY =
  "Please enter a note regarding the deal denial.";

  public static final String DEAL_RESOULTION_ENTER_CHANGES =
  "Please enter a note describing the desired changes required for your approval of the deal.";

  public static final String DEAL_RESOULTION_ENTER_EXT_LENDER_RELEASE =
  "Deal has been placed in recieved status for re-underwriting. Please enter a note regarding the external lender's denial.";

  public static final String DEAL_RESOULTION_ENTER_APPROVE_WITH_CHANGES_NOTE =
  "Please enter a note regarding the changes you have performed on the deal (for the higher approver).";

  public static final String DEAL_RESOULTION_ENTER_EXT_LENDER_APPROVAL_REQUEST =
  "Please enter a note regarding the requirement for external lender approval.";

  public static final String DEAL_RESOULTION_ENTER_DEAL_EXTENDED_NOTE =
  "Please enter a note regarding the hold on the deal.";

  public static final String PARTY_REASSOCIATE =
  "A party of this type already exists on this deal. Do you want to replace the existing party with this new one? " +
  "Select 'OK' to replace existing party.  Select 'Cancel' to retain existing party.";

  public static final String PARTY_ASSOCIATION_NOT_ALLOWED =
  "You must use the Appraiser Review screen to add an appraiser to a deal, in order to assign the correct property.";

  public static final String PARTY_INCORRECT_PARTY_TYPE =
  "Invalid party type selected";

  public static final String DEAL_SEARCH_NO_MATCH =
  "There were no deals located for the search information provided. Please try again.";


  public static final String PARTY_SEARCH_DEAL_MODE_CANNOT_CHANGE_PARTY_TYPE =
  "The party type cannot be changes when the @ screen is being used with a selected deal.  Type has been restored.";


  // ALERT
  public static final String CONDITION_REVIEW_ADD_TYPE =
  "Please select a condition type.";

  public static final String MODIFY_PARTY_CANNOT_CHANGE_TYPE =
  "When editing an existing party record the party type cannot be changed. Type has been restored";

  public static final String APPRAISAL_REVIEW_CANNOT_ENTER =
  "There does not appear to be any properties for the selected deal. The @ screen cannot be accessed " +
  "for such a deal. If this is not the case (the deal has at least one property) please report this incident " +
  "to the System Administrator.";

  public static final String SOURCE_BUSINESS_SEARCH_NO_MATCH =
  "There were no source businesses located for the search information provided. Please try again.";

  public static final String CONDITION_REVIEW_RESPONSIBLE_NOT_FOUND =
  "The Responsible Person wasn't found.";

  public static final String CLOSING_ACTIVITY_SOLICITOR_PACKAGE =
  "A solicitors package has already been produced. " +
  "Do you want another copy.";

  public static final String CLOSING_ACTIVITY_PROVIDE_INSTRUCTIONS =
  "Missing solicitor instructions - please enter instructions. ";

   public static final String DOCUMENT_TRACKING_REJECT_CONDITION =
  "Rejecting this condition will Deny the deal. Do you wish to continue?";

  public static final String FEE_REVIEW_UNDEFINED_FEE_TYPE_WARNING=
  "At least one line with undefined fee type still on the page. Please either define" +
  " or delete this(these) line(s).";

  public static final String BUREAU_DATE_INVALID =
  "Please enter a valid Credit Bureau Date in the requested format.";

  public static final String BUREAU_PULLED_DATE_INVALID =
  "Please enter a valid Date Credit Bureau Pulled in the requested format.";


  public static final String FEE_REVIEW_DELETE =
   "You are about to delete this fee. To delete this fee select 'OK'. To return to your work select 'Cancel'.";

   public static final String USER_PASSWORD_NOT_MATCH_VERIFICATION =
  "Please re-enter and verify password.";

  public static final String USER_PROFILE_ASSIGNED_GROUP =
  "User profile must be assigned a group.";

  public static final String USER_PROFILE_SUCCESSFULLY_UPDATED =
  "The user profile was successfully updated.";

  public static final String NO_USER_TYPE_IS_SELECTED =
  "You must select a user type.";

  public static final String FUNDING_ACTIVITY_ERROR =
  "Errors related to funding have been encountered. " +
  "Please review the messages listed in the 'Warnings and errors' window.";

  public static final String CHG_PSWD_MSG_TITLE = "Change Password";


////////////////////////////////////////////////////////////////////

  public static final String CHG_PSWD_PASSWORD_SUCCESSFULLY_CHANGED =
  "Password successfully changed.";

  public static final String CHG_PSWD_ALL_FIELDS_MUST_BE_ENTERED =
  "All fields must be entered.";


  public static final String CHG_PSWD_NEW_NOT_MATCH_VERIFICATION =
  "Your new password and verification of new password do not match.";

  public static final String CHG_PSWD_INCORRECT_PASSWORD =
  "The current password entered is invalid. Please re-enter your password.";

  public static final String DEC_MOD_REUNDERWRITING_REQUEST = " You are about to reset this "
  + "deal status to \"Received\". The deal will have to be re-underwritten. Do "
  + "you want to continue?";


  public static final String DEC_MOD_ENTER_RECEIVED_STATUS_NOTE =
  "Please enter a note regarding the decision modification of the deal.";

  public static final String BRIDGE_APPROVE_DELETE =
  "You are about to delete this bridge. " +
  "Select 'OK' to delete this bridge. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String SOURCE_BUSINESS_SEARCH_NO_RECORD_FOUND =
  "0 records found. Please try again.";

  public static final String SOURCE_BUSINESS_SEARCH_INPUT_REQUIRED =
  "You must enter data in at least one field..";

  public static final String BUSINESS_RULE_CRITICAL =
  "Business Rules:Error(s) have been encountered.";

  public static final String CONFIRM_UNSAVED_WORK_JUMP_OLD =     /// !!!! not used - now replaced by following !!!!
  "You have unsaved work on this screen. " +
  "To leave this screen without saving your changes select 'OK'. " +
  "To return to your work, select 'Cancel'";

  public static final String CONFIRM_UNSAVED_WORK_JUMP =
  "You are leaving the @ screen. Your changes have not yet been saved. " +
  "To continue without saving click 'OK'. " +
  "To return to @ screen click 'Cancel'";


  public static final String CONFIRM_UNSAVED_WORK_CANCEL_OLD =   /// !!!! not used - now replaced by following !!!!
  "You have unsaved work on this screen. " +
  "To leave this screen without saving your changes select 'OK'. " +
  "To return to your work, select 'Cancel'";

  public static final String CONFIRM_UNSAVED_WORK_CANCEL =
  "You are leaving the @ screen. Your changes have not yet been saved. " +
  "To continue without saving click 'OK'. " +
  "To return to @ screen click 'Cancel'";


  public static final String CONFIRM_CANCEL_CHANGES_OLD =        /// !!!! not used - now replaced by following !!!!
  "You are about to leave this screen without saving your changes. " +
  "Select 'OK' to leave the @ screen. " +
  "Select 'Cancel' to return to the @ screen.";

  public static final String CONFIRM_CANCEL_CHANGES =
  "You are leaving the @ screen. Your changes have not yet been saved. " +
  "To continue without saving click 'OK'. " +
  "To return to @ screen click 'Cancel'";

  public static final String FEE_REVIEW_INVALID_NUMBER =
  "Invalid fee amount entered. ";

  public static final String CHG_PSWD_NEW_PSWD_MUST_BE_DIF_OLD_PSWD =
  "The password you entered is the same as your existing password. Please enter a new password.";

  public static final String DEAL_SEARCH_INPUT_REQUIRED =
  "You must enter data in at least one field.";

  public static final String REASSIGN_TASK_NO_USER_TO =
  "Please select the user you want to assign this task to.";

  public static final String REASSIGN_TASK_NO_USER_FROM =
  "Please select the user whose task you want to assign.";

  public static final String REASSIGN_TASK_INVALID_USER_TO =
  "The user you assigned this task to is an invalid user type.";

  public static final String  REASSIGN_TASK_USERS_TYPE_NOT_MATCHED =
  "From and To user types are not matched.  You must re-assign deals to the same user type.";

  public static final String REASSIGN_TASK_NOACTIVE_USER_FOUND =
  "No active user found.  Task cannot re-assign.";

  public static final String REASSIGN_TASK_DIFFERENT_USER_TO =
  "The user you assigned this task to repeats the original user";

  public static final String REASSIGN_TASK_NO_CURRENT_TASK =
  "There is no current task or deal to reassign";

  public static final String USER_ADMIN_HIGHER_JOINT_APPROVER =
  "Higher Approver OR Joint Approver required for all users except for 'Senior Manager',"
  + "'Junior Administrator','Administrator','Senior Administrator','System Administrator',"
  + "'Investor Administrator','Passive'.";

  public static final String USER_ADMIN_LINE_OF_BUSINESS_REQUIRED =
  "This user type is a decisioning user. A Line of Business Limit of $0 or more is required for all Lines of Business.";

  public static final String USER_ADMIN_LINE_OF_BUSINESS_NOT_REQUIRED =
  "This user type is a non-decisioning user. Line of Business Limits are invalid for non-decisioning users.";

  public static final String USER_ADMIN_HIGHER_JOINT_APP_NOT_REQ =
  "This user type is a non-decisioning user. Higher or Joint Approvers are invalid for non-decisioning users.";

  public static final String USER_ADMIN_EMPTY_LOGIN_FIELD =
  "One or more of the following manditory fields missing User LoginID, User Profile BusinessID.";

  public static final String USER_ADMIN_LOGIN_ALREADY_EXIST =
  "User LoginID already exists in the system.";

  public static final String USER_ADMIN_BUSINESSID_ALREADY_EXIST =
  "User Business ID already exists in the system.";

  public static final String USER_ADMIN_EMPTY_CONTACT_INFO =
  "One or more of the following manditory fields missing Title, First Name, Initial, "
  + "Last Name, Work Phone, Fax.";

  public static final String USER_ADMIN_EMAIL_ADDR_MISSING =
  "Email Address is missing.";

  public static final String USER_ADMIN_PASSWORD_REQUIRED =
  "Password is required.";

  public static final String USER_ADMIN_UD_PARTNER_MUST_BE_ADMIN =
  "The Underwriter's Partner must be an Administrator.";

  public static final String USER_ADMIN_ADMIN_PARTNER_MUST_BE_UD =
  "The Administrator's Partner must be an Underwriter.";

  public static final String USER_ADMIN_USER_TYPE_IS_REQUIRED =
  "User type is required.";

  public static final String USER_ADMIN_INVALID_FAX_NUMBER =
  "Invalid fax number";

  public static final String USER_ADMIN_INVALID_PHONE_NUMBER =
  "Invalid phone number";

  public static final String USER_ADMIN_INVALID_EXT_NUMBER =
  "Invalid ext.";

  public static final String PARTY_SEARCH_NO_MATCH =
  "0 records found. Please try again.";

  public static final String PARTY_SEARCH_NO_INPUT =
  "Please enter data in at least one field";

  public static final String PARTY_ADD_VALIDATION_ERROR =
  "Please enter data in the following fields : ";

  public static final String DISALLOWED_DURING_VIEW_ONLY_MODE =
  "The action selected is not allowed during view only operation.";

  public static final String PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM =
  "The following problems have been detected in this deal ";

  public static final String PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM =
  "The following message(s) apply to applicant : ";

  public static final String PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM =
  "The following message(s) apply to property : ";

  public static final String UW_CREATE_SCENARIO_DISCARD_CURRENT =
  "You have selected create new scenario but there are unsaved changes in the current scenario. " +
  "If you proceed the changes in the current scenario will be discarded. " +
  "Select 'OK' to create new scenario and discard changes in the current scenario. " +
  "Select 'Cancel' to remain on the current scenario.";

  public static final String UW_SWITCH_SCENARIO_DISCARD_CURRENT =
  "You have selected switch scenario but there are unsaved changes in the current scenario. " +
  "If you proceed the changes in the current scenario will be discarded. " +
  "Select 'OK' to switch scenario and discard changes in the current scenario. " +
  "Select 'Cancel' to remain on the current scenario.";

  public static final String DEAL_RESOLUTION_DENY_OPTION_NOT_ALLOWED_STATUS_CATEGORY =
  "The deal cannot be denied, due to the current deal status.";

  public static final String DEAL_RESOLUTION_DENY_REASON_CAN_NOT_BE_USED =
  "The deal deny reason can not be used at this time.";

  public static final String DEAL_RESOLUTION_DENY_EXTERNAL_LENDER_REFUSED =
  "The deal has been refused by external lender.";


  // Dialog Messages
  public static final String MI_REVIEW_MIINDICATOR_ERROR_MSG =
  "The mortgage insurance indicator is incorrect.  <br>" +
  "To update the mortgage insurance indicator and associated details select [<b>OK</b>].  <br>" +
  "To leave the insurance indicator as it is select [<b>CANCEL</b>].";

  public static final String MI_VALIDATION_ERROR_MSG =
  "Critical errors found, please review messages in the 'Warnings and errors' window. " +
  "To ignore errors and continue select 'OK'. " +
  "To cancel submit select 'Cancel'.";

  public static final String MI_VALIDATION_CANCEL_ERROR_MSG =
  "Critical errors found - please review the critical errors listed in the 'Warnings and errors' window. ";

  public static final String MI_REVIEW_RCMSTATUS_NOT_AVALIABLE_MSG =
  "The mortgage insurance application cannot be automatically submitted. <br>" +
  "<b>Do you want to continue? </b>";

  public static final String MI_REVIEW_RCMSTATUS_NOT_AVALIABLE_OPEN_REQ_MSG =
  "The mortgage insurance application cannot be automatically submitted, but a request for mortgage insurance has already been submitted.<br>" +
  "<b>Do you want to cancel the request? </b>";

  public static final String MI_REVIEW_RCMSTATUS_NOT_REQUIRED_MSG =
  "Mortgage insurance is not required, but has been requested. <br>" +
  "<b>Do you want to continue? </b>";

  public static final String MI_REVIEW_RCMSTATUS_NOT_REQUIRED_OPEN_REQ_MSG =
  "Mortgage insurance is not required, but a request for mortgage insurance has already been submitted. <br>" +
  "<b>Do you want to cancel the request? </b>";

  public static final String MI_REVIEW_RCMSTATUS_NOT_ENOUGH_INFO_MSG =
  "There is not enough information to automatically request mortgage insurance. <br>" +
  "<b>Do you want to continue without requesting mortgage insurance? </b>";

  public static final String MI_REVIEW_RCMSTATUS_INIT_REQ_PENDING_MSG =
  "Mortgage insurance has not yet been requested. <br>" +
  "<b>Do you want to automatically request it? </b>";

  public static final String MI_REVIEW_RCMSTATUS_ERROR_CORR_PENDING_MSG =
  "Mortgage Insurance Errors Were Reported Previously. <br>" +
  "<b>Do you want to resend insurance request? </b>";

  public static final String MI_REVIEW_RCMSTATUS_CHANGE_PENDING_MSG =
  "Mortgage Insurance has already been Approved. <br>" +
  "<b>Do you want to send changes? </b>";

  public static final String MI_REVIEW_RCMSTATUS_CHANGE_REQ_NOT_AVALIABLE_MSG =
  "The mortgage insurance application cannot be automatically re-submitted at this time. Response from previous submission still outstanding.";
  // ======== MI Review Definition (End) =================

  public static final String MI_DATA_CHANGED_WARNING =
  "The changes being made to the deal may invalidate the Mortgage Insurance Information already " +
  "received or pending from insurer.  Please check the deal and resubmit deal to Mortgage Insurer " +
  "if necessary.";

  public static final String STANDARD_REVIEW_MESSAGES_WINDOW =
  "Please review the messages listed in the 'Warnings and errors' window.";

  public static final String UW_MI_REVIEW_CHANGES =
  "There are unsaved changes in the current scenario. " +
  "If you proceed the changes in the current scenario will be discarded. " +
  "Select 'OK' to goto Mortgage Insurance Review and discard changes in the current scenario. " +
  "Select 'Cancel' to remain on the current scenario.";

   public static final String DEAL_MOD_NEW_DEAL_CREATED =
  "Deal number # successfully created.";

  public static final String SIGN_OFF_MSG_CONFIRM =
  "You are about to log off from the system. To end this session select 'OK'." +
  " To return and continue working select 'Cancel'.";

  public static final String DEC_MOD_ENTER_NON_CRITICAL_CHANGE_NOTE =
  "Please enter a note regarding the decision modification of the deal.";

  public static final String DUPECHECK_FOUND_DUPLICATED =
  "Duplicated record(s) found -- select 'OK' to continue.";

  public static final String DEAL_ENTRY_ADD_BORROWER_COPY_ADDR =
  "Do you want to copy the current address of the Primary Borrower?";

  public static final String CCM_ACTION_NOT_ALLOWED_MI_CHECK_FAILED =
  "Action not allowed : The mortgage insurance application cannot be Cancelled at this time. " +
  "Response from previous submission still outstanding";

  public static final String UNDERWRITER_USER_ID_HAS_NOT_BEEN_ASSIGNED =
  "The Underwriter Worksheet is unavailable till the system has automatically assigned " +
  "an underwriter. Please retry this request in 1 minute and if this message persist " +
  "contact the support desk";


  public static final String UNLOCK_POST_DECISION_DEAL_MOD_QUERY =
  "This deal has been decisioned and is currently locked. " +
  "Please select the appropriate button below to view this deal. " +
  "To reverse the decision of this deal please use Decision Modification screen.";

  public static final String UNLOCK_POST_DECISION_DEAL_MOD_QUERY_TITLE =
  "Post Decision Options - Deal # ";

  public static final String CONDITION_REVIEW_POST_DECISION_COMMITMENT_REQ =
  "Do you want to produce a commitment?";

  public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_NOT_SELECTED =
  "Please select record(s) for delete. ";

  public static final String SOURCE_OF_BUSINESS_SOURCE_FIRM_NOT_SET =
  "Source not currently linked to a firm.";

  public static final String SOURCE_OF_BUSINESS_EDIT_VALIDATION_ERROR =
  "<b>Please review the following error(s) when saving the Source Of Business record : </b>";

  public static final String SOURCE_OF_BUSINESS_UNASSOC_USER_CONFIRM =
  "You are about to delete a Source to User association.  OK to continue or Cancel.";

  public static final String SOURCE_FIRM_EDIT_VALIDATION_ERROR =
  "<b>Please review the following error(s) when saving the Source Firm record : </b>";

  public static final String SOURCE_FIRM_UNASSOC_USER_CONFIRM =
  "You are about to delete a Firm to User association.  OK to continue or Cancel.";

  public static final String CONFIRM_DELETE_CREDIT_BUREAU_LIAB =
  "You are about to delete the selected credit bureau liability record(s). " +
  "Select 'OK' to delete the selected credit bureau liability record(s). " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String CONFIRM_EXPORT_CREDIT_BUREAU_LIAB =
  "You are about to export the selected credit bureau liability record(s). " +
  "Select 'OK' to export the selected credit bureau liability record(s). " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String CONFIRM_EXPORT_DELETE_CREDIT_BUREAU_LIAB =
  "You are about to export and delete the selected credit bureau liability record(s). " +
  "Select 'OK' to export the selected credit bureau liability record(s). " +
  "Select 'Cancel' to return to the @ screen. ";

  public static final String CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED =
  "Please select record(s) for export/delete. ";

  public static final String CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_NOT_SELECTED =
  "Please select liability record(s) for export. ";

  public static final String CREDIT_BUREAU_LIAB_CONFIRM_DELETE_NOT_SELECTED =
  "Please select liability record(s) for delete. ";

  //// SYNCADD
  public static final String DEAL_MODY_IS_COMMITMENT_RESEND =
  "Do you want to produce another commitment?";

  public static final String DEAL_MODY_IS_COMMITMENT_RESEND_WITH_NO_WARNING =
  "The deal has been saved. Do you want to produce another commitment?";

  public static final String SOB_RESUBMIT_DEAL_CANNOT_REDECISION =
  "Action no allowed: Deal is not in Post Decision.";
********************************************************************************************/

//--> TODO : The following is the hardcoded messages for Ingestion / DocPrep
//--> Have to figure it out later with Zivko
//--> By Billy 10Dec2002
  public static final String PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM =
  "The following message(s) apply to applicant : ";

  public static final String PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM =
  "The following message(s) apply to property : ";
  public static final int MI_INSURER_PMI = 0;
  
//===========================================================================
  
  /****************MCM Impl Team XS_2.32 changes starts********************/
  public static final String CONFIRMDELETE = "CONFIRMDELETE";
  public static final int CONFIRM_DELETE_COMPONENT = 15;
  
  /****************MCM Impl Team XS_2.32 changes ends********************/
}









