package com.filogix.util;

import MosSystem.Mc;
import MosSystem.Sc;

import com.basis100.deal.docprep.Dc;

/*
 * 13.Feb.2009 DVG #DG812 FXP23493: 	Concentra CR207 - Amend interest type code transmission to MI insurers.
 * 26.Jan.2009 DVG #DG810 FXP24114/NBC346331: Filogix NBC Production Express down
 * 29.Dec.2008 DVG #DG800 DES335687: 	Francois Lague Bug on CR304 signature
 * 15.Dec.2008 DVG #DG796 DES338501: Francois appraisal date returns even after deleting it in Appraisal review screen
// * 07/Oct/2008 DVG #DG760 LEN300825: Jane Kulbida Incorrect GDS and TDS calculations for 3 year combined
 * 05/Sep/2008 DVG #DG750 FXP22292: NBC Prod Crash
 * 28/Aug/2008 DVG #DG746 FXP21967: LS-MLMQA - "Odd" characters in Express Commitment Letter
 * 21/Aug/2008 DVG #DG744 IBC22258: Version- Anomaly in forms CR-028
 * 11/Aug/2008 DVG #DG738 LEN22179: NBC CR 105 - add Expert user's region group-id to subject of Exchange e-mail
 * 10/Jun/2008 DVG #DG728 FXP21738: SOPHY deals are not being routed properly
 * 02/Jun/2008 DVG #DG724 LEN255548: Heather MacNaughton Broker upload not triggered for 3 deal purposes
 * 09/May/2008 DVG #DG720 CTFS - CR203 - new form for requisiton of Funds
 * 04/Apr/2008 DVG #DG712 FXP21127: td prod - interest compound different between deal and mtg prod tables
 * 29/Feb/2008 DVG #DG702 MLM x BDN merge adjustments
 * 19/Feb/2008 DVG #DG701 change docprep to allow xsl transformation for 'uploads'
 * 30/Jan/2008 DVG #DG690 FXP16091: Hide UserType Options from Sys Admins
 * 24/Jan/2008 DVG #DG688 LEN217161: Chantal Blanchard MI application fee from AIG does not display in SOLPAK
 * 23/Jan/2008 DVG #DG684 FXP20073: NBC Production AppServer CPU 100%
 * 10/Dec/2007 DVG #DG672 DJ Branch docs./Sophy  project
 * 24/Oct/2007 DVG #DG648 FXP18790: DupeCheck - BDN Express - The fields "City" and "Employment History Status"'s Names are not correct in Deal Compare Notes
 * 19/Oct/2007 DVG #DG644 FXP18792  DupeCheck - BDN Express - The property type info is missing in the Deal Compare Note
 * 15/Oct/2007 DVG #DG642 FXP18698: DupeCheck - BDN Express - Some fields showing with the index instead of the item values in Deal Compare notes
 * 04/Oct/2007 DVG #DG632 FXP18500: BDN: DupeCheck Deal Compare - Tags and Translation
 * 24/Aug/2007 DVG #DG622 FXP18288: Firstline - Express to Target Download
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project
 *
 * Created on 25-Jun-07
 *
 * As per our discussion and implicit agreement of team members we shall
 * proceed moving all constants to this class thereby gradually phasing out
 * the other classes previously used for this purpose.
 * Denis V. Gadelha
 *
 -----Original Message-----
 From: Billy Lam [mailto:billy.lam@filogix.com]
 Sent: Monday, June 18, 2007 10:20 AM
 To: 'Sean Chen'; 'Denis Gadelha'; 'Midori Aida'; 'Vlad Ivanov'; 'Susan Li'; bruce.zhang@filogix.com; hiroyuki.kobayashi@filogix.com; 'Asif Atcha'; 'Jerry Dai'; venkata.nagaruru@filogix.com; alvin.yu@filogix.com; serghei.jelauc@filogix.com
 Cc: 'Tina Behroozi'; 'Michael Morris'
 Subject: RE: Translation constants
 Importance: High
 I would consider consolidating and re-packaging Dc, Mc, Sc, ...etc. into FXPConstants.java (same as option 5)
 Make sense ?
 Thx  Billy
 ...
 -----Original Message-----
 From: Denis Gadelha [mailto:denis.gadelha@filogix.com]
 Sent: Friday, June 15, 2007 10:36 AM
 To: 'Midori Aida'; 'Sean Chen'; 'Vlad Ivanov'; 'Susan Li'; bruce.zhang@filogix.com; hiroyuki.kobayashi@filogix.com; 'Asif Atcha'; 'Jerry Dai'; venkata.nagaruru@filogix.com; alvin.yu@filogix.com; serghei.jelauc@filogix.com
 Cc: 'Billy Lam'; 'Tina Behroozi'; 'Michael Morris'
 Subject: Translation constants
 I suggest we start using declaring constants whenever localizing translation through the resources:
 ...
 */

/**
 * @author DGadelha
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 * @version 1.0 Inisital version
 * @version 1.1 13-Aug-2008 Added Constants for XS_1.6
 * @version 1.2 14-Aug-2008 Added Constants for XS_1.8
 */
public interface Xc extends Mc, Sc, Dc {

	// mosys property ids
	public static final String SYS_PROPERTIES_LOCATION = "SYS_PROPERTIES_LOCATION";

	public static final String COM_BASIS100_RESOURCE_INIT = "com.basis100.resource.init";
	public static final String COM_BASIS100_WORKFLOW_TASK_MAINTENANCE_DAEMON_ENDHOUR = "com.basis100.workflow.TaskMaintenanceDaemon.endhour";
	public static final String COM_BASIS100_WORKFLOW_TASK_MAINTENANCE_DAEMON_STARTHOUR = "com.basis100.workflow.TaskMaintenanceDaemon.starthour";
	public static final String COM_BASIS100_CONTROLS_VIEWMODEBEHAVIOR_VISIBLE = "com.basis100.controls.viewmodebehavior.visible";
	public static final String COM_BASIS100_TASK_MAINT_DAEMON_DIAG_MODE = "com.basis100.task.maint.daemon.diag.mode";
	public static final String COM_BASIS100_NOTIFICATION_MONITOR_DIAG_MODE = "com.basis100.notification.monitor.diag.mode";
	public static final String COM_BASIS100_STARTUP_INITLOGINFLAGS = "com.basis100.startup.initloginflags";
	public static final String COM_BASIS100_STARTUP_ENABLE_BUSINESS_RULE_CACH = "com.basis100.startup.enableBusinessRuleCach";
	public static final String COM_BASIS100_STARTUP_DISABLE_INTEGRITY_CHECKS = "com.basis100.startup.disableIntegrityChecks";
//	public static final String COM_BASIS100_RESOURCE_CONNECTIONPOOLNAME = "com.basis100.resource.connectionpoolname";
	public static final String COM_BASIS100_WORKFLOW_SENDDEALSUMMARYPACKAGE = "com.basis100.workflow.senddealsummarypackage";
	public static final String COM_BASIS100_PARTY_INCLUDE_SERVICE_BRANCH = "com.basis100.party.includeServiceBranch";
	public static final String COM_BASIS100_FXLINK_TYPES = "com.basis100.fxlink.types";
	public static final String COM_BASIS100_CALC_ISDJCLIENT = "com.basis100.calc.isdjclient";
	public static final String COM_BASIS100_INGESTION_SET_MORTGAGE_INSURER = "com.basis100.ingestion.set.mortgage.insurer";
	public static final String COM_BASIS100_INGESTION_DUPECHECK_ENABLED = "com.basis100.ingestion.dupecheck.enabled";
	public static final String COM_BASIS100_INGESTION_PROCESS_ORIGINATION_BRANCH = "com.basis100.ingestion.process.origination.branch";	//#DG672
	public static final String COM_BASIS100_DUPECHECK_PROCESSOR_NAME = "com.basis100.dupecheck.processor.name";
	public static final String COM_BASIS100_SYSMAIL_TO = "com.basis100.sysmail.to";
	public static final String COM_BASIS100_SYSMAIL_FROM = "com.basis100.sysmail.from";
	public static final String COM_BASIS100_ALTERNATE_RTF_EXTENSION = "com.basis100.alternate.rtf.extension";
	public static final String COM_BASIS100_ALTERNATE_CSV_EXTENSION = "com.basis100.alternate.csv.extension";		//#DG701
	public static final String COM_BASIS100_ENCRYPT_EMAIL_URL = "com.basis100.encrypt.email.url";
	public static final String COM_BASIS100_ENCRYPT_EMAIL = "com.basis100.encrypt.email";
	public static final String COM_BASIS100_DOCPREP_FORCE_EMAIL_TO_ADDRESS = "com.basis100.docprep.force.email.to.address";
	public static final String COM_BASIS100_DOCUMENT_CREATE_BY_BUSINESS_RULES = "com.basis100.document.create.by.business.rules";
	public static final String COM_BASIS100_DOCPREP_FAX_ACCOUNT_PASSWORD = "com.basis100.docprep.fax.account.password";
	public static final String COM_BASIS100_DOCPREP_FAX_ACCOUNT_IDENTIFIER = "com.basis100.docprep.fax.account.identifier";
	public static final String COM_BASIS100_DOCPREP_FAX_REQUESTING_PARTY_ID = "com.basis100.docprep.fax.requesting.party.id";
	public static final String COM_BASIS100_DOCPREP_FAX_RECEIVING_PARTY_ID = "com.basis100.docprep.fax.receiving.party.id";
	public static final String COM_BASIS100_DOCPREP_FAX_DATX_URL = "com.basis100.docprep.fax.datx.url";
	public static final String COM_BASIS100_DOCPREP_MAILDEALSUMMARYPACKAGETO = "com.basis100.docprep.maildealsummarypackageto";
	public static final String COM_BASIS100_INGESTION_DETERMINE_DISCOUNT = "com.basis100.ingestion.determine.discount";		//#DG672
	public static final String COM_BASIS100_USERADMIN_PASSWORDENCRYPTED = "com.basis100.useradmin.passwordencrypted";
	public static final String COM_BASIS100_SYSTEM_FORCEPROGRESSADVANCETYPE = "com.basis100.system.forceprogressadvancetype";
	public static final String COM_BASIS100_FXP_CLIENTID_BMO = "com.basis100.fxp.clientid.bmo";		//#DG702
	public static final String COM_BASIS100_MIINSURANCE_MIPROCESSINGHANDLEDOUTSIDEXPRESS = "com.basis100.miinsurance.miprocessinghandledoutsidexpress";
	public static final String COM_BASIS100_CALC_COMMITMENTEXPIRYDATE_USECOMMITMENTTERM = "com.basis100.calc.commitmentexpirydate.usecommitmentterm";
	public static final String COM_BASIS100_DEAL_MOD_PROMPT_FOR_DECISION_MOD = "com.basis100.deal.mod.prompt.for.decision.mod";
	public static final String COM_BASIS100_LDINSURANCE_DISPLAYLIFEDISABILITYMODULE = "com.basis100.ldinsurance.displaylifedisabilitymodule";
	public static final String COM_BASIS100_DVALIDPROPERTIES_LOCATION = "com.basis100.dvalidproperties.location";
	public static final String COM_BASIS100_SYSTEM_RESET_UPLOAD_FLAG_ON_REV_FUNDING = "com.basis100.system.resetUploadFlagOnRevFunding";
	public static final String COM_BASIS100_DEAL_ALLOWREVERSEFUNDING = "com.basis100.deal.allowreversefunding";
	public static final String COM_BASIS100_DEAL_UW_DISPLAYFINANCINGPROGRAM = "com.basis100.deal.uw.displayfinancingprogram";
	public static final String COM_BASIS100_DEAL_UW_RUNBUSINESSRULSWHENENTRY = "com.basis100.deal.uw.runbusinessrulswhenentry";
	public static final String COM_BASIS100_DEAL_UW_TREATMIRULESASNONCRITICAL = "com.basis100.deal.uw.treatmirulesasnoncritical";
	public static final String COM_BASIS100_FXP_CLIENTID_GECF = "com.basis100.fxp.clientid.gecf";
	public static final String COM_BASIS100_COMMUNICATION_SSL_ENCRYPTION = "com.basis100.communication.ssl.encryption";
	public static final String COM_BASIS100_DEAL_TAXPAYORID = "com.basis100.deal.taxpayorid";
	public static final String COM_BASIS100_DEAL_PRIVILEGEPAYMENTOPTION = "com.basis100.deal.privilegepaymentoption";
	public static final String COM_BASIS100_RESOURCE_LANGUAGE = "com.basis100.resource.language";
	public static final String COM_BASIS100_GCD_SHOW_GCDSUMMARY_SECTION = "com.basis100.GCD.showGCDSummarySection";
	public static final String COM_BASIS100_PICKLIST_SOURCE = "com.basis100.picklist.source";		//#DG712
	public static final String COM_BASIS100_SUPPORT_ENVIRONMENT = "com.basis100.support.environment";
	public static final String COM_BASIS100_SYSTEM_GENERATESERVICINGMORTNUM = "com.basis100.system.generateservicingmortnum";		//#DG720
	public static final String COM_BASIS100_SYSTEM_UPDATESERVMORTGAGENUM = "com.basis100.system.updateservmortgagenum";
	public static final String COM_BASIS100_SYSTEM_SYSMULTIUPLOAD = "com.basis100.system.sysmultiupload";
	public static final String COM_BASIS100_SYSTEM_SYSUPLOAD = "com.basis100.system.sysupload";
	public static final String COM_BASIS100_SYSTEM_GENERATEPROPERTYREFNUM = "com.basis100.system.generatepropertyrefnum";
	public static final String COM_BASIS100_SYSTEM_GENERATECLIENTREFNUM = "com.basis100.system.generateclientrefnum";
	public static final String COM_BASIS100_SYSTEM_GENERATEPARTYREFNUM = "com.basis100.system.generatepartyrefnum";
	public static final String COM_BASIS100_SYSTEM_GENERATESOBREFNUM = "com.basis100.system.generatesobrefnum";
	public static final String COM_BASIS100_SYSTEM_GENERATESOURCEFIRMREFNUM = "com.basis100.system.generatesourcefirmrefnum";
	public static final String COM_BASIS100_TASKMAINTENENCEDAEMON_INTERVAL = "com.basis100.taskmaintenencedaemon.interval";
	public static final String COM_BASIS100_CALC_USEPANDIPAYMENTSBASEDONDJDAYSYEAR = "com.basis100.calc.usepandipaymentsbasedondjdaysyear";	//#DG688
	public static final String COM_BASIS100_CALC_FORCEPANDIFOR3YEAR = "com.basis100.calc.forcepandifor3year";
	public static final String COM_BASIS100_CALC_GDSTDS3YRATECODE = "com.basis100.calc.gdstds3yratecode";
	public static final String COM_BASIS100_CALC_MONTHLY_PAYMT_ONLY = "com.basis100.calc.monthly.paymt.only";
	public static final String COM_BASIS100_INGESTION_SFP_DEFAULT_PROFILESTATUSID = "com.basis100.ingestion.SFP.default.profilestatusid";		//#DG728
	public static final String COM_BASIS100_INGESTION_SOB_DEFAULT_PROFILESTATUSID = "com.basis100.ingestion.SOB.default.profilestatusid";
	public static final String COM_BASIS100_INGESTION_SFP_BRANCH_PROFILESTATUSID = "com.basis100.ingestion.SFP.default.branch.profilestatusid";		//#DG728
	public static final String COM_BASIS100_INGESTION_SOB_BRANCH_PROFILESTATUSID = "com.basis100.ingestion.SOB.default.branch.profilestatusid";
	public static final String COM_BASIS100_CALC_USEFORMULAFORWEEKLYBIWEEKLYNONACCPAY = "com.basis100.calc.useformulaforweeklybiweeklynonaccpay";		//#DG744

	public static final String COM_FILOGIX_ENVIRONMENT = "com.filogix.environment";
	public static final String COM_FILOGIX_DEAL_COPY_LENDER_NOTES = "com.filogix.deal.copy.lender.notes";
	public static final String COM_FILOGIX_INGESTION_WORKFLOW_NOTIFICATION_TYPE = "com.filogix.ingestion.workflow.notification.type";
	public static final String COM_FILOGIX_INGESTION_WORKFLOW_UPLOAD = "com.filogix.ingestion.workflow.upload";	// "Y" or "N"
	public static final String COM_FILOGIX_INGESTION_CERVUS_GE_INSURER_RANDOM_NUMBERS_PERCENT = "com.filogix.ingestion.cervus.ge.insurer.random.numbers.percent";	//#DG672
	public static final String COM_FILOGIX_DOCPREP_UPLOADFILENAMINGCONVENTION = "com.filogix.docprep.uploadfilenamingconvention";
	public static final String COM_FILOGIX_DOCPREP_SOLPACKAGE_IS_GECONVENTION = "com.filogix.docprep.solpackage.isGEConvention";		//#DG688
	public static final String COM_FILOGIX_DOCPREP_SOLPACKAGE_INCLUDE_MIPREMIUM = "com.filogix.docprep.solpackage.include.MIPremium";		//#DG688
	public static final String COM_FILOGIX_DOCPREP_SOLPACKAGE_FEES = "com.filogix.docprep.solpackage.fees";
	public static final String COM_FILOGIX_DOCPREP_USE_BC_CONDO_CLAUSES = "com.filogix.docprep.use.BC.condo.clauses";		//#DG688
	public static final String COM_FILOGIX_DOCPREP_PGP_IN_USE = "com.filogix.docprep.PGP.in.use";
	public static final String COM_FILOGIX_DOCPREP_ENABLE_FAXING = "com.filogix.docprep.enable.faxing";
	public static final String COM_FILOGIX_DOCPREP_FAX_PREFIX = "com.filogix.docprep.fax.prefix";
	public static final String COM_FILOGIX_DOCPREP_FAX_BILLING_ACCOUNT = "com.filogix.docprep.fax.billing.account";
	public static final String COM_FILOGIX_DOCPREP_ENCRYPT_NOT_DOCS = "com.filogix.docprep.encrypt.not.docs";
	public static final String COM_FILOGIX_DOCPREP_SMIME_KEYS = "com.filogix.docprep.smime.keys";
	public static final String COM_FILOGIX_DOCPREP_PROSPERA_URL = "com.filogix.docprep.prospera.url";
	public static final String COM_FILOGIX_DOCPREP_ALERT_EMAIL_RECIPIENT = "com.filogix.docprep.alert.email.recipient";
	public static final String COM_FILOGIX_DATX_URL = "com.filogix.datx.url";
	public static final String COM_FILOGIX_DATX_MCAP_RECEIVINGPARTYID = "com.filogix.datx.mcap.receivingpartyid";
	public static final String COM_FILOGIX_DATX_LDDCLOSING_ENABLED = "com.filogix.datx.lddclosing.enabled";
	public static final String COM_FILOGIX_DATX_LDDCLOSING_ACCOUNTPASSWORD = "com.filogix.datx.lddclosing.accountpassword";
	public static final String COM_FILOGIX_DATX_LDDCLOSING_ACCOUNTID = "com.filogix.datx.lddclosing.accountid";
	public static final String COM_FILOGIX_DATX_LDDCLOSING_REQUESTINGPARTYID = "com.filogix.datx.lddclosing.requestingpartyid";
	public static final String COM_FILOGIX_DATX_LDDCLOSING_RECEIVINGPARTYID = "com.filogix.datx.lddclosing.receivingpartyid";
	public static final String COM_FILOGIX_DATX_FUNDING_RECEIVINGPARTYID = "com.filogix.datx.funding.receivingpartyid";
	public static final String COM_FILOGIX_DATX_CLOSING_RECEIVINGPARTYID = "com.filogix.datx.closing.receivingpartyid";
  public static final String COM_FILOGIX_DOCCENTRAL_DATX_URL = "com.filogix.doccentral.datx.url";		//#DG738
	public static final String COM_FILOGIX_GEMONEY_URL = "com.filogix.GEMoney.url";
	public static final String COM_FILOGIX_GEMONEY_URL_USERNAME = "com.filogix.GEMoney.url.username";
	public static final String COM_FILOGIX_GEMONEY_URL_PASSWORD = "com.filogix.GEMoney.url.password";
	public static final String COM_FILOGIX_DEALMOD_HIDE_TOTALLOANAMOUNT = "com.filogix.dealmod.hide.totalloanamount";
	public static final String COM_FILOGIX_IS_DATA_OBFUSCATED = "com.filogix.is.Data.Obfuscated";
	public static final String COM_FILOGIX_FXP_ADJUDICATION_ENABLED = "com.filogix.fxp.adjudication.enabled";
	public static final String COM_FILOGIX_MI_TYPE_FEES = "com.filogix.mi.type.fees";		//#DG688
	public static final String COM_FILOGIX_MI_PST_TYPE_FEES = "com.filogix.mi.pst.type.fees";
	public static final String COM_FILOGIX_CALC_DJNUMBER_OF_PAYMENTS = "com.filogix.calc.DJNumberOfPayments";
	public static final String COM_BASIS100_APPRAISALREVIEW_SUPPORT_SUBMIT_TO_READ = "com.basis100.appraisalreview.supportSubmitToRead";
	public static final String COM_BASIS100_APPRAISALREVIEW_SUPPORTAPPRAISALORDER = "com.basis100.appraisalreview.supportappraisalorder";

	public static final String SYS_CFG_MORTGAGEPRODUCTDEFAULT = "MORTGAGEPRODUCTDEFAULT";		//#DG672

	public static final String INSTITUTION_VALIDATE_MI_AT_DEAL_RESOLVE = "institution.validate.mi.at.deal.resolve";
	public static final String ING_LTV_THRESHOLD = "com.filogix.ingestion.LTV.threshold";
	public static final String ING_RESET_PREPAY_LTVGTLIMIT = "com.filogix.ingestion.Rst.PrePay.LTVgtLimit";
	public static final String Max_Search_Records_LIMIT = "com.basis100.deal.query.MaxSearchRecordsLimit";

	public static final String MOS_APP_MOS_SYSTEM_APPRAISALREVIEW_SUPPRESSAVMDISPLAY = "mosApp.MosSystem.appraisalreview.suppressavmdisplay";	//#DG796
	public static final String MOS_APP_MOS_SYSTEM_APPRAISALREVIEW_SUPPRESSAPPRAISALDISPLAY = "mosApp.MosSystem.appraisalreview.suppressappraisaldisplay";

	// for 4.2 CM START
	public static final String STATUSUPDATE_CONDITIONS_SUPPORTED = "statusUpdate.conditions.supported";
	public static final String STATUSUPDATE_CONDITIONS_INCLUDE_CONDITION_TYPES = "statusUpdate.conditions.include.condition.types";
	public static final String DCSUDAEMON_SLEEP_TIME = "dcsuDaemon.sleep.time";
	public static final String DCSUDAEMON_MAXATTEMPTCOUNTS = "dscuDaemon.maxAttemptCounts";
	public static final String DCSUDAEMON_RETRY_TIME = "dcsuDaemon.retry.time";
	public static final String COM_BASIS100_DOCPREP_BCO_CURRENT_USER = "com.basis100.docprep.bco.current.user";
    // for 4.2 CM END

	// for 4.2 DSU START
    public static final String STATUSUPDATE_DEALANDEVENT_SUPPORT = "statusUpdate.dealAndEvent.supported";
    // for 4.2 DSU END

    // 4.3 backports...
    public static final String INGESTION_USE_PRODUCTTYPES = "com.filogix.ingestion.useproducttype";
    
    // 4.3 Exchange Link
    public static final String COM_FILOGIX_EXCHANGE_VERSION = "com.filogix.exchange.version";
    
	// 4.3 duplicate conditions
	public static final String DUPLICATE_DEFAULT_CONDITIONS = "duplicate.default.conditions";
	public static final String DUPLICATE_DEFAULT_CONDITIONS_EXPANDED = "duplicate.default.conditions.expanded";
	//End of 4.3 duplicate conditions

	// 5.0/MI
	// ticket QC212
	public static final String COM_BASIS100_DEAL_UW_DISPLAYINCOMEVERIFICATION = "com.basis100.deal.uw.displayincomeverification";


	//picklist table names
	public static final String PKL_LENDER_PROFILE = "LenderProfile";
	public static final String PKL_PROVINCESHORTNAME = "PROVINCESHORTNAME";
	public static final String PKL_DENIAL_REASON = "DenialReason";
	public static final String PKL_INCOME_TYPE = "IncomeType";
	public static final String PKL_OCCUPATION = "Occupation";
	public static final String PKL_JOB_TITLE = "JobTitle";
	public static final String PKL_INDUSTRY_SECTOR = "IndustrySector";
	public static final String PKL_BORROWER_ADDRESS_TYPE = "BorrowerAddressType";
	public static final String PKL_EMPLOYM_HIST_STAT = "EmploymentHistoryStatus";
	public static final String PKL_BANKRUPTCY_STATUS = "BankruptcyStatus";
	public static final String PKL_EMPLOYMENT_HISTORY_TYPE = "EmploymentHistoryType";
	public static final String PKL_RESIDENTIAL_STATUS = "ResidentialStatus";
	public static final String PKL_BORROWER_GENERAL_STATUS = "BorrowerGeneralStatus";
	public static final String PKL_BORROWER_TYPE = "BorrowerType";
	public static final String PKL_CITIZENSHIP_TYPE = "CitizenshipType";
	public static final String PKL_LANGUAGE_PREFERENCE = "LanguagePreference";
	public static final String PKL_MARITAL_STATUS = "MaritalStatus";
	public static final String PKL_PRODUCT_TYPE = "ProductType";
	public static final String PKL_DOWN_PAY_SRC_TYPE = "DownPaymentSourceType";
	public static final String PKL_LOC_REPAYMENT_TYPE = "LocRepaymentType";
	public static final String PKL_LINE_OF_BUSINESS = "LineOfBusiness";
	public static final String PKL_CREDIT_BUREAU_NAME = "CreditBureauName";
	public static final String PKL_STATUS = "STATUS";
	public static final String PKL_SPECIALFEATURE = "SPECIALFEATURE";
	public static final String PKL_PAYMENTTERM = "PAYMENTTERM";
    public static final String PKL_PAYMENT_TERM_TYPE = "PaymentTermType";		//#DG746
	public static final String PKL_DEALPURPOSE = "DEALPURPOSE";
	public static final String PKL_DEALTYPE = "DEALTYPE";
	public static final String PKL_DEAL_TYPE = "DealType";
	public static final String PKL_MIINDICATOR = "MIIndicator";
	public static final String PKL_LIEN_POSITION = "LienPosition";
	public static final String PKL_BRIDGEPURPOSE = "BRIDGEPURPOSE";
	public static final String PKL_REQUESTAPPRAISAL = "REQUESTAPPRAISAL";
	public static final String PKL_INSURANCEPROPORTIONS = "INSURANCEPROPORTIONS";
	public static final String PKL_LIFESTATUS = "LIFESTATUS";
	public static final String PKL_INCOME_PERIOD = "IncomePeriod";
	public static final String PKL_ASSET_TYPE = "AssetType";
	public static final String PKL_LIABILITY_TYPE = "LiabilityType";
	public static final String PKL_LIABILITY_PAY_OFF_TYPE = "LiabilityPayOffType";
	public static final String PKL_PROPERTY_EXPENSE_TYPE = "PropertyExpenseType";
	public static final String PKL_PROPERTY_EXPENSE_PERIOD = "PropertyExpensePeriod";
	public static final String PKL_PARTYTYPE = "PARTYTYPE";
	public static final String PKL_APPRAISER = "APPRAISER";
	//#DGx public static final String PKL_BORROWERSMOKER = "BORROWERSMOKER";
	public static final String PKL_SMOKESTATUS = "SMOKESTATUS";
	public static final String PKL_AFFILIATIONPROGRAM = "AFFILIATIONPROGRAM";
	public static final String PKL_PREFMETHODOFCONTACT = "PREFMETHODOFCONTACT";
	public static final String PKL_SOLICITATION = "SOLICITATION";
	public static final String PKL_PROVINCE = "PROVINCE";
	public static final String PKL_TENURE_TYPE = "TenureType";
	public static final String PKL_WATER_TYPE = "WaterType";
	public static final String PKL_SEWAGE_TYPE = "SewageType";
	public static final String PKL_STREET_TYPE = "StreetType";
	public static final String PKL_STREET_DIRECTION = "StreetDirection";
	public static final String PKL_PROPERTY_USAGE = "PropertyUsage";
	public static final String PKL_PROPERTY_TYPE = "PropertyType";
	public static final String PKL_PROPERTY_LOCATION = "PropertyLocation";
	public static final String PKL_OCCUPANCY_TYPE = "OccupancyType";
	public static final String PKL_NEW_CONSTRUCTION = "NewConstruction";
	public static final String PKL_MUNICIPALITY = "Municipality";
	public static final String PKL_LIVING_SP_UNIT_MEASURE = "LivingSpaceUnitOfMeasure";
	public static final String PKL_LOT_SIZE_UNIT_MEASURE = "LotSizeUnitOfMeasure";
	public static final String PKL_HEAT_TYPE = "HeatType";
	public static final String PKL_GARAGE_TYPE = "GarageType";
	public static final String PKL_IDENTIFICATIONTYPE = "IDENTIFICATIONTYPE";
	public static final String PKL_EMPLOYMENT_HISTORY_STATUS = "EMPLOYMENTHISTORYSTATUS";		//#DG642
	public static final String PKL_BORROWERGENDER = "BORROWERGENDER";
	public static final String PKL_DISABILITYSTATUS = "DISABILITYSTATUS";
	public static final String PKL_GARAGE_SIZE = "GarageSize";
	public static final String PKL_DWELLING_TYPE = "DwellingType";
	public static final String PKL_DWELLING_STYLE = "DwellingStyle";
	public static final String PKL_MTGPROD = "MTGPROD";		//tkt FXP19081
  public static final String PKL_USER_TYPE = "UserType";
	public static final String PKL_SALUTATION = "Salutation";
	public static final String PKL_DEAL_PURPOSE = "DealPurpose";
	public static final String PKL_PRIVILEGE_PAYMENT = "PrivilegePayment";
	public static final String PKL_MORTGAGEINSURANCECARRIER = "MORTGAGEINSURANCECARRIER";
	public static final String PKL_PAYMENT_FREQUENCY = "PaymentFrequency";
	public static final String PKL_INTEREST_TYPE = "InterestType";
	public static final String PKL_FEE_TYPE = "FeeType";		//#DG688
	public static final String PKL_GROUPPROFILE = "GROUPPROFILE";
	public static final String PKL_BRANCHPROFILE = "BRANCHPROFILE";
	public static final String PKL_REGIONPROFILE = "REGIONPROFILE";
	public static final String PKL_PAGELABEL = "PAGELABEL";
	public static final String PKL_PROFILESTATUS = "PROFILESTATUS";
	public static final String PKL_ADJUDICATIONSTATUS = "ADJUDICATIONSTATUS";		//#DG702
	public static final String PKL_RESPONSESTATUS = "RESPONSESTATUS";
	public static final String PKL_REQUESTSTATUS = "REQUESTSTATUS";
	public static final String PKL_LOCREPAYMENTTYPE = "LOCREPAYMENTTYPE";
	public static final String PKL_PROGRESSADVANCETYPE = "PROGRESSADVANCETYPE";
	public static final String PKL_MISTATUS = "MISTATUS";
	public static final String PKL_MORTGAGEINSURANCETYPE = "MORTGAGEINSURANCETYPE";
    //MCM ImplTeam. XS_2.29
    public static final String PKL_LENDERPROFILE = "LENDERPROFILE";

    //MCM ImplTeam. XS_2.27 - bug fixing
    public static final String PKL_CDSECTIONTITLE = "CDSECTIONTITLE";

    //***************MCM Impl team changes Starts - XS_1.6 *******************/
    //Added a pick list constant for Component Type
    public static final String PKL_COMPONENTTYPE = "COMPONENTTYPE";
    //***************MCM Impl team changes Ends - XS_1.6 *******************/
	public static final String PKL_MONTHS = "MONTHS";
	public static final String PKL_DJPAGELABEL = "DJPAGELABEL";
	public static final String PKL_SOURCEOFBUSINESSCATEGORY = "SOURCEOFBUSINESSCATEGORY";
	public static final String PKL_ESCROWTYPE = "ESCROWTYPE";
	public static final String PKL_FEESTATUS = "FEESTATUS";
	public static final String PKL_MORTGAGEINSURER = "MORTGAGEINSURER";
	public static final String PKL_INTERESTCOMPOUND = "INTERESTCOMPOUND";
	public static final String PKL_TAXPAYOR = "TAXPAYOR";
	public static final String PKL_PREPAYMENTOPTIONS = "PREPAYMENTOPTIONS";
	public static final String PKL_CONDITIONRESPONSIBILITYROLE = "CONDITIONRESPONSIBILITYROLE";
	public static final String PKL_SERVICEPRODUCT = "SERVICEPRODUCT";
	public static final String PKL_SERVICEPROVIDER = "SERVICEPROVIDER";
	public static final String PKL_SERVICETRANSACTIONTYPE = "SERVICETRANSACTIONTYPE";
  public static final String PKL_REPAYMENT_TYPE = "RepaymentType";		//#DG688
	public static final String PKL_FEE_PAYMENT_METHOD = "FeePaymentMethod";
	public static final String PKL_CIFBORROWERSTATUS = "CIFBORROWERSTATUS";	//#DG750
    public static final String PKL_SUFFIX = "SUFFIX";
    
	// email address used to send alerts to support team
	public static final String FXSUPPORT_EMAIL = "fxsupport@filogix.com";
	public static final String NOTIFICATION_FILOGIX_COM = "notification@filogix.com";

	// date related calc constants		#DG744
	public static final int 	YEARS_IN_YEAR = 1;
	public static final int 	MONTHS_IN_YEAR = 12;
	public static final double 	MONTHS_IN_YEAR_DBL = MONTHS_IN_YEAR;	// just for convinience
	public static final int 	SEMESTERS_IN_YEAR = MONTHS_IN_YEAR / 6;		//2
	public static final int 	TRIMESTERS_IN_YEAR = MONTHS_IN_YEAR / 3;		//4
	public static final int 	QUARTERS_IN_YEAR = MONTHS_IN_YEAR / 4;		//3
	public static final int 	SEMI_MONTHS_IN_YEAR = MONTHS_IN_YEAR * 2;		//24
	public static final int 	DAYS_IN_WEEK = 7;
	public static final int 	DAYS_IN_YEAR_INT = 365;
	public static final double 	DAYS_IN_YEAR_DBL = DAYS_IN_YEAR_INT;	//365d just for convinience
	public static final double DAYS_IN_YEAR_AVG = 365.25;
	public static final double DAYS_IN_MONTH_AVG = DAYS_IN_YEAR_AVG / MONTHS_IN_YEAR;		//=30.4375
	public static final int 	WEEKS_IN_YEAR_INT = DAYS_IN_YEAR_INT / DAYS_IN_WEEK;		// = 52
	public static final double 	WEEKS_IN_YEAR_DBL = WEEKS_IN_YEAR_INT;	//52d just for convinience
	public static final double WEEKS_IN_YEAR_AVG = DAYS_IN_YEAR_AVG / DAYS_IN_WEEK;		// = 52.1785
	public static final double WEEKS_IN_MONTH_AVG = WEEKS_IN_YEAR_AVG / MONTHS_IN_YEAR;// = 4.3482142
	public static final int 	BI_WEEKS_IN_YEAR_INT = WEEKS_IN_YEAR_INT / 2;			// = 26
	public static final double 	BI_WEEKS_IN_YEAR_DBL = BI_WEEKS_IN_YEAR_INT;	//26d just for convinience
	public static final double BI_WEEKS_IN_YEAR_AVG = WEEKS_IN_YEAR_AVG / 2;		// = 26.08928


	// calcs numbers - helps map between the calc nr. and its implementation
  public static interface CALCS	{
	  public static final String CALC_1 = "Calc 1";
	  public static final Class CALC_1_CLASS = com.basis100.deal.calc.impl.PandI.class;
	  public static final String CALC_2 = "Calc 2";
	  public static final String CALC_3 = "Calc 3";
	  public static final String CALC_4 = "Calc 4";
	  public static final String CALC_5 = "Calc 5";
//	  public static final Class CALC_5_CLASS = CombinedTotalGDS.class;
	  public static final String CALC_6 = "Calc 6";
//	  public static final Class CALC_6_CLASS = CombinedTotalGDS3Years.class;
	  public static final String CALC_7 = "Calc 7";
//	  public static final Class CALC_7_CLASS = CombinedTotalGDSBorrowerOnly.class;
	  public static final String CALC_8 = "Calc 8";
//	  public static final Class CALC_8_CLASS = CombinedTotalTDS.class;
	  public static final String CALC_9 = "Calc 9";
//	  public static final Class CALC_9_CLASS = CombinedTotalTDS3Years.class;
	  public static final String CALC_10 = "Calc 10";
//	  public static final Class CALC_10_CLASS = CombinedTotalTDSBorrowerOnly.class;
	  public static final String CALC_11 = "Calc 11";
	  public static final String CALC_12 = "Calc 12";
	  public static final String CALC_13 = "Calc 13";
	  public static final String CALC_14 = "Calc 14";
	  public static final String CALC_16 = "Calc 16";
	  public static final String CALC_20 = "Calc 20";
	  public static final String CALC_24 = "Calc 24";
	  public static final String CALC_25 = "Calc 25";
	  public static final String CALC_26 = "Calc 26";
	  public static final String CALC_27 = "Calc 27";
	  public static final String CALC_28 = "Calc 28";
	  public static final String CALC_29 = "Calc 29";
	  public static final String CALC_30 = "Calc 30";
	  public static final String CALC_32 = "Calc 32";
	  public static final String CALC_33 = "Calc 33";
	  public static final String CALC_34 = "Calc 34";
	  public static final String CALC_35 = "Calc 35";
	  public static final String CALC_38 = "Calc 38";
	  public static final String CALC_39 = "Calc 39";
	  public static final String CALC_40 = "Calc 40";
	  public static final String CALC_41 = "Calc 41";
	  public static final String CALC_42 = "Calc 42";
	  public static final String CALC_43 = "Calc 43";
	  public static final String CALC_44 = "Calc 44";
	  public static final String CALC_45 = "Calc 45";
	  public static final String CALC_46 = "Calc 46";
	  public static final String CALC_48 = "Calc 48";
	  public static final String CALC_49 = "Calc 49";
	  public static final String CALC_47 = "Calc 47";
	  public static final String CALC_50 = "Calc 50";
	  public static final String CALC_51 = "Calc 51";
	  public static final String CALC_52 = "Calc 52";
	  public static final String CALC_53 = "Calc 53";
	  public static final String CALC_54 = "Calc 54";
	  public static final String CALC_55 = "Calc 55";
	  public static final String CALC_60 = "Calc 60";
	  public static final String CALC_61 = "Calc 61";
	  public static final String CALC_68 = "Calc 68";
	  public static final String CALC_67 = "Calc 67";
	  public static final String CALC_69 = "Calc 69";
	  public static final String CALC_71 = "Calc 71";
	  public static final String CALC_73 = "Calc 73";
	  public static final String CALC_72 = "Calc 72";
	  public static final String CALC_74 = "Calc 74";
	  public static final String CALC_75 = "Calc 75";
	  public static final String CALC_76 = "Calc 76";
	  public static final String CALC_77 = "Calc 77";
	  public static final String CALC_78 = "Calc 78";
	  public static final String CALC_79 = "Calc 79";
	  public static final String CALC_80 = "Calc 80";
	  public static final String CALC_81 = "Calc 81";
	  public static final String CALC_82 = "Calc 82";
	  public static final String CALC_83 = "Calc 83";
	  public static final String CALC_84 = "Calc 84";
	  public static final String CALC_85 = "Calc 85";
	  public static final String CALC_86 = "Calc 86";
	  public static final String CALC_87 = "Calc 87";
	  public static final String CALC_88 = "Calc 88";
//	  public static final Class CALC_88_CLASS = GDSTDS3YearRate.class;
	  public static final String CALC_89 = "Calc 89";
	  public static final String CALC_90 = "Calc 90";
	  public static final String CALC_91 = "Calc 91";
	  public static final String CALC_92 = "Calc 92";
	  public static final String CALC_94 = "Calc 94";
	  public static final String CALC_95 = "Calc 95";
	  public static final String CALC_96 = "Calc 96";
	  public static final String CALC_97 = "Calc 97";
	  public static final String CALC_98 = "Calc 98";
	  public static final String CALC_99 = "Calc 99";
	  public static final String CALC_100 = "Calc 100";
	  public static final String CALC_101 = "Calc 101";
	  public static final String CALC_102 = "Calc 102";
	  public static final String CALC_103 = "Calc 103";
	  public static final String CALC_105 = "Calc 105";
	  public static final String CALC_106 = "Calc 106";
	  public static final String CALC_107 = "Calc 107";
	  public static final String CALC_108 = "Calc 108";
	  public static final String CALC_109 = "Calc 109";
//	  public static final Class CALC_109_CLASS = PandI3YearRate.class;
	  public static final String CALC_110 = "Calc 110";
	  public static final String CALC_111 = "Calc 111";
	  public static final String CALC_112 = "Calc 112";
	  public static final String CALC_113 = "Calc 113";
	  public static final String CALC_114 = "Calc 114";
	  public static final String CALC_200 = "Calc 200";
  }
	/* PRIMEINDEXRATEPROFILE_en.properties - values below depend on each lender
	0 = 0 |
	1 = 1 | 3m variable
	2 = 2 | Bank Prime
	3 = 3 | CEDOR
	4 = 4 | 1y closed
	*/
	public static final int 	PRIME_INDEX_0 = 0;
	public static final int 	PRIME_INDEX_1 = 1;
	public static final int 	PRIME_INDEX_2 = 2;
	public static final int 	PRIME_INDEX_3 = 4;
	public static final int 	PRIME_INDEX_4 = 4;

	//#DG724
	public static interface LENDER_ENVR_ID {
		public static final String CERVUS = "CV";
		public static final String GE = "GE";
		public static final String CON = "CON";		//#DG812
		public static final String MERIX = "MX";	//FXP26730
  }

	public static interface DEAL_COPY_TYPE {
  		public static final String GOLD = "G";
  		public static final String SCENARIO = "S";
  		public static final String TRANSACTIONAL = "T";
	}

	//list of localization entries makes it easier to find where they r used
	public static final String YEAR_MONTH_FORMAT = "DupChekYEAR_MONTH_FORMAT";
	public static final String DATE_FORMAT = "DupChekDATE_FORMAT";
	public static final String CURRENCY_FORMAT = "DupChekCURRENCY_FORMAT";
	public static final String RATIO_FORMAT = "DupChekRATIO_FORMAT";

	public static final String NO_LABEL = "NO_LABEL";
	public static final String YES_LABEL = "YES_LABEL";
	public static final String FEMALE_ABREVIAT = "FemaleAbrev";
	public static final String MALE_ABREVIAT = "MaleAbrev";
	public static final String AMOUNT = "AMOUNT";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String TRANSIT_NUMBER = "TRANSIT_NUMBER";
	public static final String INCOME_AMOUNT = "INCOME_AMOUNT";
	public static final String ANNUAL_INCOME_AMOUNT = "ANNUAL_INCOME_AMOUNT";
	public static final String DATE_OF_BIRTH = "DATE_OF_BIRTH";
	public static final String MTHLY_PAY = "MTHLY_PAY";
	public static final String TYPE = "TYPE";
	public static final String PERIOD = "PERIOD";
	public static final String GENDER = "GENDER";

	//********** DEAL COMPARE *****************
	public static final String DUP_CHEK_MAXDIFFRCHD = "DupChekMAXDIFFRCHD";
	//appendNote("***  Maximum allowed differences reached (" + maxDifferences + ")  ***");
	public static final String DUP_CHEK_COMPARINGDEAL_HD = "DupChekCOMPARINGDealHd";
	//noteHeader.addElement("Comparing Deal (" + str1 + ") and (" + str2 + ")...");
	public static final String DUP_CHEK_DEAL_CP_TAG = "DupChekDEAL_CP_TAG";
	//        str1 = BXResources.getDocPrepIngMsg("DEAL",lang) + ": " + str1;
	public static final String DUP_CHEK_ITEM_DESCR = "ITEM_DESCRIPTION";
	/*str1 = BXResources.getDocPrepIngMsg("DEAL",lang) + ": " + str1;
	 str2 = BXResources.getDocPrepIngMsg("DEAL",lang) + ": " + str2;
	 dealNotesHdrList.addElement("Item Description" + DELIMITER +
	 str1 + DELIMITER + str2 + DELIMITER + "Difference");	*/
	public static final String DUP_CHEK_DIFFERENCE = "DIFFERENCE";
	public static final String DUP_CHEK_DEAL_DIFFERENCES = "DupChekDEAL_DIFFERENCES";
	//int place = appendNote("* " +  BXResources.getDocPrepIngMsg("DEAL_DIFFERENCES",lang) +"  *");
	public static final String DUP_CHEK_ACTUAL_PAYMENT_TERM = "ACTUAL_PAYMENT_TERM";
	public static final String DUP_CHEK_DEAL_TYPE = "DEAL_TYPE";
	public static final String DUP_CHEK_AMORTIZATION_PERIOD = "AMORTIZATION_PERIOD";
	public static final String DUP_CHEK_ESTIMATED_CLOSING_DATE = "ESTIMATED_CLOSING_DATE";
	public static final String DUP_CHEK_EXISTING_LOAN_AMOUNT = "EXISTING_LOAN_AMOUNT";
	public static final String DUP_CHEK_CHARGE = "CHARGE";
	public static final String DUP_CHEK_MORTGAGE_INSURANCE_IND = "MORTGAGE_INSURANCE_INDICATOR";	//#DG632
	public static final String DUP_CHEK_MIUPFRONT = "MIUPFRONT";
	public static final String DUP_CHEK_LINE_OF_BUSINESS = "LineOfBusiness";
	public static final String DUP_CHEK_LIABILITY_ADDED = "DupChekLIABILITY_ADDED";
	// appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("LIABILITY_ADDED",lang)+ " *");
	public static final String DUP_CHEK_LIABILITY_DELETED = "DupChekLIABILITY_DELETED";
	//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("LIABILITY_DELETED",lang) + "  *");
	public static final String DUP_CHEK_LIABILITY_CHANGED = "DupChekLIABILITY_CHANGED";
	//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("LIABILITY",lang) + " '" + desc1 + "' " + BXResources.getDocPrepIngMsg("CHANGED",lang) + " *");*/
	public static final String DUP_CHEK_MONTHLY_INCOME_AMOUNT = "MONTHLY_INCOME_AMOUNT";
	public static final String DUP_CHEK_PERCENT_OUT_OF_TDS = "PERCENT_OUT_OF_TDS";
	public static final String DUP_CHEK_PERCENT_OUT_OF_GDS = "PERCENT_OUT_OF_GDS";
	public static final String DUP_CHEK_PERCENT_IN_TDS = "PERCENT_IN_TDS";
	public static final String DUP_CHEK_PERCENT_IN_GDS = "PERCENT_IN_GDS";
	public static final String DUP_CHEK_INCOME_DESCRIPTION = "INCOME_DESCRIPTION";
	public static final String DUP_CHEK_INCOME_ADDED = "DupChekINCOME_ADDED";
	//appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("INCOME_ADDED",lang)  + " *");
	public static final String DUP_CHEK_INCOME_DELETED = "DupChekINCOME_DELETED";
	//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("INCOME_DELETED",lang) + " *");
	public static final String DUP_CHEK_INCOME_CHANGED = "DupChekINCOME_CHANGED";
	//appendNote(NEW_LINE + "* " + prefix + BXResources.getDocPrepIngMsg("INCOME",lang) + " '"
	//	+ val + "'" + BXResources.getDocPrepIngMsg("CHANGED",lang)+  suffix + " *");*/
	public static final String DUP_CHEK_TYPE_OTHER = "DupChekIncomeTypeOther";
	public static final String DUP_CHEK_CONTACT_NO_INFO = "DupChekContactNoInfo";
	//DupChekContactNoInfo = Contact %s has no information
	public static final String DUP_CHEK_TIME_AT_JOB = "TIME_AT_JOB";
	public static final String DUP_CHEK_EMPLOYER_NAME = "EMPLOYER_NAME";
	public static final String DUP_CHEK_EMPLOY_HIST_ADDED = "DupChekEMPLOY_HIST_ADDED";
	//#DG600 appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY_ADDED",lang) + " *");
	public static final String DUP_CHEK_EMPLOY_HIST_DELETED = "DupChekEMPLOY_HIST_DELETED";
	//#DG600 appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("EMPLOYMENTHISTORY_DELETED",lang) +  " *");
	public static final String DUP_CHEK_EMPLOY_HIST_CHANGED = "DupChekEMPLOY_HIST_CHANGED";
	//#DG600 appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("EMPLOYMENT_HISTORY",lang)  +  desc + BXResources.getDocPrepIngMsg("CHANGED",lang) + " *");
	public static final String DUP_CHEK_ADDRESS_LINE_1 = "ADDRESS_LINE_1";
	public static final String DUP_CHEK_ADDRESS_LINE_2 = "ADDRESS_LINE_2";
	public static final String DUP_CHEK_BORROWER_ADDRESS_TYPE = "BORROWER_ADDRESS_TYPE";
	public static final String DUP_CHEK_CITY = "CITY";
	public static final String DUP_CHEK_PROVINCE = "PROVINCE";
	public static final String DUP_CHEK_POSTAL_CODE = "POSTAL_CODE";
	public static final String DUP_CHEK_RESIDENTIAL_STATUS = "RESIDENTIAL_STATUS";
	public static final String DUP_CHEK_TIME_AT = "TIME_AT";
	public static final String DUP_CHEK_BOR_ADR_ADDED = "DupChekBOR_ADR_ADDED";
	//#DG600 appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("BORROWER_ADDRESS_ADDED",lang) +" *");
	public static final String DUP_CHEK_BOR_ADR_DELETED = "DupChekBOR_ADR_DELETED";
	//#DG600 appendNote(NEW_LINE + "* "+ BXResources.getDocPrepIngMsg("BORROWER_ADDRESS_DELETED",lang) +" *");
	public static final String DUP_CHEK_BOR_ADR_CHANGED = "DupChekBOR_ADR_CHANGED";
	//#DG600 appendNote(NEW_LINE + "***" + BXResources.getDocPrepIngMsg("BORROWER_ADDRESS",lang) + " + desc + " + BXResources.getDocPrepIngMsg("CHANGED",lang)  +" ***");
	public static final String DUP_CHEK_AGE = "AGE";
	public static final String DUP_CHEK_BANKRUPTCY_STATUS = "BANKRUPTCY_STATUS";
	public static final String DUP_CHEK_EMAIL_ADDRESS = "EMAIL_ADDRESS";
	public static final String DUP_CHEK_FAX_NUMBER = "FAX_NUMBER";
	public static final String DUP_CHEK_HOME_PHONE_NUMBER = "HOME_PHONE_NUMBER";
	public static final String DUP_CHEK_BORROWER_GENERAL_STATUS = "BORROWER_GENERAL_STATUS";
	public static final String DUP_CHEK_TYPE = "Type";
	public static final String DUP_CHEK_BORROWER_WORK_PHONE_EXT = "BORROWER_WORK_PHONE_EXTENSION";
	public static final String DUP_CHEK_WORK_PHONE_NUMBER = "WORK_PHONE_NUMBER";
	public static final String DUP_CHEK_CITIZENSHIP_STATUS = "CITIZENSHIP_STATUS";
	public static final String DUP_CHEK_BUREAU_ATTACHMENT = "BUREAU_ATTACHMENT";
	public static final String DUP_CHEK_CREDIT_BUREAU_NAME = "CREDIT_BUREAU_NAME";
	public static final String DUP_CHEK_CREDIT_BUREAU_SUMMARY = "CREDIT_BUREAU_SUMMARY";
	public static final String DUP_CHEK_FIRST_TIME_BUYER = "FIRST_TIME_BUYER";
	public static final String DUP_CHEK_LANGUAGE_PREFERENCE = "LANGUAGE_PREFERENCE";
	public static final String DUP_CHEK_MARITAL_STATUS = "MARITAL_STATUS";
	public static final String DUP_CHEK_NUMBER_OF_DEPENDENTS = "NUMBER_OF_DEPENDENTS";
	public static final String DUP_CHEK_NUMBER_OF_TIMES_BANKRUPT = "NUMBER_OF_TIMES_BANKRUPT";
	public static final String DUP_CHEK_SIN = "SIN";
	public static final String DUP_CHEK_TOTAL_ASSET_AMOUNT = "TOTAL_ASSET_AMOUNT";
	public static final String DUP_CHEK_TOTAL_INCOME_AMOUNT = "TOTAL_INCOME_AMOUNT";
	public static final String DUP_CHEK_TOTAL_LIABILITY_AMOUNT = "TOTAL_LIABILITY_AMOUNT";
	public static final String DUP_CHEK_TOTAL_LIABILITY_PAYMENTS = "TOTAL_LIABILITY_PAYMENTS";
	public static final String DUP_CHEK_MIDDLE_INITIAL = "MIDDLE_INITIAL";
	public static final String DUP_CHEK_BORROWERS_ASSUMED_DIFFERENT = "BORROWERS_ASSUMED_DIFFERENT";
	public static final String DUP_CHEK_LAST_NAME = "LAST_NAME";
	public static final String DUP_CHEK_FIRST_NAME = "FIRST_NAME";
	public static final String DUP_CHEK_BORROWER_CHANGED = "DupChekBorrowerChanged";
	//appendNote(NEW_LINE + "***  Borrower '" + b1.getBorrowerFirstName() +
	//  " " + b1.getBorrowerLastName() + "' Changed  ***");*/
	public static final String DUP_CHEK_BORROWER_END = "DupChekBorrowerEnd";
	//appendNote(NEW_LINE + "***  End Borrower '" + b1.getBorrowerFirstName() +
	//	" " + b1.getBorrowerLastName() + "' Changed  ***");*/
	public static final String DUP_CHEK_BORROWER_DELETED = "DupChekBorrowerDeleted";
	//#DG600 appendNote(NEW_LINE + "***  Borrower  '" + name + "' Deleted  ***");
	public static final String DUP_CHEK_BORROWER_ADDED = "DupChekBorrowerAdded";
	//#DG600 appendNote(NEW_LINE + "***  Borrower  '" + name + "' Added  ***");
	public static final String DUP_CHEK_DOWN_PAY_CHANGED = "DupChekDOWN_PAY_SRC_CHANGED";
	//appendNote(NEW_LINE + "* "+ BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE",lang)+" '" + desc + "'" + BXResources.getDocPrepIngMsg("CHANGED",lang) +" *");
	public static final String DUP_CHEK_DOWN_PAY_ADDED = "DupChekDOWN_PAY_SRC_ADDED";
	//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE_ADDED",lang)+" *");
	public static final String DUP_CHEK_DOWN_PAY_DELETED = "DupChekDOWN_PAY_SRC_DELETED";
	//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("DOWN_PAYMENT_SOURCE_DELETED",lang)+" *");
	public static final String DUP_CHEK_DOWN_PAY_DESCR = "DOWN_PAYMENT_DESCRIPTION";
	public static final String DUP_CHEK_DOWN_PAY_SRC_TYPE = "DOWN_PAYMENT_SOURCE_TYPE";
	public static final String DUP_CHEK_DOWN_PAYMENT = "DOWN_PAYMENT";
	public static final String DUP_CHEK_DOWN_PAY_SRC = "DOWN_PAYMENT_SOURCE";
	public static final String DUP_CHEK_REQUESTED_RATE = "REQUESTED_RATE";
	public static final String DUP_CHEK_NET_RATE_FROM_ING = "NET_RATE_FROM_INGESTION";
	public static final String DUP_CHEK_LOC_REPAY_TYPE_ID = "LOC_REPAYMENT_TYPE_ID";
	public static final String DUP_CHEK_CMHC_PROD_TRAK_ID = "CMHC_PRODUCT_TRACKER_IDENTIFIER";
	public static final String DUP_CHEK_SELF_DIRECTED_RRSP = "SELF_DIRECTED_RRSP";
	public static final String DUP_CHEK_PRODUCT_TYPE_ID = "PRODUCT_TYPE_ID";
	public static final String DUP_CHEK_TOTAL_PURC_PRICE = "TOTAL_PURCHASE_PRICE";
	public static final String DUP_CHEK_PURPOSE = "PURPOSE";
	public static final String DUP_CHEK_ORIG_PURC_PRICE = "ORIGINAL_PURCHASE_PRICE";
	public static final String DUP_CHEK_ORIG_MTG_AMT = "ORIGINAL_MTG_AMOUNT";
	public static final String DUP_CHEK_IMPROVEMENTS = "IMPROVEMENTS";
	public static final String DUP_CHEK_MORTGAGE_HOLDER = "MORTGAGE_HOLDER";
	public static final String DUP_CHEK_VALUE_IMPROVEM = "VALUE_OF_IMPROVEMENTS";
	public static final String DUP_CHEK_OUTS_MTG_AMT = "OUTSTANDING_MTG_AMOUNT";
	public static final String DUP_CHEK_PRODUCT = "PRODUCT";
	public static final String DUP_CHEK_MI_EXISTING_POL_NR = "MI_EXISTING_POLICY_NUMBER";
	public static final String DUP_CHEK_PAYMENT_TERM_DESCR = "PAYMENT_TERM_DESCRIPTION";
	public static final String DUP_CHEK_PRE_AP_EST_PUR_PRIC = "PRE-APP_ESTIMATED_PURCHASE_PRICE";
	public static final String DUP_CHEK_NR_GUARANTS = "NUMBER_OF_GUARANTORS";
	public static final String DUP_CHEK_NR_BOROWERS = "NUMBER_OF_BORROWERS";
	public static final String DUP_CHEK_LOAN = "LOAN";
	public static final String DUP_CHEK_PROP_DIFF = "DupChekPROPERTY_DIFFERENCES";
	//#DG600 int place = appendNote("* " + BXResources.getDocPrepIngMsg("PROPERTY_DIFFERENCES",lang) + " *");
	public static final String DUP_CHEK_REQUESTAPPRAISAL = "REQUESTAPPRAISAL";
	public static final String DUP_CHEK_PROP_RES_TR_AGR_NR = "PROPERTY_ON_RESERVE_TRUST_AGREEMENT_NUMBER";
	public static final String DUP_CHEK_PROP_SUBDIV_DISC = "PROPERTY_SUBDIVISION_DISCOUNT";
	public static final String DUP_CHEK_PROP_MI_ENERG_EFIC = "PROPERTY_MI_ENERGY_EFFICIENCY";
	public static final String DUP_CHEK_ZONING = "ZONING";
	public static final String DUP_CHEK_YEAR_BUILT = "YEAR_BUILT";
	public static final String DUP_CHEK_PROP_UNIT_NR = "PROPERTY_UNIT_NUMBER";
	public static final String DUP_CHEK_STRUCTURE_AGE = "STRUCTURE_AGE";
	public static final String DUP_CHEK_PURCHASE_PRICE = "PURCHASE_PRICE";
	public static final String DUP_CHEK_PROPERTY_PROVINCE = "PROPERTY_PROVINCE";
	public static final String DUP_CHEK_PROP_STR_NAME = "PROPERTY_STREET_NAME";
	public static final String DUP_CHEK_PROP_STR_NBR = "PROPERTY_STREET_NUMBER";		//#DG644
	public static final String DUP_CHEK_PROP_STR_TYP = "PROPERTY_STREET_TYPE";
	public static final String DUP_CHEK_PROP_STR_DIR = "PROPERTY_STREET_DIRECTION";
	public static final String DUP_CHEK_PROP_POSTAL = "PROPERTY_POSTAL_CODE";
	public static final String DUP_CHEK_PROPERTY_CITY = "PROPERTY_CITY";
	public static final String DUP_CHEK_PROP_ADDR_2 = "PROPERTY_ADDRESS_LINE_2";
	public static final String DUP_CHEK_NUMBER_OF_UNITS = "NUMBER_OF_UNITS";
	public static final String DUP_CHEK_NUMBER_OF_BEDROOMS = "NUMBER_OF_BEDROOMS";
	public static final String DUP_CHEK_PROP_TENURE_TYP_ID = "PROPERTY_TENURE_TYPE_ID";
	public static final String DUP_CHEK_WATER = "WATER";
	public static final String DUP_CHEK_SEWAGE = "SEWAGE";
	public static final String DUP_CHEK_PROPERTY_USAGE = "PROPERTY_USAGE";
	public static final String DUP_CHEK_PROPERTY_TYPE = "PROPERTY_TYPE";
	public static final String DUP_CHEK_PROPERTY_LOCATION = "PROPERTY_LOCATION";
	public static final String DUP_CHEK_OCCUPANCY_TYPE = "OCCUPANCY_TYPE";
	public static final String DUP_CHEK_NEW_CONSTRUCTION = "NEW_CONSTRUCTION";
	public static final String DUP_CHEK_MUNICIPALITY = "MUNICIPALITY";
	public static final String DUP_CHEK_MONTHLY_CONDO_FEES = "MONTHLY_CONDO_FEES";
	public static final String DUP_CHEK_MLS = "MLS";
	public static final String DUP_CHEK_LOT_UNIT_MEASUR = "LOT_SIZE_UNIT_OF_MEASUREMENT";
	public static final String DUP_CHEK_LOT_SIZE_DEPTH = "LOT_SIZE_DEPTH";
	public static final String DUP_CHEK_LOT_SIZE_FRONTAGE = "LOT_SIZE_FRONTAGE";
	public static final String DUP_CHEK_LIVIN_UNIT_MEASUR = "LIVING_SPACE_UNIT_OF_MEASUREMENT";
	public static final String DUP_CHEK_LIVING_SPACE = "LIVING_SPACE";
	public static final String DUP_CHEK_LEGAL_DESCR1 = "LEGAL_DESCRIPTION_LINE_1";
	public static final String DUP_CHEK_LEGAL_DESCR2 = "LEGAL_DESCRIPTION_LINE_2";
	public static final String DUP_CHEK_LAND_VALUE = "LAND_VALUE";
	public static final String DUP_CHEK_HEAT_TYPE = "HEAT_TYPE";
	public static final String DUP_CHEK_GARAGE_TYPE = "GARAGE_TYPE";
	public static final String DUP_CHEK_GARAGE_SIZE = "GARAGE_SIZE";
	public static final String DUP_CHEK_EQUITY_AVAILABLE = "EQUITY_AVAILABLE";
	public static final String DUP_CHEK_DWELLING_TYPE = "DWELLING_TYPE";
	public static final String DUP_CHEK_DWELLING_STYLE = "DWELLING_STYLE";
	public static final String DUP_CHEK_BUILDER_NAME = "BUILDER_NAME";
	public static final String DUP_CHEK_APPRAISAL_DATE = "APPRAISAL_DATE";
	public static final String DUP_CHEK_ACT_APRAISED_VAL = "ACTUAL_APPRAISED_VALUE";
	public static final String DUP_CHEK_PREMIUM = "PREMIUM";
	public static final String DUP_CHEK_BUYDOWNRATE = "BUYDOWNRATE";
	public static final String DUP_CHEK_DISCOUNT = "DISCOUNT";
	public static final String DUP_CHEK_AFFILIATIONPROGRAM = "AFFILIATIONPROGRAM";
	public static final String DUP_CHEK_CASH_BACK_PERCENT = "CASH_BACK_PERCENT";
	public static final String DUP_CHEK_CASH_BACK_AMOUNT = "CASH_BACK_AMOUNT";
	public static final String DUP_CHEK_SMOKESTATUSID = "SMOKESTATUSID";
	public static final String DUP_CHEK_PREFCONTACTMETH_ID = "PREFCONTACTMETHODID";
	public static final String DUP_CHEK_SOLICITATIONID = "SOLICITATIONID";
	public static final String DUP_CHEK_ESCROW_AMOUNT = "ESCROW_AMOUNT"; //use same as NBC_ADMIN_FEE
	public static final String DUP_CHEK_BOROWERID_ADDED = "DupChekBORROWERIDENTIFICATION_ADDED";
	//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("LIABILITY_DELETED",lang) + "  *");
	public static final String DUP_CHEK_BOROWERID_DELETED = "DupChekBORROWERIDENTIFICATION_DELETED";
	/*appendNote(NEW_LINE	+ "* "
	 + BXResources.getDocPrepIngMsg("BORROWERIDENTIFICATION_DELETED", lang) + " *");*/
	public static final String DUP_CHEK_BOROWERID_CHANGED = "DupChekBORROWERIDENTIFICATION_CHANGED";
	/*appendNote(NEW_LINE			+ "* "			+ BXResources.getDocPrepIngMsg(
	 "BORROWERIDENTIFICATION", lang) + "'"
	 + bi1.getIdentificationNumber() + "'"_
	 + BXResources.getDocPrepIngMsg("CHANGED", lang)			+ " *");*/
	public static final String DUP_CHEK_ASSET_ADDED = "DupChekASSET_ADDED";
	/*appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("ASSET_ADDED",lang) +" *");
	 addTo(dealNotesList, printAsset((Asset)i.next()));*/
	public static final String DUP_CHEK_ASSET_DELETED = "DupChekASSET_DELETED";
	/*appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("ASSET_DELETED",lang) +" *");
	 addTo(dealNotesList, printAsset(a1));*/
	public static final String DUP_CHEK_ASSET_CHANGED = "DupChekASSET_CHANGED";
	/*appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("ASSET",lang) +"'" +
	 desc1 + "'"+ BXResources.getDocPrepIngMsg("CHANGED",lang) +" *");*/
	public static final String DUP_CHEK_PERC_INC_OUT_TDS = "PERCENT_INCLUDED_OUT_TDS";
	public static final String DUP_CHEK_PERC_INC_OUT_GDS = "PERCENT_INCLUDED_OUT_GDS";
	public static final String DUP_CHEK_PERC_INC_IN_TDS = "PERCENT_INCLUDED_IN_TDS";
	public static final String DUP_CHEK_PERC_INC_IN_GDS = "PERCENT_INCLUDED_IN_GDS";
	public static final String DUP_CHEK_MONTHLY_EXPENSE = "MONTHLY_EXPENSE";
	public static final String DUP_CHEK_EXPENSE_TYPE = "EXPENSE_TYPE";
	public static final String DUP_CHEK_EXPENSE_PERIOD = "EXPENSE_PERIOD";
	public static final String DUP_CHEK_PERC_INC_NET_WORTH = "PERCENT_INCLUDED_IN_NET_WORTH";
	public static final String DUP_CHEK_INC_IN_NET_WORTH = "INCLUDE_IN_NET_WORTH";
	public static final String DUP_CHEK_ASSET_VALUE = "ASSET_VALUE";
	public static final String DUP_CHEK_ASSET_TYPE = "ASSET_TYPE";
	public static final String DUP_CHEK_INCLUDE_IN_TDS = "INCLUDE_IN_TDS";
	public static final String DUP_CHEK_INCLUDE_IN_GDS = "INCLUDE_IN_GDS";
	public static final String DUP_CHEK_INCOME_PERIOD = "INCOME_PERIOD";
	public static final String DUP_CHEK_ANUAL_INCOME_AMT = "ANNUAL_INCOME_AMOUNT";
	public static final String DUP_CHEK_LIFESTATUSID = "LIFESTATUSID";
	public static final String DUP_CHEK_DISAB_STATID = "DISABILITYSTATUSID";
	public static final String DUP_CHEK_LIFEPERC_COVER_REQ = "LIFEPERCENTCOVERAGEREQ";
	public static final String DUP_CHEK_INSUR_PROPORT_ID = "INSURANCEPROPORTIONSID";
	public static final String DUP_CHEK_CRED_BUR_FILE_DATE = "CREDIT_BUREAU_ON_FILE_DATE";
	public static final String DUP_CHEK_INSUR_SPOU_ADDED = "DupChekInsurSpouse_ADDED";
	/*name = a.getInsureOnlyApplicantFirstName() + " " + a.getInsureOnlyApplicantLastName();
	 appendNote(NEW_LINE + "***  Insured Spouse  '" + name + "' Added  ***");*/
	public static final String DUP_CHEK_INSUR_SPOU_DELETED = "DupChekInsurSpouse_DELETED";
	/*appendNote(NEW_LINE + "***  Insured Spouse  '" + name + "' Deleted  ***");*/
	public static final String DUP_CHEK_INSUR_SPOU_CHANGED = "DupChekInsurSpouse_CHANGED";
	/*appendNote(NEW_LINE + "***  Insured Spouse '" + a1.getInsureOnlyApplicantFirstName() +
	 " " + a1.getInsureOnlyApplicantLastName() + "' Changed  ***");*/
	public static final String DUP_CHEK_INSUR_SPOE_CHANGED = "DupChekInsurSpouseEnd_CHANGED";
	/*appendNote(NEW_LINE + "***  End Insured Spouse '" + a1.getInsureOnlyApplicantFirstName() +
	 " " + a1.getInsureOnlyApplicantLastName() + "' Changed  ***");*/
	public static final String DUP_CHEK_LIAB_TYPE = "TYPE";
	public static final String DUP_CHEK_LIAB_PAYOFF_TYPE = "LIABILITY_PAYOFF_TYPE";
	public static final String DUP_CHEK_LIAB_PAY_QUAL = "LIABILITY_PAYMENT_QUALIFIER";
	public static final String DUP_CHEK_MTHLY_PAY = "MTHLY_PAY";
	public static final String DUP_CHEK_PROP_EXP_ADDED = "DupChekPROPERTY_EXPENSE_ADDED";
	//*appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("PROPERTY_EXPENSE_ADDED",lang) + " *");
	public static final String DUP_CHEK_PROP_EXP_DELETED = "DupChekPROPERTY_EXPENSE_DELETED";
	//appendNote(NEW_LINE + "* " + BXResources.getDocPrepIngMsg("PROPERTY_EXPENSE_DELETED",lang) + "  *");
	public static final String DUP_CHEK_PROP_EXP_CHANGED = "DupChekPROPERTY_EXPENSE_CHANGED";
	//appendNote(NEW_LINE + "* " +  BXResources.getDocPrepIngMsg("PROPERTY_EXPENSE",lang) + " '"  + "'" + BXResources.getDocPrepIngMsg("CHANGED",lang) + " *");
	public static final String DUP_CHEK_INCOME_TYPE = "INCOME_TYPE";		//#DG632
	public static final String DUP_CHEK_BOR_ID_NO_INFO = "DupChekBorIdNoInfo";
	public static final String DUP_CHEK_ID_COUNTRY = "IDENTIFICATION_COUNTRY";
	public static final String DUP_CHEK_ID_NUMBER = "IDENTIFICATION_NUMBER";
	public static final String DUP_CHEK_ID_TYPE = "IDENTIFICATION_TYPE";
	public static final String DUP_CHEK_CONT_FAX_NR = "CONTACT_FAX_NUMBER";
	public static final String DUP_CHEK_CONT_EMAIL = "CONTACT_EMAIL_ADDRESS";
	public static final String DUP_CHEK_CONT_JOB_TIT = "CONTACT_JOB_TITLE";
	public static final String DUP_CHEK_CONT_PHONE_EXT = "CONTACT_PHONE_NUMBER_EXTENSION";
	public static final String DUP_CHEK_CONT_PHONE_NR = "CONTACT_PHONE_NUMBER";
	public static final String DUP_CHEK_CONT_LAST_NAME = "CONTACT_LAST_NAME";
	public static final String DUP_CHEK_CONT_MID_INITIAL = "CONTACT_MIDDLE_INITIAL";
	public static final String DUP_CHEK_CONT_FIRST_NAME = "CONTACT_FIRST_NAME";
	public static final String DUP_CHEK_OCCUPATION = "OCCUPATION";
	public static final String DUP_CHEK_JOB_TITLE = "JOB_TITLE";
	public static final String DUP_CHEK_JOB_TITLE_CATEGORY = "JOB_TITLE_CATEGORY";
	public static final String DUP_CHEK_INDUSTRY_SECTOR = "INDUSTRY_SECTOR";
	public static final String DUP_CHEK_EMPLOY_HIST_TYPE = "EMPLOYMENT_HISTORY_TYPE";
	public static final String DUP_CHEK_EMPLOY_HIST_STATUS = "EMPLOYMENT_HISTORY_STATUS";		//#DG648
	public static final String DUP_CHEK_EMPLOYEENUMBER = "EMPLOYEENUMBER";
	public static final String DUP_CHEK_STAFF_OF_LENDER = "STAFF_OF_LENDER";
	public static final String DUP_CHEK_BORROWERGENDERID = "BORROWERGENDERID";
	public static final String DUP_CHEK_GUAR_OTHER_LOANS = "GUARANTOR_OTHER_LOANS";
	public static final String DUP_CHEK_CREDIT_SCORE = "CREDIT_SCORE";
	public static final String DUP_CHEK_UFFI_INSULATION = "UFFI_INSULATION";
	public static final String DUP_CHEK_RATEGUARANTEPERIOD = "RATEGUARANTEEPERIOD";
	public static final String DUP_CHEK_PROGRESS_ADVANCE = "PROGRESS_ADVANCE";
	public static final String DUP_CHEK_MI_UPFRONT = "MI_UPFRONT";
	public static final String DUP_CHEK_ASSET_DESCRIPTION = "ASSET_DESCRIPTION";

	//********** DupeCheckProcessor *****************
	public static final String DUP_CHEK_ATTEMPTEDRJCT_X = "DupChekATTEMPTEDRjctX";
	public static final String DUP_CHEK_ATTEMPTEDRJCT = "DupChekATTEMPTEDRjct";
	public static final String DUP_CHEK_DEAL_PREV_DENIED = "DupChekDealPrevDenied";
	public static final String DUP_CHEK_DEAL_RESUBMTD = "DupChekDealResubmtd";
	public static final String DUP_CHEK_DEAL_HAS_DUP_PROP = "DupChekDealHasDupProp";
	public static final String DUP_CHEK_DEAL_HAS_DUP = "DupChekDealHasDup";
	public static final String DUP_CHEK_PREAPPROVALFIRM = "DupChekPREAPPROVALFIRM";
	public static final String DUP_CHEK_PREAPPROVALFIRMUPD = "DupChekPREAPPROVALFIRMUpd";
	public static final String DUP_CHEK_RESUBMITED = "DupChekRESUBMITED";
	public static final String DUP_CHEK_RESUBMITEDBRK = "DupChekRESUBMITEDBrk";
	public static final String DUP_CHEK_RESUBMISIONRCVD = "DupChekRESUBMISIONRCVD";
	public static final String DUP_CHEK_RESUBMISION = "DupChekRESUBMISION";
	public static final String DUP_CHEK_RESUBMISIONRCVDA = "DupChekRESUBMISIONRCVDa";

	//********** NoteBuilder *****************
	public static final String DUP_CHEK_MATCHEDDEALAPL_PROP = "DupChekMATCHEDDEALAplProp";
	public static final String DUP_CHEK_MATCHEDDEALPROP = "DupChekMATCHEDDEALProp";
	public static final String DUP_CHEK_MATCHEDDEALAPL = "DupChekMATCHEDDEALApl";
	public static final String DUP_CHEK_MATCHEDDEAL = "DupChekMATCHEDDEAL";
	public static final String DUP_CHEK_ATTEMPTED = "DupChekATTEMPTED";
	public static final String DUP_CHEK_ATTEMPTEDFD = "DupChekATTEMPTEDfd";
	public static final String DUP_CHEK_GONE_FIRM = "DupChekGONE_FIRM";
	public static final String DUP_CHEK_UPDATED = "DupChekUPDATED";
	public static final String DUP_CHEK_CAUTION_PROP = "DupChekCAUTION_PROP";

	//********** SearchResult *****************
	public static final String DUP_CHEK_UNNAMED_SOURCE_FIRM = "DupChekUnnamedSourceFirm";
	public static final String DUP_CHEK_UNNAMED_SOURCE_OF_BUSINESS = "DupChekUnnamedSourceOfBusiness";
	public static final String DUP_CHEK_UNKNOWN_SOURCE_OF_BUSINESS = "DupChekUnknownSourceOfBusiness";
	public static final String DUP_CHEK_UNKNOWN_SOURCE_FIRM = "DupChekUnknownSourceFirm";

	//********** AME process *****************
	public static final String AME_DEAL_HOLD = "AME_DEAL_HOLD";
	public static final String AME_DEAL_APPROVED = "AME_DEAL_APPROVED";
	public static final String AME_DEAL_RESOLUTION_ENTER_APPROVAL = "AME_DEAL_RESOLUTION_ENTER_APPROVAL";
	public static final String AME_FAILED_MI_BUSINESS_RULE = "AME_FAILED_MI_BUSINESS_RULE";
	public static final String AME_DEAL_DECLINED = "AME_DEAL_DECLINED";

	/*#DG622 document format from table DocumentProfile.DocumentFormat
	(could be used in DocumentFormat.java??) */
  public static final String DOC_FMT_XML_PENDING = "xml-pending";
	public static final String DOC_FMT_XML_UPLOAD = "xml-upload";
	public static final String DOC_FMT_XML_DENIAL = "xml-denial";
	public static final String DOC_FMT_XML_APPROVAL = "xml-approval";
	public static final String DOC_FMT_XML_FUNDING = "xml-funding";

	//********** Address Scrube **************
	public static final String ADR_SCR_ORIGINAL = "ADRES_SCRUB_ORIGINAL";
	public static final String ADR_SCR_EMPLOYM = "ADRES_SCRUB_EMPLOYM";
	public static final String ADR_SCR_EMPLOYM_NOT = "ADRES_SCRUB_EMPLOYM_NOT";
	public static final String ADR_SCR_ADR_BORCUR = "ADRES_SCRUB_ADR_BORCUR";
	public static final String ADR_SCR_ADR_BORCUR_NOT = "ADRES_SCRUB_ADR_BORCUR_NOT";
	public static final String ADR_SCR_ADR_BORPREV = "ADRES_SCRUB_ADR_BORPREV";
	public static final String ADR_SCR_ADR_BORPREV_NOT = "ADRES_SCRUB_ADR_BORPREV_NOT";
	public static final String ADR_SCR_SUGLISTGEN = "ADRES_SCRUB_SUGLISTGEN";
	public static final String ADR_SCR_OUTPUT = "ADRES_SCRUB_OUTPUT";
	public static final String ADR_SCR_PROPADRS = "ADRES_SCRUB_PROPADRS";
	public static final String ADR_SCR_PROPADRS_NOT = "ADRES_SCRUB_PROPADRS_NOT";

	//history loggin					#DG720
	public static final String WIRE_TRANSFER_DATA_ENTRY_DOCS_SUBMITTED = "WireTransfer & DataEntry docs. Submitted";
	public static final String DISBURSMENT_LETTER_SUBMITTED = "Disbursment Letter Submitted";

	public static final String EXTERNAL_LINK_SYSTEM_ERROR = "EXTERNAL_LINK_SYSTEM_ERROR";

	public static final String supresTagStart = "<!--";		//#DG702
	public static final String supresTagEnd = "-->";

  public static final String RATE_CODE_UNCONFIRMED = "UNCNF";		//#DG688

	public static final String ENCODING_ISO_8859_1 = "ISO-8859-1";
  public static final String ENCODING_UTF_8 = "UTF-8";

	public static final String NODE_DOCUMENT = "Document";		//#DG744

	//#DG812 product names
	public static final String PRODUCT_VIP = "VIP";

  public static final String EMAIL_REG_EXP_PATERN = "^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";		//#DG728

  //#DG750 RichClientMessage
	public static final String AML_SESSION_ENDED = "AML_SESSION_ENDED";
	public static final String UPLOAD_FAILED_MESSAGE = "UPLOAD_FAILED_MESSAGE";			//#DG810
	public static final String SERVICING_ERROR_SYSTEM_AVAILABLE = "SERVICING_ERROR_SYSTEM_AVAILABLE";
   public static final String SERVICE_OCS_ERROR_PROMPT = "SERVICE_OCS_ERROR_PROMPT";

	//#DG690
	public static final String CBINSTITUTIONNAMES_NONSELECTED_LABEL = "CBINSTITUTIONNAMES_NONSELECTED_LABEL";
	public static final String CBREGIONNAMES_NONSELECTED_LABEL = "CBREGIONNAMES_NONSELECTED_LABEL";
	public static final String CBBRANCHNAMES_NONSELECTED_LABEL = "CBBRANCHNAMES_NONSELECTED_LABEL";
	public static final String CBMANAGERS_NONSELECTED_LABEL = "CBMANAGERS_NONSELECTED_LABEL";
	public static final String CBJOINTAPPROVERS_NONSELECTED_LABEL = "CBJOINTAPPROVERS_NONSELECTED_LABEL";
	public static final String CBHIGHERAPPROVERS_NONSELECTED_LABEL = "CBHIGHERAPPROVERS_NONSELECTED_LABEL";
	public static final String CBGROUPPROFILENAME_NONSELECTED_LABEL = "CBGROUPPROFILENAME_NONSELECTED_LABEL";
	public static final String CBSTANDIN_NONSELECTED_LABEL = "CBSTANDIN_NONSELECTED_LABEL";
	public static final String CBPARTNERPROFILENAMES_NONSELECTED_LABEL = "CBPARTNERPROFILENAMES_NONSELECTED_LABEL";
	public static final String CBUSERNAMES_NONSELECTED_LABEL = "CBUSERNAMES_NONSELECTED_LABEL";
	public static final String CBPAGENAMES_NONSELECTED_LABEL = "CBPAGENAMES_NONSELECTED_LABEL";
	public static final String CBUSERFILTER_ALL_LABEL = "CBUSERFILTER_ALL_LABEL";
	public static final String MWQ_STATUS_DEAL_CBOPTIONS_ALL_LABEL = "MWQ_STATUS_DEAL_CBOPTIONS_ALL_LABEL";
	public static final String MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL = "MWQ_STATUS_TASK_CBOPTIONS_ALL_LABEL";
	public static final String USER_ADMIN_DUP_GROUP_NAME = "USER_ADMIN_DUP_GROUP_NAME";
	public static final String USER_ADMIN_MISSING_BRANCHID = "USER_ADMIN_MISSING_BRANCHID";
	public static final String USER_ADMIN_MISSING_NEW_GROUP_NAME = "USER_ADMIN_MISSING_NEW_GROUP_NAME";
	public static final String USER_ADMIN_NEW_USER_CANNOT_CREATE_GROUP = "USER_ADMIN_NEW_USER_CANNOT_CREATE_GROUP";
	public static final String USER_AUTO_INVALID_NEW_USER_TYPE = "USER_AUTO_INVALID_NEW_USER_TYPE";
	public static final String USER_ADMIN_ADMIN_PARTNER_MUST_BE_UD = "USER_ADMIN_ADMIN_PARTNER_MUST_BE_UD";
	public static final String USER_ADMIN_UD_PARTNER_MUST_BE_ADMIN = "USER_ADMIN_UD_PARTNER_MUST_BE_ADMIN";
	public static final String USER_ADMIN_HIGHER_JOINT_APP_NOT_REQ = "USER_ADMIN_HIGHER_JOINT_APP_NOT_REQ";
	public static final String USER_ADMIN_HIGHER_JOINT_APPROVER = "USER_ADMIN_HIGHER_JOINT_APPROVER";
	public static final String USER_ADMIN_LINE_OF_BUSINESS_NOT_REQUIRED = "USER_ADMIN_LINE_OF_BUSINESS_NOT_REQUIRED";
	public static final String USER_ADMIN_LINE_OF_BUSINESS_REQUIRED = "USER_ADMIN_LINE_OF_BUSINESS_REQUIRED";
	public static final String USER_AUTO_PASSWORD_REQUIRED = "USER_AUTO_PASSWORD_REQUIRED";
	public static final String USER_ADMIN_PASSWORD_REQUIRED = "USER_ADMIN_PASSWORD_REQUIRED";
	public static final String USER_PASSWORD_NOT_MATCH_VERIFICATION = "USER_PASSWORD_NOT_MATCH_VERIFICATION";
	public static final String USER_ADMIN_EMPTY_CONTACT_INFO = "USER_ADMIN_EMPTY_CONTACT_INFO";
	public static final String USER_ADMIN_EMAIL_ADDR_MISSING = "USER_ADMIN_EMAIL_ADDR_MISSING";
	public static final String USER_ADMIN_BUSINESSID_ALREADY_EXIST = "USER_ADMIN_BUSINESSID_ALREADY_EXIST";
	public static final String USER_ADMIN_LOGIN_ALREADY_EXIST = "USER_ADMIN_LOGIN_ALREADY_EXIST";
	public static final String USER_ADMIN_EMPTY_LOGIN_FIELD = "USER_ADMIN_EMPTY_LOGIN_FIELD";
	public static final String USER_ADMIN_INVALID_FAX_NUMBER = "USER_ADMIN_INVALID_FAX_NUMBER";
	public static final String USER_ADMIN_INVALID_PHONE_NUMBER = "USER_ADMIN_INVALID_PHONE_NUMBER";
	public static final String USER_PROFILE_ASSIGNED_GROUP = "USER_PROFILE_ASSIGNED_GROUP";
	public static final String USERADMIN_ERROR_MSG_INFOMSG = "USERADMIN_ERROR_MSG_INFOMSG";
	public static final String USERADMIN_ERROR_MSG_TITLE = "USERADMIN_ERROR_MSG_TITLE";
	public static final String USER_PROFILE_SUCCESSFULLY_UPDATED = "USER_PROFILE_SUCCESSFULLY_UPDATED";
	public static final String USER_ADMIN_USER_TYPE_IS_REQUIRED = "USER_ADMIN_USER_TYPE_IS_REQUIRED";
	public static final String PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM = "PASSIVE_MESSAGE_SUBTITLE_DEAL_PROBLEM";
	public static final String DEH_PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM = "PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM";		//#DG702
	public static final String DEH_PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM = "PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM";
	public static final String CREDIT_SCOREING_APPLICATION_MESSAGE_MORE_THAN_ONE_LABEL = "CREDIT_SCOREING_APPLICATION_MESSAGE_MORE_THAN_ONE_LABEL";
	public static final String CREDIT_SCOREING_APPLICATION_MESSAGE_LESS_THAN_ONE_LABEL = "CREDIT_SCOREING_APPLICATION_MESSAGE_LESS_THAN_ONE_LABEL";
	public static final String POST_DECISION_UNLOCK_NO_DOC_IMAGE = "POST_DECISION_UNLOCK_NO_DOC_IMAGE";
	public static final String POST_DECISION_VIEW_ONLY_IMAGE = "POST_DECISION_VIEW_ONLY_IMAGE";
	public static final String UNLOCK_POST_DECISION_DEAL_MOD_QUERY_TITLE = "UNLOCK_POST_DECISION_DEAL_MOD_QUERY_TITLE";
	public static final String UNLOCK_POST_DECISION_DEAL_MOD_QUERY = "UNLOCK_POST_DECISION_DEAL_MOD_QUERY";
	public static final String DEAL_MOD_NEW_DEAL_CREATED = "DEAL_MOD_NEW_DEAL_CREATED";
	public static final String NOT_AVAILABLE_LABEL = "NOT_AVAILABLE_LABEL";
	public static final String GDS_TDS_WITH_LIFE_DISABILITY_LABEL = "GDS_TDS_WITH_LIFE_DISABILITY_LABEL";
	public static final String GDS_TDS_NO_LIFE_DISABILITY_LABEL = "GDS_TDS_NO_LIFE_DISABILITY_LABEL";
	public static final String TASK_RELATED_UNEXPECTED_FAILURE = "TASK_RELATED_UNEXPECTED_FAILURE";
	public static final String CONFIRM_SUBMIT_TO_AU = "CONFIRM_SUBMIT_TO_AU";
	public static final String JAN_LABEL = "JAN_LABEL";
	public static final String FEB_LABEL = "FEB_LABEL";
	public static final String MAR_LABEL = "MAR_LABEL";
	public static final String APR_LABEL = "APR_LABEL";
	public static final String MAY_LABEL = "MAY_LABEL";
	public static final String JUN_LABEL = "JUN_LABEL";
	public static final String JUL_LABEL = "JUL_LABEL";
	public static final String AUG_LABEL = "AUG_LABEL";
	public static final String SEP_LABEL = "SEP_LABEL";
	public static final String OCT_LABEL = "OCT_LABEL";
	public static final String NOV_LABEL = "NOV_LABEL";
	public static final String DEC_LABEL = "DEC_LABEL";
	public static final String AT_LABEL = "AT_LABEL";
	public static final String ON_LABEL = "ON_LABEL";
	public static final String BASED_ON_SCENARIO_LABEL = "BASED_ON_SCENARIO_LABEL";
	public static final String BASED_ON_DEAL_LABEL = "BASED_ON_DEAL_LABEL";
	public static final String MI_PREMIUM_LABEL = "MI_PREMIUM_LABEL";
	public static final String UW_SWITCH_SCENARIO_DISCARD_CURRENT = "UW_SWITCH_SCENARIO_DISCARD_CURRENT";
	public static final String UW_CREATE_SCENARIO_DISCARD_CURRENT = "UW_CREATE_SCENARIO_DISCARD_CURRENT";
	public static final String UW_SCENARIO_SAVED_CLEAN = "UW_SCENARIO_SAVED_CLEAN";
	public static final String UW_SCENARIO_SAVED_WITH_MESSAGES = "UW_SCENARIO_SAVED_WITH_MESSAGES";
	public static final String PASSIVE_MSG_APPLIED_TO_PRORERTY_LABEL = "PASSIVE_MSG_APPLIED_TO_PRORERTY_LABEL";
	public static final String PASSIVE_MSG_APPLIED_TO_APPLICANT_LABEL = "PASSIVE_MSG_APPLIED_TO_APPLICANT_LABEL";
	public static final String STANDARD_REVIEW_MESSAGES_WINDOW = "STANDARD_REVIEW_MESSAGES_WINDOW";
	public static final String UW_CANNOT_RESOLVE_VALIDATION_CRITICAL = "UW_CANNOT_RESOLVE_VALIDATION_CRITICAL";
	public static final String UW_CANNOT_RESOLVE_LOCKED_SCENARIO = "UW_CANNOT_RESOLVE_LOCKED_SCENARIO";
	public static final String UW_DEAL_RESOLUTION_NOT_ALLOWED_STATUS_CATEGORY = "UW_DEAL_RESOLUTION_NOT_ALLOWED_STATUS_CATEGORY";
	public static final String ORIGINAL_DEAL_LABEL = "ORIGINAL_DEAL_LABEL";
	public static final String MI_DATA_CHANGED_WARNING = "MI_DATA_CHANGED_WARNING";
	public static final String UNDERWRITER_USER_ID_HAS_NOT_BEEN_ASSIGNED = "UNDERWRITER_USER_ID_HAS_NOT_BEEN_ASSIGNED";
	public static final String PAGE_DENIED_DEAL_LOCKED = "PAGE_DENIED_DEAL_LOCKED";
	public static final String PAGE_DENIED_DEAL_LOCKED_FIRST_PART = "PAGE_DENIED_DEAL_LOCKED_FIRST_PART";
	public static final String OVERRIDE_DEAL_LOCK = "OVERRIDE_DEAL_LOCK";
	public static final String PAGE_DENIED_DESCLOURE = "PAGE_DENIED_DESCLOURE";
	public static final String PAGE_DENIED_DEFAULT = "PAGE_DENIED_DEFAULT";
	public static final String YEAR_SHORT = "YEAR_SHORT";
	public static final String MONTH_SHORT = "MONTH_SHORT";
	public static final String YEARS_SHORT = "YEARS_SHORT";
	public static final String MONTHS_SHORT = "MONTHS_SHORT";
	public static final String INSURER_LABEL = "INSURER_LABEL";
	public static final String LENDER_LABEL = "LENDER_LABEL";
	public static final String GOTO_PAGE_NOINPUT = "GOTO_PAGE_NOINPUT";
	public static final String GOTO_PAGE_NODEALNUM = "GOTO_PAGE_NODEALNUM";
	public static final String NO_MATCH_FOR_GO_DEAL_NUMBER = "NO_MATCH_FOR_GO_DEAL_NUMBER";
	public static final String DEALID_MISSING = "DEALID_MISSING";
	public static final String FUNCTION_NOT_SUPPORTED = "FUNCTION_NOT_SUPPORTED";
	public static final String NO_WORK_QUEUE_ACCESS = "NO_WORK_QUEUE_ACCESS";
	public static final String SIGN_OFF_MSG_CONFIRM = "SIGN_OFF_MSG_CONFIRM";
	public static final String PASSWORD_CHANGE_ANNOY_WARNING = "PASSWORD_CHANGE_ANNOY_WARNING";		//#DG684
	public static final String PASSWORD_CHANGE_MANY_GRACE = "PASSWORD_CHANGE_MANY_GRACE";
	public static final String PASSWORD_CHANGE_ONE_GRACE = "PASSWORD_CHANGE_ONE_GRACE";
	public static final String PASSWORD_CHANGE_REQUIRED = "PASSWORD_CHANGE_REQUIRED";
	public static final String LOGIN_INVALID_TITLE = "LOGIN_INVALID_TITLE";
	public static final String LOGIN_ATTEMPTS_EXCEEDED = "LOGIN_ATTEMPTS_EXCEEDED";
	public static final String TECH_FAILURE_LOGIN = "TECH_FAILURE_LOGIN";
	public static final String LOGIN_SUSPENDED = "LOGIN_SUSPENDED";
	public static final String LOGIN_IN_USE = "LOGIN_IN_USE";
	public static final String USER_NEWPASSWORD_LOCKED = "USER_NEWPASSWORD_LOCKED";
	public static final String USER_PASSWORD_LOCKED = "USER_PASSWORD_LOCKED";
	public static final String USER_INACTIVE = "USER_INACTIVE";
	public static final String INVALID_LOGIN = "INVALID_LOGIN";
	public static final String UP_PW_ERROR_SPECCHAR = "UP_PW_ERROR_SPECCHAR";
	public static final String UP_PW_ERROR_ALPNUM = "UP_PW_ERROR_ALPNUM";
	public static final String UP_PW_ERROR_MIXCASES = "UP_PW_ERROR_MIXCASES";
	public static final String UP_PW_ERROR_MINLENGTH = "UP_PW_ERROR_MINLENGTH";
	public static final String UP_PW_ERROR_SAMECURR = "UP_PW_ERROR_SAMECURR";
	public static final String UP_PW_ERROR_USED = "UP_PW_ERROR_USED";
	public static final String DEAL_ENTRY_ADD_BORROWER_COPY_ADDR = "DEAL_ENTRY_ADD_BORROWER_COPY_ADDR";
	public static final String BRANCH_ORIGINATION_LABEL = "BRANCH_ORIGINATION_LABEL";
	public static final String MARKETTYPE_LABEL = "MARKETTYPE_LABEL";
	public static final String CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_NOT_SELECTED = "CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_NOT_SELECTED";
	public static final String CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED = "CREDIT_BUREAU_LIAB_CONFIRM_EXPORT_DELETE_NOT_SELECTED";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_NOT_SELECTED = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_NOT_SELECTED";
	public static final String DUPECHECK_FOUND_DUPLICATED = "DUPECHECK_FOUND_DUPLICATED";
	public static final String DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS = "DEAL_ENTRY_MOD_SAVED_NO_PROBLEMS";
	public static final String DEAL_ENTRY_MOD_SAVED_WITH_WARNINGS = "DEAL_ENTRY_MOD_SAVED_WITH_WARNINGS";
	public static final String DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL = "DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL";
	public static final String DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL_INCOMPLETE_STATUS = "DEAL_ENTRY_MOD_SAVED_WITH_CRITICAL_INCOMPLETE_STATUS";
	public static final String DEAL_MODY_IS_COMMITMENT_RESEND_WITH_NO_WARNING = "DEAL_MODY_IS_COMMITMENT_RESEND_WITH_NO_WARNING";
	public static final String DEAL_MODY_IS_COMMITMENT_RESEND = "DEAL_MODY_IS_COMMITMENT_RESEND";
  public static final String CONFIRM_DELETE_INSURE_ONLY_IDENTIFICATION = "CONFIRM_DELETE_INSURE_ONLY_IDENTIFICATION";
	public static final String CONFIRM_DELETE_INSURE_ONLY_APPLICANT = "CONFIRM_DELETE_INSURE_ONLY_APPLICANT";
	public static final String CONFIRM_DELETE_CREDIT_BUREAU_LIAB = "CONFIRM_DELETE_CREDIT_BUREAU_LIAB";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_LIAB = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_LIAB";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ASSET = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ASSET";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_CREDIT_REFERENCE = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_CREDIT_REFERENCE";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_INCOME_RECORD = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_INCOME_RECORD";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_EMP_RECORD = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_EMP_RECORD";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ADDRESS = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ADDRESS";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_PROPERTY_EXPENSE = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_PROPERTY_EXPENSE";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ESCROW_PAYMENT = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_ESCROW_PAYMENT";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_DOWN_PAYMENT = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_DOWN_PAYMENT";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_PROPERTY = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_PROPERTY";
	public static final String DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_APPLICANT = "DEAL_ENTRY_MOD_UW_CONFIRM_DELETE_APPLICANT";
  public static final String CONFIRM_EXPORT_CREDIT_BUREAU_LIAB = "CONFIRM_EXPORT_CREDIT_BUREAU_LIAB";
  public static final String CONFIRM_EXPORT_DELETE_CREDIT_BUREAU_LIAB = "CONFIRM_EXPORT_DELETE_CREDIT_BUREAU_LIAB";
  public static final String PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM = "PASSIVE_MESSAGE_SUBTITLE_APPLICANT_PROBLEM";	// moved from Sc
  public static final String PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM = "PASSIVE_MESSAGE_SUBTITLE_PROPERTY_PROBLEM";
	public static final String EMAIL_ALERT_RUNNING_SHORT_ON_IP_CLIENT_NUMBERS = "EMAIL_ALERT_RUNNING_SHORT_ON_IP_CLIENT_NUMBERS";		//#DG720
	public static final String EMAIL_ALERT_RUNNING_SHORT_ON_IP_MTG_SERVICING_NUMBERS = "EMAIL_ALERT_RUNNING_SHORT_ON_IP_MTG_SERVICING_NUMBERS";
	public static final String DEAL_DATA_GOLD_AT_APPROVAL = "Deal data (gold) at Approval";
	public static final String DEAL_DATA_GOLD_AT_COLLAPSE = "Deal data (gold) at Collapse";
	public static final String DEAL_DATA_GOLD_AT_DENIAL = "Deal data (gold) at Denial";
	public static final String DEAL_DATA_GOLD_AT_PRE_APPROVAL = "Deal data (gold) at PreApproval";
	public static final String APPROVED_AT = "Approved at ";
	public static final String COLLASPED_AT = "Collasped at ";
	public static final String DENIED_AT = "Denied at ";
	public static final String PRE_APPROVED_AT = "PreApproved at ";
	public static final String DEAL_RESOLUTION = "Deal Resolution";
	public static final String SERVICE_AVM_CONFIDENCE_NOT_AVAILABLE = "SERVICE_AVM_CONFIDENCE_NOT_AVAILABLE";	//#DG796
	public static final String MI_VALIDATION_CANCEL_ERROR_MSG = "MI_VALIDATION_CANCEL_ERROR_MSG";
	public static final String APPRAISER_THIS_ROW = "APPRAISER_THIS_ROW";
	public static final String APPRAISAL_DOCUMENT_FAILURE = "APPRAISAL_DOCUMENT_FAILURE";
	public static final String APPRAISAL_REVIEW_CANNOT_ENTER = "APPRAISAL_REVIEW_CANNOT_ENTER";
	public static final String REASSIGN_TASK_USERS_TYPE_NOT_MATCHED = "REASSIGN_TASK_USERS_TYPE_NOT_MATCHED";
	public static final String REASSIGN_TASK_INVALID_USER_TO = "REASSIGN_TASK_INVALID_USER_TO";
	public static final String REASSIGN_TASK_NOACTIVE_USER_FOUND = "REASSIGN_TASK_NOACTIVE_USER_FOUND";
	public static final String RELATED_UNEXPECTED_FAILURE = "TASK_RELATED_UNEXPECTED_FAILURE";
	public static final String TASK_REASSIGN_CURRENT_DEAL_LABEL = "TASK_REASSIGN_CURRENT_DEAL_LABEL";
	public static final String TASK_REASSIGN_CURRENT_TASK_LABEL = "TASK_REASSIGN_CURRENT_TASK_LABEL";
	public static final String NEW_TASK_ALERT_EMAIL_MESSAGE = "NEW_TASK_ALERT_EMAIL_MESSAGE";
	public static final String NEW_TASK_ALERT_EMAIL_SUBJECT = "NEW_TASK_ALERT_EMAIL_SUBJECT";
	public static final String REASSIGN_TASK_NO_USER_FROM = "REASSIGN_TASK_NO_USER_FROM";
	public static final String REASSIGN_TASK_DIFFERENT_USER_TO = "REASSIGN_TASK_DIFFERENT_USER_TO";
	public static final String REASSIGN_TASK_NO_USER_TO = "REASSIGN_TASK_NO_USER_TO";
	public static final String REASSIGN_TASK_NO_CURRENT_TASK = "REASSIGN_TASK_NO_CURRENT_TASK";
	public static final String DOC_CENTRAL_FOLDER_PRODUCED = "DOC_CENTRAL_FOLDER_PRODUCED";
	public static final String DISALLOWED_DURING_VIEW_ONLY_MODE = "DISALLOWED_DURING_VIEW_ONLY_MODE";
	//***************MCM Impl team changes Starts - XS_1.6 *******************/
	//Added a Constants for "Product code/rate match failure"
	public static final String PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG1 = "You have received this email notification because express ingestion failed to match one of the products or rate codes on an incoming deal with the existing product and rate inventory.";
	public static final String PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG2 = "Product Code and Rate Inventory Date match process has failed for the following deal:";
	public static final String PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG3 ="Please verify that the rate synchronization between express and point of sale is active and functions properly.";
	public static final String PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG4 ="An IC-reject notification has been sent to the broker. Please contact the broker ASAP and provide instruction on further action.";

	public static final String PRODUCT_CODE_OR_RATE_MATCH_FAILURE_MAIL_FORMAT_MSG5 ="Pricing record for product %s with effective date %s specified at the %s level does not exist in express.";

	public static final String REASON_FOR_FAILURE_PRODUCT = "Product";
	public static final String REASON_FOR_FAILURE_PRODUCT_RATE = "Product And Rate";
	public static final String REASON_FOR_FAILURE_RATE = "Rate";
	public static final String PRODUCT_CODE_OR_RATE_FAILURE_DEAL_LEVEL = "Deal";
	//***************MCM Impl team changes Ends - XS_1.6 *******************/
	//***************MCM Impl team changes Starts - XS_1.8 *****************/
	public static final String DUP_CHECK_COMPONENT_ADDED = "DupChekComponentAdded";
	public static final String DUP_CHECK_COMPONENT_DELETED = "DupChekComponentDeleted";
	public static final String DUP_CHECK_COMPONENT_CHANGED = "DupChekComponentChanged";
	public static final String DUP_CHECK_COMPONENT_END = "DupChekComponentEnd";
	public static final String DUP_CHEK_MORTGAGE_AMOUNT = "MORTGAGE_AMOUNT";
	public static final String DUP_CHEK_LINE_OF_CREDIT_AMOUNT = "LINE_OF_CREDIT_AMOUNT";
	public static final String DUP_CHEK_LOAN_AMOUNT = "LOAN_AMOUNT";
	public static final String DUP_CHEK_CREDIT_CARD_AMOUNT = "CREDIT_CARD_AMOUNT";
	public static final String DUP_CHEK_OVERDRAFT_AMOUNT = "OVERDRAFT_AMOUNT";
	public static final String PKL_COMPONENT_TYPE = "ComponentType";
	//***************MCM Impl team changes Starts - XS_1.8 *****************/

	  // 4.1GR -- configurable exchange STARTS
	public static final String EXCHANGE_VERSION = "com.filogix.exchange.version";
    // 4.1GR -- configurable exchange ENDS

	// 4.2 DA -- configurations for generic alert email start
	public static final String SIMPLE_ALERT_EMAIL_HOSTNAME = "com.filogix.express.email.simplealert.hostname";
	public static final String SIMPLE_ALERT_EMAIL_RECIPIENT = "com.filogix.express.email.simplealert.recipient";
	public static final String SIMPLE_ALERT_EMAIL_SENDER = "com.filogix.express.email.simplealert.sender";
	public static final String SIMPLE_ALERT_EMAIL_SUBJECT = "com.filogix.express.email.simplealert.subject";
	// 4.2 DA -- configurations for generic alert email end
    
    /***** FFATE START *****/
    public static final String DUP_CHEK_FINANCING_WAIVER = "FINANCINGWAIVER";
    public static final String DUP_CHEK_CREDIT_LIMIT = "CREDITLIMIT";
    public static final String DUP_CHEK_MATURITY_DATE = "MATURITYDATE";
    public static final String DUP_CHEK_CREDITBUREAURECORDINDICATOR = "CREDITBUREAURECORDINDICATOR";
    public static final String DUP_CHEK_PREFERREDCONTACTMETHODID = "PREFERREDCONTACTMETHODID";
    
    public static final String DUP_CHEK_BORROWERCELLPHONE = "BORROWERCELLPHONE"; 
    public static final String DUP_CHEK_SUFFIXID = "SUFFIXID";
    public static final String DUP_CHEK_CREDITBUREAUAUTHORIZATIONDATE = "CREDITBUREAUAUTHORIZATIONDATE";
    public static final String DUP_CHEK_CREDITBUREAUAUTHORIZATIONMETHOD = "CREDITBUREAUAUTHORIZATIONMETHOD";
    /***** FFATE END *****/
    
    // Media channel - manual, electronic
    public static final String CHANNEL_MEDIA_M = "M";
    public static final String CHANNEL_MEDIA_E = "E";

}
