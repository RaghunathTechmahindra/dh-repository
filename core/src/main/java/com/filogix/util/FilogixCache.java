package com.filogix.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * @author Filogix
 * Copyright 2009
 */
public final class FilogixCache {
	private static Log logger = LogFactory.getLog(FilogixCache.class);

	private CacheManager filogixCacheManager ;
	private static FilogixCache instance ;

	/**
	 * 
	 */
	private FilogixCache() {
		filogixCacheManager = CacheManager.getInstance();
	}

	/**
	 * @return
	 */
	public static FilogixCache getInstance() {
		if (null == instance) {
			synchronized (FilogixCache.class) {
				instance = new FilogixCache();
			}
		}
		return instance;
	}

	
	
	/**
	 * @param key
	 * @param value
	 * @param clazz
	 */
	public void put(Object key, Object value, Class clazz) {
			put(key,value,clazz.getName());
	}

	
	/**
	 * @param key
	 * @param value
	 * @param regionName
	 */
	public void putIntoNamedRegion(Object key, Object value, String regionName) {
		put(key,value,regionName);
	}
	
	/**
	 * @param key
	 * @param value
	 * @param cacheName
	 */
	public void put(Object key, Object value, String cacheName) {
		Cache cache = filogixCacheManager.getCache(cacheName);
		if (null == cache) {
			// cache not found so create a new one with this name

			try {
				filogixCacheManager.addCache(cacheName);
				logger.debug("New Cache created with name -->" + cacheName);
				cache = filogixCacheManager.getCache(cacheName);
			} catch (IllegalStateException i) {
				logger.error("Cache for class " + cacheName
						+ " not created due to following exception" + i);
			} catch (CacheException c) {
				logger.error("Cache for class " + cacheName
						+ " not created due to following exception" + c);
			}
		}

		// consider using the Element with time to live etc.
		Element element = new Element(key, value);
		logger.debug("Performing put into filogixCache for class --> " + cacheName);
		cache.put(element);
	}

	/**
	 * @param key
	 * @param value
	 * @param clazz
	 */
	public void put(int key, Object value, Class clazz) {
		put(new Integer(key), value, clazz);
	}

	/**
	 * @param key
	 * @param clazz
	 * @return
	 */
	public Object get(Object key, String regionName) {

		logger.debug("Performing get for class --> " + regionName);
		Cache cache = filogixCacheManager.getCache(regionName);
		if(cache == null) {
			return null;
		}
		Object value = null;
		Element element = cache.get(key);
		if (null != element) {
			 value = element.getObjectValue();
		}
		return value;
	}
	
	public Object getFromNamedRegion(Object key, String regionName){
		return get(key,regionName);
	}
	
	
	/**
	 * @param key
	 * @param clazz
	 * @return
	 */
	public Object get(int key, Class clazz) {
		return get(new Integer(key), clazz.getName());
	}
	
	/**
	 * @param key
	 * @param clazz
	 * @return
	 */
	public Object get(Object key, Class clazz) {
		return get(key, clazz.getName());
	}

}
