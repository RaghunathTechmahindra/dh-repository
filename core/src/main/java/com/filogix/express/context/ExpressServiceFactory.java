/*
 * @(#)ExpressServiceFactory.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.context;

/**
 * ExpressServiceFactory provides a centerlized entry point to get services
 * and/or beans for all Express applications and/or modules.  Those services and
 * beans may be managed by EJB container or some light weight micro kernel
 * framework such as Spring Framework.
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface ExpressServiceFactory {

    /**
     * returns an instance of service or bean
     *
     * @param name the name of the service.
     * @throws ExpressServiceException if failed to get
     * @return an instance of the service or bean.
     */
    public Object getService(String Name) throws ExpressServiceException;
}