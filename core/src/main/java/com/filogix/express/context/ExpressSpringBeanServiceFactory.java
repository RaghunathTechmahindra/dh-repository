/*
 * @(#)ExpressSpringBeanServiceFactory.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * ExpressSpringBeanServiceFactory 
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressSpringBeanServiceFactory
    extends ClassPathXmlApplicationContext implements ExpressServiceFactory {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressSpringBeanServiceFactory.class);

    /**
     * Constructor functions
     */
    public ExpressSpringBeanServiceFactory(String configLocation)
        throws BeansException {

        super(configLocation);
    }

    public ExpressSpringBeanServiceFactory(String[] configLocations)
        throws BeansException {

        super(configLocations);
    }

    public ExpressSpringBeanServiceFactory(String[] configLocations,
                                           ApplicationContext parent)
        throws BeansException {

        super(configLocations, parent);
    }

    public ExpressSpringBeanServiceFactory(String[] configLocations,
                                           boolean refresh)
        throws BeansException {

        super(configLocations, refresh);
    }

    public ExpressSpringBeanServiceFactory(String[] configLocations,
                                           boolean refresh,
                                           ApplicationContext parent)
        throws BeansException {

        super(configLocations, refresh, parent);
    }

    public ExpressSpringBeanServiceFactory(String[] paths, Class clazz)
        throws BeansException {

        super(paths, clazz);
    }

    public ExpressSpringBeanServiceFactory(String path, Class clazz)
        throws BeansException {

        super(path, clazz);
    }

    public ExpressSpringBeanServiceFactory(String[] paths, Class clazz,
                                           ApplicationContext parent)
        throws BeansException {

        super(paths, clazz, parent);
    }

    /**
     * returns the spring bean as a service in Express.
     */
    public Object getService(String name) throws ExpressServiceException {

        Object service;

        try {
            service = getBean(name);
        } catch (BeansException be) {
            _log.error("Failed to get Spring Bean: " + name, be);
            throw new ExpressServiceException("Failded to created service: " +
                                              name, be);
        }

        return service;
    }
}