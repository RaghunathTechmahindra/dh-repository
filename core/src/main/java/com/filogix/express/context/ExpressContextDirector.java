/*
 * @(#)ExpressContextDirector.java    2007-7-26
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ExpressContextDirector - 
 *
 * @version   1.0 2007-7-26
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class ExpressContextDirector {

    // the director
    private static ExpressContextDirector _director;

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressContextDirector.class);

    /**
     * returns the singleton instance of ExpressContextDirector.
     *
     * @return the singleton director.
     * 
     */
    synchronized public static ExpressContextDirector getDirector() {

        if (_director == null) {
            _log.info("Initializing an instance of ExpressContextDirector ...");
            _director = new ExpressSpringContextDirector();
        }

        return _director;
    }

    /**
     * try to find a suitable {@link ExpressServiceFactory} for the given factory key.
     *
     * @param factoryKey a key to the expected factory.
     * @return an instance of <code>ExpressServiceFactory</code>
     * @throws ExpressContextException if failed to get an instance.
     */
    public abstract ExpressServiceFactory getServiceFactory(String factoryKey)
        throws ExpressContextException;
}