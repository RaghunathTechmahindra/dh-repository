/*
 * @(#)ExpressContextException.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.context;

import com.filogix.express.core.ExpressRuntimeException;

/**
 * ExpressContextException - 
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressContextException extends ExpressRuntimeException {

    /**
     * Constructor function
     */
    public ExpressContextException() {
        super();
    }

    public ExpressContextException(String message) {
        super(message);
    }

    public ExpressContextException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpressContextException(Throwable cause) {
        super(cause);
    }
}