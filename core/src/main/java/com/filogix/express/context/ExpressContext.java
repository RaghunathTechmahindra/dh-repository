/*
 * @(#)ExpressContext.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.context;

/**
 * ExpressContext is just type interface for now and includes service name as
 * constants for easier accessing.
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface ExpressContext {

    /**
     * the service name for express datasource.
     */
    public static final String EXPRESS_DATASOURCE_KEY = "expressDataSource";

    /**
     * the service name for datasource connection factory.
     */
    public static final String DATASOURCE_CONNECTION_FACTORY_KEY =
        "dataSourceConnectionFactory";

    /**
     * service name for express connection helper.
     */
    public static final String EXPRESS_CONNECTION_HELPER_KEY =
        "expressConnectionHelper";

    /**
     * the service name for Express VPD State Mediator.
     */
    public static final String VPD_STATE_MEDIATOR_KEY =
        "expressVPDStateMediator";

    /**
     * the service name for ExpressState factory.
     */
    public static final String EXPRESS_STATE_FACTORY_KEY =
        "expressStateFactory";
}