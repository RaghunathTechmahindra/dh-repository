/*
 * @(#)ExpressServiceException.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.context;

import com.filogix.express.core.ExpressRuntimeException;

/**
 * ExpressServiceException - 
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressServiceException extends ExpressRuntimeException {

    /**
     * Constructor function
     */
    public ExpressServiceException() {
        super();
    }

    public ExpressServiceException(String message) {
        super(message);
    }

    public ExpressServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpressServiceException(Throwable cause) {
        super(cause);
    }
}