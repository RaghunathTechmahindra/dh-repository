/*
 * @(#)ExpressSpringContextDirector.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.beans.factory.access.SingletonBeanFactoryLocator;

/**
 * ExpressSpringContextDirector is the director to get a factory from the
 * context managed by Spring Framework.
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressSpringContextDirector extends ExpressContextDirector {

    // the context location for the SingletonBeanFactoryLocator.
    private static final String EXPRESS_SPRING_CONTEXT_LOCATION =
        "classpath*:com/filogix/express/**/express*Context.xml";

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressSpringContextDirector.class);

    /**
     * Constructor function
     */
    public ExpressSpringContextDirector() {
    }

    @Override
    public ExpressServiceFactory getServiceFactory(String factoryKey)
        throws ExpressContextException {

        ExpressServiceFactory factory = null;

        try {
            BeanFactoryLocator bfl = SingletonBeanFactoryLocator.
                getInstance(EXPRESS_SPRING_CONTEXT_LOCATION);
            BeanFactoryReference bf = bfl.useBeanFactory(factoryKey);
            factory = (ExpressServiceFactory) bf.getFactory();
        } catch (BeansException be) {
            _log.error("failed to get factory for: " + factoryKey, be);
            throw new ExpressContextException(be);
        }

        return factory;
    }
}