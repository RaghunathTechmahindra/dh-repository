/*
 * @(#)ExpressEmail.java    2009-7-13
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.email;

import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.util.Assert;

import com.basis100.deal.util.TypeConverter;
import com.basis100.resources.PropertiesCache;
import com.filogix.express.core.ExpressRuntimeException;

/**
 * implements IexpressEmail<br>
 * this class has two sendEmail methods.
 * 
 * 
 * all other programs should use email function through this class.
 * 
 * @version 1.0 initial July 11, 2009, Hiro
 */
public class ExpressEmail implements IExpressEmail, InitializingBean {

	private boolean html = true;
	private String messageTemplateFile;
	private String subjectTemplateFile;
	private VelocityEngine velocityEngine;
	private String emailPriority;
	private boolean debug;

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public void setEmailPriority(String emailPriority) {
		this.emailPriority = emailPriority;
	}

	public void setHtml(boolean html) {
		this.html = html;
	}

	public void setMessageTemplateFile(String messageTemplateFile) {
		this.messageTemplateFile = messageTemplateFile;
	}

	public void setSubjectTemplateFile(String subjectTemplateFile) {
		this.subjectTemplateFile = subjectTemplateFile;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	/**
	 * called by Spring
	 * make sure necessary fields are populated
	 */
	public void afterPropertiesSet() throws Exception {
		int intPriority = TypeConverter.intTypeFrom(this.emailPriority);
		Assert.isTrue(1 <= intPriority && intPriority <= 5);
	}

	/**
	 * returns text for the email subject.<br>
	 * this method can be overridden by sub classes.
	 * 
	 * @param info
	 * @param messageMap
	 * @return text for email subject
	 */
	protected String prepareSubject(EmailInfo info,
			Map<String, Object> messageMap) {

		Assert.notNull(velocityEngine);
		Assert.notNull(subjectTemplateFile);
		Assert.notNull(messageMap);

		return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				subjectTemplateFile, messageMap);
	}

	/**
	 * returns text for the email body message.<br>
	 * this method can be overridden by sub classes.
	 * 
	 * @param info
	 * @param messageMap
	 * @return text for email subject
	 */
	protected String prepareMessage(EmailInfo info,
			Map<String, Object> messageMap) {

		return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				messageTemplateFile, messageMap);
	}


	/**
	 * sends an email based on the parameter <tt>EmailInfo</tt>
	 */
	public void sendFreeFormEmail(EmailInfo info) {

		Assert.notNull(info);
		Assert.notNull(info.getMailSubject());
		Assert.notNull(info.getMailMessage());
		Assert.notNull(info.getMailAddressesTo());
		Assert.notNull(info.getMailAddressFrom());

		try {

			PropertiesCache prop = PropertiesCache.getInstance();
			String host = prop.getProperty(IExpressEmail.DEFAULT_INSTITUTION,
			"com.filogix.express.email.simplealert.hostname");
			Assert.notNull(host);
			Properties props = new Properties();
			props.put("mail.smtp.host", host);

			Session session = Session.getDefaultInstance(props, null);
			session.setDebug(this.debug);
			Message msg = new MimeMessage(session);
			for (String emailTo : ExpressEmailUtils.splitEmailAddresses(
					info.getMailAddressesTo())) {

				msg.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(emailTo, false));
			}
			msg.setFrom(new InternetAddress(info.getMailAddressFrom()));

			msg.setSubject(info.getMailSubject());


			msg.setHeader("X-Mailer", "Java Mail");
			msg.setHeader("X-Priority", emailPriority);
			if(this.html){
				msg.setContent(info.getMailMessage(), "text/html");	
			}else{
				msg.setText(info.getMailMessage());
			}

			msg.setSentDate(new Date());
			Transport.send(msg);

		} catch (AddressException e) {
			throw new ExpressRuntimeException("an email address is invalid ", e);
		} catch (MessagingException e) {
			throw new ExpressRuntimeException("faild to send an alert email ", e);
		}
	}

	/**
	 * sends an email using <tt>info.mailAddressesTo</tt> and <tt>info.mailAddressFrom</tt>.<br>
	 * email's subject and message are created using messageMap and templates set by Spring. 
	 */
	public void sendTemplateEmail(EmailInfo info, Map<String, Object> messageMap) {

		Assert.notNull(info);
		Assert.notNull(info.getMailAddressesTo());
		Assert.notNull(info.getMailAddressFrom());
		Assert.notNull(messageMap);//can not be null, empty is fine

		//info = ExpressEmailUtils.cloneEmailInfo(info);
		info.setMailSubject(prepareSubject(info, messageMap));
		info.setMailMessage(prepareMessage(info, messageMap));
		this.sendFreeFormEmail(info);
	}

}
