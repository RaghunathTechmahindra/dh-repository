/*
 * @(#)EmailInfo.java    2009-7-13
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.email;

/**
 * Parameters bean for Email the fields can be added when it's needed.
 * 
 * @version 1.0 initial July 11, 2009, Hiro
 */
public class EmailInfo {

	private String mailAddressesTo;
	private String mailAddressFrom;
	private String mailSubject;
	private String mailMessage;

	public String getMailAddressesTo() {
		return mailAddressesTo;
	}

	public void setMailAddressesTo(String mailAddressesTo) {
		this.mailAddressesTo = mailAddressesTo;
	}

	public String getMailAddressFrom() {
		return mailAddressFrom;
	}

	public void setMailAddressFrom(String mailAddressFrom) {
		this.mailAddressFrom = mailAddressFrom;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailMessage() {
		return mailMessage;
	}

	public void setMailMessage(String mailMessage) {
		this.mailMessage = mailMessage;
	}

}
