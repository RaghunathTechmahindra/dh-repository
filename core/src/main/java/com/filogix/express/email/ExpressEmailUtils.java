package com.filogix.express.email;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import com.filogix.express.core.ExpressRuntimeException;

public class ExpressEmailUtils {
	
	/**
	 * this method splits emails connected with "<b>,</b>", "<b>;</b>", or
	 * white space, and makes String's array.<br>
	 * for example: String "aaa@bbb.com; aax@bbb.com" becomes an array which
	 * contains "aaa@bbb.com" and "aax@bbb.com"
	 * 
	 * @param mailAddress
	 * @return Sting's array
	 */
	public static String[] splitEmailAddresses(String mailAddress) {
		return StringUtils.split(mailAddress,
				IExpressEmail.EMAIL_ADDRES_DELIMITER);
	}

	/**
	 * makes deep copied EmailInfo's clone object using BeanUtils
	 * 
	 * @param info -
	 *            original EmailInfo
	 * @return cloned EmailInfo
	 */
	public static EmailInfo cloneEmailInfo(EmailInfo info) {
		EmailInfo infoClone = new EmailInfo();
		try {
			BeanUtils.copyProperties(infoClone, info);
		} catch (Exception e) {
			throw new ExpressRuntimeException(
					"Exception happends in ExpressEmail.sendEmail", e);
		}
		return infoClone;
	}
}
