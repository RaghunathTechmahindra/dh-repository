/*
 * @(#)IExpressEmail.java    2009-7-13
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.email;

import java.util.Map;

public interface IExpressEmail {

	public static final String EMAIL_ADDRES_DELIMITER = ",; ";
	public static final int DEFAULT_INSTITUTION = -1;

	/**
	 * sends an email based on the parameter <tt>EmailInfo</tt>
	 */
	public void sendFreeFormEmail(EmailInfo info);

	/**
	 * sends an email using <tt>info.mailAddressesTo</tt> and <tt>info.mailAddressFrom</tt>.<br>
	 * email's subject and message are created using messageMap and templates set by Spring. 
	 */
	public void sendTemplateEmail(EmailInfo info, Map<String, Object> messageMap);

}
