/*
 * @(#)EmailSender.java    2009-7-13
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.express.email;

import static com.filogix.express.email.IExpressEmail.DEFAULT_INSTITUTION;

import java.util.HashMap;
import java.util.Map;

import com.basis100.resources.PropertiesCache;
import com.filogix.express.core.ExpressCoreContext;

/**
 * Delegate express email service.
 * 
 * @version 1.0 initial July 11, 2009, Hiro
 */
public class EmailSender {

	/**
	 * sends generic alert email.<br>
	 * this method uses the following properties in system property table so that all
	 * components can share the same configuration.<br>
	 * 
	 * email address from :
	 * <tt>"com.filogix.express.email.simplealert.sender"</tt><br>
	 * email address to :
	 * <tt>"com.filogix.express.email.simplealert.recipient"</tt><br>
	 * SMTP host IP: <tt>"com.filogix.express.email.simplealert.hostname"</tt><br>
	 * 
	 * all parameters of this method are optional. you can set null, if you
	 * don't need those fields.
	 * 
	 * @param SystemTypeName -
	 *            Program name which utilises this method. (e.g, "fxLink
	 *            Client")
	 * @param error -
	 *            Throwable object which you want to show to TA. you can set
	 *            null as well.
	 * @param optionalMessage -
	 *            you can set any message in String, if you want to add more
	 *            info. can be null.
	 * @param channelTypeName -
	 *            you can set channel Type name optionally. it can be null.
	 */
	public static void sendAlertEmail(String SystemTypeName, Throwable error,
			String channelTypeName, String optionalMessage) {

		PropertiesCache prop = PropertiesCache.getInstance();
		String emailFrom = prop.getProperty(DEFAULT_INSTITUTION,
				"com.filogix.express.email.simplealert.sender");
		String emailTo = prop.getProperty(DEFAULT_INSTITUTION,
				"com.filogix.express.email.simplealert.recipient");

		EmailInfo info = new EmailInfo();
		info.setMailAddressFrom(emailFrom);
		info.setMailAddressesTo(emailTo);

		Map<String, Object> messageMap = new HashMap<String, Object>();
		messageMap.put("systemTypeName", SystemTypeName);
		messageMap.put("channelTypeName", channelTypeName);
		messageMap.put("environmentName", PropertiesCache.getInstance()
				.getProperty(DEFAULT_INSTITUTION,
						"com.filogix.docprep.running.environment"));

		if (error != null) {
			Throwable t = error;
			StringBuilder sb = new StringBuilder();
			sb.append(t.getMessage());
			while ((t = t.getCause()) != null) {
				sb.append(" -- cause: ");
				sb.append(t.getMessage());
			}
			messageMap.put("error", sb.toString());
		}
		
		messageMap.put("optionalMessage", optionalMessage);

		IExpressEmail alertMail = (IExpressEmail) ExpressCoreContext
				.getService("genericAlertTemplateEmail");
		alertMail.sendTemplateEmail(info, messageMap);
	}

	public static void sendAlertEmail(String SystemTypeName, Throwable error,
			String channelTypeName) {
		sendAlertEmail(SystemTypeName, error, channelTypeName, null);
	}

	public static void sendAlertEmail(String SystemTypeName, Throwable error) {
		sendAlertEmail(SystemTypeName, error, null, null);
	}
	
	/**
	 * sends free format email.
	 * 
	 * @param to -
	 *            email address. it can be multiple email addresses like
	 *            "aa@b.com, bb@c.com". the delimiter is defined in
	 *            <tt>IExpressEmail.EMAIL_ADDRES_DELIMITER</tt>
	 * @param from -
	 *            email address.
	 * @param subject -
	 *            email's subject
	 * @param message -
	 *            email's body message
	 */
	public static void sendFreeFormatSimpleEmail(String to, String from,
			String subject, String message) {

		EmailInfo info = new EmailInfo();

		info.setMailAddressesTo(to);
		info.setMailAddressFrom(from);
		info.setMailSubject(subject);
		info.setMailMessage(message);

		IExpressEmail freeFormatEmail = (IExpressEmail) ExpressCoreContext
				.getService("freeFormatEmail");
		freeFormatEmail.sendFreeFormEmail(info);

	}

}
