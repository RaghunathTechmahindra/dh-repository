/*
 * @(#)ISavedPagesModel.java    2007-7-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.legacy.mosapp;

import mosApp.MosSystem.PageEntry;

/**
 * ISavedPagesModel - 
 *
 * @version   1.0 2007-7-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface ISavedPagesModel {

	public void setNextPage(PageEntry page);
}