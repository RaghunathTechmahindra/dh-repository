/*
 * @(#)ISourceFirmReviewHandler.java    2007-7-14
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.legacy.mosapp;

/**
 * ISourceFirmReviewHandler - 
 *
 * @version   1.0 2007-7-14
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface ISourceFirmReviewHandler {

    public static final String SFIRM_ID_PASSED_PARM="sourceFirmIdPassed";
}