/*
 * @(#)ISessionStateModel.java    2007-7-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.legacy.mosapp;

/**
 * ISessionStateModel the interface to access SessionStateModel in mosapp
 * module.
 *
 * @version   1.0 2007-7-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface ISessionStateModel {

    /**
     * returns language id.
     */
    public int getLanguageId();

    /**
     * returns the session user id.
     */
    public int getSessionUserId();

    /**
     * returns the session user type id.
     */
    public int getSessionUserType();
}