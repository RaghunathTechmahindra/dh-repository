/*
 * @(#)FleetPageHandlerCommon.java    2007-7-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.legacy.mosapp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFEExternalTaskCompletion;
import mosApp.MosSystem.PageEntry;

/**
 * FakePageHandlerCommon is the fake implementation of IPageHandlerCommon for
 * backend end use only.
 *
 * @version   1.0 2007-7-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class FleetPageHandlerCommon implements IPageHandlerCommon {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(FleetPageHandlerCommon.class);

    /**
     * Constructor function
     */
    public FleetPageHandlerCommon() {
    }

    /**
     * the srk
     */
    private SessionResourceKit _sessionResourceKit;

    /**
     * Get the value of the srk
     */
    public SessionResourceKit getSessionResourceKit() {
        return _sessionResourceKit;
    }

    /**
     * Set the value of the srk
     */
    public void setSessionResourceKit(SessionResourceKit sessionResourceKit) {
        _sessionResourceKit = sessionResourceKit;
    }

    /**
     * returns null for this backend one.
     */
    public ISavedPagesModel getSavedPages() {

        return null;
    }

    /**
     */
    public boolean standardAdoptTxCopy(PageEntry pe, boolean mainPageSubmit) {

        return true;
    }

    /**
     */
    public void workflowTrigger(int type, SessionResourceKit srk,
                                PageEntry pg, WFEExternalTaskCompletion extObj)
        throws Exception {
    }
}