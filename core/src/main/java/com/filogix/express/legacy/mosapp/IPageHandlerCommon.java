/*
 * @(#)IPageHandlerCommon.java    2007-7-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.legacy.mosapp;

import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFEExternalTaskCompletion;

import mosApp.MosSystem.PageEntry;

/**
 * IPageHandlerCommon is the interface to access PageHandlerCommon in mosapp
 * module.
 *
 * @version   1.0 2007-7-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface IPageHandlerCommon {

    /**
     * Get the value of the srk
     */
    public SessionResourceKit getSessionResourceKit();

    /**
     * Set the value of the srk
     */
    public void setSessionResourceKit(SessionResourceKit sessionResourceKit);

    /**
     * returns the savedpagesmodel.
     */
    public ISavedPagesModel getSavedPages();

    /**
     */
    public boolean standardAdoptTxCopy(PageEntry pe, boolean mainPageSubmit)
        throws Exception;

    /**
     */
    public void workflowTrigger(int type, SessionResourceKit srk,
                                PageEntry pg, WFEExternalTaskCompletion extObj)
        throws Exception;
}