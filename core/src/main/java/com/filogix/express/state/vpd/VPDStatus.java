/*
 * @(#)VPDStatus.java    2007-8-10
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

/**
 * VPDStatus represents the current vpd status.
 *
 * @version   1.0 2007-8-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface VPDStatus {

    /**
     * facility method to return the VPD store procedure callable statement.
     */
    public String getVPDStatement( String _vpdSchName);

    /**
     * returns the input values for the VPD store precedure callable statement
     * as array
     */
    public int[] getInputValues();
}