/*
 * @(#)VPDVoidStatus.java    2007-8-10
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * VPDVoidStatus - 
 *
 * @version   1.0 2007-8-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class VPDVoidStatus extends AbstractVPDStatus {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(VPDVoidStatus.class);

    /**
     * Constructor function
     */
    public VPDVoidStatus() {
    }

    /**
     * there is no parameter for this status.
     */
    protected String prepareParameters() {

        return "";
    }

    /**
     * no input values for this status.
     */
    public int[] getInputValues() {

//        return null;
    	return new int[]{};
    }
}