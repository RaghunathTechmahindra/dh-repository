/*
 * @(#)ExpressStateFactory.java    2007-8-15
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state;

/**
 * ExpressStateFactory defines the interface to get a {@link ExpressState}
 * instance.
 *
 * @version   1.0 2007-8-15
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface ExpressStateFactory {

    /**
     * return an instance of ExpressState.
     */
    public ExpressState getExpressState();
}