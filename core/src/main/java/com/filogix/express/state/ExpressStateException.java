/*
 * @(#)ExpressStateException.java    2007-8-10
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state;

import com.filogix.express.core.ExpressRuntimeException;

/**
 * ExpressStateException is a run time exception.
 *
 * @version   1.0 2007-8-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressStateException extends ExpressRuntimeException {

    /**
     * Constructor function
     */
    public ExpressStateException() {
    }

    public ExpressStateException(String message) {
        super(message);
    }

    public ExpressStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpressStateException(Throwable cause) {
        super(cause);
    }
}