/*
 * @(#)VPDStateAware.java    2007-8-10
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import com.filogix.express.state.ExpressStateException;

/**
 * VPDStateAware defines the interface for objects, who are care about the
 * Express VPD state, to update their behaviours.
 *
 * @version   1.0 2007-8-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface VPDStateAware {

    /**
     * the VPD state aware object show update to follow the VPD state change.
     */
    public void followChange(VPDStateDetectable newState)
        throws ExpressStateException;
}