/*
 * @(#)VPDStateDetectable.java    2007-8-10
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

/**
 * VPDStateDetectable 
 *
 * @version   1.0 2007-8-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface VPDStateDetectable {

    /**
     * attache a VPD state aware object.
     */
    public void attach(VPDStateAware awareness);

    /**
     * detach a VPD state aware object.
     */
    public void detach(VPDStateAware awareness);

    /**
     * notify the VPD status change
     */
    public void notifyChange();

    /**
     * returns current VPD status.
     */
    public VPDStatus getVPDStatus();
}