/*
 * @(#)VPDUserStatus.java    2007-8-10
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * VPDUserStatus represents the VPD store procedure for (userid, institutionid). 
 *
 * @version   1.0 2007-8-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class VPDUserStatus extends AbstractVPDStatus {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(VPDUserStatus.class);

    // the userprofile id
    protected int _userId;
    // the institutionprofile id.
    protected int _institutionId;

    /**
     * Constructor function
     */
    public VPDUserStatus() {
    }

    /**
     * the paramaters are 2
     */
    protected String prepareParameters() {

        return "?, ?, ?";
    }

    /**
     * returns userid and institution profile id as the input value.
     */
    public int[] getInputValues() {

        return new int[] {_userId, _institutionId};
    }

    /**
     * set the user profile id.
     */
    public void setUserId(int id) {

        _userId = id;
    }

    /**
     * set the institution profile id.
     */
    public void setInstitutionId(int id) {

        _institutionId = id;
    }
}