/*
 * @(#)AbstractVPDStatus.java    2007-8-10
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * AbstractVPDStatus - 
 *
 * @version   1.0 2007-8-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class AbstractVPDStatus implements VPDStatus, Serializable {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(AbstractVPDStatus.class);

    /**
     * returns the store procedure signature.
     */
    public String getVPDStatement(String vpdSchName) {

        StringBuffer vpdProcedure = new StringBuffer();
        vpdProcedure.append("{CALL ").
            append(vpdSchName).
            append(".VPD_SECURITY_CONTEXT.set_app_context (").
            append(prepareParameters()).
            append(")}");

        if (_log.isDebugEnabled())
            _log.debug("VPD Signature: " + vpdProcedure.toString());

        return vpdProcedure.toString();
    }

    /**
     * preparing the prarmeters for VPD store procedure based on different
     * status.   
     */
    protected abstract String prepareParameters();
}