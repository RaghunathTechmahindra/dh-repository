/*
 * @(#)ExpressState.java    2007-8-15
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.state.vpd.VPDStateDetectable;

/**
 * ExpressState is the facility class for 
 *
 * @version   1.0 2007-8-15
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public abstract class ExpressState
    implements IExpressState, VPDStateDetectable {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressState.class);

    /**
     * user profile id
     */
    protected int _userProfileId;

    /**
     * user insititution profile id
     */
    protected int _userInstitutionId;

    /**
     * Get the value of user profile id
     */
    public int getUserProfileId() {
        return _userProfileId;
    }

    /**
     * Get the value of user insititution profile id
     */
    public int getUserInstitutionId() {
        return _userInstitutionId;
    }

    /**
     * Set the value of user insititution profile id
     */
    public void setUserIds(int userProfileId, int userInstitutionId) {

        _userProfileId = userProfileId;
        _userInstitutionId = userInstitutionId;
    }

    /**
     * deal's ids
     */
    protected int _dealInstitutionId = -1;
    protected int _dealId = -1;
    protected int _dealCopyId = -1;

    /**
     * Get the value of deal's institution profile id
     */
    public int getDealInstitutionId() {
        return _dealInstitutionId;
    }

    /**
     * {@inheritDoc}
     */
    public int getDealId() {
        return _dealId;
    }

    /**
     * {@inheritDoc}
     */
    public int getDealCopyId() {
        return _dealCopyId;
    }

    /**
     * Set the value of deal's institution profile id
     */
    public void setDealIds(int dealId, int dealInstitutionId, int copyId) {

        _dealId = dealId;
        _dealInstitutionId = dealInstitutionId;
        _dealCopyId = copyId;
    }

    /**
     * {@inheritDoc}
     */
    public void setDealInstitutionId(int dealInstitutionId) {

        setDealIds(0, dealInstitutionId, 0);
    }

    /**
     * {@inheritDoc}
     */
    public void cleanDealIds() {

        _dealId = 0;
        // changed for business changing 
        // InstitutionProfileId starts with 0
        // jan 15, 2008 - Midori
        _dealInstitutionId = -1;
        _dealCopyId = 0;
    }

    /**
     * {@inheritDoc}
     */
    public void cleanAllIds() {

        _userProfileId = 0;
        // changed for business changing 
        // InstitutionProfileId starts with 0
        // jan 15, 2008 - Midori
        _userInstitutionId = -1;
        cleanDealIds();
    }
}