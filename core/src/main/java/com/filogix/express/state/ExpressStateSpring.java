/*
 * @(#)ExpressStateSpring.java    2007-8-15
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.state.vpd.VPDStatus;
import com.filogix.express.state.vpd.ExpressVPDStatus;
import com.filogix.express.state.vpd.VPDStateChangeMediator;
import com.filogix.express.state.vpd.VPDStateAware;

/**
 * ExpressStateSpring is the default implementation for {@link ExpressState}.
 * It is also implemented as Spring bean, which can be injected dependencies
 * throught a XML configuration file.
 *
 * @version   1.0 2007-8-15
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressStateSpring extends ExpressState
    implements Serializable {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressStateSpring.class);

    // we need maintain a VPD status to make it VPD state detectabel.
    protected ExpressVPDStatus _vpdStatus = new ExpressVPDStatus();

    // the VPD State mediator.
    protected VPDStateChangeMediator _vpdMediator;

    /**
     * Constructor function
     */
    public ExpressStateSpring() {
    }

    /**
     * inject the VPD state change mediator.
     */
    public void setExpressVPDStateMediator(VPDStateChangeMediator mediator) {

        _vpdMediator = mediator;
    }

    /**
     * {@inthertedDoc}
     */
    public void attach(VPDStateAware awareness) {

        _vpdMediator.register(this, awareness);
    }

    /**
     * {@inheritedDoc}
     */
    public void detach(VPDStateAware awareness) {

        _vpdMediator.unregister(this, awareness);
    }

    /**
     * notify the VPD status change.
     */
    public void notifyChange() {

        _vpdMediator.notifyChange(this);
    }

    /**
     * returns the current VPD Status.
     */
    public VPDStatus getVPDStatus() {

        return _vpdStatus.getCurrentStatus();
    }

    /**
     * update VPD status and notify state aware objects.
     */
    private void updateVPDStatus(int... ids) {
        if (ids.length >= 1) {
            _vpdStatus.resetStatus();
            notifyChange();
        }
        _vpdStatus.setStatus(ids);
        notifyChange();
    }

    /**
     * set user ids and also update VPD status.
     */
    @Override
    public void setUserIds(int userProfileId, int userInstitutionId) {

        super.setUserIds(userProfileId, userInstitutionId);
        // update VPD status.
        updateVPDStatus(userProfileId, userInstitutionId);
    }

    /**
     * set deal ids and update VPD status.
     */
    @Override
    public void setDealIds(int dealId, int dealInstitutionId, int copyId) {

        super.setDealIds(dealId, dealInstitutionId, copyId);
        // update VPD status.
        updateVPDStatus(dealInstitutionId);
    }

    /**
     * clean deal ids and update VPD Status.
     */
    @Override
    public void cleanDealIds() {

        if (_log.isDebugEnabled())
            _log.debug("Cleaning deal ids from current Express state.");

        _dealId = 0;
        _dealInstitutionId = -1;
        _dealCopyId = 0;
        // update VPD status.
        if (_userProfileId > 0) {
            if (_log.isDebugEnabled())
                _log.debug("There is a user working on this state! " +
                           "Updating VPD to user [" + _userProfileId +
                           ", " + _userInstitutionId + "]");
            updateVPDStatus(_userProfileId, _userInstitutionId);
        } else {
            if (_log.isDebugEnabled())
                _log.debug("No user working on this state! Cleaning ...");
            // clean VPD status.
            updateVPDStatus();
        }
    }

    /**
     * clean all ids.
     */
    @Override
    public void cleanAllIds() {

        _userProfileId = 0;
        _userInstitutionId = 0;
        cleanDealIds();
    }
}