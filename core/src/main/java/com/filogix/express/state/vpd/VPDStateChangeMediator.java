/*
 * @(#)VPDStateChangeMediator.java    2007-8-10
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import com.filogix.express.state.ExpressStateException;

/**
 * VPDStateChangeMediator defines the interface to manage the VPD State change
 * in Express. It also maps {@link VPDStateDetectable} to its {@link
 * VPDStateAware} objects.
 *
 * @version   1.0 2007-8-10
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface VPDStateChangeMediator {

    /**
     * register a VPDStateAware object to a VPDState.
     *
     * @param state
     * @param stateAware
     */
    public void register(VPDStateDetectable state, VPDStateAware stateAware);

    /**
     * unregister a VPDStateAware object from a VPDState.
     *
     * @param stateAware
     * @param state
     */
    public void unregister(VPDStateDetectable state, VPDStateAware stateAware);

    /**
     * notify change to VPDStateAware objects registered to the given VPD state.
     *
     * @param state the given VPDState.
     */
    public void notifyChange(VPDStateDetectable state)
        throws ExpressStateException;
}