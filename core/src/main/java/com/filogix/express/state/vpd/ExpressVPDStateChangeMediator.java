/*
 * @(#)ExpressVPDStateChangeMediator.java    2007-8-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.collections.list.TreeList;

import com.filogix.express.datasource.ExpressVPDConnection;
import com.filogix.express.state.ExpressStateException;

/**
 * ExpressVPDStateChangeMediator the default implementation for
 * VPDStateChangeMediator.  It is a Singleton instance and managed by
 * ExpressContext.
 *
 * @version   1.0 2007-8-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 * 
 * @version 1.1 2009-01-09, FXP24042, MCM, fixed java.util.ConcurrentModificationException
 */
public class ExpressVPDStateChangeMediator
    implements VPDStateChangeMediator, Serializable {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressVPDStateChangeMediator.class);

    /**
     * Constructor function
     */
    public ExpressVPDStateChangeMediator() {
    }

    // the registration map.
    private Map<VPDStateDetectable, List> _registrationMap =
        Collections.synchronizedMap(new HashMap<VPDStateDetectable, List>());

    /**
     * retister the awareness to the state.
     */
    public void register(VPDStateDetectable state, VPDStateAware awareness) {

        if (_log.isDebugEnabled())
            _log.debug("Trying to register StateAware [" + awareness.toString() +
                       "] to State [" + state.toString() + "] ...");

        if (!_registrationMap.containsKey(state)) {
            List<VPDStateAware> values = new TreeList();
            _registrationMap.put(state, values);
        }

        List<VPDStateAware> theValues = _registrationMap.get(state);

        if (!theValues.contains(awareness))  {
        	synchronized (theValues) { // FXP24042, MCM Jan 9, 2009
        		theValues.add(awareness);
        	}
            if (_log.isDebugEnabled())
                _log.debug("Successfully Registered.");
        }
    }

    /**
     * unregister.
     */
    public void unregister(VPDStateDetectable state, VPDStateAware awareness) {

        if (_log.isDebugEnabled())
            _log.debug("Trying to unregister StateAware [" +
                       awareness.toString() +
                       "] from State [" + state.toString() + "] ...");

        if (!_registrationMap.containsKey(state)) {
            _log.debug("No awareness registered to this state: " +
                      state.toString());
            return;
        }

        List<VPDStateAware> theValues = _registrationMap.get(state);
        if (!theValues.contains(awareness)) {

            _log.warn("Awareness [" + awareness.toString() + "] is not " +
                      "registered to state [" + state.toString() + "]");
            return;
        }

        synchronized (theValues) { // FXP24042, MCM Jan 9, 2009
        	theValues.remove(theValues.indexOf(awareness));
        }
        if (_log.isDebugEnabled())
            _log.debug("Successfully Unregistered.");
    }

    /**
     * notify changes to all awareness of this state.
     */
    public void notifyChange(VPDStateDetectable state)
        throws ExpressStateException {

        if (!_registrationMap.containsKey(state)) {

            _log.debug("No awareness registered to this state: " +
                      state.toString());
            return;
        }

        List<VPDStateAware> theValues = _registrationMap.get(state);
        synchronized (theValues) { // FXP24042, MCM Jan 9, 2009
	        for (Iterator<VPDStateAware> one = theValues.iterator();
	             one.hasNext(); ) {
	
	            // force each awareness to follow the change.
	            //FXP23475 MCM, Nov 11, 2008 -- start
	            //one.next().followChange(state);
	            VPDStateAware awareness = one.next();
	            if(((ExpressVPDConnection)awareness).isClosed()){
	                _log.warn("The connection has been closed. the connection will be unregistered.");
	                one.remove();
	            }
	            awareness.followChange(state);
	            //FXP23475 MCM, Nov 11, 2008 -- end
	        }
        }
    }
}