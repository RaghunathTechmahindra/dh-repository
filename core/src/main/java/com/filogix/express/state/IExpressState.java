/*
 * @(#)IExpressState.java    2007-8-15
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state;

/**
 * IExpressState defines the interfaces to maintain Express state, such as
 * userprofileid, userinstitutionid, dealid, copyid, dealinstitutionid.
 *
 * @version   1.0 2007-8-15
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface IExpressState {

    /**
     * Set the value of user profile id and user institution profile id.
     */
    public void setUserIds(int userProfileId, int userInstitutionId);

    /**
     * Get the value of user profile id
     */
    public int getUserProfileId();

    /**
     * Get the value of user insititution profile id
     */
    public int getUserInstitutionId();

    /**
     * Get the value of deal's institution profile id
     */
    public int getDealInstitutionId();

    /**
     * get the deal id.
     */
    public int getDealId();

    /**
     * get the deal copy id.
     */
    public int getDealCopyId();

    /**
     * Set the value of deal's institution profile id
     */
    public void setDealIds(int dealId, int dealInstitutionId, int copyId);

    /**
     * set the institution profile id only, this method will implicitly clean
     * the dealid and copyid.
     */
    public void setDealInstitutionId(int dealInstitutionId);

    /**
     * clean all ids related to a deal, meaning that we are not work on a
     * specific deal.
     */
    public void cleanDealIds();

    /**
     * clean all ids.
     */
    public void cleanAllIds();
}