/*
 * @(#)ExpressVPDStatus.java    2007-8-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * ExpressVPDStatus the vpd status in Exprss.
 *
 * @version   1.0 2007-8-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressVPDStatus implements Serializable {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressVPDStatus.class);

    // the vpdstatus.
    private VPDVoidStatus _voidStatus = new VPDVoidStatus();
    private VPDUserStatus _userStatus;
    private VPDDealStatus _dealStatus;

    // the active vpd status, the default is void.
    private VPDStatus _currentStatus = _voidStatus;

    /**
     * Constructor function
     */
    public ExpressVPDStatus() {
    }

    /**
     * return the current status.
     */
    public VPDStatus getCurrentStatus() {

        return _currentStatus;
    }
    /**
     * reset status.
     */
    public void resetStatus() {
        _currentStatus = _voidStatus;
    }

    /**
     * set status.
     */
    public void setStatus(int... ids) {

        if (_log.isDebugEnabled())
            _log.debug("Trying to set Express VPD status to: " +
                       ids.toString());

        if (ids.length < 1) {
            // this is void status.
            _currentStatus = _voidStatus;
        } else if (ids.length < 2) {
            // this is deal status.
            if (null == _dealStatus) 
                _dealStatus = new VPDDealStatus();
            _dealStatus.setInstitutionId(ids[0]);

            _currentStatus = _dealStatus;
        } else if (ids.length < 3) {
            // this is user status.
            if (null == _userStatus)
                _userStatus = new VPDUserStatus();
            _userStatus.setUserId(ids[0]);
            _userStatus.setInstitutionId(ids[1]);

            _currentStatus = _userStatus;
        } else {

            _log.warn("Not supported status: " + ids.toString());
        }
    }
}