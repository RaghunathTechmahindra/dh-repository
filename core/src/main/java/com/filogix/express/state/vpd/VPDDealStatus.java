/*
 * @(#)VPDDealStatus.java    2007-8-11
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.state.vpd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * VPDDealStatus prepares the VPD store procedure for deal institution.
 *
 * @version   1.0 2007-8-11
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class VPDDealStatus extends AbstractVPDStatus {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(VPDDealStatus.class);

    // the deal institution profile id.
    protected int _institutionId;

    /**
     * Constructor function
     */
    public VPDDealStatus() {
    }

    /**
     * this case only has one prarmeter for the procedure.
     */
    public String prepareParameters() {

        return "?, ?";
    }

    /**
     * the input value only has the institution id.
     */
    public int[] getInputValues() {

        return new int[] {_institutionId};
    }

    /**
     * set the deal institution profile id.
     */
    public void setInstitutionId(int id) {

        _institutionId = id;
    }
}