/*
 * @(#)ExpressRuntimeException.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.core;

/**
 * ExpressRuntimeException is the general wrapper for any exception that may
 * occure during runtime processing in Express, which are not required to
 * catch.  This class also intens to bring us the posibility for future
 * extension.
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @see ExpressException
 */
public class ExpressRuntimeException extends RuntimeException {

    /**
     * consturct a express runtime exception withour any message.
     */
    public ExpressRuntimeException() {
        super();
    }

    public ExpressRuntimeException(String message) {
        super(message);
    }

    public ExpressRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpressRuntimeException(Throwable cause) {
        super(cause);
    }
}