/*
 * @(#)ExpressCoreContext.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.context.ExpressContext;
import com.filogix.express.context.ExpressServiceException;
import com.filogix.express.context.ExpressContextDirector;

/**
 * ExpressCoreContext is the helper class to get the services in core module.
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public abstract class ExpressCoreContext implements ExpressContext {

    // the key to get a service factory for core module.
    private static final String EXPRESS_CORE_CONTEXT_KEY =
        "com.filogix.express.core";

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressCoreContext.class);

    /**
     * return a services from express core context.
     */
    synchronized public static Object getService(String name)
        throws ExpressServiceException {

        return ExpressContextDirector.getDirector().
            getServiceFactory(EXPRESS_CORE_CONTEXT_KEY).
            getService(name);
    }
}