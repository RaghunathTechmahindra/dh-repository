package com.filogix.express.core;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.spring.properties.EncryptablePropertyPlaceholderConfigurer;
import org.jasypt.util.text.TextEncryptor;
import java.util.Properties;
import java.io.IOException;

/**
 * TODO: Incorrect usage/extension of PropertyPlaceholderConfigurer
 * Express inappropriately use/extend PropertyPlaceholderConfigurer as properties holder
 *
 */
public class ExpressSystemProperties extends EncryptablePropertyPlaceholderConfigurer {

	public ExpressSystemProperties(StringEncryptor stringEncryptor) {
		super(stringEncryptor);
	}

	public ExpressSystemProperties(TextEncryptor textEncryptor) {
		super(textEncryptor);
	}

	public Properties mergeProperties() throws IOException {
		return super.mergeProperties();
	}
}
