/*
 * @(#)ExpressException.java    2007-7-27
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.core;

/**
 * ExpressException will be used as the superclass for any exception happened in
 * Express that we should catch and handle.  The main purpose of this class is
 * try to bring a possibility of future extention.
 *
 * @version   1.0 2007-7-27
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @see ExpressRuntimeException
 */
public class ExpressException extends Exception {

    /**
     * Construct an express without any message.
     */
    public ExpressException() {
        super();
    }

    /**
     * construct an exception with message.
     */
    public ExpressException(String message) {
        super(message);
    }

    public ExpressException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpressException(Throwable cause) {
        super(cause);
    }
}