/*
 * @(#)ExpressDataSourceException.java    2007-8-8
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import com.filogix.express.core.ExpressRuntimeException;

/**
 * ExpressDataSourceException - 
 *
 * @version   1.0 2007-8-8
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressDataSourceException extends ExpressRuntimeException {

    /**
     * Constructor function
     */
    public ExpressDataSourceException() {
        super();
    }

    public ExpressDataSourceException(String message) {
        super(message);
    }

    public ExpressDataSourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExpressDataSourceException(Throwable cause) {
        super(cause);
    }
}