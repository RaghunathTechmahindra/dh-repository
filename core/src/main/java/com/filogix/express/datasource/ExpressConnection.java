/*
 * @(#)ExpressConnection.java    2007-8-8
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import java.lang.reflect.InvocationHandler;

import com.filogix.express.state.vpd.VPDStateAware;

/**
 * ExpressConnection generally works as a wrapper of JDBC Connection.
 *
 * @version   1.0 2007-8-8
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public abstract class ExpressConnection
	implements IExpressConnection, InvocationHandler, VPDStateAware {

}