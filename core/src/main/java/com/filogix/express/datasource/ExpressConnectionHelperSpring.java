/*
 * @(#)ExpressConnectionHelperSpring.java    2008-3-23
 *
 * Copyright (C) 2008 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.support.nativejdbc.NativeJdbcExtractor;

/**
 * ExpressConnectionHelperSpring is an implementation of
 * ExpressConnectionHelper, which is leveraged on Spring Framework.
 *
 * @version   1.0 2008-3-23
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public class ExpressConnectionHelperSpring implements ExpressConnectionHelper {

    // The logger
    private final static Logger _logger = 
        LoggerFactory.getLogger(ExpressConnectionHelperSpring.class);

    // the native jdbc extractor.
    private NativeJdbcExtractor _extractor;

    /**
     * Constructor function
     */
    public ExpressConnectionHelperSpring() {
    }

    /**
     * set an appropriate native jdbc extractor from the context xml file.
     */
    public void setNativeJdbcExtractor(NativeJdbcExtractor extractor) {

        _extractor = extractor;
    }

    /**
     * returns the native jdbc connection.
     */
    public Connection getNativeConnection(Connection conn) throws SQLException {

        return _extractor.getNativeConnection(conn);
    }
}