/*
 * @(#)DataSourceConnectionFactory.java    2007-8-9
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import java.sql.Connection;

import com.filogix.express.state.vpd.VPDStateDetectable;

/**
 * DataSourceConnectionFactory defines the interfae to get a JDBC {@link
 * Connection} from DataSource.
 *
 * @version   1.0 2007-8-9
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface DataSourceConnectionFactory {

    /**
     * returns an instance or JDBC Connection from DataSource.
     */
    public Connection getConnection() throws ExpressDataSourceException;

    /**
     */
    public Connection getConnection(VPDStateDetectable state)
        throws ExpressDataSourceException;
}