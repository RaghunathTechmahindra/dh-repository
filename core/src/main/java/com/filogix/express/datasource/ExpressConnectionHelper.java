/*
 * @(#)ExpressConnectionHelper.java    2008-3-23
 *
 * Copyright (C) 2008 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * ExpressConnectionHelper defines facility interfaces to access and control a
 * connection in Express.
 *
 * @version   1.0 2008-3-23
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface ExpressConnectionHelper {

    /**
     * returns the native jdbc connection from a wrapped connection which is got
     * from a connection pool.
     */
    public Connection getNativeConnection(Connection conn) throws SQLException;
}