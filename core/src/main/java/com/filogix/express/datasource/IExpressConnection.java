/*
 * @(#)IExpressConnection.java    2007-8-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import java.sql.Connection;
import java.sql.SQLException;

import com.filogix.express.state.ExpressStateException;
import com.filogix.express.state.vpd.VPDStateDetectable;

/**
 * IExpressConnection - 
 *
 * @version   1.0 2007-8-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public interface IExpressConnection {

    /**
     * set the underlying JDBC Connection.
     */
    public void setJdbcConnection(Connection connection) throws SQLException;

    /**
     * set the VPD state detectable instance.
     */
    public void setVPDState(VPDStateDetectable state)
        throws ExpressStateException;
}