/*
 * @(#)ExpressVPDConnection.java    2007-8-8
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.commons.dbcp.DelegatingConnection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.express.cache.IPropertiesReader;
import com.filogix.express.core.ExpressSystemProperties;
import com.filogix.express.state.ExpressStateException;
import com.filogix.express.state.vpd.VPDStateDetectable;
import com.filogix.express.state.vpd.VPDStatus;

/**
 * ExpressVPDConnection works as a smart reference to apply VPD context on the
 * provided JDBC {@link Connection}.
 *
 * @version   1.0 2007-8-8
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class ExpressVPDConnection extends ExpressConnection {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(ExpressVPDConnection.class);

    // the target connection.
    protected Connection _connection;
    // the vpd state.
    protected VPDStateDetectable _vpdState;
    // the data Schema name
    protected String _dataSchName;
    // the VPD Schema name
    protected String _vpdSchName;

    /**
     * Constructor function
     */
    public ExpressVPDConnection() {
    	_vpdSchName = null;
    	_dataSchName = null;
    }

    /**
     * create a VPD connection wrapping the provided JDBC {@link Connection}.
     * 
     * @param Connection connection
     */
    public ExpressVPDConnection(Connection connection) throws SQLException {

        setJdbcConnection(connection);
    }

    /**
     * create a VPD connection wrapping the provided JDBC connection and a VPD
     * state detectable instance.
     *
     * @param connection the JDBC connection.
     * @param state an VPD state detectable instance.
     */
    public ExpressVPDConnection(Connection connection,
                                VPDStateDetectable state)
        throws SQLException, ExpressStateException {

        setJdbcConnection(connection);
        setVPDState(state);
    }

    public String getVpdSchName()
    {
        return _vpdSchName;
    }

    public String getDataSchName()
    {
        return _dataSchName;
    }

    public void setVpdSchName(String vpdSchName)
    {
        this._vpdSchName = vpdSchName;
    }

    public void setDataSchName(String dataSchName)
    {
        this._dataSchName = dataSchName;
    }

    /**
     * {@inheritDoc}
     */
    public void setJdbcConnection(Connection connection) throws SQLException {

        _connection = connection;
        // set the timestamp format.
        setDateFormat("YYYY-MM-DD HH24:MI:SS");
        // disable auto commit.
        _connection.setAutoCommit(false);
    }

    /**
     */
    public void setVPDState(VPDStateDetectable state)
        throws ExpressStateException {

        _vpdState = state;
        _vpdState.attach(this);
        
        // update underlying JDBC connection.
        updateConnection(_vpdState.getVPDStatus());
    }

    /**
     * {@inheritDoc}
     */
    public Object invoke(Object proxy, Method method, Object[] args)
        throws Throwable {

        try {
            if (_log.isDebugEnabled())
                _log.debug("Before invoking method: " + method.getName());

            if (method.getName().equals("close")) {
                if (_log.isDebugEnabled())
                    _log.debug("Detaching from express vpd state ...");
                _vpdState.detach(this);
            }

            Object ret = method.invoke(_connection, args);

            if (_log.isDebugEnabled())
                _log.debug("After invoking method: " + method.getName());

            return ret;
        } catch (IllegalAccessException e) {
            _log.error(e);
            throw e;
        } catch (InvocationTargetException e) {
            _log.error("Error ocurred in invoker, rolling back any pending db transaction.",
                       e);
            try {
                if (_connection != null)
                    _connection.rollback();
            } catch (Exception et) {
                _log.error("Roll back failed! " + et.getMessage());
            }

            throw e.getTargetException();
        } finally {
            // the final checking...
        }
    }

    /**
     * following the VPD state's change.
     */
    public void followChange(VPDStateDetectable state)
        throws ExpressStateException {

        // TODO: assert it is the same state instance.

        VPDStatus newStatus = state.getVPDStatus();

        updateConnection(newStatus);
    }

    /**
     * update the underlying JDBC connection based on the given status.
     *
     * @param status the VPD status.
     */
    protected void updateConnection(VPDStatus newStatus)
        throws ExpressStateException {

        CallableStatement statement = null;
        try {
            //FXP23475 MCM, Nov 11, 2008 -- start
            if(_connection == null || _connection.isClosed()){
                _log.warn("The connection has been closed. this method won't be executed.");
                return;
            }
            //FXP23475 MCM, Nov 11, 2008 -- end
            statement = _connection.prepareCall(newStatus.getVPDStatement(_vpdSchName));

            int[] inputs = newStatus.getInputValues();
            if (null != inputs) 
                switch (inputs.length) {
                case 0:
                    // do nothing,
                    break;
                case 1:
                    // deal institution case.
                    statement.setInt(1, inputs[0]);
                    statement.setString(2, _dataSchName);
                    break;
                case 2:
                    // user institution.
                    statement.setInt(1, inputs[0]);
                    statement.setInt(2, inputs[1]);
                    statement.setString(3, _dataSchName);
                    break;
                default:
                    _log.error("No such status: " + newStatus.toString());
                    throw new ExpressStateException("No such status: " +
                                                    newStatus.toString());
                }

            statement.execute();
        } catch (SQLException se) {
            _log.error("Failed to follow change!", se);
            throw new ExpressStateException("Failed to follow Change!", se);
        } finally {
            if (null != statement) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    /**
     * set the date format for each connection.
     */
    protected void setDateFormat(String theFormat) throws SQLException {

        Statement statement = _connection.createStatement();
        String sql = "ALTER SESSION SET NLS_DATE_FORMAT='" + theFormat + "'";
        try {
            statement.execute(sql);
        } finally {
            if (null != statement)
                statement.close();
        }
    }

    /**
     * <P> isClosed </p>
     * 
     * MCM 26-Nov-08 added to fix FXP23475
     * @return true - connection is closed (or marked as closed in connection pooling framework)
     * @throws SQLException
     * @author MCM team
     */
    public boolean isClosed() {
        boolean result = true;
        try {
            result = _connection.isClosed();
        } catch (SQLException e) {
            //if exception occurs, regard the connection is closed
            _log.error("ignore", e);
        }
        return result;
    }
    
    /**
     * Returns the innermostdelegate connection for the connection.
     * @return
     */
    //4.4GR FXP27535
    public Connection getInnermostDelegate()	{
      	 return ((DelegatingConnection) _connection).getInnermostDelegate();
    }   
   
}