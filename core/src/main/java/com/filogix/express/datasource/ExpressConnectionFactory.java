/*
 * @(#)ExpressConnectionFactory.java    2007-8-8
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

/**
 * ExpressConnectionFactory defines the api to get a Express Connection from
 * Express DataSource.
 *
 * @version   1.0 2007-8-8
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public interface ExpressConnectionFactory {

    /**
     * returns an instance of Express Connection.
     */
    public ExpressConnection getExpressConnection();
}