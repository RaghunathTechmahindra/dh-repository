/*
 * @(#)DataSourceConnectionFactorySpring.java    2007-8-8
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.datasource;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbcp.DelegatingConnection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.mail.DeliveryException;
import com.filogix.express.state.vpd.VPDStateAware;
import com.filogix.express.state.vpd.VPDStateDetectable;
import com.filogix.express.state.vpd.VPDStateChangeMediator;

/**
 * DataSourceConnectionFactorySpring is a Spring implementation for {@link
 * DataSourceConnectionFactory}.
 *
 * @version   1.0 2007-8-8
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 * @since     3.3
 */
public class DataSourceConnectionFactorySpring
    implements DataSourceConnectionFactory {

    // The logger
    private final static Log _log = 
        LogFactory.getLog(DataSourceConnectionFactorySpring.class);

    // the datasource
    private DataSource _dataSource;
    // express connection factory.
    private ExpressConnectionFactory _expressConnectionFactory;

    /**
     * Constructor function
     */
    public DataSourceConnectionFactorySpring() {
    }

    /**
     * inject DataSource from the spring context.
     */
    public void setDataSource(DataSource dataSource) {

        _dataSource = dataSource;
    }

    /**
     * inject express connection factory through Spring Framework.
     */
    public void
        setExpressConnectionFactory(ExpressConnectionFactory theFactory) {

        _expressConnectionFactory = theFactory;
    }

    /**
     * returns a instance of JDBC Connection.
     */
    public Connection getConnection() throws ExpressDataSourceException {

        return createExpressConnection(null);
    }

    /**
     * returns an instance of JDBC connection, which listening to the VPD
     * state.
     */
    public Connection getConnection(VPDStateDetectable state)
        throws ExpressDataSourceException {

        return createExpressConnection(state);
    }

    /**
     * creates a express connection proxy for the JDBC connection;
     */
    protected Connection createExpressConnection(VPDStateDetectable state)
        throws ExpressDataSourceException {

        // preparing the proxy.
        Object theConnection = null;
        try {
            // get connection from datasource.
            if (_log.isDebugEnabled())
                _log.debug("Getting connection from datasource ...");
            Connection connection = _dataSource.getConnection();
            // create an instance of ExpressConnection.
            ExpressConnection expressConnection =
                _expressConnectionFactory.getExpressConnection();
            if (_log.isDebugEnabled()){
                if (_dataSource instanceof BasicDataSource) {
                    BasicDataSource bds = (BasicDataSource) _dataSource;
                    StringBuffer sb = new StringBuffer();
                    sb.append("Created new db connection: Active(curr/max) = ");
                    sb.append(bds.getNumActive()).append("/").append(bds.getMaxActive());
                    sb.append(", Idle(min/curr/max) = ");
                    sb.append(bds.getMinIdle()).append("/").append(bds.getNumIdle()).append("/").append(bds.getMaxIdle());
                    sb.append(", MaxWait = ").append(bds.getMaxWait());
                    sb.append(", connection = ").append(connection.getMetaData().getURL());
                    if (connection instanceof DelegatingConnection) {
                        DelegatingConnection dConn = (DelegatingConnection) connection;
                        sb.append(", DelegatingConnection = ").append(dConn.getInnermostDelegate());
                    }
                    _log.debug(sb.toString());
                }
            }
            
            expressConnection.setJdbcConnection(connection);
            if (null != state)
                expressConnection.setVPDState(state);

            theConnection =
                Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                                       new Class[] {
                                           Connection.class,
                                           IExpressConnection.class,
                                           VPDStateAware.class
                                       },
                                       expressConnection);
        } catch (SQLException se) {
            _log.error("Failed to create Express Connection! ", se);
            throw new ExpressDataSourceException("Failed to create Express Connection!",
                                                 se);
        }

        return (Connection) theConnection;
    }
}