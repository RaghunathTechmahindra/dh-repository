package com.filogix.express.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * SystemPropertyCacheUtil
 * 
 * @version 1.0 Oct 2, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class SystemPropertyCacheUtil {

    private static final String CACHE_LABEL_SYSPROPERTY       = "SYSTEM_PROPERTY";
    private static final String CACHE_LABEL_SYSPROPERTY_INDEX = "SYSTEM_PROPERTY_INDEX";

    private static final Log    _log                          = LogFactory
                                                                      .getLog(SystemPropertyCacheUtil.class);

    /**
     * Generate cache key
     * @param InstitutionProfileId
     * @param propertyName
     * @return
     */
    public static String generateCacheKey(int InstitutionProfileId,
            String propertyName) {

        StringBuffer sb = new StringBuffer();
        sb.append(CACHE_LABEL_SYSPROPERTY);
        sb.append("#");
        sb.append(String.valueOf(InstitutionProfileId));
        sb.append("#");
        sb.append(propertyName);

        _log.debug("generateCacheKey >> " + sb.toString());
        return sb.toString();
    }

    /**
     * Generate cache key index
     * @param InstitutionProfileId
     * @return
     */
    public static String generateCacheIndexKey(int InstitutionProfileId) {

        StringBuffer sb = new StringBuffer();
        sb.append(CACHE_LABEL_SYSPROPERTY_INDEX);
        sb.append("#");
        sb.append(String.valueOf(InstitutionProfileId));

        _log.debug("generateCacheIndexKey >> " + sb.toString());
        return sb.toString();
    }
}
