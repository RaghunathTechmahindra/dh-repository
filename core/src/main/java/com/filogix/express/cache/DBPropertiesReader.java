package com.filogix.express.cache;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.SysProperty;
import com.basis100.entity.FinderException;
import com.basis100.resources.SessionResourceKit;

/**
 * 
 * DBPropertiesReader
 * 
 * @version 1.0 Oct 2, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class DBPropertiesReader implements IPropertiesReader {

    private static final Log _log = LogFactory
                                          .getLog(DBPropertiesReader.class);

    public DBPropertiesReader() {
    }

    /**
     * get the properties from the Entity
     * 
     * @param targetSysType
     * @param institutionId
     * @return Properties object
     */
    public Properties getProperties(int targetSysType, int institutionId) {

        Properties properties = null;
        SessionResourceKit srk = new SessionResourceKit(
                "cacheDBPropertiesReader", true);
        try {

            SysProperty sysPropDao = new SysProperty(srk, targetSysType);
            properties = sysPropDao.getProperties(institutionId);

        } catch (FinderException e) {
            _log.warn("target data was not found systemtype = " + targetSysType
                    + ", institutionId = " + institutionId);
        } finally {
            srk.freeResources();
        }

        return properties;
    }
}
