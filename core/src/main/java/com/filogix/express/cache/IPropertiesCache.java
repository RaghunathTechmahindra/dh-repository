package com.filogix.express.cache;

/**
 * 
 * IPropertiesCache
 * 
 * @version 1.0 Oct 2, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public interface IPropertiesCache {
	
	/**
     * 
     * @param keyName
     * @return
     */
    public abstract String getProperty(String keyName);

    /**
     * 
     * @param institutionId
     * @param keyName
     * @return
     */
    public abstract String getProperty(int institutionId, String keyName);

    /**
     * 
     */
    public abstract void flush();

}