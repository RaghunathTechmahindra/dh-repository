/**
 * <p>@(#)ClientMessageCache.java Oct 10, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;
import com.basis100.util.message.ClientMessage;

public class ClientMessageCache implements InitializingBean, IMessageCache  {
    
    private static final Log  _log = LogFactory.getLog(ClientMessageCache.class);
    private Cache             cache;
    private static boolean loaded = false;
    
    /**
     * 
     * @param cache
     */
    public void setCache(Cache cache) {
        _log.info("setCache@ClientMessageCache");
        this.cache = cache;
    }

    public ClientMessage findMessage(int institutonProfileId, int id, int lang) 
    {
        if (!loaded) {
            try {
                load();
            }
            catch(Exception e) {
                _log.info("findMessage@ClientMessageCache - failed loading Client Messagage");
                return null;
            }
        }
        
        String key = "" + institutonProfileId +"#"+id+"#"+lang;
        _log.trace("findMessage:" + key);
        
        // Patch FXP22377 for test purpose,,,: Sep 8, 2008 STARTS
        if (cache.getSize() == 0 ) {
            try {
                load();
            } catch (Exception e ) {
                _log.error("Faild ClientMessageCache Reload");
            }
        }
        Element e = cache.get(key);
        String msg = "";
        
        if (e != null) {
            msg = (String)e.getValue();
        }
        
        ClientMessage message = new ClientMessage(id, lang, msg);
        // Patch FXP22377 for test purpose,,,: Sep 8, 2008 ENDS
        return message;
    }

    /**
     * 
     */
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(cache);
    }

    /**
     * Clear the cache
     */
    public void flush(){
        this.cache.removeAll();
    }

    // new loadmessage for ML
    public void load() throws Exception
    {
        SessionResourceKit srk = new SessionResourceKit("ClientMessageCache.class");
        try {
            JdbcExecutor jExec = srk.getJdbcExecutor();
            String sql = "SELECT count(*) FROM message";
            int key = jExec.execute(sql);
            int count = 0;
            for (; jExec.next(key); )
            {
              count = jExec.getInt(key, 1);
            }
            jExec.closeData(key);
            
            if (count == 0) return;

            sql = "SELECT * FROM message";
            key = jExec.execute(sql);
            for (; jExec.next(key); )
            {
                ClientMessage clientMessage = new ClientMessage();
                clientMessage.setMessageId( jExec.getInt(key, "MESSAGEID") );
                clientMessage.setLanguagePreferenceId( jExec.getInt(key, 
                                                                    "LANGUAGEPREFERENCEID"));
                clientMessage.setMessageText( jExec.getString(key, "MESSAGETEXT") );
                int institutionProfileId = jExec.getInt(key, "INSTITUTIONPROFILEID");
                Element message = new Element(""+institutionProfileId+"#"
                                              +clientMessage.getMessageId()+"#"
                                              +clientMessage.getLanguagePreferenceId(),
                                              clientMessage.getMessageText());
                cache.put(message);
            }
            jExec.closeData(key);

            }catch(Exception exc){
                throw exc;
        }
        srk = null;
        loaded = true;
    }

}
