package com.filogix.express.cache;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.basis100.picklist.BXResources;
import com.filogix.express.core.ExpressSystemProperties;

/**
 * 
 * SystemPropertyCache
 * 
 * @version 1.0 Oct 1, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public class SystemPropertyCache implements InitializingBean, IPropertiesCache {

    private static final Log  _log = LogFactory
                                           .getLog(SystemPropertyCache.class);

    private Cache             cache;

    private List<Integer>     targetSystemTypes;

    private IPropertiesReader propertiesReader;
    
    private Properties	externalProperties;
        
	/**
	 * @param externalProperties the externalProperties to set
	 */
	public void setExternalProperties(ExpressSystemProperties externalProperties) {
		try {
			this.externalProperties = externalProperties.mergeProperties();
		} catch (IOException ioe) {
			_log.error(ioe.getMessage());
		}
	}
	
	/**
     * 
     * @param propertiesReader
     */
    public void setPropertiesReader(IPropertiesReader propertiesReader) {
        _log.info("SystemPropertyCache - propertiesReader " + propertiesReader);
        this.propertiesReader = propertiesReader;
    }

    /**
     * 
     * @param cache
     */
    public void setCache(Cache cache) {
        _log.info("SystemPropertyCache - Cache " + cache);
        this.cache = cache;
    }

    // public void setTargetSystemTypes(List<Integer> targetSystemTypes) {
    // this.targetSystemTypes = targetSystemTypes;
    // }

    /**
     * 
     * @param targetSystemTypesCSV
     */
    public void setTargetSystemTypes(String targetSystemTypesCSV) {
        _log.info("System Types CSV: " + targetSystemTypesCSV);
        this.targetSystemTypes = new ArrayList<Integer>();
        StringTokenizer st = new StringTokenizer(targetSystemTypesCSV, ",");
        while (st.hasMoreTokens()) {
            String theToken = st.nextToken().trim();
            _log.info("Parsing token: " + theToken);
            this.targetSystemTypes.add(Integer.parseInt(theToken));
        }
    }

    /**
     * 
     */
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(cache);
        Assert.notNull(propertiesReader);
        Assert.notNull(externalProperties);
        Assert.notNull(targetSystemTypes);
        
    }

    private SystemPropertyCache() {
    }

    /**check if the institution is cached
     * 
     * @param InstitutionProfileId
     * @return
     */
    public boolean isCached(int InstitutionProfileId) {

        String idxKey = SystemPropertyCacheUtil
                .generateCacheIndexKey(InstitutionProfileId);
        return (this.cache.get(idxKey) != null);
    }

    /**
     * Load the cache
     * @param institutionId
     */
    public void load(int institutionId) {
        for (int sysTypeId : targetSystemTypes) {
            _log.debug("cache loading ...  sysTypeId = " + sysTypeId
                    + ", institutionId=" + institutionId);

            Properties properties = propertiesReader.getProperties(sysTypeId,
                    institutionId);

            if (properties == null)
                continue;

            // cache all values one by one
            for (Iterator it = properties.keySet().iterator(); it.hasNext();) {

                String propertyName = (String) it.next();
                String cacheKey = SystemPropertyCacheUtil.generateCacheKey(
                        institutionId, propertyName);

                Element element = new Element(cacheKey,
                        (Serializable) properties.getProperty(propertyName));
                cache.put(element);
            }
        }

        // cache index key
        cache.put(new Element(SystemPropertyCacheUtil
                .generateCacheIndexKey(institutionId), "cached"));

    }

    /**
     * get the property for the institution and the key
     * @param institutionId
     * @param keyName
     */
    public String getProperty(int institutionId, String keyName) {
         if (institutionId < 0)
         {
             institutionId = BXResources.getDefaultInstitutionId();
         }
         String cachekey = SystemPropertyCacheUtil.generateCacheKey(
                institutionId, keyName);

        Element element = cache.get(cachekey);
        if (element == null) {
            //Check if the institution ID is cached
            if (isCached(institutionId))
                return null; // already cached but no entry

            //load the entries for the cache
            load(institutionId);
            element = cache.get(cachekey);

            if (element == null) {
                _log.warn("property was not found: institutionId="
                        + institutionId + ", keyName=" + keyName);
                return null;
            }
        }

        return (String) element.getValue();
    }
    
    /**
     * get the property for the key
     * @param keyName
     */
    public String getProperty(String keyName) {
        String returnValue = null;
        
        returnValue = (String) externalProperties.get(keyName);

        return returnValue;
    }

    /**
     * Clear the cache
     */
    public void flush() {
        this.cache.removeAll();
    }
    
   

}
