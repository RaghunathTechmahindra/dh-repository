package com.filogix.express.cache;

import java.util.Properties;

/**
 * 
 * IPropertiesReader
 * 
 * @version 1.0 Oct 2, 2007
 * @author <A HREF="mailto:mohit.khurana@filogix.com">Mohit Khurana</A>
 */
public interface IPropertiesReader {

    /**
     * 
     * @param targetSysType
     * @param institutionId
     * @return
     */
    Properties getProperties(int targetSysType, int institutionId);
}
