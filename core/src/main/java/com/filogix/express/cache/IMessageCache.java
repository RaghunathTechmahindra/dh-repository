/**
 * <p>@(#)IMessageCache.java Oct 12, 2007</p>
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.filogix.express.cache;

import java.io.IOException;
import com.basis100.util.message.ClientMessage;

public interface IMessageCache {
    
    /**
     * 
     * @param institutionId
     * @param keyName
     * @return
     */
    public ClientMessage findMessage(int institutionId, int id, int lang);

    /**
     * 
     */
    public void flush() throws IOException;
    
    public void load() throws Exception;

}
