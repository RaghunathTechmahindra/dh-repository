package com.filogix.spring;

import javax.xml.namespace.QName; 
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.Stub;

import javax.xml.rpc.encoding.TypeMapping;
import javax.xml.rpc.encoding.TypeMappingRegistry;

import org.apache.axis.Constants;
import org.apache.axis.encoding.ser.BeanDeserializerFactory;
import org.apache.axis.encoding.ser.BeanSerializerFactory;
import org.springframework.remoting.jaxrpc.JaxRpcPortProxyFactoryBean;

import com.filogix.datx.framework.packaging.FXRequest;
import com.filogix.datx.framework.packaging.FXResponse;

public class ExpressJaxRpcPortProxyFactoryBean extends JaxRpcPortProxyFactoryBean {
	
	// DEFAULT_MESSAGE_TIMEOUT in org.apache.axis.Constants is set to 600000 ms.
	private Integer timeout = new Integer(Constants.DEFAULT_MESSAGE_TIMEOUT);
	private static final String TIMEOUT_PROPERTY_KEY = "axis.connection.timeout";
	
	/**
	 * Overriden method of JaxRpcPortProxyFactoryBean class.
	 */
	protected void preparePortStub(Stub stub) {
		stub._setProperty(TIMEOUT_PROPERTY_KEY, timeout);
		super.preparePortStub(stub);
	}
	
	/**
	 * Overriden method of JaxRpcPortProxyFactoryBean class.
	 */
	 protected void postProcessJaxRpcService( javax.xml.rpc.Service service ) {
		 TypeMappingRegistry registry = service.getTypeMappingRegistry();
	     TypeMapping mapping = registry.createTypeMapping();
	     registerBeanMapping(mapping, FXResponse.class, "FXResponse");
	     registerBeanMapping(mapping, FXRequest.class, "FXRequest");
	     registry.register("http://schemas.xmlsoap.org/soap/encoding/", mapping);
	 }
	 
	 /**
	 * Register all type mappings set by
	 */
	 protected void registerBeanMapping(TypeMapping mapping, Class type, String name) {
		 QName qName = new QName("http://packaging.framework.datx.filogix.com", name);
	     mapping.register(type, qName, new BeanSerializerFactory(type, qName), new BeanDeserializerFactory(type, qName));
	 }

	 /***
	 * Set the call timeout in milliseconds.
	 * @see Call#setTimeout(java.lang.Integer)
	 * @see Constants#DEFAULT_MESSAGE_TIMEOUT
	 */
	 public void setTimeout(int millis) {
	    this.timeout = new Integer(millis);
	 }
	 
	 
	 /**
	  * This is needed to be able to send simultanious requests to the WS.
	  */
	 protected boolean alwaysUseJaxRpcCall() {
			return true;
	 }
}
