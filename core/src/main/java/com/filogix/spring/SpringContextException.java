package com.filogix.spring;

public class SpringContextException extends Exception {

    public SpringContextException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public SpringContextException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public SpringContextException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public SpringContextException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
