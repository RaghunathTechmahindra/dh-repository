package com.filogix.spring.advice.error;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

/**
 * 
 * Created on Jul 28, 2006
 * @author LZhou
 *
 */
public class ErrorEmailNotificationAdvice implements ThrowsAdvice {
    private static Log logger = LogFactory.getLog(ErrorEmailNotificationAdvice.class);
    private MailSender mailSender;
    private SimpleMailMessage mailMessage;
    private static int counter = 0;
    private int frequence = 1;  // default value, bombing operations guys
    private int max = 10000; // default value
    
    public void setMax(int max) {
    	this.max = max;
    }
    
    public void setFrequence(int frequence) {
    	this.frequence = frequence;
    }
    
    public void setMailSender(MailSender mailSender) {
    	this.mailSender = mailSender;
    }
    
    public void setMailMessage(SimpleMailMessage mailMessage) {
    	this.mailMessage = mailMessage;
    }

    public void afterThrowing(Exception ex) throws Throwable {
        logger.info(this.getClass().getName() + ": " + ex.getMessage());
        
        if (ex.getMessage().indexOf("java.net.ConnectException") != -1) {

            System.out.println("connection failure.");
            if (counter++ % frequence == 0) {
                sendEmailNotification();
            }
            
            if (counter > max) {
            	counter = 0;
            }
            
            throw new Exception("datx connection failure");
        }
    }
    
    private void sendEmailNotification() {
    	try {
    		mailSender.send(mailMessage);
    	} catch (MailException e) {
    		System.out.println("Failed to send email notification");
    		logger.error(e.getMessage());
    	}
    	
    }
    
}

