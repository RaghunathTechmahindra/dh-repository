package com.filogix.spring.advice.error;

import org.springframework.aop.ThrowsAdvice;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

public class ErrorThrowsAdvice implements ThrowsAdvice {
    private static Log logger = LogFactory.getLog(ErrorThrowsAdvice.class);

    public void afterThrowing(Exception ex) throws Throwable {
        logger.info(this.getClass().getName() + ": " + ex.getMessage());
    }

}
