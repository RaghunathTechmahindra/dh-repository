package com.filogix.spring.advice.before;

import java.lang.reflect.Method;
import org.springframework.aop.MethodBeforeAdvice;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
//import com.basis100.resources.SessionResourceKit;
//import com.basis100.deal.entity.UserProfile;
//import com.filogix.express.services.MessageDescriptor;
//import com.filogix.express.services.WSSignInService;
//import com.filogix.express.services.ServiceException;

public class LogBeforeAdvice implements MethodBeforeAdvice {

    private static Log logger = LogFactory.getLog(LogBeforeAdvice.class);

    public void before(Method m, Object[] args, Object target) throws Throwable {
        // Log anything interested, printout for now
        logger.info(this.getClass().getName() + ": " + target.getClass().getName() + "::" + m.getName());
        System.out.print("LogAdvice: ");
        System.out.print(target.getClass().getName() + "::" + m.getName());
        System.out.print("(");

        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                System.out.print(args[i].getClass().getName());
                if (i < args.length - 1) {
                    System.out.print(", ");
                }
            }
        }

        System.out.println(")");
    }
}
