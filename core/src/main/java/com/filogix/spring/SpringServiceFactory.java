package com.filogix.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


/**
 * <p>Title: Filogix Express - AIG UG</p>
 *
 * <p>Description: Filogix AIGUG MI Project</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Filogix</p>
 *
 * @author Lin Zhou
 * @version 1.0
 */
public class SpringServiceFactory  {
    private ApplicationContext ctx;

    public static SpringServiceFactory getInstance(String
            ctxLocation) throws Exception {
        return new SpringServiceFactory(ctxLocation);
    }

    public static SpringServiceFactory getInstance(ApplicationContext ctx) 
             throws Exception {
        return new SpringServiceFactory(ctx);
    }
    
    private SpringServiceFactory(String ctxLocation) throws Exception {
        
        try {
            ctx = new FileSystemXmlApplicationContext(ctxLocation);
        } catch (Exception e) {
            ctx = new ClassPathXmlApplicationContext(ctxLocation);
        }
    }

    private SpringServiceFactory(ApplicationContext ctx) throws Exception {
    	this.ctx = ctx;
    }
    
    public Object getService(String id) throws Exception {
        return ctx.getBean(id);
    }
}
