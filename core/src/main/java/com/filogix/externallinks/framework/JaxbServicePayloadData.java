/**
 * <p>Title: JaxbServicePayloadData.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 16, 2006)
 *
 */

package com.filogix.externallinks.framework;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.PayloadType;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.ServiceProvider;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.PayloadTypePK;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.schema.datx.AddressType;
import com.filogix.schema.datx.BorrowerType;
import com.filogix.schema.datx.ContactInformationType;
import com.filogix.schema.datx.RequesterType;
import com.filogix.schema.datx.impl.AddressTypeImpl;
import com.filogix.schema.datx.impl.ContactInformationTypeImpl;
import com.filogix.schema.datx.impl.RequesterTypeImpl;

public abstract class JaxbServicePayloadData extends ServicePayloadData
{
    protected List jaxbBorrowers;
    protected AddressType  jaxbProperty;
    protected RequesterType jaxbRequester;
    protected ContactInformationType jaxbContactInfo;
    public static String YNFLAG_Y = "Y";
    public static String YNFLAG_N = "N";
    
    public static String CANADA = "Canada";
        
    public JaxbServicePayloadData(SessionResourceKit srk, int requestId, int borrowerId, int copyId, int langId) 
        throws RemoteException, ExternalServicesException
    {
        super(srk, requestId, borrowerId, copyId, langId);
        init();
    }

    public JaxbServicePayloadData(SessionResourceKit srk, int requestId, int copyId, int langId) 
        throws RemoteException, ExternalServicesException
    {
        super(srk, requestId, copyId, langId);
        init();
    }

    public void init()
        throws ExternalServicesException
    {
        
        setCopyId(getRequest().getCopyId());
        try
        {
            Borrower borrower = new Borrower(_srk, null);
            borrower = borrower.findByPrimaryBorrower(getDealId(), getCopyId());
            if (borrower != null)
                setBorrowerId(borrower.getBorrowerId());
        }
        catch (Exception e)
        {
            _logger.error(StringUtil.stack2string(e));
            throw new ExternalServicesException("Cannot instantiate ServiceRequestContact" );
        } 
        
        // 3) get ServiceContact - not require for OSC
        if (requireServiceRequestContact())
        {
        try
        {
            ServiceRequestContact serviceContact = new ServiceRequestContact(_srk);
            serviceContact = serviceContact.findLastByRequestId(getRequestId());
            if (serviceContact.getBorrowerId() > 0)
            {
                int borroweId = serviceContact.getBorrowerId();
                Borrower borrower = new Borrower(_srk,null);
                BorrowerPK bpk = new BorrowerPK(borroweId, 
                                                getRequest().getCopyId());
                borrower = borrower.findByPrimaryKey(bpk);
                serviceContact.setFirstName(borrower.getBorrowerFirstName());
                serviceContact.setLastName(borrower.getBorrowerLastName());
                String homePhoneNumber = borrower.getBorrowerHomePhoneNumber();
                if (homePhoneNumber != null && !homePhoneNumber.trim().equals(""))
                {
                    homePhoneNumber = homePhoneNumber.trim();
                    if ( homePhoneNumber.length()==10)
                    {
                        serviceContact.setHomeAreaCode(homePhoneNumber.substring(0,3));
                        serviceContact.setHomePhoneNumber(homePhoneNumber.substring(3,10));
                    }
//                    else if ()  // borrower home phone number is varchar2(14), may need to add scrub
                }
                String workPhoneNumber = borrower.getBorrowerWorkPhoneNumber();
                if (workPhoneNumber != null && !workPhoneNumber.trim().equals(""))
                {
                    workPhoneNumber = workPhoneNumber.trim();
                    if ( workPhoneNumber.length()==10)
                    {
                        serviceContact.setWorkAreaCode(workPhoneNumber.substring(0,3));
                        serviceContact.setWorkPhoneNumber(workPhoneNumber.substring(3,10));
                    }
//                    else if ()  // borrower home phone number is varchar2(14), may need to add scrub
                }
                String workExt = borrower.getBorrowerWorkPhoneExtension();
                if (workExt != null && !workExt.trim().equals(""))
                {
                    serviceContact.setWorkExtension(workExt.trim());
                }
                serviceContact.setCopyId(getRequest().getCopyId());
                serviceContact.setEmailAddress(borrower.getBorrowerEmailAddress());
            }

	            setServiceContact(serviceContact);
            if (serviceContact != null )
            {
	                setServiceRequestContactId(serviceContact.getServiceRequestContactId());
            }
        }
        catch (Exception e) {
            _logger.error(StringUtil.stack2string(e));
            throw new ExternalServicesException("Cannot instantiate ServiceRequestContact" );
        }  
        }
        // 4) get property
        try
        {
            Property property = new Property(_srk);
            
            // CR: this can be any property, not necessary the primary one
            property = property.findByPrimaryKey(new PropertyPK(this._propertyId, 
                                                                this._copyId));
            setProperty(property);
        }
        catch (Exception e) {
            _logger.error(StringUtil.stack2string(e));
            throw new ExternalServicesException("Cannot instantiate Property for requestId = " + _requestId + ", copyId = " + _copyId );
        }  
        
        // 5) getServiceProduct
        setServiceProductId(getRequest().getServiceProductId());
        try
        {
            setServiceProduct(new ServiceProduct(_srk, getServiceProductId()));
        }
        catch(Exception e)
        {
            _logger.error(StringUtil.stack2string(e));
            throw new ExternalServicesException("Cannot instantiate ServiceProduct for serviceProductuId = "  + getServiceProductId());
        }

        // 6) getServiceProvider
        try
        {
            setServiceProviderId(getServiceProduct().getServiceProviderId());
            setServiceProvider(new ServiceProvider(_srk, getServiceProviderId()));
        }
        catch(Exception e)
        {
            _logger.error(StringUtil.stack2string(e));
            throw new ExternalServicesException("Cannot instantiate ServiceProduct for id = "  + getServiceProviderId());
        }
        
        // 7) get User
        setUserId(getRequest().getUserProfileId());
        try
        {
            UserProfileBeanPK upk = new UserProfileBeanPK(getUserId(), _srk.getExpressState().getDealInstitutionId());
            UserProfile userProfile = new UserProfile(_srk);
            userProfile = userProfile.findByPrimaryKey(upk);
            setUserProfile(userProfile);
        } catch (Exception e) {
            _logger.error(StringUtil.stack2string(e));
            throw new ExternalServicesException("Cannot instantiate User for userid = " + getUserId());
        }
        // 7) payloadType
        setPayloadTypeId(getRequest().getPayloadTypeId());
        if (getPayloadTypeId() > -1 )
        {
            try
            {
                PayloadTypePK plk = new PayloadTypePK(getPayloadTypeId());
                PayloadType payloadType = new PayloadType(_srk);
                payloadType = payloadType.findByPrimaryKey(plk);
                setPayloadType(payloadType);
            } catch (Exception e) {
                _logger.error(StringUtil.stack2string(e));
                throw new ExternalServicesException("Cannot instantiate PayloadType for PayloadTypeId = " + getPayloadTypeId());
            }
        }
        
        try
        {
            jaxbBorrowers = new ArrayList();
            jaxbProperty = new AddressTypeImpl();
            jaxbContactInfo = new ContactInformationTypeImpl();
            jaxbRequester = new RequesterTypeImpl();
        }
        catch(Exception e)
        {
            _logger.error(StringUtil.stack2string(e));
            throw new ExternalServicesException("Cannot instantiate Request for requestId = " + getRequestId());
        }
    }
    
    public List getJaxbBorrower()
    {
        return jaxbBorrowers;        
    }
    
    public void addJaxbBorrower(BorrowerType borrower)
    {
        jaxbBorrowers.add(borrower);        
    }


    public AddressType getJaxbProperty()
    {
        return jaxbProperty;
    }

    public void setJaxbProperty(AddressType jaxbProperty)
    {
        this.jaxbProperty = jaxbProperty;
    }

    public RequesterType getJaxbRequester()
    {
        return jaxbRequester;
    }

    public void setJaxbRequester(RequesterType jaxbRequester)
    {
        this.jaxbRequester = jaxbRequester;
    }

    public abstract Object getServicePayload();

    public ContactInformationType getJaxbContactInfo()
    {
        return jaxbContactInfo;
    }

    public void setJaxbContactInfo(ContactInformationType jaxbContactInfo)
    {
        this.jaxbContactInfo = jaxbContactInfo;
    }
    
    protected boolean empty(String str)
    {
    	return str == null || "".equals(str.trim());
    }
    
    protected boolean requireServiceRequestContact()
    {
    	return true;
    }
}
