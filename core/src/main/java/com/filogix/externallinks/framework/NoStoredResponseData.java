/**
 * <p>Title: NoStoredResponseData.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Dec 13, 2006)
 *
 */
package com.filogix.externallinks.framework;

import javax.xml.bind.JAXBContext;

public abstract class NoStoredResponseData extends NoStoredTransactionData
{
  protected Object _responseBean;
  protected Object[] _payloads;
  protected int _resultCode;
  protected String _resultDesc;
  private JAXBContext _payloadSchemaJaxbContext = null;

  /**
   * <p>NoStoredResponseData</p>
   * Constructor
   */
  public NoStoredResponseData()
  {
    super();
  }

  public Object[] getPayloads()
  {
    return _payloads;
  }


  public Object getResponseBean()
  {
    return _responseBean;
  }


  public int getResultCode()
  {
    return _resultCode;
  }

  public void setResultCode(int code)
  {
    _resultCode = code;
  }

  public String getResultDesc()
  {
    return _resultDesc;
  }

  public void setResultDesc(String desc)
  {
    _resultDesc = desc;
  }

  public void setResponseBean(Object bean)
    throws ExternalServicesException
  {
    _responseBean = bean;
  }
  
  public JAXBContext getPayloadSchemaJaxbContext()
  {
    return _payloadSchemaJaxbContext;
  }

  public void setPayloadSchemaJaxbContext(JAXBContext payloadSchemaJaxbContext)
  {
    this._payloadSchemaJaxbContext = payloadSchemaJaxbContext;
  }


}
