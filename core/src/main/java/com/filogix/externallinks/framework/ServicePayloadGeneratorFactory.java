package com.filogix.externallinks.framework;

import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.filogix.externallinks.services.datx.DatxAppReqPayloadGenerator;
import com.filogix.externallinks.services.datx.DatxAvmPayloadGenerator;
import com.filogix.externallinks.services.datx.DatxOscPayloadGenerator;
import com.filogix.externallinks.services.datx.DatxServiceCancelPayloadGenerator;

public class ServicePayloadGeneratorFactory implements ServiceConst {
  private SysLogger _logger;
  private static ServicePayloadGeneratorFactory _factory = new ServicePayloadGeneratorFactory();
  
  public static synchronized ServicePayloadGeneratorFactory getInstance(){
    return _factory;
  }
  
  private ServicePayloadGeneratorFactory(){
    _logger = ResourceManager.getSysLogger( "SvcPldGFct" );
    _logger.debug("Started");
  }
  
  /**
   * 
   * @param serviceRequestTypeId
   * @return
   */ 
  // TODO : Catherine: implement DRUles or Spring later
  public ServicePayloadGeneratorBase getGenerator(String requestTypeStr){
    
    ServicePayloadGeneratorBase _generator = null;
    
    if (requestTypeStr.equals(REQUEST_TYPE_AVM_STR)) {
        _generator = new DatxAvmPayloadGenerator();
        _logger.debug("generator = " + _generator);
      }
    else if(requestTypeStr.equals(REQUEST_TYPE_CLOSING_FIXED_REQUEST_STR)
    		|| requestTypeStr.equals(REQUEST_TYPE_CLOSING_OSC_REQUEST_STR)) {
        _generator = new DatxOscPayloadGenerator();
        _logger.debug("generator = " + _generator);
      }
    else if (requestTypeStr.equals(REQUEST_TYPE_APPRAISAL_REQUEST_STR)) {
        _generator = new DatxAppReqPayloadGenerator();
        _logger.debug("generator = " + _generator);
    }
    else if (requestTypeStr.equals(REQUEST_TYPE_APPRAISAL_CANCEL_STR)
    		|| requestTypeStr.equals(REQUEST_TYPE_CLOSING_OSC_CANCEL_STR)) {
        _generator = new DatxServiceCancelPayloadGenerator();
        _logger.debug("generator = " + _generator);
      }
    
    return _generator;
  }

  public ServicePayloadGeneratorBase getGenerator(int requestTypeId){
    
    ServicePayloadGeneratorBase _generator = null;
      
     switch (requestTypeId) {
      case REQUEST_TYPE_AVM_A_INT:
      case REQUEST_TYPE_AVM_B_INT:
      case REQUEST_TYPE_AVM_C_INT:  
      {
        _generator = new DatxAvmPayloadGenerator();
        break;
      }
      case REQUEST_TYPE_CLOSING_FIXED_REQUEST_INT:
      case REQUEST_TYPE_CLOSING_OUTSOURCED_REQUEST_INT:
      {
    	  _generator = new DatxOscPayloadGenerator();
          break;
      }
      case REQUEST_TYPE_APPRAISAL_FULL_REQUEST_INT: {
          _generator = new DatxAppReqPayloadGenerator();
        break;
      }  
      case REQUEST_TYPE_APPRAISAL_FULL_CANCEL_INT: {
          _generator = new DatxServiceCancelPayloadGenerator();
        break;
      }
      default: {
        break;
      }
    }
    return _generator;    
  }
}
