
package com.filogix.externallinks.framework;

public class ExternalServicesException extends Exception {

    public ExternalServicesException() {

        super();
    }

    public ExternalServicesException(String msg){

        super(msg);
    }

    public ExternalServicesException(String msg, Exception e) {

        super(msg, e);
    }

    public ExternalServicesException(Exception e) {

        super(e);
    }


}
