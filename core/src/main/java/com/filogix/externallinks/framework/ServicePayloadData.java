package com.filogix.externallinks.framework;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import java.util.ArrayList;
import java.util.Collection;
import com.basis100.deal.entity.DealPurpose;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.PayloadType;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.ServiceProvider;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

/**
 * @author CRutgaizer <br>
 *         This class hold data specific to service request. <br>
 *         <p>
 *         For Valuation services the following attributes might be set:
 *         <li> propertyID, </li>
 *         <li> borrowerID, </li>
 *         <li> serviceRequestContactID </li>
 *         <br>
 *         <p>
 */

public class ServicePayloadData {
  // ids
  protected int                   _dealId;
  protected int                   _propertyId;
  protected int                   _borrowerId;
  protected int                   _copyId;
  protected int                   _requestId;
  protected int                   _userId;
  protected int                   _serviceContactId;
  protected int                   _serviceProductId;
  protected int                   _serviceProviderId;
  protected int                   _payloadTypeId;
  protected int                   _langId;

  // objects
  protected SessionResourceKit    _srk;
  protected SysLogger             _logger;
  protected Deal                  _deal;
  protected Request               _request;
  protected ServiceRequest        _serviceRequest;
  protected ArrayList             _properties;
  protected Property              _property;
  protected UserProfile           _userProfile;
  protected ServiceProduct        _serviceProduct;
  protected ServiceProvider       _serviceProvider;
  protected ServiceRequestContact _serviceContact;
  protected PayloadType           _payloadType;
  
  public ServicePayloadData(SessionResourceKit srk, int requestId, int borrowerId, int copyId, int langId) 
  throws RemoteException, ExternalServicesException {
    super();
    init(srk, requestId, borrowerId, copyId, langId);
  }

  public ServicePayloadData(SessionResourceKit srk, int requestId, int copyId, int langId) 
  throws RemoteException, ExternalServicesException {
    super();
    init(srk, requestId, 0, copyId, langId);
  }

  protected void init(SessionResourceKit srk, int requestId, int borrowerId, int copyId, int langId) 
  throws ExternalServicesException {
    _srk = srk;
    _logger = ResourceManager.getSysLogger("SrvcPldDat");
    _logger.debug(this.getClass(), "Started");

    _requestId = requestId;
    _borrowerId = borrowerId;

    // added by Midori
    _copyId = copyId;
    _langId = langId;

    // 1) get the request
    try {
      _request = new Request(_srk, _requestId, copyId);
    } catch (Exception e) {
      _logger.error(StringUtil.stack2string(e));
      throw new ExternalServicesException("Cannot instantiate for requestId = " + _requestId + ", copyId = " + _copyId);
    }

    // 2) get the deal
    _dealId = _request.getDealId();
    try {
      _deal = new Deal(_srk, null, _request.getDealId(), _request.getCopyId());

    } catch (Exception e) {
      _logger.error(StringUtil.stack2string(e));
      throw new ExternalServicesException("Cannot instantiate Deal for requestId = " + _requestId + ", copyId = " + _copyId);
    }
    
    // 3) service product Id
    _serviceProductId = _request.getServiceProductId();
     getServiceProduct();

     _serviceProviderId = _serviceProduct.getServiceProviderId();
     
     // 4) property Id
     try {
    	 _propertyId = getPropertyId();

       } catch (Exception e) {
         _logger.error(StringUtil.stack2string(e));
         throw new ExternalServicesException("Cannot instantiate property for requestId = " + _requestId + ", copyId = " + _copyId);
       }
     
        
  }

  public ServiceRequest getServiceRequest() throws RemoteException {
    if (_serviceRequest == null) {
      try {
        _serviceRequest = new ServiceRequest(_srk, _requestId, _copyId );
        
        _logger.debug("_data.serviceRequest = " + _serviceRequest);
        if (_serviceRequest != null) {
          _propertyId = _serviceRequest.getPropertyId();
          _logger.debug("_data.propertyId = " + _propertyId + " ; copyId = " + _copyId);
        }

      } catch (FinderException e) {
        // serviceRequest doesn't exist for this type of service
      } catch (RemoteException e) {
        _logger.error(StringUtil.stack2string(e));
        throw new RemoteException("Cannot instantiate serviceRequest for requestId = " + _requestId + ", copyId = " + _copyId);
      }
    }
    return _serviceRequest;
  }

  public void setServiceRequest(ServiceRequest srvcRequest) {
    _serviceRequest = srvcRequest;
  }

  public Property getProperty() throws FinderException, RemoteException {
    if (_property ==  null){
      try {
        _property = new Property(_srk, null, _propertyId, _copyId);
        
      } catch (FinderException e) {
        _logger.error(StringUtil.stack2string(e));
        throw new FinderException("Cannot instantiate user profile for requestId = " + _requestId + ", copyId = " + _copyId);
      } catch (RemoteException e) {
        _logger.error(StringUtil.stack2string(e));
        throw new RemoteException("Cannot instantiate user profile for requestId = " + _requestId + ", copyId = " + _copyId);
      }
      
    }
    return _property;
  }

  public void setProperty(Property _property) {
    this._property = _property;
  }

  public int getBorrowerId() {
    return _borrowerId;
  }

  public void setBorrowerId(int borrowerId) {
    this._borrowerId = borrowerId;
  }

  public int getPropertyId() throws RemoteException {
    if (_propertyId == 0) {
      getServiceRequest();
      if (_serviceRequest != null) {
        _propertyId = _serviceRequest.getPropertyId();
        }
    }
    return _propertyId;
  }

  public void setPropertyId(int propertyId) {
    this._propertyId = propertyId;
  }

  public int getServiceRequestContactId() {
    if (_serviceContactId == 0) {
      if (_serviceContact != null) {
        _serviceContactId = _serviceContact.getServiceRequestContactId();
      }
    }
    return _serviceContactId;
  }

  public void setServiceRequestContactId(int requestContactId) {
    _serviceContactId = requestContactId;
  }
  
  public ServiceRequestContact getServiceRequestContact() throws ExternalServicesException {
    if (_serviceContact == null) {
      try {
        _serviceContact = new ServiceRequestContact(_srk);
        _serviceContact.findLastByRequestId(_requestId);
      } catch (FinderException e) {
        // serviceRequest doesn't exist for this type of service
      } catch (RemoteException e) {
        _logger.error(StringUtil.stack2string(e));
        throw new ExternalServicesException("Cannot instantiate serviceRequestContact for requestId = " + _requestId
            + ", copyId = " + _copyId);
      }
    }
    return _serviceContact;
  }

  public void setServiceRequestContact(ServiceRequestContact requestContact) {
    _serviceContact = requestContact;
  }
  
  /**
   * Catherine: 22-Mar-06
   * ToDo: Move this to the parent class if the same account id will be for other services
   * @return
   */
  public String getDatxdAccountId() {
    String datxAccountId = "";
    StringBuffer sb = new StringBuffer();

    String ecniL = extractEcniLenderId();
    if (ecniL != null) {
      _logger.debug("ecniLenderId = " + ecniL);

      sb.append(ecniL).append(".");
    }
    String shortName = extractLenderShortName();
    if (shortName != null) {
      _logger.debug("shortName = " + shortName);

      sb.append(shortName);
    }

    return datxAccountId;
  }

  private String extractEcniLenderId() {
    String retVal = null;
    try {
      InstitutionProfile ip = new InstitutionProfile(_srk);
      ip = ip.findByFirst();
      retVal = ip.getECNILenderId();

      if (retVal != null) {
        return retVal.trim();
      }
    } catch (Exception exc) {
      _logger.error(StringUtil.stack2string(exc));
    }
    return retVal;
  }

  private String extractLenderShortName() {
    String retVal = null;
    try {
      LenderProfile lp = new LenderProfile(_srk);

      int id = _deal.getLenderProfileId();
      lp = lp.findByPrimaryKey(new LenderProfilePK(id));

      retVal = lp.getLPShortName();

      if (retVal != null) {
        return retVal.trim();
      }
    } catch (Exception exc) {
      _logger.error(StringUtil.stack2string(exc));
    }
    return retVal;
  }

  public int getCopyId() {
    return _copyId;
  }

  public void setCopyId(int id) {
    _copyId = id;
  }

  public Request getRequest() {
    return _request;
  }

  public int getRequestId() {
    return _requestId;
  }

  public Deal getDeal() {
    return _deal;
  }

  public int getDealId() {
    return _dealId;
  }

  public int getUserId() {
    return _userId;
  }

  public void setUserId(int id) {
    _userId = id;
  }

  public UserProfile getUserProfile() throws RemoteException, FinderException {
    if (_userProfile == null) {
      // instantiate user profile for current user   
      try {
        _userProfile = new UserProfile (_srk);
        _userProfile = _userProfile
                 .findByPrimaryKey(new UserProfileBeanPK (_request.getUserProfileId(), 
                                                          _srk.getExpressState().getDealInstitutionId()));
      } catch (FinderException e) {
        _logger.error(StringUtil.stack2string(e));
        throw new FinderException("Cannot instantiate user profile for requestId = " + _requestId + ", copyId = " + _copyId);
      } catch (RemoteException e) {
        _logger.error(StringUtil.stack2string(e));
        throw new RemoteException("Cannot instantiate user profile for requestId = " + _requestId + ", copyId = " + _copyId);
      }
    }
    return _userProfile;
  }

  public void setUserProfile(UserProfile profile) {
    _userProfile = profile;
  }

  public ServiceProduct getServiceProduct() {
    if (_serviceProduct == null){
      try {
        _serviceProduct = _request.getServiceProduct();
      } catch (Exception e) {
        _logger.error("Cannot instantiate ServiceProuct for requestId = " + _requestId + ", copyId = " + _copyId + "\n" + StringUtil.stack2string(e));
      }
    }
    
    return _serviceProduct;
  }

  public ServiceProvider getServiceProvider() {
    if (_serviceProvider == null) {
      try {
        _serviceProvider = new ServiceProvider(_srk, _serviceProviderId);
      } catch (Exception e) {
        _logger.error("Cannot instantiate ServiceProvider for requestId = " + _requestId + ", copyId = " + _copyId + "\n"+ StringUtil.stack2string(e));
      }
    }
    return _serviceProvider;
  }

  public String getMortgagePurpose() throws RemoteException, FinderException {

    if (_deal == null)
      return null;
    DealPurpose purpose = new DealPurpose(_srk, _deal.getDealPurposeId());
    if (purpose != null)
      return purpose.getDpDescription();
    else
      return null;
  }

  public void setServiceContact(ServiceRequestContact srvcContact) {
    this._serviceContact = srvcContact;
  }

  public int getServiceProductId() {
    return _serviceProductId;
  }

  public void setServiceProductId(int serviceProductId) {
    this._serviceProductId = serviceProductId;
  }

  public int getServiceProviderId() {
    return _serviceProviderId;
  }

  public void setServiceProviderId(int serviceProviderId) {
    this._serviceProviderId = serviceProviderId;
  }

  public void setServiceProduct(ServiceProduct serviceProduct) {
    this._serviceProduct = serviceProduct;
  }

  public void setServiceProvider(ServiceProvider serviceProvider) {
    this._serviceProvider = serviceProvider;
  }

  public int getPayloadTypeId() {
    return _payloadTypeId;
  }

  public void setPayloadTypeId(int typeId) {
    _payloadTypeId = typeId;
  }

  public PayloadType getPayloadType() {
    return _payloadType;
  }

  public void setPayloadType(PayloadType type) {
    _payloadType = type;
  }

  public Collection getProperties() throws Exception {
    if (_properties == null) {
      _properties = (ArrayList) _deal.getProperties();
    }
    
    return _properties;
  }

}
