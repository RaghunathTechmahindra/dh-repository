/**
 * <p>Title: NoStoredRequestData.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Dec 13, 2006)
 *
 */
package com.filogix.externallinks.framework;

import com.basis100.deal.entity.PayloadType;

public abstract class NoStoredRequestData extends NoStoredTransactionData
{
  protected PayloadType _payloadType;
  protected String _xmlpayload;
  protected Object _requestBean;

  /**
   * <p>NoStoreRequestData</p>
   * Constructor
   */
  public NoStoredRequestData()
  {
    super();
  }

  // Getters and Setters
  public PayloadType getPayloadType()
  {
    return _payloadType;
  }

  public void setPayloadType(PayloadType type)
  {
    _payloadType = type;
  }

  public String getXmlpayload()
  {
    return _xmlpayload;
  }

  public void setXmlpayload(String _xmlpayload)
  {
    this._xmlpayload = _xmlpayload;
  }

  public Object getRequestBean()
  {
    return _requestBean;
  }

  // this method should be override
  public void setRequestBean(Object bean)
  {
    _requestBean = bean;
  }
}
