package com.filogix.externallinks.framework;

import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.services.datx.DatxResponseParser;


public abstract class ServiceResponseProcessorBase {
  protected SessionResourceKit _srk;
  protected int                _requestId;
  protected int                _responseId;
  protected SysLogger          _logger;
  protected int                _langId = 0;

  public void init(SessionResourceKit srk, int requestId){
      init(srk, requestId, ServiceConst.LANG_ENGLISH);
    }

  public void init(SessionResourceKit srk, int requestId, int langId){
      _srk = srk;
      _requestId = requestId;
      _logger = ResourceManager.getSysLogger("SrRspPrcBs");
      _logger.debug("Initialized");
      _langId = langId;
      _logger = ResourceManager.getSysLogger("SrRspPrcBs");
      _logger.debug("Initialized");
    }

  /*
   * @return responseId of a new record
   * -1 if failed
   */
  public abstract int process(ServiceResponseBase response) 
    throws ExternalServicesException, RemoteException, FinderException;

  // gets the status id from the BXResources
  protected int getStatusId(String statusCondition, String statusCode) {
    int result = ServiceConst.RESPONSE_STATUS_SYSTEM_ERROR;
    String cond = statusCondition;
      
    String table = "RESPONSESTATUS";
    
    try {

      // DatX doesn't follow the spec and returns FAILED that we have to map to System Error
      if (statusCondition.equals(DatxResponseParser.CONDITION_FAILED)) 
      {{
        if ((statusCode != null) && 
            ((statusCode.trim().equals(DatxResponseParser.ERROR_PROPERTY_ADDRESS_NOT_UNIQUE)|| 
             (statusCode.equals(DatxResponseParser.ERROR_INVALID_ADDRESS)) ||
             (statusCode.equals(DatxResponseParser.ERROR_PROCESSING))
             )))
        {
          result = ServiceConst.RESPONSE_STATUS_PROCESSING_ERROR;   
        } else {
          result = ServiceConst.RESPONSE_STATUS_SYSTEM_ERROR;   
        }}
      } else {
      
        // Get IDs and Labels from BXResources
        String sResult = BXResources.getPickListDescriptionId(_srk.getExpressState().getDealInstitutionId(),table, cond, ServiceConst.LANG_ENGLISH);
  
        if (!(sResult == null)){
          try {
            result = Integer.parseInt(sResult);
          }
          catch (NumberFormatException ne){
            _logger.error("ServiceResponseProcessor@getStatusId: incorrect status id for _Condition = " + statusCondition + ". Update " + table + " picklist");
          }
        }
      }
    } catch (Exception ex) {
      _logger.error("Error in getStatusId(): " + StringUtil.stack2string(ex));
    }

    return result;
  }

}
