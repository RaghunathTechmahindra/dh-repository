package com.filogix.externallinks.framework;

import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

public abstract class ServicePayloadGeneratorBase {
  protected SessionResourceKit _srk;
  protected int                _requestId;
  protected int                _copyId;
  protected String             _payloadXml;
  protected ServicePayloadData _data;
  protected SysLogger          _logger;
  
  protected void init(SessionResourceKit srk, int requestId, int copyId){
    this._srk = srk;
    this._requestId = requestId;
    this._copyId = copyId;
    this._logger = ResourceManager.getSysLogger("PayldGnBse");
    _logger.debug("Initialized");
  }

  public abstract ServicePayloadData getPayloadData() 
    throws RemoteException, FinderException, ExternalServicesException;  
  
  public abstract ServicePayloadData getPayloadData(int langId) 
    throws RemoteException, FinderException, ExternalServicesException;  

  protected abstract String getXmlPayload() throws Exception;
  
}
