/**
 * <p>Title: JaxbPayloadHelper.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Nov 23, 2006)
 *
 */
package com.filogix.externallinks.framework;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

public class JaxbPayloadHelper
{
  public static final String CustomCharacterEscapeHandlerType_U = "U"; // use \\u00XX
  public static final String CustomCharacterEscapeHandlerType_AMP = "AMP"; // use &xXX;
  public JaxbPayloadHelper()
  {
    super();
  }

  public static String getXmlPayload(Object payload, JAXBContext jaxbContext, String noNamespaceURL) 
    throws Exception
  {
    return getXmlPayload(payload, jaxbContext, noNamespaceURL, CustomCharacterEscapeHandlerType_U);
  }
  
  public static String getXmlPayload(Object payload, JAXBContext jaxbContext, String noNamespaceURL, String customCharacterEscapeHandlerType) 
    throws Exception
  {
      if (jaxbContext == null ) return null;
      Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

      // use CustomeCharacterExcapeHandler using //u00XX over ascii charactors
      if (CustomCharacterEscapeHandlerType_U.equals(customCharacterEscapeHandlerType))
      {
        marshaller.setProperty(
                "com.sun.xml.bind.characterEscapeHandler",
                new CustomCharacterEscapeHandler() );
      }
//      else if (customCharacterEscapeHandlerType_AMP.equals(customCharacterEscapeHandlerType))
//      {
//        marshaller.setProperty(
//                "com.sun.xml.bind.characterEscapeHandler",
//                new CustomCharacterEscapeHandler() );
//      }
      
      StringWriter xmlWriter = new StringWriter();
      marshaller.marshal(payload, xmlWriter);

      if(noNamespaceURL != null && noNamespaceURL.trim().length() > 0)
      {
        String xmlString = fixNamespace(xmlWriter.toString(), noNamespaceURL);
        return xmlString;
      }
      
      return xmlWriter.toString();
  }
  
  /**
   * <p>fixNameSpace</p>
   * remove generated namespace attribute and add noNamespaceSchemaLocation attribute
   *  
   * @param orgnalXML
   * @param noNamespaceURL
   * @return String fixed XML
   */
  public static String fixNamespace(String orgnalXML, String noNamespaceURL)
  {
      int firstLineStart = orgnalXML.indexOf('<');
      int firstLineEnd = orgnalXML.indexOf('>');
      String firstLine = orgnalXML.substring(firstLineStart, firstLineEnd+1 );
      
      String temp = orgnalXML.substring(firstLineEnd+1);

      int rootTagElementEnd = temp.indexOf(' ');
      int rootTagEnd = temp.indexOf('>');
      if (rootTagElementEnd < rootTagEnd)
      {
        String rootTagNS = temp.substring(rootTagElementEnd, rootTagEnd);
        temp = temp.replaceFirst(rootTagNS, " xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  xsi:noNamespaceSchemaLocation=\"" + noNamespaceURL + "\"  ");
                
        while (temp.indexOf("<ns") > -1 )
        {
            int tagStart = temp.indexOf("<ns");
            String nsPrefix = temp.substring(tagStart+1, temp.indexOf(":", tagStart));
            temp = temp.replaceFirst(nsPrefix+":", "");
            
            int defNSStart = temp.indexOf("xmlns:"+nsPrefix);
            int defNSEnd1 = temp.indexOf("/>", defNSStart);
            int defNSEnd2 = temp.indexOf(">", defNSStart);
            int defNSEnd = defNSEnd2;
            if (defNSEnd1 > -1 &&  defNSEnd1 < defNSEnd2) defNSEnd = defNSEnd1;
            String defNS = temp.substring(defNSStart,  defNSEnd);
            temp = temp.replaceAll(" " + defNS, "");            
            temp = temp.replaceFirst(nsPrefix+":", "");
        }
      }
      
      return firstLine + temp;
  }
  
  /**
   * <p>validate</p>
   * validate jaxb Object against schema after unMarshalling 
   * @param o jaxb Object
   * @return true if input is valid, otherwise false.
   */
  public boolean validate(Object o) 
  {
    try
    {
      String packageName = o.getClass().getPackage().getName();
      JAXBContext jaxbContext = JAXBContext.newInstance(packageName);
      if(jaxbContext == null) return false;
      
          Marshaller marshaller = jaxbContext.createMarshaller();
          
          StringWriter xmlWriter = new StringWriter();
          marshaller.marshal(o, xmlWriter);
      
      return true;
    }
    catch(JAXBException jaxbe)
    {
      return false;
    }
  }

  /**
   * <p>getPayloadBean<p>
   * get response payload object generated from schema from xml string based on FXResponse.
   * 
   * @param responseBody String
   * @param jaxbContext Response Payload JAXContext
   * @param payloadTagName String response payload root tag name
   * @return Response payload object 
   * @throws JAXBException
   */
  public static Object getPayloadBean(String responseBody, JAXBContext jaxbContext, String payloadTagName) 
    throws JAXBException
  {
      if (responseBody.indexOf(payloadTagName) < 0 ) return null;
      int startPayload = responseBody.indexOf(payloadTagName) - 1;
      int endPayload = responseBody.indexOf(payloadTagName, startPayload + 2) + payloadTagName.length() + 1;
      
      String payloadString = responseBody.substring(startPayload, endPayload);
      StreamSource st = new StreamSource( new StringReader(payloadString));
      try
      {
          // work request as String
          if (jaxbContext == null) return null;
      
          Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
          Object payload = unmarshaller.unmarshal(st);
          return payload;
      }
      catch(JAXBException e)
      {
          throw e;
      }
  }
}
