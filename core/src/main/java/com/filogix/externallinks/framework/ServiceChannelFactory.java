package com.filogix.externallinks.framework;

import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.filogix.externallinks.services.datx.DatXClosingCanCannel;
import com.filogix.externallinks.services.datx.DatXClosingChannel;
import com.filogix.externallinks.services.datx.DatxAppCanChannel;
import com.filogix.externallinks.services.datx.DatxAppReqChannel;
import com.filogix.externallinks.services.datx.DatxAvmChannel;

public class ServiceChannelFactory implements ServiceConst {

    private SysLogger _logger;
    private static ServiceChannelFactory _factory = new ServiceChannelFactory();
    
    public static synchronized ServiceChannelFactory getInstance(){
      return _factory;
    }
    
    private ServiceChannelFactory(){
      _logger = ResourceManager.getSysLogger( "SvcChnlFct" );
      _logger.debug("Started");
    }
    
    /**
     * 
     * @param serviceRequestTypeId
     * @return
     */ 
    // TODO : Catherine: implement DRUles later
    public ServiceChannelBase getChannel(String requestTypeStr){
      ServiceChannelBase _channel = null;
      
      if (requestTypeStr.equals( REQUEST_TYPE_AVM_STR) ) {
        _channel = new DatxAvmChannel();
      }
      else if(requestTypeStr.equals( REQUEST_TYPE_CLOSING_FIXED_REQUEST_STR) )
      {
        _channel = new DatXClosingChannel();
      }
      else if(requestTypeStr.equals( REQUEST_TYPE_CLOSING_OSC_REQUEST_STR) )
      {
        _channel = new DatXClosingChannel();
      }      
      else if(requestTypeStr.equals( REQUEST_TYPE_CLOSING_OSC_CANCEL_STR) )
      {
          _channel = new DatXClosingCanCannel();
      }
      else if (requestTypeStr.equals( REQUEST_TYPE_APPRAISAL_REQUEST_STR) )
      {
          _channel = new DatxAppReqChannel();
      }  
      else if (requestTypeStr.equals( REQUEST_TYPE_APPRAISAL_CANCEL_STR) ) 
      {
          _channel = new DatxAppCanChannel();
      }
      
      return _channel;
    }

    public ServiceChannelBase getChannel(int requestTypeId){
      ServiceChannelBase _service = null;
        
       switch (requestTypeId) {
        case REQUEST_TYPE_AVM_A_INT:
        case REQUEST_TYPE_AVM_B_INT:
        case REQUEST_TYPE_AVM_C_INT:  
        {
          _service = new DatxAvmChannel();
          break;
        }
        case REQUEST_TYPE_CLOSING_FIXED_REQUEST_INT: 
        case REQUEST_TYPE_CLOSING_OUTSOURCED_REQUEST_INT: 
        {
        	_service = new DatXClosingChannel();
        	break;
        }
        case REQUEST_TYPE_CLOSING_OUTSOURCED_CANCEL_INT: 
        {
        	_service = new DatXClosingCanCannel();
        	break;
        }
        case REQUEST_TYPE_APPRAISAL_FULL_REQUEST_INT: {
          _service = new DatxAppReqChannel();
          break;
        }  
        case REQUEST_TYPE_APPRAISAL_FULL_CANCEL_INT: {
          _service = new DatxAppCanChannel();
          break;
        }
        default: {
          break;
        }
      }
      return _service;    
    }
}
