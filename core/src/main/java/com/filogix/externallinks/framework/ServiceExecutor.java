package com.filogix.externallinks.framework;

import java.util.Date;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Request;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

public class ServiceExecutor {

  // framework attributes
  private SessionResourceKit _srk;
  private SysLogger          _logger;
  private Request            _request;
   
  public ServiceExecutor(SessionResourceKit srk){
    this._srk = srk;
    _logger = ResourceManager.getSysLogger( "SrvcExec" );
    _logger.debug(this.getClass(), "Started");
  }
  
  public int processSyncRequest(int requestId, int copyId) {
    int langId = ServiceConst.LANG_ENGLISH;
    return processSyncRequest(requestId, copyId, langId);
  }

    /**
   * requestId refers to the REQUEST table
   * @throws RemoteException 
   * @throws FinderException 
   * 
   * @return reponseId for a new record
   */
  public int processSyncRequest(int requestId, int copyId, int langId) {
    int result = -1;
    
    try {

      // instantiate the request record and find the type of 
      _request = new Request(_srk);
      
      _request.findByPrimaryKey(new RequestPK(requestId, copyId));
      if (_request == null){
        throw new Exception ("Cannot find Request: requestId = " + requestId + ", copyId = " + copyId);
      }
      
      String reqType = _request.getRequestTypeShortName(requestId, copyId).trim();
      
      result = processSync(reqType, requestId, copyId, langId);
      
    } catch (ExternalServicesException es) {
      setRequestStatusToSystemError();

//      String poolname = PropertiesCache.getInstance().getProperty(_srk.getExpressState().getDealInstitutionId(),
//              "com.basis100.resource.connectionpoolname");
      try {
        sendEmailAlert( "External services Error in express, \n",
//      sendEmailAlert( "External services Error in express, " + poolname + " environment \n",
            "Express External Services: error processing request for " + _request.getRequestTypeShortName(requestId).trim() + ". \n" +
            "Error code = [" + es.getClass() + " " + es.getMessage() + "]; " + "\n" +
            "Deal = " + _request.getDealId() +"\n"
          );
      } catch (Exception e) {
        _logger.error(StringUtil.stack2string(es));
      } 
      _logger.error(StringUtil.stack2string(es));
      
    } catch (Exception e) {
      _logger.error(StringUtil.stack2string(e));
      
    }
    return result;
  }  
  
  private void setRequestStatusToSystemError() {
    try {
      _srk.beginTransaction();
      _request.setStatusDate(new Date());
      _request.setRequestStatusId(ServiceConst.REQUEST_STATUS_SYSTEM_ERROR);
      _request.setStatusMessage(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "REQUESTSTATUS", 
          ServiceConst.REQUEST_STATUS_SYSTEM_ERROR, ServiceConst.LANG_ENGLISH));

      _request.ejbStore();

      _srk.commitTransaction();
      
    } catch (Exception e) {
      _logger.error(StringUtil.stack2string(e));
      try {
        _srk.cleanTransaction();
      } catch (Exception e1) {
        _logger.error(StringUtil.stack2string(e1));
      }
    }
  }
  
  /**
   *  method to process synchronous request for service
   *  TODO: Catherine: return ResponseId or -1
   * @throws FinderException 
   * @throws RemoteException 
   */  
  private int processSync(String requestTypeStr, int requestId, int copyId, int langId) 
    throws ExternalServicesException, RemoteException, FinderException {
    
    int result = -1;
    
    _logger.debug("processSyncRequest() started. requestTypeStr = " + requestTypeStr + ", requestId = " + requestId);

    // 1) create datapayload    
    ServicePayloadGeneratorBase generator = ServicePayloadGeneratorFactory.getInstance().getGenerator(requestTypeStr);
    if(generator == null){
      throw new ExternalServicesException("Cannot instantiate payload generator for requestId = " + requestId + " of type = " + requestTypeStr);
    }
    generator.init(_srk, requestId, copyId);
    
    ServicePayloadData data;
    try {
      data = generator.getPayloadData();
      
    } catch (Exception e) {
      _logger.error(StringUtil.stack2string(e));
      throw new ExternalServicesException("Cannot generate payload data for requestId = " + requestId + " of type = " + requestTypeStr);
    }

    // 2) initialize the channel
    ServiceChannelBase srvcChan = ServiceChannelFactory.getInstance().getChannel(requestTypeStr);
    if(srvcChan == null){
      throw new ExternalServicesException("Cannot instantiate service channel for requestId = " + requestId + " of type = " + requestTypeStr);
    }

    try {
      srvcChan.init(_srk, requestId, copyId);
    } catch (RemoteException re) {
      _logger.error(StringUtil.stack2string(re));
      throw new ExternalServicesException("Error sending data through the channel = " + srvcChan.getClass() + "; Request failed: requestId = " + requestId); 
    } catch (FinderException fe) {
      _logger.error(StringUtil.stack2string(fe));
      throw new ExternalServicesException("Error sending data through the channel = " + srvcChan.getClass() + "; Request failed: requestId = " + requestId); 
    }

    // 3) send the payload and get the response
    _logger.debug("Sending requestId = " + requestId + " of type = "+ requestTypeStr + " through channel= " + srvcChan);
    ServiceResponseBase response;
    try {
      response = srvcChan.sendData(data);
    } catch (Exception e) {
      _logger.error(StringUtil.stack2string(e));
      throw new ExternalServicesException("Error sending data through the channel = " + srvcChan.getClass() + "; Request failed: requestId = " + requestId); 
    }

    // 4) check if the response is null (might have been timed out by DatX) 
    if (response == null) {
      throw new ExternalServicesException("DatX response is null. The response might have been timed out for requestId = " + requestId + " of type = " + requestTypeStr);
    }
    
    // 5) persist the response
    ServiceResponseProcessorBase processor = ServiceResponseProcessorFactory.getInstance().getProcessor(requestTypeStr);
    if(srvcChan == null){
      throw new ExternalServicesException("Cannot instantiate service response processor for requestId = " + requestId + " of type = " + requestTypeStr);
    }
    processor.init(_srk, requestId, langId);
    result = processor.process(response);
    
    return result;
  }

  private void sendEmailAlert(String subject, String text) throws DocPrepException, JdbcTransactionException {

    String emailTo = null;
    emailTo = PropertiesCache.getInstance().getProperty(_srk.getExpressState().getDealInstitutionId(),
            "com.filogix.docprep.alert.email.recipient"); 

    _logger.trace("Placing a request for an alert email into documentqueue...");
    // srk.beginTransaction();
    DocumentRequest.requestAlertEmail(_srk, _request.getDealId(), _request.getCopyId(), emailTo, subject, text);
    // srk.commitTransaction();
    _logger.trace("request for documentqueue is done");

  }

}
