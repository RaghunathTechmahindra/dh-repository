package com.filogix.externallinks.framework;

import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.Request;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.datx.messagebroker.AbstractMessageSender;

public abstract class ServiceChannelBase {
  protected SessionResourceKit    _srk;
  protected SysLogger             _logger;
  protected int                   _requestId;
  protected int                   _copyId;
  
  protected AbstractMessageSender _messageSender;
  protected Object                _requestBean;
  protected ServicePayloadData    _data;
  protected Channel               _channel;
  protected Request               _request;
  protected String                _url;
    
  public void init(SessionResourceKit srk, int requestId, int copyId) throws RemoteException, FinderException {
    _srk = srk;
    _requestId = requestId;
    _copyId = copyId;
    _logger = ResourceManager.getSysLogger("SrvcChnlBs");
    _logger.debug("Initialized");
    
    // initialize channel
    _request = new Request(_srk, _requestId, copyId);
    int channelId = _request.getChannelId();    
    _channel = new Channel(_srk, channelId);
    
    _logger.debug("entity Channel = " + _channel);
    
    buildUrl();
    
  }
  
  /*
   * as per Frank's email of 22-Mar-06:
   * express shall populate it with: (InstitutionProfile.ECNILenderID).(LenderProfile.LPshortName)
   */
  
  private void buildUrl() {
    
    StringBuffer url = new StringBuffer();
    String protocol = _channel.getProtocol();
    String ip = _channel.getIp();
    String path = _channel.getPath();
    
    if (protocol != null && !protocol.trim().equals("")) {
      url.append(protocol + "://");
    }
    
    if ( ip != null && !ip.trim().equals("")) {
      url.append(ip);
    }
    
    if (_channel.getPort() != 0) {
      url.append(":" + _channel.getPort());
    }
    
    if (path != null && !path.trim().equals("")){
      if (path.indexOf('\\') != -1 ){
        path = path.replace('\\', '/');
      }
      if (!path.startsWith("/")){
      url.append("/" + path);
      }
      url.append(path);
    }

    this._url = url.toString();
    
    _logger.debug("Channel url = " + _url);
  }

  protected abstract void initRequestBean() throws RemoteException, FinderException, ExternalServicesException;
  
  public abstract ServiceResponseBase sendData(ServicePayloadData data) throws Exception;

  public Channel get_channel() {
    return _channel;
  }

  public String getUrl(){
    return _url; 
  }

}
