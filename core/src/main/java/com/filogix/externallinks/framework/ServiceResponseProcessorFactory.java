package com.filogix.externallinks.framework;

import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.filogix.externallinks.services.datx.AcknowledgementAppraisalResProcessor;
import com.filogix.externallinks.services.datx.AcknowledgementClosingResProcessor;
import com.filogix.externallinks.services.datx.DatxAvmResponseProcessor;

public class ServiceResponseProcessorFactory implements ServiceConst {

  private SysLogger _logger;
  private static ServiceResponseProcessorFactory _factory = new ServiceResponseProcessorFactory();
  
  public static synchronized ServiceResponseProcessorFactory getInstance(){
    return _factory;
  }
  
  private ServiceResponseProcessorFactory(){
    _logger = ResourceManager.getSysLogger( "SvcRspnFct" );
    _logger.debug("Started");
  }
  
  /**
   * 
   * @param serviceRequestTypeId
   * @return
   */ 
  // TODO : Catherine: implement DRUles or Spring later
  public ServiceResponseProcessorBase getProcessor(String requestTypeStr){
    ServiceResponseProcessorBase _processor = null;
    
    if (requestTypeStr.equals(REQUEST_TYPE_AVM_STR)) {
      _processor = new DatxAvmResponseProcessor();
    }
    else if (REQUEST_TYPE_CLOSING_FIXED_REQUEST_STR.equals(requestTypeStr)
    		|| REQUEST_TYPE_CLOSING_OSC_REQUEST_STR.equals(requestTypeStr)
    		|| REQUEST_TYPE_CLOSING_OSC_CANCEL_STR.equals(requestTypeStr))
    {
      _processor = new AcknowledgementClosingResProcessor();
    }
    else if(REQUEST_TYPE_APPRAISAL_REQUEST_STR.equals(requestTypeStr)
    		|| REQUEST_TYPE_APPRAISAL_CANCEL_STR.equals(requestTypeStr))
    {
        _processor = new AcknowledgementAppraisalResProcessor();
    }
    return _processor;
  }

  public ServiceResponseProcessorBase createRequest(int requestTypeId){
    ServiceResponseProcessorBase _processor = null;
      
     switch (requestTypeId) {
      case REQUEST_TYPE_AVM_A_INT:
      case REQUEST_TYPE_AVM_B_INT:
      case REQUEST_TYPE_AVM_C_INT:  
      {
             _processor = new DatxAvmResponseProcessor();
        break;
      }
      case REQUEST_TYPE_CLOSING_FIXED_REQUEST_INT:
      {
         _processor = new AcknowledgementClosingResProcessor();
         break;
      }
      case REQUEST_TYPE_APPRAISAL_FULL_REQUEST_INT: 
      case REQUEST_TYPE_APPRAISAL_FULL_CANCEL_INT:
      {
              _processor = new AcknowledgementAppraisalResProcessor();
        break;
      }  

      default: {
        break;
      }
    }
    return _processor;    
  }
}
