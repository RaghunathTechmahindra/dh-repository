/**
 * <p>Title: JaxbServicePayloadGeneratorBase.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 16, 2006)
 *
 */

package com.filogix.externallinks.framework;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.transform.stream.StreamSource;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Province;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.schema.datx.AddressType;
import com.filogix.schema.datx.BorrowerType;
import com.filogix.schema.datx.ContactInformationType;
import com.filogix.schema.datx.RequesterType;
import com.filogix.schema.datx.impl.BorrowerTypeImpl;
import com.sun.xml.bind.JAXBObject;
import config.Config;

public abstract class JaxbServicePayloadGenerator  extends ServicePayloadGeneratorBase
{   
  
    protected void init(SessionResourceKit srk, int requestId, int copyId)
    {
        super.init(srk, requestId, copyId);
    }

    protected void setServiceRequestToData() throws Exception
    {
        try {
          ServiceRequest srvcReq = new ServiceRequest(_srk, _requestId, _copyId );
          int propertyId = srvcReq.getPropertyId();
          int copyId = srvcReq.getCopyId();
          
          _data.setServiceRequest(srvcReq);
          _data.setPropertyId(propertyId);
          _data.setCopyId(copyId);
    
          _logger.debug("_data.serviceRequest = " + srvcReq);
          _logger.debug("_data.propertyId = " + propertyId + " ; copyId = " + copyId);
    
        } catch (FinderException fe) {
          _logger.error(StringUtil.stack2string(fe));
          throw new ExternalServicesException("Cannot instantiate ServiceRequest for requestId = " + _requestId);
        }
    }
    
    protected void setBorrowersCmnTypes() throws ExternalServicesException
    {
        Collection borrowers = null;
        try
        {
            borrowers = _data.getDeal().getBorrowers();
        }
        catch(Exception e)
        {
            throw new ExternalServicesException("Cannot instantiate ServiceRequest for requestId = " + _requestId);
        }
        
        JaxbServicePayloadData _jaxbData = (JaxbServicePayloadData)_data;
        Iterator iter = borrowers.iterator();
        while (iter.hasNext())
        {
            Borrower b = (Borrower)iter.next();
                BorrowerType jaxbBorrower = new BorrowerTypeImpl();
                jaxbBorrower.setFirstName(b.getBorrowerFirstName());
            if(b.getBorrowerFirstName()==null)
                jaxbBorrower.setFirstName("");
                jaxbBorrower.setLastName(b.getBorrowerLastName());
            if(b.getBorrowerLastName()==null)
                jaxbBorrower.setLastName("");
                if (b.isPrimaryBorrower())
                {
                    jaxbBorrower.setPrimaryBorrowerIndicator(JaxbServicePayloadData.YNFLAG_Y);
                }
                else
                {
                    jaxbBorrower.setPrimaryBorrowerIndicator(JaxbServicePayloadData.YNFLAG_N);
                }                   
                _jaxbData.getJaxbBorrower().add(jaxbBorrower);
            }
        }            
    
    protected void setPropertyCmnTypes() throws ExternalServicesException, FinderException, RemoteException
    {
        Property property = _data.getProperty();

        JaxbServicePayloadData _jaxbData = (JaxbServicePayloadData)_data;
        AddressType jaxbProperty = _jaxbData.getJaxbProperty();
        jaxbProperty.setCity(property.getPropertyCity());
        if (property.getPropertyCity()==null)
            jaxbProperty.setCity("");            
        jaxbProperty.setCountry("Canada");
        String postalCode = "       ";
        if (property.getPropertyPostalFSA()!= null && property.getPropertyPostalLDU()!= null)
        {
            postalCode = property.getPropertyPostalFSA()+" " + property.getPropertyPostalLDU();
        }
        jaxbProperty.setPostalCode(postalCode);
        Province province = new Province(_srk,property.getProvinceId() );
        if (province==null || province.getProvinceName()==null) jaxbProperty.setProvince("");
        else jaxbProperty.setProvince(province.getProvinceName());
        jaxbProperty.setStreetName(property.getPropertyStreetName());
        if (property.getPropertyStreetName()==null) 
            jaxbProperty.setStreetName("");
        jaxbProperty.setStreetNumber(property.getPropertyStreetNumber());
        if (property.getPropertyStreetNumber()==null) 
            jaxbProperty.setStreetNumber("");
        jaxbProperty.setUnitNumber(property.getUnitNumber());

        try
        {
            jaxbProperty.setStreetDirection(property.getStreetDirectionDescription());
            jaxbProperty.setStreetType(property.getStreetTypeDescription());
            if (property.getStreetTypeDescription()==null)
                jaxbProperty.setStreetType("");
        }
        catch(Exception e)
        {
            throw new ExternalServicesException("Cannot instantiate ServiceRequest for requestId = " + _requestId);
        }
    }
    
    /*
     * Requester is underwriter who made the original request
     */
    protected void setRequesterCmnTypes()  throws RemoteException, FinderException
    {
        UserProfile user = _data.getUserProfile();
        Contact userContact = user.getContact();

        JaxbServicePayloadData _jaxbData = (JaxbServicePayloadData)_data;
        RequesterType jaxbRequester = _jaxbData.getJaxbRequester();

        jaxbRequester.setFirstName(userContact.getContactFirstName());
        if(userContact.getContactFirstName() == null)jaxbRequester.setFirstName("");
        jaxbRequester.setLastName(userContact.getContactLastName());
        if(userContact.getContactLastName() == null)jaxbRequester.setLastName("");
        jaxbRequester.setEmail(userContact.getContactEmailAddress());
        String phoneNumber = userContact.getContactPhoneNumber();
        if (phoneNumber!=null &&  phoneNumber.trim().length() == 10)
            jaxbRequester.setWorkPhoneNumber(phoneNumber.trim());
        else
            jaxbRequester.setWorkPhoneNumber("");

        if (userContact.getContactPhoneNumberExtension()!=null)
            jaxbRequester.setWorkPhoneExtension(userContact.getContactPhoneNumberExtension());
    }

    /*
     * contactInfo is primary borrower's info that is stored serviceRequestContact table
     */
    protected void setContactInfoCmnTypes() throws ExternalServicesException, RemoteException 
    {
        ServiceRequestContact src = _data.getServiceRequestContact();
        JaxbServicePayloadData _jaxbData = (JaxbServicePayloadData)_data;
        ContactInformationType jaxbContactInfo = _jaxbData.getJaxbContactInfo();
        
        jaxbContactInfo.setFirstName(src.getFirstName());
        if(src.getFirstName()==null)
            jaxbContactInfo.setFirstName("");
        jaxbContactInfo.setLastName(src.getLastName());
        if(src.getLastName()==null)
            jaxbContactInfo.setLastName("");
        jaxbContactInfo.setEmail(src.getEmailAddress());
        String homeArea = src.getHomeAreaCode();
        String homeNumber = src.getHomePhoneNumber();
        if (homeArea!=null && homeArea.trim().length() == 3
                && homeNumber!=null && homeNumber.trim().length() == 7 )
            jaxbContactInfo.setHomePhoneNumber(homeArea.trim()+homeNumber.trim());
        
        String workArea = src.getWorkAreaCode();
        String workNumber = src.getWorkPhoneNumber();
        if (workArea!=null && workArea.trim().length() == 3
                && workNumber!=null && workNumber.trim().length() == 7 )
            jaxbContactInfo.setWorkPhoneNumber(workArea.trim()+workNumber.trim());

        if (src.getWorkExtension()!=null)
            jaxbContactInfo.setWorkPhoneExtension(src.getWorkExtension().trim());
        
        String cellArea = src.getCellAreaCode();
        String cellNumber = src.getCellPhoneNumber();
        if (cellArea!=null && cellArea.trim().length() == 3
                && cellNumber!=null && cellNumber.trim().length() == 7 )
            jaxbContactInfo.setCellPhoneNumber(cellArea.trim()+cellNumber.trim());
        
        String _specialInstruction = _jaxbData.getServiceRequest().getSpecialInstructions();
        jaxbContactInfo.setSpecialInstructions(_specialInstruction);
    }
    
    protected ServiceProduct getServiceProduct() throws RemoteException, FinderException
    {
        return _data.getServiceProduct();
    }
    
    protected String getXmlPayload() throws Exception
    {
        if (_payloadXml == null )
        {
            JaxbServicePayloadData _jaxbData = (JaxbServicePayloadData)_data;
            JAXBContext jaxbContext = getJaxbContext();
            if (jaxbContext == null ) return null;
            Marshaller marshaller = jaxbContext.createMarshaller();
            StringWriter xmlWriter = new StringWriter();
//            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(
                    "com.sun.xml.bind.characterEscapeHandler",
                    new CustomCharacterEscapeHandler() );        
            marshaller.marshal(_jaxbData.getServicePayload(), xmlWriter);
            _payloadXml = xmlWriter.toString();
        }
        return _payloadXml;
    }
    
    public static String getXmlPayload(Object payload, JAXBContext jaxbContext, String noNamespaceURL)
      throws Exception
    {
      return JaxbPayloadHelper.getXmlPayload(payload, jaxbContext, noNamespaceURL, JaxbPayloadHelper.CustomCharacterEscapeHandlerType_U);
    }
    
    public static String getXmlPayload(Object payload, JAXBContext jaxbContext, String noNamespaceURL, String escapeHandlerType)
      throws Exception
    {
      return JaxbPayloadHelper.getXmlPayload(payload, jaxbContext, noNamespaceURL, escapeHandlerType);
    }
    public abstract String getPackageName();
    protected abstract JAXBContext getJaxbContext();
}
