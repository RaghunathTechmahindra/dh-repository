/**
 * <p>Title: GenericServiceChannel.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 16, 2006)
 *
 */

package com.filogix.externallinks.framework;

import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import org.apache.axis.client.Stub;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.RemoteException;
import com.basis100.entity.FinderException;
import com.filogix.schema.datx.ServiceCancelType;

public abstract class WSServiceChannel extends ServiceChannelBase implements ServiceConst
{    
    public static JAXBContext _cancelJaxbContext;
    static
    {
        try
        {
            _cancelJaxbContext = JAXBContext.newInstance(ServiceCancelType.class.getPackage().getName());
        }
        catch(JAXBException e)
        {
            
        }
    }

    
    protected String _servicePort = null;
    protected String _payloadSchemaName = null;
    protected String _payloadNNSL = null;
   
    private int LANGUAGE_ID_ENGLISH = 0;

    private String            _requestXML;
    private String            _responseXML;

    protected JaxbServicePayloadData _payloadData = null;
    protected String          _xmlPayload;
    // for new transaction standard
//    public static final String TO_DATX_DATX_KEY = "DATX";
//    public static final String TO_DATX_ENCHODING = "UTF-8";

    // this method must be called after setWSEnv() from subclass
    // if each channel needs more data in request, then override this method
    // just call super.initReuestBean() first, then add more data in _datXRequest

/* copy this method in subclasses since DatxRequest will be generated for each service so that
 * package will be different
    protected void initRequestBean()
            throws RemoteException,
                FinderException,
                ExternalServicesException
    {
        _datXRequest = new DatXRequest();
        
        // Theses data is NOT confirmed with datX 
        _datXRequest.setClientKey(""+_request.getRequestId());

        _datXRequest.setUsername(_channel.getUserId());
        _datXRequest.setPassword(_channel.getPassword());
        _datXRequest.setDatXKey("");        
        _datXRequest.setEncoding("");
        _datXRequest.setAuxData(null);
        _datXRequest.setSchemaName(_schemaName);
        
        int timeout = _request.findDatxTimeout(_request.getServiceProductId(), _request.getChannelId());
        _logger.debug("Timeout = " + timeout);
        _datXRequest.setTimeout(new Integer(timeout));
        
    }
 */
    protected void prepare(ServicePayloadData data) throws Exception
    {
        _servicePort = _channel.getWsServicePort();
        _logger.debug("prepare Datx request --->>> Channel, _url = " + _url);
        _payloadNNSL = data.getPayloadType().getNoNameSchemaLocation();
        _payloadSchemaName = data.getPayloadType().getXmlSchemaName();
        initRequestBean();
        initPayload(data, _payloadNNSL);
    }
    
    public void initPayload(ServicePayloadData data, String noNameSpaceSchemaLocation) throws Exception
    {
        JaxbServicePayloadData payloadData = (JaxbServicePayloadData)data;
        
        try
        {
            _xmlPayload = JaxbServicePayloadGenerator.getXmlPayload
                (payloadData.getServicePayload(), getJaxbContext(), noNameSpaceSchemaLocation);
        }
        catch(Exception e)
        {
            _logger.error("failed to genarate request payload*** ");
            throw e;
        }
    }
    
    public String generateXMLStringFromResponse(Object payload)
        throws JAXBException
    {
        JAXBContext jaxbContext = getJaxbContext();
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter xmlWriter = new StringWriter();
        marshaller.marshal(payload, xmlWriter);
        return xmlWriter.toString();       
    }

    protected JAXBContext getCancelJaxbContext() 
    {
        return _cancelJaxbContext;
    }
    
    protected abstract JAXBContext getJaxbContext() throws JAXBException;
}
