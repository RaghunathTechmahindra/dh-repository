/**
 * <p>Title: NoReqRequestPayloadData.java</p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Nov 23, 2006)
 *
 */
package com.filogix.externallinks.framework;

/*
 *  <p>abstruct class for payload for requests that isn't stored in DB so 
 *  that no requestId is used
 */
public abstract class NoStoredTransactionData
{
  protected int _dealId;  

  /**
   * <p>NoStoredTransationData</p>
   * Constructor
   */
  public NoStoredTransactionData()
  {
    super();
  }

  public int getDealId()
  {
    return _dealId;
  }

  public void setDealId(int id)
  {
    _dealId = id;
  }
}
