package com.filogix.externallinks.framework;

import com.basis100.deal.entity.Request;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

public class ServiceResponseBase {
  
  protected SessionResourceKit _srk;
  protected int                _requestId;
  protected int                _copyId;
  protected int                _responseId;
  protected SysLogger          _logger;
  protected Object             _responseBean;
  protected String             _responseXml;
  protected Request            _request; 
  
  public ServiceResponseBase(SessionResourceKit srk, int requestId, int copyId) throws RemoteException, FinderException{
    _srk = srk;
    _requestId = requestId;
    _copyId = copyId;
    _logger = ResourceManager.getSysLogger("SrvcRspnBs");
    _logger.debug("Initialized");
    
    _request = new Request(_srk, _requestId, copyId);
  }

  public Object getResponseBean() {
    return _responseBean;
  }

  public void setResponseBean(Object bean) {
    _responseBean = bean;

  }

  public Request getRequest() {
    return _request;
  }

  public String getResponseXml() {
    return _responseXml;
  }

  public void setResponseXml(String xml) {
    _responseXml = xml;
  }
  
}
