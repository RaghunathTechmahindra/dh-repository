/**
 * <p>Title: JaxbServiceResponse.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 24, 2006)
 *
 */

package com.filogix.externallinks.framework;

import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;

public class JaxbServiceResponse extends ServiceResponseBase
{
    public JaxbServiceResponse(SessionResourceKit srk, int requestId, int copyId) 
        throws RemoteException, FinderException
    {
        super(srk, requestId, copyId);
    }
    
    public String getXmlPayload(Object payload, JAXBContext jaxbContext)
        throws JAXBException
    {
        if (jaxbContext == null ) return null;
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter xmlWriter = new StringWriter();
        marshaller.marshal(payload, xmlWriter);
        return xmlWriter.toString();       
    }
}
