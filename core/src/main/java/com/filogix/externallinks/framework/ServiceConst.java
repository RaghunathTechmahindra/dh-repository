package com.filogix.externallinks.framework;

public interface ServiceConst {

  /**
   * test constants
   * will be eventually moved to Drools and table
   */

  public static final String CONST_UNKNOWN = "Unknown";
  public static final String CONST_NA = "N/A";
  // -------------------------------  SERVICE TYPES
  public static final int SERVICE_TYPE_ADJUDICATION = 1;
  public static final int SERVICE_TYPE_CIF = 2;
  public static final int SERVICE_TYPE_CLOSING = 3;
  public static final int SERVICE_TYPE_CREDIT_BUREAU = 4;
  public static final int SERVICE_TYPE_MI = 5;
  public static final int SERVICE_TYPE_VALUATION = 6;
  
  //since 4.2
  public static final int SERVICE_TYPE_CONDITION_STATUS_UPDATE = 8;
  public static final int SERVICE_TYPE_DEALEVENT_STATUS_UPDATE = 9;
  public static final int SERVICE_TYPE_LOANDECISION_UPDATE = 11;
  public static final int SERVICE_TYPE_MI_GENERIC = 12; //5.0
  
  // -------------------------------  SERVICE SUB TYPES
  public static final int SERVICE_SUB_TYPE_AVM = 1;
  public static final int SERVICE_SUB_TYPE_APPRAISAL = 2;
  public static final int SERVICE_SUB_TYPE_FULL_MI = 4;
  public static final int SERVICE_SUB_TYPE_FIXED_PRICE_CLOSING = 3;
  public static final int SERVICE_SUB_TYPE_LOW_RATIO_ASSESSMENT = 5;
  public static final int SERVICE_SUB_TYPE_OUTSORCED_CLOSING = 6;
  public static final int SERVICE_SUB_TYPE_PREMIUM_CALC = 7;
  public static final int SERVICE_SUB_TYPE_PREQUALIFICATION = 8;
//Appraisal READ change
  public static final int SERVICE_SUB_TYPE_READ_APPRAISAL = 10;
  
  // -------------------------------  TRANSACTION TYPES
  public static final int SERVICE_TRANSACTION_TYPE_REQUEST = 1;
  public static final int SERVICE_TRANSACTION_TYPE_UPDATE = 2;
  public static final int SERVICE_TRANSACTION_TYPE_CANCEL = 3;
  
  // -------------------------------  SERVICE PAYLOAD TYPES
  public static final int SERVICE_PAYLOAD_TYPE_DEAL_UPLOAD = 1;
  public static final int SERVICE_PAYLOAD_TYPE_APPROVAL_UPLOAD = 2;
  public static final int SERVICE_PAYLOAD_TYPE_MI_REQUIRED = 3;
  public static final int SERVICE_PAYLOAD_TYPE_MI_CANCELLED = 4;
  public static final int SERVICE_PAYLOAD_TYPE_DENIAL_UPLOAD = 5;
  public static final int SERVICE_PAYLOAD_TYPE_FUNDING_UPLOAD = 6;
  public static final int SERVICE_PAYLOAD_TYPE_CCAPS_UPLOAD = 7;
  public static final int SERVICE_PAYLOAD_TYPE_GEMONEY_UPLOAD = 8;
  public static final int SERVICE_PAYLOAD_TYPE_CLOSING_UPLOAD = 9;
  public static final int SERVICE_PAYLOAD_TYPE_SRVC_REQUEST = 10;
  public static final int SERVICE_PAYLOAD_TYPE_SRVC_CANCEL = 11;
  public static final int SERVICE_PAYLOAD_TYPE_APP_REQUEST = 12;
  public static final int SERVICE_PAYLOAD_TYPE_APP_CANCEL = 13;
  public static final int SERVICE_PAYLOAD_TYPE_OSC_REQUEST = 20;
  public static final int SERVICE_PAYLOAD_TYPE_OSC_CANCEL = 21;
//Appraisal READ Change
  public static final int SERVICE_PAYLOAD_TYPE_READ_APP_REQUEST = 22;
//Address Scrub 
  public static final int SERVICE_PAYLOAD_TYPE_ADDR_SCRUB = 24;
//Exchange2 Folder Creation
  public static final int SERVICE_PAYLOAD_TYPE_EXCHANGE2 = 25;
  
  
  // -------------------------------  CHANNEL TYPES
  public static final int SERVICE_CHANNLE_TYPE_DATX = 1;
  public static final int SERVICE_CHANNEL_ID_ADDR_SCRUB = 10;
  public static final int SERVICE_CHANNEL_ID_EXCHANGE2 = 16;

  // -------------------------------  REQUEST TYPES
  public static final int REQUEST_TYPE_AVM_A_INT = 1;
  public static final int REQUEST_TYPE_AVM_B_INT = 2;
  public static final int REQUEST_TYPE_AVM_C_INT = 3;
  
  public static final int REQUEST_TYPE_CLOSING_FIXED_REQUEST_INT = 11;
//public static final int REQUEST_TYPE_CLOSING_FIXED_CANCEL_INT = 5;
  public static final int REQUEST_TYPE_CLOSING_OUTSOURCED_REQUEST_INT = 17;
  public static final int REQUEST_TYPE_CLOSING_OUTSOURCED_CANCEL_INT = 18;
  
  public static final int REQUEST_TYPE_MI_INT = 8;
  public static final int REQUEST_TYPE_APPRAISAL_REQUEST_INT = 8;
  public static final int REQUEST_TYPE_APPRAISAL_FULL_REQUEST_INT = 8;
  public static final int REQUEST_TYPE_APPRAISAL_DRIVEBY_REQUEST_INT = 9;
  public static final int REQUEST_TYPE_APPRAISAL_FULL_CANCEL_INT = 15;
  public static final int REQUEST_TYPE_APPRAISAL_DRIVEBY_CANCEL_INT = 16;
  
  
  public static final String REQUEST_TYPE_AVM_STR = "AVM";
  public static final String REQUEST_TYPE_APPRAISAL_REQUEST_STR = "APP_REQ";
  public static final String REQUEST_TYPE_APPRAISAL_CANCEL_STR = "APP_CAN";
  public static final String REQUEST_TYPE_CLOSING_FIXED_REQUEST_STR = "CLOF_REQ";  
  
  public static final String REQUEST_TYPE_CLOSING_OSC_REQUEST_STR = "OSC_REQ";
  public static final String REQUEST_TYPE_CLOSING_OSC_CANCEL_STR = "OSC_CAN";
    
  public static final int LANG_ENGLISH = 0;
  public static final int LANG_FRENCH = 1;
  
  // -------------------------------  REQUEST/REPSONSE statuses
  public static final int RESPONSE_STATUS_BLANK = 0;
  public static final int RESPONSE_STATUS_NOT_SUBMITTED = 1;
  public static final int RESPONSE_STATUS_REQUEST_ACCEPTED = 2;
  public static final int RESPONSE_STATUS_JOB_ASSIGNED = 3;
  public static final int RESPONSE_STATUS_JOB_DELAYED = 4;
  public static final int RESPONSE_STATUS_INSPECTION_COMPLETED = 5;
  public static final int RESPONSE_STATUS_COMPLETED = 6;
  public static final int RESPONSE_STATUS_COMPLETED_CONFIRMED = 7;
  public static final int RESPONSE_STATUS_CLOSING_CONFIRMED = 8;
  public static final int RESPONSE_STATUS_SYSTEM_ERROR = 9;
  public static final int RESPONSE_STATUS_PROCESSING_ERROR = 10;
  public static final int RESPONSE_STATUS_CANCELLATION_NOT_SUBMITTED = 11;
  public static final int RESPONSE_STATUS_CANCELLATION_REQUESTED = 12;
  public static final int RESPONSE_STATUS_CANCELLATION_ACCEPTED = 13;
  public static final int RESPONSE_STATUS_CANCELLATION_CONFIRMED = 14;
  public static final int RESPONSE_STATUS_CANCELLATION_REJECTED = 15;
  public static final int RESPONSE_STATUS_QUOTATION_RECEIVED = 16;
  public static final int RESPONSE_STATUS_QUOTATION_CONFIRMED = 17;
  public static final int RESPONSE_STATUS_FAILED = 18;

  public static final int REQUEST_STATUS_BLANK = 0;
  public static final int REQUEST_STATUS_NOT_SUBMITTED = 1;
  public static final int REQUEST_STATUS_REQUEST_ACCEPTED = 2;
  public static final int REQUEST_STATUS_JOB_ASSIGNED = 3;
  public static final int REQUEST_STATUS_JOB_DELAYED = 4;
  public static final int REQUEST_STATUS_INSPECTION_COMPLETED = 5;
  public static final int REQUEST_STATUS_COMPLETED = 6;
  public static final int REQUEST_STATUS_COMPLETED_CONFIRMED = 7;
  public static final int REQUEST_STATUS_CLOSING_CONFIRMED = 8;
  public static final int REQUEST_STATUS_SYSTEM_ERROR = 9;
  public static final int REQUEST_STATUS_PROCESSING_ERROR = 10;
  public static final int REQUEST_STATUS_CANCELLATION_NOT_SUBMITTED = 11;
  public static final int REQUEST_STATUS_CANCELLATION_REQUESTED = 12;
  public static final int REQUEST_STATUS_CANCELLATION_ACCEPTED = 13;
  public static final int REQUEST_STATUS_CANCELLATION_CONFIRMED = 14;
  public static final int REQUEST_STATUS_CANCELLATION_REJECTED = 15;
  public static final int REQUEST_STATUS_QUOTATION_RECEIVED = 16;
  public static final int REQUEST_STATUS_QUOTATION_CONFIRMED = 17;
  public static final int REQUEST_STATUS_FAILED = 18;
//Appraisal READ Change
  public static final int REQUEST_STATUS_SUBMIT_TO_READ = 19;

  public static final int RESULT_CODE_BLANK = -1;
  public static final int RESULT_CODE_SUCCEED = 0;
  public static final int RESULT_CODE_NOT_SUBMITTED = -1;
  public static final int RESULT_CODE_REQUEST_ACCEPTED = 1;
  public static final int RESULT_CODE_JOB_ASSIGNED = 9;
  public static final int RESULT_CODE_JOB_DELAYED = 10;
  public static final int RESULT_CODE_INSPECTION_COMPLETED = 13;
  public static final int RESULT_CODE_COMPLETED = 16;
  public static final int RESULT_CODE_COMPLETED_CONFIRMED = 7;
  public static final int RESULT_CODE_CLOSING_CONFIRMED = 7;
  public static final int RESULT_CODE_SYSTEM_ERROR = 11;
  public static final int RESULT_CODE_PROCESSING_ERROR = 12;
  public static final int RESULT_CODE_CANCELLATION_NOT_SUBMITTED = -1;
  public static final int RESULT_CODE_CANCELLATION_REQUESTED = 17;
  public static final int RESULT_CODE_CANCELLATION_ACCEPTED = 18;
  public static final int RESULT_CODE_CANCELLATION_CONFIRMED = -1;
  public static final int RESULT_CODE_CANCELLATION_REJECTED = 19;
  public static final int RESULT_CODE_QUOTATION_RECEIVED = 20;
  public static final int RESULT_CODE_QUOTATION_CONFIRMED = 21;
  public static final int RESULT_CODE_FAILED = -1;
  
  public static final String RESULT_DESC_SUCCEED = "SUCCESS";
  
  // ---------- result codes for closing
  
  public static final int RESULT_CODE_RECEIVED = 4;
  public static final int RESULT_CODE_CLOSING_CANCELLATION_CONFIRMED = 18;

  // ---------- end test constants
  
  public static final String DATX_AVM_REQUESTING_PARTY_PROPERTY_NAME = "com.filogix.datx.services.requestingpartyid";
  public static final String DATX_AVM_ACCOUNT_ID_PROPERTY_NAME = "com.filogix.datx.services.accountid"; 
  
  // Message key
  public static final String CLOSING_REQ_ACCEPTED_MSG_DH = "CLOSING_REQ_ACCEPTED_MSG_DH";
  public static final String EXTERNAL_LINK_SYSTEM_ERROR = "EXTERNAL_LINK_SYSTEM_ERROR";
  
  // Contact point types
  public static final String OSC_CONTACT_POINT_TYPE_HOME = "Home";
  public static final String OSC_CONTACT_POINT_TYPE_WORK = "Work";
  public static final String OSC_CONTACT_POINT_TYPE_MOBILE = "Mobile";
  public static final String COUNTRY_CANADA = "Canada";
  
  public static final String BORROWER_INDICATOR_PRIMARY = "Primary";
  public static final String BORROWER_INDICATOR_COAPPLICANT = "Co-Applicant";
  public static final String BORROWER_INDICATOR_OTHER = "Other";
  // Schema String ristriction
  public static final String LANGUAGE_INDICATOR_ENGLISH = "English";
  public static final String LANGUAGE_INDICATOR_FRENCH = "French";  
  
  public static final String TRANSACTION_TYPE_REQUEST = "Request"; 
  public static final String TRANSACTION_TYPE_UPDATE = "Update"; 
  public static final String TRANSACTION_TYPE_CANCEL = "Cancel"; 
  
  public static final String CONTACT_POINT_TYPE_HOME = "Home";
  public static final String CONTACT_POINT_TYPE_MOBILE = "Mobile";
  public static final String CONTACT_POINT_TYPE_WORK = "Work";
  
  public static final String PRIMARY_BORROWER_INDICATOR_PRIMARY = "Primary";
  public static final String PRIMARY_BORROWER_INDICATOR_COAPPLICANT = "Co-Applicant";
  public static final String PRIMARY_BORROWER_INDICATOR_OTHER = "Other";

  public static final String CONST_Y = "Y";
  public static final String CONST_N = "N";
  public static final String BORROWER_TYPE_BORROWER = "Borrower";
  public static final String BORROWER_TYPE_GUARANTOR = "Guarantor";

  public static final String MARITAL_STATUS_TYPE_SINGLE= "Single";
  public static final String MARITAL_STATUS_TYPE_MARRIED= "Married";
  public static final String MARITAL_STATUS_TYPE_COMMONLOW= "Common Law";
  public static final String MARITAL_STATUS_TYPE_SEPARATED= "Separated";
  public static final String MARITAL_STATUS_TYPE_DIVORCED= "Divorced";
  public static final String MARITAL_STATUS_TYPE_WIDOWED= "Widowed";
  public static final String MARITAL_STATUS_TYPE_UNKNOWN= "Unknown";
  public static final String MARITAL_STATUS_TYPE_OTHER= "Other";
  public static final String MARITAL_STATUS_TYPE_NULL= "";
  
  public static final String ADDRESS_ATTR_TYPE_CURRENT = "Current";
  public static final String ADDRESS_ATTR_TYPE_PREVIOUS = "Previous";

  public static final String TAX_PAYOR_BORROWER = "Borrower";
  public static final String TAX_PAYOR_LENDER = "Lender";

  public static final String MI_PROVIDER_CMHC = "CMHC";
  public static final String MI_PROVIDER_GENWORTH = "Genworth";
  public static final String MI_PROVIDER_AIGUG = "AIG UG";
  
  public static final String PAY_OFF_TYPE_REMAINS = "Remains";
  public static final String PAY_OFF_TYPE_PRIOR_TO_CLOSING = "Prior to Closing";
  public static final String PAY_OFF_TYPE_FROM_PROCEEDS = "From Proceeds";
  
  public static final String[] REQUEST_TRANSACTION_TYPE_CLOSING_STR = {"createClosingRequest", "submitOutsourcedClosingUpdateRequest", "cancelClosingRequest" };
  public static final String SERVICE_PRODUCT_CLOSING = "CLOSING";
  
  public static final int PROPERTY_TENURE_TYPE_FREEHOLD = 1;
  public static final int PROPERTY_TENURE_TYPE_LEASEHOLD = 2;
  public static final int PROPERTY_TENURE_TYPE_CONDO = 3;
  
//Appraisal READ Change
//Adding Service Product entries for READ
  public static final int SERVICE_PRODUCT_READ_APPRAISAL = 25;
  
 
  /***** transaction status id for dscu daemon start *****/
  public static final int ESBOUT_TRANSACTIONSTATUS_SUCCESS = 0;
  public static final int ESBOUT_TRANSACTIONSTATUS_TIMEOUT = 1;
  public static final int ESBOUT_TRANSACTIONSTATUS_SOAPERROR = 2;
  public static final int ESBOUT_TRANSACTIONSTATUS_AAAPERROR = 3;
  public static final int ESBOUT_TRANSACTIONSTATUS_XMLERROR = 4;
  
  public static final int ESBOUT_TRANSACTIONSTATUS_AVALIABLE = 9;
  public static final int ESBOUT_TRANSACTIONSTATUS_PROCESSING = 8;
  
  public static final int ESBOUT_TRANSACTIONSTATUS_SYSTEMERROR = 99;

  /***** transaction status id for dscu daemon end *****/
  
  public static final int SERVICE_SUBTYPE_LOANDECISION_COMMITMENT = 1;
  public static final int SERVICE_SUBTYPE_LOANDECISION_DENIAL = 2;
  public static final int SERVICE_SUBTYPE_LOANDECISION_HOLD = 3;
  
}