package com.filogix.externallinks.services.channel.ug;

import java.net.*;
import com.filogix.datx.framework.packaging.*;
import com.filogix.datx.mi.*;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.*;
import org.apache.commons.logging.*;
import org.apache.axis.client.Stub;

/**
 * UGPremiumRequestChannel
 * 
 * @author: Lin Zhou
 * @date: July 21, 2006
 */
public class UGPremiumRequestChannel extends UGMIChannel {
	private static Log logger = LogFactory
			.getLog(UGPremiumRequestChannel.class);

	private static AIGUGPremiumRequestService service;

	private UGPremiumRequestChannel() {
	}

	public static IChannel getInstance() {
		return new UGPremiumRequestChannel();
	}

	protected FXResponse transmit(ServiceInfo serviceInfo, String xmlPayload) throws Exception {
		FXResponse response = service.invoke(construct(serviceInfo), xmlPayload);
		System.out.println("Prem Response - " + response);
		return response;
	}

	public void init(ServiceInfo serviceInfo) throws Exception {
		logger.debug("Service - " + service);
		setupChannelInfo(serviceInfo);
		logger.debug("Endpoint - " + getEndpoint());
		if (service == null) {
			if (getEndpoint() != null) {
				try {
					service = (new AIGUGPremiumRequestServiceServiceLocator())
							.getAIGUGPremiumRequestService(new URL(getEndpoint()));
					((Stub) service).setTimeout(getWsTimeout() * 1000); 
				} catch (Exception e) {
					logger.error(this.getClass().getName()
							+ ": initialization failed.");
					throw new Exception(this.getClass().getName()
							+ ": initialization failed.");
				}
			} else {
				logger.error(this.getClass().getName()
						+ ": endpoint is not specified.");
				throw new Exception(this.getClass().getName()
						+ ": endpoint is not specified.");
			}
		}
	}

	
}
