/**
 * <p>Title: DatxServiceCancelPayloadData.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Jun 25, 2006)
 *
 */

package com.filogix.externallinks.services.datx;

import com.basis100.deal.entity.Response;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbServicePayloadData;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.schema.datx.ServiceCancel;
import com.filogix.schema.datx.impl.ServiceCancelImpl;;

public class DatxServiceCancelPayloadData
        extends JaxbServicePayloadData
{
    public ServiceCancel  serviceCancelPayload;
    public Response          responseEntity;
    
    public DatxServiceCancelPayloadData(SessionResourceKit srk, int requestId, int borrowerId, int copyId, int langId) 
        throws RemoteException, ExternalServicesException
    {
        super(srk, requestId, borrowerId, copyId, langId);
        initServiceCancel();
    }

    public DatxServiceCancelPayloadData(SessionResourceKit srk, int requestId, int copyId, int langId) 
        throws RemoteException, ExternalServicesException
    {
        super(srk, requestId, copyId, langId);
        initServiceCancel();
    }

    public void initServiceCancel()
        throws ExternalServicesException
    {
        serviceCancelPayload = new ServiceCancelImpl();
        serviceCancelPayload.setApplicationReferenceNumber(""+getDealId());
        serviceCancelPayload.setTransactionReferenceNumber(""+getRequestId());
    }

    public ServiceCancel getServiceCancelPayload()
    {
        return (ServiceCancel)getServicePayload();
    }
    
    
    public Object getServicePayload()
    {
        return serviceCancelPayload;
    }
    // this methods need to change every time ServiceCancel requires for any service
    // currently Only handle Appraisal and OSC
    protected boolean requireServiceRequestContact()
    {
        try
        {
      	  String reqTypeStr = _request.getRequestTypeShortName(_requestId, _copyId).trim();
            return (!reqTypeStr.equals(ServiceConst.REQUEST_TYPE_CLOSING_OSC_CANCEL_STR));
        }
        catch (Exception e)
        {
      	  _logger.debug("Exception in DatxOscPayloadData@requireServiceRequestContact() requestId: " + _requestId);
      	  return false;
        }
    }

}
