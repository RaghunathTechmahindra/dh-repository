package com.filogix.externallinks.services.channel;

import java.util.Map;

import MosSystem.Mc;
import com.basis100.picklist.BXResources;
import com.filogix.datx.framework.packaging.FXResponse;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.services.RequestService;
import com.filogix.externallinks.services.ServiceInfo;

public abstract class SimpleChannel extends Channel {

	private Map webServices;
	
	public void setWebServices(Map webServices) {
		this.webServices = webServices;
	}
	
	public Map getWebServices() {
		return this.webServices;
	}
}
