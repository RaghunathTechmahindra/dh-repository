/**
 * <p>Title: DatxAppraisalPayloadGenerator.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 16, 2006)
 *
 */

package com.filogix.externallinks.services.datx;

import java.io.StringWriter;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.w3c.dom.DOMException;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.ServiceProvider;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.ResourceManager;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbServicePayloadGenerator;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServicePayloadData;
import com.filogix.externallinks.framework.ServicePayloadGeneratorBase;
import com.filogix.schema.datx.ContactInformationType;
import com.filogix.schema.datx.appraisal.AppraisalRequestType;

public class DatxAppReqPayloadGenerator
        extends JaxbServicePayloadGenerator
{
    public static String _packageName = AppraisalRequestType.class.getClass().getPackage().getName();
    
    // CR, 7-Aug-06: addition for language support  --- begin
    public ServicePayloadData getPayloadData() throws RemoteException, FinderException, ExternalServicesException {
      _data = getPayloadData(ServiceConst.LANG_ENGLISH);
      
      return _data;  
    }
    // CR, 7-Aug-06: addition for language support  --- end

    public ServicePayloadData getPayloadData(int langId) throws RemoteException,  FinderException,    ExternalServicesException
    {
        _data = new DatxAppReqPayloadData(_srk, _requestId, _copyId, langId);
        DatxAppReqPayloadData appReqPayloadData = (DatxAppReqPayloadData)_data;
        AppraisalRequestType appReqPayload = appReqPayloadData.getAppraisalRequestPayload();
        
        // 1) set the ServiceRequest to Data
        try
        {
            setServiceRequestToData();
        }
        catch(Exception e)
        {
            throw new FinderException();
        }
               
        // 2) set Requester(under writer)
        setRequesterCmnTypes();
        appReqPayload.setRequester(appReqPayloadData.getJaxbRequester());
        
        // 3) set property
        setPropertyCmnTypes();
        appReqPayload.setProperty(appReqPayloadData.getJaxbProperty());
        
        // 4) set borrowers
        setBorrowersCmnTypes();
        List bs = appReqPayloadData.getJaxbBorrower();
        for (int i=0; i<bs.size(); i++)
        {
            appReqPayload.getBorrower().add(appReqPayloadData.getJaxbBorrower().get(i));
        }
     
        
        // 5) other info
        setProvider(appReqPayload);
        setProduct(appReqPayload);
        setAppraisalType(appReqPayload);

        // 6) set serviceContact - primary borrower
        setContactInfoCmnTypes();
            appReqPayload.setAppraisalContactInformation(appReqPayloadData.getJaxbContactInfo());
        if(appReqPayload.getAppraisalContactInformation().getFirstName() == null)
            appReqPayload.getAppraisalContactInformation().setFirstName("");
        if(appReqPayload.getAppraisalContactInformation().getLastName()==null)
            appReqPayload.getAppraisalContactInformation().setLastName("");
        
        
        // according FS, only English is supported for now
        appReqPayload.setAppraisalReportLanguagePreference(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "LANGUAGEPREFERENCE", langId, ServiceConst.LANG_ENGLISH));
        
        appReqPayload.setMortgagePurpose(_data.getMortgagePurpose());
        appReqPayload.setApplicationReferenceNumber(""+_data.getDealId());

        return _data;
    }

    
    private void setProvider(AppraisalRequestType appReqPayload) throws RemoteException, FinderException
    {
        ServiceProvider serviceProvider = _data.getServiceProvider();
        appReqPayload.setAppraisalProviderName(serviceProvider.getServiceProviderName());
    }

    private void setProduct(AppraisalRequestType appReqPayload) throws FinderException, RemoteException
    {
        ServiceProduct serviceProduct = _data.getServiceProduct();
        appReqPayload.setAppraisalProductName(serviceProduct.getServiceSubTypeName());
    }
    
    private void setAppraisalType(AppraisalRequestType appReqPayload) throws RemoteException, FinderException
    {
        ServiceProduct serviceProduct = _data.getServiceProduct();
        appReqPayload.setAppraisalType( serviceProduct.getServiceProductName() );
        
    }
    public String getPackageName()
    {
        return _packageName;
    }
    
    public JAXBContext getJaxbContext()
    {
        return DatxAppReqChannel._jaxbContext;
    }

}
