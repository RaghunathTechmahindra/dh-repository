/**
 * <p>Title: DatxServiceCancelChannel.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Jun 25, 2006)
 *
 */

package com.filogix.externallinks.services.datx;

import javax.xml.bind.JAXBContext;
import com.basis100.deal.entity.Response;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.filogix.externallinks.framework.JaxbServiceResponse;
import com.filogix.externallinks.framework.ServicePayloadData;
import com.filogix.externallinks.framework.ServiceResponseBase;
import com.filogix.externallinks.services.wsdl.appraisal.AppraisalService_PortType;
import com.filogix.externallinks.services.wsdl.appraisal.AppraisalService_ServiceLocator;
import com.filogix.externallinks.services.wsdl.appraisal.DatXRequest;
import com.filogix.externallinks.services.wsdl.appraisal.DatXResponse;

public class DatxAppCanChannel extends DatxAppReqChannel
{
    public ServiceResponseBase sendData(ServicePayloadData data) throws Exception
    {
        _data = data;
        try
        {
            prepare(_data);

            AppraisalService_ServiceLocator servLoc = getServiceLocater();
            if (servLoc==null)
            {
                String msg = "DatXAppReqChannel: DatX service is not available!";
                _logger.error(msg);
                throw new DatxException(msg);
            }
            AppraisalService_PortType stub = getServiceStub(servLoc);
            if (stub==null)
            {
                String msg = "DatXAppReqChannel: DatX service is not available!";
                _logger.error(msg);
                throw new DatxException(msg);
            }
            _logger.debug("Sending Appraisal request --->>> ");
            _logger.debug("Datx Request Client Key: " + _datXRequest.getClientKey());
            _logger.debug("Datx Request Datx Key: " + _datXRequest.getDatXKey());
            _logger.debug("Datx Request Password: " + _datXRequest.getPassword());
            _logger.debug("Datx Request SchemaName: " + _datXRequest.getSchemaName());
            _logger.debug("Datx Request UserName: " + _datXRequest.getUsername());
            _logger.debug("Datx Request TimeOut: " + _datXRequest.getTimeout());
            _logger.debug("Datx Request Aux Data: " + _datXRequest.getAuxData());
            _logger.debug("xmlPayload --->>");
            _logger.debug(_xmlPayload);
            System.out.println(_xmlPayload);
            DatXResponse response = stub.cancelAppraisalRequest(_datXRequest, _xmlPayload);
            if (response != null)
            {
                _logger.debug("---- Receiving response for Appraisal Cancel Request --->>> ");
                _logger.debug("Datx Response Result Code: " + response.getResultCode());
                _logger.debug("Datx Response Result Desc: " + response.getResultDescription());
                _logger.debug("Datx Response External Ref Number: " + response.getExternalKey());
                // this is only acknowdlegement
                JaxbServiceResponse result = new JaxbServiceResponse(_srk, _requestId, _copyId);
                result.setResponseBean(response);
                return result;
            }
            else
            {
                _logger.error(this.getClass(), "DatXResponse is null for request id " + _datXRequest.getClientKey());
                throw new DatxException("DatXResponse is null for request id " + _datXRequest.getClientKey());
            }
        } 
        catch (Exception e)
        {
          String msg = "Error when sending service request to DatX: RequestId = " + this._requestId;
          _logger.error(msg + ": "+ StringUtil.stack2string(e));
          throw new DatxException(msg + ": " + e);
        }
    }
    
    protected JAXBContext getJaxbContext()
    {
        return getCancelJaxbContext();
    }
    
    private DatXResponse getStubResponse(DatXRequest datXRequest, String payload) throws Exception
    {
        Response responseEntity = new Response(_srk);
        responseEntity = responseEntity.findLastResponseByRequestId(new Integer(datXRequest.getClientKey()).intValue());
        
        DatXResponse response = new DatXResponse();
        response.setClientKey(datXRequest.getClientKey());
        response.setDatXKey(datXRequest.getDatXKey());
        response.setExternalKey(responseEntity.getChannelTransactionKey());
        response.setResultCode(new Integer(18));
        response.setResultDescription("0:SUCCEED");
        return response;
    }


}
