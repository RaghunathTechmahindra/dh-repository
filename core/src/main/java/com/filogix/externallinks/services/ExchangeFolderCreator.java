package com.filogix.externallinks.services;

import java.io.IOException;

import MosSystem.Mc;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.entity.DCConfig;
import com.basis100.deal.entity.DcFolder;
import com.basis100.deal.entity.Deal;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressCoreContext;
import com.filogix.externallinks.framework.ExternalServicesException;

import com.filogix.util.Xc;

public class ExchangeFolderCreator {

    protected SessionResourceKit srk;
    protected static String SPACE = " ";
    protected int lang = 0;
    protected SysLogger logger;
    protected Deal deal;
    protected JdbcExecutor jExec;
	  
    private IExchangeFolderCreator creator = null;
    /**
     * Constructor: please specify SessionResourceKit to initialize the class
     * @param srk SessionResourceKit
     */
    
    public ExchangeFolderCreator(SessionResourceKit srk) throws IOException {
	    
        this.srk = srk;
        jExec = srk.getJdbcExecutor();
        logger = ResourceManager.getSysLogger( "ExchangeFolderCreator" );
	    
        String exchangeversionStr = PropertiesCache.getInstance().getProperty(
                srk.getExpressState().getDealInstitutionId(), Xc.EXCHANGE_VERSION, "0");
        logger.debug("EXCHANG VERSION: " + exchangeversionStr);
        if (creator == null && !"0".equalsIgnoreCase(exchangeversionStr)) {
            creator = (IExchangeFolderCreator) ExpressCoreContext.
            getService(Mc.EXCHANGE_FOLDERCREATOR + exchangeversionStr.trim());
            creator.init(srk, logger, deal, jExec); 
        }
    }
	  
    /**
       * This method returns Doc Central 'Folder Creation Indicator' <br>
       * valid values <br>
       *    = 0 - Do not create folder; <br>
       *    = 1 - create on submission;  <br>
       *    = 2 - create on approval; <br>
       *
       * @throws FinderException
       * @throws RemoteException
       * @return int
       */
    public int getFolderCreationIndicator() throws FinderException, RemoteException {
		
        int result = 0;                            // 0 - do not create DC folder
        DCConfig cnfg = new DCConfig(srk, 1);      // can only be one record with id=1 for each institution
        result = cnfg.getDcFolderCreationId();

        return result;
    }

    public boolean folderExists(Deal deal) throws RemoteException {
        
        this.deal = deal;
        boolean result = false;

        // 1) check DCImaging table
        DcFolder dcf = new DcFolder(srk);
        try {
            dcf = dcf.findBySourceAppId(deal.getSourceApplicationId());
            result =  (dcf != null);
        } catch (FinderException fe) {
            result = false;
        }

	    return result;
	  }

	  // Catherne, 14-Jul-05, fix for #1517 ----- begin -----------------  
    public String getFolderCode(Deal deal) throws RemoteException {
        String result = ""; 
        DcFolder dcf = new DcFolder(srk);
        try {
            dcf.setSilentMode(true);
            dcf = dcf.findBySourceAppId(deal.getSourceApplicationId());
            if (dcf != null) {
                result = dcf.getDcFolderCode();
            }
        } catch (FinderException fe) {
        }
        return result;
    }

    public String createFolder(Deal deal) throws ExternalServicesException, DocPrepException, JdbcTransactionException {
        if (creator != null) 
            return creator.createFolder(deal);
        else return null;
    }

}
