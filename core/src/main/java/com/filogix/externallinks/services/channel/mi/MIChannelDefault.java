package com.filogix.externallinks.services.channel.mi;

import java.net.SocketTimeoutException;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMNode;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.OperationClient;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.MessageContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.Axis2BaseChannel;
import com.filogix.externallinks.services.channel.ChannelRequestConnectivityException;

public class MIChannelDefault extends Axis2BaseChannel {

	private final static Logger logger = LoggerFactory
			.getLogger(MIChannelDefault.class);

	public Object send(ServiceInfo serviceInfo, String payload)
			throws ExternalServicesException,
			ChannelRequestConnectivityException, 
			SocketTimeoutException {

		OperationClient opClient = null;

		try {
			ServiceClient serClient = new ServiceClient();

			setupMetaData(serClient, serviceInfo);
			serClient.setTargetEPR(new EndpointReference(getEndpoint()));

			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace omNs = factory.createOMNamespace(XMLNS_MP_ESB_SCHEMAS, "sch");

			OMElement request = factory.createOMElement("request", omNs);
			OMElement document = factory.createOMElement("document", omNs);
			OMElement data = factory.createOMElement("data", omNs);
			request.addChild(document);
			document.addChild(data);

			data.addChild(AXIOMUtil.stringToOM(payload));

			opClient = serClient.createClient(ServiceClient.ANON_OUT_IN_OP);

			MessageContext outMsgCtx = new MessageContext();


			SOAPFactory soapFactory = OMAbstractFactory.getSOAP11Factory();
			SOAPEnvelope envelope = soapFactory.getDefaultEnvelope();
			envelope.getBody().addChild(request);

//request from Dave Krick - add dealId to soap header
			OMNamespace fxrNs = factory.createOMNamespace(XMLNS_MP_FXR_REQUEST, "fxr");
			String dealHeader = "<dealID>" + serviceInfo.getDeal().getDealId() + "</dealID>";
			OMElement fxrRequest = factory.createOMElement("request", fxrNs);
			OMNode dealIdNode = AXIOMUtil.stringToOM(dealHeader);
			fxrRequest.addChild(dealIdNode);
			
			envelope.getHeader().addChild(fxrRequest);
//end request for DK.

			serClient.addHeadersToEnvelope(envelope);
			outMsgCtx.setEnvelope(envelope);

			opClient.addMessageContext(outMsgCtx);

		} catch (AxisFault e) {
			String msg = "failed to prepare MI submission";
			logger.error(msg, e);
			throw new ExternalServicesException(msg, e);

		} catch (XMLStreamException e) {
			String msg = "failed to attach payload XML to soap envelope";
			logger.error(msg, e);
			throw new ExternalServicesException(msg, e);
		}

		try {
			opClient.execute(true);
		} catch (AxisFault af) {
			if(af.getCause() instanceof SocketTimeoutException) {
				throw (SocketTimeoutException)af.getCause();
			}
			logger.error("failed to submit MI request", af);
			handleRequestSendingException(af);
		}

		try {
			MessageContext inMsgCtx = opClient.getMessageContext("In");
			SOAPEnvelope envelope = inMsgCtx.getEnvelope();

			OMElement EndpointReference = getChildElement(envelope.getHeader(), XMLNS_WSA, "EndpointReference");
			OMElement ReferenceProperties = getChildElement(EndpointReference, XMLNS_WSA, "ReferenceProperties");
			OMElement transactionId = getChildElement(ReferenceProperties, XMLNS_MP_ESB_HEADERS, "transactionId");

			// get marketplace's transactionid
			logger.debug("returned transactionid:" + transactionId.getText());
			//Dave was here - 443, async responses not working, this was overwriting wsa:MessageID
			//serviceInfo.setChannelTransactionKey(transactionId.getText());

			OMElement response = getChildElement(envelope.getBody(), XMLNS_MP_ESB_SCHEMAS, "response");
			OMElement document = getChildElement(response, XMLNS_MP_ESB_SCHEMAS, "document");
			OMElement data = getChildElement(document, XMLNS_MP_ESB_SCHEMAS, "data");

			return data.getFirstElement().toString();

		} catch (AxisFault e) {
			String msg = "failed to process response after MI submission";
			logger.error(msg, e);
			throw new ExternalServicesException(msg, e);
		}
	}

	private static OMElement getChildElement(OMElement parent,
			String namespaceURI, String localPart) throws ExternalServicesException {
		
		OMElement child = parent.getFirstChildWithName(new QName(namespaceURI, localPart));
		if (child == null) {
			throw new ExternalServicesException("the element <" + namespaceURI + ":" + localPart + "> doesn't exist");
		}
		return child;
	}
}
