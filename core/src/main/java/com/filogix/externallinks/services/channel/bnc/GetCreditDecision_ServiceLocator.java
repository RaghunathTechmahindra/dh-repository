/**
 * GetCreditDecision_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package com.filogix.externallinks.services.channel.bnc;

public class GetCreditDecision_ServiceLocator extends org.apache.axis.client.Service implements com.filogix.externallinks.services.channel.bnc.GetCreditDecision_Service {

    public GetCreditDecision_ServiceLocator() {
    }


    public GetCreditDecision_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetCreditDecision_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetCreditDecision
    private java.lang.String GetCreditDecision_address = "http://10.1.1.225:8080/GCD/services/GetCreditDecision";

    public java.lang.String getGetCreditDecisionAddress() {
        return GetCreditDecision_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetCreditDecisionWSDDServiceName = "GetCreditDecision";

    public java.lang.String getGetCreditDecisionWSDDServiceName() {
        return GetCreditDecisionWSDDServiceName;
    }

    public void setGetCreditDecisionWSDDServiceName(java.lang.String name) {
        GetCreditDecisionWSDDServiceName = name;
    }

    public com.filogix.externallinks.services.channel.bnc.GetCreditDecision_PortType getGetCreditDecision() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetCreditDecision_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetCreditDecision(endpoint);
    }

    public com.filogix.externallinks.services.channel.bnc.GetCreditDecision_PortType getGetCreditDecision(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.filogix.externallinks.services.channel.bnc.GetCreditDecisionSoapBindingStub _stub = new com.filogix.externallinks.services.channel.bnc.GetCreditDecisionSoapBindingStub(portAddress, this);
            _stub.setPortName(getGetCreditDecisionWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetCreditDecisionEndpointAddress(java.lang.String address) {
        GetCreditDecision_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.filogix.externallinks.services.channel.bnc.GetCreditDecision_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.filogix.externallinks.services.channel.bnc.GetCreditDecisionSoapBindingStub _stub = new com.filogix.externallinks.services.channel.bnc.GetCreditDecisionSoapBindingStub(new java.net.URL(GetCreditDecision_address), this);
                _stub.setPortName(getGetCreditDecisionWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetCreditDecision".equals(inputPortName)) {
            return getGetCreditDecision();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.filogix.com/datx", "GetCreditDecision");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.filogix.com/datx", "GetCreditDecision"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetCreditDecision".equals(portName)) {
            setGetCreditDecisionEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
