package com.filogix.externallinks.services.datx;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import MosSystem.Mc;
import MosSystem.Sc;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.pk.ServiceProductPk;
import com.basis100.deal.util.BXStringTokenizer;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.filogix.datx.router.service.FXResponse;

public class AcknowledgementClosingResProcessor
    extends AcknowledgementResProcessor
{

  protected DatxResponseCodeConverterBase getResponseCodeConverter()
  {
    return new DatxResponseCodeClosingConverter();
  }
  
  protected String getResultDescription(Object acknowledgement)
  {
      FXResponse myAcknowledgement = (FXResponse)acknowledgement;
    return myAcknowledgement.getResultDescription();
  }
  protected Integer getResultCode(Object acknowledgement)
  {
	  FXResponse myAcknowledgement = (FXResponse)acknowledgement;
    return myAcknowledgement.getResultCode();
  }
  protected String getDatXKey(Object acknowledgement)
  {
	  FXResponse myAcknowledgement = (FXResponse)acknowledgement;
    return myAcknowledgement.getServiceKey();
  }
  protected String getExternalKey(Object acknowledgement)
  {
	  FXResponse myAcknowledgement = (FXResponse)acknowledgement;
    return myAcknowledgement.getExternalKey();
  }
  
  protected void insertDealHistory() throws RemoteException, FinderException, CreateException
  {
	if (_statusId != RESPONSE_STATUS_REQUEST_ACCEPTED) return;
	
    Request request = _base.getRequest();
    ServiceProduct serviceProduct = new ServiceProduct(_srk);
    serviceProduct = serviceProduct.findByPrimaryKey(new ServiceProductPk(request.getServiceProductId()));
    
    // dealHistory needs to be added only for Fixed Price Closing
    if( serviceProduct.getServiceTypeId() == SERVICE_SUB_TYPE_FIXED_PRICE_CLOSING)
    {
        Deal deal = new Deal(_srk, null);
        // get deal from deal table that is S or G for the request
    	int dealId = request.getDealId();
    	int copyId =  request.getCopyId();
//        Collection requestEntities = request.findRequestsByRequestId(request.getRequestId());
        try
        {
            deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));
//            Iterator iter = requestEntities.iterator();
//            while (iter.hasNext())
//            {
//                Request requestEntity = (Request)iter.next();
//                dealId = requestEntity.getDealId();
//                copyId = requestEntity.getCopyId();
//                deal = new Deal(_srk, null);
//                deal =  deal.findByPrimaryKey( new DealPK(dealId, copyId));
//                if ("S".equals(deal.getCopyType()) || "G".equals(deal.getCopyType()))
//                {
//                    break;
//                }
//            }
        }
        catch(FinderException fe)
        {
            _logger.trace(fe.getMessage());
            throw fe;
        }
        int providerId = request.getServiceProduct().getServiceProviderId();
        String serviceProviderName = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "SERVICEPROVIDER", providerId, _langId);
        String transactionText = BXStringTokenizer.replace
            ( BXResources.getSysMsg(CLOSING_REQ_ACCEPTED_MSG_DH, _langId), "%s", serviceProviderName);
        DealHistory dealHistory = new DealHistory(_srk);
    
        dealHistory.create(request.getDealId(), deal.getUnderwriterUserId(), Mc.DEAL_TX_TYPE_EVENT, deal.getStatusId(), transactionText, new Date());
        dealHistory.ejbStore();
    }
  }
}
