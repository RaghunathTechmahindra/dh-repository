package com.filogix.externallinks.services.payload.mi;

import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.dhltd.marketplace.mi.jaxb.request.BranchProfileType;
import com.dhltd.marketplace.mi.jaxb.request.DealType;
import com.dhltd.marketplace.mi.jaxb.request.InstitutionProfileType;
import com.filogix.externallinks.services.util.XMLUtil;

public class MIRequestPayloadGeneratorCMHC extends MIRequestPayloadGenerator {

    @Override
    protected void populateInstitutionProfile(InstitutionProfileType institutionProfileType, InstitutionProfile institutionProfile) {

        super.populateInstitutionProfile(institutionProfileType, institutionProfile);

        if (!XMLUtil.isEmpty(institutionProfile.getCMHCId())) {
            institutionProfileType.setMIProviderInstititionCode(institutionProfile.getCMHCId());
        }
    }

    @Override
    protected void populateBranchProfile(BranchProfile branchProfile, BranchProfileType branchProfileType) {

        super.populateBranchProfile(branchProfile, branchProfileType);

        if (!XMLUtil.isEmpty(branchProfile.getCMHCBranchTransitNumber())) {
            branchProfileType.setMIProviderBranchTransitNumber(branchProfile.getCMHCBranchTransitNumber());
        }
    }

    @Override
    protected void populateDealType(DealType dealType, Deal deal) {

        super.populateDealType(dealType, deal);

        if (!XMLUtil.isEmpty(deal.getCmhcProductTrackerIdentifier())) {
            dealType.setProductTrackerIdentification(deal.getCmhcProductTrackerIdentifier());
        }
        
        if (!XMLUtil.isEmpty(deal.getMIPolicyNumCMHC())) {
            dealType.setMIPolicyNumber(deal.getMIPolicyNumCMHC());
        } else {
            if (!XMLUtil.isEmpty(deal.getMIPolicyNumber())) {
                dealType.setMIPolicyNumber(deal.getMIPolicyNumber());
            }
        }
    }

}
