package com.filogix.externallinks.services.responseprocessor;

public class ResponseOutdatedException extends Exception {

    private static final long serialVersionUID = 1L;

    public ResponseOutdatedException() {
    }

    public ResponseOutdatedException(String message) {
        super(message);
    }

    public ResponseOutdatedException(Throwable cause) {
        super(cause);
    }

    public ResponseOutdatedException(String message, Throwable cause) {
        super(message, cause);
    }

}
