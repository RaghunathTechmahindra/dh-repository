package com.filogix.externallinks.services.datx;

import javax.xml.bind.JAXBContext;


public class DatXClosingCanCannel extends DatXClosingChannel
{
    public DatXClosingCanCannel()
    {
    	super();
    }
    
	protected JAXBContext getJaxbContext()
    {
        return getCancelJaxbContext();
    }
    
    protected String getLogMessage()
    {
    	return "---- Receiving Acknowledgment response for Closing Cancel Request --->>> ";
    }
	
}
