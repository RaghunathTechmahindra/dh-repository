package com.filogix.externallinks.services.payload;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.mi.pmi.extended.jaxb.BorrowerType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.BranchProfileType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.ContactType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.DownPaymentSourceType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.InstitutionProfileType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.MtgProdType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.PropertyType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.UnderwriterType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.DealType;
import com.filogix.externallinks.mi.pmi.extended.jaxb.PremiumRequest;
import com.filogix.externallinks.mi.pmi.extended.jaxb.ObjectFactory;
import com.filogix.externallinks.mi.pmi.extended.jaxb.BorrowerType.IncomeType;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.util.XMLUtil;

public class MIPremiumExtendedRequestGenerator implements IPayloadGenerator {
	
    private static Log logger = LogFactory.getLog(MIPremiumExtendedRequestGenerator.class);
    protected static ObjectFactory objFactory = new ObjectFactory();
    private static JAXBContext jc;
    protected static final String schema = "com.filogix.externallinks.mi.pmi.extended.jaxb";
    private static IPayloadGenerator instance = new MIPremiumExtendedRequestGenerator();
    
    static {
        try {
            jc = JAXBContext.newInstance(schema);
        } catch (JAXBException e) {
            logger.error(
                    "MIPremiumExtendedRequestGenerator: JAXBContext initialization failed.");
        }
    }
    
    public static IPayloadGenerator getInstance() {
        return instance;
    }
    
    private MIPremiumExtendedRequestGenerator() {
    	
    }
	
	public String getXMLPayload(boolean validate, ServiceInfo info) throws Exception {
		SessionResourceKit srk = info.getSrk();
		Deal deal = new Deal(srk, info.getCalcMonitor(), info.getDealId().intValue(),
                     		info.getCopyId().intValue());
		info.setDeal(deal);
		return getXMLPayload(validate, srk, deal);
	}
	
    protected String getXMLPayload(boolean validate, SessionResourceKit srk, Deal deal) throws Exception {
    	return getXMLPayload(getExtendedPremiumRequest(srk, deal), validate);
    }
    
    protected PremiumRequest getExtendedPremiumRequest(SessionResourceKit srk, Deal deal) throws Exception {
    	if (deal != null && srk != null) {
	        PremiumRequest req = objFactory.createPremiumRequest();
	        req.setDeal(createDealType(srk, deal));
	        return req;
	    } 
    	else {
	        logger.error(this.getClass().getName() + ": ServiceInfo problem.");
	        throw new Exception(this.getClass().getName() + ": ServiceInfo problem.");
	    }
	}
    
    protected boolean validate(PremiumRequest req) {
        boolean retVal = false;

        try {
            retVal = jc.createValidator().validate(req);
        } catch (Exception e) {
            logger.error(this.getClass().getName() + ": validation failed.");
        }

        return retVal;
    }
    
    protected String getXMLPayload(PremiumRequest req, boolean validate) throws Exception {
    	Marshaller marshaller = jc.createMarshaller();
    	marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    	//marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
    	//marshaller.setProperty(
    	//        "com.sun.xml.bind.characterEscapeHandler",
    	//        new CustomCharacterEscapeHandler() );  
    	if (!validate) {
    		return XMLUtil.jaxb2String(marshaller, req);
    	} 
    	else {
    		if (validate(req)) {
    			return XMLUtil.jaxb2String(marshaller, req);
    		} 
    		else {
    			throw new Exception(this.getClass().getName() + ": validation failed.");
    		}
    	}
    }
    
    protected DealType createDealType(SessionResourceKit srk, Deal deal) throws Exception {
    	
    	if (logger.isDebugEnabled()) {
    		logger.debug("*** MIPremiumExtendedRequestGenerator:: createDealType ***");
    	}
    	
    	DealType dealType = objFactory.createDeal();
    	populateDealType(dealType, srk, deal);
    	
    	createBranchProfile(dealType, srk, deal);
        createBorrower(dealType, srk, deal);
        createDownPaymentSource(dealType, srk, deal);
        createMtgProd(dealType, srk, deal);
        createInstitutionProfile(dealType, srk, deal);
        createUnderwriter(dealType, srk, deal);
        createProperty(dealType, srk, deal);
        return dealType;
    	
    }
    
    protected void populateDealType(DealType dealType, SessionResourceKit srk,
            Deal deal) throws Exception {
		if (!XMLUtil.isEmpty(deal.getAmortizationTerm())) {
		    dealType.setAmortizationTerm(XMLUtil.int2BigInteger(deal.getAmortizationTerm()));
		}
		
		dealType.setCombinedLTV(XMLUtil.double2BigDecimal(deal.getCombinedLTV()));
		dealType.setDealId(XMLUtil.int2BigInteger(deal.getDealId()));
		dealType.setDealPurposeId(XMLUtil.int2BigInteger(deal.getDealPurposeId()));
		
		if (!XMLUtil.isEmpty(deal.getExistingLoanAmount())) {
		    dealType.setExistingLoanAmount(XMLUtil.double2BigDecimal(deal.getExistingLoanAmount()));
		}
		
		dealType.setLienPositionId(XMLUtil.int2BigInteger(deal.getLienPositionId()));
		
		if (!XMLUtil.isEmpty(deal.getLocRepaymentTypeId())) {
			dealType.setLOCRepaymentTypeId(XMLUtil.int2BigInteger(deal.getLocRepaymentTypeId()));
		}
		
		if (!XMLUtil.isEmpty(deal.getMIExistingPolicyNumber())) {
			dealType.setMIExistingPolicyNumber(deal.getMIExistingPolicyNumber());
		}
		
		if (!XMLUtil.isEmpty(deal.getMITypeId())) {
			dealType.setMortgageInsuranceTypeId(XMLUtil.int2BigInteger(deal.getMITypeId()));
		}
		
		dealType.setNetLoanAmount(XMLUtil.double2BigDecimal(deal.getNetLoanAmount()));
		
		if (!XMLUtil.isEmpty(deal.getNextAdvanceAmount())) {
			dealType.setNextAdvanceAmount(XMLUtil.double2BigDecimal(deal.getNextAdvanceAmount()));
		}
		
		if (!XMLUtil.isEmpty(deal.getProductTypeId())) {
			dealType.setProductTypeId(XMLUtil.int2BigInteger(deal.getProductTypeId()));
		}
		
		if (!XMLUtil.isEmpty(deal.getProgressAdvance())) {
			dealType.setProgressAdvance(deal.getProgressAdvance());
		}
		
		if (!XMLUtil.isEmpty(deal.getProgressAdvanceInspectionBy())) {
			dealType.setProgressAdvanceInspectionBy(deal.getProgressAdvanceInspectionBy());
		}
		
		if (!XMLUtil.isEmpty(deal.getProgressAdvanceTypeId())) {
			dealType.setProgressAdvanceTypeId(XMLUtil.int2BigInteger(deal.getProgressAdvanceTypeId()));
		}
		
		if (!XMLUtil.isEmpty(deal.getSelfDirectedRRSP())) {
			dealType.setSelfDirectedRRSP(deal.getSelfDirectedRRSP());
		}
		
       // New fields for Extended Premium Request
		if (!XMLUtil.isEmpty(deal.getActualPaymentTerm())) {
			dealType.setActualPaymentTerm(XMLUtil.int2BigInteger(deal.getActualPaymentTerm()));
		}
		if (!XMLUtil.isEmpty(deal.getNetInterestRate())) {
			dealType.setNetInterestRate(XMLUtil.double2BigDecimal(deal.getNetInterestRate()));
		}
		if (!XMLUtil.isEmpty(deal.getIncomeVerificationTypeId())) {
			dealType.setIncomeVerificationTypeId(XMLUtil.int2BigInteger(deal.getIncomeVerificationTypeId()));
		}
		if (!XMLUtil.isEmpty(deal.getRepaymentTypeId())) {
			dealType.setRepaymentTypeId(XMLUtil.int2BigInteger(deal.getRepaymentTypeId()));
		}
		if (!XMLUtil.isEmpty(deal.getApplicationDate())) {
			dealType.setApplicationDate(XMLUtil.date2Calendar(deal.getApplicationDate()));
		}
		if (!XMLUtil.isEmpty(deal.getCombinedTDSBorrower())) {
			dealType.setCombinedTDSBorrower(XMLUtil.double2BigDecimal(deal.getCombinedTDSBorrower()));
		}
		if (!XMLUtil.isEmpty(deal.getCombinedTDS())) {
			dealType.setCombinedTDS(XMLUtil.double2BigDecimal(deal.getCombinedTDS()));
		}
		
    }
    
    protected void createBorrower(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
    	Collection borrowers = deal.getBorrowers();

    	if (borrowers != null) {
    		for (Iterator iter = borrowers.iterator(); iter.hasNext(); ) {
    			Borrower borrower = (Borrower) iter.next();
    			BorrowerType borrowerType = objFactory.createBorrower();
    			List incomeTypes = borrowerType.getIncome();
    			
    			if(borrower.getStaffOfLender()) {
    			    borrowerType.setStaffOfLender("Y");
    			}
    			else {
    				borrowerType.setStaffOfLender("N");	
    			}
    			if (!XMLUtil.isEmpty(borrower.getPrimaryBorrowerFlag())) {
    				borrowerType.setPrimaryBorrowerFlag(borrower.getPrimaryBorrowerFlag());
    			}
    			if (!XMLUtil.isEmpty(borrower.getBorrowerTypeId())) {
    				borrowerType.setBorrowerTypeId(XMLUtil.int2BigInteger(borrower.getBorrowerTypeId()));
    			}
    			if (!XMLUtil.isEmpty(borrower.getCreditScore())) {
    				borrowerType.setCreditScore(XMLUtil.int2BigInteger(borrower.getCreditScore()));
    			}
    			if (!XMLUtil.isEmpty(borrower.getTDS())) {
    				borrowerType.setTDS(XMLUtil.double2BigDecimal(borrower.getTDS()));
    			}
    			
    			ArrayList incomes = (ArrayList)borrower.getIncomes();
    			if(incomes != null && incomes.size() > 0) {
    				
    				for(int i = 0; i < incomes.size(); i++) {
    					Income income = (Income)incomes.get(i);
    					int incomeTypeId = income.getIncomeTypeId();
    					
    					IncomeType incomeType = objFactory.createBorrowerTypeIncomeType();
    					incomeType.setIncomeTypeId(XMLUtil.int2BigInteger(incomeTypeId));
    					
    					incomeTypes.add(incomeType);
    					
    				}
    				
    			}
    			
    			dealType.getBorrower().add(borrowerType);
    			
    		}
    	}
    }


    protected void createBranchProfile(DealType dealType, SessionResourceKit srk,
                 													Deal deal) throws Exception {
    	BranchProfile bp = new BranchProfile(srk, deal.getBranchProfileId());
    	BranchProfileType branchProfileType = objFactory.createBranchProfileType();
    	branchProfileType.setMIProviderBranchTransitNumber(bp.getPMIBranchTransitNumber());
    	dealType.setBranchProfile(branchProfileType);
    }

    protected void createDownPaymentSource(DealType dealType,
                     								SessionResourceKit srk, Deal deal) throws Exception {
    	Collection downPaymentSources = deal.getDownPaymentSources();

    	if (downPaymentSources != null) {
    		DownPaymentSourceType downPaymentSourceType = objFactory.
    		createDownPaymentSourceType();

    		for (Iterator iter = downPaymentSources.iterator(); iter.hasNext(); ) {
    			DownPaymentSource dps = (DownPaymentSource) iter.next();
    			if (dps.getDownPaymentSourceTypeId() == 2) {
    				downPaymentSourceType.setDownPaymentSourceTypeId(XMLUtil.int2BigInteger(2));
    				dealType.setDownPaymentSource(downPaymentSourceType);
    				break;
    			}
    		}
    	}
    }

	 void createInstitutionProfile(DealType dealType, SessionResourceKit srk, Deal deal) throws
	 																					Exception {
		 InstitutionProfile institutionProfile = new InstitutionProfile(srk);
		 institutionProfile = institutionProfile.findByFirst();
		 InstitutionProfileType institutionProfileType = objFactory.createInstitutionProfileType();
		 institutionProfileType.setMIProviderId(institutionProfile.getPMIId());
		 dealType.setInstitutionProfile(institutionProfileType);
	 }

	 protected void createMtgProd(DealType dealType, SessionResourceKit srk,
			 												Deal deal) throws Exception {
		 if (deal.getMtgProd() != null) {
			 if (!XMLUtil.isEmpty(deal.getMtgProd().getInterestTypeId())) {
				 MtgProdType mtgProdType = objFactory.createMtgProdType();

				 if (!XMLUtil.isEmpty(deal.getMtgProd().getInterestTypeId())) {
					 mtgProdType.setInterestTypeId(XMLUtil.int2BigInteger(deal.getMtgProd().
							 													getInterestTypeId()));
					 dealType.setMtgProd(mtgProdType);
				 }
			 }
		 }
	 }


	 protected void createProperty(DealType dealType, SessionResourceKit srk,
			 														Deal deal) throws Exception {
		 List propertyTypes = dealType.getProperty();
		 Collection properties = deal.getProperties();

		 if (properties != null) {
			 for (Iterator iter = properties.iterator(); iter.hasNext(); ) {
				 Property property = (Property) iter.next();
				 PropertyType propertyType = objFactory.createPropertyType();

				 if (!XMLUtil.isEmpty(property.getPrimaryPropertyFlag())) {
					 propertyType.setPrimaryPropertyFlag(property.getPrimaryPropertyFlag() + "");
				 }

				 if (!XMLUtil.isEmpty(property.getNumberOfUnits())) {
					 propertyType.setNumberOfUnits(XMLUtil.int2BigInteger(property.getNumberOfUnits()));
				 }

				 if (!XMLUtil.isEmpty(property.getPropertyUsageId())) {
					 propertyType.setPropertyUsageId(XMLUtil.int2BigInteger(property.getPropertyUsageId()));
				 }

				 if (!XMLUtil.isEmpty(property.getProvinceId())) {
					 propertyType.setProvinceId(XMLUtil.int2BigInteger(property.getProvinceId()));
				 }
				 // New fields for Extended Premium Request
				 if(!XMLUtil.isEmpty(property.getPropertyTypeId())) {
					 propertyType.setPropertyTypeId(XMLUtil.int2BigInteger(property.getPropertyTypeId()));
				 }
				 if(!XMLUtil.isEmpty(property.getDwellingTypeId())) {
					 propertyType.setDwellingTypeId(XMLUtil.int2BigInteger(property.getDwellingTypeId()));
				 }
				 if(!XMLUtil.isEmpty(property.getOccupancyTypeId())) {
					 propertyType.setOccupancyTypeId(XMLUtil.int2BigInteger(property.getOccupancyTypeId()));
				 }
				 if(!XMLUtil.isEmpty(property.getPurchasePrice())) {
					 propertyType.setPurchasePrice(XMLUtil.double2BigDecimal(property.getPurchasePrice()));
				 }
				 if(!XMLUtil.isEmpty(property.getActualAppraisalValue())) {
					 propertyType.setActualAppraisalValue(XMLUtil.double2BigDecimal(property.getActualAppraisalValue()));
				 }
				 if(!XMLUtil.isEmpty(property.getAppraisalSourceId())) {
					 propertyType.setAppraisalSourceId(XMLUtil.int2BigInteger(property.getAppraisalSourceId()));
				 }

				 propertyTypes.add(propertyType);
			 }
		 }
	 }

	 protected void createUnderwriter(DealType dealType, SessionResourceKit srk,
               													Deal deal) throws Exception {
		 UserProfile userProfile = new UserProfile(srk);
	     userProfile.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId(), 
                                                               deal.getInstitutionProfileId()));


		 if (userProfile != null) {
			 ContactType contactType = objFactory.createContact();
			 contactType.setLanguagePreferenceId(XMLUtil.int2BigInteger(userProfile.getUserLanguageId()));
			 UnderwriterType underwriterType = objFactory.createUnderwriterType();
			 underwriterType.setContact(contactType);
			 dealType.setUnderwriter(underwriterType);
		 }
	 }

}
