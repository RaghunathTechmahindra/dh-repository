package com.filogix.externallinks.services;

import java.util.UUID;

import com.filogix.externallinks.framework.ExternalServicesException;

public class WSS4JUUIDGenerator extends UUIDGenerator {

	public String getUUID() throws ExternalServicesException {
		// TODO Auto-generated method stub
		try {
			//String uuid = org.apache.ws.security.util.UUIDGenerator.getUUID();
			//return uuid;
			String uuid = UUID.randomUUID().toString();
			return uuid = "urn:uuid:" + uuid;		
			
		} catch (Exception e) {
			_log.debug("Couldn't get UUIDGenerator");
			_log.debug(e);
			throw new ExternalServicesException(e.getMessage());
		}
 	}

}