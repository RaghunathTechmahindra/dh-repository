package com.filogix.externallinks.services;

import com.filogix.externallinks.framework.ExternalServicesException;

public interface IUUIDGenerator {
	
	public String getUUID() throws ExternalServicesException;

}
