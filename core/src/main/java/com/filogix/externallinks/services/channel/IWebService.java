package com.filogix.externallinks.services.channel;

import java.rmi.Remote;

public interface IWebService {
	Remote getWebService(String serviceName);
}
