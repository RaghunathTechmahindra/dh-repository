package com.filogix.externallinks.services.esb;

import java.io.IOException;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.DCConfig;
import com.basis100.deal.entity.DcFolder;
import com.basis100.deal.entity.Deal;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.exchange.LenderFolderResponse;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.IExchangeFolderCreator;
import com.filogix.externallinks.services.channel.esb.Exchange2FolderCreationChannel;

public class Exchange2Service implements IExchangeFolderCreator {

	  // ============== constants  ==============
	  private static String DC_FOLDER_ID_KEY_NAME = "FOLDER_ID";
	  private static String DC_STATUS_FAILED = "FAILED";
	  private static String DC_STATUS_COMPLETED = "COMPLETED";
	  private static String DC_STATUS_COMPLETED_DESCRIPTION = "BLITZDOCS executed successfully.";
	  private String onExceptionContact = "catherine.rutgaizer@filogix.com";

	  // ============== constants  ==============

	  private Exchange2FolderCreationChannel folderCreationChannel;
	  private SessionResourceKit srk;
	  private static String SPACE = " ";
	  private int lang = 0;
	  private SysLogger logger;
	  private Deal deal;
	  private JdbcExecutor jExec;

      public Exchange2Service() throws
          IOException {
          logger = ResourceManager.getSysLogger( "Exchange2Service" );          
      }
//	  /**
//	   * Constructor: please specify SessionResourceKit to initialize the class
//	   * @param srk SessionResourceKit
//	   */
//	  public Exchange2Service(SessionResourceKit srk) throws
//	      IOException {
//	    this.srk = srk;
//	    jExec = srk.getJdbcExecutor();
//
//	  }

      public void init(SessionResourceKit srk, SysLogger logger, Deal deal, JdbcExecutor jExec) {
          this.srk = srk;
          this.logger = logger;
          this.deal = deal;
          this.jExec = jExec;
          jExec = srk.getJdbcExecutor();
    }


	  /**
	   * This method creates Doc Central folder
	   * @param deal Deal
	   * @throws DocPrepException - in case of DatX error or Doc Central request failure DocPrep exception is thrown.
	   * @return String - this is FOLDER_ID returned by Doc Central / DatX. <br>
	   * Note: it is unknown at that moment if this value is strictly numeric or might be alpha-numeric,
	   * that is why it is defined as 'String'
	   */
	  public String createFolder(Deal deal) 
	  	throws ExternalServicesException, JdbcTransactionException, DocPrepException {
	    this.deal = deal;
	    String result = null;

	    try {

	      if (!satisfiesConditions(deal)) {
	        throw new ExternalServicesException(
	            "Folder not created because conditions are not satisfied. ");
	      }

	      // 1) set data
	      // doesn't need since no payload for the new request

	      // 2) send request
	      try {
	        result = sendAndProcessRequest();
	      }
	      catch (Error er) {
	        String msg = "Exchange2Service.CreateFolder: " + "Error: [" + er.getClass() + " Messge = " + er.toString() + "]";
	        throw new ExternalServicesException(msg);
	      }
	      catch (Exception e) {
	        String msg = e.toString();
	        throw new ExternalServicesException(msg);
	      }
	    } catch (ExternalServicesException dcie) {
	      String poolname = PropertiesCache.getInstance().getInstanceProperty("com.basis100.resource.connectionpoolname");
	      sendEmailAlert( "Error creating Folder in filogix exchange repository, " + poolname + " environment \n",
	                      "Could not create folder in filogix exchange repository"+ poolname + ". \n" +
	                      "Error = [" + dcie.getClass() + " " + dcie.getMessage() + "]; " + "\n" +
	                      "Deal = " + deal.getDealId() + "; Source Application id = " + deal.getSourceApplicationId()+ "\n"
	                    ); // text, subject
	      logger.error(dcie.getMessage());
	    } catch (Exception e){
	      logger.error(e.getMessage());
	      e.printStackTrace();
	    }
	    return result;
	  }

	  /**
	   * This method returns Doc Central 'Folder Creation Indicator' <br>
	   * valid values <br>
	   *    = 0 - Do not create folder; <br>
	   *    = 1 - create on submission; <br>
	   *    = 2 - create on approval; <br>
	   * @throws FinderException
	   * @throws RemoteException
	   * @return int
	   */
	  public int getDCFolderCreationIndicator() throws FinderException, RemoteException {
	    int result = 0;                                                      // 0 - do not create DC folder
	    DCConfig cnfg = new DCConfig(srk, 1);                                // 1 is hard coded
	    result = cnfg.getDcFolderCreationId();

	    return result;
	  }

	  public boolean dcFolderExists(Deal deal) throws RemoteException {
	    this.deal = deal;                                                     /** @todo consolidate this */
	    boolean result = false;

	    /** @todo add functionality here */
	    // 1) check DCImaging table
	    DcFolder dcf = new DcFolder(srk);
	    try {
	      dcf = dcf.findBySourceAppId(deal.getSourceApplicationId());
	      result =  (dcf != null);
	    } catch (FinderException fe) {
	      result = false;
	    }

	    // 2) try to call Doc Central, if it returns and ID then the folder already exists
	    /**@todo is there an Advectis funtion that returns true if folder already exists? */
	    /*
	    try {
	      createFolder(deal);
	    } catch (Exception e) {

	    }
	    */
	    return result;
	  }

	  // Catherne, 14-Jul-05, fix for #1517 ----- begin -----------------
	  public String getFolderCode(Deal deal) throws RemoteException {
	    String result = "";
	    DcFolder dcf = new DcFolder(srk);
	    try {
	      dcf = dcf.findBySourceAppId(deal.getSourceApplicationId());
	      if (dcf != null) {
	        result = dcf.getDcFolderCode();
	      }
	    } catch (FinderException fe) {
	    }
	    return result;
	  }
	  // Catherne, 14-Jul-05, fix for #1517 ----- begin -----------------

//	  private void setRequestData(Deal deal) throws FinderException,
//	      RemoteException, ExternalServicesException
//	  {
//	    String val = null;
//
//	    requestBean = new LenderFolder();
//
//	    Exchange2Config cnfg = new Exchange2Config(srk, 1);                                // 1 is hard coded
//
//	    // -------------- these are set up by Advectis --------------------
//	    /** 1) DOCCENTRALCONFIG.DCREQUESTINGPARTY
//	    /* "com.filogix.mb" for production MB
//	    /* "com.filogix.exp" for production Expert
//	    */
//	    val = cnfg.getDcRequestingParty();
//	    logger.debug("getDcRequestingParty() = " + val);
//	    if (val == null){ val = "com.filogix.fxp";}
//	    requestBean.setRequestingPartyId(val);
//
//	    /** 2) DOCCENTRALCONFIG.DCRECEIVINGPARTY
//	    /* "com.blitzdocs" for production,
//	    /* "com.blitzdocs.staging" for UAT / QA
//	    /* Joe's example : net.blitzdocs.folder
//	    */
//	    val = cnfg.getDcReceivingParty();
//	    logger.debug("getDcReceivingParty() = " + val);
//	    if (val == null){ val = "net.blitzdocs.folder";}
//	    requestBean.setReceivingPartyId(val);
//
//	    // 3)
//	    requestBean.setAccountIdentifier(cnfg.getDcAccountIdentifier());
//	    logger.debug("getDcAccountIdentifier() = " + cnfg.getDcAccountIdentifier());
//	    requestBean.setAccountPassword(cnfg.getDcAccountPassword());
//	    logger.debug("getDcAccountPassword() = " + cnfg.getDcAccountPassword());
//
//	    // 4)
//	    requestBean.setFolderConfigurationID(cnfg.getDcFolderConfigId());
//	    logger.trace("getDcFolderConfigId() = "+ cnfg.getDcFolderConfigId());
//	    requestBean.setLenderCode(cnfg.getDcLenderId());
//	    logger.debug("getDcLenderId() = "+ cnfg.getDcLenderId());
//	    // -------------- these are set up by Advectis --------------------
//
//	    // 5)
//	    // Advectis system folder Id : lender_code + channel_code + POS_App_code
//	    // channel: DEAL.SYSTEMTYPEID -> SYSTEMTYPE.DCCHANNELCODE ('MB' or 'EXP')
//
//	    int sysType = deal.getSystemTypeId();
//
//	    String channelCode = getChannelCode(sysType);
//	    if (channelCode == null) {
//	      throw new DocPrepException ("Channel code is null. Please set up SYSTEMTYPE.DCCHANNELCODE for SYSTEMTYPEID = " + sysType);
//	    }
//	    String branchTransitNum = getGebBranchTransit(deal.getBranchProfileId());
//	    String sfi = branchTransitNum +"."+ channelCode +"."+ deal.getSourceApplicationId();
//	    logger.trace("SystemFolderId = " + sfi);
//
//	    requestBean.setExternalSystemFolderID(sfi);  // Test 41MBBMSB001-00972-F
//
//	    // 6) POS application number
//	    requestBean.setApplicationID(deal.getSourceApplicationId());
//	    logger.trace("ApplicationId = " + deal.getSourceApplicationId());
//
//	    // 7) primary borrower's info
//	    Borrower bBorrower = null;
//	    try
//	    {
//	      bBorrower = new Borrower(srk, null);
//	      bBorrower = bBorrower.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
//	    }
//	    catch (FinderException fe)
//	    {
//	       logger.error(fe.getClass() + ": " + fe.getMessage());
//	    }
//
//	    lang = bBorrower.getLanguagePreferenceId();
//
//	    requestBean.setBorrowerFirstName(bBorrower.getBorrowerFirstName());
//	    requestBean.setBorrowerLastName(bBorrower.getBorrowerLastName());
//
//	    // 8) primary property
//	    Property pProperty = null;
//
//	    try {
//	      pProperty = new Property(srk, null);
//	      pProperty = pProperty.findByPrimaryProperty(deal.getDealId(), deal.getCopyId());
//	    }
//	    catch (Exception fe) {
//	      logger.error(fe.getClass() + ": " + fe.getMessage());
//	    }
//
//	    requestBean.setPropertyAddressLine1(extractAddressLine1(pProperty, lang));
//
//	    val = BXResources.getPickListDescription("ProvinceShortName", pProperty.getProvinceId(), lang);
//	    if (val != null) {
//	      requestBean.setPropertyProvinceAbbreviation(val);
//	    }
//	  }
//
//	  private String getChannelCode(int sysTypeId) {
//	    String result = "";
//
//	    String sql = "SELECT DCCHANNELCODE FROM SYSTEMTYPE WHERE SYSTEMTYPEID = " + sysTypeId;
//
//	    boolean gotRecord = false;
//
//	    try
//	    {
//	        int key = jExec.execute(sql);
//	        for (; jExec.next(key); )
//	        {
//	            gotRecord = true;
//	            result = jExec.getString(key, 1);
//	            break;                                                  // can only be one record
//	        }
//
//	        jExec.closeData(key);
//	        if (gotRecord == false)
//	        {
//	          String msg = "DocCentralInterface.getChannelCode(), channel code not found";
//	          logger.error(msg);
//	        }
//	      }
//	      catch (Exception e)
//	      {
//	          FinderException fe = new FinderException("DocCentralInterface.getChannelCode() exception." );
//	          logger.error(fe.getMessage());
//	          logger.error("finder sql: " + sql);
//	          logger.error(e);
//	      }
//	    return result;
//	  }

//	  private String getGebBranchTransit(int branchId) throws FinderException,
//	      RemoteException {
//
//	     String result = "";
//	     BranchProfilePK pk = new BranchProfilePK(branchId);
//	     BranchProfile bp = new BranchProfile(srk);
//	     bp = bp.findByPrimaryKey(pk);
//
//	     if (bp != null){
//	       result = bp.getGEBranchTransitNumber();
//	     }
//	     logger.trace("Branch GEB transit number = " + result);
//
//	     return result;
//	  }
//
	  private boolean satisfiesConditions(Deal deal){
	    // put any additional conditions here
	    return true;
	  }

	  // 1) submit request
	  private String sendAndProcessRequest() throws Exception {
	    String result = null;

	    Exchange2FolderCreationChannel channel = new Exchange2FolderCreationChannel();
	    try {
	    	channel.init(srk, deal.getDealId(),deal.getCopyId(), "System");
	    } catch (Exception e) {
		      logger.error(e.getMessage());
		      logger.error("error during init Reqiest");
		      throw new ExternalServicesException("error during init Reqiest");	    		    	
	    }
	    LenderFolderResponse response = null;
	    try {
	    	response = (LenderFolderResponse)channel.sendData(null);
	    } catch (Exception e) {
		      logger.error(e.getMessage());
		      logger.error("error from ESB");
		      throw new ExternalServicesException("error from ESB");	    	
	    }
	    // 2) check the response
	    if(response  == null) {
		      logger.error("No Key found in response");
	      throw new ExternalServicesException("No Responise from ESB");
	    }

	    String folderId = response.getDealFolderID();
	    String folderName = response.getDealFoldername();

	    if (folderId==null || folderId.trim().length() == 0) {
		      logger.error("No DealFolderId found in response");
		      throw new ExternalServicesException("No DealFolderId found in response");
		    }

	    if (folderName==null || folderName.trim().length() == 0) {
		      logger.error("No DealFolderName found in response");
		      throw new ExternalServicesException("No DealFolderName found in response");
		    }

	      result = folderId;

	    // 3) save results in the Imaging table
	    try {
	      saveResults(null, folderId, DC_STATUS_COMPLETED, DC_STATUS_COMPLETED_DESCRIPTION);
	    } catch (Exception re){
	      logger.error("Cannot save in DCImagingReference :"+ re.getClass() + ": " + re.getMessage() );
	    }
	    return result;
	  }

	  private void saveResults(String keyName, String keyValue, String statusCode, String statusDesc)
	      throws RemoteException, CreateException, RemoteException {

	    DcFolder dcf = new DcFolder(srk);
	    // int dealId, String folderCode, String folderStatusCode, String folderStatusDesc

	      dcf.create(deal.getSourceApplicationId(), keyValue, statusCode, statusDesc);
	  }

//	  private String extractAddressLine1(Property p, int lang)
//	  {
//	      String val = null;
//	      try
//	      {   val = "";
//
//	          if(  !isEmpty(p.getUnitNumber())  ){
//	            val += p.getUnitNumber();
//	            val += "-";
//	          }
//
//	          if (!isEmpty(p.getPropertyStreetNumber()))
//	             val += p.getPropertyStreetNumber();
//
//	          if (!isEmpty(p.getPropertyStreetName()))
//	             val += SPACE + p.getPropertyStreetName();
//
//	          if (!isEmpty(p.getStreetTypeDescription())){
//	            val += SPACE + BXResources.getPickListDescription("StreetType",  p.getStreetTypeId() , lang);
//	          }
//	          if (p.getStreetDirectionId() != 0)
//	             val += SPACE + BXResources.getPickListDescription("StreetDirection",  p.getStreetDirectionId() , lang);
//	      }
//	      catch (Exception e){
//	        logger.error(e.getClass() + ": " + e.getMessage());
//	      }
//	      return val;
//	  }

	  private boolean isEmpty(String str)
	  {
	      return (str == null || str.trim().length() == 0);
	  }


	  private void sendEmailAlert(String pSubject, String pText ) throws
	      DocPrepException, JdbcTransactionException {

	    String emailTo = null;
	    emailTo = PropertiesCache.getInstance().getProperty(-1, "com.filogix.docprep.alert.email.recipient"); //, "catherine.rutgaizer@filogix.com");

	    logger.trace("Placing a request for an alert email into documentqueue...");
	    // srk.beginTransaction();
	    DocumentRequest.requestAlertEmail(srk, deal, emailTo, pSubject, pText);
	    // srk.commitTransaction();
	    logger.trace("request for documentqueue is done");

	  }

	/*
	      // do not send the message is mossys.property is not set to y
	      if( !PropertiesCache.getInstance().getProperty("com.filogix.docprep.send.email.alert", "Y").equalsIgnoreCase("y")  ){
	        return;
	      }
	      SimpleMailMessage message;
	      try{

	        message = new SimpleMailMessage();
	        message.clearTo();
	        message.setFrom( PropertiesCache.getInstance().getProperty("com.filogix.docprep.alert.email.sender") );
	        message.appendTo( PropertiesCache.getInstance().getProperty("com.filogix.docprep.alert.email.recipient") );
	        message.setText( pText );
	        message.setSubject("DocPrep - Alert : " + PropertiesCache.getInstance().getProperty("com.filogix.docprep.running.environment") + " " + pSubject );
	        MailDeliver.send(message);
	      }catch(Exception exc){
	        // try it one more time ( this time we send the message to emergency recipient )
	        if(onExceptionContact != null ){
	          try{
	            message = new SimpleMailMessage();
	            message.clearTo();
	            message.setFrom("qa@bx.filogix.com");
	            message.appendTo(onExceptionContact);
	            message.setText( pText );
	            message.setSubject( "DocPrep - Alert : " + PropertiesCache.getInstance().getProperty("com.filogix.docprep.running.environment") + " " + pSubject  );
	            MailDeliver.send(message);
	          }catch(Exception anotherExce){
	            anotherExce.printStackTrace();
	          }
	        }
	        exc.printStackTrace();
	      }

	*/
}
