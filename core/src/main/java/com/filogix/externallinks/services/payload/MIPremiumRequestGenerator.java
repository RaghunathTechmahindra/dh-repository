package com.filogix.externallinks.services.payload;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.CustomCharacterEscapeHandler;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.BorrowerType;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.BranchProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.ContactType;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.DealType;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.DownPaymentSourceType;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.InstitutionProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.MtgProdType;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.ObjectFactory;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.PremiumRequest;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.PropertyType;
import com.filogix.externallinks.mi.aigug.jaxb.premiumrequest.UnderwriterType;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.util.XMLUtil;

/**
 * MIPremiumRequestGenerator for extracting AIG UG premium request data.
 *
 * @author: Lin Zhou
 * @date: July 13, 2006
 */
public class MIPremiumRequestGenerator implements IPayloadGenerator {
    private static Log logger = LogFactory.getLog(MIPremiumRequestGenerator.class);
    protected static ObjectFactory objFactory = new ObjectFactory();
    private static JAXBContext jc;
    private static final String schema =
            "com.filogix.externallinks.mi.aigug.jaxb.premiumrequest";
    private static IPayloadGenerator instance = new MIPremiumRequestGenerator();
    static {
        try {
            jc = JAXBContext.newInstance(schema);
        } catch (JAXBException e) {
            logger.error(
                    "MIPremiumRequestGenerator: JAXBContext initialization failed.");
        }
    }

    public static IPayloadGenerator getInstance() {
        return instance;
    }

    public MIPremiumRequestGenerator() {
    }

    public String getXMLPayload(boolean validate, ServiceInfo info) throws
            Exception {
        SessionResourceKit srk = info.getSrk();
        
        // changed for FXP24408 - deal may be from info/calc
        Deal deal = info.getDeal();
        if (deal == null ) {
            deal = new Deal(srk, info.getCalcMonitor(), info.getDealId().intValue(),
                    info.getCopyId().intValue());
//          srk.getExpressState().setDealInstitutionId(deal.getInstitutionProfileId());
          info.setDeal(deal);
        }
        return getXMLPayload(validate, srk, deal);
    }

    protected String getXMLPayload(boolean validate, SessionResourceKit srk,
                                 Deal deal) throws Exception {
        return getXMLPayload(getPremiumRequest(srk, deal), validate);
    }

    protected PremiumRequest getPremiumRequest(SessionResourceKit srk, Deal deal) throws
            Exception {
        if (deal != null && srk != null) {
            PremiumRequest req = objFactory.createPremiumRequest();
            req.setDeal(createDealType(srk, deal));
            return req;
        } else {
            logger.error(this.getClass().getName() + ": ServiceInfo problem.");
            throw new Exception(this.getClass().getName() +
                                ": ServiceInfo problem.");
        }
    }

    protected boolean validate(PremiumRequest req) {
        boolean retVal = false;

        try {
            retVal = jc.createValidator().validate(req);
        } catch (Exception e) {
            logger.error(this.getClass().getName() + ": validation failed.");
        }

        return retVal;
    }


    protected String getXMLPayload(PremiumRequest req, boolean validate) throws
            Exception {
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                               Boolean.TRUE);

		//marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        //marshaller.setProperty(
        //        "com.sun.xml.bind.characterEscapeHandler",
        //        new CustomCharacterEscapeHandler() );  
        if (!validate) {
            return XMLUtil.jaxb2String(marshaller, req);
        } else {
            if (validate(req)) {
                return XMLUtil.jaxb2String(marshaller, req);
            } else {
                throw new Exception(this.getClass().getName() +
                                    ": validation failed.");
            }
        }
    }


    protected DealType createDealType(SessionResourceKit srk, Deal deal) throws
            Exception {
        DealType dealType = objFactory.createDeal();
        populateDealType(dealType, srk, deal);

        createBranchProfile(dealType, srk, deal);
        createBorrower(dealType, srk, deal);
        createDownPaymentSource(dealType, srk, deal);
        createMtgProd(dealType, srk, deal);
        createInstitutionProfile(dealType, srk, deal);
        createUnderwriter(dealType, srk, deal);
        createProperty(dealType, srk, deal);
        return dealType;
    }

    protected void populateDealType(DealType dealType, SessionResourceKit srk,
                                  Deal deal) throws Exception {
        if (!XMLUtil.isEmpty(deal.getAmortizationTerm())) {
            dealType.setAmortizationTerm(XMLUtil.int2BigInteger(deal.
                    getAmortizationTerm()));
        }

        dealType.setCombinedLTV(XMLUtil.double2BigDecimal(deal.getCombinedLTV()));
        dealType.setDealId(XMLUtil.int2BigInteger(deal.getDealId()));
        dealType.setDealPurposeId(XMLUtil.int2BigInteger(deal.getDealPurposeId()));

        if (!XMLUtil.isEmpty(deal.getExistingLoanAmount())) {
            dealType.setExistingLoanAmount(XMLUtil.double2BigDecimal(deal.
                    getExistingLoanAmount()));
        }

        dealType.setLienPositionId(XMLUtil.int2BigInteger(deal.
                getLienPositionId()));

        if (!XMLUtil.isEmpty(deal.getLocRepaymentTypeId())) {
            dealType.setLOCRepaymentTypeId(XMLUtil.int2BigInteger(deal.
                    getLocRepaymentTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getMIExistingPolicyNumber())) {
            dealType.setMIExistingPolicyNumber(deal.getMIExistingPolicyNumber());
        }

        if (!XMLUtil.isEmpty(deal.getMITypeId())) {
            dealType.setMortgageInsuranceTypeId(XMLUtil.int2BigInteger(deal.
                    getMITypeId()));
        }

        dealType.setNetLoanAmount(XMLUtil.double2BigDecimal(deal.
                getNetLoanAmount()));

        if (!XMLUtil.isEmpty(deal.getNextAdvanceAmount())) {
            dealType.setNextAdvanceAmount(XMLUtil.double2BigDecimal(deal.
                    getNextAdvanceAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getProductTypeId())) {
            dealType.setProductTypeId(XMLUtil.int2BigInteger(deal.
                    getProductTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvance())) {
            dealType.setProgressAdvance(deal.getProgressAdvance());
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvanceInspectionBy())) {
            dealType.setProgressAdvanceInspectionBy(deal.
                    getProgressAdvanceInspectionBy());
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvanceTypeId())) {
            dealType.setProgressAdvanceTypeId(XMLUtil.int2BigInteger(deal.
                    getProgressAdvanceTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getSelfDirectedRRSP())) {
            dealType.setSelfDirectedRRSP(deal.getSelfDirectedRRSP());
        }
    }

    protected void createBorrower(DealType dealType, SessionResourceKit srk,
                                Deal deal) throws Exception {
        Collection borrowers = deal.getBorrowers();
        BorrowerType borrowerType = objFactory.createBorrower();

        if (borrowers != null) {
            for (Iterator iter = borrowers.iterator(); iter.hasNext(); ) {
                Borrower b = (Borrower) iter.next();
                if (b.getStaffOfLender()) {
                    borrowerType.setStaffOfLender("Y");
                    dealType.setBorrower(borrowerType);
                    break;
                }
            }
        }
    }


    protected void createBranchProfile(DealType dealType, SessionResourceKit srk,
                                     Deal deal) throws Exception {
        BranchProfile bp = new BranchProfile(srk, deal.getBranchProfileId());
        BranchProfileType branchProfileType = objFactory.
                                              createBranchProfileType();
        branchProfileType.setAIGUGBranchTransitNumber(bp.
                getAIGUGBranchTransitNumber());
        dealType.setBranchProfile(branchProfileType);
    }

    protected void createDownPaymentSource(DealType dealType,
                                         SessionResourceKit srk, Deal deal) throws
            Exception {
        Collection downPaymentSources = deal.getDownPaymentSources();

        if (downPaymentSources != null) {
            DownPaymentSourceType downPaymentSourceType = objFactory.
                    createDownPaymentSourceType();

            for (Iterator iter = downPaymentSources.iterator(); iter.hasNext(); ) {
                DownPaymentSource dps = (DownPaymentSource) iter.next();
                if (dps.getDownPaymentSourceTypeId() == 2) {
                    downPaymentSourceType.setDownPaymentSourceTypeId(
                            XMLUtil.int2BigInteger(2));
                    dealType.setDownPaymentSource(downPaymentSourceType);
                    break;
                }
            }
        }
    }

    protected void createInstitutionProfile(DealType dealType,
                                          SessionResourceKit srk, Deal deal) throws
            Exception {
        InstitutionProfile institutionProfile = new InstitutionProfile(srk);
        institutionProfile = institutionProfile.findByFirst();
    	InstitutionProfileType institutionProfileType = objFactory.
                createInstitutionProfileType();
        institutionProfileType.setAIGUGId(institutionProfile.getAIGUGId());
        dealType.setInstitutionProfile(institutionProfileType);
    }

    protected void createMtgProd(DealType dealType, SessionResourceKit srk,
                               Deal deal) throws Exception {
        if (deal.getMtgProd() != null) {
            if (!XMLUtil.isEmpty(deal.getMtgProd().getInterestTypeId())) {
                MtgProdType mtgProdType = objFactory.createMtgProdType();

                if (!XMLUtil.isEmpty(deal.getMtgProd().getInterestTypeId())) {
                    mtgProdType.setInterestTypeId(XMLUtil.int2BigInteger(deal.
                            getMtgProd().
                            getInterestTypeId()));
                    dealType.setMtgProd(mtgProdType);
                }
            }
        }
    }


    protected void createProperty(DealType dealType, SessionResourceKit srk,
                                Deal deal) throws Exception {
        List propertyTypes = dealType.getProperty();
        Collection properties = deal.getProperties();

        if (properties != null) {
            for (Iterator iter = properties.iterator(); iter.hasNext(); ) {
                Property property = (Property) iter.next();
                PropertyType propertyType = objFactory.createPropertyType();

                if (!XMLUtil.isEmpty(property.getPrimaryPropertyFlag())) {
                    propertyType.setPrimaryPropertyFlag(property.
                            getPrimaryPropertyFlag() +
                            "");
                }

                if (!XMLUtil.isEmpty(property.getNumberOfUnits())) {
                    propertyType.setNumberOfUnits(XMLUtil.int2BigInteger(
                            property.
                            getNumberOfUnits()));
                }

                if (!XMLUtil.isEmpty(property.getPropertyUsageId())) {
                    propertyType.setPropertyUsageId(XMLUtil.int2BigInteger(
                            property.
                            getPropertyUsageId()));
                }

                if (!XMLUtil.isEmpty(property.getProvinceId())) {
                    propertyType.setProvinceId(XMLUtil.int2BigInteger(property.
                            getProvinceId()));
                }

                propertyTypes.add(propertyType);
            }
        }
    }

    protected void createUnderwriter(DealType dealType, SessionResourceKit srk,
                                   Deal deal) throws Exception {
        UserProfile userProfile = new UserProfile(srk);
        userProfile.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId(), 
                                                           deal.getInstitutionProfileId()));

        if (userProfile != null) {
            ContactType contactType = objFactory.createContact();
            contactType.setLanguagePreferenceId(XMLUtil.int2BigInteger(
                    userProfile.
                    getUserLanguageId()));
            UnderwriterType underwriterType = objFactory.createUnderwriterType();
            underwriterType.setContact(contactType);
            dealType.setUnderwriter(underwriterType);
        }
    }

}
