package com.filogix.externallinks.services.datx;

import com.filogix.externallinks.framework.ServiceConst;

public class DatxResponseCodeClosingConverter
    implements DatxResponseCodeConverterBase, ServiceConst
{

  public int converetToFxpStatusCode(int datxResultCode)
  {
    switch (datxResultCode)
    {
    case RESULT_CODE_SUCCEED:
        return RESPONSE_STATUS_REQUEST_ACCEPTED;
    case RESULT_CODE_RECEIVED:
      return RESPONSE_STATUS_REQUEST_ACCEPTED;
    case RESULT_CODE_JOB_ASSIGNED:
      return RESPONSE_STATUS_JOB_ASSIGNED;
    case RESULT_CODE_JOB_DELAYED:
      return RESPONSE_STATUS_JOB_DELAYED;
    case RESULT_CODE_CLOSING_CONFIRMED:
      return RESPONSE_STATUS_CLOSING_CONFIRMED;
    case RESULT_CODE_SYSTEM_ERROR:
      return RESPONSE_STATUS_SYSTEM_ERROR;
    case RESULT_CODE_PROCESSING_ERROR:
      return RESPONSE_STATUS_PROCESSING_ERROR;
    case RESULT_CODE_CANCELLATION_REQUESTED:
      return RESPONSE_STATUS_CANCELLATION_REQUESTED;
    case RESULT_CODE_CLOSING_CANCELLATION_CONFIRMED:
      return RESPONSE_STATUS_CANCELLATION_CONFIRMED;
    case RESULT_CODE_CANCELLATION_REJECTED:
      return RESPONSE_STATUS_CANCELLATION_REJECTED;

    default:
      return -1;
    }
  }

}
