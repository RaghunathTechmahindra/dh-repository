/**
 * <p>Title: DatxAddrScrubChannel.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Nov 22, 2006)
 *
 */
package com.filogix.externallinks.services.datx;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Province;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.validation.addressScrub.AddScrubConstants;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLog;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.datx.addrscrub.payload.AddressScrubberRequestType;
import com.filogix.datx.addrscrub.payload.AddressScrubberResponseType;
import com.filogix.datx.addrscrub.payload.AddressType;
import com.filogix.datx.wsdl.addressscrub.AddressScrubber_PortType;
import com.filogix.datx.wsdl.addressscrub.AddressScrubber_ServiceLocator;
import com.filogix.datx.wsdl.addressscrub.FXRequest;
import com.filogix.datx.wsdl.addressscrub.FXResponse;
import com.filogix.datx.wsdl.addressscrub.Payload;
import com.filogix.externallinks.framework.NoStoredRequestData;
import com.filogix.externallinks.framework.NoStoredResponseData;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbPayloadHelper;
import com.filogix.externallinks.framework.ServiceConst;

/*
 * <p>DatxAddrScrubChannel</p>
 * This class is a Datx Address Scrub WebService client.
 * Send Request, Receive Response, generates response XML paylod String
 * from JAXB request Payload Object, generates JAXB Response payload Object 
 * from response XML payload String
 */
public class DatxAddrScrubChannel extends NoStoredRequestChannel
{
  /**
   * static variable JAXBContext associated with this request/response payload xml schema
   */
  public static JAXBContext JaxbContext = null;
  static
  {
      try
      {
          JaxbContext = JAXBContext.newInstance
          (AddressScrubberRequestType.class.getPackage().getName());
      }
      catch(JAXBException e)
      {
          
      }
  }
  
  /**
   * static variable String ResponsePayloadTag, response payload root tag
   */
  public static final String ResponsePayloadTag = "AddressScrubberResponse";
  
  // these calsses are commonly used but package will be different so that 
  // they cannot be in the super class and initRequestBean() method
  protected FXRequest     _fxRequest;
  protected FXResponse    _fxResponse;

  protected String  _xmlPayload;
  protected AddressScrubberResponseType _responseBean;
  public Map  _provinceShortNames = null;

  // constructor
  public DatxAddrScrubChannel()
  {
    super();
  }
  
  public void init(SessionResourceKit srk, Map provinceShortNames, 
                int dealId, int copyId, String loginId) 
    throws RemoteException, FinderException, ExternalServicesException, Exception
  {
    super.init(srk, dealId, copyId, 
               ServiceConst.SERVICE_CHANNEL_ID_ADDR_SCRUB, 
               ServiceConst.SERVICE_PAYLOAD_TYPE_ADDR_SCRUB, "System");
    _provinceShortNames = provinceShortNames;
    initRequestBean();
  }

   /**
   * <p>initRequestBean
   * initialize requestBean i.e. FxRequest for this request
   * @see com.filogix.externallinks.datx.NoStoredRequestChannel#initRequestBean()
   */
  public void initRequestBean()
    throws RemoteException,
      FinderException,
      ExternalServicesException
  {
    _fxRequest = new FXRequest();
      
    // clientKey is dealId for AddressScrub
    _fxRequest.setClientKey("" + _deal.getDealId());
    _fxRequest.setUsername(_channel.getUserId());
    _fxRequest.setPassword(_channel.getPassword());
    // inisital ServiceKey is empty - for address scrub, it's always empty
    _fxRequest.setServiceKey("");
    _fxRequest.setEncoding("UTF-8");
    _fxRequest.setSchemaName("");
//  _fxRequest.setSchemaName(((DatxAddrScrubRequestData)_data).getPayloadType().getNoNameSchemaLocation());
    _fxRequest.setAuxData(null); 
    
    // They are not required for AddressScrub 
//    _fxRequest.setProductType("");
//    _fxRequest.setRequestType("");
//    _fxRequest.setServiceProvider("");
  }
 
  /**
   * <p> sendData </p>
   * create WS service client(ServiceLocator and Port these 
   * clases were generated by AXIS) 
   * and sending request to Datx
   * @param String payload
   * @return String returned payload
   * @throws Exception
   * @see com.filogix.externallinks.datx.NoStoredRequestChannel
   * #sendData(java.lang.String)
   */
  public Object sendData(String payload)
    throws Exception
  {    
    if (payload == null)
    {
      _logger.info("******* Input Address does not meet criteria. *******");
      _fxResponse = new FXResponse();
      _fxResponse.setClientKey("" + _deal.getDealId());
      _fxResponse.setResultCode(new Integer(AddScrubConstants.RESULT_CODE_NOT_SEND));
      _fxResponse.setResultDescription("Input Address does not meet criteria.");
      return _fxResponse;
    }
    
    try
    {
      AddressScrubber_ServiceLocator servLoc = getServiceLocater();
        if (servLoc==null)
        {
            String msg = "DatxAddrScrubChannel: DatX service is not available: "
              + "AddressScrubber_ServiceLocator = null.";
            _logger.error(msg);
            throw new DatxException(msg);
        }
        AddressScrubber_PortType stub = getServiceStub(servLoc);
        if (stub==null)
        {
            String msg = "DatxAddrScrubChannel: DatX service is not available: "
              + "AddressScrubber_PortType = null";
            _logger.error(msg);
            throw new DatxException(msg);
        }
        _logger.info("Sending Address Scurub request --->>> ");
        _logger.info("FxRequest UserName: " + _fxRequest.getUsername());
        _logger.info("FxRequest Password: " + _fxRequest.getPassword());
        _logger.info("FxRequest Client Key: " + _fxRequest.getClientKey());
        _logger.info("FxRequest Service Key: " + _fxRequest.getServiceKey());
        _logger.info("FxRequest SchemaName: " + _fxRequest.getSchemaName());
        _logger.info("FxRequest Product Type: " + _fxRequest.getProductType());
        _logger.info("FxRequest Request Type: " + _fxRequest.getRequestType());
        _logger.info("FxRequest Service Provider: " 
                     + _fxRequest.getServiceProvider());
        _logger.info("FxRequest TimeOut: " + _fxRequest.getTimeout());
        _logger.info("FxRequest Aux Data: " + _fxRequest.getAuxData());
        _logger.info("xmlPayload --->>");
        _logger.info(payload);
        
        _fxResponse = stub.requestScrub(_fxRequest, payload);
        if (_fxResponse != null)
        {
            _logger.info("Received Address Scurub response --->>>");
            _logger.info("FxResponse Result Code: " 
                         + _fxResponse.getResultCode());
            _logger.info("FxResponse Result Desc: " 
                         + _fxResponse.getResultDescription());
            _logger.info("FxResponse Result Service Key: " 
                         + _fxResponse.getServiceKey());
            _logger.info("FxResponse External Ref Number: " 
                         + _fxResponse.getExternalKey());
            
            Payload[] responsePayload = _fxResponse.getPayloads();
            if ( _fxResponse.getResultCode() != null 
                && (_fxResponse.getResultCode().intValue()==ServiceConst.RESULT_CODE_PROCESSING_ERROR
                    || _fxResponse.getResultCode().intValue()==ServiceConst.RESULT_CODE_SYSTEM_ERROR))
            {
              _logger.info("Response Payload:-->>");
              _logger.info("<< EMPTY >>");
              return _fxResponse;              
            }
            else if (responsePayload!=null && responsePayload.length > 0)
            {
              String responseXML = new String(responsePayload[0].getContent());
              
              _logger.info("Response Payload:-->>");
              _logger.info(responseXML);
              return _fxResponse;              
            }
            else
            {
              _logger.error("FxResponse doesn't include payload request id " 
                            + _fxRequest.getClientKey());
              throw new DatxException("FxResponse doesn't include payload or no error code for request id " 
                                      + _fxRequest.getClientKey());
            }
        }
        else
        {
            _logger.error("DatXResponse is null for request id " 
                          + _fxRequest.getClientKey());
            throw new DatxException("DatXResponse is null for request id " 
                                    + _fxRequest.getClientKey());
        }
    } 
    catch (Exception e)
    {
      String msg = "Error when sending service request to DatX: RequestId = " 
        + _fxRequest.getClientKey();
      _logger.error(msg + ": "+ StringUtil.stack2string(e));
      System.out.println(StringUtil.stack2string(e));
      throw new DatxException(msg + ": " + e);
    }
  }
  
  /**
   * <p> getServiceLocator</p>
   * Generate one class for Axis Client
   * @return AddressScrubber_ServiceLocator
   **/
  public AddressScrubber_ServiceLocator getServiceLocater() 
    throws ServiceException
  {
    AddressScrubber_ServiceLocator servLoc = new AddressScrubber_ServiceLocator();
      if (servLoc == null)
      {
          return null;
      }
      servLoc.setEndpointAddress(_channel.getWsServicePort(), _url+"?wsdl");
      return servLoc;
  }
    
  /**
   * <p> getServiceStub</p>
   * <p>Generate one class for Axis Client</p>
   * @return AddressScrubber_PortType
   **/
  public AddressScrubber_PortType getServiceStub(AddressScrubber_ServiceLocator servLoc) 
    throws ServiceException
  {
    AddressScrubber_PortType stub = (AddressScrubber_PortType)servLoc.getAddressScrubber();
    return stub;
  }
    
  /**
   * <p> createRequest</p>
   * <p>Generates XML Request payload String from JAXB DatxAddrScrubRequestPayloadData 
   * one class for Axis Client</p>
   * @return AddressScrubber_PortType
   **/
  public String createRequest(NoStoredRequestData payloadObject)
    throws Exception
  {
    if (payloadObject == null) return null;
    AddressScrubberRequestType addrScrubRequestBean 
      = (AddressScrubberRequestType) payloadObject.getRequestBean();
    
    // JaxbPayloadHelper.getXmlPayload(Object payload, JAXBContext jaxbContext, 
    //                                  String noNamespaceURL, 
    //                                  String customCharacterEscapeHandlerType) 
    String xmlPayload = JaxbPayloadHelper.getXmlPayload(addrScrubRequestBean, JaxbContext, null, null);
    return xmlPayload;
  }

  /**
   * <p> createResponse </p>
   * 
   * @param fxResponse - depends on package i.e. Datx Services
   * @return DatxAddrScrubResponseData 
   * @throws Exception
   */
  public Object createResponse(Object fxResponse)
    throws Exception
  {
    NoStoredResponseData responsePayloadData = new DatxAddrScrubResponseData(_srk, _provinceShortNames);
    responsePayloadData.setResponseBean(fxResponse);
    return responsePayloadData;
  }
  
//*********** Below this point, all methods are for Test purpose ****************//  
  /**
   * test method
   * @param args
   */
  public static void main(String[] args)
  {
    try
    {
      DatxAddrScrubChannel.setUp();
      SessionResourceKit srk = new SessionResourceKit("System");

//      UserProfile user = new UserProfile(srk);
//      user = user.findByPrimaryKey( new UserProfileBeanPK(USERID));
      DatxAddrScrubChannel wsChannel = new DatxAddrScrubChannel();      
      Map provinceShortNames = new HashMap();
      Province province = new Province(srk);
      List allProvinces = province.getAllProvince();
      Iterator ittr = allProvinces.iterator();
      while(ittr.hasNext())
      {
        Province p = (Province)ittr.next();
        provinceShortNames.put(p.getProvinceAbbreviation(), p);
      }

//      int dealId = 5049;
//      int copyId = 2;
//      int dealId = 9005;
//      int copyId = 12;
      
      // French Address with accent
    int dealId = 9072;
    int copyId = 1;
      
      int channelId = 10;
      int payloadTypeId = 24;
      Deal deal = new Deal(srk, null);
      deal = deal.findByPrimaryKey(new DealPK(dealId, copyId));

      wsChannel.init(srk, provinceShortNames, dealId, copyId, "System");
      AddressScrubber_ServiceLocator locator = wsChannel.getServiceLocater();
      AddressScrubber_PortType service = locator.getAddressScrubber();

      FXRequest _request = null;
      _request = new FXRequest();
      _request.setUsername("express_scrubber");
      _request.setPassword("pwd_scrubber_express");
      _request.setClientKey(""+dealId);
      _request.setRequestType("AddressScrubber");
      _request.setSchemaName("AddressScrubberRequest_v7.xsd");
      _request.setAuxData("");
      _request.setEncoding("UTF-8");
      _request.setProductType("");
      _request.setServiceKey("");
      _request.setServiceProvider("");
      DatxAddrScrubRequestData requestData = new DatxAddrScrubRequestData(srk, provinceShortNames, true);
      requestData.setDealId(dealId);
      
//    Addr addr = wsChannel.getBorrowerAddress(deal);
    Addr addr = wsChannel.createPropertyAddress(deal);
      
      requestData = DatxAddrScrubRequestData.create(srk, addr, provinceShortNames, true); // 2nd parameter indicate parsed or unparsed
      String requestPayload = wsChannel.createRequest(requestData);
      store(TEST_REQUEST_PAYLOAD.getBytes("UTF-8"), "Request_UTF8");
      store(TEST_REQUEST_PAYLOAD.getBytes(), "Request");
//      System.out.println(TEST_REQUEST_PAYLOAD);
      FXResponse fxResponse = service.requestScrub(_request, TEST_REQUEST_PAYLOAD);
      System.out.println(fxResponse.getResultCode());
      System.out.println(fxResponse.getResultDescription());
      Payload[] payloads = fxResponse.getPayloads();
      AddressScrubberResponseType response = null;
      if (payloads != null && payloads.length > 0)
      {
        Payload payload = payloads[0];
        byte[] content = payload.getContent();

        store(content, "PRE");
        String contentString = new String(content);
//        store(contentString.getBytes("UTF-8"), "AFTER");
//
//        System.out.println(contentString);
        response = (AddressScrubberResponseType)JaxbPayloadHelper.getPayloadBean
          (contentString, JaxbContext, ResponsePayloadTag);
        AddressType address = response.getMatch();
        if (address != null)
        {
          System.out.println(toStringAddress(address));
        }
        else
        {
          List sugList = response.getSuggestion();
          if (sugList != null)
          {
            System.out.println("Suggestion List was returned(size = " + sugList.size() + ") ");
            Iterator iter = sugList.iterator();
            
            while (iter.hasNext())
            {
              address = (AddressType)iter.next();
              System.out.println(toStringAddress(address));              
            }
          }
        }
      }
    }
    catch(Exception ex){
        System.out.println(ex);
    }
    finally
    {
      DatxAddrScrubChannel.tearDown();
    }
  }

  private Addr getBorrowerAddress(Deal deal)
    throws Exception
  {
    Addr pAddr = null;
    Collection borrowers = deal.getBorrowers();
    Iterator iterB = borrowers.iterator();
    while (iterB.hasNext())
    {
      Borrower borrower = (Borrower)iterB.next();
      if (borrower.isPrimaryBorrower())
      {
        Collection bAddresses = borrower.getBorrowerAddresses();
        Iterator iterA = bAddresses.iterator();
        while (iterA.hasNext())
        {
          BorrowerAddress bAddr = (BorrowerAddress)iterA.next();
          if (bAddr.getBorrowerAddressTypeId() == 0)
          {
            return bAddr.getAddr();
          }
          else if (bAddr.getBorrowerAddressTypeId() == 1) 
          {
            pAddr = bAddr.getAddr();
          }
        }
      }
    }
    return pAddr;
  }
  
  private Addr createPropertyAddress(Deal deal)
    throws Exception
  {
    Addr pAddr = new Addr(_srk);
    Collection properties = deal.getProperties();
    Iterator iterB = properties.iterator();
    while (iterB.hasNext())
    {
      Property property = (Property)iterB.next();
      if (property.isPrimaryProperty())
      {
        pAddr.setProvinceId(property.getProvinceId());
        pAddr.setCity(property.getPropertyCity());
        pAddr.setStreetName(property.getPropertyStreetName());
        pAddr.setStreetNumber(property.getPropertyStreetNumber());
        break;
      }
    }
    return pAddr;
  }
  
  public static String toStringAddress(AddressType addr)
  {
    String nl = "\n\r";
    return new StringBuffer()
    .append(addr.getUnitNumber()!=null?"Unit Number: " + addr.getUnitNumber() + nl:"")
    .append(addr.getStreetNumber()!=null?"Street Number: " + addr.getStreetNumber()+ nl:"")
    .append(addr.getStreetName()!=null?"Street Name: " + addr.getStreetName()+ nl:"")
    .append(addr.getStreetType()!=null?"Street Type: " + addr.getStreetType()+ nl:"")
    .append(addr.getStreetDirection()!=null?"Street Direction: " + addr.getStreetDirection()+ nl:"")
    .append(addr.getCity()!=null?"City: " + addr.getCity()+ nl:"")
    .append(addr.getProvince()!=null?"Province: " + addr.getProvince()+ nl:"")
    .append(addr.getPostalCode()!=null?"PostalCode: " + addr.getPostalCode()+ nl:"").toString();
  }

  public static void setUp() throws FinderException, RemoteException, CreateException, Exception
  {
      System.out.println("************* start init ");
      
      File location = new File("mossys.properties"); 
      String property_file = location.getAbsolutePath();     
      PropertiesCache.addPropertiesFile(property_file);
    
      propertiesCache = PropertiesCache.getInstance();
         
      File locate = new File("admin"); 
      String strHome = locate.getAbsolutePath(); 
      SysLog.init(strHome); 
       
//      String strPoolName = propertiesCache.getProperty("com.basis100.resource.connectionpoolname");
//      String strRetryCounter = propertiesCache.getProperty(ServiceConst.PROPERTY_RETRY_COUNTRER, "5");
//      intRetryCounter = TypeConverter.intTypeFrom(strRetryCounter);
//      ResourceManager.init(strHome, strPoolName);
      System.out.println("************* init done successfully");

  }
  private static void store(byte[] context, String type)
  {
    try
    {
      File file = new File(type+"FixedPayload.dat");    
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(context);
      fos.close();
    }
    catch(Exception e)
    {
      System.out.println("Error Occured!");
    }
  }
  public static void tearDown()
  {
      ResourceManager.shutdown();

  }

//  public static String TEST_REQUEST_PAYLOAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
//  "<AddressScrubberRequest>"+
//  "<UnparsedAddress>"+
//      "<addressLine1>2839 Street 35th SW</addressLine1>"+
//      "<city>Calgary</city>"+
//      "<province>AB</province>"+
//      "<postalCode>T3E2Y4</postalCode>"+
//  "</UnparsedAddress>"+
//"</AddressScrubberRequest>";
//  "<AddressScrubberRequest>"+
//  "<UnparsedAddress>"+
//      "<addressLine1>60 Bloor St. W</addressLine1>"+
//      "<city>Toronto</city>"+
//      "<province>ON</province>"+
//  "</UnparsedAddress>"+
//"</AddressScrubberRequest>";
  public static String TEST_REQUEST_PAYLOAD = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
  "<AddressScrubberRequest><UnparsedAddress>"
+"<addressLine1>2097 rue Tillemont</addressLine1>"
+"<city>Montr�al</city>"
+"<province>QC</province>"
+"<postalCode>H2E1E2</postalCode>"
+"</UnparsedAddress></AddressScrubberRequest>";
  
  
//"<AddressScrubberRequest>" +
//"<ParsedAddress>" + 
//  "<unitNumber>2</unitNumber>" + 
//  "<streetNumber>101</streetNumber>" + 
//  "<streetName>Wellington</streetName>" +
//  "<streetType>Street</streetType>" + 
////  "<streetDirection>N</streetDirection>" +
//  "<city>London</city>" + 
//  "<province>ON</province>" +
//  "<postalCode>N6C4M7</postalCode>" +
//  "<country>Canada</country>" +
//"</ParsedAddress>" +
//"</AddressScrubberRequest>";
  private static PropertiesCache propertiesCache;


}
