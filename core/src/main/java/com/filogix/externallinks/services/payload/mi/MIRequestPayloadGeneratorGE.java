package com.filogix.externallinks.services.payload.mi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.util.TypeConverter;
import com.dhltd.marketplace.mi.jaxb.request.BranchProfileType;
import com.dhltd.marketplace.mi.jaxb.request.DealType;
import com.dhltd.marketplace.mi.jaxb.request.InstitutionProfileType;
import com.filogix.externallinks.services.util.XMLUtil;

public class MIRequestPayloadGeneratorGE extends MIRequestPayloadGenerator {

    private final static Logger logger = LoggerFactory.getLogger(MIRequestPayloadGeneratorGE.class);

    @Override
    protected void populateInstitutionProfile(InstitutionProfileType institutionProfileType, InstitutionProfile institutionProfile) {

        super.populateInstitutionProfile(institutionProfileType, institutionProfile);

        if (!XMLUtil.isEmpty(institutionProfile.getGEId())) {
            institutionProfileType.setMIProviderInstititionCode(institutionProfile.getGEId());
        }
    }

    @Override
    protected void populateBranchProfile(BranchProfile branchProfile, BranchProfileType branchProfileType) {

        super.populateBranchProfile(branchProfile, branchProfileType);

        if (!XMLUtil.isEmpty(branchProfile.getGEBranchTransitNumber())) {
            branchProfileType.setMIProviderBranchTransitNumber(branchProfile.getGEBranchTransitNumber());
        }
    }

    @Override
    protected void populateDealType(DealType dealType, Deal deal) {

        super.populateDealType(dealType, deal);

        // excerpt from mapping spec
        /**
         * <pre>
         * 
         * If MORTGAGEINSURERID = 1 then Deal/MIPolicyNumCMHC
         * Else if MORTGAGEINSURERID = 2 then Deal/MIPolicyNumGE
         * 
         * Genworth only!
         * RULE 1
         * (Adopt a deal/ Resurrected deals - referenced deal number is the original deal id. Get policy number from original deal if insurer was Genworth)
         * if DEAL.MIPOLICYNUMBER is null and MORTGAGEINSURERID = 2
         *                 if DEAL.REFERENCEDEALNUMBER <> null
         *                                 if (DEAL.MIPOLICYNUMGE  <> null where DEALID = DEAL.REFERENCEDEALNUMBER)
         *                                                 then set MortgageInsuranceRequest/Deal/MIPolicyNumber to 
         *                                                                 (DEAL.MIPOLICYNUMGE  where DEALID = DEAL.REFERENCEDEALNUMBER)
         *                                                 else do not create MortgageInsuranceRequest/Deal/MIPolicyNumber
         * End special rule for Genworth
         * 
         * Standard rule for all providers:
         * if DEAL.MIPOLICYNUMBER <> null
         *                 then set MortgageInsuranceRequest/Deal/MIPolicyNumber to DEAL.MIPOLICYNUMBER
         *                 else do not do not create MortgageInsuranceRequest/Deal/MIPolicyNumber
         * </pre>
         */

        if (XMLUtil.isEmpty(deal.getMIPolicyNumber())) {

            int refDealid = TypeConverter.intTypeFrom(deal.getReferenceDealNumber());
            if (refDealid > 0) {

                Deal refDeal = null;
                try {
                    refDeal = new Deal(deal.getSessionResourceKit(), null);
                    refDeal.findByGoldCopy(refDealid);
                    if (!XMLUtil.isEmpty(refDeal.getMIPolicyNumGE())) {
                        dealType.setMIPolicyNumber(refDeal.getMIPolicyNumGE());
                    }
                } catch (Exception e) {
                    handleEntityException(e, Deal.class);
                    logger.error("failed to find Deal from ReferenceDealNumber = " + refDealid, e);
                }

            }
        } else {
            dealType.setMIPolicyNumber(deal.getMIPolicyNumber());
        }
    }
}
