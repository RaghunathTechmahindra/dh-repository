package com.filogix.externallinks.services.channel.ug;

import java.net.*;

import com.filogix.datx.framework.packaging.*;
import com.filogix.datx.mi.*;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.*;

import org.apache.axis.client.Stub;
import org.apache.commons.logging.*;

/**
 * UGPremiumRequestChannel
 *
 * @author: Lin Zhou
 * @date: July 20, 2006
 */
public class UGMIRequestChannel extends SimpleChannel {
    private static Log logger = LogFactory.getLog(UGMIRequestChannel.class);
    private static AIGUGMIRequestService service;

	private UGMIRequestChannel() {
	}
	
	public static IChannel getInstance() {
		return new UGMIRequestChannel();
	}

    public synchronized FXResponse transmit(ServiceInfo serviceInfo, String xmlPayload) throws Exception {
    	
        return service.invoke(construct(serviceInfo), xmlPayload);
    }

	public void init(ServiceInfo serviceInfo) throws Exception {
	    
	    setupChannelInfo(serviceInfo);
	    
	    if (service == null) {
			if (getEndpoint() != null) {
				try {
					service = (new AIGUGMIRequestServiceServiceLocator())
							.getAIGUGMIRequestService(new URL(getEndpoint()));
					((Stub) service).setTimeout(getWsTimeout() * 1000); 
				} catch (Exception e) {
					logger.error(this.getClass().getName()
							+ ": initialization failed.");
					throw new Exception(this.getClass().getName()
							+ ": initialization failed.");
				}
			} else {
				logger.error(this.getClass().getName()
						+ ": endpoint is not specified.");
				throw new Exception(this.getClass().getName()
						+ ": endpoint is not specified.");
			}
		}
	}
	

}

