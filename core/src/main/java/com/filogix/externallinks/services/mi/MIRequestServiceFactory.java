package com.filogix.externallinks.services.mi;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.filogix.express.core.ExpressCoreContext;

public class MIRequestServiceFactory {

    Map<Integer, String> miProviderServiceNameMap;

    public static MIRequestServiceFactory getInstance() {
        return (MIRequestServiceFactory) ExpressCoreContext.getService("MIRequestServiceFactory");
    }

    public MIRequestService getMIRequestService(int mortgageInsurerId) {
        String serviceName = miProviderServiceNameMap.get(mortgageInsurerId);
        if (StringUtils.isEmpty(serviceName))
            throw new IllegalArgumentException(
                "Express couldn't find MIRequestService for mortgageInsurerId=" + mortgageInsurerId);
        return (MIRequestService) ExpressCoreContext.getService(serviceName);
    }

    public void setMiProviderServiceNameMap(Map<Integer, String> miProviderServiceNameMap) {
        this.miProviderServiceNameMap = miProviderServiceNameMap;
    }

}
