package com.filogix.externallinks.services.datx;

import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.ServiceProvider;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.filogix.datx.messagebroker.valuation.AVMMessageSender;
import com.filogix.datx.messagebroker.valuation.AVMRequestBean;
import com.filogix.datx.messagebroker.valuation.AVMResponseBean;
import com.filogix.externallinks.framework.ServiceChannelBase;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServicePayloadData;
import com.filogix.externallinks.framework.ServiceResponseBase;

/*
 * DatxAvmChannel.java
 * 
 * External links framework
 * 
 * Created: 20-Feb-2006
 * Author:  Catherine Rutgaizer
 * 
 */
public class DatxAvmChannel extends ServiceChannelBase {
  //
  private AVMRequestBean    _requestBean;
  private AVMResponseBean   _responseBean;
  private String            _requestXML;
  private String            _responseXML;
  
  protected void initRequestBean() throws RemoteException, FinderException{
    _requestBean = new AVMRequestBean();
    
    int productId = _request.getServiceProductId();

    ServiceProduct product = new ServiceProduct(_srk, productId);
    int providerId = product.getServiceProviderId();
    
    ServiceProvider provider = new ServiceProvider(_srk, providerId); 
    String receivingParty = provider.getReceivingPartyCode();
    _logger.debug("receivingPartyCode = " + receivingParty);

    _requestBean.setReceivingPartyId(receivingParty);
    String l_reqParty = PropertiesCache.getInstance().getProperty(_srk.getExpressState().getDealInstitutionId(),
                ServiceConst.DATX_AVM_REQUESTING_PARTY_PROPERTY_NAME);
    if (l_reqParty == null){
      throw new FinderException("System property missing: " + ServiceConst.DATX_AVM_REQUESTING_PARTY_PROPERTY_NAME); 
    }
    _requestBean.setRequestingPartyId(l_reqParty); // "com.filogix"
    String l_accountId = PropertiesCache.getInstance().getProperty(_srk.getExpressState().getDealInstitutionId(),
            "com.filogix.datx.services.accountid");
    if (l_accountId == null){
      throw new FinderException("System property missing: " + ServiceConst.DATX_AVM_ACCOUNT_ID_PROPERTY_NAME); 
    }
    _requestBean.setAccountIdentifier(l_accountId);
    
//    _requestBean.setAccountIdentifier(_data.getDatxdAccountId());
    // Todo: ---- end
    _requestBean.setAccountPassword(_channel.getPassword()); 
    
    // property info
    Property prop = _data.getProperty();
    if (prop == null) {
      throw new FinderException("Property not found for requestId = " + _requestId);
    }
    
    if ((prop.getUnitNumber()!= null) && !(prop.getUnitNumber().trim().equals(""))){
      _requestBean.setUnitNumber(prop.getUnitNumber());
    }
    if ((prop.getPropertyStreetNumber() != null) && !(prop.getPropertyStreetNumber().trim().equals(""))){
      _requestBean.setStreetNumber(prop.getPropertyStreetNumber());
    }
    if ((prop.getPropertyStreetName() != null) && !(prop.getPropertyStreetName().trim().equals(""))) {
      _requestBean.setStreetName(prop.getPropertyStreetName());
    }
    
    if (prop.getStreetTypeId() != 0) {
    _requestBean.setStreetType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "STREETTYPE", prop.getStreetTypeId(), ServiceConst.LANG_ENGLISH));
    }
      
    if (prop.getStreetDirectionId() != 0){
    _requestBean.setStreetDirectionSuffix(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "STREETDIRECTION", prop.getStreetDirectionId(), ServiceConst.LANG_ENGLISH));
    }
    
    _requestBean.setStreetDirectionPrefix(null);
    
    // _requestBean.setMunicipality(BXResources.getPickListDescription("MUNICIPALITY", prop.getMunicipalityId(), ServiceConst.LANG_ENGLISH));
    // Catherine: DatX pvcs #12:
    if ((prop.getPropertyCity() != null)) {
      _requestBean.setMunicipality(prop.getPropertyCity());
    } else {
      _requestBean.setMunicipality("");
    }
    int timeout = _request.findDatxTimeout(_request.getServiceProductId(), _request.getChannelId());
    _logger.debug("Timeout = " + timeout);
    _requestBean.setTimeout(timeout);
    
    _requestBean.setProvince(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PROVINCESHORTNAME", prop.getProvinceId(), ServiceConst.LANG_ENGLISH)); // FIXME: English is hard-coded! in the future release might require language here
    
    StringBuffer sb = new StringBuffer();
    if (prop.getPropertyPostalFSA() != null){
      sb.append(prop.getPropertyPostalFSA());
    }
    if (prop.getPropertyPostalLDU() != null) {
      sb.append(prop.getPropertyPostalLDU());
    }
    
    _requestBean.setPostalCode(sb.toString() ); // "L5M"
    
  }

  public ServiceResponseBase sendData(ServicePayloadData data) throws RemoteException, FinderException, DatxException {
    _data = data;
    
    ServiceResponseBase result = new ServiceResponseBase(_srk, _requestId, _copyId);
    
    initRequestBean();

    AVMMessageSender valuationMessageSender = new AVMMessageSender();

    try {
      _requestXML = valuationMessageSender.createRequest(_requestBean);
      _logger.debug("REQUEST: \n" + _requestXML );
      _responseXML = valuationMessageSender.execute(_requestXML, _url);
      _logger.debug("RESPONSE: \n" + _responseXML );
      _responseBean = valuationMessageSender.createResponse(_responseXML);
      result.setResponseBean(_responseBean);
      result.setResponseXml(_responseXML);
      
    } catch (Exception e) {
      String msg = "Error when sending service request to DatX: RequestId = " + this._requestId;
      _logger.error(msg + ": "+ StringUtil.stack2string(e));
      throw new DatxException(msg + ": " + e);
    }
    
    return result;
  }

  
  public static void main(String args[]) {
/* * DatX ---- Test
  */
    try {
      AVMRequestBean    requestBean;
      AVMResponseBean   valuationResponseBean;
      AVMMessageSender  valuationMessageSender;
      String        requestXML;
      String        responseXML;
      
      requestBean = new AVMRequestBean();
      requestBean.setReceivingPartyId("com.reavs.valuation");
      requestBean.setRequestingPartyId("com.filogix");  
      requestBean.setAccountIdentifier("FXP");
      requestBean.setAccountPassword("FXP123");
      requestBean.setUnitNumber("1");
      requestBean.setStreetNumber("5659");
      requestBean.setStreetName("Glen Erin");
      requestBean.setStreetType("");
      //requestBean.setStreetDirectionSuffix("NW");
      //requestBean.setMunicipality("Edmonton");
      requestBean.setProvince("ON");
      requestBean.setPostalCode("L5M");
      
      valuationMessageSender = new AVMMessageSender();
      requestXML = valuationMessageSender.createRequest(requestBean);
      System.out.println("REQUEST:" + requestXML);
      responseXML = valuationMessageSender.execute(requestXML, "http://10.1.1.225:7001/datxShell/services/MessageBrokerWS");
      System.out.println("RESPONSE:" + responseXML);
      valuationResponseBean = valuationMessageSender.createResponse(responseXML);
      System.out.println("Valuation response bean = " + valuationResponseBean);
    }
    catch (Exception exception) {
      System.err.println(exception.getMessage());
    }
  // */
  }
}
