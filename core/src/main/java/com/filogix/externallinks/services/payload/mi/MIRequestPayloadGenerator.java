package com.filogix.externallinks.services.payload.mi;

import java.io.StringWriter;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.CreditReference;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.dhltd.marketplace.mi.jaxb.request.AddrType;
import com.dhltd.marketplace.mi.jaxb.request.AssetType;
import com.dhltd.marketplace.mi.jaxb.request.BorrowerAddressType;
import com.dhltd.marketplace.mi.jaxb.request.BorrowerType;
import com.dhltd.marketplace.mi.jaxb.request.BorrowerType.BorrowerPhone;
import com.dhltd.marketplace.mi.jaxb.request.BranchProfileType;
import com.dhltd.marketplace.mi.jaxb.request.ContactType;
import com.dhltd.marketplace.mi.jaxb.request.CreditReferenceType;
import com.dhltd.marketplace.mi.jaxb.request.DealType;
import com.dhltd.marketplace.mi.jaxb.request.DownPaymentSourceType;
import com.dhltd.marketplace.mi.jaxb.request.EmploymentHistoryType;
import com.dhltd.marketplace.mi.jaxb.request.EscrowPaymentType;
import com.dhltd.marketplace.mi.jaxb.request.IncomeType;
import com.dhltd.marketplace.mi.jaxb.request.InstitutionProfileType;
import com.dhltd.marketplace.mi.jaxb.request.LenderProfileType;
import com.dhltd.marketplace.mi.jaxb.request.LiabilityType;
import com.dhltd.marketplace.mi.jaxb.request.MortgageInsuranceRequestType;
import com.dhltd.marketplace.mi.jaxb.request.MtgProdType;
import com.dhltd.marketplace.mi.jaxb.request.ObjectFactory;
import com.dhltd.marketplace.mi.jaxb.request.PartyProfileType;
import com.dhltd.marketplace.mi.jaxb.request.PricingProfileType;
import com.dhltd.marketplace.mi.jaxb.request.PropertyExpenseType;
import com.dhltd.marketplace.mi.jaxb.request.PropertyType;
import com.dhltd.marketplace.mi.jaxb.request.SourceFirmProfileType;
import com.dhltd.marketplace.mi.jaxb.request.SourceOfBusinessProfileType;
import com.dhltd.marketplace.mi.jaxb.request.UnderwriterType;
import com.filogix.express.core.ExpressRuntimeException;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.payload.IPayloadGenerator;
import com.filogix.externallinks.services.util.XMLUtil;

public abstract class MIRequestPayloadGenerator implements IPayloadGenerator {

    private final static Logger logger = LoggerFactory.getLogger(MIRequestPayloadGenerator.class);

    protected static JAXBContext jc;

    private static ObjectFactory factory = new ObjectFactory();

    private boolean formatted_output = true;

    private String encoding = "UTF-8";

    private boolean stopOnSQLError = false;

    private List<String> mandatoryTables;

    public String getXMLPayload(boolean validate, ServiceInfo info) throws JAXBException {

        String contextPath = MortgageInsuranceRequestType.class.getPackage().getName();
        //add classloader to newInstance to prevent jaxb.properties error in stress env.
        jc = JAXBContext.newInstance(contextPath, this.getClass().getClassLoader());
        Deal deal = info.getDeal();

        MortgageInsuranceRequestType mortgageInsuranceRequestType = factory.createMortgageInsuranceRequestType();
        mortgageInsuranceRequestType.setDeal(createDealType(deal));
        JAXBElement<MortgageInsuranceRequestType> mortgageInsuranceRequest = factory.createMortgageInsuranceRequest(mortgageInsuranceRequestType);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, encoding);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatted_output);
        
        StringWriter xmlWriter = new StringWriter();
        marshaller.marshal(mortgageInsuranceRequest, xmlWriter);
        return xmlWriter.toString();
        
    }


    protected DealType createDealType(Deal deal) throws JAXBException {

        DealType dealType = factory.createDealType();
        populateDealType(dealType, deal);

        createProperty(dealType, deal);
        createBorrower(dealType, deal);
        createDownPaymentSource(dealType, deal);
        createEscrowPayment(dealType, deal);
        createBranchProfile(dealType, deal);
        createInstitutionProfile(dealType, deal);
        createLenderProfile(dealType, deal);
        createUnderwriter(dealType, deal);
        createPartyProfile(dealType, deal);
        createMtgProd(dealType, deal);
        createSourceFirmProfile(dealType, deal);
        createSourceOfBusinessProfile(dealType, deal);
        return dealType;
    }

    protected void handleEntityException(Exception e, Class<? extends DealEntity> clazz) {

        String msg = "faild to find data for " + clazz.getName();

        if (stopOnSQLError) {
            if (mandatoryTables != null && clazz != null) {
                for (String table : mandatoryTables) {
                    if (table.equalsIgnoreCase(clazz.getSimpleName())) {
                        logger.error(msg, e);
                        throw new ExpressRuntimeException(msg, e);
                    }
                }
            } else {
                logger.error(msg, e);
                throw new ExpressRuntimeException(msg, e);
            }
        }
        logger.warn(msg);
    }

    protected void createPartyProfile(DealType dealType, Deal deal) throws JAXBException {

        List<PartyProfileType> partyProfileTypes = dealType.getPartyProfile();

        Collection<PartyProfile> partyProfiles;
        try {
            partyProfiles = deal.getPartyProfiles();
        } catch (Exception e) {
            handleEntityException(e, PartyProfile.class);
            return;
        }
        Iterator<PartyProfile> iter = partyProfiles.iterator();
        while (iter.hasNext()) {
            PartyProfile partyProfile = (PartyProfile) iter.next();
            PartyProfileType partyProfileType = factory.createPartyProfileType();

            populatePartyProfile(partyProfile, partyProfileType);

            Contact contact = null;
            try {
                contact = partyProfile.getContact();
            } catch (Exception e) {
                handleEntityException(e, Contact.class);
                return;
            }
            if (contact != null) {
                partyProfileType.setContact(factory.createPartyProfileTypeContact(createContactPayload(contact)));
            }

            partyProfileTypes.add(partyProfileType);
        }
    }

    protected void populatePartyProfile(PartyProfile partyProfile, PartyProfileType partyProfileType) {

        if (!XMLUtil.isEmpty(partyProfile.getPartyName())) {

            partyProfileType.setPartyName(factory.createPartyProfileTypePartyName(partyProfile.getPartyName()));
        }

        if (!XMLUtil.isEmpty(partyProfile.getProfileStatusId())) {
            partyProfileType.setProfileStatusId(XMLUtil.int2BigInteger(partyProfile.getProfileStatusId()));
        }

        if (!XMLUtil.isEmpty(partyProfile.getPartyTypeId())) {
            partyProfileType.setPartyTypeId(XMLUtil.int2BigInteger(partyProfile.getPartyTypeId()));
        }

        if (!XMLUtil.isEmpty(partyProfile.getPtPShortName())) {
            partyProfileType.setPtPShortName(factory.createPartyProfileTypePtPShortName(partyProfile.getPtPShortName()));
        }

        if (!XMLUtil.isEmpty(partyProfile.getPtPBusinessId())) {
            partyProfileType.setPtPBusinessId(factory.createPartyProfileTypePtPBusinessId(partyProfile.getPtPBusinessId()));
        }

        if (!XMLUtil.isEmpty(partyProfile.getPartyCompanyName())) {
            partyProfileType.setPartyCompanyName(factory.createPartyProfileTypePartyCompanyName(partyProfile.getPartyCompanyName()));
        }

        if (!XMLUtil.isEmpty(partyProfile.getNotes())) {
            partyProfileType.setNotes(factory.createPartyProfileTypeNotes(partyProfile.getNotes()));
        }
    }

    /*protected void createSourceOfBusinessProfile(DealType dealType, Deal deal) throws JAXBException {

        SourceOfBusinessProfile sourceOfBusinessProfile1 = null;
        SourceOfBusinessProfileType sourceOfBusinessProfileType1 = null;

        try {
            sourceOfBusinessProfile1 = deal.getSourceOfBusinessProfile();
        } catch (Exception e) {
            handleEntityException(e, SourceOfBusinessProfile.class);
        }

        if (sourceOfBusinessProfile1 != null) {
            sourceOfBusinessProfileType1 = factory.createSourceOfBusinessProfileType();
            populateSourceOfBusinessProfileType(sourceOfBusinessProfile1, sourceOfBusinessProfileType1);
            if (sourceOfBusinessProfileType1 != null) {
                dealType.setSourceOfBusinessProfile1(sourceOfBusinessProfileType1);
            }
        }

        SourceOfBusinessProfile sourceOfBusinessProfile2 = null;
        SourceOfBusinessProfileType sourceOfBusinessProfileType2 = null;
        try {
            sourceOfBusinessProfile2 = deal.getSourceOfBusinessProfile2ndary();
        } catch (Exception e) {
            handleEntityException(e, SourceOfBusinessProfile.class);
        }

        if (sourceOfBusinessProfile2 != null) {
            sourceOfBusinessProfileType2 = factory.createSourceOfBusinessProfileType();
            populateSourceOfBusinessProfileType(sourceOfBusinessProfile2, sourceOfBusinessProfileType2);
            if (sourceOfBusinessProfileType2 != null) {
                dealType.setSourceOfBusinessProfile2(sourceOfBusinessProfileType2);
            }
        }

        // special logic
        
         * if SourceOfBusinessProfile1 and SourceOfBusinessProfile2 exists and
         * they do not match then SourceOfBusinessProfile2 else
         * SourceOfBusinessProfile1
         * 
         * to sourceOfBusinessProfileType1.setSourceOfBusinessProfileId
         

        if (sourceOfBusinessProfileType1 != null) {
            sourceOfBusinessProfileType1
                .setSourceOfBusinessProfileId(XMLUtil.int2BigInteger(sourceOfBusinessProfile1.getSourceOfBusinessProfileId()));
        }
        if (sourceOfBusinessProfileType2 != null) {
            sourceOfBusinessProfileType2
                .setSourceOfBusinessProfileId(XMLUtil.int2BigInteger(sourceOfBusinessProfile2.getSourceOfBusinessProfileId()));
        }

        if (sourceOfBusinessProfileType1 != null && sourceOfBusinessProfileType2 != null
            && sourceOfBusinessProfile1.getSourceOfBusinessPriorityId() != sourceOfBusinessProfile2.getSourceOfBusinessProfileId()) {
            // both sob1 and sob2 exist and they are different, set
            // sob2.sobProfileid
            sourceOfBusinessProfileType1
                .setSourceOfBusinessProfileId(XMLUtil.int2BigInteger(sourceOfBusinessProfile2.getSourceOfBusinessProfileId()));
            sourceOfBusinessProfileType2
                .setSourceOfBusinessProfileId(XMLUtil.int2BigInteger(sourceOfBusinessProfile2.getSourceOfBusinessProfileId()));
        }

    }*/
    
    /*
     *  Ticket 414 - modified logic to create sourceOfBusinessProfileType1 and sourceOfBusinessProfileType2
     */
    protected void createSourceOfBusinessProfile(DealType dealType, Deal deal) throws JAXBException {

        SourceOfBusinessProfile sourceOfBusinessProfile1 = null;
        SourceOfBusinessProfileType sourceOfBusinessProfileType1 = null;

        try {
            sourceOfBusinessProfile1 = deal.getSourceOfBusinessProfile();
        } catch (Exception e) {
            handleEntityException(e, SourceOfBusinessProfile.class);
        }
        
        SourceOfBusinessProfile sourceOfBusinessProfile2 = null;
        SourceOfBusinessProfileType sourceOfBusinessProfileType2 = null;
        try {
            sourceOfBusinessProfile2 = deal.getSourceOfBusinessProfile2ndary();
        } catch (Exception e) {
            handleEntityException(e, SourceOfBusinessProfile.class);
        }
        
        sourceOfBusinessProfileType1 = factory.createSourceOfBusinessProfileType();
        
        /*Ticket 414 - if sourceOfBusinessProfile1 and sourceOfBusinessProfile2 exists 
         * then
         * set SourceOfBusinessProfile1 to sourceOfBusinessProfileType1 and SourceOfBusinessProfile2 to sourceOfBusinessProfileType2
         * else
         * set SourceOfBusinessProfile2 to sourceOfBusinessProfileType1 and do not create sourceOfBusinessProfileType2
        */
        if (sourceOfBusinessProfile1 != null && sourceOfBusinessProfile2 != null)
        {
        	populateSourceOfBusinessProfileType(sourceOfBusinessProfile1, sourceOfBusinessProfileType1);
        	sourceOfBusinessProfileType1.setSourceOfBusinessProfileId(XMLUtil.int2BigInteger(sourceOfBusinessProfile1.getSourceOfBusinessProfileId()));
        
        	sourceOfBusinessProfileType2 = factory.createSourceOfBusinessProfileType();
        	populateSourceOfBusinessProfileType(sourceOfBusinessProfile2, sourceOfBusinessProfileType2);
        	sourceOfBusinessProfileType2.setSourceOfBusinessProfileId(XMLUtil.int2BigInteger(sourceOfBusinessProfile2.getSourceOfBusinessProfileId()));
        }
        else
        {
        	if (sourceOfBusinessProfile2 != null)
        	{
        		populateSourceOfBusinessProfileType(sourceOfBusinessProfile2, sourceOfBusinessProfileType1);
        		sourceOfBusinessProfileType1.setSourceOfBusinessProfileId(XMLUtil.int2BigInteger(sourceOfBusinessProfile2.getSourceOfBusinessProfileId()));
        	}
        }
        
        if (sourceOfBusinessProfileType1 != null) 
        {
        	dealType.setSourceOfBusinessProfile1(sourceOfBusinessProfileType1);   
        }

        if (sourceOfBusinessProfileType2 != null) 
        {
            dealType.setSourceOfBusinessProfile2(sourceOfBusinessProfileType2);
        }

    }

    protected void populateSourceOfBusinessProfileType(SourceOfBusinessProfile sourceOfBusinessProfile,
        SourceOfBusinessProfileType sourceOfBusinessProfileType) throws JAXBException {

        if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getPartyProfileId())) {
            sourceOfBusinessProfileType.setPartyProfileId(XMLUtil.int2BigInteger(sourceOfBusinessProfile.getPartyProfileId()));
        }

        if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSOBPBusinessId())) {
            sourceOfBusinessProfileType.setSOBPBusinessId(sourceOfBusinessProfile.getSOBPBusinessId());
        }

        if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSOBPShortName())) {
            sourceOfBusinessProfileType.setSOBPShortName(sourceOfBusinessProfile.getSOBPShortName());
        }

        if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSOBRegionId())) {
            sourceOfBusinessProfileType.setSOBRegionId(XMLUtil.int2BigInteger(sourceOfBusinessProfile.getSOBRegionId()));
        }

        if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSourceOfBusinessCategoryId())) {
            sourceOfBusinessProfileType
                .setSourceOfBusinessCategoryId(XMLUtil.int2BigInteger(sourceOfBusinessProfile.getSourceOfBusinessCategoryId()));
        }

        if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSourceOfBusinessCode())) {
            sourceOfBusinessProfileType.setSourceOfBusinessCode(sourceOfBusinessProfile.getSourceOfBusinessCode());
        }

        if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSourceOfBusinessPriorityId())) {
            sourceOfBusinessProfileType
                .setSourceOfBusinessPriorityId(XMLUtil.int2BigInteger(sourceOfBusinessProfile.getSourceOfBusinessPriorityId()));
        }

        if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSystemTypeId())) {
            sourceOfBusinessProfileType.setSystemTypeId(XMLUtil.int2BigInteger(sourceOfBusinessProfile.getSystemTypeId()));
        }

        Contact contact = null;
        try {
            contact = sourceOfBusinessProfile.getContact();
        } catch (Exception e) {
            handleEntityException(e, Contact.class);
        }

        if (contact != null) {
            sourceOfBusinessProfileType.setContact(createContactPayload(contact));
        }

    }

    protected void createSourceFirmProfile(DealType dealType, Deal deal) throws JAXBException {

        SourceFirmProfile sourceFirmProfile = null;
        try {
            sourceFirmProfile = deal.getSourceFirmProfile();
        } catch (Exception e) {
            handleEntityException(e, SourceFirmProfile.class);
            return;
        }

        if (sourceFirmProfile == null)
            return;

        SourceFirmProfileType sourceFirmProfileType = factory.createSourceFirmProfileType();

        populateSourceFirmProfile(sourceFirmProfile, sourceFirmProfileType);

        Contact contact = null;
        try {
            contact = sourceFirmProfile.getContact();
        } catch (Exception e) {
            handleEntityException(e, Contact.class);
            return;
        }

        if (contact != null) {
            sourceFirmProfileType.setContact(createContactPayload(contact));
        }

        dealType.setSourceFirmProfile(sourceFirmProfileType);

    }

    protected void populateSourceFirmProfile(SourceFirmProfile sourceFirmProfile, SourceFirmProfileType sourceFirmProfileType) {

        if (!XMLUtil.isEmpty(sourceFirmProfile.getProfileStatusId())) {
            sourceFirmProfileType.setProfileStatusId(XMLUtil.int2BigInteger(sourceFirmProfile.getProfileStatusId()));
        }

        if (!XMLUtil.isEmpty(sourceFirmProfile.getSfBusinessId())) {
            sourceFirmProfileType.setSfBusinessId(sourceFirmProfile.getSfBusinessId());
        }

        if (!XMLUtil.isEmpty(sourceFirmProfile.getSfMOProfileId())) {
            sourceFirmProfileType.setSfMOProfileId(XMLUtil.int2BigInteger(sourceFirmProfile.getSfMOProfileId()));
        }

        if (!XMLUtil.isEmpty(sourceFirmProfile.getSfShortName())) {
            sourceFirmProfileType.setSfShortName(sourceFirmProfile.getSfShortName());
        }

        if (!XMLUtil.isEmpty(sourceFirmProfile.getSourceFirmCode())) {
            sourceFirmProfileType.setSourceFirmCode(sourceFirmProfile.getSourceFirmCode());
        }

        if (!XMLUtil.isEmpty(sourceFirmProfile.getSourceFirmName())) {
            sourceFirmProfileType.setSourceFirmName(sourceFirmProfile.getSourceFirmName());
        }

        if (!XMLUtil.isEmpty(sourceFirmProfile.getSystemTypeId())) {
            sourceFirmProfileType.setSystemTypeId(XMLUtil.int2BigInteger(sourceFirmProfile.getSystemTypeId()));
        }
    }

    protected void populateMtgProdType(MtgProdType mtgProdType, MtgProd mtgProd) {

        if (!XMLUtil.isEmpty(mtgProd.getInterestTypeId())) {
            mtgProdType.setInterestTypeId(XMLUtil.int2BigInteger(mtgProd.getInterestTypeId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMtgProdName())) {
            mtgProdType.setMtgProdName(mtgProd.getMtgProdName());
        }

        if (!XMLUtil.isEmpty(mtgProd.getMinimumAmount())) {
            mtgProdType.setMinimumAmount(XMLUtil.double2BigDecimal(mtgProd.getMinimumAmount()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMaximumAmount())) {
            mtgProdType.setMaximumAmount(XMLUtil.double2BigDecimal(mtgProd.getMaximumAmount()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMinimumLTV())) {
            mtgProdType.setMinimumLTV(XMLUtil.double2BigDecimal(mtgProd.getMinimumLTV()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMaximumLTV())) {
            mtgProdType.setMaximumLTV(XMLUtil.double2BigDecimal(mtgProd.getMaximumLTV()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getPaymentTermId())) {
            mtgProdType.setPaymentTermId(XMLUtil.int2BigInteger(mtgProd.getPaymentTermId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getLineOfBusinessId())) {
            mtgProdType.setLineOfBusinessId(XMLUtil.int2BigInteger(mtgProd.getLineOfBusinessId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getCommitmentTerm())) {
            mtgProdType.setCommitmentTerm(mtgProd.getCommitmentTerm());
        }

        if (!XMLUtil.isEmpty(mtgProd.getMPBusinessId())) {
            mtgProdType.setMpBusinessId(mtgProd.getMPBusinessId());
        }

        if (!XMLUtil.isEmpty(mtgProd.getPrePaymentOptionsId())) {
            mtgProdType.setPrePaymentOptionsId(XMLUtil.int2BigInteger(mtgProd.getPrePaymentOptionsId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getPrivilegePaymentId())) {
            mtgProdType.setPrivilegePaymentId(XMLUtil.int2BigInteger(mtgProd.getPrivilegePaymentId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMPShortName())) {
            mtgProdType.setMpShortName(mtgProd.getMPShortName());
        }

        if (!XMLUtil.isEmpty(mtgProd.getInterestCompoundingId())) {
            mtgProdType.setInterestCompoundingId(XMLUtil.int2BigInteger(mtgProd.getInterestCompoundingId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMiscellaneousRate())) {
            mtgProdType.setMiscellaneousRate(XMLUtil.double2BigDecimal(mtgProd.getMiscellaneousRate()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getUnderwriteAsTypeId())) {
            mtgProdType.setUnderWriteAsTypeId(XMLUtil.int2BigInteger(mtgProd.getUnderwriteAsTypeId()));
        }
    }

    protected void populatePricingProfilePayload(PricingProfileType pricingProfileType, PricingProfile pricingProfile) throws JAXBException {

        if (!XMLUtil.isEmpty(pricingProfile.getProfileStatusId())) {
            pricingProfileType.setProfileStatusId(XMLUtil.int2BigInteger(pricingProfile.getProfileStatusId()));
        }

        if (!XMLUtil.isEmpty(pricingProfile.getPPBusinessId())) {
            pricingProfileType.setPpBusinessId(pricingProfile.getPPBusinessId());
        }

        // questionable int - > double -- down-cast
        pricingProfileType.setInterestRateChangeFrequency(XMLUtil.int2BigInteger((int) pricingProfile.getInterestRateChangeFrequency()));
        pricingProfileType.setPricingRegistrationTerm(XMLUtil.int2BigInteger((int) pricingProfile.getPricingRegistrationTerm()));

        pricingProfileType.setPrimeIndexIndicator(XMLUtil.boolean2String(pricingProfile.getPrimeIndexIndicator()));

        if (!XMLUtil.isEmpty(pricingProfile.getPricingStatusId())) {
            pricingProfileType.setPricingStatusId(XMLUtil.int2BigInteger(pricingProfile.getPricingStatusId()));
        }

        if (!XMLUtil.isEmpty(pricingProfile.getRateCode())) {
            pricingProfileType.setRateCode(pricingProfile.getRateCode());
        }

    }

    protected void createMtgProd(DealType dealType, Deal deal) throws JAXBException {
        MtgProd mtgProd;
        try {
            mtgProd = deal.getMtgProd();
        } catch (Exception e) {
            handleEntityException(e, MtgProd.class);
            return;
        }

        if (mtgProd == null)
            return;

        MtgProdType mtgProdType = factory.createMtgProdType();

        populateMtgProdType(mtgProdType, mtgProd);

        PricingProfile pricingProfile = null;
        try {
            pricingProfile = mtgProd.getPricingProfile();
        } catch (Exception e) {
            handleEntityException(e, PricingProfile.class);
        }

        if (pricingProfile != null) {
            PricingProfileType pricingProfileType = factory.createPricingProfileType();
            populatePricingProfilePayload(pricingProfileType, pricingProfile);
            mtgProdType.setPricingProfile(pricingProfileType);
        }

        dealType.setMtgProd(mtgProdType);

    }

    protected void createUnderwriter(DealType dealType, Deal deal) throws JAXBException {
        UserProfile userProfile = null;
        try {
            userProfile = new UserProfile(deal.getSessionResourceKit());
            userProfile.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId(), deal.getInstitutionProfileId()));

        } catch (Exception e) {
            handleEntityException(e, UserProfile.class);
            return;
        }

        if (userProfile != null) {

            Contact contact;
            try {
                contact = userProfile.getContact();
            } catch (Exception e) {
                handleEntityException(e, Contact.class);
                return;
            }

            if (contact != null) {
                UnderwriterType underwriterType = factory.createUnderwriterType();
                underwriterType.setContact(createContactPayload(contact));
                dealType.setUnderwriter(underwriterType);
            }
        }

    }

    protected void createLenderProfile(DealType dealType, Deal deal) throws JAXBException {

        LenderProfileType lenderProfileType = factory.createLenderProfileType();
        LenderProfile lenderProfile;
        try {
            lenderProfile = deal.getLenderProfile();
        } catch (Exception e) {
            handleEntityException(e, LenderProfile.class);
            return;
        }

        populateLenderProfile(lenderProfileType, lenderProfile);

        dealType.setLenderProfile(lenderProfileType);

        Contact contact;
        try {
            contact = lenderProfile.getContact();
        } catch (Exception e) {
            handleEntityException(e, Contact.class);
            return;
        }

        if (contact != null) {
            lenderProfileType.setContact(createContactPayload(contact));
        }

    }

    protected void populateLenderProfile(LenderProfileType lenderProfileType, LenderProfile lenderProfile) {

        if (!XMLUtil.isEmpty(lenderProfile.getDefaultLender())) {
            lenderProfileType.setDefaultLender(String.valueOf(lenderProfile.getDefaultLender()));
        }

        if (!XMLUtil.isEmpty(lenderProfile.getLenderName())) {
            lenderProfileType.setLenderName(lenderProfile.getLenderName());
        }

        if (!XMLUtil.isEmpty(lenderProfile.getProfileStatusId())) {
            lenderProfileType.setProfileStatusId(XMLUtil.int2BigInteger(lenderProfile.getProfileStatusId()));
        }

        if (!XMLUtil.isEmpty(lenderProfile.getLPShortName())) {
            lenderProfileType.setLpShortName(lenderProfile.getLPShortName());
        }

        if (!XMLUtil.isEmpty(lenderProfile.getLPBusinessId())) {
            lenderProfileType.setLpBusinessId(lenderProfile.getLPBusinessId());
        }

        lenderProfileType.setUpfrontMIAllowed(XMLUtil.boolean2String(lenderProfile.getUpfrontMIAllowed()));

        if (!XMLUtil.isEmpty(lenderProfile.getDefaultInvestorId())) {
            lenderProfileType.setDefaultInvestorId(XMLUtil.int2BigInteger(lenderProfile.getDefaultInvestorId()));
        }

        if (!XMLUtil.isEmpty(lenderProfile.getPrivateLabelIndicator())) {
            lenderProfileType.setPrivateLabelIndicator(lenderProfile.getPrivateLabelIndicator());
        }

        if (!XMLUtil.isEmpty(lenderProfile.getRegistrationName())) {
            lenderProfileType.setRegistrationName(lenderProfile.getRegistrationName());
        }
    }

    protected void createInstitutionProfile(DealType dealType, Deal deal) throws JAXBException {

        InstitutionProfile institutionProfile;
        try {
            institutionProfile = new InstitutionProfile(deal.getSessionResourceKit());
            institutionProfile = institutionProfile.findByPrimaryKey(new InstitutionProfilePK(deal.getInstitutionProfileId()));
        } catch (Exception e) {
            handleEntityException(e, InstitutionProfile.class);
            return;
        }

        InstitutionProfileType institutionProfileType = factory.createInstitutionProfileType();

        populateInstitutionProfile(institutionProfileType, institutionProfile);

        dealType.setInstitutionProfile(institutionProfileType);

    }

    protected void populateInstitutionProfile(InstitutionProfileType institutionProfileType, InstitutionProfile institutionProfile) {

        if (!XMLUtil.isEmpty(institutionProfile.getInstitutionName())) {
            institutionProfileType.setInstitutionName(institutionProfile.getInstitutionName());
        }

        if (!XMLUtil.isEmpty(institutionProfile.getBrandName())) {
            institutionProfileType.setBrandName(institutionProfile.getBrandName());
        }
    }

    protected void createBranchProfile(DealType dealType, Deal deal) throws JAXBException {

        BranchProfile branchProfile;
        try {
            branchProfile = new BranchProfile(deal.getSessionResourceKit(), deal.getBranchProfileId());
        } catch (Exception e) {
            handleEntityException(e, BranchProfile.class);
            return;
        }

        BranchProfileType branchProfileType = factory.createBranchProfileType();

        populateBranchProfile(branchProfile, branchProfileType);

        dealType.setBranchProfile(branchProfileType);

        Contact contact;
        try {
            contact = branchProfile.getContact();
        } catch (Exception e) {
            handleEntityException(e, Contact.class);
            return;
        }

        if (contact != null) {
            branchProfileType.setContact(createContactPayload(contact));
        }

    }

    protected void populateBranchProfile(BranchProfile branchProfile, BranchProfileType branchProfileType) {

        if (!XMLUtil.isEmpty(branchProfile.getBranchName())) {
            branchProfileType.setBranchName(branchProfile.getBranchName());
        }

        if (!XMLUtil.isEmpty(branchProfile.getRegionProfileId())) {
            branchProfileType.setRegionProfileId(XMLUtil.int2BigInteger(branchProfile.getRegionProfileId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getBPShortName())) {
            branchProfileType.setBPShortName(branchProfile.getBPShortName());
        }

        if (!XMLUtil.isEmpty(branchProfile.getBPBusinessId())) {
            branchProfileType.setBPBusinessId(branchProfile.getBPBusinessId());
        }

        if (!XMLUtil.isEmpty(branchProfile.getTimeZoneEntryId())) {
            branchProfileType.setTimeZoneEntryId(XMLUtil.int2BigInteger(branchProfile.getTimeZoneEntryId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getPartyProfileId())) {
            branchProfileType.setPartyProfileId(XMLUtil.int2BigInteger(branchProfile.getPartyProfileId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getBranchTypeId())) {
            branchProfileType.setBranchTypeId(XMLUtil.int2BigInteger(branchProfile.getBranchTypeId()));
        }
        
        if (!XMLUtil.isEmpty(branchProfile.getFaxCertificate())) {
            branchProfileType.setFaxCertificate(branchProfile.getFaxCertificate());
        }

    }

    protected void createEscrowPayment(DealType dealType, Deal deal) throws JAXBException {

        Collection<EscrowPayment> escrowPayments;
        try {
            escrowPayments = deal.getEscrowPayments();
        } catch (Exception e) {
            handleEntityException(e, EscrowPayment.class);
            return;
        }

        List<EscrowPaymentType> escrowPaymentTypes = dealType.getEscrowPayment();

        Iterator<EscrowPayment> iter = escrowPayments.iterator();
        while (iter.hasNext()) {

            EscrowPayment escrowPayment = (EscrowPayment) iter.next();
            EscrowPaymentType escrowPaymentType = factory.createEscrowPaymentType();

            populateEscrowPayment(escrowPaymentType, escrowPayment);

            escrowPaymentTypes.add(escrowPaymentType);
        }
    }

    protected void populateEscrowPayment(EscrowPaymentType escrowPaymentType, EscrowPayment escrowPayment) {

        if (!XMLUtil.isEmpty(escrowPayment.getEscrowTypeId())) {
            escrowPaymentType.setEscrowTypeId(XMLUtil.int2BigInteger(escrowPayment.getEscrowTypeId()));
        }

        if (!XMLUtil.isEmpty(escrowPayment.getEscrowPaymentDescription())) {
            escrowPaymentType.setEscrowPaymentDescription(escrowPayment.getEscrowPaymentDescription());
        }

        if (!XMLUtil.isEmpty(escrowPayment.getEscrowPaymentAmount())) {
            escrowPaymentType.setEscrowPaymentAmount(XMLUtil.double2BigDecimal(escrowPayment.getEscrowPaymentAmount()));
        }
    }

    protected void createDownPaymentSource(DealType dealType, Deal deal) throws JAXBException {

        Collection<DownPaymentSource> downPaymentSources;
        try {
            downPaymentSources = deal.getDownPaymentSources();
        } catch (Exception e) {
            handleEntityException(e, DownPaymentSource.class);
            return;
        }

        List<DownPaymentSourceType> downPaymentSourceTypes = dealType.getDownPaymentSource();

        Iterator<DownPaymentSource> iter = downPaymentSources.iterator();
        while (iter.hasNext()) {

            DownPaymentSource downPaymentSource = (DownPaymentSource) iter.next();
            DownPaymentSourceType downPaymentSourceType = factory.createDownPaymentSourceType();

            populateDownPaymentSource(downPaymentSourceType, downPaymentSource);

            downPaymentSourceTypes.add(downPaymentSourceType);
        }
    }

    protected void populateDownPaymentSource(DownPaymentSourceType downPaymentSourceType, DownPaymentSource downPaymentSource) {

        if (!XMLUtil.isEmpty(downPaymentSource.getDownPaymentSourceTypeId())) {
            downPaymentSourceType.setDownPaymentSourceTypeId(XMLUtil.int2BigInteger(downPaymentSource.getDownPaymentSourceTypeId()));
        }

        if (!XMLUtil.isEmpty(downPaymentSource.getAmount())) {
            downPaymentSourceType.setAmount(XMLUtil.double2BigDecimal(downPaymentSource.getAmount()));
        }

        if (!XMLUtil.isEmpty(downPaymentSource.getDPSDescription())) {
            downPaymentSourceType.setDPSDescription(downPaymentSource.getDPSDescription());
        }
    }

    protected void populateBorrowerType(BorrowerType borrowerType, Borrower borrower) {

        if (!XMLUtil.isEmpty(borrower.getBorrowerNumber())) {
            borrowerType.setBorrowerNumber(XMLUtil.int2BigInteger(borrower.getBorrowerNumber()));
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerFirstName())) {
            borrowerType.setBorrowerFirstName(borrower.getBorrowerFirstName());
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerMiddleInitial())) {
            borrowerType.setBorrowerMiddleInitial(borrower.getBorrowerMiddleInitial());
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerLastName())) {
            borrowerType.setBorrowerLastName(borrower.getBorrowerLastName());
        }

        if (!XMLUtil.isEmpty(borrower.getFirstTimeBuyer())) {
            borrowerType.setFirstTimeBuyer(borrower.getFirstTimeBuyer());
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerBirthDate())) {
            borrowerType.setBorrowerBirthDate(XMLUtil.date2XMLGregorianCalendar(borrower.getBorrowerBirthDate()));
        }

        if (!XMLUtil.isEmpty(borrower.getSalutationId())) {
            borrowerType.setSalutationId(XMLUtil.int2BigInteger(borrower.getSalutationId()));
        }

        if (!XMLUtil.isEmpty(borrower.getMaritalStatusId())) {
            borrowerType.setMaritalStatusId(XMLUtil.int2BigInteger(borrower.getMaritalStatusId()));
        }

        if (!XMLUtil.isEmpty(borrower.getSocialInsuranceNumber())) {
            borrowerType.setSocialInsuranceNumber(borrower.getSocialInsuranceNumber());
        }

        if (!XMLUtil.isEmpty(borrower.getClientReferenceNumber())) {
            borrowerType.setClientReferenceNumber(borrower.getClientReferenceNumber());
        }

        if (!XMLUtil.isEmpty(borrower.getNumberOfDependents())) {
            borrowerType.setNumberOfDependents(XMLUtil.int2BigInteger(borrower.getNumberOfDependents()));
        }

        if (!XMLUtil.isEmpty(borrower.getNumberOfTimesBankrupt())) {
            borrowerType.setNumberOfTimesBankrupt(XMLUtil.int2BigInteger(borrower.getNumberOfTimesBankrupt()));
        }

        if (!XMLUtil.isEmpty(borrower.getCreditScore())) {
            borrowerType.setCreditScore(XMLUtil.int2BigInteger(borrower.getCreditScore()));
        }

        if (!XMLUtil.isEmpty(borrower.getGDS())) {
            borrowerType.setGDS(XMLUtil.double2BigDecimal(borrower.getGDS()));
        }

        if (!XMLUtil.isEmpty(borrower.getTDS())) {
            borrowerType.setTDS(XMLUtil.double2BigDecimal(borrower.getTDS()));
        }

        if (!XMLUtil.isEmpty(borrower.getNetWorth())) {
            borrowerType.setNetWorth(XMLUtil.double2BigDecimal(borrower.getNetWorth()));
        }

        if (!XMLUtil.isEmpty(borrower.getExistingClient())) {
            borrowerType.setExistingClient(borrower.getExistingClient());
        }

        if (!XMLUtil.isEmpty(borrower.getLanguagePreferenceId())) {
            borrowerType.setLanguagePreferenceId(XMLUtil.int2BigInteger(borrower.getLanguagePreferenceId()));
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerGeneralStatusId())) {
            borrowerType.setBorrowerGeneralStatusId(XMLUtil.int2BigInteger(borrower.getBorrowerGeneralStatusId()));
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerTypeId())) {
            borrowerType.setBorrowerTypeId(XMLUtil.int2BigInteger(borrower.getBorrowerTypeId()));
        }

        if (!XMLUtil.isEmpty(borrower.getCitizenshipTypeId())) {
            borrowerType.setCitizenshipTypeId(XMLUtil.int2BigInteger(borrower.getCitizenshipTypeId()));
        }

        if (!XMLUtil.isEmpty(borrower.getPrimaryBorrowerFlag())) {
            borrowerType.setPrimaryBorrowerFlag(borrower.getPrimaryBorrowerFlag());
        }

        if (!XMLUtil.isEmpty(borrower.getTotalAssetAmount())) {
            borrowerType.setTotalAssetAmount(XMLUtil.double2BigDecimal(borrower.getTotalAssetAmount()));
        }

        if (!XMLUtil.isEmpty(borrower.getTotalLiabilityAmount())) {
            borrowerType.setTotalLiabilityAmount(XMLUtil.double2BigDecimal(borrower.getTotalLiabilityAmount()));
        }

        if (!XMLUtil.isEmpty(borrower.getPaymentHistoryTypeId())) {
            borrowerType.setPaymentHistoryTypeId(XMLUtil.int2BigInteger(borrower.getPaymentHistoryTypeId()));
        }

        if (!XMLUtil.isEmpty(borrower.getTotalIncomeAmount())) {
            borrowerType.setTotalIncomeAmount(XMLUtil.double2BigDecimal(borrower.getTotalIncomeAmount()));
        }

        if (!XMLUtil.isEmpty(borrower.getBankruptcyStatusId())) {
            borrowerType.setBankruptcyStatusId(XMLUtil.int2BigInteger(borrower.getBankruptcyStatusId()));
        }

        if (!XMLUtil.isEmpty(borrower.getAge())) {
            borrowerType.setAge(XMLUtil.int2BigInteger(borrower.getAge()));
        }

        borrowerType.setStaffOfLender(XMLUtil.boolean2String(borrower.getStaffOfLender()));

        if (!XMLUtil.isEmpty(borrower.getTotalLiabilityPayments())) {
            borrowerType.setTotalLiabilityPayments(XMLUtil.double2BigDecimal(borrower.getTotalLiabilityPayments()));
        }

        if (!XMLUtil.isEmpty(borrower.getGDS3Year())) {
            borrowerType.setGDS3Year(XMLUtil.double2BigDecimal(borrower.getGDS3Year()));
        }

        if (!XMLUtil.isEmpty(borrower.getTDS3Year())) {
            borrowerType.setTDS3Year(XMLUtil.double2BigDecimal(borrower.getTDS3Year()));
        }

        if (!XMLUtil.isEmpty(borrower.getSINCheckDigit())) {
            borrowerType.setSINCheckDigit(borrower.getSINCheckDigit());
        }

        if (!XMLUtil.isEmpty(borrower.getCreditBureauNameId())) {
            borrowerType.setCreditBureauNameId(XMLUtil.int2BigInteger(borrower.getCreditBureauNameId()));
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerGenderId())) {
            borrowerType.setBorrowerGenderId(XMLUtil.int2BigInteger(borrower.getBorrowerGenderId()));
        }

        if (!XMLUtil.isEmpty(borrower.getGuarantorOtherLoans())) {
            borrowerType.setGuarantorOtherLoans(borrower.getGuarantorOtherLoans());
        }

    }

    protected AddrType createAddrTypePayload(Addr addr) throws JAXBException {

        AddrType addrType = factory.createAddrType();

        populateAddrTypePayload(addrType, addr);

        return addrType;

    }

    protected void populateAddrTypePayload(AddrType addrType, Addr addr) {

        if (!XMLUtil.isEmpty(addr.getProvinceId())) {
            addrType.setProvinceId(XMLUtil.int2BigInteger(addr.getProvinceId()));
        }

        if (!XMLUtil.isEmpty(addr.getAddressLine1())) {
            addrType.setAddressLine1(addr.getAddressLine1());
        }

        if (!XMLUtil.isEmpty(addr.getAddressLine2())) {
            addrType.setAddressLine2(addr.getAddressLine2());
        }

        if (!XMLUtil.isEmpty(addr.getCity())) {
            addrType.setCity(addr.getCity());
        }

        if (!XMLUtil.isEmpty(addr.getPostalFSA())) {
            addrType.setPostalFSA(addr.getPostalFSA());
        }

        if (!XMLUtil.isEmpty(addr.getPostalLDU())) {
            addrType.setPostalLDU(addr.getPostalLDU());
        }

        if (!XMLUtil.isEmpty(addr.getStreetNumber())) {
            addrType.setStreetNumber(addr.getStreetNumber());
        }

        if (!XMLUtil.isEmpty(addr.getStreetName())) {
            addrType.setStreetName(addr.getStreetName());
        }

        if (!XMLUtil.isEmpty(addr.getStreetTypeId())) {
            addrType.setStreetTypeId(XMLUtil.int2BigInteger(addr.getStreetTypeId()));
        }

        if (!XMLUtil.isEmpty(addr.getStreetDirectionId())) {
            addrType.setStreetDirectionId(XMLUtil.int2BigInteger(addr.getStreetDirectionId()));
        }

        if (!XMLUtil.isEmpty(addr.getUnitNumber())) {
            addrType.setUnitNumber(addr.getUnitNumber());
        }
    }

    protected void createBorrowerPhone(BorrowerType borrowerType, Borrower borrower) throws JAXBException {

        BorrowerPhone phoneType = factory.createBorrowerTypeBorrowerPhone();

        populateBorrowerPhone(borrower, phoneType);

        borrowerType.setBorrowerPhone(phoneType);
    }

    protected void populateBorrowerPhone(Borrower borrower, BorrowerPhone phoneType) {

        if (!XMLUtil.isEmpty(borrower.getBorrowerHomePhoneNumber()))
            phoneType.setBorrowerHomePhoneNumber(borrower.getBorrowerHomePhoneNumber());
        if (!XMLUtil.isEmpty(borrower.getBorrowerCellPhoneNumber()))
            phoneType.setBorrowerCellPhoneNumber(borrower.getBorrowerCellPhoneNumber());
        if (!XMLUtil.isEmpty(borrower.getBorrowerWorkPhoneNumber()))
            phoneType.setBorrowerWorkPhoneNumber(borrower.getBorrowerWorkPhoneNumber());
        if (!XMLUtil.isEmpty(borrower.getBorrowerWorkPhoneExtension()))
            phoneType.setBorrowerWorkPhoneExtension(borrower.getBorrowerWorkPhoneExtension());
        if (!XMLUtil.isEmpty(borrower.getBorrowerFaxNumber()))
            phoneType.setBorrowerFaxNumber(borrower.getBorrowerFaxNumber());
    }

    protected void createBorrowerAddress(BorrowerType borrowerType, Borrower borrower) throws JAXBException {

        Collection<BorrowerAddress> borrowerAddresses;
        try {
            borrowerAddresses = borrower.getBorrowerAddresses();
        } catch (Exception e) {
            handleEntityException(e, BorrowerAddress.class);
            return;
        }

        List<BorrowerAddressType> borrowerAddressTypes = borrowerType.getBorrowerAddress();

        Iterator<BorrowerAddress> iter = borrowerAddresses.iterator();

        while (iter.hasNext()) {

            BorrowerAddress borrowerAddress = (BorrowerAddress) iter.next();
            BorrowerAddressType borrowerAddressType = factory.createBorrowerAddressType();

            populateBorrowerAddress(borrowerAddressType, borrowerAddress);

            borrowerAddressTypes.add(borrowerAddressType);

            Addr addr = null;
            try {
                addr = borrowerAddress.getAddr();
            } catch (Exception e) {
                handleEntityException(e, Addr.class);
            }
            if (addr != null) {
                borrowerAddressType.setAddr(createAddrTypePayload(addr));
            }
        }
    }

    protected void populateBorrowerAddress(BorrowerAddressType borrowerAddressType, BorrowerAddress borrowerAddress) {

        if (!XMLUtil.isEmpty(borrowerAddress.getBorrowerAddressTypeId())) {
            borrowerAddressType.setBorrowerAddressTypeId(XMLUtil.int2BigInteger(borrowerAddress.getBorrowerAddressTypeId()));
        }

        if (!XMLUtil.isEmpty(borrowerAddress.getMonthsAtAddress())) {
            borrowerAddressType.setMonthsAtAddress(XMLUtil.int2BigInteger(borrowerAddress.getMonthsAtAddress()));
        }

        if (!XMLUtil.isEmpty(borrowerAddress.getResidentialStatusId())) {
            borrowerAddressType.setResidentialStatusId(XMLUtil.int2BigInteger(borrowerAddress.getResidentialStatusId()));
        }
    }

    protected void createBorrower(DealType dealType, Deal deal) throws JAXBException {

        Collection<Borrower> borrowers;
        try {
            borrowers = deal.getBorrowers();
        } catch (Exception e) {
            handleEntityException(e, Borrower.class);
            return;
        }

        List<BorrowerType> borrowerTypes = dealType.getBorrower();

        Iterator<Borrower> iter = borrowers.iterator();
        while (iter.hasNext()) {

            Borrower borrower = (Borrower) iter.next();
            BorrowerType borrowerType = factory.createBorrowerType();

            populateBorrowerType(borrowerType, borrower);

            createBorrowerPhone(borrowerType, borrower);
            createBorrowerAddress(borrowerType, borrower);
            createIncome(borrowerType, borrower);
            createAsset(borrowerType, borrower);
            createLiability(borrowerType, borrower);
            createCreditReference(borrowerType, borrower);
            createEmploymentHistory(borrowerType, borrower);

            borrowerTypes.add(borrowerType);
        }
    }

    protected void createEmploymentHistory(BorrowerType borrowerType, Borrower borrower) throws JAXBException {

        Collection<EmploymentHistory> employmentHistories;
        try {
            employmentHistories = borrower.getEmploymentHistories();
        } catch (Exception e) {
            handleEntityException(e, EmploymentHistory.class);
            return;
        }

        List<EmploymentHistoryType> employmentHistoryTypes = borrowerType.getEmploymentHistory();

        Iterator<EmploymentHistory> iter = employmentHistories.iterator();

        while (iter.hasNext()) {

            EmploymentHistoryType employmentHistoryType = factory.createEmploymentHistoryType();
            EmploymentHistory employmentHistory = (EmploymentHistory) iter.next();

            populateEmploymentHistory(employmentHistoryType, employmentHistory);

            employmentHistoryTypes.add(employmentHistoryType);

            Contact contact = null;
            try {
                contact = employmentHistory.getContact();
            } catch (Exception e) {
                handleEntityException(e, Contact.class);
            }

            if (contact != null) {
                employmentHistoryType.setContact(createContactPayload(contact));
            }

        }
    }

    protected void populateEmploymentHistory(EmploymentHistoryType employmentHistoryType, EmploymentHistory employmentHistory) {

        if (!XMLUtil.isEmpty(employmentHistory.getEmployerName())) {
            employmentHistoryType.setEmployerName(employmentHistory.getEmployerName());
        }

        if (!XMLUtil.isEmpty(employmentHistory.getEmploymentHistoryId())) {
            employmentHistoryType.setEmploymentHistoryStatusId(XMLUtil.int2BigInteger(employmentHistory.getEmploymentHistoryStatusId()));
        }

        if (!XMLUtil.isEmpty(employmentHistory.getEmploymentHistoryTypeId())) {
            employmentHistoryType.setEmploymentHistoryTypeId(XMLUtil.int2BigInteger(employmentHistory.getEmploymentHistoryTypeId()));
        }

        if (!XMLUtil.isEmpty(employmentHistory.getIncomeId())) {
            employmentHistoryType.setIncomeId(XMLUtil.int2BigInteger(employmentHistory.getIncomeId()));
        }

        if (!XMLUtil.isEmpty(employmentHistory.getIndustrySectorId())) {
            employmentHistoryType.setIndustrySectorId(XMLUtil.int2BigInteger(employmentHistory.getIndustrySectorId()));
        }

        if (!XMLUtil.isEmpty(employmentHistory.getJobTitle())) {
            employmentHistoryType.setJobTitleId(XMLUtil.int2BigInteger(employmentHistory.getJobTitleId()));
        }

        if (!XMLUtil.isEmpty(employmentHistory.getMonthsOfService())) {
            employmentHistoryType.setMonthsOfService(XMLUtil.int2BigInteger(employmentHistory.getMonthsOfService()));
        }

        if (!XMLUtil.isEmpty(employmentHistory.getOccupationId())) {
            employmentHistoryType.setOccupationId(XMLUtil.int2BigInteger(employmentHistory.getOccupationId()));
        }
    }

    protected void populateContactType(ContactType contactType, Contact contact) {

        if (!XMLUtil.isEmpty(contact.getContactEmailAddress())) {
            contactType.setContactEmailAddress(contact.getContactEmailAddress());
        }

        if (!XMLUtil.isEmpty(contact.getContactFaxNumber())) {
            contactType.setContactFaxNumber(contact.getContactFaxNumber());
        }

        if (!XMLUtil.isEmpty(contact.getContactFirstName())) {
            contactType.setContactFirstName(contact.getContactFirstName());
        }

        if (!XMLUtil.isEmpty(contact.getContactLastName())) {
            contactType.setContactLastName(contact.getContactLastName());
        }

        if (!XMLUtil.isEmpty(contact.getContactMiddleInitial())) {
            contactType.setContactMiddleInitial(contact.getContactMiddleInitial());
        }

        if (!XMLUtil.isEmpty(contact.getContactPhoneNumber())) {
            contactType.setContactPhoneNumber(contact.getContactPhoneNumber());
        }

        if (!XMLUtil.isEmpty(contact.getContactPhoneNumberExtension())) {
            contactType.setContactPhoneNumberExtension(contact.getContactPhoneNumberExtension());
        }

        if (!XMLUtil.isEmpty(contact.getPreferredDeliveryMethodId())) {
            contactType.setPreferredDeliveryMethodId(XMLUtil.int2BigInteger(contact.getPreferredDeliveryMethodId()));
        }

        if (!XMLUtil.isEmpty(contact.getSalutationId())) {
            contactType.setSalutationId(XMLUtil.int2BigInteger(contact.getSalutationId()));
        }
        
        //QC459 - set language to default to english whenever not set to french.
        if (contact.getLanguagePreferenceId() == Mc.LANGUAGE_PREFERENCE_FRENCH)
        	contactType.setLanguagePreferenceId(XMLUtil.int2BigInteger(Mc.LANGUAGE_PREFERENCE_FRENCH));
        else
        	contactType.setLanguagePreferenceId(XMLUtil.int2BigInteger(Mc.LANGUAGE_PREFERENCE_ENGLISH));
    }

    protected ContactType createContactPayload(Contact contact) throws JAXBException {

        ContactType contactType = factory.createContactType();
        populateContactType(contactType, contact);

        Addr addr = null;
        try {
            addr = contact.getAddr();
        } catch (Exception e) {
            handleEntityException(e, Addr.class);
        }
        if (addr != null) {
            contactType.setAddr(createAddrTypePayload(addr));
        }
        return contactType;
    }

    protected void createCreditReference(BorrowerType borrowerType, Borrower borrower) throws JAXBException {

        Collection<CreditReference> creditReferences;
        try {
            creditReferences = borrower.getCreditReferences();
        } catch (Exception e) {
            handleEntityException(e, CreditReference.class);
            return;
        }

        List<CreditReferenceType> creditReferenceTypes = borrowerType.getCreditReference();

        Iterator<CreditReference> iter = creditReferences.iterator();
        while (iter.hasNext()) {

            CreditReferenceType creditReferenceType = factory.createCreditReferenceType();
            CreditReference creditReference = (CreditReference) iter.next();

            populateCreditReference(creditReferenceType, creditReference);

            creditReferenceTypes.add(creditReferenceType);
        }
    }

    protected void populateCreditReference(CreditReferenceType creditReferenceType, CreditReference creditReference) {

        if (!XMLUtil.isEmpty(creditReference.getInstitutionName())) {
            creditReferenceType.setInstitutionName(creditReference.getInstitutionName());
        }

        if (!XMLUtil.isEmpty(creditReference.getAccountNumber())) {
            creditReferenceType.setAccountNumber(creditReference.getAccountNumber());
        }

        if (!XMLUtil.isEmpty(creditReference.getCurrentBalance())) {
            creditReferenceType.setCurrentBalance(XMLUtil.double2BigDecimal(creditReference.getCurrentBalance()));
        }

        if (!XMLUtil.isEmpty(creditReference.getCreditRefTypeId())) {
            creditReferenceType.setCreditRefTypeId(XMLUtil.int2BigInteger(creditReference.getCreditRefTypeId()));
        }

        if (!XMLUtil.isEmpty(creditReference.getCreditReferenceDescription())) {
            creditReferenceType.setCreditReferenceDescription(creditReference.getCreditReferenceDescription());
        }

        if (!XMLUtil.isEmpty(creditReference.getTimeWithReference())) {
            creditReferenceType.setTimeWithReference(XMLUtil.int2BigInteger(creditReference.getTimeWithReference()));
        }
    }

    protected void createLiability(BorrowerType borrowerType, Borrower borrower) throws JAXBException {

        Collection<Liability> liabilities;
        try {
            liabilities = borrower.getLiabilities();
        } catch (Exception e) {
            handleEntityException(e, Liability.class);
            return;
        }

        List<LiabilityType> liabilityTypes = borrowerType.getLiability();

        Iterator<Liability> iter = liabilities.iterator();
        while (iter.hasNext()) {

            Liability liability = (Liability) iter.next();
            LiabilityType liabilityType = factory.createLiabilityType();

            populateLiability(liabilityType, liability);

            liabilityTypes.add(liabilityType);
        }
    }

    protected void populateLiability(LiabilityType liabilityType, Liability liability) {

        if (!XMLUtil.isEmpty(liability.getLiabilityTypeId())) {
            liabilityType.setLiabilityTypeId(XMLUtil.int2BigInteger(liability.getLiabilityTypeId()));
        }

        if (!XMLUtil.isEmpty(liability.getLiabilityAmount())) {
            liabilityType.setLiabilityAmount(XMLUtil.double2BigDecimal(liability.getLiabilityAmount()));
        }

        if (!XMLUtil.isEmpty(liability.getLiabilityMonthlyPayment())) {
            liabilityType.setLiabilityMonthlyPayment(XMLUtil.double2BigDecimal(liability.getLiabilityMonthlyPayment()));
        }

        liabilityType.setIncludeInGDS(XMLUtil.boolean2String(liability.getIncludeInGDS()));

        liabilityType.setIncludeInTDS(XMLUtil.boolean2String(liability.getIncludeInTDS()));

        if (!XMLUtil.isEmpty(liability.getLiabilityDescription())) {
            liabilityType.setLiabilityDescription(liability.getLiabilityDescription());
        }

        if (!XMLUtil.isEmpty(liability.getLiabilityPayOffTypeId())) {
            liabilityType.setLiabilityPayOffTypeId(XMLUtil.int2BigInteger(liability.getLiabilityPayOffTypeId()));
        }

        if (!XMLUtil.isEmpty(liability.getPercentInGDS())) {
            liabilityType.setPercentInGDS(XMLUtil.double2BigDecimal(liability.getPercentInGDS()));
        }

        if (!XMLUtil.isEmpty(liability.getPercentInTDS())) {
            liabilityType.setPercentInTDS(XMLUtil.double2BigDecimal(liability.getPercentInTDS()));
        }

        if (!XMLUtil.isEmpty(liability.getLiabilityPaymentQualifier())) {
            liabilityType.setLiabilityPaymentQualifier(liability.getLiabilityPaymentQualifier());
        }

        if (!XMLUtil.isEmpty(liability.getPercentOutGDS())) {
            liabilityType.setPercentOutGDS(XMLUtil.int2BigInteger((int) liability.getPercentOutGDS()));
        }

        if (!XMLUtil.isEmpty(liability.getCreditLimit())) {
            liabilityType.setCreditLimit(XMLUtil.double2BigDecimal(liability.getCreditLimit()));
        }

        if (!XMLUtil.isEmpty(liability.getMaturityDate())) {
            liabilityType.setMaturityDate(XMLUtil.date2XMLGregorianCalendar(liability.getMaturityDate()));
        }
    }

    protected void createAsset(BorrowerType borrowerType, Borrower borrower) throws JAXBException {

        Collection<Asset> assets;
        try {
            assets = borrower.getAssets();
        } catch (Exception e) {
            handleEntityException(e, Asset.class);
            return;
        }

        List<AssetType> assetTypes = borrowerType.getAsset();

        Iterator<Asset> iter = assets.iterator();

        while (iter.hasNext()) {

            AssetType assetType = factory.createAssetType();
            Asset asset = (Asset) iter.next();

            populateAsset(assetType, asset);

            assetTypes.add(assetType);
        }
    }

    protected void populateAsset(AssetType assetType, Asset asset) {

        if (!XMLUtil.isEmpty(asset.getAssetValue())) {
            assetType.setAssetValue(XMLUtil.double2BigDecimal(asset.getAssetValue()));
        }

        if (!XMLUtil.isEmpty(asset.getAssetTypeId())) {
            assetType.setAssetTypeId(XMLUtil.int2BigInteger(asset.getAssetTypeId()));
        }

        assetType.setIncludeInNetWorth(XMLUtil.boolean2String(asset.getIncludeInNetWorth()));

        if (!XMLUtil.isEmpty(asset.getPercentInNetWorth())) {
            assetType.setPercentInNetWorth(XMLUtil.int2BigInteger(asset.getPercentInNetWorth()));
        }
    }

    protected void createIncome(BorrowerType borrowerType, Borrower borrower) throws JAXBException {

        Collection<Income> incomes;
        try {
            incomes = borrower.getIncomes();
        } catch (Exception e) {
            handleEntityException(e, Income.class);
            return;
        }

        List<IncomeType> incomeTypes = borrowerType.getIncome();

        if (incomes == null)
            return;

        for (Iterator<Income> iter = incomes.iterator(); iter.hasNext();) {

            IncomeType incomeType = factory.createIncomeType();
            Income income = iter.next();

            populateIncome(incomeType, income);

            incomeTypes.add(incomeType);
        }

    }

    protected void populateIncome(IncomeType incomeType, Income income) {

        if (!XMLUtil.isEmpty(income.getIncomePeriodId())) {
            incomeType.setIncomePeriodId(XMLUtil.int2BigInteger(income.getIncomePeriodId()));
        }

        if (!XMLUtil.isEmpty(income.getIncomeTypeId())) {
            incomeType.setIncomeTypeId(XMLUtil.int2BigInteger(income.getIncomeTypeId()));
        }

        if (!XMLUtil.isEmpty(income.getIncomeId())) {
            incomeType.setIncomeId(XMLUtil.int2BigInteger(income.getIncomeId()));
        }

        if (!XMLUtil.isEmpty(income.getIncomeDescription())) {
            incomeType.setIncomeDescription(income.getIncomeDescription());
        }

        if (!XMLUtil.isEmpty(income.getIncomeAmount())) {
            incomeType.setIncomeAmount(XMLUtil.double2BigDecimal(income.getIncomeAmount()));
        }

        incomeType.setIncIncludeInGDS(XMLUtil.boolean2String(income.getIncIncludeInGDS()));
        incomeType.setIncIncludeInTDS(XMLUtil.boolean2String(income.getIncIncludeInTDS()));

        if (!XMLUtil.isEmpty(income.getIncPercentInGDS())) {
            incomeType.setIncPercentInGDS(XMLUtil.int2BigInteger(income.getIncPercentInGDS()));
        }

        if (!XMLUtil.isEmpty(income.getIncPercentInTDS())) {
            incomeType.setIncPercentInTDS(XMLUtil.int2BigInteger(income.getIncPercentInTDS()));
        }

        if (!XMLUtil.isEmpty(income.getAnnualIncomeAmount())) {
            incomeType.setAnnualIncomeAmount(XMLUtil.double2BigDecimal(income.getAnnualIncomeAmount()));
        }

        if (!XMLUtil.isEmpty(income.getMonthlyIncomeAmount())) {
            incomeType.setMonthlyIncomeAmount(XMLUtil.double2BigDecimal(income.getMonthlyIncomeAmount()));
        }
    }

    protected void createProperty(DealType dealType, Deal deal) throws JAXBException {

        Collection<Property> properties;
        try {
            properties = deal.getProperties();
        } catch (Exception e) {
            handleEntityException(e, Property.class);
            return;
        }

        List<PropertyType> propertyTypes = dealType.getProperty();

        Iterator<Property> iter = properties.iterator();

        while (iter.hasNext()) {

            PropertyType propertyType = factory.createPropertyType();
            Property property = (Property) iter.next();

            populatePropertyType(propertyType, property);
            createPropertyExpense(propertyType, property);
            propertyTypes.add(propertyType);
        }
    }

    protected void createPropertyExpense(PropertyType propertyType, Property property) throws JAXBException {

        Collection<PropertyExpense> propertyExpenses;
        try {
            propertyExpenses = property.getPropertyExpenses();
        } catch (Exception e) {
            handleEntityException(e, PropertyExpense.class);
            return;
        }

        List<PropertyExpenseType> propertyExpenseTypes = propertyType.getPropertyExpense();

        Iterator<PropertyExpense> iter = propertyExpenses.iterator();

        while (iter.hasNext()) {

            PropertyExpense propertyExpense = iter.next();
            PropertyExpenseType propertyExpenseType = factory.createPropertyExpenseType();

            populatePropertyExpense(propertyExpense, propertyExpenseType);

            propertyExpenseTypes.add(propertyExpenseType);
        }
    }

    protected void populatePropertyExpense(PropertyExpense propertyExpense, PropertyExpenseType propertyExpenseType) {

        if (!XMLUtil.isEmpty(propertyExpense.getPropertyExpensePeriodId())) {
            propertyExpenseType.setPropertyExpensePeriodId(XMLUtil.int2BigInteger(propertyExpense.getPropertyExpensePeriodId()));
        }

        if (!XMLUtil.isEmpty(propertyExpense.getPropertyExpenseTypeId())) {
            propertyExpenseType.setPropertyExpenseTypeId(XMLUtil.int2BigInteger(propertyExpense.getPropertyExpenseTypeId()));
        }

        if (!XMLUtil.isEmpty(propertyExpense.getPropertyExpenseAmount())) {
            propertyExpenseType.setPropertyExpenseAmount(XMLUtil.double2BigDecimal(propertyExpense.getPropertyExpenseAmount()));
        }

        if (!XMLUtil.isEmpty(propertyExpense.getMonthlyExpenseAmount())) {
            propertyExpenseType.setMonthlyExpenseAmount(XMLUtil.double2BigDecimal(propertyExpense.getMonthlyExpenseAmount()));
        }

        propertyExpenseType.setPeIncludeInGDS(XMLUtil.boolean2String(propertyExpense.getPeIncludeInGDS()));

        propertyExpenseType.setPeIncludeInTDS(XMLUtil.boolean2String(propertyExpense.getPeIncludeInTDS()));

        if (!XMLUtil.isEmpty(propertyExpense.getPePercentInGDS())) {
            propertyExpenseType.setPePercentInGDS(XMLUtil.int2BigInteger(propertyExpense.getPePercentInGDS()));
        }

        if (!XMLUtil.isEmpty(propertyExpense.getPePercentInTDS())) {
            propertyExpenseType.setPePercentInTDS(XMLUtil.int2BigInteger(propertyExpense.getPePercentInTDS()));
        }
    }

    protected void populatePropertyType(PropertyType propertyType, Property property) {

        if (!XMLUtil.isEmpty(property.getStreetTypeId())) {
            propertyType.setStreetTypeId(XMLUtil.int2BigInteger(property.getStreetTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyTypeId())) {
            propertyType.setPropertyTypeId(XMLUtil.int2BigInteger(property.getPropertyTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyUsageId())) {
            propertyType.setPropertyUsageId(XMLUtil.int2BigInteger(property.getPropertyUsageId()));
        }

        if (!XMLUtil.isEmpty(property.getDwellingStyleId())) {
            propertyType.setDwellingStyleId(XMLUtil.int2BigInteger(property.getDwellingStyleId()));
        }

        if (!XMLUtil.isEmpty(property.getOccupancyTypeId())) {
            propertyType.setOccupancyTypeId(XMLUtil.int2BigInteger(property.getOccupancyTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyStreetNumber())) {
            propertyType.setPropertyStreetNumber(property.getPropertyStreetNumber());
        }

        if (!XMLUtil.isEmpty(property.getPropertyStreetName())) {
            propertyType.setPropertyStreetName(property.getPropertyStreetName());
        }

        if (!XMLUtil.isEmpty(property.getHeatTypeId())) {
            propertyType.setHeatTypeId(XMLUtil.int2BigInteger(property.getHeatTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyAddressLine2())) {
            propertyType.setPropertyAddressLine2(property.getPropertyAddressLine2());
        }

        if (!XMLUtil.isEmpty(property.getPropertyCity())) {
            propertyType.setPropertyCity(property.getPropertyCity());
        }

        if (!XMLUtil.isEmpty(property.getPropertyPostalFSA())) {
            propertyType.setPropertyPostalFSA(property.getPropertyPostalFSA());
        }

        if (!XMLUtil.isEmpty(property.getPropertyPostalLDU())) {
            propertyType.setPropertyPostalLDU(property.getPropertyPostalLDU());
        }

        if (!XMLUtil.isEmpty(property.getLegalLine1())) {
            propertyType.setLegalLine1(property.getLegalLine1());
        }

        if (!XMLUtil.isEmpty(property.getLegalLine2())) {
            propertyType.setLegalLine2(property.getLegalLine2());
        }

        if (!XMLUtil.isEmpty(property.getLegalLine3())) {
            propertyType.setLegalLine3(property.getLegalLine3());
        }

        if (!XMLUtil.isEmpty(property.getNumberOfBedrooms())) {
            propertyType.setNumberOfBedrooms(XMLUtil.int2BigInteger(property.getNumberOfBedrooms()));
        }

        if (!XMLUtil.isEmpty(property.getYearBuilt())) {
            propertyType.setYearBuilt(XMLUtil.int2BigInteger(property.getYearBuilt()));
        }

        propertyType.setInsulatedWithUFFI(XMLUtil.boolean2String(property.getInsulatedWithUFFI()));

        if (!XMLUtil.isEmpty(property.getProvinceId())) {
            propertyType.setProvinceId(XMLUtil.int2BigInteger(property.getProvinceId()));
        }

        if (!XMLUtil.isEmpty(property.getMonthlyCondoFees())) {
            propertyType.setMonthlyCondoFees(XMLUtil.double2BigDecimal(property.getMonthlyCondoFees()));
        }

        if (!XMLUtil.isEmpty(property.getPurchasePrice())) {
            propertyType.setPurchasePrice(XMLUtil.double2BigDecimal(property.getPurchasePrice()));
        }

        if (!XMLUtil.isEmpty(property.getMunicipalityId())) {
            propertyType.setMunicipalityId(XMLUtil.int2BigInteger(property.getMunicipalityId()));
        }

        if (!XMLUtil.isEmpty(property.getLandValue())) {
            propertyType.setLandValue(XMLUtil.double2BigDecimal(property.getLandValue()));
        }

        if (!XMLUtil.isEmpty(property.getAppraisalSourceId())) {
            propertyType.setAppraisalSourceId(XMLUtil.int2BigInteger(property.getAppraisalSourceId()));
        }

        if (!XMLUtil.isEmpty(property.getEquityAvailable())) {
            propertyType.setEquityAvailable(XMLUtil.double2BigDecimal(property.getEquityAvailable()));
        }

        if (!XMLUtil.isEmpty(property.getNewConstructionId())) {
            propertyType.setNewConstructionId(XMLUtil.int2BigInteger(property.getNewConstructionId()));
        }

        if (!XMLUtil.isEmpty(property.getEstimatedAppraisalValue())) {
            propertyType.setEstimatedAppraisalValue(XMLUtil.double2BigDecimal(property.getEstimatedAppraisalValue()));
        }

        if (!XMLUtil.isEmpty(property.getActualAppraisalValue())) {
            propertyType.setActualAppraisalValue(XMLUtil.double2BigDecimal(property.getActualAppraisalValue()));
        }

        if (!XMLUtil.isEmpty(property.getLoanToValue())) {
            propertyType.setLoanToValue(XMLUtil.double2BigDecimal(property.getLoanToValue()));
        }

        if (!XMLUtil.isEmpty(property.getUnitNumber())) {
            propertyType.setUnitNumber(property.getUnitNumber());
        }

        if (!XMLUtil.isEmpty(property.getLotSize())) {
            propertyType.setLotSize(XMLUtil.int2BigInteger(property.getLotSize()));
        }

        if (!XMLUtil.isEmpty(property.getPrimaryPropertyFlag())) {
            propertyType.setPrimaryPropertyFlag(property.getPrimaryPropertyFlag() + "");
        }

        if (!XMLUtil.isEmpty(property.getAppraisalDateAct())) {
            propertyType.setAppraisalDateAct(XMLUtil.date2XMLGregorianCalendar(property.getAppraisalDateAct()));
        }

        if (!XMLUtil.isEmpty(property.getLotSizeDepth())) {
            propertyType.setLotSizeDepth(XMLUtil.int2BigInteger(property.getLotSizeDepth()));
        }

        if (!XMLUtil.isEmpty(property.getLotSizeFrontage())) {
            propertyType.setLotSizeFrontage(XMLUtil.int2BigInteger(property.getLotSizeFrontage()));
        }

        if (!XMLUtil.isEmpty(property.getLotSizeUnitOfMeasureId())) {
            propertyType.setLotSizeUnitOfMeasureId(XMLUtil.int2BigInteger(property.getLotSizeUnitOfMeasureId()));
        }

        if (!XMLUtil.isEmpty(property.getNumberOfUnits())) {
            propertyType.setNumberOfUnits(XMLUtil.int2BigInteger(property.getNumberOfUnits()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyLocationId())) {
            propertyType.setPropertyLocationId(XMLUtil.int2BigInteger(property.getPropertyLocationId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyOccurenceNumber())) {
            propertyType.setPropertyOccurenceNumber(XMLUtil.int2BigInteger(property.getPropertyOccurenceNumber()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyReferenceNumber())) {
            propertyType.setPropertyReferenceNumber(property.getPropertyReferenceNumber());
        }

        if (!XMLUtil.isEmpty(property.getSewageTypeId())) {
            propertyType.setSewageTypeId(XMLUtil.int2BigInteger(property.getSewageTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getStreetDirectionId())) {
            propertyType.setStreetDirectionId(XMLUtil.int2BigInteger(property.getStreetDirectionId()));
        }

        if (!XMLUtil.isEmpty(property.getWaterTypeId())) {
            propertyType.setWaterTypeId(XMLUtil.int2BigInteger(property.getWaterTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getLivingSpace())) {
            propertyType.setLivingSpace(XMLUtil.int2BigInteger(property.getLivingSpace()));
        }

        if (!XMLUtil.isEmpty(property.getLivingSpaceUnitOfMeasureId())) {
            propertyType.setLivingSpaceUnitOfMeasureId(XMLUtil.int2BigInteger(property.getLivingSpaceUnitOfMeasureId()));
        }

        if (!XMLUtil.isEmpty(property.getStructureAge())) {
            propertyType.setStructureAge(XMLUtil.int2BigInteger(property.getStructureAge()));
        }

        if (!XMLUtil.isEmpty(property.getDwellingTypeId())) {
            propertyType.setDwellingTypeId(XMLUtil.int2BigInteger(property.getDwellingTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getLendingValue())) {
            propertyType.setLendingValue(XMLUtil.double2BigDecimal(property.getLendingValue()));
        }

        if (!XMLUtil.isEmpty(property.getTotalPropertyExpenses())) {
            propertyType.setTotalPropertyExpenses(XMLUtil.double2BigDecimal(property.getTotalPropertyExpenses()));
        }

        if (!XMLUtil.isEmpty(property.getTotalAnnualTaxAmount())) {
            propertyType.setTotalAnnualTaxAmount(XMLUtil.double2BigDecimal(property.getTotalAnnualTaxAmount()));
        }

        if (!XMLUtil.isEmpty(property.getZoning())) {
            propertyType.setZoning(XMLUtil.int2BigInteger(property.getZoning()));
        }

        if (!XMLUtil.isEmpty(property.getMLSListingFlag())) {
            propertyType.setMLSListingFlag(property.getMLSListingFlag());
        }

        if (!XMLUtil.isEmpty(property.getGarageSizeId())) {
            propertyType.setGarageSizeId(XMLUtil.int2BigInteger(property.getGarageSizeId()));
        }

        if (!XMLUtil.isEmpty(property.getGarageTypeId())) {
            propertyType.setGarageTypeId(XMLUtil.int2BigInteger(property.getGarageTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getBuilderName())) {
            propertyType.setBuilderName(property.getBuilderName());
        }

        if (!XMLUtil.isEmpty(property.getTenureTypeId())) {
            propertyType.setTenureTypeId(XMLUtil.int2BigInteger(property.getTenureTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getMiEnergyEfficiency())) {
            propertyType.setMIEnergyEfficiency(property.getMiEnergyEfficiency());
        }

        if (!XMLUtil.isEmpty(property.getOnReserveTrustAgreementNumber())) {
            propertyType.setOnReserveTrustAgreementNumber(property.getOnReserveTrustAgreementNumber());
        }

    }

    protected void populateDealType(DealType dealType, Deal deal) {

        dealType.setDealId(XMLUtil.int2BigInteger(deal.getDealId()));

        if (!XMLUtil.isEmpty(deal.getApplicationDate())) {
            dealType.setApplicationDate(XMLUtil.date2XMLGregorianCalendar(deal.getApplicationDate()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalLoanAmount())) {
            dealType.setTotalLoanAmount(XMLUtil.double2BigDecimal(deal.getTotalLoanAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getAmortizationTerm())) {
            dealType.setAmortizationTerm(XMLUtil.int2BigInteger(deal.getAmortizationTerm()));
        }

        if (!XMLUtil.isEmpty(deal.getEstimatedClosingDate())) {
            dealType.setEstimatedClosingDate(XMLUtil.date2XMLGregorianCalendar(deal.getEstimatedClosingDate()));
        }

        if (!XMLUtil.isEmpty(deal.getDealTypeId())) {
            dealType.setDealTypeId(XMLUtil.int2BigInteger(deal.getDealTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalPurchasePrice())) {
            dealType.setTotalPurchasePrice(XMLUtil.double2BigDecimal(deal.getTotalPurchasePrice()));
        }

        if (!XMLUtil.isEmpty(deal.getPandiPaymentAmount())) {
            dealType.setPandiPaymentAmount(XMLUtil.double2BigDecimal(deal.getPandiPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getEscrowPaymentAmount())) {
            dealType.setEscrowPaymentAmount(XMLUtil.double2BigDecimal(deal.getEscrowPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getNetLoanAmount())) {
            dealType.setNetLoanAmount(XMLUtil.double2BigDecimal(deal.getNetLoanAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getSecondaryFinancing())) {
            dealType.setSecondaryFinancing(deal.getSecondaryFinancing());
        }

        if (!XMLUtil.isEmpty(deal.getNetInterestRate())) {
            dealType.setNetInterestRate(XMLUtil.double2BigDecimal(deal.getNetInterestRate()));
        }

        if (!XMLUtil.isEmpty(deal.getDealPurposeId())) {
            dealType.setDealPurposeId(XMLUtil.int2BigInteger(deal.getDealPurposeId()));
        }

        if (!XMLUtil.isEmpty(deal.getBuydownRate())) {
            dealType.setBuydownRate(XMLUtil.double2BigDecimal(deal.getBuydownRate()));
        }

        if (!XMLUtil.isEmpty(deal.getSpecialFeatureId())) {
            dealType.setSpecialFeatureId(XMLUtil.int2BigInteger(deal.getSpecialFeatureId()));
        }

        if (!XMLUtil.isEmpty(deal.getDownPaymentAmount())) {
            dealType.setDownPaymentAmount(XMLUtil.double2BigDecimal(deal.getDownPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getLineOfBusinessId())) {
            dealType.setLineOfBusinessId(XMLUtil.int2BigInteger(deal.getLineOfBusinessId()));
        }

        if (!XMLUtil.isEmpty(deal.getMortgageInsurerId())) {
            dealType.setMortgageInsurerId(XMLUtil.int2BigInteger(deal.getMortgageInsurerId()));
        }

        if (!XMLUtil.isEmpty(deal.getMtgProdId())) {
            dealType.setMtgProdId(XMLUtil.int2BigInteger(deal.getMtgProdId()));
        }

        if (!XMLUtil.isEmpty(deal.getFirstPaymentDate())) {
            dealType.setFirstPaymentDate(XMLUtil.date2XMLGregorianCalendar(deal.getFirstPaymentDate()));
        }

        if (!XMLUtil.isEmpty(deal.getNetPaymentAmount())) {
            dealType.setNetPaymentAmount(XMLUtil.double2BigDecimal(deal.getNetPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getMaturityDate())) {
            dealType.setMaturityDate(XMLUtil.date2XMLGregorianCalendar(deal.getMaturityDate()));
        }

        if (!XMLUtil.isEmpty(deal.getRefExistingMtgNumber())) {
            dealType.setRefExistingMtgNumber(deal.getRefExistingMtgNumber());
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalLiabilities())) {
            dealType.setCombinedTotalLiabilities(XMLUtil.double2BigDecimal(deal.getCombinedTotalLiabilities()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalAssets())) {
            dealType.setCombinedTotalAssets(XMLUtil.double2BigDecimal(deal.getCombinedTotalAssets()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalIncome())) {
            dealType.setCombinedTotalIncome(XMLUtil.double2BigDecimal(deal.getCombinedTotalIncome()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTDS())) {
            dealType.setCombinedTDS(XMLUtil.double2BigDecimal(deal.getCombinedTDS()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedGDS())) {
            dealType.setCombinedGDS(XMLUtil.double2BigDecimal(deal.getCombinedGDS()));
        }

        if (!XMLUtil.isEmpty(deal.getNumberOfBorrowers())) {
            dealType.setNumberOfBorrowers(XMLUtil.int2BigInteger(deal.getNumberOfBorrowers()));
        }

        if (!XMLUtil.isEmpty(deal.getNumberOfGuarantors())) {
            dealType.setNumberOfGuarantors(XMLUtil.int2BigInteger(deal.getNumberOfGuarantors()));
        }

        if (!XMLUtil.isEmpty(deal.getPaymentTermId())) {
            dealType.setPaymentTermId(XMLUtil.int2BigInteger(deal.getPaymentTermId()));
        }

        if (!XMLUtil.isEmpty(deal.getInterestTypeId())) {
            dealType.setInterestTypeId(XMLUtil.int2BigInteger(deal.getInterestTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getPaymentFrequencyId())) {
            dealType.setPaymentFrequencyId(XMLUtil.int2BigInteger(deal.getPaymentFrequencyId()));
        }

        if (!XMLUtil.isEmpty(deal.getInterestCompoundId())) {
            dealType.setInterestCompoundId(XMLUtil.int2BigInteger(deal.getInterestCompoundId()));
        }

        if (!XMLUtil.isEmpty(deal.getRefinanceNewMoney())) {
            dealType.setRefinanceNewMoney(XMLUtil.double2BigDecimal(deal.getRefinanceNewMoney()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedLTV())) {
            dealType.setCombinedLTV(XMLUtil.double2BigDecimal(deal.getCombinedLTV()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalAnnualHeatingExp())) {
            dealType.setCombinedTotalAnnualHeatingExp(XMLUtil.double2BigDecimal(deal.getCombinedTotalAnnualHeatingExp()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalAnnualTaxExp())) {
            dealType.setCombinedTotalAnnualTaxExp(XMLUtil.double2BigDecimal(deal.getCombinedTotalAnnualTaxExp()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalCondoFeeExp())) {
            dealType.setCombinedTotalCondoFeeExp(XMLUtil.double2BigDecimal(deal.getCombinedTotalCondoFeeExp()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalPropertyExp())) {
            dealType.setCombinedTotalPropertyExp(XMLUtil.double2BigDecimal(deal.getCombinedTotalPropertyExp()));
        }

        if (!XMLUtil.isEmpty(deal.getLienPositionId())) {
            dealType.setLienPositionId(XMLUtil.int2BigInteger(deal.getLienPositionId()));
        }

        if (!XMLUtil.isEmpty(deal.getMITypeId())) {
            dealType.setMortgageInsuranceTypeId(XMLUtil.int2BigInteger(deal.getMITypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getNewMoney())) {
            dealType.setNewMoney(XMLUtil.double2BigDecimal(deal.getNewMoney()));
        }

        if (!XMLUtil.isEmpty(deal.getNumberOfProperties())) {
            dealType.setNumberOfProperties(XMLUtil.int2BigInteger(deal.getNumberOfProperties()));
        }

        if (!XMLUtil.isEmpty(deal.getOldMoney())) {
            dealType.setOldMoney(XMLUtil.double2BigDecimal(deal.getOldMoney()));
        }

        if (!XMLUtil.isEmpty(deal.getPreApproval())) {
            dealType.setPreApproval(deal.getPreApproval());
        }

        if (!XMLUtil.isEmpty(deal.getReferenceDealNumber())) {
            dealType.setReferenceDealNumber(deal.getReferenceDealNumber());
        }

        if (!XMLUtil.isEmpty(deal.getReferenceDealTypeId())) {
            dealType.setReferenceDealTypeId(XMLUtil.int2BigInteger(deal.getReferenceDealTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getRemainingBalanceSecondaryFin())) {
            dealType.setRemainingBalanceSecondaryFin(XMLUtil.double2BigDecimal(deal.getRemainingBalanceSecondaryFin()));
        }

        if (!XMLUtil.isEmpty(deal.getTaxPayorId())) {
            dealType.setTaxPayorId(XMLUtil.int2BigInteger(deal.getTaxPayorId()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalActAppraisedValue())) {
            dealType.setTotalActAppraisedValue(XMLUtil.double2BigDecimal(deal.getTotalActAppraisedValue()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalEstAppraisedValue())) {
            dealType.setTotalEstAppraisedValue(XMLUtil.double2BigDecimal(deal.getTotalEstAppraisedValue()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalPropertyExpenses())) {
            dealType.setTotalPropertyExpenses(XMLUtil.double2BigDecimal(deal.getTotalPropertyExpenses()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalPaymentAmount())) {
            dealType.setTotalPaymentAmount(XMLUtil.double2BigDecimal(deal.getTotalPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getActualPaymentTerm())) {
            dealType.setActualPaymentTerm(XMLUtil.int2BigInteger(deal.getActualPaymentTerm()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedGDSBorrower())) {
            dealType.setCombinedGDSBorrower(XMLUtil.double2BigDecimal(deal.getCombinedGDSBorrower()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTDSBorrower())) {
            dealType.setCombinedTDSBorrower(XMLUtil.double2BigDecimal(deal.getCombinedTDSBorrower()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedGDS3Year())) {
            dealType.setCombinedGDS3Year(XMLUtil.double2BigDecimal(deal.getCombinedGDS3Year()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTDS3Year())) {
            dealType.setCombinedTDS3Year(XMLUtil.double2BigDecimal(deal.getCombinedTDS3Year()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalEquityAvailable())) {
            dealType.setCombinedTotalEquityAvailable(XMLUtil.double2BigDecimal(deal.getCombinedTotalEquityAvailable()));
        }

        if (!XMLUtil.isEmpty(deal.getEffectiveAmortizationMonths())) {
            dealType.setEffectiveAmortizationMonths(XMLUtil.int2BigInteger(deal.getEffectiveAmortizationMonths()));
        }

        if (!XMLUtil.isEmpty(deal.getMIUpfront())) {
            dealType.setMIUpfront(deal.getMIUpfront());
        }

        if (!XMLUtil.isEmpty(deal.getRequiredDownpayment())) {
            dealType.setRequiredDownpayment(XMLUtil.double2BigDecimal(deal.getRequiredDownpayment()));
        }

        if (!XMLUtil.isEmpty(deal.getMIIndicatorId())) {
            dealType.setMIIndicatorId(XMLUtil.int2BigInteger(deal.getMIIndicatorId()));
        }

        if (!XMLUtil.isEmpty(deal.getRepaymentTypeId())) {
            dealType.setRepaymentTypeId(XMLUtil.int2BigInteger(deal.getRepaymentTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getMIComments())) {
            dealType.setMiComments(deal.getMIComments());
        }

        if (!XMLUtil.isEmpty(deal.getSystemTypeId())) {
            dealType.setSystemTypeId(XMLUtil.int2BigInteger(deal.getSystemTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedLendingValue())) {
            dealType.setCombinedLendingValue(XMLUtil.double2BigDecimal(deal.getCombinedLendingValue()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalLoanBridgeAmount())) {
            dealType.setTotalLoanBridgeAmount(XMLUtil.double2BigDecimal(deal.getTotalLoanBridgeAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getExistingLoanAmount())) {
            dealType.setExistingLoanAmount(XMLUtil.double2BigDecimal(deal.getExistingLoanAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getGDSTDS3YearRate())) {
            dealType.setGDSTDS3YearRate(XMLUtil.double2BigDecimal(deal.getGDSTDS3YearRate()));
        }

        if (!XMLUtil.isEmpty(deal.getMIExistingPolicyNumber())) {
            dealType.setMIExistingPolicyNumber(deal.getMIExistingPolicyNumber());
        }

        if (!XMLUtil.isEmpty(deal.getPandIPaymentAmountMonthly())) {
            dealType.setPandIPaymentAmountMonthly(XMLUtil.double2BigDecimal(deal.getPandIPaymentAmountMonthly()));
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvance())) {
            dealType.setProgressAdvance(deal.getProgressAdvance());
        }

        if (!XMLUtil.isEmpty(deal.getAdvanceToDateAmount())) {
            dealType.setAdvanceToDateAmount(XMLUtil.double2BigDecimal(deal.getAdvanceToDateAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getAdvanceNumber())) {
            dealType.setAdvanceNumber(XMLUtil.int2BigInteger(deal.getAdvanceNumber()));
        }

        if (!XMLUtil.isEmpty(deal.getMIRUIntervention())) {
            dealType.setMIRUIntervention(deal.getMIRUIntervention());
        }

        if (!XMLUtil.isEmpty(deal.getPreQualificationMICertNum())) {
            dealType.setPreQualificationMICertNum(deal.getPreQualificationMICertNum());
        }

        if (!XMLUtil.isEmpty(deal.getRefiOrigPurchasePrice())) {
            dealType.setRefiOrigPurchasePrice(XMLUtil.double2BigDecimal(deal.getRefiOrigPurchasePrice()));
        }

        if (!XMLUtil.isEmpty(deal.getRefiBlendedAmortization())) {
            dealType.setRefiBlendedAmortization(deal.getRefiBlendedAmortization());
        }

        if (!XMLUtil.isEmpty(deal.getRefiOrigMtgAmount())) {
            dealType.setRefiOrigMtgAmount(XMLUtil.double2BigDecimal(deal.getRefiOrigMtgAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getRefiImprovementAmount())) {
            dealType.setRefiImprovementAmount(XMLUtil.double2BigDecimal(deal.getRefiImprovementAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getNextAdvanceAmount())) {
            dealType.setNextAdvanceAmount(XMLUtil.double2BigDecimal(deal.getNextAdvanceAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getInterestRateIncrement())) {
            dealType.setInterestRateIncrement(XMLUtil.double2BigDecimal(deal.getInterestRateIncrement()));
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvanceTypeId())) {
            dealType.setProgressAdvanceTypeId(XMLUtil.int2BigInteger(deal.getProgressAdvanceTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getIncomeVerificationTypeId())) {
            dealType.setIncomeVerificationTypeId(XMLUtil.int2BigInteger(deal.getIncomeVerificationTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getProductTypeId())) {
            dealType.setProductTypeId(XMLUtil.int2BigInteger(deal.getProductTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvanceInspectionBy())) {
        	if("Y".equalsIgnoreCase(deal.getProgressAdvance())){
        		dealType.setProgressAdvanceInspectionBy(deal.getProgressAdvanceInspectionBy());
        	}
        }

        if (!XMLUtil.isEmpty(deal.getSelfDirectedRRSP())) {
            dealType.setSelfDirectedRRSP(deal.getSelfDirectedRRSP());
        }

        if (!XMLUtil.isEmpty(deal.getLocRepaymentTypeId())) {
            dealType.setLOCRepaymentTypeId(XMLUtil.int2BigInteger(deal.getLocRepaymentTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getRequestStandardService())) {
            dealType.setRequestStandardService(deal.getRequestStandardService());
        }

        if (!XMLUtil.isEmpty(deal.getLocAmortizationMonths())) {
            dealType.setLOCAmortizationMonths(XMLUtil.int2BigInteger(deal.getLocAmortizationMonths()));
        }

        if (!XMLUtil.isEmpty(deal.getLocInterestOnlyMaturityDate())) {
            dealType.setLOCInterestOnlyMaturityDate(XMLUtil.date2XMLGregorianCalendar(deal.getLocInterestOnlyMaturityDate()));
        }

        if (!XMLUtil.isEmpty(deal.getRefiProductTypeId())) {
            dealType.setRefiProductTypeId(XMLUtil.int2BigInteger(deal.getRefiProductTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getPandIUsing3YearRate())) {
            dealType.setPandIUsing3YearRate(XMLUtil.double2BigDecimal(deal.getPandIUsing3YearRate()));
        }

        if (!XMLUtil.isEmpty(deal.getServicingMortgageNumber())) {
            dealType.setServicingMortgageNumber(deal.getServicingMortgageNumber());
        }

        if (!XMLUtil.isEmpty(deal.getRefiOrigPurchaseDate())) {
            dealType.setRefiOrigPurchaseDate(XMLUtil.date2XMLGregorianCalendar(deal.getRefiOrigPurchaseDate()));
        }

        if (!XMLUtil.isEmpty(deal.getMIDealRequestNumber())) {
            dealType.setMIDealRequestNumber(BigInteger.valueOf((deal.getMIDealRequestNumber())));
        }

    }

    public void setFormatted_output(boolean formatted_output) {
        this.formatted_output = formatted_output;
    }

    public void setStopOnSQLError(boolean stopOnSQLError) {
        this.stopOnSQLError = stopOnSQLError;
    }

    public void setMandatoryTables(List<String> mandatoryTables) {
        this.mandatoryTables = mandatoryTables;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

}
