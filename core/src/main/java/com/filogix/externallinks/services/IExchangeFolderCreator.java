package com.filogix.externallinks.services;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.entity.Deal;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ExternalServicesException;

public interface IExchangeFolderCreator {

    // ============== constants  ==============
    public static String DC_FOLDER_ID_KEY_NAME = "FOLDER_ID";
    public static String DC_STATUS_FAILED = "FAILED";
    public static String DC_STATUS_COMPLETED = "COMPLETED";

    // ============== constants  ==============

    public void init(SessionResourceKit srk, SysLogger logger, Deal deal, JdbcExecutor jExec);
    public String createFolder(Deal deal) 
		throws ExternalServicesException, DocPrepException, JdbcTransactionException;
	
}
