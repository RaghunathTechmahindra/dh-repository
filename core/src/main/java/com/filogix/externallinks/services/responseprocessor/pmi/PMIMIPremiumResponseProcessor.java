package com.filogix.externallinks.services.responseprocessor.pmi;

import com.filogix.externallinks.services.responseprocessor.IResponseProcessor;
import com.filogix.externallinks.services.responseprocessor.MIPremiumResponseProcessor;

public class PMIMIPremiumResponseProcessor
    extends MIPremiumResponseProcessor
{
    static private IResponseProcessor instance = new PMIMIPremiumResponseProcessor();

    public PMIMIPremiumResponseProcessor()
    {
    }

    public static IResponseProcessor getInstance()
    {
        return instance;
    }
}
