/**
 * GetCreditDecision_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package com.filogix.externallinks.services.channel.bnc;

public interface GetCreditDecision_Service extends javax.xml.rpc.Service {
    public java.lang.String getGetCreditDecisionAddress();

    public com.filogix.externallinks.services.channel.bnc.GetCreditDecision_PortType getGetCreditDecision() throws javax.xml.rpc.ServiceException;

    public com.filogix.externallinks.services.channel.bnc.GetCreditDecision_PortType getGetCreditDecision(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
