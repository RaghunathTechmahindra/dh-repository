package com.filogix.externallinks.services;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.pk.DealPK;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.basis100.resources.SessionResourceWrapper;
import com.filogix.express.core.ExpressCoreContext;
import com.filogix.externallinks.adjudication.BncGcdServiceInfo;
import com.filogix.externallinks.services.channel.IChannel;
import com.filogix.externallinks.services.payload.IPayloadGenerator;
import com.filogix.spring.SpringServiceFactory;

public class ServiceDelegate {
	
	private static SysLogger logger = ResourceManager.getSysLogger("ServiceDelegate");
	
	public static final String MI_REQUEST_TYPE = "mi";
	
	/**
	 * the value of this fields, should be used in the 
	 * spring context .xml files as id for RequestService beans.
	 */
	public static final String MI_REQUEST_TYPE_AIGUG = "mi_aigug";
	public static final String MI_REQUEST_TYPE_PMI = "mi_pmi";
	public static final String PREMIUM_REQUEST_TYPE_AIGUG = "premium_aigug";
	public static final String PREMIUM_REQUEST_TYPE_PMI = "premium_pmi";
	
	private final int MI_INSURER_ID_AIGUG = 3;
	private final int MI_INSURER_ID_PMI = 4;

    public void sendMIPremiumRequest(ServiceInfo info) throws Exception {
    	int productId = info.getProductType().intValue();
    	if(MI_INSURER_ID_AIGUG == productId)
			info.setRequestType(PREMIUM_REQUEST_TYPE_AIGUG);
		else
			if(MI_INSURER_ID_PMI == productId)
				info.setRequestType(PREMIUM_REQUEST_TYPE_PMI);

		//ToDo: Invoke SE.
		invoke(info);
	}
	

	public void sendMIRequest(ServiceInfo info) throws Exception {
		int productId = info.getProductType().intValue();
		if(MI_INSURER_ID_AIGUG == productId)
			info.setRequestType(MI_REQUEST_TYPE_AIGUG);
		else
			if(MI_INSURER_ID_PMI == productId)
				info.setRequestType(MI_REQUEST_TYPE_PMI);

		//ToDo: Invoke SE.
		invoke(info);
	}
	
	public void sendGCDRequest(BncGcdServiceInfo serviceInfo) throws Exception
    {
        SessionResourceKit srk = serviceInfo.getSrk();
        SysLogger logger = srk.getSysLogger();
        String logString = "ServiceDelegate.sendGCDRequest() - ";
        logger.debug(logString + "Entering method.");
        if(serviceInfo.getRequest() == null)
        {
        	logger.debug(logString 
                       + "Request does not exist in the ServiceInfo, instantiate a new Request for request ID " 
                       + serviceInfo.getRequestId());
            Request request = new Request(srk);
            request.findLastRequestByRequestId(serviceInfo.getRequestId());
            serviceInfo.setRequest(request);
        }
    
        if(serviceInfo.getDeal() == null)
        {
        	logger.debug(logString 
                       + "Deal does not exist in the ServiceInfo, instantiate a new Deal for deal ID " 
                       + serviceInfo.getDealId().intValue() + " and copy ID " 
                       + serviceInfo.getCopyId().intValue());
            Deal aDeal = new Deal(srk, null);
            DealPK dealpk = new DealPK(serviceInfo.getDealId().intValue(),
                                       serviceInfo.getCopyId().intValue());
            aDeal.findByPrimaryKey(dealpk);  
            serviceInfo.setDeal(aDeal);
        }
    
        invoke(serviceInfo);
    }
	
	public void sendMIPremiumExtendedRequest(ServiceInfo info) throws Exception {
		
		int productId = info.getProductType().intValue();
    	if(MI_INSURER_ID_PMI == productId)
			info.setRequestType(PREMIUM_REQUEST_TYPE_PMI + "_extended");

		invoke(info);
		
	}
      

    /**
     * The method performs the task according to the requestType of ServiceInfo
	 * It's possible to wrap any existing ServiceExecutors inside the
	 * RequestService, and make them work the same as the following.
     *
	 * @param info
	 *            ServiceInfo
     * @throws Exception
     */
	private IRequestService rh = null;
    private void invoke(ServiceInfo info) throws Exception {
		rh = (IRequestService) ExpressCoreContext.getService(info.getRequestType());
        rh.service(info);
    }

}
