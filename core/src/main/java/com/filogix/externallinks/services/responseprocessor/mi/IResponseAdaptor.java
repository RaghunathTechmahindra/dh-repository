package com.filogix.externallinks.services.responseprocessor.mi;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.datatype.XMLGregorianCalendar;

import com.filogix.externallinks.framework.ExternalServicesException;

public interface IResponseAdaptor {

    /**
     * Initialise contents
     * 
     * @param payloadXml
     * @throws ExternalServicesException
     */
    public void initialize(String payloadXml) throws ExternalServicesException;

    /**
     * checks if one of the given String is contained in Message elements
     * 
     * @param arrayCodes
     * @return
     */
    public boolean messageContainsAny(String[] arrayCodes);

    /**
     * checks if given String is contained in Message elements
     * 
     * @param code
     * @return
     */
    public boolean messageContains(String code);

    /**
     * looks up message element with the code and parse the value to double
     * 
     * @param code
     * @return
     */
    public double getFeeAmountFor(String code);

    /* from here, just delegate methods */

    public String getMIProviderBranchTransitNumber();

    public BigInteger getDealId();

    public String getMIProviderInstitutionCode();

    public BigInteger getLOCAmortizationMonths();

    public XMLGregorianCalendar getLOCInterestOnlyMaturityDate();

    public BigInteger getLanguagePreferenceId();

    public BigInteger getMIDealRequestNumber();

    public BigDecimal getMIFeeAmount();

    public BigDecimal getMIPremium();

    public BigDecimal getMIPremiumPst();

    public BigDecimal getMIPremiumRate();

    public String getMIProviderReferenceNumber();

    public String getMIResponse();

    public BigInteger getMortgageInsurerId();

    public String getRequestedInsuranceProduct();

    public String getStatusCode();

    public String getStatusMessage();
}
