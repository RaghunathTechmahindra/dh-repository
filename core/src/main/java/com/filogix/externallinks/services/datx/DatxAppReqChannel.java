/**
 * <p>Title: DatxAppReqChannel.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 17, 2006)
 *
 */

package com.filogix.externallinks.services.datx;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.rpc.ServiceException;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.WSServiceChannel;
import com.filogix.externallinks.framework.JaxbServiceResponse;
import com.filogix.externallinks.framework.ServicePayloadData;
import com.filogix.externallinks.framework.ServiceResponseBase;
import com.filogix.externallinks.services.wsdl.appraisal.AppraisalService_PortType;
import com.filogix.externallinks.services.wsdl.appraisal.AppraisalService_ServiceLocator;
import com.filogix.externallinks.services.wsdl.appraisal.DatXRequest;
import com.filogix.externallinks.services.wsdl.appraisal.DatXResponse;
import com.filogix.schema.datx.appraisal.AppraisalRequestType;

public class DatxAppReqChannel extends WSServiceChannel
{
    // they should be in DB. Channel Table need to updated
    public static JAXBContext _jaxbContext;
    static
    {
        try
        {
            _jaxbContext = JAXBContext.newInstance(AppraisalRequestType.class.getPackage().getName());
        }
        catch(JAXBException e)
        {
            
        }
    }

    // these calsses are commonly used but package will be different so that 
    // they cannot be in the super class and initRequestBean() method
    protected DatXRequest     _datXRequest;
    protected DatXResponse    _datXresponse;
    
    protected void initRequestBean()
    throws RemoteException,
        FinderException,
        ExternalServicesException
    {
        _datXRequest = new DatXRequest();
        
        _datXRequest.setClientKey(""+_request.getRequestId());
        _datXRequest.setUsername(_channel.getUserId());
        _datXRequest.setPassword(_channel.getPassword());
        _datXRequest.setDatXKey(""+_request.getChannelTransactionKey());        
        _datXRequest.setEncoding("");
        _datXRequest.setSchemaName(_payloadSchemaName);
        _datXRequest.setAuxData(null);
    //    int timeout = _request.findDatxTimeout(_request.getServiceProductId(), _request.getChannelId());
    //    _logger.debug("Timeout = " + timeout);
    //    _datXRequest.setTimeout(new Integer(timeout));    
    }
        
    protected String getAuxData()
    {
        return null;
    }
    
    public ServiceResponseBase sendData(ServicePayloadData data)
        throws Exception
    {
        _data=data;
        try
        {
            prepare(_data);

            AppraisalService_ServiceLocator servLoc = getServiceLocater();
            if (servLoc==null)
            {
                String msg = "DatXAppReqChannel: DatX service is not available!";
                _logger.error(msg);
                throw new DatxException(msg);
            }
            AppraisalService_PortType stub = getServiceStub(servLoc);
            if (stub==null)
            {
                String msg = "DatXAppReqChannel: DatX service is not available!";
                _logger.error(msg);
                throw new DatxException(msg);
            }
            _logger.debug("Sending Appraisal request --->>> ");
            _logger.debug("Datx Request Client Key: " + _datXRequest.getClientKey());
            _logger.debug("Datx Request Datx Key: " + _datXRequest.getDatXKey());
            _logger.debug("Datx Request Password: " + _datXRequest.getPassword());
            _logger.debug("Datx Request SchemaName: " + _datXRequest.getSchemaName());
            _logger.debug("Datx Request UserName: " + _datXRequest.getUsername());
            _logger.debug("Datx Request TimeOut: " + _datXRequest.getTimeout());
            _logger.debug("Datx Request Aux Data: " + _datXRequest.getAuxData());
            _logger.debug("xmlPayload --->>");
            _logger.debug(_xmlPayload);
            System.out.println(_xmlPayload);
            
            DatXResponse response = stub.createAppraisalRequest(_datXRequest, _xmlPayload);
            if (response != null)
            {
                _logger.debug("Receiving Appraisal Request Acknowledgment response --->>> ");
                _logger.debug("Datx Response Result Code: " + response.getResultCode());
                _logger.debug("Datx Response Result Desc: " + response.getResultDescription());
                _logger.debug("Datx Response External Ref Number: " + response.getExternalKey());
                // this is only acknowdlegement
                JaxbServiceResponse result = new JaxbServiceResponse(_srk, _requestId, _copyId);
                result.setResponseBean(response);
                return result;
            }
            else
            {
                _logger.error(this.getClass(), "DatXResponse is null for request id " + _datXRequest.getClientKey());
                throw new DatxException("DatXResponse is null for request id " + _datXRequest.getClientKey());
            }
        } 
        catch (Exception e)
        {
          String msg = "Error when sending service request to DatX: RequestId = " + this._requestId;
          _logger.error(msg + ": "+ StringUtil.stack2string(e));
          throw new DatxException(msg + ": " + e);
        }
    }
    

    public AppraisalService_ServiceLocator getServiceLocater() throws ServiceException
    {
        AppraisalService_ServiceLocator servLoc = new AppraisalService_ServiceLocator();
        if (servLoc == null)
        {
            return null;
        }
        servLoc.setEndpointAddress(_servicePort, _url+"?wsdl");
        return servLoc;
    }
    
    public AppraisalService_PortType getServiceStub(AppraisalService_ServiceLocator servLoc) throws ServiceException
    {
        AppraisalService_PortType stub = (AppraisalService_PortType)servLoc.getAppraisalService();
        return stub;
    }
    
    protected JAXBContext getJaxbContext()
    {
        return _jaxbContext;
    }
}
