package com.filogix.externallinks.services.responseprocessor.bnc;

import javax.xml.bind.*;
import javax.xml.transform.stream.*;
import java.io.*;
import org.apache.log4j.Logger;
import com.basis100.resources.*;
import com.basis100.jdbcservices.jdbcexecutor.*;
import com.basis100.deal.entity.*;
import com.basis100.deal.pk.*;
import java.util.*;
import com.filogix.externallinks.services.responseprocessor.*;
import com.filogix.externallinks.services.*;
import com.filogix.externallinks.adjudication.responseprocessor.jaxb.payload.*;
import com.filogix.externallinks.adjudication.responseprocessor.jaxb.payload.
	CreditDecisionResponseType.*;
import java.math.*;
import com.filogix.externallinks.adjudication.responseprocessor.jaxb.payload.
	CreditDecisionResponseType.DecisionType.*;

/**
 * <p>Title: BncGcdResponseProcessor</p>
 * <p>Description: Resposible for parsing the response XML and storing its
 * data into respective database tables</p>
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 * <p>Company: Filogix Inc.</p>
 * @author NBC/GCD Implementation Team
 * @version 1.0
 */

public class BncGcdResponseProcessor
	implements IResponseProcessor
{
	public static final Logger log = Logger.getLogger(BncGcdResponseProcessor.class);
	protected SessionResourceKit srk;
	private ServiceInfo serviceInfo;
	protected int requestId = -1;
	protected int responseId = -1;
	protected int dealId = -1;
	protected int copyId = -1;
	private static final String JAXB_PACKAGE =
		"com.filogix.externallinks.adjudication.responseprocessor.jaxb.payload";

	public BncGcdResponseProcessor()
	{
	}

	/**
	 * Process the response
	 * @param obj Object: This contains the response XML text
	 * @param info ServiceInfo
	 * @param validate boolean True if validation is required, false otherwise
	 * @throws Exception
	 */
	public void processResponse(Object obj, ServiceInfo info, boolean validate)
		throws Exception
	{
		CreditDecisionResponse bncGcdResponse = null;

		serviceInfo = info;
		srk = serviceInfo.getSrk();
		Request request = retrieveRequest(serviceInfo.getChannelTransactionKey(), serviceInfo.getRequestType());
		requestId = request.getRequestId();
		dealId = request.getDealId();
		copyId = request.getCopyId();

		//obtain JAXB context
		JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

		// create the unmarshaller
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		unmarshaller.setValidating(validate);

		if (obj != null && ((String)obj).length() > 0)
		{
			// create the highest level object
			bncGcdResponse = (CreditDecisionResponse)unmarshaller.unmarshal(new
				StreamSource(new StringReader((String)obj)));

			storeData(bncGcdResponse);
		}
		else
		{
			throw new Exception("XML response is null or empty.");
		}
	}

	/**
	 * Stores the CreditDecisionResponse data object into respective database tables
	 * @param bncGcdResponse CreditDecisionResponse data object created
	 * from the XML response
	 * @return boolean True if successful, false otherwise
	 */
	private void storeData(CreditDecisionResponse bncGcdResponse)
		throws Exception
	{
		try
		{
			srk.beginTransaction();
			responseId = storeResponse(bncGcdResponse);
			if (responseId != -1)
			{
				storeAdjudicationResponse(bncGcdResponse);
				storeBncAdjudicationResponse(bncGcdResponse);
				storeBncManReviewReason(bncGcdResponse);
				storeBncAutoDeclineReason(bncGcdResponse);
				storeAdjudicationApplicantResponse(bncGcdResponse);
			}
			srk.commitTransaction();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
			try
			{
				srk.rollbackTransaction();
			}
			catch (JdbcTransactionException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				log.error("Failed to store data due to the following error: " +
						  e.getMessage());
				System.out.println("Failed to store data due to the following error: " +
						  e.getMessage());
				e.printStackTrace();
				throw e;
			}
		}
	}

	/**
	 * Helper function that creates a Response entity object using relevent
	 * data from the CreditDecisionResponse object and persists it into the database
	 * @param bncGcdResponse CreditDecisionResponse data object created
	 * from the XML response
	 * @return responseId int The current response ID for this response
	 * @throws Exception Thrown if error in database communication
	 */
	public int storeResponse(CreditDecisionResponse bncGcdResponse)
		throws Exception
	{
		Response response = new Response(srk);
		if (bncGcdResponse.getDealId() != null)
		{
			int resposneStatusId = 0; // ie. Blank
			String channelTrascationKey = serviceInfo.getChannelTransactionKey();
			DealPK dealPk = new DealPK(dealId, copyId);
			response.create(new Date(), requestId, dealPk, new Date(),
							resposneStatusId);
			response.setChannelTransactionKey(channelTrascationKey);
			response.ejbStore();
			return response.getPk().getId();
		}
		return -1;
	}

	/**
	 * Helper function that creates a AdjudicationResponse entity object using relevent
	 * data from the CreditDecisionResponse object and persists it into the database
	 * @param bncGcdResponse CreditDecisionResponse data object created
	 * from the XML response
	 * @throws Exception Thrown if error in database communication
	 */
	public void storeAdjudicationResponse(CreditDecisionResponse bncGcdResponse)
		throws Exception
	{
		AdjudicationResponse response = new AdjudicationResponse(srk);
		int adjudicationStatus = parseAdjudicationsStatusID(bncGcdResponse);
		ResponsePK responsePK = new ResponsePK(responseId);
		response.create(responsePK, adjudicationStatus);
		DecisionType decision = bncGcdResponse.getDecision();
		Object data = null;
		if (decision != null)
		{
			if ((data = decision.getGlobalCreditBureauScore()) != null)
			{
				response.setGlobalCreditBureauScore(((BigInteger)data).intValue());
			}
			if ((data = decision.getGlobalInternalScore()) != null)
			{
				response.setGlobalInternalScore(((BigInteger)data).intValue());
			}
			if ((data = decision.getGlobalRiskRating()) != null)
			{
				response.setGlobalRiskRating(((BigDecimal)data).doubleValue());
			}
			response.ejbStore();
		}

	}

	/**
	 * Helper function that creates a BncAdjudicationResponse entity object using relevent
	 * data from the CreditDecisionResponse object and persists it into the database
	 * @param bncGcdResponse CreditDecisionResponse data object created
	 * from the XML response
	 * @throws Exception Thrown if error in database communication
	 */
	public void storeBncAdjudicationResponse(CreditDecisionResponse
											 bncGcdResponse)
		throws Exception
	{
		BncAdjudicationResponse response = new BncAdjudicationResponse(srk);
		response.create(new BncAdjudicationResponsePK(responseId),
						parseRecommendedCodeId(bncGcdResponse));

		Object data = null;
		MortgageinsuranceType mi = bncGcdResponse.getMortgageinsurance();
		if (mi != null)
		{
			if ((data = mi.getAmountInsured()) != null)
			{
				response.setAmountInsured(((BigDecimal)data).
										  doubleValue());
			}
			if ((data = mi.getPrimaryApplicantRiskId()) != null)
			{
				response.setPrimaryApplicantRiskId(((BigInteger)data).
					intValue());
			}
			if ((data = mi.getSecondaryApplicantRiskId()) != null)
			{
				response.setSecondaryApplicantRiskId(((BigInteger)
					data).intValue());
			}
			if ((data = mi.getMarketRiskId()) != null)
			{
				response.setMarketRiskId(((BigInteger)data).intValue());
			}
			if ((data = mi.getPropertyRiskId()) != null)
			{
				response.setPropertyRiskId(((BigInteger)data).intValue());
			}
			if ((data = mi.getNeighborhoodRiskId()) != null)
			{
				response.setNeighbourhoodRiskId(((BigInteger)data).
												intValue());
			}
			if ((data = mi.getMIDecision()) != null)
			{
				response.setMiDecision((String)data);
			}
		}

		DecisionType decision = bncGcdResponse.getDecision();
		if (decision != null)
		{
			if ((data = decision.getNetWorth()) != null)
			{
				response.setGlobalNetworth(((BigDecimal)data).
										   doubleValue());
			}
			if ((data = decision.getCreditBureauNameUsedId()) != null)
			{
				response.setGlobalUsedCreditBureauNameId((
					(BigInteger)data).intValue());
			}
		}

		ProductType product = bncGcdResponse.getProduct();
		if (product != null)
		{
			if ((data = product.getProductCode()) != null)
			{
				response.setProductCode((String)data);
			}
		}

		if ((data = bncGcdResponse.getRandomDigit()) != null)
		{
			response.setRandomDigit(((BigInteger)data).intValue());
		}
		if ((data = bncGcdResponse.getSubmissionCount()) != null)
		{
			response.setSubmissionCount(((BigInteger)data).intValue());
		}

		response.ejbStore();
	}

	/**
	 * Parses the recommend code from the response into the respective
	 * recommend code id
	 * @param bncGcdResponse CreditDecisionResponse response object
	 * @return int recommend code id
	 */
	private int parseRecommendedCodeId(CreditDecisionResponse
									   bncGcdResponse)
	{
		int recommendedCodeId = 2;
		DecisionType decision = bncGcdResponse.getDecision();
		if (decision != null)
		{
			String recommendedCode = decision.getRecommendCode();
			if (recommendedCode != null)
			{
				if (recommendedCode.equalsIgnoreCase("A"))
				{
					recommendedCodeId = 0;
				}
				else if (recommendedCode.equalsIgnoreCase("R"))
				{
					recommendedCodeId = 1;
				}
				else if (recommendedCode.equalsIgnoreCase("I"))
				{
					recommendedCodeId = 2;
				}
			}
		}
		return recommendedCodeId;
	}

	/**
	 * Parses the decision code from the response into the respective
	 * adjudication status id
	 * @param bncGcdResponse CreditDecisionResponse response object
	 * @return int adjudication status id
	 */
	private int parseAdjudicationsStatusID(CreditDecisionResponse
										   bncGcdResponse)
	{
		int adjudicationStatusId = 0;
		DecisionType decision = bncGcdResponse.getDecision();
		if (decision != null)
		{
			String decisionCode = decision.getDecisionCode();
			if (decisionCode != null)
			{
				if (decisionCode.equalsIgnoreCase("A"))
				{
					adjudicationStatusId = 1;
				}
				else if (decisionCode.equalsIgnoreCase("R"))
				{
					adjudicationStatusId = 2;
				}
				else if (decisionCode.equalsIgnoreCase("C"))
				{
					adjudicationStatusId = 3;
				}
			}
		}
		return adjudicationStatusId;
	}

	/**
	 * Helper function that creates a BncManReviewReason entity object using relevent
	 * data from the CreditDecisionResponse object and persists it into the database
	 * @param bncGcdResponse CreditDecisionResponse data object created
	 * from the XML response
	 * @throws Exception Thrown if error in database communication
	 */
	public void storeBncManReviewReason(CreditDecisionResponse
										bncGcdResponse)
		throws Exception
	{

		BncManReviewReason response = null;
		Object data = null;
		DecisionType decision = bncGcdResponse.getDecision();
		if (decision != null)
		{
			List manualReviews = decision.getManualReview();
			String code = "";
			String description = "No description.";
			Iterator reviewIterator = manualReviews.iterator();
			while (reviewIterator.hasNext())
			{
				ManualReviewType review = (ManualReviewType) reviewIterator.next();

					// ensure that if the response tag is empty, we do not insert into the database
				response = new BncManReviewReason(srk);
				code = ( String ) review.getManualReviewReasonCode();
				if ( code != null && ! code.equals( "" ) )
				{

				if ((data = review.getManualReviewReasonDescription()) != null)
				{
					description = (String)data;
				}

				response.create(responseId, code, description);
			}
		}
	}
	}

	/**
	 * Helper function that creates a BncAutoDeclineReason entity object using relevent
	 * data from the CreditDecisionResponse object and persists it into the database
	 * @param bncGcdResponse CreditDecisionResponse data object created
	 * from the XML response
	 * @throws Exception Thrown if error in database communication
	 */
	public void storeBncAutoDeclineReason(CreditDecisionResponse
										  bncGcdResponse)
		throws Exception
	{

		BncAutoDeclineReason response = null;
		Object data = null;
		DecisionType decision = bncGcdResponse.getDecision();
		if (decision != null)
		{
			List autoReclines = decision.getAutomaticDecline();
			String code = "";
			String description = "No description.";
			Iterator declineIterator = autoReclines.iterator();
			while (declineIterator.hasNext())
			{


				AutomaticDeclineType review = (AutomaticDeclineType)  declineIterator.next();
				response = new BncAutoDeclineReason(srk);

					//ensure that if the response tag is empty, we do not insert into the database
				code = ( String ) review.getAutomaticDeclineReasonCode();
				if ( code != null && ! code.equals( "" ) )
				{
				if ((data = review.getAutomaticDeclineReasonDescription()) != null)
				{
					description = (String)data;
				}

				response.create(responseId, code, description);
			}

			}
		}
	}

	/**
	 * Helper function that creates AdjudicationApplicant and
	 * BncAdjudicationApplicant entity object using relevent
	 * data from the CreditDecisionResponse object and persists it into the database
	 * @param bncGcdResponse CreditDecisionResponse data object created
	 * from the XML response
	 * @throws Exception Thrown if error in database communication
	 */
	public void storeAdjudicationApplicantResponse(CreditDecisionResponse
		bncGcdResponse)
		throws Exception
	{
		AdjudicationApplicantResponse response;
		BncAdjudicationApplicantResponse bncResponse = null;
		AdjudicationApplicantRequest applicantRequest = null;

		double networth = 0.0;
		int usedCreditBureauNamedId = -1;
		int borrowerId = -1;

		List borrowers = bncGcdResponse.getBorrower();
		Iterator borrowerIterator = borrowers.iterator();
		Object data = null;

		while (borrowerIterator.hasNext())
		{

			BorrowerType borrower = (BorrowerType)borrowerIterator.next();
			if ((data = borrower.getBorrowerId()) != null)
			{
				// insert into Adjudication Applicant response
				borrowerId = ((BigInteger)data).intValue();
				applicantRequest = new
					AdjudicationApplicantRequest(srk);
				int applicantNumber = 0;
				try
				{
					applicantNumber = applicantRequest.
						findByBorrowerAndRequestId(
						new RequestPK(requestId, copyId),
						new BorrowerPK(borrowerId, copyId))
						.getApplicantNumber();
				}
				catch (Exception ex)
				{
					log.error("Unable to locate borrower[" + borrowerId +
							  "] for request[" + requestId +
							  "] and copy ID [" + copyId +
							  "] in AdjudicationApplicantRequest Table: " +
							  ex.getMessage());
					System.out.println("Unable to locate borrower[" +
									   borrowerId +
									   "] for request[" + requestId +
									   "] and copy ID [" + copyId +
									   "] in AdjudicationApplicantRequest Table: " +
									   ex.getMessage());
					//ex.printStackTrace();
					continue;
				}
				response = new AdjudicationApplicantResponse(srk);
				AdjudicationApplicantResponsePK pk = new
					AdjudicationApplicantResponsePK(responseId, applicantNumber);
				response.create(pk, copyId, borrowerId, " ");
				if ((data = borrower.getRiskRating()) != null)
				{
					response.setRiskRating(((BigDecimal)data).doubleValue());
				}
				if ((data = borrower.getInternalScore()) != null)
				{
					response.setInternalScore(((BigInteger)data).intValue());
				}
				if ((data = borrower.getCreditBureauScore()) != null)
				{
					response.setCreditBureauScore(((BigInteger)data).intValue());
				}
				if ((data = borrower.getCreditBureauReport()) != null)
				{
					response.saveReport((String)data, pk);
				}
				response.ejbStore();

				// insert into BNC Adjudication Applicant response
				bncResponse = new BncAdjudicationApplicantResponse(srk);
				if ((data = borrower.getCreditBureauNameUsedId()) != null)
				{
					usedCreditBureauNamedId = ((BigInteger)data).intValue();
				}
				if ((data = borrower.getNetWorth()) != null)
				{
					networth = ((BigDecimal)data).doubleValue();
				}
				bncResponse.create(new BncAdjudicationApplicantResponsePK(
					responseId,
					applicantNumber));
				bncResponse.setNetworth(networth);
				bncResponse.setUsedCreditBureauNamedId(usedCreditBureauNamedId);
				bncResponse.ejbStore();
			}

		}
	}

	/**
	 * Retrieve the corresponding Request for the given channle transaction key
	 * @param channelTransactionKey String Channel Trasnaction Key
	 * @throws Exception Thrown in error
	 * @return Request Corresponding request object
	 */
	private Request retrieveRequest(String channelTransactionKey, String requestType)
		throws Exception
	{
		Request request = new Request(srk);
		request = request.findByChannelTranKeyAndServProduct(channelTransactionKey, requestType);
		return request;
	}

	/**
	 * Retrieves the current response Id that is created
	 * @return int Response Id
	 */
	public int getResponseId()
	{
		return responseId;
	}

	/**
	 * Retrieves the current request Id that the response is mapped to
	 * @return int Request Id
	 */
	public int getRequestId()
	{
		return requestId;
	}

	/**
	 * Retrieves the current copy Id that the response is mapped to
	 * @return int Copy Id
	 */
	public int getCopyId()
	{
		return copyId;
	}

}
