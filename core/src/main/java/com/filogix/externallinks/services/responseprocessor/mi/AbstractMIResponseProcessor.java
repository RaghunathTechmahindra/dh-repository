package com.filogix.externallinks.services.responseprocessor.mi;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.entity.Response;
import com.basis100.deal.util.DealFeeCreator;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.picklist.PicklistData;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.mi.MIRequestUtils;
import com.filogix.externallinks.services.responseprocessor.IResponseProcessor;
import com.filogix.externallinks.services.responseprocessor.ResponseOutdatedException;

public abstract class AbstractMIResponseProcessor implements IResponseProcessor {

    private final static Logger logger = LoggerFactory.getLogger(AbstractMIResponseProcessor.class);

    private int serviceProductId;
    private boolean executeCalc = false;
    private IResponseAdaptor miResponseAdaptor;
    private boolean asynchronous = true;

    /**
     * process response.
     * 
     * @param payload
     * @param info
     * @param validate
     * @throws ExternalServicesException
     *             - all errors
     * @throws ResponseOutdatedException
     *             - when MIDealRequestNumber doesn't match
     */
    public void processResponse(Object payload, ServiceInfo info, boolean validate)
        throws ExternalServicesException, ResponseOutdatedException {

        if (payload == null || "".equals(payload))
            throw new IllegalArgumentException("payload is not assigned");
        if (info == null)
            throw new IllegalArgumentException("ServiceInfo is invalid ");
        if (info.getSrk() == null)
            throw new IllegalArgumentException("srk is invalid");
        if (info.getDeal() == null || info.getDeal().getDealId() <= 0)
            throw new IllegalArgumentException("ServiceInfo.deal is invalid ");
        if (info.getChannelTransactionKey() == null)
            throw new IllegalArgumentException("ChannelTransactionKey is invalid ");

        SessionResourceKit srk = info.getSrk();
        Deal currentDeal = info.getDeal();

        if (this.isAsynchronous()) {
            // check if current mi status can accept async response
            if (canAcceptAsyncResponse(srk, currentDeal) == false){
                //log to dealhistory
                MIRequestUtils.addDealHistLogUnsolicitedReject(currentDeal, srk);
                return;
            }
        }

        String payloadXml = (String) payload;
        miResponseAdaptor.initialize(payloadXml);

        if (this.isAsynchronous()) {
            // check if the response is for the latest request
            verifyMIResponse(currentDeal, srk);
        }

        try {
            // calc instance is created only when it's needed
            CalcMonitor dcm = null;
            if (isExecuteCalc()) {
                dcm = CalcMonitor.getMonitor(srk);
            }

            DealPropagator dealPropagator = new DealPropagator(currentDeal, dcm);
            DealFeeCreator dfCreator = new DealFeeCreator();

            // main for each MI provider
            setPropagateMIFields(payloadXml, info, miResponseAdaptor, dcm, dealPropagator, dfCreator);

            // notifies Premium change (regardless of sync or async response)
            // also updates mistatus if premium has been changed
            handleMIPremiumChange(dealPropagator, currentDeal.getMIPremiumAmount());

            
            srk.beginTransaction();

            // delete old deal fee
            // delete existing MI related dealFee
            DealFee dealFee = new DealFee(srk, null);
            dealFee.removeMIFees(currentDeal.getDealId());

            // create required MI dealfees
            dfCreator.batchCreateDealFees(srk, currentDeal);

            // This ejbStore updates deal without copyid
            dealPropagator.ejbStore();
            info.setDeal(dealPropagator);

            // calc is executed only when it's needed
            if (isExecuteCalc()) {
                dcm.calc();
            }

            // update final result to response
            updateResponse(miResponseAdaptor, srk, dealPropagator, info.getChannelTransactionKey());

            MIRequestUtils.addDealHistLogForResponse(dealPropagator, srk);

            srk.commitTransaction();

        } catch (Exception e) {
            srk.cleanTransaction();
            String errMsg = "Exception in AbstractMIResponseProcessor";
            logger.error(errMsg, e);
            throw new ExternalServicesException(errMsg, e);
        }

    }

    /**
     * make sure to process the latest response checking
     * deal.MIDealRequestNumber
     * 
     * @param deal
     * @throws ExternalServicesException
     *             : response doesn't contain MIDealRequestNumber
     * @throws ResponseOutdatedException
     *             : MIDealRequestNumber doesn't match
     */
    public void verifyMIResponse(Deal deal, SessionResourceKit srk) throws ExternalServicesException, ResponseOutdatedException {

        BigInteger miDealReqNum = miResponseAdaptor.getMIDealRequestNumber();
        if (miDealReqNum == null) {
            throw new ExternalServicesException("MIDealRequestNumber in the payload is empty");
        }

        // check MIDealRequestNumber for asynchronous response
        if (deal.getMIDealRequestNumber() != miDealReqNum.intValue()) {
            // log to dealhistory
            MIRequestUtils.addDealHistLogUnsolicitedReject(deal, srk);

            throw new ResponseOutdatedException("MIDealRequestNumber doesn't match. expected:"
                    + deal.getMIDealRequestNumber() + " but received:" +
                    miDealReqNum.intValue());
        }

        // just in case, make sure MortgageInsurer is the same
        BigInteger mortgageInsurer = miResponseAdaptor.getMortgageInsurerId();
        if (mortgageInsurer != null) {
            if (deal.getMortgageInsurerId() != mortgageInsurer.intValue()) {
                // log to dealhistory
                MIRequestUtils.addDealHistLogUnsolicitedReject(deal, srk);

                throw new ResponseOutdatedException("MortgageInsurer doesn't match. expected:"
                    + deal.getMIDealRequestNumber() + " but received:" +
                    miDealReqNum.intValue());
            }
        }

    }

    public boolean canAcceptAsyncResponse(SessionResourceKit srk, Deal deal) throws ExternalServicesException {

        // when deal is Approval, Decline
        // or Cancellation and there are additional unsolicited responses are
        // submitted by the MI Provider, these responses will be thrown away

        switch (deal.getMIStatusId()) {
        case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC:
        case Mc.MI_STATUS_CANCELLED:
        case Mc.MI_STATUS_APPROVAL_RECEIVED:
        case Mc.MI_STATUS_APPROVED:
        case Mc.MI_STATUS_DENIED_RECEIVED:
        case Mc.MI_STATUS_DENIED:
        case Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED:
            return false;
        default:
            return true;
        }
    }

    /**
     * main logic for each concrete MI processor
     * 
     * @param payload
     * @param info
     * @param miResponse
     * @param dcm
     * @param dealPropagetor
     * @param dfCreator
     * @throws ExternalServicesException
     */
    abstract protected void setPropagateMIFields(String payload, ServiceInfo info, IResponseAdaptor miResponse, CalcMonitor dcm,
        DealPropagator dealPropagetor,
        DealFeeCreator dfCreator)
        throws ExternalServicesException;

    /**
     * update status message in response table
     * 
     * @param miResponse
     * @param srk
     * @param deal
     * @param channelTransactionKey
     * @throws Exception
     */
    protected void updateResponse(IResponseAdaptor miResponse,
        SessionResourceKit srk, Deal deal, String channelTransactionKey) throws Exception {

        Response response = new Response(srk);
        response = response.findChannelTransactionData(channelTransactionKey, deal.getDealId(), getServiceProductId());
        String stausMessage = miResponse.getStatusMessage();
        response.setStatusMessage(stausMessage);

        response.ejbStore();
    }

    /**
     * append mi response text
     * 
     * @param miResponse
     * @param deal
     * @return
     */
    protected static String makeMortgageInsuranceResponseText(IResponseAdaptor miResponse, Deal deal) {

        String responseText = miResponse.getMIResponse();

        String previousResponses = deal.getMortgageInsuranceResponse();
        String crlf = "\r\n"; // make it consistent both in windows and UNIX

        StringBuilder sb = new StringBuilder();
        if (responseText != null) {
            sb.append(responseText);
        }
        if (previousResponses != null) {
            if (responseText != null) {
                sb.append(crlf).append(crlf);
            }
            sb.append(previousResponses);
        }

        return sb.toString();
    }

    /**
     * notifies Premium change (regardless of sync or async response), and also
     * updates MI status if premium has been changed
     * 
     * @param deal
     * @param currentPremiumAmount
     * @throws ExternalServicesException
     * @throws RemoteException
     */
    public static void handleMIPremiumChange(DealPropagator deal, double currentPremiumAmount)
        throws ExternalServicesException, RemoteException {

        if (deal == null) return;

        // target is approved response only
        if (deal.getMIStatusId() != Mc.MI_STATUS_APPROVAL_RECEIVED) return;

        if (deal.getMIPremiumAmount() == currentPremiumAmount) return;

        // preserve previous mi amount
        deal.setMIPremiumAmountPrevious(currentPremiumAmount);
        // flag that tells users MI Premium is changed
        deal.setMIPremiumAmountChangeNortified("N");

        // overwrite mi status
        deal.setMIStatusId(Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED);

    }

    public int getServiceProductId() {
        return serviceProductId;
    }

    public void setServiceProductId(int serviceProductId) {
        this.serviceProductId = serviceProductId;
    }

    public boolean isExecuteCalc() {
        return executeCalc;
    }

    public void setExecuteCalc(boolean executeCalc) {
        this.executeCalc = executeCalc;
    }

    public boolean isAsynchronous() {
        return asynchronous;
    }

    public void setAsynchronous(boolean asynchronous) {
        this.asynchronous = asynchronous;
    }

    public IResponseAdaptor getMiResponseAdaptor() {
        return miResponseAdaptor;
    }

    public void setMiResponseAdaptor(IResponseAdaptor miResponseAdaptor) {
        this.miResponseAdaptor = miResponseAdaptor;
    }

}
