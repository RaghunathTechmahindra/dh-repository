package com.filogix.externallinks.services.channel;

import java.rmi.Remote;
import java.rmi.RemoteException;

import com.filogix.datx.framework.packaging.FXRequest;
import com.filogix.datx.framework.packaging.FXResponse;

public interface GenericWebServiceInterface extends Remote {
	
	public FXResponse Invoke(FXRequest request, String xmlPayload)throws RemoteException;

}
