package com.filogix.externallinks.services.channel.ug;


import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.pk.DealPK;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.datx.framework.packaging.FXRequest;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.*;

/**
 * UGPremiumRequestChannel
 * 
 * This abtract class serves as a base class for UGMIRequestChannel and UGPremiumRequestChannel
 * @author: Lin Zhou
 * @date: August 30, 2006
 */
public abstract class UGMIChannel extends Channel {
	
	private static Log log = LogFactory.getLog(UGMIChannel.class);

	

//	/**
//	 * Create a record in REQUEST Table
//	 */
//	protected void createRequest(ServiceInfo info) throws Exception {
//
//		SessionResourceKit srk = info.getSrk();
//		try {
//			Deal aDeal = new Deal(info.getSrk(), null);
//
//			DealPK dealpk = new DealPK(info.getDealId().intValue(), info
//					.getCopyId().intValue());
//			aDeal.findByPrimaryKey(dealpk);
//			Request request = new Request(srk);
//
//			srk.beginTransaction();
//			request.create(dealpk, 0, new Date(), aDeal
//					.getUnderwriterUserId());
//			request.setRequestTypeId(0);
//
//			request.setDealId(info.getDealId().intValue());
//			request.setCopyId(info.getCopyId().intValue());
//
//			// ug setting
//			request.setChannelId(getChannelId());
//			request.setPayloadTypeId(getPayloadTypeId());
//			request.setServiceTransactionTypeId(getServiceTransactionTypeId());
//			request.setServiceProductId(getServiceProductId());
//
//			request.ejbStore();
//			srk.commitTransaction();
//
//			info.setRequestId(request.getRequestId());
//		} catch (Exception e) {
//			srk.cleanTransaction();
////			e.printStackTrace();
//			log.error("Can not create entities!", e);
//            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED", info.getLanguageId());
//            throw new RequestProcessException(msg, e);
//		}
//	}


	
	
}
