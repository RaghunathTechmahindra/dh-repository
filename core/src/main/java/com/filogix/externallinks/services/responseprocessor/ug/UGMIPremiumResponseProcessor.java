package com.filogix.externallinks.services.responseprocessor.ug;

import com.filogix.externallinks.services.responseprocessor.IResponseProcessor;
import com.filogix.externallinks.services.responseprocessor.MIPremiumResponseProcessor;

public class UGMIPremiumResponseProcessor
    extends MIPremiumResponseProcessor
{
    static private IResponseProcessor instance = new UGMIPremiumResponseProcessor();

    public UGMIPremiumResponseProcessor()
    {
    }

    public static IResponseProcessor getInstance()
    {
        return instance;
    }
}
