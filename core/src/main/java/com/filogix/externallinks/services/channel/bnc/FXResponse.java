/**
 * FXResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package com.filogix.externallinks.services.channel.bnc;

public class FXResponse  implements java.io.Serializable {
    private java.lang.String clientKey;

    private java.lang.String externalKey;

    private com.filogix.externallinks.services.channel.bnc.Payload[] payloads;

    private java.lang.Integer resultCode;

    private java.lang.String resultDescription;

    private java.lang.String serviceKey;

    public FXResponse() {
    }

    public FXResponse(
           java.lang.String clientKey,
           java.lang.String externalKey,
           com.filogix.externallinks.services.channel.bnc.Payload[] payloads,
           java.lang.Integer resultCode,
           java.lang.String resultDescription,
           java.lang.String serviceKey) {
           this.clientKey = clientKey;
           this.externalKey = externalKey;
           this.payloads = payloads;
           this.resultCode = resultCode;
           this.resultDescription = resultDescription;
           this.serviceKey = serviceKey;
    }


    /**
     * Gets the clientKey value for this FXResponse.
     * 
     * @return clientKey
     */
    public java.lang.String getClientKey() {
        return clientKey;
    }


    /**
     * Sets the clientKey value for this FXResponse.
     * 
     * @param clientKey
     */
    public void setClientKey(java.lang.String clientKey) {
        this.clientKey = clientKey;
    }


    /**
     * Gets the externalKey value for this FXResponse.
     * 
     * @return externalKey
     */
    public java.lang.String getExternalKey() {
        return externalKey;
    }


    /**
     * Sets the externalKey value for this FXResponse.
     * 
     * @param externalKey
     */
    public void setExternalKey(java.lang.String externalKey) {
        this.externalKey = externalKey;
    }


    /**
     * Gets the payloads value for this FXResponse.
     * 
     * @return payloads
     */
    public com.filogix.externallinks.services.channel.bnc.Payload[] getPayloads() {
        return payloads;
    }


    /**
     * Sets the payloads value for this FXResponse.
     * 
     * @param payloads
     */
    public void setPayloads(com.filogix.externallinks.services.channel.bnc.Payload[] payloads) {
        this.payloads = payloads;
    }


    /**
     * Gets the resultCode value for this FXResponse.
     * 
     * @return resultCode
     */
    public java.lang.Integer getResultCode() {
        return resultCode;
    }


    /**
     * Sets the resultCode value for this FXResponse.
     * 
     * @param resultCode
     */
    public void setResultCode(java.lang.Integer resultCode) {
        this.resultCode = resultCode;
    }


    /**
     * Gets the resultDescription value for this FXResponse.
     * 
     * @return resultDescription
     */
    public java.lang.String getResultDescription() {
        return resultDescription;
    }


    /**
     * Sets the resultDescription value for this FXResponse.
     * 
     * @param resultDescription
     */
    public void setResultDescription(java.lang.String resultDescription) {
        this.resultDescription = resultDescription;
    }


    /**
     * Gets the serviceKey value for this FXResponse.
     * 
     * @return serviceKey
     */
    public java.lang.String getServiceKey() {
        return serviceKey;
    }


    /**
     * Sets the serviceKey value for this FXResponse.
     * 
     * @param serviceKey
     */
    public void setServiceKey(java.lang.String serviceKey) {
        this.serviceKey = serviceKey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FXResponse)) return false;
        FXResponse other = (FXResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clientKey==null && other.getClientKey()==null) || 
             (this.clientKey!=null &&
              this.clientKey.equals(other.getClientKey()))) &&
            ((this.externalKey==null && other.getExternalKey()==null) || 
             (this.externalKey!=null &&
              this.externalKey.equals(other.getExternalKey()))) &&
            ((this.payloads==null && other.getPayloads()==null) || 
             (this.payloads!=null &&
              java.util.Arrays.equals(this.payloads, other.getPayloads()))) &&
            ((this.resultCode==null && other.getResultCode()==null) || 
             (this.resultCode!=null &&
              this.resultCode.equals(other.getResultCode()))) &&
            ((this.resultDescription==null && other.getResultDescription()==null) || 
             (this.resultDescription!=null &&
              this.resultDescription.equals(other.getResultDescription()))) &&
            ((this.serviceKey==null && other.getServiceKey()==null) || 
             (this.serviceKey!=null &&
              this.serviceKey.equals(other.getServiceKey())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClientKey() != null) {
            _hashCode += getClientKey().hashCode();
        }
        if (getExternalKey() != null) {
            _hashCode += getExternalKey().hashCode();
        }
        if (getPayloads() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPayloads());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPayloads(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResultCode() != null) {
            _hashCode += getResultCode().hashCode();
        }
        if (getResultDescription() != null) {
            _hashCode += getResultDescription().hashCode();
        }
        if (getServiceKey() != null) {
            _hashCode += getServiceKey().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FXResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.filogix.com/datx", "FXResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.filogix.com/datx", "clientKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.filogix.com/datx", "externalKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payloads");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.filogix.com/datx", "payloads"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.filogix.com/datx", "Payload"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.filogix.com/datx", "resultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.filogix.com/datx", "resultDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.filogix.com/datx", "serviceKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
