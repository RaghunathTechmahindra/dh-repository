package com.filogix.externallinks.services.datx;

import org.w3c.dom.DOMException;

import com.basis100.deal.docprep.extract.data.AVMExtractor;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServicePayloadData;
import com.filogix.externallinks.framework.ServicePayloadGeneratorBase;

public class DatxAvmPayloadGenerator extends ServicePayloadGeneratorBase {

  protected String getXmlPayload() throws DOMException, Exception {
    AVMExtractor _extr = new AVMExtractor(this._srk, this._requestId,  this._copyId, 0 );
    this._payloadXml = _extr.buildXML();
    
    return this._payloadXml;
  }

  public ServicePayloadData getPayloadData() throws RemoteException, FinderException, ExternalServicesException {
    _data = new ServicePayloadData(_srk, _requestId, _copyId, ServiceConst.LANG_ENGLISH);
    
    return _data;
  }

  public ServicePayloadData getPayloadData(int langId) throws RemoteException, FinderException, ExternalServicesException {
    _data = new ServicePayloadData(_srk, _requestId, _copyId, langId);
    
    return _data;  
  }
}
