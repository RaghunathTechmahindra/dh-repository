/**
 * <p>Title: DatxCancelPayloadGenerator.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � Jun 12, 2006)
 *
 */

package com.filogix.externallinks.services.datx;

import javax.xml.bind.JAXBContext;
import com.basis100.deal.entity.Response;
import com.basis100.deal.entity.ServiceProduct;
import com.basis100.deal.entity.ServiceProvider;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbServicePayloadGenerator;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServicePayloadData;
import com.filogix.externallinks.framework.WSServiceChannel;
import com.filogix.schema.datx.ServiceCancelType;


public class DatxServiceCancelPayloadGenerator extends JaxbServicePayloadGenerator
{
    public static String _packageName = ServiceCancelType.class.getClass().getPackage().getName();
    
    // CR, 7-Aug-06: addition for language support  --- begin
    public ServicePayloadData getPayloadData() throws RemoteException, FinderException, ExternalServicesException {
      _data = getPayloadData(ServiceConst.LANG_ENGLISH);
      
      return _data;  
    }
    // CR, 7-Aug-06: addition for language support  --- end

    public ServicePayloadData getPayloadData(int langId)
            throws RemoteException,
                FinderException,
                ExternalServicesException
    {
        _data = new DatxServiceCancelPayloadData(_srk, _requestId, _copyId, langId);
        DatxServiceCancelPayloadData cancelPayloadData = (DatxServiceCancelPayloadData)_data;
        ServiceCancelType cancelPayload = cancelPayloadData.getServiceCancelPayload();
        
        // 1) set the ServiceRequest to Data
        try
        {
            setServiceRequestToData();
        }
        catch(Exception e)
        {
            throw new FinderException();
        }
        
        // 2) set Requester(under writer)
        setRequesterCmnTypes();
        cancelPayload.setRequester(cancelPayloadData.getJaxbRequester());

/**
        <xs:element name="providerName" type="xs:string"/>
        <xs:element name="productName" type="xs:string"/>
        <xs:element name="cancellationReason" type="xs:string" minOccurs="0"/>
        <xs:element ref="common:applicationReferenceNumber"/>
        <xs:element ref="common:transactionReferenceNumber" minOccurs="0"/>
        <xs:element ref="common:middlewareReferenceNumber" minOccurs="0"/>
        <xs:element ref="common:providerReferenceNumber" minOccurs="0"/>
        <xs:element name="requester" type="common:requesterType" minOccurs="0">
**/        
                
        // 6) other info        
        setProvider(cancelPayload);
        setProduct(cancelPayload);

        // 3) set ServiceType
        setServiceType(cancelPayload);

// special instruction holds cancel reason.
        cancelPayload.setCancellationReason(cancelPayloadData.getServiceRequest().getSpecialInstructions()); 
        
/**        
    <xs:element name="applicationReferenceNumber" type="xs:string">
        <xs:annotation>
            <xs:documentation>For express, this is dealId</xs:documentation>
        </xs:annotation>
    </xs:element>
**
        cancelPayload.setApplicationReferenceNumber("" + cancelPayloadData.getDealId()); 
/**        
    <xs:element name="transactionReferenceNumber" type="xs:string">
        <xs:annotation>
            <xs:documentation>For express, this is requestId</xs:documentation>
        </xs:annotation>
    </xs:element>
**/
        cancelPayload.setTransactionReferenceNumber("" + cancelPayloadData.getRequestId());
/**        
    <xs:element name="middlewareReferenceNumber" type="xs:string">
        <xs:annotation>
            <xs:documentation>This is DatX reference number.</xs:documentation>
        </xs:annotation>
    </xs:element>
**/
        Response responseEntity = getLastResponse(cancelPayloadData.getRequestId());
        cancelPayload.setMiddlewareReferenceNumber("" + responseEntity.getChannelTransactionKey());
/**        
    <xs:element name="providerReferenceNumber" type="xs:string">
        <xs:annotation>
            <xs:documentation>This is the reference or confirmation number returned from the service provider upon successful request.</xs:documentation>
        </xs:annotation>
    </xs:element>
**/        
        cancelPayload.setProviderReferenceNumber("" + cancelPayloadData.getServiceRequest().getServiceProviderRefNo());

        return _data;
    }

    private Response getLastResponse(int requestId) throws ExternalServicesException
    {
        Response response = null;
        try
        {
            response = new Response(_srk);
            response = response.findLastResponseByRequestId(requestId);
            return response;
        }
        catch(Exception fe)
        {
            throw new ExternalServicesException(fe.getMessage());
        }
    }

    
    private void setProvider(ServiceCancelType cancelPayload) throws RemoteException, FinderException
    {
        ServiceProvider serviceProvider = _data.getServiceProvider();
        cancelPayload.setProviderName(serviceProvider.getServiceProviderName());
    }

    private void setProduct(ServiceCancelType cancelPayload) throws FinderException, RemoteException
    {
        ServiceProduct serviceProduct = _data.getServiceProduct();
        cancelPayload.setProductName(serviceProduct.getServiceProductName());
    }
    
    private void setServiceType(ServiceCancelType cancelPayload) throws FinderException, RemoteException
    {
        ServiceProduct serviceProduct = _data.getServiceProduct();
        cancelPayload.setServiceType(serviceProduct.getServiceSubTypeName());
    }
    
    public String getPackageName()
    {
        return _packageName;
    }
    
    public JAXBContext getJaxbContext()
    {
        return WSServiceChannel._cancelJaxbContext;
    }
}
