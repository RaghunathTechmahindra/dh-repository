package com.filogix.externallinks.services.channel;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.ChannelPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.datx.framework.packaging.FXRequest;
import com.filogix.datx.framework.packaging.FXResponse;
import com.filogix.datx.framework.packaging.Payload;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.services.RequestService;
import com.filogix.externallinks.services.ServiceInfo;

/**
 * This is an abstract class that provides the following basic functions for
 * subclasses --> createRequest --> createResponse
 * 
 * Created on Aug 28, 2006
 * 
 * @author LZhou
 * 
 */

public abstract class Channel implements IChannel {
	
	protected static Log log = LogFactory.getLog(Channel.class);
	
	private int channelId;

	private int payloadTypeId;

	private int serviceTransactionTypeId;

	private int serviceProductId;

	private String endpoint;

	private String username;

	private String password;

	private int datxTimeout = 160000; // default to 160 seconds

	private int wsTimeout = 120; // default to 12 seconds

	/**
	 * @return String representation of real payload
	 */
	public Object send(ServiceInfo info, String xmlPayload) throws Exception {
		createRequest(info);
		FXResponse fxResponse = transmit(info, xmlPayload);
    if (fxResponse == null) 
    {
//      throw new ExternalServicesException(RequestService.DATX_CONNECTION_ERROR);
      throw new ExternalServicesException(RequestService.DATX_ERROR);
    }

    log.debug("**** FXResponse ****");
    log.debug("Clinet Key" + fxResponse.getClientKey());
    log.debug("Datx Key" + fxResponse.getDatXKey());
    log.debug("External Key" + fxResponse.getExternalKey());
    log.debug("Result Code" + fxResponse.getResultCode());
    log.debug("Result Description" + fxResponse.getResultDescription());
    createResponse(info, fxResponse);

//    if (fxResponse.getResultCode() == null) 
//    {
//      throw new ExternalServicesException(RequestService.DATX_ERROR + " Result Code is NULL");
//    }
//    
//    int resultCode = fxResponse.getResultCode().intValue();
//    
//    if (ServiceConst.RESULT_CODE_SYSTEM_ERROR == resultCode)
//    {
//      String errorMsg = BXResources.getPickListDescription("REQUESTSTATUS",
//                                                           ServiceConst.REQUEST_STATUS_SYSTEM_ERROR,
//                                                           Mc.LANGUAGE_PREFERENCE_ENGLISH);
//      throw new ExternalServicesException(RequestService.DATX_ERROR + errorMsg);
//    }                                     
//    if (ServiceConst.RESULT_CODE_PROCESSING_ERROR == resultCode)
//    {
//      String errorMsg = BXResources.getPickListDescription("REQUESTSTATUS",
//                                                           ServiceConst.REQUEST_STATUS_PROCESSING_ERROR,
//                                                           Mc.LANGUAGE_PREFERENCE_ENGLISH);
//      throw new ExternalServicesException(RequestService.DATX_ERROR + errorMsg);
//    }
    return getPayload(fxResponse);
	}

	protected String getPayload(FXResponse fxResponse) throws Exception {
		Payload[] payloads = fxResponse.getPayloads();
		byte[] xmlPayload = null;

		if (payloads != null && payloads.length > 0)
			xmlPayload = payloads[0].getContent();

        //FXP29912 fix
		if (xmlPayload != null)
			return new String(xmlPayload, "UTF-8");
		else
			return null;
	}

	protected void setupChannelInfo(ServiceInfo serviceInfo){
	    try {
            // changed since MLM
            // moved channel info, i.e. userid, password etc for Datx from xml to DB
            com.basis100.deal.entity.Channel channelEntity 
                = new com.basis100.deal.entity.Channel(serviceInfo.getSrk());
            channelEntity.findByPrimaryKey(new ChannelPK(getChannelId()));
            setUsername(channelEntity.getUserId());
            setPassword(channelEntity.getPassword());
            StringBuffer endpoint = new StringBuffer();
            endpoint.append(channelEntity.getProtocol()).append("://").append(channelEntity.getIp());
            if (channelEntity.getPort() != 0 )
                endpoint.append(":" + channelEntity.getPort());
            endpoint.append("/");
            endpoint.append(channelEntity.getPath());
            setEndpoint(endpoint.toString());
        } catch (Exception e) {
            log.error("@" + this.getClass().getName() 
                      + ": Cannot retreive channel info from DB with channel Id = " +  getChannelId());
        }
	}
	protected FXRequest construct(ServiceInfo serviceInfo) {
	    
		FXRequest req = new FXRequest();
		req.setUsername(getUsername());
		req.setPassword(getPassword());
		req.setTimeout(new Integer(getDatxTimeout()));
		req.setClientKey(serviceInfo.getDealId() + ""); // added in response to
    log.debug("**** FXRequest ****");
    log.debug("Clinet Key" + req.getClientKey());
    log.debug("Datx Key" + req.getDatXKey());
		// datx's request LZhou
		// Oct. 5, 2006
		return req;
	}

	abstract protected FXResponse transmit(ServiceInfo serviceInfo,
			String xmlPayload) throws Exception;
	
	abstract public void init(ServiceInfo serviceInfo) throws Exception ;

	/**
	 * Create a record in REQUEST Table
	 */
	protected void createRequest(ServiceInfo info) throws Exception {

		SessionResourceKit srk = info.getSrk();
		try {
		    // changed for FXP24408 to use deal from calc ---- START
			Deal aDeal = info.getDeal();
			DealPK dealpk = null;
			if (aDeal == null) {
			    aDeal = new Deal(info.getSrk(), null);
	            dealpk = new DealPK(info.getDealId().intValue(), info.getCopyId().intValue());
	            aDeal.findByPrimaryKey(dealpk);
			} else {
			    dealpk = (DealPK)aDeal.getPk();
			}
            // changed for FXP24408 to use deal from calc ---- END
			
			Request request = new Request(srk);

			srk.beginTransaction();
			request.create(dealpk, 0, new Date(), aDeal.getUnderwriterUserId());
			request.setRequestTypeId(0); 
		
			
			request.setDealId(info.getDealId().intValue());
			request.setCopyId(info.getCopyId().intValue());
			
			request.setChannelId(getChannelId());
			request.setPayloadTypeId(getPayloadTypeId());
			request.setServiceTransactionTypeId(getServiceTransactionTypeId());
			request.setServiceProductId(getServiceProductId());
			
			request.ejbStore();
			srk.commitTransaction();

			info.setRequestId(request.getRequestId());
		} catch (Exception e) {
			srk.cleanTransaction();
			log.error("@Channel:: createRequest:: Error recieved - " + StringUtil.stack2string(e));
		}
	}

	/**
	 * Create a record in RESPONSE Table
	 */
	protected void createResponse(ServiceInfo info, FXResponse fxResponse)
			throws Exception {
		SessionResourceKit srk = info.getSrk();
		

		try {
			Response response = new Response(srk);
			Date responseDate = new Date();
			Date statusDate = responseDate;
			String channelTransactionKey = fxResponse.getDatXKey();
			
			srk.beginTransaction();
			
			response.create(response.createPrimaryKey(), 
          responseDate, info.getRequestId(), info.getDealId().intValue(), statusDate, // ?
					0); // ?

			// response.setStatusMessage(""); // ?
			response.setChannelTransactionKey(channelTransactionKey); // ?
			response.ejbStore();
			
			srk.commitTransaction();
			
			srk.beginTransaction();
			// update request table with channelTransactionKey
			Request request = new Request(srk, info.getRequestId(), info.getCopyId().intValue());
			
			request.setChannelTransactionKey(channelTransactionKey);
			request.ejbStore();
			
			srk.commitTransaction();
			info.setChannelTransactionKey(channelTransactionKey);
			

		} catch (Exception e) {
			srk.cleanTransaction();
			e.printStackTrace();
		}
	}

	// Setters
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public void setPayloadTypeId(int payloadTypeId) {
		this.payloadTypeId = payloadTypeId;
	}

	public void setServiceTransactionTypeId(int serviceTransactionTypeId) {
		this.serviceTransactionTypeId = serviceTransactionTypeId;
	}

	public void setServiceProductId(int serviceProductId) {
		this.serviceProductId = serviceProductId;
	} 

	// Getters
	public String getEndpoint() {
		return endpoint;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public int getChannelId() {
		return channelId;
	}

	public int getPayloadTypeId() {
		return payloadTypeId;
	}

	public int getServiceTransactionTypeId() {
		return serviceTransactionTypeId;
	}

	public int getServiceProductId() {
		return serviceProductId;
	}

	public int getDatxTimeout() {
		return datxTimeout;
	}

	public void setDatxTimeout(int datxTimeout) {
		this.datxTimeout = datxTimeout;
	}

	public int getWsTimeout() {
		return wsTimeout;
	}

	public void setWsTimeout(int wsTimeout) {
		this.wsTimeout = wsTimeout;
	}
}
