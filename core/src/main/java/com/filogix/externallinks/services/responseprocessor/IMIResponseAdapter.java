package com.filogix.externallinks.services.responseprocessor;

import java.math.BigDecimal;

public interface IMIResponseAdapter
{

  Object getAdapteeInstance();

  String getServiceSpecificReferenceNumber();

  String getDeal_StatusCode();

  String getDeal_MIResponse();

  BigDecimal getDeal_MIPremium();

  BigDecimal getDeal_MIFeeAmount();

  BigDecimal getDeal_MIPremiumPst();

  String getDeal_StatusMessage();
}
