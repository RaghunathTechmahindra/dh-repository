package com.filogix.externallinks.services.responseprocessor.mi;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.filogix.express.core.ExpressCoreContext;

public class MIResponseProcessorFactory {

    Map<Integer, String> responseProcessorMap;

    public static MIResponseProcessorFactory getInstance() {
        return (MIResponseProcessorFactory) ExpressCoreContext.getService("MIResponseProcessorFactory");
    }

    public AbstractMIResponseProcessor getAsyncMIResponseProcessor(int mortgageInsurerId) {
        String processorName = responseProcessorMap.get(mortgageInsurerId);
        if (StringUtils.isEmpty(processorName))
            throw new IllegalArgumentException(
                "Express couldn't find MIResponseProcessor for mortgageInsurerId=" + mortgageInsurerId);
        return (AbstractMIResponseProcessor) ExpressCoreContext.getService(processorName);
    }

    public void setResponseProcessorMap(Map<Integer, String> responseProcessorMap) {
        this.responseProcessorMap = responseProcessorMap;
    }

}
