package com.filogix.externallinks.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.payload.IPayloadGenerator;
import com.filogix.externallinks.services.channel.IChannel;
import com.filogix.externallinks.services.responseprocessor.IResponseProcessor;
import com.basis100.deal.util.StringUtil;

/**
 *
 * <p>Title: Filogix Express - AIG UG</p>
 *
 * <p>Description: Filogix AIGUG MI Project</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Filogix</p>
 *
 * @author Lin Zhou
 * @version 1.0
 */

public class RequestService implements IRequestService {
	private static Log log = LogFactory.getLog(IRequestService.class);
    private IPayloadGenerator payloadGenerator;
    private IChannel channel;
    private IResponseProcessor responseProcessor;
    
  public static final String DATX_ERROR = "datx connection failure";
//  public static final String DATX_ERROR = "datx failure";
//    public static final String DATX_CONNECTION_ERROR = "datx connection failure";
    
    public void service(ServiceInfo serviceInfo) throws Exception {
    	/* As discussed on Sept. 18, 2006, we turn off request payload validation,
    	 * as it may cause java.lang.StackOverflowError, and the validation is always
    	 * performed on DatX and UG sides ---Lin Zhou
    	 */
    	try {
    	    channel.init(serviceInfo);
    		Object response = channel.send(serviceInfo, payloadGenerator.getXMLPayload(false, serviceInfo));
    		responseProcessor.processResponse(response, serviceInfo, true);
    	}
    	catch(Exception e) {
			
    	  log.error("Error recieved - " + StringUtil.stack2string(e));
    		if(serviceInfo.getRequestType().equals(ServiceDelegate.PREMIUM_REQUEST_TYPE_AIGUG) || 
    				serviceInfo.getRequestType().equals(ServiceDelegate.PREMIUM_REQUEST_TYPE_PMI)) {
    			String message = e.getMessage();
    			
    			if(serviceInfo.getLanguageId() == 0)
    			{
    				if(message != null && !message.equals(""))
    					serviceInfo.getDeal().setMIPremiumErrorMessage(Mc.MI_UG_PREMIUM_ERROR_EN + message);
    				else
    					serviceInfo.getDeal().setMIPremiumErrorMessage(Mc.MI_UG_PREMIUM_ERROR_EN + Mc.MI_UG_PREMIUM_ERROR_TIMEOUT_EN);
    			}
    			else
    			{
    				if(message != null && !message.equals(""))
    					serviceInfo.getDeal().setMIPremiumErrorMessage(Mc.MI_UG_PREMIUM_ERROR_FR + message);
    				else
    					serviceInfo.getDeal().setMIPremiumErrorMessage(Mc.MI_UG_PREMIUM_ERROR_FR + Mc.MI_UG_PREMIUM_ERROR_TIMEOUT_FR);
    			}
    		
    		responseProcessor.processResponse(null, serviceInfo, false);
    		}
    		else
    			if(serviceInfo.getRequestType().equals(ServiceDelegate.MI_REQUEST_TYPE_AIGUG) || 
        				serviceInfo.getRequestType().equals(ServiceDelegate.MI_REQUEST_TYPE_PMI)) 
    			{
		          if(DATX_ERROR.equals(e.getMessage()) || e instanceof java.net.SocketTimeoutException) 
		          {
		          		throw new ExternalServicesException(DATX_ERROR, e);
		          }
//    BETTER to distigusih between Real DATX Error and Connection Error
//    Currently Result Code 11 and 12 is included in Connection Error          
//          if(DATX_CONNECTION_ERROR.equals(e.getMessage()) || e instanceof java.net.SocketTimeoutException) {
//          throw new RequestProcessException(DATX_CONNECTION_ERROR, e);
//        }
//          else if (e.getMessage()!=null && e.getMessage().startsWith(DATX_ERROR))
//            {
//              throw new RequestProcessException(DATX_ERROR);
//            }
    				else
    					throw new ExternalServicesException(e);
    			}
    	}
    }

    public void setPayloadGenerator(IPayloadGenerator payloadGenerator) {
         this.payloadGenerator = payloadGenerator;
    }

    public void setChannel(IChannel channel) {
        this.channel = channel;

    }

    public void setResponseProcessor(IResponseProcessor responseProcessor) {
        this.responseProcessor = responseProcessor;
    } 
}
