package com.filogix.externallinks.services.responseprocessor;

import com.filogix.externallinks.services.ServiceInfo;

public interface IResponseProcessor {

	public abstract void processResponse(Object obj, ServiceInfo info, boolean validate) throws Exception;
}
