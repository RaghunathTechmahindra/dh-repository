package com.filogix.externallinks.services.channel.bnc;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
 
import MosSystem.Mc;

import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.GroupProfile;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.RegionProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.GroupProfilePK;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.RegionProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.auxdata.NBCAuxData;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.auxdata.ObjectFactory;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.auxdata.NBCAuxDataType.TmpSAMLParamsType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.auxdata.NBCAuxDataType.TmpWSAParamsType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.auxdata.NBCAuxDataType.TmpWSAParamsType.ReplyToType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.auxdata.NBCAuxDataType.TmpWSAParamsType.ReplyToType.ReferenceParametersType;


public class AuxDataHelper
{
    private SessionResourceKit srk;
    private Deal deal;
    public AuxDataHelper(SessionResourceKit sessionResourceKit, Deal deal)
    {
        this.srk = sessionResourceKit;
        this.deal = deal;
    }
    public String getAuxData() throws JAXBException, RemoteException, FinderException
    {
    	String logString = "AuxDataHelper.getAuxData() - ";
    	SysLogger logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
        OutputStream sf = new ByteArrayOutputStream();
        
        JAXBContext jc = JAXBContext.newInstance( Mc.JAXB_AUXDATA );
        ObjectFactory objFactory = new ObjectFactory();
        
        NBCAuxData auxDat =  (NBCAuxData)objFactory.createNBCAuxData();
       
        TmpSAMLParamsType tmpSMALLParams = objFactory.createNBCAuxDataTypeTmpSAMLParamsType();
        tmpSMALLParams = getDataForTmpSAMLParams(tmpSMALLParams);
        auxDat.setTmpSAMLParams(tmpSMALLParams);
     
        TmpWSAParamsType tmpWSAParams = objFactory.createNBCAuxDataTypeTmpWSAParamsType(); 
        tmpWSAParams = getDataForTmpWSAParams(objFactory, tmpWSAParams);
        auxDat.setTmpWSAParams(tmpWSAParams);
        
        Marshaller m = jc.createMarshaller();
        m.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
        m.marshal( auxDat, sf );
        
        return sf.toString();
    }
    private TmpWSAParamsType getDataForTmpWSAParams(ObjectFactory objFactory, TmpWSAParamsType tmpWSAParams) throws JAXBException, RemoteException, FinderException
    {
    	String logString = "AuxDataHelper.getDataForTmpWSAParams() - ";
    	SysLogger logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
        tmpWSAParams.setAction("");
        tmpWSAParams.setMessageID("");
        ReplyToType replyTo = objFactory
                .createNBCAuxDataTypeTmpWSAParamsTypeReplyToType();
        replyTo.setAddress("");

        ReferenceParametersType setReferenceParamenters = objFactory
                .createNBCAuxDataTypeTmpWSAParamsTypeReplyToTypeReferenceParametersType();
        
        
        
        UserProfile userProfile = new UserProfile(srk);
        userProfile = userProfile.findByPrimaryKey(new UserProfileBeanPK(srk.getExpressState().getUserProfileId(), 
                                                                         srk.getExpressState().getDealInstitutionId()));
        if(userProfile.getUserLanguageId() ==0)
        {
            setReferenceParamenters.setLanguageID("en");
        }
        else
        {
            setReferenceParamenters.setLanguageID("fr");
        }
        setReferenceParamenters.setClientID(userProfile.getUserLogin());
        replyTo.setReferenceParameters(setReferenceParamenters);
        tmpWSAParams.setReplyTo(replyTo);
        tmpWSAParams.setTo("");
        
        return tmpWSAParams;
    }
    private TmpSAMLParamsType getDataForTmpSAMLParams(TmpSAMLParamsType tmpSMALLParams) throws RemoteException, FinderException
    {

    	String logString = "AuxDataHelper.getDataForTmpSAMLParams() - ";
    	SysLogger logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
        // UserProfile
        UserProfile userProfile = new UserProfile(srk);
        userProfile = userProfile.findByPrimaryKey(new UserProfileBeanPK(srk.getExpressState().getUserProfileId(), 
                                                                         srk.getExpressState().getDealInstitutionId()));
        
        String userName = userProfile.getContact().getContactLastName() + " "
                + userProfile.getContact().getContactFirstName();
        String userID = userProfile.getUserLogin();

        tmpSMALLParams.setUserID(userID);
        tmpSMALLParams.setUserName(userName.trim());

        // Group
        GroupProfile groupProfile = new GroupProfile(srk);
        groupProfile = groupProfile.findByPrimaryKey(new GroupProfilePK(
                userProfile.getGroupProfileId()));

        tmpSMALLParams.setGroupName(groupProfile.getGroupName());

        // Branch
        int branchProfileId = deal.getBranchProfileId();
        BranchProfile branchProfile = new BranchProfile(srk);
        branchProfile = branchProfile.findByPrimaryKey(new BranchProfilePK(branchProfileId));

        tmpSMALLParams.setBranchName(branchProfile.getBranchName());

        // Region
        int regionProfileId = branchProfile.getRegionProfileId();
        RegionProfile regionProfile = new RegionProfile(srk);
        regionProfile = regionProfile.findByPrimaryKey(new RegionProfilePK(regionProfileId));

        tmpSMALLParams.setRegionName(regionProfile.getRegionName());

        // Instititution
        InstitutionProfile institutionProfile = new InstitutionProfile(srk);
        institutionProfile = institutionProfile
                .findByPrimaryKey(new InstitutionProfilePK(srk.getExpressState().getDealInstitutionId()));
        tmpSMALLParams.setInstitutionName(institutionProfile
                .getInstitutionName());

        return tmpSMALLParams;
    }
}
