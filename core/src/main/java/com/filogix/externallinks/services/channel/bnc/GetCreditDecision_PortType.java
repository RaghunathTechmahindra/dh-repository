/**
 * GetCreditDecision_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package com.filogix.externallinks.services.channel.bnc;

public interface GetCreditDecision_PortType extends java.rmi.Remote {
    public com.filogix.externallinks.services.channel.bnc.FXResponse requestCreditDecision(com.filogix.externallinks.services.channel.bnc.FXRequest request, java.lang.String payload) throws java.rmi.RemoteException;
}
