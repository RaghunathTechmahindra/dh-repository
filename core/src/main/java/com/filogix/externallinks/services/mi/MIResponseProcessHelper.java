package com.filogix.externallinks.services.mi;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.ProcessStatus;
import com.basis100.deal.entity.ServiceResponseQueue;
import com.basis100.deal.security.DLM;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressException;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.responseprocessor.ResponseOutdatedException;
import com.filogix.externallinks.services.responseprocessor.mi.AbstractMIResponseProcessor;
import com.filogix.externallinks.services.responseprocessor.mi.MIResponseProcessorFactory;

public abstract class MIResponseProcessHelper {

    private final static Logger logger = LoggerFactory.getLogger(MIResponseProcessHelper.class);

    public static void processPendingAsyncMIResponse(SessionResourceKit srk, Deal deal) throws ExpressException {

        try {
        	//don't do this for AIGUG for now
        	if(deal.getMortgageInsurerId() == Mc.MI_INSURER_AIGUG)return;
        	
            MIResponseProcessorFactory factry = MIResponseProcessorFactory.getInstance();
            AbstractMIResponseProcessor responseProcessor =
                factry.getAsyncMIResponseProcessor(deal.getMortgageInsurerId());

            ServiceResponseQueue srq = new ServiceResponseQueue(srk, null);
            List<ServiceResponseQueue> srqList = srq.findAvailableByDealIdAndServProdId(
                deal.getDealId(), responseProcessor.getServiceProductId());

            if (srqList == null || srqList.isEmpty()) return;

            // sort by ServiceResponseQueueId, descending order by
            // ServiceResponseQueueId => first record is the latest
            Collections.sort(srqList, new Comparator<ServiceResponseQueue>() {
                public int compare(ServiceResponseQueue o1, ServiceResponseQueue o2) {
                    return o2.getServiceResponseQueueId() - o1.getServiceResponseQueueId();
                }
            });

            // first record is to process
            srq = srqList.remove(0);

            // if old available records are found, change the state to expired
            for (ServiceResponseQueue oldRec : srqList) {
                oldRec.setProcessStatusId(ProcessStatus.RESPONSE_EXPIRED);
                oldRec.ejbStore();
            }
            srqList = null;

            ServiceInfo info = new ServiceInfo();
            info.setDeal(deal);
            info.setSrk(srk);
            info.setChannelTransactionKey(srq.getChannelTransactionKey());

            try {
                responseProcessor.processResponse(srq.getResponseXML(), info, false);
            } catch (ResponseOutdatedException e) {
                logger.warn(e.getMessage(), e);
                return;
            }

            srq.setProcessStatusId(ProcessStatus.PROCESSED);
            srq.ejbStore();

            // turn off the flag
            DLM dlm = DLM.getInstance();
            dlm.setMIResponseExpected(srk, deal.getDealId(), "N");

        } catch (Exception e) {
            logger.error("failed to process pending async mi response", e);
            throw new ExpressException(e);
        }

    }

}
