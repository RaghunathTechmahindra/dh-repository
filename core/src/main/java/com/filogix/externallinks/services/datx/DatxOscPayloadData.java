package com.filogix.externallinks.services.datx;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.xml.bind.JAXBException;
import MosSystem.Mc;

import com.basis100.deal.conditions.ConditionParser;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.PartyProfile;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PrimeIndexRateProfile;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Response;
import com.basis100.deal.entity.ServiceRequestContact;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbServicePayloadData;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.schema.datx.closing.AddressType;
import com.filogix.schema.datx.closing.BorrowerType;
import com.filogix.schema.datx.closing.ClosingContactInformationType;
import com.filogix.schema.datx.closing.ClosingServices;
import com.filogix.schema.datx.closing.ClosingServicesType;
import com.filogix.schema.datx.closing.ContactPointType;
import com.filogix.schema.datx.closing.ObjectFactory;


/**
 * @author CRutgaizer
 * @version 1
 * 
 */
public class DatxOscPayloadData extends JaxbServicePayloadData 
{
  protected ClosingServices                             oscPayload;
  protected ClosingContactInformationType               fpcContact;

  private com.filogix.schema.datx.closing.ObjectFactory factory;
  private double escrowPaymentPropTaxTotal = 0d;
  private double miFee = 0d;

  // leave this for backward compatibility with FPC
	public DatxOscPayloadData(SessionResourceKit srk, int requestId, int borrowerId, int copyId, int langId) 
          throws RemoteException, ExternalServicesException {
		super(srk, requestId, borrowerId, copyId, langId);
	}

	public DatxOscPayloadData(SessionResourceKit srk, int requestId, int copyId, int langId) 
         throws RemoteException, ExternalServicesException {
		super(srk, requestId, copyId, langId);
	}
		
	
	public void init() throws ExternalServicesException {
		factory = new ObjectFactory(); 

		try {

			super.init();
			oscPayload = factory.createClosingServices();			
	
			oscPayload.setTransactionType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "SERVICETRANSACTIONTYPE", 
                getRequest().getServiceTransactionTypeId(), ServiceConst.LANG_ENGLISH));
			oscPayload.setClosingProviderName
				(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "SERVICEPROVIDER", getServiceProviderId(), ServiceConst.LANG_ENGLISH));
			oscPayload.setClosingProductName(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "SERVICEPRODUCT", getServiceProductId(), ServiceConst.LANG_ENGLISH));
			oscPayload.setApplicationRefNo(_dealId + "");
			oscPayload.setTransactionRefNo(getRequestId() + "");                               // opt
			
	  // make sure if response has ChannelTransactionKey. - Midori
      Response resp = new Response(_srk);
      try {
        resp = resp.findLastResponseByRequestId(_requestId);
        if ( !empty( resp.getChannelTransactionKey()) )
        	oscPayload.setMiddlewareRefNo(resp.getChannelTransactionKey());                  // opt
      } catch (FinderException e) {
        _logger.trace("FinderException: Response does not exist for requestId = " + _requestId);
      }
      if (getServiceRequest() != null) {
        oscPayload.setProviderRefNo(getServiceRequest().getServiceProviderRefNo());      // opt
      }

      // borrowers
      
		Collection b = 	getBorrowerListOsc();
		if (b !=null && b.size() > 0)
			oscPayload.getBorrower().addAll(getBorrowerListOsc());			
      // -------------------------------------------- TODO: CONTINUE FROM HERE ---------------------
			oscPayload.setProperty(getPropertyOsc());
			
      // if FPC
      String reqTypeStr = _request.getRequestTypeShortName(_requestId, _copyId).trim();
      _logger.trace("DatxOscPayloadData@init(): RequestTypeShortName = " + reqTypeStr);
      if (reqTypeStr.equals(ServiceConst.REQUEST_TYPE_CLOSING_FIXED_REQUEST_STR)) {
        populateClosingContactInfo();
        if (!(fpcContact == null)) {
          oscPayload.setClosingContactInformation(fpcContact);                        // opt
        }
      }
      
			oscPayload.setRequester(getRequesterOsc());
			oscPayload.setSourceOfBusiness(getSobOsc());                                    // opt
			oscPayload.setLender(getLenderOsc());                                           // opt
      if (_deal.getBranchProfileId() != 0) {
        oscPayload.setLenderBranch(buildBranchType(_deal.getBranchProfileId()));   // opt
      }
			oscPayload.setCustomerBranch(getCustomerBranchOsc());                           // opt
			oscPayload.setMortgage(getMortgageOsc());                                       // opt
			oscPayload.setExistingMortgage(getExistingMortgageOsc());                       // opt
			
			// liabilties
			Collection l = getLiabilityListOsc();
			if (l != null && l.size() > 0 )
				oscPayload.getLiability().addAll(l);                        // opt

			Collection c = getConditionListOsc();
			if (c != null && c.size() > 0 )
			oscPayload.getCondition().addAll(c);                    // opt
			
			oscPayload.setSolicitor(getSolicitorOsc());                                   // opt
		
		} catch (JAXBException e) {
			_logger.debug("DatxOscPayloadData@init(): exception " + StringUtil.stack2string(e));
      throw new ExternalServicesException("Error for requestId = " + _requestId + ", copyId = " + _copyId);
		} catch (RemoteException e) {
      _logger.error("Error for requestId = " + _requestId + ", copyId = " + _copyId  + ": " + StringUtil.stack2string(e));
    } catch (FinderException e) {
      _logger.trace("FinderException: RequestTypeShortName doesn't exist for requestId = " + _requestId);
    }
		
	}

  private Collection getConditionListOsc() throws JAXBException {
    Collection result = new ArrayList();

    try {
      Collection docTrackList = new DocumentTracking(_srk).findForClosingServices((DealPK) _deal.getPk());

      if (!docTrackList.isEmpty()) {
        Iterator it = docTrackList.iterator();

        while (it.hasNext()) {
          com.filogix.schema.datx.closing.ConditionType cond = factory.createConditionType();

          DocumentTracking doc = (DocumentTracking) it.next();
          // FIXME: fix the schema and regenerate JAXB classes, then uncomment -> DONE Midori Aug 31, 2006
           cond.setOwner(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "CONDITIONRESPONSIBILITYROLE",
           doc.getDocumentResponsibilityRoleId(), ServiceConst.LANG_ENGLISH));

          String text = doc.getDocumentTextByLanguage(_langId);
          if (text == null)
            continue;
          cond.setText(text);

          result.add(cond);
        }
      }
    } catch (Exception e) {
      _logger.error("Error in DatxOscPayloadData@getConditionListOsc() for requestId = " + _requestId + ", copyId = " + _copyId + ": " + StringUtil.stack2string(e));
    }

    return result;
  }

  // TODO: Unit test this method
  private Collection getLiabilityListOsc() {
    Collection result = null;
    
    Collection col;
    try {
      col = _deal.getLiabilities();
      if ((col != null) && col.size() > 0) {
        result = new ArrayList();
        Iterator it = col.iterator();
        while (it.hasNext()) {
          Liability ll = (Liability) it.next();
          com.filogix.schema.datx.closing.LiabilityType liab = factory.createLiabilityType();
          
          liab.setType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "LIABILITYTYPE", ll.getLiabilityTypeId(), ServiceConst.LANG_ENGLISH));
          liab.setDescription(ll.getLiabilityDescription());
          liab.setAmount(ll.getLiabilityAmount());
          if (ll.getLiabilityPayOffTypeId() > 0) // LiablityPayOffType is optional in schema and id=0 in DB is empty
          {
        	  liab.setPayoffType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "LIABILITYPAYOFFTYPE", ll.getLiabilityPayOffTypeId(), ServiceConst.LANG_ENGLISH));
          }
          
          result.add(liab);
        }   
      }
    } catch (Exception e) {
      _logger.error("Error in DatxOscPayloadData@getLiabilityListOsc() for requestId = " + _requestId + ", copyId = " + _copyId + ": " + StringUtil.stack2string(e));
    }
    return result;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.ExistingMortgageType getExistingMortgageOsc() throws JAXBException {
    com.filogix.schema.datx.closing.ExistingMortgageType result = null;
    Collection col = null;
    try {
      col = _deal.getPartyProfiles();
    } catch (Exception e) {
      _logger.error("Error getting party profiles for requestId = " + _requestId + ", copyId = " + _copyId + ": " + StringUtil.stack2string(e));
    }
    if (col != null && col.size() > 0) 
    {
        Iterator it = col.iterator();
        while (it.hasNext())
        {
          PartyProfile party = (PartyProfile) it.next();
          if (party.getPartyTypeId() == Mc.PARTY_TYPE_TRANSFERRING_LENDER)
          {
            if (result == null)
              result = factory.createExistingMortgageType();
            com.filogix.schema.datx.closing.ExistingMortgageType.MortgageeType mgee = factory.createExistingMortgageTypeMortgageeType();
            result.setMortgagee(mgee);
            result.getMortgagee().setCompanyName(party.getPartyCompanyName());
            try 
            {
              if ((party.getContact() != null) )
              {
                if (party.getContact().getAddr() != null)
                {
                  result.getMortgagee().setAddress(buildAddressType(party.getContact().getAddr()));
                }
                Collection c = buildContactPoints(party.getContact());
                if ( c != null && c.size()>0)
                	result.getMortgagee().getContactPoint().addAll(c);
              }
            } 
            catch (FinderException e)
            {
              // no record
            } 
            catch (RemoteException e)
            {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }      
        }
    }

    // **** fixed for NBC183471  - May 4, 2007 START **
    if (_deal.getExistingLoanAmount() > 0 )
    {
        if (result == null)
            result = factory.createExistingMortgageType();
        // ******* changed base on email from Celeste Aug 31 ******** start
        //        result.setMortgageAmount(_deal.getRefiCurrentBal());
        result.setMortgageAmount(_deal.getExistingLoanAmount());
    }
    if (_deal.getRefExistingMtgNumber() != null && _deal.getRefExistingMtgNumber().trim().length()>0)
    {
        if (result == null)
            result = factory.createExistingMortgageType();
        // ******* changed base on email from Celeste Aug 31 ******** end
        result.setMortgageNumber(_deal.getRefExistingMtgNumber());
    }
    // **** fixed for NBC183471  - May 4, 2007 END **
    
    return result;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.MortgageType getMortgageOsc() throws JAXBException {
    com.filogix.schema.datx.closing.MortgageType result = factory.createMortgageType();
    try {
    
      result.setRefNo(_deal.getDealId() + "");
      result.setServicingRefNo(_deal.getServicingMortgageNumber());
      result.setMortgagePurpose(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "DEALPURPOSE", _deal.getDealPurposeId(), ServiceConst.LANG_ENGLISH));
      result.setMortgageType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "DEALTYPE", _deal.getDealTypeId(), ServiceConst.LANG_ENGLISH));
      if (_deal.getProductTypeId() > 0)
    	  result.setProductType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PRODUCTTYPE", _deal.getProductTypeId(), ServiceConst.LANG_ENGLISH));
      result.setInterestType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "INTERESTTYPE", _deal.getInterestTypeId(), ServiceConst.LANG_ENGLISH));
      result.setPaymentType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PAYMENTTERMTYPE", _deal.getPaymentTermTypeId(), ServiceConst.LANG_ENGLISH));
      result.setPrepaymentOptionType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PREPAYMENTOPTIONS", _deal.getPrePaymentOptionsId(),
          ServiceConst.LANG_ENGLISH));
      
      if (_deal.getLocRepaymentTypeId() > 0)
    	  result.setLOCRepaymentType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "LOCREPAYMENTTYPE", _deal.getLocRepaymentTypeId(), ServiceConst.LANG_ENGLISH));

	  result.setProductName(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "MTGPROD", _deal.getMtgProdId(), ServiceConst.LANG_ENGLISH));
      result.setLineOfBusiness(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "LINEOFBUSINESS", _deal.getLineOfBusinessId(), ServiceConst.LANG_ENGLISH));
      result.setLienPosition(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "LIENPOSITION", _deal.getLienPositionId(), ServiceConst.LANG_ENGLISH));
      result.setIADDate(dateToCalendar(_deal.getInterimInterestAdjustmentDate()));
      result.setInterestAdjustmentAmount(_deal.getInterimInterestAmount());

      result.setCommitmentExpirationDate(dateToCalendar(_deal.getCommitmentExpirationDate()));
      result.setClosingDate(dateToCalendar(_deal.getEstimatedClosingDate()));
      result.setMaturityDate(dateToCalendar(_deal.getMaturityDate()));
      result.setTermMonths(BigInteger.valueOf(_deal.getActualPaymentTerm()));

      // TODO: confirm at merging with 3.2 
      PricingProfile pp = null;
      int pricingProfileId = _deal.getActualPricingProfileId();
      if (pricingProfileId > 0)
      {
          pp = new PricingProfile(_srk, pricingProfileId);
          if (pp.getPricingRegistrationTerm() > 0)
          {
        	  int prt = new Double(pp.getPricingRegistrationTerm()).intValue();
              result.setRegistrationTermMonths(new BigInteger("" + prt));    	  
          }
      }
      result.setAmortizationMonths(BigInteger.valueOf(_deal.getAmortizationTerm()));      
      result.setTermTotalInterestAmount(_deal.getInterestOverTerm());
      result.setTeaserPeriodInterestSavingsAmount(_deal.getTeaserRateInterestSaving());
      result.setMaturityBalanceAmount(_deal.getBalanceRemainingAtEndOfTerm());
      result.setCashbackAmount(_deal.getCashBackAmount());
      result.setCashbackPercent(new BigDecimal("" + _deal.getCashBackPercent()));      
      result.setNetLoanAmount(_deal.getNetLoanAmount());
      result.setTotalLoanAmount(_deal.getTotalLoanAmount());
      
      Property prop = new Property(_srk);
      prop = prop.findByPrimaryProperty(_dealId, 
										_copyId,
										_srk.getExpressState().getDealInstitutionId());
      if (prop != null && prop.getTotalAnnualTaxAmount() > 0)
    	  result.setTotalAnnualTaxAmount(prop.getTotalAnnualTaxAmount());
      result.setTaxPayor(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "TAXPAYOR", _deal.getTaxPayorId(), ServiceConst.LANG_ENGLISH));
      // this field is nillble in DB, so if it's null then  Deal.getCombinedLTV reutrns 0.0 as double default.
      result.setLTVRatio(new BigDecimal(""+_deal.getCombinedLTV()));
      // this field is nillble in DB
      if ("Y".equals(_deal.getProgressAdvance()) || "N".equals(_deal.getProgressAdvance()))
    	  result.setMultipleAdvanceIndicator(_deal.getProgressAdvance());
      Collection escrowDetails = buildEscrowDetails();
      if (escrowDetails != null && escrowDetails.size() > 0)
    	  result.getEscrowDetails().addAll(escrowDetails); // note that this method will get a total for escrowPaymentPropTaxTotal (tax amount) 

      // payment details
      com.filogix.schema.datx.closing.MortgageType.PaymentDetailsType pmntDetails = factory.createMortgageTypePaymentDetailsType();
      
      pmntDetails.setPaymentFrequencyType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PAYMENTFREQUENCY", _deal.getPaymentFrequencyId(), ServiceConst.LANG_ENGLISH));
      pmntDetails.setFirstPaymentDate(dateToCalendar(_deal.getFirstPaymentDate()));
      pmntDetails.setPandIPaymentAmountMonthly(_deal.getPandIPaymentAmountMonthly());
      pmntDetails.setPandIPaymentAmount(_deal.getPandiPaymentAmount());
      pmntDetails.setTotalPaymentAmount(_deal.getTotalPaymentAmount());
      pmntDetails.setTaxAmount(this.escrowPaymentPropTaxTotal);
      
      result.setPaymentDetails(pmntDetails);
      
      // Interest rate details      
      com.filogix.schema.datx.closing.MortgageType.InterestRateDetailsType irDetails = factory.createMortgageTypeInterestRateDetailsType(); 
      irDetails.setCompoundingFrequency(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "INTERESTCOMPOUND", _deal.getInterestCompoundId(), ServiceConst.LANG_ENGLISH));

      // TODO: need to confirm at merge with 3.2
      if (pricingProfileId > 0 && pp!=null && pp.getInterestRateChangeFrequency() > 0)
      {
    	  irDetails.setInterestRateChangeFrequency(""+ pp.getInterestRateChangeFrequency());
      }
      irDetails.setPrimeIndexRate(new BigDecimal(""+_deal.getPrimeIndexRate()));
      
      result.setInterestRateDetails(irDetails);
      
      int primInd = _deal.getPrimeIndexId();
      if (primInd != 0) {
        PrimeIndexRateProfile prof = new PrimeIndexRateProfile(_srk);
        prof = prof.findByPrimeIndexRateInventory(primInd);
        if (prof != null) {
          result.getInterestRateDetails().setPrimeIndexDescription(prof.getPrimeIndexName());
        }
      }
      
      result.getInterestRateDetails().setPrimeBaseAdjustment(new BigDecimal(""+_deal.getPrimeBaseAdj()));
      result.getInterestRateDetails().setPostedRate(new BigDecimal(""+_deal.getPostedRate()));
      result.getInterestRateDetails().setPremium(new BigDecimal(""+_deal.getPremium()));
      result.getInterestRateDetails().setDiscount(new BigDecimal(""+_deal.getDiscount()));
      result.getInterestRateDetails().setNetInterestRate(new BigDecimal(""+_deal.getNetInterestRate()));
      result.getInterestRateDetails().setBuydownRate(new BigDecimal(""+_deal.getBuydownRate()));
      result.getInterestRateDetails().setTeaserDiscount(new BigDecimal(""+_deal.getTeaserDiscount()));
      result.getInterestRateDetails().setTeaserNetInterestRate(new BigDecimal(""+_deal.getTeaserNetInterestRate()));
      result.getInterestRateDetails().setTeaserMaturityDate(dateToCalendar(_deal.getTeaserMaturityDate()));
      result.getInterestRateDetails().setTeaserPAndIPaymentAmount(_deal.getTeaserPIAmount());
      result.getInterestRateDetails().setTeaserTerm(BigInteger.valueOf(_deal.getTeaserTerm()));
      Collection fd = buildFeeDetails();
      if (fd !=null && fd.size() > 0)
    	  result.getFeeDetails().addAll(fd);

      // mortgage insurance - optional. If _deal.getMortgageInsurerId() == 0, then no MI
      if (_deal.getMortgageInsurerId() > 0)
      {
	      com.filogix.schema.datx.closing.MortgageType.MortgageInsuranceType miType = factory.createMortgageTypeMortgageInsuranceType();
	      result.setMortgageInsurance(miType);
	      result.getMortgageInsurance().setMIProvider(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "MORTGAGEINSURER", _deal.getMortgageInsurerId(), ServiceConst.LANG_ENGLISH));
	      result.getMortgageInsurance().setMIRefNo(_deal.getMIPolicyNumber());
	      result.getMortgageInsurance().setMIPremiumAmount(_deal.getMIPremiumAmount());
	      result.getMortgageInsurance().setMIPSTAmount(_deal.getMIPremiumPST());
	      result.getMortgageInsurance().setMIFee(this.miFee); // note that miFee will be calculated in on buildFeeDetails() method
	      if (!empty(_deal.getMIUpfront()))
	    	  result.getMortgageInsurance().setCapitalizedIndicator(("Y".equals(_deal.getMIUpfront().trim()) ? ServiceConst.CONST_N : ServiceConst.CONST_Y));
	      else 
	    	  result.getMortgageInsurance().setCapitalizedIndicator(ServiceConst.CONST_Y);
      }
    } catch (Exception e) {
      _logger.debug("Cannot create MortgageType for dealId = " + _dealId + ", requestId = " + _requestId + ", copyId = "
          + _copyId + ": \n" + StringUtil.stack2string(e));
    }
      
    return result;
  }
  
  private Collection buildFeeDetails() throws JAXBException {
    Collection result = null;
    
    Collection col;
    try {
      col = _deal.getDealFees();
    } catch (Exception e) {
      return result;
    }
    
    if (col == null) {
      return result;
    }
    result = new ArrayList();
    
    Iterator it = col.iterator();
    while (it.hasNext()) {
      DealFee  df = (DealFee ) it.next();
      if ((df.getFeeId() == Mc.FEE_TYPE_CMHC_FEE)|| (df.getFeeId() == Mc.FEE_TYPE_GE_CAPITAL_FEE))
	        this.miFee  = df.getFeeAmount();
      if (!df.getPayableIndicator()) // changed based on updating Sep 15, 2006
      {
	      com.filogix.schema.datx.closing.MortgageType.FeeDetailsType feeDet = factory.createMortgageTypeFeeDetailsType();
	      
	      feeDet.setType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "FEETYPE", df.getFeeId(), ServiceConst.LANG_ENGLISH));
	      feeDet.setAmount(df.getFeeAmount());
	      feeDet.setStatus(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "FEESTATUS", df.getFeeStatusId(), ServiceConst.LANG_ENGLISH));
	      result.add(feeDet);
	   }
    }
    return result;
  }

  protected Collection buildEscrowDetails() throws Exception
  {
    
    Collection col = null;
    try{
       col =  _deal.getEscrowPayments();
    }catch(Exception exc){
      return null;
    }
    if(col == null || col.isEmpty()){
      return null;
    }

    Collection result = new ArrayList();
    
    Iterator it = col.iterator();
    while( it.hasNext() ){
      com.filogix.schema.datx.closing.MortgageType.EscrowDetailsType esd = factory.createMortgageTypeEscrowDetailsType();
      EscrowPayment  ep = (EscrowPayment) it.next();
      
      esd.setType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "ESCROWTYPE", ep.getEscrowTypeId(), ServiceConst.LANG_ENGLISH));
      esd.setAmount(ep.getEscrowPaymentAmount());
      
      result.add(esd);
      
      if(ep.getEscrowTypeId() == 0){
        escrowPaymentPropTaxTotal += ep.getEscrowPaymentAmount();
      }
    }
    return result;
  }

  

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.SolicitorType getSolicitorOsc() {
    com.filogix.schema.datx.closing.SolicitorType result = null;
    
    try {
      Collection col = _deal.getPartyProfiles(); 
      if ((col != null) && col.size() > 0) {
        Iterator it = col.iterator();
        while (it.hasNext()) {
          PartyProfile pp = (PartyProfile) it.next();
          
          if (pp.getPartyTypeId() == MosSystem.Mc.PARTY_TYPE_SOLICITOR) {
              result = factory.createSolicitorType();
            
              result.setSolicitorFirmID(pp.getPtPBusinessId());
//          result.setSolicitorFirmID("E0000002"); //-- test data
            result.setSolicitorFirmName(pp.getPartyCompanyName());
      	    result.setSolicitorSpecialInstructions(_deal.getSolicitorSpecialInstructions());
            try
            {
	            if (pp.getContact() != null) {
	              result.setAddress(buildAddressType(pp.getContact().getAddr())); // contact.addrId is not nullble
	              result.setFirstName(pp.getContact().getContactFirstName());
	              result.setLastName(pp.getContact().getContactLastName());
	              Collection cp = buildContactPoints(pp.getContact());
	              if (cp != null && cp.size() > 0)
	            	  result.getContactPoint().addAll(cp);
		            }
		    }
		    catch(FinderException fe)
		    {
		    	// no contact record was found
		    }
            break;
          }
        }
      }
      
      if (result == null && _deal.getSolicitorSpecialInstructions()!= null)
      {
    	  result = factory.createSolicitorType();
    	  result.setSolicitorSpecialInstructions(_deal.getSolicitorSpecialInstructions());
      }
    } 
    catch (Exception e) {
      _logger.debug("Cannot create SolicitorType for dealId = " + _dealId + ", requestId = " + _requestId + ", copyId = "
          + _copyId + ": \n"+ StringUtil.stack2string(e));
    }

    return result;
  }

  // TODO: Unit test this method
  /**
   * this method
   */
  private com.filogix.schema.datx.closing.PropertyType getPropertyOsc() throws JAXBException 
  {
    com.filogix.schema.datx.closing.PropertyType result = factory.createProperty();
    
    Collection col;
    try {
      col = _deal.getProperties();
      if ((col != null) && (col.size() > 0)) {
        Iterator it = col.iterator();
        while (it.hasNext()) {
          Property prop = (Property) it.next();
          if ((prop.getPrimaryPropertyFlag() + "").equalsIgnoreCase("Y")) {
            int typeId = prop.getPropertyTypeId();
            result.setPropertyType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PROPERTYTYPE", prop.getPropertyTypeId(), ServiceConst.LANG_ENGLISH));
            result.setPropertyUsage(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PROPERTYUSAGE", prop.getPropertyUsageId(), ServiceConst.LANG_ENGLISH));
            result.setDwellingStyle(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "DWELLINGSTYLE", prop.getDwellingStyleId(), ServiceConst.LANG_ENGLISH));
            result.setDwellingType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "DWELLINGTYPE", prop.getDwellingTypeId(), ServiceConst.LANG_ENGLISH));
            result.setOccupancyType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "OCCUPANCYTYPE", prop.getOccupancyTypeId(), ServiceConst.LANG_ENGLISH));
            // fixed Midori Sep 5, 2006            
            result.setCondominiumIndicator( prop.getTenureTypeId() == ServiceConst.PROPERTY_TENURE_TYPE_CONDO  ? 
            		ServiceConst.CONST_Y : ServiceConst.CONST_N);
            // TODO: confirm
            result.setNewConstructionIndicator((prop.getNewConstructionId() == 0) ? ServiceConst.CONST_N : ServiceConst.CONST_Y); // This field is nillble in DB, should 0 be set ?
            
            result.setNumberOfUnits(BigInteger.valueOf(prop.getNumberOfUnits())); // This field is nillble in DB, should 0 be set ?
            result.setPurchasePrice(prop.getPurchasePrice()); // This field is nillble in DB, should 0 be set ?
            result.setEstimatedAppraisalValue(prop.getEstimatedAppraisalValue()); // This field is nillble in DB, should 0 be set ?
            result.setActualAppraisalValue(prop.getActualAppraisalValue()); // This field is nillble in DB, should 0 be set ?
            result.setAppraisalDate(dateToCalendar(prop.getAppraisalDateAct()));
            result.setAnnualTaxAmount(prop.getTotalAnnualTaxAmount()); // This field is nillble in DB, should 0 be set ?
            result.setLegalDescription(prop.getLegalLine1() + " " + prop.getLegalLine2() + " " + prop.getLegalLine3());
            result.setPropertyAddress(buildAddressType(prop));
          }
        }
      }
      else // property/address is mandatory. need to be set empty address
    	  result.setPropertyAddress (factory.createAddressType());
    } catch (Exception e) {
      _logger.debug("Cannot create PropertyType for propertyId = " + _propertyId + ", requestId = " + _requestId + ", copyId = "
          + _copyId + ": \n"+ StringUtil.stack2string(e));
    }
    
    return result;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.BranchType getCustomerBranchOsc() {
    com.filogix.schema.datx.closing.BranchType result = null;
    
    try {
      Collection col = _deal.getPartyProfiles(); 
      if ((col != null) && col.size() > 0) {
        Iterator it = col.iterator();
        while (it.hasNext()) {
          PartyProfile pp = (PartyProfile) it.next();
          
          if (pp.getPartyTypeId() == MosSystem.Mc.PARTY_TYPE_SERVICE_BRANCH) {
            result = factory.createBranchType();
            result.setBranchID(pp.getPartyProfileId() + "");
            result.setTransitNumber(pp.getPtPBusinessId()); 
//            result.setTransitNumber("01881");  //-- test data
            try
            {
                if (pp.getContact() != null) {
            		result.setAddress(buildAddressType(pp.getContact().getAddr()));
                  result.getBranchContactDetails().add(buildBranchContactDetails(pp.getContact()));
                }            	
            }
            catch(FinderException fe)
            {
            	// no contact record was found
            }
            break;
          }  
        }
        
      }
    } catch (Exception e) {
      _logger.debug("Cannot create CustomeBranchType for dealId = " + _dealId + ", requestId = " + _requestId + ", copyId = "
          + _copyId + ": \n"+ StringUtil.stack2string(e));
    }
    
    return result;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.BranchType buildBranchType(int bpId) throws JAXBException {
    com.filogix.schema.datx.closing.BranchType result = null;
    if (bpId == 0) {
      return result;
    }
    try {
      BranchProfile bp = new BranchProfile(_srk).findByPrimaryKey(new BranchProfilePK(bpId));

      result = factory.createBranchType();
      result.setBranchID(bp.getBranchProfileId()+"");
      result.setTransitNumber(bp.getBPBusinessId()); 
//      result.setTransitNumber("01881"); //-- test data
      try
      {
          if (bp.getContact() != null) {
              result.setAddress(buildAddressType(bp.getContact().getAddr()));
              result.getBranchContactDetails().add(buildBranchContactDetails(bp.getContact()));
            }    	  
      }
      catch(FinderException fe)
      {
    	  // no contact record was found
      }
    } catch (Exception e) {
      _logger.debug("Cannot create BranchType for dealId = " + _dealId + ", requestId = " + _requestId + ", copyId = "
          + _copyId);
    }
    return result;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.BranchType.BranchContactDetailsType buildBranchContactDetails(Contact cont) 
  throws JAXBException {
    com.filogix.schema.datx.closing.BranchType.BranchContactDetailsType lContDet = factory.createBranchTypeBranchContactDetailsType();
    
    lContDet.setFirstName(cont.getContactFirstName());
    lContDet.setLastName(cont.getContactLastName());
    lContDet.setJobTitle(cont.getContactJobTitle());
    Collection cp = buildContactPoints(cont);
    	if (cp != null && cp.size() > 0)
    lContDet.getContactPoint().addAll(cp);
    return lContDet;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.LenderType.LenderContactDetailsType buildLenderContactDetails(Contact cont) 
  throws JAXBException {
    com.filogix.schema.datx.closing.LenderType.LenderContactDetailsType lContDet 
       = factory.createLenderTypeLenderContactDetailsType();
    lContDet.setFirstName(cont.getContactFirstName());
    lContDet.setLastName(cont.getContactLastName());
    lContDet.setJobTitle(cont.getContactJobTitle());
    Collection cp = buildContactPoints(cont);
	if (cp != null && cp.size() > 0)
		lContDet.getContactPoint().addAll(cp);
    return lContDet;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.LenderType getLenderOsc() throws JAXBException {
    com.filogix.schema.datx.closing.LenderType result = null; 

    try {
      LenderProfile lp = _deal.getLenderProfile();

      if (lp != null) {

        result = factory.createLenderType();
        result.setLenderID(lp.getLPBusinessId()); // this is nillble string
        result.setLenderName(lp.getLenderName());
        try {
        	if (lp.getContact() != null)
        		result.setAddress(buildAddressType(lp.getContact().getAddr()));
        } catch (Exception e) {
          // skip the address
        }

        // Administrator
        if (_deal.getAdministratorId() != 0) {
          UserProfile up = new UserProfile(_srk)
                   .findByPrimaryKey(new UserProfileBeanPK(_deal.getAdministratorId(), _deal.getInstitutionProfileId()));
          if (up != null) {
            Contact cont = up.getContact();
            if (cont != null) {
              result.getLenderContactDetails().add(buildLenderContactDetails(cont));
            }
          }
        }

        // Funder
        if (_deal.getFunderProfileId() != 0) {
          UserProfile up = new UserProfile(_srk)
                .findByPrimaryKey(new UserProfileBeanPK(_deal.getFunderProfileId(), 
                                                        _deal.getInstitutionProfileId()));
          if (up != null) {
            Contact cont = up.getContact();
            if (cont != null) {
              result.getLenderContactDetails().add(buildLenderContactDetails(cont));
            }
          }
        }

        // Underwriter
        if (_deal.getUnderwriterUserId() != 0) {
          UserProfile up = new UserProfile(_srk)
                    .findByPrimaryKey(new UserProfileBeanPK(_deal.getUnderwriterUserId(), 
                                                            _deal.getInstitutionProfileId()));
          if (up != null) {
            Contact cont = up.getContact();
            if (cont != null) {
              result.getLenderContactDetails().add(buildLenderContactDetails(cont));
            }
          }
        }
      }
    } catch (FinderException e) {
      _logger.trace("LenderProfile does not exist for dealId = " + _dealId);
    } catch (Exception e) {
      _logger.trace("Error generating Lender type for requestId = " + _requestId + ", copyId = " + _copyId);
    }
    return result;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.SourceOfBusinessType getSobOsc() throws JAXBException {
    com.filogix.schema.datx.closing.SourceOfBusinessType result = null; 

    try {
    SourceOfBusinessProfile sob = _deal.getSourceOfBusinessProfile();
    if (sob != null) {
    	result = factory.createSourceOfBusinessType();  
		result.setOriginationSystemRefNo(_deal.getSourceApplicationId());
	    result.setType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "SOURCEOFBUSINESSCATEGORY", sob.getSourceOfBusinessCategoryId(), ServiceConst.LANG_ENGLISH));

	    // firm is mandatory for SourceOfBusiness
	    com.filogix.schema.datx.closing.SourceOfBusinessType.FirmType firm = factory.createSourceOfBusinessTypeFirmType();
	    result.setFirm(firm);
        
        firm.setFirmID(sob.getSOBPBusinessId());
        if (_deal.getSourceFirmProfile() != null
        		&& !empty(_deal.getSourceFirmProfile().getSourceFirmName()))
        	firm.setFirmName(_deal.getSourceFirmProfile().getSourceFirmName());

	    // agent is mandatory for SourceOfBusiness
		com.filogix.schema.datx.closing.SourceOfBusinessType.AgentType ag = factory.createSourceOfBusinessTypeAgentType();
		result.setAgent(ag);

        try
        {
			if (sob.getContact()!=null) 
	        {
	        	result.getFirm().setAddress(buildAddressType(sob.getContact().getAddr()));
	        	if (!empty(sob.getContact().getContactFirstName()))
	        		ag.setFirstName(sob.getContact().getContactFirstName());
	        	if (!empty(sob.getContact().getContactLastName()))
	        		ag.setLastName(sob.getContact().getContactLastName());
	            Collection cp = buildContactPoints(sob.getContact());
	        	if (cp != null && cp.size() > 0)
	        		ag.getContactPoint().addAll(cp);
			}
        }
        catch(FinderException fe)
        {
        	// no countact record was found
        }
      }
    } catch (FinderException e) {
      _logger.trace("SourceOfBusinessProfile does not exist for dealId = " + _dealId);
    } catch (RemoteException e) {
      _logger.error("Error instantiating com.filogix.schema.datx.closing.RequesterType: " + StringUtil.stack2string(e));
    }

    return result;
  }

  // TODO: Unit test this method
  private com.filogix.schema.datx.closing.RequesterType getRequesterOsc() throws JAXBException {
	  // requester is mandatory
    com.filogix.schema.datx.closing.RequesterType result = factory.createRequester();
    
    try {
      Contact cont = new Contact(_srk).findByUserProfileId(_request.getUserProfileId());
      
      if (cont != null) {
        result.setFirstName(cont.getContactFirstName());
        result.setLastName(cont.getContactLastName());
        Collection cp = buildContactPoints(cont);
    	if (cp != null && cp.size() > 0)
    		result.getContactPoint().addAll(cp);
      }
      else // requester.FirstName and requester.LastNameis mandatory
      {	
          result.setFirstName("");
          result.setLastName("");
    	  
      }
    } catch (Exception e) {
      _logger.error("Error instantiating com.filogix.schema.datx.closing.RequesterType: " + StringUtil.stack2string(e));
    }
    
    return result;
  }

  // TODO: Unit test this method
  private Collection getBorrowerListOsc() throws ExternalServicesException {
    Collection result = new ArrayList();
    BorrowerType jaxbBor;
    
    try {
      Collection borList = _deal.getBorrowers();
      Iterator it = borList.iterator();
      while (it.hasNext()) {
        Borrower bor = (Borrower) it.next();
        jaxbBor = factory.createBorrowerType();
        
        String sInd = ServiceConst.BORROWER_INDICATOR_OTHER;
        String ind = bor.getPrimaryBorrowerFlag();
        
        if (ind.trim().equalsIgnoreCase("Y"))  {            
          sInd = ServiceConst.BORROWER_INDICATOR_PRIMARY;
        }  else { 
          if (ind.trim().equalsIgnoreCase("C")){
            sInd = ServiceConst.BORROWER_INDICATOR_COAPPLICANT;
          }
        }  
        jaxbBor.setPrimaryBorrowerIndicator(sInd);
        
        jaxbBor.setBorrowerType(ServiceConst.BORROWER_TYPE_BORROWER);
        if (bor.getBorrowerTypeId() != 0) {
          jaxbBor.setBorrowerType(ServiceConst.BORROWER_TYPE_GUARANTOR);
        } 
          
        jaxbBor.setStaffOfLenderIndicator(ServiceConst.CONST_N);                                                   //opt
        if ((bor.getStaffOfLender() + "").equalsIgnoreCase("Y")) {  
          jaxbBor.setStaffOfLenderIndicator(ServiceConst.CONST_Y);
        }
        if (bor.getSalutationId()>0)
        	jaxbBor.setSalutation(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "SALUTATION", bor.getSalutationId(), ServiceConst.LANG_ENGLISH));  // opt
        jaxbBor.setFirstName(bor.getBorrowerFirstName());
        if (empty(bor.getBorrowerFirstName())) {
          jaxbBor.setFirstName(ServiceConst.CONST_UNKNOWN);
        }
        if (!empty(bor.getBorrowerMiddleInitial())){
          jaxbBor.setMiddleName(bor.getBorrowerMiddleInitial());                                                  // opt
        }
        
        jaxbBor.setLastName(bor.getBorrowerLastName());
        if (empty(bor.getBorrowerLastName())) {
          jaxbBor.setLastName(ServiceConst.CONST_UNKNOWN);
        }
        jaxbBor.setMaritalStatusType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "MARITALSTATUS", bor.getMaritalStatusId(), ServiceConst.LANG_ENGLISH));
        
        if (bor.getBorrowerBirthDate() != null) {
          jaxbBor.setBirthDate(dateToCalendar(bor.getBorrowerBirthDate()));  
        }
        
        try
        {
            EmploymentHistory empl = new EmploymentHistory(_srk);
            empl = empl.findByCurrentEmployment((BorrowerPK)bor.getPk());
            if (empl != null) {
              if (empl.getJobTitleId() == 0) {
                jaxbBor.setJobTitle(empl.getJobTitle());
              } else {
                jaxbBor.setJobTitle(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "JOBTITLE", empl.getJobTitleId(), ServiceConst.LANG_ENGLISH));    // opt
              }
            }
        }
        catch(FinderException fe)
        {
            //do nothing - just ignore
        }
        jaxbBor.setLanguagePreference(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "LANGUAGEPREFERENCE", bor.getLanguagePreferenceId(), ServiceConst.LANG_ENGLISH));
        Collection bal = buildBorrowerAddressList(bor);
        if (bal !=null && bal.size() > 0)
        	jaxbBor.getAddress().addAll(bal);
        Collection cp = buildContactPoints(bor);
        if (cp !=null && cp.size() > 0)
        	jaxbBor.getContactPoint().addAll(cp);                                            // opt
        
        result.add(jaxbBor);
        
      } // end while
        
    } catch (JAXBException e1) {
      _logger.error("Error instantiating com.filogix.schema.datx.closing.BorrowerType: " + StringUtil.stack2string(e1));
    } catch (Exception e) {
      _logger.error("Error building com.filogix.schema.datx.closing.BorrowerType: " + StringUtil.stack2string(e));
    }
    
    return result;
  }

  /**
   * XML requires Calendar instead of Date
   * @param date
   * @return
   * @throws DatatypeConfigurationException
   */
  private Calendar dateToCalendar(java.util.Date date)  {
	  if (date == null) return null;

    Calendar resultDate = new GregorianCalendar();
    resultDate.setTime(date);
    return resultDate;
  }
  
  /**
   * Applies to Fixed Price transactions only. This is the person who shall be contacted for the request.  
   * Though each phone number element is optional, at least one should be provided. 
	 * @throws ExternalServicesException 
	 * @throws JAXBException 
	 */
  private void populateClosingContactInfo() throws JAXBException {
    // fpcContact = new ClosingContactInformationTypeImpl();
    fpcContact = factory.createClosingContactInformationType();

    try {
      getServiceRequestContact();

      
      // if usePrimaryBorrower check box is checked:
      if (_serviceContact.getBorrowerId() > 0) {
        int borroweId = _serviceContact.getBorrowerId();
        Borrower bor = new Borrower(_srk, null);
        bor = bor.findByPrimaryKey(new BorrowerPK(borroweId, _copyId));
        if (bor == null) {
          throw new FinderException("DatxOscPayloadData@populateClosingContactInfo(): cannot find borrower = " + borroweId + "  for ServiceContact, requestId = " + _requestId);
        }
        
        if (!(bor.getBorrowerFirstName().trim().equals(""))) {
          fpcContact.setFirstName(bor.getBorrowerFirstName());
        } else {
          fpcContact.setFirstName(ServiceConst.CONST_UNKNOWN);
        }
        
        if (!(bor.getBorrowerLastName().trim().equals(""))) {
          fpcContact.setLastName(bor.getBorrowerLastName());
        } else {
          fpcContact.setLastName(ServiceConst.CONST_UNKNOWN);
        }
        
        Collection col = buildContactPoints(bor);
        if ((col != null) && (col.size() > 0) ) {
          fpcContact.getContactPoint().addAll(col);
        }
      } 
      //  if usePrimaryBorrower check box is NOT checked:
      else 
      {
        if (!(_serviceContact.getFirstName().trim().equals(""))) {
          fpcContact.setFirstName(_serviceContact.getFirstName());
        } else {
          // although this should never happen because should be prevented by the BR on the screen
          fpcContact.setFirstName(ServiceConst.CONST_UNKNOWN); 
        }
        
        if (!(_serviceContact.getLastName().trim().equals(""))) {
          fpcContact.setLastName(_serviceContact.getLastName());
        } else { 
          // although this should never happen because should be prevented by the BR on the screen
          fpcContact.setLastName(ServiceConst.CONST_UNKNOWN); 
        }

        Collection col = buildContactPoints(_serviceContact);
        if ((col != null) && (col.size() > 0)) {
          fpcContact.getContactPoint().addAll(col);
        }  
      }

      if ((_serviceContact.getComments() != null) && !(_serviceContact.getComments().trim().equals(""))) {
        fpcContact.setSpecialInstructions(_serviceContact.getComments());
      }

    } catch (Exception e) {
      _logger.error("Error in populateClosingContactInfo() " + StringUtil.stack2string(e));
    }
  }

  // ===================================================================
  // ----------------------------------- Methods for Contact Point Type
  // ===================================================================
  /**
   * Create contact point element from Borrower 
   * @param com.basis100.deal.entity.Borrower borrower
   * @return
   * @throws JAXBException 
   */
  private Collection buildContactPoints(Borrower borrower) throws JAXBException {
    ArrayList result = new ArrayList();
    ContactPointType contactPoint;
    boolean isPhone, isExt, isEmail, isFax;
    
    // Home
    String homePhone = scrabPhoneNumber(borrower.getBorrowerHomePhoneNumber());
    String homeFax = scrabPhoneNumber(borrower.getBorrowerFaxNumber());
    
    isPhone = homePhone != null;
    isFax = homeFax != null;
    isEmail = (borrower.getBorrowerEmailAddress() != null) && !(borrower.getBorrowerEmailAddress().trim().equals(""));
    
    if (isPhone || isFax || isEmail) {
      
      // contactPoint = new ContactPointTypeImpl();
      contactPoint = factory.createContactPointType();
      
      contactPoint.setType(ServiceConst.OSC_CONTACT_POINT_TYPE_HOME);

      if (isPhone) {
        contactPoint.setPhoneNumber(homePhone);
      }
      if (isFax) {
        contactPoint.setFaxNumber(homeFax);
      }
      if (isEmail) {
        contactPoint.setEmail(borrower.getBorrowerEmailAddress().trim());
      }
      result.add(contactPoint);
    }
    
    // Work
    String workPhone = scrabPhoneNumber(borrower.getBorrowerWorkPhoneNumber());
    String workExt = borrower.getBorrowerWorkPhoneExtension();
    isPhone = workPhone != null;
    isExt = isDigits(workExt);
      
    if (isPhone) 
    {
      // contactPoint = new ContactPointTypeImpl();
      contactPoint = factory.createContactPointType();
      contactPoint.setType(ServiceConst.OSC_CONTACT_POINT_TYPE_WORK);      
      contactPoint.setPhoneNumber(workPhone);
      
      if (isExt)
    	  contactPoint.setPhoneExtension(workExt.trim());
      
      result.add(contactPoint);
      
    }    
    return result;
  }

  /**
   * Create contact point element from ServiceRequestContact  
   * @param com.basis100.deal.entity.ServiceRequest srvcContact
   * @return
   * @throws JAXBException 
   */
  private Collection buildContactPoints(ServiceRequestContact srvcContact) throws JAXBException {
    ArrayList result = new ArrayList();
    ContactPointType contactPoint;
    boolean isArea, isPhone, isExt, isEmail;
    
    // Home
    isArea  = isDigits(srvcContact.getHomeAreaCode()) &&  srvcContact.getHomeAreaCode().trim().length()==3;
    isPhone  = isDigits(srvcContact.getHomePhoneNumber()) &&  srvcContact.getHomePhoneNumber().trim().length()==7;

    isEmail = (srvcContact.getEmailAddress() != null) && !(srvcContact.getEmailAddress().trim().equals(""));
    
    if (isPhone || isEmail)
    {

    // contactPoint = new ContactPointTypeImpl();
      contactPoint = factory.createContactPointType();

      contactPoint.setType(ServiceConst.OSC_CONTACT_POINT_TYPE_HOME);

		if (isPhone) {
			String phoneNumber = null;
			if  (isArea) 
				phoneNumber = scrabPhoneNumber(srvcContact.getHomeAreaCode().trim() + srvcContact.getHomePhoneNumber().trim());
			else
				phoneNumber = scrabPhoneNumber(srvcContact.getHomePhoneNumber().trim());
			contactPoint.setPhoneNumber(phoneNumber);
		
      }
      if (isEmail) {
        contactPoint.setEmail(srvcContact.getEmailAddress());
      }
      result.add(contactPoint);
    }
    // Work
    isArea  = isDigits(srvcContact.getWorkAreaCode()) && (srvcContact.getWorkAreaCode().trim().length()==3); 
    isPhone = isDigits(srvcContact.getWorkPhoneNumber()) && (srvcContact.getWorkPhoneNumber().trim().length()==7);
    isExt   = isDigits(srvcContact.getWorkExtension());
      
    if (isPhone) {

      // contactPoint = new ContactPointTypeImpl();
      contactPoint = factory.createContactPointType();
      contactPoint.setType(ServiceConst.OSC_CONTACT_POINT_TYPE_WORK);
	  	String phoneNumber = null;
		if  (isArea) 
			phoneNumber = scrabPhoneNumber(srvcContact.getWorkAreaCode().trim() + srvcContact.getWorkPhoneNumber().trim());
		else
			phoneNumber = scrabPhoneNumber(srvcContact.getWorkPhoneNumber());
      
		contactPoint.setPhoneNumber(phoneNumber);
      
        if (isExt) {
            contactPoint.setPhoneExtension(srvcContact.getWorkExtension().trim());
        }
        result.add(contactPoint);      
    }    
    
    // Mobile
      isArea  = isDigits(srvcContact.getCellAreaCode()) && (srvcContact.getCellAreaCode().trim().length()==3); 
      isPhone = isDigits(srvcContact.getCellPhoneNumber()) && (srvcContact.getCellPhoneNumber().trim().length()==7);
    
    if (isPhone) {
      
      // contactPoint = new ContactPointTypeImpl();
      contactPoint = factory.createContactPointType();

      contactPoint.setType(ServiceConst.OSC_CONTACT_POINT_TYPE_MOBILE);
	  	String phoneNumber = null;
		if  (isArea) 
			phoneNumber = scrabPhoneNumber(srvcContact.getCellAreaCode().trim() + srvcContact.getCellPhoneNumber().trim());
		else
			phoneNumber = scrabPhoneNumber(srvcContact.getCellPhoneNumber().trim());

        contactPoint.setPhoneNumber(phoneNumber);
      result.add(contactPoint);
    }
    
    return result;
  }

  /**
   * Create contact point element from Contact 
   * @param com.basis100.deal.entity.Contact contact
   * @return
   * @throws JAXBException 
   */
  private Collection buildContactPoints(Contact contact) throws JAXBException {
    ArrayList result = new ArrayList();
    ContactPointType contactPoint;
    boolean isPhone, isExt, isEmail, isFax;
    
    // Work
    String workPhone = scrabPhoneNumber(contact.getContactPhoneNumber());
    String workExt = contact.getContactPhoneNumberExtension();
    String workFax = scrabPhoneNumber(contact.getContactFaxNumber());
    isPhone = workPhone != null;
    isExt   = isDigits(workExt);
    isFax   = workFax != null;
    isEmail = (contact.getContactEmailAddress() != null) && !(contact.getContactEmailAddress().trim().equals(""));
      
    if (isPhone || isFax || isEmail) {

      // contactPoint = new ContactPointTypeImpl();
      contactPoint = factory.createContactPointType();

      contactPoint.setType(ServiceConst.OSC_CONTACT_POINT_TYPE_WORK);
      
      if (isPhone) {
        contactPoint.setPhoneNumber(workPhone);
        if (isExt) {
            contactPoint.setPhoneExtension(workExt.trim());
          }
      }
      if (isFax) {
        contactPoint.setFaxNumber(workFax);
      }
      if (isEmail) {
        contactPoint.setEmail(contact.getContactEmailAddress());
      }

      result.add(contactPoint);
      
    }    
    return result;
  }

  // check if phone number is 10 digits
  // if it's 7 digits, then add "000" at the begining
  // if it inlcudes non digits, then return null;
  public String scrabPhoneNumber(String number)
  {
	  if (number == null) return null;
	  String trimedNumber = number.trim();
	  if (!isDigits(number)) return null;
	  if (trimedNumber.length() < 7) return null;
	  if (trimedNumber.length() == 7) return "000" + trimedNumber;
	  if (trimedNumber.length() == 10) return trimedNumber;
	  return null;
  }
  
  public boolean isDigits(String numbers)
  {
	  if (numbers == null || numbers.trim().length()==0) return false;
	  for (int i=0; i<numbers.length(); i++)
	  {
		  if (!Character.isDigit(numbers.charAt(i))) 
			  return false; // phone number includes non digit char
	  }
	  return true;
  }
  
  // =================================================================
  // ----------------------------------- Methods for Address Type
  // =================================================================

  /**
   * Create an AddressType node from Property record
   * @return com.filogix.schema.datx.closing.AddressType 
   * @throws JAXBException 
   */
  private com.filogix.schema.datx.closing.AddressType buildAddressType(Property prop) throws JAXBException 
  {
	  // property Address is mandatory, however all fields are optional
    AddressType address = factory.createAddressType();
    
    if (prop == null) {
      return address;
    }
    
    String sUnit = "", sNum="", sName="", sType="", sDir="";
        
    if ((prop.getPropertyAddressLine2() != null) && !(prop.getPropertyAddressLine2().trim().equals(""))) {
      address.setAddressLine2(prop.getPropertyAddressLine2());
    }
    
    if ((prop.getUnitNumber()!= null) && !(prop.getUnitNumber().trim().equals(""))){
      sUnit = prop.getUnitNumber();
      address.setUnitNumber(sUnit);
      sUnit = sUnit + " - ";
    }
    
    if ((prop.getPropertyStreetNumber() != null) && !(prop.getPropertyStreetNumber().trim().equals(""))){
      sNum = prop.getPropertyStreetNumber();  
      address.setStreetNumber(sNum);
    }
    
    if ((prop.getPropertyStreetName() != null) && !(prop.getPropertyStreetName().trim().equals(""))) {
      sName = prop.getPropertyStreetName();
      address.setStreetName(sName);
    }
    
    if (prop.getStreetTypeId() != 0) {
      sType = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "STREETTYPE", prop.getStreetTypeId(), ServiceConst.LANG_ENGLISH);
      address.setStreetType(sType);
    }
      
    if (prop.getStreetDirectionId() != 0){
      sDir = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "STREETDIRECTION", prop.getStreetDirectionId(), ServiceConst.LANG_ENGLISH);
      address.setStreetDirection(sDir);
    }
    
    if ((prop.getPropertyCity() != null)) {
      address.setCity(prop.getPropertyCity());
    }
    
    address.setProvince(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PROVINCE", prop.getProvinceId(), ServiceConst.LANG_ENGLISH));
    
    if (prop.getPropertyPostalFSA() != null && prop.getPropertyPostalLDU() != null 
        && !(prop.getPropertyPostalFSA().trim().equals("")) && !(prop.getPropertyPostalLDU().trim().equals(""))) {
      address.setPostalCode(prop.getPropertyPostalFSA().toUpperCase() + " " + prop.getPropertyPostalLDU().toUpperCase());
    }

    address.setCountry(ServiceConst.COUNTRY_CANADA);
    
    switch (_langId){
      case ServiceConst.LANG_FRENCH: {
        address.setAddressLine1(sUnit + sNum + " " + sName + " " + sType + " " + sDir);
        break;
      }
      default: {
        address.setAddressLine1(sUnit + sNum + " " + sType + " " + sName + " " + sDir);        
        break;
      }  
    }
    return address;
  }
  
  /**
   * Create an AddressType node from Contact record
   * @return com.filogix.schema.datx.closing.AddressType 
   * @throws JAXBException 
   */
  // contact.getAddr()
  private com.filogix.schema.datx.closing.AddressType buildAddressType(Addr aAddr) throws JAXBException 
  {
    AddressType address = null;
    if (aAddr == null) { 
      return address; 
    }
    
    address = factory.createAddressType();
    
    if ((aAddr.getAddressLine1() != null) && (!aAddr.getAddressLine1().trim().equals(""))) {
      address.setAddressLine1(aAddr.getAddressLine1());
    }
    if ((aAddr.getAddressLine2() != null) && !(aAddr.getAddressLine2().trim().equals(""))) {
      address.setAddressLine2(aAddr.getAddressLine2());
    }
    if ((aAddr.getCity() != null) && !(aAddr.getCity().trim().equals(""))) {
      address.setCity(aAddr.getCity());
    }
    address.setProvince(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "PROVINCE", aAddr.getProvinceId(), ServiceConst.LANG_ENGLISH));
    if (aAddr.getProvinceId() != 0) {
      address.setCountry(ServiceConst.COUNTRY_CANADA);
    }
    if ((aAddr.getPostalFSA() != null) && (aAddr.getPostalLDU() != null) && !(aAddr.getPostalFSA().trim().equals(""))
        && !(aAddr.getPostalLDU().trim().equals(""))) {

      address.setPostalCode(aAddr.getPostalFSA().toUpperCase() + " " + aAddr.getPostalLDU().toUpperCase());
    }
    
    return address;
  }

  /**
   * Create an AddressType node from Borrower record
   * 
   * @return com.filogix.schema.datx.closing.AddressType
   * @throws JAXBException
   */
  private Collection buildBorrowerAddressList(Borrower bor) throws JAXBException 
  {
    com.filogix.schema.datx.closing.BorrowerType.AddressType address;
    ArrayList result = new ArrayList();
    
    Collection col;
    try {
      col = bor.getBorrowerAddresses();
      if ((col != null) && (col.size() > 0)) {

        Iterator it = col.iterator();
        while (it.hasNext()) {
          BorrowerAddress bAddr = (BorrowerAddress) it.next();
          address = factory.createBorrowerTypeAddressType();
                    
          address.setType(BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "BORROWERADDRESSTYPE", bAddr.getBorrowerAddressTypeId(), ServiceConst.LANG_ENGLISH));
          
          int id = bAddr.getAddrId();
          if (id != 0) {
          Addr aAddr = bAddr.getAddr();
            if (aAddr != null) {
              com.filogix.schema.datx.closing.AddressType aaa = buildAddressType(aAddr);
              address.setAddressLine1(aaa.getAddressLine1());
              address.setAddressLine2(aaa.getAddressLine2());
              address.setCity(aaa.getCity());
              address.setProvince(aaa.getProvince());
              if (aaa.getPostalCode()!=null)
            	  address.setPostalCode(aaa.getPostalCode().toUpperCase());
              address.setCountry(aaa.getCountry());
              result.add(address);
             }
          }
        }
      }
    } catch (Exception e) {
      _logger.debug("Exception in DatxOscPayloadData@buildAddressType(): for BorrowerId = " + bor.getBorrowerId() + 
          ", copyId = " + bor.getCopyId()+ " for requestId = " + _requestId + ", copyId = " + _copyId+ ": \n" + StringUtil.stack2string(e));
    }
    return result;
  }

  public ClosingServicesType getDatxOscPayloadData()
  {
    return (ClosingServicesType)getServicePayload();
  }
  
  public Object getServicePayload() {
    return oscPayload;
  }

  protected boolean requireServiceRequestContact()
  {
      try
      {
    	  String reqTypeStr = _request.getRequestTypeShortName(_requestId, _copyId).trim();
          return (reqTypeStr.equals(ServiceConst.REQUEST_TYPE_CLOSING_FIXED_REQUEST_STR));
      }
      catch (Exception e)
      {
    	  _logger.debug("Exception in DatxOscPayloadData@requireServiceRequestContact() requestId: " + _requestId);
    	  return false;
      }
  }
}
