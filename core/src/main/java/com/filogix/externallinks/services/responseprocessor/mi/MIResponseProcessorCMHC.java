package com.filogix.externallinks.services.responseprocessor.mi;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealFee;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.DealFeeCreator;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.util.XMLUtil;

public class MIResponseProcessorCMHC extends AbstractMIResponseProcessor {

    private final static Logger logger = LoggerFactory.getLogger(MIResponseProcessorCMHC.class);

    // APPROVAL_CODES
    protected final static String APPLICATION_APPROVED = "0108";
    protected final static String APPLICATION_APPROVED_BY_CMHC = "0102";
    protected final static String THIS_APPLICATION_IS_INSURABLE = "0210";
    protected final static String PRE_QUALIFICATION_SUBJECT_TO_CMHC = "0424";

    protected final static String[] APPROVAL_CODES = {
        APPLICATION_APPROVED, APPLICATION_APPROVED_BY_CMHC, THIS_APPLICATION_IS_INSURABLE,
        PRE_QUALIFICATION_SUBJECT_TO_CMHC };

    // UNACCEPTABLE_CODES
    protected final static String APPLICATION_UNACCEPTABLE_TO_CMHC = "0123";
    protected final static String THIS_APPLICATION_IS_NON_INSURABLE = "0211";
    protected final static String INSURANCE_DECLINED_DUE_BORROWER_RISK = "0157";

    protected final static String[] UNACCEPTABLE_CODES = {
        APPLICATION_UNACCEPTABLE_TO_CMHC, THIS_APPLICATION_IS_NON_INSURABLE,
        INSURANCE_DECLINED_DUE_BORROWER_RISK };

    protected final static String APPLICATION_REFERRED_TO_UNDERWRITER = "0110";
	protected final static String APPLICATION_REFERRED_TO_UNDERWRITER2 = "000";
	//Oddly, CMHC rarely sends '000' as Manual Underwriting replay.   
	protected final static String[] MANUAL_UNDERWRITING_CODES = {
			APPLICATION_REFERRED_TO_UNDERWRITER,
			APPLICATION_REFERRED_TO_UNDERWRITER2 };
    
    protected final static String APPLICATION_CANCELLED_AS_REQUESTED = "0115";

    protected final static String PREMIUM_DUE_AMOUNT = "0109";
    protected final static String TAX_DUE_AMOUNT = "0135";
    protected final static String FEE_DUE_AMOUNT = "0137";

    protected final static String APPLICATION_FEE = "0230";
    protected final static String APPLICATION_FEE_PRE_QUALIFICATION = "0601";

    @Override
    protected void setPropagateMIFields(String payload, ServiceInfo info, IResponseAdaptor miResponse,
        CalcMonitor dcm, DealPropagator dealp, DealFeeCreator dfCreator) throws ExternalServicesException {

        int miStatus = createMIStatusID(miResponse, dealp);

        dealp.setMIStatusId(miStatus);
        dealp.setMortgageInsuranceResponse(
                makeMortgageInsuranceResponseText(miResponse, dealp));

        String refNum = miResponse.getMIProviderReferenceNumber();

        if (dealp.getMIIndicatorId() == Mc.MI_INDICATOR_PRE_QUALIFICATION) {
            // MI pre-qualification response must not update the
            // deal.MIPolicyNumber.
            if (refNum != null) {
                if (refNum.equals(dealp.getPreQualificationMICertNum())) {
                    // do nothing
                } else {
                    dealp.setPreQualificationMICertNum(refNum);
                }
            }
        } else {
            dealp.setMIPolicyNumber(refNum);
            dealp.setMIPolicyNumCMHC(refNum);
        }

        if (XMLUtil.isEmpty(refNum) == false) {

            if (miResponse.messageContains(THIS_APPLICATION_IS_INSURABLE) &&
                    dealHasFeeAttached(dealp, Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE) == false) {

                dfCreator.add(Mc.FEE_TYPE_CMHC_LOAN_ASSESSMENT_FEE,
                        miResponse.getFeeAmountFor(APPLICATION_FEE));
            }
        }
        if (dealp.getPreQualificationMICertNum() != null &&
                miResponse.messageContains(PRE_QUALIFICATION_SUBJECT_TO_CMHC)
                && dealHasFeeAttached(dealp, Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE) == false) {

            dfCreator.add(Mc.FEE_TYPE_CMHC_PRE_QUALIFICATION_FEE,
                    miResponse.getFeeAmountFor(APPLICATION_FEE_PRE_QUALIFICATION));
        }

        // on approval
        if (miStatus == Mc.MI_STATUS_APPROVAL_RECEIVED || miStatus == Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED) 
        {

            dealp.setMIPremiumAmount(miResponse.getFeeAmountFor(PREMIUM_DUE_AMOUNT));
            dealp.setMIFeeAmount(miResponse.getFeeAmountFor(FEE_DUE_AMOUNT));
            dealp.setMIPremiumPST(miResponse.getFeeAmountFor(TAX_DUE_AMOUNT));

            dfCreator.add(Mc.FEE_TYPE_CMHC_PREMIUM, miResponse.getFeeAmountFor(PREMIUM_DUE_AMOUNT));
            dfCreator.add(Mc.FEE_TYPE_CMHC_FEE, miResponse.getFeeAmountFor(FEE_DUE_AMOUNT));
            dfCreator.add(Mc.FEE_TYPE_CMHC_PST_ON_PREMIUM, miResponse.getFeeAmountFor(TAX_DUE_AMOUNT));
        }

    }

    protected int createMIStatusID(IResponseAdaptor miResponse, Deal deal) {

        int miStatusId = deal.getMIStatusId();

        if (XMLUtil.isEmpty(miResponse.getMIProviderReferenceNumber())) {
            miStatusId = Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER;

        } else if (miResponse.messageContains(APPLICATION_CANCELLED_AS_REQUESTED)) {
            miStatusId = Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC;

        } else if (miResponse.messageContainsAny(MANUAL_UNDERWRITING_CODES)) {
            miStatusId = Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY;

        } else if (miResponse.messageContainsAny(UNACCEPTABLE_CODES)) {
            miStatusId = Mc.MI_STATUS_DENIED_RECEIVED;

        } else if (miResponse.messageContainsAny(APPROVAL_CODES)) {
            // if MI premium is changed, this status is to be overwritten
            miStatusId = Mc.MI_STATUS_APPROVAL_RECEIVED;

        } else {
            miStatusId = Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER;
        }

        return miStatusId;

    }

    protected static boolean dealHasFeeAttached(Deal deal, int feeTyepId) {
        try {
            DealFee dealFee = new DealFee(deal.getSessionResourceKit(), null);
            Collection<DealFee> dealFees = dealFee.findByDealAndType(new DealPK(deal.getDealId(), deal.getCopyId()), feeTyepId);
            return !dealFees.isEmpty();
        } catch (Exception e) {
            logger.warn("failed to find dealFee", e);
        }
        return false;

    }

}
