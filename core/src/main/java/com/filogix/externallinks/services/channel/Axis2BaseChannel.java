package com.filogix.externallinks.services.channel;

import java.rmi.RemoteException;
import java.util.UUID;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axiom.soap.SOAPHeaderBlock;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.util.TypeConverter;
import com.filogix.externallinks.services.ServiceInfo;

public abstract class Axis2BaseChannel extends AbstractBaseChannel {

	private final static Logger logger = LoggerFactory.getLogger(Axis2BaseChannel.class);

	protected void setupMetaData(ServiceClient sc, ServiceInfo serviceInfo) {

		/**
		 * <pre>
		 * sample Soap header (https://c4e.filogix.com/srr/Wiki%20Pages/SOAP%20Routing%20at%20Filogix.aspx)
		 * 
		 * <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
		 *   xmlns:wsa="http://www.w3.org/2005/08/addressing" 
		 *   xmlns:wss="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" 
		 *   xmlns:fx="http://www.filogix.com/esb">
		 * 
		 *  <soapenv:Header>
		 *   <wsa:MessageID>urn:uuid:FB485FAF-E94C-497F-9B20-662F7C5D9680</wsa:MessageID>
		 *   <wsa:Action>urn:action:Exchange.CreateFolder.Broker.1_0</wsa:Action>
		 *   <wsa:To>http://10.1.201.39:3045/CreateBrokerFolder</wsa:To>
		 *   <wsa:From>
		 *    <wsa:Address>http://qa8.expert.filogix.com/services.exchange</wsa:Address>
		 *   </wsa:From>
		 *   <wsa:ReferenceParameters>
		 *    <fx:BusinessTransactionID>QA-38334</fx:BusinessTransactionID>
		 *   </wsa:ReferenceParameters>
		 *   <wss:Security>
		 *    <wss:UsernameToken>
		 *     <wss:Username>expert</wss:Username>
		 *     <wss:Password>********</wss:Password>
		 *    </wss:UsernameToken>
		 *   </wss:Security>
		 * </soapenv:Header>
		 * 
		 * <pre>
		 */

		// timeout between Express and DataPower
		Options op = sc.getOptions();
		op.setTimeOutInMilliSeconds(this.getTimeoutDP());
		
		SOAPFactory factory = OMAbstractFactory.getSOAP11Factory();
		
		setupEndpointReference(sc, factory);

		setupWSAddressing(sc, serviceInfo, factory);

		setupWSSecurity(sc, serviceInfo, factory);

	}


	protected void setupEndpointReference(ServiceClient sc, SOAPFactory factory) {
		// timeout between DataPower and MQ server
		/**
		 * <pre>
		 * xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
		 * xmlns:wsa=http://www.w3.org/2005/08/addressing
		 * xmlns:esb="http://marketplace.dhltd.com/esb/headers"
		 * 
		 * /soap:Envelope/soap:Header/wsa:EndpointReference/wsa:ReferenceProperties/esb:timeout"/
		 * </pre>
		 */
		
		SOAPHeaderBlock endPointRef = createSOAPHeaderEndpointReference(factory);
		sc.addHeader(endPointRef);
	}

	protected SOAPHeaderBlock createSOAPHeaderEndpointReference(SOAPFactory factory) {

		OMNamespace omNsWsa = factory.createOMNamespace(XMLNS_WSA, "wsa");
		OMNamespace omNsEsb = factory.createOMNamespace(XMLNS_MP_ESB_HEADERS, "esb");

		SOAPHeaderBlock endPointRef = factory.createSOAPHeaderBlock(
				"EndpointReference", omNsWsa);
		OMElement refParams = factory.createOMElement("ReferenceParameters", omNsWsa);
		OMElement address = factory.createOMElement("Address", omNsWsa);
		OMElement timeout = factory.createOMElement("timeout", omNsEsb);

		endPointRef.addChild(address);
		endPointRef.addChild(refParams);
		refParams.addChild(timeout);
		timeout.setText(String.valueOf(this.getTimeoutMQ()));

		return endPointRef;
	}

	protected void setupWSSecurity(ServiceClient sc, ServiceInfo serviceInfo, SOAPFactory factory) {

		// WS-Security block
		OMNamespace omNsWsse = factory.createOMNamespace(XMLNS_WSS, "wss");

		SOAPHeaderBlock securityBlock = factory.createSOAPHeaderBlock("Security", omNsWsse);
		OMElement usernameTokenElement = factory.createOMElement("UsernameToken", omNsWsse);

		OMElement usernameElement = factory.createOMElement("Username", omNsWsse);
		OMText omTextUsername = factory.createOMText(usernameElement, this.getUsername());
		usernameElement.addChild(omTextUsername);

		OMElement passwordElement = factory.createOMElement("Password", omNsWsse);
		OMText omTextPassword = factory.createOMText(passwordElement, this.getPassword());
		passwordElement.addChild(omTextPassword);

		usernameTokenElement.addChild(usernameElement);
		usernameTokenElement.addChild(passwordElement);
		securityBlock.addChild(usernameTokenElement);
		securityBlock.setMustUnderstand(true);
		sc.addHeader(securityBlock);
	}


	protected void setupWSAddressing(ServiceClient sc, ServiceInfo serviceInfo, SOAPFactory factory) {

		// WS-Addresing block
		OMNamespace omNsWsa = factory.createOMNamespace(XMLNS_WSA, "wsa");

		sc.addHeader(createSOAPHeaderWSAMessageId(serviceInfo, factory, omNsWsa));

		sc.addHeader(createSOAPHeaderWSAAction(factory, omNsWsa));

		sc.addHeader(createSOAPHeaderWSATo(factory, omNsWsa));

		sc.addHeader(createSOAPHeaderWSAFrom(factory, omNsWsa));

		sc.addHeader(createSOAPHeaderWSAReferenceParameters(serviceInfo, factory, omNsWsa));
	}


	protected SOAPHeaderBlock createSOAPHeaderWSAMessageId(ServiceInfo serviceInfo,
			SOAPFactory factory, OMNamespace omNsWsa) {
		
		SOAPHeaderBlock messageId = factory.createSOAPHeaderBlock("MessageID", omNsWsa);
		String uuid = UUID.randomUUID().toString();
		logger.info("generated uuid = " + uuid + " for dealid = " + serviceInfo.getDeal().getDealId());
		messageId.setText("urn:uuid:" + uuid);
		messageId.setMustUnderstand(false);
		logger.debug("wsa:MessageId=" + messageId.getText());
		serviceInfo.setChannelTransactionKey(messageId.getText().substring(9)); //dirty hack so that UC3 will work
		
		return messageId;
	}


	protected SOAPHeaderBlock createSOAPHeaderWSAReferenceParameters(
			ServiceInfo serviceInfo, SOAPFactory factory, OMNamespace omNsWsa) {
		
		SOAPHeaderBlock refParams = factory.createSOAPHeaderBlock("ReferenceParameters", omNsWsa);
		OMNamespace omNsFx = factory.createOMNamespace(XMLNS_FILOGIC_ESB, "fx");
		OMElement bizTranId = factory.createOMElement("BusinessTransactionID", omNsFx);
		refParams.addChild(bizTranId);
		bizTranId.setText(String.valueOf(serviceInfo.getDeal().getDealId()));
		return refParams;
	}


	protected SOAPHeaderBlock createSOAPHeaderWSAFrom(SOAPFactory factory,
			OMNamespace omNsWsa) {
		
		SOAPHeaderBlock from = factory.createSOAPHeaderBlock("From", omNsWsa);
		OMElement address = factory.createOMElement("Address", omNsWsa);
		from.addChild(address);
		address.setText(this.getWsAddressing_from());
		return from;
	}


	protected SOAPHeaderBlock createSOAPHeaderWSATo(SOAPFactory factory,
			OMNamespace omNsWsa) {
		
		SOAPHeaderBlock to = factory.createSOAPHeaderBlock("To", omNsWsa);
		to.setText(this.getEndpoint());
		to.setMustUnderstand(false);
		return to;
	}


	protected SOAPHeaderBlock createSOAPHeaderWSAAction(SOAPFactory factory,
			OMNamespace omNsWsa) {
		
		SOAPHeaderBlock action = factory.createSOAPHeaderBlock("Action", omNsWsa);
		action.setText(this.getWsAddressing_action());
		action.setMustUnderstand(false);
		return action;
	}


	protected static void handleRequestSendingException(RemoteException e) throws ChannelRequestConnectivityException {

		logger.error("handleRequestSendingException", e);

		ChannelRequestConnectivityException cre = new ChannelRequestConnectivityException(e.getMessage(), e);

		if (e instanceof AxisFault) {

			// try to grab more information if the exception is AxisFault
			AxisFault af = (AxisFault) e;
			if(af.getFaultCode() != null){
				cre.setFaultCode(cleanFaultCode(af.getFaultCode().toString()));
			}

			logger.error("handleRequestSendingException AxisFault details start----------");
			logger.error("FaultCode = " + af.getFaultCode());
			logger.error("FaultType = " + af.getFaultType());
			logger.error("FaultAction = " + af.getFaultAction());
			logger.error("FaultNode = " + af.getFaultNode());
			logger.error("FaultRole = " + af.getFaultRole());
			logger.error("Message = " + af.getMessage());
			logger.error("NodeURI = " + af.getNodeURI());
			logger.error("Reason = " + af.getReason());
			logger.error("handleRequestSendingException AxisFault details end----------");
		}

		throw cre;
	}

	/**
	 * pick up errorcode from faultCode value. The faultcode format is
	 * "<processorName>.<error code>".
	 * 
	 * @see https
	 *      ://c4e.filogix.com/srr/Wiki%20Pages/Marketplace.Framework.FaultCodes
	 *      .aspx
	 * @param faultCodeStr
	 * @return -1(no valid number found) or number value of error code
	 */
	private static int cleanFaultCode(String faultCodeStr) {

		int defaultCode = ChannelRequestConnectivityException.FAULT_CODE_BLANK;

		// if null or empty, return invalid
		if (StringUtils.isBlank(faultCodeStr))
			return defaultCode;
		// if the code is already numeric, just return it
		if (StringUtils.isNumeric(faultCodeStr))
			return TypeConverter.intTypeFrom(faultCodeStr);

		int lastIdx = StringUtils.lastIndexOf(faultCodeStr, '.');
		if (lastIdx < 0)
			return defaultCode;

		String right = StringUtils.substring(faultCodeStr, lastIdx + 1);
		if (StringUtils.isNumeric(right))
			return TypeConverter.intTypeFrom(right);

		return defaultCode;
	}

}
