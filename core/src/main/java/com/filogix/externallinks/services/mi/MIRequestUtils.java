package com.filogix.externallinks.services.mi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.util.Xc;

public abstract class MIRequestUtils {

    private final static Logger logger = LoggerFactory.getLogger(MIRequestUtils.class);

    public static void addDealHistLog(Deal deal, SessionResourceKit srk, String text) {

        DealHistoryLogger dhl = new DealHistoryLogger(srk);
        int userProfId = srk.getExpressState().getUserProfileId();

        int statusID = deal.getStatusId();
        try {
            dhl.log(deal.getDealId(), deal.getCopyId(), text, Mc.DEAL_TX_TYPE_EVENT, userProfId, statusID);
        } catch (Exception e) {
            logger.error("faild to write dealHistory", e);
        }
    }

    public static void addDealHistLogForResponse(Deal deal, SessionResourceKit srk) {

        DealHistoryLogger dhl = new DealHistoryLogger(srk);
        String text = dhl.concatMortgageInsuranceResponseText(deal);
        addDealHistLog(deal, srk, text);
    }

    public static void addDealHistLogForRequest(Deal deal, SessionResourceKit srk) {

        DealHistoryLogger dhl = new DealHistoryLogger(srk);
        String text = dhl.concatMortgageInsuranceRequestText(deal);
        addDealHistLog(deal, srk, text);
    }

    public static void addDealHistLogUnsolicitedReject(Deal deal, SessionResourceKit srk) {

        int languageId = Mc.LANGUAGE_PREFERENCE_ENGLISH;
        try {
            Borrower borrower = new Borrower(srk, null);
            borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
            languageId = borrower.getLanguagePreferenceId();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        String msg = BXResources.getSysMsg(Mc.MI_RESPONSE_MSG_UNSOLICITED_RESPONSE, languageId);

        String insurerName = BXResources.getPickListDescription(
            srk.getExpressState().getDealInstitutionId(), Xc.PKL_MORTGAGEINSURER,
            deal.getMortgageInsurerId(), languageId);
        msg = msg.replaceAll("%s", insurerName);

        addDealHistLog(deal, srk, msg);
    }

}
