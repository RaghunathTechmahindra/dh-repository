package com.filogix.externallinks.services.payload;

import com.filogix.externallinks.services.ServiceInfo;

public interface IPayloadGenerator {
    String getXMLPayload(boolean validate, ServiceInfo info) throws Exception;
}
