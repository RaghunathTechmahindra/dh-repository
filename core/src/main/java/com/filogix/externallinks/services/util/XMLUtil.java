package com.filogix.externallinks.services.util;

import java.util.*;
import java.math.*;
import java.io.*;
import javax.xml.bind.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import com.filogix.express.core.ExpressRuntimeException;


/**
 *
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Filogix</p>
 *
 * XMLUtil serves as a converter for XML data type and java data type.
 * All functions perform basic conversion, and the caller is responsible
 * for ensuring  valid parameters.
 *
 * @author Lin Zhou
 * @version 1.0
 */
public class XMLUtil {

    public static Calendar date2Calendar(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Given a java.util.Date, generate the XMLGregorianCalendar.
     * @param date
     * @return
     * @throws DatatypeConfigurationException
     */
    public static XMLGregorianCalendar date2XMLGregorianCalendar(Date date){
        if(date == null)return null;
        DatatypeFactory df;
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new ExpressRuntimeException(e);
        }
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        return df.newXMLGregorianCalendar(gc);
    }
    
    public static BigInteger int2BigInteger(int val) {
        return BigInteger.valueOf(val);
    }

    public static BigDecimal double2BigDecimal(double val) {
        return new BigDecimal(val + "");
    }

    public static double bigDecimal2Double(BigDecimal val, int scale) {
        if(val == null) return 0.0;
        return val.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static double bigDecimal2Double(BigDecimal val, int scale,
                                           int roundingMode) {
        if(val == null) return 0.0;
        return val.setScale(scale, roundingMode).doubleValue();
    }


    public static int bigInteger2Int(BigInteger val) {
        // if BigInteger is too big to fit in an int, only the low-order 32 bits are returned
        return val.intValue();
    }

    public static Date calendar2Date(Calendar val) {
        return val.getTime();
    }

    public static String boolean2String(boolean val) {
        return val?"Y":"N";
    }

    public static boolean isEmpty(int val) {
        //return val == 0;
    	return false;
    }

    public static boolean isEmpty(char val) {
        return val == (char)32;
    }

    public static boolean isEmpty(double val) {
        //return val == 0;
        return false;
    }

    public static boolean isEmpty(String val) {
		// FXP29827: rolled back to the original code in order to avoid
		// generating empty tags for the fields with null string.

    	//return val == null;
    	return val == null || val.trim().length() == 0;
    }

    public static boolean isEmpty(Date val) {
        return val == null;
    }

    public static String jaxb2String(Marshaller marshaller, Object element) throws JAXBException  {
        StringWriter xmlWriter = new StringWriter();
        marshaller.marshal(element, xmlWriter);
        return xmlWriter.toString();
    }

    public static Object string2jaxb(Unmarshaller unmarshaller, String element) throws Exception {
        return unmarshaller.unmarshal(new StreamSource(new StringReader(element)));
    }

    public static Object byteArray2jaxb(Unmarshaller unmarshaller, byte[] element) throws Exception {
        return unmarshaller.unmarshal(new ByteArrayInputStream(element));
    }
}
