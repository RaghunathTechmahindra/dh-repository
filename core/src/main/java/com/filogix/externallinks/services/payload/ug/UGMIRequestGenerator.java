package com.filogix.externallinks.services.payload.ug;

import java.util.*;
import javax.xml.bind.*;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.MtgProd;
import com.basis100.deal.entity.PricingProfile;
import com.basis100.deal.entity.PropertyExpense;
import com.basis100.deal.entity.DownPaymentSource;
import com.basis100.deal.entity.EscrowPayment;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.CreditReference;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.resources.*;
import com.filogix.externallinks.mi.aigug.jaxb.request.ObjectFactory;
import com.filogix.externallinks.mi.aigug.jaxb.request.MortgageInsuranceRequest;
import com.filogix.externallinks.mi.aigug.jaxb.request.DealType;
import com.filogix.externallinks.mi.aigug.jaxb.request.DownPaymentSourceType;
import com.filogix.externallinks.mi.aigug.jaxb.request.InstitutionProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.request.MtgProdType;
import com.filogix.externallinks.mi.aigug.jaxb.request.EscrowPaymentType;
import com.filogix.externallinks.mi.aigug.jaxb.request.PricingProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.request.BorrowerType;
import com.filogix.externallinks.mi.aigug.jaxb.request.PropertyType;
import com.filogix.externallinks.mi.aigug.jaxb.request.PropertyExpenseType;
import com.filogix.externallinks.mi.aigug.jaxb.request.
        SourceOfBusinessProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.request.AddrType;
import com.filogix.externallinks.mi.aigug.jaxb.request.SourceFirmProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.request.UnderwriterType;
import com.filogix.externallinks.mi.aigug.jaxb.request.LenderProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.request.BranchProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.request.BorrowerAddressType;
import com.filogix.externallinks.mi.aigug.jaxb.request.IncomeType;
import com.filogix.externallinks.mi.aigug.jaxb.request.AssetType;
import com.filogix.externallinks.mi.aigug.jaxb.request.LiabilityType;
import com.filogix.externallinks.mi.aigug.jaxb.request.CreditReferenceType;
import com.filogix.externallinks.mi.aigug.jaxb.request.EmploymentHistoryType;
import com.filogix.externallinks.mi.aigug.jaxb.request.ContactType;
import com.filogix.externallinks.services.util.XMLUtil;
import com.filogix.externallinks.services.payload.IPayloadGenerator;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import com.filogix.externallinks.services.ServiceInfo;
import  com.filogix.externallinks.framework.CustomCharacterEscapeHandler;

public class UGMIRequestGenerator implements IPayloadGenerator {
    private static Log logger = LogFactory.getLog(UGMIRequestGenerator.class);
    private static ObjectFactory objFactory = new ObjectFactory();
    private static JAXBContext jc;
    private static UGMIRequestGenerator instance = new UGMIRequestGenerator();
    private ServiceInfo sInfo;
    private static final String schema = "com.filogix.externallinks.mi.aigug.jaxb.request";

    static {
        try {
            jc = JAXBContext.newInstance(schema);
        } catch (JAXBException e) {
            logger.error(
                    "UGMIRequestGenerator: JAXBContext initialization failed.");
        }
    }

//    public static IPayloadGenerator getInstance() {
//        return instance;
//    }

    public UGMIRequestGenerator() {
    }

    public String getXMLPayload(boolean validate, ServiceInfo info) throws Exception {
        SessionResourceKit srk = info.getSrk();
        //changed for FXP24408 - deal may be from info/calc
        Deal deal = info.getDeal();
        if (deal==null) {
            deal = new Deal(srk, info.getCalcMonitor(), info.getDealId().intValue(), info.getCopyId().intValue());
            info.setDeal(deal);
        }
        sInfo = info;
        return getXMLPayload(validate, srk, deal);
    }

    private String getXMLPayload(boolean validate, SessionResourceKit srk, Deal deal) throws Exception {
    	logger.info("==========>>>>>>>>>> UGMIRequestGenerator:: getting XMLPAYLOAD for Dealid:: " + deal.getDealId());
    	String paylod = getXMLPayload(getMortgageInsuranceRequest(srk, deal), validate);
    	logger.info("===== MI REQUEST ===== + \r\n" + paylod);
        return getXMLPayload(getMortgageInsuranceRequest(srk, deal), validate);
    }

    private String getXMLPayload(MortgageInsuranceRequest req, boolean validate) throws
            Exception {
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                               Boolean.TRUE);

		marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        //marshaller.setProperty(
        //       "com.sun.xml.bind.characterEscapeHandler",
        //        new CustomCharacterEscapeHandler() );  

        if (!validate) {
            return XMLUtil.jaxb2String(marshaller, req);
        } else {
            if (validate(req)) {
                return XMLUtil.jaxb2String(marshaller, req);
            } else {
                throw new Exception(this.getClass().getName() +
                                    ": validation failed.");
            }
        }
    }

    private MortgageInsuranceRequest getMortgageInsuranceRequest(SessionResourceKit srk, Deal deal) throws
            Exception {
        srk.getExpressState().setDealInstitutionId(deal.getInstitutionProfileId());
        if (deal != null && srk != null) {
            MortgageInsuranceRequest req = objFactory.
                                           createMortgageInsuranceRequest();

            req.setDeal(createDealType(srk, deal));
            return req;
        } else {
            logger.error(this.getClass().getName() + ": ServiceInfo problem.");
            throw new Exception(this.getClass().getName() + ": ServiceInfo problem.");
        }
    }

    private boolean validate(MortgageInsuranceRequest req) {
        boolean retVal = false;

        try {
            retVal = jc.createValidator().validate(req);
        } catch (Exception e) {
            logger.error(this.getClass().getName() + ": validation failed.");
        }

        return retVal;
    }

    private DealType createDealType(SessionResourceKit srk, Deal deal) throws Exception {
        DealType dealType = objFactory.createDeal();
        populateDealType(dealType, srk, deal);

        createProperty(dealType, srk, deal);
        createBorrower(dealType, srk, deal);
        createDownPaymentSource(dealType, srk, deal);
        createEscrowPayment(dealType, srk, deal);
        createBranchProfile(dealType, srk, deal);
        createInstitutionProfile(dealType, srk, deal);
        createLenderProfile(dealType, srk, deal);
        createUnderwriter(dealType, srk, deal);
        createMtgProd(dealType, srk, deal);
        createSourceFirmProfile(dealType, srk, deal);
        createSourceOfBusinessProfile(dealType, srk, deal);

        return dealType;
    }


    private void populateDealType(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
        dealType.setDealId(XMLUtil.int2BigInteger(deal.getDealId()));

        if (!XMLUtil.isEmpty(deal.getMIPolicyNumber())) {
        	dealType.setMIPolicyNumber(deal.getMIPolicyNumber());
        }

        if (!XMLUtil.isEmpty(deal.getServicingMortgageNumber())) {
        	dealType.setServicingMortgageNumber(deal.getServicingMortgageNumber());
        }

        if (!XMLUtil.isEmpty(deal.getRefiOrigPurchaseDate())) {
        	dealType.setRefiOrigPurchaseDate(XMLUtil.date2Calendar(deal.getRefiOrigPurchaseDate()));
        }

        if (!XMLUtil.isEmpty(deal.getApplicationDate())) {
            dealType.setApplicationDate(XMLUtil.date2Calendar(deal.
                    getApplicationDate()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalLoanAmount())) {
            dealType.setTotalLoanAmount(XMLUtil.double2BigDecimal(deal.
                    getTotalLoanAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getAmortizationTerm())) {
            dealType.setAmortizationTerm(XMLUtil.int2BigInteger(deal.
                    getAmortizationTerm()));
        }

        if (!XMLUtil.isEmpty(deal.getEstimatedClosingDate())) {
            dealType.setEstimatedClosingDate(XMLUtil.date2Calendar(deal.
                    getEstimatedClosingDate()));
        }

        if (!XMLUtil.isEmpty(deal.getDealTypeId())) {
            dealType.setDealTypeId(XMLUtil.int2BigInteger(deal.getDealTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalPurchasePrice())) {
            dealType.setTotalPurchasePrice(XMLUtil.double2BigDecimal(deal.
                    getTotalPurchasePrice()));
        }

        if (!XMLUtil.isEmpty(deal.getPandiPaymentAmount())) {
            dealType.setPandiPaymentAmount(XMLUtil.double2BigDecimal(deal.
                    getPandiPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getEscrowPaymentAmount())) {
            dealType.setEscrowPaymentAmount(XMLUtil.double2BigDecimal(deal.
                    getEscrowPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getNetLoanAmount())) {
            dealType.setNetLoanAmount(XMLUtil.double2BigDecimal(deal.
                    getNetLoanAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getSecondaryFinancing())) {
            dealType.setSecondaryFinancing(deal.getSecondaryFinancing());
        }

        if (!XMLUtil.isEmpty(deal.getNetInterestRate())) {
            dealType.setNetInterestRate(XMLUtil.double2BigDecimal(deal.
                    getNetInterestRate()));
        }

        if (!XMLUtil.isEmpty(deal.getDealPurposeId())) {
            dealType.setDealPurposeId(XMLUtil.int2BigInteger(deal.
                    getDealPurposeId()));
        }

        if (!XMLUtil.isEmpty(deal.getBuydownRate())) {
            dealType.setBuydownRate(XMLUtil.double2BigDecimal(deal.
                    getBuydownRate()));
        }

        if (!XMLUtil.isEmpty(deal.getSpecialFeatureId())) {
            dealType.setSpecialFeatureId(XMLUtil.int2BigInteger(deal.
                    getSpecialFeatureId()));
        }

        if (!XMLUtil.isEmpty(deal.getDownPaymentAmount())) {
            dealType.setDownPaymentAmount(XMLUtil.double2BigDecimal(deal.
                    getDownPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getLineOfBusinessId())) {
            dealType.setLineOfBusinessId(XMLUtil.int2BigInteger(deal.
                    getLineOfBusinessId()));
        }

        if (!XMLUtil.isEmpty(deal.getMortgageInsurerId())) {
            dealType.setMortgageInsuranceTypeId(XMLUtil.int2BigInteger(deal.
                    getMortgageInsurerId()));
        }

        if (!XMLUtil.isEmpty(deal.getMtgProdId())) {
            dealType.setMtgProdId(XMLUtil.int2BigInteger(deal.getMtgProdId()));
        }

        if (!XMLUtil.isEmpty(deal.getFirstPaymentDate())) {
            dealType.setFirstPaymentDate(XMLUtil.date2Calendar(deal.
                    getFirstPaymentDate()));
        }

        if (!XMLUtil.isEmpty(deal.getNetPaymentAmount())) {
            dealType.setNetPaymentAmount(XMLUtil.double2BigDecimal(deal.
                    getNetPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getMaturityDate())) {
            dealType.setMaturityDate(XMLUtil.date2Calendar(deal.getMaturityDate()));
        }

        if (!XMLUtil.isEmpty(deal.getReferenceDealNumber())) {
            dealType.setRefExistingMtgNumber(deal.getReferenceDealNumber());
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalLiabilities())) {
            dealType.setCombinedTotalLiabilities(XMLUtil.double2BigDecimal(deal.
                    getCombinedTotalLiabilities()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalAssets())) {
            dealType.setCombinedTotalAssets(XMLUtil.double2BigDecimal(deal.
                    getCombinedTotalAssets()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalIncome())) {
            dealType.setCombinedTotalIncome(XMLUtil.double2BigDecimal(deal.
                    getCombinedTotalIncome()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTDS())) {
            dealType.setCombinedTDS(XMLUtil.double2BigDecimal(deal.
                    getCombinedTDS()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedGDS())) {
            dealType.setCombinedGDS(XMLUtil.double2BigDecimal(deal.
                    getCombinedGDS()));
        }

        if (!XMLUtil.isEmpty(deal.getNumberOfBorrowers())) {
            dealType.setNumberOfBorrowers(XMLUtil.int2BigInteger(deal.
                    getNumberOfBorrowers()));
        }

        if (!XMLUtil.isEmpty(deal.getNumberOfGuarantors())) {
            dealType.setNumberOfGuarantors(XMLUtil.int2BigInteger(deal.
                    getNumberOfGuarantors()));
        }

        if (!XMLUtil.isEmpty(deal.getPaymentTermId())) {
            dealType.setPaymentTermId(XMLUtil.int2BigInteger(deal.
                    getPaymentTermId()));
        }

        if (!XMLUtil.isEmpty(deal.getInterestTypeId())) {
            dealType.setInterestTypeId(XMLUtil.int2BigInteger(deal.
                    getInterestTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getPaymentFrequencyId())) {
            dealType.setPaymentFrequencyId(XMLUtil.int2BigInteger(deal.
                    getPaymentFrequencyId()));
        }

        if (!XMLUtil.isEmpty(deal.getInterestCompoundId())) {
            dealType.setInterestCompoundId(XMLUtil.int2BigInteger(deal.
                    getInterestCompoundId()));
        }

        if (!XMLUtil.isEmpty(deal.getRefinanceNewMoney())) {
            dealType.setRefinanceNewMoney(XMLUtil.double2BigDecimal(deal.
                    getRefinanceNewMoney()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedLTV())) {
            dealType.setCombinedLTV(XMLUtil.double2BigDecimal(deal.
                    getCombinedLTV()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalAnnualHeatingExp())) {
            dealType.setCombinedTotalAnnualHeatingExp(XMLUtil.double2BigDecimal(
                    deal.getCombinedTotalAnnualHeatingExp()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalAnnualTaxExp())) {
            dealType.setCombinedTotalAnnualTaxExp(XMLUtil.double2BigDecimal(
                    deal.
                    getCombinedTotalAnnualTaxExp()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalCondoFeeExp())) {
            dealType.setCombinedTotalCondoFeeExp(XMLUtil.double2BigDecimal(deal.
                    getCombinedTotalCondoFeeExp()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalPropertyExp())) {
            dealType.setCombinedTotalPropertyExp(XMLUtil.double2BigDecimal(deal.
                    getCombinedTotalPropertyExp()));
        }

        if (!XMLUtil.isEmpty(deal.getLienPositionId())) {
            dealType.setLienPositionId(XMLUtil.int2BigInteger(deal.
                    getLienPositionId()));
        }

        if (!XMLUtil.isEmpty(deal.getMITypeId())) {
            dealType.setMortgageInsuranceTypeId(XMLUtil.int2BigInteger(deal.
                    getMITypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getNewMoney())) {
            dealType.setNewMoney(XMLUtil.double2BigDecimal(deal.getNewMoney()));
        }

        if (!XMLUtil.isEmpty(deal.getNumberOfProperties())) {
            dealType.setNumberOfProperties(
                    XMLUtil.int2BigInteger(deal.getNumberOfProperties()));
        }

        if (!XMLUtil.isEmpty(deal.getOldMoney())) {
            dealType.setOldMoney(XMLUtil.double2BigDecimal(deal.getOldMoney()));
        }

        if (!XMLUtil.isEmpty(deal.getPreApproval())) {
            dealType.setPreApproval(deal.getPreApproval());
        }

        if (!XMLUtil.isEmpty(deal.getRefiCurrentBal())) {
            dealType.setRefiCurrentBal(XMLUtil.double2BigDecimal(deal.
                    getRefiCurrentBal()));
        }

        if (!XMLUtil.isEmpty(deal.getRemainingBalanceSecondaryFin())) {
            dealType.setRemainingBalanceSecondaryFin(XMLUtil.double2BigDecimal(
                    deal.
                    getRemainingBalanceSecondaryFin()));
        }

        if (!XMLUtil.isEmpty(deal.getTaxPayorId())) {
            dealType.setTaxPayorId(XMLUtil.int2BigInteger(deal.getTaxPayorId()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalActAppraisedValue())) {
            dealType.setTotalActAppraisedValue(XMLUtil.double2BigDecimal(deal.
                    getTotalActAppraisedValue()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalEstAppraisedValue())) {
            dealType.setTotalEstAppraisedValue(XMLUtil.double2BigDecimal(deal.
                    getTotalEstAppraisedValue()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalPropertyExpenses())) {
            dealType.setTotalPropertyExpenses(XMLUtil.double2BigDecimal(deal.
                    getTotalPropertyExpenses()));
        }

        if (!XMLUtil.isEmpty(deal.getTotalPaymentAmount())) {
            dealType.setTotalPaymentAmount(XMLUtil.double2BigDecimal(deal.
                    getTotalPaymentAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getActualPaymentTerm())) {
            dealType.setActualPaymentTerm(XMLUtil.int2BigInteger(deal.
                    getActualPaymentTerm()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedGDSBorrower())) {
            dealType.setCombinedGDSBorrower(XMLUtil.double2BigDecimal(deal.
                    getCombinedGDSBorrower()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTDSBorrower())) {
            dealType.setCombinedTDSBorrower(XMLUtil.double2BigDecimal(deal.
                    getCombinedTDSBorrower()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedGDS3Year())) {
            dealType.setCombinedGDS3Year(XMLUtil.double2BigDecimal(deal.
                    getCombinedGDS3Year()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTDS3Year())) {
            dealType.setCombinedTDS3Year(XMLUtil.double2BigDecimal(deal.
                    getCombinedTDS3Year()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedTotalEquityAvailable())) {
            dealType.setCombinedTotalEquityAvailable(XMLUtil.double2BigDecimal(
                    deal.
                    getCombinedTotalEquityAvailable()));
        }

        if (!XMLUtil.isEmpty(deal.getEffectiveAmortizationMonths())) {
            dealType.setEffectiveAmortizationMonths(XMLUtil.int2BigInteger(deal.
                    getEffectiveAmortizationMonths()));
        }

        if (!XMLUtil.isEmpty(deal.getMIUpfront())) {
            dealType.setMIUpfront(deal.getMIUpfront());
        }

        if (!XMLUtil.isEmpty(deal.getRequiredDownpayment())) {
            dealType.setRequiredDownpayment(XMLUtil.double2BigDecimal(deal.
                    getRequiredDownpayment()));
        }

        if (!XMLUtil.isEmpty(deal.getRepaymentTypeId())) {
            dealType.setMIIndicatorId(XMLUtil.int2BigInteger(deal.
                    getMIIndicatorId()));
        }

        if (!XMLUtil.isEmpty(deal.getRepaymentTypeId())) {
            dealType.setRepaymentTypeId(XMLUtil.int2BigInteger(deal.
                    getRepaymentTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getMIComments())) {
            dealType.setMiComments(deal.getMIComments());
        }

        if (!XMLUtil.isEmpty(deal.getSystemTypeId())) {
            dealType.setSystemTypeId(XMLUtil.int2BigInteger(deal.
                    getSystemTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getCombinedLendingValue())) {
            dealType.setCombinedLendingValue(XMLUtil.double2BigDecimal(deal.
                    getCombinedLendingValue()));
        }

        if (!XMLUtil.isEmpty(deal.getExistingLoanAmount())) {
            dealType.setExistingLoanAmount(XMLUtil.double2BigDecimal(deal.
                    getExistingLoanAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getGDSTDS3YearRate())) {
            dealType.setGDSTDS3YearRate(XMLUtil.double2BigDecimal(deal.
                    getGDSTDS3YearRate()));
        }

        if (!XMLUtil.isEmpty(deal.getMIExistingPolicyNumber())) {
            dealType.setMIExistingPolicyNumber(deal.getMIExistingPolicyNumber());
        }

        if (!XMLUtil.isEmpty(deal.getPandIPaymentAmountMonthly())) {
            dealType.setPandIPaymentAmountMonthly(XMLUtil.double2BigDecimal(
                    deal.
                    getPandIPaymentAmountMonthly()));
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvance())) {
            dealType.setProgressAdvance(deal.getProgressAdvance());
        }

        if (!XMLUtil.isEmpty(deal.getAdvanceToDateAmount())) {
            dealType.setAdvanceToDateAmount(XMLUtil.double2BigDecimal(deal.
                    getAdvanceToDateAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getAdvanceNumber())) {
            dealType.setAdvanceNumber(XMLUtil.int2BigInteger(deal.
                    getAdvanceNumber()));
        }

        if (!XMLUtil.isEmpty(deal.getMIRUIntervention())) {
            dealType.setMIRUIntervention(deal.getMIRUIntervention());
        }

        if (!XMLUtil.isEmpty(deal.getRefiOrigPurchasePrice())) {
            dealType.setRefiOrigPurchasePrice(XMLUtil.double2BigDecimal(deal.
                    getRefiOrigPurchasePrice()));
        }

        if (!XMLUtil.isEmpty(deal.getRefiBlendedAmortization())) {
            dealType.setRefiBlendedAmortization(deal.getRefiBlendedAmortization());
        }

        if (!XMLUtil.isEmpty(deal.getRefiOrigMtgAmount())) {
            dealType.setRefiOrigMtgAmount(XMLUtil.double2BigDecimal(deal.
                    getRefiOrigMtgAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getRefiImprovementAmount())) {
            dealType.setRefiImprovementAmount(XMLUtil.double2BigDecimal(deal.
                    getRefiImprovementAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getNextAdvanceAmount())) {
            dealType.setNextAdvanceAmount(XMLUtil.double2BigDecimal(deal.
                    getNextAdvanceAmount()));
        }

        if (!XMLUtil.isEmpty(deal.getInterestRateIncrement())) {
            dealType.setInterestRateIncrement(XMLUtil.double2BigDecimal(deal.
                    getInterestRateIncrement()));
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvanceTypeId())) {
            dealType.setProgressAdvanceTypeId(XMLUtil.int2BigInteger(deal.
                    getProgressAdvanceTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getIncomeVerificationTypeId())) {
            dealType.setIncomeVerificationTypeId(XMLUtil.int2BigInteger(deal.
                    getIncomeVerificationTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getProductTypeId())) {
            dealType.setProductTypeId(XMLUtil.int2BigInteger(deal.
                    getProductTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getProgressAdvanceInspectionBy())) {
            dealType.setProgressAdvanceInspectionBy(deal.
                    getProgressAdvanceInspectionBy());
        }

        if (!XMLUtil.isEmpty(deal.getSelfDirectedRRSP())) {
            dealType.setSelfDirectedRRSP(deal.getSelfDirectedRRSP());
        }

        if (!XMLUtil.isEmpty(deal.getCmhcProductTrackerIdentifier())) {
            dealType.setProductTrackerIdentification(deal.
                                             getCmhcProductTrackerIdentifier());
        }

        if (!XMLUtil.isEmpty(deal.getLocRepaymentTypeId())) {
            dealType.setLOCRepaymentTypeId(XMLUtil.int2BigInteger(deal.
                    getLocRepaymentTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getRequestStandardService())) {
            dealType.setRequestStandardService(deal.getRequestStandardService());
        }

        if (!XMLUtil.isEmpty(deal.getLocAmortizationMonths())) {
            dealType.setLOCAmortizationMonths(XMLUtil.int2BigInteger(deal.
                    getLocAmortizationMonths()));
        }

        if (!XMLUtil.isEmpty(deal.getLocInterestOnlyMaturityDate())) {
            dealType.setLOCInterestOnlyMaturityDate(XMLUtil.date2Calendar(deal.
                    getLocInterestOnlyMaturityDate()));
        }

        if (!XMLUtil.isEmpty(deal.getRefiProductTypeId())) {
            dealType.setRefiProductTypeId(XMLUtil.int2BigInteger(deal.
                    getRefiProductTypeId()));
        }

        if (!XMLUtil.isEmpty(deal.getPandIUsing3YearRate())) {
            dealType.setPandIUsing3YearRate(XMLUtil.double2BigDecimal(deal.
                    getPandIUsing3YearRate()));
        }
    }

    private void createProperty(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
        List propertyTypes = dealType.getProperty();
        Collection props = deal.getProperties();

        if (props != null) {
            for (Iterator iter = props.iterator(); iter.hasNext(); ) {
                PropertyType propertyType = objFactory.createPropertyType();

                Property property = (Property) iter.next();
                populatePropertyType(propertyType, property);
                createPropertyExpense(propertyType, property);
                propertyTypes.add(propertyType);
            }
        }
    }

    private void populatePropertyType(PropertyType propertyType,
                                      Property property) throws Exception {
        if (!XMLUtil.isEmpty(property.getStreetTypeId())) {
            propertyType.setStreetTypeId(XMLUtil.int2BigInteger(property.
                    getStreetTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyTypeId())) {
            propertyType.setPropertyTypeId(XMLUtil.int2BigInteger(property.
                    getPropertyTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyUsageId())) {
            propertyType.setPropertyUsageId(XMLUtil.int2BigInteger(property.
                    getPropertyUsageId()));
        }

        if (!XMLUtil.isEmpty(property.getDwellingStyleId())) {
            propertyType.setDwellingStyleId(XMLUtil.int2BigInteger(property.
                    getDwellingStyleId()));
        }

        if (!XMLUtil.isEmpty(property.getOccupancyTypeId())) {
            propertyType.setOccupancyTypeId(XMLUtil.int2BigInteger(property.
                    getOccupancyTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyStreetNumber())) {
            propertyType.setPropertyStreetNumber(property.
                                                 getPropertyStreetNumber());
        }

        if (!XMLUtil.isEmpty(property.getPropertyStreetName())) {
            propertyType.setPropertyStreetName(property.getPropertyStreetName());
        }

        if (!XMLUtil.isEmpty(property.getHeatTypeId())) {
            propertyType.setHeatTypeId(XMLUtil.int2BigInteger(property.
                    getHeatTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyAddressLine2())) {
            propertyType.setPropertyAddressLine2(property.
                                                 getPropertyAddressLine2());
        }

        if (!XMLUtil.isEmpty(property.getPropertyCity())) {
            propertyType.setPropertyCity(property.getPropertyCity());
        }

        if (!XMLUtil.isEmpty(property.getPropertyPostalFSA())) {
            propertyType.setPropertyPostalFSA(property.getPropertyPostalFSA());
        }

        if (!XMLUtil.isEmpty(property.getPropertyPostalLDU())) {
            propertyType.setPropertyPostalLDU(property.getPropertyPostalLDU());
        }

        if (!XMLUtil.isEmpty(property.getLegalLine1())) {
            propertyType.setLegalLine1(property.getLegalLine1());
        }

        if (!XMLUtil.isEmpty(property.getLegalLine2())) {
            propertyType.setLegalLine2(property.getLegalLine2());
        }

        if (!XMLUtil.isEmpty(property.getLegalLine3())) {
            propertyType.setLegalLine3(property.getLegalLine3());
        }

        if (!XMLUtil.isEmpty(property.getNumberOfBedrooms())) {
            propertyType.setNumberOfBedrooms(XMLUtil.int2BigInteger(property.
                    getNumberOfBedrooms()));
        }

        if (!XMLUtil.isEmpty(property.getYearBuilt())) {
            propertyType.setYearBuilt(XMLUtil.int2BigInteger(property.
                    getYearBuilt()));
        }

        propertyType.setInsulatedWithUFFI(XMLUtil.boolean2String(property.
                getInsulatedWithUFFI()));

        if (!XMLUtil.isEmpty(property.getProvinceId())) {
            propertyType.setProvinceId(XMLUtil.int2BigInteger(property.
                    getProvinceId()));
        }

        if (!XMLUtil.isEmpty(property.getMonthlyCondoFees())) {
            propertyType.setMonthlyCondoFees(XMLUtil.double2BigDecimal(
                    property.getMonthlyCondoFees()));
        }

        if (!XMLUtil.isEmpty(property.getPurchasePrice())) {
            propertyType.setPurchasePrice(XMLUtil.double2BigDecimal(property.
                    getPurchasePrice()));
        }

        if (!XMLUtil.isEmpty(property.getMunicipalityId())) {
            propertyType.setMunicipalityId(XMLUtil.int2BigInteger(property.
                    getMunicipalityId()));
        }

        if (!XMLUtil.isEmpty(property.getLandValue())) {
            propertyType.setLandValue(XMLUtil.double2BigDecimal(property.
                    getLandValue()));
        }

        if (!XMLUtil.isEmpty(property.getAppraisalSourceId())) {
            propertyType.setAppraisalSourceId(XMLUtil.int2BigInteger(property.
                    getAppraisalSourceId()));
        }

        if (!XMLUtil.isEmpty(property.getEquityAvailable())) {
            propertyType.setEquityAvailable(XMLUtil.double2BigDecimal(
                    property.getEquityAvailable()));
        }

        if (!XMLUtil.isEmpty(property.getNewConstructionId())) {
            propertyType.setNewConstructionId(XMLUtil.int2BigInteger(property.
                    getNewConstructionId()));
        }

        if (!XMLUtil.isEmpty(property.getEstimatedAppraisalValue())) {
            propertyType.setEstimatedAppraisalValue(XMLUtil.double2BigDecimal(
                    property.getEstimatedAppraisalValue()));
        }

        if (!XMLUtil.isEmpty(property.getActualAppraisalValue())) {
            propertyType.setActualAppraisalValue(XMLUtil.double2BigDecimal(
                    property.getActualAppraisalValue()));
        }

        if (!XMLUtil.isEmpty(property.getLoanToValue())) {
            propertyType.setLoanToValue(XMLUtil.double2BigDecimal(property.
                    getLoanToValue()));
        }

        if (!XMLUtil.isEmpty(property.getUnitNumber())) {
            propertyType.setUnitNumber(property.getUnitNumber());
        }

        if (!XMLUtil.isEmpty(property.getLotSize())) {
            propertyType.setLotSize(XMLUtil.int2BigInteger(property.
                    getLotSize()));
        }

        if (!XMLUtil.isEmpty(property.getPrimaryPropertyFlag())) {
            propertyType.setPrimaryPropertyFlag(property.
                                                getPrimaryPropertyFlag() + "");
        }

        if (!XMLUtil.isEmpty(property.getAppraisalDateAct())) {
            propertyType.setAppraisalDateAct(XMLUtil.date2Calendar(property.
                    getAppraisalDateAct()));
        }

        if (!XMLUtil.isEmpty(property.getLotSizeDepth())) {
            propertyType.setLotSizeDepth(XMLUtil.int2BigInteger(property.
                    getLotSizeDepth()));
        }

        if (!XMLUtil.isEmpty(property.getLotSizeFrontage())) {
            propertyType.setLotSizeFrontage(XMLUtil.int2BigInteger(property.
                    getLotSizeFrontage()));
        }

        if (!XMLUtil.isEmpty(property.getLotSizeUnitOfMeasureId())) {
            propertyType.setLotSizeUnitOfMeasureId(XMLUtil.int2BigInteger(
                    property.getLotSizeUnitOfMeasureId()));
        }

        if (!XMLUtil.isEmpty(property.getNumberOfUnits())) {
            propertyType.setNumberOfUnits(XMLUtil.int2BigInteger(property.
                    getNumberOfUnits()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyLocationId())) {
            propertyType.setPropertyLocationId(XMLUtil.int2BigInteger(
                    property.getPropertyLocationId()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyOccurenceNumber())) {
            propertyType.setPropertyOccurenceNumber(XMLUtil.int2BigInteger(
                    property.getPropertyOccurenceNumber()));
        }

        if (!XMLUtil.isEmpty(property.getPropertyReferenceNumber())) {
            propertyType.setPropertyReferenceNumber(property.
                    getPropertyReferenceNumber());
        }

        if (!XMLUtil.isEmpty(property.getSewageTypeId())) {
            propertyType.setSewageTypeId(XMLUtil.int2BigInteger(property.
                    getSewageTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getStreetDirectionId())) {
            propertyType.setStreetDirectionId(XMLUtil.int2BigInteger(property.
                    getStreetDirectionId()));
        }

        if (!XMLUtil.isEmpty(property.getWaterTypeId())) {
            propertyType.setWaterTypeId(XMLUtil.int2BigInteger(property.
                    getWaterTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getLivingSpace())) {
            propertyType.setLivingSpace(XMLUtil.int2BigInteger(property.
                    getLivingSpace()));
        }

        if (!XMLUtil.isEmpty(property.getLivingSpaceUnitOfMeasureId())) {
            propertyType.setLivingSpaceUnitOfMeasureId(XMLUtil.int2BigInteger(
                    property.getLivingSpaceUnitOfMeasureId()));
        }

        if (!XMLUtil.isEmpty(property.getStructureAge())) {
            propertyType.setStructureAge(XMLUtil.int2BigInteger(property.
                    getStructureAge()));
        }

        if (!XMLUtil.isEmpty(property.getDwellingStyleId())) {
            propertyType.setDwellingTypeId(XMLUtil.int2BigInteger(property.
                    getDwellingTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getLendingValue())) {
            propertyType.setLendingValue(XMLUtil.double2BigDecimal(property.
                    getLendingValue()));
        }

        if (!XMLUtil.isEmpty(property.getTotalPropertyExpenses())) {
            propertyType.setTotalPropertyExpenses(XMLUtil.double2BigDecimal(
                    property.getTotalPropertyExpenses()));
        }

        if (!XMLUtil.isEmpty(property.getTotalAnnualTaxAmount())) {
            propertyType.setTotalAnnualTaxAmount(XMLUtil.double2BigDecimal(
                    property.getTotalAnnualTaxAmount()));
        }

        if (!XMLUtil.isEmpty(property.getZoning())) {
            propertyType.setZoning(XMLUtil.int2BigInteger(property.getZoning()));
        }

        if (!XMLUtil.isEmpty(property.getMLSListingFlag())) {
            propertyType.setMLSListingFlag(property.getMLSListingFlag());
        }

        if (!XMLUtil.isEmpty(property.getGarageSizeId())) {
            propertyType.setGarageSizeId(XMLUtil.int2BigInteger(property.
                    getGarageSizeId()));
        }

        if (!XMLUtil.isEmpty(property.getGarageTypeId())) {
            propertyType.setGarageTypeId(XMLUtil.int2BigInteger(property.
                    getGarageTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getBuilderName())) {
            propertyType.setBuilderName(property.getBuilderName());
        }

        if (!XMLUtil.isEmpty(property.getTenureTypeId())) {
            propertyType.setTenureTypeId(XMLUtil.int2BigInteger(property.
                    getTenureTypeId()));
        }

        if (!XMLUtil.isEmpty(property.getMiEnergyEfficiency())) {
            propertyType.setMIEnergyEfficiency(property.getMiEnergyEfficiency());
        }

        if (!XMLUtil.isEmpty(property.getOnReserveTrustAgreementNumber())) {
            propertyType.setOnReserveTrustAgreementNumber(property.
                    getOnReserveTrustAgreementNumber());
        }

        if (!XMLUtil.isEmpty(property.getSubdivisionDiscount())) {
            propertyType.setSubdivisionDiscount(property.
                                                getSubdivisionDiscount());
        }
    }


    private void createPropertyExpense(PropertyType propertyType,
                                       Property property) throws Exception {
        List propertyExpenseTypes = propertyType.getPropertyExpense();
        Collection propertyExpenses = property.getPropertyExpenses();

        if (propertyExpenses != null) {
            for (Iterator iter = propertyExpenses.iterator(); iter.hasNext(); ) {
                PropertyExpense propertyExpense = (PropertyExpense) iter.next();
                PropertyExpenseType propertyExpenseType = objFactory.
                        createPropertyExpenseType();

                if (!XMLUtil.isEmpty(propertyExpense.getPropertyExpensePeriodId())) {
                    propertyExpenseType.setPropertyExpensePeriodId(XMLUtil.
                            int2BigInteger(propertyExpense.
                                           getPropertyExpensePeriodId()));
                }

                if (!XMLUtil.isEmpty(propertyExpense.getPropertyExpenseTypeId())) {
                    propertyExpenseType.setPropertyExpenseTypeId(XMLUtil.
                            int2BigInteger(propertyExpense.
                                           getPropertyExpenseTypeId()));
                }

                if (!XMLUtil.isEmpty(propertyExpense.getPropertyExpenseAmount())) {
                    propertyExpenseType.setPropertyExpenseAmount(XMLUtil.
                            double2BigDecimal(
                                    propertyExpense.
                                    getPropertyExpenseAmount()));
                }

                if (!XMLUtil.isEmpty(propertyExpense.getMonthlyExpenseAmount())) {
                    propertyExpenseType.setMonthlyExpenseAmount(XMLUtil.
                            double2BigDecimal(
                                    propertyExpense.
                                    getMonthlyExpenseAmount()));
                }

                propertyExpenseType.setPeIncludeInGDS(XMLUtil.boolean2String(
                        propertyExpense.
                        getPeIncludeInGDS()));

                propertyExpenseType.setPeIncludeInTDS(XMLUtil.boolean2String(
                        propertyExpense.
                        getPeIncludeInTDS()));

                if (!XMLUtil.isEmpty(propertyExpense.getPePercentInGDS())) {
                    propertyExpenseType.setPePercentInGDS(XMLUtil.
                            int2BigInteger(
                                    propertyExpense.getPePercentInGDS()));
                }

                if (!XMLUtil.isEmpty(propertyExpense.getPePercentInTDS())) {
                    propertyExpenseType.setPePercentInTDS(XMLUtil.
                            int2BigInteger(
                                    propertyExpense.getPePercentInTDS()));
                }

                propertyExpenseTypes.add(propertyExpenseType);

            }
        }
    }

    private void createBorrower(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
        List borrowerTypes = dealType.getBorrower();
        Collection borrowers = deal.getBorrowers();

        if (borrowers != null) {
            for (Iterator iter = borrowers.iterator(); iter.hasNext(); ) {
                Borrower borrower = (Borrower) iter.next();
                BorrowerType borrowerType = objFactory.createBorrowerType();

                populateBorrowerType(borrowerType, borrower);
                createBorrowerAddress(borrowerType, borrower);
                createIncome(borrowerType, borrower);
                createAsset(borrowerType, borrower);
                createLiability(borrowerType, borrower);
                createCreditReference(borrowerType, borrower);
                createEmploymentHistory(borrowerType, borrower);

                borrowerTypes.add(borrowerType);
            }
        }

    }

    private void populateBorrowerType(BorrowerType borrowerType,
                                      Borrower borrower) throws
            Exception {
        if (!XMLUtil.isEmpty(borrower.getBorrowerNumber())) {
            borrowerType.setBorrowerNumber(XMLUtil.int2BigInteger(borrower.
                    getBorrowerNumber()));
        }

        	//Covering this in a try catch because the code underneath in borrower looks a little off. - M
				try{

					borrowerType.setExistingClient(XMLUtil.boolean2String(borrower.isExistingClient()));
				}catch(Exception e){

					borrowerType.setExistingClient("N");
				}


        if (!XMLUtil.isEmpty(borrower.getBorrowerFirstName())) {
            borrowerType.setBorrowerFirstName(borrower.getBorrowerFirstName());
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerMiddleInitial())) {
            borrowerType.setBorrowerMiddleInitial(borrower.
                                                  getBorrowerMiddleInitial());
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerLastName())) {
            borrowerType.setBorrowerLastName(borrower.getBorrowerLastName());
        }

        if (!XMLUtil.isEmpty(borrower.getFirstTimeBuyer())) {
            borrowerType.setFirstTimeBuyer(borrower.getFirstTimeBuyer());
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerBirthDate())) {
            borrowerType.setBorrowerBirthDate(XMLUtil.date2Calendar(borrower.
                    getBorrowerBirthDate()));
        }

        if (!XMLUtil.isEmpty(borrower.getSalutationId())) {
            borrowerType.setSalutationId(XMLUtil.int2BigInteger(borrower.
                    getSalutationId()));
        }

        if (!XMLUtil.isEmpty(borrower.getMaritalStatusId())) {
            borrowerType.setMaritalStatusId(XMLUtil.int2BigInteger(borrower.
                    getMaritalStatusId()));
        }

        if (!XMLUtil.isEmpty(borrower.getSocialInsuranceNumber())) {
            borrowerType.setSocialInsuranceNumber(borrower.
                                                  getSocialInsuranceNumber());
        }

        if (!XMLUtil.isEmpty(borrower.getClientReferenceNumber())) {
            borrowerType.setClientReferenceNumber(borrower.
                                                  getClientReferenceNumber());
        }

        if (!XMLUtil.isEmpty(borrower.getNumberOfDependents())) {
            borrowerType.setNumberOfDependents(XMLUtil.int2BigInteger(borrower.
                    getNumberOfDependents()));
        }

        if (!XMLUtil.isEmpty(borrower.getNumberOfTimesBankrupt())) {
            borrowerType.setNumberOfTimesBankrupt(XMLUtil.int2BigInteger(
                    borrower.
                    getNumberOfTimesBankrupt()));
        }

        if (!XMLUtil.isEmpty(borrower.getCreditScore())) {
            borrowerType.setCreditScore(XMLUtil.int2BigInteger(borrower.
                    getCreditScore()));
        }

        if (!XMLUtil.isEmpty(borrower.getGDS())) {
            borrowerType.setGDS(XMLUtil.double2BigDecimal(borrower.getGDS()));
        }

        if (!XMLUtil.isEmpty(borrower.getTDS())) {
            borrowerType.setTDS(XMLUtil.double2BigDecimal(borrower.getTDS()));
        }

        if (!XMLUtil.isEmpty(borrower.getNetWorth())) {
            borrowerType.setNetWorth(XMLUtil.double2BigDecimal(borrower.
                    getNetWorth()));
        }

/*        if (!XMLUtil.isEmpty(borrower.getExistingClient())) {
            borrowerType.setExistingClient(borrower.getExistingClient());
        }*/

        if (!XMLUtil.isEmpty(borrower.getLanguagePreferenceId())) {
            borrowerType.setLanguagePreferenceId(XMLUtil.int2BigInteger(
                    borrower.
                    getLanguagePreferenceId()));
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerGeneralStatusId())) {
            borrowerType.setBorrowerGeneralStatusId(XMLUtil.int2BigInteger(
                    borrower.
                    getBorrowerGeneralStatusId()));
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerTypeId())) {
            borrowerType.setBorrowerTypeId(XMLUtil.int2BigInteger(borrower.
                    getBorrowerTypeId()));
        }

        if (!XMLUtil.isEmpty(borrower.getCitizenshipTypeId())) {
            borrowerType.setCitizenshipTypeId(XMLUtil.int2BigInteger(borrower.
                    getCitizenshipTypeId()));
        }

        if (!XMLUtil.isEmpty(borrower.getPrimaryBorrowerFlag())) {
            borrowerType.setPrimaryBorrowerFlag(borrower.getPrimaryBorrowerFlag());
        }

        if (!XMLUtil.isEmpty(borrower.getTotalAssetAmount())) {
            borrowerType.setTotalAssetAmount(XMLUtil.double2BigDecimal(borrower.
                    getTotalAssetAmount()));
        }

        if (!XMLUtil.isEmpty(borrower.getTotalLiabilityAmount())) {
            borrowerType.setTotalLiabilityAmount(XMLUtil.double2BigDecimal(
                    borrower.
                    getTotalLiabilityAmount()));
        }

        if (!XMLUtil.isEmpty(borrower.getPaymentHistoryTypeId())) {
            borrowerType.setPaymentHistoryTypeId(XMLUtil.int2BigInteger(
                    borrower.
                    getPaymentHistoryTypeId()));
        }

        if (!XMLUtil.isEmpty(borrower.getTotalIncomeAmount())) {
            borrowerType.setTotalIncomeAmount(XMLUtil.double2BigDecimal(
                    borrower.
                    getTotalIncomeAmount()));
        }

        if (!XMLUtil.isEmpty(borrower.getBankruptcyStatusId())) {
            borrowerType.setBankruptcyStatusId(XMLUtil.int2BigInteger(borrower.
                    getBankruptcyStatusId()));
        }

        if (!XMLUtil.isEmpty(borrower.getAge())) {
            borrowerType.setAge(XMLUtil.int2BigInteger(borrower.getAge()));
        }

        if (borrower.getStaffOfLender()) {
            borrowerType.setStaffOfLender(XMLUtil.boolean2String(borrower.
                getStaffOfLender()));
        }

        if (!XMLUtil.isEmpty(borrower.getTotalLiabilityPayments())) {
            borrowerType.setTotalLiabilityPayments(XMLUtil.double2BigDecimal(
                    borrower.getTotalLiabilityPayments()));
        }

        if (!XMLUtil.isEmpty(borrower.getGDS3Year())) {
            borrowerType.setGDS3Year(XMLUtil.double2BigDecimal(borrower.
                    getGDS3Year()));
        }

        if (!XMLUtil.isEmpty(borrower.getTDS3Year())) {
            borrowerType.setTDS3Year(XMLUtil.double2BigDecimal(borrower.
                    getTDS3Year()));
        }

        if (!XMLUtil.isEmpty(borrower.getSINCheckDigit())) {
            borrowerType.setSINCheckDigit(borrower.getSINCheckDigit());
        }

        if (!XMLUtil.isEmpty(borrower.getCreditBureauNameId())) {
            borrowerType.setCreditBureauNameId(XMLUtil.int2BigInteger(borrower.
                    getCreditBureauNameId()));
        }

        if (!XMLUtil.isEmpty(borrower.getBorrowerGenderId())) {
            borrowerType.setBorrowerGenderId(XMLUtil.int2BigInteger(borrower.
                    getBorrowerGenderId()));
        }

        if (!XMLUtil.isEmpty(borrower.getGuarantorOtherLoans())) {
            borrowerType.setGuarantorOtherLoans(borrower.getGuarantorOtherLoans());
        }
    }

    private void createBorrowerAddress(BorrowerType borrowerType,
                                       Borrower borrower) throws Exception {
        List borrowerAddressTypes = borrowerType.getBorrowerAddress();
        Collection borrowerAddresses = borrower.getBorrowerAddresses();

        if (borrowerAddresses != null) {
            for (Iterator iter = borrowerAddresses.iterator(); iter.hasNext(); ) {
                BorrowerAddress borrowerAddress = (BorrowerAddress) iter.
                                                  next();
                BorrowerAddressType borrowerAddressType = objFactory.
                        createBorrowerAddressType();
                Addr addr = borrowerAddress.getAddr();

                if (addr != null) {
                    borrowerAddressType.setAddr(createAddrType(addr));
                }

                if (!XMLUtil.isEmpty(borrowerAddress.getBorrowerAddressTypeId())) {
                    borrowerAddressType.setBorrowerAddressTypeId(XMLUtil.
                            int2BigInteger(borrowerAddress.
                                           getBorrowerAddressTypeId()));
                }

                if (!XMLUtil.isEmpty(borrowerAddress.getMonthsAtAddress())) {
                    borrowerAddressType.setMonthsAtAddress(XMLUtil.
                            int2BigInteger(
                                    borrowerAddress.getMonthsAtAddress()));
                }

                if (!XMLUtil.isEmpty(borrowerAddress.getResidentialStatusId())) {
                    borrowerAddressType.setResidentialStatusId(XMLUtil.
                            int2BigInteger(borrowerAddress.
                                           getResidentialStatusId()));
                }

                borrowerAddressTypes.add(borrowerAddressType);
            }
        }
    }

    private AddrType createAddrType(Addr addr) throws Exception {
        AddrType addrType = objFactory.createAddrType();

        if (!XMLUtil.isEmpty(addr.getAddressLine1())) {
            addrType.setAddressLine1(addr.getAddressLine1());
        }

        if (!XMLUtil.isEmpty(addr.getAddressLine2())) {
            addrType.setAddressLine2(addr.getAddressLine2());
        }

        if (!XMLUtil.isEmpty(addr.getCity())) {
            addrType.setCity(addr.getCity());
        }

        if (!XMLUtil.isEmpty(addr.getPostalFSA())) {
            addrType.setPostalFSA(addr.getPostalFSA());
        }

        if (!XMLUtil.isEmpty(addr.getPostalLDU())) {
            addrType.setPostalLDU(addr.getPostalLDU());
        }

        if (!XMLUtil.isEmpty(addr.getProvinceId())) {
            addrType.setProvinceId(XMLUtil.int2BigInteger(addr.getProvinceId()));
        }

        //addrt.setStreetDirectionId(addr.getStreetDirection());// No DB entry
        //addrt.setStreetName();// No DB entry
        //addrt.setStreetNumber();// No DB entry
        //addrt.setStreetTypeId();// No DB entry
        //addrt.setUnitNumber(addr.getUnitNumber());// No DB entry
        return addrType;

    }

    private void createIncome(BorrowerType borrowerType, Borrower borrower) throws
            Exception {
        List incomeTypes = borrowerType.getIncome();
        Collection incomes = borrower.getIncomes();

        if (incomes != null) {
            for (Iterator iter = incomes.iterator(); iter.hasNext(); ) {
                IncomeType incomeType = objFactory.createIncomeType();
                Income income = (Income) iter.next();

                if (!XMLUtil.isEmpty(income.getAnnualIncomeAmount())) {
                    incomeType.setAnnualIncomeAmount(XMLUtil.double2BigDecimal(
                            income.getAnnualIncomeAmount()));
                }

                incomeType.setIncIncludeInGDS(XMLUtil.boolean2String(income.
                        getIncIncludeInGDS()));
                incomeType.setIncIncludeInTDS(XMLUtil.boolean2String(income.
                        getIncIncludeInTDS()));

                if (!XMLUtil.isEmpty(income.getIncomeAmount())) {
                    incomeType.setIncomeAmount(XMLUtil.double2BigDecimal(income.
                            getIncomeAmount()));
                }

                if (!XMLUtil.isEmpty(income.getIncomeDescription())) {
                    incomeType.setIncomeDescription(income.getIncomeDescription());
                }

                if (!XMLUtil.isEmpty(income.getIncomeId())) {
                    incomeType.setIncomeId(XMLUtil.int2BigInteger(income.
                            getIncomeId()));
                }

                if (!XMLUtil.isEmpty(income.getIncomePeriodId())) {
                    incomeType.setIncomePeriodId(XMLUtil.int2BigInteger(income.
                            getIncomePeriodId()));
                }

                if (!XMLUtil.isEmpty(income.getIncomeTypeId())) {
                    incomeType.setIncomeTypeId(XMLUtil.int2BigInteger(income.
                            getIncomeTypeId()));
                }

                if (!XMLUtil.isEmpty(income.getIncPercentInGDS())) {
                    incomeType.setIncPercentInGDS(XMLUtil.int2BigInteger(income.
                            getIncPercentInGDS()));
                }

                if (!XMLUtil.isEmpty(income.getIncPercentInTDS())) {
                    incomeType.setIncPercentInTDS(XMLUtil.int2BigInteger(income.
                            getIncPercentInTDS()));
                }

                if (!XMLUtil.isEmpty(income.getMonthlyIncomeAmount())) {
                    incomeType.setMonthlyIncomeAmount(XMLUtil.double2BigDecimal(
                            income.getMonthlyIncomeAmount()));
                }

                incomeTypes.add(incomeType);
            }
        }

    }

    private void createAsset(BorrowerType borrowerType, Borrower borrower) throws
            Exception {
        List assetTypes = borrowerType.getAsset();
        Collection assets = borrower.getAssets();

        if (assets != null) {
            for (Iterator iter = assets.iterator(); iter.hasNext(); ) {
                AssetType assetType = objFactory.createAssetType();
                Asset asset = (Asset) iter.next();

                if (!XMLUtil.isEmpty(asset.getAssetValue())) {
                    assetType.setAssetValue(XMLUtil.double2BigDecimal(asset.
                            getAssetValue()));
                }

                if (!XMLUtil.isEmpty(asset.getAssetTypeId())) {
                    assetType.setAssetTypeId(XMLUtil.int2BigInteger(asset.
                            getAssetTypeId()));
                }

                if (!XMLUtil.isEmpty(asset.getPercentInNetWorth())) {
                    assetType.setIncludeInNetWorth(XMLUtil.boolean2String(asset.
                            getIncludeInNetWorth()));
                }

                if (!XMLUtil.isEmpty(asset.getPercentInNetWorth())) {
                    assetType.setPercentInNetWorth(XMLUtil.int2BigInteger(asset.
                            getPercentInNetWorth()));
                }

                assetTypes.add(assetType);
            }
        }
    }

    private void createLiability(BorrowerType borrowerType,
                                 Borrower borrower) throws
            Exception {
        List liabilityTypes = borrowerType.getLiability();
        Collection liabilities = borrower.getLiabilities();

        if (liabilities != null) {

            for (Iterator iter = liabilities.iterator(); iter.hasNext(); ) {
                Liability liability = (Liability) iter.next();
                LiabilityType liabilityType = objFactory.
                                              createLiabilityType();

                liabilityType.setIncludeInGDS(XMLUtil.boolean2String(
                        liability.getIncludeInGDS()));

                liabilityType.setIncludeInTDS(XMLUtil.boolean2String(
                        liability.
                        getIncludeInTDS()));

                if (!XMLUtil.isEmpty(liability.getLiabilityAmount())) {
                    liabilityType.setLiabilityAmount(XMLUtil.double2BigDecimal(
                            liability.getLiabilityAmount()));
                }

                if (!XMLUtil.isEmpty(liability.getLiabilityDescription())) {
                    liabilityType.setLiabilityDescription(liability.
                            getLiabilityDescription());
                }

                if (!XMLUtil.isEmpty(liability.getLiabilityMonthlyPayment())) {
                    liabilityType.setLiabilityMonthlyPayment(XMLUtil.
                            double2BigDecimal(liability.
                                              getLiabilityMonthlyPayment()));
                }

                if (!XMLUtil.isEmpty(liability.getLiabilityPaymentQualifier())) {
                    liabilityType.setLiabilityPaymentQualifier(liability.
                            getLiabilityPaymentQualifier());
                }

                if (!XMLUtil.isEmpty(liability.getLiabilityPayOffTypeId())) {
                    liabilityType.setLiabilityPayOffTypeId(XMLUtil.
                            int2BigInteger(
                                    liability.getLiabilityPayOffTypeId()));
                }

                if (!XMLUtil.isEmpty(liability.getLiabilityTypeId())) {
                    liabilityType.setLiabilityTypeId(XMLUtil.int2BigInteger(
                            liability.getLiabilityTypeId()));
                }

                if (!XMLUtil.isEmpty(liability.getPercentInGDS())) {
                    liabilityType.setPercentInGDS(XMLUtil.double2BigDecimal(
                            liability.getPercentInGDS()));
                }

                if (!XMLUtil.isEmpty(liability.getPercentInTDS())) {
                    liabilityType.setPercentInTDS(XMLUtil.double2BigDecimal(
                            liability.getPercentInTDS()));
                }

                if (!XMLUtil.isEmpty(liability.getPercentOutGDS())) {
                    liabilityType.setPercentOutGDS(XMLUtil.double2BigDecimal(
                            liability.getPercentOutGDS()));
                }

                liabilityTypes.add(liabilityType);
            }
        }

    }

    private void createCreditReference(BorrowerType borrowerType,
                                       Borrower borrower) throws
            Exception {
        List creditReferenceTypes = borrowerType.getCreditReference();
        Collection creditReferences = borrower.getCreditReferences();

        if (creditReferences != null) {
            for (Iterator iter = creditReferences.iterator(); iter.hasNext(); ) {
                CreditReferenceType creditReferenceType = objFactory.
                        createCreditReferenceType();
                CreditReference creditReference = (CreditReference) iter.
                                                  next();

                if (!XMLUtil.isEmpty(creditReference.getAccountNumber())) {
                    creditReferenceType.setAccountNumber(creditReference.
                            getAccountNumber());
                }

                if (!XMLUtil.isEmpty(creditReference.
                                     getCreditReferenceDescription())) {
                    creditReferenceType.setCreditReferenceDescription(
                            creditReference.getCreditReferenceDescription());
                }

                if (!XMLUtil.isEmpty(creditReference.getCreditRefTypeId())) {
                    creditReferenceType.setCreditRefTypeId(XMLUtil.
                            int2BigInteger(
                                    creditReference.getCreditRefTypeId()));
                }

                if (!XMLUtil.isEmpty(creditReference.getCurrentBalance())) {
                    creditReferenceType.setCurrentBalance(XMLUtil.
                            double2BigDecimal(
                                    creditReference.
                                    getCurrentBalance()));
                }

                if (!XMLUtil.isEmpty(creditReference.getInstitutionName())) {
                    creditReferenceType.setInstitutionName(creditReference.
                            getInstitutionName());
                }

                if (!XMLUtil.isEmpty(creditReference.getTimeWithReference())) {
                    creditReferenceType.setTimeWithReference(XMLUtil.
                            int2BigInteger(
                                    creditReference.getTimeWithReference()));
                }

                creditReferenceTypes.add(creditReferenceType);
            }
        }

    }

    private void createEmploymentHistory(BorrowerType borrowerType,
                                         Borrower borrower) throws
            Exception {
        List employmentHistoryTypes = borrowerType.getEmploymentHistory();
        Collection employmentHistories = borrower.getEmploymentHistories();

        if (employmentHistories != null) {
            for (Iterator iter = employmentHistories.iterator();
                                 iter.hasNext(); ) {
                EmploymentHistoryType employmentHistoryType = objFactory.
                        createEmploymentHistory();
                EmploymentHistory employmentHistory = (EmploymentHistory)
                        iter.next();

                if (!XMLUtil.isEmpty(employmentHistory.getEmployerName())) {
                    employmentHistoryType.setEmployerName(employmentHistory.
                            getEmployerName());
                }

                if (!XMLUtil.isEmpty(employmentHistory.getEmploymentHistoryId())) {
                    employmentHistoryType.setEmploymentHistoryStatusId(XMLUtil.
                            int2BigInteger(employmentHistory.
                                           getEmploymentHistoryStatusId()));
                }

                if (!XMLUtil.isEmpty(employmentHistory.
                                     getEmploymentHistoryTypeId())) {
                    employmentHistoryType.setEmploymentHistoryTypeId(XMLUtil.
                            int2BigInteger(employmentHistory.
                                           getEmploymentHistoryTypeId()));
                }

                if (!XMLUtil.isEmpty(employmentHistory.getIncomeId())) {
                    employmentHistoryType.setIncomeId(XMLUtil.int2BigInteger(
                            employmentHistory.getIncomeId()));
                }

                if (!XMLUtil.isEmpty(employmentHistory.getIndustrySectorId())) {
                    employmentHistoryType.setIndustrySectorId(XMLUtil.
                            int2BigInteger(employmentHistory.
                                           getIndustrySectorId()));
                }

                if (!XMLUtil.isEmpty(employmentHistory.getJobTitle())) {
                    employmentHistoryType.setJobTitleId(XMLUtil.int2BigInteger(
                            employmentHistory.getJobTitleId()));
                }

                if (!XMLUtil.isEmpty(employmentHistory.getMonthsOfService())) {
                    employmentHistoryType.setMonthsOfService(XMLUtil.
                            int2BigInteger(
                                    employmentHistory.getMonthsOfService()));
                }

                if (!XMLUtil.isEmpty(employmentHistory.getOccupationId())) {
                    employmentHistoryType.setOccupationId(XMLUtil.
                            int2BigInteger(
                                    employmentHistory.
                                    getOccupationId()));
                }

                Contact contact = employmentHistory.getContact();

                if (contact != null) {
                    employmentHistoryType.setContact(createContact(contact));
                }

                employmentHistoryTypes.add(employmentHistoryType);
            }
        }

    }

    private void createDownPaymentSource(DealType dealType, SessionResourceKit srk, Deal deal) throws
            Exception {
        List downPaymentSourceTypes = dealType.getDownPaymentSource();
        Collection downPaymentSources = deal.getDownPaymentSources();

        if (downPaymentSources != null) {
            for (Iterator iter = downPaymentSources.iterator(); iter.hasNext(); ) {
                DownPaymentSource downPaymentSource = (DownPaymentSource) iter.
                        next();
                DownPaymentSourceType downPaymentSourceType = objFactory.
                        createDownPaymentSourceType();

                if (!XMLUtil.isEmpty(downPaymentSource.
                                     getDownPaymentSourceTypeId())) {
                    downPaymentSourceType.setDownPaymentSourceTypeId(XMLUtil.
                            int2BigInteger(downPaymentSource.
                                           getDownPaymentSourceTypeId()));
                }

                if (!XMLUtil.isEmpty(downPaymentSource.getAmount())) {
                    downPaymentSourceType.setAmount(XMLUtil.double2BigDecimal(
                            downPaymentSource.
                            getAmount()));
                }

                if (!XMLUtil.isEmpty(downPaymentSource.getDPSDescription())) {
                    downPaymentSourceType.setDPSDescription(downPaymentSource.
                            getDPSDescription());
                }

                downPaymentSourceTypes.add(downPaymentSourceType);
            }
        }
    }

    private void createEscrowPayment(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
        List escrowPaymentTypes = dealType.getEscrowPayment();
        Collection escrowPayments = deal.getEscrowPayments();

        if (escrowPayments != null) {
            for (Iterator iter = escrowPayments.iterator(); iter.hasNext(); ) {
                EscrowPayment escrowPayment = (EscrowPayment) iter.next();
                EscrowPaymentType escrowPaymentType = objFactory.
                        createEscrowPaymentType();

                if (!XMLUtil.isEmpty(escrowPayment.getEscrowTypeId())) {
                    escrowPaymentType.setEscrowTypeId(XMLUtil.int2BigInteger(
                            escrowPayment.getEscrowTypeId()));
                }

                if (!XMLUtil.isEmpty(escrowPayment.getEscrowPaymentDescription())) {
                    escrowPaymentType.setEscrowPaymentDescription(escrowPayment.
                            getEscrowPaymentDescription());
                }

                if (!XMLUtil.isEmpty(escrowPayment.getEscrowPaymentAmount())) {
                    escrowPaymentType.setEscrowPaymentAmount(XMLUtil.
                            double2BigDecimal(escrowPayment.
                                              getEscrowPaymentAmount()));
                }

                escrowPaymentTypes.add(escrowPaymentType);
            }
        }
    }

    private void createBranchProfile(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
        BranchProfileType branchProfileType = objFactory.createBranchProfile();
        BranchProfile branchProfile = new BranchProfile(srk,
                deal.getBranchProfileId());
        branchProfileType.setAIGUGBranchTransitNumber(branchProfile.
                getAIGUGBranchTransitNumber());

        if (!XMLUtil.isEmpty(branchProfile.getBPBusinessId())) {
            branchProfileType.setBPBusinessId(branchProfile.getBPBusinessId());
        }

        if (!XMLUtil.isEmpty(branchProfile.getBPShortName())) {
            branchProfileType.setBPShortName(branchProfile.getBPShortName());
        }

        if (!XMLUtil.isEmpty(branchProfile.getBranchName())) {
            branchProfileType.setBranchName(branchProfile.getBranchName());
        }

        if (!XMLUtil.isEmpty(branchProfile.getBranchTypeId())) {
            branchProfileType.setBranchTypeId(XMLUtil.int2BigInteger(
                    branchProfile.
                    getBranchTypeId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getPartyProfileId())) {
            branchProfileType.setPartyProfileId(XMLUtil.int2BigInteger(
                    branchProfile.
                    getPartyProfileId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getTimeZoneEntryId())) {
            branchProfileType.setTimeZoneEntryId(XMLUtil.int2BigInteger(
                    branchProfile.
                    getTimeZoneEntryId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getRegionProfileId())) {
            branchProfileType.setRegionProfileId(XMLUtil.int2BigInteger(
                    branchProfile.
                    getRegionProfileId()));
        }

        Contact contact = branchProfile.getContact();

        if (contact != null) {
            branchProfileType.setContact(createContact(contact));
        }

        dealType.setBranchProfile(branchProfileType);
    }

    private void createInstitutionProfile(DealType dealType, SessionResourceKit srk, Deal deal) throws
            Exception {
        InstitutionProfile institutionProfile = new InstitutionProfile(srk);
        institutionProfile = institutionProfile.findByPrimaryKey(new InstitutionProfilePK(deal.getInstitutionProfileId()));
    	InstitutionProfileType institutionProfileType = objFactory.
                createInstitutionProfileType();
        institutionProfileType.setAIGUGId(institutionProfile.getAIGUGId());

        if (!XMLUtil.isEmpty(institutionProfile.getBrandName())) {
            institutionProfileType.setBrandName(institutionProfile.getBrandName());
        }

        institutionProfileType.setInstitutionName(institutionProfile.
                                                  getInstitutionName());

        dealType.setInstitutionProfile(institutionProfileType);
    }

    private void createLenderProfile(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
        LenderProfileType lenderProfileType = objFactory.createLenderProfile();
        LenderProfile lenderProfile = deal.getLenderProfile();

        if (!XMLUtil.isEmpty(lenderProfile.getDefaultInvestorId())) {
            lenderProfileType.setDefaultInvestorId(XMLUtil.int2BigInteger(
                    lenderProfile.
                    getDefaultInvestorId()));
        }

        if (!XMLUtil.isEmpty(lenderProfile.getDefaultLender())) {
            lenderProfileType.setDefaultLender(lenderProfile.getDefaultLender() +
                                               "");
        }

        if (!XMLUtil.isEmpty(lenderProfile.getLenderName())) {
            lenderProfileType.setLenderName(lenderProfile.getLenderName());
        }

        if (!XMLUtil.isEmpty(lenderProfile.getLPBusinessId())) {
            lenderProfileType.setLpBusinessId(lenderProfile.getLPBusinessId());
        }

        if (!XMLUtil.isEmpty(lenderProfile.getLPShortName())) {
            lenderProfileType.setLpShortName(lenderProfile.getLPShortName());
        }

        if (!XMLUtil.isEmpty(lenderProfile.getPrivateLabelIndicator())) {
            lenderProfileType.setPrivateLabelIndicator(lenderProfile.
                    getPrivateLabelIndicator());
        }

        if (!XMLUtil.isEmpty(lenderProfile.getProfileStatusId())) {
            lenderProfileType.setProfileStatusId(XMLUtil.int2BigInteger(
                    lenderProfile.
                    getProfileStatusId()));
        }

        if (!XMLUtil.isEmpty(lenderProfile.getRegistrationName())) {
            lenderProfileType.setRegistrationName(lenderProfile.
                                                  getRegistrationName());
        }

        lenderProfileType.setUpfrontMIAllowed(XMLUtil.boolean2String(
                lenderProfile.
                getUpfrontMIAllowed()));

        Contact contact = lenderProfile.getContact();

        if (contact != null) {
            lenderProfileType.setContact(createContact(contact));
        }

        dealType.setLenderProfile(lenderProfileType);
    }

    private void createUnderwriter(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
        UserProfile userProfile = new UserProfile(srk);
        userProfile.findByPrimaryKey(new UserProfileBeanPK(deal.getUnderwriterUserId(), 
                                                           deal.getInstitutionProfileId()));

        if (userProfile != null) {
            Contact contact = userProfile.getContact();

            if (contact != null) {
                UnderwriterType underwriterType = objFactory.createUnderwriter();
                underwriterType.setContact(createContact(contact));
                dealType.setUnderwriter(underwriterType);
            }
        }
    }

    private void createMtgProd(DealType dealType, SessionResourceKit srk, Deal deal) throws Exception {
        MtgProd mtgProd = deal.getMtgProd();

        if (mtgProd != null) {
            MtgProdType mtgProdType = objFactory.createMtgProd();
            populateMtgProdType(mtgProdType, mtgProd);

            if (mtgProd.getPricingProfile() != null) {
                mtgProdType.setPricingProfile(createPricingProfileType(mtgProd.
                        getPricingProfile()));
            }

            dealType.setMtgProd(mtgProdType);
        }
    }

    private void populateMtgProdType(MtgProdType mtgProdType,
                                     MtgProd mtgProd) throws
            Exception {

        if (!XMLUtil.isEmpty(mtgProd.getInterestTypeId())) {
            mtgProdType.setInterestTypeId(XMLUtil.int2BigInteger(mtgProd.
                    getInterestTypeId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMtgProdName())) {
            mtgProdType.setMtgProdName(mtgProd.getMtgProdName());
        }

        if (!XMLUtil.isEmpty(mtgProd.getMinimumAmount())) {
            mtgProdType.setMinimumAmount(XMLUtil.double2BigDecimal(mtgProd.
                    getMinimumAmount()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMaximumAmount())) {
            mtgProdType.setMaximumAmount(XMLUtil.double2BigDecimal(mtgProd.
                    getMaximumAmount()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMinimumLTV())) {
            mtgProdType.setMinimumLTV(XMLUtil.double2BigDecimal(mtgProd.
                    getMinimumLTV()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMaximumLTV())) {
            mtgProdType.setMaximumLTV(XMLUtil.double2BigDecimal(mtgProd.
                    getMaximumLTV()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getPaymentTermId())) {
            mtgProdType.setPaymentTermId(XMLUtil.int2BigInteger(mtgProd.
                    getPaymentTermId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getLineOfBusinessId())) {
            mtgProdType.setLineOfBusinessId(XMLUtil.int2BigInteger(mtgProd.
                    getLineOfBusinessId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getCommitmentTerm())) {
            mtgProdType.setCommitmentTerm(mtgProd.getCommitmentTerm());
        }

        if (!XMLUtil.isEmpty(mtgProd.getMPBusinessId())) {
            mtgProdType.setMpBusinessId(mtgProd.getMPBusinessId());
        }

        if (!XMLUtil.isEmpty(mtgProd.getPrePaymentOptionsId())) {
            mtgProdType.setPrePaymentOptionsId(XMLUtil.int2BigInteger(mtgProd.
                    getPrePaymentOptionsId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getPrivilegePaymentId())) {
            mtgProdType.setPrivilegePaymentId(XMLUtil.int2BigInteger(mtgProd.
                    getPrivilegePaymentId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMPShortName())) {
            mtgProdType.setMpShortName(mtgProd.getMPShortName());
        }

        if (!XMLUtil.isEmpty(mtgProd.getInterestCompoundingId())) {
            mtgProdType.setInterestCompoundingId(XMLUtil.int2BigInteger(mtgProd.
                    getInterestCompoundingId()));
        }

        if (!XMLUtil.isEmpty(mtgProd.getMiscellaneousRate())) {
            mtgProdType.setMiscellaneousRate(XMLUtil.double2BigDecimal(mtgProd.
                    getMiscellaneousRate()));
        }

    }

    private PricingProfileType createPricingProfileType(PricingProfile
            pricingProfile) throws Exception {
        PricingProfileType pricingProfileType = objFactory.
                                                createPricingProfile();

        if (!XMLUtil.isEmpty(pricingProfile.getProfileStatusId())) {
            pricingProfileType.setProfileStatusId(XMLUtil.int2BigInteger(
                    pricingProfile.getProfileStatusId()));
        }

        if (!XMLUtil.isEmpty(pricingProfile.getPPBusinessId())) {
            pricingProfileType.setPpBusinessId(pricingProfile.getPPBusinessId());
        }

        // questionable int - > double -- down-cast
        pricingProfileType.setInterestRateChangeFrequency(XMLUtil.int2BigInteger((int)pricingProfile.getInterestRateChangeFrequency()));
        pricingProfileType.setPricingRegistrationTerm(XMLUtil.int2BigInteger((int)pricingProfile.getPricingRegistrationTerm()));

        pricingProfileType.setPrimeIndexIndicator(XMLUtil.boolean2String(
                pricingProfile.getPrimeIndexIndicator()));

        if (!XMLUtil.isEmpty(pricingProfile.getPricingStatusId())) {
            pricingProfileType.setPricingStatusId(XMLUtil.int2BigInteger(
                    pricingProfile.getPricingStatusId()));
        }

        if (!XMLUtil.isEmpty(pricingProfile.getRateCode())) {
            pricingProfileType.setRateCode(pricingProfile.getRateCode());
        }

        return pricingProfileType;
    }

    private void createSourceFirmProfile(DealType dealType, SessionResourceKit srk, Deal deal) throws
            Exception {
        SourceFirmProfile sourceFirmProfile = deal.getSourceFirmProfile();

        if (sourceFirmProfile != null) {
            SourceFirmProfileType sourceFirmProfileType = objFactory.
                    createSourceFirmProfile();

            if (!XMLUtil.isEmpty(sourceFirmProfile.getProfileStatusId())) {
                sourceFirmProfileType.setProfileStatusId(XMLUtil.int2BigInteger(
                        sourceFirmProfile.getProfileStatusId()));
            }

            if (!XMLUtil.isEmpty(sourceFirmProfile.getSfBusinessId())) {
                sourceFirmProfileType.setSfBusinessId(sourceFirmProfile.
                        getSfBusinessId());
            }

            if (!XMLUtil.isEmpty(sourceFirmProfile.getSfMOProfileId())) {
                sourceFirmProfileType.setSfMOProfileId(XMLUtil.int2BigInteger(
                        sourceFirmProfile.getSfMOProfileId()));
            }

            if (!XMLUtil.isEmpty(sourceFirmProfile.getSfShortName())) {
                sourceFirmProfileType.setSfShortName(sourceFirmProfile.
                        getSfShortName());
            }

            if (!XMLUtil.isEmpty(sourceFirmProfile.getSourceFirmCode())) {
                sourceFirmProfileType.setSourceFirmCode(sourceFirmProfile.
                        getSourceFirmCode());
            }

            if (!XMLUtil.isEmpty(sourceFirmProfile.getSourceFirmName())) {
                sourceFirmProfileType.setSourceFirmName(sourceFirmProfile.
                        getSourceFirmName());
            }

            if (!XMLUtil.isEmpty(sourceFirmProfile.getSystemTypeId())) {
                sourceFirmProfileType.setSystemTypeId(XMLUtil.int2BigInteger(
                        sourceFirmProfile.getSystemTypeId()));
            }

            Contact contact = sourceFirmProfile.getContact();

            if (contact != null) {
                sourceFirmProfileType.setContact(createContact(contact));
            }

            dealType.setSourceFirmProfile(sourceFirmProfileType);
        }
    }

    private void createSourceOfBusinessProfile(DealType dealType, SessionResourceKit srk, Deal deal) throws
            Exception {
        SourceOfBusinessProfile sourceOfBusinessProfile = deal.
                getSourceOfBusinessProfile();

        if (sourceOfBusinessProfile != null) {
            SourceOfBusinessProfileType sourceOfBusinessProfileType =
                    objFactory.
                    createSourceOfBusinessProfile();

            if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getPartyProfileId())) {
                sourceOfBusinessProfileType.setPartyProfileId(XMLUtil.
                        int2BigInteger(
                                sourceOfBusinessProfile.getPartyProfileId()));
            }

            if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSOBPBusinessId())) {
                sourceOfBusinessProfileType.setSOBPBusinessId(
                        sourceOfBusinessProfile.
                        getSOBPBusinessId());
            }

            if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSOBPShortName())) {
                sourceOfBusinessProfileType.setSOBPShortName(
                        sourceOfBusinessProfile.
                        getSOBPShortName());
            }

            if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSOBRegionId())) {
                sourceOfBusinessProfileType.setSOBRegionId(XMLUtil.
                        int2BigInteger(
                                sourceOfBusinessProfile.getSOBRegionId()));
            }

            if (!XMLUtil.isEmpty(sourceOfBusinessProfile.
                                 getSourceOfBusinessCategoryId())) {
                sourceOfBusinessProfileType.setSourceOfBusinessCategoryId(
                        XMLUtil.
                        int2BigInteger(sourceOfBusinessProfile.
                                       getSourceOfBusinessCategoryId()));
            }

            if (!XMLUtil.isEmpty(sourceOfBusinessProfile.
                                 getSourceOfBusinessCode())) {
                sourceOfBusinessProfileType.setSourceOfBusinessCode(
                        sourceOfBusinessProfile.
                        getSourceOfBusinessCode());
            }

            if (!XMLUtil.isEmpty(sourceOfBusinessProfile.
                                 getSourceOfBusinessPriorityId())) {
                sourceOfBusinessProfileType.setSourceOfBusinessPriorityId(
                        XMLUtil.
                        int2BigInteger(sourceOfBusinessProfile.
                                       getSourceOfBusinessPriorityId()));
            }

            if (!XMLUtil.isEmpty(sourceOfBusinessProfile.getSystemTypeId())) {
                sourceOfBusinessProfileType.setSystemTypeId(XMLUtil.
                        int2BigInteger(
                                sourceOfBusinessProfile.getSystemTypeId()));
            }

            Contact contact = sourceOfBusinessProfile.getContact();

            if (contact != null) {
                sourceOfBusinessProfileType.setContact(createContact(contact));
            }

            dealType.setSourceOfBusinessProfile(sourceOfBusinessProfileType);
        }
    }

    private ContactType createContact(Contact contact) throws Exception {
        ContactType contactType = objFactory.createContactType();
        populateContactType(contactType, contact);

        contactType.setLanguagePreferenceId(XMLUtil.int2BigInteger(sInfo.getLanguageId()));
        Addr addr = contact.getAddr();

        if (addr != null) {
            contactType.setAddr(createAddrType(addr));
        }

        return contactType;
    }

    private void populateContactType(ContactType contactType, Contact contact) throws
            Exception {
        if (!XMLUtil.isEmpty(contact.getContactEmailAddress())) {
            contactType.setContactEmailAddress(contact.getContactEmailAddress());
        }

        if (!XMLUtil.isEmpty(contact.getContactFaxNumber())) {
            contactType.setContactFaxNumber(contact.getContactFaxNumber());
        }

        if (!XMLUtil.isEmpty(contact.getContactFirstName())) {
            contactType.setContactFirstName(contact.getContactFirstName());
        }

        if (!XMLUtil.isEmpty(contact.getContactLastName())) {
            contactType.setContactLastName(contact.getContactLastName());
        }

        if (!XMLUtil.isEmpty(contact.getContactMiddleInitial())) {
            contactType.setContactMiddleInitial(contact.getContactMiddleInitial());
        }

        if (!XMLUtil.isEmpty(contact.getContactPhoneNumber())) {
            contactType.setContactPhoneNumber(contact.getContactPhoneNumber());
        }

        if (!XMLUtil.isEmpty(contact.getContactPhoneNumberExtension())) {
            contactType.setContactPhoneNumberExtension(contact.
                    getContactPhoneNumberExtension());
        }

        /*if (!XMLUtil.isEmpty(contact.getLanguagePreferenceId())) {
            contactType.setLanguagePreferenceId(XMLUtil.int2BigInteger(contact.
                    getLanguagePreferenceId()));
        }*/



        if (!XMLUtil.isEmpty(contact.getPreferredDeliveryMethodId())) {
            contactType.setPreferredDeliveryMethodId(XMLUtil.int2BigInteger(
                    contact.
                    getPreferredDeliveryMethodId()));
        }

        if (!XMLUtil.isEmpty(contact.getSalutationId())) {
            contactType.setSalutationId(XMLUtil.int2BigInteger(contact.
                    getSalutationId()));
        }

    }

}
