package com.filogix.externallinks.services.datx;

import javax.xml.bind.JAXBContext;

import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbServicePayloadGenerator;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServicePayloadData;
import com.filogix.schema.datx.closing.ClosingServicesType;

public class DatxOscPayloadGenerator extends JaxbServicePayloadGenerator {

  public static String _packageName = ClosingServicesType.class.getClass().getPackage().getName();

  public String getPackageName() {
    return _packageName;
  }

  protected JAXBContext getJaxbContext() {
    return DatXClosingChannel._jaxbContext;
  }

  public ServicePayloadData getPayloadData() throws RemoteException, FinderException, ExternalServicesException {
    _data = getPayloadData(ServiceConst.LANG_ENGLISH);
    
    return _data;  
  }

  public ServicePayloadData getPayloadData(int langId) throws RemoteException, FinderException, ExternalServicesException {

    _data = new DatxOscPayloadData(_srk, _requestId, _copyId, langId);
    
    DatxOscPayloadData payloadData = (DatxOscPayloadData) _data;
    ClosingServicesType closingRequest = payloadData.getDatxOscPayloadData();

    return _data;
  }

}
