/*
 * @(#)ChannelUtils.java    2009-6-23
 *
 * Copyright (C) 2009 Filogix Limited Partnership. All rights reserved.
 */
package com.filogix.externallinks.services.util;

import com.basis100.deal.entity.Channel;
import com.basis100.deal.pk.ChannelPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;

public class ChannelUtils {

	/**
	 * Constructs Endpoint's URL from channel entity
	 * @param channel
	 * @return URL
	 */
    public static String concatEndpoint(Channel channel) {
        if (channel == null)
            throw new IllegalArgumentException("channel is null");

        StringBuffer endpoint = new StringBuffer();
        endpoint.append(channel.getProtocol())
        .append("://").append(channel.getIp());
        if (channel.getPort() != 0)
            endpoint.append(":" + channel.getPort());
        if(channel.getPath() != null){
            if (channel.getPath().startsWith("/") == false)
                endpoint.append("/");
            endpoint.append(channel.getPath());
        }
        return endpoint.toString();
    }
	
	/**
	 * Constructs Endpoint's URL from channelid and srk
	 * srk has to be set proper institutionProfileId.
	 * 
	 * @param srk
	 * @param channelId
	 * @return endPoint url 
	 */
	public static String getEndPointURL(SessionResourceKit srk, int channelId)
			throws FinderException {
		if (srk == null)
			throw new IllegalArgumentException("srk is null");
		Channel ch = null;
		try {
			ch = new Channel(srk);
			ch = ch.findByPrimaryKey(new ChannelPK(channelId));
		} catch (RemoteException neverhappen) {
			throw new ExpressRuntimeException(neverhappen);
		}
		return concatEndpoint(ch);

	}
}
