package com.filogix.externallinks.services.datx;


import java.io.IOException;

import com.basis100.deal.docprep.DocPrepException;
import com.basis100.deal.docrequest.DocumentRequest;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import com.filogix.datx.bindings.internal.folder.response.version0100.BindingContext;
import com.filogix.datx.bindings.internal.folder.response.version0100.KEYType;
import com.filogix.datx.bindings.internal.folder.response.version0100.RESPONSEGROUPType;
import com.filogix.datx.bindings.internal.folder.response.version0100.RESPONSEType;
import com.filogix.datx.bindings.internal.folder.response.version0100.STATUSType;
import com.filogix.datx.messagebroker.folder.FolderMessageSender;
import com.filogix.datx.messagebroker.folder.FolderRequestBean;
import com.filogix.externallinks.services.IExchangeFolderCreator;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.DcFolder;
import com.basis100.deal.entity.DCConfig;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Property;
import com.basis100.deal.pk.BranchProfilePK;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;

/**
 * <p>Title: Advectis Doc Central Interface </p>
 * <p>Description: This class serves as an interface between express and Doc Central. <br>
*  All requests to Doc Central are provided through DatX</p>
 * <p>Copyright: Copyright filogix(c) 2004</p>
 * <p>Company: filogix inc.</p>
 * @author Catherine Rutgaizer
 * @version 0.1
 */

public class DocCentralInterface implements IExchangeFolderCreator{

  /** @todo: put URL into SYSTEMPROPERTY */

  private FolderRequestBean requestBean;
  private SessionResourceKit srk;
  private static String SPACE = " ";
  private int lang = 0;
  private SysLogger logger;
  private Deal deal;
  private JdbcExecutor jExec;

  // new constructor for spring
  public DocCentralInterface() throws IOException {
		logger = ResourceManager.getSysLogger( "DCInterface" );
  }
 
  public void init(SessionResourceKit srk, SysLogger logger, Deal deal, JdbcExecutor jExec) {
	    this.srk = srk;
	    this.logger = logger;
	    this.deal = deal;
	    this.jExec = jExec;
	    jExec = srk.getJdbcExecutor();
  }

  /**
   * Constructor: please specify SessionResourceKit to initialize the class
   * @param srk SessionResourceKit
   */
  public DocCentralInterface(SessionResourceKit srk) throws
      IOException {
    this.srk = srk;
    jExec = srk.getJdbcExecutor();
    logger = ResourceManager.getSysLogger( "DCInterface" );
  }

  public DocCentralInterface(SessionResourceKit srk, SysLogger logger) throws IOException {
    this(srk);
    this.logger = logger;
  }


  /**
   * This method creates Doc Central folder
   * @param deal Deal
   * @throws DocPrepException - in case of DatX error or Doc Central request failure DocPrep exception is thrown.
   * @return String - this is FOLDER_ID returned by Doc Central / DatX. <br>
   * Note: it is unknown at that moment if this value is strictly numeric or might be alpha-numeric,
   * that is why it is defined as 'String'
   */
  public String createFolder(Deal deal) throws DocPrepException,
      JdbcTransactionException, DocPrepException {
    this.deal = deal;
    String result = null;

    try {

      if (!satisfiesConditions(deal)) {
        throw new DocPrepException(
            "Folder not created because conditions are not satisfied. ");
      }

      // 1) set data
      try {
        setRequestData(deal);
      }
      catch (Exception e) {
        String msg = "DocCentralInterface.CreateFolder: cannot set message. Exception: [ " + e.toString() + "]";
        throw new DocPrepException(msg);
      }

      // 2) send request
      try {
        result = sendAndProcessRequest(deal.getInstitutionProfileId());
      }
      catch (Error er) {
        String msg = "DocCentralInterface.CreateFolder: " + "Error: [" + er.getClass() + " Messge = " + er.toString() + "]";
        throw new DocCentalInterfaceException(msg);
      }
      catch (Exception e) {
        String msg = e.toString();
        throw new DocCentalInterfaceException(msg);
      }
    } catch (DocCentalInterfaceException dcie) {
//      String poolname = PropertiesCache.getInstance().getProperty(deal.getInstitutionProfileId(),"com.basis100.resource.connectionpoolname");
      sendEmailAlert( "Error creating Folder in filogix exchange repository, \n",
                      "Could not create folder in filogix exchange repository \n" +
                      "Error = [" + dcie.getClass() + " " + dcie.getMessage() + "]; " + "\n" +
                      "Deal = " + deal.getDealId() + "; Source Application id = " + deal.getSourceApplicationId()+ "\n",
                      deal.getInstitutionProfileId()); // text, subject
//      sendEmailAlert( "Error creating Folder in filogix exchange repository, " + poolname + " environment \n",
//    		  "Could not create folder in filogix exchange repository"+ poolname + ". \n" +
//    		  "Error = [" + dcie.getClass() + " " + dcie.getMessage() + "]; " + "\n" +
//    		  "Deal = " + deal.getDealId() + "; Source Application id = " + deal.getSourceApplicationId()+ "\n",
//    		  deal.getInstitutionProfileId()); // text, subject
      logger.error(dcie.getMessage());
    } catch (Exception e){
      logger.error(e.getMessage());
      e.printStackTrace();
    }
    return result;
  }

  private void setRequestData(Deal deal) throws FinderException,
      RemoteException, DocPrepException
  {
    String val = null;

    requestBean = new FolderRequestBean();

    DCConfig cnfg = new DCConfig(srk, 1);                                // 1 is hard coded

    // -------------- these are set up by Advectis --------------------
    /** 1) DOCCENTRALCONFIG.DCREQUESTINGPARTY
    /* "com.filogix.mb" for production MB
    /* "com.filogix.exp" for production Expert
    */
    val = cnfg.getDcRequestingParty();
    logger.debug("getDcRequestingParty() = " + val);
    if (val == null){ val = "com.filogix.fxp";}
    requestBean.setRequestingPartyId(val);

    /** 2) DOCCENTRALCONFIG.DCRECEIVINGPARTY
    /* "com.blitzdocs" for production,
    /* "com.blitzdocs.staging" for UAT / QA
    /* Joe's example : net.blitzdocs.folder
    */
    val = cnfg.getDcReceivingParty();
    logger.debug("getDcReceivingParty() = " + val);
    if (val == null){ val = "net.blitzdocs.folder";}
    requestBean.setReceivingPartyId(val);

    // 3)
    requestBean.setAccountIdentifier(cnfg.getDcAccountIdentifier());
    logger.debug("getDcAccountIdentifier() = " + cnfg.getDcAccountIdentifier());
    requestBean.setAccountPassword(cnfg.getDcAccountPassword());
    logger.debug("getDcAccountPassword() = " + cnfg.getDcAccountPassword());

    // 4)
    requestBean.setFolderConfigurationID(cnfg.getDcFolderConfigId());
    logger.trace("getDcFolderConfigId() = "+ cnfg.getDcFolderConfigId());
    requestBean.setLenderCode(cnfg.getDcLenderId());
    logger.debug("getDcLenderId() = "+ cnfg.getDcLenderId());
    // -------------- these are set up by Advectis --------------------

    // 5)
    // Advectis system folder Id : lender_code + channel_code + POS_App_code
    // channel: DEAL.SYSTEMTYPEID -> SYSTEMTYPE.DCCHANNELCODE ('MB' or 'EXP')

    int sysType = deal.getSystemTypeId();

    String channelCode = getChannelCode(sysType);
    if (channelCode == null) {
      throw new DocPrepException ("Channel code is null. Please set up SYSTEMTYPE.DCCHANNELCODE for SYSTEMTYPEID = " + sysType);
    }
    String branchTransitNum = getGebBranchTransit(deal.getBranchProfileId());
    String sfi = branchTransitNum +"."+ channelCode +"."+ deal.getSourceApplicationId();
    logger.trace("SystemFolderId = " + sfi);

    requestBean.setExternalSystemFolderID(sfi);  // Test 41MBBMSB001-00972-F

    // 6) POS application number
    requestBean.setApplicationID(deal.getSourceApplicationId());
    logger.trace("ApplicationId = " + deal.getSourceApplicationId());

    // 7) primary borrower's info
    Borrower bBorrower = null;
    try
    {
      bBorrower = new Borrower(srk, null);
      bBorrower = bBorrower.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
    }
    catch (FinderException fe)
    {
       logger.error(fe.getClass() + ": " + fe.getMessage());
    }

    lang = bBorrower.getLanguagePreferenceId();

    requestBean.setBorrowerFirstName(bBorrower.getBorrowerFirstName());
    requestBean.setBorrowerLastName(bBorrower.getBorrowerLastName());

    // 8) primary property
    Property pProperty = null;

    try {
      pProperty = new Property(srk, null);
      pProperty = pProperty.findByPrimaryProperty(deal
          .getDealId(), deal.getCopyId(), deal.getInstitutionProfileId());
    }
    catch (Exception fe) {
      logger.error(fe.getClass() + ": " + fe.getMessage());
    }

    requestBean.setPropertyAddressLine1(extractAddressLine1(pProperty, lang));

    val = BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "ProvinceShortName", pProperty.getProvinceId(), lang);
    if (val != null) {
      requestBean.setPropertyProvinceAbbreviation(val);
    }
  }

  private String getChannelCode(int sysTypeId) {
    String result = "";

    String sql = "SELECT DCCHANNELCODE FROM SYSTEMTYPE WHERE SYSTEMTYPEID = " + sysTypeId;

    boolean gotRecord = false;

    try
    {
        int key = jExec.execute(sql);
        for (; jExec.next(key); )
        {
            gotRecord = true;
            result = jExec.getString(key, 1);
            break;                                                  // can only be one record
        }

        jExec.closeData(key);
        if (gotRecord == false)
        {
          String msg = "DocCentralInterface.getChannelCode(), channel code not found";
          logger.error(msg);
        }
      }
      catch (Exception e)
      {
          FinderException fe = new FinderException("DocCentralInterface.getChannelCode() exception." );
          logger.error(fe.getMessage());
          logger.error("finder sql: " + sql);
          logger.error(e);
      }
    return result;
  }

  private String getGebBranchTransit(int branchId) throws FinderException,
      RemoteException {

     String result = "";
     BranchProfilePK pk = new BranchProfilePK(branchId);
     BranchProfile bp = new BranchProfile(srk);
     bp = bp.findByPrimaryKey(pk);

     if (bp != null){
       result = bp.getGEBranchTransitNumber();
     }
     logger.trace("Branch GEB transit number = " + result);

     return result;
  }

  private boolean satisfiesConditions(Deal deal){
    // put any additional conditions here
    return true;
  }

  // 1) submit request
  private String sendAndProcessRequest(int institutionProfileId) throws Exception {
    String result = null;

    FolderMessageSender requestSender = new FolderMessageSender();

    String requestXML = requestSender.createRequest(requestBean);
    logger.trace("REQUEST: " + requestXML);

    logger.trace("DatX URL = " +  PropertiesCache.getInstance().getUnchangedProperty(institutionProfileId,"com.filogix.doccentral.datx.url", null ));
    String responseXML = requestSender.execute(requestXML, PropertiesCache.getInstance().getUnchangedProperty(institutionProfileId,"com.filogix.doccentral.datx.url", null ));

    logger.trace("RESPONSE: " + responseXML);

    // 2) check the response
    RESPONSEGROUPType responseGroup = (RESPONSEGROUPType) BindingContext.unmarshall(responseXML);
    if (responseGroup.getRESPONSE().size() <= 0) {
      logger.error("No Response data in envelope");
      throw new DocCentalInterfaceException("No Response data in envelope");
    }
    RESPONSEType response = (RESPONSEType) responseGroup.getRESPONSE().get(0);

    if (response.getKEY().size() <= 0) {
      logger.error("No Key found in response");
      throw new DocCentalInterfaceException("No Key found in response");
    }

    KEYType key = (KEYType) response.getKEY().get(0);
    String keyName = key.getName();
    String keyValue = key.getValue();
    logger.debug("key.getName() = " + keyName);
    logger.debug("key.getValue() = " + keyValue);

    if (response.getSTATUS().size() <= 0) {
      logger.error("No Status found in response");
      throw new DocCentalInterfaceException("No Status found in response");
    }

    STATUSType status = (STATUSType) response.getSTATUS().get(0);

    String statusCondition = status.getCondition();
    String statusDescription = status.getDescription();
    logger.debug("status.getCondition() = " + statusCondition);
    logger.debug("status.getDescription() = " + statusDescription);

    if ((statusCondition.equals(DC_STATUS_COMPLETED)) && (keyName.equals(DC_FOLDER_ID_KEY_NAME))) {
      result = keyValue;
    } else {
      if ((statusCondition.equals(DC_STATUS_FAILED))){
        logger.error("folder create request failed");
        throw new DocCentalInterfaceException("Folder create request failed. Status condition = " + statusCondition +
                                              " Status description = "+ statusDescription + ". See log for details");
      }
    }

    // 3) save results in the Imaging table
    try {
      saveResults(keyName, keyValue, statusCondition, statusDescription);
    } catch (Exception re){
      logger.error("Cannot save in DCImagingReference :"+ re.getClass() + ": " + re.getMessage() );
    }
    return result;
  }

  private void saveResults(String keyName, String keyValue, String statusCode, String statusDesc)
      throws RemoteException, CreateException, RemoteException, DocPrepException {

    DcFolder dcf = new DcFolder(srk);
    // int dealId, String folderCode, String folderStatusCode, String folderStatusDesc

    if (keyName.equals(DC_FOLDER_ID_KEY_NAME)){
      dcf.create(deal.getSourceApplicationId(), keyValue, statusCode, statusDesc);
    } else {
      throw new DocPrepException("DocCentralInterface.saveResults(): expected " + DC_FOLDER_ID_KEY_NAME + "as key name, received: " + keyName );
    }
  }

  private String extractAddressLine1(Property p, int lang)
  {
      String val = null;
      try
      {   val = "";

          if(  !isEmpty(p.getUnitNumber())  ){
            val += p.getUnitNumber();
            val += "-";
          }

          if (!isEmpty(p.getPropertyStreetNumber()))
             val += p.getPropertyStreetNumber();

          if (!isEmpty(p.getPropertyStreetName()))
             val += SPACE + p.getPropertyStreetName();

          if (!isEmpty(p.getStreetTypeDescription())){
             val += SPACE + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetType",  p.getStreetTypeId() , lang);
          }
          if (p.getStreetDirectionId() != 0)
             val += SPACE + BXResources.getPickListDescription(srk.getExpressState().getDealInstitutionId(), "StreetDirection",  p.getStreetDirectionId() , lang);
      }
      catch (Exception e){
        logger.error(e.getClass() + ": " + e.getMessage());
      }
      return val;
  }

  private boolean isEmpty(String str)
  {
      return (str == null || str.trim().length() == 0);
  }


  private void sendEmailAlert(String pSubject, String pText, int institutionProfileId ) throws
      DocPrepException, JdbcTransactionException {

    String emailTo = null;
    emailTo = PropertiesCache.getInstance().getProperty(institutionProfileId,"com.filogix.docprep.alert.email.recipient"); //, "catherine.rutgaizer@filogix.com");

    logger.trace("Placing a request for an alert email into documentqueue...");
    // srk.beginTransaction();
    DocumentRequest.requestAlertEmail(srk, deal, emailTo, pSubject, pText);
    // srk.commitTransaction();
    logger.trace("request for documentqueue is done");

  }

/*
      // do not send the message is mossys.property is not set to y
      if( !PropertiesCache.getInstance().getProperty("com.filogix.docprep.send.email.alert", "Y").equalsIgnoreCase("y")  ){
        return;
      }
      SimpleMailMessage message;
      try{

        message = new SimpleMailMessage();
        message.clearTo();
        message.setFrom( PropertiesCache.getInstance().getProperty("com.filogix.docprep.alert.email.sender") );
        message.appendTo( PropertiesCache.getInstance().getProperty("com.filogix.docprep.alert.email.recipient") );
        message.setText( pText );
        message.setSubject("DocPrep - Alert : " + PropertiesCache.getInstance().getProperty("com.filogix.docprep.running.environment") + " " + pSubject );
        MailDeliver.send(message);
      }catch(Exception exc){
        // try it one more time ( this time we send the message to emergency recipient )
        if(onExceptionContact != null ){
          try{
            message = new SimpleMailMessage();
            message.clearTo();
            message.setFrom("qa@bx.filogix.com");
            message.appendTo(onExceptionContact);
            message.setText( pText );
            message.setSubject( "DocPrep - Alert : " + PropertiesCache.getInstance().getProperty("com.filogix.docprep.running.environment") + " " + pSubject  );
            MailDeliver.send(message);
          }catch(Exception anotherExce){
            anotherExce.printStackTrace();
          }
        }
        exc.printStackTrace();
      }

*/
}
