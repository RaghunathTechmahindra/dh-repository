package com.filogix.externallinks.services.responseprocessor.ug;

import java.math.BigDecimal;

import com.filogix.externallinks.mi.aigug.jaxb.response.MortgageInsuranceResponse;
import com.filogix.externallinks.services.responseprocessor.IMIResponseAdapter;

public class UGMIResponseAdapter
    implements IMIResponseAdapter
{

  private MortgageInsuranceResponse mir;
  
  public UGMIResponseAdapter(MortgageInsuranceResponse mir)
  {
    this.mir = mir;
  }

  public BigDecimal getDeal_MIFeeAmount()
  {
    return mir.getDeal().getMIFeeAmount();
  }

  public BigDecimal getDeal_MIPremium()
  {
    return mir.getDeal().getMIPremium();
  }

  public BigDecimal getDeal_MIPremiumPst()
  {
    return mir.getDeal().getMIPremiumPst();
  }

  public String getDeal_MIResponse()
  {
    return mir.getDeal().getMIResponse();
  }

  public String getDeal_StatusCode()
  {
    return mir.getDeal().getStatusCode();
  }

  public String getDeal_StatusMessage()
  {
    return mir.getDeal().getStatusMessage();
  }

  public String getServiceSpecificReferenceNumber()
  {
    return mir.getDeal().getAIGUGReferenceNumber();
  }

  public Object getAdapteeInstance()
  {
    return mir;
  }

  
}
