/*
 * @(#)PMIMIRequestGenerator.java    Sep 6, 2007
 *
 * Copyrightę 2007. Filogix Limited Partnership. All rights reserved. 
 */

package com.filogix.externallinks.services.payload.pmi;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.entity.BranchProfile;
import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.InstitutionProfile;
import com.basis100.deal.pk.InstitutionProfilePK;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.mi.aigug.jaxb.request.BranchProfileType;
import com.filogix.externallinks.mi.aigug.jaxb.request.DealType;
import com.filogix.externallinks.mi.aigug.jaxb.request.InstitutionProfileType;
import com.filogix.externallinks.services.payload.IPayloadGenerator;
import com.filogix.externallinks.services.payload.MIRequestGenerator;
import com.filogix.externallinks.services.util.XMLUtil;

/**
 * Payload generator for PMI MI
 * 
 * @version 1.0 Sep 6, 2007
 * @author hiro
 */
public class PMIMIRequestGenerator extends MIRequestGenerator {
	
	private static IPayloadGenerator instance = new PMIMIRequestGenerator();

    private static Log _log = LogFactory.getLog(PMIMIRequestGenerator.class);
    
    static {
        try {
            jc = JAXBContext.newInstance(schema);
        } catch (JAXBException e) {
            logger.error("MIRequestGenerator: JAXBContext initialization failed.");
            logger.error(StringUtil.stack2string(e));
        }
    }
    
    private PMIMIRequestGenerator() {
    }

    public static IPayloadGenerator getInstance() {
        return instance;
    }

    protected void createBranchProfile(DealType dealType,
            SessionResourceKit srk, Deal deal) throws Exception {

        BranchProfileType branchProfileType = objFactory.createBranchProfile();

        _log.debug("deal.getBranchProfileId()  =" + deal.getBranchProfileId());
        BranchProfile branchProfile 
            = new BranchProfile(srk, deal.getBranchProfileId());

        _log.debug("branchProfile.getPMIBranchTransitNumber()  =" + branchProfile.getPMIBranchTransitNumber());
        _log.debug("branchProfile.getAIGUGBranchTransitNumber()=" + branchProfile.getAIGUGBranchTransitNumber());
        // ------- use PMIBranchTransitNumber -------
        if (!XMLUtil.isEmpty(branchProfile.getPMIBranchTransitNumber())) {
            branchProfileType.setAIGUGBranchTransitNumber(branchProfile
                    .getPMIBranchTransitNumber());
        }

        if (!XMLUtil.isEmpty(branchProfile.getBPBusinessId())) {
            branchProfileType.setBPBusinessId(branchProfile.getBPBusinessId());
        }

        if (!XMLUtil.isEmpty(branchProfile.getBPShortName())) {
            branchProfileType.setBPShortName(branchProfile.getBPShortName());
        }

        if (!XMLUtil.isEmpty(branchProfile.getBranchName())) {
            branchProfileType.setBranchName(branchProfile.getBranchName());
        }

        if (!XMLUtil.isEmpty(branchProfile.getBranchTypeId())) {
            branchProfileType.setBranchTypeId(XMLUtil
                    .int2BigInteger(branchProfile.getBranchTypeId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getPartyProfileId())) {
            branchProfileType.setPartyProfileId(XMLUtil
                    .int2BigInteger(branchProfile.getPartyProfileId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getTimeZoneEntryId())) {
            branchProfileType.setTimeZoneEntryId(XMLUtil
                    .int2BigInteger(branchProfile.getTimeZoneEntryId()));
        }

        if (!XMLUtil.isEmpty(branchProfile.getRegionProfileId())) {
            branchProfileType.setRegionProfileId(XMLUtil
                    .int2BigInteger(branchProfile.getRegionProfileId()));
        }

        Contact contact = branchProfile.getContact();

        if (contact != null) {
            branchProfileType.setContact(createContact(contact));
        }

        dealType.setBranchProfile(branchProfileType);
    }

    protected void createInstitutionProfile(DealType dealType,
            SessionResourceKit srk, Deal deal) throws Exception {

        InstitutionProfile institutionProfile = new InstitutionProfile(srk);
        institutionProfile = institutionProfile.findByPrimaryKey(new InstitutionProfilePK(deal.getInstitutionProfileId()));

        InstitutionProfileType institutionProfileType = objFactory
                .createInstitutionProfileType();

        _log.debug("institutionProfile.getInstitutionProfileId()  =" + institutionProfile.getInstitutionProfileId());
        
        _log.debug("institutionProfile.getPMIId()  =" + institutionProfile.getPMIId());
        _log.debug("institutionProfile.getAIGUGId()=" + institutionProfile.getAIGUGId());
        
        // ------- use PMIBranchTransitNumber -------
        if (!XMLUtil.isEmpty(institutionProfile.getPMIId())) {
            institutionProfileType.setAIGUGId(institutionProfile.getPMIId());
        }

        if (!XMLUtil.isEmpty(institutionProfile.getBrandName())) {
            institutionProfileType.setBrandName(institutionProfile
                    .getBrandName());
        }

        if (!XMLUtil.isEmpty(institutionProfile.getInstitutionName())) {
            institutionProfileType.setInstitutionName(institutionProfile
                    .getInstitutionName());
        }

        dealType.setInstitutionProfile(institutionProfileType);
    }

}
