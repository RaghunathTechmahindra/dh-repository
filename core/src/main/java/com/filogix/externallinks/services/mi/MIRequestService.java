package com.filogix.externallinks.services.mi;

import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.IRequestService;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.AbstractBaseChannel;
import com.filogix.externallinks.services.channel.ChannelRequestConnectivityException;
import com.filogix.externallinks.services.channel.IChannel;
import com.filogix.externallinks.services.payload.IPayloadGenerator;
import com.filogix.externallinks.services.responseprocessor.IResponseProcessor;
import com.filogix.externallinks.services.util.ChannelUtils;

/**
 * MIRequestService
 * 
 * @version 1.0 - Aug 22, 2011 initial
 * @since 5.0 MI
 */

public class MIRequestService implements IRequestService {

	private static Logger logger = LoggerFactory.getLogger(MIRequestService.class);

	private Set<Integer> errorCodesAssumedRequestWent;

	private IPayloadGenerator payloadGenerator;
	private IChannel channel;
	private IResponseProcessor responseProcessor;

	// only used for Request table
	private int payloadTypeId;
	private int serviceTransactionTypeId;
	private int serviceProductId;
	
	public static final int STATUS_RESPONSE_TIMEOUT = 455;

	public void service(ServiceInfo serviceInfo) throws ExternalServicesException, ChannelRequestConnectivityException, SocketTimeoutException {

		SessionResourceKit srk = serviceInfo.getSrk();
		Deal deal = serviceInfo.getDeal();
		if (srk == null)throwServicesException("srk is null");
		if (deal == null)throwServicesException("deal is null");
		serviceInfo.setChannelTransactionKey(null);

		logger.debug("MIRequestService.service start for deal:" + deal.getDealId());

		DealPropagator dealPropagator = null;

		try {
			dealPropagator = new DealPropagator(deal, null);
			//replacing base deal object with dealPropagator
			serviceInfo.setDeal(dealPropagator);

			// make sure auto-commit is enabled in connection
			// service request phase doesn't use DB transaction
			if (srk.isInTransaction() || srk.getConnection().getAutoCommit() == false) {
				srk.commitTransaction();// just in case
				srk.getConnection().setAutoCommit(true);
			}
		} catch (Exception e) {
			throwServicesException("faild to prepare data for MI request", e);
		}

		updateDealBeforeSubmittion(serviceInfo, dealPropagator);

		Request request = createRequest(serviceInfo);

		String requestPayload = null;
		try {
			requestPayload = payloadGenerator.getXMLPayload(false, serviceInfo);
			logger.debug(StringUtils.left("\n" + requestPayload, 2000));
		} catch (Exception e) {
			throwServicesException("failed to generate request payload", e);
		}

		setUpChannel(serviceInfo);

		MIRequestUtils.addDealHistLogForRequest(deal, srk);

		Object responsePayload = null;
		try {
			logger.debug("MIRequestService before sending request for deal:" + deal.getDealId());
			responsePayload = channel.send(serviceInfo, requestPayload);
		} catch (Exception e) {
			
			if(e instanceof SocketTimeoutException) {
				updateRequest(request, serviceInfo, Mc.REQUEST_STATUS_TRANSMISSION_ERROR, e.getMessage());
				createResponse(serviceInfo, request, Mc.RESPONSE_STATUS_ID_MI_ERROR_ASSUME_WENT_THRU, e.getMessage());
				updateDealAfterSubmission(serviceInfo, dealPropagator, Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED);
				throw (SocketTimeoutException)e;
			}

			if (e instanceof ChannelRequestConnectivityException) {
				ChannelRequestConnectivityException crce = (ChannelRequestConnectivityException) e;

				if (errorCodesAssumedRequestWent != null
						&& errorCodesAssumedRequestWent.contains(crce.getFaultCode())) {

					// soft error: assuming MI request went through
					logger.warn("assuming MI request went through. fault code: " + crce.getFaultCode(), e);
					updateRequest(request, serviceInfo, Mc.REQUEST_STATUS_ID_SUBMITTED, crce.getFaultCode() + ": " + crce.getMessage());
					createResponse(serviceInfo, request, Mc.RESPONSE_STATUS_ID_MI_ERROR_ASSUME_WENT_THRU, e.getMessage());
					//updateDealAfterSuccessSubmittion(serviceInfo, dealPropagator);
					updateDealAfterSubmission(serviceInfo, dealPropagator, Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED);
					//return;
					if(crce.getFaultCode() == STATUS_RESPONSE_TIMEOUT)
						throw  new SocketTimeoutException(e.getMessage());
					else
						throw crce; 
					

				} else {
					// error in ESB or MIProvider
					updateRequest(request, serviceInfo, Mc.REQUEST_STATUS_ID_ERROR_SUBMIT, crce.getFaultCode() + ": " + crce.getMessage());
					throw crce;
				}
			} else {
				// error other than network
				updateRequest(request, serviceInfo, Mc.REQUEST_STATUS_ID_ERROR_SUBMIT, e.getMessage());
				throwServicesException(e.getMessage(), e);
			}
		} finally {
			logger.debug("MIRequestService after sending request for deal:" + deal.getDealId() 
					+ StringUtils.left("\n" + responsePayload, 2000));
		}

		updateRequest(request, serviceInfo, Mc.REQUEST_STATUS_ID_SUBMITTED, "sent request successfully");
		//updateDealAfterSuccessSubmittion(serviceInfo, dealPropagator);
		updateDealAfterSubmission(serviceInfo, dealPropagator, Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED);

		if (responsePayload != null) {
			// request was successful
			createResponse(serviceInfo, request, Mc.RESPONSE_STATUS_COMPLETED, "received MI response successfully");

			try {
				responseProcessor.processResponse(responsePayload, serviceInfo, true);
			} catch (Exception e) {
				throwServicesException("failed to process MI response payload", e);
			}
		}
		
		logger.debug("MIRequestService.service end for deal:" + deal.getDealId());
	}

	public void setUpChannel(ServiceInfo serviceInfo) throws ExternalServicesException {

		if (channel instanceof AbstractBaseChannel == false) {
			throwServicesException("unsupported channel class:" + channel);
		}

		AbstractBaseChannel baseChannel = (AbstractBaseChannel) channel;

		if (baseChannel.isInitDone())
			return;

		SessionResourceKit srk = serviceInfo.getSrk();
		Channel channelEntiry = null;
		try {
			channelEntiry = new Channel(srk, baseChannel.getChannelId());
		} catch (Exception e) {
			String errMsg = "failed to find channel where channelid = " + baseChannel.getChannelId();
			logger.error(errMsg);
			throw new ExternalServicesException(errMsg, e);
		}

		String endpoint = ChannelUtils.concatEndpoint(channelEntiry);
		logger.debug("endpoint = " + endpoint);

		baseChannel.setEndpoint(endpoint);
		baseChannel.setUsername(channelEntiry.getUserId());
		baseChannel.setPassword(channelEntiry.getPassword());

		baseChannel.setInitDone(true);
	}

	protected void throwServicesException(String message, Exception e) throws ExternalServicesException {

		logger.error(message, e);
		if (e instanceof ExternalServicesException) {
			throw (ExternalServicesException) e;
		}
		throw new ExternalServicesException(message, e);
	}

	protected void throwServicesException(String message) throws ExternalServicesException {
		logger.error(message);
		throw new ExternalServicesException(message);
	}

	protected void updateDealBeforeSubmittion(ServiceInfo serviceInfo, DealPropagator dealPropagator) throws ExternalServicesException {

		try {

			// increment MIDealRequestNumber and propagating the same value to all copies.
			int miDealRecNum = dealPropagator.getMIDealRequestNumber();
			miDealRecNum++;
			logger.debug("new MIDealRequestNumber = " + miDealRecNum);
			dealPropagator.setMIDealRequestNumber(miDealRecNum);

			dealPropagator.setMIStatusId(dealPropagator.getMIStatusId());
			dealPropagator.setMIIndicatorId(dealPropagator.getMIIndicatorId());
			dealPropagator.setMIPremiumAmount(dealPropagator.getMIPremiumAmount());
			dealPropagator.setMortgageInsurerId(dealPropagator.getMortgageInsurerId());
			dealPropagator.setMIPolicyNumber(dealPropagator.getMIPolicyNumber());
			dealPropagator.setMIPayorId(dealPropagator.getMIPayorId());
			dealPropagator.setMITypeId(dealPropagator.getMITypeId());
			dealPropagator.setMIUpfront(dealPropagator.getMIUpfront());
			dealPropagator.setMIPolicyNumAIGUG(dealPropagator.getMIPolicyNumAIGUG());
			dealPropagator.setMIPolicyNumCMHC(dealPropagator.getMIPolicyNumCMHC());
			dealPropagator.setMIPolicyNumGE(dealPropagator.getMIPolicyNumGE());
			dealPropagator.setMIPolicyNumPMI(dealPropagator.getMIPolicyNumPMI());
			dealPropagator.setMIFeeAmount(dealPropagator.getMIFeeAmount());
			dealPropagator.setMIPremiumPST(dealPropagator.getMIPremiumPST());
			dealPropagator.setLocAmortizationMonths(dealPropagator.getLocAmortizationMonths());
			dealPropagator.setLocInterestOnlyMaturityDate(dealPropagator.getLocInterestOnlyMaturityDate());

			dealPropagator.ejbStore();

		} catch (Exception e) {
			throwServicesException("faild to save deal data for MI request (before submission)", e);
		}
	}

	protected void updateDealAfterSuccessSubmittion(ServiceInfo serviceInfo, DealPropagator dealPropagator) throws ExternalServicesException {

		try {
			// indicate submit happened.
			// with happy path, this method will be overwritten soon after the
			// process, but this status remains on timeout error.
			dealPropagator.setMIStatusId(Mc.MI_STATUS_INITIAL_REQUEST_SUBMITTED);
			//save the last MIIndicator sent to MI Provider
			dealPropagator.setPreviousMIIndicatorId(dealPropagator.getMIIndicatorId());
			dealPropagator.ejbStore();

		} catch (Exception e) {
			throwServicesException("faild to save deal data for MI request (after the submission)", e);
		}
	}
	protected void updateDealAfterSubmission(ServiceInfo serviceInfo, DealPropagator dealPropagator, int MIStatus) throws ExternalServicesException {

		try {
			// indicate submit happened.
			// with happy path, this method will be overwritten soon after the
			// process, but this status remains on timeout error.
			dealPropagator.setMIStatusId(MIStatus);
			//save the last MIIndicator sent to MI Provider
			dealPropagator.setPreviousMIIndicatorId(dealPropagator.getMIIndicatorId());
			dealPropagator.ejbStore();

		} catch (Exception e) {
			throwServicesException("faild to save deal data for MI request (after the submission)", e);
		}
	}
	
	protected Request createRequest(ServiceInfo info) throws ExternalServicesException {

		try {
			Deal deal = info.getDeal();
			DealPK dealpk = (DealPK) deal.getPk();
			AbstractBaseChannel absChannel = (AbstractBaseChannel) channel;

			Request request = new Request(info.getSrk());
			request.create(dealpk, Mc.REQUEST_STATUS_ID_NOT_SUBMITTED, new Date(), deal.getUnderwriterUserId());

			request.setDealId(deal.getDealId());
			request.setCopyId(deal.getCopyId());
			request.setChannelId(absChannel.getChannelId());
			request.setPayloadTypeId(payloadTypeId);
			request.setServiceTransactionTypeId(serviceTransactionTypeId);
			request.setServiceProductId(serviceProductId);
			request.ejbStore();

			logger.debug("Created a request record. requestid = " + request.getRequestId());

			return request;

		} catch (Exception e) {
			logger.error("faild to create Request table", e);
			throw new ExternalServicesException(e);
		}
	}

	protected void updateRequest(Request request, ServiceInfo info, int requestStatusId, String statusMessage) throws ExternalServicesException {

		try {
			request.setRequestStatusId(requestStatusId);
			request.setStatusMessage(StringUtils.left(statusMessage, 255));
			request.setChannelTransactionKey(info.getChannelTransactionKey());

			request.ejbStore();

		} catch (RemoteException e) {
			logger.error("faild to update Request table", e);
			throw new ExternalServicesException(e);
		}
	}

	protected Response createResponse(ServiceInfo info, Request request, int responseStatusid, String statusMessage) throws ExternalServicesException {

		try {
			Response response = new Response(info.getSrk());
			Date now = new Date();

			ResponsePK pk = response.createPrimaryKey();

			response.create(pk, now, request.getRequestId(), request.getDealId(), now, responseStatusid);
			response.setChannelTransactionKey(info.getChannelTransactionKey());

			if (StringUtils.isNotEmpty(statusMessage)) {
				response.setStatusMessage(StringUtils.left(statusMessage, 255));
			}

			response.ejbStore();

			return response;

		} catch (Exception e) {
			logger.error("faild to create/update Response table", e);
			throw new ExternalServicesException(e);
		}
	}

	public void setPayloadGenerator(IPayloadGenerator payloadGenerator) {
		this.payloadGenerator = payloadGenerator;
	}

	public void setChannel(IChannel channel) {
		this.channel = channel;
	}

	public void setResponseProcessor(IResponseProcessor responseProcessor) {
		this.responseProcessor = responseProcessor;
	}

	public void setPayloadTypeId(int payloadTypeId) {
		this.payloadTypeId = payloadTypeId;
	}

	public void setServiceTransactionTypeId(int serviceTransactionTypeId) {
		this.serviceTransactionTypeId = serviceTransactionTypeId;
	}

	public void setServiceProductId(int serviceProductId) {
		this.serviceProductId = serviceProductId;
	}

	public void setErrorCodesAssumedRequestWent(Set<Integer> softErrorCodes) {
		this.errorCodesAssumedRequestWent = softErrorCodes;
	}
}
