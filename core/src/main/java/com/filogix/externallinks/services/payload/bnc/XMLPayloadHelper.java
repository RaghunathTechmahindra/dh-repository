package com.filogix.externallinks.services.payload.bnc;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import MosSystem.Mc;

import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.AdjudicationApplicantRequest;
import com.basis100.deal.entity.Asset;
import com.basis100.deal.entity.BncAdjudicationApplRequest;
import com.basis100.deal.entity.BncAdjudicationResponse;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.BorrowerAddress;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.EmploymentHistory;
import com.basis100.deal.entity.Income;
import com.basis100.deal.entity.Liability;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.BncAdjudicationApplRequestPK;
import com.basis100.deal.pk.BncAdjudicationResponsePK;
import com.basis100.deal.pk.BorrowerPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;

import com.filogix.externallinks.adjudication.BncGcdServiceInfo;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.ObjectFactory;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecision;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.BorrowerType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.DealType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.MortgageInsuranceType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.ProductType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.PropertyType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.BorrowerType.AssetType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.BorrowerType.BorrowerAddressType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.BorrowerType.EmploymentHistoryType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.BorrowerType.IncomeType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.BorrowerType.LiabilityType;
import com.filogix.externallinks.adjudication.requestprocessor.jaxb.payload.GetMortgageCreditDecisionType.BorrowerType.BorrowerAddressType.AddrType;
import com.filogix.externallinks.services.ServiceInfo;

public class XMLPayloadHelper {
	private static final int FIVE_TOTAL_DIGITS = 5;

	private static final int CURRENCY_TOTAL_DIGITS = 13;

	private static final int DECIMAL_DIGITS = 2;

	private static SysLogger logger;

	protected SessionResourceKit srk;

	protected BncGcdServiceInfo serviceInfo;

	public XMLPayloadHelper(SessionResourceKit sessionResourceKit) {
		this.srk = sessionResourceKit;

	}

	public XMLPayloadHelper() {

	}

	public XMLPayloadHelper(ServiceInfo info) {
		this.srk = info.getSrk();
		this.serviceInfo = (BncGcdServiceInfo) info;
	}

	public String getXMLPayload() throws JAXBException, FinderException,
			RemoteException, Exception {
		String logString = "XMLPayloadHelper.getXMLPayload - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		OutputStream sf = new ByteArrayOutputStream();

		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(Mc.JAXB_PAYLOAD);
		} catch (JAXBException e) {
			logger.error(logString
					+ "Error in getting a JAXBContext instance: "
					+ e.getMessage());
			throw e;
		}
		ObjectFactory objFactory = new ObjectFactory();

		GetMortgageCreditDecision root;
		try {
			root = (GetMortgageCreditDecision) objFactory
					.createGetMortgageCreditDecision();
		} catch (JAXBException e) {
			logger
					.error(logString
							+ "Error in instantiating a GetMortgageCreditDecision root class: "
							+ e.getMessage());
			throw e;
		}

		populateRootNode(objFactory, root);

		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    m.setProperty(
            "com.sun.xml.bind.characterEscapeHandler",
            new CustomCharacterEscapeHandler() );        

		m.marshal(root, sf);

		return sf.toString();
	}

	protected void populateRootNode(ObjectFactory objFactory,
			GetMortgageCreditDecision root) throws Exception {
		String logString = "XMLPayloadHelper.populateRootNode - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Deal deal = serviceInfo.getDeal();
		try {
			root.setBranchTransitNumber(notNullString(deal
					.getSourceOfBusinessProfile().getAlternativeId()));
		} catch (FinderException e1) {
			logger
					.error(logString
							+ "Error in finding the deal.getSourceOfBusinessProfile().getAlternativeId() for deal "
							+ deal.getDealId() + ": " + e1.getMessage());
			throw e1;
		} catch (RemoteException e1) {
			logger
					.error(logString
							+ "Remote error while calling deal.getSourceOfBusinessProfile().getAlternativeId() for deal "
							+ deal.getDealId() + ": " + e1.getMessage());
			throw e1;
		}

		root.setDealId(toBigInteger(deal.getDealId()));

		if (deal.getSourceFirmProfile().getSourceFirmCode().equalsIgnoreCase(
				Mc.SOURCE_FIRM_CODE)) {
			root.setDistributionChannel(BigInteger.ZERO);
		} else {
			root.setDistributionChannel(BigInteger.ONE);
		}

		String sourceApplicationId = deal.getSourceApplicationId();
		if (sourceApplicationId != null && sourceApplicationId.length() > 16)
			sourceApplicationId = sourceApplicationId.substring(0, 16);
		root.setSourceApplicationId(sourceApplicationId);

		try {
			root.setSourceProvinceId(toBigInteger(deal
					.getSourceOfBusinessProfile().getContact().getAddr()
					.getProvinceId()));
		} catch (Exception e) {
			logger
					.info(logString
							+ "Unable to find deal.getSourceOfBusinessProfile().getContact().getAddr().getProvinceId() for deal ID "
							+ serviceInfo.getDealId()
							+ "; using default of ZERO");
			root.setSourceProvinceId(BigInteger.ZERO);
		}

		root.setDeal(getDealType(objFactory));
		root.setMortgageInsurance(getMortgageInsurance(objFactory));
		root.setProduct(getProduct(objFactory));

		Property property = getPrimaryProperty(deal);

		if (property != null) {
			root.setProperty(getProperty(objFactory, property));
		}

		List b = root.getBorrower();
		getBorrowers(b, objFactory);

		Response response = new Response(srk);

		try {
			Request lastRequest = new Request(srk);
			lastRequest = lastRequest.findLastRequestByServiceProductId(Mc.SERVICE_PRODUCT_ID_GCD, 
                        new DealPK(serviceInfo.getDealId().intValue(), 
                                   serviceInfo.getCopyId().intValue()), 
                        new RequestPK(serviceInfo.getRequestId(), serviceInfo.getCopyId().intValue()));

			if (lastRequest == null || lastRequest.getRequestId() <= 0) throw new FinderException();

			response = response.findLastResponseByRequestId(lastRequest.getRequestId());

			BncAdjudicationResponse bncAdjResponse = new BncAdjudicationResponse(
					srk);
			bncAdjResponse = bncAdjResponse
					.findByPrimaryKey(new BncAdjudicationResponsePK(response
							.getResponseId()));
			root.setSubmissionCount(toBigInteger(bncAdjResponse
					.getSubmissionCount()));
			root.setRandomDigit(toBigInteger(bncAdjResponse.getRandomDigit()));

		} catch (FinderException e) {
			logger.info(logString
					+ "Unable to find a previous GCD response for deal ID "
					+ serviceInfo.getDealId() + " and copy ID "
					+ serviceInfo.getCopyId() + "; using default of ZERO.");
			root.setSubmissionCount(BigInteger.ZERO);
		}

	}

	private Property getPrimaryProperty(Deal deal) throws Exception {
		String logString = "XMLPayloadHelper.getPrimaryProperty - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Collection col = deal.getProperties();
		Iterator it = col.iterator();
		while (it.hasNext()) {
			Property property = (Property) it.next();
			if (property.getPrimaryPropertyFlag() == 'Y'
					|| property.getPrimaryPropertyFlag() == 'y') {
				return property;
			}
		}

		return null;
	}

	protected void getBorrowers(List b, ObjectFactory objFactory)
			throws Exception {
		String logString = "XMLPayloadHelper.getBorrowers - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Deal deal = serviceInfo.getDeal();
		Collection borrowers = deal.getBorrowers();
		Iterator it = borrowers.iterator();

		while (it.hasNext()) {
			Borrower borrower = (Borrower) it.next();

			BorrowerType xmlBorrower = objFactory
					.createGetMortgageCreditDecisionTypeBorrowerType();

			xmlBorrower.setBorrowerBirthDate(getCalendar(borrower.getBorrowerBirthDate()));
			xmlBorrower.setBorrowerFirstName(borrower.getBorrowerFirstName());
			xmlBorrower.setBorrowerGenderId(toBigInteger(borrower
					.getBorrowerGenderId()));
			xmlBorrower.setBorrowerId(toBigInteger(borrower.getBorrowerId()));
			xmlBorrower.setBorrowerLastName(borrower.getBorrowerLastName());
			xmlBorrower.setBorrowerTypeId(toBigInteger(borrower
					.getBorrowerTypeId()));
			xmlBorrower.setCitizenshipTypeId(toBigInteger(borrower
					.getCitizenshipTypeId()));
			try {
				AdjudicationApplicantRequest adjRequest = new AdjudicationApplicantRequest(
						srk);
				adjRequest = adjRequest
						.findByBorrowerAndRequestId(new RequestPK(serviceInfo
								.getRequestId(), serviceInfo.getCopyId()
								.intValue()), new BorrowerPK(borrower
								.getBorrowerId(), borrower.getCopyId()));
				BncAdjudicationApplRequest bncAdjApplRequest = new BncAdjudicationApplRequest(
						srk);
				bncAdjApplRequest = bncAdjApplRequest
						.findByPrimaryKey(new BncAdjudicationApplRequestPK(serviceInfo.getRequestId(), adjRequest.getApplicantNumber(), serviceInfo.getCopyId().intValue()));
				xmlBorrower
						.setCreditBureauNameRequestedId(toBigInteger(bncAdjApplRequest
								.getRequestedCreditBureauNameId()));
			} catch (Exception e) {
				logger
						.info(logString
								+ "Unable to find BncAdjudicationApplRequest information; use default creditBureauNameRequestedId of ZERO.");
				xmlBorrower.setCreditBureauNameRequestedId(BigInteger.ZERO);
			}

			xmlBorrower.setGDS(toFiveDigits(borrower.getGDS()));
			xmlBorrower.setMaritalStatusId(toBigInteger(borrower
					.getMaritalStatusId()));
			xmlBorrower.setNetWorth(toCurrency(borrower.getNetWorth()));
			xmlBorrower.setPrimaryBorrowerFlag(borrower
					.getPrimaryBorrowerFlag());
			xmlBorrower.setSocialInsuranceNumber(borrower
					.getSocialInsuranceNumber());
			xmlBorrower.setStaffOfLender(borrower.getStaffOfLender() ? "Y"
					: "N");
			xmlBorrower.setTDS(toFiveDigits(borrower.getTDS()));
			xmlBorrower.setTotalIncomeAmount(toCurrency(borrower
					.getTotalIncomeAmount()));

			// Add Lists for the borrower
			List xmlAddresses = xmlBorrower.getBorrowerAddress();
			getBorrowerAddress(xmlAddresses, borrower, objFactory);

			List xmlAsset = xmlBorrower.getAsset();
			getBorrowerAsset(xmlAsset, borrower, objFactory);

			List xmlLiabilities = xmlBorrower.getLiability();
			getBorrowerLiability(xmlLiabilities, borrower, objFactory);

			List xmlEmployeeHistory = xmlBorrower.getEmploymentHistory();
			getBorrowerEmployeeHistory(xmlEmployeeHistory, borrower, objFactory);

			List xmlIncomes = xmlBorrower.getIncome();
			getBorrowerIncome(xmlIncomes, borrower, objFactory);

			b.add(xmlBorrower);
		}
	}

	protected void getBorrowerIncome(List list, Borrower borrower,
			ObjectFactory objFactory) throws Exception {
		String logString = "XMLPayloadHelper.getBorrowerIncome - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Map employmentIncomes = getEmploymentIncomes(borrower);

		Collection incomes = borrower.getIncomes();
		Iterator it = incomes.iterator();

		while (it.hasNext()) {
			IncomeType xmlIncome = objFactory
					.createGetMortgageCreditDecisionTypeBorrowerTypeIncomeType();
			Income income = (Income) it.next();

			Integer thisIncomeType = (Integer) employmentIncomes
					.get(new Integer(income.getIncomeId()));
			if (thisIncomeType == null
					|| thisIncomeType
							.equals(new Integer(Mc.CURRENT_EMPLOYMENT))) {
				xmlIncome.setAnnualIncomeAmount(toCurrency(income
						.getAnnualIncomeAmount()));
				xmlIncome.setIncIncludeInGDS(income.getIncIncludeInGDS() ? "Y"
						: "N");
				xmlIncome.setIncIncludeInTDS(income.getIncIncludeInTDS() ? "Y"
						: "N");
				//Venkata, 12/12/2006 - Fix for Ticket # 1855 - Begin
				//xmlIncome.setIncomeAmount(new BigDecimal(income
				//		.getIncomeAmount()));
				xmlIncome.setIncomeAmount(toCurrency(income
						.getIncomeAmount()));
				//Venkata, 12/12/2006 - Fix for Ticket # 1855 - End
				xmlIncome.setIncomeId(toBigInteger(income.getIncomeId()));
				xmlIncome.setIncomePeriodId(toBigInteger(income
						.getIncomePeriodId()));
				xmlIncome
						.setIncomeTypeId(toBigInteger(income.getIncomeTypeId()));
				xmlIncome.setIncPercentInGDS(toBigInteger(income
						.getIncPercentInGDS()));
				xmlIncome.setIncPercentInTDS(toBigInteger(income
						.getIncPercentInTDS()));
				xmlIncome.setMonthlyIncomeAmount(toCurrency(income
						.getMonthlyIncomeAmount()));

				list.add(xmlIncome);
			}
		}
	}

	private boolean isValidIncome(Income income, Borrower borrower)
			throws Exception {
		String logString = "XMLPayloadHelper.isValidIncome - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Collection employeeHistories = borrower.getEmploymentHistories();
		Iterator it = employeeHistories.iterator();
		while (it.hasNext()) {
			EmploymentHistory history = (EmploymentHistory) it.next();
			if (income.getIncomeId() == history.getIncomeId()
					&& history.getEmploymentHistoryStatusId() == Mc.CURRENT_EMPLOYMENT) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method will return a Map of all incomes for the borrower in the employmenthistory table.
	 * The key-value pair is: IncomeID --> status, where current=0, previous=1.
	 * @param borrower
	 * @return
	 * @throws Exception
	 */
	private Map getEmploymentIncomes(Borrower borrower) throws Exception {
		String logString = "XMLPayloadHelper.getCurrentEmploymentIncomes - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");

		Map employeeHistory = new HashMap();

		Collection employeeHistories = borrower.getEmploymentHistories();
		Iterator it = employeeHistories.iterator();
		while (it.hasNext()) {
			EmploymentHistory history = (EmploymentHistory) it.next();
			if (history.getEmploymentHistoryStatusId() == Mc.CURRENT_EMPLOYMENT) {
				employeeHistory.put(new Integer(history.getIncomeId()),
						new Integer(Mc.CURRENT_EMPLOYMENT));
			} else {
				employeeHistory.put(new Integer(history.getIncomeId()),
						new Integer(Mc.PREVIOUS_EMPLOYMENT));
			}
		}

		return employeeHistory;
	}

	protected void getBorrowerEmployeeHistory(List list, Borrower borrower,
			ObjectFactory objFactory) throws Exception {
		String logString = "XMLPayloadHelper.getBorrowerEmployeeHistory - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Collection histories = borrower.getEmploymentHistories();
		Iterator it = histories.iterator();

		while (it.hasNext()) {
			EmploymentHistory history = (EmploymentHistory) it.next();

			if (history.getEmploymentHistoryStatusId() == 0) {
				EmploymentHistoryType xmlHistory = objFactory
						.createGetMortgageCreditDecisionTypeBorrowerTypeEmploymentHistoryType();

				xmlHistory.setIncomeId(toBigInteger(history.getIncomeId()));
				xmlHistory.setIndustrySectorId(toBigInteger(history
						.getIndustrySectorId()));
				xmlHistory.setMonthsOfService(toBigInteger(history
						.getMonthsOfService()));
				xmlHistory.setOccupationId(toBigInteger(history
						.getOccupationId()));

				list.add(xmlHistory);
			}
		}
	}

	protected void getBorrowerLiability(List list, Borrower borrower,
			ObjectFactory objFactory) throws Exception {
		String logString = "XMLPayloadHelper.getBorrowerLiability - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Collection liabilities = borrower.getLiabilities();
		Iterator it = liabilities.iterator();

		while (it.hasNext()) {
			LiabilityType xmlLiability = objFactory
					.createGetMortgageCreditDecisionTypeBorrowerTypeLiabilityType();
			Liability liability = (Liability) it.next();

			xmlLiability.setIncludeInGDS(liability.getIncludeInGDS() ? "Y"
					: "N");
			xmlLiability.setIncludeInTDS(liability.getIncludeInTDS() ? "Y"
					: "N");
			xmlLiability.setLiabilityAmount(toCurrency(liability
					.getLiabilityAmount()));
			xmlLiability.setLiabilityMonthlyPayment(toCurrency(liability
					.getLiabilityMonthlyPayment()));
			xmlLiability.setLiabilityPayOffTypeId(toBigInteger(liability
					.getLiabilityPayOffTypeId()));
			xmlLiability.setLiabilityTypeId(toBigInteger(liability
					.getLiabilityTypeId()));
			xmlLiability.setPercentInGDS(toFiveDigits(liability
					.getPercentInGDS()));
			xmlLiability.setPercentInTDS(toFiveDigits(liability
					.getPercentInTDS()));
			xmlLiability.setPercentOutGDS(toFiveDigits(liability
					.getPercentOutGDS()));

			list.add(xmlLiability);
		}
	}

	protected void getBorrowerAsset(List list, Borrower borrower,
			ObjectFactory objFactory) throws Exception {
		String logString = "XMLPayloadHelper.getBorrowerAsset - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Collection assets = borrower.getAssets();
		Iterator it = assets.iterator();
		while (it.hasNext()) {
			Asset asset = (Asset) it.next();
			AssetType xmlAsset = objFactory
					.createGetMortgageCreditDecisionTypeBorrowerTypeAssetType();

			xmlAsset.setAssetTypeId(toBigInteger(asset.getAssetTypeId()));
			xmlAsset.setAssetValue(toCurrency(asset.getAssetValue()));
			xmlAsset.setIncludeInNetWorth(asset.getIncludeInNetWorth() ? "Y"
					: "N");
			xmlAsset.setPercentInNetWorth(toBigInteger(asset
					.getPercentInNetWorth()));

			list.add(xmlAsset);
		}

	}

	private void getBorrowerAddress(List list, Borrower borrower,
			ObjectFactory objFactory) throws Exception {
		String logString = "XMLPayloadHelper.getBorrowerAddress - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		Collection addresses = borrower.getBorrowerAddresses();
		Iterator it = addresses.iterator();
		while (it.hasNext()) {
			BorrowerAddress address = (BorrowerAddress) it.next();
			BorrowerAddressType xmlAddress = objFactory
					.createGetMortgageCreditDecisionTypeBorrowerTypeBorrowerAddressType();
			AddrType xmlAddressType = objFactory
					.createGetMortgageCreditDecisionTypeBorrowerTypeBorrowerAddressTypeAddrType();

			Addr addr = address.getAddr();
			xmlAddressType.setAddressLine2(addr.getAddressLine2());
			xmlAddressType.setCity(addr.getCity());
			xmlAddressType.setPostalFSA(addr.getPostalFSA());
			xmlAddressType.setPostalLDU(addr.getPostalLDU());
			xmlAddressType.setProvinceId(toBigInteger(addr.getProvinceId()));
			xmlAddressType.setStreetDirectionId(toBigInteger(addr
					.getStreetDirectionId()));
			xmlAddressType.setStreetName(addr.getStreetName());
			xmlAddressType.setStreetNumber(addr.getStreetNumber());
			xmlAddressType
					.setStreetTypeId(toBigInteger(addr.getStreetTypeId()));
			xmlAddressType.setUnitNumber(addr.getUnitNumber());

			xmlAddress.setAddr(xmlAddressType);
			xmlAddress.setBorrowerAddressTypeId(toBigInteger(address
					.getBorrowerAddressTypeId()));
			xmlAddress.setMonthsAtAddress(toBigInteger(address
					.getMonthsAtAddress()));
			xmlAddress.setResidentialStatusId(toBigInteger(address
					.getResidentialStatusId()));

			list.add(xmlAddress);
		}
	}

	protected PropertyType getProperty(ObjectFactory objFactory,
			Property property) throws JAXBException {
		String logString = "XMLPayloadHelper.getProperty - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		PropertyType xmlProperty = objFactory
				.createGetMortgageCreditDecisionTypePropertyType();

		xmlProperty.setDwellingTypeId(toBigInteger(property
				.getDwellingTypeId()));  //Ticket 1293//
		xmlProperty.setNewConstructionId(toBigInteger(property
				.getNewConstructionId()));
		xmlProperty.setNumberOfUnits(toBigInteger(property.getNumberOfUnits()));
		xmlProperty.setOccupancyTypeId(toBigInteger(property
				.getOccupancyTypeId()));
		xmlProperty
				.setPropertyTypeId(toBigInteger(property.getPropertyTypeId()));
		xmlProperty.setPropertyUsageId(toBigInteger(property
				.getPropertyUsageId()));
		xmlProperty.setTenureTypeId(toBigInteger(property.getTenureTypeId()));
		xmlProperty.setWaterTypeId(toBigInteger(property.getWaterTypeId()));
		xmlProperty.setSewageTypeId(toBigInteger(property.getSewageTypeId()));
		return xmlProperty;
	}

	protected ProductType getProduct(ObjectFactory objFactory)
			throws JAXBException {
		String logString = "XMLPayloadHelper.getProduct - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		ProductType product = objFactory
				.createGetMortgageCreditDecisionTypeProductType();
		Deal deal = serviceInfo.getDeal();

		product.setDealTypeId(toBigInteger(deal.getDealTypeId()));
		product.setPreApproval(deal.getPreApproval());
		product.setProductTypeId(toBigInteger(deal.getProductTypeId()));
		product.setProgressAdvance(deal.getProgressAdvance());
		product.setSpecialFeatureId(toBigInteger(deal.getSpecialFeatureId()));

		return product;
	}

	protected MortgageInsuranceType getMortgageInsurance(
			ObjectFactory objFactory) throws JAXBException {
		String logString = "XMLPayloadHelper.getMortgageInsurance - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		MortgageInsuranceType ins = objFactory
				.createGetMortgageCreditDecisionTypeMortgageInsuranceType();

		Deal deal = serviceInfo.getDeal();

		ins.setMIIndicatorId(toBigInteger(deal.getMIIndicatorId()));
		ins.setMIStatusId(toBigInteger(deal.getMIStatusId()));
		ins.setMortgageInsuranceResponse(deal.getMortgageInsuranceResponse());
		ins.setMortgageInsuranceTypeId(toBigInteger(deal.getMITypeId()));
		ins.setMortgageInsurerId(toBigInteger(deal.getMortgageInsurerId()));

		return ins;
	}

	protected DealType getDealType(ObjectFactory objFactory)
			throws JAXBException {
		String logString = "XMLPayloadHelper.getDealType - ";
		logger = srk.getSysLogger();
		logger.debug(logString + "Entering method.");
		DealType dealType = objFactory
				.createGetMortgageCreditDecisionTypeDealType();
		Deal deal = serviceInfo.getDeal();

		dealType.setCombinedGDSBorrower(toFiveDigits(deal
				.getCombinedGDSBorrower()));
		dealType.setCombinedLTV(toFiveDigits(deal.getCombinedLTV()));
		dealType.setCombinedTDSBorrower(toFiveDigits(deal
				.getCombinedTDSBorrower()));
		dealType.setDealPurposeId(toBigInteger(deal.getDealPurposeId()));
		dealType
				.setExistingLoanAmount(toCurrency(deal.getExistingLoanAmount()));

		dealType.setMIPremiumAmount(toCurrency(deal.getMIPremiumAmount()));
		dealType.setNetLoanAmount(toCurrency(deal.getNetLoanAmount()));
		dealType.setProgressAdvanceTypeId(toBigInteger(deal
				.getProgressAdvanceTypeId()));
		dealType.setRefiBlendedAmortization(deal.getRefiBlendedAmortization());
		dealType.setRefiCurMortgageHolder(deal.getRefiCurMortgageHolder());

		dealType.setRefiImprovementAmount(toCurrency(deal
				.getRefiImprovementAmount()));
		dealType.setRefiImprovementsDesc(deal.getRefiImprovementsDesc());
		dealType.setRefiOrigMtgAmount(toCurrency(deal.getRefiOrigMtgAmount()));
		dealType.setRefiOrigPurchaseDate(getCalendar(deal
				.getRefiOrigPurchaseDate()));

		dealType.setRefiOrigPurchasePrice(toCurrency(deal
				.getRefiOrigPurchasePrice()));
		dealType
				.setRefiProductTypeId(toBigInteger(deal.getRefiProductTypeId()));
		dealType.setRefiPurpose(deal.getRefiPurpose());
		dealType.setTotalLoanAmount(toCurrency(deal.getTotalLoanAmount()));

		return dealType;
	}

	protected BigInteger toBigInteger(int i) {
		return new BigInteger(String.valueOf(i));
	}

	protected String notNullString(String s) {
		return (s == null ? "" : s);
	}

	protected Calendar getCalendar(Date date) {
		if (date != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} else {
			return null;
		}
	}

	private BigDecimal toCurrency(double value) {
		return toXSDDecimalFormat(value, CURRENCY_TOTAL_DIGITS, DECIMAL_DIGITS);
	}

	private BigDecimal toFiveDigits(double value) {
		return toXSDDecimalFormat(value, FIVE_TOTAL_DIGITS, DECIMAL_DIGITS);
	}

	private BigDecimal toXSDDecimalFormat(double value, int totalDigitsQty,
			int decimalsQty) {

		final String FORMAT_CHAR = "##############################";
		// prepare to remove decimals
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(decimalsQty, BigDecimal.ROUND_DOWN);
		// format
		DecimalFormat df = new DecimalFormat(FORMAT_CHAR.substring(0,
				totalDigitsQty)
				+ "." + FORMAT_CHAR.substring(0, decimalsQty));
		String s = df.format(bd.doubleValue()).toString();
		// enforce max number of digits
		int is = Math.max(0, s.indexOf(".") - totalDigitsQty);
		int ie = Math.min(s.length(), is + totalDigitsQty
				+ (s.indexOf(".") >= 0 ? 1 : 0));
		bd = new BigDecimal(s.substring(is, ie));

		return bd;
	}
}
