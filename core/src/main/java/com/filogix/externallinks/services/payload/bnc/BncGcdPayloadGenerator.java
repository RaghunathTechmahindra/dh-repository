package com.filogix.externallinks.services.payload.bnc;

import com.basis100.log.SysLogger;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.payload.IPayloadGenerator;

/**
 * 
 * This class will generate an XML string to be used in 
 * a web service call to DatX.
 * 
 * @author
 *
 */
public class BncGcdPayloadGenerator implements IPayloadGenerator
{
	private static SysLogger logger;
    private static BncGcdPayloadGenerator instance = new BncGcdPayloadGenerator(); 
    public BncGcdPayloadGenerator()
    {
        super();
        
    }

    public String getXMLPayload(boolean validate, ServiceInfo info) throws Exception
    {
		String logString = "BncGcdPayloadGenerator.getXMLPayload - ";
		logger = info.getSrk().getSysLogger();
		logger.debug(logString + "Entering method for request ID: " + info.getRequestId());
        XMLPayloadHelper helper = new XMLPayloadHelper(info);
        String payload = helper.getXMLPayload();
        return payload;
        
    }
    public static IPayloadGenerator getInstance()
    {
        return instance;
    }

}
