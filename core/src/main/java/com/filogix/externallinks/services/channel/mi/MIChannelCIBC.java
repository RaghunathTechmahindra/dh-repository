package com.filogix.externallinks.services.channel.mi;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axiom.soap.SOAPHeaderBlock;

public class MIChannelCIBC extends MIChannelDefault {

	private String clientSystemId;
	
	
	public String getClientSystemId() {
		return clientSystemId;
	}

	public void setClientSystemId(String clientSystemId) {
		this.clientSystemId = clientSystemId;
	}

	
	@Override
	protected SOAPHeaderBlock createSOAPHeaderEndpointReference(
			SOAPFactory factory) {

		SOAPHeaderBlock endPointRef = super.createSOAPHeaderEndpointReference(factory);
		
		OMNamespace omNsWsa = factory.createOMNamespace(XMLNS_WSA, "wsa");
		OMNamespace omNsMmx = factory.createOMNamespace(XMLNS_MP_SCH_META, "mmx");
		
		OMElement wsaMetadata = factory.createOMElement("Metadata", omNsWsa);
		OMElement mmxMetadata = factory.createOMElement("metadata", omNsMmx);
		OMElement clientSystemId = factory.createOMElement("clientSystemId", omNsMmx);
		
		endPointRef.addChild(wsaMetadata);
		wsaMetadata.addChild(mmxMetadata);
		mmxMetadata.addChild(clientSystemId);
		
		clientSystemId.setText(this.getClientSystemId());
		
		return endPointRef;
	}

	
}
