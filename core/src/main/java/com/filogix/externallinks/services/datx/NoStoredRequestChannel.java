/**
 * <p>Title: NoStoredRequestChannel.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0(Initial Version � Nov 22, 2006)
 *
 */
package com.filogix.externallinks.services.datx;

import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.PayloadType;
import com.basis100.deal.pk.ChannelPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.PayloadTypePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.NoStoredRequestData;
import com.filogix.externallinks.framework.NoStoredTransactionData;
import com.filogix.externallinks.framework.ExternalServicesException;

/*
 * <p> This class is abstruct class for channel Classes
 * to send message to Datx and the request doesn't require callback.
 * Because of the funcitonality, request is not stored in DB
 */
public abstract class NoStoredRequestChannel
{
  protected SessionResourceKit    _srk;
  protected SysLogger             _logger;
  protected Deal                  _deal;
  
  protected NoStoredTransactionData  _data;
  protected Channel               _channel;
  protected PayloadType           _payload;
  protected String                _url;
  
  // constructor
  public NoStoredRequestChannel()
  {
    super();    
  }

  /*
   * <p>init 
   * @param SessionResouceKit srk
   * @param int               dealId
   * @param int               copyId
   * @param int               channelId
   * @param int               payloadId
   * @throw RemoteException
   * @throw FinderException
   */
  public void init(SessionResourceKit srk, int dealId, int copyId, 
                int channelId, int payloadId, String loginId) 
    throws RemoteException, FinderException
  {
    _logger = ResourceManager.getSysLogger(loginId);
    _logger.debug(this.getClass().getName() + " Initialized");    
    _srk = srk;
    setDeal(dealId, copyId);
    setPayload(payloadId);
    setChannel(channelId);
    buildUrl();
  }
  
  /**
   * <p> buildUrl
   * build String url from channel entity
   **/
  private void buildUrl() {
    
    if (_srk == null ) return;
    if (_channel == null ) return;
    
    StringBuffer url = new StringBuffer();
    String protocol = _channel.getProtocol();
    String ip = _channel.getIp();
    String path = _channel.getPath();
    
    if (protocol != null && !protocol.trim().equals(""))
    {
      url.append(protocol + "://");
    }
    
    if ( ip != null && !ip.trim().equals("")) 
    {
      url.append(ip);
    }
    
    if (_channel.getPort() != 0) 
    {
      url.append(":" + _channel.getPort());
    }
    
    if (path != null && !path.trim().equals(""))
    {
      if (path.indexOf('\\') != -1 )
      {
        path = path.replace('\\', '/');
      }
      if (!path.startsWith("/"))
      {
        url.append("/" + path);
      }
      url.append(path);
    }

    this._url = url.toString();
    _logger.debug("chanel url: " + _url);
  }

  // Getters and Setters STARTS
  public Channel getChannel() 
  {
    return _channel;
  }

  public String getUrl()
  {
    return _url; 
  }

  public void setChannel(Channel channel)
  {
    this._channel = channel;
  }

  public void setChannel(int id)
    throws FinderException, RemoteException
  {
    if ( _srk == null )
    {
      return;
    }
    
    _channel = new Channel(_srk);
    _channel = _channel.findByPrimaryKey(new ChannelPK(id));
  }

  public SysLogger getLogger()
  {
    return _logger;
  }

  public void setLogger(SysLogger logger)
  {
    this._logger = logger;
  }

  public SessionResourceKit getSrk()
  {
    return _srk;
  }

  public void setSrk(SessionResourceKit srk)
  {
    this._srk = srk;
  }
  
  public Deal getDeal()
  {
    return _deal;
  }
  
  public void setDeal(int dealId, int copyId)
  {
    if ( _srk == null )
    {
      return;
    }
    try
    {
      _deal = new Deal(_srk, null);
      _deal = _deal.findByPrimaryKey(new DealPK(dealId, copyId));
    }
    catch(Exception e)
    {
      // deal doesn't exist
    }
  }
  
  public PayloadType getPayload()
  {
    return _payload;
  }

  public void setPayload(PayloadType payload)
  {
    this._payload = payload;
  }
  
  public void setPayload(int payloadId)
    throws FinderException, RemoteException
  {
    if ( _srk == null )
    {
      return;
    }
  
    _payload = new PayloadType(_srk);
    _payload = _payload.findByPrimaryKey(new PayloadTypePK(payloadId));
  }
  // Getters and Setters END

  /**
   * <p> abstract initRequestBean
   * @throw RemoteException
   * @throw FinderException
   * @throw ExternalServiceException
   **/
  protected abstract void initRequestBean() 
    throws RemoteException, FinderException, ExternalServicesException;

  /**
   * <p> abstract initRequestBean
   * @throw Exception
   **/
  public abstract Object sendData(String payload) 
    throws Exception;

  /**
   * <p> abstract createRequest
   * @throw Exception
   */
  public abstract String createRequest(NoStoredRequestData payloadData)
    throws Exception;

}
