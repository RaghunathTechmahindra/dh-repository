package com.filogix.externallinks.services.channel.bnc;

import java.net.MalformedURLException;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import MosSystem.Mc;

import com.basis100.deal.entity.Request;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLogger;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.adjudication.BncGcdServiceInfo;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.IChannel;


/**
 * 
 * This class will create and send a request to 
 * the DatX web service.
 * @author
 *
 */
public class BncGcdChannel implements IChannel
{
	private SysLogger logger;
	
    private static BncGcdChannel instance = new BncGcdChannel();
    
    private static String endpoint;

    private static String username;

    private static String password;
    
    private static String serviceName;

        
    public String getEndpoint()
    {
        return endpoint;
    }

    public void setEndpoint(String endpoint)
    {
        BncGcdChannel.endpoint = endpoint;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        BncGcdChannel.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        BncGcdChannel.username = username;
    }

    public BncGcdChannel()
    {
        super();
       
    }
  
    public static IChannel getInstance() {
        return instance;
    }
    public void init(ServiceInfo serviceInfo) throws Exception
    {
        //Initialize the setting for web service call
    }

    public Object send(ServiceInfo serviceInfo, String xmlPayload) throws Exception
    {
        logger = serviceInfo.getSrk().getSysLogger();
        String logString = "BncGcdChannel.send() - ";
		logger.debug(logString + "Entering method.");
        
        BncGcdServiceInfo info = (BncGcdServiceInfo) serviceInfo;
        BncGcdChannelHelper helper = new BncGcdChannelHelper(info); 
        FXRequest fxRequest = helper.getFXRequest();
        
        //The value of user name and password were assigned 
        //from spring application context when this object was loaded.

        fxRequest.setUsername(this.getUsername());
        fxRequest.setPassword(this.getPassword());
        
        logger.debug("======== FXREQUEST ========");
        displayFXRequest(logger, fxRequest);
        logger.debug("======== XMLPAYLOD ========");
        logger.debug(xmlPayload);
        
        
        Request request = info.getRequest();
        request.setRequestStatusId(Mc.REQUEST_STATUS_ID_SUBMIT_PENDING);
        saveRequest(request, info.getSrk());
        
        FXResponse fxResponse = null;
        
        try
        {
        	logger.debug(logString + "START: invoking web service");
            fxResponse = callGCDWebService(fxRequest, xmlPayload); 
            logger.debug(logString + "STOP: invoking web service");
            
        } catch (RemoteException e)
        {
            logger.error(logString + "RemoteException occurred while invoking the web service, swallow the exception: " + e.getMessage());

        } catch (ServiceException e)
        {
            logger.error(logString + "ServiceException occurred while invoking the web service " + e.getMessage());
            throw e;
        } catch (MalformedURLException e)
        {
            
            logger.error(logString + "MalformedURLException occurred while invoking the web service " + e.getMessage());
            throw e;
        }

        return fxResponse;
    }

    private void saveRequest(Request request, SessionResourceKit srk) 
    throws com.basis100.entity.RemoteException, JdbcTransactionException
    {
        request.ejbStore();
    }

    private void displayFXRequest(SysLogger logger, FXRequest fxRequest)
    {
       logger.debug("AuxData = " + fxRequest.getAuxData());
       logger.debug("ClientKey=" + fxRequest.getClientKey());
       logger.debug("DatXKey=" + fxRequest.getServiceKey());
       logger.debug("Encoding=" + fxRequest.getEncoding());
       logger.debug("Password=" + fxRequest.getPassword());
       logger.debug("SchemaName=" + fxRequest.getSchemaName());
       logger.debug("getTimeout=" + fxRequest.getTimeout());
       logger.debug("Username=" + fxRequest.getUsername());
  
    }

    /**
     * @param fxRequest
     * @param xmlPayload
     * @return
     * @throws RemoteException
     * @throws ServiceException
     * @throws MalformedURLException
     */
    private FXResponse callGCDWebService(FXRequest fxRequest, String xmlPayload) 
    throws RemoteException, ServiceException, MalformedURLException
    {
		String logString = "XMLPayloadHelper.getDealType - ";
		if (logger != null) logger.debug(logString + "Entering method.");
        // The value of endpoing and port name were assigned 
        //from spring application context when this object was loaded.

        java.net.URL endpoint = new java.net.URL(this.getEndpoint());

        GetCreditDecisionSoapBindingStub binding = (GetCreditDecisionSoapBindingStub) new GetCreditDecision_ServiceLocator()
                .getGetCreditDecision(endpoint);

        binding.setPortName(this.getServiceName());
        binding.setTimeout(fxRequest.getTimeout().intValue() * 1000);

        return binding.requestCreditDecision(fxRequest, xmlPayload);

    }

    private String getServiceName()
    {
        return serviceName;
       
    }
    public void setServiceName(String pName)
    {
        serviceName = pName;
    }
}
