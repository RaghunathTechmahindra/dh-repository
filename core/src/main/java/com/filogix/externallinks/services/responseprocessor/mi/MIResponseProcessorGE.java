package com.filogix.externallinks.services.responseprocessor.mi;

import java.math.BigInteger;

import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.util.DealFeeCreator;
import com.basis100.deal.util.TypeConverter;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.util.XMLUtil;

public class MIResponseProcessorGE extends AbstractMIResponseProcessor {

    private final static Logger logger = LoggerFactory.getLogger(MIResponseProcessorGE.class);

    protected static final String ERROR = "E";
    protected static final String CANCELLED = "C";
    protected static final String REFER = "R";
    protected static final String APPROVED = "A";
    protected static final String DECLINED = "D";
    protected static final String DECLINEDN = "N";
    protected static final String RECEIVED = "I";

    @Override
    protected void setPropagateMIFields(String payload, ServiceInfo info, IResponseAdaptor miResponse,
        CalcMonitor dcm, DealPropagator dealp, DealFeeCreator dfCreator) throws ExternalServicesException {

        int miStatus = createMIStatusID(miResponse, dealp);

        dealp.setMIStatusId(miStatus);
        dealp.setMortgageInsuranceResponse(makeMortgageInsuranceResponseText(miResponse, dealp));

        String miProviderRefNum = miResponse.getMIProviderReferenceNumber();
        if (TypeConverter.longTypeFrom(miProviderRefNum, 0) > 0) {
            dealp.setMIPolicyNumber(miProviderRefNum);
            dealp.setMIPolicyNumGE(miProviderRefNum);
        }

        // on approval
        if (miStatus == Mc.MI_STATUS_APPROVAL_RECEIVED || miStatus == Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED) 
        {

            BigInteger locAmortMonths = miResponse.getLOCAmortizationMonths();
            if (locAmortMonths != null) {
                dealp.setLocAmortizationMonths(locAmortMonths.intValue());
            }

            XMLGregorianCalendar locIOMaturityDate = miResponse.getLOCInterestOnlyMaturityDate();
            if (locIOMaturityDate != null) {
                dealp.setLocInterestOnlyMaturityDate(locIOMaturityDate.toGregorianCalendar().getTime());
            }

            dealp.setMIPremiumAmount(XMLUtil.bigDecimal2Double(miResponse.getMIPremium(), 2));
            dealp.setMIFeeAmount(XMLUtil.bigDecimal2Double(miResponse.getMIFeeAmount(), 2));
            dealp.setMIPremiumPST(XMLUtil.bigDecimal2Double(miResponse.getMIPremiumPst(), 2));

            dfCreator.add(Mc.FEE_TYPE_GE_CAPITAL_PREMIUM, miResponse.getMIPremium());
            dfCreator.add(Mc.FEE_TYPE_GE_CAPITAL_FEE, miResponse.getMIFeeAmount());
            dfCreator.add(Mc.FEE_TYPE_GE_CAPITAL_PST_ON_PREMIUM, miResponse.getMIPremiumPst());
        }
    }

    protected int createMIStatusID(IResponseAdaptor miResponse, Deal deal) {

        int miStatusId = deal.getMIStatusId();

        if (ERROR.equals(miResponse.getStatusCode())) {
            if (TypeConverter.longTypeFrom(miResponse.getMIProviderReferenceNumber(), 0) > 0) {
                miStatusId = Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER;
            } else {
                miStatusId = Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER;
            }

        } else if (CANCELLED.equals(miResponse.getStatusCode())) {
            miStatusId = Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC;

        } else if (REFER.equals(miResponse.getStatusCode())
            || RECEIVED.equals(miResponse.getStatusCode())) {
            miStatusId = Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY;

        } else if (APPROVED.equals(miResponse.getStatusCode())) {
            // if MI premium is changed, this status is to be overwritten
            miStatusId = Mc.MI_STATUS_APPROVAL_RECEIVED;

        } else if ((DECLINED.equals(miResponse.getStatusCode()) 
            || DECLINEDN.equals(miResponse.getStatusCode()))) {
            
            miStatusId = Mc.MI_STATUS_DENIED_RECEIVED;
        }

        return miStatusId;

    }

}
