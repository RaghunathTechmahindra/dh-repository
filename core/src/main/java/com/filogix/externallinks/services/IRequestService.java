package com.filogix.externallinks.services;

import com.filogix.externallinks.services.channel.IChannel;
import com.filogix.externallinks.services.payload.IPayloadGenerator;
/**
 *
 * <p>Title: Filogix Express - AIG UG</p>
 *
 * <p>Description: Filogix AIGUG MI Project</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Filogix</p>
 *
 * @author Lin Zhou
 * @version 1.0
 */

public interface IRequestService {
    void service(ServiceInfo serviceInfo) throws Exception;
    
    void setPayloadGenerator(IPayloadGenerator payloadGenerator);
    
    void setChannel(IChannel channel);
}
