package com.filogix.externallinks.services.datx;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.xml.sax.SAXException;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.AvmSummary;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.pk.PropertyPK;
import com.basis100.deal.pk.RequestPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.filogix.datx.messagebroker.valuation.AVMResponseBean;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServiceResponseBase;
import com.filogix.externallinks.framework.ServiceResponseProcessorBase;

/*
 * DatxAvmResponseProcessor.java
 * 
 * External links framework
 * 
 * Created: 20-Feb-2006
 * Author:  Catherine Rutgaizer
 * 
 */
public class DatxAvmResponseProcessor extends ServiceResponseProcessorBase {
  private ServiceResponseBase _base;
  private AVMResponseBean     _bean;
  private Response            _response;
  private int                 _responseId;
  private int                 _statusId;
  private String              _statusDesc;
  private Date                _statusDate;
  
  public int process(ServiceResponseBase base) throws ExternalServicesException {
    int result = -1;
    _base = base;
    _bean = (AVMResponseBean)base.getResponseBean();

    try {
      _srk.beginTransaction();

      try {
        String statusCondition = _bean.getStatusCondition();
        String statusCode = _bean.getStatusCode();
        // sanity check
        if (statusCondition == null) {
          _logger.error("DatXAvmResponseProcessor: STATUS _condition is null");
        }
        
        _statusDate = new Date();
        _statusId = getStatusId(statusCondition, statusCode);
        _statusDesc = _bean.getStatusDescription();
        if (_statusDesc == null) {
          _statusDesc = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "RESPONSESTATUS", _statusId, ServiceConst.LANG_ENGLISH);
        }
        
        updateRequest();
        updateResponse();

        // Catherine: sanity check 
        if (_responseId == 0){
          throw new Exception("Could not create a new response record, responseId == null");
        } else {
          result = _responseId;          
        }
        
        if (_statusId != ServiceConst.RESPONSE_STATUS_FAILED &&
            _statusId != ServiceConst.RESPONSE_STATUS_SYSTEM_ERROR &&
            _statusId != ServiceConst.RESPONSE_STATUS_PROCESSING_ERROR)
        {        
          updateAvmSummary();
          updateProperty();
        }

        _srk.commitTransaction();
        
      } catch (Exception e) {
        _logger.error(StringUtil.stack2string(e));
        
        _srk.cleanTransaction();
        
        _statusDate = new Date();
        _statusId = ServiceConst.RESPONSE_STATUS_PROCESSING_ERROR;
        _statusDesc = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "REQUESTSTATUS", ServiceConst.RESPONSE_STATUS_PROCESSING_ERROR, ServiceConst.LANG_ENGLISH);

        _srk.beginTransaction();
        try {
          updateRequest();
          _srk.commitTransaction();

        } catch (RemoteException e1) {
          _logger.error(StringUtil.stack2string(e1));
          _srk.cleanTransaction();
        }
      }

    } catch (JdbcTransactionException e1) {
      _logger.error(StringUtil.stack2string(e1));
      throw new ExternalServicesException("Error when processing DatX reponse for requestId = " + _requestId + " :" + e1.getMessage());
    }

    return result;
  }

  /**
   * update the values of the PROPERTY table  
   * only if they are null
   * @throws ExternalServicesException 
   */
  private void updateProperty() throws ExternalServicesException {
    Request req = _base.getRequest();
    int copyId = req.getCopyId();
    
    try {
      
      ServiceRequest sReq = new ServiceRequest(_srk);
      
      Collection col = sReq.findByRequest(new RequestPK(_requestId, copyId));
      Iterator it = col.iterator();

      while(it.hasNext())
      {
        sReq = (ServiceRequest) it.next();
        if (!(sReq.getRequestId() == _requestId)){
          throw new FinderException("Cannot find ServiceRequest for request = " + _requestId + ", copyId = " + copyId);
        }
        break;                                                                 // should be only one
      }
      
      int propId = sReq.getPropertyId();
      this._logger.debug("DatXAvmResponseProcessor@updateProperty(): propertyId = " + propId +  
          ", requestId = " + _requestId + ", copyId = " + sReq.getCopyId());
        
      // get the property
      CalcMonitor dcm = CalcMonitor.getMonitor(_srk);
      Property prop = new Property(_srk, dcm);
      Collection copyCol = prop.findCopyIdByPropertyId(propId);
      it = copyCol.iterator();

      while(it.hasNext()){
          Integer copyInt = (Integer)it.next();
          int propCopyId = copyInt.intValue();
          prop = new Property(_srk, dcm, propId, propCopyId);
      if ((prop.getActualAppraisalValue() == 0d) && (prop.getAppraisalDateAct() == null)) {
        prop.setActualAppraisalValue(Double.parseDouble(_bean.getValuationValue()));
        
        if (!(_bean.getValuationEffectiveDate()== null )){
        	prop.setAppraisalDateAct(getDateFromString(_bean.getValuationEffectiveDate()));
        }	
        prop.ejbStore();
              dcm.calc();
          }
      }
    } catch (RemoteException e) {
      _logger.error(StringUtil.stack2string(e));
    } catch (FinderException e) {
    } catch (Exception e) {
      _logger.error(StringUtil.stack2string(e));
      throw new ExternalServicesException("Error DatxAvmResponseProcessor@updateProperty(): for requestId = " + _requestId + ", copyId = " + copyId);
    }
  }

  private void updateRequest() throws RemoteException{
   
    // Update Request table
    Request request = _base.getRequest();
    try
    {
        Collection requests = request.findRequestsByRequestId(request.getRequestId());
        Iterator iReq = requests.iterator();
        while (iReq.hasNext())
        {
            Request r = (Request)iReq.next();
            r.setStatusMessage(_statusDesc);
            r.setRequestStatusId(_statusId);
            r.setStatusDate(_statusDate);
            r.ejbStore();
        }
    }
    catch(FinderException fe)
    {
        // ignore
    }
  }
  /*
   * save Response record
   */
  private void updateResponse() throws RemoteException, FinderException, CreateException {
    
    // 3) Create new Response record
    _response = new Response(_srk);
    _response.create( _response.createPrimaryKey(),
                  new Date(), _requestId, _base.getRequest().getDealId(),  
                  _statusDate, _statusId);
    
    _response.setStatusMessage(_statusDesc);
    _response.setChannelTransactionKey(getConversationKey());
    
    _response.ejbStore();
    
    _responseId = _response.getResponseId();
    
  }

  /*
   * save Response record
   */
  private void updateAvmSummary() throws RemoteException, FinderException, CreateException {
    AvmSummary avms = new AvmSummary(_srk);
    
    avms.create(_responseId);
    
    avms.setAvmValue(Double.parseDouble(_bean.getValuationValue()));
    avms.setHighAvmRange(Double.parseDouble(_bean.getValuationValueRangeHighLimit()));
    avms.setLowAvmRange(Double.parseDouble(_bean.getValuationValueRangeLowLimit()));
    
    // DatX #10-11 --------- begin
    
    if (!(_bean.getValuationEffectiveDate()== null )){
     avms.setValuationDate(getDateFromString(_bean.getValuationEffectiveDate()));
    }
    
    String confLevel =  BXResources.getGenericMsg("NOT_AVAILABLE_LABEL", ServiceConst.LANG_ENGLISH); // NOT_AVAILABLE_LABEL;
    
    if ((_bean.getConfidenceRating() != null) && !(_bean.getConfidenceRating().trim().equals(""))) {
      confLevel = _bean.getConfidenceRating();
    }
    avms.setConfidenceLevel(confLevel); 
    // DatX #10-11 --------- end
    
    avms.setAvmReport(_bean.getAvmReport());  

    avms.ejbStore();
  }
  
  private String getConversationKey() {
    String result = "";
    DatxResponseParser parser = new DatxResponseParser();
    
    try {
      parser.read(_base.getResponseXml());
      result = parser.getConversationKey();
      
    } catch (IOException e) {
      _logger.error("Error parsing Datx response @getConversationKey(): " + StringUtil.stack2string(e));
    } catch (SAXException e) {
      _logger.error("Error parsing Datx response @getConversationKey(): " + StringUtil.stack2string(e));
    } catch (NullPointerException e) {
      _logger.error("NullPointerException when parsing Datx response @getConversationKey(): " + StringUtil.stack2string(e));
    }
    return result;
  }

  /**
   * DatX forwards data from different providers without changing any formats.
   * Different formats that could be found in the Response sent by DatX:
             // MPAC: --------
             // requestDate = '2006-05-31 14:46:25'
             // _LastSalesDate="1972-01-01 00:00:00.0"
             
             // Teranet: --------
             // _LastSalesDate="Aug 11, 2004"
             //_EffectiveDate="May 25, 2006"
             
             // reavs: --------
             // _ClosingDate="Dec  17,  2004"
             // _EffectiveDate="May  22,  2006"
             // _EffectiveDate="May  20,  2006"
             
             // Landcor: 
             // _LastSalesDate="15-6-2005"
             // _ClosingDate="30-03-2006"
   *  
   * @param strDate
   * @return
   */
  private static Date getDateFromString(String strDate) {
    final String format1 = "MMM dd, yyyy";            // 
    final String format2 = "yyyy-MM-dd HH:mm:ssz";    //  2002-05-01 00:00:00EDT
    final String format3 = "yyyy-MM-dd HH:mm:ss.S";   //  1972-01-01 00:00:00.0
    final String format4 = "yyyy-MM-dd HH:mm:ss";     //  2006-05-31 14:46:25
    final String format5 = "dd-M-yyyy";
    final String format6 = "dd-MM-yyyy";
    
    SimpleDateFormat df1 = new SimpleDateFormat(format1);     
    SimpleDateFormat df2 = new SimpleDateFormat(format2);     
    SimpleDateFormat df3 = new SimpleDateFormat(format3);     
    SimpleDateFormat df4 = new SimpleDateFormat(format4);     
    SimpleDateFormat df5 = new SimpleDateFormat(format5);     
    SimpleDateFormat df6 = new SimpleDateFormat(format6);     

    System.out.println("Parsing date: " + strDate);
    Date date = new Date();
    
    try {
      date = df1.parse(strDate);
    } catch (ParseException e1) {
      System.out.println("df1: Error parsing date using " + format1  + ": " + e1 + ". Retrying using " + format2);
      try {
        date = df2.parse(strDate);
      } catch (ParseException e2) {
        System.out.println("df2: Error parsing date using " + format2  + ": " + e2 + ". Retrying using " + format3);
        try {
          date = df3.parse(strDate);
        } catch (ParseException e3) {
          System.out.println("df3: Error parsing date using " + format3  + ": " + e3 + ". Retrying using " + format4);
          try {
            date = df4.parse(strDate);
          } catch (ParseException e4) {
            System.out.println("df4: Error parsing date using " + format4  + ": " + e4 + ". Retrying using " + format5);
            try {
              date = df5.parse(strDate);
            } catch (ParseException e5) {
              System.out.println("df5: Error parsing date using " + format5  + ": " + e5 + ". Retrying using " + format6);
              try {
                date = df6.parse(strDate);
              } catch (ParseException e6) {
                System.out.println("df6: Error parsing date using " + format6  + ": " + e6 );
              }
            }
          }
        }
      }
    }

    return date;
  }

}
