package com.filogix.externallinks.services;

import javax.xml.soap.SOAPException;

import org.apache.axis.message.SOAPHeaderElement;

public class RequestHeaderElement {

	public String namespace;
	public String localPart;
	public Object value;
	public String prefix;
	
	public SOAPHeaderElement headerElement;
	
    public RequestHeaderElement(String namespace, String localPart, String prefix, 
            String value)  throws SOAPException {
    	this.namespace = namespace;
    	this.localPart = localPart;
    	this.prefix = prefix;
    	this.value = value;
    	headerElement = new SOAPHeaderElement(namespace, localPart);
    	headerElement.addNamespaceDeclaration(prefix, namespace);
    }

    public RequestHeaderElement(String namespace, String localPart, String prefix) 
    	throws SOAPException{
    	this.namespace = namespace;
    	this.localPart = localPart;
    	this.prefix = prefix;
    	headerElement = new SOAPHeaderElement(namespace, localPart);
    	headerElement.addNamespaceDeclaration(prefix, namespace);
    }

    public void setPrefix(String prefix) {
    	this.prefix = prefix;    	
    	headerElement.setPrefix(prefix);
    }
    
    public void setValue(String value) {
    	this.value = value;
    	headerElement.setValue(value);
    }
    
    public String getPrefix() {
    	return this.prefix;
    }
    
    public String getNamespace() {
    	return namespace;
    }
    
    public String getLocalPart() {
    	return localPart;
    }

    public Object getValue() {
    	return value;
    }
    
    public SOAPHeaderElement getSOAPHeaderElement() {
    	return headerElement;
    }
}
