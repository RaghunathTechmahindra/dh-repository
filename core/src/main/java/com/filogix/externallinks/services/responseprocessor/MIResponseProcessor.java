package com.filogix.externallinks.services.responseprocessor;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.MasterDeal;
import com.basis100.deal.entity.Response;
import com.basis100.deal.entity.ServiceResponseQueue;
import com.basis100.deal.history.DealHistoryLogger;
import com.basis100.deal.miprocess.MIProcessHandlerWrapper;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.util.TypeConverter;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.picklist.BXResources;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.services.RequestService;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.mi.MIRequestUtils;
import com.filogix.externallinks.services.util.XMLUtil;
import com.filogix.util.Xc;

public abstract class MIResponseProcessor
    implements IResponseProcessor
{
    private final static Logger logger = 
        LoggerFactory.getLogger(MIResponseProcessor.class);
    protected static final String MI_RESPONSE_FLAG_M = "M";
    protected static final String MI_RESPONSE_FLAG_C = "C";
    protected static final String ERROR = "R";
    protected static final String CANCELLED = "C";
    protected static final String MANUAL_UNDERWRITING = "M";
    protected static final String APPROVED = "A";
    protected static final String DECLINED = "D";
    protected final static String DECLINEDN = "N";

    public void processResponse(Object payload, ServiceInfo info, boolean validate)
        throws Exception
    {
        logger.debug("MIResponseProcessor.processResponse -- start");
        
        if (payload == null || "".equals(payload))
            throw new IllegalArgumentException(RequestService.DATX_ERROR);
        if (info == null)
            throw new IllegalArgumentException("ServiceInfo is invalid ");
        if (info.getSrk() == null)
            throw new IllegalArgumentException("srk is invalid");
        if (info.getDeal() == null || info.getDeal().getDealId() <= 0)
            throw new IllegalArgumentException("ServiceInfo.deal is invalid ");
        if (info.getChannelTransactionKey() == null)
            throw new IllegalArgumentException("ChannelTransactionKey is invalid ");

        //logger.debug("payload=\r\n" + payload);
        logger.debug("dealid = " + info.getDeal().getDealId());
        logger.debug("copyid = " + info.getDeal().getCopyId());
        logger.debug("ChannelTransactionKey = " + info.getChannelTransactionKey());
        
        
        processResponse((String) payload, info);
        logger.debug("MIResponseProcessor.processResponse -- end");

    }

    protected void processResponse(String payload, ServiceInfo info) throws Exception
    {
        SessionResourceKit srk = info.getSrk();
        Deal deal = info.getDeal();

        try
        {
            IMIResponseAdapter miResponse = getMIResponse(payload);
            MIProcessHandlerWrapper handler = new MIProcessHandlerWrapper();


    	    // Express 3.2.2GR -- STARTS
    	    // FXP24416, Mar 5 09, fixed merge problem - start
    	    // Express 3.4GR -- STARTS
    	    if (async(info.getChannelTransactionKey(), srk) && (Mc.MI_INSURER_AIG_UG != deal.getMortgageInsurerId()
                    || checkMIStatus(deal.getMIStatusId()))) {
    	        logger.info("UGMIResponseProcessor: UNSOLICITED RESPONSE");

    	    	// message lanugage is by Primary Borrower preference
    	    	Borrower borrower = new Borrower(deal.getSessionResourceKit(), null);
    	    	borrower = borrower.findByPrimaryBorrower(deal.getDealId(), deal.getCopyId());
    	    	String msg = BXResources.getSysMsg(Mc.MI_RESPONSE_MSG_UNSOLICITED_RESPONSE, 
    	    			borrower.getLanguagePreferenceId());
    	    	String insurerName = BXResources.getPickListDescription(
    	    			srk.getExpressState().getDealInstitutionId(), 
    	    			Xc.PKL_MORTGAGEINSURER, 
    	    			Mc.MI_INSURER_AIGUG, borrower.getLanguagePreferenceId());
    	    	msg = msg.replaceAll("%s", insurerName);
    	    	logger.info("UGMIResponseProcessor: Message for DealHisroty: " + msg);
    		    addDealHisotry(deal, msg);
    		    this.updateResponse(miResponse, srk, info.getChannelTransactionKey());
    		    
    	    	return;
    	    }	   
    	    // Express 3.2.2GR -- ENDS

            int miStatus = convertStatus(miResponse, deal);
            logger.debug("MIResponseProcessor.processResponse miStatus = " + miStatus);
            // FXP24416, Mar 5 09, fixed merge problem  - end
            // set values to common field of MIProcessHandler
            setPropagateMIFieldBase(miResponse, handler, deal, miStatus);


            // set values to specific field of MIProcessHandler
            setPropagateMIFieldServiceSpecific(miResponse, handler, deal, miStatus);

            // delegate propagate
            handler.propagateMIAttributes(srk, deal);

            deal.findByPrimaryKey((DealPK) deal.getPk());

            writeLog(deal, srk);

            if (checkMIStatusIsApproved(miStatus))
            {
                maintainDealFee(handler, miResponse, deal, srk);
            }

            updateResponse(miResponse, srk, info.getChannelTransactionKey());
            
            MIRequestUtils.addDealHistLogForResponse(deal, srk);

        } catch (Exception e)
        {
            logger.error(StringUtil.stack2string(e));
            logger.error(e.getMessage(), e);
            String msg = BXResources.getRichClientMsg("AML_SESSION_ENDED", info
                .getLanguageId());
            throw new Exception(msg, e);
        }
    }


    abstract protected void setPropagateMIFieldServiceSpecific(
        IMIResponseAdapter miResponse, MIProcessHandlerWrapper handler, Deal deal,
        int miStatus);

    /**
     * @return - MAP(feeid, premiumAmount)
     */
    abstract protected Map getFeeAndPremiumAmountMap(IMIResponseAdapter miResponse,
        Deal deal);

    abstract protected IMIResponseAdapter getMIResponse(String response) throws Exception;

    protected void setPropagateMIFieldBase(IMIResponseAdapter miResponse,
        MIProcessHandlerWrapper handler, Deal deal, int miStatus)
    {
        handler.setMIStatusId(miStatus);

        // FS, Functional Requirements for AIG UG Ingestion
        // Update deal.openMIResponseFlag = nulls if MI Status <> �Manual
        // Underwriting Required�
        if (miStatus != Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY)
            handler.setOpenMIResponseFlag(null);

        handler.setMIResponse(makeMortgageInsuranceResponseText(miResponse, deal));

        if (checkMIStatusIsApproved(miStatus) && checkMIPremiumChange(miResponse, deal))
        {
            handler.setMIPremiumAmount(XMLUtil.bigDecimal2Double(miResponse
                .getDeal_MIPremium(), BigDecimal.ROUND_HALF_UP));
            handler.setMIFeeAmount(XMLUtil.bigDecimal2Double(miResponse
                .getDeal_MIFeeAmount(), BigDecimal.ROUND_HALF_UP));
            handler.setMIPremiumPST(XMLUtil.bigDecimal2Double(miResponse
                .getDeal_MIPremiumPst(), BigDecimal.ROUND_HALF_UP));
        }
    }

    protected void updateResponse(IMIResponseAdapter miResponse, SessionResourceKit srk,
        String channelTransactionKey) throws Exception
    {
        logger.debug("updateResponse - start");

        boolean inTx = srk.isInTransaction();
        if (inTx == false)
            srk.beginTransaction();

        Response response = new Response(srk);
        logger.info("updateResponse:info.getChannelTransactionKey() = "
            + channelTransactionKey);

        response = response.findByChannelTransactionKey(channelTransactionKey);

        String stausMessage = miResponse.getDeal_StatusMessage();
        logger.debug("updateResponse:stausMessage is " + stausMessage);

        response.setStatusMessage(stausMessage);
        response.ejbStore();

        if (inTx == false)
            srk.commitTransaction();
        logger.debug("updateResponse - end");
    }

    protected void maintainDealFee(MIProcessHandlerWrapper handler,
        IMIResponseAdapter miResponse, Deal deal, SessionResourceKit srk)
        throws Exception
    {
        logger.debug("maintainDealFee - start");
        // delete dealfee data with dealid
        handler.propogateDealFeeRemoval(srk, deal);

        // create dealfee for every copyid
        Map feeMap = getFeeAndPremiumAmountMap(miResponse, deal);
        if(feeMap == null || feeMap.isEmpty()){
            return;
        }
        CalcMonitor dcm = CalcMonitor.getMonitor(srk);
        MasterDeal md = new MasterDeal(srk, dcm, deal.getDealId());
        Iterator dit = md.getCopyIds().iterator();
        // create dealfee for all deal copies
        while (dit.hasNext())
        {
            Integer curcpy = (Integer) dit.next();
            Deal currentDeal = null;

            try
            {
                currentDeal = new Deal(srk, dcm, deal.getDealId(), curcpy.intValue());
            } catch (FinderException fe)
            {
                logger.warn("this copy no longer exists - copyId: " + curcpy);
                continue;
            }
            logger.debug("maintainDealFee currentDeal " + "dealid ="
                + currentDeal.getDealId() + " copyid=" + currentDeal.getCopyId());

            for (Iterator feeids = feeMap.keySet().iterator(); feeids.hasNext();)
            {
                Integer feeid = (Integer) feeids.next();

                BigDecimal bdValue = (BigDecimal) feeMap.get(feeid);

                logger.debug("maintainDealFee feeMap " + "feeId =" + feeid + " premium="
                    + bdValue);

                if (existsPremium(bdValue) == false)
                    continue;

                double feeAmount = XMLUtil.bigDecimal2Double(bdValue,
                    BigDecimal.ROUND_HALF_UP);
                // MIDealFeeBuilder is different from MIFeeBuilder.
                // MIFeeBuilder will use always CMHC, if payorprofileid is
                // mortgageinsurer
                // MIDealFeeBuilder will use deal.mortgageInsurerId
                MIDealFeeBuilder.buildFee(srk, dcm, currentDeal, feeid.intValue(), feeAmount);
            }
        } // end while

        logger.debug("maintainDealFee - end");
    }

    protected boolean existsPremium(BigDecimal premium)
    {
        if(premium == null) 
            return false;
        
        try
        {
            return (XMLUtil.bigDecimal2Double(premium, BigDecimal.ROUND_HALF_UP) > 0.00);
        } catch (RuntimeException e)
        {
            return false;
        }
        
    }

    protected boolean checkMIStatusIsApproved(int miStatus)
    {
        return (miStatus == Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED 
            || miStatus == Mc.MI_STATUS_APPROVAL_RECEIVED);
    }

    protected boolean checkMIPremiumChange(IMIResponseAdapter miResponse, Deal deal)
    {
        double miPremiumAmount = 0.0;
        if(miResponse.getDeal_MIPremium() != null){
            miPremiumAmount = XMLUtil.bigDecimal2Double(
                    miResponse.getDeal_MIPremium(), BigDecimal.ROUND_HALF_UP);
        }
        return (deal.getMIPremiumAmount() != miPremiumAmount);
    }

    protected String makeMIPolicyNumber(Deal deal, String serviceSpecificRefNum)
    {
        String miPolicyNumStr = deal.getMIPolicyNumber();

        if (TypeConverter.longTypeFrom(serviceSpecificRefNum, 0) <= 0)
            return miPolicyNumStr;

        // this is pintless comparison, just in case follow fs
        return (serviceSpecificRefNum.equals(miPolicyNumStr)) ? miPolicyNumStr
            : serviceSpecificRefNum;
    }

    protected String makeMortgageInsuranceResponseText(IMIResponseAdapter miResponse,
        Deal deal)
    {
        String responseStr = miResponse.getDeal_MIResponse();
        String previousResponses = deal.getMortgageInsuranceResponse();
        String crlf = System.getProperty("line.separator", "\n\r");
        StringBuffer sb = new StringBuffer();
        if (responseStr != null){
            sb.append(responseStr);
        }
        if (previousResponses != null){
            if (responseStr != null){
                sb.append(crlf);
            }
            sb.append(previousResponses);
        }

        //logger.debug("makeMortgageInsuranceResponseText responseStr= \r\n" + sb.toString());
        return sb.toString();
    }

    protected int convertStatus(IMIResponseAdapter miResponse, Deal deal) {

        String openMIResponseFlag = deal.getOpenMIResponseFlag();
        boolean NULL_OR_M = (openMIResponseFlag == null)
                || (openMIResponseFlag.equals(MI_RESPONSE_FLAG_M));
        boolean NOT_M = ((openMIResponseFlag == null) || openMIResponseFlag
                .equals(MI_RESPONSE_FLAG_C));

        int miStatusId = deal.getMIStatusId();
        long refNum = TypeConverter.longTypeFrom(miResponse
                .getServiceSpecificReferenceNumber(), 0);
        String responseIndicator = miResponse.getDeal_StatusCode();

        logger.debug("MIResponseProcessor.convertStatus original miStatusId = " + miStatusId);
        logger.debug("MIResponseProcessor.convertStatus NULL_OR_M = " + NULL_OR_M);
        logger.debug("MIResponseProcessor.convertStatus NOT_M = " + NOT_M);
        logger.debug("MIResponseProcessor.convertStatus refNum = " + refNum);
        logger.debug("MIResponseProcessor.convertStatus responseIndicator = " + responseIndicator);
        
        if (refNum <= 0 && ERROR.equals(responseIndicator)) {
            miStatusId = Mc.MI_STATUS_CRITICAL_ERRORS_REC_FROM_INSURER;
        } else if (refNum > 0 && ERROR.equals(responseIndicator)) {
            miStatusId = Mc.MI_STATUS_ERRORS_RECEIVED_FROM_INSURER;
        } else if (CANCELLED.equals(responseIndicator) && NOT_M) {
            miStatusId = Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC;
        } else if (MANUAL_UNDERWRITING.equals(responseIndicator) && NULL_OR_M) {
            miStatusId = Mc.MI_STATUS_MANUAL_UNDERWRITING_REPLY;
        } else if (APPROVED.equals(responseIndicator) && NULL_OR_M) {
            double ingestedPremiumDue = XMLUtil.bigDecimal2Double(miResponse
                    .getDeal_MIPremium(), BigDecimal.ROUND_HALF_UP);
            miStatusId = (deal.getMIPremiumAmount() != ingestedPremiumDue) 
                    ? Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED
                    : Mc.MI_STATUS_APPROVAL_RECEIVED;
        } else if ((DECLINED.equals(responseIndicator) || DECLINEDN
                .equals(responseIndicator))
                && NULL_OR_M) {
            miStatusId = Mc.MI_STATUS_DENIED_RECEIVED;
        }
        logger.debug("MIResponseProcessor.convertStatus result = " + miStatusId);
        return miStatusId;

    }

    public void writeLog(Deal deal, SessionResourceKit srk) throws Exception
    {
        DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);

        String sText = dhl.mortgageInsuranceResponse(deal.getDealId(), deal.getCopyId());

        int statusID = deal.getMIStatusId();

        dhl.log(deal.getDealId(), deal.getCopyId(), sText, Mc.DEAL_TX_TYPE_EVENT, srk
            .getUserProfileId(), statusID);

        logger.info("changed MI Status : dealid = " + deal.getDealId()
            + ", miStatusId = " + statusID);
        logger.info(sText);
    }

	/**
	 * addDealHistory
	 * when the unsolicited asynchronous response came, 
	 * add dealHistory and don't update request/response table
	 * @param deal
	 * @param msg
	 * 
	 * @since 3.2.2GR
	 * 
	 * @vrsion 1.1 deleted befinTransaction, commitTransaction, freeResources since ECMManager does it
	 */
	private void addDealHisotry(Deal deal, String msg) 
		throws RemoteException, CreateException {
		
        SessionResourceKit srk = deal.getSessionResourceKit();
        DealHistory dh = new DealHistory(srk);
        try {
//	        srk.beginTransaction();
	        dh = dh.create(deal.getDealId(), deal.getUnderwriterUserId(), Mc.DEAL_TX_TYPE_EVENT,
			    		deal.getStatusId(), msg, new Date());
//	        srk.commitTransaction();
        } catch (Exception e) {
        	logger.error("could not add DealHisotry for Unsolicited reaponse");
        } finally {
//            srk.freeResources();
        }
    } 

    /**
     * <p>async</p>
     * find if the process is called by ECM by find "in Process" 
     * ServiceResponseQueue with servieTransaction Key
     * @param ctKey
     * @return
     * @since 3.2.2GR
     */
    private boolean async(String ctKey, SessionResourceKit srk) {
		
		if (ctKey == null || ctKey.trim().length() == 0 ){
			return false;
		}
		try {
			ServiceResponseQueue res = new ServiceResponseQueue(srk, null);
			res.setSilentMode(true);
			Collection reses = res.findOpenByTransactionkey(ctKey, "UGMI");
			if ( reses == null || reses.size()== 0 ) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}
    

    /**
     * MI Status in MI response
     * 4.3GR AIG/UG Multiple Response
     * @param miStatusId
     * @return
     */
    public boolean checkMIStatus(int miStatusId) {
        switch (miStatusId) {
        case Mc.MI_STATUS_BLANK:
        case Mc.MI_STATUS_CANCELLED:
        case Mc.MI_STATUS_CANCELLATION_CONFIRMATION_REC:
        case Mc.MI_STATUS_APPROVAL_RECEIVED:
        case Mc.MI_STATUS_APPROVED:
        case Mc.MI_STATUS_DENIED_RECEIVED:
        case Mc.MI_STATUS_DENIED:
        case Mc.MI_STATUS_APPROVAL_RECIEVED_PREMIUM_CHANGED:    
            return true;
        default:
            return false;
        }
    }
}
