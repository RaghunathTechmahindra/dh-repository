/*
 * Created on Feb 7, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.filogix.externallinks.services;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.basis100.deal.calc.CalcMonitor;
import com.basis100.deal.entity.Deal;
import com.basis100.resources.SessionResourceKit;

/**
 * @author MMorris
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ServiceInfo implements Cloneable, Serializable
{
	private Log log = LogFactory.getLog(ServiceInfo.class);  
    
    private static final String PAYLOAD_XML = "xml";
    private static final String PAYLOAD_OBJECT = "object";
    private Integer dealId;
    private Integer copyId;
    private Integer dealStatusId;
    private Integer miStatusId;
    private Deal deal;
    private Integer transactionType;
    private String requestType;
    private int requestId;
    private Integer productType;
    private SessionResourceKit srk;
    private CalcMonitor calcMonitor;
    private String payLoadType;
    private String channelTransactionKey;
    private int languageId;
    
    
    
    public String getChannelTransactionKey(){
     
        return channelTransactionKey;
    }

    public void setChannelTransactionKey(String key){

        channelTransactionKey = key;
    }
    
    public Integer getCopyId()
    {
        return copyId;
    }
    public void setCopyId(Integer copyId)
    {
        this.copyId = copyId;
    }
    public Integer getDealId()
    {
        return dealId;
    }
    public void setDealId(Integer dealId)
    {
        this.dealId = dealId;
    }
    public Integer getProductType()
    {
        return productType;
    }
    public void setProductType(Integer productType)
    {
        this.productType = productType;
    }
    public Integer getTransactionType()
    {
        return transactionType;
    }
    public void setTransactionType(Integer transactionType)
    {
        this.transactionType = transactionType;
    }
    public SessionResourceKit getSrk()
    {
        return srk;
    }
    public void setSrk(SessionResourceKit srk)
    {
        this.srk = srk;
    }
    public CalcMonitor getCalcMonitor()
    {
        return calcMonitor;
    }
    public void setCalcMonitor(CalcMonitor calcMonitor)
    {
        this.calcMonitor = calcMonitor;
    }
    public String getRequestType()
    {
        return requestType;
    }
    public void setRequestType(String requestType)
    {
        this.requestType = requestType;
    }
    public int getRequestId()
    {
        return requestId;
    }
    public void setRequestId(int requestId)
    {
        this.requestId = requestId;
    }
    public String getPayLoadType()
    {
        return payLoadType;
    }
    public void setPayLoadType(String payLoadType)
    {
        this.payLoadType = payLoadType;
    }
    public void setDeal(Deal deal) {
    	this.deal = deal;
    }
    public Deal getDeal() {
    	return deal;
    }
    
    public void setLanguageId(int languageId) {
    	this.languageId = languageId;
    }
    public int getLanguageId() {
    	return languageId;
    }

	public Integer getDealStatusId() {
		return dealStatusId;
	}

	public void setDealStatusId(Integer dealStatusId) {
		this.dealStatusId = dealStatusId;
	}

	public Integer getMiStatusId() {
		return miStatusId;
	}

	public void setMiStatusId(Integer miStatusId) {
		this.miStatusId = miStatusId;
	}
	
	public Object clone() {
		ServiceInfo info = null;
		try {
			info = (ServiceInfo)super.clone();
		}
		catch(CloneNotSupportedException e) {
			log.debug("EXCEPTION:: SetviceInfo:: " + e.getMessage());
		}
		
		return info;
	}
    
    
   }
