/**
 * <p>Title: AppraisalPayloadData.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 16, 2006)
 *
 */

package com.filogix.externallinks.services.datx;

import com.basis100.entity.RemoteException;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbServicePayloadData;
import com.filogix.schema.datx.appraisal.AppraisalRequest;
import com.filogix.schema.datx.appraisal.impl.AppraisalRequestImpl;

public class DatxAppReqPayloadData extends JaxbServicePayloadData
{
    public AppraisalRequest  appraisalRequestPayload;
    
    public DatxAppReqPayloadData(SessionResourceKit srk, int requestId, int borrowerId, int copyId, int langId) 
        throws RemoteException, ExternalServicesException
    {
        super(srk, requestId, borrowerId, copyId);
        initAppReq();
    }

    public DatxAppReqPayloadData(SessionResourceKit srk, int requestId, int copyId, int langId) 
        throws RemoteException, ExternalServicesException
    {
        super(srk, requestId, copyId, langId);
        initAppReq();
    }

    public void initAppReq()
        throws ExternalServicesException
    {
        appraisalRequestPayload = new AppraisalRequestImpl();
        appraisalRequestPayload.setProperty(jaxbProperty);
        appraisalRequestPayload.setRequester(jaxbRequester);
        appraisalRequestPayload.setAppraisalContactInformation(jaxbContactInfo);
    }

    public AppraisalRequest getAppraisalRequestPayload()
    {
        return (AppraisalRequest)getServicePayload();
    }
    
    public Object getServicePayload()
    {
        return appraisalRequestPayload;
    }
    
}
