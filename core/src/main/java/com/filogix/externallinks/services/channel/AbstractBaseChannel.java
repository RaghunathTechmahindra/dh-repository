package com.filogix.externallinks.services.channel;

import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.ServiceInfo;

public abstract class AbstractBaseChannel implements IChannel {

    public final static String XMLNS_WSA = "http://www.w3.org/2005/08/addressing";
    public final static String XMLNS_WSS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
    public final static String XMLNS_FILOGIC_ESB = "http://www.filogix.com/esb";
    public static final String XMLNS_MP_ESB_SCHEMAS = "http://marketplace.dhltd.com/esb/schemas";
    public static final String XMLNS_MP_ESB_HEADERS = "http://marketplace.dhltd.com/esb/headers";
    public static final String XMLNS_MP_SCH_META ="http://marketplace.dhltd.com/schemas/metadata";
    public static final String XMLNS_MP_FXR_REQUEST = "http://www.filogix.com/connectivity/request";
	
    // timeoutDP (Express and DP) in milliseconds.
    private long timeoutDP = 60000;
    
	// timeoutMQ (DP and MQ) in milliseconds.
    // timeoutMQ has to be smaller than timeoutDP
    private long timeoutMQ = 55000;
    
    
    // key to channel table
    private int channelId;
    // will populated with channel data
    private String endpoint;
    private String username;
    private String password;
    // soap header elements
    private String wsAddressing_action;
    private String wsAddressing_from;
    
    private boolean initDone;


    public void init(ServiceInfo serviceInfo) throws ExternalServicesException {
    }
    

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getWsAddressing_action() {
        return wsAddressing_action;
    }

    public void setWsAddressing_action(String wsAddressing_action) {
        this.wsAddressing_action = wsAddressing_action;
    }

    public String getWsAddressing_from() {
        return wsAddressing_from;
    }

    public void setWsAddressing_from(String wsAddressing_from) {
        this.wsAddressing_from = wsAddressing_from;
    }

    public boolean isInitDone() {
        return initDone;
    }

    public void setInitDone(boolean initDone) {
        this.initDone = initDone;
    }

    public long getTimeoutDP() {
		return timeoutDP;
	}


	public void setTimeoutDP(long timeoutDP) {
		this.timeoutDP = timeoutDP;
	}


	public long getTimeoutMQ() {
		return timeoutMQ;
	}


	public void setTimeoutMQ(long timeoutMQ) {
		this.timeoutMQ = timeoutMQ;
	}
}
