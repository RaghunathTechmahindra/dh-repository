package com.filogix.externallinks.services.channel;

import java.net.URL;
import java.rmi.Remote;

import org.apache.axis.client.Stub;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.datx.framework.packaging.FXResponse;
import com.filogix.datx.framework.packaging.MIPremiumRequestService;
import com.filogix.datx.framework.packaging.MIPremiumRequestServiceServiceLocator;
import com.filogix.datx.framework.packaging.MIRequestServiceServiceLocator;
import com.filogix.datx.framework.packaging.PMIService;
import com.filogix.externallinks.services.ServiceDelegate;
import com.filogix.externallinks.services.ServiceInfo;

/**
 * This is a class that provides the following basic functions for
 * subclasses --> createRequest --> createResponse
 * 
 * Created on Aug 28, 2006
 * Refactored May 02, 2007
 * 
 * @author LZhou
 * refactored by SJelauc
 */

public class TransactionalChannel extends SimpleChannel {
	
	protected static Log logger = LogFactory.getLog(TransactionalChannel.class);
	private static PMIService service;
	
	private TransactionalChannel() {
	}

	public static IChannel getInstance() {
		return new TransactionalChannel();
	}
	
	public void init(ServiceInfo serviceInfo) throws Exception {
		setupChannelInfo(serviceInfo);
	    
		if (getEndpoint() != null) {
			try {
				if (serviceInfo.getRequestType().contains(ServiceDelegate.PREMIUM_REQUEST_TYPE_PMI)) {
					service = (new MIPremiumRequestServiceServiceLocator())
					.getMIPremiumRequestService(new URL(getEndpoint()));
				} else if (serviceInfo.getRequestType().equals(ServiceDelegate.MI_REQUEST_TYPE_PMI)) {
					service = (new MIRequestServiceServiceLocator())
					.getMIRequestService(new URL(getEndpoint()));
				}
				((Stub) service).setTimeout(getWsTimeout() * 1000); 
			} catch (Exception e) {
				logger.error(this.getClass().getName()
						+ ": initialization failed.");
				throw new Exception(this.getClass().getName()
						+ ": initialization failed.");
			}
		} else {
			logger.error(this.getClass().getName()
					+ ": endpoint is not specified.");
			throw new Exception(this.getClass().getName()
					+ ": endpoint is not specified.");
		}
	}
		
	// REMOVE ME
//	abstract protected FXResponse transmit(ServiceInfo serviceInfo,
//			String xmlPayload) throws Exception;


	public synchronized FXResponse transmit(ServiceInfo serviceInfo, String xmlPayload) throws Exception {
    	
        return service.invoke(construct(serviceInfo), xmlPayload);
    }
}
