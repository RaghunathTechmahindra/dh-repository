package com.filogix.externallinks.services.datx;

import com.filogix.externallinks.services.wsdl.appraisal.DatXResponse;




public class AcknowledgementAppraisalResProcessor
    extends AcknowledgementResProcessor
{

  protected DatxResponseCodeConverterBase getResponseCodeConverter()
  {
    return new DatxResponseCodeAppraisalConverter();
  }

  protected String getResultDescription(Object acknowledgement)
  {
      DatXResponse myAcknowledgement = (DatXResponse)acknowledgement;
    return myAcknowledgement.getResultDescription();
  }
  protected Integer getResultCode(Object acknowledgement)
  {
      DatXResponse myAcknowledgement = (DatXResponse)acknowledgement;
    return myAcknowledgement.getResultCode();
  }
  protected String getDatXKey(Object acknowledgement)
  {
      DatXResponse myAcknowledgement = (DatXResponse)acknowledgement;
    return myAcknowledgement.getDatXKey();
  }
  protected String getExternalKey(Object acknowledgement)
  {
      DatXResponse myAcknowledgement = (DatXResponse)acknowledgement;
    return myAcknowledgement.getExternalKey();
  }
}
