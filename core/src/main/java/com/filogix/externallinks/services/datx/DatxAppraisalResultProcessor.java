package com.filogix.externallinks.services.datx;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xml.sax.SAXException;

import com.basis100.deal.entity.AppraisalOrder;
import com.basis100.deal.entity.AvmSummary;
import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.ResponsePK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.log.SysLog;
import com.filogix.datx.messagebroker.valuation.AVMResponseBean;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServiceResponseBase;
import com.filogix.externallinks.framework.ServiceResponseProcessorBase;

/*
 * DatxAppraisalResponseProcessor.java
 * 
 * External links framework
 * 
 * Created: 4-April-2006
 * Author:  Midori Aida
 * 
 */
public class DatxAppraisalResultProcessor extends ServiceResponseProcessorBase {
  private ServiceResponseBase _base = null;
  private AVMResponseBean   _bean;
  private Response          _response;
  private int               _responseId;
  private int               _statusId;
  private String            _statusDesc;
  private Date              _statusDate;
  
  public int process(ServiceResponseBase base) throws ExternalServicesException {
    int result = -1;
    _base = base;
    _bean = (AVMResponseBean)base.getResponseBean();
//  _bean = (AppraisalResponseBean)base.getResponseBean();
    try {
      _srk.beginTransaction();

      try {
        String statusCondition = _bean.getStatusCondition();
        
        _statusId = getStatusId(statusCondition);
        _statusDesc = _bean.getStatusDescription();
        
        updateRequest();
        
        if (_statusId != ServiceConst.RESPONSE_STATUS_FAILED &&
            _statusId != ServiceConst.RESPONSE_STATUS_SYSTEM_ERROR &&
            _statusId != ServiceConst.RESPONSE_STATUS_PROCESSING_ERROR)
        {        
          updateResponse();
          
          // Catherine: sanity check //TODO: remove!
          if (_responseId == 0){
            throw new Exception("Could not create a new response record, responseId == null");
          }
          
          updateAppraisal();          

          result = _responseId;          
        }

        _srk.commitTransaction();
        
      } catch (Exception e) {
        _logger.error(StringUtil.stack2string(e));
        _srk.cleanTransaction();
      }

    } catch (JdbcTransactionException e1) {
      _logger.error(StringUtil.stack2string(e1));
      throw new ExternalServicesException("Error when processing DatX reponse for requestId = " + _requestId + " :" + e1.getMessage());
    }

    return result;
  }

  private void updateRequest() throws RemoteException{
    // 1) get status, status date and condition
    _statusDate = new Date();
    
    String sDate = _bean.getValuationEffectiveDate();
    if (sDate != null){
      _statusDate = getDateFromString(sDate);
    }
    
    // 2) Update Request table
    Request request = _base.getRequest();
    
    request.setStatusDate(_statusDate);
    request.setStatusMessage(_statusDesc);
    request.setRequestStatusId(_statusId);
    
    request.ejbStore();
    
  }
  /*
   * save Response record
   */
  private void updateResponse() throws RemoteException, FinderException, CreateException {
    
    // 3) Create new Response record
    _response = new Response(_srk);
//    _response.create( _response.createPrimaryKey(), new Date(), _requestId, 
//                      _base.getRequest().getDealId(),  _base.getRequest().getCopyId(), _statusDate, _statusId);
    
    _response.setStatusMessage(_statusDesc);
    _response.setChannelTransactionKey(getConversationKey());
    
    _response.ejbStore();
    
    _responseId = _response.getResponseId();
    
  }

  /*
   * save Response record
   */
  private void updateAppraisal() throws RemoteException, FinderException, CreateException {
      AppraisalOrder appraisal = new AppraisalOrder(_srk, null);
    
//      appraisal.create(_responseId);
    // appraisal.setappraisalReport(_bean.getReport);  // FIXME: Catherine: update this when new version of DatX AVM response bean is ready

        
      appraisal.ejbStore();
  }
  
  private String getConversationKey() {
    String result = "";
    DatxResponseParser parser = new DatxResponseParser();
    
    try {
      parser.read(_base.getResponseXml());
      result = parser.getConversationKey();
      
    } catch (IOException e) {
      _logger.error("Error parsing Datx response @getConversationKey(): " + StringUtil.stack2string(e));
    } catch (SAXException e) {
      _logger.error("Error parsing Datx response @getConversationKey(): " + StringUtil.stack2string(e));
    }
    return result;
  }

  private Date getDateFromString(String strDate) {
    SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy");     // FIXME: Catherine: make sure the date format is fixed! or is it locale?

    Date date = new Date();
    try {
      date = df.parse(strDate);
    } catch (ParseException e) {
      _logger.error("Error parsing date: " + e);
    }

    return date;
  }

  
  protected int getStatusId(String statusCondition)
  {
      int statusId = -1;
      try
      {
         statusId = Integer.parseInt(_bean.getStatusCode() );
      }
      catch(NumberFormatException nfe)
      {
          SysLog.debug(this.getClass(), "status Code is not integer");
      }
      return statusId;
  }
}
