package com.filogix.externallinks.services.datx;

import java.net.URL;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import com.basis100.deal.entity.Response;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.filogix.datx.router.service.DatXRouter_PortType;
import com.filogix.datx.router.service.DatXRouter_Service;
import com.filogix.datx.router.service.DatXRouter_ServiceLocator;
import com.filogix.datx.router.service.FXRequest;
import com.filogix.datx.router.service.FXResponse;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbServicePayloadData;
import com.filogix.externallinks.framework.JaxbServicePayloadGenerator;
import com.filogix.externallinks.framework.JaxbServiceResponse;
import com.filogix.externallinks.framework.ServiceChannelBase;
import com.filogix.externallinks.framework.ServicePayloadData;
import com.filogix.externallinks.framework.ServiceResponseBase;
import com.filogix.externallinks.framework.WSServiceChannel;
import com.filogix.schema.datx.closing.ClosingServicesType;

public class DatXClosingChannel extends WSServiceChannel
{
	public static JAXBContext _jaxbContext;

	static
  {
    try
    {
        _jaxbContext = JAXBContext.newInstance(ClosingServicesType.class.getPackage().getName());
    }
    catch(JAXBException e)
    {
        System.out.println("ERROR");
    }
  }
  // these calsses are commonly used but package will be different so that 
  // they cannot be in the super class and initRequestBean() method
  protected FXRequest     _datXRequest;
  protected FXResponse    _datXresponse;
	
  protected void initRequestBean()
      throws RemoteException,
          FinderException,
          ExternalServicesException
  {
    _datXRequest = new FXRequest();
    
    _datXRequest.setClientKey(""+_request.getRequestId());
    _datXRequest.setUsername(_channel.getUserId());
    _datXRequest.setPassword(_channel.getPassword());
    _datXRequest.setServiceKey(_request.getChannelTransactionKey());        
    _datXRequest.setEncoding("");
    if (_payloadSchemaName.indexOf(".") < 0 )
    {
    	_datXRequest.setSchemaName(_payloadSchemaName + ".xsd");
    }
    else
    {
      _datXRequest.setSchemaName(_payloadSchemaName);
    }    	
    _datXRequest.setAuxData(null);
//        int timeout = _request.findDatxTimeout(_request.getServiceProductId(), _request.getChannelId());
//        _logger.debug("Timeout = " + timeout);
//        _datXRequest.setTimeout(new Integer(timeout));    

    _datXRequest.setProductType(SERVICE_PRODUCT_CLOSING);
    _datXRequest.setRequestType(REQUEST_TRANSACTION_TYPE_CLOSING_STR[_request.getServiceTransactionTypeId() - 1 ]);
    _datXRequest.setServiceProvider(_data.getServiceProvider().getServiceProviderName().toUpperCase() );
  }

	protected JAXBContext getJaxbContext() 
  {
		return _jaxbContext;
	}
    
	public ServiceResponseBase sendData(ServicePayloadData data) throws Exception
    {
        _data = data;
        try
        {
            prepare(_data);

            DatXRouter_ServiceLocator servLoc = getServiceLocater();
            if (servLoc==null)
            {
                String msg = "DatXClosingChannel: DatX service is not available: ClosingService_ServiceLocator = null.";
                _logger.error(msg);
                throw new DatxException(msg);
            }
            DatXRouter_PortType stub = getServiceStub(servLoc);
            if (stub==null)
            {
                String msg = "DatXClosingChannel: DatX service is not available: ClosingService_PortType = null";
                _logger.error(msg);
                throw new DatxException(msg);
            }
            _logger.info("Sending Closing request --->>> ");
            _logger.info("Datx Request UserName: " + _datXRequest.getUsername());
            _logger.info("Datx Request Password: " + _datXRequest.getPassword());
            _logger.info("Datx Request Client Key: " + _datXRequest.getClientKey());
            _logger.info("Datx Request Service Key: " + _datXRequest.getServiceKey());
            _logger.info("Datx Request SchemaName: " + _datXRequest.getSchemaName());
            _logger.info("Datx Request Product Type: " + _datXRequest.getProductType());
            _logger.info("Datx Request Request Type: " + _datXRequest.getRequestType());
            _logger.info("Datx Request Service Provider: " + _datXRequest.getServiceProvider());
            _logger.info("Datx Request TimeOut: " + _datXRequest.getTimeout());
            _logger.info("Datx Request Aux Data: " + _datXRequest.getAuxData());
            _logger.info("xmlPayload --->>");
            _logger.info(_xmlPayload);
            
//            System.out.println("Sending request --->>> ");
//            System.out.println("Datx Request UserName: " + _datXRequest.getUsername());
//            System.out.println("Datx Request Password: " + _datXRequest.getPassword());
//            System.out.println("Datx Request Client Key: " + _datXRequest.getClientKey());
//            System.out.println("Datx Request Service Key: " + _datXRequest.getServiceKey());
//            System.out.println("Datx Request SchemaName: " + _datXRequest.getSchemaName());
//            System.out.println("Datx Request Product Type: " + _datXRequest.getProductType());
//            System.out.println("Datx Request Request Type: " + _datXRequest.getRequestType());
//            System.out.println("Datx Request Service Provider: " + _datXRequest.getServiceProvider());
//            System.out.println("Datx Request TimeOut: " + _datXRequest.getTimeout());
//            System.out.println("Datx Request Aux Data: " + _datXRequest.getAuxData());
//            System.out.println("xmlPayload --->>");
//            System.out.println(_xmlPayload);
            
            FXResponse response = stub.invoke(_datXRequest, _xmlPayload);
            if (response != null)
            {
                _logger.info(getLogMessage());
                _logger.info("Datx Response Result Code: " + response.getResultCode());
                _logger.info("Datx Response Result Desc: " + response.getResultDescription());
                _logger.info("Datx Response Result Service Key: " + response.getServiceKey());
                _logger.info("Datx Response External Ref Number: " + response.getExternalKey());
                
//                System.out.println(getLogMessage());
//                System.out.println("Datx Response Result Code: " + response.getResultCode());
//                System.out.println("Datx Response Result Desc: " + response.getResultDescription());
//                System.out.println("Datx Response Result Service Key: " + response.getServiceKey());
//                System.out.println("Datx Response External Ref Number: " + response.getExternalKey());
                // this is only acknowdlegement
                JaxbServiceResponse result = new JaxbServiceResponse(_srk, _requestId, _copyId);
                result.setResponseBean(response);
                return result;
            }
            else
            {
                _logger.error("DatXResponse is null for request id " + _datXRequest.getClientKey());
                throw new DatxException("DatXResponse is null for request id " + _datXRequest.getClientKey());
            }
        } 
        catch (Exception e)
        {
          String msg = "Error when sending service request to DatX: RequestId = " + this._requestId;
          _logger.error(msg + ": "+ StringUtil.stack2string(e));
          System.out.println(StringUtil.stack2string(e));
          throw new DatxException(msg + ": " + e);
        }
	}
    
    public DatXRouter_ServiceLocator getServiceLocater() throws ServiceException
    {
    	DatXRouter_ServiceLocator servLoc = new DatXRouter_ServiceLocator();
      servLoc.setEndpointAddress(_servicePort, _url+"?wsdl");
      return servLoc;
    }
    
    public DatXRouter_PortType getServiceStub(DatXRouter_ServiceLocator servLoc) throws ServiceException
    {
    	DatXRouter_PortType stub = servLoc.getDatXRouter();
      return stub;
    }

    protected String getLogMessage()
    {
    	return "---- Receiving Acknowledgment response for Closing Request --->>> ";
    }
    

    private FXResponse getStubResponse(FXRequest datXRequest, String payload) throws Exception
    {
        Response responseEntity = new Response(_srk);
        responseEntity = responseEntity.findLastResponseByRequestId(new Integer(datXRequest.getClientKey()).intValue());
        
        FXResponse response = new FXResponse();
        response.setClientKey(datXRequest.getClientKey());
        response.setServiceKey(datXRequest.getServiceKey());
        response.setExternalKey(responseEntity.getChannelTransactionKey());
        response.setResultCode(new Integer(18));
        response.setResultDescription("0:SUCCEED");
        return response;
    }
    
    public static void main(String[] args){
        try{
        	DatXRouter_Service locator = new DatXRouter_ServiceLocator();
        	DatXRouter_PortType service = locator.getDatXRouter();


            FXRequest _request = null;
            _request = new FXRequest();
            _request.setUsername("express_closing_routing");
            _request.setPassword("pwd_routing");
            _request.setClientKey("212121");
            _request.setServiceProvider("CENTRACT");
            _request.setProductType("CLOSING");
            _request.setRequestType("cancelClosingRequest");
            _request.setSchemaName("ServiceCancel.xsd");


            FXResponse response = service.invoke(_request,"<xml></xml>");
            System.out.println(response.getResultCode());
            System.out.println(response.getResultDescription());


        }
        catch(Exception ex){
            System.out.println(ex);
        }
    }



}
