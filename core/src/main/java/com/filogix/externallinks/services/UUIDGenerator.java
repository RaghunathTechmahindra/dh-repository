package com.filogix.externallinks.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.filogix.externallinks.framework.ExternalServicesException;

public abstract class UUIDGenerator implements IUUIDGenerator{
	
    protected static Log _log = LogFactory.getLog(UUIDGenerator.class);
    	
	public abstract String getUUID()throws ExternalServicesException;
	
}
