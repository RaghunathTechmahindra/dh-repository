package com.filogix.externallinks.services.channel;

import com.filogix.externallinks.services.ServiceInfo;

public interface IChannel {
   Object send(ServiceInfo serviceInfo, String xmlPayload) throws Exception;
   public void init(ServiceInfo serviceInfo) throws Exception;
}
