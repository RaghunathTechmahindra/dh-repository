package com.filogix.externallinks.services.responseprocessor.mi;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basis100.deal.util.TypeConverter;
import com.dhltd.marketplace.mi.jaxb.response.MessageType;
import com.dhltd.marketplace.mi.jaxb.response.MortgageInsuranceResponse;
import com.dhltd.marketplace.mi.jaxb.response.ResponseType;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.util.XMLUtil;

public class MarketPlaceMIResponseAdaptor implements IResponseAdaptor {

    private final static Logger logger = LoggerFactory.getLogger(MarketPlaceMIResponseAdaptor.class);

    protected MortgageInsuranceResponse miResponse;
    protected ResponseType response;
    protected List<MessageType> messages;

    public String getMIProviderBranchTransitNumber() {
        return response.getBranchProfile().getMIProviderBranchTransitNumber();
    }

    public BigInteger getDealId() {
        return response.getDealId();
    }

    public String getMIProviderInstitutionCode() {
        return response.getInstitutionProfile().getMIProviderInstitutionCode();
    }

    public BigInteger getLOCAmortizationMonths() {
        return response.getLOCAmortizationMonths();
    }

    public XMLGregorianCalendar getLOCInterestOnlyMaturityDate() {
        return response.getLOCInterestOnlyMaturityDate();
    }

    public BigInteger getLanguagePreferenceId() {
        return response.getLanguagePreferenceId();
    }

    public BigInteger getMIDealRequestNumber() {
        return response.getMIDealRequestNumber();
    }

    public BigDecimal getMIFeeAmount() {
        return response.getMIFeeAmount();
    }

    public BigDecimal getMIPremium() {
        return response.getMIPremium();
    }

    public BigDecimal getMIPremiumPst() {
        return response.getMIPremiumPst();
    }

    public BigDecimal getMIPremiumRate() {
        return response.getMIPremiumRate();
    }

    public String getMIProviderReferenceNumber() {
        return response.getMIProviderReferenceNumber();
    }

    public String getMIResponse() {
        return response.getMIResponse();
    }

    public BigInteger getMortgageInsurerId() {
        return response.getMortgageInsurerId();
    }

    public String getRequestedInsuranceProduct() {
        return response.getRequestedInsuranceProduct();
    }

    public String getStatusCode() {
        return response.getStatusCode();
    }

    public String getStatusMessage() {
        return response.getStatusMessage();
    }

    public void initialize(String payloadXml) throws ExternalServicesException {

        String contextPath = MortgageInsuranceResponse.class.getPackage().getName();

        try {
            JAXBContext jc = JAXBContext.newInstance(contextPath);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            miResponse = (MortgageInsuranceResponse) XMLUtil.string2jaxb(unmarshaller, payloadXml);

            response = miResponse.getResponse();

            messages = miResponse.getMessage();

        } catch (Exception e) {
            logger.error("failed to unmarshall payload", e);
            throw new ExternalServicesException(e);
        }
    }

    public boolean messageContainsAny(String[] arrayCodes) {
        for (String code : arrayCodes) {
            if (messageContains(code))
                return true;
        }
        return false;
    }

    public boolean messageContains(String code) {
        return (search(code) != null);
    }

    private MessageType search(String code) {
        for (MessageType mt : messages) {
            if (code.equals(mt.getMessageCode()))
                return mt;
        }
        return null;
    }

    public double getFeeAmountFor(String code) {
        MessageType mt = search(code);
        if (mt == null)
            return 0.0d;
        return parseCurrencyValueFrom(mt.getMessageText());
    }

    private static double parseCurrencyValueFrom(String msg) {

        StringBuilder msgbuf = new StringBuilder();
        boolean currency = false;
        for (char c : msg.toCharArray()) {
            if (c == '$')
                currency = true;
            if (currency && (Character.isDigit(c) || c == '.'))
                msgbuf.append(c);
        }
        return TypeConverter.doubleTypeFrom(msgbuf.toString());
    }
}
