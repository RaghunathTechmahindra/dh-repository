package com.filogix.externallinks.services.responseprocessor;

import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.Validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealPropagator;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.mi.aigug.jaxb.premiumresponse.Message;
import com.filogix.externallinks.mi.aigug.jaxb.premiumresponse.PremiumResponse;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.util.XMLUtil;

public abstract class MIPremiumResponseProcessor implements IResponseProcessor {

    private static final Log _log = LogFactory.getLog(MIPremiumResponseProcessor.class);
    
    protected static JAXBContext jc;
    protected static final String schema = "com.filogix.externallinks.mi.aigug.jaxb.premiumresponse";
    protected static final String ERROR_STATUS_CODE = "R";
    private static final int MAX_LENGHT_DEAL_MIPREMIUMERRORMESSAGE = 256;

    static {
        try {
            jc = JAXBContext.newInstance(schema);
        } catch (Exception e) {
            _log.error("MIPremiumResponseProcessor: JAXBContext initialization failed.");
            _log.error(StringUtil.stack2string(e));
            _log.error(e);
        }
    }

    /**
     * inplementation of the IResponseProcessor interface method.
     */
    public void processResponse(Object obj, ServiceInfo info, boolean validate) throws Exception {

        if(obj == null && info.getDeal().getMIPremiumErrorMessage() != null) {
            processDatXError(info);
            return;
        }

        SessionResourceKit srk = info.getSrk();
        String payload = (String) obj;

        boolean isInTx = srk.isInTransaction();

        try {
            PremiumResponse response = getPremiumResponse(payload);

            this.handleResponse(response, info, validate);

            //this.createResponse(info);   // LZhou Aug. 28, 2006 see function desc.
            // changed for FXP24408 to use deal from calc ---- START
            Deal deal = null;
            DealPK dealPk = null;
            Integer dealId = info.getDealId();
            Integer copyId = info.getCopyId();
            if (info.getDeal() != null ) {
                deal = info.getDeal();
                dealPk = (DealPK)deal.getPk();
            } else {
                srk.getExpressState().setDealInstitutionId(info.getDeal().getInstitutionProfileId());
                dealPk = new DealPK(dealId.intValue(), copyId.intValue());
                deal = new Deal(srk, null).findByPrimaryKey(dealPk);
            }

            if(isInTx == false)
                srk.beginTransaction();

            BigDecimal ingestedMIPremium    = response.getDeal().getMIPremium();
            BigDecimal ingestedMIFeeAmount  = response.getDeal().getMIFeeAmount();
            BigDecimal ingestedMIPremiumPST = response.getDeal().getMIPremiumPst();
            
            _log.debug("MIPremiumResponseProcessor.processResponse::MIPremium=" + ingestedMIPremium);
            _log.debug("MIPremiumResponseProcessor.processResponse::MIFeeAmt=" + ingestedMIFeeAmount);
            _log.debug("MIPremiumResponseProcessor.processResponse::PremiumPST=" + ingestedMIPremiumPST);
            
            if(ingestedMIPremium == null)
                ingestedMIPremium = new BigDecimal(0.0);
            if(ingestedMIFeeAmount == null)
                ingestedMIFeeAmount = new BigDecimal(0.0);
            if(ingestedMIPremiumPST == null)
                ingestedMIPremiumPST = new BigDecimal(0.0);
            
            deal.setMIPremiumAmount(XMLUtil.bigDecimal2Double(ingestedMIPremium, BigDecimal.ROUND_HALF_UP));
            deal.setMIFeeAmount(XMLUtil.bigDecimal2Double(ingestedMIFeeAmount, BigDecimal.ROUND_HALF_UP));
            deal.setMIPremiumPST(XMLUtil.bigDecimal2Double(ingestedMIPremiumPST, BigDecimal.ROUND_HALF_UP));
            /*
             * It's up to MI Provider to attach the message entry to the response with the "R"
             * status code. So, in case of message entry is present and it's size > 0, the processor
             * just loops through the list of messages, concatinate them and sets the result to the deal\object.
             * Else, if message entry is absent or it's size is 0, the processor sets the default error message 
             * to the deal object.
             * 
             */
            boolean isError = ERROR_STATUS_CODE.equals(response.getDeal().getStatusCode());
            if(isError) {
                List messages = response.getMessage();
                String messageText = (info.getLanguageId() == 0) ? 
                        Mc.MI_UG_PREMIUM_ERROR_EN : Mc.MI_UG_PREMIUM_ERROR_FR;
                
                if(messages != null && messages.size() > 0) {
                    for(int i = 0; i < messages.size(); i++) {
                        Message message = (Message)messages.get(i);
                        messageText += message.getMessageText();
                    }
                }
                if(messageText.length() > MAX_LENGHT_DEAL_MIPREMIUMERRORMESSAGE){
                    messageText = messageText.substring(0, MAX_LENGHT_DEAL_MIPREMIUMERRORMESSAGE);
                }
                deal.setMIPremiumErrorMessage(messageText);
            
            }else{
                // status success
                deal.setMIPremiumErrorMessage(null);
                //ensure returned value reaches all copies of the deal.
                DealPropagator dp = new DealPropagator(deal, deal.getCalcMonitor());
                dp.setMIPremiumAmount(deal.getMIPremiumAmount());
                dp.ejbStore();
            }
            
            deal.ejbStore();
            
            if(isInTx == false)
                srk.commitTransaction();

        }
        catch(Exception e) {
            _log.error("MIPremiumResponseProcessor: " + e.getMessage());
            _log.error(e);
            
            if(srk.isInTransaction())
                srk.rollbackTransaction();
            throw e;
        }
    }

    /**
     * The purpose of this method, is to do some things (like validation, etc.) before the respose actuly be processed.
     * @param obj must be of FXResponse type.
     * @param info - type of ServiceInfo.
     * @param validate - type of boolean
     * @throws Exception
     */
    //todo needs to be refactored: muved in high layer
    protected void handleResponse(PremiumResponse response, ServiceInfo info, boolean validate) throws Exception {
        try {

            if (validate) {
                Validator validator = jc.createValidator();

                if (validator.validate(response)) {
                    System.out.println("Premium Response Validation succeed!");
                }
            }
        }
        catch(Exception e) {
            StringUtil.stack2string(e);
            throw e;
        }
    }

    /**
     *
     * @param obj
     * @return PremiumResponse object.
     * @throws Exception
     */
    protected PremiumResponse getPremiumResponse (String response) throws Exception {
        PremiumResponse premiumResponse = null;
        premiumResponse = unmarshal(response);

        return premiumResponse;
    }


    protected PremiumResponse unmarshal(String response) throws Exception {
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        return (PremiumResponse) XMLUtil.string2jaxb(unmarshaller, response);
    }

    protected void processDatXError(ServiceInfo info) throws Exception
    {
        SessionResourceKit srk = info.getSrk();
        try
        {

            Integer dealId = info.getDealId();
            Integer copyId = info.getCopyId();
            DealPK dealPk = null;
            Deal deal = null;
            srk.getExpressState().setDealInstitutionId(info.getDeal().getInstitutionProfileId());
            dealPk = new DealPK(dealId.intValue(), copyId.intValue());
            deal = new Deal(srk, null).findByPrimaryKey(dealPk);

            String message = info.getDeal().getMIPremiumErrorMessage();

            boolean isInTx = srk.isInTransaction();
            if (isInTx == false)
                srk.beginTransaction();
            deal.setMIPremiumErrorMessage(message);
            deal.ejbStore();
            if (isInTx == false)
                srk.commitTransaction();
        } catch (Exception e)
        {
            e.printStackTrace();
            if (srk.isInTransaction())
                srk.rollbackTransaction();
            StringUtil.stack2string(e);
            throw e;
        }
    }

    // todo Populate the response instance before updating in the DB.
    /* taken off because Channel class taking care of it  Aug. 28, 2006 
    public void createResponse(ServiceInfo info) throws Exception{
        SessionResourceKit srk = info.getSrk();
        try {
            Response response = new Response(srk);
            Date responseDate = new Date();
            Date statusDate   = responseDate;
            int requestId = info.getRequestId();
            Integer dealId = info.getDealId();
            Integer copyId = info.getCopyId();
            DealPK dealPk = null;
            if(dealId != null && copyId != null)
                dealPk = new DealPK(dealId.intValue(), copyId.intValue());
            int responseSatatusId = 0;

            srk.beginTransaction();
                response.create(responseDate, requestId, dealPk, statusDate, responseSatatusId);
                // some additional fieldsds need to be set.
                response.ejbStore();
            srk.commitTransaction();
        }
        catch(Exception e) {
            if(srk.isInTransaction())
                srk.rollbackTransaction();
            StringUtil.stack2string(e);
            throw e;
        }
    }
     */
}