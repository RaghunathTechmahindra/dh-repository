package com.filogix.externallinks.services.channel;

import com.filogix.express.core.ExpressException;

public class ChannelRequestConnectivityException extends ExpressException {

    private static final long serialVersionUID = -5630107633517801931L;

    public static final int FAULT_CODE_BLANK = -1;

    private int faultCode = FAULT_CODE_BLANK;

    public ChannelRequestConnectivityException() {
        super();
    }

    public ChannelRequestConnectivityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChannelRequestConnectivityException(String message) {
        super(message);
    }

    public ChannelRequestConnectivityException(Throwable cause) {
        super(cause);
    }

    public void setFaultCode(int faultCode) {
        this.faultCode = faultCode;
    }

    public int getFaultCode() {
        return faultCode;
    }

}
