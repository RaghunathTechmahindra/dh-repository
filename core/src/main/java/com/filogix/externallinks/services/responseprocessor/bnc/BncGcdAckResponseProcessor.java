package com.filogix.externallinks.services.responseprocessor.bnc;

import MosSystem.Mc;

import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.WorkflowTrigger;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.basis100.workflow.WFETrigger;
import com.filogix.externallinks.adjudication.BncGcdServiceInfo;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.bnc.FXResponse;
import com.filogix.externallinks.services.responseprocessor.IResponseProcessor;

/**
 * This class is used to process the ack received from DatX following an outbound GCD request.
 * It assumes that a non-null ack represents a successful request submission (request status set to "Submitted"), 
 * while a null ack indicates that an error occurred (request status set to "Error Submit").
 * @author jcheun
 *
 */
public class BncGcdAckResponseProcessor implements IResponseProcessor
{
	private static SysLogger logger;
    private static BncGcdAckResponseProcessor instance = new BncGcdAckResponseProcessor();

    public BncGcdAckResponseProcessor()
    {
        super();

    }

    public void processResponse(Object obj, ServiceInfo info, boolean validate)
            throws Exception
    {
    	String logString = "BncGcdAckResponseProcessor.processResponse - ";
    	logger = info.getSrk().getSysLogger();
		logger.debug(logString + "Entering method.");
        BncGcdServiceInfo serviceInfo = (BncGcdServiceInfo) info;
        Request request = serviceInfo.getRequest();
        SessionResourceKit srk = info.getSrk();
        
        FXResponse ack = (FXResponse) obj;
        
        //Submission failed.
        //Either the ack is empty due to a SOAP fault being caught in the BncGcdChannel, 
        //or we have no transaction key.
        if (ack == null || ack.getServiceKey() == null || ack.getServiceKey().length() == 0) {
            request.setRequestStatusId(Mc.REQUEST_STATUS_ID_ERROR_SUBMIT);
            request.ejbStore();
            logger.info(logString + " FXResponse is null, set Request " + request.getRequestId() + " status to " + Mc.REQUEST_STATUS_ID_ERROR_SUBMIT + ".");
            return;
        }
        
        //Submission was successful.
        String transactionKey = ack.getServiceKey();

        String statusMessage = ack.getResultDescription();
        if (statusMessage.length() > 255) statusMessage = statusMessage.substring(0, 255);  //Database has a limit on the size of the status message, 255 bytes.

        request.setChannelTransactionKey(transactionKey);
        request.setStatusMessage(statusMessage);
        request.setRequestStatusId(Mc.REQUEST_STATUS_ID_SUBMITTED);
        request.ejbStore();
        
        //Fix for NBC202098: change type from 1 to 2
        //with 1, it always results duplicate tasks if open tasks exist.
        //WFETrigger.workflowTrigger(1, srk, info.getDeal().getUnderwriterUserId(),
        //		info.getDeal().getDealId(), info.getDeal().getCopyId(), -1, null);
        WFETrigger.workflowTrigger(2, srk, info.getDeal().getUnderwriterUserId(),
        		info.getDeal().getDealId(), info.getDeal().getCopyId(), -1, null);
        
        logger.debug(logString + " finished processing FXResponse.");
    }

    public static IResponseProcessor getInstance()
    {
        return instance;
    }
}
