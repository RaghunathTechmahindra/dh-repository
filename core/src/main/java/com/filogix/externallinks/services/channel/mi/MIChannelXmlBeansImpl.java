package com.filogix.externallinks.services.channel.mi;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dhltd.marketplace.mi.wsdl.axis2.schema.Document;
import com.dhltd.marketplace.mi.wsdl.axis2.schema.RequestDocument;
import com.dhltd.marketplace.mi.wsdl.axis2.schema.RequestDocument.Request;
import com.dhltd.marketplace.mi.wsdl.axis2.schema.ResponseDocument;
import com.dhltd.marketplace.mi.wsdl.axis2.schema.ResponseDocument.Response;
import com.dhltd.marketplace.mi.wsdl.axis2.xmlbeans.ServiceStub;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.services.ServiceInfo;
import com.filogix.externallinks.services.channel.Axis2BaseChannel;
import com.filogix.externallinks.services.channel.ChannelRequestConnectivityException;

public class MIChannelXmlBeansImpl extends Axis2BaseChannel {

    private final static Logger logger = LoggerFactory.getLogger(MIChannelXmlBeansImpl.class);

    public Object send(ServiceInfo serviceInfo, String payload) throws ExternalServicesException, ChannelRequestConnectivityException {

        ServiceStub stub;
        try {
            stub = new ServiceStub(getEndpoint());
        } catch (AxisFault e) {
            logger.error("wrong endpoint = " + getEndpoint(), e);
            throw new ExternalServicesException(e);
        }

        ServiceClient sc = stub._getServiceClient();

        // set SOAP header, timeout
        setupMetaData(sc, serviceInfo);

        RequestDocument requestDocument = RequestDocument.Factory.newInstance();
        Request request = requestDocument.addNewRequest();
        Document docForRequest = request.addNewDocument();

        XmlObject xmlPayload;
        try {
            xmlPayload = XmlObject.Factory.parse(payload);
        } catch (XmlException e) {
            logger.error("faild to parse payload xml", e);
            throw new ExternalServicesException("faild to parse payload xml", e);
        }

        XmlObject data = docForRequest.addNewData();
        data.set(xmlPayload);

        ResponseDocument responseDocument = null;
        try {
            logger.debug("before sending request for deal:" + serviceInfo.getDeal().getDealId());
            responseDocument = stub.submit(requestDocument);
            logger.debug("after sending request successfuly for deal:" + serviceInfo.getDeal().getDealId());

        } catch (RemoteException e) {
            handleRequestSendingException(e);
        }

        Response response = responseDocument.getResponse();
        Document docForResponse = response.getDocumentArray(0);
        XmlObject responseSchema = docForResponse.getData();

        return responseSchema.toString();

    }

}
