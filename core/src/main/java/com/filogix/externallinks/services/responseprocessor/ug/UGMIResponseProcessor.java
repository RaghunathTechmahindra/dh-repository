package com.filogix.externallinks.services.responseprocessor.ug;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.miprocess.MIProcessHandlerWrapper;
import com.basis100.deal.util.StringUtil;
import com.filogix.externallinks.mi.aigug.jaxb.response.MortgageInsuranceResponse;
import com.filogix.externallinks.services.responseprocessor.IMIResponseAdapter;
import com.filogix.externallinks.services.responseprocessor.IResponseProcessor;
import com.filogix.externallinks.services.responseprocessor.MIResponseProcessor;
import com.filogix.externallinks.services.util.XMLUtil;

public class UGMIResponseProcessor
    extends MIResponseProcessor
{

    private static final Log logger = LogFactory.getLog(UGMIResponseProcessor.class);

    static private IResponseProcessor instance = new UGMIResponseProcessor();
    private static final String schema = "com.filogix.externallinks.mi.aigug.jaxb.response";

    private static JAXBContext jc;
    static
    {
        try
        {
            jc = JAXBContext.newInstance(schema);
        } catch (Exception e)
        {
            logger.error(StringUtil.stack2string(e));
            logger.error("MIResponseProcessor: JAXBContext initialization failed.");
            logger.error(e);
        }
    }

    public UGMIResponseProcessor()
    {
    }

    public static IResponseProcessor getInstance()
    {
        return instance;
    }
    
    protected void setPropagateMIFieldServiceSpecific(IMIResponseAdapter mi,
        MIProcessHandlerWrapper handler, Deal deal, int miStatus)
    {
        MortgageInsuranceResponse miResponseAIGUG = (MortgageInsuranceResponse) mi
            .getAdapteeInstance();
        // String miRefNumAIGStr =
        // miResponse.getServiceSpecificReferenceNumber();

        String miRefNumAIGStr = miResponseAIGUG.getDeal().getAIGUGReferenceNumber();
        handler.setMIPolicyNumber(makeMIPolicyNumber(deal, miRefNumAIGStr));
        handler.setMIPolicyNumAIGUG(miRefNumAIGStr);
    }

    protected Map getFeeAndPremiumAmountMap(IMIResponseAdapter mi, Deal deal)
    {

        Map feeMap = new HashMap();
        MortgageInsuranceResponse miResponseAIGUG = (MortgageInsuranceResponse) mi
            .getAdapteeInstance();

        BigDecimal miPremium = miResponseAIGUG.getDeal().getMIPremium();
        if (existsPremium(miPremium))
        {
            feeMap.put(new Integer(Mc.FEE_TYPE_AIG_UG_PREMIUM), miPremium);
            // feeMap.put(new Integer(Mc.FEE_TYPE_AIG_UG_PREMIUM_PAYABLE),
            // miPremium);
        }
        BigDecimal miFeeAmount = miResponseAIGUG.getDeal().getMIFeeAmount();
        if (existsPremium(miFeeAmount))
        {
            feeMap.put(new Integer(Mc.FEE_TYPE_AIG_UG_FEE), miFeeAmount);
            // feeMap.put(new Integer(Mc.FEE_TYPE_AIG_UG_FEE_PAYABLE),
            // miFeeAmount);
        }
        BigDecimal miPremiumPst = miResponseAIGUG.getDeal().getMIPremiumPst();
        if (existsPremium(miPremiumPst))
        {
            feeMap.put(new Integer(Mc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM), miPremiumPst);
            // feeMap.put(new
            // Integer(Mc.FEE_TYPE_AIG_UG_PST_ON_PREMIUM_PAYABLE),miPremiumPst);
        }
        return feeMap;
    }

    protected IMIResponseAdapter getMIResponse(String response) throws Exception
    {
        Unmarshaller unmarshaller = jc.createUnmarshaller();

        return new UGMIResponseAdapter((MortgageInsuranceResponse) 
            XMLUtil.string2jaxb(unmarshaller, response));
    }

}
