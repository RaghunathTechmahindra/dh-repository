/**
 * <p>Title: AcknoledgementResponseProcessor.java</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Filogix Inc. (c) 2006</p>
 *
 * <p>Company: Filogix Inc.</p>
 *
 * @author Midori Aida
 * @version 1.0 (Initial Version � May 19, 2006)
 *
 */

package com.filogix.externallinks.services.datx;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.basis100.deal.entity.Request;
import com.basis100.deal.entity.Response;
import com.basis100.deal.entity.ServiceRequest;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.ServiceRequestPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcTransactionException;
import com.basis100.picklist.BXResources;
import com.basis100.workflow.WFETrigger;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.JaxbServiceResponse;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.framework.ServiceResponseBase;
import com.filogix.externallinks.framework.ServiceResponseProcessorBase;

/**
 * this class can be used for all Business Asynchoronous transaction
 * 
 * @author Maida
 */
abstract public class AcknowledgementResProcessor
    extends ServiceResponseProcessorBase implements ServiceConst
{
    protected JaxbServiceResponse _base;
    protected Object _acknowledgement = null;
    protected int _statusId;
    protected String _statusDesc;
    protected Date _statusDate;

  public int process(ServiceResponseBase response)
      throws ExternalServicesException
  {
    int result = -1;
    _base = (JaxbServiceResponse) response;
    _acknowledgement = response.getResponseBean();
    _statusDate = new Date();
    
    try
    {
      _srk.beginTransaction();
      try
      {
        _statusId = getStatusId();
        _statusDesc = getStatusDesc(_statusId, _langId);

        updateRequest();
        insertResponse();
        insertDealHistory();
        if(!isErrorResponse(_statusId)){
          // update serviceRequest table only when response is not error.
          updateServiceRequest();
        }
        
        _srk.commitTransaction();
        result = _responseId;
      }
      catch (Exception e)
      {
        _srk.cleanTransaction();
        _statusId = ServiceConst.REQUEST_STATUS_PROCESSING_ERROR;
        _statusDesc = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), "REQUESTSTATUS",
            ServiceConst.REQUEST_STATUS_PROCESSING_ERROR,
            _langId);

        _srk.beginTransaction();
        try
        {
          updateRequest();

          /***** add for Deal Status Update *****/
          try {
            WFETrigger.workflowTrigger(2, _srk,   _base.getRequest().getUserProfileId(),
                      _base.getRequest().getDealId(), _base.getRequest().getCopyId(), -1, null);
          } catch (Exception e1) {
            e1.printStackTrace();
          }
          /***** add for Deal Status Update *****/
          
          _srk.commitTransaction();

        }
        catch (RemoteException e1)
        {
          _logger.error(StringUtil.stack2string(e1));
          _srk.cleanTransaction();
        }
      }
    }
    catch (JdbcTransactionException e1)
    {
      _logger.error(StringUtil.stack2string(e1));
      throw new ExternalServicesException(
          "Error when processing DatX reponse for requestId = " + _requestId
              + " :" + e1.getMessage());
    }

    return result;
  }
  
  /**
   * check if the response is error or not
   * @param resultCode
   * @return result true - error response
   */
  protected boolean isErrorResponse(int resultCode)
  {
    boolean result = false;
    if (resultCode < 0
        || resultCode == ServiceConst.RESPONSE_STATUS_SYSTEM_ERROR
        || resultCode == ServiceConst.RESPONSE_STATUS_PROCESSING_ERROR)
    {
      result = true;
    }
    return result;
  }

  protected void updateRequest() throws RemoteException
  {
    // Update Request table
    Request request = _base.getRequest();
    try
    {
        Collection requests = request.findRequestsByRequestId(request.getRequestId());
        Iterator iReq = requests.iterator();
        while (iReq.hasNext())
        {
            Request r = (Request)iReq.next();
            r.setStatusMessage(_statusDesc);
            r.setRequestStatusId(_statusId);
            r.setStatusDate(_statusDate);
            if(getDatXKey(_acknowledgement)!=null 
            		&& !"0".equals(getDatXKey(_acknowledgement).trim()))
            	r.setChannelTransactionKey(getDatXKey(_acknowledgement));
            r.ejbStore();
        }
    }
    catch(NumberFormatException nf)
    {
    	//TODO: what should I do?
    }
    catch(FinderException fe)
    {
        // ignore
    }
  }

  protected void updateServiceRequest() throws RemoteException, FinderException
  {
    // Update ServiceRequest table
    Request requestEntity = _base.getRequest();
    Collection requests = requestEntity.findRequestsByRequestId(requestEntity.getRequestId());
    Iterator iReq = requests.iterator();
    while (iReq.hasNext())
    {
        Request r = (Request)iReq.next();

        try
        {
    ServiceRequest serviceRequest = new ServiceRequest(_srk);
    serviceRequest = serviceRequest.findByPrimaryKey(new ServiceRequestPK(
                r.getRequestId(), r.getCopyId()));

    serviceRequest.setServiceProviderRefNo(getExternalKey(_acknowledgement));
    serviceRequest.ejbStore();
  }
        catch(FinderException fe)
        {
            // ignore
        }
    }
  }

  protected void insertResponse() throws RemoteException, FinderException,
      CreateException
  {
    // Insert Response table
    Response responseEntity = new Response(_srk);
    responseEntity.create(responseEntity.createPrimaryKey(),
        _statusDate, _requestId, _base.getRequest().getDealId(), 
        _statusDate, _statusId);

    responseEntity.setStatusMessage(_statusDesc);
    responseEntity.setChannelTransactionKey(getDatXKey(_acknowledgement));

    responseEntity.ejbStore();

    _responseId = responseEntity.getResponseId();

  }

  /*
   * defult method insertDealHistory
   * do nothing
   * if need to insertDealHistory, then subclass must override this method
   */
  protected void insertDealHistory() throws RemoteException, FinderException, CreateException
  {
      // do nothing
  }
  protected int getStatusId()
  {
    int fxpStatusCode = ServiceConst.RESPONSE_STATUS_SYSTEM_ERROR;

    if (getResultCode(_acknowledgement) != null)
    {
      int datXResultCode = getResultCode(_acknowledgement).intValue();
      DatxResponseCodeConverterBase converter = getResponseCodeConverter();
      fxpStatusCode = converter.converetToFxpStatusCode(datXResultCode);
    }
    else
    {
      fxpStatusCode = super.getStatusId(getResultDescription(_acknowledgement),getResultCode(_acknowledgement).toString());
    }
    return fxpStatusCode;
  }
  
  protected String getStatusDesc(int statusId, int langId)
  {
	  String statusDesc = "";
	  if (statusId == RESPONSE_STATUS_SYSTEM_ERROR)
	  {
	      statusDesc = BXResources.getSysMsg(EXTERNAL_LINK_SYSTEM_ERROR, langId);
	  }
	  else
	  {
		  statusDesc = getResultDescription(_acknowledgement);
	      if (statusDesc == null || statusDesc.trim().equals(""))
	      {
	          try
	          {
	              if (getMessageTable() != null)
	              {
	            	  statusDesc = BXResources.getPickListDescription(_srk.getExpressState().getDealInstitutionId(), getMessageTable(), statusId, langId );
	              }
	              else
	            	  statusDesc = getResultDescription(_acknowledgement);
	          }
	          catch(Exception e)
	          {
	              _logger.error(this.getClass(), "No Status Message found");
	          }
	      }
	  }
	  
	  return statusDesc;
  }

  
  private String getMessageTable()
  {
      return "REQUESTSTATUS";
  }
  
  abstract protected DatxResponseCodeConverterBase getResponseCodeConverter();
  abstract protected String getResultDescription(Object acknowledgement);
  abstract protected Integer getResultCode(Object acknowledgement);
  abstract protected String getDatXKey(Object acknowledgement);
  abstract protected String getExternalKey(Object acknowledgement);
}
