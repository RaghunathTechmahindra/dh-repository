package com.filogix.externallinks.services.channel.bnc;

import MosSystem.Mc;

import com.basis100.deal.entity.Channel;
import com.basis100.deal.entity.PayloadType;
import com.basis100.deal.entity.Response;
import com.basis100.deal.pk.ChannelPK;
import com.basis100.deal.pk.PayloadTypePK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.adjudication.BncGcdServiceInfo;

public class BncGcdChannelHelper
{
    private SessionResourceKit sessionResource;
    private JdbcExecutor jExec;
    private SysLogger logger;
    private BncGcdServiceInfo serviceInfo;
    
    public BncGcdChannelHelper( BncGcdServiceInfo serviceInfo)
    {
        this.serviceInfo = serviceInfo;
        sessionResource = serviceInfo.getSrk();
        jExec = sessionResource.getJdbcExecutor();
        logger = sessionResource.getSysLogger();
    }

    public Channel getChannel(int i) throws RemoteException, FinderException
    {
    	String logString = "BncGcdChannelHelper.getChannel() - ";
		logger.debug(logString + "Entering method.");
        Channel channel = new Channel(sessionResource);
        ChannelPK channelPK = new ChannelPK(i);
        
        channel = channel.findByPrimaryKey(channelPK);
        return channel;
    }

    public int getTimeout(int channelID) throws FinderException
    {
    	String logString = "BncGcdChannelHelper.getTimeout() - ";
		logger.debug(logString + "Entering method.");
        String sql = "SELECT DATXTIMEOUT FROM TIMEOUTDEFINITIONASSOC WHERE CHANNELID  = " + String.valueOf(channelID);

        int timeout=-1;
        
            boolean gotRecord = false;

            try {
              int key = jExec.execute(sql);

              for (; jExec.next(key);) {
                gotRecord = true;
                timeout = jExec.getInt(key, "DATXTIMEOUT");
                break; // can only be one record
              }

              jExec.closeData(key);

              if (gotRecord == false) 
              {
                  String msg = "Timeout Entity: @findByQuery(), key= " + String.valueOf(channelID) + ", entity not found";
                  logger.error(logString + msg);
                  throw new FinderException(msg);
              }
            } catch (Exception e) 
            {
              FinderException fe = new FinderException("Channel Entity - findByQuery() exception");

              logger.error(logString + fe.getMessage());
              logger.error(logString + "finder sql: " + sql);
              logger.error(logString + e);

              throw fe;
            }
        return timeout;
    }

    public PayloadType getPayloadType(int payloadTypeId) throws RemoteException, FinderException
    {
    	String logString = "BncGcdChannelHelper.getPayloadType() - ";
		logger.debug(logString + "Entering method.");
        PayloadType channel = new PayloadType(sessionResource);
        PayloadTypePK payloadTypePK = new PayloadTypePK(payloadTypeId);
        
        channel = channel.findByPrimaryKey(payloadTypePK);
        return channel;
    }

    public Response getResponse(int requestId) throws RemoteException
    {
    	String logString = "BncGcdChannelHelper.getResponse() - ";
		logger.debug(logString + "Entering method.");
        Response response = new Response(sessionResource);
        
        try
        {
            response = response.findLastResponseByRequestId(requestId);
            
        } catch (FinderException e)
        {
            logger.debug(logString + e.getMessage());
            return null;
        }
        return response;
    }

    public FXRequest getFXRequest() throws Exception
    {
    	String logString = "BncGcdChannelHelper.getFXRequest() - ";
		logger.debug(logString + "Entering method.");
        FXRequest fxRequest = new FXRequest();
        
        AuxDataHelper auxDataHelper = new AuxDataHelper(sessionResource, serviceInfo.getDeal());
        String auxData = auxDataHelper.getAuxData();
        fxRequest.setAuxData(auxData);
 
        int timeout = serviceInfo.getRequest().findDatxTimeout();
        fxRequest.setTimeout(new Integer(timeout));
        
        PayloadType payloadType = this.getPayloadType(Mc.BNC_GCD_REQUEST_PAYLOAD_TYPE);
        fxRequest.setSchemaName(payloadType.getXmlSchemaName());
        
        int requestId = serviceInfo.getRequestId();
        fxRequest.setClientKey(String.valueOf(requestId));
        
        fxRequest.setEncoding(Mc.GCD_DEFAULT_FXREQUEST_ENCODING);
                
        return fxRequest;
    }

  }
