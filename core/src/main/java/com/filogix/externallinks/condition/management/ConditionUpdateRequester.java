package com.filogix.externallinks.condition.management;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import MosSystem.Mc;

import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.ConditionStatusUpdateAssoc;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.ESBOutboundQueue;
import com.basis100.deal.pk.ConditionPK;
import com.basis100.deal.pk.ConditionStatusUpdateAssocPK;
import com.basis100.deal.pk.DealPK;
import com.basis100.entity.RemoteException;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.util.Xc;

public class ConditionUpdateRequester {
    
    protected SessionResourceKit _srk;    
    
    private String conditionTypeList;
    private final static Logger _logger = LoggerFactory.getLogger(ConditionUpdateRequester.class);
    
    public ConditionUpdateRequester(SessionResourceKit srk) {
        _srk = srk;
        getConTypeSysProperty();
    }
    
    /**
     * A method to check if the deal updates the condition status and insert
     * an entry into status update queue 
     * @param dealId the id of the deal that leads to condition update
     * @return the id of the status update queue entry
     */
    public int insertConditionUpdateRequest(int dealId, boolean docTracStatusCheck) {
        //Step 1: check if institution supports condition status update
        String conTypeSysPro = PropertiesCache.getInstance().getProperty(
                _srk.getExpressState().getDealInstitutionId(), Xc.STATUSUPDATE_CONDITIONS_SUPPORTED);
        if(conTypeSysPro == null) {
            _logger.info("Institution [" + _srk.getExpressState().getDealInstitutionId() 
            		+ "] Property[" + Xc.STATUSUPDATE_CONDITIONS_SUPPORTED + "] not found");
            return -1;
        }
        else if(conTypeSysPro.equalsIgnoreCase("N")) {
            _logger.info("Institution [" + _srk.getExpressState().getDealInstitutionId() 
            		+ "] doesn't support Condition Status Update functionality.");
            return -1;
        }
        else if(conditionTypeList == null) {
            _logger.info("List of condition type IDs is not found in system property.");            
            return -1;
        } 
        
        //Step 2: retrieve recommended scenario copy of the deal by DealId
        Deal deal = getDeal(dealId);
        if(deal == null) {
            _logger.info("No recommended scenario copy of the deal found. DealId[" + dealId + "]");
            return -1;
        }
        
        //Step 2.5: check assoc if the system type id included in
        //FXP25721 - July 7
        try {
            ConditionStatusUpdateAssoc csua = new ConditionStatusUpdateAssoc(_srk);
            csua.setSilentMode(true);
            Collection<ConditionStatusUpdateAssoc> c = csua.findBySystemType(deal.getSystemTypeId());
            if (c==null || c.size()==0) {
                return -1;
            }
        } catch (Exception rd) {
            _logger.info("Couldn't get ConditionStatusUpdateAssoc");
            return -1;
        }
        
        //Step 3: retrieve list of document tracking by the retrieved dealid + copyid
//        Collection<DocumentTracking> docTrackList = getDocTrackList(deal);
//        if(docTrackList == null || docTrackList.isEmpty())  {
//            _logger.info("No document tracking found. DealId[" + dealId + "] CopyId["+deal.getCopyId()+"]");
//            return -1;
//        }
//        
//        //Step 4: retrieve the condition associated with the document tracking and check their condition type
//        boolean conditionFound = false;
//        
//        Iterator<DocumentTracking> iterator = docTrackList.iterator();
//        while(iterator.hasNext()) {
//            DocumentTracking docTrac = (DocumentTracking)iterator.next();
//            int conditionTypeId = getConditionTypeId(docTrac.getConditionId());
//            
//            conditionFound = isConditionTypeIncluded(conditionTypeId);
//                
//            if(conditionFound) {
//                //for condition review screen to check the document tracking status
//                if(!docTracStatusCheck) {
//                    break;
//                }
//                else if(checkDocTracStatus(deal, docTrac))
//                {
//                    break;
//                }
//                conditionFound = false;
//            }        
//        }
        // changed for performance enhancement
        // combined step 3 and 4, i.e. check conditionTypId in sql
        boolean conditionFound = false;
        try {
            DocumentTracking docTrac = new DocumentTracking(_srk);
            conditionFound = docTrac.needStatusUpdate(new DealPK(deal.getDealId(), deal.getCopyId()), 
            	conditionTypeList, docTracStatusCheck);
        } catch (Exception e) {
                _logger.error("ConditionUpdateRequester insertConditionUpdateRequest(): error occurs while retrieving document tracking." );
                _logger.error("DealId: " + deal.getDealId() + " CopyId: " + deal.getCopyId());
                _logger.error("Exception: " + e.getMessage());
                return -1;
        }

        if(!conditionFound) {
            _logger.info("No condition with supported condition type generated. No record insert into StatusEventQueue");
            return -1;
        }
        
        // For Manual Deal originated from Deal Entry screen will have channel Media as 'M' 
    	// Block updating the Deal event status and Condition update status for deals created for express 
    	// FXP32829 - Dec 06 2011
      	if(deal.getChannelMedia() != null && deal.getChannelMedia().equalsIgnoreCase(Xc.CHANNEL_MEDIA_M))
    		return -1;
    
    
        //Step 5: if condition is found, insert an entry into StatusEventQueue
        try {
            ESBOutboundQueue queue = new ESBOutboundQueue(_srk);        
            
            queue = queue.create(queue.createPrimaryKey(), ServiceConst.SERVICE_TYPE_CONDITION_STATUS_UPDATE, dealId, deal.getSystemTypeId(), getUserProfileId(deal));
            
            return queue.getESBOutboundQueueId();
        } catch(Exception e) {
            _logger.error("ConditionUpdateRequesterError insertConditionUpdateRequest(): occurs while inserting status event. ");            
            _logger.error("Exception: " + e.getMessage());
            return -1;
        }        
        
    }
    
    //retrieve the recommended scennario deal by the dealid
    private Deal getDeal(int dealId) {
        try {
            Deal deal = new Deal(_srk, null);
            return deal.findByRecommendedScenario(new DealPK(dealId,0), true);
        }
        catch (Exception e) {
            _logger.error("ConditionUpdateRequester getDeal(): error occurs while retrieving recommended scenario for this deal id.");
            _logger.error("DealId: " + dealId);
            _logger.error("Exception: " + e.getMessage());
            return null;
        }
    }
    
    //retrieve the list of document tracking by the scenario recommanded deal
    private Collection<DocumentTracking> getDocTrackList(Deal deal) {
        try {
            DocumentTracking docTrac = new DocumentTracking(_srk);
            return docTrac.findByDeal(new DealPK(deal.getDealId(), deal.getCopyId()));
        }
        catch (Exception e) {
            _logger.error("ConditionUpdateRequester getDocTrackList(): error occurs while retrieving document tracking." );
            _logger.error("DealId: " + deal.getDealId() + " CopyId: " + deal.getCopyId());
            _logger.error("Exception: " + e.getMessage());
            return null;
        }
    }

    
    
    //retrieve the condition type id of the condition
    private int getConditionTypeId(int conditionId) {
        try {
            Condition condition = new Condition(_srk);
            condition = condition.findByPrimaryKey(new ConditionPK(conditionId));
            return condition.getConditionTypeId();
        } catch (Exception e) {
            _logger.error("ConditionUpdateRequester getConditionTypeId(): error occurs while retreiving condition. ");
            _logger.error("ConditionId: " + conditionId);
            _logger.error("Exception: " + e.getMessage());
        }    
        return -1;
    }
    
    //retrieve the list of supported condition types from sys property
    private void getConTypeSysProperty() {
	conditionTypeList  = PropertiesCache.getInstance().getProperty(
                _srk.getExpressState().getDealInstitutionId(), Xc.STATUSUPDATE_CONDITIONS_INCLUDE_CONDITION_TYPES);
        
    }
    
    // NOT Use it
    //check if the condition's condition type is included in the list of support condition types in sysproperty
//    private boolean isConditionTypeIncluded(int conditionTypeId) {
//        for(int i = 0; i < conditionTypeList.length(); i++) {
//            if(conditionTypeId == Integer.parseInt(conditionTypeList[i].trim()))
//            {
//                return true;
//            }
//        }
//        return false;
//    }
    
    //check the com.basis100.docprep.bco.current.user flag
    //Y = retrieve current user profile id
    //N = retrieve the administrator profile id of the deal
    private int getUserProfileId(Deal deal) {
        if (PropertiesCache.getInstance().getProperty(
                _srk.getExpressState().getDealInstitutionId(), Xc.COM_BASIS100_DOCPREP_BCO_CURRENT_USER, "Y").equalsIgnoreCase("Y")) {
            return _srk.getExpressState().getUserProfileId();
        }
        else {
            return deal.getAdministratorId();
        }
    }
    
    //check if the status if the docTrack is approve or not
    //if it is check the timestamp
    //if it is not then return true
    private boolean checkDocTracStatus(Deal deal, DocumentTracking docTrack) {
        // if this docTrack status is not approved
        if(docTrack.getDocumentStatusId() != Mc.DOC_STATUS_APPROVED) {
            return true;
        }
        else {
            //check if we have at least one approved condition status date is equal to current date
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String dStatusDate = df.format(docTrack.getDStatusChangeDate());
            String dealStatusDate = df.format(deal.getStatusDate());
            String currentDate = df.format(new Date());
            
            if(currentDate.equals(dStatusDate) && currentDate.equals(dealStatusDate))
            {
                return true;
            }
            return false;
        }
    }
}
