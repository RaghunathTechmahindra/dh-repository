package com.filogix.externallinks.adjudication;

import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.Request;
import com.filogix.externallinks.services.ServiceInfo;

/**
 * This class represents a ServiceInfo for Get Credit Decision.
 * While the parent ServiceInfo class has a request ID and deal ID, this
 * class contains actual Request and Deal objects.  It also holds a
 * language ID which is carried inward from the presentation layer.
 * @author jcheun
 *
 */
public class BncGcdServiceInfo extends ServiceInfo {
	private Request request;
	private int languageId;
	
	/**
	 * Retrieve the Request object.
	 */
	public Request getRequest() {
		return this.request;
	}
	
	/**
	 * Set a Request object.
	 * @param request
	 */
	public void setRequest(Request request) {
		this.request = request;
	}
	
	/**
	 * Retrieve the language ID
	 */
	public int getLanguageId() {
		return this.languageId;
	}
	
	/**
	 * Set a language ID.
	 * @param languageId
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
}
