package com.filogix.externallinks.adjudication.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
 
/**
 * This class is intended to be used by JAXB-generated classes.  It will ensure that any "dateTime"
 * element will be marshalled into a String of the following format:
 * 
 * yyyy-MM-ddTHH:mm:ss
 * 
 * This class is specified in the BNCGCDRequestBinding.xjb file.
 * 
 * @author jcheun
 *
 */
public class DateConversionUtility 
{
 
	static final SimpleDateFormat onlyDate = new SimpleDateFormat("yyyy-MM-dd");
	static final SimpleDateFormat dateAndTime=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	
	public static String printCalendar(Calendar c) 
	{
		return onlyDate.format(c.getTime());
	}
 
	public static Calendar parseCalendar(String c) throws ParseException 
	{
		Date d = onlyDate.parse(c);
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		
		return cal;
	}
	
	
	public static String printDateTime(Calendar d) throws ParseException
	{
		
		String s=dateAndTime.format(d.getTime());
 
		return s;
	}
	
	public static Calendar parseDateTime(String dt) throws ParseException
	{
		
		dateAndTime.setLenient(false);
		Date date = (Date)dateAndTime.parse(dt);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		return Calendar.getInstance();
	}
}


