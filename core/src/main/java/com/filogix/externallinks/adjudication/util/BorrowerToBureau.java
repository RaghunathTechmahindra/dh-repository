package com.filogix.externallinks.adjudication.util;

public class BorrowerToBureau {
	private int borrowerId;
	private int bureauId;
	
	public BorrowerToBureau() {
		
	}
	
	public BorrowerToBureau(int borrowerId, int bureauId) {
		this.borrowerId = borrowerId;
		this.bureauId = bureauId;
	}

	public int getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(int borrowerId) {
		this.borrowerId = borrowerId;
	}

	public int getBureauId() {
		return bureauId;
	}

	public void setBureauId(int bureauId) {
		this.bureauId = bureauId;
	}
}
