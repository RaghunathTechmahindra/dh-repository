package com.basis100.support;

/**
 * Title:        Your Product Name
 * Description:  Your description
 * Copyright:    Copyright (c) 1999
 * Company:
 * @author
 * @version
 */

public class TitleContentPair
{
  private String title;
  private String content;

  public TitleContentPair(String pTitle, String pContent)
  {
    this.title = pTitle;
    this.content = pContent;
    if(title == null){
      title = "";
    }
    if(content == null){
      content = "";
    }
  }

  public String createMessage(){
    if( title.trim().equals("") ){
      return content;
    }
    return title + " : " + content;
  }
}