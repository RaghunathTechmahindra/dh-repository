package com.basis100.support;

import com.basis100.resources.*;
import com.basis100.log.SysLog;
import com.basis100.mail.*;
import com.basis100.deal.docprep.deliver.*;
/**
 * Title:        Your Product Name
 * Description:  Your description
 * Copyright:    Copyright (c) 1999
 * Company:
 * @author
 * @version
 */

public class SupportNotifier
{
  private static SupportNotifier INSTANCE = null;
  private static final int NOTIFICATION_TYPE_E_MAIL = 1;
  private static final int NOTIFICATION_TYPE_JMS = 2;
  private static final int NOTIFICATION_TYPE_FILE = 3;

  private static String eMailSubjectLine;
  private int notificationType;
  private String eMailAddress;
  private String environment;

  private SupportNotifier()
  {
    eMailSubjectLine = "Basis100 automated support notification system: ";
    try{
      environment = PropertiesCache.getInstance().getProperty(-1, "com.basis100.support.environment", "Unknown environment - nothing found in mossysproperties file.");
      String lSupportType = PropertiesCache.getInstance().getProperty(-1, "com.basis100.support.type", "email");
      eMailSubjectLine += environment;
      if( lSupportType.equalsIgnoreCase("email") ){
        notificationType = NOTIFICATION_TYPE_E_MAIL;
        eMailAddress = PropertiesCache.getInstance().getProperty(-1, "com.basis100.support.email.address", "dcoleman@basis100.com");
      }else{
        System.out.println(lSupportType);
      }
    }catch(Exception exc){
       notificationType = NOTIFICATION_TYPE_E_MAIL;
    }
  }

  public static synchronized SupportNotifier getInstance(){
    if(INSTANCE == null){
      INSTANCE = new SupportNotifier();
    }
    return INSTANCE;
  }

  public void notifySupport(NotifierMessageHolder holder){
    try{
      SimpleMailMessage message = new SimpleMailMessage();
      message.setSubject(eMailSubjectLine);
      message.appendText(holder.composeMessage());
      message.clearTo();
      message.appendTo(eMailAddress);
      message.setFrom("notification@filogix.com");
      message.send(null);
    }catch(DeliveryException dExc){
      System.out.println("This is delivery exception");
      SysLog.error(dExc);
    }catch(Exception exc){
      System.out.println("Here is exception in e-mailing support");
      exc.printStackTrace();
      SysLog.error(exc);
    }
  }

}


