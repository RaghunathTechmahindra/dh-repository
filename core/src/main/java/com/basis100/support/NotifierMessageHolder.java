package com.basis100.support;

import java.util.*;
/**
 * Title:        Your Product Name
 * Description:  Your description
 * Copyright:    Copyright (c) 1999
 * Company:
 * @author
 * @version
 */

public class NotifierMessageHolder
{
  Vector titleContVect;

  public NotifierMessageHolder()
  {
    titleContVect = new java.util.Vector();
  }

  public void addTitleContentPair(TitleContentPair tcPair){
    titleContVect.add(tcPair);
  }

  public void addTitleContentPair(String pTitle, String pMsg){
    TitleContentPair tcp = new TitleContentPair(pTitle,pMsg);
    addTitleContentPair(tcp);
  }

  public String composeMessage(){
    StringBuffer rv = new StringBuffer("");
    TitleContentPair pair;
    Iterator it = titleContVect.iterator();
    while(it.hasNext()){
      pair = (TitleContentPair) it.next();
      rv.append( pair.createMessage() );
    }
    return rv.toString();
  }

}