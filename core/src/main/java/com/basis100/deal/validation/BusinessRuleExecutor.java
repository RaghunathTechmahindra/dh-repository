package com.basis100.deal.validation;

import java.util.Vector;
import com.basis100.BREngine.BRUtil;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.StringUtil;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.*;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.basis100.deal.util.BXStringTokenizer;

import com.filogix.express.legacy.mosapp.ISessionStateModel;
import com.filogix.util.Xc;

import mosApp.MosSystem.PageEntry;
import MosSystem.*;

/*
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
*/

/**
 * <p>Title: BusinessRuleExecutor</p>
 * <p>Description:
 *
  * Contains services for executing MOS business rules.
  *
  * Summary:
  *
  * . BREValidator()
  *
  *   Methods for executing business rules by rule prefix. Rules are housed in the Business Rule
  *   Engine.
  *
  * . setCurrentBorrower()
  * . setCurrentProperty()
  *
  *   Support invocations with local scoping of rules to be evaluated. Some rule type (rule
  *   prefixes) run with local scoping - e.g. rules to be evaluated for a specific borrower,
  *   or for a specific property. Usage - set the desired properties before invoking a high level
  *   validation function (e.g. BREValidator()), e.g.
  *
  *        BusinessRuleExecutor brExec = new BusinessRuleExecutor(...);
  *        brExec.setCurrentBorrower();
  *
  *        o = brExec.BREValidator(...);
  *        
  * RE-VERSIONING Starts with 2.0
  * @version 1.0 original Version at beginging of MCM
  * @version 1.1 July 9, 2008: added DEC (XS 2.39)- MCM Impl team
  * @version 1.2 July 14, 2008: added DCC (XS 2.39) - MCM Impl team
  * @version 1.3 July 16, 2008: deleted DCC (XS 2.39)- MCM impl team
  * @version 1.4 Aug 5, 2008: added RequestId for non page br exec - MCM impl team
  **/

/*
validateDate()
{
	PassiveMessage pm = new PassiveMessage();

	// custom validations as required

	// validate closing date
	getAndValidateDate(pm, "Invalid Closing Date", Mc.CRITICAL, list of date display fields - three of then);




	valObj.BREValidator(theSession, pe, srk, pm, "DE");

	if (pm.isMessages() == true)
	{
		pe.setPassiveMessage(pm);
		return;
	}
}
*/
public class BusinessRuleExecutor
{
  private int currentBorrower = -1;
  private int currentProperty = -1;
  private int currentRequest = -1;
  private int currentComponent = -1;

  // For optimization purpose : we found that the initialization of the BREngine takes about 1 second.
  //      In order to avoid that we will reuse it to improve the performance.
  private BusinessRule br = null;

  //// temp
  public SysLogger logger;

  //****** WARNING ************
  // this method is not used currently, (setting on mossys.properties)
  // and not implmended for ML
  // Sep 25, 2007 - Midori
  // ********  SHOULDN'T USE ************************
  // Method to setup the cach of the most frequency used BusinessRules
  public void initCach(SessionResourceKit srk) throws Exception
  {
    BusinessRule.setCachMode((PropertiesCache.getInstance().getProperty(-1,
			Xc.COM_BASIS100_STARTUP_ENABLE_BUSINESS_RULE_CACH, "N")).equals("Y"));
    br = new BusinessRule(srk, -1, -1, "na", "");
    Vector v = new Vector();
    v.add("DI-%");
    v.add("DE-%");
    v.add("IC-%");
    v.add("UN-%");
    v.add("MI-%");
    v.add("DEA-%");
    v.add("DEP-%");

    // MCM Team changes STARTs
    v.add("DEC-%");
    // MCM Team changes ENDs
    
    //--DJ_LDI_CR--start--//
    v.add("LDI-%");
    //--DJ_LDI_CR--end--//
    //--DJ_#548_Ticket--start--//
    // Appraisal only set of BR.
    v.add("AR-%");
    //--DJ_#548_Ticket--end--//
    //--AVM/Valuation--start--//
    v.add("ARR-%");
    v.add("ARC-%");
    v.add("AVR-%");
    //--AVM/Valuation--end--//
    
    //--Closing--start--//
    v.add("FPC-%");
    //--Closing--end--//
    
    //--#1260 ticket--start--//
    v.add("GCD-%");
    //--#1260 ticket--end--//
    
    int lim = v.size();
    String tmpRuleName = "";
    for (int i = 0; i < lim; ++i)
    {
        tmpRuleName = (String)v.elementAt(i);
        ////temp to test
        logger = SysLog.getSysLogger("BRE");
        logger.debug("BRE@tmpRuleName: " + tmpRuleName);

        br.reQuery(-1, -1, "na", tmpRuleName, srk.getExpressState().getDealInstitutionId());
    }
  }


  public void setCurrentBorrower(int id)
  {
    currentBorrower = id;
  }

  public void setCurrentProperty(int id)
  {
    currentProperty = id;
  }

  public void setCurrentRequest(int id)
  {
      currentRequest = id;
  }

  /**
   * <p>setCurrentComponent</p>
   * @param id
   * 
   * @auther MCM Team
   * @version 1.1  July 15, 2008  XS_2.39
   */
    public void setCurrentComponent(int id) {
      currentComponent = id;
    }
  //
  // Version for evaluating a single rule category/prefix
  //
  ////public PassiveMessage BREValidator(SessionState theSession, PageEntry pe, SessionResourceKit srk, PassiveMessage pm, String rulePrefix) throws Exception
  public PassiveMessage BREValidator(ISessionStateModel theSession, PageEntry pe, SessionResourceKit srk, PassiveMessage pm, String rulePrefix) throws Exception
  {
    return BREValidator(theSession, pe, srk, pm, rulePrefix, false);
  }
  ////public PassiveMessage BREValidator(SessionState theSession, PageEntry pe, SessionResourceKit srk, PassiveMessage pm, String rulePrefix, boolean optimal) throws Exception
  public PassiveMessage BREValidator(ISessionStateModel theSession, PageEntry pe, SessionResourceKit srk, PassiveMessage pm, String rulePrefix, boolean optimal) throws Exception
  {
      Vector v = new Vector();
      v.add(rulePrefix);

      return BREValidator(theSession, pe, srk, pm, v, optimal);
  }

  //
  // Version for evaluating multiple rule categories/prefixs
  //
  //    Notes: All rules are run within the scope of a single business rule instance (intrepretor).
  //           This means that global scope variables created by one prefix invocation are available
  //           in subsequent invocations.
  //
   ////public PassiveMessage BREValidator(SessionState theSession, PageEntry pe, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs) throws Exception
   public PassiveMessage BREValidator(ISessionStateModel theSession, PageEntry pe, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs) throws Exception
  {
    return BREValidator(theSession, pe, srk, pm, rulePrefixs, false);
  }
   
   /**
    * 
    * @param theSession
    * @param pe
    * @param srk
    * @param pm
    * @param rulePrefixs
    * @param optimal
    * @return
    * @throws Exception
    * 
    * @version 1.4 added Global variable currentRequest for XS 10.2 (Aug 5, 2008)
    */
  ////public PassiveMessage BREValidator(SessionState theSession, PageEntry pe, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs, boolean optimal) throws Exception
  public PassiveMessage BREValidator(ISessionStateModel theSession, PageEntry pe, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs, boolean optimal) throws Exception
  {
    boolean pmCreated = false;
    String rulePrefix = null;

    srk.setCloseJdbcConnectionOnRelease(true);

    boolean diagnosticToClient = srk.getDiagnosticViaClient();

    SysLogger logger = srk.getSysLogger();

    // if no passive message object passed create one
    if (pm == null)
    {
      pmCreated = true;
      pm = new PassiveMessage();
    }


    // determine if ctitical messages are to be morphed as non-critical
    boolean disallowCritical = false;
    String YN = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.bre.morph.critical.to.noncritical");
    if (YN != null && YN.equals("Y"))
        disallowCritical = true;

    boolean exceptionEncountered = pm.getExceptionEncountered();

    srk.getJdbcExecutor().closeExecutorTemporary();

    if((optimal == false) || ((optimal == true) && (br == null)))
    {

      // added for ML
      // set VPD from page Entry
      srk.getExpressState().setDealInstitutionId(pe.getDealInstitutionId());
      br = new BusinessRule(srk, -1, -1, "na", "");

      br.setDealId(pe.getPageDealId(), pe.getPageDealCID());
      // #2242: Catherine, 23-Nov-05 --------- start ---------  
      br.setLenderName();
      BRUtil.debug("BusinessRuleExecutor: set LenderName done");
      // #2242: Catherine, 23-Nov-05 --------- end ---------  
      //MCM team XS 10.2 Starts
      br.setRequestId(this.currentRequest);
      //MCM team XS 10.2 ends
      //MCM team XS 2.39 Starts
      br.setComponentId(this.currentComponent);
      //MCM team XS 2.39 Ends
    }

    // set the standard BRE (instance) variables ...
    br.setUserTypeId(theSession.getSessionUserType());

    // set global scope variables
    br.setGlobal("UserProfileId",     new Integer(theSession.getSessionUserId()));
    br.setGlobal("CurrentBorrowerId", new Integer(currentBorrower));
    br.setGlobal("CurrentPropertyId", new Integer(currentProperty));
    br.setGlobal("CurrentRequestId", new Integer(currentRequest));
    //MCM team XS 2.39 Starts
    br.setGlobal("CurrentComponentId", new Integer(currentComponent));
    //MCM team XS 2.39 Ends
    br.setGlobal("InstitutionProfileId", new Integer(pe.getDealInstitutionId()));
    

    // evaluate remaining prefixes

    int lim = rulePrefixs.size();

    //--Release2.1--//
    //Get LanguageId from SessionState -- By Billy 14Nov2002
    int languageId = theSession.getLanguageId();
    //=========================================

    for (int i = 0; i < lim; ++i)
    {
        rulePrefix = (String)rulePrefixs.elementAt(i);

        br.reQuery(-1, -1, "na", rulePrefix, srk.getExpressState().getDealInstitutionId());

        //--Release2.1--//
        //Pass LanguageId to get a Rule Messages in specified Language -- By Billy 14Nov2002
        exceptionEncountered = evaluateRulePrefix(pm, br, pe, rulePrefix, logger, exceptionEncountered,
          diagnosticToClient, disallowCritical, languageId);
        //===================================================================================
    }

    // close the rule object
    if(optimal == false)
      br.close();

    // return null if no messages - but only if message object was created here (i.e. always
    // return message object provided even when no messages)

    if (pmCreated == true && pm.getNumMessages() <= 0)
      return null;

    pm.setExceptionEncountered(exceptionEncountered);

    return pm;
  }

  //--Release2.1--//
  //Added Language ID input to indicate language of messages to return.
  //Assumptions : Result Parameter #3 Message in English
  //              Result Parameter #4 Message in French
  //--> By Billy 14Nov2002
  private boolean evaluateRulePrefix(PassiveMessage pm, BusinessRule br, PageEntry pe, String rulePrefix,
      SysLogger logger, boolean breException, boolean diagnosticToClient, boolean disallowCritical, int languageId)
  {
      // used for morphing critical messages to non-critical (e.g. used in special circumstances only)
      int criticalValue = (disallowCritical == true) ?  PassiveMessage.NONCRITICAL : PassiveMessage.CRITICAL;
      
      // includes componentId: MCM team
      logger.debug("BusinessRuleExecutor@evaluateRulePrefix: Rule Prefix = <" + rulePrefix + ">, deal id = " + pe.getPageDealId() + ", copy id = " +
                           pe.getPageDealCID() + ", request id = " + currentRequest + ", borrower id = " + currentBorrower +
                           ", property id = " + currentProperty + ", component id = " + currentComponent);

      String message = null;
      int criticality = 0;

      boolean reportRuleName = rulePrefix.indexOf("%") != -1;

      String ruleName   = null;
      String actionProc = null;
      String exp        = null;

      try
      {
          while(br.next())
          {
              try
              {
                  exp               = br.ruleEntity.ruleExpression;
                  ruleName          = br.ruleEntity.ruleName;
                  actionProc        = br.ruleEntity.actionProc;

                  RuleResult rr = br.evaluate();

                  if (rr.ruleStatus == true)
                  {
                      logger.debug("BusinessRuleExecutor@evaluateRulePrefix: Rule Name = <" + ruleName + "> - tripped");

                      criticality = rr.getInt(2);
                      //--Release2.1--//
                      //Check which language to return -- By Billy 14Nov2002
                      try{
                        if(languageId == Mc.LANGUAGE_PREFERENCE_FRENCH)
                          // Get french messages
                          message = rr.getString(4);
                        else
                          // Get english messages -- default
                          message = rr.getString(3);
                      }
                      catch(Exception e)
                      {
                        //Force to generate Error message
                        message = null;
                      }
                      //===============================================

                      if (message == null || message.length() == 0)
                      {
                          criticality = criticalValue;
                          //--Release2.1--//
                          //Convert Error message to multilingual -- By Billy 14Nov2002
                          //message = "Business Rule " + ruleName + " has failed.  For details, please contact your system administrator.";
                          message = BXStringTokenizer.replace(
                            BXResources.getSysMsg("BRV_RULE_FAILED", languageId),
                            "%s",
                            ruleName);
                          //===========================================================
                      }
                      else
                          //--R
                          message = message + "  (Rule " + ruleName + ")";

                      pm.addMsg(message, criticality);

                      logActionProc(pm, logger, actionProc, criticalValue, diagnosticToClient);
                  }
                  else
                  {
                      //// logger.debug("BusinessRuleExecutor@evaluateRulePrefix: Rule Name = <" + ruleName + "> - executed without incident" + " --> diagnosticToClient = " + diagnosticToClient);

                      if (diagnosticToClient == true)
                      {
                         String msg = "BREValidator: rulePrefix = {" + rulePrefix + "}, rule name = {" + ruleName + "} - executed without incident";
                         pm.addMsg(msg, PassiveMessage.INFO);

                         logActionProc(pm, logger, actionProc, PassiveMessage.INFO, diagnosticToClient);
                      }
                  }
              }
              catch (Exception e)
              {
                  breException = true;
                  logRuleException(pm, e, rulePrefix, ruleName, exp, actionProc, pe, logger, diagnosticToClient, disallowCritical, languageId);
                  continue;
              }
          }
      }
      catch (Exception e)
      {
          breException = true;
          logRuleException(pm, e, rulePrefix, ruleName, exp, actionProc, pe, logger, diagnosticToClient, disallowCritical, languageId);
      }

      return breException;
  }

  //--Release2.1--//
  //Added LanguageId to get a Generate Messages in specified Language -- By Billy 14Nov2002
  private void logRuleException(PassiveMessage pm, Exception e,
      String rulePrefix, String ruleName, String exp, String actionProc,
      PageEntry pe, SysLogger logger, boolean diagnosticToClient, boolean disallowCritical, int languageId)
  {
      // used for morphing critical messages to non-critical (e.g. used in special circumstances only)
      int criticalValue = (disallowCritical == true) ?  PassiveMessage.NONCRITICAL : PassiveMessage.CRITICAL;

      // added componsntId : MCM team
      String msg = "BREValidator: Business Rule Engine exception evaluating rule prefix = {" +
                   rulePrefix + "}, rule name = {" + ruleName + "}, deal id = " + pe.getPageDealId() + ", copy id = " +
                   pe.getPageDealCID() + ", borrower id = " + currentBorrower +
                   ", property id = " + currentProperty + ", componentId = " + currentComponent;

      logger.error(msg);

      if (diagnosticToClient)
      {
          pm.addSeparator();
          pm.addMsg(msg, criticalValue);
          pm.addMsg("Rule Expression: " + exp, criticalValue);
          logActionProc(pm, logger, actionProc, criticalValue, diagnosticToClient);
      }

      msg = "BREValidator: Business Rule Engine exception = " + e.getMessage() + " (" + e + ")";
      // Added StackTrace message for debug -- By Billy 07May2002
      msg += StringUtil.stack2string(e);

      logger.error(msg);

      if (diagnosticToClient)
        pm.addMsg(msg, criticalValue);

      logger.error(e);

      //String rulename = (ruleName != null) ? ruleName : rulePrefix;

      //--Release2.1--//
      //Convert Error message to multilingual -- By Billy 14Nov2002
      //msg = "Business Rule " + rulename + " has failed.  For details, please contact your system administrator.";
      msg = BXStringTokenizer.replace(
        BXResources.getSysMsg("BRV_RULE_FAILED", languageId),
        "%s",
        ruleName);
      //===========================================================

      pm.addMsg(msg, criticalValue);

      if (diagnosticToClient)
          pm.addSeparator();
  }

  private void logActionProc(PassiveMessage pm, SysLogger logger, String actionProc, int criticality, boolean diagnosticToClient)
  {
      if (diagnosticToClient == false)
        return;

      if (actionProc == null)
      {
        /// pm.addMsg("Rule Action Proc: {not present}", criticality);
        return;
      }

      pm.addMsg("Rule Action Proc: {follows}", PassiveMessage.INFO);

      int ndx = 0;
      int end = 0;
      String remainder = null;

      for (;;)
      {
          ndx = actionProc.indexOf(";");

          remainder = null;

          end = actionProc.length() - 1;

          if (ndx != -1 && ndx != end)
          {
              remainder = actionProc.substring(ndx+1, end+1);
              actionProc = actionProc.substring(0,  ndx+1);
          }

          pm.addMsg(" " + actionProc, PassiveMessage.INFO);

          if (remainder == null)
            return;

          actionProc = remainder;
      }

  }

/***************************************************************************/
/* non - page level use methods:  */
/***************************************************************************/


  /**
  * Version for evaluating a single rule category/prefix
  */
  //--Release2.1--//
  //Provided overload method to pass LanguageId
  //Default to English
  // -- By Billy 14Nov2002
  public PassiveMessage BREValidator(DealPK pk, SessionResourceKit srk, PassiveMessage pm, String rulePrefix, int languageId) throws Exception
  {
    return BREValidator(pk, srk, pm, rulePrefix, false, languageId);
  }
  public PassiveMessage BREValidator(DealPK pk, SessionResourceKit srk, PassiveMessage pm, String rulePrefix) throws Exception
  {
    return BREValidator(pk, srk, pm, rulePrefix, false, Mc.LANGUAGE_PREFERENCE_ENGLISH);
  }
  public PassiveMessage BREValidator(DealPK pk, SessionResourceKit srk, PassiveMessage pm, String rulePrefix, boolean optimal, int languageId) throws Exception
  {
      Vector v = new Vector();
      v.add(rulePrefix);

      return BREValidator(pk, srk, pm, v, optimal, languageId);
  }
  public PassiveMessage BREValidator(DealPK pk, SessionResourceKit srk, PassiveMessage pm, String rulePrefix, boolean optimal) throws Exception
  {
      Vector v = new Vector();
      v.add(rulePrefix);

      return BREValidator(pk, srk, pm, v, optimal, Mc.LANGUAGE_PREFERENCE_ENGLISH);
  }

  public PassiveMessage BREValidator(DealPK pk, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs,  int languageId) throws Exception
  {
    return BREValidator(pk, srk, pm, rulePrefixs, false, languageId);
  }
  public PassiveMessage BREValidator(DealPK pk, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs) throws Exception
  {
    return BREValidator(pk, srk, pm, rulePrefixs, false, Mc.LANGUAGE_PREFERENCE_ENGLISH);
  }
  
  /**
   * <p>BREValidator</p>
   * @param pk
   * @param srk
   * @param pm
   * @param rulePrefixs
   * @param optimal
   * @param languageId
   * @return
   * @throws Exception
   * 
   * @version 1.1 July 15, 2008 -- MCM Team (XS 2.39)
   * @version 1.4 Aug 5, 2008 -- MCM Team (XS 10.2)
   */
  public PassiveMessage BREValidator(DealPK pk, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs, boolean optimal, int languageId) throws Exception
  {
    boolean pmCreated = false;
    String rulePrefix = null;

    SysLogger logger = srk.getSysLogger();

    // if no passive message object passed create one
    if (pm == null)
    {
      pmCreated = true;
      pm = new PassiveMessage();
    }

    if(pk == null)
    {
      String msg = "ERROR: BREValidator - null primary key recieved.";
      logger.error(msg);
      throw new Exception(msg);
    }

    // determine if ctitical messages are to be morphed as non-critical
    boolean disallowCritical = false;
    String YN = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),"com.basis100.bre.morph.critical.to.noncritical");
    if (YN != null && YN.equals("Y"))
        disallowCritical = true;

    boolean exceptionEncountered = pm.getExceptionEncountered();

    srk.getJdbcExecutor().closeExecutorTemporary();

    if((optimal == false) || ((optimal == true) && (br == null)))
    {
      br = new BusinessRule(srk, -1, -1, "na", "");
      // set the standard BRE (instance) variables ...
      br.setDealId(pk.getId(), pk.getCopyId());
      // #2242: Catherine, 23-Nov-05 --------- start ---------  
      br.setLenderName();
      BRUtil.debug("BusinessRuleExecutor: set LenderName done");
      // #2242: Catherine, 23-Nov-05 --------- end ---------  
      
      //MCM team (XS 10.2) STARTS
      br.setRequestId(currentRequest);
      br.setComponentId(currentComponent);
      //MCM team (XS 10.2) ENDS
    }


  //  br.setUserTypeId(theSession.getSessionUserId());
   //ALERT: what is the effect of usertype on br execution
   // Added the setting of the usertype to the br 	
	
   UserProfile up = new UserProfile(srk);
   UserProfileBeanPK upPK = new UserProfileBeanPK(srk.getExpressState().getUserProfileId(), 
                                                  srk.getExpressState().getDealInstitutionId());
   up.findByPrimaryKey(upPK);
   br.setUserTypeId(up.getUserTypeId());
   //end addition
		
    // set global scope variables
    br.setGlobal("CurrentBorrowerId", new Integer(currentBorrower));
    br.setGlobal("CurrentPropertyId", new Integer(currentProperty));
    // MCM team (XS 10.2) starts
    br.setGlobal("CurrentRequestId", new Integer(currentRequest));
    // MCM team (XS 10.2) ends
    // MCM team (XS 2.39) starts
    br.setGlobal("CurrentComponentId", new Integer(currentComponent));
    // MCM team (XS 2.39) ends

    // bug fix (rule prefix should not be null. Now we run everything from the
    // for loop.
    //exceptionEncountered = evaluateRulePrefix(pm, br, rulePrefix, pk, logger, exceptionEncountered, disallowCritical);

    // evaluate remaining prefixes (if any)

    int lim = rulePrefixs.size();

    for (int i = 0; i < lim; ++i)
    {
        rulePrefix = (String)rulePrefixs.elementAt(i);

        br.reQuery(-1, -1, "na", rulePrefix, srk.getExpressState().getDealInstitutionId());

        //--Release2.1--//
        //Pass LanguageId to get a Rule Messages in spoecified Language -- By Billy 14Nov2002
        exceptionEncountered = evaluateRulePrefix(pm, br, rulePrefix, pk, logger,
          exceptionEncountered, disallowCritical, languageId);
    }

    // close the rule object
    if(optimal == false)
      br.close();

    // return null if no messages - but only if message object was created here (i.e. always
    // return message object provided even when no messages)

    if (pmCreated == true && pm.getNumMessages() <= 0)
      return null;

    pm.setExceptionEncountered(exceptionEncountered);

    return pm;
  }

  private boolean evaluateRulePrefix(PassiveMessage pm, BusinessRule br, String rulePrefix, DealPK pk,
                                      SysLogger logger, boolean breException, boolean disallowCritical, int languageId)
  {

      PageEntry pe = new PageEntry();
      pe.setPageDealId(pk.getId());
      pe.setPageDealCID(pk.getCopyId());

      return evaluateRulePrefix(pm, br, pe, rulePrefix, logger, breException, false, disallowCritical, languageId);
  }

  public void close() throws Exception
  {
    br.close();
  }

}
