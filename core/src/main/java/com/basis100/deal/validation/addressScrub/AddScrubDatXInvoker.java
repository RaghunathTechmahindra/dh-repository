package com.basis100.deal.validation.addressScrub;

import java.util.Iterator;

import com.filogix.datx.messagebroker.*;
import com.filogix.datx.messagebroker.exception.*;
import com.filogix.datx.messagebroker.scrubaddress.*;
import com.filogix.datx.bindings.internal.valuation.request.version0100.*;
import com.filogix.datx.bindings.internal.valuation.request.version0100.impl.*;
import com.filogix.datx.bindings.internal.valuation.response.version0100.*;
import com.filogix.datx.bindings.internal.valuation.response.version0100.PROPERTYType;
import com.filogix.datx.bindings.internal.valuation.response.version0100.impl.*;
import javax.xml.bind.*;


public class AddScrubDatXInvoker {

/**
 * Title: Sample Abstract webservice caller.
 * @author AKORU
 * @version 1.0
 */




    //Sample webservice interface to DatX's address scrubber.
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        try {

            //Set the request object
            AddressScrubRequestBean requestObj = new AddressScrubRequestBean();
            /*
            requestObj.setAccountIdentifier("FXP");              //Mandatory for Expert
            requestObj.setAccountPassword("FXP123");             //Mandatory for Expert
            requestObj.setReceivingPartyId("com.filogix");          //Mandatory for Expert
            requestObj.setRequestingPartyId("com.filogix.fxp");  //Mandatory for Expert
            requestObj.setLocaleCode("EN");  // (fr=French en-English Language type for response address
            requestObj.setStreetName("townley");  // This can have suffixes, direction as well as unit numbers
            requestObj.setStreetNumber("40");
            requestObj.setCity("brampton");
            requestObj.setProvince("on");
            requestObj.setStreetSuffix("street");
            requestObj.setApartmentOrUnit("");
            requestObj.setPostalCode("");
            */
            requestObj.setAccountIdentifier("FXP");              //Mandatory for Expert
            requestObj.setAccountPassword("FXP123");             //Mandatory for Expert
            requestObj.setReceivingPartyId("com.filogix");          //Mandatory for Expert
            requestObj.setRequestingPartyId("com.filogix.fxp");  //Mandatory for Expert
            requestObj.setLocaleCode("EN");  // (fr=French en-English Language type for response address
            requestObj.setStreetName("#607 182 Church St Apt 607");  // This can have number, suffixes, direction as well as unit numbers
            requestObj.setStreetNumber("");
            requestObj.setCity("Brampton");
            requestObj.setProvince("on");
            requestObj.setStreetSuffix("");
            requestObj.setApartmentOrUnit("");
            requestObj.setPostalCode("L6T3X9");

            //Get the message sender to send the request as XML string
            AddressScrubResponseBean responseObj = null;


              for (int i=0; i<1; i++)
              {
                //Test in a loop and see performance improvements once class definitions are loaded in the JVM.
                  AddrScrubMessageSender requestSender = new AddrScrubMessageSender();

                  String reqXML = requestSender.createRequest(requestObj);

                if (requestObj != null) {
                  System.out.println("REQUEST:" + reqXML);
                  System.out.println("REQObj: StreetNumber = " + requestObj.getStreetNumber());
                  System.out.println("REQObj: StreetName = " + requestObj.getStreetName());
                  System.out.println("REQObj: StreetSuffix = " + requestObj.getStreetSuffix());
                  System.out.println("REQObj: ApartmentOrUnit = " + requestObj.getApartmentOrUnit());
                  System.out.println("REQObj: City = " + requestObj.getCity());
                  System.out.println("REQObj: Province = " + requestObj.getProvince());
                  System.out.println("REQObj: PostalCode = " + requestObj.getPostalCode());

                  //String respXML = requestSender.execute(reqXML, IMessageSender.DATX_TEST_URL);
                  String respXML = requestSender.execute(reqXML, IMessageSender.DATX_PROD_URL);

                  System.out.println(i + " RESPONSE:" + respXML);
                  responseObj = requestSender.createResponse(respXML);
                  System.out.println("REPObj: Condition = " + responseObj.getCondition());
                  System.out.println("REPObj: Desc = " + responseObj.getDescription());
                  System.out.println("REPObj: StreetNumber = " + responseObj.getStreetNumber());
                  System.out.println("REPObj: StreetName = " + responseObj.getStreetName());
                  System.out.println("REPObj: StreetDir = " + responseObj.getDirection());
                  System.out.println("REPObj: StreetSuffix = " + responseObj.getStreetSuffix());
                  System.out.println("REPObj: ApartmentOrUnit = " + responseObj.getApartmentOrUnit());
                  System.out.println("REPObj: City = " + responseObj.getCity());
                  System.out.println("REPObj: Province = " + responseObj.getProvince());
                  System.out.println("REPObj: PostalCode = " + responseObj.getPostalCode());

                }
              }


            System.exit(0);

          }
          catch (CommunicationException ce) {
             // The application can perform a retry if required or send e-mail to support
             System.out.println("Trapped: Connection Exception Occured.");
          }
          catch (Exception e) {
             // The application can perform a retry if required or send e-mail to support
             System.out.println("Trapped: unKnown Exception Occured." + e.getMessage());
        }
    }


}
