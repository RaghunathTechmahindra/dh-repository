package com.basis100.deal.validation.addressScrub;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: AddScrubConstants</p>
 *
 * <p>Description: Class to define all the constants for Address Scrubbing.</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Filogix Inc.</p>
 * @author Billy Lam
 * @version 1.0 (Initial Version - 15 June 2004)
*
* 12/Sep/2005 DVG #DG314 address scrub revisit
* 13/Sep/2004 DVG #DG80 address scrub - added final to constants
 */

public class AddScrubConstants
{
  //Address Source Definitions
  public static final int ADDRESS_SOURCE_INGESTION = 0;
  public static final int ADDRESS_SOURCE_SCREEN = 1;

  //Address Type Definitions
  public static final int ADDRESS_TYPE_PRIM_BORROWER_CURR = 0;
  public static final int ADDRESS_TYPE_PRIM_BORROWER_PREV = 1;
  public static final int ADDRESS_TYPE_CO_BORROWER_CURR = 2;
  public static final int ADDRESS_TYPE_CO_BORROWER_PREV = 3;
  //--> 4 to 13 is not for Phase I
  public static final int ADDRESS_TYPE_EMPLOYMENT = 4;  //#DG314

  public static final int ADDRESS_TYPE_PROPERTY = 14;

  public static final Map AddressTypes = new HashMap();
  static 
  {
    AddressTypes.put(new Integer(ADDRESS_TYPE_PRIM_BORROWER_CURR), "Primary Borrower's Current Address");
    AddressTypes.put(new Integer(ADDRESS_TYPE_PRIM_BORROWER_PREV), "Primary Borrower's Previous Address");
    AddressTypes.put(new Integer(ADDRESS_TYPE_CO_BORROWER_CURR), "Guarantor's Current Address");
    AddressTypes.put(new Integer(ADDRESS_TYPE_CO_BORROWER_PREV), "Guarantor's Previous Address");
    AddressTypes.put(new Integer(ADDRESS_TYPE_EMPLOYMENT), "Employer's Address");
    AddressTypes.put(new Integer(ADDRESS_TYPE_PROPERTY), "Property's Address");
  }
  
  // Definitions for DatX interface ??  TODO ??
  public static final boolean PARSED = true;
  public static final boolean UNPARSED = false;
  public static final int RESULT_CODE_FOUND_MATCH = 20;
  public static final int RESULT_CODE_SUGGESTIONS = 21;
  public static final boolean CONCAT_INCLUDES_LINE2 = true;
  public static final boolean CONCAT_NOTINCLUDES_LINE2 = false;
  public static final int icAddrAddressLineSize = 35; // size of addres line
  public static final String PICKLIST_STREETTYPE = "STREETTYPE";
  public static final String PICKLIST_STREETDIRECTION = "STREETDIRECTION";
  public static final String PICKLIST_PROVINCESHORTNAME = "PROVINCESHORTNAME";
  
  public static final int RESULT_CODE_NOT_SEND = 10;

}
