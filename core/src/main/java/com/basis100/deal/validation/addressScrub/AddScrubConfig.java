package com.basis100.deal.validation.addressScrub;

import java.util.*;

import com.basis100.resources.*;
import com.basis100.entity.*;
import com.basis100.deal.util.DBA;
import com.basis100.deal.util.TypeConverter;

/**
 * <p>Title: AddScrubConfig</p>
 *
 * <p>Description: Entity class to repressent a record of AddScrubConfig table.</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Filogix Inc.</p>
 * @author Billy Lam
 * @version 1.0 (Initial Version - 15 June 2004)
*
*13/Sep/2004 #DG80 streetAddressLine now replaces detailed cols.
 */

public class AddScrubConfig
  extends EntityBase
{
  EntityContext ctx;

  // properties
  private int addressTypeId;
  private int addressSourceId;
  private boolean isCritical;
  private boolean streetAddressLine;
  private boolean city;
  private boolean province;
  private boolean postalCode;

  //
  // Constructor: Accepts session resources object (access to system logger and Jdbc
  //              connection resources.
  //
  public AddScrubConfig(SessionResourceKit srk)
  {
    super(srk);
  }

  // setters/getters
  public boolean isIsCritical()
  {
    return isCritical;
  }

  public boolean isCity()
  {
    return city;
  }

  public int getAddressSourceId()
  {
    return addressSourceId;
  }

  public int getAddressTypeId()
  {
    return addressTypeId;
  }

  public boolean isProvince()
  {
    return province;
  }

  public boolean streetAddressLine()
  {
    return streetAddressLine;
  }

  public boolean isPostalCode()
  {
    return postalCode;
  }

  public void setAddressSourceId(int addressSourceId)
  {
    this.addressSourceId = addressSourceId;
  }

  public void setAddressTypeId(int addressTypeId)
  {
    this.addressTypeId = addressTypeId;
  }

  public void setPostalCode(String postalCode)
  {
    this.postalCode = TypeConverter.booleanFrom(postalCode);
  }

  public void setPostalCode(boolean postalCode)
  {
    this.postalCode = postalCode;
  }

  public void setIsCritical(String isCritical)
  {
    this.isCritical = TypeConverter.booleanFrom(isCritical);
  }

  public void setIsCritical(boolean isCritical)
  {
    this.isCritical = isCritical;
  }

  public void setCity(String city)
  {
    this.city = TypeConverter.booleanFrom(city);
  }

  public void setCity(boolean city)
  {
    this.city = city;
  }

  public void setProvince(String province)
  {
    this.province = TypeConverter.booleanFrom(province);
  }

  public void setProvince(boolean province)
  {
    this.province = province;
  }

  public void setStreetAddressLine(String streetAdrLine)
  {
    this.streetAddressLine = TypeConverter.booleanFrom(streetAdrLine);
  }

  public void setStreetAddressLine(boolean streetAdrLine)
  {
    this.streetAddressLine = streetAdrLine;
  }

  //
  // Remote-Home inerface methods:
  //
  // The following methods have counterparts in Enterprise Bean classes - in an
  // Enterprise Entity Bean each methods name would be prefixed with "ejb"
  //

  public AddScrubConfig create(int addressTypeId, int addressSourceId, boolean isCritical,
    boolean streetAddressLine, boolean city, boolean province,
    boolean postalCode) throws RemoteException, CreateException
  {
    String sql = "Insert into addscrubconfig (addressTypeId, addressSourceId, isCritical, streetNumber, streetName, " +
      "streetSuffix, direction, apartmentOrUnit, city, province, postalCode) " +
      "Values (" +
      addressTypeId + ", " +
      addressSourceId + ", " +
      DBA.sqlStringFrom(isCritical)+ ", " +
      DBA.sqlStringFrom(streetAddressLine) + ", " +
      DBA.sqlStringFrom(city) + ", " +
      DBA.sqlStringFrom(province) + ", " +
      DBA.sqlStringFrom(postalCode) +
      ")";

    try
    {
      jExec.executeUpdate(sql);
    }
    catch(Exception e)
    {
      CreateException ce = new CreateException("AddScrubConfig Entity - create() exception");

      logger.error(ce.getMessage());
      logger.error("create sql: " + sql);
      logger.error(e);

      throw ce;
    }

    // Set all local fields
    setAddressTypeId(addressTypeId);
    setAddressSourceId(addressSourceId);
    setIsCritical(isCritical);
    setStreetAddressLine(streetAddressLine);
    setCity(city);
    setProvince(province);
    setPostalCode(postalCode);

    return this;
  }

  public void ebjLoad() throws RemoteException
  {
    // implementation not required for now!
  }

  public void ebjPassivate() throws RemoteException
  {
    ;
  }

  public void ejbActivate() throws RemoteException
  {
    ;
  }

  public void ejbStore() throws RemoteException
  {
    String sql = "Update AddScrubConfig set " +
      "addressTypeId = " + getAddressTypeId() + ", " +
      "addressSourceId = " + getAddressSourceId() + ", " +
      "isCritical = '" + (isIsCritical() ? "Y" : "N") + "', " +
      //#DG80       "streetNumber = '" + (isStreetNumber() ? "Y" : "N") + "', " +
      //#DG80       "streetName = '" + (isStreetName() ? "Y" : "N") + "', " +
      //#DG80       "streetSuffix = '" + (isStreetSuffix() ? "Y" : "N") + "', " +
      //#DG80       "direction = '" + (isDirection() ? "Y" : "N") + "', " +
      //#DG80       "apartmentOrUnit = '" + (isApartmentOrUnit() ? "Y" : "N") + "', " +
      "STREETADDRESSLINE = '" + (streetAddressLine() ? "Y" : "N") + "', " +

      "city = '" + (isCity() ? "Y" : "N") + "', " +
      "province = '" + (isProvince() ? "Y" : "N") + "', " +
      "postalCode = '" + (isPostalCode() ? "Y" : "N") + "'" +

      " where addressTypeId = " + getAddressTypeId() + " and " +
      "addressSourceId = " + getAddressSourceId();

    try
    {
      jExec.executeUpdate(sql);
    }
    catch(Exception e)
    {
      RemoteException re = new RemoteException("AddScrubConfig Entity - ejbStore() exception");

      logger.error(re.getMessage());
      logger.error("ejbStore sql: " + sql);
      logger.error(e);

      throw re;
    }
  }

  public void remove() throws RemoteException, RemoveException
  {
    String sql = "Delete from ADDSCRUBCONFIG where " +
      "addressTypeId = " + getAddressTypeId() + " and " +
      "addressSourceId = " + getAddressSourceId();
    try
    {
      jExec.executeUpdate(sql);
    }
    catch(Exception e)
    {
      RemoveException re = new RemoveException("AddScrubConfig Entity - remove() exception");

      logger.error(re.getMessage());
      logger.error("remove sql: " + sql);
      logger.error(e);

      throw re;
    }
  }

  // finders:
  // Find by Unique Keys (addressTypeId + addressSourceId)
  public AddScrubConfig findByPrimaryKeys(int addressTypeId, int addressSourceId) throws RemoteException, FinderException
  {
    String sql = "Select * from ADDSCRUBCONFIG where " +
      "addressTypeId = " + addressTypeId + " and " +
      "addressSourceId = " + addressSourceId;

    boolean gotRecord = false;

    try
    {
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        gotRecord = true;
        setPropertiesFromQueryResult(key);
        break; // can only be one record
      }

      jExec.closeData(key);

      if(gotRecord == false)
      {
        String msg = "AddScrubConfig Entity: @findByPrimaryKeys(), " +
          "addressTypeId=" + addressTypeId + ", addressSourceId=" + addressSourceId + ", entity not found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException("AddScrubConfig Entity - findByPrimaryKeys() exception");

      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }

    return this;
  }

  // Return all AddScrubConfig records for the specified AddressSourceId
  public Vector findByAddressSourceId(int addressSourceId) throws RemoteException, FinderException
  {
    String sql = "Select * from ADDSCRUBCONFIG where " +
      "addressSourceId = " + addressSourceId;

    boolean gotRecord = false;
    Vector v = new Vector();

    try
    {
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        gotRecord = true;
        AddScrubConfig aRecord = new AddScrubConfig(srk);
        aRecord.setPropertiesFromQueryResult(key);
        v.add(aRecord);
      }

      jExec.closeData(key);

      if(gotRecord == false)
      {
        String msg = "AddScrubConfig Entity: @findByAddressSourceId() addressSourceId=" + addressSourceId +
          ", no entities found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException("AddScrubConfig Entity: @findByAddressSourceId() exception");
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
    return v;
  }

  // Return all AddScrubConfig records for the specified AddressTypeId
  public Vector findByAddressTypeId(int addressTypeId) throws RemoteException, FinderException
  {
    String sql = "Select * from ADDSCRUBCONFIG where " +
      "addressTypeId = " + addressTypeId;

    boolean gotRecord = false;
    Vector v = new Vector();

    try
    {
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        gotRecord = true;
        AddScrubConfig aRecord = new AddScrubConfig(srk);
        aRecord.setPropertiesFromQueryResult(key);
        v.add(aRecord);
      }

      jExec.closeData(key);

      if(gotRecord == false)
      {
        String msg = "AddScrubConfig Entity: @findByAddressTypeId() addressTypeId=" + addressTypeId + ", no entities found";
        logger.error(msg);
        throw new FinderException(msg);
      }
    }
    catch(Exception e)
    {
      FinderException fe = new FinderException("AddScrubConfig Entity: @findByAddressTypeId() exception");
      logger.error(fe.getMessage());
      logger.error("finder sql: " + sql);
      logger.error(e);

      throw fe;
    }
    return v;
  }

  //
  // Private
  //
  private void setPropertiesFromQueryResult(int key) throws Exception
  {

  //ADDRESSTYPEID, ADDRESSSOURCEID, ISCRITICAL, STREETADDRESSLINE, CITY, PROVINCE, POSTALCODE

    setAddressTypeId(jExec.getInt(key, "addressTypeId"));
    setAddressSourceId(jExec.getInt(key, "addressSourceId"));
    setIsCritical(jExec.getString(key, "isCritical"));

    //#DG80 setStreetNumber(jExec.getString(key, "streetNumber"));
    //#DG80 setStreetName(jExec.getString(key, "streetName"));
    setStreetAddressLine(jExec.getString(key, "STREETADDRESSLINE"));
    //#DG80 setStreetSuffix(jExec.getString(key, "streetSuffix"));
    //#DG80 setDirection(jExec.getString(key, "direction"));
    //#DG80 setApartmentOrUnit(jExec.getString(key, "apartmentOrUnit"));

    setCity(jExec.getString(key, "city"));
    setProvince(jExec.getString(key, "province"));
    setPostalCode(jExec.getString(key, "postalCode"));
  }

  public void unsetEntityContext()
  {
    ctx = null;
  }

  public String toString()
  {
    String tmpStr = "AddScrubConfig : addressTypeId=" + addressTypeId + ", addressSourceId=" + addressSourceId + "\n" +
      "isCritical=" + isCritical + "\n" +
      "streetAddressLine=" + streetAddressLine + "\n" +
      "city=" + city + "\n" +
      "province=" + province + "\n" +
      "postalCode=" + postalCode;

    return tmpStr;
  }

}
