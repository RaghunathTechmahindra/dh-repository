package com.basis100.deal.validation;

/**
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 */

import java.util.Formattable;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.Vector;

import MosSystem.Mc;

import com.basis100.BREngine.BRUtil;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.BXStringTokenizer;
import com.basis100.deal.util.StringUtil;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

import config.Config;
/**
  * Contains services for executing MOS business rules.
  *
  * Summary:
  *
  *
  *   Methods for executing business rules by rule prefix. Rules are housed in the Business Rule
  *   Engine.
  *
  * . setCurrentBorrower()
  * . setCurrentProperty()
  *
  *   Support invocations with local scoping of rules to be evaluated. Some rule type (rule
  *   prefixes) run with local scoping - e.g. rules to be evaluated for a specific borrower,
  *   or for a specific property. Usage - set the desired properties before invoking a high level
  *   validation function (e.g. BREValidator()), e.g.
  *
  *        PagelessBusinessRuleExecutor brExec = new PagelessBusinessRuleExecutor(...);
  *        brExec.setCurrentBorrower();
  *
  *        o = brExec.validate(...);
  *
  **/
public class PagelessBusinessRuleExecutor
{
  private int currentBorrower = -1;
  private int currentProperty = -1;

  private boolean diagnosticToClient = false;

  public void setCurrentBorrower(int id)
  {
    currentBorrower = id;
  }

  public void setCurrentProperty(int id)
  {
    currentProperty = id;
  }

  public PagelessBusinessRuleExecutor(){;}

  public PagelessBusinessRuleExecutor(boolean diagnosticToClient)
  {
    this.diagnosticToClient = diagnosticToClient;
  }

  //--Release2.1--//
  //Provided overload method to pass LanguageId
  //Default to English
  // -- By Billy 14Nov2002
  // changed the first param from DealPk to Deal for ML
  public PassiveMessage validate(Deal deal, SessionResourceKit srk, PassiveMessage pm, String rulePrefix) throws Exception
  {
    return validate(deal, srk, pm, rulePrefix, Mc.LANGUAGE_PREFERENCE_ENGLISH);
  }
  // changed the first param from DealPk to Deal for ML
  public PassiveMessage validate(Deal deal, SessionResourceKit srk, PassiveMessage pm, String rulePrefix, int languageId) throws Exception
  {
      Vector v = new Vector();
      v.add(rulePrefix);

      return validate(deal, srk, pm, v, languageId);
  }

  // changed the first param from DealPk to Deal for ML
  public PassiveMessage validate(Deal deal, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs) throws Exception
  {
    return validate(deal, srk, pm, rulePrefixs, Mc.LANGUAGE_PREFERENCE_ENGLISH);
  }
  
  // changed the first param from DealPk to Deal for ML
  public PassiveMessage validate(Deal deal, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs, int languageId) throws Exception
  {
    boolean pmCreated = false;
    String rulePrefix = null;

    SysLogger logger = srk.getSysLogger();

    // if no passive message object passed create one
    if (pm == null)
    {
      pmCreated = true;
      pm = new PassiveMessage();
    }

    if(deal == null)
    {
      String msg = "ERROR: BREValidator - null primary key recieved.";
      logger.error(msg);
      throw new Exception(msg);
    }

    // determine if ctitical messages are to be morphed as non-critical
    boolean disallowCritical = false;
    String YN = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(), "com.basis100.bre.morph.critical.to.noncritical");
    if (YN != null && YN.equals("Y"))
        disallowCritical = true;

    boolean exceptionEncountered = pm.getExceptionEncountered();

    rulePrefix = (String)rulePrefixs.elementAt(0);

    srk.setCloseJdbcConnectionOnRelease(true);

    srk.getJdbcExecutor().closeExecutorTemporary();

    // added for ML
    // set VPD from deal insittuionProfileID
    srk.getExpressState().setDealInstitutionId(deal.getInstitutionProfileId());
    BusinessRule br = new BusinessRule(srk, -1, -1, "na", rulePrefix);

    // set the standard BRE (instance) variables ...
    br.setDealId(deal.getDealId(), deal.getCopyId());
    
    // #2242: Catherine, 23-Nov-05 --------- start ---------  
    br.setLenderName();
    BRUtil.debug("PagelessBusinessRuleExecutor: set LenderName done");
    // #2242: Catherine, 23-Nov-05 --------- end ---------  

    //  br.setUserTypeId(theSession.getSessionUserId());
   //ALERT: what is the effect of usertype on br execution

    // set global scope variables
    br.setGlobal("CurrentBorrowerId", new Integer(currentBorrower));
    br.setGlobal("CurrentPropertyId", new Integer(currentProperty));

    //--Release2.1--//
    //Pass LanguageId for Multilingual Support -- By Billy 14Nov2002
    exceptionEncountered 
        = evaluateRulePrefix(pm, br, rulePrefix, 
                             new DealPK(deal.getDealId(), deal.getCopyId()), 
                             logger, exceptionEncountered, disallowCritical, 
                             languageId);

    // evaluate remaining prefixes (if any)

    int lim = rulePrefixs.size();

    for (int i = 1; i < lim; ++i)
    {
        rulePrefix = (String)rulePrefixs.elementAt(i);

        br.reQuery(-1, -1, "na", rulePrefix, srk.getExpressState().getDealInstitutionId());

        //--Release2.1--//
        //Pass LanguageId for Multilingual Support -- By Billy 14Nov2002
        exceptionEncountered 
            = evaluateRulePrefix(pm, br, rulePrefix, 
                                 new DealPK(deal.getDealId(), deal.getCopyId()), 
                                 logger, exceptionEncountered, disallowCritical, 
                                 languageId);
    }

    // close the rule object
    br.close();

    // return null if no messages - but only if message object was created here (i.e. always
    // return message object provided even when no messages)

    if (pmCreated == true && pm.getNumMessages() <= 0)
      return null;

    pm.setExceptionEncountered(exceptionEncountered);

    return pm;
 }

 /**
  * Modification of the method PassiveMessage validate(DealPK pk, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs).
  * This one allows entry of flobal values which can be set to business rules object.
  */
  //--Release2.1--//
  //Added LanguageId for Multilingual support -- By Billy 14Nov2002
  public void validate(Deal deal, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs,
      HashMap globPairs) throws Exception
  {
    validate(deal, srk, pm, rulePrefixs, globPairs, Mc.LANGUAGE_PREFERENCE_ENGLISH);
  }
  public void validate(Deal deal, SessionResourceKit srk, PassiveMessage pm, Vector rulePrefixs,
      HashMap globPairs, int languageId) throws Exception
  {
    //boolean pmCreated = false;
    String rulePrefix = null;

    SysLogger logger = srk.getSysLogger();

    // if no passive message object passed create one
    if (pm == null)
    {
      //pmCreated = true;
      pm = new PassiveMessage();
    }

    if(deal == null)
    {
      String msg = "ERROR: BREValidator - null primary key recieved.";
      logger.error(msg);
      throw new Exception(msg);
    }

    // determine if ctitical messages are to be morphed as non-critical
    boolean disallowCritical = false;
    boolean exceptionEncountered = pm.getExceptionEncountered();
    rulePrefix = (String)rulePrefixs.elementAt(0);
    srk.setCloseJdbcConnectionOnRelease(true);
    srk.getJdbcExecutor().closeExecutorTemporary();

    // added for ML
    srk.getExpressState().setDealInstitutionId(deal.getInstitutionProfileId());
    BusinessRule br = new BusinessRule(srk, -1, -1, "na", rulePrefix);
    // set the standard BRE (instance) variables ...
    br.setDealId(deal.getDealId(), deal.getCopyId());
    // #2242: Catherine, 23-Nov-05 --------- start ---------  
    br.setLenderName();
    BRUtil.debug("PagelessBusinessRuleExecutor: set LenderName done");
    // #2242: Catherine, 23-Nov-05 --------- end ---------  

    // set global scope variables
    Set keySet = globPairs.keySet();
    Iterator it = keySet.iterator();
    while(it.hasNext()){
      String oneKey = (String) it.next();
      br.setGlobal(oneKey,globPairs.get(oneKey));
    }

    //--Release2.1--//
    //Pass LanguageId to get a Rule Messages in specified Language -- By Billy 14Nov2002
    exceptionEncountered = evaluateRulePrefix(pm, br, rulePrefix, new DealPK(deal.getDealId(), deal.getCopyId()), logger, exceptionEncountered, disallowCritical, languageId);

    // evaluate remaining prefixes (if any)
    int lim = rulePrefixs.size();

    for (int i = 1; i < lim; ++i)
    {
        rulePrefix = (String)rulePrefixs.elementAt(i);

        br.reQuery(-1, -1, "na", rulePrefix, srk.getExpressState().getDealInstitutionId());
        //--Release2.1--//
        //Pass LanguageId to get a Rule Messages in specified Language -- By Billy 14Nov2002
        exceptionEncountered = evaluateRulePrefix(pm, br, rulePrefix, new DealPK(deal.getDealId(), deal.getCopyId()), logger, exceptionEncountered, disallowCritical, languageId);
    }

    // close the rule object
    br.close();
 }

  //--Release2.1--//
  //Added Language ID input to indicate language of messages to return.
  //Assumptions : Result Parameter #3 Message in English
  //              Result Parameter #4 Message in French
  //--> By Billy 14Nov2002
  private boolean evaluateRulePrefix(PassiveMessage pm, BusinessRule br,  String rulePrefix, DealPK pk,
      SysLogger logger, boolean breException, boolean disallowCritical, int languageId)
  {
      // used for morphing critical messages to non-critical (e.g. used in special circumstances only)
      int criticalValue = (disallowCritical == true) ?  PassiveMessage.NONCRITICAL : PassiveMessage.CRITICAL;

      logger.debug("BusinessRuleExecutor@evaluateRulePrefix: Rule Prefix = <" + rulePrefix + ">, deal id = " + pk.getId() + ", copy id = " +
                           pk.getCopyId() + ", borrower id = " + currentBorrower +
                           ", property id = " + currentProperty);

      /*#DG600 
      String message = null;
      int criticality = 0; */

      boolean reportRuleName = rulePrefix.indexOf("%") != -1;

      String ruleName   = null;
      String actionProc = null;
      String exp        = null;

      try
      {
          while(br.next())
          {
              try
              {
                  exp               = br.ruleEntity.ruleExpression;
                  ruleName          = br.ruleEntity.ruleName;
                  actionProc        = br.ruleEntity.actionProc;

                  RuleResult rr = br.evaluate();

                  if (rr.ruleStatus == true)
                  {
                      logger.debug("BusinessRuleExecutor@evaluateRulePrefix: Rule Name = <" + ruleName + "> - tripped");

                      int criticality = rr.getInt(2);

                      Formattable formBRMsg = newBREntry(ruleName, rr);
                      String message = String.format(BXResources.getLocale(languageId), "%s", formBRMsg);
                      pm.addMsg(message, criticality, formBRMsg);

                      logActionProc(pm, logger, actionProc, criticalValue, false);
                  }
                  else
                  {
                      logger.debug("BusinessRuleExecutor@evaluateRulePrefix: Rule Name = <" + ruleName + "> - executed without incident" + " --> diagnosticToClient = " + diagnosticToClient);

                      if (diagnosticToClient == true)
                      {
                         String msg = "BREValidator: rulePrefix = {" + rulePrefix + "}, rule name = {" + ruleName + "} - executed without incident";
                         pm.addMsg(msg, PassiveMessage.INFO);

                         logActionProc(pm, logger, actionProc, PassiveMessage.INFO, diagnosticToClient);
                      }
                  }
              }
              catch (Exception e)
              {
                  breException = true;
                  logRuleException(pm, e, rulePrefix, ruleName, exp, actionProc, pk, logger, false, disallowCritical, languageId);
                  continue;
              }
          }
      }
      catch (Exception e)
      {
          breException = true;
          logRuleException(pm, e, rulePrefix, ruleName, exp, actionProc, pk, logger, false, disallowCritical, languageId);
      }

      return breException;
  }

  //--Release2.1--//
  //Added LanguageId to get a Generate Messages in specified Language -- By Billy 14Nov2002
   private void logRuleException(PassiveMessage pm, Exception e,
          String rulePrefix, String ruleName, String exp, String actionProc,
          DealPK pk, SysLogger logger, boolean diagnosticToClient, boolean disallowCritical, int languageId)
  {
      // used for morphing critical messages to non-critical (e.g. used in special circumstances only)
      int criticalValue = (disallowCritical == true) ?  PassiveMessage.NONCRITICAL : PassiveMessage.CRITICAL;

      String msg = "BREValidator: Business Rule Engine exception evaluating rule prefix = {" +
                   rulePrefix + "}, rule name = {" + ruleName + "}, deal id = " + pk.getId() + ", copy id = " +
                   pk.getCopyId() + ", borrower id = " + currentBorrower +
                   ", property id = " + currentProperty;

      logger.error(msg);

      if (diagnosticToClient)
      {
          pm.addSeparator();
          pm.addMsg(msg, criticalValue);
          pm.addMsg("Rule Expression: " + exp, criticalValue);
          logActionProc(pm, logger, actionProc, criticalValue, diagnosticToClient);
      }

      msg = "BREValidator: Business Rule Engine exception = " + e.getMessage() + " (" + e + ")";
      // Added StackTrace message for debug -- By Billy 07May2002
      msg += StringUtil.stack2string(e);

      logger.error(msg);

      if (diagnosticToClient)
        pm.addMsg(msg, criticalValue);

      logger.error(e);

      //String rulename = (ruleName != null) ? ruleName : rulePrefix;

      //--Release2.1--//
      //Convert Error message to multilingual -- By Billy 14Nov2002
      //msg = "Business Rule " + rulename + " has failed.  For details, please contact your system administrator.";
      msg = BXStringTokenizer.replace(
        BXResources.getSysMsg("BRV_RULE_FAILED", languageId),
        "%s",
        ruleName);
      //===========================================================
      pm.addMsg(msg, criticalValue);

      if (diagnosticToClient)
          pm.addSeparator();
  }

  private void logActionProc(PassiveMessage pm, SysLogger logger, String actionProc, int criticality, boolean diagnosticToClient)
  {
      if (diagnosticToClient == false)
        return;

      if (actionProc == null)
      {
        /// pm.addMsg("Rule Action Proc: {not present}", criticality);
        return;
      }

      pm.addMsg("Rule Action Proc: {follows}", PassiveMessage.INFO);

      int ndx = 0;
      int end = 0;
      String remainder = null;

      for (;;)
      {
          ndx = actionProc.indexOf(";");

          remainder = null;

          end = actionProc.length() - 1;

          if (ndx != -1 && ndx != end)
          {
              remainder = actionProc.substring(ndx+1, end+1);
              actionProc = actionProc.substring(0,  ndx+1);
          }

          pm.addMsg(" " + actionProc, PassiveMessage.INFO);

          if (remainder == null)
            return;

          actionProc = remainder;
      }

  }

  /** #DG600
   * @param rr rule result with the br entry data
   * 
   * @return a formattable object to be used for localization
   */
  public static Formattable newBREntry(final String ruleName, final RuleResult rr) {
  	return new Formattable() {
  	public void formatTo(Formatter fmt, int flagas, int width, int precision) {
        		String message = null;
  			short optInd;   
  			StringBuffer sb = new StringBuffer();
  			try{
  				if(fmt.locale().getLanguage().equalsIgnoreCase(Locale.FRENCH.getLanguage()))
  					// Get french messages
       					optInd = 4;
  				else
  					// Get english messages -- default
       					optInd = 3;
    				message = rr.getString(optInd);
    				sb.append(message);
  			}
  			catch(Exception e) {}
  			
  			if (sb.length() <= 0)  			{
  				//message = "Business Rule " + ruleName + " has failed.  For details, please contact your system administrator.";
  				message = String.format(BXResources.getSysMsg("BRV_RULE_FAILED", fmt.locale()),ruleName);
  				//===========================================================
  			}
  			else
  				//message = message + "  (Rule " + ruleName + ")";
  				message = String.format(BXResources.getSysMsg("BRV_RULE_SUCCESS", fmt.locale()),ruleName);
  				sb.append(message);
  			
  			fmt.format("%s", sb.toString());
  		}
  	};
  }
  //#DG600 end 

}
