package com.basis100.deal.validation;

import java.util.*;
import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.BREngine.BRUtil;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.deal.conditions.*;
import com.basis100.deal.docprep.DocPrepLanguage;

public class BCBusinessRuleExecutor
{
  private static final String C_DEAL_PREFIX = "BC";
  private static final String C_APPLICANT_PREFIX = "BCA";
  private static final String C_PROPERTY_PREFIX = "BCP";
  private static final String C_PERCENT = "-%";
  private int currentBorrowerId ;
  private int currentPropertyId ;
  private Deal deal;
  private ArrayList retVals;
  private SessionResourceKit srk;
  private DealEntity parsingContext;
  private BusinessRule br;
  private String dealPrefix;
  private String applicantPrefix;
  private String propertyPrefix;

  public BCBusinessRuleExecutor(SessionResourceKit pSrk)
  {
    this.srk = pSrk;
    currentBorrowerId = -1;
    currentPropertyId = -1;
    deal = null;
    retVals = null;
    parsingContext = null;
    br = null;

    // set the default values
    dealPrefix = C_DEAL_PREFIX;
    applicantPrefix = C_APPLICANT_PREFIX;
    propertyPrefix = C_PROPERTY_PREFIX;
  }

  private void processRules(String rulePrefix)throws Exception
  {
    srk.setCloseJdbcConnectionOnRelease(true);
    srk.getJdbcExecutor().closeExecutorTemporary();

    // Added StackTrack Message dump for debug -- By Billy 07May2002
    try{
      // set the standard BRE (instance) variables ...
      br.reQuery(-1, -1, "na", rulePrefix, srk.getExpressState().getDealInstitutionId());
      // set global scope variables
      br.setGlobal("CurrentBorrowerId", new Integer(currentBorrowerId));
      br.setGlobal("CurrentPropertyId", new Integer(currentPropertyId));

      while( br.next() ){
        RuleResult rr = br.evaluate();
        if(rr.ruleStatus){
          this.createConditionText(br.ruleEntity.ruleName);
        }
      }// end while
    }
    catch(Exception e)
    {
      srk.getSysLogger().error(StringUtil.stack2string(e));
      throw e;
    }
  }

  private void createConditionText(String pRuleName)throws Exception
  {

    String condIdAsStr = StringUtil.extractTrailingNumbers(pRuleName);
    if(condIdAsStr == null){return;}
    int id ;
    try{
      id = Integer.parseInt(condIdAsStr);
    }catch(Exception exc){
      throw new Exception("Condition id could not be extracted as required. Source string:" + condIdAsStr);
    }

    Condition cond = new Condition(srk,id);
    if( !cond.isActiveType() ){return;}
    ConditionParser parser = new ConditionParser();
    int lang = DocPrepLanguage.getInstance().findPreferredLanguageId(deal,srk);
    String parsedConditionText = parser.parseText( cond.getConditionTextByLanguage(lang),lang,parsingContext,srk );

    ConditionContainer container = new ConditionContainer(id,parsedConditionText);
    //retVals.add(parsedConditionText);
    retVals.add(container);

  }

  private void setPropertyId(int pId){
    this.currentPropertyId = pId;
  }

  private void setBorrowerId(int pId){
    this.currentBorrowerId = pId;
  }

  public Collection extractConditions(Deal pDeal)throws Exception
  {
    this.deal = pDeal;
    retVals = new java.util.ArrayList();

    if(this.deal == null){return retVals;}

    // added for ML, sep 25, 2007
    srk.getExpressState().setDealInstitutionId(pDeal.getInstitutionProfileId());

    br = new BusinessRule(srk, -1, -1, "na", "");
    
    br.setDealId(deal.getDealId(), deal.getCopyId());
    // #2242: Catherine, 23-Nov-05 --------- start ---------  
    br.setLenderName();
    BRUtil.debug("BCBusinessRuleExecutor: set LenderName done");
    // #2242: Catherine, 23-Nov-05 --------- end ---------  
    
    this.processDealConditions();
    this.processPropConditions();
    this.processBorrConditions();
    return retVals;
  }

  private void processPropConditions()throws Exception
  {
    Collection properties = deal.getProperties();
    Iterator it = properties.iterator();

    while(it.hasNext()){
      Property oneProperty = (Property) it.next();
      this.parsingContext = oneProperty;
      this.setPropertyId(oneProperty.getPropertyId());
      this.setBorrowerId(-1);
      this.processRules(propertyPrefix + C_PERCENT);
    }
  }

  private void processBorrConditions() throws Exception
  {
    Collection borrowers = deal.getBorrowers();
    Iterator it = borrowers.iterator();

    while(it.hasNext()){
      Borrower oneBorrower = (Borrower) it.next();
      this.parsingContext = oneBorrower;
      this.setBorrowerId(oneBorrower.getBorrowerId());
      this.setPropertyId(-1);
      this.processRules(applicantPrefix + C_PERCENT);
    }
  }

  private void processDealConditions()throws Exception
  {
    this.setBorrowerId(-1);
    this.setPropertyId(-1);
    this.parsingContext = this.deal;
    this.processRules(dealPrefix + C_PERCENT);
  }

  public void setDealPrefix(String pref){
    dealPrefix = pref;
  }

  public void setApplicantPrefix(String pref){
    applicantPrefix = pref;
  }

  public void setPropertyPrefix(String pref){
    propertyPrefix = pref;
  }

}


