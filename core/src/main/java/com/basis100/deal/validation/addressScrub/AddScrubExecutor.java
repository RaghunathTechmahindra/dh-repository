/**
 * <p>
 * Title: AddScrubExecutor.java
 * </p>
 * 
 * <p>
 * Description: New Version Address Executor. This class uses Datx Web
 * Service to scrub Address
 * </p>
 * 
 * <p>
 * Company: Filogix Inc.
 * </p>
 * 
 * @author Midori Aida
 * @version 2.0
 * <p>Changed to use datx WEB Service
 * 
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 */

//06/June/2007 SL bilingual deal notes project

package com.basis100.deal.validation.addressScrub;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.text.BreakIterator;
import java.util.*;

import MosSystem.Mc;

import com.basis100.deal.docrequest.DocumentRequest;
import com.basis100.deal.entity.Addr;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealNotes;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.Province;
import com.basis100.deal.pk.DealPK;
import com.basis100.deal.security.PassiveMessage;
import com.basis100.deal.util.StringUtil;
import com.basis100.entity.CreateException;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;
import com.filogix.datx.addrscrub.payload.AddressType;
import com.filogix.externallinks.framework.ExternalServicesException;
import com.filogix.externallinks.framework.ServiceConst;
import com.filogix.externallinks.services.datx.DatxAddrScrubChannel;
import com.filogix.externallinks.services.datx.DatxAddrScrubRequestData;
import com.filogix.externallinks.services.datx.DatxAddrScrubResponseData;
import com.filogix.util.Xc;

import config.Config;


/**
 * <p> AddScrubExecutor.java </p>
 * <p> Address Scrub Executor scrub address by calling Datx Web Service for deal in DB. </p>
 * <p> ResourceManager must be initialized before using this class. </p>
 * <p> How to Call </p>
 * <p></p>
 * <p>SessionResourceKit srk = new SessionResourceKit("System");</p>
 * <p>AddScrubExecutor adrScruber = new AddScrubExecutor(srk);</p>
 * <p>PassiveMessage pm = new PassiveMessage();</p>
 * <p>pm.clearMessages();</p>
 * <p>pm = adrScruber.validateAddresses
 *        (deal, pm, AddScrubConstants.ADDRESS_SOURCE_INGESTION); </p>
 * <p></p>
 */
public class AddScrubExecutor implements Xc
{
	public static Map ProvincePossibleMap = null;
	private static boolean UNUSED_BOOLEAN = true;
	private static final String NL = System.getProperty("line.separator");
	private DatxAddrScrubRequestData req = null; // requester
	private SessionResourceKit dbsrk;
	private SysLogger logger;
	private JdbcExecutor jExec;
	private Deal deal;
	private AddScrubConfig aAdrScrbConfig; // #DG372 this is the current
	// addrScrbConfig in process
	//#DG600 private int lang = Mc.LANGUAGE_PREFERENCE_ENGLISH; // #DG376 add localization
	private Locale locala; // #DG376 add localization 

	/**
	 * Constructor
	 */
	public AddScrubExecutor(SessionResourceKit srk) throws Exception
	{
		super();
		setSessionResourceKit(srk);
		aAdrScrbConfig = new AddScrubConfig(srk);
		if (ProvincePossibleMap == null)
			initProvinceAbbrMap(srk);
	}

	/**
	 * builds the object setting logger, DB connection and DB executor
	 * 
	 * @param dbsrk
	 *          SessionResourceKit
	 */
	public void setSessionResourceKit(SessionResourceKit dbsrk)
	{
		this.dbsrk = dbsrk;
		logger = dbsrk.getSysLogger();
		jExec = dbsrk.getJdbcExecutor();
	}

	public void initProvinceAbbrMap(SessionResourceKit srk)
	throws RemoteException, FinderException, ExternalServicesException, Exception
	{
		ProvincePossibleMap = new HashMap();
		Province province = new Province(srk);
		List allProvinces = province.getAllProvince();
		Iterator ittr = allProvinces.iterator();
		while(ittr.hasNext())
		{
			Province p = (Province)ittr.next();
			// ON
			ProvincePossibleMap.put(p.getProvinceAbbreviation(), p);
			// on
			ProvincePossibleMap.put(p.getProvinceAbbreviation().toLowerCase(), p);
			// Ontario
			ProvincePossibleMap.put(p.getProvinceName(), p);
			// ontario
			ProvincePossibleMap.put(p.getProvinceName().toLowerCase(), p);
			// e.g. ONTARIO
			ProvincePossibleMap.put(p.getProvinceName().toUpperCase(), p);
			// what else,,,, Ont.?
		}
	}

	/**
	 * Method to Validate the address(es) on the deal based on the
	 * configurations in AddScrubConfig table.
	 * 
	 * @param deal
	 *          Deal
	 * @param pasMsg
	 *          PassiveMessage
	 * @param sourceId
	 *          int (0 - Ingestion, 1 - Screen)
	 * @return PassiveMessage
	 * @throws Exception
	 */
	public PassiveMessage validateAddresses(Deal deal,
			PassiveMessage pasMsg,
			int sourceId) throws Exception
			{
		this.deal = deal;
		// --> Get all AddScrubConfig collections
		// ADDRESS_SOURCE_INGESTION
		// ADDRESS_SOURCE_SCREEN
		Vector laASConfigs = aAdrScrbConfig.findByAddressSourceId(sourceId);
		int adtyp;
		String lsdealid = Integer.toString(deal.getDealId());
		String lscopyid = Integer.toString(deal.getCopyId());
		int query;
		Property theProp = new Property(dbsrk,
				deal.dcm);
		String sqlBorAdr = "Select a.Addrid,b.LANGUAGEPREFERENCEID,  b.BORROWERID, "
			+ " ba.BORROWERADDRESSTYPEid "
			+ " from Borrower b, BorrowerAddress ba, addr a "
			+ "where b.dealId = "
			+ lsdealid
			+ " AND b.copyId = "
			+ lscopyid
			+ " AND b.primaryBorrowerFlag = ? "
			+ " AND ba.BorrowerId = b.BorrowerId "
			+ " AND ba.copyId = "
			+ lscopyid
			+ " AND ba.BORROWERADDRESSTYPEid = ? "
			+ " AND a.Addrid = ba.Addrid "
			+ " AND a.copyId = "
			+ lscopyid;
		PreparedStatement psqlBorAdr = jExec.getPreparedStatement(sqlBorAdr);
		// #DG314 check also employm. addresses
		String sqlEmpAdr = "Select a.Addrid,b.LANGUAGEPREFERENCEID, b.BORROWERID "
			+ " from Borrower b, EmploymentHistory eh, contact c, addr a "
			+ " where b.dealId = "
			+ lsdealid
			+ " AND b.copyId = "
			+ lscopyid
			+ " AND eh.BorrowerId = b.BorrowerId "
			+ " AND eh.copyId = "
			+ lscopyid
			+ " and eh.EmploymentHistoryStatusId = 0"
			+ " AND c.contactid = eh.contactid"
			+ " AND c.copyId = "
			+ lscopyid
			+ " AND a.Addrid = c.Addrid "
			+ " AND a.copyId = "
			+ lscopyid;
		PreparedStatement psqlEmpAdr = jExec.getPreparedStatement(sqlEmpAdr);
		for (int li = 0; li < laASConfigs.size(); li++)
		{
			aAdrScrbConfig = ((AddScrubConfig) (laASConfigs.elementAt(li)));
			adtyp = aAdrScrbConfig.getAddressTypeId();
			switch (adtyp)
			{
			case AddScrubConstants.ADDRESS_TYPE_PRIM_BORROWER_CURR :
			case AddScrubConstants.ADDRESS_TYPE_PRIM_BORROWER_PREV :
			case AddScrubConstants.ADDRESS_TYPE_CO_BORROWER_CURR :
			case AddScrubConstants.ADDRESS_TYPE_CO_BORROWER_PREV : {
				boolean lbPrim = adtyp == AddScrubConstants.ADDRESS_TYPE_PRIM_BORROWER_CURR
				|| adtyp == AddScrubConstants.ADDRESS_TYPE_PRIM_BORROWER_PREV;
				boolean lbCur = adtyp == AddScrubConstants.ADDRESS_TYPE_PRIM_BORROWER_CURR
				|| adtyp == AddScrubConstants.ADDRESS_TYPE_CO_BORROWER_CURR;
				/*
				 * #DG 312 optimize? sqla = "Select a.*,b.LANGUAGEPREFERENCEID from
				 * Borrower b, BorrowerAddress ba, addr a " + "where b.dealId = " +
				 * lsdealid + " AND b.copyId = " + lscopyid + " AND
				 * b.primaryBorrowerFlag = " + DBA.sqlStringFrom(lbPrim) + " AND
				 * ba.BorrowerId = b.BorrowerId " + " AND ba.copyId = b.copyId " + "
				 * AND ba.BORROWERADDRESSTYPEid = " + (lbCur ?
				 * Mc.BT_ADDR_TYPE_CURRENT : Mc.BT_ADDR_TYPE_PREVIOUS) + " AND
				 * a.Addrid = ba.Addrid " + " AND a.copyId = ba.copyId ";
				 */
				psqlBorAdr.setString(1, lbPrim ? "Y" : "N");
				psqlBorAdr.setInt(2, lbCur
						? Mc.BT_ADDR_TYPE_CURRENT
								: Mc.BT_ADDR_TYPE_PREVIOUS);
				logger.debug("AddScrubExecutor.validateAddresses-BORROWER " + (lbCur?"Current":"Previous")+" Address:"
						+ sqlBorAdr);
				query = jExec.executePreparedStatement(psqlBorAdr,
						sqlBorAdr);
				// #DG312 query = jExec.execute(sqla);
				while (jExec.next(query))
				{
					try
					{
						procAddr(pasMsg, query, adtyp);
					}
					catch (Exception ex)
					{
						// catch (NoSuchMethodError loex) { // datx failed
						sendAlert(ex, buildMsgHead(query, false), pasMsg); //#DG600
					}
				}
				jExec.closeData(query);
				break;
			}
			case AddScrubConstants.ADDRESS_TYPE_EMPLOYMENT : { // #DG314 check
				// employment
				logger.debug("AddScrubExecutor.validateAddresses - EMPLOYER:"
						+ sqlEmpAdr);
				query = jExec.executePreparedStatement(psqlEmpAdr,
						sqlEmpAdr);
				while (jExec.next(query))
				{
					try
					{
						procAddr(pasMsg, query, adtyp);
					}
					catch (Exception ex)
					{
						sendAlert(ex, buildMsgHead(query, false), pasMsg);	//#DG600
					}
				}
				jExec.closeData(query);
				break;
			}
			case AddScrubConstants.ADDRESS_TYPE_PROPERTY : {
				logger.debug("AddScrubExecutor.validateAddresses - PROPERTY:" );
				try
				{
					theProp = theProp.findByPrimaryProperty(deal.getDealId(),
							deal.getCopyId(), dbsrk.getExpressState().getDealInstitutionId());
					procPropAddr(pasMsg, theProp);
				}
				catch (FinderException ex)
				{ // no property just skip it
				}
				catch (Exception ex)
				{
					// catch (NoSuchMethodError loex) { // datx failed
					// pasMsg.addMsg(ex.toString(), pasMsg.CRITICAL); //#DG314
					Formattable head = BXResources.newDPIGMsg(ADR_SCR_PROPADRS_NOT);	// #DG376//#DG600 
					sendAlert(ex, head, pasMsg);
				}
				break;
			}
			default :
				break;
			}
		}
		return pasMsg;
			}

	/**
	 * prepare and send alert message when in error
	 * 
	 * @param ex
	 *          Exception
	 * @param noteHeader
	 *          String
	 * @param pasMsg
	 *          PassiveMessage
	 * @throws Exception
	 */
	private void sendAlert(Exception ex,
			Formattable noteHeader,
			PassiveMessage pasMsg) throws Exception
			{
		// #DG376 rearranged
		// log detailed error
		//String lsHead =noteHeader+ "- "; // #DG376 add localization
		String lsHead =  
			String.format(Locale.ENGLISH, "%s- ", noteHeader); //#DG600
		String msg;
		String trace = StringUtil.stack2string(ex);
		logger.debug(lsHead+ trace);
		// split err message
		String envirId = BXResources.getPickListDescription(deal.getInstitutionProfileId(), PKL_LENDER_PROFILE,
				deal.getLenderProfileId(),Locale.ENGLISH);
		// prepare and send alert message
		msg = "Date/Time:\t\t"+ new java.util.Date()+ NL
		+ "Lender:\t\t"+ envirId+ NL
		+ "Deal #:\t\t"+ deal.getDealId()+ NL
		+ NL
		+ "Error:\t\t"+ lsHead+ NL
		+ trace+ NL;
		String defEmail = PropertiesCache.getInstance()
		                  .getProperty(dbsrk.getExpressState().getDealInstitutionId(),COM_BASIS100_SYSMAIL_TO, FXSUPPORT_EMAIL);
		if (msg.length() > 4000) // truncate to size of column below
			msg = msg.substring(0, 4000 - 5);
		DocumentRequest.requestAlertEmail(dbsrk, deal, defEmail, envirId
				+ "-Address Scrubbing", msg);

		// alert doesn't need to be in DealNotes: Jan24, 2007 Midori     
		//    updDealNotes(ls); // #DG372
		if (aAdrScrbConfig.isIsCritical()) 
			pasMsg.addMsg(msg, PassiveMessage.CRITICAL);
			}

	/**
	 * Prepare the service call, call the service and prepare the response
	 * object
	 * 
	 * @throws Exception
	 * @return AddressScrubResponseBean
	 */
	private DatxAddrScrubResponseData procReq() throws Exception
	{
		// Get the message sender to send the request as XML string
		DatxAddrScrubChannel requestSender = new DatxAddrScrubChannel();
		requestSender.init(dbsrk, ProvincePossibleMap, deal.getDealId(), deal.getCopyId(), "System"  );
		String reqXML = requestSender.createRequest(req);
		// logger.debug("REQUEST:" + reqXML);
		Object resObj = requestSender.sendData(reqXML);
		DatxAddrScrubResponseData resData = (DatxAddrScrubResponseData)requestSender.createResponse(resObj);
		return resData;
	}

	/**
	 * Set the scrub object with the Addr data, call the service, change
	 * (optionally) the Addr record and issue message
	 * 
	 * @param pasMsg
	 *          PassiveMessage
	 * @param query
	 *          int
	 * @throws Exception
	 */
	public void procAddr(PassiveMessage pasMsg, int query, int addressType) throws Exception 
	{
		Addr adr = new Addr(dbsrk, jExec.getInt(query, "Addrid"),deal.getCopyId());
		//bilingual deal notes, use locale instead of language
		int prov = adr.getProvinceId(); // #DG376 add localization
		if (prov == Mc.PROVINCE_QUEBEC)
			locala = BXResources.getLocale(Mc.LANGUAGE_PREFERENCE_FRENCH); // #DG600
		else
			locala = BXResources.getLocale(Mc.LANGUAGE_PREFERENCE_ENGLISH); // #DG600

		boolean parsed = false;
		if (addressType == AddScrubConstants.ADDRESS_TYPE_EMPLOYMENT
				|| addressType == AddScrubConstants.ADDRESS_TYPE_PRIM_BORROWER_PREV
				|| addressType == AddScrubConstants.ADDRESS_TYPE_CO_BORROWER_PREV)
		{
			req = DatxAddrScrubRequestData.create(dbsrk, adr, ProvincePossibleMap, AddScrubConstants.UNPARSED);
		}
		else
		{
			req = DatxAddrScrubRequestData.create(dbsrk, adr, ProvincePossibleMap, AddScrubConstants.PARSED);
			parsed = true;
		}
		if (req != null)
			req.setDealId(deal.getDealId());
		logger.debug("AdrScrub: AddressType = " + addressType + ":" 
				+ AddScrubConstants.AddressTypes.get(addressType));

		// ---------------------------------------

		DatxAddrScrubResponseData loRep = procReq();   // <<<<<<<<<<<<<<<<<<<<<<

		// ---------------------------------------

		// bilingual deal notes
		String message = null;
		FmtblNoteList noteA = new FmtblNoteList();

		// if  Input address doesn't meet criteria, then do nothing.
		if (loRep != null && loRep.getResultCode() == AddScrubConstants.RESULT_CODE_NOT_SEND)
			return;

		// communication error with WS Server occured
		if (loRep == null || loRep.getResultCode() == ServiceConst.RESULT_CODE_SYSTEM_ERROR 
				|| ((loRep.getResultCode() != ServiceConst.RESULT_CODE_PROCESSING_ERROR
						&& loRep.getResultCode() != AddScrubConstants.RESULT_CODE_SUGGESTIONS
						&& loRep.getResultCode() != AddScrubConstants.RESULT_CODE_FOUND_MATCH)))
		{
			//bilingual deal notes
			noteA.add(buildMsgHead(query, false));
			noteA.add(BXResources.newResFormata(BXResources.SYS_MSG_PATH, EXTERNAL_LINK_SYSTEM_ERROR));
			updDealNotes(noteA);    // #DG372

			if (aAdrScrbConfig.isIsCritical())
			{
				message = String.format(locala, "%s", noteA);
				pasMsg.addMsg(message, PassiveMessage.CRITICAL, noteA);		//#DG600
			}

			if (loRep == null)
				throw new ExternalServicesException("Address Scrub response is null");
			if (loRep.getResultCode() == ServiceConst.RESULT_CODE_SYSTEM_ERROR)
				throw new ExternalServicesException(loRep.getResultDesc());
			throw new ExternalServicesException("Address Scrub response has unexpected Result Code: "
					+ loRep.getResultCode() + NL 
					+ "Result Desc: " + loRep.getResultDesc() + NL);      
		}

		int resultCode = loRep.getResultCode();
		String resultDesc = loRep.getResultDesc();

		// if return address is error
		if (resultCode == ServiceConst.RESULT_CODE_PROCESSING_ERROR)
		{
			noteA.add(buildMsgHead(query, false));
			noteA.add(BXResources.newDPIGMsg(ADR_SCR_ORIGINAL));
			noteA.add(BXResources.newStrFormata(getConcatAddress(adr, false, parsed)));
			noteA.add(BXResources.newStrFormata(resultDesc));
			updDealNotes(noteA);    // #DG372

			if (aAdrScrbConfig.isIsCritical())
			{
				message = String.format(locala, "%s", noteA);
				pasMsg.addMsg(message, PassiveMessage.CRITICAL, noteA);		//#DG600
			}
			return;
		}

		if (resultCode == AddScrubConstants.RESULT_CODE_SUGGESTIONS)
		{
			noteA.add(buildMsgHead(query, false));
			noteA.add(BXResources.newDPIGMsg(ADR_SCR_ORIGINAL));
			noteA.add(BXResources.newStrFormata(getConcatAddress(adr, false, parsed)));
			buildSugList(loRep, noteA);
		    updDealNotes(noteA);    // #DG372
		    if (aAdrScrbConfig.isIsCritical()) {
				message = String.format(locala, "%s", noteA);
				pasMsg.addMsg(message, PassiveMessage.CRITICAL, noteA);		//#DG600
			}
			return;
		}
		//resultCode == RESULT_CODE_FOUND_MATCH 

		noteA.add(buildMsgHead(query, true));
		noteA.add(BXResources.newDPIGMsg(ADR_SCR_ORIGINAL));

		//  note partB #DG376 add localization
		FmtblNoteList noteB = new FmtblNoteList();
		noteB.add(BXResources.newDPIGMsg(ADR_SCR_OUTPUT)); 

		//#DG600 refactored
		//boolean isDataChanged = false;   // #DG376

		if (addressType == AddScrubConstants.ADDRESS_TYPE_EMPLOYMENT
				|| addressType == AddScrubConstants.ADDRESS_TYPE_PRIM_BORROWER_PREV
				|| addressType == AddScrubConstants.ADDRESS_TYPE_CO_BORROWER_PREV)
			// if it's true, then update AddressLine1, 2, StreetName,,
		{
			updAdr(adr, loRep, noteA, noteB);	     
		}
		else
		{ 
			updAdrNot(adr, loRep, noteA, noteB);
		}
		//#DG600 end
	}

	private void updAdr(Addr adr, DatxAddrScrubResponseData loRep, 
			FmtblNoteList noteA, FmtblNoteList noteB)
	throws Exception {

		String addrSep = "";// #DG376 BXResources.getDocPrepIngMsg("ADDRESS_LINE_1",borLang);    
		boolean isDataChanged = false;   
		StringBuffer lAdrLine = new StringBuffer();
		lAdrLine.append(loRep.getConcatAddress
				(loRep.getMatch(), AddScrubConstants.CONCAT_INCLUDES_LINE2, UNUSED_BOOLEAN));

		// generate addrLine1, addrLine2 from response
		String[] addrLins = adr.torncatAddress(lAdrLine.toString().trim());
		String addrLine1 = addrLins[0];
		String addrLine2 = addrLins[1];

		addrLine1 = setUpdate(addrLine1, adr.getAddressLine1(), 
				addrSep, noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(addrLine1)) 
		{
			if(aAdrScrbConfig.streetAddressLine())
				adr.setAddressLine1(addrLine1);
			isDataChanged = true;
		}
		addrLine2 = setUpdate(addrLine2, adr.getAddressLine2(), 
				addrSep, noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(addrLine2))
		{
			if(aAdrScrbConfig.streetAddressLine()) adr.setAddressLine2(addrLine2);
			isDataChanged = true;          
		}
		boolean fromMorty = adr.getCity()==null;

		String city = loRep.getCity();
		addrSep = ", ";// #DG376 BXResources.getDocPrepIngMsg("CITY",borLang);
		city = setUpdate(city, adr.getCity(), addrSep, noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(city))
		{
			if (aAdrScrbConfig.isCity()) adr.setCity(city);
			isDataChanged = true;
		}

		addrSep = " ";// #DG376 BXResources.getDocPrepIngMsg("PROVINCE",borLang);

//		logger.debug("REPObj: Province = " + loRep.getProvince());
//		if (adr.getProvinceId() > 0)
//		{
		String oldprovinceShortName = BXResources.getPickListDescription
			(dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME, adr.getProvinceId(), locala );
		String province = loRep.getProvince();
		if (fromMorty)
			province = setUpdate(province, oldprovinceShortName, addrSep, null, noteB, UNUSED_BOOLEAN);
		else
			province = setUpdate(province, oldprovinceShortName, addrSep, noteA, noteB, UNUSED_BOOLEAN);

		if (!isEmpty(province))
		{
			try
			{
				if( aAdrScrbConfig.isProvince()) 
					adr.setProvinceId
					(Integer.parseInt(BXResources.getPickListDescriptionId
					  (dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME, province, locala)));
				isDataChanged = true;
			}
			catch (NumberFormatException ex) {
				logger.debug("adr.setProvinceId failed:"+ ex);
			}
		}
//		}

//		logger.debug("REPObj: PostalCode = " + loRep.getPostalCode());
		String postalCode = loRep.getPostalCode();
		// if postalCode length > 6, i.e. includes space 
		if (postalCode != null && postalCode.trim().length() > 6 )
		{
			StringTokenizer tokens = new StringTokenizer(postalCode, " ");
			postalCode = tokens.nextToken() + tokens.nextToken();
		}
		postalCode = setUpdate(postalCode, StringUtil.concat
				(adr.getPostalFSA(), adr.getPostalLDU()).toString(), 
				addrSep, noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(postalCode)) 
		{
			if(aAdrScrbConfig.isPostalCode())
			{
				adr.setPostalFSA(postalCode.substring(0,3));
				adr.setPostalLDU(postalCode.substring(3));
			}
			isDataChanged = true;
		}

		if (isDataChanged)
		{
			if( aAdrScrbConfig.streetAddressLine() 
					|| aAdrScrbConfig.isCity() 
					|| aAdrScrbConfig.isPostalCode() 
					|| aAdrScrbConfig.isProvince()
					|| aAdrScrbConfig.isPostalCode())
				adr.ejbStore();

			// bilingual deal notes
			noteA.add(BXResources.newStrFormata(NL));
			noteA.add(noteB);
			//logger.debug("added dealNotes"+ String.format(locala, "%s", noteA));
			updDealNotes(noteA);    // #DG372
			// pasMsg.addMsg(theNote, isCritical);
		}
	}

	private void updAdrNot(Addr adr, DatxAddrScrubResponseData loRep, FmtblNoteList noteA, 
			FmtblNoteList noteB) 
	throws Exception {

		boolean isDataChanged = false;   
		String addrSep;
		String unitNumber = loRep.getUnitNumber() != null ? loRep.getUnitNumber():"";
		String streetNumber = loRep.getStreetNumber() != null ? loRep.getStreetNumber():"";
		String streetName = loRep.getStreetName() != null ? loRep.getStreetName():"";
		String streetType = loRep.getStreetType() != null ? loRep.getStreetType(): "";
		String streetDirection = loRep.getStreetDirection() != null ? loRep.getStreetDirection(): "";
		addrSep="";
		unitNumber = setUpdate(unitNumber, adr.getUnitNumber(), addrSep, 
				noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(adr.getUnitNumber()))
		{
			noteA.add(BXResources.newStrFormata("-"));
		}
		if (!isEmpty(loRep.getUnitNumber()))
		{
			noteB.add(BXResources.newStrFormata("-"));        
		}
		if (!isEmpty(unitNumber))
		{
			if (aAdrScrbConfig.streetAddressLine()) adr.setUnitNumber(unitNumber);
			isDataChanged = true;
		}

		addrSep="";
		streetNumber = setUpdate(streetNumber, adr.getStreetNumber(), addrSep, 
				noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(streetNumber))
		{
			if (aAdrScrbConfig.streetAddressLine()) adr.setStreetNumber(streetNumber);
			isDataChanged = true;          
		}

		addrSep=" ";
		if (locala.getLanguage().equalsIgnoreCase(Locale.FRENCH.getLanguage()))
		{
			String oldStreetType = BXResources.getPickListDescription
			(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, adr.getStreetTypeId(), locala );
			streetType = setUpdate(streetType, oldStreetType, addrSep, 
					noteA, noteB, UNUSED_BOOLEAN);
			if (!isEmpty(streetType))
			{
				if (aAdrScrbConfig.streetAddressLine()) 
					adr.setStreetTypeId(Integer.parseInt
							(BXResources.getPickListDescriptionId
							 (dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, streetType, locala)));
				isDataChanged = true;          
			}
		}

		streetName = setUpdate(streetName, adr.getStreetName(), addrSep, 
				noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(streetName))
		{
			adr.setStreetName(streetName);
			isDataChanged = true;          
		}
		if (!locala.getLanguage().equalsIgnoreCase(Locale.FRENCH.getLanguage()))
		{
			String oldStreetType = BXResources.getPickListDescription
				(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, adr.getStreetTypeId(), locala );
			streetType = setUpdate(streetType, oldStreetType, addrSep, 
					noteA, noteB, UNUSED_BOOLEAN);
			if (!isEmpty(streetType))
			{
				if(aAdrScrbConfig.streetAddressLine())
					adr.setStreetTypeId(Integer.parseInt
							(BXResources.getPickListDescriptionId
									(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, streetType, locala)));
				isDataChanged = true;          
			}
		}

		if(adr.getStreetDirectionId()>0)
		{
			String oldStreetDirection = BXResources.getPickListDescription
			(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_DIRECTION, adr.getStreetDirectionId(), locala );

			if (streetDirection != null)
			{
				String srteerTypeIdStr = BXResources.getPickListDescriptionId
				(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_DIRECTION, streetDirection, locala);
				if (srteerTypeIdStr != null)
				{
					streetDirection = setUpdate(streetDirection, oldStreetDirection, addrSep, 
							noteA, noteB, UNUSED_BOOLEAN);
					adr.setStreetDirectionId(Integer.parseInt(srteerTypeIdStr));
					isDataChanged = true;
				}
			}
		}
		addrSep = ", ";// #DG376
		// BXResources.getDocPrepIngMsg("ADDRESS_LINE_2",borLang);

//		logger.debug("REPObj: City = " + loRep.getCity());
		boolean fromMorty = adr.getCity() == null;
		String city = loRep.getCity();
		addrSep = " ";// #DG376 BXResources.getDocPrepIngMsg("CITY",borLang);
		city = setUpdate(city, adr.getCity(), addrSep, noteA, noteB, UNUSED_BOOLEAN);
		if (city != null)
		{
			if (aAdrScrbConfig.isCity()) adr.setCity(city);
			isDataChanged = true;
		}

		addrSep = " ";// #DG376
//		logger.debug("REPObj: Province = " + loRep.getProvince());
//		if (adr.getProvinceId() > 0)
//		{
		String oldprovinceShortName = BXResources.getPickListDescription
		(dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME, adr.getProvinceId(), locala );
		String province = loRep.getProvince();
		addrSep = " ";// #DG376 BXResources.getDocPrepIngMsg("PROVINCE",borLang);
		if (fromMorty) // province is other just don't add to original note
			province = setUpdate(province, oldprovinceShortName, addrSep, null, noteB, UNUSED_BOOLEAN);
		else
			province = setUpdate(province, oldprovinceShortName, addrSep, noteA, noteB, UNUSED_BOOLEAN);

		if (province != null )
		{
			try
			{
				if(aAdrScrbConfig.isProvince()) 
					adr.setProvinceId(Integer.parseInt(BXResources.getPickListDescriptionId
							(dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME, province, locala)));
				isDataChanged = true;
			}
			catch (NumberFormatException ex) {
				logger.debug("adr.setProvinceId failed:"+ ex);
			}
		}
		//}

		//logger.debug("REPObj: PostalCode = " + loRep.getPostalCode());
		String postalCode = loRep.getPostalCode();
		//if postalCode length > 6, i.e. includes space 
		if (postalCode != null && postalCode.trim().length() > 6 )
		{
			StringTokenizer tokens = new StringTokenizer(postalCode, " ");
			postalCode = tokens.nextToken() + tokens.nextToken();
		}
		postalCode = setUpdate(postalCode, StringUtil.concat
				(adr.getPostalFSA(), adr.getPostalLDU()).toString(), 
				addrSep, noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(postalCode))
		{
			if(aAdrScrbConfig.isPostalCode())
			{
				adr.setPostalFSA(postalCode.substring(0,3));
				adr.setPostalLDU(postalCode.substring(3));
			}
			isDataChanged = true;
		}

		if (isDataChanged) 
		{
			if( aAdrScrbConfig.streetAddressLine() 
					|| aAdrScrbConfig.isCity() 
					|| aAdrScrbConfig.isPostalCode() 
					|| aAdrScrbConfig.isProvince()
					|| aAdrScrbConfig.isPostalCode())
				adr.ejbStore();

			// bilingual deal notes
			noteA.add(BXResources.newStrFormata(NL));
			noteA.add(noteB);
			//logger.debug("added dealNotes"+ String.format(locala, "%s", noteA));
			updDealNotes(noteA);    // #DG372
			// pasMsg.addMsg(theNote, isCritical);
		}
	}


	/**
	 * build the message header depending on the type of address
	 * 
	 * @param query
	 *          int
	 * @return StringBuffer
	 * @throws FinderException
	 * @throws RemoteException
	 * @throws Exception
	 */
	// bilingual deal notes project
	private Formattable buildMsgHead(int query, boolean isAssigned)
	throws FinderException,
	RemoteException,
	Exception
	{
		Borrower bor = new Borrower(dbsrk,
				deal.dcm,
				jExec.getInt(query,
						"BORROWERID"),
						deal.getCopyId());
		//  #DG600 rewritten 

		/*StringBuffer sb = new StringBuffer(512);
    sb.append(bor.getBorrowerFirstName());
//    String midName = bor.getBorrowerMiddleInitial();
//    if (midName != null
//        && (!midName.trim().equals(""))) sb.append(' ')
//        .append(midName)
//        .append('.');
    sb.append(' ').append(bor.getBorrowerLastName()).append(" ");    
    vf.add(BXResources.newStrFormata(sb.toString()));*/

		String transLabel;
		switch (aAdrScrbConfig.getAddressTypeId())
		{
			case AddScrubConstants.ADDRESS_TYPE_EMPLOYMENT :
				transLabel = isAssigned? ADR_SCR_EMPLOYM : ADR_SCR_EMPLOYM_NOT;
				break;
			default :
				int typeId = jExec.getInt(query, "borrowerAddressTypeId");
				if (typeId == Mc.BT_ADDR_TYPE_CURRENT)
				{
					transLabel = isAssigned? ADR_SCR_ADR_BORCUR : ADR_SCR_ADR_BORCUR_NOT;
				}
				else
				{          
					transLabel = isAssigned? ADR_SCR_ADR_BORPREV : ADR_SCR_ADR_BORPREV_NOT;
				}
		}
		return BXResources.newFormata(transLabel, bor.getBorrowerFirstName(), bor.getBorrowerLastName()); 
	}

	/**
	 * Set the scrub object with the Property data, call the service, change
	 * (optionally) the Property record and issue message
	 * 
	 * @param pasMsg
	 *          PassiveMessage
	 * @param prop
	 *          Property
	 * @throws Exception
	 */
	private void procPropAddr(PassiveMessage pasMsg, Property prop)
	throws Exception
	{
		int prov = prop.getProvinceId(); // #DG376 add localization
		if (prov == Mc.PROVINCE_OTHER) // if other, forget it
			return;
		if (prov == Mc.PROVINCE_QUEBEC) 
			locala = BXResources.getLocale(Mc.LANGUAGE_PREFERENCE_FRENCH); // #DG600
		else
			locala = BXResources.getLocale(Mc.LANGUAGE_PREFERENCE_ENGLISH); // #DG600

		req = new DatxAddrScrubRequestData(dbsrk, ProvincePossibleMap, AddScrubConstants.PARSED);
		setPropAddressToRequestData(req, prop);

		logger.debug("AdrScrub: AddressType = " + AddScrubConstants.ADDRESS_TYPE_PROPERTY + ":" 
				+ AddScrubConstants.AddressTypes.get(AddScrubConstants.ADDRESS_TYPE_PROPERTY));
		// ---------------------------------------
		DatxAddrScrubResponseData loRep = procReq(); // <<<<<<<<<<<<<<<<<<<<<<
		// ---------------------------------------

		// if  Input address doesn't meet criteria, then do nothing.
		if (loRep != null && loRep.getResultCode() == AddScrubConstants.RESULT_CODE_NOT_SEND)
			return;

		FmtblNoteList noteA = new FmtblNoteList();
		String message = null;

		if (loRep == null || loRep.getResultCode() == ServiceConst.RESULT_CODE_SYSTEM_ERROR
				|| (loRep.getResultCode() != ServiceConst.RESULT_CODE_PROCESSING_ERROR
						&& loRep.getResultCode() != AddScrubConstants.RESULT_CODE_SUGGESTIONS
						&& loRep.getResultCode() != AddScrubConstants.RESULT_CODE_FOUND_MATCH))
		{
			noteA.add(BXResources.newDPIGMsg(ADR_SCR_PROPADRS_NOT));
//		.append(BXResources.getSysMsg("EXTERNAL_LINK_SYSTEM_ERROR", Mc.LANGUAGE_PREFERENCE_ENGLISH));
			noteA.add(BXResources.newResFormata(BXResources.SYS_MSG_PATH, EXTERNAL_LINK_SYSTEM_ERROR));
			updDealNotes(noteA);    // #DG372

			if (aAdrScrbConfig.isIsCritical()){
				message = String.format(locala, "%s", noteA);
				pasMsg.addMsg(message, PassiveMessage.CRITICAL, noteA);		//#DG600
			}

			if (loRep == null)
				throw new ExternalServicesException("Address Scrub response is null");
			if (loRep.getResultCode() == ServiceConst.RESULT_CODE_SYSTEM_ERROR)
				throw new ExternalServicesException(loRep.getResultDesc());      
			throw new ExternalServicesException("Address Scrub response has unexpected Result Code: "
					+ loRep.getResultCode() + NL 
					+ "Result Desc: " + loRep.getResultDesc() + NL);      
		}

		int resultCode = loRep.getResultCode();    
		String resultDesc = loRep.getResultDesc();

		// if return address is error
		if (resultCode == ServiceConst.RESULT_CODE_PROCESSING_ERROR)
		{
			noteA.add(BXResources.newDPIGMsg(ADR_SCR_PROPADRS_NOT));
			noteA.add(BXResources.newDPIGMsg(ADR_SCR_ORIGINAL));
			noteA.add(BXResources.newStrFormata(getPropertyConcatAddress(prop)));
			noteA.add(BXResources.newStrFormata(loRep.getResultDesc()));

			updDealNotes(noteA);    // #DG372
			if (aAdrScrbConfig.isIsCritical()) {
				message = String.format(locala, "%s", noteA);
				pasMsg.addMsg(message, PassiveMessage.CRITICAL, noteA);		//#DG600
			}
			return;
		}

		if (resultCode == AddScrubConstants.RESULT_CODE_SUGGESTIONS)
		{
			noteA.add(BXResources.newDPIGMsg(ADR_SCR_PROPADRS_NOT));
			noteA.add(BXResources.newDPIGMsg(ADR_SCR_ORIGINAL));
			noteA.add(BXResources.newStrFormata(getPropertyConcatAddress(prop)));
			buildSugList(loRep, noteA);
			updDealNotes(noteA); // #DG372
			if (aAdrScrbConfig.isIsCritical()) {
				message = String.format(locala, "%s", noteA);
				pasMsg.addMsg(message, PassiveMessage.CRITICAL, noteA);		//#DG600
			}
			return;
		}

		noteA.add(BXResources.newDPIGMsg(ADR_SCR_PROPADRS));
		noteA.add(BXResources.newDPIGMsg(ADR_SCR_ORIGINAL));

		FmtblNoteList noteB = new FmtblNoteList();
		noteB.add(BXResources.newDPIGMsg(ADR_SCR_OUTPUT)); 

		String addrSep = "";
		boolean isDataChanged = false; // #DG376
		String unitNumber = loRep.getUnitNumber() != null ? loRep.getUnitNumber():"";
		String streetNumber = loRep.getStreetNumber() != null ? loRep.getStreetNumber():"";
		String streetName = loRep.getStreetName() != null ? loRep.getStreetName():"";
		String streetType = loRep.getStreetType() != null ? loRep.getStreetType(): "";
		String streetDirection = loRep.getStreetDirection() != null ? loRep.getStreetDirection(): "";

		unitNumber = setUpdate(unitNumber, prop.getUnitNumber(), addrSep, noteA, noteB, 
				UNUSED_BOOLEAN);
		if (!isEmpty(prop.getUnitNumber()))
		{
			noteA.add(BXResources.newStrFormata("-"));
		}
		if (!isEmpty(loRep.getUnitNumber()))
		{
			noteB.add(BXResources.newStrFormata("-"));
		}
		if (unitNumber != null)
		{
			if (aAdrScrbConfig.streetAddressLine()) 
				prop.setUnitNumber(unitNumber);
			isDataChanged = true;
		}
		streetNumber = setUpdate(streetNumber, prop.getPropertyStreetNumber(), addrSep, 
				noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(streetNumber))
		{
			if(aAdrScrbConfig.streetAddressLine()) 
				prop.setPropertyStreetNumber(streetNumber);
			isDataChanged = true;          
		}

		addrSep = " ";
		if (locala.getLanguage().equalsIgnoreCase(Locale.FRENCH.getLanguage()))
		{
			String oldStreetType = BXResources.getPickListDescription
			(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, prop.getStreetTypeId(), locala );
			streetType = setUpdate(streetType, oldStreetType, addrSep, noteA, noteB, 
					UNUSED_BOOLEAN);
			if (!isEmpty(streetType))
			{
				if(aAdrScrbConfig.streetAddressLine())
					prop.setStreetTypeId(Integer.parseInt(BXResources.getPickListDescriptionId
							(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, 
									streetType, locala)));
				isDataChanged = true;          
			}
		}

		streetName = setUpdate(streetName, prop.getPropertyStreetName(), addrSep, 
				noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(streetName))
		{
			if(aAdrScrbConfig.streetAddressLine())
				prop.setPropertyStreetName(streetName);
			isDataChanged = true;          
		}

		if (!locala.getLanguage().equalsIgnoreCase(Locale.FRENCH.getLanguage()))
		{
			String oldStreetType = BXResources.getPickListDescription
			(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, prop.getStreetTypeId(), locala );
			streetType = setUpdate(streetType, oldStreetType, addrSep, noteA, noteB, 
					UNUSED_BOOLEAN);
			if (!isEmpty(streetType))
			{
				if(aAdrScrbConfig.streetAddressLine())
					prop.setStreetTypeId
					(Integer.parseInt(BXResources.getPickListDescriptionId
							(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, 
									streetType, locala)));
				isDataChanged = true;          
			}
		}

		String oldStreetDirection 
		= BXResources.getPickListDescription
		(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_DIRECTION, prop.getStreetDirectionId(), locala );
		streetDirection = setUpdate(streetDirection, oldStreetDirection, addrSep, 
				noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(streetDirection))
		{
			if(aAdrScrbConfig.streetAddressLine())
				prop.setStreetDirectionId
				(Integer.parseInt
						(BXResources.getPickListDescriptionId
								(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_DIRECTION, 
										streetDirection, locala)));
			isDataChanged = true;          
		}

		addrSep = ", ";// #DG376
		// BXResources.getDocPrepIngMsg("ADDRESS_LINE_2",borLang);

		//logger.debug("REPObj: City = " + loRep.getCity());
		String city = loRep.getCity();
		city = setUpdate(city, prop.getPropertyCity(), addrSep, noteA, noteB, 
				UNUSED_BOOLEAN);
		if (!isEmpty(city))
		{
			if(aAdrScrbConfig.isCity()) prop.setPropertyCity(city);
			isDataChanged = true;
		}

		String oldprovinceShortName = BXResources.getPickListDescription
		(dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME, prop.getProvinceId(), locala );
		String province = loRep.getProvince();
		province = setUpdate(province, oldprovinceShortName, addrSep, noteA, noteB, 
				UNUSED_BOOLEAN);
		if (!isEmpty(province))
		{
			try
			{
				if(aAdrScrbConfig.isProvince())
					prop.setProvinceId(Integer.parseInt(
							BXResources.getPickListDescriptionId
							(dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME, province, locala)));
				isDataChanged = true;
			}
			catch (NumberFormatException ex)
			{
				logger.debug("adr.setProvinceId failed:"+ ex);
			}
		}

		addrSep = " ";// #DG376
		String postalCode = loRep.getPostalCode();
		// if postalCode length > 6, i.e. includes space 
		if (postalCode != null && postalCode.trim().length() > 6 )
		{
			StringTokenizer tokens = new StringTokenizer(postalCode, " ");
			postalCode = tokens.nextToken() + tokens.nextToken();
		}
		postalCode = setUpdate(postalCode, StringUtil.concat(prop.getPropertyPostalFSA(), 
				prop.getPropertyPostalLDU()).toString(),
				addrSep, noteA, noteB, UNUSED_BOOLEAN);
		if (!isEmpty(postalCode))
		{
			if(aAdrScrbConfig.isPostalCode())
			{
				prop.setPropertyPostalFSA(postalCode.substring(0,3));
				prop.setPropertyPostalLDU(postalCode.substring(3));
			}
			isDataChanged = true;
		}

		if (isDataChanged)
		{
			if( aAdrScrbConfig.streetAddressLine() 
					|| aAdrScrbConfig.isCity() 
					|| aAdrScrbConfig.isPostalCode() 
					|| aAdrScrbConfig.isProvince()
					|| aAdrScrbConfig.isPostalCode())
				prop.ejbStore();

			// Bilingual Deal Notes
			noteA.add(BXResources.newStrFormata(NL));
			noteA.add(noteB);
			//logger.debug("update adr:"+ String.format(BXResources.getLocale(lang), "%s", noteA));
			updDealNotes(noteA); // #DG372
			// pasMsg.addMsg(theNote, isCritical);
		}
	}

	private void setPropAddressToRequestData(DatxAddrScrubRequestData req, Property prop)
	{
		String unitNumber = prop.getUnitNumber();
		req.setUnitNumber(unitNumber == null
				? "" : unitNumber);
		String streetNumber = prop.getPropertyStreetNumber();
		req.setStreetNumber(streetNumber == null
				? "" : streetNumber);
		String streetName = prop.getPropertyStreetName();
		req.setStreetName(streetName == null
				? "" : streetName);

		String streetType = BXResources.getPickListDescription(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE,
				prop.getStreetTypeId(),
				locala);
		req.setStreetType(streetType == null
				? "" : streetType);

		String addressLine2 =  prop.getPropertyAddressLine2();
		req.setAddressLine2(addressLine2 == null
				? "" : addressLine2);
		String streetDirection = BXResources.getPickListDescription(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_DIRECTION,
				prop.getStreetDirectionId(),
				locala);
		req.setStreetDirection(streetDirection == null
				? "" : streetDirection);
		String postalCode = StringUtil.concat(prop.getPropertyPostalFSA(),
				prop.getPropertyPostalLDU()).toString();
		if (postalCode != null && postalCode.trim().length() == 6)
			req.setPostalCode(postalCode);

		String city = prop.getPropertyCity();
		req.setCity(city == null
				? "" : city);
		String province = BXResources.getPickListDescription(dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME,
				prop.getProvinceId(),
				locala);
		req.setProvince(province);
		req.setDealId(prop.getDealId());
	}

	/**
	 * build the suggestion list
	 * 
	 * @param loRep
	 *          AddressScrubResponseBean
	 * @param noteA
	 *          object to which the suggestion list is to be added 
	 */
	private void buildSugList(DatxAddrScrubResponseData loRep, FmtblNoteList noteA)
	{
		noteA.add(BXResources.newDPIGMsg(ADR_SCR_SUGLISTGEN));
		AddressType[] sugList; // #DG376 don't show if empty
		sugList = loRep.getSuggestionList();
		if (sugList != null && sugList.length <= 0 )
			return;

		int size = sugList.length;
		for( int i=0; i<size; i++)
		{
			AddressType address = sugList[i];
			// add concat address include address Line2
			noteA.add(BXResources.newStrFormata(loRep.getConcatAddress(address, true, false),NL));
		}
		noteA.add(BXResources.newStrFormata(NL));		//#DG600
	}


	/**
	 * Update deal notes directly - Category: INGESTION
	 * 
	 * @param theNote
	 *          String
	 * @throws CreateException
	 * @throws FinderException
	 * @throws RemoteException
	 */
	private void updDealNotes(FmtblNoteList theNoteList)
	throws CreateException,
	FinderException,
	RemoteException
	{
		DealNotes dealNote = new DealNotes(dbsrk);
		dealNote.create((DealPK) deal.getPk(),deal.getDealId(),
				Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION, 0,theNoteList);
	}

	// public methods to get addresses based on AddressType
	public void parseAddressLines(String source)
	{
		BreakIterator boundary = BreakIterator.getWordInstance();
		boundary.setText(source);
		int start = boundary.first();
		int end = boundary.next();
		String token = null;
		while (end != BreakIterator.DONE)
		{
			token = source.substring(start,
					end);
			token = token.trim();
			start = end;
			end = boundary.next();
		}
	}


	/**
	 * Compare the values and determine if it <code>lsb</code> needs update
	 * 
	 * @param newVal
	 *          Stringproposed correct value
	 * @param oldVal
	 *          String value subject to correction
	 * @param noteHdr
	 *          String header for this note entry
	 * @param noteA
	 *          FmtblNoteList part A of note
	 * @param noteB
	 *          FmtblNoteList part B of note
	 * @param changeFlag
	 *          boolean
	 * @return String
	 */
	private String setUpdate(String newVal,
			String oldVal,
			String separa,
			FmtblNoteList noteA,
			FmtblNoteList noteB,
			boolean changeFlag)
	{
		// #DG376 removed line by line
		// noteA.append(noteHdr + ":" + (oldVal == null ? "" : oldVal));
		// noteB.append(noteHdr + ":" + (newVal == null ? "" : newVal));
    /* #DG600
    if (!isEmpty(oldVal)) noteA.append(separa
        + oldVal);
    if (!isEmpty(newVal)) noteB.append(separa
        + newVal);*/
		if (!isEmpty(oldVal) && noteA!=null)
			noteA.add(BXResources.newStrFormata(separa, oldVal));
		if (!isEmpty(newVal) && noteB!=null)
			noteB.add(BXResources.newStrFormata(separa, newVal));

		dochange :
		{
				if (newVal == null
						|| newVal.length() == 0)
				{
					if (oldVal == null
							|| oldVal.length() == 0) // let's leave it if blank or null
						break dochange;
				}
				else
				{
					if (newVal.equals(oldVal)) 
						break dochange; 
				}
				// noteA.append(icHighLight+nl);
				// noteB.append(icHighLight+" change:"+changeFlag+nl); //#DG372 report
				// changeFlag
				// #DG314 return DBA.sqlStringFrom(psNewVal);
				return newVal;
		}
		// noteA.append(nl);
		// noteB.append(nl);
		return null;
	}

	private String getConcatAddress(Addr adr, boolean addressLineOnly, boolean parsed)
	{
		String addressLine = adr.getConcatenatedAddressLine();
		if (addressLineOnly)
			return addressLine;

		StringBuffer buf = new StringBuffer(addressLine).append(" ");
		if (adr.getCity() != null && adr.getCity().trim().length() > 0)
			buf.append( adr.getCity()).append(" ");

		if (adr.getProvinceId()>0)
		{
			String desc = BXResources.getPickListDescription(dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME, adr.getProvinceId(), locala);
			buf.append(desc).append(" ");
		}

		if (adr.getPostalFSA()!=null && adr.getPostalFSA().length()>0)
			buf.append(adr.getPostalFSA());
		if (adr.getPostalLDU()!=null && adr.getPostalLDU().length()>0)
			buf.append(adr.getPostalLDU());
		buf.append(NL);		//#DG600

		return buf.toString();
	}

	public String getPropertyConcatAddress(Property prop)
	{
		StringBuffer buf = new StringBuffer();

		if (prop.getUnitNumber() != null && prop.getUnitNumber().trim().length() > 0)
			buf.append(prop.getUnitNumber()).append("-");
		if (prop.getPropertyStreetNumber()!=null && prop.getPropertyStreetNumber().length()>0)
			buf.append(prop.getPropertyStreetNumber()).append(" ");

		if (locala.getLanguage().equalsIgnoreCase(Locale.FRENCH.getLanguage()))
		{
			if (prop.getStreetTypeId()>0)
			{
				String desc = BXResources.getPickListDescription(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, prop.getStreetTypeId(), locala);
				buf.append(desc).append(" ");
			}
		}

		if (prop.getPropertyStreetName()!=null && prop.getPropertyStreetName().length()>0)
			buf.append(prop.getPropertyStreetName()).append(" ");

		if (!locala.getLanguage().equalsIgnoreCase(Locale.FRENCH.getLanguage()))
		{
			if (prop.getStreetTypeId()>0)
			{
				String streetType = BXResources.getPickListDescription(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_TYPE, prop.getStreetTypeId(), locala);
				buf.append(streetType).append(" ");
			}
		}

		if (prop.getStreetDirectionId()>0)
		{
			String desc = BXResources.getPickListDescription(dbsrk.getExpressState().getDealInstitutionId(), PKL_STREET_DIRECTION, prop.getStreetDirectionId(), locala);
			buf.append(desc).append(" ");
		}

		if (prop.getPropertyCity()!=null && prop.getPropertyCity().length()>0)
			buf.append(prop.getPropertyCity()).append(" ");

		if (prop.getProvinceId()>0)
		{
			String desc = BXResources.getPickListDescription(dbsrk.getExpressState().getDealInstitutionId(), PKL_PROVINCESHORTNAME, prop.getProvinceId(), locala);
			buf.append(desc).append(" ");
		}

		if (prop.getPropertyPostalFSA()!=null && prop.getPropertyPostalFSA().length()>0)
			buf.append(prop.getPropertyPostalFSA());
		if (prop.getPropertyPostalLDU()!=null && prop.getPropertyPostalLDU().length()>0)
			buf.append(prop.getPropertyPostalLDU());
		buf.append(NL);		//#DG600

		return buf.toString();
	}

	private boolean isEmpty(String str)
	{
		return str == null
		|| str.trim().length() == 0;
	}

	/*
	 * iterates all items of this collection localizing and outputting it
	 */
	class FmtblNoteList extends Vector<Formattable> implements Formattable 
	{
		private static final long serialVersionUID = 1L;

		public void formatTo(Formatter fmt, int flagas, int width, int precision) {
			for(Formattable itema: this) {
				try {
					itema.formatTo(fmt, flagas, width, precision);
					IOException ioException = fmt.ioException();
					if (ioException != null)
						logger.error("item="+itema+':'+ioException);							
				} catch (Exception e) {
					logger.error("item="+itema+':'+e);
				}
			}				
		}
		/**
		 * Returns a <code>String</code> representation of the note, used for the alerts
		 *
		 * @return the deal note in text format.
		 */
		public String toString()
		{
			//final Locale locala = BXResources.getLocale(dbsrk.getLanguageId());
			final Locale locala = Locale.ENGLISH;
			return String.format(locala, "%s", this);
		}
	};

	// Unit testing
	public static void main(String[] args) throws Exception
	{
		System.out.println("=========== AddScrubExecutor Test Begin ==============");

		final String baseDir = "../ADMIN/";
		final String loginId = "DevTester";
		Config.SYS_PROPERTIES_LOCATION_DEV = baseDir;
		SysLogger loger = new SysLogger(AddScrubExecutor.class, loginId);
		loger.info("\n*****\n*****          AddScrubExecutor Test Begin ==============\n");
		PropertiesCache.addPropertiesFile(baseDir +"mossys.properties");
		ResourceManager.init(baseDir, PropertiesCache.getInstance().getInstanceProperty("com.basis100.resource.connectionpoolname"));
		SessionResourceKit srk = new SessionResourceKit(loginId);
		SysLog.init(baseDir);
		new BXResources();

		PassiveMessage pm = new PassiveMessage();
		AddScrubExecutor adrScruber = new AddScrubExecutor(srk);
		for (int li = 1222; li < 1444; li+=33) {
			//for (int li = 9005; li < 9006; li++)   {
			// Deal deal = new Deal(srk, null, 22, 7);
			Deal deal = null;
			try
			{
				// deal = new Deal(loDBsrk, null, li, 1);
				deal = new Deal(srk,null);
				deal = deal.findByGoldCopy(li);
			}
			catch (Exception ex)
			{
				continue;
			}
			// FiLogix - Mississauga
			// 276 King Street West, Suite 400
			// Toronto, ON, M5V 1J2
			/*
			 * // theExecutor.ioReq.setStreetNumber("276 1/2");
			 * theExecutor.ioReq.setStreetNumber("276"); //
			 * theExecutor.ioReq.setStreetNumber("");
			 *  // theExecutor.ioReq.setStreetName("276 1/2 King Street West, Suite
			 * 400"); theExecutor.ioReq.setStreetName("King Street West, Suite
			 * 400"); theExecutor.ioReq.setStreetName("King Street");
			 * 
			 * theExecutor.ioReq.setStreetSuffix("West");
			 * 
			 * theExecutor.ioReq.setApartmentOrUnit("suite 400"); //
			 * theExecutor.ioReq.setApartmentOrUnit("");
			 * 
			 * theExecutor.ioReq.setCity("toronto");
			 * theExecutor.ioReq.setProvince("on");
			 * theExecutor.ioReq.setPostalCode("M5V 1J2");
			 *  // GERRY SLOAN // 2765 7TH CONCESSION // S I T E 6 COMP 10 // RR 8
			 * STN MAIN // MILLARVILLE AB T0L 1K0
			 * 
			 * theExecutor.ioReq.setStreetNumber("2765");
			 * theExecutor.ioReq.setStreetName("7TH CONCESSION SITE 6 COMP 10 RR 8
			 * STN MAIN"); theExecutor.ioReq.setStreetSuffix("");
			 * theExecutor.ioReq.setApartmentOrUnit("");
			 * theExecutor.ioReq.setCity("MILLARVILLE");
			 * theExecutor.ioReq.setProvince("ab");
			 * theExecutor.ioReq.setPostalCode("T0L 1K0");
			 * 
			 * theExecutor.procReq();
			 */
			srk.beginTransaction();
			System.out.println(deal.getPk());
			pm.clearMessages();
			pm = adrScruber.validateAddresses(deal,
					pm,
					AddScrubConstants.ADDRESS_SOURCE_INGESTION);
			// pm = adrScruber.validateAddresses(deal,
					// pm,AddScrubConstants.ADDRESS_SOURCE_SCREEN);
			System.out.println("\n-------pass mesg results:");
			for (Iterator iter = pm.getMsgs().iterator(); iter.hasNext();)
			{
				Object item = (Object) iter.next();
				System.out.println("item = "
						+ item);
			}
			System.out.println("\n-------deal notes results:");
			DealNotes dealNotes = new DealNotes(srk);
			Collection dns = dealNotes.findByDeal((DealPK) deal.getPk());
			for (Iterator iter = dns.iterator(); iter.hasNext();)
			{
				DealNotes item = (DealNotes) iter.next();
				if (item.getDealNotesCategoryId() == Mc.DEAL_NOTES_CATEGORY_DEAL_INGESTION) 
					System.out.println("item = " + item.getDealNotesText());
			}
			System.out.println();
			// // Test for Parse passing
			// String tmpStr = "#415-1711 4th St. SW";
			// theExecutor.parseAddressLines(tmpStr);
			srk.rollbackTransaction();
			// dbsrk.commitTransaction(); //<<<<<<<<<<<<<<<<<<<<<<<<
		}
		srk.freeResources();
		// logger = null;
		ResourceManager.shutdown();
		System.out.println("=========== AddScrubExecutor Test Ended ==============");
		// System.exit(0); // Syslog never shuts down!!
	}
}
