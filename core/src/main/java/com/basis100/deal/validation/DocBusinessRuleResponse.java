package com.basis100.deal.validation;

import MosSystem.Mc;

public class DocBusinessRuleResponse
{
  public static final int C_OUTPUT_TYPE_RTF = 0;
  public static final int C_OUTPUT_TYPE_PDF = 1;
  public static final int C_OUTPUT_TYPE_CSV = 3;
  

  public static final int C_DESTINATION_TYPE_FILE = 0;
  public static final int C_DESTINATION_TYPE_E_MAIL = 1;

  private int outputType;
  private String outputTypeStr;
  private int language;
  private boolean create;
  private String documentVersion;
  private int lenderProfileId;
  private String documentFormat;
  private int documentProfileStatus;
  private String eMailSubject;
  private String eMailFullName;
  private int destinationType;
  private String eMailText;                                                    // Catherine, for BR-generated emailText

  public DocBusinessRuleResponse()
  {
    outputType = C_OUTPUT_TYPE_RTF;  // default
    outputTypeStr = null;
    language = Mc.LANGUAGE_PREFERENCE_ENGLISH; // default
    create = false; // default
    documentProfileStatus = 0;   // default
    eMailSubject = null;
    eMailFullName = null;
    destinationType = C_DESTINATION_TYPE_FILE;
  }

  public void setDestinationType(int destType){
    destinationType = destType;
  }
  public int getDestinationType(){
    return destinationType;
  }

  public boolean isEMailSet(){
    if( eMailSubject == null && eMailFullName == null ){
      return false;
    }
    return true;
  }

  public String getEMailFullName(){
    return eMailFullName;
  }
  public void setEMailFullName(String subjToSet){
    eMailFullName = subjToSet;
  }

  public String getEmailSubject(){
    return eMailSubject;
  }
  public void setEMailSubject(String subjToSet){
    eMailSubject = subjToSet;
  }

// -------------- Catherine, for BR-generated emailText start ------------
  public String getEmailText(){
    return eMailText;
  }
  public void setEMailText(String textToSet){
    eMailText = textToSet;
  }
  // -------------- Catherine, for BR-generated emailText end ------------

  public int getDocumentProfileStatus(){
    return documentProfileStatus;
  }

  public void setDocumentProfileStatus(int statusToSet){
    documentProfileStatus = statusToSet;
  }

  public String getDocumentFormat(){
    return documentFormat;
  }

  public void setDocumentFormat(String formatToSet){
    documentFormat = formatToSet;
  }

  public int getLenderProfileId(){
    return lenderProfileId;
  }

  public void setLenderProfileId(int idToSet){
    lenderProfileId = idToSet;
  }

  public String getDocumentVersion(){
    return documentVersion;
  }

  public void setDocumentVersion(String versionToSet){
    documentVersion = versionToSet;
  }

  public boolean isCreate(){
    return create;
  }

  public void setCreate(boolean createOrNot){
    create = createOrNot;
  }

  public void setLanguage(int lang){
    language = lang;
  }

  public int getLanguage(){
    return language;
  }

  public void setOutputType(String type){
    if(type == null){ return; }
    outputTypeStr = null;
    if(type.trim().equalsIgnoreCase("pdf")){
      outputType = C_OUTPUT_TYPE_PDF;
    }else if( type.trim().equalsIgnoreCase("rtf1")){
      outputType = C_OUTPUT_TYPE_RTF;
    }else if( type.trim().equalsIgnoreCase("csv")){
        outputType = C_OUTPUT_TYPE_CSV;
    }else{
      outputType = C_OUTPUT_TYPE_RTF;
      outputTypeStr = type;
    }
  }

  public String getOutputTypeStr(){
    return outputTypeStr;
  }

  public int getOutputType(){
    return outputType;
  }
}
