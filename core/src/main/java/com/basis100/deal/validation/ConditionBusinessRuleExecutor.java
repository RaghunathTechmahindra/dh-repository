package com.basis100.deal.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import com.basis100.deal.conditions.ConditionParser;
import com.basis100.deal.conditions.ConditionRegExParser;
import com.basis100.deal.entity.Borrower;
import com.basis100.deal.entity.Component;
import com.basis100.deal.entity.Condition;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealEntity;
import com.basis100.deal.entity.DocumentTracking;
import com.basis100.deal.entity.Property;
import com.basis100.deal.entity.UserProfile;
import com.basis100.BREngine.BRUtil;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;

/**
 * <p>
 * Title: ConditionBusinessRuleExecutor.java
 * </p>
 * <p>
 * Description: Process the Conditional business rules
 * </p>
 * 
 * 
 * @author MCM Team
 * @version 1.0 Initial Version 
 * @version 1.1 06-Aug-2008 | XS_2.49 | Added call for processing DCC-rules 
 
 */

public class ConditionBusinessRuleExecutor
{

  private int currentBorrowerId ;
  private int currentPropertyId ;
  private int currentComponentId;
  private Deal deal;
  private ArrayList retVals;
  private SessionResourceKit srk;
  private DealEntity parsingContext;
  private BusinessRule br;
 
    public ConditionBusinessRuleExecutor(SessionResourceKit pSrk)
  {
    this.srk = pSrk;
    currentBorrowerId = -1;
    currentPropertyId = -1;
    currentComponentId = -1;
    deal = null;
    retVals = null;
    parsingContext = null;
    br = null;
  }

  private void processRules(String rulePrefix)throws Exception
  {
      // for CIBC Perfomance enhancment - Use RegEx: added May 7 2010
      if(PropertiesCache.getInstance().getProperty( -1,
	      "com.filogix.conditions.useRegExParser", "Y").equals("Y")){
          processRulesRegEx(rulePrefix);
          return;
      }

    srk.setCloseJdbcConnectionOnRelease(true);
    srk.getJdbcExecutor().closeExecutorTemporary();

    // Added StackTrack Message dump for debug -- By Billy 07May2002
    try{
      // set the standard BRE (instance) variables ...
      br.reQuery(-1, -1, "na", rulePrefix, srk.getExpressState().getDealInstitutionId());
      // set global scope variables
      br.setGlobal("CurrentBorrowerId", new Integer(currentBorrowerId));
      br.setGlobal("CurrentPropertyId", new Integer(currentPropertyId));
      br.setGlobal("CurrentComponentId", new Integer(currentComponentId));
      srk.getSysLogger().debug("Property ID :" + currentPropertyId );
      srk.getSysLogger().debug("Borrower ID :" + currentBorrowerId );
      srk.getSysLogger().debug("Component ID :" + currentComponentId );
      while( br.next() ){
        //--Merge--//
        //Added debug message to capture BusinessRule Problems -- By BILLY 27June2002
        srk.getSysLogger().debug("@ConditionBusinessRuleExecutor :: Rule to check next = " + br.ruleEntity.ruleName);
        //===========================================================================
        RuleResult rr = br.evaluate();
        if(rr.ruleStatus){
          this.createConditionText(br.ruleEntity.ruleName);
        }
      }// end while
    }
    catch(Exception e)
    {
      srk.getSysLogger().error(StringUtil.stack2string(e));
      throw e;
    }
  }

  private void createConditionText(String pRuleName)throws Exception
  {
    String condIdAsStr = StringUtil.extractTrailingNumbers(pRuleName);
    if(condIdAsStr == null){return;}
    int id ;
    try{
      id = Integer.parseInt(condIdAsStr);
    }catch(Exception exc){
      throw new Exception("Condition id could not be extracted as required. Source string:" + condIdAsStr);
    }

    Condition cond = new Condition(srk,id);
    if( !cond.isActiveType() ){return;}
    ConditionParser parser = new ConditionParser();
    DocumentTracking dt = parser.createSystemGeneratedDocTracking(cond,deal,parsingContext,srk);

    retVals.add(dt);
  }

  private void setPropertyId(int pId){
    this.currentPropertyId = pId;
  }

  private void setBorrowerId(int pId){
    this.currentBorrowerId = pId;
  }
  
  /**
   * sets currentComponentId
   */
  private void setComponentId(int pId)
  {
      this.currentComponentId = pId;
  }
  
  public Collection extractConditions(Deal pDeal)throws Exception
  {
    this.deal = pDeal;
    retVals = new java.util.ArrayList();

    if(this.deal == null){return retVals;}

    // set VPD
    srk.getExpressState().setDealInstitutionId(pDeal.getInstitutionProfileId());
    br = new BusinessRule(srk, -1, -1, "na", "");
    br.setDealId(deal.getDealId(), deal.getCopyId());
    // #2242: Catherine, 23-Nov-05 --------- start ---------  
    br.setLenderName();
    BRUtil.debug("ConditionBusinessRuleExecutor: set LenderName done");
    // #2242: Catherine, 23-Nov-05 --------- end ---------  

    srk.getSysLogger().debug("Extracting conditions for deal :" + deal.getDealId() + "(" + deal.getCopyId()+").");

    this.processDealConditions();
    this.processPropConditions();
    this.processBorrConditions();
    // Call for processing Component Conditions
    this.processComponentConditions();
    return retVals;
  }

  private void processPropConditions()throws Exception
  {
    Collection properties = deal.getProperties();
    Iterator it = properties.iterator();

    while(it.hasNext()){
      Property oneProperty = (Property) it.next();
      this.parsingContext = oneProperty;
      this.setPropertyId(oneProperty.getPropertyId());
      this.setBorrowerId(-1);
      this.setComponentId(-1);  
      this.processRules("DCP-%");
    }
  }

  private void processBorrConditions() throws Exception
  {
    Collection borrowers = deal.getBorrowers();
    Iterator it = borrowers.iterator();

    while(it.hasNext()){
      Borrower oneBorrower = (Borrower) it.next();
      this.parsingContext = oneBorrower;
      this.setBorrowerId(oneBorrower.getBorrowerId());
      this.setPropertyId(-1);
      this.setComponentId(-1);  
      this.processRules("DCA-%");
    }
  }

  private void processDealConditions()throws Exception
  {
    this.setBorrowerId(-1);
    this.setPropertyId(-1);
    this.setComponentId(-1);  
    this.parsingContext = this.deal;
    this.processRules("DC-%");
  }
  
//06-Aug-2008 |XS_2.49|MCM IMPL STARTS 
//Added to execute the Component Condition rules.
  private void processComponentConditions() throws Exception
  {
      Collection components = deal.getComponents();
      Iterator componentIterator = components.iterator();

      while (componentIterator.hasNext())
      {
          Component currentComponent  = (Component) componentIterator.next();
          this.parsingContext = currentComponent ;
          this.setComponentId(currentComponent.getComponentId());
          this.setPropertyId(-1);
          this.setBorrowerId(-1);
          // Process DCC businessrules
          this.processRules("DCC-%");
      }
  }
//06-Aug-2008 |XS_2.49|MCM IMPL ENDS
  
  //------------------------------- Process Business Rules by RegEx ------------------------- 
  private void processRulesRegEx(String rulePrefix)throws Exception
  {
    srk.setCloseJdbcConnectionOnRelease(true);
    srk.getJdbcExecutor().closeExecutorTemporary();

    try{
      // set the standard BRE (instance) variables ...
      br.reQuery(-1, -1, "na", rulePrefix, srk.getExpressState().getDealInstitutionId());
      // set global scope variables
      br.setGlobal("CurrentBorrowerId", new Integer(currentBorrowerId));
      br.setGlobal("CurrentPropertyId", new Integer(currentPropertyId));
      br.setGlobal("CurrentComponentId", new Integer(currentComponentId));
      srk.getSysLogger().debug("Property ID :" + currentPropertyId );
      srk.getSysLogger().debug("Borrower ID :" + currentBorrowerId );
      srk.getSysLogger().debug("Component ID :" + currentComponentId );
      
      ArrayList conditionList = new ArrayList();
      while( br.next() ){

        RuleResult rr = br.evaluate();
        if(rr.ruleStatus){
          Condition cond = createCondition(br.ruleEntity.ruleName);
          if(cond != null){
        	  conditionList.add(cond);
          }
        }
      }
      
      ConditionRegExParser RegExParser = new ConditionRegExParser(srk);
      RegExParser.createSystemGeneratedDocTracking(conditionList, deal, parsingContext, retVals);
    }
    catch(Exception e)
    {
      srk.getSysLogger().error(StringUtil.stack2string(e));
      throw e;
    }
  }
  
  private Condition createCondition(String pRuleName)throws Exception
  {
    String condIdAsStr = StringUtil.extractTrailingNumbers(pRuleName);
    if(condIdAsStr == null){
    	return null;
    }
    int id ;
    try{
      id = Integer.parseInt(condIdAsStr);
    }catch(Exception exc){
      throw new Exception("Condition id could not be extracted as required. Source string:" + condIdAsStr);
    }

    Condition cond = new Condition(srk,id);
    
    //ConditionFactory condFac = ConditionFactory.getInstance();
    //Condition cond = condFac.getCondition(srk, id);
    
    if( !cond.isActiveType() ){
    	return null;
    }
    return cond;
  }

}


