package com.basis100.deal.validation;

import java.util.*;
import java.io.*;
import MosSystem.Mc;
import com.basis100.resources.*;
import com.basis100.deal.entity.*;
import com.basis100.BREngine.BRUtil;
import com.basis100.BREngine.BusinessRule;
import com.basis100.BREngine.RuleResult;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.StringUtil;
import com.basis100.support.*;
public class DocBusinessRuleExecutor
{

  private int currentBorrowerId ;
  private int currentPropertyId ;

  private SessionResourceKit srk;
  private Deal deal;
  private int docProfile ;
  private DocBusinessRuleResponse response;
  private String ruleIdPattern;
  private BusinessRule br;

  public DocBusinessRuleExecutor(SessionResourceKit pSrk)
  {
    this.srk = pSrk;
    response = new DocBusinessRuleResponse();

    currentBorrowerId = -1;
    currentPropertyId = -1;
  }

  private void findPrimBorrowerId(){
    if(this.deal == null){ return; }

    try{
      Collection borrowers = deal.getBorrowers();
      if(borrowers.isEmpty()){return;}
      Iterator it = borrowers.iterator();
      while(it.hasNext()){
        Borrower b = (Borrower)it.next();
        if(b.isPrimaryBorrower()){
          this.currentBorrowerId = b.getBorrowerId();
          return;
        }
      }
    } catch(Exception exc){
      return;
    }
  }

  /*
   * rollback change on revision #965 on ML
   * Aug 22 2008 MCM Impl Team
   * ticket FXP22279
   */
  public DocBusinessRuleResponse evaluateRequest ( Deal pDeal, int docProfileNum )throws Exception
  {
      srk.getSysLogger().info(this.getClass().getName() + ": evaluating deal:" + pDeal.getDealId() + " (" + pDeal.getCopyId() + ")" + ":profile requested : " + docProfileNum);
      this.deal = pDeal;
      this.docProfile = docProfileNum;
      this.findPrimBorrowerId();

      if(this.deal == null){
        throw new Exception("DocBusinessRuleExecutor:Supplied deal is null.");
      }
      if(docProfile == -1){
        throw new Exception("DocBusinessRuleExecutor: Invalid document profile number:" + this.docProfile);
      }

      ruleIdPattern = new String("DOC-" + docProfile);

      // added for ML
      //  set VPD from deal InstitutionProfileId
      srk.getExpressState().setDealInstitutionId(pDeal.getInstitutionProfileId());

      
      br = new BusinessRule(srk, -1, -1, "na", "");
      br.setDealId(deal.getDealId(), deal.getCopyId());
      // #2242: Catherine, 23-Nov-05 --------- start ---------  
      br.setLenderName();
      BRUtil.debug("DocRuleExecutor: set LenderName done");
      // #2242: Catherine, 23-Nov-05 --------- end ---------  

      // by default
      response = new DocBusinessRuleResponse();
      processRules(ruleIdPattern);
      return response;
  }

  /*Method added for Deal Summary*/
  public DocBusinessRuleResponse evaluateRequest ( Deal pDeal, int docProfileNum,int lang)throws Exception
  {
    srk.getSysLogger().info("******************************INSIDE DocBusinessRuleResponse evaluateRequest ************************");
    srk.getSysLogger().info("******************************INSIDE DocBusinessRuleResponse evaluateRequest - docProfileNum ************************"+docProfileNum);
    srk.getSysLogger().info(this.getClass().getName() + ": evaluating deal:" + pDeal.getDealId() + " (" + pDeal.getCopyId() + ")" + ":profile requested : " + docProfileNum);
    this.deal = pDeal;
    this.docProfile = docProfileNum;
    this.findPrimBorrowerId();
    
    if(this.deal == null){
      throw new Exception("DocBusinessRuleExecutor:Supplied deal is null.");
    }
    if(docProfile == -1){
      throw new Exception("DocBusinessRuleExecutor: Invalid document profile number:" + this.docProfile);
    }

    ruleIdPattern = new String("DOC-" + docProfile);
    
    // added for ML
    //  set VPD from deal InstitutionProfileId
    srk.getExpressState().setDealInstitutionId(pDeal.getInstitutionProfileId());

    br = new BusinessRule(srk, -1, -1, "na", "");
    srk.getSysLogger().info("******************************INSIDE DocBusinessRuleResponse evaluateRequest - getDealId()************************"+ deal.getDealId());
    srk.getSysLogger().info("******************************INSIDE DocBusinessRuleResponse evaluateRequest - getCopyId()************************"+ deal.getCopyId());
    
    br.setDealId(deal.getDealId(), deal.getCopyId());
    // #2242: Catherine, 23-Nov-05 --------- start ---------  
    br.setLenderName();
    BRUtil.debug("DocRuleExecutor: set LenderName done");
    // #2242: Catherine, 23-Nov-05 --------- end ---------  

    // by default
    response = new DocBusinessRuleResponse();
    if(response != null){
         srk.getSysLogger().info("******************************if(response != null)************************");        
    }
    processRules(ruleIdPattern);
    response.setLanguage(lang);

    return response;
  }  
  
  private void processRules(String rulePrefix)throws Exception
  {
    srk.getSysLogger().info(this.getClass().getName() + ":method processRules entered."  );
    srk.setCloseJdbcConnectionOnRelease(true);
    srk.getJdbcExecutor().closeExecutorTemporary();

    // Added StackTrack Message dump for debug -- By Billy 07May2002
    try{
      // set the standard BRE (instance) variables ...
      br.reQuery(-1, -1, "na", rulePrefix, srk.getExpressState().getDealInstitutionId());
      // set global scope variables
      br.setGlobal("CurrentBorrowerId", new Integer(currentBorrowerId));
      br.setGlobal("CurrentPropertyId", new Integer(currentPropertyId));
      br.setGlobal("UserProfileId", new Integer( srk.getExpressState().getUserProfileId() ) );
      while( br.next() ){
        //--Merge--//
        //Added debug message to capture BusinessRule Problems -- By BILLY 27June2002
        srk.getSysLogger().debug("@DocBusinessRuleExecutor :: Rule to check next = " + br.ruleEntity.ruleName);
        //===========================================================================
        RuleResult rr = br.evaluate();
        if(rr.ruleStatus){
          srk.getSysLogger().info(this.getClass().getName() + ":method processRules - true branch entered."   );
          //===============================================
          // first true means that we HAVE to make document
          //===============================================
          response.setCreate(true);
          response.setOutputType(rr.getString(1));
          response.setLanguage(rr.getInt(2));
          response.setDocumentVersion(rr.getString(3));
          response.setEMailSubject(rr.getString(4));
          response.setEMailFullName(rr.getString(5));
          response.setDestinationType(rr.getInt(6));
          if (rr.opCount > 6){
            response.setEMailText(rr.getString(7));                                      // OPValue 7, for BR-generated emailText
          }
          srk.getSysLogger().info(this.getClass().getName() + ":method processRules finished (1)."  );
          return;
        }
      }// end while
    }
    catch(Exception e)
    {
      srk.getSysLogger().error(StringUtil.stack2string(e));
      NotifierMessageHolder holder = new NotifierMessageHolder();

      StringWriter swr = new StringWriter();
      PrintWriter pwr = new PrintWriter(swr);
      e.printStackTrace(pwr);
      String outStr = swr.toString();

      holder.addTitleContentPair("Problem          ","Business rule evaluation failure.\n");
      holder.addTitleContentPair("Rule name        ",br.ruleEntity.ruleName + "\n" );
      holder.addTitleContentPair("Deal Id          ","" +deal.getDealId() + "\n");
      holder.addTitleContentPair("Copy             ","" +deal.getCopyId() + "\n");
      holder.addTitleContentPair("Class            ","" +this.getClass().getName() + "\n");
      holder.addTitleContentPair("Exception Path   ","" +outStr + "\n");

      SupportNotifier.getInstance().notifySupport(holder);
      throw e;
    }
    srk.getSysLogger().info(this.getClass().getName() + ":method processRules finished (2)."  );
  }

}
