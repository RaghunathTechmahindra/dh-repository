package com.basis100.deal.history;

import java.lang.Exception;
import java.util.Date;

import MosSystem.Mc;

import com.basis100.deal.entity.Contact;
import com.basis100.deal.entity.Deal;
import com.basis100.deal.entity.DealHistory;
import com.basis100.deal.entity.LenderProfile;
import com.basis100.deal.entity.SourceFirmProfile;
import com.basis100.deal.entity.SourceOfBusinessProfile;
import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.ContactPK;
import com.basis100.deal.pk.LenderProfilePK;
import com.basis100.deal.pk.SourceOfBusinessProfilePK;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.entity.FinderException;
import com.basis100.entity.RemoteException;
import com.basis100.log.SysLogger;
import com.basis100.picklist.PicklistData ;
import com.basis100.workflow.UserProfileQuery;
import com.basis100.workflow.entity.AssignedTask;
import com.basis100.workflow.entity.Task;
import com.basis100.workflow.pk.AssignedTaskBeanPK;
import com.basis100.workflow.pk.TaskBeanPK;
import com.basis100.picklist.BXResources;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;

////import mosApp.MosSystem.SessionStateModelImpl;

/**
* An instance of this class provides access to facilities for logging
* major events occur on a deal process throughout the origination process.
*
* USAGE
* ---------------
*
* User session creates an instance providing its session resource kit (access to database connection).
* These are no special cleanup or resource freeing requirements.
*
*
* public void log(int dealID, int copyID, String sText, int typeID, int userProfileID) throws RemoteException, FinderException
*
**/

public final class DealHistoryLogger
{
  private SessionResourceKit  srk;
  private SysLogger           logger;

  public DealHistoryLogger(SessionResourceKit srk)
  {
     this.srk = srk;
     logger   = ResourceManager.getSysLogger("DealHist");
  }

/**
  * Since this class not suitable as a singleton we basically use this method as a
  * simple constructor in disguise. At a  later date all references to:
  *
  *        DealHistoryLogger dhl = DealHistoryLogger.getInstance(srk);
  *
  * should be changed to:
  *
  *        DealHistoryLogger dhl = new DealHistoryLogger(srk);
  *
  * Why not a singleton? A singleton typically caches some resource for shared access (such as database
  * records, etc). This class on the other hand simply provides access to deal history create record methods
  * and other query methods and,  these methods should use the resources provided by the caller (hence
  * caller provides session resource kit when constructing an instance).
  *
  **/

  public static DealHistoryLogger getInstance(SessionResourceKit srk)  throws Exception
  {
     return new DealHistoryLogger(srk);
  }

  ////public SessionStateModelImpl theSessionState;

  //create a record in the dealHistory table whenever a major event occurs
  public void log(int dealID, int copyID, String sText, int typeID, int userProfileID)
           throws RemoteException, FinderException
  {
    try
    {
        Deal deal = new Deal(srk, null, dealID, copyID);
        int statusID = (int)deal.getStatusId();
        log(dealID, copyID, sText, typeID, userProfileID, statusID);
    }
    catch(Exception e)
    {
       logger.error("DealHistoryLogger: log() error.");
       logger.error(e);
    }
  }

  //create a record in the dealHistory table whenever a major event occurs
  public void log(int dealID, int copyID, String sText, int typeID, int userProfileID, int statusID)
           throws RemoteException, FinderException
  {
     try
     {
        Date transactionDate = new Date();

        //create an instance of DealHistoryBean
        DealHistory dealHistory = new DealHistory(srk);

        // ensure message field does exceed 256 characters
        if (sText.length() > 256)
          sText = sText.substring(0, 256);

        // create a new record in the dealHistory table
        dealHistory.create(dealID, userProfileID, typeID, statusID, sText, transactionDate);
     }
     catch(Exception e)
     {
        logger.error("DealHistoryLogger: log() error.");
        logger.error(e);
     }
  }

  // provides the appropriate text that is fed into the log() whenever the
  // status has changed
  public String statusChange(int previousStatusID, String process)
                          throws RemoteException, FinderException{

     String previousDesc = PicklistData.getDescription("Status", previousStatusID);
     String result = "Status Change - " + previousDesc + ", " + process;

     return result;
  }

  // return texts contain  the name for source of business
  public String dealIngestion(int sourceOfBusinessProfileID)
                throws RemoteException, FinderException{

     SourceOfBusinessProfile SOBPBean =
         new SourceOfBusinessProfile(srk).findByPrimaryKey(
         new SourceOfBusinessProfilePK(sourceOfBusinessProfileID));

     String result = "Deal Ingestion -" + SOBPBean.getSOBPShortName();

     return result;
  }

  //--DocCentral_FolderCreate--21Sep--2004--start--//

  public String docCentralFolderCreationInfo(SourceFirmProfile sourceFirmProfile, String folderIdenticator)
                throws Exception
 {
     String msg = "";

     try
     {
       // In theory it should be translated later but it leads to drastically increasing of
       // ingestion.jar file since it needs mosApp/MosSystemSessionStateModelImpl class, etc., etc,
       // Moreover, the whole HistoryLogger class is not bilingual yet, for now just English as usual.
       ////msg = BXResources.getSysMsg("BROKER_OUTSTANDING_CONDITIONS_PRODUCED",
       ////                      theSessionState.getLanguageId());

       msg = BXResources.getSysMsg("DOC_CENTRAL_FOLDER_PRODUCED", Mc.LANGUAGE_PREFERENCE_ENGLISH);

     }
     catch (Exception e)
     {
         SysLogger logger = srk.getSysLogger();

         logger.error("Exception @DealHistoryLogger.docCentralFolderInfo: Problem getting the message");
         logger.error(e);

         throw new Exception("DealHistoryLogger exception");
     }

     String result = folderIdenticator + "-" + msg  + sourceFirmProfile.getSourceFirmName();

     return result;

  }
  //--DocCentral_FolderCreate--21Sep--2004--end--//

  // return texts contain document preparation info
 /* public static String documentPreparation(int documentProfileID)
                throws RemoteException, FinderException{

     DocumentProfile DPBean =
        new DocumentProfile(srk).findByPrimaryKey(
        new DocumentProfilePK(documentProfileID));

     String message = DPBean.getDocumentFullName();

     String result = "Document Preparation -" + message;

     return result;
  }   */

  // return text contain the mortgage insurer name and status
  public String mortgageInsuranceRequest(int dealID, int copyID)
                          throws RemoteException, FinderException{

     Deal deal = new Deal(srk, null, dealID, copyID);

     return concatMortgageInsuranceRequestText(deal);
  }

  public String concatMortgageInsuranceRequestText(Deal deal) {
      
      int MIId = deal.getMortgageInsurerId();

      String MIName = PicklistData.getDescription("MortgageInsurer", MIId);

      int MIStatusID = deal.getMIStatusId();

      String MIStatus = PicklistData.getDescription("MIStatus", MIStatusID);

      String result = "MI Request -" + MIName + ", " + MIStatus;

      return result;
  }

  // return text contains mortgage insurer name and status
  public String mortgageInsuranceResponse(int dealID, int copyID)
                 throws RemoteException, FinderException{

     Deal deal = new Deal(srk, null, dealID, copyID);

     return concatMortgageInsuranceResponseText(deal);
  }

  public String concatMortgageInsuranceResponseText(Deal deal) {
      
      int MIId = deal.getMortgageInsurerId();

      String MIName = PicklistData.getDescription("MortgageInsurer", MIId);

      int MIStatusID = deal.getMIStatusId();

      String MIStatus = PicklistData.getDescription("MIStatus", MIStatusID);

      //FXP22330 - added 's'
      String result = "MI Response -" + MIName + ", " + MIStatus;

      return result;
  }

  //  return the name of the business source
  public String sourceSystemReponse(int sourceOfBusinessProfileID)
            throws RemoteException, FinderException{

     SourceOfBusinessProfile SOBPBean =
         new SourceOfBusinessProfile(srk).findByPrimaryKey(
         new SourceOfBusinessProfilePK(sourceOfBusinessProfileID));

     String result = "Response to Source - " + SOBPBean.getSOBPShortName();

     return result;

  }

  // return the previous and current user full names when the deal is rerouted

  public String taskAndDealRerouting(int preATWQID, int curATWQID)
          throws RemoteException, FinderException
 {
     //retrieve previous user name
     AssignedTask preAssignedTaskBean =
                new AssignedTask(srk).findByPrimaryKey(
                new AssignedTaskBeanPK(preATWQID));

     int preTaskID =  preAssignedTaskBean.getTaskId();

     Task preTask = new Task(srk).findByPrimaryKey(new TaskBeanPK(preTaskID));

     int preUserProfileID = preAssignedTaskBean.getUserProfileId();

     UserProfile preUserProfile =
               new UserProfile(srk).findByPrimaryKey(
               new UserProfileBeanPK(preUserProfileID, srk.getExpressState().getDealInstitutionId()));

     int preContactId = preUserProfile.getContactId();

     Contact preContact = new Contact(srk).findByPrimaryKey(
                          new ContactPK(preContactId, 1 )) ;

     String preName =  preContact.getContactFullName();

     //retrieve current task name and user name
     AssignedTask curAssignedTaskBean =
                new AssignedTask(srk).findByPrimaryKey(
                    new AssignedTaskBeanPK(curATWQID));

     int curTaskID =  curAssignedTaskBean.getTaskId();

     Task curTask = new Task(srk).findByPrimaryKey(new TaskBeanPK(curTaskID));

     String taskName = curTask.getTaskName();

     int curUserProfileID = curAssignedTaskBean.getUserProfileId();


     UserProfile curUserProfile =
            new UserProfile(srk).findByPrimaryKey(
               new UserProfileBeanPK(curUserProfileID, srk.getExpressState().getDealInstitutionId()));

     int curContactId = curUserProfile.getContactId();

     Contact curContact = new Contact(srk).findByPrimaryKey(new ContactPK(curContactId, 1));

     String curName =  curContact.getContactFullName();

     String result = "Manual Routing -" + taskName + ", " + preName
                      + ", " + curName;
     return result;
  }

  //using for reassign current task in Master Work Queue
  //#DG800 add change type to make it generic
  public String taskAndDealReassign(AssignedTask at, int toIUserType, int userProfileFromId,
    int userProfileToId)
  {
     try
     {
     	StringBuffer sb = new StringBuffer(128);
   	sb.append("Manual Routing ");
   	
      switch (toIUserType) {
    	case UserProfileQuery.UNDERWRITER_USER_TYPE:
         sb.append("(UW)");
         break;
    	case UserProfileQuery.ADMIN_USER_TYPE:
         sb.append("(AD)");
         break;
    	case UserProfileQuery.FUNDER_USER_TYPE:		
         sb.append("(FUNDER)");
    		break;
      }
      sb.append("- ");
   	
   	if(at != null)	{
       Task task = new Task(srk);

       task.findByPrimaryKey(new TaskBeanPK(at.getTaskId()));
       sb.append(task.getTaskName()).append(", ");
   	}
   	sb.append("Assigned From User ")
   		.append(getUserShortname(userProfileFromId))
   		.append(", Assigned To User ")
   		.append(getUserShortname(userProfileToId));
   	return sb.toString();
     }
     catch(Exception e){
        logger.error("DealHistoryLogger: taskAndDealReassign() error.");
        logger.error(e);
     }
     return null;
  }
  
  /*/using for reassign current task in Master Work Queue
  public String taskAndDealReassign(AssignedTask at, int userProfileFromId,
    int userProfileToId)
  {
     try
     {
       Task task = new Task(srk);

       task.findByPrimaryKey(new TaskBeanPK(at.getTaskId()));

       return  "Manual Routing - " + task.getTaskName() + ", Assigned From User " +
       getUserShortname(userProfileFromId) +  ", Assigned To User " +
       getUserShortname(userProfileToId);
     }
     catch(Exception e){

        logger.error("DealHistoryLogger: taskAndDealReassign() error.");
        logger.error(e);
     }
     return null;
  }

 //using for reassign current deal in Master Work Queue
  public String taskAndDealReassignUW(int dealid, int userProfileFromId, int userProfileToId)
  {
      try
     {
       return  "Manual Routing (UW)-  Current Deal" + ", Assigned From User " +
       getUserShortname(userProfileFromId) +  ", Assigned To User " +
       getUserShortname(userProfileToId);
     }catch(Exception e){

        logger.error("DealHistoryLogger: taskAndDealReassignUW() error.");
        logger.error(e);
     }
     return null;
  }
  public String taskAndDealReassignFN(int dealid, int userProfileFromId, int userProfileToId)
  {
      try
     {
       return  "Manual Routing (FUNDER)-  Current Deal" + ", Assigned From User " +
       getUserShortname(userProfileFromId) +  ", Assigned To User " +
       getUserShortname(userProfileToId);
     }catch(Exception e){

        logger.error("DealHistoryLogger: taskAndDealReassignFN() error.");
        logger.error(e);
     }
     return null;
  }
  public String taskAndDealReassignAD(int dealid, int userProfileFromId, int userProfileToId)
  {
      try
     {
       return  "Manual Routing (ADMIN)-  Current Deal" + ", Assigned From User " +
       getUserShortname(userProfileFromId) +  ", Assigned To User " +
       getUserShortname(userProfileToId);
     }catch(Exception e){

        logger.error("DealHistoryLogger: taskAndDealReassignAD() error.");
        logger.error(e);
     }
     return null;
  }*/

  private String getUserShortname(int userProfileId) throws Exception
  {
       UserProfile userProfile =
               new UserProfile(srk).findByPrimaryKey(
               new UserProfileBeanPK(userProfileId, srk.getExpressState().getDealInstitutionId()));

       Contact preContact = new Contact(srk).findByPrimaryKey(
                          new ContactPK(userProfile.getContactId(), 1 ));

       String resStr = "";

       String firstName =preContact.getContactFirstName();
       String lastName = preContact.getContactLastName();

       if( firstName!=null && firstName.length()>0)
       {
         resStr = firstName.substring(0,1)+ ".";
       }
       if( lastName!=null && lastName.length()>0)
       {
          return resStr + lastName;
       }
       return " ";
  }

 /*
  //  return a message whenever data changes
  public static String dataChange(int preStatusID, String process)
                    throws RemoteException, FinderException{

     String previousDesc = PicklistData.getDescription("Status", preStatusID);

     String result = "Data Change - " + previousDesc + ", " + process;

     return result;
  }
  */

  // return the institution name for the export service system
  public  String exportToServicingSystem(int lenderProfileId)
                   throws RemoteException, FinderException{
      
//FXP20842 - Mar 20, 2008
//     InstitutionProfile institutionBean =
//                new InstitutionProfile(srk).findByPrimaryKey(
//                      new InstitutionProfilePK(institutionProfileID));

      LenderProfile lender =
          new LenderProfile(srk).findByPrimaryKey(
                new LenderProfilePK(lenderProfileId));

      String lenderName = lender.getLenderName();

     String result = "Export to Servicing -" + lenderName;

     return result;
  }

  // return the full name of final approver
  public  String getFinalApproverLogMessage(int userId, SessionResourceKit srk) throws Exception
  {
    try
    {
        UserProfile userProfile =
                   new UserProfile(srk).findByPrimaryKey(
                   new UserProfileBeanPK(userId, srk.getExpressState().getDealInstitutionId()));

         int contactId = userProfile.getContactId();

         Contact contact = new Contact(srk). findByPrimaryKey(new ContactPK(contactId, 1));

         return "Final Approver - " + contact.getContactFullName();
     }
     catch (Exception e)
     {
         SysLogger logger = srk.getSysLogger();

         logger.error("Exception @DealHistoryLogger.getFinalApproverLogMessage: Problem determining approver name");
         logger.error(e);

         throw new Exception("DealHistoryLogger exception");
     }
   }

     //--Ticket#582--start--07Sep--2004--//
     // Message in History Table should be produced whenever Broker Outstanding Condition doc is sent.
     // No need for extra information (such SourceName, etc.) since it has been displaying in the header.
     public final static String BROKER_OUTSTANDING_CONDITIONS_PRODUCED =
                 "Broker outstanding conditions update produced, DocTracking";

     public  String getBrokerOutstandingLogMsg() throws Exception
     {
       String msg = "";
       try
       {
         // In theory it should be translated later but it leads to drastically increasing of
         // ingestion.jar file since it needs mosApp/MosSystemSessionStateModelImpl class, etc., etc,
         // Moreover, the whole HistoryLogger class is not bilingual yet, for now just English as usual.
         ////msg = BXResources.getSysMsg("BROKER_OUTSTANDING_CONDITIONS_PRODUCED",
         ////                      theSessionState.getLanguageId());

         msg = this.BROKER_OUTSTANDING_CONDITIONS_PRODUCED;

       }
       catch (Exception e)
       {
           SysLogger logger = srk.getSysLogger();

           logger.error("Exception @DealHistoryLogger.getBrokerOutstandingLogMs: Problem getting the message");
           logger.error(e);

           throw new Exception("DealHistoryLogger exception");
       }

       return msg;
     }
    //--Ticket#582--start--07Sep--2004--//
     
     public String getMIAutoCancelMsg(int MIId)
     {
//         int MIId = deal.getMortgageInsurerId();

         String MIName = PicklistData.getDescription("MortgageInsurer", MIId);
         String message = "Mortgage insurance cancelled - " + MIName + ".";
          
         return message;
     }
  }
