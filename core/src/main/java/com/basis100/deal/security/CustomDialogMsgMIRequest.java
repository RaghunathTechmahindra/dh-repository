package com.basis100.deal.security;

import MosSystem.Mc;

class CustomDialogMsgMIRequest extends ActiveMessage {
    private static final long serialVersionUID = 1L;

    public CustomDialogMsgMIRequest(String replacement,
            String replacement2) {
        
        setParmReplace(replacement);
        setParmReplace2(replacement2);
        common();
    }

    public CustomDialogMsgMIRequest(String replacement) {
        setParmReplace(replacement);
        common();
    }

    public CustomDialogMsgMIRequest() {
        common();
    }

    public void common() {
        generate = true;
        hasOK = true;
        hasCancel = true;
        hasClose = false;

        buttonHtml = Mc.OK_MI_REQUEST_EXECUTE_BTN + Mc.SPACER_EXECUTE_BTN
                + Mc.CANCEL_CLIENT_EXECUTE_BTN;

        responseIdentifier = OK_NAME;

    }

}