package com.basis100.deal.security;

import MosSystem.Sc;
import MosSystem.Mc;
import com.basis100.picklist.BXResources;

class FatalMsg extends ActiveMessage {

  //--Release2.1--//
  //--> Added LanguageId for Mulingual Support
  //--> By Billy 10Dec2002
  public FatalMsg(String replacement, String replacement2, int languageId)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common(languageId);
  }

  public FatalMsg(String replacement, int languageId)
  {
      setParmReplace(replacement);
      common(languageId);
  }

  public FatalMsg(int languageId)
  {
      common(languageId);
  }

  public void common(int languageId)
  {
      generate  = true;
      setDialogMsg(BXResources.getSysMsg("FATAL_SESSION_ENDED", languageId));
      hasOK    = true;
      hasCancel= false;
      hasClose = false;
      buttonHtml = Mc.OK_CLIENT_EXECUTE_BTN;
  }

}