package com.basis100.deal.security;

import com.basis100.picklist.BXResources;
import com.basis100.deal.util.BXStringTokenizer;

public class ActiveMsgFactory
{
  public static final int ISJUMP = 1;
  public static final int ISCANCEL = 2;   // this one not being used! MosSystem uses ISPAGECONFIRMCANCEL (below)
  public static final int ISFATAL = 3;
  public static final int ISPAGEDENIEDMSG = 4;
  public static final int ISEDITSUBPAGE = 5;
  public static final int ISVIEWSUBPAGE = 6;
  public static final int ISCUSTOMDIALOG = 7;
  public static final int ISCUSTOMCONFIRM = 8;
  public static final int ISCUSTOMFATAL = 9;
  public static final int ISPAGECONFIRMCANCEL = 10;
  public static final int ISDEALENTRYCONFIRMCANCEL = 11;
  public static final int ISCONFIRMENDSESSIONDIALOG = 14;

  // This type has both buttons generating server events!
  public static final int ISCUSTOMDIALOG2 = 12;  //

  public static final int ISCUSTOMLEAVEPAGE = 13;

  // This type doesn't produce the interactive screen. All actions related to the
  // web event system promotes without negotiations. Use it very careful.
  public static final int ISNODIALOG = 15;

  // New type for Exit page -- By BILLY 07May2002
  public static final int ISDEALENTRYCONFIRMEXIT = 16;
  public static final int ISPAGECONFIRMEXIT = 17;
  //=================================================

  //--DJ_LDI_CR--start--//
  public static final int ISLIFEDISABILITYCONFIRMSUBMIT = 18;
  //--DJ_LDI_CR--end--//

  // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
  // new type with "YES" and "NO" buttons
  public static final int ISYESNO = 19;
  // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------

  public static final int ISCUSTOMDIALOG_MI_REQUEST = 20;
  
  public ActiveMsgFactory()
  {
  }

  // parmReplacement
  //
  // String to replace '%' appearing in the dialog/message text

  public static ActiveMessage getActiveMessage (int msgType, int languageId)
  {
    return getActiveMessage (msgType, "", "", languageId);
  }

  public static ActiveMessage getActiveMessage (int msgType, String replacement, int languageId)
  {
    return getActiveMessage (msgType, replacement, "", languageId);
  }


  //--Release2.1--//
  //Added Multilingual Support
  //Based on the input LanguageId to replace all Button Images with diferent Language.
  //Special Tokens are used to identify the position of the specific Button:
  //  !OK! -- OK button
  //  !CANCEL! -- Cancel button
  //  !CLOSE! -- Close button
  //--> By Billy 12Nov2002
  public static ActiveMessage getActiveMessage (int msgType, String replacement, String replacement2, int languageId)
  {
     ActiveMessage am = null;

     switch ( msgType )
     {
        case ISJUMP:                    am = new JumpMsg(replacement, replacement2, languageId); break;
        case ISCANCEL:                  am = new CancelMsg(replacement, replacement2, languageId); break;
        case ISFATAL:                   am = new FatalMsg(replacement, replacement2, languageId); break;
        case ISPAGEDENIEDMSG:           am = new PageDeniedMsg(replacement, replacement2, languageId); break;
        case ISEDITSUBPAGE:             am = new SubPageEditMsg(replacement, replacement2, languageId); break;
        case ISVIEWSUBPAGE:             am = new SubPageViewMsg(replacement, replacement2, languageId); break;
        case ISPAGECONFIRMCANCEL:       am = new ConfirmPageCancelMsg(ISPAGECONFIRMCANCEL, replacement, replacement2, languageId); break;
        case ISDEALENTRYCONFIRMCANCEL:  am = new ConfirmPageCancelMsg(ISDEALENTRYCONFIRMCANCEL, replacement, replacement2, languageId); break;
        case ISCUSTOMDIALOG:            am = new CustomDialogMsg(replacement, replacement2); break;
        case ISCUSTOMDIALOG2:           am = new CustomDialogMsg2(replacement, replacement2); break;
        case ISCUSTOMDIALOG_MI_REQUEST: am = new CustomDialogMsgMIRequest(replacement, replacement2); break;
        case ISCUSTOMCONFIRM:           am = new CustomConfirmMsg(replacement, replacement2); break;
        case ISCUSTOMFATAL:             am = new CustomFatalMsg(replacement, replacement2); break;
        case ISCUSTOMLEAVEPAGE:         am = new CustomFatalMsg(replacement, replacement2); break;
        case ISCONFIRMENDSESSIONDIALOG: am = new CustomDialogMsg(replacement, replacement2); break;
        case ISNODIALOG:                am = new NoDialogMsg(replacement, replacement2); break;

        // New Alert Type for Exit screen and continue with workflow -- By BILLY 07May2002
        case ISPAGECONFIRMEXIT:       am = new ConfirmPageCancelMsg(ISPAGECONFIRMEXIT, replacement, replacement2, languageId); break;
        case ISDEALENTRYCONFIRMEXIT:  am = new ConfirmPageCancelMsg(ISDEALENTRYCONFIRMEXIT, replacement, replacement2, languageId); break;

        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
        case ISYESNO:                 am = new CustomDialogMsgYesNo(replacement, replacement2); break;

        // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------

     }

     if (am != null)
     {
        am.setMsgType(msgType);

        //--Release2.1--//
        //Replace all OK buttons from the buttonHtml
        am.setButtonHtml(BXStringTokenizer.replace(am.getButtonHtml(), "!OK!",
            BXResources.getGenericMsg("AM_OK_IMAGE", languageId)));

        //Replace all Cancel buttons from the buttonHtml
        am.setButtonHtml(BXStringTokenizer.replace(am.getButtonHtml(), "!CANCEL!",
            BXResources.getGenericMsg("AM_CANCEL_IMAGE", languageId)));

        //Replace all Close buttons from the buttonHtml
        am.setButtonHtml(BXStringTokenizer.replace(am.getButtonHtml(), "!CLOSE!",
            BXResources.getGenericMsg("AM_CLOSE_IMAGE", languageId)));

       // -------------- Catherine, 26-Apr-05, GECF, new dialog screen begin --------------
       //Replace all Yes buttons from the buttonHtml
        am.setButtonHtml(BXStringTokenizer.replace(am.getButtonHtml(), "!YES!",
            BXResources.getGenericMsg("AM_YES_IMAGE", languageId)));

        //Replace all Yes buttons from the buttonHtml
        am.setButtonHtml(BXStringTokenizer.replace(am.getButtonHtml(), "!NO!",
            BXResources.getGenericMsg("AM_NO_IMAGE", languageId)));
       // -------------- Catherine, 26-Apr-05, GECF, new dialog screen end --------------

        return am;

     }

     return null;
  }

}


