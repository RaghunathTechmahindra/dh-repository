package com.basis100.deal.security;

import MosSystem.Mc;

class NoDialogMsg extends ActiveMessage {


  public NoDialogMsg(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public NoDialogMsg(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

  public NoDialogMsg()
  {
      common();
  }

  public void common()
  {
       generate   = false;
       hasOK     = false;
       hasCancel = false;
       hasClose  = false;

        ////buttonHtml = Mc.OK_SERVER_EXECUTE_BTN;

        ////responseIdentifier = OK_NAME;
  }

}