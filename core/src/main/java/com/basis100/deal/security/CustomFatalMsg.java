package com.basis100.deal.security;

import MosSystem.Sc;
import MosSystem.Mc;

class CustomFatalMsg extends ActiveMessage {

  public CustomFatalMsg(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public CustomFatalMsg(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

  public CustomFatalMsg()
  {
      common();
  }

  public void common()
  {
      generate  = true;
      hasOK    = true;
      hasCancel= false;
      hasClose = false;

      buttonHtml = Mc.OK_SERVER_EXECUTE_BTN;

      responseIdentifier = OK_NAME;

  }

}