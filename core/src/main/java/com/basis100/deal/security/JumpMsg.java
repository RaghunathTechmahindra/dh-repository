package com.basis100.deal.security;

import MosSystem.Sc;
import MosSystem.Mc;
import com.basis100.picklist.BXResources;

class JumpMsg extends ActiveMessage
{

  //--Release2.1--//
  //--> Added LanguageId for Mulingual Support
  //--> By Billy 10Dec2002
  public JumpMsg(String replacement, String replacement2, int languageId)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common(languageId);
  }

  public JumpMsg(String replacement, int languageId)
  {
      setParmReplace(replacement);
      common(languageId);
  }

  public JumpMsg(int languageId)
  {
      common(languageId);
  }

  public void common(int languageId)
  {
      generate   = true;
      setDialogMsg(BXResources.getSysMsg("CONFIRM_UNSAVED_WORK_JUMP", languageId));
      hasOK     = true;
      hasCancel = true;
      hasClose  = false;
      buttonHtml = Mc.OK_SERVER_EXECUTE_BTN + Mc.SPACER_EXECUTE_BTN + Mc.CANCEL_CLIENT_EXECUTE_BTN;

      responseIdentifier = OK_NAME;

  }

}