package com.basis100.deal.security;

import MosSystem.Mc;

class SubCustomerConfirmMsg extends ActiveMessage {

  public SubCustomerConfirmMsg(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public SubCustomerConfirmMsg(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

  public SubCustomerConfirmMsg()
  {
      common();
  }

  public void common()
  {
       generate   = true;
       hasOK     = false;
       hasCancel = false;
       hasClose  = true;

       buttonHtml = Mc.OK_CLIENT_EXECUTE_BTN;

  }

}