package com.basis100.deal.security;

/**
 * 25/Apr/2007 DVG #DG600 bilingual deal notes project 
 */

import java.io.Serializable;
import java.util.*;

public class PassiveMessage implements Serializable
{
  public static final int NONCRITICAL = 1;
  public static final int CRITICAL    = 0;
  public static final int INFO        = 2;
  public static final int SEPARATOR   = 3;

  private static final Integer nonCriticalObj = new Integer(NONCRITICAL);

  protected boolean generate  = false;
  protected boolean hasOK     = true;

  protected String  title      = null;
  protected String  infoMsg    = null;

  // indexed properties
  protected Vector msgs     = new Vector();
  protected Vector msgTypes = new Vector();
  protected ArrayList<Formattable[]> msgObjs = new ArrayList<Formattable[]>();	//#DG600 store list of formatable objects

  // true if any at leat one message type is CRITICAL;
  private boolean critical = false;

  // used for development purposes
  private boolean exceptionEncountered = false;

  public PassiveMessage()
  {
  }

  public void clearMessages()
  {
    //#DG600 rewritten
//  	msgs     = new Vector();
//  	msgTypes = new Vector();
  	msgs.clear();
  	msgTypes.clear();
  	msgObjs.clear();
    //#DG600 end 
  	critical = false;
  }

  /**
   *  addPmMsg(msg, type) - used instead of setters
   *  type should be one of the constants defined above (top of class)
   *
   **/
  public void addMsg(String msg, int type)
  {
  	//#DG600 unified call
    /*  msgs.add(msg);
      msgTypes.add(new Integer(type));

      if (type == CRITICAL)
          critical = true;
     */
  	addMsg(msg, type, null);
    //#DG600 end 
  }

  public void addMsg(String msg, int type, Formattable ...msgObj)
  {
  	msgs.add(msg);
  	msgTypes.add(new Integer(type));
  	msgObjs.add(msgObj);
  	
  	if (type == CRITICAL)
  		critical = true;
  }
  
  public void addSeparator()
  {
      addMsg("&nbsp;", SEPARATOR);
  }


  /** copyMsgs:
   *
   * Clear existing messages and copy messages from other object
   *
   **/

  public void copyMsgs(PassiveMessage other)
  {
      clearMessages();

      //#DG600 after clear just add all instead!
      addAllMessages(other);
      /*
      if (other == null)
        return;

      Vector messages = other.getMsgs();
      Vector types    = other.getMsgTypes();
      ArrayList<Formattable[]> msgObjsa = other.getMsgsObjs();	//#DG600 store list of formatable objects

      int lim = messages.size();

      for (int i = 0; i < lim; ++i)
      	//#DG600 addMsg((String)messages.elementAt(i), ((Integer)types.elementAt(i)).intValue());
      	addMsg((String)messages.elementAt(i), ((Integer)types.elementAt(i)).intValue(), msgObjsa.get(i));
      	*/
  }


   /* addAllMessages:
   *
   * Add all messages from other object - current messages not affected
   *
   **/

  public void addAllMessages(PassiveMessage other)
  {
      if (other == null)
        return;

      Vector messages = other.getMsgs();
      Vector types    = other.getMsgTypes();
      ArrayList<Formattable[]> msgObjsa = other.getMsgsObjs();	//#DG600 store list of formatable objects

      int lim = messages.size();

      for (int i = 0; i < lim; ++i)
        //#DG600 addMsg((String)messages.elementAt(i), ((Integer)types.elementAt(i)).intValue());
      	addMsg((String)messages.elementAt(i), ((Integer)types.elementAt(i)).intValue(), msgObjsa.get(i));
  }

  /* setAllAsNonCritcal:
   *
   * Ensure all message types are NONCRITICAL and clear critical flag
   *
   **/

  public void setAllAsNonCritcal()
  {
      critical = false;

      if (msgTypes != null)
      {
          int lim =  msgTypes.size();

          Vector newMsgTypes = new Vector(lim);

          for (int i = 0; i < lim; ++i)
          {
            // but leave informational message as is
            if (((Integer)msgTypes.get(i)).intValue() == INFO)
              newMsgTypes.add(msgTypes.get(i));
            else
              newMsgTypes.add(nonCriticalObj);
          }

          msgTypes = newMsgTypes;
      }
  }


  // getters & setters

  public void setGenerate (boolean b)
  {
      generate = b;
  }

  public boolean getGenerate ()
  {
      return generate;
  }

  public void setTitle (String s)
  {
      title = s;
  }
  public String getTitle ()
  {
      return title;
  }

  public void setInfoMsg (String s)
  {
      infoMsg = s;
  }
  public String getInfoMsg ()
  {
      return infoMsg;
  }

  public void setHasOK (boolean b)
  {
      hasOK = b;
  }
  public boolean getHasOK ()
  {
      return hasOK;
  }


  public int getNumMessages()
  {
    return msgs.size();
  }

  public Vector getMsgs()
  {
      return msgs;
  }
  
  //#DG600 
  public final ArrayList<Formattable[]> getMsgsObjs()
  {
  	return msgObjs;
  }
  //#DG600 end 
  
  public String getMsgs(int ndx)
  {
      try
      {
        return (String)msgs.elementAt(ndx);
      }
      catch (Exception e) {;}

      return "Exception - no message for ndx = " + ndx + ".";
  }

  public Vector getMsgTypes()
  {
      return msgTypes;
  }
  public int getMsgTypes(int ndx)
  {
      try
      {
        return ((Integer)msgTypes.elementAt(ndx)).intValue();
      }
      catch (Exception e) {;}

      return 0;
  }

  public boolean getCritical()
  {
      return critical;
  }

  public boolean getExceptionEncountered()
  {
    return exceptionEncountered;
  }

  public void setExceptionEncountered(boolean b)
  {
    exceptionEncountered = b;
  }

}
