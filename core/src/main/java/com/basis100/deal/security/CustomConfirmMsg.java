package com.basis100.deal.security;

import MosSystem.Mc;

class CustomConfirmMsg extends ActiveMessage {

  public CustomConfirmMsg(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public CustomConfirmMsg(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

  public CustomConfirmMsg()
  {
      common();
  }

  public void common()
  {
       generate   = true;
       hasOK     = false;
       hasCancel = false;
       hasClose  = true;

       buttonHtml = Mc.OK_CLIENT_EXECUTE_BTN;

  }

}