package com.basis100.deal.security;

import java.util.Hashtable;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.resources.SessionResourceKit;

import MosSystem.Sc;

public class DealStatusPageAccess
{
  private Hashtable<Integer, Integer> pageStatusMatrix;
  private boolean initProperly;

  public DealStatusPageAccess(SessionResourceKit srk){
    pageStatusMatrix = new Hashtable<Integer, Integer>();
    initProperly = false;
    try{
      generateTable(srk);
      initProperly = true;
    } catch (Exception exc){
      initProperly = false;
    }
  }

  public void refresh(SessionResourceKit srk) throws Exception
  {
    initProperly = false;
    pageStatusMatrix = new Hashtable<Integer, Integer>();
    try{
      generateTable(srk);
    } catch (Exception exc){
      initProperly = false;
      throw new Exception("Could not refresh data from DealStatusPageAccess Table.");
    }
    initProperly = true;
  }

  private void generateTable(SessionResourceKit srk) throws Exception
  {
    JdbcExecutor jexec = srk.getJdbcExecutor();

    String sqlStr = "select pageid, statusid, accesstypeid from dealstatuspageaccess ";

    int lPageId, lStatusId, lAccessTypeId;
    int key =  jexec.execute(sqlStr);
    Integer hashKey;
    for( ; jexec.next(key); ){
      lPageId = jexec.getInt(key,"PAGEID");
      lStatusId = jexec.getInt(key,"STATUSID");
      lAccessTypeId = jexec.getInt(key,"ACCESSTYPEID");
      hashKey = new Integer( lPageId + ( lStatusId << 16 ) );
      pageStatusMatrix.put( hashKey, new Integer(lAccessTypeId) );
    }
    jexec.closeData(key);
  }

  public int check(int pPageID, int pStatusID)  throws Exception
  {
    if (initProperly == false){
      throw new Exception("Hashtable was not properly initialized.");
    }
    Integer hashKey = new Integer( pPageID + (pStatusID << 16) );
    Object lObj = pageStatusMatrix.get(hashKey);
    return lObj == null ? Sc.PAGE_ACCESS_UNASSIGNED : ((Integer)lObj).intValue();
  }

}