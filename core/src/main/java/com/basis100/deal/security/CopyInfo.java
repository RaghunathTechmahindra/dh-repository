package com.basis100.deal.security;

import java.io.Serializable;

public class CopyInfo implements Serializable
{
  protected int         dealId;
  protected int         copyId;
  protected boolean     goldCopy;
  protected String      copyType;

    CopyInfo(int dealId, int copyId, boolean goldCopy, String copyType)
    {
      this.dealId = dealId;
      this.copyId = copyId;
      this.goldCopy = goldCopy;
      this.copyType = copyType;
    }

    public int getDealId()  { return dealId; }

    public int getCopyId()  { return copyId; }
    public void setCopyId(int copyId)  { this.copyId = copyId; }

    public boolean isGoldCopy()  { return goldCopy; }
    public String getCopyType() { return copyType; }
}
