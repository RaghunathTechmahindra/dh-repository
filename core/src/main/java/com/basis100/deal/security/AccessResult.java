package com.basis100.deal.security;

public class AccessResult implements java.io.Serializable
{
  private int accessTypeID;
  private String accessMessage;

  public AccessResult(int pAccessType){
    accessTypeID = pAccessType;
    accessMessage = null;
  }

  public AccessResult(int pAccessType, String pMsg) {
    accessTypeID = pAccessType;
    accessMessage = new String(pMsg);
  }

  public void setAccessTypeID(int pAccessType){
    accessTypeID = pAccessType;
  }

  public void setAccessMessage(String pMsg){
    accessMessage = new String(pMsg);
  }

  public int getAccessTypeID(){
    return accessTypeID;
  }

  public String getAccessMessage(){
    return accessMessage;
  }
}