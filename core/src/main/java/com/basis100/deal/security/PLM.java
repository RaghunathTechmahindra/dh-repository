package com.basis100.deal.security;

import java.util.Calendar;

import MosSystem.Mc;

import com.basis100.deal.entity.UserProfile;
import com.basis100.deal.pk.UserProfileBeanPK;
import com.basis100.deal.util.TypeConverter;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;

import com.basis100.resources.SessionResourceKit;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PLM class is always used singleton, no state can be stored using instance variable.
 */
public class PLM
  implements Cloneable
{
	private final static Logger  logger = LoggerFactory.getLogger(PLM.class);
	
  
  private static PLM instance;
  
  /**
   * empty constructor
   */
  private PLM()
  {
  }
  
  /**
   * init
   * @param srk SessionResourceKit
   * @throws Exception
   */
  public static void init(SessionResourceKit srk) throws Exception
  {
    init(srk, true);
  }
  
  
  /**
   * init
   * @param srk SessionResourceKit
   * @param clearAllLocks boolean
   * @throws Exception
   */
  public static void init(SessionResourceKit srk, boolean clearAllLocks) throws Exception
  {

    // To ensure lock table empty
    try
    {
      // This method is called MosSystemServlet to clear all page locks  
      if(clearAllLocks == true)
      {
        srk.beginTransaction();

        //delete remaining lock data except the records created 
        //by stand alone programs (e.g, AME, Ingestion, etc)
        srk.getJdbcExecutor().executeUpdate(
        		"DELETE FROM pagelocks pl " +
        		" WHERE EXISTS ( " +
        		"  SELECT 1 " +
        		"  FROM userprofile up " +
        		"  WHERE up.institutionprofileid = pl.institutionprofileid " +
        		"  AND up.userprofileid = pl.userprofileid " +
        		"  AND up.usertypeid between " + 
        		Mc.USER_TYPE_PASSIVE + " and " + Mc.USER_TYPE_SPECIAL + " )");
        
        srk.commitTransaction();
      }

      // instantiate PLM
      instance = new PLM();
    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      String msg = "@PLM.init() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg);
    }
  }
  

  /**
   * getInstance
   * @throws Exception
   * @return PLM
   */
  public static PLM getInstance() throws Exception
  {
    try
    {
      if(instance == null)
    	instance = new PLM();
      return instance;
    }
    catch(Exception e)
    {
      String msg = "@PLM.getInstance() exception = " + e.getMessage();
      logger.error(msg);
      
      throw new Exception(msg);
    }
  }

  
  /**
   * getPageLockedUserDetails
   * This method gets User details for locked page
   * @param pageId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return String 
   * @author 
   */
  public String getPageLockedUserDetails(
    int pageId,
    int userProfileId,
    SessionResourceKit srk) throws Exception
  {
    StringBuilder  sql = new StringBuilder();
    sql.append("SELECT * FROM PAGELOCKS WHERE USERPROFILEID <> " )
    .append( userProfileId )
    .append(" AND PAGEID = " )
    .append( pageId )
    .append(" AND INSTITUTIONPROFILEID = " )
    .append( srk.getExpressState().getUserInstitutionId() );
    
    logger.debug("@PLM.getPageLockedUserDetails()- sql Query "+ sql.toString());
    String lockedUserInfo = "";
    
    try
    {
    	 JdbcExecutor jExec = srk.getJdbcExecutor();
         int key = jExec.execute(sql.toString());
         
      if(srk.getJdbcExecutor().next(key))
      {
    	  lockedUserInfo = getUserName(jExec.getInt(key, 2), jExec.getInt(key, 6), srk);
      }

      srk.getJdbcExecutor().closeData(key);

    }
    catch(Exception e)
    {
      
      String msg = "@PLM.getPageLockedUserDetails() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg, e);
    }

    return lockedUserInfo;
  }
  
  /**
   * getPageLockTimeStamp
   * This method TimeStamp of locked page.
   * @param pageId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return String 
   * @author 
   */
  public String getPageLockTimeStamp(
    int pageId,
    int userProfileId,
    SessionResourceKit srk) throws Exception
  {
    StringBuilder  sql = new StringBuilder();
    sql.append("SELECT * FROM PAGELOCKS WHERE USERPROFILEID <> " )
    .append( userProfileId )
    .append(" AND PAGEID = " )
    .append( pageId )
    .append(" AND INSTITUTIONPROFILEID = " )
    .append( srk.getExpressState().getUserInstitutionId() );
    
    logger.debug("@PLM.getPageLockTimeStamp()- sql Query "+ sql.toString());
    String timestampInfo = "";
    
    try
    {
    	 JdbcExecutor jExec = srk.getJdbcExecutor();
         int key = jExec.execute(sql.toString());
         
      if(srk.getJdbcExecutor().next(key))
      {
    	  timestampInfo = jExec.getString(key, 4);
      }

      srk.getJdbcExecutor().closeData(key);

    }
    catch(Exception e)
    {
      String msg = "@PLM.getPageLockTimeStamp() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg, e);
    }

    return timestampInfo;
  }
  
  
  /**
   * isLocked
   * This method checks for locking of page by user for selected institution.
   * @param pageId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return boolean
   * @author 
   */
  public boolean isLocked(
    int pageId,
    int userProfileId,
    SessionResourceKit srk) throws Exception
  {
    StringBuilder  sql = new StringBuilder();
    sql.append("SELECT * FROM PAGELOCKS WHERE USERPROFILEID <> " )
    .append( userProfileId )
    .append(" AND PAGEID = " )
    .append( pageId )
    .append(" AND INSTITUTIONPROFILEID = " )
    .append( srk.getExpressState().getUserInstitutionId() );
    
    logger.debug("@PLM.isLocked()- sql Query "+ sql.toString());
    boolean gotRecord = false;
    
    try
    {

      int key = srk.getJdbcExecutor().execute(sql.toString());

      if(srk.getJdbcExecutor().next(key))
      {
        gotRecord = true;
      }

      srk.getJdbcExecutor().closeData(key);

    }
    catch(Exception e)
    {
      
      String msg = "@PLM.isLocked() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg, e);
    }

    return gotRecord;
  }

  /**
   * isLockedByUser
   * @param PAGEID int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return boolean true if specified page locked by the input user. else return false.
   */
  public boolean isLockedByUser(
    int pageId,
    int userProfileId,
    SessionResourceKit srk) throws Exception
  {
    StringBuffer sql = new StringBuffer();
    
    sql.append("SELECT * FROM PAGELOCKS WHERE USERPROFILEID = ")
    .append( userProfileId )
    .append( " AND PAGEID = " )
    .append( pageId )
    .append( " AND INSTITUTIONPROFILEID = " )
    .append( srk.getExpressState().getUserInstitutionId() );
    
    logger.debug("@PLM.isLockedByUser() - sql Query:" + sql.toString());
    
    boolean gotRecord = false;

    try
    {
      int key = srk.getJdbcExecutor().execute(sql.toString());

      if(srk.getJdbcExecutor().next(key))
      {
        gotRecord = true;
      }

      srk.getJdbcExecutor().closeData(key);

    }
    catch(Exception e)
    {
      String msg = "@PLM.isLockedByUser() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg);
    }

    return gotRecord;
  }

  /**
   * lock
   * Locking the page for single user access 
   * @param PAGEID int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @author 
   */
  public synchronized void lock(
    int pageId,
    int userProfileId,
    SessionResourceKit srk,
    String sessionId ) throws Exception
  {

    try
    {
      // Check if Locked by other users
      if(isLocked(pageId, userProfileId, srk) == true)
      {
        throw new Exception(
          // already locked by specified user - not considered exception condition
          "Page locked by some other user, page id = " + pageId);
      }
      // Check if locked by Current User
      // If so, update lockTime to current time.
      String date = null; 
      StringBuilder  sql = null;
      if(isLockedByUser(pageId, userProfileId, srk) == true)
      {
    	//  Should refresh the locktime to sysdate
    	  Date d = Calendar.getInstance().getTime();
    	  date = sqlStringFrom(d);
    	  sql = new StringBuilder();
          sql.append( "UPDATE PAGELOCKS SET LOCKTIME = " )
          .append( date )
          .append( " WHERE PAGEID = " )
          .append(  pageId )
          .append( " AND USERPROFILEID = " )
          .append(  userProfileId )
          .append( " AND INSTITUTIONPROFILEID = " )
          .append( srk.getExpressState().getUserInstitutionId() );
          logger.debug("@PLM.lock() - sql Query: "+ sql.toString()); 
          srk.getJdbcExecutor().executeUpdate(sql.toString());
      } else {
    			sql = new StringBuilder();
    			sql.append("INSERT INTO PAGELOCKS (PAGEID, USERPROFILEID, LOCKTYPE, INSTITUTIONPROFILEID, SESSIONID) VALUES(" )
    	          .append( pageId )
    	          .append(" , ")
    			  .append( userProfileId )
    	          .append( ", 'E' ," )
    	          .append( srk.getExpressState().getUserInstitutionId() )
    	          .append(" , '")
    			  .append( sessionId)
    			  .append("')");
    			srk.getJdbcExecutor().executeUpdate(sql.toString());
      }  
    }
    catch(Exception e)
    {
    	String msg = "@PLM.lock() exception = " + e.getMessage();
    	logger.error(msg);
    	throw new Exception(msg);
    }
  }
  
  /**
   * Converts Date to String.
   * @param d Date
   * @return
   */
  private String sqlStringFrom(java.util.Date d)
  {
    String strRep = TypeConverter.stringTypeFrom(d);

    if(strRep == null)
    {
      return "null";
    }
    else
    {
      return "\'" + strRep + "\'";
    }
  }
  
  /**
   * Unlock the page for  user and institutionid 
   * @param pageId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param callInTransaction boolean
   * @throws Exception
   * @author 
   */
  public void unlock(
    int pageId,
    int userProfileId,
    SessionResourceKit srk,
    boolean callInTransaction,
    String sessionId) throws Exception
  {
    try
    {
   	
      if(callInTransaction == false)
      {
        srk.beginTransaction();
      }
      JdbcExecutor jExec = srk.getJdbcExecutor();
      StringBuilder sql = new StringBuilder();
      
      sql.append(" DELETE FROM PAGELOCKS WHERE PAGEID = " )
      .append( pageId )
      .append( " AND USERPROFILEID = " )
      .append( userProfileId)
      .append( " AND INSTITUTIONPROFILEID = " )
      .append( srk.getExpressState().getUserInstitutionId())
      .append( " AND SESSIONID = '")
      .append( sessionId)
      .append( "'");
           
      logger.debug("@PLM.unlock callInTransaction - Sql Query:"+ sql.toString());
      
      jExec.executeUpdate(sql.toString());
       
      if(callInTransaction == false)
      {
        srk.commitTransaction();
      }
    }
    catch(Exception e)
    {
      if(callInTransaction == false)
      {
        srk.cleanTransaction();
      }
      
      String msg = "@PLM.unlock() - callInTransaction exception = " + e.getMessage();
      logger.error(msg);

      throw new Exception(msg);
    }
  }
 
 
  /**
   * unlockAnyByUser
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param callInTransaction boolean
   * @throws Exception
   */
  public void unlockAnyByUser(
    int userProfileId,
    SessionResourceKit srk,
    boolean callInTransaction) throws Exception
  {
    try
    {

      if(callInTransaction == false)
      {
        srk.beginTransaction();

      }
      JdbcExecutor jExec = srk.getJdbcExecutor();

      jExec.executeUpdate(
        "DELETE FROM PAGELOCKS WHERE USERPROFILEID = " + userProfileId);

      if(callInTransaction == false)
      {
        srk.commitTransaction();
      }
    }
    catch(Exception e)
    {
      if(callInTransaction == false)
      {
        srk.cleanTransaction();

      }
      
      String msg = "@PLM.unlockAnyByUser() exception = " + e.getMessage();
      logger.error(msg);

      throw new Exception(msg);
    }
  }

  /**
   * getUserName
   * get user full name for given user profile id
   * @param userProfileId int
   * @param institutionId int
   * @param srk SessionResourceKit
   * @throws Exception
   */
  private String getUserName(int userprofileid, int institutionId, SessionResourceKit srk)
  {
      String username = "";

      try
      {        	
          UserProfile up = new UserProfile(srk)
                  .findByPrimaryKey(new UserProfileBeanPK(userprofileid, institutionId));
          username = up.getUserFullName();
      } catch (Exception e)
      {
          logger.error("Exception occurred PLM@getUserName() - error details:" + e.getMessage());
      }

      return username;
  }
  
  
  /**
   * unlockAnyBySessionID
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param callInTransaction boolean
   * @throws Exception
   */
  public void unlockAnyBySessionID(
    SessionResourceKit srk,
    boolean callInTransaction,
    String SessionID
    ) throws Exception
  {
    try
    {
      
      if(callInTransaction == false)
      {
        srk.beginTransaction();

      }
      JdbcExecutor jExec = srk.getJdbcExecutor();

      int ret = jExec.executeUpdate(
        "DELETE FROM PAGELOCKS WHERE SESSIONID = '" + SessionID + "'");

      if(callInTransaction == false)
      {
        srk.commitTransaction();
      }
    }
    catch(Exception e)
    {
      if(callInTransaction == false)
      {
        srk.cleanTransaction();

      }
      
      String msg = "@PLM.unlockAnyBySessionID() exception = " + e.getMessage();
      logger.error(msg);

      throw new Exception(msg);
    }
  }
}
