package com.basis100.deal.security;

import java.io.Serializable;

public class JumpState implements Serializable
{
  public static final int GOTO              = 1;
  public static final int PREVIOUSSCREENS   = 2;
  public static final int SEARCH            = 3;
  public static final int HELP              = 4;
  public static final int LOGOFF            = 5;
  public static final int TOOLS             = 6;
  public static final int HISTORY           = 7;
  public static final int NOTES             = 8;
  public static final int WORKQUEUE         = 9;
  public static final int TASKNEXTPAGE      = 10;
  public static final int TASKPREVPAGE      = 11;
  public static final int UNCONFIRMEDCANCEL = 12;
  // New Jump type for Exit screen but navigate to next workflow screen
  //  -- Billy 07May2002
  public static final int UNCONFIRMEDEXIT   = 13;

  private int jumpType;
  private int gotoDealId;
  private int gotoPageId;
  private int previousScreenNdx;
  private int toolNdx;

  public JumpState(int jumpType, int dealId, int pageId, int previousNdx, int toolNdx)
  {
      this.jumpType = jumpType;
      this.gotoDealId = dealId;
      this.gotoPageId = pageId;
      this.previousScreenNdx = previousNdx;
      this.toolNdx = toolNdx;
  }

  // getters (setters not required)

  public int getJumpType()
  {
    return jumpType;
  }

  public int getGotoDealId()
  {
    return gotoDealId;
  }

  public int getGotoPageId()
  {
    return gotoPageId;
  }

  public int getPreviousNdx()
  {
    return previousScreenNdx;
  }

  public int getToolNdx()
  {
    return toolNdx;
  }
}
