package com.basis100.deal.security;

import MosSystem.Sc;
import MosSystem.Mc;
import com.basis100.picklist.BXResources;

class PageDeniedMsg extends ActiveMessage {

  //--Release2.1--//
  //--> Added LanguageId for Mulingual Support
  //--> By Billy 10Dec2002
  public PageDeniedMsg(String replacement, String replacement2, int languageId)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common(languageId);
  }

  public PageDeniedMsg(String replacement, int languageId)
  {
      setParmReplace(replacement);
      common(languageId);
  }

  public PageDeniedMsg(int languageId)
  {
      common(languageId);
  }

  public void common(int languageId)
  {
       generate   = true;
       setDialogMsg(BXResources.getSysMsg("PAGE_DENIED_DEFAULT", languageId));
       hasOK     = false;
       hasCancel = false;
       hasClose  = true;
       buttonHtml = Mc.OK_CLIENT_EXECUTE_BTN;

  }

}