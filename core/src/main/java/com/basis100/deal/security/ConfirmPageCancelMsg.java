package com.basis100.deal.security;

import MosSystem.Sc;
import MosSystem.Mc;
import com.basis100.picklist.BXResources;

class ConfirmPageCancelMsg extends ActiveMessage{

  //--Release2.1--//
  //--> Added LanguageId for Mulingual Support
  //--> By Billy 10Dec2002
  public ConfirmPageCancelMsg(int type, String replacement, String replacement2, int languageId)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common(type, languageId);
  }

  public ConfirmPageCancelMsg(int type, String replacement, int languageId)
  {
      setParmReplace(replacement);
      common(type, languageId);
  }

  public ConfirmPageCancelMsg(int type, int languageId)
  {
      common(type, languageId);
  }

  public void common(int type, int languageId)
  {
      generate   = true;

      if (type == ActiveMsgFactory.ISDEALENTRYCONFIRMCANCEL || type == ActiveMsgFactory.ISDEALENTRYCONFIRMEXIT)
        setDialogMsg(BXResources.getSysMsg("CONFIRM_CANCEL_DEAL_ENTRY", languageId));
      else
        setDialogMsg(BXResources.getSysMsg("CONFIRM_CANCEL_CHANGES", languageId));

      hasOK     = true;
      hasCancel = true;
      hasClose  = false;

      buttonHtml = Mc.OK_SERVER_EXECUTE_BTN + Mc.SPACER_EXECUTE_BTN + Mc.CANCEL_CLIENT_EXECUTE_BTN;

      responseIdentifier = OK_NAME;
  }

}
