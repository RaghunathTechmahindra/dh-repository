package com.basis100.deal.security;

import MosSystem.Mc;

class CustomDialogMsg extends ActiveMessage
  {
  public CustomDialogMsg(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public CustomDialogMsg(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

   public CustomDialogMsg()
   {
      common();
   }

  public void common()
  {
        generate   = true;
        hasOK     = true;
        hasCancel = true;
        hasClose  = false;

        buttonHtml = Mc.OK_SERVER_EXECUTE_BTN + Mc.SPACER_EXECUTE_BTN + Mc.CANCEL_CLIENT_EXECUTE_BTN;

        responseIdentifier = OK_NAME;

  }

  }