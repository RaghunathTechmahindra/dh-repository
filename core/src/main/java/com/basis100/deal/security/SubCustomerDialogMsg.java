package com.basis100.deal.security;

import MosSystem.Mc;

class SubCustomerDialogMsg extends ActiveMessage
  {
  public SubCustomerDialogMsg(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public SubCustomerDialogMsg(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

   public SubCustomerDialogMsg()
   {
      common();
   }

  public void common()
  {
        generate   = true;
        hasOK     = true;
        hasCancel = true;
        hasClose  = false;

        buttonHtml = Mc.OK_SERVER_EXECUTE_BTN + Mc.SPACER_EXECUTE_BTN + Mc.CANCEL_CLIENT_EXECUTE_BTN;

        responseIdentifier = OK_NAME;

  }

  }