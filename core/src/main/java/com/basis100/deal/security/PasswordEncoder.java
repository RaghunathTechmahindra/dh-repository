package com.basis100.deal.security;
/**
 * Title:        MOS Core Development
 * Description:  Billy's Local Working Copy
 * Copyright:    Copyright (c) 2002
 * Company:      Basis100
 * @author Billy Lam
 * @version
 */

import java.security.*;
import java.util.*;

public class PasswordEncoder
{
  private static final String DEFAULT_ALGORITHM = "SHA";
  //private static final String DEFAULT_ALGORITHM = "MD5";
  private static MessageDigest md = null;

  public PasswordEncoder()
  {
  }

  public static byte[] decode(String base64) {
		int pad = 0;
		for (int i = base64.length() - 1; base64.charAt(i) == '='; i--)
			pad++;
		int length = base64.length() * 6 / 8 - pad;
		byte[] raw = new byte[length];
		int rawIndex = 0;
		for (int i = 0; i < base64.length(); i += 4) {
			int block =
				(getValue(base64.charAt(i)) << 18)
					+ (getValue(base64.charAt(i + 1)) << 12)
					+ (getValue(base64.charAt(i + 2)) << 6)
					+ (getValue(base64.charAt(i + 3)));
			for (int j = 0; j < 3 && rawIndex + j < raw.length; j++)
				raw[rawIndex + j] = (byte) ((block >> (8 * (2 - j))) & 0xff);
			rawIndex += 3;
		}
		return raw;
	}

	public static String encode(byte[] raw) {
		StringBuffer encoded = new StringBuffer();
		for (int i = 0; i < raw.length; i += 3) {
			encoded.append(encodeBlock(raw, i));
		}
		return encoded.toString();
	}

	protected static char[] encodeBlock(byte[] raw, int offset) {
		int block = 0;
		int slack = raw.length - offset - 1;
		int end = (slack >= 2) ? 2 : slack;
		for (int i = 0; i <= end; i++) {
			byte b = raw[offset + i];
			int neuter = (b < 0) ? b + 256 : b;
			block += neuter << (8 * (2 - i));
		}
		char[] base64 = new char[4];
		for (int i = 0; i < 4; i++) {
			int sixbit = (block >>> (6 * (3 - i))) & 0x3f;
			base64[i] = getChar(sixbit);
		}
		if (slack < 1)
			base64[2] = '=';
		if (slack < 2)
			base64[3] = '=';
		return base64;
	}

	protected static char getChar(int sixBit) {
		if (sixBit >= 0 && sixBit <= 25)
			return (char) ('A' + sixBit);
		if (sixBit >= 26 && sixBit <= 51)
			return (char) ('a' + (sixBit - 26));
		if (sixBit >= 52 && sixBit <= 61)
			return (char) ('0' + (sixBit - 52));
		if (sixBit == 62)
			return '+';
		if (sixBit == 63)
			return '/';
		return '?';
	}

	protected static int getValue(char c) {
		if (c >= 'A' && c <= 'Z')
			return c - 'A';
		if (c >= 'a' && c <= 'z')
			return c - 'a' + 26;
		if (c >= '0' && c <= '9')
			return c - '0' + 52;
		if (c == '+')
			return 62;
		if (c == '/')
			return 63;
		if (c == '=')
			return 0;
		return -1;
	}

  /**
	 * @return an encoded digest
	 * @exception java.security.NoSuchAlgorithmException
	 * @param param:a String parameter to generate the digest
	 */
	public static String getEncodedPassword(String param)
  {
    try{
		  return PasswordEncoder.encode(passwordGen(param, ""));
    }
    catch(Exception e)
    {
      return "";
    }
	}
  public static String getEncodedPassword(String param, String userId)
	{
    try{
		  return PasswordEncoder.encode(passwordGen(param, userId));
    }
    catch(Exception e)
    {
      return "";
    }
	}

  /**
	 * @param param: the parameter to be used to generate digest
	 * @return an digest
	 * @exception java.security.NoSuchAlgorithmException
	 */
	public static byte[] passwordGen(String param, String UserId)
		throws NoSuchAlgorithmException {
		String[] strs = new String[2];
		strs[0] = param;
    strs[1] = UserId;
		return passwordGen(strs);
	}

  /**
	 * @return an digest
	 * @exception java.security.NoSuchAlgorithmException
	 * @param params: an array of String parameters to  generate the digest
	 */
	public static byte[] passwordGen(String[] params)
		throws NoSuchAlgorithmException {

		ArrayList ls = new ArrayList(params.length);
		for (int i = 0; i < params.length; i++) {
			ls.add(params[i].getBytes());
		}
		return passwordGen(ls);
	}

  /**
	 * @return a digest
	 * @exception java.security.NoSuchAlgorithmException
	 * @param params: list of parameters in byte array to generate the digest
	 */
	public static byte[] passwordGen(List params) throws NoSuchAlgorithmException {
		return passwordGen(DEFAULT_ALGORITHM, params);
	}

  /**
	 * @return a digest
	 * @exception java.security.NoSuchAlgorithmException
	 * @param algorithm: algorith name to be used to generate the digest
	 * @param params: list of parameters in byte array to be used to generate the digest
	 */
	public static byte[] passwordGen(String algorithm, List params)
		throws NoSuchAlgorithmException {

		md = MessageDigest.getInstance(algorithm);
		Iterator it = params.iterator();
		while (it.hasNext()) {
			md.update((byte[]) it.next());
		}
		return md.digest();
	}

  /**
	 * @param pw1:the first encoded digest
	 * @param pw2:the second encoded digest
	 * @return are they the same
	 */
	public static boolean isIdentical(String pw1, String pw2) {
		byte[] p1 = PasswordEncoder.decode(pw1.trim());
		byte[] p2 = PasswordEncoder.decode(pw2.trim());
		return MessageDigest.isEqual(p1, p2);
	}

}