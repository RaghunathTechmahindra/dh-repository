package com.basis100.deal.security;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import MosSystem.Sc;

import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLogger;
import com.basis100.picklist.BXResources;
import com.basis100.resources.ResourceManager;
import com.basis100.resources.SessionResourceKit;


public class DealStatusManager  implements Cloneable{

  private static DealStatusManager instance;
  private boolean ready;

  private Map<Integer, DealStatusGotoExceptionPageAcc> dspAccessGotoExceptionMap;
  private Map<Integer, DealStatusPageAccess> dspAccessMap;
  private Map<Integer, Hashtable<Integer, String>> statusDescriptionMap;
  private Map<Integer, Hashtable<Integer, Integer>> statusCategorieMap;

  private DealStatusManager(SessionResourceKit srk){
    ready = false;

    dspAccessGotoExceptionMap = new HashMap<Integer, DealStatusGotoExceptionPageAcc>();
    dspAccessMap = new HashMap<Integer, DealStatusPageAccess>();
    statusDescriptionMap = new HashMap<Integer, Hashtable<Integer, String>>();
    statusCategorieMap = new HashMap<Integer, Hashtable<Integer, Integer>>();
    
    try {
        load(srk);
    }
    catch (Exception e)
    {
        SysLogger logger = srk.getSysLogger();

        logger.error("Failure initializing DSM");
        logger.error(e);

        // leave in non-ready state
        return;
    }
    ready = true;
  }
  
  /*
   * load DealStatusPageAccess, DealStatusGotoExceptionPageAcc by the srk. 
   * i.e. for one institution
   */
  private void load(SessionResourceKit srk) throws Exception {
      
      Integer institutionId = new Integer(srk.getExpressState().getDealInstitutionId());
      
      DealStatusPageAccess dspAccess = new DealStatusPageAccess(srk);
      dspAccessMap.put(institutionId, dspAccess);
      
      DealStatusGotoExceptionPageAcc dspAccessGotoException 
          = new DealStatusGotoExceptionPageAcc(srk);
      dspAccessGotoExceptionMap.put(institutionId, dspAccessGotoException);

    // get deal status codes details

      Hashtable<Integer, String> statusDescriptions  = new Hashtable<Integer, String>();
      Hashtable<Integer, Integer> statusCategories    = new Hashtable<Integer, Integer>();

    JdbcExecutor jExec = srk.getJdbcExecutor();
    String sql = "select * from status order by statuscategoryid";

    int key = jExec.execute(sql);

    while (jExec.next(key))
    {
        Integer SID = new Integer(jExec.getInt(key, 1));

        statusDescriptions.put(SID, jExec.getString(key, 2));
        statusCategories.put(SID,   new Integer(jExec.getInt(key, 3)));
    }
    jExec.closeData(key);
    // added for ml
    statusDescriptionMap.put(institutionId, statusDescriptions);
    statusCategorieMap.put(institutionId, statusCategories);

    ready = true;
  }

  public static DealStatusManager getInstance(SessionResourceKit srk){
    if(instance == null){
      instance = new DealStatusManager(srk);
    }
    return instance;
  }

  public void refresh(SessionResourceKit srk) throws Exception
  {      
      Integer institutionId = new Integer(srk.getExpressState().getDealInstitutionId());

    ready = false;
    try{
        DealStatusPageAccess dspAccess = (DealStatusPageAccess)dspAccessMap.get(institutionId);
        DealStatusGotoExceptionPageAcc dspAccessGotoException 
            = (DealStatusGotoExceptionPageAcc)dspAccessGotoExceptionMap.get(institutionId);
        if (dspAccess == null || dspAccessGotoException == null) {
            load(srk);
        } else {
            if (dspAccess != null)
                dspAccess.refresh(srk);
            if (dspAccessGotoException != null)
                dspAccessGotoException.refresh(srk);
        }              
    } catch (Exception exc){
      throw exc;
    }
    ready = true;
  }

  //--Release2.1--//
  //--> Added LanguageId to support multiLingual
  //--> By Billy 10Dec2002
  public AccessResult pageAccess(int pPageID, int pStatusID, int pDecisionModTypeID, boolean isGoto, int languageId, SessionResourceKit srk) throws Exception
  {
    Thread sleepThread = new Thread();
    int sleepTime = 0;
    while(ready == false){
      sleepThread.sleep(1000);
      sleepTime += 1000;
      if(sleepTime > 20000){
        throw new Exception("Access result could not be obtained for too long.");
      }
    }
    
    Integer institutionId = new Integer(srk.getExpressState().getDealInstitutionId());
    DealStatusPageAccess dspAccess = (DealStatusPageAccess)dspAccessMap.get(institutionId);
    DealStatusGotoExceptionPageAcc dspAccessGotoException 
        = (DealStatusGotoExceptionPageAcc)dspAccessGotoExceptionMap.get(institutionId);
    if (dspAccessGotoException == null || dspAccess == null) {
        load(srk);
        dspAccess = (DealStatusPageAccess)dspAccessMap.get(institutionId);
        dspAccessGotoException 
            = (DealStatusGotoExceptionPageAcc)dspAccessGotoExceptionMap.get(institutionId);
    }    
    
    int lRes = Sc.PAGE_ACCESS_UNASSIGNED;

    // check for goto exception page access definition
    if (isGoto == true && dspAccessGotoException != null)
      lRes = dspAccessGotoException.check(pPageID, pStatusID);

    // chech default (including no exception definition in goto case) access
    if (lRes == Sc.PAGE_ACCESS_UNASSIGNED)
      lRes = dspAccess.check(pPageID, pStatusID);

    AccessResult result = new AccessResult(lRes);
    switch (lRes) {
      case Sc.PAGE_ACCESS_DISALLOWED :
        {
        result.setAccessMessage(BXResources.getSysMsg("PAGE_ACCESS_DENIED", languageId));
        break;
        }
      case Sc.PAGE_ACCESS_SUSPENDED :
        {
        result.setAccessMessage(BXResources.getSysMsg("PAGE_ACCESS_DENIED", languageId));
        break;
        }
      case Sc.PAGE_ACCESS_UNASSIGNED :
        {
        result.setAccessMessage(BXResources.getSysMsg("PAGE_ACCESS_ERROR", languageId));
        break;
        }
      case Sc.PAGE_ACCESS_EDIT_ON_DECISION_MOD : {
        switch(pDecisionModTypeID){
          case 0 :{result.setAccessTypeID(Sc.PAGE_ACCESS_VIEW_ONLY); break;}
          case 2 :{result.setAccessTypeID(Sc.PAGE_ACCESS_EDIT); break;}
          case 3 :{result.setAccessTypeID(Sc.PAGE_ACCESS_EDIT); break;}
          default :{
            result.setAccessTypeID(Sc.PAGE_ACCESS_UNASSIGNED);
            result.setAccessMessage(BXResources.getSysMsg("PAGE_ACCESS_ERROR", languageId));
            break;
          }
        } // end switch(pDecisionModTypeID)
        break;
      } // end case Sc.PAGE_ACCESS_EDITONDECISIONMOD
    }  // end switch(lRes)
    return result;
  }

  public String getStatusDescription(int statusId, SessionResourceKit srk)
  {
      Integer institutionId = new Integer(srk.getExpressState().getDealInstitutionId());
      Hashtable<Integer, String> statusDescriptions 
          = (Hashtable<Integer, String>)statusDescriptionMap.get(institutionId);
      String desc = null;
      if (statusDescriptions == null) {
          try {
              load(srk);
              statusDescriptions = (Hashtable<Integer, String>)statusDescriptionMap.get(institutionId);
          } catch (Exception e) {
              // do nothing
          }
      }
      if (statusDescriptions != null)
          desc = (String)statusDescriptions.get(new Integer(statusId));

      if (desc == null)
        return "(" + statusId + ") Unknown Deal Status Code";

      return desc;
  }

  public int getStatusCategory(int statusId, SessionResourceKit srk)
  {
      Integer institutionId = new Integer(srk.getExpressState().getDealInstitutionId());
      Hashtable<Integer, Integer> statusCategories 
          = (Hashtable<Integer, Integer>)statusCategorieMap.get(institutionId);
      Integer catCode = null;
      if (statusCategories == null) {
          try {
              load(srk);
              statusCategories = (Hashtable<Integer, Integer>)statusCategorieMap.get(institutionId);
          } catch(Exception e) {
              // do nothing
          }
      }
      if (statusCategories != null)
          catCode = (Integer)statusCategories.get(new Integer(statusId));

      if (catCode == null)
        return 0; // assume pre-decision

      return catCode.intValue();
  }

}