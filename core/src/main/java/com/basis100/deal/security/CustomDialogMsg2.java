package com.basis100.deal.security;

import MosSystem.Mc;

class CustomDialogMsg2 extends ActiveMessage
  {

  public CustomDialogMsg2(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public CustomDialogMsg2(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

   public CustomDialogMsg2()
   {
    common();
   }

  public void common()
  {
        generate   = true;
        hasOK     = true;
        hasCancel = true;
        hasClose  = false;

        buttonHtml = Mc.OK_SERVER_EXECUTE_BTN + Mc.SPACER_EXECUTE_BTN + Mc.CANCEL_SERVER_EXECUTE_BTN;

        responseIdentifier = OK_NAME;
        responseIdentifier2 = CANCEL_NAME;

  }

  }