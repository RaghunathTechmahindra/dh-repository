package com.basis100.deal.security;

import com.basis100.resources.*;
import java.util.Hashtable;
import MosSystem.Sc;
import com.basis100.jdbcservices.*;
import com.basis100.jdbcservices.jdbcexecutor.*;

public class DealStatusGotoExceptionPageAcc
{
  private Hashtable pageStatusMatrix;
  private boolean initProperly;

  public DealStatusGotoExceptionPageAcc(SessionResourceKit srk) throws Exception
  {
    initProperly = false;
    pageStatusMatrix = new Hashtable();
    try
    {
      generateTable(srk);
      initProperly = true;
    }
    catch (Exception exc)
    {
      initProperly = false;
      throw exc;
    }
  }

  public void refresh(SessionResourceKit srk) throws Exception
  {
    initProperly = false;
    pageStatusMatrix = new Hashtable();
    try
    {
      generateTable(srk);
    }
    catch (Exception exc)
    {
      initProperly = false;
      throw new Exception("Could not refresh data from DealStatusGotoExceptionPageAcc Table.");
    }

    initProperly = true;
  }

  private void generateTable(SessionResourceKit srk) throws Exception
  {
    JdbcExecutor jexec = srk.getJdbcExecutor();

    String sqlStr = "select pageid, statusid, accesstypeid from DealStatusGotoExceptionPageAcc ";

    int lPageId, lStatusId, lAccessTypeId;
    int key =  jexec.execute(sqlStr);
    Integer hashKey;
    for( ; jexec.next(key); )
    {
      lPageId = jexec.getInt(key,"PAGEID");
      lStatusId = jexec.getInt(key,"STATUSID");
      lAccessTypeId = jexec.getInt(key,"ACCESSTYPEID");
      hashKey = new Integer( lPageId + ( lStatusId << 16 ) );
      pageStatusMatrix.put( hashKey, new Integer(lAccessTypeId) );
    }
    jexec.closeData(key);
  }

  public int check(int pPageID, int pStatusID)  throws Exception
  {
    if (initProperly == false){
      throw new Exception("Hashtable was not properly initialized.");
    }
    Integer hashKey = new Integer( pPageID + (pStatusID << 16) );
    Object lObj = pageStatusMatrix.get(hashKey);
    return lObj == null ? Sc.PAGE_ACCESS_UNASSIGNED : ((Integer)lObj).intValue();
  }

}