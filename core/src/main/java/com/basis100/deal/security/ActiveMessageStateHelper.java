package com.basis100.deal.security;

import java.io.Serializable;
import java.util.*;

/** ActiveMessageStateHelper
*
* Simple general purpose helper class intended to act as a response object for an active
* message - i.e. to save simple state information for aa ActiveMessage.
*
**/

public class ActiveMessageStateHelper extends PassiveMessage implements Serializable
{
    public int i1;
    public int i2;
    public int i3;

    public boolean b1;
    public boolean b2;
    public boolean b3;

    public String s1;
    public String s2;
    public String s3;
}



