/*
 * @(#)ViewBeanFactory.java    2007-7-13
 *
 * Copyright (C) 2007 Filogix, Inc. All rights reserved.
 */


package com.basis100.deal.security;

/**
 * ViewBeanFactory - Abstract View Bean Factory.  Concrete subclasses determine
 * which pgViewBean to instaniate when the 'Go To' does not map to a pgViewBean
 * 1-1.  It is copied from class mosApp.MosSystem.ViewBeanFactory.
 *
 * @version   1.0 2007-7-13
 * @author    <A HREF="mailto:sean.chen@filogix.com">Chen Xiang (Sean)</A>
 */
public abstract class ViewBeanFactory {

    //the path
    protected String path;

    /**
     * Constructor function
     */
    public ViewBeanFactory() {
    }

    public String getPath() {

        return path;
    }

    /**
     * returns the page name based on viewben name.
     */
    public abstract String getViewBeanByClassName();
}