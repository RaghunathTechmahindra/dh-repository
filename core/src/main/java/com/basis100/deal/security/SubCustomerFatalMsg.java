package com.basis100.deal.security;

import MosSystem.Sc;
import MosSystem.Mc;

class SubCustomerFatalMsg extends ActiveMessage {

  public SubCustomerFatalMsg(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public SubCustomerFatalMsg(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

  public SubCustomerFatalMsg()
  {
      common();
  }

  public void common()
  {
      generate  = true;
      hasOK    = true;
      hasCancel= false;
      hasClose = false;

      buttonHtml = Mc.OK_SERVER_EXECUTE_BTN;

      responseIdentifier = OK_NAME;

  }

}