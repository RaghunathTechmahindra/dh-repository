package com.basis100.deal.security;

import java.io.Serializable;
import java.util.*;

public class ActiveMessage extends PassiveMessage implements Serializable
{
  public static String OK_NAME = "OK";
  public static String CANCEL_NAME = "CANCEL";
  public static String CLOSE_NAME = "CLOSE";

  // active message type
  protected int msgType;

  // the dialog message question
  protected String  dialogMsg  = null;

  //  additional properties for buttons on the active dialog
  protected boolean hasCancel     = false;
  protected boolean hasClose      = false;
  protected String  buttonHtml    = "";
  protected boolean hasNoDialog   = true;

  // contents replace occurances of token '#' in the dialog message (see setDialogMsg())
  protected String parmReplace    = "";

  // contents replace occurances of token '@' in the dialog message (see setDialogMsg())
  protected String parmReplace2   = "";


  // Response identifier - used for custom dialog responses to identify the response
  // for programatic purposes.
  //
  // Explanation:
  //
  // The OK button featured in the dialog to the user generates a event to the server
  // page. The event triggers the generic page event handler method with the following
  // signature:
  //
  //                 PageHandlerCommon.handleActMessageOK(String[] args)
  //
  // By default args is a one element array with args[0] = "@". This is appropraite for
  // active message types with pre-determined handling requirements. For example receiving
  // the OK event when active message type is ISCANCEL means that the user is confirming
  // a page Cancel operation - simply confirming that data modifications preformed on the page
  // are to be discarded. Such an operation has a pre-determined response.
  //
  // However the OK event when the message type is ISCUSTOMDIALOG generally has an application
  // specific meaning (vs. framework specific meaning). That is to say that the event generally
  // triggers some application specific page activity. In this case the following method is
  // called by the framework:
  //
  //                PageHandlerCommon.handleCustomActMessageOk(String[] args)
  //
  // By default this method behaves like a standard page Cancel (confirmed) operation but is
  // intended to be overriden in the handler class (sub class of PageHandlerCommon) for the page
  // that has generated the custom dialog message.
  //
  // Now if the page in question generates more than one ISCUSTOMDIALOG active message (e.g.
  // multiple dialogs for various business logic requirements) there must be a mechanism to
  // determine what dialog is answering. The response identifer property allows for such
  // detection. Basically when a page generates an (ISCUSTOMDIALOG) active message it can set the
  // response identifier to allows easy detection of the context of the OK server event. When
  // a response identifer is set the args parameter in handleCustomActMessageOk(String[] args)
  // calls will contain the following:
  //
  //        args[0] = {the response identified}
  //        args[1] = {page id}   // string representation of page that generated active message
  //
  //        Note: When a ISCUSTOMDIALOG active message object is created via usage of the
  //              ActiveMsgFactory class a default response identified of "OK" is set
  //              automatically.
  //
  // While the response identified can be any string adoption of a simplified scheme for
  // response identification is recommended. For example a simple sequence of letters
  // (e.g. "A", "B", etc.) or integers (e.g. "1", "2", etc.) could be used. In any event such
  // identified can be defined as symbols in the page (handler) to enhance clarity, e.g.
  //
  //     public static final String CONFIRM_RATE_DISCOUNT_EXCEEDS_POLICY = "A";
  //     public static final String CONFIRM_RATE_LOCKED                  = "B";
  //
  // Active message creation example:
  //
  //     -- > using handler utility method:
  //
  //          setActiveMessageToAlert(String msgtext, int msgType, String responseIdentifier)
  //
  //     --> generation fragment
  //
  //          setActiveMessageToAlert("Discount exceeds max. of 5.0% - press OK to override policy",
  //                                  ActiveMsgFactory.ISCUSTOMDIALOG, CONFIRM_RATE_DISCOUNT_EXCEEDS_POLICY);
  //
  // Then in the handler class we would have:
  //
  //  public void handleCustomActMessageOk(String[] args)
  //  {
  //      if (args[0].equals(CONFIRM_RATE_DISCOUNT_EXCEEDS_POLICY)
  //      {
  //          ... code ...
  //      }
  //
  //      if (args[0].equals(CONFIRM_RATE_LOCKED)
  //      {
  //          ... code ...
  //      }
  //
  //      ...
  //  }
  //

  protected String  responseIdentifier   = null;

  // second response identifier - used in (rare) instance when CANCEL button generates a server
  // event

  protected String  responseIdentifier2  = null;

  // general purpose object with application specific context - e.g. used to
  // retrieve information useful for handling a response (must be searializable)

  protected Object responseObject = null;
  protected Object res1 = null;
  protected Object res2 = null;
  protected Object res3 = null;
  protected TreeMap mapResObjects = new TreeMap();

  public ActiveMessage()
  {
  }

  // getters & setters
  public int getMsgType()
  {
      return msgType;
  }
  public void setMsgType (int pMsgType)
  {
      msgType = pMsgType;
  }

  public void setParmReplace(String s)
  {
      parmReplace = s;
  }

  public void setParmReplace2(String s)
  {
      parmReplace2 = s;
  }


  // setDialogMsg:
  //
  // When setting the dialog message two 'token' are subject to parameter substitution. Namely,
  //
  //     Token # - occurances replaced with contents of replacement parameter one - this is a string that
  //               is provided by calling setParmPreplace() before setting the dialog message.
  //
  //     Token @ - occurances replaced with contents of replacement parameter two - this is a string that
  //               is provided by calling setParmPreplace2() before setting the dialog message.
  //
  // Notes:
  //
  // Within the MOS project the usages of the replacement tokens have been standardized as follows:
  //
  //      Token # - replacement string is deal number.
  //
  //      Token @ - replacement string is page name (label).
  //

  public static final String REPLACE_TOKEN = "#";
  public static final String REPLACE_TOKEN2 = "@";

  public void setDialogMsg (String s)
  {
      dialogMsg = replaceString(REPLACE_TOKEN, s, parmReplace);
      dialogMsg = replaceString(REPLACE_TOKEN2, dialogMsg, parmReplace2);
  }
  public String getDialogMsg ()
  {
      return dialogMsg;
  }


  public boolean getCancel ()
  {
      return hasCancel;
  }
  public void setCancel (boolean pHasCancel)
  {
      hasCancel = pHasCancel;
  }

  public boolean getClose ()
  {
      return hasClose;
  }
  public void setClose (boolean pHasClose)
  {
      hasClose = pHasClose;
  }

  public boolean getNoDialog()
  {
      return hasNoDialog;
  }
  public void setNoDialog(boolean pHasNoDialog)
  {
      hasNoDialog = pHasNoDialog;
  }

  public String getButtonHtml()
  {
    return buttonHtml;
  }
  public void setButtonHtml(String pBtnHtml)
  {
    buttonHtml = pBtnHtml;
  }


  /**
   *  Customization of buttons to appear on the active message dialog. Used when:
   *
   *      . custom image buttons desired (e.g. other than standard SUBMIT, OK, and CANCEL buttons)
   *      . dialog required more than two buttons.
   *
   *  All buttons appearing on the dialog generate server events.
   *
   * @Args
   *
   *  responseIdentifiers - response identifiers - see documentation for property <responseIdentifier> above.
   *  imageName           - names of image files (button images). The source for an image files is fromed as:
   *
   *              /eNet_Images/{image name provided}
   *
   *
   **/
  //--> In order to achieve the same functionality of the Active Message Button in JATO we have to use
  //--> HREF instead and invoke the JavaScript function (performActMsgBtSubmit) in order to pass button
  //--> parameters to the special Commadn Handler (ActMessageCommand).
  //--> by BILLY 15July2002
	public static final String SERVER_EXECUTE_GENERIC_BTN =
	//	"<INPUT TYPE=IMAGE NAME=\"handleActMessageOK(@)\" VALUE=\" \" SRC=\"/eNet_images/^\" ALIGN=TOP  alt=\"\" border=\"0\">";
  //--> Increased Width the Images to get them readable for ticket #64
    "<a onclick=\"return IsSubmited();\" href=\"javascript:performActMsgBtSubmit(\\'@\\');\"><img src=\"../images/^\" width=220 height=16 alt=\"\" border=\"0\"></a>";
  //=================================================================================

  public static final String SPACER_EXECUTE_BTN = "&nbsp;&nbsp;&nbsp;&nbsp;";


  public void setButtons(Vector responseIdentifiers, Vector imageNames)
  {
    setResponseIdentifier(null);
    setResponseIdentifier2(null);

    StringBuffer sb = new StringBuffer();

    ///// Part of table approach - see below
    ///// sb.append( "<table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=\"d1eff\">");

    int limResponses  = (responseIdentifiers == null) ? 0 : responseIdentifiers.size();
    int limImages     = (imageNames == null)          ? 0 : imageNames.size();

    int lim = (limResponses > limImages) ? limResponses : limImages;

    for (int i = 0; i < lim; ++i)
    {
        String btnHtml = SERVER_EXECUTE_GENERIC_BTN;

        if (i < limResponses)
          btnHtml = replaceString("@", btnHtml, (String)responseIdentifiers.elementAt(i));

        String imgName = "ok.gif";
        if (i < limImages)
          imgName = (String)imageNames.elementAt(i);

        btnHtml = replaceString("^", btnHtml, imgName);

        sb.append(btnHtml);

        if (i < (lim - 1))
          sb.append(SPACER_EXECUTE_BTN);
    }

    buttonHtml = sb.toString();

  }

/*

    // table approach

    for (int i = 0; i < lim; ++i)
    {
        sb.append("<tr><td align=center>");

        String btnHtml = SERVER_EXECUTE_GENERIC_BTN;

        if (i < limResponses)
          btnHtml = replaceString("@", btnHtml, (String)responseIdentifiers.elementAt(i));

        String imgName = "ok.gif";
        if (i < limImages)
          imgName = (String)imageNames.elementAt(i);

        btnHtml = replaceString("^", btnHtml, imgName);

        sb.append(btnHtml);
        sb.append("</td></tr>");
    }

    sb.append("</table>");

*/


  public String getResponseIdentifier()
  {
    return responseIdentifier;
  }
  public void setResponseIdentifier(String responseIdentifier)
  {
    this.responseIdentifier = responseIdentifier;
  }

  public String getResponseIdentifier2()
  {
    return responseIdentifier2;
  }
  public void setResponseIdentifier2(String responseIdentifier2)
  {
    this.responseIdentifier2 = responseIdentifier2;
  }

  public Object getResponseObject()
  {
    return responseObject;
  }

  public void setResponseObject(Object responseObject)
  {
    this.responseObject = responseObject;
  }

  public void setResponseObject(Object[] arrayResObjects)
  {
    this.responseObject = new Object[] {res1, res2, res3};
  }

  public void setResponseObject(TreeMap mapResObjects)
  {
      this.responseObject = mapResObjects;
  }

	// token replacenment routine
	//
	//   token = @
	//   example:    replaceString("@", "super(@)", "5") --> "super(5)"
	//
	// Warning: currently if multiple tokens found the replacement string
	//          is simply replaced multiple time:
	//
	//   replaceString("@", "super(@,@)", "5") --> "super(5,5)"
  //
  // Known Bug: Doesn't replace token at end of input string.


	public static String replaceString(String token, String str, String replaceStr)
	{

		StringTokenizer parser = new StringTokenizer(str, token);
		String[] elements = null;
		String retStr = "";

		try
		{
		  elements = new String[parser.countTokens()];
		  int indx = 0;
		  while(parser.hasMoreElements())
		  {
		    elements[indx] = (String)parser.nextElement();
		    indx++;
		  }

		  if (elements.length <2)
		    return str;

		  for (int i=0; i<elements.length; i++)
		  {
		    if(i == 0)
		    {
		      retStr = elements[0];
		      continue;
		    }

		    retStr = retStr + replaceStr + elements[i];

		  }

		} catch (Exception ex)
		{
		  retStr = str;
		}

		return retStr;
	}


}



