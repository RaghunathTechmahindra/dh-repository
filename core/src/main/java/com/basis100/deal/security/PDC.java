package com.basis100.deal.security;

// The MOS user interface is composed of a number of individual pages that
// allow display and optionally modification of deal level data. A record
// in the PAGE table in the MOS database describes each page.
//
// Basically when a user wishes to gain edit access to a page for a particular
// deal, the DLM singleton is queried to determine if the deal is locked (by
// some other user).
//
// If a lock is not present, page properties (accessed via
// PDM singleton) are interrogated to determine if a lock should be established.
//
// If the deal is locked, page progerties(i.e. the allowEditWhenLocked property)
// are used to determine if access to the page should be denied.
//
// Designed by:  Bernard  Hickey
// Impletmented by: Derek Guo
// Last modification date: June 30, 2000

import com.basis100.resources.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.log.*;
import java.util.Vector;
import java.util.Enumeration;
import com.basis100.deal.entity.Deal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class PDC  implements Cloneable
{

    private final static Log _log = LogFactory.getLog(PDC.class);

  private static boolean ready = false;
  private static PDC instance;
  private static Vector allPgs;
  private static final int DELAY = 100;
  private static final int DELAY_TIMEOUT = 20000;
  private static final String DisclosureFactory = "DisclosureFactory";
  private static final String AppraisalReviewFactory = "AppraisalReviewFactory";

  // constructor
  private PDC()
  {
  }

  // when system startup, generate singleton object PDC
  public static void init(SessionResourceKit srk) throws Exception
  {
    try
    {
      // To get all pages
      Page pg = new Page(srk);
      allPgs = pg.findByAll();

      instance = new PDC();

      ready = true;
    }
    catch (Exception e)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = e.getMessage() + "--- error in init()@PDC";
      logger.error(msg);
      throw new Exception(msg);
    }
  }


  public static PDC getInstance() throws Exception
  {
    try
    {
       waitTillReady();
       return instance;
      }
    catch (Exception e)
    {
      String msg = e.getMessage() + "--- error in getInstance()@PDC";
      SysLog.error(msg, "{System}");
      throw new Exception(msg);
    }
  }

  private ViewBeanFactory getViewBeanFactory(String factoryName, Deal deal) {

      String className = "mosApp.MosSystem." + factoryName;

      try {
          // TODO: tempory impl.
          //For AppraisalReviewFactory and DisclosureFactory, Deal is needed to make 
          //a decision. Hence the special processing
          if ((factoryName.equals(AppraisalReviewFactory)) || (factoryName.equals(DisclosureFactory)))
          {
              Class clazz = Class.forName(className);
              return (ViewBeanFactory) clazz.getConstructor(deal.getClass()).newInstance(deal);
          }
          else
          {
              Class clazz = Class.forName(className);
              return (ViewBeanFactory) clazz.newInstance();
          }
      } catch (Exception e) {
          _log.error("Failed to get view bean factory: " + e.getMessage(), e);
      }

      return null;
  }

  public Page getPage(int pageId) throws Exception
  {
    try
    {
      waitTillReady();

      Enumeration en = allPgs.elements();
      while (en.hasMoreElements())
      {
        Page pg = (Page)en.nextElement();
        if(pg.getPageId() == pageId)
          return pg;
      }
    }
    catch (Exception e)
    {
      String msg = e.getMessage() + "--- error in getPage(" + pageId + ")@PDC";
      SysLog.error(msg, "{System}");
      throw new Exception(msg);
    }

    String msg = "Unknow pageId --- error in getPage(" + pageId + ")@PDC";
    SysLog.error(msg, "{System}");
    throw new Exception(msg);
  }

  /**
   * Uses the dealId whenever a factory is required to determine which pg to return based on some details in the deal
   * @param pageId int
   * @param dealId int
   * @return Page
   */
  public Page getPage(int goToPageId, Deal deal) throws Exception {
    Page pg = getPage(goToPageId);
    String pgName = pg.getPageName();

    // the goto refers to a factory.  i.e. goto entry not 1-1 with pgViewBean
    if (pgName.indexOf("Factory") >= 0) {
      ViewBeanFactory vbf = getViewBeanFactory(pgName, deal);
      String concretePg = vbf.getViewBeanByClassName();

      return getPageByName(concretePg);
    }

    // return what getPage(int) returned if factory is not required (most common case)
    return pg;
  }

  public Page getPageByLabel(String pageLabel) throws Exception
  {
    try
    {
      waitTillReady();

      Enumeration en = allPgs.elements();
      while (en.hasMoreElements())
      {
        Page pg = (Page)en.nextElement();
        if(pg.getPageLabel().equals(pageLabel))
          return pg;
      }
      }
    catch (Exception e)
    {
      String msg = e.getMessage() + "--- error in getPageByLabel(" + pageLabel + ")@PDC";
      SysLog.error(msg, "{System}");
      throw new Exception(msg);
    }

    String msg = "Unknow pageLabel --- error in getPageByLabel(" + pageLabel + ")@PDC";
    SysLog.error(msg, "{System}");
    throw new Exception(msg);
  }

  public Page getPageByName(String pageName) throws Exception
  {
    try
    {
      waitTillReady();

      Enumeration en = allPgs.elements();
      while (en.hasMoreElements())
      {
        Page pg = (Page)en.nextElement();
        if(pg.getPageName().equals(pageName))
          return pg;
      }
      }
    catch (Exception e)
    {
      String msg = e.getMessage() + "--- error in getPageByName(" + pageName + ")@PDC";
      SysLog.error(msg, "{System}");
      throw new Exception(msg);
    }

    String msg = "Unknow pageLabel --- error in getPageByName(" + pageName + ")@PDC";
    SysLog.error(msg, "{System}");
    throw new Exception(msg);
  }

  public static void reset(SessionResourceKit srk) throws Exception
  {

    try
    {
        waitTillReady();

        ready = false;   //return instance to non-ready state

        allPgs = null;   //purge existing cached list(s)

        init(srk);       //re-initialize
    }
    catch(Exception e)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = e.getMessage() + "--- error in reset()@PDC";
      logger.error(msg);
      throw new Exception(msg);
    }
  }

  private static void waitTillReady() throws Exception
  {
      int sleepTime = 0;
        while(ready == false)
        {
        Thread.sleep(DELAY);

        sleepTime += DELAY;
        if(sleepTime > DELAY_TIMEOUT)
        {
          String msg = "thread wait exception - timeout (" + (DELAY_TIMEOUT/1000) + " sec.)";
          throw new Exception(msg);
          }
      }
  }

}
