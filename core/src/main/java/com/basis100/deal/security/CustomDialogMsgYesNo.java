package com.basis100.deal.security;

import MosSystem.Mc;

class CustomDialogMsgYesNo extends ActiveMessage
  {
  public CustomDialogMsgYesNo(String replacement, String replacement2)
  {
      setParmReplace(replacement);
      setParmReplace2(replacement2);
      common();
  }

  public CustomDialogMsgYesNo(String replacement)
  {
      setParmReplace(replacement);
      common();
  }

   public CustomDialogMsgYesNo()
   {
      common();
   }

  public void common()
  {
        generate   = true;
        hasOK     = true;
        hasCancel = true;
        hasClose  = false;

        buttonHtml = Mc.YES_SERVER_EXECUTE_BTN + Mc.SPACER_EXECUTE_BTN + Mc.NO_SERVER_EXECUTE_BTN;

        responseIdentifier = OK_NAME;
  }

  }
