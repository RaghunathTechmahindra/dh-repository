package com.basis100.deal.security;

import MosSystem.Sc;

import java.util.*;
import com.basis100.resources.*;
import com.basis100.workflow.entity.*;
import com.basis100.workflow.pk.*;
import com.basis100.picklist.BXResources;
import com.filogix.express.core.ExpressRuntimeException;

public class ACM  implements Cloneable,java.io.Serializable
{
  private String userLogin;  
  private int userProfileID;
  private int userTypeID;
  private boolean ready;
  private Hashtable<String, Integer> userAccessTable;

  public ACM(SessionResourceKit srk, int userTypeID, int userProfileID) throws Exception
  {
    this.userProfileID = userProfileID;
    this.userTypeID = userTypeID;
    buildUA(srk);
    ready = true;
  }

  // added for FXP21790
  // when user login, this method should be called from SignonHandler.finishLogin
  public ACM(SessionResourceKit srk, int userTypeID, String userLogin) throws Exception
  {
    this.userLogin = userLogin;
    this.userTypeID = userTypeID;
    buildUAByUserLogin(srk);
    ready = true;
  }

  public void buildUA(SessionResourceKit srk)
   {
      PageAccess defaultAccess = null;
      PageAccess userAccess    = null;
      try
      {
         defaultAccess = new PageAccess(srk);
         defaultAccess.findByPrimaryKey(new PageAccessBeanPK(userTypeID,0));
      }
      catch (Exception e)
      {
         srk.getSysLogger().error(e.getMessage() + " : PageAccess Entity Exception Follows");
      }

      try
      {
         userAccess = new PageAccess(srk);
         userAccess.setSilentMode(true); // ok if not found - avoid logging
         userAccess.findByPrimaryKey(new PageAccessBeanPK(userTypeID,userProfileID));
      }
      catch (Exception e)
      {
         userAccess = null;
      }

      if (userAccess != null)
      {
          defaultAccess.applyAsOverrides(userAccess);
      }
      userAccessTable = defaultAccess.getAccessTable();
  }

  public void buildUAByUserLogin(SessionResourceKit srk)
  {
     PageAccess defaultAccess = null;
     PageAccess userAccess    = null;
     try
     {
        defaultAccess = new PageAccess(srk);
        defaultAccess.findByPrimaryKey(new PageAccessBeanPK(userTypeID,0));
     }
     catch (Exception e)
     {
        srk.getSysLogger().error(e.getMessage() + " : PageAccess Entity Exception Follows");
     }

     try
     {
        userAccess = new PageAccess(srk);
        userAccess.setSilentMode(true); // ok if not found - avoid logging
        userAccess.findByPrimaryKey(new PageAccessBeanPK(userTypeID,userLogin));
     }
     catch (Exception e)
     {
        userAccess = null;
     }

     if (userAccess != null)
     {
         defaultAccess.applyAsOverrides(userAccess);
     }
     userAccessTable = defaultAccess.getAccessTable();
 }

  
  void refresh(SessionResourceKit  srk)
  {
     ready = false;
     buildUA(srk);
     ready = true;
  }

  public AccessResult pageAccess(int pageID, int languageId,
          int dealInstitutionId) throws Exception {
      
      if(dealInstitutionId <= -1){
          throw new ExpressRuntimeException("institutionId is invalid : [" + dealInstitutionId + "]");
      }
      
      int sleepTime = 0;
      while (ready == false) {
          Thread.sleep(1000);
          sleepTime += 1000;
          if (sleepTime > 10000) {
              throw new Exception(
                      "Access result could not be obtained for too long.");
          }
      }

      AccessResult result = new AccessResult(Sc.PAGE_ACCESS_DISALLOWED);
      
      String tableKey = PageAccess.makeKey(pageID, dealInstitutionId);

      Integer accessType = userAccessTable.get(tableKey);
      if (accessType == null) {
          result.setAccessTypeID(Sc.PAGE_ACCESS_DISALLOWED);
      } else {
          result.setAccessTypeID(accessType.intValue());
      }
      
      int lRes = result.getAccessTypeID();
      switch (lRes) {
      case Sc.PAGE_ACCESS_VIEW_ONLY: {
          break;
      }
      case Sc.PAGE_ACCESS_EDIT: {
          break;
      }
      case Sc.PAGE_ACCESS_SUSPENDED: {
          result.setAccessTypeID(Sc.PAGE_ACCESS_DISALLOWED);
          result.setAccessMessage((String) BXResources.getSysMsg(
                  "PAGE_ACCESS_SUSPENDED_MSG", languageId));
          break;
      }
      default: {
          result.setAccessTypeID(Sc.PAGE_ACCESS_DISALLOWED);
          result.setAccessMessage((String) BXResources.getSysMsg(
                  "PAGE_ACCESS_DENIED_MSG", languageId));
          break;
      } // end default
      } // end switch(lRes)
      

      
      return result;
  }

}
