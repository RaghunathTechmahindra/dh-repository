package com.basis100.deal.security;

// The Deal Lock Management(DLM) class provides facilities to ensure that the activites
// of multiple simultaneous user session do not disrupt the integrity of deal level data.
//
// Designed by:  Bernard  Hickey
// Impletmented by: Derek Guo
// Last modification date: Oct 08, 2004
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import MosSystem.Mc;

import com.basis100.deal.util.TypeConverter;
import com.basis100.jdbcservices.jdbcexecutor.JdbcExecutor;
import com.basis100.log.SysLog;
import com.basis100.log.SysLogger;
import com.basis100.resources.PropertiesCache;
import com.basis100.resources.SessionResourceKit;
import com.filogix.express.core.ExpressRuntimeException;

import java.util.Date;

/**
 * By Neil: DLM class is always used singlton, no state can be stored using instance variable.
 */
public class DLM
  implements Cloneable
{
  private static boolean ready = false;
  private static DLM instance;
  private static final int DELAY = 100;
  private static final int DELAY_TIMEOUT = 20000;

  //	//--> By Neil : Nov/03/2004
  //	//--> commented out. never used.
  //	private static Hashtable locksList;    		// key = dealId, value = userProfileId
  //	private static Hashtable reverseLocksList; 	// key = userProfileId, value = dealId

  //--> By Neil : Oct/08/2004
  // empty dealid
  private static final int EMPTY_DEAL_ID = -1;
  // lockreason field length limit
  private static final int REASON_LENGTH_LIMIT = 50;
  //---------- Lock Status ----------//
  public static final int NOT_LOCKED = 0;
  public static final int LOCKED_BY_SELF = 1;
  public static final int LOCKED_BUT_OVERRIDABLE = 2;
  public static final int LOCKED_AND_UNOVERRIDABLE = 3;

  /**
   * empty constructor
   */
  private DLM()
  {
  }

  /**
   * init
   * @param srk SessionResourceKit
   * @throws Exception
   */
  public static void init(SessionResourceKit srk) throws Exception
  {
    init(srk, true);
  }

  /**
   * init
   * @param srk SessionResourceKit
   * @param clearAllLocks boolean
   * @throws Exception
   */
  public static void init(SessionResourceKit srk, boolean clearAllLocks) throws Exception
  {

    // To ensure lock table empty
    try
    {
      //if Stand alone programs call this method with clearAllLocks=true, 
      //this method will delete all recored mosapp is activly using. 
      //use 'false' for stand alone programs.
      if(clearAllLocks == true)
      {
        srk.beginTransaction();

        //delete remaining lock data except the records created 
        //by stand alone programs (e.g, AME, Ingestion, etc)
        srk.getJdbcExecutor().executeUpdate(
        		"DELETE FROM deallocks dl " +
        		" WHERE EXISTS ( " +
        		"  SELECT 1 " +
        		"  FROM userprofile up " +
        		"  WHERE up.institutionprofileid = dl.institutionprofileid " +
        		"  AND up.userprofileid = dl.userprofileid " +
        		"  AND up.usertypeid between " + 
        		Mc.USER_TYPE_PASSIVE + " and " + Mc.USER_TYPE_SPECIAL + " )");
        
        srk.commitTransaction();
      }

      // instantiate DLM
      instance = new DLM();
      // To set ready semaphore
      ready = true;

    }
    catch(Exception e)
    {
      srk.cleanTransaction();
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.init() exception = " + e.getMessage();
      logger.error(msg);
      logger.error(e);
      throw new Exception(msg);
    }
  }

  /**
   * getInstance
   * @throws Exception
   * @return DLM
   */
  public static DLM getInstance() throws Exception
  {
    try
    {
      waitTillReady();
      return instance;
    }
    catch(Exception e)
    {
      String msg = "@DLM.getInstance() exception = " + e.getMessage();
      SysLog.error(msg, "{System}");
      SysLog.error(e, "{System}");
      throw new Exception(msg);
    }
  }

  /**
   * isLocked
   * Changed to check it from DB as External process (e.g. Ingestion) may lock dael as well
   * @param dealId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return boolean
   * @author Billy Apr/23/2002
   */
  public boolean isLocked(
    int dealId,
    int userProfileId,
    SessionResourceKit srk) throws Exception
  {
    String sql =
      "SELECT * FROM DEALLOCKS WHERE USERPROFILEID <> "
      + userProfileId
      + " AND DEALID = "
      + dealId;

    boolean gotRecord = false;

    try
    {
      syncMasterDeal(srk, dealId);
      int key = srk.getJdbcExecutor().execute(sql);

      for(; srk.getJdbcExecutor().next(key); )
      {
        gotRecord = true;
        break; // one record is enough
      }

      srk.getJdbcExecutor().closeData(key);

    }
    catch(Exception e)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.isLocked() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg, e);
    }

    return gotRecord;
  }

  /**
   * isLockedByUser
   * @param dealId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return boolean true if specified deal locked by the input user. else return false.
   */
  public boolean isLockedByUser(
    int dealId,
    int userProfileId,
    SessionResourceKit srk) throws Exception
  {
    String sql =
      "SELECT * FROM DEALLOCKS WHERE USERPROFILEID = "
      + userProfileId
      + " AND DEALID = "
      + dealId;

    boolean gotRecord = false;

    try
    {
      int key = srk.getJdbcExecutor().execute(sql);

      for(; srk.getJdbcExecutor().next(key); )
      {
        gotRecord = true;
        break; // one record is enough
      }

      srk.getJdbcExecutor().closeData(key);

    }
    catch(Exception e)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.isLocked() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg);
    }

    return gotRecord;
  }

  /**
   * lock
   * Changed to check it from DB as External process (e.g. Ingestion) may lock dael as well
   * @param dealId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @author Billy Apr/23/2002
   */
  public synchronized void lock(
    int dealId,
    int userProfileId,
    SessionResourceKit srk) throws Exception
  {
      lock(dealId, userProfileId, srk, "", "N");
  }
  
  
  /**
   * lock
   * Changed to check it from DB as External process (e.g. Ingestion) may lock deal as well
   * @param dealId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param sessionID sessionID 
   * @parama 
   * @throws Exception
   */
  public synchronized void lock(
    int dealId,
    int userProfileId,
    SessionResourceKit srk,
    String sessionID,
    String miResponse) throws Exception
  {
    try
    {
      waitTillReady();

      // Check if Locked by other users
      if(isLocked(dealId, userProfileId, srk) == true)
      {
        throw new Exception(
          // already locked by specified user - not considered exception condition
          "Deal locked by some other user, deal id = " + dealId);
      }

      String date = null;
      if(isLockedByUser(dealId, userProfileId, srk) == true)
      {
        Date d = Calendar.getInstance().getTime();
        date = sqlStringFrom(d);
      }

      if(date == null)
      {
        srk.getJdbcExecutor().executeUpdate(
          "INSERT INTO DEALLOCKS (DEALID, USERPROFILEID, LOCKTYPE, INSTITUTIONPROFILEID, SESSIONID, MIRESPONSEEXPECTED) VALUES("
          + dealId + ", "
          + userProfileId
          + ", 'L' ," 
          + srk.getExpressState().getDealInstitutionId() + ", "
          + "'" + sessionID + "',"
          + "'" + miResponse + "'" + ")");
      }
      else
      {
        String sql =
          "UPDATE DEALLOCKS SET LOCKTIME = "
          + date
          + " WHERE DEALID = "
          + dealId
          + " AND USERPROFILEID = "
          + userProfileId;
        srk.getJdbcExecutor().executeUpdate(sql);
      }
      //----------> To Refresh the lockTime Ends <----------//

    }
    catch(Exception e)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.lockBySessionID() exception = " + e.getMessage();
      logger.error(msg);
      logger.error(e);
      throw new Exception(msg);
    }
  }

  /**
   * unlock
   * Changed to check it from DB as External process (e.g. Ingestion) may lock dael as well
   * @param dealId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param callInTransaction boolean
   * @throws Exception
   * @author Billy Apr/23/2002
   */
  public void unlock(
    int dealId,
    int userProfileId,
    SessionResourceKit srk,
    boolean callInTransaction) throws Exception
  {
    try
    {
      waitTillReady();

      if(callInTransaction == false)
      {
        srk.beginTransaction();
      }
      JdbcExecutor jExec = srk.getJdbcExecutor();

      jExec.executeUpdate(
        "DELETE FROM DEALLOCKS WHERE DEALID = "
        + dealId
        + " AND USERPROFILEID = "
        + userProfileId);

      if(callInTransaction == false)
      {
        srk.commitTransaction();
      }
    }
    catch(Exception e)
    {
      if(callInTransaction == false)
      {
        srk.cleanTransaction();

      }
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.unlock() exception = " + e.getMessage();
      logger.error(msg);

      throw new Exception(msg);
    }
  }
  
  /**
   * unlockAnyBySessionID
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param callInTransaction boolean
   * @throws Exception
   */
  public void unlockAnyBySessionID(
    SessionResourceKit srk,
    boolean callInTransaction,
    String SessionID
    ) throws Exception
  {
    try
    {
      waitTillReady();
      
      if(callInTransaction == false)
      {
        srk.beginTransaction();

      }
      JdbcExecutor jExec = srk.getJdbcExecutor();

      int ret = jExec.executeUpdate(
        "DELETE FROM DEALLOCKS WHERE SESSIONID = '" + SessionID + "'");

      if(callInTransaction == false)
      {
        srk.commitTransaction();
      }
    }
    catch(Exception e)
    {
      if(callInTransaction == false)
      {
        srk.cleanTransaction();

      }
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.unlockAnyBySessionID() exception = " + e.getMessage();
      logger.error(msg);

      throw new Exception(msg);
    }
  }

  /**
   * unlockAnyByUser
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param callInTransaction boolean
   * @throws Exception
   */
  public void unlockAnyByUser(
    int userProfileId,
    SessionResourceKit srk,
    boolean callInTransaction) throws Exception
  {
    try
    {
      waitTillReady();

      if(callInTransaction == false)
      {
        srk.beginTransaction();

      }
      JdbcExecutor jExec = srk.getJdbcExecutor();

      jExec.executeUpdate(
        "DELETE FROM DEALLOCKS WHERE USERPROFILEID = " + userProfileId);

      if(callInTransaction == false)
      {
        srk.commitTransaction();
      }
    }
    catch(Exception e)
    {
      if(callInTransaction == false)
      {
        srk.cleanTransaction();

      }
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.unlockAnyByUser() exception = " + e.getMessage();
      logger.error(msg);

      throw new Exception(msg);
    }
  }

  /**
   * waitTillReady
   * @throws Exception
   */
  private static void waitTillReady() throws Exception
  {
    int sleepTime = 0;
    while(ready == false)
    {
      Thread.sleep(DELAY);
      sleepTime += DELAY;
      if(sleepTime > DELAY_TIMEOUT)
      {
        String msg =
          "@DLM.waitTillReady: timeout ("
          + (DELAY_TIMEOUT / 1000)
          + " sec.)";
        throw new Exception(msg);
      }
    }
  }

  /**
   * getLockStatus
   * Check the lock status.
   * @param dealId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return int 0 - not locked;
   *             1 - locked but overridable;
   *             2 - locked and unoverridable
   * @author Neil 10/08/2004
   */
  public int getLockStatus(int dealId,
    int userProfileId,
    SessionResourceKit srk) throws Exception
  {

    int lockStatus = DLM.LOCKED_AND_UNOVERRIDABLE;

    StringBuffer sqlBuff = null;
    String sql = null;
    // Read lockExpireSecond property from property file.
    String s = PropertiesCache.getInstance().getProperty(srk.getExpressState().getDealInstitutionId(),
                "com.basis100.dlm.lockExpireSeconds", "3600");   

    long validSessionPeriod = new Long(s.trim()).longValue();

    // Calculate the expire time.
    Calendar cal = Calendar.getInstance();
    long currentTime = cal.getTime().getTime();

    try
    {
      syncMasterDeal(srk, dealId);
      // Look for locks placed by other users.
      sqlBuff =
        new StringBuffer("SELECT * FROM DEALLOCKS WHERE USERPROFILEID <> ");
      sqlBuff.append(userProfileId);
      sqlBuff.append(" AND DEALID = ");
      sqlBuff.append(dealId);
      sql = sqlBuff.toString();

      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sql);

      if(jExec.next(key))
      {
        // Lock exists.  It's placed by other user.
        // Check if the lock is overridable.
        String lockType = jExec.getString(key, "LOCKTYPE");
        // Get LOCKTYPE
        long lockedTime = jExec.getDate(key, "LOCKTIME").getTime();
        // Get LOCKTIME

        long timeDiff = (currentTime - lockedTime) / 1000;

        // check the lock status
        if(lockType.trim().equalsIgnoreCase("S"))
        {
          // SoftLock is overridable.
          lockStatus = DLM.LOCKED_BUT_OVERRIDABLE;
        }
        else if(timeDiff >= validSessionPeriod)
        {
          // RegularLock and expires, it's overridable.
          lockStatus = DLM.LOCKED_BUT_OVERRIDABLE;
        }
        else
        {
          // RegularLock and NOT expires, it's unoverridable.
          lockStatus = DLM.LOCKED_AND_UNOVERRIDABLE;
        }

        jExec.closeData(key);
      }
      else
      {
        // Either the lock is placed by the inputted user.
        // Or there is no lock exists.
        jExec.closeData(key);

        // Look for locks placed by this user himself.
        sqlBuff = new StringBuffer("SELECT * FROM DEALLOCKS WHERE USERPROFILEID = ");
        sqlBuff.append(userProfileId);
        sqlBuff.append(" AND DEALID = ");
        sqlBuff.append(dealId);
        sql = sqlBuff.toString();

        jExec = srk.getJdbcExecutor();
        key = jExec.execute(sql);

        if(jExec.next(key))
        {
          // Found locks placed by this user himself.
          lockStatus = DLM.LOCKED_BY_SELF;

        }
        else
        { // Just return NOT_LOCKED.
          lockStatus = DLM.NOT_LOCKED;
        }

        jExec.closeData(key);

      } // if (jExec.next(key))

    }
    catch(Exception ex)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM : isLocked() : Exception = " + ex.getMessage();
      logger.error(msg);
      throw new Exception(msg, ex);
    }

    return lockStatus;
  }

  /**
   * doLock
   * Lock a particular deal by a certain user.
   * This method executes actual database update.
   * This method should never be exposed.
   * @param dealId int
   * @param userProfileId int
   * @param lockType char
   * @param lockReason String
   * @param srk SessionResourceKit
   * @param isForceToOverride boolean
   * @param isCalledInTransaction boolean
   * @throws Exception
   * @author Neil Oct/08/2004
   */
  protected void doLock(
    int dealId,
    int userProfileId,
    char lockType,
    String lockReason,
    SessionResourceKit srk,
    boolean isForceToOverride,
    boolean isCalledInTransaction) throws Exception
  {

    // Check if the lockReason is less than 50 characters.
    // If too many characters, trim it.
    lockReason = lockReason.trim();
    if(lockReason.length() > REASON_LENGTH_LIMIT)
    {
      lockReason = lockReason.substring(0, REASON_LENGTH_LIMIT - 1);
    }

    Date d = Calendar.getInstance().getTime();
    String lockTime = sqlStringFrom(d);

    // If lock is overridable, using UPDATE
    // If lock does not exist, using INSERT
    StringBuffer sqlBuff = null;
    switch(getLockStatus(dealId, userProfileId, srk))
    {

      case DLM.NOT_LOCKED: // Using INSERT SQL statement
        sqlBuff =
          new StringBuffer("INSERT INTO DEALLOCKS (DEALID, USERPROFILEID, " +
                "LOCKTYPE, LOCKTIME , REASON, INSTITUTIONPROFILEID) VALUES (");
        sqlBuff.append(dealId);
        sqlBuff.append(", ");
        sqlBuff.append(userProfileId);
        sqlBuff.append(", '");
        sqlBuff.append(Character.toUpperCase(lockType));
        sqlBuff.append("', sysdate");
        sqlBuff.append(", '");
        sqlBuff.append(lockReason);
        sqlBuff.append("', ");
        sqlBuff.append(srk.getExpressState().getDealInstitutionId());
        sqlBuff.append(")");
        break;

      case DLM.LOCKED_BY_SELF:

        // Using UPDATE SQL and no need for isForceToOverride
        sqlBuff = new StringBuffer("UPDATE DEALLOCKS SET ");
        sqlBuff.append("LOCKTYPE = '");
        sqlBuff.append(lockType);
        sqlBuff.append("', LOCKTIME = sysdate");
        sqlBuff.append(", REASON = '");
        sqlBuff.append(lockReason);
        sqlBuff.append("' WHERE DEALID = ");
        sqlBuff.append(dealId);
        sqlBuff.append(" AND USERPROFILEID = ");
        sqlBuff.append(userProfileId);
        break;

      case DLM.LOCKED_BUT_OVERRIDABLE: // Using UPDATE SQL statement
        if(isForceToOverride)
        {
          sqlBuff = new StringBuffer("UPDATE DEALLOCKS SET ");
          sqlBuff.append("LOCKTYPE = '");
          sqlBuff.append(lockType);
          sqlBuff.append("', LOCKTIME = sysdate");
          sqlBuff.append(", REASON = '");
          sqlBuff.append(lockReason);
          sqlBuff.append("' WHERE DEALID = ");
          sqlBuff.append(dealId);
          sqlBuff.append(" AND USERPROFILEID = ");
          sqlBuff.append(userProfileId);
          break;
        }
        else
        {
          String errMsg =
            "@DLM : doLock : To override the existing overridable lock, please set isForceToOverride to TRUE.";
          throw new Exception(errMsg);
        }

      case DLM.LOCKED_AND_UNOVERRIDABLE:
      default:
        String errMsg =
          "@DLM : doLock : The existing lock cannot be override.";
        throw new Exception(errMsg);
        //				break;
    }

    String sql = sqlBuff.toString();
    updateDb(srk, isCalledInTransaction, sql);
  }

  /**
   * placeLock
   * Place a regular lock.
   * @param dealId int
   * @param userProfileId int
   * @param lockReason String
   * @param srk SessionResourceKit
   * @param isForceToOverride boolean
   * @throws Exception
   * @author Neil 10/08/2004
   */
  public void placeLock(
    int dealId,
    int userProfileId,
    String lockReason,
    SessionResourceKit srk,
    boolean isForceToOverride) throws Exception
  {

    // Make sure when being invoked in production environment,
    // there is always transaction exists.
    boolean isCalledInTransaction = true;
    doLock(
      dealId,
      userProfileId,
      'L',
      lockReason,
      srk,
      isForceToOverride,
      isCalledInTransaction);
  }

  /**
   * placeSoftLock
   * Place a soft lock.
   * @param dealId int
   * @param userProfileId int
   * @param lockReason String
   * @param srk SessionResourceKit
   * @param isForceToOverride boolean
   * @throws Exception
   * @author Neil 10/08/2004
   */
  public void placeSoftLock(
    int dealId,
    int userProfileId,
    String lockReason,
    SessionResourceKit srk,
    boolean isForceToOverride) throws Exception
  {

    // Make sure when being invoked in production environment,
    // there is always transaction exists.
    boolean isCalledInTransaction = true;
    doLock(
      dealId,
      userProfileId,
      'S',
      lockReason,
      srk,
      isForceToOverride,
      isCalledInTransaction);

  }

  /**
   * doUnlock
   * Unlock a particular deal.
   * This method executes acutal database update.  It should never been exposed.
   * @param dealId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param isForceToOverride boolean
   * @param isCalledInTransaction boolean
   * @throws Exception
   * @author Neil 10/06/2004
   */
  public void doUnlock(
    int dealId,
    int userProfileId,
    SessionResourceKit srk,
    boolean isForceToOverride,
    boolean isCalledInTransaction) throws Exception
  {

    String errMsg = null;
    StringBuffer sqlBuff = null;
    String sql = null;
    switch(getLockStatus(dealId, userProfileId, srk))
    {

      case DLM.NOT_LOCKED:
        StringBuffer errMsgBuff =
          new StringBuffer("@DLM : doUnlock : No such lock(");
        errMsgBuff.append(dealId);
        errMsgBuff.append(", ");
        errMsgBuff.append(userProfileId);
        errMsgBuff.append(") exists.");
        errMsg = errMsgBuff.toString();
        throw new Exception(errMsg);
        //				break;

      case DLM.LOCKED_BY_SELF:
        if(dealId == EMPTY_DEAL_ID)
        {
          // No dealId specified.  Unlock all deals by the user.
          sqlBuff =
            new StringBuffer("DELETE FROM DEALLOCKS WHERE USERPROFILEID = ");
          sqlBuff.append(userProfileId);
        }
        else
        {
          // Unlock specified deal.
          sqlBuff =
            new StringBuffer("DELETE FROM DEALLOCKS WHERE DEALID = ");
          sqlBuff.append(dealId);
          sqlBuff.append(" AND USERPROFILEID = ");
          sqlBuff.append(userProfileId);
        }
        sql = sqlBuff.toString();

        updateDb(srk, isCalledInTransaction, sql);
        break;

      case DLM.LOCKED_BUT_OVERRIDABLE:
        if(isForceToOverride)
        {
          if(dealId == EMPTY_DEAL_ID)
          {
            // No dealId specified.  Unlock all deals by the user.
            sqlBuff =
              new StringBuffer("DELETE FROM DEALLOCKS WHERE USERPROFILEID = ");
            sqlBuff.append(userProfileId);
          }
          else
          {
            // Unlock specified deal.
            sqlBuff =
              new StringBuffer("DELETE FROM DEALLOCKS WHERE DEALID = ");
            sqlBuff.append(dealId);
            //						sqlBuff.append(" AND USERPROFILEID = ");
            //						sqlBuff.append(userProfileId);
          }
          sql = sqlBuff.toString();

          updateDb(srk, isCalledInTransaction, sql);
          break;

        }
        else
        {
          errMsg =
            "@DLM : doUnlock : To unlock the existing overridable lock, please set isForceToOverride to TRUE.";
          throw new Exception(errMsg);
        }
        //break;
      case DLM.LOCKED_AND_UNOVERRIDABLE:
      default:
        errMsg =
          "@DLM : doUnlock : The existing lock cannot be removed.";
        throw new Exception(errMsg);
        //break;
    }
  }

  /**
   * updateDb
   * Update database table MOS.DEALLOCKS
   * @param srk SessionResourceKit
   * @param isCalledInTransaction boolean
   * @param sqlStatement String
   * @throws Exception
   * @author Neil : 10/13/2004
   */
  protected synchronized void updateDb(
    SessionResourceKit srk,
    boolean isCalledInTransaction,
    String sqlStatement) throws Exception
  {

    String msg;
    JdbcExecutor jExec = null;

    try
    {
      waitTillReady();
      jExec = srk.getJdbcExecutor();

      // Check if transaction control needed.
      if(!isCalledInTransaction)
      {
        // Not called in transaction, need to start transaction manually.
        srk.beginTransaction();
        //srk.getJdbcExecutor().executeUpdate(sql);
        jExec.executeUpdate(sqlStatement);
        srk.commitTransaction();
      }
      else
      {
        // Automatical transaction started.
        jExec.executeUpdate(sqlStatement);
      }
    }
    catch(Exception ex)
    {
      if(!isCalledInTransaction)
      {
        srk.cleanTransaction(); // Rollback
      }
      SysLogger logger = srk.getSysLogger();
      msg = "@DLM.updateDb() : Exception = " + ex.getMessage();
      logger.error(msg);
      logger.error(ex);
      throw new Exception(msg);
    }
    /**finally {
       try {
        jExec.closeExecutor();
       } catch (Exception ex) {
        SysLogger logger = srk.getSysLogger();
        msg =
    "@DLM.updateDb() : When finalizing, Exception = "
     + ex.getMessage();
        logger.error(msg);
        logger.error(ex);
        throw new Exception(msg);
                      }
      } // try-catch-finally
    **/
  }

  /**
   * unlock
   * Unlock a specified deal with a user.
   * @param dealId int
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param isForceToOverride boolean
   * @param isCalledInTransaction boolean
   * @throws Exception
   * @author Neil : 10/08/2004
   */
  public void unlock(
    int dealId,
    int userProfileId,
    SessionResourceKit srk,
    boolean isForceToOverride,
    boolean isCalledInTransaction) throws Exception
  {
    try
    {
      doUnlock(
        dealId,
        userProfileId,
        srk,
        isForceToOverride,
        isCalledInTransaction);
    }
    catch(Exception ex)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.unlock() : Exception = " + ex.getMessage();
      logger.error(msg);
      throw new Exception(msg);
    }
  }

  /**
   * unlockAnyByUser
   * Unlock all deals related with the specified user.
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @param isForceToOverride boolean
   * @param isCalledInTransaction boolean
   * @throws Exception
   * @author Neil 10/08/2004
   */
  public void unlockAnyByUser(
    int userProfileId,
    SessionResourceKit srk,
    boolean isForceToOverride,
    boolean isCalledInTransaction) throws Exception
  {
    try
    {
      doUnlock(
        EMPTY_DEAL_ID,
        userProfileId,
        srk,
        isForceToOverride,
        isCalledInTransaction);
    }
    catch(Exception ex)
    {
      SysLogger logger = srk.getSysLogger();
      String msg =
        "@DLM.unlockAnyByUser() : Exception = " + ex.getMessage();
      logger.error(msg);
      throw new Exception(msg);
    }
  }

  //---------> Ticket#541--18Aug2004 Begins <---------//
  /**
   * getDealIdOfLockedDeal
   * @param userProfileId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return int
   * @author Vlad Oct/21/2004
   */
  public int getDealIdOfLockedDeal(int userProfileId, SessionResourceKit srk) throws Exception
  {
    String sql =
      "Select dealId from DEALLOCKS where userProfileId = "
      + userProfileId;

    int dealId = -1;

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        dealId = jExec.getInt(key, 1);
        break;
      }

      jExec.closeData(key);

    }
    catch(Exception e)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.isLocked() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg);
    }

    return dealId;
  }

  /**
   * getUserIdOfLockedDeal
   * @param dealId int
   * @param srk SessionResourceKit
   * @throws Exception
   * @return int
   * @author Vlad Oct/21/2004
   */
  public int getUserIdOfLockedDeal(int dealId, SessionResourceKit srk) throws Exception
  {
    String sql =
      "Select userProfileId from DEALLOCKS where dealId = " + dealId;

    int userProfileId = -1;

    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sql);

      for(; jExec.next(key); )
      {
        userProfileId = jExec.getInt(key, 1);
        break;
      }

      jExec.closeData(key);

    }
    catch(Exception e)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.isLocked() exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg);
    }

    return userProfileId;
  }

  //---------> Ticket#541--18Aug2004 Ends <---------//

  //---------> Cervus II : Section 5.2.2 Begins <---------//
  //--> By Neil : Nov/08/2004
  /**
   * getLockedTime
   * @param dealId
   * @param userProfileId
   * @param srk
   * @return
   * @throws Exception
   * @author Neil Nov/08/2004
   */
  public String getLockedTime(String dealId,
    String userProfileId,
    SessionResourceKit srk) throws Exception
  {
    String sql =
      "SELECT LOCKTIME FROM DEALLOCKS WHERE DEALID = "
      + dealId
      + " AND USERPROFILEID = "
      + userProfileId;
    Date date = null;
    try
    {
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(sql);
      if(jExec.next(key))
      {
        date = jExec.getDate(key, "LOCKTIME");
      }
      jExec.closeData(key);
      return sqlStringFrom(date);
    }
    catch(Exception e)
    {
      SysLogger logger = srk.getSysLogger();
      String msg = "@DLM.getLockedTime() : Exception = " + e.getMessage();
      logger.error(msg);
      throw new Exception(msg);
    }
  }

  /**
   * Converte Date to String.
   * @param d Date
   * @return
   */
  private String sqlStringFrom(java.util.Date d)
  {
    String strRep = TypeConverter.stringTypeFrom(d);

    if(strRep == null)
    {
      return "null";
    }
    else
    {
      return "\'" + strRep + "\'";
    }
  }
  
  
  //4.3GR Item MI Reponse Alert STRTS <--  CIBC CR63
    public void setMIResponseExpected(SessionResourceKit srk, int dealId, String flag) throws Exception {
        
        flag = ("N".equalsIgnoreCase(flag))? "N" : "Y"; 
        String query = "UPDATE DEALLOCKS SET MIRESPONSEEXPECTED = '" + flag + "' WHERE DEALID = " + dealId;

        try {
            updateDb(srk, false, query);
        } catch (Exception e) {
            SysLogger logger = srk.getSysLogger();
            String msg = "@DLM.setMIResponseExpected() : Exception = " + e.getMessage();
            logger.error(msg);
            throw new Exception(msg);
        }

    }
  
  /**
   * get MIResponseExpected from deallock
   */
  public String getMIResponseExpected(SessionResourceKit srk, int dealId,
          int userProfileId) throws Exception {

      String querySQL = "SELECT MIRESPONSEEXPECTED FROM DEALLOCKS WHERE DEALID = "
              + dealId + " and USERPROFILEID = " + userProfileId;

      String miResponse = null;
      JdbcExecutor jExec = srk.getJdbcExecutor();
      int key = jExec.execute(querySQL);

      for (; jExec.next(key);) {
          miResponse = jExec.getString(key, "MIRESPONSEEXPECTED");
      }

      return miResponse;
  }

  //4.3GR Item MI Reponse Alert ENDS <--  CIBC CR63

	/**
	 * FXP24959 workaround
	 * Due to the existing design flaw, DLM basically cannot handle users working on same deal simutaneously.
	 * Without redesigning DLM, this method is introduced to block other users from locking the same deal thru
	 * DLM if the deal is already locked.
	 * 
	 * This method will take advantage of SELECT FOR UPDATE from Oracle.  It will lock a row in table MasterDeal  
	 * which represent the given deal, and release the lock only when its transaction is complete/rollback.  While
	 * a row is being locked, any other transactions that try to execute SELECT FOR UPDATE for the same deal
	 * will be blocked, or in other words, a longer query execution time, til the row lock is being released.
	 * 
	 */
	private void syncMasterDeal(SessionResourceKit srk, int dealId) throws SQLException {
		Statement statement = null;
		ResultSet result = null;
		try {
			statement = srk.getConnection().createStatement();
			String sql = "SELECT dealid FROM masterdeal WHERE dealid="+dealId+" FOR UPDATE";
			srk.getSysLogger().debug(sql);
			result = statement.executeQuery(sql);
			if (!result.next())
				throw new ExpressRuntimeException("Unable to synchronize/lock using masterdeal table, dealId="+dealId+" does not exist in masterdeal table.");
		} finally {
			try {
				result.close();
			} catch (Exception ignore) {
			}
			try {
				statement.close();
			} catch (Exception ignore) {
			}
		}
	}
  
}
